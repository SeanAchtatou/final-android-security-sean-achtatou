package com.fsck.k9.preferences;

import android.os.Environment;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.preferences.Settings;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import yahoo.mail.app.R;

public class GlobalSettings {
    public static final Map<String, TreeMap<Integer, Settings.SettingsDescription>> SETTINGS;
    public static final Map<Integer, Settings.SettingsUpgrader> UPGRADERS;

    static {
        Map<String, TreeMap<Integer, Settings.SettingsDescription>> s = new LinkedHashMap<>();
        s.put("animations", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("attachmentdefaultpath", Settings.versions(new Settings.V(1, new DirectorySetting(Environment.getExternalStorageDirectory())), new Settings.V(41, new DirectorySetting(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)))));
        s.put("backgroundOperations", Settings.versions(new Settings.V(1, new Settings.EnumSetting(K9.BACKGROUND_OPS.class, K9.BACKGROUND_OPS.ALWAYS))));
        s.put("changeRegisteredNameColor", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("confirmDelete", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("confirmDeleteStarred", Settings.versions(new Settings.V(2, new Settings.BooleanSetting(false))));
        s.put("confirmSpam", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("countSearchMessages", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("enableDebugLogging", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("enableSensitiveLogging", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("fontSizeAccountDescription", Settings.versions(new Settings.V(1, new Settings.FontSizeSetting(-1))));
        s.put("fontSizeAccountName", Settings.versions(new Settings.V(1, new Settings.FontSizeSetting(-1))));
        s.put("fontSizeFolderName", Settings.versions(new Settings.V(1, new Settings.FontSizeSetting(-1))));
        s.put("fontSizeFolderStatus", Settings.versions(new Settings.V(1, new Settings.FontSizeSetting(-1))));
        s.put("fontSizeMessageComposeInput", Settings.versions(new Settings.V(5, new Settings.FontSizeSetting(-1))));
        s.put("fontSizeMessageListDate", Settings.versions(new Settings.V(1, new Settings.FontSizeSetting(-1))));
        s.put("fontSizeMessageListPreview", Settings.versions(new Settings.V(1, new Settings.FontSizeSetting(-1))));
        s.put("fontSizeMessageListSender", Settings.versions(new Settings.V(1, new Settings.FontSizeSetting(-1))));
        s.put("fontSizeMessageListSubject", Settings.versions(new Settings.V(1, new Settings.FontSizeSetting(-1))));
        s.put("fontSizeMessageViewAdditionalHeaders", Settings.versions(new Settings.V(1, new Settings.FontSizeSetting(-1))));
        s.put("fontSizeMessageViewCC", Settings.versions(new Settings.V(1, new Settings.FontSizeSetting(-1))));
        s.put("fontSizeMessageViewContent", Settings.versions(new Settings.V(1, new Settings.WebFontSizeSetting(3)), new Settings.V(31, null)));
        s.put("fontSizeMessageViewDate", Settings.versions(new Settings.V(1, new Settings.FontSizeSetting(-1))));
        s.put("fontSizeMessageViewSender", Settings.versions(new Settings.V(1, new Settings.FontSizeSetting(-1))));
        s.put("fontSizeMessageViewSubject", Settings.versions(new Settings.V(1, new Settings.FontSizeSetting(-1))));
        s.put("fontSizeMessageViewTime", Settings.versions(new Settings.V(1, new Settings.FontSizeSetting(-1))));
        s.put("fontSizeMessageViewTo", Settings.versions(new Settings.V(1, new Settings.FontSizeSetting(-1))));
        s.put("gesturesEnabled", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true)), new Settings.V(4, new Settings.BooleanSetting(false))));
        s.put("hideSpecialAccounts", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("keyguardPrivacy", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false)), new Settings.V(12, null)));
        s.put("language", Settings.versions(new Settings.V(1, new LanguageSetting())));
        s.put("measureAccounts", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("messageListCheckboxes", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("messageListPreviewLines", Settings.versions(new Settings.V(1, new Settings.IntegerRangeSetting(1, 100, 2))));
        s.put("messageListStars", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("messageViewFixedWidthFont", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("messageViewReturnToList", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("messageViewShowNext", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("quietTimeEnabled", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("quietTimeEnds", Settings.versions(new Settings.V(1, new TimeSetting("9:00"))));
        s.put("quietTimeStarts", Settings.versions(new Settings.V(1, new TimeSetting("21:00"))));
        s.put("registeredNameColor", Settings.versions(new Settings.V(1, new Settings.ColorSetting(-16777073))));
        s.put("showContactName", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("showCorrespondentNames", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("sortTypeEnum", Settings.versions(new Settings.V(10, new Settings.EnumSetting(Account.SortType.class, Account.DEFAULT_SORT_TYPE))));
        s.put("sortAscending", Settings.versions(new Settings.V(10, new Settings.BooleanSetting(false))));
        s.put("startIntegratedInbox", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("theme", Settings.versions(new Settings.V(1, new ThemeSetting(K9.Theme.LIGHT))));
        s.put("messageViewTheme", Settings.versions(new Settings.V(16, new ThemeSetting(K9.Theme.LIGHT)), new Settings.V(24, new SubThemeSetting(K9.Theme.USE_GLOBAL))));
        s.put("useVolumeKeysForListNavigation", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("useVolumeKeysForNavigation", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("wrapFolderNames", Settings.versions(new Settings.V(22, new Settings.BooleanSetting(false))));
        s.put("notificationHideSubject", Settings.versions(new Settings.V(12, new Settings.EnumSetting(K9.NotificationHideSubject.class, K9.NotificationHideSubject.NEVER))));
        s.put("useBackgroundAsUnreadIndicator", Settings.versions(new Settings.V(19, new Settings.BooleanSetting(true))));
        s.put("threadedView", Settings.versions(new Settings.V(20, new Settings.BooleanSetting(true))));
        s.put("splitViewMode", Settings.versions(new Settings.V(23, new Settings.EnumSetting(K9.SplitViewMode.class, K9.SplitViewMode.NEVER))));
        s.put("messageComposeTheme", Settings.versions(new Settings.V(24, new SubThemeSetting(K9.Theme.USE_GLOBAL))));
        s.put("fixedMessageViewTheme", Settings.versions(new Settings.V(24, new Settings.BooleanSetting(true))));
        s.put("showContactPicture", Settings.versions(new Settings.V(25, new Settings.BooleanSetting(true))));
        s.put("autofitWidth", Settings.versions(new Settings.V(28, new Settings.BooleanSetting(true))));
        s.put("colorizeMissingContactPictures", Settings.versions(new Settings.V(29, new Settings.BooleanSetting(true))));
        s.put("messageViewDeleteActionVisible", Settings.versions(new Settings.V(30, new Settings.BooleanSetting(true))));
        s.put("messageViewArchiveActionVisible", Settings.versions(new Settings.V(30, new Settings.BooleanSetting(false))));
        s.put("messageViewMoveActionVisible", Settings.versions(new Settings.V(30, new Settings.BooleanSetting(false))));
        s.put("messageViewCopyActionVisible", Settings.versions(new Settings.V(30, new Settings.BooleanSetting(false))));
        s.put("messageViewSpamActionVisible", Settings.versions(new Settings.V(30, new Settings.BooleanSetting(false))));
        s.put("fontSizeMessageViewContentPercent", Settings.versions(new Settings.V(31, new Settings.IntegerRangeSetting(40, 250, 100))));
        s.put("hideUserAgent", Settings.versions(new Settings.V(32, new Settings.BooleanSetting(false))));
        s.put("hideTimeZone", Settings.versions(new Settings.V(32, new Settings.BooleanSetting(false))));
        s.put("lockScreenNotificationVisibility", Settings.versions(new Settings.V(37, new Settings.EnumSetting(K9.LockScreenNotificationVisibility.class, K9.LockScreenNotificationVisibility.MESSAGE_COUNT))));
        s.put("confirmDeleteFromNotification", Settings.versions(new Settings.V(38, new Settings.BooleanSetting(true))));
        s.put("messageListSenderAboveSubject", Settings.versions(new Settings.V(38, new Settings.BooleanSetting(true))));
        s.put("notificationQuickDelete", Settings.versions(new Settings.V(38, new Settings.EnumSetting(K9.NotificationQuickDelete.class, K9.NotificationQuickDelete.ALWAYS))));
        s.put("notificationDuringQuietTimeEnabled", Settings.versions(new Settings.V(39, new Settings.BooleanSetting(true))));
        s.put("confirmDiscardMessage", Settings.versions(new Settings.V(40, new Settings.BooleanSetting(true))));
        SETTINGS = Collections.unmodifiableMap(s);
        Map<Integer, Settings.SettingsUpgrader> u = new HashMap<>();
        u.put(12, new SettingsUpgraderV12());
        u.put(24, new SettingsUpgraderV24());
        u.put(31, new SettingsUpgraderV31());
        UPGRADERS = Collections.unmodifiableMap(u);
    }

    public static Map<String, Object> validate(int version, Map<String, String> importedSettings) {
        return Settings.validate(version, SETTINGS, importedSettings, false);
    }

    public static Set<String> upgrade(int version, Map<String, Object> validatedSettings) {
        return Settings.upgrade(version, UPGRADERS, SETTINGS, validatedSettings);
    }

    public static Map<String, String> convert(Map<String, Object> settings) {
        return Settings.convert(settings, SETTINGS);
    }

    public static Map<String, String> getGlobalSettings(Storage storage) {
        Map<String, String> result = new HashMap<>();
        for (String key : SETTINGS.keySet()) {
            String value = storage.getString(key, null);
            if (value != null) {
                result.put(key, value);
            }
        }
        return result;
    }

    public static class SettingsUpgraderV12 implements Settings.SettingsUpgrader {
        public Set<String> upgrade(Map<String, Object> settings) {
            Boolean keyguardPrivacy = (Boolean) settings.get("keyguardPrivacy");
            if (keyguardPrivacy == null || !keyguardPrivacy.booleanValue()) {
                settings.put("notificationHideSubject", K9.NotificationHideSubject.NEVER);
            } else {
                settings.put("notificationHideSubject", K9.NotificationHideSubject.WHEN_LOCKED);
            }
            return new HashSet(Arrays.asList("keyguardPrivacy"));
        }
    }

    public static class SettingsUpgraderV24 implements Settings.SettingsUpgrader {
        public Set<String> upgrade(Map<String, Object> settings) {
            K9.Theme messageViewTheme = (K9.Theme) settings.get("messageViewTheme");
            K9.Theme theme = (K9.Theme) settings.get("theme");
            if (theme == null || messageViewTheme == null || theme != messageViewTheme) {
                return null;
            }
            settings.put("messageViewTheme", K9.Theme.USE_GLOBAL);
            return null;
        }
    }

    public static class SettingsUpgraderV31 implements Settings.SettingsUpgrader {
        public Set<String> upgrade(Map<String, Object> settings) {
            settings.put("fontSizeMessageViewContentPercent", Integer.valueOf(convertFromOldSize(((Integer) settings.get("fontSizeMessageViewContent")).intValue())));
            return new HashSet(Arrays.asList("fontSizeMessageViewContent"));
        }

        public static int convertFromOldSize(int oldSize) {
            switch (oldSize) {
                case 1:
                    return 40;
                case 2:
                    return 75;
                case 3:
                default:
                    return 100;
                case 4:
                    return 175;
                case 5:
                    return 250;
            }
        }
    }

    public static class LanguageSetting extends Settings.PseudoEnumSetting<String> {
        private final Map<String, String> mMapping;

        public LanguageSetting() {
            super("");
            Map<String, String> mapping = new HashMap<>();
            for (String value : K9.app.getResources().getStringArray(R.array.settings_language_values)) {
                if (value.length() == 0) {
                    mapping.put("", "default");
                } else {
                    mapping.put(value, value);
                }
            }
            this.mMapping = Collections.unmodifiableMap(mapping);
        }

        /* access modifiers changed from: protected */
        public Map<String, String> getMapping() {
            return this.mMapping;
        }

        public Object fromString(String value) throws Settings.InvalidSettingValueException {
            if (this.mMapping.containsKey(value)) {
                return value;
            }
            throw new Settings.InvalidSettingValueException();
        }
    }

    public static class ThemeSetting extends Settings.SettingsDescription {
        private static final String THEME_DARK = "dark";
        private static final String THEME_LIGHT = "light";

        public ThemeSetting(K9.Theme defaultValue) {
            super(defaultValue);
        }

        public Object fromString(String value) throws Settings.InvalidSettingValueException {
            try {
                Integer theme = Integer.valueOf(Integer.parseInt(value));
                if (theme.intValue() == K9.Theme.LIGHT.ordinal() || theme.intValue() == 16973836) {
                    return K9.Theme.LIGHT;
                }
                if (theme.intValue() == K9.Theme.DARK.ordinal() || theme.intValue() == 16973829) {
                    return K9.Theme.DARK;
                }
                throw new Settings.InvalidSettingValueException();
            } catch (NumberFormatException e) {
            }
        }

        public Object fromPrettyString(String value) throws Settings.InvalidSettingValueException {
            if (THEME_LIGHT.equals(value)) {
                return K9.Theme.LIGHT;
            }
            if (THEME_DARK.equals(value)) {
                return K9.Theme.DARK;
            }
            throw new Settings.InvalidSettingValueException();
        }

        public String toPrettyString(Object value) {
            switch ((K9.Theme) value) {
                case DARK:
                    return THEME_DARK;
                default:
                    return THEME_LIGHT;
            }
        }

        public String toString(Object value) {
            return Integer.toString(((K9.Theme) value).ordinal());
        }
    }

    public static class SubThemeSetting extends ThemeSetting {
        private static final String THEME_USE_GLOBAL = "use_global";

        public SubThemeSetting(K9.Theme defaultValue) {
            super(defaultValue);
        }

        public Object fromString(String value) throws Settings.InvalidSettingValueException {
            try {
                if (Integer.valueOf(Integer.parseInt(value)).intValue() == K9.Theme.USE_GLOBAL.ordinal()) {
                    return K9.Theme.USE_GLOBAL;
                }
                return super.fromString(value);
            } catch (NumberFormatException e) {
                throw new Settings.InvalidSettingValueException();
            }
        }

        public Object fromPrettyString(String value) throws Settings.InvalidSettingValueException {
            if (THEME_USE_GLOBAL.equals(value)) {
                return K9.Theme.USE_GLOBAL;
            }
            return super.fromPrettyString(value);
        }

        public String toPrettyString(Object value) {
            if (((K9.Theme) value) == K9.Theme.USE_GLOBAL) {
                return THEME_USE_GLOBAL;
            }
            return super.toPrettyString(value);
        }
    }

    public static class TimeSetting extends Settings.SettingsDescription {
        public TimeSetting(String defaultValue) {
            super(defaultValue);
        }

        public Object fromString(String value) throws Settings.InvalidSettingValueException {
            if (value.matches(TimePickerPreference.VALIDATION_EXPRESSION)) {
                return value;
            }
            throw new Settings.InvalidSettingValueException();
        }
    }

    public static class DirectorySetting extends Settings.SettingsDescription {
        public DirectorySetting(File defaultPath) {
            super(defaultPath.toString());
        }

        public Object fromString(String value) throws Settings.InvalidSettingValueException {
            try {
                if (new File(value).isDirectory()) {
                    return value;
                }
            } catch (Exception e) {
            }
            throw new Settings.InvalidSettingValueException();
        }
    }
}
