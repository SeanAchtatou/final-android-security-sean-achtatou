package com.fsck.k9.preferences;

import android.content.SharedPreferences;
import android.util.Log;
import com.fsck.k9.K9;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StorageEditor {
    /* access modifiers changed from: private */
    public Map<String, String> changes = new HashMap();
    /* access modifiers changed from: private */
    public List<String> removals = new ArrayList();
    Map<String, String> snapshot = new HashMap();
    /* access modifiers changed from: private */
    public Storage storage;

    StorageEditor(Storage storage2) {
        this.storage = storage2;
        this.snapshot.putAll(storage2.getAll());
    }

    public void copy(SharedPreferences input) {
        for (Map.Entry<String, ?> entry : input.getAll().entrySet()) {
            String key = (String) entry.getKey();
            Object value = entry.getValue();
            if (key != null && value != null) {
                if (K9.DEBUG) {
                    Log.d("k9", "Copying key '" + key + "', value '" + value + "'");
                }
                this.changes.put(key, "" + value);
            } else if (K9.DEBUG) {
                Log.d("k9", "Skipping copying key '" + key + "', value '" + value + "'");
            }
        }
    }

    public boolean commit() {
        try {
            commitChanges();
            return true;
        } catch (Exception e) {
            Log.e("k9", "Failed to save preferences", e);
            return false;
        }
    }

    private void commitChanges() {
        long startTime = System.currentTimeMillis();
        Log.i("k9", "Committing preference changes");
        this.storage.doInTransaction(new Runnable() {
            public void run() {
                for (String removeKey : StorageEditor.this.removals) {
                    StorageEditor.this.storage.remove(removeKey);
                }
                Map<String, String> insertables = new HashMap<>();
                for (Map.Entry<String, String> entry : StorageEditor.this.changes.entrySet()) {
                    String key = (String) entry.getKey();
                    String newValue = (String) entry.getValue();
                    String oldValue = StorageEditor.this.snapshot.get(key);
                    if (StorageEditor.this.removals.contains(key) || !newValue.equals(oldValue)) {
                        insertables.put(key, newValue);
                    }
                }
                StorageEditor.this.storage.put(insertables);
            }
        });
        Log.i("k9", "Preferences commit took " + (System.currentTimeMillis() - startTime) + "ms");
    }

    public StorageEditor putBoolean(String key, boolean value) {
        this.changes.put(key, "" + value);
        return this;
    }

    public StorageEditor putInt(String key, int value) {
        this.changes.put(key, "" + value);
        return this;
    }

    public StorageEditor putLong(String key, long value) {
        this.changes.put(key, "" + value);
        return this;
    }

    public StorageEditor putString(String key, String value) {
        if (value == null) {
            remove(key);
        } else {
            this.changes.put(key, value);
        }
        return this;
    }

    public StorageEditor remove(String key) {
        this.removals.add(key);
        return this;
    }
}
