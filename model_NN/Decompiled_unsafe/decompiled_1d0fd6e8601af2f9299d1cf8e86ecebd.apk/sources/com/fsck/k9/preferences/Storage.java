package com.fsck.k9.preferences;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import com.fsck.k9.K9;
import com.fsck.k9.helper.UrlEncodingHelper;
import com.fsck.k9.helper.Utility;
import com.fsck.k9.mail.filter.Base64;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class Storage {
    private static ConcurrentMap<Context, Storage> storages = new ConcurrentHashMap();
    private String DB_NAME = "preferences_storage";
    private int DB_VERSION = 2;
    private Context context = null;
    private volatile ConcurrentMap<String, String> storage = new ConcurrentHashMap();
    private ThreadLocal<List<String>> workingChangedKeys = new ThreadLocal<>();
    private ThreadLocal<SQLiteDatabase> workingDB = new ThreadLocal<>();
    private ThreadLocal<ConcurrentMap<String, String>> workingStorage = new ThreadLocal<>();

    private SQLiteDatabase openDB() {
        SQLiteDatabase mDb = this.context.openOrCreateDatabase(this.DB_NAME, 0, null);
        if (mDb.getVersion() == 1) {
            Log.i("k9", "Updating preferences to urlencoded username/password");
            String accountUuids = readValue(mDb, "accountUuids");
            if (!(accountUuids == null || accountUuids.length() == 0)) {
                String[] uuids = accountUuids.split(",");
                int length = uuids.length;
                for (int i = 0; i < length; i++) {
                    String uuid = uuids[i];
                    try {
                        String storeUriStr = Base64.decode(readValue(mDb, uuid + ".storeUri"));
                        String transportUriStr = Base64.decode(readValue(mDb, uuid + ".transportUri"));
                        URI uri = new URI(transportUriStr);
                        String newUserInfo = null;
                        if (transportUriStr != null) {
                            String[] userInfoParts = uri.getUserInfo().split(":");
                            String usernameEnc = UrlEncodingHelper.encodeUtf8(userInfoParts[0]);
                            String passwordEnc = "";
                            String authType = "";
                            if (userInfoParts.length > 1) {
                                passwordEnc = ":" + UrlEncodingHelper.encodeUtf8(userInfoParts[1]);
                            }
                            if (userInfoParts.length > 2) {
                                authType = ":" + userInfoParts[2];
                            }
                            newUserInfo = usernameEnc + passwordEnc + authType;
                        }
                        if (newUserInfo != null) {
                            writeValue(mDb, uuid + ".transportUri", Base64.encode(new URI(uri.getScheme(), newUserInfo, uri.getHost(), uri.getPort(), uri.getPath(), uri.getQuery(), uri.getFragment()).toString()));
                        }
                        URI uri2 = new URI(storeUriStr);
                        String newUserInfo2 = null;
                        if (storeUriStr.startsWith("imap")) {
                            String[] userInfoParts2 = uri2.getUserInfo().split(":");
                            if (userInfoParts2.length == 2) {
                                newUserInfo2 = UrlEncodingHelper.encodeUtf8(userInfoParts2[0]) + ":" + UrlEncodingHelper.encodeUtf8(userInfoParts2[1]);
                            } else {
                                newUserInfo2 = userInfoParts2[0] + ":" + UrlEncodingHelper.encodeUtf8(userInfoParts2[1]) + ":" + UrlEncodingHelper.encodeUtf8(userInfoParts2[2]);
                            }
                        } else if (storeUriStr.startsWith("pop3")) {
                            String[] userInfoParts3 = uri2.getUserInfo().split(":", 2);
                            String usernameEnc2 = UrlEncodingHelper.encodeUtf8(userInfoParts3[0]);
                            String passwordEnc2 = "";
                            if (userInfoParts3.length > 1) {
                                passwordEnc2 = ":" + UrlEncodingHelper.encodeUtf8(userInfoParts3[1]);
                            }
                            newUserInfo2 = usernameEnc2 + passwordEnc2;
                        } else if (storeUriStr.startsWith("webdav")) {
                            String[] userInfoParts4 = uri2.getUserInfo().split(":", 2);
                            String usernameEnc3 = UrlEncodingHelper.encodeUtf8(userInfoParts4[0]);
                            String passwordEnc3 = "";
                            if (userInfoParts4.length > 1) {
                                passwordEnc3 = ":" + UrlEncodingHelper.encodeUtf8(userInfoParts4[1]);
                            }
                            newUserInfo2 = usernameEnc3 + passwordEnc3;
                        }
                        if (newUserInfo2 != null) {
                            writeValue(mDb, uuid + ".storeUri", Base64.encode(new URI(uri2.getScheme(), newUserInfo2, uri2.getHost(), uri2.getPort(), uri2.getPath(), uri2.getQuery(), uri2.getFragment()).toString()));
                        }
                    } catch (Exception e) {
                        Log.e("k9", "ooops", e);
                    }
                }
            }
            mDb.setVersion(this.DB_VERSION);
        }
        if (mDb.getVersion() != this.DB_VERSION) {
            Log.i("k9", "Creating Storage database");
            mDb.execSQL("DROP TABLE IF EXISTS preferences_storage");
            mDb.execSQL("CREATE TABLE preferences_storage (primkey TEXT PRIMARY KEY ON CONFLICT REPLACE, value TEXT)");
            mDb.setVersion(this.DB_VERSION);
        }
        return mDb;
    }

    public static Storage getStorage(Context context2) {
        Storage tmpStorage = storages.get(context2);
        if (tmpStorage != null) {
            if (K9.DEBUG) {
                Log.d("k9", "Returning already existing Storage");
            }
            return tmpStorage;
        }
        if (K9.DEBUG) {
            Log.d("k9", "Creating provisional storage");
        }
        Storage tmpStorage2 = new Storage(context2);
        Storage oldStorage = storages.putIfAbsent(context2, tmpStorage2);
        if (oldStorage == null) {
            if (K9.DEBUG) {
                Log.d("k9", "Returning the Storage we created");
            }
            return tmpStorage2;
        } else if (!K9.DEBUG) {
            return oldStorage;
        } else {
            Log.d("k9", "Another thread beat us to creating the Storage, returning that one");
            return oldStorage;
        }
    }

    private void loadValues() {
        String str;
        String str2;
        String str3;
        long startTime = System.currentTimeMillis();
        Log.i("k9", "Loading preferences from DB into Storage");
        Cursor cursor = null;
        SQLiteDatabase mDb = null;
        try {
            mDb = openDB();
            cursor = mDb.rawQuery("SELECT primkey, value FROM preferences_storage", null);
            while (cursor.moveToNext()) {
                String key = cursor.getString(0);
                String value = cursor.getString(1);
                if (K9.DEBUG) {
                    Log.d("k9", "Loading key '" + key + "', value = '" + value + "'");
                }
                this.storage.put(key, value);
            }
        } finally {
            Utility.closeQuietly(cursor);
            if (mDb != null) {
                mDb.close();
            }
            str = "k9";
            str2 = "Preferences load took ";
            str3 = "ms";
            Log.i(str, str2 + (System.currentTimeMillis() - startTime) + str3);
        }
    }

    private Storage(Context context2) {
        this.context = context2;
        loadValues();
    }

    private void keyChange(String key) {
        List<String> changedKeys = this.workingChangedKeys.get();
        if (!changedKeys.contains(key)) {
            changedKeys.add(key);
        }
    }

    /* access modifiers changed from: package-private */
    public void put(Map<String, String> insertables) {
        SQLiteStatement stmt = this.workingDB.get().compileStatement("INSERT INTO preferences_storage (primkey, value) VALUES (?, ?)");
        for (Map.Entry<String, String> entry : insertables.entrySet()) {
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            stmt.bindString(1, key);
            stmt.bindString(2, value);
            stmt.execute();
            stmt.clearBindings();
            liveUpdate(key, value);
        }
        stmt.close();
    }

    private void liveUpdate(String key, String value) {
        this.workingStorage.get().put(key, value);
        keyChange(key);
    }

    /* access modifiers changed from: package-private */
    public void remove(String key) {
        this.workingDB.get().delete("preferences_storage", "primkey = ?", new String[]{key});
        this.workingStorage.get().remove(key);
        keyChange(key);
    }

    /* access modifiers changed from: package-private */
    public void doInTransaction(Runnable dbWork) {
        ConcurrentMap<String, String> newStorage = new ConcurrentHashMap<>();
        newStorage.putAll(this.storage);
        this.workingStorage.set(newStorage);
        SQLiteDatabase mDb = openDB();
        this.workingDB.set(mDb);
        this.workingChangedKeys.set(new ArrayList<>());
        mDb.beginTransaction();
        try {
            dbWork.run();
            mDb.setTransactionSuccessful();
            this.storage = newStorage;
        } finally {
            this.workingDB.remove();
            this.workingStorage.remove();
            this.workingChangedKeys.remove();
            mDb.endTransaction();
            mDb.close();
        }
    }

    public boolean isEmpty() {
        return this.storage.isEmpty();
    }

    public boolean contains(String key) {
        return this.storage.containsKey(key);
    }

    public StorageEditor edit() {
        return new StorageEditor(this);
    }

    public Map<String, String> getAll() {
        return this.storage;
    }

    public boolean getBoolean(String key, boolean defValue) {
        String val = this.storage.get(key);
        return val == null ? defValue : Boolean.parseBoolean(val);
    }

    public int getInt(String key, int defValue) {
        String val = this.storage.get(key);
        if (val == null) {
            return defValue;
        }
        try {
            return Integer.parseInt(val);
        } catch (NumberFormatException nfe) {
            Log.e("k9", "Could not parse int", nfe);
            return defValue;
        }
    }

    public long getLong(String key, long defValue) {
        String val = this.storage.get(key);
        if (val == null) {
            return defValue;
        }
        try {
            return Long.parseLong(val);
        } catch (NumberFormatException nfe) {
            Log.e("k9", "Could not parse long", nfe);
            return defValue;
        }
    }

    public String getString(String key, String defValue) {
        String val = this.storage.get(key);
        return val == null ? defValue : val;
    }

    private String readValue(SQLiteDatabase mDb, String key) {
        Cursor cursor = null;
        String value = null;
        try {
            cursor = mDb.query("preferences_storage", new String[]{SettingsExporter.VALUE_ELEMENT}, "primkey = ?", new String[]{key}, null, null, null);
            if (cursor.moveToNext()) {
                value = cursor.getString(0);
                if (K9.DEBUG) {
                    Log.d("k9", "Loading key '" + key + "', value = '" + value + "'");
                }
            }
            return value;
        } finally {
            Utility.closeQuietly(cursor);
        }
    }

    private void writeValue(SQLiteDatabase mDb, String key, String value) {
        ContentValues cv = new ContentValues();
        cv.put("primkey", key);
        cv.put(SettingsExporter.VALUE_ELEMENT, value);
        if (mDb.insert("preferences_storage", "primkey", cv) == -1) {
            Log.e("k9", "Error writing key '" + key + "', value = '" + value + "'");
        }
    }
}
