package com.fsck.k9.ui.compose;

import android.text.TextWatcher;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import com.fsck.k9.FontSizes;
import com.fsck.k9.activity.MessageCompose;
import com.fsck.k9.mailstore.AttachmentResolver;
import com.fsck.k9.message.QuotedTextMode;
import com.fsck.k9.message.SimpleMessageFormat;
import com.fsck.k9.ui.EolConvertingEditText;
import com.fsck.k9.view.MessageWebView;
import yahoo.mail.app.R;

public class QuotedMessageMvpView {
    private final EolConvertingEditText mMessageContentView;
    private final MessageWebView mQuotedHTML;
    private final EolConvertingEditText mQuotedText;
    private final View mQuotedTextBar;
    private final ImageButton mQuotedTextDelete;
    private final ImageButton mQuotedTextEdit;
    private final Button mQuotedTextShow;

    public QuotedMessageMvpView(MessageCompose messageCompose) {
        this.mQuotedTextShow = (Button) messageCompose.findViewById(R.id.quoted_text_show);
        this.mQuotedTextBar = messageCompose.findViewById(R.id.quoted_text_bar);
        this.mQuotedTextEdit = (ImageButton) messageCompose.findViewById(R.id.quoted_text_edit);
        this.mQuotedTextDelete = (ImageButton) messageCompose.findViewById(R.id.quoted_text_delete);
        this.mQuotedText = (EolConvertingEditText) messageCompose.findViewById(R.id.quoted_text);
        this.mQuotedText.getInputExtras(true).putBoolean("allowEmoji", true);
        this.mQuotedHTML = (MessageWebView) messageCompose.findViewById(R.id.quoted_html);
        this.mQuotedHTML.configure();
        this.mQuotedHTML.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }
        });
        this.mMessageContentView = (EolConvertingEditText) messageCompose.findViewById(R.id.message_content);
    }

    public void setOnClickPresenter(final QuotedMessagePresenter presenter) {
        View.OnClickListener onClickListener = new View.OnClickListener() {
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.quoted_text_show /*2131755288*/:
                        presenter.onClickShowQuotedText();
                        return;
                    case R.id.quoted_text_edit /*2131755293*/:
                        presenter.onClickEditQuotedText();
                        return;
                    case R.id.quoted_text_delete /*2131755294*/:
                        presenter.onClickDeleteQuotedText();
                        return;
                    default:
                        return;
                }
            }
        };
        this.mQuotedTextShow.setOnClickListener(onClickListener);
        this.mQuotedTextEdit.setOnClickListener(onClickListener);
        this.mQuotedTextDelete.setOnClickListener(onClickListener);
    }

    public void addTextChangedListener(TextWatcher draftNeedsChangingTextWatcher) {
        this.mQuotedText.addTextChangedListener(draftNeedsChangingTextWatcher);
    }

    public void showOrHideQuotedText(QuotedTextMode mode, SimpleMessageFormat quotedTextFormat) {
        switch (mode) {
            case NONE:
                this.mQuotedTextShow.setVisibility(8);
                this.mQuotedTextBar.setVisibility(8);
                this.mQuotedText.setVisibility(8);
                this.mQuotedHTML.setVisibility(8);
                this.mQuotedTextEdit.setVisibility(8);
                return;
            case HIDE:
                this.mQuotedTextShow.setVisibility(0);
                this.mQuotedTextBar.setVisibility(8);
                this.mQuotedText.setVisibility(8);
                this.mQuotedHTML.setVisibility(8);
                this.mQuotedTextEdit.setVisibility(8);
                return;
            case SHOW:
                this.mQuotedTextShow.setVisibility(8);
                this.mQuotedTextBar.setVisibility(0);
                if (quotedTextFormat == SimpleMessageFormat.HTML) {
                    this.mQuotedText.setVisibility(8);
                    this.mQuotedHTML.setVisibility(0);
                    this.mQuotedTextEdit.setVisibility(0);
                    return;
                }
                this.mQuotedText.setVisibility(0);
                this.mQuotedHTML.setVisibility(8);
                this.mQuotedTextEdit.setVisibility(8);
                return;
            default:
                return;
        }
    }

    public void setFontSizes(FontSizes mFontSizes, int fontSize) {
        mFontSizes.setViewTextSize(this.mQuotedText, fontSize);
    }

    public void setQuotedHtml(String quotedContent, AttachmentResolver attachmentResolver) {
        this.mQuotedHTML.displayHtmlContentWithInlineAttachments(quotedContent, attachmentResolver, null);
    }

    public void setQuotedText(String quotedText) {
        this.mQuotedText.setCharacters(quotedText);
    }

    public String getQuotedText() {
        return this.mQuotedText.getCharacters();
    }

    public void setMessageContentCharacters(String text) {
        this.mMessageContentView.setCharacters(text);
    }

    public void setMessageContentCursorPosition(int messageContentCursorPosition) {
        this.mMessageContentView.setSelection(messageContentCursorPosition);
    }
}
