package com.fsck.k9.ui.messageview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.fsck.k9.helper.SizeFormatter;
import com.fsck.k9.mailstore.AttachmentViewInfo;
import yahoo.mail.app.R;

public class AttachmentView extends FrameLayout implements View.OnClickListener, View.OnLongClickListener {
    private AttachmentViewInfo attachment;
    private AttachmentViewCallback callback;
    private Button downloadButton;
    private Button viewButton;

    public AttachmentView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public AttachmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AttachmentView(Context context) {
        super(context);
    }

    public AttachmentViewInfo getAttachment() {
        return this.attachment;
    }

    public void enableButtons() {
        this.viewButton.setEnabled(true);
        this.downloadButton.setEnabled(true);
    }

    public void disableButtons() {
        this.viewButton.setEnabled(false);
        this.downloadButton.setEnabled(false);
    }

    public void setAttachment(AttachmentViewInfo attachment2) {
        this.attachment = attachment2;
        displayAttachmentInformation();
    }

    private void displayAttachmentInformation() {
        this.viewButton = (Button) findViewById(R.id.view);
        this.downloadButton = (Button) findViewById(R.id.download);
        if (this.attachment.size > 134217728) {
            this.viewButton.setVisibility(8);
            this.downloadButton.setVisibility(8);
        }
        this.viewButton.setOnClickListener(this);
        this.downloadButton.setOnClickListener(this);
        this.downloadButton.setOnLongClickListener(this);
        ((TextView) findViewById(R.id.attachment_name)).setText(this.attachment.displayName);
        setAttachmentSize(this.attachment.size);
        refreshThumbnail();
    }

    private void setAttachmentSize(long size) {
        TextView attachmentSize = (TextView) findViewById(R.id.attachment_info);
        if (size == -1) {
            attachmentSize.setText("");
        } else {
            attachmentSize.setText(SizeFormatter.formatSize(getContext(), size));
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.download /*2131755362*/:
                onSaveButtonClick();
                return;
            case R.id.view /*2131755363*/:
                onViewButtonClick();
                return;
            default:
                return;
        }
    }

    public boolean onLongClick(View view) {
        if (view.getId() != R.id.download) {
            return false;
        }
        onSaveButtonLongClick();
        return true;
    }

    private void onViewButtonClick() {
        this.callback.onViewAttachment(this.attachment);
    }

    private void onSaveButtonClick() {
        this.callback.onSaveAttachment(this.attachment);
    }

    private void onSaveButtonLongClick() {
        this.callback.onSaveAttachmentToUserProvidedDirectory(this.attachment);
    }

    public void setCallback(AttachmentViewCallback callback2) {
        this.callback = callback2;
    }

    public void refreshThumbnail() {
        Glide.with(getContext()).load(this.attachment.internalUri).placeholder((int) R.drawable.attached_image_placeholder).centerCrop().into((ImageView) findViewById(R.id.attachment_icon));
    }
}
