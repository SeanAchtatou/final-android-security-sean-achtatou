package com.fsck.k9.ui.messageview;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.AttrRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import com.fsck.k9.view.MessageCryptoDisplayStatus;
import com.fsck.k9.view.ThemeUtils;
import yahoo.mail.app.R;

public class CryptoInfoDialog extends DialogFragment {
    public static final String ARG_DISPLAY_STATUS = "display_status";
    public static final int ICON_ANIM_DELAY = 400;
    public static final int ICON_ANIM_DURATION = 350;
    /* access modifiers changed from: private */
    public View bottomIconFrame;
    private ImageView bottomIcon_1;
    private ImageView bottomIcon_2;
    /* access modifiers changed from: private */
    public TextView bottomText;
    private View dialogView;
    /* access modifiers changed from: private */
    public View topIconFrame;
    private ImageView topIcon_1;
    private ImageView topIcon_2;
    private ImageView topIcon_3;
    /* access modifiers changed from: private */
    public TextView topText;

    public interface OnClickShowCryptoKeyListener {
        void onClickShowCryptoKey();
    }

    public static CryptoInfoDialog newInstance(MessageCryptoDisplayStatus displayStatus) {
        CryptoInfoDialog frag = new CryptoInfoDialog();
        Bundle args = new Bundle();
        args.putString(ARG_DISPLAY_STATUS, displayStatus.toString());
        frag.setArguments(args);
        return frag;
    }

    @SuppressLint({"InflateParams"})
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
        this.dialogView = LayoutInflater.from(getActivity()).inflate((int) R.layout.message_crypto_info_dialog, (ViewGroup) null);
        this.topIconFrame = this.dialogView.findViewById(R.id.crypto_info_top_frame);
        this.topIcon_1 = (ImageView) this.topIconFrame.findViewById(R.id.crypto_info_top_icon_1);
        this.topIcon_2 = (ImageView) this.topIconFrame.findViewById(R.id.crypto_info_top_icon_2);
        this.topIcon_3 = (ImageView) this.topIconFrame.findViewById(R.id.crypto_info_top_icon_3);
        this.topText = (TextView) this.dialogView.findViewById(R.id.crypto_info_top_text);
        this.bottomIconFrame = this.dialogView.findViewById(R.id.crypto_info_bottom_frame);
        this.bottomIcon_1 = (ImageView) this.bottomIconFrame.findViewById(R.id.crypto_info_bottom_icon_1);
        this.bottomIcon_2 = (ImageView) this.bottomIconFrame.findViewById(R.id.crypto_info_bottom_icon_2);
        this.bottomText = (TextView) this.dialogView.findViewById(R.id.crypto_info_bottom_text);
        MessageCryptoDisplayStatus displayStatus = MessageCryptoDisplayStatus.valueOf(getArguments().getString(ARG_DISPLAY_STATUS));
        setMessageForDisplayStatus(displayStatus);
        b.setView(this.dialogView);
        b.setPositiveButton((int) R.string.crypto_info_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                CryptoInfoDialog.this.dismiss();
            }
        });
        if (displayStatus.hasAssociatedKey()) {
            b.setNeutralButton((int) R.string.crypto_info_view_key, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    Fragment frag = CryptoInfoDialog.this.getTargetFragment();
                    if (!(frag instanceof OnClickShowCryptoKeyListener)) {
                        throw new AssertionError("Displaying activity must implement OnClickShowCryptoKeyListener!");
                    }
                    ((OnClickShowCryptoKeyListener) frag).onClickShowCryptoKey();
                }
            });
        }
        return b.create();
    }

    private void setMessageForDisplayStatus(MessageCryptoDisplayStatus displayStatus) {
        if (displayStatus.textResTop == null) {
            throw new AssertionError("Crypto info dialog can only be displayed for items with text!");
        } else if (displayStatus.textResBottom == null) {
            setMessageSingleLine(displayStatus.colorAttr, displayStatus.textResTop.intValue(), displayStatus.statusIconRes, displayStatus.statusDotsRes);
        } else if (displayStatus.statusDotsRes == null) {
            throw new AssertionError("second icon must be non-null if second text is non-null!");
        } else {
            setMessageWithAnimation(displayStatus.colorAttr, displayStatus.textResTop.intValue(), displayStatus.statusIconRes, displayStatus.textResBottom.intValue(), displayStatus.statusDotsRes.intValue());
        }
    }

    private void setMessageSingleLine(@AttrRes int colorAttr, @StringRes int topTextRes, @DrawableRes int statusIconRes, @DrawableRes Integer statusDotsRes) {
        int color = ThemeUtils.getStyledColor(getActivity(), colorAttr);
        this.topIcon_1.setImageResource(statusIconRes);
        this.topIcon_1.setColorFilter(color);
        this.topText.setText(topTextRes);
        if (statusDotsRes != null) {
            this.topIcon_3.setImageResource(statusDotsRes.intValue());
            this.topIcon_3.setColorFilter(color);
            this.topIcon_3.setVisibility(0);
        } else {
            this.topIcon_3.setVisibility(8);
        }
        this.bottomText.setVisibility(8);
        this.bottomIconFrame.setVisibility(8);
    }

    private void setMessageWithAnimation(@AttrRes int colorAttr, @StringRes int topTextRes, @DrawableRes int statusIconRes, @StringRes int bottomTextRes, @DrawableRes int statusDotsRes) {
        this.topIcon_1.setImageResource(statusIconRes);
        this.topIcon_2.setImageResource(statusDotsRes);
        this.topIcon_3.setVisibility(8);
        this.topText.setText(topTextRes);
        this.bottomIcon_1.setImageResource(statusIconRes);
        this.bottomIcon_2.setImageResource(statusDotsRes);
        this.bottomText.setText(bottomTextRes);
        this.topIcon_1.setColorFilter(ThemeUtils.getStyledColor(getActivity(), colorAttr));
        this.bottomIcon_2.setColorFilter(ThemeUtils.getStyledColor(getActivity(), colorAttr));
        prepareIconAnimation();
    }

    private void prepareIconAnimation() {
        this.topText.setAlpha(0.0f);
        this.bottomText.setAlpha(0.0f);
        this.dialogView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
                float halfVerticalPixelDifference = (CryptoInfoDialog.this.bottomIconFrame.getY() - CryptoInfoDialog.this.topIconFrame.getY()) / 2.0f;
                CryptoInfoDialog.this.topIconFrame.setTranslationY(halfVerticalPixelDifference);
                CryptoInfoDialog.this.bottomIconFrame.setTranslationY(-halfVerticalPixelDifference);
                CryptoInfoDialog.this.topIconFrame.animate().translationY(0.0f).setStartDelay(400).setDuration(350).setInterpolator(new AccelerateDecelerateInterpolator()).start();
                CryptoInfoDialog.this.bottomIconFrame.animate().translationY(0.0f).setStartDelay(400).setDuration(350).setInterpolator(new AccelerateDecelerateInterpolator()).start();
                CryptoInfoDialog.this.topText.animate().alpha(1.0f).setStartDelay(750).start();
                CryptoInfoDialog.this.bottomText.animate().alpha(1.0f).setStartDelay(750).start();
                view.removeOnLayoutChangeListener(this);
            }
        });
    }
}
