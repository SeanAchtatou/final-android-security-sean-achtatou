package com.fsck.k9.ui.messageview;

import com.fsck.k9.mailstore.AttachmentViewInfo;

interface AttachmentViewCallback {
    void onSaveAttachment(AttachmentViewInfo attachmentViewInfo);

    void onSaveAttachmentToUserProvidedDirectory(AttachmentViewInfo attachmentViewInfo);

    void onViewAttachment(AttachmentViewInfo attachmentViewInfo);
}
