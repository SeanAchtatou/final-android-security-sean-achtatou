package com.fsck.k9.ui.messageview;

import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.WorkerThread;
import android.util.Log;
import android.widget.Toast;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.cache.TemporaryAttachmentStore;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.controller.MessagingListener;
import com.fsck.k9.helper.FileHelper;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.mailstore.AttachmentViewInfo;
import com.fsck.k9.mailstore.LocalMessage;
import com.fsck.k9.mailstore.LocalPart;
import com.fsck.k9.provider.AttachmentTempFileProvider;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.commons.io.IOUtils;
import yahoo.mail.app.R;

public class AttachmentController {
    /* access modifiers changed from: private */
    public final AttachmentViewInfo attachment;
    /* access modifiers changed from: private */
    public final Context context;
    private final MessagingController controller;
    private final DownloadManager downloadManager;
    /* access modifiers changed from: private */
    public final MessageViewFragment messageViewFragment;

    AttachmentController(MessagingController controller2, DownloadManager downloadManager2, MessageViewFragment messageViewFragment2, AttachmentViewInfo attachment2) {
        this.context = messageViewFragment2.getApplicationContext();
        this.controller = controller2;
        this.downloadManager = downloadManager2;
        this.messageViewFragment = messageViewFragment2;
        this.attachment = attachment2;
    }

    public void viewAttachment() {
        if (!this.attachment.isContentAvailable) {
            downloadAndViewAttachment((LocalPart) this.attachment.part);
        } else {
            viewLocalAttachment();
        }
    }

    public void saveAttachment() {
        saveAttachmentTo(K9.getAttachmentDefaultPath());
    }

    public void saveAttachmentTo(String directory) {
        saveAttachmentTo(new File(directory));
    }

    private void downloadAndViewAttachment(LocalPart localPart) {
        downloadAttachment(localPart, new Runnable() {
            public void run() {
                AttachmentController.this.viewLocalAttachment();
            }
        });
    }

    private void downloadAndSaveAttachmentTo(LocalPart localPart, final File directory) {
        downloadAttachment(localPart, new Runnable() {
            public void run() {
                AttachmentController.this.messageViewFragment.refreshAttachmentThumbnail(AttachmentController.this.attachment);
                AttachmentController.this.saveLocalAttachmentTo(directory);
            }
        });
    }

    private void downloadAttachment(LocalPart localPart, final Runnable attachmentDownloadedCallback) {
        Account account = Preferences.getPreferences(this.context).getAccount(localPart.getAccountUuid());
        LocalMessage message = localPart.getMessage();
        this.messageViewFragment.showAttachmentLoadingDialog();
        this.controller.loadAttachment(account, message, this.attachment.part, new MessagingListener() {
            public void loadAttachmentFinished(Account account, Message message, Part part) {
                AttachmentController.this.messageViewFragment.hideAttachmentLoadingDialogOnMainThread();
                AttachmentController.this.messageViewFragment.runOnMainThread(attachmentDownloadedCallback);
            }

            public void loadAttachmentFailed(Account account, Message message, Part part, String reason) {
                AttachmentController.this.messageViewFragment.hideAttachmentLoadingDialogOnMainThread();
            }
        });
    }

    /* access modifiers changed from: private */
    public void viewLocalAttachment() {
        new ViewAttachmentAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    private void saveAttachmentTo(File directory) {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            displayMessageToUser(this.context.getString(R.string.message_view_status_attachment_not_saved));
        } else if (!this.attachment.isContentAvailable) {
            downloadAndSaveAttachmentTo((LocalPart) this.attachment.part, directory);
        } else {
            saveLocalAttachmentTo(directory);
        }
    }

    /* access modifiers changed from: private */
    public void saveLocalAttachmentTo(File directory) {
        new SaveAttachmentAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, directory);
    }

    /* access modifiers changed from: private */
    public File saveAttachmentWithUniqueFileName(File directory) throws IOException {
        File file = FileHelper.createUniqueFile(directory, FileHelper.sanitizeFilename(this.attachment.displayName));
        writeAttachmentToStorage(file);
        addSavedAttachmentToDownloadsDatabase(file);
        return file;
    }

    private void writeAttachmentToStorage(File file) throws IOException {
        OutputStream out;
        InputStream in = this.context.getContentResolver().openInputStream(this.attachment.internalUri);
        try {
            out = new FileOutputStream(file);
            IOUtils.copy(in, out);
            out.flush();
            out.close();
            in.close();
        } catch (Throwable th) {
            in.close();
            throw th;
        }
    }

    private void addSavedAttachmentToDownloadsDatabase(File file) {
        String fileName = file.getName();
        String path = file.getAbsolutePath();
        long fileLength = file.length();
        this.downloadManager.addCompletedDownload(fileName, fileName, true, this.attachment.mimeType, path, fileLength, true);
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public Intent getBestViewIntentAndSaveFile() {
        IntentAndResolvedActivitiesCount resolvedIntentInfo;
        try {
            Uri intentDataUri = AttachmentTempFileProvider.createTempUriForContentUri(this.context, this.attachment.internalUri);
            String displayName = this.attachment.displayName;
            String inferredMimeType = MimeUtility.getMimeTypeByExtension(displayName);
            String mimeType = this.attachment.mimeType;
            if (MimeUtility.isDefaultMimeType(mimeType)) {
                resolvedIntentInfo = getBestViewIntentForMimeType(intentDataUri, inferredMimeType);
            } else {
                resolvedIntentInfo = getBestViewIntentForMimeType(intentDataUri, mimeType);
                if (!resolvedIntentInfo.hasResolvedActivities() && !inferredMimeType.equals(mimeType)) {
                    resolvedIntentInfo = getBestViewIntentForMimeType(intentDataUri, inferredMimeType);
                }
            }
            if (!resolvedIntentInfo.hasResolvedActivities()) {
                resolvedIntentInfo = getBestViewIntentForMimeType(intentDataUri, MimeUtility.DEFAULT_ATTACHMENT_MIME_TYPE);
            }
            if (!resolvedIntentInfo.hasResolvedActivities() || !resolvedIntentInfo.containsFileUri()) {
                return resolvedIntentInfo.getIntent();
            }
            try {
                File tempFile = TemporaryAttachmentStore.getFileForWriting(this.context, displayName);
                writeAttachmentToStorage(tempFile);
                return createViewIntentForFileUri(resolvedIntentInfo.getMimeType(), Uri.fromFile(tempFile));
            } catch (IOException e) {
                if (K9.DEBUG) {
                    Log.e("k9", "Error while saving attachment to use file:// URI with ACTION_VIEW Intent", e);
                }
                return createViewIntentForAttachmentProviderUri(intentDataUri, MimeUtility.DEFAULT_ATTACHMENT_MIME_TYPE);
            }
        } catch (IOException e2) {
            Log.e("k9", "Error creating temp file for attachment!", e2);
            return null;
        }
    }

    private IntentAndResolvedActivitiesCount getBestViewIntentForMimeType(Uri contentUri, String mimeType) {
        Intent contentUriIntent = createViewIntentForAttachmentProviderUri(contentUri, mimeType);
        int contentUriActivitiesCount = getResolvedIntentActivitiesCount(contentUriIntent);
        if (contentUriActivitiesCount > 0) {
        }
        Intent fileUriIntent = createViewIntentForFileUri(mimeType, Uri.fromFile(TemporaryAttachmentStore.getFile(this.context, this.attachment.displayName)));
        int fileUriActivitiesCount = getResolvedIntentActivitiesCount(fileUriIntent);
        if (fileUriActivitiesCount > 0) {
            return new IntentAndResolvedActivitiesCount(fileUriIntent, fileUriActivitiesCount);
        }
        return new IntentAndResolvedActivitiesCount(contentUriIntent, contentUriActivitiesCount);
    }

    private Intent createViewIntentForAttachmentProviderUri(Uri contentUri, String mimeType) {
        Uri uri = AttachmentTempFileProvider.getMimeTypeUri(contentUri, mimeType);
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(uri, mimeType);
        intent.addFlags(1);
        addUiIntentFlags(intent);
        return intent;
    }

    private Intent createViewIntentForFileUri(String mimeType, Uri uri) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(uri, mimeType);
        addUiIntentFlags(intent);
        return intent;
    }

    private void addUiIntentFlags(Intent intent) {
        intent.addFlags(268959744);
    }

    private int getResolvedIntentActivitiesCount(Intent intent) {
        return this.context.getPackageManager().queryIntentActivities(intent, 65536).size();
    }

    /* access modifiers changed from: private */
    public void displayAttachmentNotSavedMessage() {
        displayMessageToUser(this.context.getString(R.string.message_view_status_attachment_not_saved));
    }

    /* access modifiers changed from: private */
    public void displayMessageToUser(String message) {
        Toast.makeText(this.context, message, 1).show();
    }

    private static class IntentAndResolvedActivitiesCount {
        private int activitiesCount;
        private Intent intent;

        IntentAndResolvedActivitiesCount(Intent intent2, int activitiesCount2) {
            this.intent = intent2;
            this.activitiesCount = activitiesCount2;
        }

        public Intent getIntent() {
            return this.intent;
        }

        public boolean hasResolvedActivities() {
            return this.activitiesCount > 0;
        }

        public String getMimeType() {
            return this.intent.getType();
        }

        public boolean containsFileUri() {
            return "file".equals(this.intent.getData().getScheme());
        }
    }

    private class ViewAttachmentAsyncTask extends AsyncTask<Void, Void, Intent> {
        private ViewAttachmentAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            AttachmentController.this.messageViewFragment.disableAttachmentButtons(AttachmentController.this.attachment);
        }

        /* access modifiers changed from: protected */
        public Intent doInBackground(Void... params) {
            return AttachmentController.this.getBestViewIntentAndSaveFile();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Intent intent) {
            viewAttachment(intent);
            AttachmentController.this.messageViewFragment.enableAttachmentButtons(AttachmentController.this.attachment);
        }

        private void viewAttachment(Intent intent) {
            try {
                AttachmentController.this.context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Log.e("k9", "Could not display attachment of type " + AttachmentController.this.attachment.mimeType, e);
                AttachmentController.this.displayMessageToUser(AttachmentController.this.context.getString(R.string.message_view_no_viewer, AttachmentController.this.attachment.mimeType));
            }
        }
    }

    private class SaveAttachmentAsyncTask extends AsyncTask<File, Void, File> {
        private SaveAttachmentAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            AttachmentController.this.messageViewFragment.disableAttachmentButtons(AttachmentController.this.attachment);
        }

        /* access modifiers changed from: protected */
        public File doInBackground(File... params) {
            try {
                return AttachmentController.this.saveAttachmentWithUniqueFileName(params[0]);
            } catch (IOException e) {
                if (K9.DEBUG) {
                    Log.e("k9", "Error saving attachment", e);
                }
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(File file) {
            AttachmentController.this.messageViewFragment.enableAttachmentButtons(AttachmentController.this.attachment);
            if (file == null) {
                AttachmentController.this.displayAttachmentNotSavedMessage();
            }
        }
    }
}
