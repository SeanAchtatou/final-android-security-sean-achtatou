package com.jcraft.jzlib;

import android.support.v4.internal.view.SupportMenu;
import android.support.v4.media.TransportMediator;

final class InfCodes {
    private static final int BADCODE = 9;
    private static final int COPY = 5;
    private static final int DIST = 3;
    private static final int DISTEXT = 4;
    private static final int END = 8;
    private static final int LEN = 1;
    private static final int LENEXT = 2;
    private static final int LIT = 6;
    private static final int START = 0;
    private static final int WASH = 7;
    private static final int Z_BUF_ERROR = -5;
    private static final int Z_DATA_ERROR = -3;
    private static final int Z_ERRNO = -1;
    private static final int Z_MEM_ERROR = -4;
    private static final int Z_NEED_DICT = 2;
    private static final int Z_OK = 0;
    private static final int Z_STREAM_END = 1;
    private static final int Z_STREAM_ERROR = -2;
    private static final int Z_VERSION_ERROR = -6;
    private static final int[] inflate_mask = {0, 1, 3, 7, 15, 31, 63, TransportMediator.KEYCODE_MEDIA_PAUSE, 255, 511, 1023, 2047, 4095, 8191, 16383, 32767, SupportMenu.USER_MASK};
    byte dbits;
    int dist;
    int[] dtree;
    int dtree_index;
    int get;
    byte lbits;
    int len;
    int lit;
    int[] ltree;
    int ltree_index;
    int mode;
    int need;
    int[] tree;
    int tree_index = 0;

    InfCodes() {
    }

    /* access modifiers changed from: package-private */
    public void init(int bl, int bd, int[] tl, int tl_index, int[] td, int td_index, ZStream z) {
        this.mode = 0;
        this.lbits = (byte) bl;
        this.dbits = (byte) bd;
        this.ltree = tl;
        this.ltree_index = tl_index;
        this.dtree = td;
        this.dtree_index = td_index;
        this.tree = null;
    }

    /* access modifiers changed from: package-private */
    public int proc(InfBlocks s, ZStream z, int r) {
        int f;
        int p = z.next_in_index;
        int n = z.avail_in;
        int b = s.bitb;
        int k = s.bitk;
        int q = s.write;
        int m = q < s.read ? (s.read - q) - 1 : s.end - q;
        while (true) {
            switch (this.mode) {
                case 0:
                    if (m >= 258 && n >= 10) {
                        s.bitb = b;
                        s.bitk = k;
                        z.avail_in = n;
                        z.total_in += (long) (p - z.next_in_index);
                        z.next_in_index = p;
                        s.write = q;
                        r = inflate_fast(this.lbits, this.dbits, this.ltree, this.ltree_index, this.dtree, this.dtree_index, s, z);
                        p = z.next_in_index;
                        n = z.avail_in;
                        b = s.bitb;
                        k = s.bitk;
                        q = s.write;
                        m = q < s.read ? (s.read - q) - 1 : s.end - q;
                        if (r != 0) {
                            this.mode = r == 1 ? 7 : 9;
                            break;
                        }
                    }
                    this.need = this.lbits;
                    this.tree = this.ltree;
                    this.tree_index = this.ltree_index;
                    this.mode = 1;
                case 1:
                    int j = this.need;
                    while (true) {
                        int p2 = p;
                        if (k >= j) {
                            int tindex = (this.tree_index + (inflate_mask[j] & b)) * 3;
                            b >>>= this.tree[tindex + 1];
                            k -= this.tree[tindex + 1];
                            int e = this.tree[tindex];
                            if (e == 0) {
                                this.lit = this.tree[tindex + 2];
                                this.mode = 6;
                                p = p2;
                                break;
                            } else if ((e & 16) != 0) {
                                this.get = e & 15;
                                this.len = this.tree[tindex + 2];
                                this.mode = 2;
                                p = p2;
                                break;
                            } else if ((e & 64) == 0) {
                                this.need = e;
                                this.tree_index = (tindex / 3) + this.tree[tindex + 2];
                                p = p2;
                                break;
                            } else if ((e & 32) != 0) {
                                this.mode = 7;
                                p = p2;
                                break;
                            } else {
                                this.mode = 9;
                                z.msg = "invalid literal/length code";
                                s.bitb = b;
                                s.bitk = k;
                                z.avail_in = n;
                                z.total_in += (long) (p2 - z.next_in_index);
                                z.next_in_index = p2;
                                s.write = q;
                                return s.inflate_flush(z, -3);
                            }
                        } else if (n != 0) {
                            r = 0;
                            n--;
                            p = p2 + 1;
                            b |= (z.next_in[p2] & 255) << k;
                            k += 8;
                        } else {
                            s.bitb = b;
                            s.bitk = k;
                            z.avail_in = n;
                            z.total_in += (long) (p2 - z.next_in_index);
                            z.next_in_index = p2;
                            s.write = q;
                            return s.inflate_flush(z, r);
                        }
                    }
                case 2:
                    int j2 = this.get;
                    while (true) {
                        int p3 = p;
                        if (k >= j2) {
                            this.len = this.len + (inflate_mask[j2] & b);
                            b >>= j2;
                            k -= j2;
                            this.need = this.dbits;
                            this.tree = this.dtree;
                            this.tree_index = this.dtree_index;
                            this.mode = 3;
                            p = p3;
                        } else if (n != 0) {
                            r = 0;
                            n--;
                            p = p3 + 1;
                            b |= (z.next_in[p3] & 255) << k;
                            k += 8;
                        } else {
                            s.bitb = b;
                            s.bitk = k;
                            z.avail_in = n;
                            z.total_in += (long) (p3 - z.next_in_index);
                            z.next_in_index = p3;
                            s.write = q;
                            return s.inflate_flush(z, r);
                        }
                    }
                case 3:
                    int j3 = this.need;
                    while (true) {
                        int p4 = p;
                        if (k >= j3) {
                            int tindex2 = (this.tree_index + (inflate_mask[j3] & b)) * 3;
                            b >>= this.tree[tindex2 + 1];
                            k -= this.tree[tindex2 + 1];
                            int e2 = this.tree[tindex2];
                            if ((e2 & 16) != 0) {
                                this.get = e2 & 15;
                                this.dist = this.tree[tindex2 + 2];
                                this.mode = 4;
                                p = p4;
                                break;
                            } else if ((e2 & 64) == 0) {
                                this.need = e2;
                                this.tree_index = (tindex2 / 3) + this.tree[tindex2 + 2];
                                p = p4;
                                break;
                            } else {
                                this.mode = 9;
                                z.msg = "invalid distance code";
                                s.bitb = b;
                                s.bitk = k;
                                z.avail_in = n;
                                z.total_in += (long) (p4 - z.next_in_index);
                                z.next_in_index = p4;
                                s.write = q;
                                return s.inflate_flush(z, -3);
                            }
                        } else if (n != 0) {
                            r = 0;
                            n--;
                            p = p4 + 1;
                            b |= (z.next_in[p4] & 255) << k;
                            k += 8;
                        } else {
                            s.bitb = b;
                            s.bitk = k;
                            z.avail_in = n;
                            z.total_in += (long) (p4 - z.next_in_index);
                            z.next_in_index = p4;
                            s.write = q;
                            return s.inflate_flush(z, r);
                        }
                    }
                case 4:
                    int j4 = this.get;
                    while (true) {
                        int p5 = p;
                        if (k >= j4) {
                            this.dist = this.dist + (inflate_mask[j4] & b);
                            b >>= j4;
                            k -= j4;
                            this.mode = 5;
                            p = p5;
                        } else if (n != 0) {
                            r = 0;
                            n--;
                            p = p5 + 1;
                            b |= (z.next_in[p5] & 255) << k;
                            k += 8;
                        } else {
                            s.bitb = b;
                            s.bitk = k;
                            z.avail_in = n;
                            z.total_in += (long) (p5 - z.next_in_index);
                            z.next_in_index = p5;
                            s.write = q;
                            return s.inflate_flush(z, r);
                        }
                    }
                case 5:
                    int f2 = q - this.dist;
                    while (f < 0) {
                        f2 = f + s.end;
                    }
                    while (this.len != 0) {
                        if (m == 0) {
                            if (q == s.end && s.read != 0) {
                                q = 0;
                                m = 0 < s.read ? (s.read - 0) - 1 : s.end - 0;
                            }
                            if (m == 0) {
                                s.write = q;
                                r = s.inflate_flush(z, r);
                                q = s.write;
                                int m2 = q < s.read ? (s.read - q) - 1 : s.end - q;
                                if (q == s.end && s.read != 0) {
                                    q = 0;
                                    m2 = 0 < s.read ? (s.read - 0) - 1 : s.end - 0;
                                }
                                if (m == 0) {
                                    s.bitb = b;
                                    s.bitk = k;
                                    z.avail_in = n;
                                    z.total_in += (long) (p - z.next_in_index);
                                    z.next_in_index = p;
                                    s.write = q;
                                    return s.inflate_flush(z, r);
                                }
                            }
                        }
                        int q2 = q + 1;
                        int f3 = f + 1;
                        s.window[q] = s.window[f];
                        m--;
                        if (f3 == s.end) {
                            f = 0;
                        } else {
                            f = f3;
                        }
                        this.len = this.len - 1;
                        q = q2;
                    }
                    this.mode = 0;
                    break;
                case 6:
                    if (m == 0) {
                        if (q == s.end && s.read != 0) {
                            q = 0;
                            m = 0 < s.read ? (s.read - 0) - 1 : s.end - 0;
                        }
                        if (m == 0) {
                            s.write = q;
                            int r2 = s.inflate_flush(z, r);
                            int q3 = s.write;
                            int m3 = q3 < s.read ? (s.read - q3) - 1 : s.end - q3;
                            if (q3 == s.end && s.read != 0) {
                                q3 = 0;
                                m3 = 0 < s.read ? (s.read - 0) - 1 : s.end - 0;
                            }
                            if (m == 0) {
                                s.bitb = b;
                                s.bitk = k;
                                z.avail_in = n;
                                z.total_in += (long) (p - z.next_in_index);
                                z.next_in_index = p;
                                s.write = q;
                                return s.inflate_flush(z, r2);
                            }
                        }
                    }
                    r = 0;
                    s.window[q] = (byte) this.lit;
                    m--;
                    this.mode = 0;
                    q++;
                    break;
                case 7:
                    if (k > 7) {
                        k -= 8;
                        n++;
                        p--;
                    }
                    s.write = q;
                    int r3 = s.inflate_flush(z, r);
                    q = s.write;
                    if (q < s.read) {
                        int m4 = (s.read - q) - 1;
                    } else {
                        int m5 = s.end - q;
                    }
                    if (s.read == s.write) {
                        this.mode = 8;
                        break;
                    } else {
                        s.bitb = b;
                        s.bitk = k;
                        z.avail_in = n;
                        z.total_in += (long) (p - z.next_in_index);
                        z.next_in_index = p;
                        s.write = q;
                        return s.inflate_flush(z, r3);
                    }
                case 8:
                    break;
                case 9:
                    s.bitb = b;
                    s.bitk = k;
                    z.avail_in = n;
                    z.total_in += (long) (p - z.next_in_index);
                    z.next_in_index = p;
                    s.write = q;
                    return s.inflate_flush(z, -3);
                default:
                    s.bitb = b;
                    s.bitk = k;
                    z.avail_in = n;
                    z.total_in += (long) (p - z.next_in_index);
                    z.next_in_index = p;
                    s.write = q;
                    return s.inflate_flush(z, -2);
            }
        }
        s.bitb = b;
        s.bitk = k;
        z.avail_in = n;
        z.total_in += (long) (p - z.next_in_index);
        z.next_in_index = p;
        s.write = q;
        return s.inflate_flush(z, 1);
    }

    /* access modifiers changed from: package-private */
    public void free(ZStream z) {
    }

    /* access modifiers changed from: package-private */
    public int inflate_fast(int bl, int bd, int[] tl, int tl_index, int[] td, int td_index, InfBlocks s, ZStream z) {
        int m;
        int m2;
        int r;
        int q;
        int q2;
        int q3;
        int p = z.next_in_index;
        int n = z.avail_in;
        int b = s.bitb;
        int k = s.bitk;
        int q4 = s.write;
        if (q4 < s.read) {
            m = (s.read - q4) - 1;
        } else {
            m = s.end - q4;
        }
        int ml = inflate_mask[bl];
        int md = inflate_mask[bd];
        int q5 = q4;
        while (true) {
            int p2 = p;
            if (k < 20) {
                n--;
                p = p2 + 1;
                b |= (z.next_in[p2] & 255) << k;
                k += 8;
            } else {
                int t = b & ml;
                int[] tp = tl;
                int tp_index = tl_index;
                int tp_index_t_3 = (tp_index + t) * 3;
                int e = tp[tp_index_t_3];
                if (e == 0) {
                    b >>= tp[tp_index_t_3 + 1];
                    k -= tp[tp_index_t_3 + 1];
                    q2 = q5 + 1;
                    s.window[q5] = (byte) tp[tp_index_t_3 + 2];
                    m2--;
                    p = p2;
                } else {
                    while (true) {
                        b >>= tp[tp_index_t_3 + 1];
                        k -= tp[tp_index_t_3 + 1];
                        if ((e & 16) != 0) {
                            int e2 = e & 15;
                            int c = tp[tp_index_t_3 + 2] + (inflate_mask[e2] & b);
                            int b2 = b >> e2;
                            int k2 = k - e2;
                            while (k2 < 15) {
                                n--;
                                b2 |= (z.next_in[p2] & 255) << k2;
                                k2 += 8;
                                p2++;
                            }
                            int t2 = b2 & md;
                            int[] tp2 = td;
                            int tp_index2 = td_index;
                            int tp_index_t_32 = (tp_index2 + t2) * 3;
                            int e3 = tp2[tp_index_t_32];
                            while (true) {
                                b2 >>= tp2[tp_index_t_32 + 1];
                                k2 -= tp2[tp_index_t_32 + 1];
                                if ((e3 & 16) != 0) {
                                    int e4 = e3 & 15;
                                    while (k2 < e4) {
                                        n--;
                                        b2 |= (z.next_in[p2] & 255) << k2;
                                        k2 += 8;
                                        p2++;
                                    }
                                    int d = tp2[tp_index_t_32 + 2] + (inflate_mask[e4] & b2);
                                    b = b2 >> e4;
                                    k = k2 - e4;
                                    m2 -= c;
                                    if (q5 >= d) {
                                        int r2 = q5 - d;
                                        if (q5 - r2 <= 0 || 2 <= q5 - r2) {
                                            System.arraycopy(s.window, r2, s.window, q5, 2);
                                            q = q5 + 2;
                                            r = r2 + 2;
                                            c -= 2;
                                        } else {
                                            int q6 = q5 + 1;
                                            int r3 = r2 + 1;
                                            s.window[q5] = s.window[r2];
                                            r = r3 + 1;
                                            s.window[q6] = s.window[r3];
                                            c -= 2;
                                            q = q6 + 1;
                                        }
                                    } else {
                                        r = q5 - d;
                                        do {
                                            r += s.end;
                                        } while (r < 0);
                                        int e5 = s.end - r;
                                        if (c > e5) {
                                            c -= e5;
                                            if (q5 - r <= 0 || e5 <= q5 - r) {
                                                System.arraycopy(s.window, r, s.window, q5, e5);
                                                q = q5 + e5;
                                                int r4 = r + e5;
                                            } else {
                                                while (true) {
                                                    int q7 = q5;
                                                    q5 = q7 + 1;
                                                    int r5 = r + 1;
                                                    s.window[q7] = s.window[r];
                                                    e5--;
                                                    if (e5 == 0) {
                                                        break;
                                                    }
                                                    r = r5;
                                                }
                                                q = q5;
                                            }
                                            r = 0;
                                        } else {
                                            q = q5;
                                        }
                                    }
                                    if (q - r <= 0 || c <= q - r) {
                                        System.arraycopy(s.window, r, s.window, q, c);
                                        q2 = q + c;
                                        int r6 = r + c;
                                        p = p2;
                                    } else {
                                        while (true) {
                                            q3 = q + 1;
                                            int r7 = r + 1;
                                            s.window[q] = s.window[r];
                                            c--;
                                            if (c == 0) {
                                                break;
                                            }
                                            r = r7;
                                            q = q3;
                                        }
                                        q2 = q3;
                                        p = p2;
                                    }
                                } else if ((e3 & 64) == 0) {
                                    t2 = t2 + tp2[tp_index_t_32 + 2] + (inflate_mask[e3] & b2);
                                    tp_index_t_32 = (tp_index2 + t2) * 3;
                                    e3 = tp2[tp_index_t_32];
                                } else {
                                    z.msg = "invalid distance code";
                                    int c2 = z.avail_in - n;
                                    if ((k2 >> 3) < c2) {
                                        c2 = k2 >> 3;
                                    }
                                    int p3 = p2 - c2;
                                    s.bitb = b2;
                                    s.bitk = k2 - (c2 << 3);
                                    z.avail_in = n + c2;
                                    z.total_in += (long) (p3 - z.next_in_index);
                                    z.next_in_index = p3;
                                    s.write = q5;
                                    return -3;
                                }
                            }
                        } else if ((e & 64) == 0) {
                            t = t + tp[tp_index_t_3 + 2] + (inflate_mask[e] & b);
                            tp_index_t_3 = (tp_index + t) * 3;
                            e = tp[tp_index_t_3];
                            if (e == 0) {
                                b >>= tp[tp_index_t_3 + 1];
                                k -= tp[tp_index_t_3 + 1];
                                q2 = q5 + 1;
                                s.window[q5] = (byte) tp[tp_index_t_3 + 2];
                                m2--;
                                p = p2;
                                break;
                            }
                        } else if ((e & 32) != 0) {
                            int c3 = z.avail_in - n;
                            if ((k >> 3) < c3) {
                                c3 = k >> 3;
                            }
                            int p4 = p2 - c3;
                            s.bitb = b;
                            s.bitk = k - (c3 << 3);
                            z.avail_in = n + c3;
                            z.total_in += (long) (p4 - z.next_in_index);
                            z.next_in_index = p4;
                            s.write = q5;
                            return 1;
                        } else {
                            z.msg = "invalid literal/length code";
                            int c4 = z.avail_in - n;
                            if ((k >> 3) < c4) {
                                c4 = k >> 3;
                            }
                            int p5 = p2 - c4;
                            s.bitb = b;
                            s.bitk = k - (c4 << 3);
                            z.avail_in = n + c4;
                            z.total_in += (long) (p5 - z.next_in_index);
                            z.next_in_index = p5;
                            s.write = q5;
                            return -3;
                        }
                    }
                }
                if (m2 < 258 || n < 10) {
                    int c5 = z.avail_in - n;
                } else {
                    q5 = q2;
                }
            }
        }
        int c52 = z.avail_in - n;
        if ((k >> 3) < c52) {
            c52 = k >> 3;
        }
        int p6 = p - c52;
        s.bitb = b;
        s.bitk = k - (c52 << 3);
        z.avail_in = n + c52;
        z.total_in += (long) (p6 - z.next_in_index);
        z.next_in_index = p6;
        s.write = q2;
        return 0;
    }
}
