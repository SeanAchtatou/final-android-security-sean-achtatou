package com.jcraft.jzlib;

import android.support.v4.internal.view.SupportMenu;
import android.support.v4.media.TransportMediator;

final class InfBlocks {
    private static final int BAD = 9;
    private static final int BTREE = 4;
    private static final int CODES = 6;
    private static final int DONE = 8;
    private static final int DRY = 7;
    private static final int DTREE = 5;
    private static final int LENS = 1;
    private static final int MANY = 1440;
    private static final int STORED = 2;
    private static final int TABLE = 3;
    private static final int TYPE = 0;
    private static final int Z_BUF_ERROR = -5;
    private static final int Z_DATA_ERROR = -3;
    private static final int Z_ERRNO = -1;
    private static final int Z_MEM_ERROR = -4;
    private static final int Z_NEED_DICT = 2;
    private static final int Z_OK = 0;
    private static final int Z_STREAM_END = 1;
    private static final int Z_STREAM_ERROR = -2;
    private static final int Z_VERSION_ERROR = -6;
    static final int[] border = {16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15};
    private static final int[] inflate_mask = {0, 1, 3, 7, 15, 31, 63, TransportMediator.KEYCODE_MEDIA_PAUSE, 255, 511, 1023, 2047, 4095, 8191, 16383, 32767, SupportMenu.USER_MASK};
    int[] bb = new int[1];
    int bitb;
    int bitk;
    int[] blens;
    long check;
    Object checkfn;
    InfCodes codes = new InfCodes();
    int end;
    int[] hufts = new int[4320];
    int index;
    InfTree inftree = new InfTree();
    int last;
    int left;
    int mode;
    int read;
    int table;
    int[] tb = new int[1];
    byte[] window;
    int write;

    InfBlocks(ZStream z, Object checkfn2, int w) {
        this.window = new byte[w];
        this.end = w;
        this.checkfn = checkfn2;
        this.mode = 0;
        reset(z, null);
    }

    /* access modifiers changed from: package-private */
    public void reset(ZStream z, long[] c) {
        if (c != null) {
            c[0] = this.check;
        }
        if (this.mode == 4 || this.mode == 5) {
        }
        if (this.mode == 6) {
            this.codes.free(z);
        }
        this.mode = 0;
        this.bitk = 0;
        this.bitb = 0;
        this.write = 0;
        this.read = 0;
        if (this.checkfn != null) {
            long adler32 = z._adler.adler32(0, null, 0, 0);
            this.check = adler32;
            z.adler = adler32;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x0890, code lost:
        r1.write = r30;
        r34 = inflate_flush(r33, r34);
        r30 = r0.write;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x08a6, code lost:
        if (r30 >= r0.read) goto L_0x08ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x08a8, code lost:
        r26 = (r0.read - r30) - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x08b8, code lost:
        if (r0.read == r0.write) goto L_0x08f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x08ba, code lost:
        r1.bitb = r20;
        r1.bitk = r25;
        r33.avail_in = r27;
        r33.total_in += (long) (r28 - r33.next_in_index);
        r33.next_in_index = r28;
        r1.write = r30;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x08ee, code lost:
        r26 = r0.end - r30;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x08f5, code lost:
        r0.mode = 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x08fb, code lost:
        r1.bitb = r20;
        r1.bitk = r25;
        r33.avail_in = r27;
        r33.total_in += (long) (r28 - r33.next_in_index);
        r33.next_in_index = r28;
        r1.write = r30;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:242:?, code lost:
        return inflate_flush(r33, -3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:244:?, code lost:
        return inflate_flush(r33, r34);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:245:?, code lost:
        return inflate_flush(r33, 1);
     */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x04e8 A[LOOP:7: B:102:0x04e0->B:104:0x04e8, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0568  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x060e  */
    /* JADX WARNING: Removed duplicated region for block: B:183:0x0522 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:219:0x0587 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x045c A[LOOP:5: B:92:0x045c->B:95:0x0463, LOOP_START, PHI: r20 r25 r27 r28 r34 
      PHI: (r20v14 'b' int) = (r20v13 'b' int), (r20v16 'b' int) binds: [B:91:0x0458, B:95:0x0463] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r25v14 'k' int) = (r25v13 'k' int), (r25v16 'k' int) binds: [B:91:0x0458, B:95:0x0463] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r27v11 'n' int) = (r27v10 'n' int), (r27v12 'n' int) binds: [B:91:0x0458, B:95:0x0463] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r28v15 'p' int) = (r28v14 'p' int), (r28v18 'p' int) binds: [B:91:0x0458, B:95:0x0463] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r34v14 'r' int) = (r34v12 'r' int), (r34v15 'r' int) binds: [B:91:0x0458, B:95:0x0463] A[DONT_GENERATE, DONT_INLINE]] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int proc(com.jcraft.jzlib.ZStream r33, int r34) {
        /*
            r32 = this;
            r0 = r33
            int r0 = r0.next_in_index
            r28 = r0
            r0 = r33
            int r0 = r0.avail_in
            r27 = r0
            r0 = r32
            int r0 = r0.bitb
            r20 = r0
            r0 = r32
            int r0 = r0.bitk
            r25 = r0
            r0 = r32
            int r0 = r0.write
            r30 = r0
            r0 = r32
            int r4 = r0.read
            r0 = r30
            if (r0 >= r4) goto L_0x006a
            r0 = r32
            int r4 = r0.read
            int r4 = r4 - r30
            int r26 = r4 + -1
        L_0x002e:
            r0 = r32
            int r4 = r0.mode
            switch(r4) {
                case 0: goto L_0x0973;
                case 1: goto L_0x096f;
                case 2: goto L_0x0237;
                case 3: goto L_0x096b;
                case 4: goto L_0x044c;
                case 5: goto L_0x0572;
                case 6: goto L_0x07f7;
                case 7: goto L_0x0890;
                case 8: goto L_0x08fb;
                case 9: goto L_0x0931;
                default: goto L_0x0035;
            }
        L_0x0035:
            r34 = -2
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r28 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r28
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
        L_0x0069:
            return r4
        L_0x006a:
            r0 = r32
            int r4 = r0.end
            int r26 = r4 - r30
            goto L_0x002e
        L_0x0071:
            r4 = 3
            r0 = r25
            if (r0 >= r4) goto L_0x00c4
            if (r27 == 0) goto L_0x008f
            r34 = 0
            int r27 = r27 + -1
            r0 = r33
            byte[] r4 = r0.next_in
            int r28 = r29 + 1
            byte r4 = r4[r29]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r25
            r20 = r20 | r4
            int r25 = r25 + 8
            r29 = r28
            goto L_0x0071
        L_0x008f:
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r29 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r29
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            r28 = r29
            goto L_0x0069
        L_0x00c4:
            r31 = r20 & 7
            r4 = r31 & 1
            r0 = r32
            r0.last = r4
            int r4 = r31 >>> 1
            switch(r4) {
                case 0: goto L_0x00d5;
                case 1: goto L_0x00e5;
                case 2: goto L_0x0117;
                case 3: goto L_0x0121;
                default: goto L_0x00d1;
            }
        L_0x00d1:
            r28 = r29
            goto L_0x002e
        L_0x00d5:
            int r20 = r20 >>> 3
            int r25 = r25 + -3
            r31 = r25 & 7
            int r20 = r20 >>> r31
            int r25 = r25 - r31
            r4 = 1
            r0 = r32
            r0.mode = r4
            goto L_0x00d1
        L_0x00e5:
            r4 = 1
            int[] r8 = new int[r4]
            r4 = 1
            int[] r9 = new int[r4]
            r4 = 1
            int[][] r10 = new int[r4][]
            r4 = 1
            int[][] r11 = new int[r4][]
            r0 = r33
            com.jcraft.jzlib.InfTree.inflate_trees_fixed(r8, r9, r10, r11, r0)
            r0 = r32
            com.jcraft.jzlib.InfCodes r4 = r0.codes
            r5 = 0
            r5 = r8[r5]
            r6 = 0
            r6 = r9[r6]
            r7 = 0
            r7 = r10[r7]
            r8 = 0
            r12 = 0
            r9 = r11[r12]
            r10 = 0
            r11 = r33
            r4.init(r5, r6, r7, r8, r9, r10, r11)
            int r20 = r20 >>> 3
            int r25 = r25 + -3
            r4 = 6
            r0 = r32
            r0.mode = r4
            goto L_0x00d1
        L_0x0117:
            int r20 = r20 >>> 3
            int r25 = r25 + -3
            r4 = 3
            r0 = r32
            r0.mode = r4
            goto L_0x00d1
        L_0x0121:
            int r20 = r20 >>> 3
            int r25 = r25 + -3
            r4 = 9
            r0 = r32
            r0.mode = r4
            java.lang.String r4 = "invalid block type"
            r0 = r33
            r0.msg = r4
            r34 = -3
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r29 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r29
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            r28 = r29
            goto L_0x0069
        L_0x0169:
            r4 = 32
            r0 = r25
            if (r0 >= r4) goto L_0x01be
            if (r27 == 0) goto L_0x0188
            r34 = 0
            int r27 = r27 + -1
            r0 = r33
            byte[] r4 = r0.next_in
            int r28 = r29 + 1
            byte r4 = r4[r29]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r25
            r20 = r20 | r4
            int r25 = r25 + 8
            r29 = r28
            goto L_0x0169
        L_0x0188:
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r29 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r29
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            r28 = r29
            goto L_0x0069
        L_0x01be:
            r4 = r20 ^ -1
            int r4 = r4 >>> 16
            r5 = 65535(0xffff, float:9.1834E-41)
            r4 = r4 & r5
            r5 = 65535(0xffff, float:9.1834E-41)
            r5 = r5 & r20
            if (r4 == r5) goto L_0x0211
            r4 = 9
            r0 = r32
            r0.mode = r4
            java.lang.String r4 = "invalid stored block lengths"
            r0 = r33
            r0.msg = r4
            r34 = -3
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r29 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r29
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            r28 = r29
            goto L_0x0069
        L_0x0211:
            r4 = 65535(0xffff, float:9.1834E-41)
            r4 = r4 & r20
            r0 = r32
            r0.left = r4
            r25 = 0
            r20 = r25
            r0 = r32
            int r4 = r0.left
            if (r4 == 0) goto L_0x022d
            r4 = 2
        L_0x0225:
            r0 = r32
            r0.mode = r4
            r28 = r29
            goto L_0x002e
        L_0x022d:
            r0 = r32
            int r4 = r0.last
            if (r4 == 0) goto L_0x0235
            r4 = 7
            goto L_0x0225
        L_0x0235:
            r4 = 0
            goto L_0x0225
        L_0x0237:
            if (r27 != 0) goto L_0x026d
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r28 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r28
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            goto L_0x0069
        L_0x026d:
            if (r26 != 0) goto L_0x031c
            r0 = r32
            int r4 = r0.end
            r0 = r30
            if (r0 != r4) goto L_0x028f
            r0 = r32
            int r4 = r0.read
            if (r4 == 0) goto L_0x028f
            r30 = 0
            r0 = r32
            int r4 = r0.read
            r0 = r30
            if (r0 >= r4) goto L_0x0307
            r0 = r32
            int r4 = r0.read
            int r4 = r4 - r30
            int r26 = r4 + -1
        L_0x028f:
            if (r26 != 0) goto L_0x031c
            r0 = r30
            r1 = r32
            r1.write = r0
            int r34 = r32.inflate_flush(r33, r34)
            r0 = r32
            int r0 = r0.write
            r30 = r0
            r0 = r32
            int r4 = r0.read
            r0 = r30
            if (r0 >= r4) goto L_0x030e
            r0 = r32
            int r4 = r0.read
            int r4 = r4 - r30
            int r26 = r4 + -1
        L_0x02b1:
            r0 = r32
            int r4 = r0.end
            r0 = r30
            if (r0 != r4) goto L_0x02d1
            r0 = r32
            int r4 = r0.read
            if (r4 == 0) goto L_0x02d1
            r30 = 0
            r0 = r32
            int r4 = r0.read
            r0 = r30
            if (r0 >= r4) goto L_0x0315
            r0 = r32
            int r4 = r0.read
            int r4 = r4 - r30
            int r26 = r4 + -1
        L_0x02d1:
            if (r26 != 0) goto L_0x031c
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r28 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r28
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            goto L_0x0069
        L_0x0307:
            r0 = r32
            int r4 = r0.end
            int r26 = r4 - r30
            goto L_0x028f
        L_0x030e:
            r0 = r32
            int r4 = r0.end
            int r26 = r4 - r30
            goto L_0x02b1
        L_0x0315:
            r0 = r32
            int r4 = r0.end
            int r26 = r4 - r30
            goto L_0x02d1
        L_0x031c:
            r34 = 0
            r0 = r32
            int r0 = r0.left
            r31 = r0
            r0 = r31
            r1 = r27
            if (r0 <= r1) goto L_0x032c
            r31 = r27
        L_0x032c:
            r0 = r31
            r1 = r26
            if (r0 <= r1) goto L_0x0334
            r31 = r26
        L_0x0334:
            r0 = r33
            byte[] r4 = r0.next_in
            r0 = r32
            byte[] r5 = r0.window
            r0 = r28
            r1 = r30
            r2 = r31
            java.lang.System.arraycopy(r4, r0, r5, r1, r2)
            int r28 = r28 + r31
            int r27 = r27 - r31
            int r30 = r30 + r31
            int r26 = r26 - r31
            r0 = r32
            int r4 = r0.left
            int r4 = r4 - r31
            r0 = r32
            r0.left = r4
            if (r4 != 0) goto L_0x002e
            r0 = r32
            int r4 = r0.last
            if (r4 == 0) goto L_0x0366
            r4 = 7
        L_0x0360:
            r0 = r32
            r0.mode = r4
            goto L_0x002e
        L_0x0366:
            r4 = 0
            goto L_0x0360
        L_0x0368:
            r4 = 14
            r0 = r25
            if (r0 >= r4) goto L_0x03bd
            if (r27 == 0) goto L_0x0387
            r34 = 0
            int r27 = r27 + -1
            r0 = r33
            byte[] r4 = r0.next_in
            int r28 = r29 + 1
            byte r4 = r4[r29]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r25
            r20 = r20 | r4
            int r25 = r25 + 8
            r29 = r28
            goto L_0x0368
        L_0x0387:
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r29 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r29
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            r28 = r29
            goto L_0x0069
        L_0x03bd:
            r0 = r20
            r0 = r0 & 16383(0x3fff, float:2.2957E-41)
            r31 = r0
            r0 = r31
            r1 = r32
            r1.table = r0
            r4 = r31 & 31
            r5 = 29
            if (r4 > r5) goto L_0x03d7
            int r4 = r31 >> 5
            r4 = r4 & 31
            r5 = 29
            if (r4 <= r5) goto L_0x041b
        L_0x03d7:
            r4 = 9
            r0 = r32
            r0.mode = r4
            java.lang.String r4 = "too many length or distance symbols"
            r0 = r33
            r0.msg = r4
            r34 = -3
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r29 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r29
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            r28 = r29
            goto L_0x0069
        L_0x041b:
            r4 = r31 & 31
            int r4 = r4 + 258
            int r5 = r31 >> 5
            r5 = r5 & 31
            int r31 = r4 + r5
            r0 = r32
            int[] r4 = r0.blens
            if (r4 == 0) goto L_0x0434
            r0 = r32
            int[] r4 = r0.blens
            int r4 = r4.length
            r0 = r31
            if (r4 >= r0) goto L_0x047a
        L_0x0434:
            r0 = r31
            int[] r4 = new int[r0]
            r0 = r32
            r0.blens = r4
        L_0x043c:
            int r20 = r20 >>> 14
            int r25 = r25 + -14
            r4 = 0
            r0 = r32
            r0.index = r4
            r4 = 4
            r0 = r32
            r0.mode = r4
            r28 = r29
        L_0x044c:
            r0 = r32
            int r4 = r0.index
            r0 = r32
            int r5 = r0.table
            int r5 = r5 >>> 10
            int r5 = r5 + 4
            if (r4 >= r5) goto L_0x04e0
            r29 = r28
        L_0x045c:
            r4 = 3
            r0 = r25
            if (r0 >= r4) goto L_0x04c2
            if (r27 == 0) goto L_0x048c
            r34 = 0
            int r27 = r27 + -1
            r0 = r33
            byte[] r4 = r0.next_in
            int r28 = r29 + 1
            byte r4 = r4[r29]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r25
            r20 = r20 | r4
            int r25 = r25 + 8
            r29 = r28
            goto L_0x045c
        L_0x047a:
            r22 = 0
        L_0x047c:
            r0 = r22
            r1 = r31
            if (r0 >= r1) goto L_0x043c
            r0 = r32
            int[] r4 = r0.blens
            r5 = 0
            r4[r22] = r5
            int r22 = r22 + 1
            goto L_0x047c
        L_0x048c:
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r29 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r29
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            r28 = r29
            goto L_0x0069
        L_0x04c2:
            r0 = r32
            int[] r4 = r0.blens
            int[] r5 = com.jcraft.jzlib.InfBlocks.border
            r0 = r32
            int r6 = r0.index
            int r7 = r6 + 1
            r0 = r32
            r0.index = r7
            r5 = r5[r6]
            r6 = r20 & 7
            r4[r5] = r6
            int r20 = r20 >>> 3
            int r25 = r25 + -3
            r28 = r29
            goto L_0x044c
        L_0x04e0:
            r0 = r32
            int r4 = r0.index
            r5 = 19
            if (r4 >= r5) goto L_0x04fe
            r0 = r32
            int[] r4 = r0.blens
            int[] r5 = com.jcraft.jzlib.InfBlocks.border
            r0 = r32
            int r6 = r0.index
            int r7 = r6 + 1
            r0 = r32
            r0.index = r7
            r5 = r5[r6]
            r6 = 0
            r4[r5] = r6
            goto L_0x04e0
        L_0x04fe:
            r0 = r32
            int[] r4 = r0.bb
            r5 = 0
            r6 = 7
            r4[r5] = r6
            r0 = r32
            com.jcraft.jzlib.InfTree r4 = r0.inftree
            r0 = r32
            int[] r5 = r0.blens
            r0 = r32
            int[] r6 = r0.bb
            r0 = r32
            int[] r7 = r0.tb
            r0 = r32
            int[] r8 = r0.hufts
            r9 = r33
            int r31 = r4.inflate_trees_bits(r5, r6, r7, r8, r9)
            if (r31 == 0) goto L_0x0568
            r34 = r31
            r4 = -3
            r0 = r34
            if (r0 != r4) goto L_0x0534
            r4 = 0
            r0 = r32
            r0.blens = r4
            r4 = 9
            r0 = r32
            r0.mode = r4
        L_0x0534:
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r28 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r28
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            goto L_0x0069
        L_0x0568:
            r4 = 0
            r0 = r32
            r0.index = r4
            r4 = 5
            r0 = r32
            r0.mode = r4
        L_0x0572:
            r0 = r32
            int r0 = r0.table
            r31 = r0
            r0 = r32
            int r4 = r0.index
            r5 = r31 & 31
            int r5 = r5 + 258
            int r6 = r31 >> 5
            r6 = r6 & 31
            int r5 = r5 + r6
            if (r4 < r5) goto L_0x060e
            r0 = r32
            int[] r4 = r0.tb
            r5 = 0
            r6 = -1
            r4[r5] = r6
            r4 = 1
            int[] r8 = new int[r4]
            r4 = 1
            int[] r9 = new int[r4]
            r4 = 1
            int[] r10 = new int[r4]
            r4 = 1
            int[] r11 = new int[r4]
            r4 = 0
            r5 = 9
            r8[r4] = r5
            r4 = 0
            r5 = 6
            r9[r4] = r5
            r0 = r32
            int r0 = r0.table
            r31 = r0
            r0 = r32
            com.jcraft.jzlib.InfTree r4 = r0.inftree
            r5 = r31 & 31
            int r5 = r5 + 257
            int r6 = r31 >> 5
            r6 = r6 & 31
            int r6 = r6 + 1
            r0 = r32
            int[] r7 = r0.blens
            r0 = r32
            int[] r12 = r0.hufts
            r13 = r33
            int r31 = r4.inflate_trees_dynamic(r5, r6, r7, r8, r9, r10, r11, r12, r13)
            if (r31 == 0) goto L_0x07d3
            r4 = -3
            r0 = r31
            if (r0 != r4) goto L_0x05d8
            r4 = 0
            r0 = r32
            r0.blens = r4
            r4 = 9
            r0 = r32
            r0.mode = r4
        L_0x05d8:
            r34 = r31
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r28 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r28
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            goto L_0x0069
        L_0x060e:
            r0 = r32
            int[] r4 = r0.bb
            r5 = 0
            r31 = r4[r5]
            r29 = r28
        L_0x0617:
            r0 = r25
            r1 = r31
            if (r0 >= r1) goto L_0x066c
            if (r27 == 0) goto L_0x0636
            r34 = 0
            int r27 = r27 + -1
            r0 = r33
            byte[] r4 = r0.next_in
            int r28 = r29 + 1
            byte r4 = r4[r29]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r25
            r20 = r20 | r4
            int r25 = r25 + 8
            r29 = r28
            goto L_0x0617
        L_0x0636:
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r29 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r29
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            r28 = r29
            goto L_0x0069
        L_0x066c:
            r0 = r32
            int[] r4 = r0.tb
            r5 = 0
            r4 = r4[r5]
            r5 = -1
            if (r4 != r5) goto L_0x0676
        L_0x0676:
            r0 = r32
            int[] r4 = r0.hufts
            r0 = r32
            int[] r5 = r0.tb
            r6 = 0
            r5 = r5[r6]
            int[] r6 = com.jcraft.jzlib.InfBlocks.inflate_mask
            r6 = r6[r31]
            r6 = r6 & r20
            int r5 = r5 + r6
            int r5 = r5 * 3
            int r5 = r5 + 1
            r31 = r4[r5]
            r0 = r32
            int[] r4 = r0.hufts
            r0 = r32
            int[] r5 = r0.tb
            r6 = 0
            r5 = r5[r6]
            int[] r6 = com.jcraft.jzlib.InfBlocks.inflate_mask
            r6 = r6[r31]
            r6 = r6 & r20
            int r5 = r5 + r6
            int r5 = r5 * 3
            int r5 = r5 + 2
            r21 = r4[r5]
            r4 = 16
            r0 = r21
            if (r0 >= r4) goto L_0x06c4
            int r20 = r20 >>> r31
            int r25 = r25 - r31
            r0 = r32
            int[] r4 = r0.blens
            r0 = r32
            int r5 = r0.index
            int r6 = r5 + 1
            r0 = r32
            r0.index = r6
            r4[r5] = r21
            r28 = r29
            goto L_0x0572
        L_0x06c4:
            r4 = 18
            r0 = r21
            if (r0 != r4) goto L_0x06f3
            r22 = 7
        L_0x06cc:
            r4 = 18
            r0 = r21
            if (r0 != r4) goto L_0x06f6
            r24 = 11
        L_0x06d4:
            int r4 = r31 + r22
            r0 = r25
            if (r0 >= r4) goto L_0x072f
            if (r27 == 0) goto L_0x06f9
            r34 = 0
            int r27 = r27 + -1
            r0 = r33
            byte[] r4 = r0.next_in
            int r28 = r29 + 1
            byte r4 = r4[r29]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r25
            r20 = r20 | r4
            int r25 = r25 + 8
            r29 = r28
            goto L_0x06d4
        L_0x06f3:
            int r22 = r21 + -14
            goto L_0x06cc
        L_0x06f6:
            r24 = 3
            goto L_0x06d4
        L_0x06f9:
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r29 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r29
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            r28 = r29
            goto L_0x0069
        L_0x072f:
            int r20 = r20 >>> r31
            int r25 = r25 - r31
            int[] r4 = com.jcraft.jzlib.InfBlocks.inflate_mask
            r4 = r4[r22]
            r4 = r4 & r20
            int r24 = r24 + r4
            int r20 = r20 >>> r22
            int r25 = r25 - r22
            r0 = r32
            int r0 = r0.index
            r22 = r0
            r0 = r32
            int r0 = r0.table
            r31 = r0
            int r4 = r22 + r24
            r5 = r31 & 31
            int r5 = r5 + 258
            int r6 = r31 >> 5
            r6 = r6 & 31
            int r5 = r5 + r6
            if (r4 > r5) goto L_0x0763
            r4 = 16
            r0 = r21
            if (r0 != r4) goto L_0x07ac
            r4 = 1
            r0 = r22
            if (r0 >= r4) goto L_0x07ac
        L_0x0763:
            r4 = 0
            r0 = r32
            r0.blens = r4
            r4 = 9
            r0 = r32
            r0.mode = r4
            java.lang.String r4 = "invalid bit length repeat"
            r0 = r33
            r0.msg = r4
            r34 = -3
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r29 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r29
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            r28 = r29
            goto L_0x0069
        L_0x07ac:
            r4 = 16
            r0 = r21
            if (r0 != r4) goto L_0x07d0
            r0 = r32
            int[] r4 = r0.blens
            int r5 = r22 + -1
            r21 = r4[r5]
        L_0x07ba:
            r0 = r32
            int[] r4 = r0.blens
            int r23 = r22 + 1
            r4[r22] = r21
            int r24 = r24 + -1
            if (r24 != 0) goto L_0x0967
            r0 = r23
            r1 = r32
            r1.index = r0
            r28 = r29
            goto L_0x0572
        L_0x07d0:
            r21 = 0
            goto L_0x07ba
        L_0x07d3:
            r0 = r32
            com.jcraft.jzlib.InfCodes r12 = r0.codes
            r4 = 0
            r13 = r8[r4]
            r4 = 0
            r14 = r9[r4]
            r0 = r32
            int[] r15 = r0.hufts
            r4 = 0
            r16 = r10[r4]
            r0 = r32
            int[] r0 = r0.hufts
            r17 = r0
            r4 = 0
            r18 = r11[r4]
            r19 = r33
            r12.init(r13, r14, r15, r16, r17, r18, r19)
            r4 = 6
            r0 = r32
            r0.mode = r4
        L_0x07f7:
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r28 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r28
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            r0 = r32
            com.jcraft.jzlib.InfCodes r4 = r0.codes
            r0 = r32
            r1 = r33
            r2 = r34
            int r34 = r4.proc(r0, r1, r2)
            r4 = 1
            r0 = r34
            if (r0 == r4) goto L_0x083e
            int r4 = r32.inflate_flush(r33, r34)
            goto L_0x0069
        L_0x083e:
            r34 = 0
            r0 = r32
            com.jcraft.jzlib.InfCodes r4 = r0.codes
            r0 = r33
            r4.free(r0)
            r0 = r33
            int r0 = r0.next_in_index
            r28 = r0
            r0 = r33
            int r0 = r0.avail_in
            r27 = r0
            r0 = r32
            int r0 = r0.bitb
            r20 = r0
            r0 = r32
            int r0 = r0.bitk
            r25 = r0
            r0 = r32
            int r0 = r0.write
            r30 = r0
            r0 = r32
            int r4 = r0.read
            r0 = r30
            if (r0 >= r4) goto L_0x0884
            r0 = r32
            int r4 = r0.read
            int r4 = r4 - r30
            int r26 = r4 + -1
        L_0x0877:
            r0 = r32
            int r4 = r0.last
            if (r4 != 0) goto L_0x088b
            r4 = 0
            r0 = r32
            r0.mode = r4
            goto L_0x002e
        L_0x0884:
            r0 = r32
            int r4 = r0.end
            int r26 = r4 - r30
            goto L_0x0877
        L_0x088b:
            r4 = 7
            r0 = r32
            r0.mode = r4
        L_0x0890:
            r0 = r30
            r1 = r32
            r1.write = r0
            int r34 = r32.inflate_flush(r33, r34)
            r0 = r32
            int r0 = r0.write
            r30 = r0
            r0 = r32
            int r4 = r0.read
            r0 = r30
            if (r0 >= r4) goto L_0x08ee
            r0 = r32
            int r4 = r0.read
            int r4 = r4 - r30
            int r26 = r4 + -1
        L_0x08b0:
            r0 = r32
            int r4 = r0.read
            r0 = r32
            int r5 = r0.write
            if (r4 == r5) goto L_0x08f5
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r28 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r28
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            goto L_0x0069
        L_0x08ee:
            r0 = r32
            int r4 = r0.end
            int r26 = r4 - r30
            goto L_0x08b0
        L_0x08f5:
            r4 = 8
            r0 = r32
            r0.mode = r4
        L_0x08fb:
            r34 = 1
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r28 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r28
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            goto L_0x0069
        L_0x0931:
            r34 = -3
            r0 = r20
            r1 = r32
            r1.bitb = r0
            r0 = r25
            r1 = r32
            r1.bitk = r0
            r0 = r27
            r1 = r33
            r1.avail_in = r0
            r0 = r33
            long r4 = r0.total_in
            r0 = r33
            int r6 = r0.next_in_index
            int r6 = r28 - r6
            long r6 = (long) r6
            long r4 = r4 + r6
            r0 = r33
            r0.total_in = r4
            r0 = r28
            r1 = r33
            r1.next_in_index = r0
            r0 = r30
            r1 = r32
            r1.write = r0
            int r4 = r32.inflate_flush(r33, r34)
            goto L_0x0069
        L_0x0967:
            r22 = r23
            goto L_0x07ba
        L_0x096b:
            r29 = r28
            goto L_0x0368
        L_0x096f:
            r29 = r28
            goto L_0x0169
        L_0x0973:
            r29 = r28
            goto L_0x0071
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jcraft.jzlib.InfBlocks.proc(com.jcraft.jzlib.ZStream, int):int");
    }

    /* access modifiers changed from: package-private */
    public void free(ZStream z) {
        reset(z, null);
        this.window = null;
        this.hufts = null;
    }

    /* access modifiers changed from: package-private */
    public void set_dictionary(byte[] d, int start, int n) {
        System.arraycopy(d, start, this.window, 0, n);
        this.write = n;
        this.read = n;
    }

    /* access modifiers changed from: package-private */
    public int sync_point() {
        return this.mode == 1 ? 1 : 0;
    }

    /* access modifiers changed from: package-private */
    public int inflate_flush(ZStream z, int r) {
        int p = z.next_out_index;
        int q = this.read;
        int n = (q <= this.write ? this.write : this.end) - q;
        if (n > z.avail_out) {
            n = z.avail_out;
        }
        if (n != 0 && r == -5) {
            r = 0;
        }
        z.avail_out -= n;
        z.total_out += (long) n;
        if (this.checkfn != null) {
            long adler32 = z._adler.adler32(this.check, this.window, q, n);
            this.check = adler32;
            z.adler = adler32;
        }
        System.arraycopy(this.window, q, z.next_out, p, n);
        int p2 = p + n;
        int q2 = q + n;
        if (q2 == this.end) {
            if (this.write == this.end) {
                this.write = 0;
            }
            int n2 = this.write - 0;
            if (n2 > z.avail_out) {
                n2 = z.avail_out;
            }
            if (n2 != 0 && r == -5) {
                r = 0;
            }
            z.avail_out -= n2;
            z.total_out += (long) n2;
            if (this.checkfn != null) {
                long adler322 = z._adler.adler32(this.check, this.window, 0, n2);
                this.check = adler322;
                z.adler = adler322;
            }
            System.arraycopy(this.window, 0, z.next_out, p2, n2);
            p2 += n2;
            q2 = 0 + n2;
        }
        z.next_out_index = p2;
        this.read = q2;
        return r;
    }
}
