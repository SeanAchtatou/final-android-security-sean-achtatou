package com.google.tagmanager.protobuf.nano;

import java.io.IOException;
import java.util.Arrays;

public abstract class MessageNano {
    protected int cachedSize = -1;

    public abstract MessageNano mergeFrom(CodedInputByteBufferNano codedInputByteBufferNano) throws IOException;

    public abstract void writeTo(CodedOutputByteBufferNano codedOutputByteBufferNano) throws IOException;

    public int getCachedSize() {
        if (this.cachedSize < 0) {
            getSerializedSize();
        }
        return this.cachedSize;
    }

    public int getSerializedSize() {
        this.cachedSize = 0;
        return 0;
    }

    public static final byte[] toByteArray(MessageNano msg) {
        byte[] result = new byte[msg.getSerializedSize()];
        toByteArray(msg, result, 0, result.length);
        return result;
    }

    public static final void toByteArray(MessageNano msg, byte[] data, int offset, int length) {
        try {
            CodedOutputByteBufferNano output = CodedOutputByteBufferNano.newInstance(data, offset, length);
            msg.writeTo(output);
            output.checkNoSpaceLeft();
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public static final <T extends MessageNano> T mergeFrom(T msg, byte[] data) throws InvalidProtocolBufferNanoException {
        return mergeFrom(msg, data, 0, data.length);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static final <T extends com.google.tagmanager.protobuf.nano.MessageNano> T mergeFrom(T r4, byte[] r5, int r6, int r7) throws com.google.tagmanager.protobuf.nano.InvalidProtocolBufferNanoException {
        /*
            com.google.tagmanager.protobuf.nano.CodedInputByteBufferNano r1 = com.google.tagmanager.protobuf.nano.CodedInputByteBufferNano.newInstance(r5, r6, r7)     // Catch:{ InvalidProtocolBufferNanoException -> 0x000c, IOException -> 0x000e }
            r4.mergeFrom(r1)     // Catch:{ InvalidProtocolBufferNanoException -> 0x000c, IOException -> 0x000e }
            r2 = 0
            r1.checkLastTagWas(r2)     // Catch:{ InvalidProtocolBufferNanoException -> 0x000c, IOException -> 0x000e }
            return r4
        L_0x000c:
            r0 = move-exception
            throw r0
        L_0x000e:
            r0 = move-exception
            java.lang.RuntimeException r2 = new java.lang.RuntimeException
            java.lang.String r3 = "Reading from a byte array threw an IOException (should never happen)."
            r2.<init>(r3)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.tagmanager.protobuf.nano.MessageNano.mergeFrom(com.google.tagmanager.protobuf.nano.MessageNano, byte[], int, int):com.google.tagmanager.protobuf.nano.MessageNano");
    }

    public static final boolean messageNanoEquals(MessageNano a, MessageNano b) {
        int serializedSize;
        if (a == b) {
            return true;
        }
        if (a == null || b == null || a.getClass() != b.getClass() || b.getSerializedSize() != (serializedSize = a.getSerializedSize())) {
            return false;
        }
        byte[] aByteArray = new byte[serializedSize];
        byte[] bByteArray = new byte[serializedSize];
        toByteArray(a, aByteArray, 0, serializedSize);
        toByteArray(b, bByteArray, 0, serializedSize);
        return Arrays.equals(aByteArray, bByteArray);
    }

    public String toString() {
        return MessageNanoPrinter.print(this);
    }
}
