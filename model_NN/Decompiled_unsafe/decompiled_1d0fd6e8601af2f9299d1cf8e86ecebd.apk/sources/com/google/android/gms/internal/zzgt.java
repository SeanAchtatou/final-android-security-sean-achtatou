package com.google.android.gms.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import com.google.android.gms.internal.zzgr;
import com.google.android.gms.internal.zzif;

@TargetApi(19)
@zzhb
public class zzgt extends zzgs {
    private Object zzGv = new Object();
    private PopupWindow zzGw;
    private boolean zzGx = false;

    zzgt(Context context, zzif.zza zza, zzjp zzjp, zzgr.zza zza2) {
        super(context, zza, zzjp, zza2);
    }

    private void zzgj() {
        synchronized (this.zzGv) {
            this.zzGx = true;
            if ((this.mContext instanceof Activity) && ((Activity) this.mContext).isDestroyed()) {
                this.zzGw = null;
            }
            if (this.zzGw != null) {
                if (this.zzGw.isShowing()) {
                    this.zzGw.dismiss();
                }
                this.zzGw = null;
            }
        }
    }

    public void cancel() {
        zzgj();
        super.cancel();
    }

    /* access modifiers changed from: protected */
    public void zzC(int i) {
        zzgj();
        super.zzC(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [android.widget.FrameLayout, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    /* access modifiers changed from: protected */
    public void zzgi() {
        Window window = this.mContext instanceof Activity ? ((Activity) this.mContext).getWindow() : null;
        if (window != null && window.getDecorView() != null && !((Activity) this.mContext).isDestroyed()) {
            FrameLayout frameLayout = new FrameLayout(this.mContext);
            frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            frameLayout.addView(this.zzpD.getView(), -1, -1);
            synchronized (this.zzGv) {
                if (!this.zzGx) {
                    this.zzGw = new PopupWindow((View) frameLayout, 1, 1, false);
                    this.zzGw.setOutsideTouchable(true);
                    this.zzGw.setClippingEnabled(false);
                    zzin.zzaI("Displaying the 1x1 popup off the screen.");
                    try {
                        this.zzGw.showAtLocation(window.getDecorView(), 0, -1, -1);
                    } catch (Exception e) {
                        this.zzGw = null;
                    }
                }
            }
        }
    }
}
