package com.google.android.gms.internal;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@zzhb
public class zzdu extends zzdr {
    private static final Set<String> zzzX = Collections.synchronizedSet(new HashSet());
    private static final DecimalFormat zzzY = new DecimalFormat("#,###");
    private boolean zzAa;
    private File zzzZ;

    public zzdu(zzjp zzjp) {
        super(zzjp);
        File cacheDir = this.mContext.getCacheDir();
        if (cacheDir == null) {
            zzin.zzaK("Context.getCacheDir() returned null");
            return;
        }
        this.zzzZ = new File(cacheDir, "admobVideoStreams");
        if (!this.zzzZ.isDirectory() && !this.zzzZ.mkdirs()) {
            zzin.zzaK("Could not create preload cache directory at " + this.zzzZ.getAbsolutePath());
            this.zzzZ = null;
        } else if (!this.zzzZ.setReadable(true, false) || !this.zzzZ.setExecutable(true, false)) {
            zzin.zzaK("Could not set cache file permissions at " + this.zzzZ.getAbsolutePath());
            this.zzzZ = null;
        }
    }

    private File zza(File file) {
        return new File(this.zzzZ, file.getName() + ".done");
    }

    private static void zzb(File file) {
        if (file.isFile()) {
            file.setLastModified(System.currentTimeMillis());
            return;
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
        }
    }

    public void abort() {
        this.zzAa = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzih.zzb(java.lang.Throwable, boolean):void
     arg types: [java.lang.Throwable, int]
     candidates:
      com.google.android.gms.internal.zzih.zzb(android.content.Context, com.google.android.gms.ads.internal.util.client.VersionInfoParcel):void
      com.google.android.gms.internal.zzih.zzb(java.lang.Throwable, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x033d, code lost:
        if (r0.tryAcquire() == false) goto L_0x02af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x033f, code lost:
        zza(r27, r11.getAbsolutePath(), r5, r6, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x034d, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x034e, code lost:
        r3 = null;
        r4 = org.openintents.openpgp.util.OpenPgpApi.RESULT_ERROR;
        r5 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0353, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x035b, code lost:
        if (com.google.android.gms.internal.zzin.zzQ(3) == false) goto L_0x0386;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x035d, code lost:
        com.google.android.gms.internal.zzin.zzaI("Preloaded " + com.google.android.gms.internal.zzdu.zzzY.format((long) r5) + " bytes from " + r27);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0386, code lost:
        r11.setReadable(true, false);
        zzb(r12);
        zza(r27, r11.getAbsolutePath(), r5);
        com.google.android.gms.internal.zzdu.zzzX.remove(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x039e, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x03a1, code lost:
        com.google.android.gms.internal.zzin.zzd("Preload failed for URL \"" + r27 + "\"", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x03c7, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x03c8, code lost:
        r3 = null;
        r4 = org.openintents.openpgp.util.OpenPgpApi.RESULT_ERROR;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x03cc, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x03cd, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x03d3, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x03d4, code lost:
        r3 = null;
        r4 = org.openintents.openpgp.util.OpenPgpApi.RESULT_ERROR;
        r5 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x03d9, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x03da, code lost:
        r3 = null;
        r5 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x03de, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x03df, code lost:
        r3 = null;
        r4 = org.openintents.openpgp.util.OpenPgpApi.RESULT_ERROR;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x03e3, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x03e4, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00df, code lost:
        r5 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r3 = new java.net.URL(r27).openConnection();
        r2 = com.google.android.gms.internal.zzbt.zzvT.get().intValue();
        r3.setConnectTimeout(r2);
        r3.setReadTimeout(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0102, code lost:
        if ((r3 instanceof java.net.HttpURLConnection) == false) goto L_0x01bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0104, code lost:
        r2 = ((java.net.HttpURLConnection) r3).getResponseCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x010e, code lost:
        if (r2 < 400) goto L_0x01bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0110, code lost:
        r4 = "badUrl";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r3 = "HTTP request failed. Code: " + java.lang.Integer.toString(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x014d, code lost:
        throw new java.io.IOException("HTTP status code " + r2 + " at " + r27);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x014e, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0151, code lost:
        if ((r2 instanceof java.lang.RuntimeException) != false) goto L_0x0153;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0153, code lost:
        com.google.android.gms.ads.internal.zzr.zzbF().zzb(r2, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0162, code lost:
        if (r0.zzAa != false) goto L_0x0164;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0164, code lost:
        com.google.android.gms.internal.zzin.zzaJ("Preload aborted for URL \"" + r27 + "\"");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x018e, code lost:
        com.google.android.gms.internal.zzin.zzaK("Could not delete partial cache file at " + r11.getAbsolutePath());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x01a8, code lost:
        zza(r27, r11.getAbsolutePath(), r4, r3);
        com.google.android.gms.internal.zzdu.zzzX.remove(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        r6 = r3.getContentLength();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01bf, code lost:
        if (r6 >= 0) goto L_0x01ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x01c1, code lost:
        com.google.android.gms.internal.zzin.zzaK("Stream cache aborted, missing content-length header at " + r27);
        zza(r27, r11.getAbsolutePath(), "contentLengthMissing", null);
        com.google.android.gms.internal.zzdu.zzzX.remove(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x01ef, code lost:
        r4 = com.google.android.gms.internal.zzdu.zzzY.format((long) r6);
        r14 = com.google.android.gms.internal.zzbt.zzvP.get().intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0202, code lost:
        if (r6 <= r14) goto L_0x024e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0204, code lost:
        com.google.android.gms.internal.zzin.zzaK("Content length " + r4 + " exceeds limit at " + r27);
        zza(r27, r11.getAbsolutePath(), "sizeExceeded", "File too big for full file cache. Size: " + r4);
        com.google.android.gms.internal.zzdu.zzzX.remove(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x024e, code lost:
        com.google.android.gms.internal.zzin.zzaI("Caching " + r4 + " bytes from " + r27);
        r15 = java.nio.channels.Channels.newChannel(r3.getInputStream());
        r10 = new java.io.FileOutputStream(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        r16 = r10.getChannel();
        r17 = java.nio.ByteBuffer.allocate(1048576);
        r18 = com.google.android.gms.ads.internal.zzr.zzbG();
        r5 = 0;
        r20 = r18.currentTimeMillis();
        r0 = new com.google.android.gms.internal.zziz(com.google.android.gms.internal.zzbt.zzvS.get().longValue());
        r22 = com.google.android.gms.internal.zzbt.zzvR.get().longValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x02af, code lost:
        r2 = r15.read(r17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x02b5, code lost:
        if (r2 < 0) goto L_0x0353;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x02b7, code lost:
        r5 = r5 + r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x02b8, code lost:
        if (r5 <= r14) goto L_0x02df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        r3 = "File too big for full file cache. Size: " + java.lang.Integer.toString(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x02da, code lost:
        throw new java.io.IOException("stream cache file size limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x02db, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x02dc, code lost:
        r5 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
        r17.flip();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x02e6, code lost:
        if (r16.write(r17) > 0) goto L_0x02e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x02e8, code lost:
        r17.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x02f7, code lost:
        if ((r18.currentTimeMillis() - r20) <= (1000 * r22)) goto L_0x0324;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
        r3 = "Timeout exceeded. Limit: " + java.lang.Long.toString(r22) + " sec";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x031f, code lost:
        throw new java.io.IOException("stream cache time limit exceeded");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x0320, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0321, code lost:
        r5 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0328, code lost:
        if (r0.zzAa == false) goto L_0x0339;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0333, code lost:
        throw new java.io.IOException("abort requested");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0334, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0335, code lost:
        r3 = null;
        r5 = r10;
     */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x03a1  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0153  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0164  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean zzU(java.lang.String r27) {
        /*
            r26 = this;
            r0 = r26
            java.io.File r2 = r0.zzzZ
            if (r2 != 0) goto L_0x0013
            r2 = 0
            java.lang.String r3 = "noCacheDir"
            r4 = 0
            r0 = r26
            r1 = r27
            r0.zza(r1, r2, r3, r4)
            r2 = 0
        L_0x0012:
            return r2
        L_0x0013:
            int r3 = r26.zzea()
            com.google.android.gms.internal.zzbp<java.lang.Integer> r2 = com.google.android.gms.internal.zzbt.zzvO
            java.lang.Object r2 = r2.get()
            java.lang.Integer r2 = (java.lang.Integer) r2
            int r2 = r2.intValue()
            if (r3 <= r2) goto L_0x003d
            boolean r2 = r26.zzeb()
            if (r2 != 0) goto L_0x0013
            java.lang.String r2 = "Unable to expire stream cache"
            com.google.android.gms.internal.zzin.zzaK(r2)
            r2 = 0
            java.lang.String r3 = "expireFailed"
            r4 = 0
            r0 = r26
            r1 = r27
            r0.zza(r1, r2, r3, r4)
            r2 = 0
            goto L_0x0012
        L_0x003d:
            java.lang.String r2 = r26.zzV(r27)
            java.io.File r11 = new java.io.File
            r0 = r26
            java.io.File r3 = r0.zzzZ
            r11.<init>(r3, r2)
            r0 = r26
            java.io.File r12 = r0.zza(r11)
            boolean r2 = r11.isFile()
            if (r2 == 0) goto L_0x0086
            boolean r2 = r12.isFile()
            if (r2 == 0) goto L_0x0086
            long r2 = r11.length()
            int r2 = (int) r2
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Stream cache hit at "
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r27
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            com.google.android.gms.internal.zzin.zzaI(r3)
            java.lang.String r3 = r11.getAbsolutePath()
            r0 = r26
            r1 = r27
            r0.zza(r1, r3, r2)
            r2 = 1
            goto L_0x0012
        L_0x0086:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r0 = r26
            java.io.File r3 = r0.zzzZ
            java.lang.String r3 = r3.getAbsolutePath()
            java.lang.StringBuilder r2 = r2.append(r3)
            r0 = r27
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r13 = r2.toString()
            java.util.Set<java.lang.String> r3 = com.google.android.gms.internal.zzdu.zzzX
            monitor-enter(r3)
            java.util.Set<java.lang.String> r2 = com.google.android.gms.internal.zzdu.zzzX     // Catch:{ all -> 0x00d6 }
            boolean r2 = r2.contains(r13)     // Catch:{ all -> 0x00d6 }
            if (r2 == 0) goto L_0x00d9
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d6 }
            r2.<init>()     // Catch:{ all -> 0x00d6 }
            java.lang.String r4 = "Stream cache already in progress at "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ all -> 0x00d6 }
            r0 = r27
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x00d6 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00d6 }
            com.google.android.gms.internal.zzin.zzaK(r2)     // Catch:{ all -> 0x00d6 }
            java.lang.String r2 = r11.getAbsolutePath()     // Catch:{ all -> 0x00d6 }
            java.lang.String r4 = "inProgress"
            r5 = 0
            r0 = r26
            r1 = r27
            r0.zza(r1, r2, r4, r5)     // Catch:{ all -> 0x00d6 }
            r2 = 0
            monitor-exit(r3)     // Catch:{ all -> 0x00d6 }
            goto L_0x0012
        L_0x00d6:
            r2 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00d6 }
            throw r2
        L_0x00d9:
            java.util.Set<java.lang.String> r2 = com.google.android.gms.internal.zzdu.zzzX     // Catch:{ all -> 0x00d6 }
            r2.add(r13)     // Catch:{ all -> 0x00d6 }
            monitor-exit(r3)     // Catch:{ all -> 0x00d6 }
            r5 = 0
            java.lang.String r9 = "error"
            r8 = 0
            java.net.URL r2 = new java.net.URL     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r0 = r27
            r2.<init>(r0)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.net.URLConnection r3 = r2.openConnection()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            com.google.android.gms.internal.zzbp<java.lang.Integer> r2 = com.google.android.gms.internal.zzbt.zzvT     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.Object r2 = r2.get()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            int r2 = r2.intValue()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r3.setConnectTimeout(r2)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r3.setReadTimeout(r2)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            boolean r2 = r3 instanceof java.net.HttpURLConnection     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            if (r2 == 0) goto L_0x01bb
            r0 = r3
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r2 = r0
            int r2 = r2.getResponseCode()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r4 = 400(0x190, float:5.6E-43)
            if (r2 < r4) goto L_0x01bb
            java.lang.String r4 = "badUrl"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x03e3, RuntimeException -> 0x03cc }
            r3.<init>()     // Catch:{ IOException -> 0x03e3, RuntimeException -> 0x03cc }
            java.lang.String r6 = "HTTP request failed. Code: "
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ IOException -> 0x03e3, RuntimeException -> 0x03cc }
            java.lang.String r6 = java.lang.Integer.toString(r2)     // Catch:{ IOException -> 0x03e3, RuntimeException -> 0x03cc }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ IOException -> 0x03e3, RuntimeException -> 0x03cc }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x03e3, RuntimeException -> 0x03cc }
            java.io.IOException r6 = new java.io.IOException     // Catch:{ IOException -> 0x014e, RuntimeException -> 0x03d0 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x014e, RuntimeException -> 0x03d0 }
            r7.<init>()     // Catch:{ IOException -> 0x014e, RuntimeException -> 0x03d0 }
            java.lang.String r8 = "HTTP status code "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ IOException -> 0x014e, RuntimeException -> 0x03d0 }
            java.lang.StringBuilder r2 = r7.append(r2)     // Catch:{ IOException -> 0x014e, RuntimeException -> 0x03d0 }
            java.lang.String r7 = " at "
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ IOException -> 0x014e, RuntimeException -> 0x03d0 }
            r0 = r27
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ IOException -> 0x014e, RuntimeException -> 0x03d0 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x014e, RuntimeException -> 0x03d0 }
            r6.<init>(r2)     // Catch:{ IOException -> 0x014e, RuntimeException -> 0x03d0 }
            throw r6     // Catch:{ IOException -> 0x014e, RuntimeException -> 0x03d0 }
        L_0x014e:
            r2 = move-exception
        L_0x014f:
            boolean r6 = r2 instanceof java.lang.RuntimeException
            if (r6 == 0) goto L_0x015b
            com.google.android.gms.internal.zzih r6 = com.google.android.gms.ads.internal.zzr.zzbF()
            r7 = 1
            r6.zzb(r2, r7)
        L_0x015b:
            r5.close()     // Catch:{ IOException -> 0x03c1, NullPointerException -> 0x03c4 }
        L_0x015e:
            r0 = r26
            boolean r5 = r0.zzAa
            if (r5 == 0) goto L_0x03a1
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r5 = "Preload aborted for URL \""
            java.lang.StringBuilder r2 = r2.append(r5)
            r0 = r27
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r5 = "\""
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r2 = r2.toString()
            com.google.android.gms.internal.zzin.zzaJ(r2)
        L_0x0182:
            boolean r2 = r11.exists()
            if (r2 == 0) goto L_0x01a8
            boolean r2 = r11.delete()
            if (r2 != 0) goto L_0x01a8
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r5 = "Could not delete partial cache file at "
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r5 = r11.getAbsolutePath()
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r2 = r2.toString()
            com.google.android.gms.internal.zzin.zzaK(r2)
        L_0x01a8:
            java.lang.String r2 = r11.getAbsolutePath()
            r0 = r26
            r1 = r27
            r0.zza(r1, r2, r4, r3)
            java.util.Set<java.lang.String> r2 = com.google.android.gms.internal.zzdu.zzzX
            r2.remove(r13)
            r2 = 0
            goto L_0x0012
        L_0x01bb:
            int r6 = r3.getContentLength()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            if (r6 >= 0) goto L_0x01ef
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r2.<init>()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.String r3 = "Stream cache aborted, missing content-length header at "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r0 = r27
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            com.google.android.gms.internal.zzin.zzaK(r2)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.String r2 = r11.getAbsolutePath()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.String r3 = "contentLengthMissing"
            r4 = 0
            r0 = r26
            r1 = r27
            r0.zza(r1, r2, r3, r4)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.util.Set<java.lang.String> r2 = com.google.android.gms.internal.zzdu.zzzX     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r2.remove(r13)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r2 = 0
            goto L_0x0012
        L_0x01ef:
            java.text.DecimalFormat r2 = com.google.android.gms.internal.zzdu.zzzY     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            long r14 = (long) r6     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.String r4 = r2.format(r14)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            com.google.android.gms.internal.zzbp<java.lang.Integer> r2 = com.google.android.gms.internal.zzbt.zzvP     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.Object r2 = r2.get()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            int r14 = r2.intValue()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            if (r6 <= r14) goto L_0x024e
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r2.<init>()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.String r3 = "Content length "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.String r3 = " exceeds limit at "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r0 = r27
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            com.google.android.gms.internal.zzin.zzaK(r2)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r2.<init>()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.String r3 = "File too big for full file cache. Size: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.String r3 = r11.getAbsolutePath()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.String r4 = "sizeExceeded"
            r0 = r26
            r1 = r27
            r0.zza(r1, r3, r4, r2)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.util.Set<java.lang.String> r2 = com.google.android.gms.internal.zzdu.zzzX     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r2.remove(r13)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r2 = 0
            goto L_0x0012
        L_0x024e:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r2.<init>()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.String r7 = "Caching "
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.String r4 = " bytes from "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r0 = r27
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            com.google.android.gms.internal.zzin.zzaI(r2)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.io.InputStream r2 = r3.getInputStream()     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.nio.channels.ReadableByteChannel r15 = java.nio.channels.Channels.newChannel(r2)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.io.FileOutputStream r10 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            r10.<init>(r11)     // Catch:{ IOException -> 0x03de, RuntimeException -> 0x03c7 }
            java.nio.channels.FileChannel r16 = r10.getChannel()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            r2 = 1048576(0x100000, float:1.469368E-39)
            java.nio.ByteBuffer r17 = java.nio.ByteBuffer.allocate(r2)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            com.google.android.gms.internal.zzmq r18 = com.google.android.gms.ads.internal.zzr.zzbG()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            r5 = 0
            long r20 = r18.currentTimeMillis()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            com.google.android.gms.internal.zzbp<java.lang.Long> r2 = com.google.android.gms.internal.zzbt.zzvS     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            java.lang.Object r2 = r2.get()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            long r2 = r2.longValue()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            com.google.android.gms.internal.zziz r19 = new com.google.android.gms.internal.zziz     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            r0 = r19
            r0.<init>(r2)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            com.google.android.gms.internal.zzbp<java.lang.Long> r2 = com.google.android.gms.internal.zzbt.zzvR     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            java.lang.Object r2 = r2.get()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            long r22 = r2.longValue()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
        L_0x02af:
            r0 = r17
            int r2 = r15.read(r0)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            if (r2 < 0) goto L_0x0353
            int r5 = r5 + r2
            if (r5 <= r14) goto L_0x02df
            java.lang.String r4 = "sizeExceeded"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
            r2.<init>()     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
            java.lang.String r3 = "File too big for full file cache. Size: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
            java.lang.String r3 = java.lang.Integer.toString(r5)     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
            java.lang.String r3 = r2.toString()     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
            java.io.IOException r2 = new java.io.IOException     // Catch:{ IOException -> 0x02db, RuntimeException -> 0x0320 }
            java.lang.String r5 = "stream cache file size limit exceeded"
            r2.<init>(r5)     // Catch:{ IOException -> 0x02db, RuntimeException -> 0x0320 }
            throw r2     // Catch:{ IOException -> 0x02db, RuntimeException -> 0x0320 }
        L_0x02db:
            r2 = move-exception
            r5 = r10
            goto L_0x014f
        L_0x02df:
            r17.flip()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
        L_0x02e2:
            int r2 = r16.write(r17)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            if (r2 > 0) goto L_0x02e2
            r17.clear()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            long r2 = r18.currentTimeMillis()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            long r2 = r2 - r20
            r24 = 1000(0x3e8, double:4.94E-321)
            long r24 = r24 * r22
            int r2 = (r2 > r24 ? 1 : (r2 == r24 ? 0 : -1))
            if (r2 <= 0) goto L_0x0324
            java.lang.String r4 = "downloadTimeout"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
            r2.<init>()     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
            java.lang.String r3 = "Timeout exceeded. Limit: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
            java.lang.String r3 = java.lang.Long.toString(r22)     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
            java.lang.String r3 = " sec"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
            java.lang.String r3 = r2.toString()     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
            java.io.IOException r2 = new java.io.IOException     // Catch:{ IOException -> 0x02db, RuntimeException -> 0x0320 }
            java.lang.String r5 = "stream cache time limit exceeded"
            r2.<init>(r5)     // Catch:{ IOException -> 0x02db, RuntimeException -> 0x0320 }
            throw r2     // Catch:{ IOException -> 0x02db, RuntimeException -> 0x0320 }
        L_0x0320:
            r2 = move-exception
            r5 = r10
            goto L_0x014f
        L_0x0324:
            r0 = r26
            boolean r2 = r0.zzAa     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            if (r2 == 0) goto L_0x0339
            java.lang.String r4 = "externalAbort"
            java.io.IOException r2 = new java.io.IOException     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
            java.lang.String r3 = "abort requested"
            r2.<init>(r3)     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
            throw r2     // Catch:{ IOException -> 0x0334, RuntimeException -> 0x03d9 }
        L_0x0334:
            r2 = move-exception
            r3 = r8
            r5 = r10
            goto L_0x014f
        L_0x0339:
            boolean r2 = r19.tryAcquire()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            if (r2 == 0) goto L_0x02af
            java.lang.String r4 = r11.getAbsolutePath()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            r7 = 0
            r2 = r26
            r3 = r27
            r2.zza(r3, r4, r5, r6, r7)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            goto L_0x02af
        L_0x034d:
            r2 = move-exception
            r3 = r8
            r4 = r9
            r5 = r10
            goto L_0x014f
        L_0x0353:
            r10.close()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            r2 = 3
            boolean r2 = com.google.android.gms.internal.zzin.zzQ(r2)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            if (r2 == 0) goto L_0x0386
            java.text.DecimalFormat r2 = com.google.android.gms.internal.zzdu.zzzY     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            long r6 = (long) r5     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            java.lang.String r2 = r2.format(r6)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            r3.<init>()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            java.lang.String r4 = "Preloaded "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            java.lang.String r3 = " bytes from "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            r0 = r27
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            com.google.android.gms.internal.zzin.zzaI(r2)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
        L_0x0386:
            r2 = 1
            r3 = 0
            r11.setReadable(r2, r3)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            zzb(r12)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            java.lang.String r2 = r11.getAbsolutePath()     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            r0 = r26
            r1 = r27
            r0.zza(r1, r2, r5)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            java.util.Set<java.lang.String> r2 = com.google.android.gms.internal.zzdu.zzzX     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            r2.remove(r13)     // Catch:{ IOException -> 0x034d, RuntimeException -> 0x03d3 }
            r2 = 1
            goto L_0x0012
        L_0x03a1:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Preload failed for URL \""
            java.lang.StringBuilder r5 = r5.append(r6)
            r0 = r27
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r6 = "\""
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            com.google.android.gms.internal.zzin.zzd(r5, r2)
            goto L_0x0182
        L_0x03c1:
            r5 = move-exception
            goto L_0x015e
        L_0x03c4:
            r5 = move-exception
            goto L_0x015e
        L_0x03c7:
            r2 = move-exception
            r3 = r8
            r4 = r9
            goto L_0x014f
        L_0x03cc:
            r2 = move-exception
            r3 = r8
            goto L_0x014f
        L_0x03d0:
            r2 = move-exception
            goto L_0x014f
        L_0x03d3:
            r2 = move-exception
            r3 = r8
            r4 = r9
            r5 = r10
            goto L_0x014f
        L_0x03d9:
            r2 = move-exception
            r3 = r8
            r5 = r10
            goto L_0x014f
        L_0x03de:
            r2 = move-exception
            r3 = r8
            r4 = r9
            goto L_0x014f
        L_0x03e3:
            r2 = move-exception
            r3 = r8
            goto L_0x014f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzdu.zzU(java.lang.String):boolean");
    }

    public int zzea() {
        int i = 0;
        if (this.zzzZ != null) {
            for (File name : this.zzzZ.listFiles()) {
                if (!name.getName().endsWith(".done")) {
                    i++;
                }
            }
        }
        return i;
    }

    public boolean zzeb() {
        boolean z;
        long j;
        File file;
        if (this.zzzZ == null) {
            return false;
        }
        File file2 = null;
        long j2 = Long.MAX_VALUE;
        File[] listFiles = this.zzzZ.listFiles();
        int length = listFiles.length;
        int i = 0;
        while (i < length) {
            File file3 = listFiles[i];
            if (!file3.getName().endsWith(".done")) {
                j = file3.lastModified();
                if (j < j2) {
                    file = file3;
                    i++;
                    file2 = file;
                    j2 = j;
                }
            }
            j = j2;
            file = file2;
            i++;
            file2 = file;
            j2 = j;
        }
        if (file2 != null) {
            z = file2.delete();
            File zza = zza(file2);
            if (zza.isFile()) {
                z &= zza.delete();
            }
        } else {
            z = false;
        }
        return z;
    }
}
