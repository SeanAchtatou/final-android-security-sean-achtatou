package com.google.android.gms.internal;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.ads.mediation.MediationAdapter;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;
import com.google.android.gms.ads.internal.client.AdRequestParcel;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.formats.NativeAdOptionsParcel;
import com.google.android.gms.ads.internal.reward.mediation.client.zza;
import com.google.android.gms.ads.internal.util.client.zzb;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzey;
import java.util.List;

@zzhb
public final class zzfj<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> extends zzey.zza {
    private final MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> zzCO;
    private final NETWORK_EXTRAS zzCP;

    public zzfj(MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> mediationAdapter, NETWORK_EXTRAS network_extras) {
        this.zzCO = mediationAdapter;
        this.zzCP = network_extras;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: SERVER_PARAMETERS
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private SERVER_PARAMETERS zzb(java.lang.String r6, int r7, java.lang.String r8) throws android.os.RemoteException {
        /*
            r5 = this;
            if (r6 == 0) goto L_0x0034
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0028 }
            r2.<init>(r6)     // Catch:{ Throwable -> 0x0028 }
            java.util.HashMap r1 = new java.util.HashMap     // Catch:{ Throwable -> 0x0028 }
            int r0 = r2.length()     // Catch:{ Throwable -> 0x0028 }
            r1.<init>(r0)     // Catch:{ Throwable -> 0x0028 }
            java.util.Iterator r3 = r2.keys()     // Catch:{ Throwable -> 0x0028 }
        L_0x0014:
            boolean r0 = r3.hasNext()     // Catch:{ Throwable -> 0x0028 }
            if (r0 == 0) goto L_0x003b
            java.lang.Object r0 = r3.next()     // Catch:{ Throwable -> 0x0028 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Throwable -> 0x0028 }
            java.lang.String r4 = r2.getString(r0)     // Catch:{ Throwable -> 0x0028 }
            r1.put(r0, r4)     // Catch:{ Throwable -> 0x0028 }
            goto L_0x0014
        L_0x0028:
            r0 = move-exception
            java.lang.String r1 = "Could not get MediationServerParameters."
            com.google.android.gms.ads.internal.util.client.zzb.zzd(r1, r0)
            android.os.RemoteException r0 = new android.os.RemoteException
            r0.<init>()
            throw r0
        L_0x0034:
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ Throwable -> 0x0028 }
            r1 = 0
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0028 }
            r1 = r0
        L_0x003b:
            com.google.ads.mediation.MediationAdapter<NETWORK_EXTRAS, SERVER_PARAMETERS> r0 = r5.zzCO     // Catch:{ Throwable -> 0x0028 }
            java.lang.Class r2 = r0.getServerParametersType()     // Catch:{ Throwable -> 0x0028 }
            r0 = 0
            if (r2 == 0) goto L_0x004d
            java.lang.Object r0 = r2.newInstance()     // Catch:{ Throwable -> 0x0028 }
            com.google.ads.mediation.MediationServerParameters r0 = (com.google.ads.mediation.MediationServerParameters) r0     // Catch:{ Throwable -> 0x0028 }
            r0.load(r1)     // Catch:{ Throwable -> 0x0028 }
        L_0x004d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzfj.zzb(java.lang.String, int, java.lang.String):com.google.ads.mediation.MediationServerParameters");
    }

    public void destroy() throws RemoteException {
        try {
            this.zzCO.destroy();
        } catch (Throwable th) {
            zzb.zzd("Could not destroy adapter.", th);
            throw new RemoteException();
        }
    }

    public Bundle getInterstitialAdapterInfo() {
        return new Bundle();
    }

    public zzd getView() throws RemoteException {
        if (!(this.zzCO instanceof MediationBannerAdapter)) {
            zzb.zzaK("MediationAdapter is not a MediationBannerAdapter: " + this.zzCO.getClass().getCanonicalName());
            throw new RemoteException();
        }
        try {
            return zze.zzC(((MediationBannerAdapter) this.zzCO).getBannerView());
        } catch (Throwable th) {
            zzb.zzd("Could not get banner view from adapter.", th);
            throw new RemoteException();
        }
    }

    public boolean isInitialized() {
        return true;
    }

    public void pause() throws RemoteException {
        throw new RemoteException();
    }

    public void resume() throws RemoteException {
        throw new RemoteException();
    }

    public void showInterstitial() throws RemoteException {
        if (!(this.zzCO instanceof MediationInterstitialAdapter)) {
            zzb.zzaK("MediationAdapter is not a MediationInterstitialAdapter: " + this.zzCO.getClass().getCanonicalName());
            throw new RemoteException();
        }
        zzb.zzaI("Showing interstitial from adapter.");
        try {
            ((MediationInterstitialAdapter) this.zzCO).showInterstitial();
        } catch (Throwable th) {
            zzb.zzd("Could not show interstitial from adapter.", th);
            throw new RemoteException();
        }
    }

    public void showVideo() {
    }

    public void zza(AdRequestParcel adRequestParcel, String str, String str2) {
    }

    public void zza(zzd zzd, AdRequestParcel adRequestParcel, String str, zza zza, String str2) throws RemoteException {
    }

    public void zza(zzd zzd, AdRequestParcel adRequestParcel, String str, zzez zzez) throws RemoteException {
        zza(zzd, adRequestParcel, str, (String) null, zzez);
    }

    public void zza(zzd zzd, AdRequestParcel adRequestParcel, String str, String str2, zzez zzez) throws RemoteException {
        if (!(this.zzCO instanceof MediationInterstitialAdapter)) {
            zzb.zzaK("MediationAdapter is not a MediationInterstitialAdapter: " + this.zzCO.getClass().getCanonicalName());
            throw new RemoteException();
        }
        zzb.zzaI("Requesting interstitial ad from adapter.");
        try {
            ((MediationInterstitialAdapter) this.zzCO).requestInterstitialAd(new zzfk(zzez), (Activity) zze.zzp(zzd), zzb(str, adRequestParcel.zztG, str2), zzfl.zzj(adRequestParcel), this.zzCP);
        } catch (Throwable th) {
            zzb.zzd("Could not request interstitial ad from adapter.", th);
            throw new RemoteException();
        }
    }

    public void zza(zzd zzd, AdRequestParcel adRequestParcel, String str, String str2, zzez zzez, NativeAdOptionsParcel nativeAdOptionsParcel, List<String> list) {
    }

    public void zza(zzd zzd, AdSizeParcel adSizeParcel, AdRequestParcel adRequestParcel, String str, zzez zzez) throws RemoteException {
        zza(zzd, adSizeParcel, adRequestParcel, str, null, zzez);
    }

    public void zza(zzd zzd, AdSizeParcel adSizeParcel, AdRequestParcel adRequestParcel, String str, String str2, zzez zzez) throws RemoteException {
        if (!(this.zzCO instanceof MediationBannerAdapter)) {
            zzb.zzaK("MediationAdapter is not a MediationBannerAdapter: " + this.zzCO.getClass().getCanonicalName());
            throw new RemoteException();
        }
        zzb.zzaI("Requesting banner ad from adapter.");
        try {
            ((MediationBannerAdapter) this.zzCO).requestBannerAd(new zzfk(zzez), (Activity) zze.zzp(zzd), zzb(str, adRequestParcel.zztG, str2), zzfl.zzb(adSizeParcel), zzfl.zzj(adRequestParcel), this.zzCP);
        } catch (Throwable th) {
            zzb.zzd("Could not request banner ad from adapter.", th);
            throw new RemoteException();
        }
    }

    public void zzb(AdRequestParcel adRequestParcel, String str) {
    }

    public zzfb zzeF() {
        return null;
    }

    public zzfc zzeG() {
        return null;
    }

    public Bundle zzeH() {
        return new Bundle();
    }

    public Bundle zzeI() {
        return new Bundle();
    }
}
