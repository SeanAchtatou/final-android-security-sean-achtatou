package com.google.android.gms.internal;

import android.os.StrictMode;
import com.google.android.gms.ads.internal.zzr;
import java.util.concurrent.Callable;

@zzhb
public class zzjb {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzih.zzb(java.lang.Throwable, boolean):void
     arg types: [java.lang.Throwable, int]
     candidates:
      com.google.android.gms.internal.zzih.zzb(android.content.Context, com.google.android.gms.ads.internal.util.client.VersionInfoParcel):void
      com.google.android.gms.internal.zzih.zzb(java.lang.Throwable, boolean):void */
    public static <T> T zzb(Callable<T> callable) {
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        try {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitDiskReads().permitDiskWrites().build());
            return callable.call();
        } catch (Throwable th) {
            zzin.zzb("Unexpected exception.", th);
            zzr.zzbF().zzb(th, true);
            return null;
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }
}
