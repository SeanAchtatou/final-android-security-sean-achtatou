package com.google.android.gms.ads;

public final class R {

    public static final class attr {
        public static final int adSize = 2130772001;
        public static final int adSizes = 2130772002;
        public static final int adUnitId = 2130772003;
        public static final int circleCrop = 2130772256;
        public static final int imageAspectRatio = 2130772255;
        public static final int imageAspectRatioAdjust = 2130772254;
    }

    public static final class color {
    }

    public static final class dimen {
    }

    public static final class drawable {
    }

    public static final class id {
        public static final int adjust_height = 2131755075;
        public static final int adjust_width = 2131755076;
        public static final int auto = 2131755054;
        public static final int none = 2131755037;
        public static final int normal = 2131755033;
        public static final int wrap_content = 2131755053;
    }

    public static final class integer {
        public static final int google_play_services_version = 2131558406;
    }

    public static final class layout {
    }

    public static final class raw {
    }

    public static final class string {
        public static final int accept = 2131231635;
        public static final int auth_google_play_services_client_facebook_display_name = 2131231650;
        public static final int auth_google_play_services_client_google_display_name = 2131231651;
        public static final int common_google_play_services_unknown_issue = 2131230739;
        public static final int create_calendar_message = 2131231656;
        public static final int create_calendar_title = 2131231657;
        public static final int decline = 2131231680;
        public static final int store_picture_message = 2131231729;
        public static final int store_picture_title = 2131231730;
    }

    public static final class style {
        public static final int Theme_IAPTheme = 2131427644;
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {yahoo.mail.app.R.attr.adSize, yahoo.mail.app.R.attr.adSizes, yahoo.mail.app.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] LoadingImageView = {yahoo.mail.app.R.attr.imageAspectRatioAdjust, yahoo.mail.app.R.attr.imageAspectRatio, yahoo.mail.app.R.attr.circleCrop};
        public static final int LoadingImageView_circleCrop = 2;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 0;
    }
}
