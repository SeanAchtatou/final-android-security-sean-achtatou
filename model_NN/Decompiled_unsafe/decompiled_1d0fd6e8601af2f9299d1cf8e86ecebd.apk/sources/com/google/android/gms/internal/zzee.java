package com.google.android.gms.internal;

import android.content.Context;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import java.util.concurrent.Future;

@zzhb
public class zzee {

    private static class zza<JavascriptEngine> extends zzjd<JavascriptEngine> {
        JavascriptEngine zzAR;

        private zza() {
        }
    }

    /* access modifiers changed from: private */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: JavascriptEngine
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public com.google.android.gms.internal.zzed zza(android.content.Context r3, com.google.android.gms.ads.internal.util.client.VersionInfoParcel r4, final com.google.android.gms.internal.zzee.zza<com.google.android.gms.internal.zzed> r5, com.google.android.gms.internal.zzan r6) {
        /*
            r2 = this;
            com.google.android.gms.internal.zzef r0 = new com.google.android.gms.internal.zzef
            r0.<init>(r3, r4, r6)
            r5.zzAR = r0
            com.google.android.gms.internal.zzee$2 r1 = new com.google.android.gms.internal.zzee$2
            r1.<init>(r5)
            r0.zza(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzee.zza(android.content.Context, com.google.android.gms.ads.internal.util.client.VersionInfoParcel, com.google.android.gms.internal.zzee$zza, com.google.android.gms.internal.zzan):com.google.android.gms.internal.zzed");
    }

    public Future<zzed> zza(Context context, VersionInfoParcel versionInfoParcel, String str, zzan zzan) {
        final zza zza2 = new zza();
        final Context context2 = context;
        final VersionInfoParcel versionInfoParcel2 = versionInfoParcel;
        final zzan zzan2 = zzan;
        final String str2 = str;
        zzir.zzMc.post(new Runnable() {
            public void run() {
                zzee.this.zza(context2, versionInfoParcel2, zza2, zzan2).zzaa(str2);
            }
        });
        return zza2;
    }
}
