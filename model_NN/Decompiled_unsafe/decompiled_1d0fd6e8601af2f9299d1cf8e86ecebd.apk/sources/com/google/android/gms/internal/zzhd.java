package com.google.android.gms.internal;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.internal.client.AdSizeParcel;
import com.google.android.gms.ads.internal.request.AdRequestInfoParcel;
import com.google.android.gms.ads.internal.request.AdResponseParcel;
import com.google.android.gms.ads.internal.request.zzj;
import com.google.android.gms.ads.internal.request.zzk;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.internal.zzeg;
import com.google.android.gms.internal.zzhn;
import com.google.android.gms.internal.zzji;
import com.google.android.gms.internal.zzjq;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

@zzhb
public final class zzhd extends zzj.zza {
    private static zzhd zzIQ;
    private static final Object zzqy = new Object();
    private final Context mContext;
    private final zzhc zzIR;
    private final zzbm zzIS;
    private final zzeg zzIT;

    zzhd(Context context, zzbm zzbm, zzhc zzhc) {
        this.mContext = context;
        this.zzIR = zzhc;
        this.zzIS = zzbm;
        this.zzIT = new zzeg(context.getApplicationContext() != null ? context.getApplicationContext() : context, new VersionInfoParcel(8487000, 8487000, true), zzbm.zzdp(), new zzeg.zzb<zzed>() {
            /* renamed from: zza */
            public void zze(zzed zzed) {
                zzed.zza("/log", zzde.zzzf);
            }
        }, new zzeg.zzc());
    }

    private static AdResponseParcel zza(Context context, zzeg zzeg, zzbm zzbm, zzhc zzhc, AdRequestInfoParcel adRequestInfoParcel) {
        Bundle bundle;
        zzjg zzjg;
        String string;
        zzin.zzaI("Starting ad request from service.");
        zzbt.initialize(context);
        final zzcb zzcb = new zzcb(zzbt.zzwg.get().booleanValue(), "load_ad", adRequestInfoParcel.zzrp.zzuh);
        if (adRequestInfoParcel.versionCode > 10 && adRequestInfoParcel.zzHL != -1) {
            zzcb.zza(zzcb.zzb(adRequestInfoParcel.zzHL), "cts");
        }
        zzbz zzdB = zzcb.zzdB();
        final Bundle bundle2 = (adRequestInfoParcel.versionCode < 4 || adRequestInfoParcel.zzHA == null) ? null : adRequestInfoParcel.zzHA;
        if (!zzbt.zzwp.get().booleanValue() || zzhc.zzIP == null) {
            bundle = bundle2;
            zzjg = null;
        } else {
            if (bundle2 == null && zzbt.zzwq.get().booleanValue()) {
                zzin.v("contentInfo is not present, but we'll still launch the app index task");
                bundle2 = new Bundle();
            }
            if (bundle2 != null) {
                final zzhc zzhc2 = zzhc;
                final Context context2 = context;
                final AdRequestInfoParcel adRequestInfoParcel2 = adRequestInfoParcel;
                bundle = bundle2;
                zzjg = zziq.zza(new Callable<Void>() {
                    /* renamed from: zzdt */
                    public Void call() throws Exception {
                        zzhc2.zzIP.zza(context2, adRequestInfoParcel2.zzHu.packageName, bundle2);
                        return null;
                    }
                });
            } else {
                bundle = bundle2;
                zzjg = null;
            }
        }
        zzhc.zzIK.zzex();
        zzhj zzE = zzr.zzbI().zzE(context);
        if (zzE.zzKc == -1) {
            zzin.zzaI("Device is offline.");
            return new AdResponseParcel(2);
        }
        String uuid = adRequestInfoParcel.versionCode >= 7 ? adRequestInfoParcel.zzHI : UUID.randomUUID().toString();
        final zzhf zzhf = new zzhf(uuid, adRequestInfoParcel.applicationInfo.packageName);
        if (adRequestInfoParcel.zzHt.extras != null && (string = adRequestInfoParcel.zzHt.extras.getString("_ad")) != null) {
            return zzhe.zza(context, adRequestInfoParcel, string);
        }
        Location zzd = zzhc.zzIK.zzd(250);
        String token = zzhc.zzIL.getToken(context, adRequestInfoParcel.zzrj, adRequestInfoParcel.zzHu.packageName);
        List<String> zza = zzhc.zzII.zza(adRequestInfoParcel);
        String zzf = zzhc.zzIM.zzf(adRequestInfoParcel);
        zzhn.zza zzF = zzhc.zzIN.zzF(context);
        if (zzjg != null) {
            try {
                zzin.v("Waiting for app index fetching task.");
                zzjg.get(zzbt.zzwr.get().longValue(), TimeUnit.MILLISECONDS);
                zzin.v("App index fetching task completed.");
            } catch (InterruptedException | ExecutionException e) {
                zzin.zzd("Failed to fetch app index signal", e);
            } catch (TimeoutException e2) {
                zzin.zzaI("Timed out waiting for app index fetching task");
            }
        }
        JSONObject zza2 = zzhe.zza(context, adRequestInfoParcel, zzE, zzF, zzd, zzbm, token, zzf, zza, bundle);
        if (adRequestInfoParcel.versionCode < 7) {
            try {
                zza2.put("request_id", uuid);
            } catch (JSONException e3) {
            }
        }
        if (zza2 == null) {
            return new AdResponseParcel(0);
        }
        final String jSONObject = zza2.toString();
        zzcb.zza(zzdB, "arc");
        final zzbz zzdB2 = zzcb.zzdB();
        if (zzbt.zzvC.get().booleanValue()) {
            final zzeg zzeg2 = zzeg;
            final zzhf zzhf2 = zzhf;
            final zzcb zzcb2 = zzcb;
            zzir.zzMc.post(new Runnable() {
                public void run() {
                    zzeg.zzd zzer = zzeg2.zzer();
                    zzhf2.zzb(zzer);
                    zzcb2.zza(zzdB2, "rwc");
                    final zzbz zzdB = zzcb2.zzdB();
                    zzer.zza(new zzji.zzc<zzeh>() {
                        /* renamed from: zzd */
                        public void zze(zzeh zzeh) {
                            zzcb2.zza(zzdB, "jsf");
                            zzcb2.zzdC();
                            zzeh.zza("/invalidRequest", zzhf2.zzJk);
                            zzeh.zza("/loadAdURL", zzhf2.zzJl);
                            try {
                                zzeh.zze("AFMA_buildAdURL", jSONObject);
                            } catch (Exception e) {
                                zzin.zzb("Error requesting an ad url", e);
                            }
                        }
                    }, new zzji.zza() {
                        public void run() {
                        }
                    });
                }
            });
        } else {
            final Context context3 = context;
            final AdRequestInfoParcel adRequestInfoParcel3 = adRequestInfoParcel;
            final zzbz zzbz = zzdB2;
            final String str = jSONObject;
            final zzbm zzbm2 = zzbm;
            zzir.zzMc.post(new Runnable() {
                public void run() {
                    zzjp zza = zzr.zzbD().zza(context3, new AdSizeParcel(), false, false, null, adRequestInfoParcel3.zzrl);
                    if (zzr.zzbF().zzhi()) {
                        zza.clearCache(true);
                    }
                    zza.getWebView().setWillNotDraw(true);
                    zzhf.zzh(zza);
                    zzcb.zza(zzbz, "rwc");
                    zzjq.zza zzb = zzhd.zza(str, zzcb, zzcb.zzdB());
                    zzjq zzhU = zza.zzhU();
                    zzhU.zza("/invalidRequest", zzhf.zzJk);
                    zzhU.zza("/loadAdURL", zzhf.zzJl);
                    zzhU.zza("/log", zzde.zzzf);
                    zzhU.zza(zzb);
                    zzin.zzaI("Loading the JS library.");
                    zza.loadUrl(zzbm2.zzdp());
                }
            });
        }
        try {
            zzhi zzhi = zzhf.zzgC().get(10, TimeUnit.SECONDS);
            if (zzhi == null) {
                return new AdResponseParcel(0);
            }
            if (zzhi.getErrorCode() != -2) {
                AdResponseParcel adResponseParcel = new AdResponseParcel(zzhi.getErrorCode());
                final zzhc zzhc3 = zzhc;
                final Context context4 = context;
                final AdRequestInfoParcel adRequestInfoParcel4 = adRequestInfoParcel;
                zzir.zzMc.post(new Runnable() {
                    public void run() {
                        r0.zzIJ.zza(r1, zzhf, r2.zzrl);
                    }
                });
                return adResponseParcel;
            }
            if (zzcb.zzdE() != null) {
                zzcb.zza(zzcb.zzdE(), "rur");
            }
            String str2 = null;
            if (zzhi.zzgG()) {
                str2 = zzhc.zzIH.zzaz(adRequestInfoParcel.zzHu.packageName);
            }
            AdResponseParcel zza3 = zza(adRequestInfoParcel, context, adRequestInfoParcel.zzrl.afmaVersion, zzhi.getUrl(), str2, zzhi.zzgH() ? token : null, zzhi, zzcb, zzhc);
            if (zza3.zzIf == 1) {
                zzhc.zzIL.clearToken(context, adRequestInfoParcel.zzHu.packageName);
            }
            zzcb.zza(zzdB, "tts");
            zza3.zzIh = zzcb.zzdD();
            final zzhc zzhc4 = zzhc;
            final Context context5 = context;
            final AdRequestInfoParcel adRequestInfoParcel5 = adRequestInfoParcel;
            zzir.zzMc.post(new Runnable() {
                public void run() {
                    r0.zzIJ.zza(r1, zzhf, r2.zzrl);
                }
            });
            return zza3;
        } catch (Exception e4) {
            return new AdResponseParcel(0);
        } finally {
            final zzhc zzhc5 = zzhc;
            final Context context6 = context;
            final AdRequestInfoParcel adRequestInfoParcel6 = adRequestInfoParcel;
            zzir.zzMc.post(new Runnable() {
                public void run() {
                    zzhc5.zzIJ.zza(context6, zzhf, adRequestInfoParcel6.zzrl);
                }
            });
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzir.zza(android.content.Context, java.lang.String, boolean, java.net.HttpURLConnection):void
     arg types: [android.content.Context, java.lang.String, int, java.net.HttpURLConnection]
     candidates:
      com.google.android.gms.internal.zzir.zza(android.view.View, int, int, boolean):android.widget.PopupWindow
      com.google.android.gms.internal.zzir.zza(android.content.Context, java.lang.String, java.util.List<java.lang.String>, java.lang.String):void
      com.google.android.gms.internal.zzir.zza(android.content.Context, java.lang.String, boolean, java.net.HttpURLConnection):void */
    /* JADX WARNING: Code restructure failed: missing block: B:106:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:?, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00b6, code lost:
        r6 = r7.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r4 = new java.io.InputStreamReader(r2.getInputStream());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        r5 = com.google.android.gms.ads.internal.zzr.zzbC().zza(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        com.google.android.gms.internal.zzna.zzb(r4);
        zza(r6, r12, r5, r9);
        r8.zzb(r6, r12, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00d5, code lost:
        if (r20 == null) goto L_0x00e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00d7, code lost:
        r20.zza(r3, "ufe");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00e4, code lost:
        r3 = r8.zzj(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00eb, code lost:
        if (r21 == null) goto L_0x00f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00ed, code lost:
        r21.zzIO.zzgK();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0130, code lost:
        r3 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0131, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        com.google.android.gms.internal.zzna.zzb(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0135, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0152, code lost:
        com.google.android.gms.internal.zzin.zzaK("No location header to follow redirect.");
        r3 = new com.google.android.gms.ads.internal.request.AdResponseParcel(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:?, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0160, code lost:
        if (r21 == null) goto L_0x0169;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x0162, code lost:
        r21.zzIO.zzgK();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0175, code lost:
        com.google.android.gms.internal.zzin.zzaK("Too many redirects.");
        r3 = new com.google.android.gms.ads.internal.request.AdResponseParcel(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0183, code lost:
        if (r21 == null) goto L_0x018c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0185, code lost:
        r21.zzIO.zzgK();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:?, code lost:
        com.google.android.gms.internal.zzin.zzaK("Received error HTTP response code: " + r9);
        r3 = new com.google.android.gms.ads.internal.request.AdResponseParcel(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01ae, code lost:
        if (r21 == null) goto L_0x01b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01b0, code lost:
        r21.zzIO.zzgK();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01cd, code lost:
        r3 = th;
     */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:61:0x0132=Splitter:B:61:0x0132, B:48:0x00fc=Splitter:B:48:0x00fc, B:85:0x018f=Splitter:B:85:0x018f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.ads.internal.request.AdResponseParcel zza(com.google.android.gms.ads.internal.request.AdRequestInfoParcel r13, android.content.Context r14, java.lang.String r15, java.lang.String r16, java.lang.String r17, java.lang.String r18, com.google.android.gms.internal.zzhi r19, com.google.android.gms.internal.zzcb r20, com.google.android.gms.internal.zzhc r21) {
        /*
            if (r20 == 0) goto L_0x00f6
            com.google.android.gms.internal.zzbz r2 = r20.zzdB()
            r3 = r2
        L_0x0007:
            com.google.android.gms.internal.zzhg r8 = new com.google.android.gms.internal.zzhg     // Catch:{ IOException -> 0x010e }
            r8.<init>(r13)     // Catch:{ IOException -> 0x010e }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x010e }
            r2.<init>()     // Catch:{ IOException -> 0x010e }
            java.lang.String r4 = "AdRequestServiceImpl: Sending request: "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IOException -> 0x010e }
            r0 = r16
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ IOException -> 0x010e }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x010e }
            com.google.android.gms.internal.zzin.zzaI(r2)     // Catch:{ IOException -> 0x010e }
            java.net.URL r4 = new java.net.URL     // Catch:{ IOException -> 0x010e }
            r0 = r16
            r4.<init>(r0)     // Catch:{ IOException -> 0x010e }
            r2 = 0
            com.google.android.gms.internal.zzmq r5 = com.google.android.gms.ads.internal.zzr.zzbG()     // Catch:{ IOException -> 0x010e }
            long r10 = r5.elapsedRealtime()     // Catch:{ IOException -> 0x010e }
            r6 = r2
            r7 = r4
        L_0x0036:
            if (r21 == 0) goto L_0x003f
            r0 = r21
            com.google.android.gms.internal.zzhm r2 = r0.zzIO     // Catch:{ IOException -> 0x010e }
            r2.zzgJ()     // Catch:{ IOException -> 0x010e }
        L_0x003f:
            java.net.URLConnection r2 = r7.openConnection()     // Catch:{ IOException -> 0x010e }
            java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ IOException -> 0x010e }
            com.google.android.gms.internal.zzir r4 = com.google.android.gms.ads.internal.zzr.zzbC()     // Catch:{ all -> 0x0100 }
            r5 = 0
            r4.zza(r14, r15, r5, r2)     // Catch:{ all -> 0x0100 }
            boolean r4 = android.text.TextUtils.isEmpty(r17)     // Catch:{ all -> 0x0100 }
            if (r4 != 0) goto L_0x005a
            java.lang.String r4 = "x-afma-drt-cookie"
            r0 = r17
            r2.addRequestProperty(r4, r0)     // Catch:{ all -> 0x0100 }
        L_0x005a:
            boolean r4 = android.text.TextUtils.isEmpty(r18)     // Catch:{ all -> 0x0100 }
            if (r4 != 0) goto L_0x007a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0100 }
            r4.<init>()     // Catch:{ all -> 0x0100 }
            java.lang.String r5 = "Bearer "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0100 }
            r0 = r18
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ all -> 0x0100 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0100 }
            java.lang.String r5 = "Authorization"
            r2.addRequestProperty(r5, r4)     // Catch:{ all -> 0x0100 }
        L_0x007a:
            if (r19 == 0) goto L_0x00a6
            java.lang.String r4 = r19.zzgF()     // Catch:{ all -> 0x0100 }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x0100 }
            if (r4 != 0) goto L_0x00a6
            r4 = 1
            r2.setDoOutput(r4)     // Catch:{ all -> 0x0100 }
            java.lang.String r4 = r19.zzgF()     // Catch:{ all -> 0x0100 }
            byte[] r9 = r4.getBytes()     // Catch:{ all -> 0x0100 }
            int r4 = r9.length     // Catch:{ all -> 0x0100 }
            r2.setFixedLengthStreamingMode(r4)     // Catch:{ all -> 0x0100 }
            r5 = 0
            java.io.BufferedOutputStream r4 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x00fa }
            java.io.OutputStream r12 = r2.getOutputStream()     // Catch:{ all -> 0x00fa }
            r4.<init>(r12)     // Catch:{ all -> 0x00fa }
            r4.write(r9)     // Catch:{ all -> 0x01d0 }
            com.google.android.gms.internal.zzna.zzb(r4)     // Catch:{ all -> 0x0100 }
        L_0x00a6:
            int r9 = r2.getResponseCode()     // Catch:{ all -> 0x0100 }
            java.util.Map r12 = r2.getHeaderFields()     // Catch:{ all -> 0x0100 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r9 < r4) goto L_0x0136
            r4 = 300(0x12c, float:4.2E-43)
            if (r9 >= r4) goto L_0x0136
            java.lang.String r6 = r7.toString()     // Catch:{ all -> 0x0100 }
            r5 = 0
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ all -> 0x0130 }
            java.io.InputStream r7 = r2.getInputStream()     // Catch:{ all -> 0x0130 }
            r4.<init>(r7)     // Catch:{ all -> 0x0130 }
            com.google.android.gms.internal.zzir r5 = com.google.android.gms.ads.internal.zzr.zzbC()     // Catch:{ all -> 0x01cd }
            java.lang.String r5 = r5.zza(r4)     // Catch:{ all -> 0x01cd }
            com.google.android.gms.internal.zzna.zzb(r4)     // Catch:{ all -> 0x0100 }
            zza(r6, r12, r5, r9)     // Catch:{ all -> 0x0100 }
            r8.zzb(r6, r12, r5)     // Catch:{ all -> 0x0100 }
            if (r20 == 0) goto L_0x00e4
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ all -> 0x0100 }
            r5 = 0
            java.lang.String r6 = "ufe"
            r4[r5] = r6     // Catch:{ all -> 0x0100 }
            r0 = r20
            r0.zza(r3, r4)     // Catch:{ all -> 0x0100 }
        L_0x00e4:
            com.google.android.gms.ads.internal.request.AdResponseParcel r3 = r8.zzj(r10)     // Catch:{ all -> 0x0100 }
            r2.disconnect()     // Catch:{ IOException -> 0x010e }
            if (r21 == 0) goto L_0x00f4
            r0 = r21
            com.google.android.gms.internal.zzhm r2 = r0.zzIO     // Catch:{ IOException -> 0x010e }
            r2.zzgK()     // Catch:{ IOException -> 0x010e }
        L_0x00f4:
            r2 = r3
        L_0x00f5:
            return r2
        L_0x00f6:
            r2 = 0
            r3 = r2
            goto L_0x0007
        L_0x00fa:
            r3 = move-exception
            r4 = r5
        L_0x00fc:
            com.google.android.gms.internal.zzna.zzb(r4)     // Catch:{ all -> 0x0100 }
            throw r3     // Catch:{ all -> 0x0100 }
        L_0x0100:
            r3 = move-exception
            r2.disconnect()     // Catch:{ IOException -> 0x010e }
            if (r21 == 0) goto L_0x010d
            r0 = r21
            com.google.android.gms.internal.zzhm r2 = r0.zzIO     // Catch:{ IOException -> 0x010e }
            r2.zzgK()     // Catch:{ IOException -> 0x010e }
        L_0x010d:
            throw r3     // Catch:{ IOException -> 0x010e }
        L_0x010e:
            r2 = move-exception
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Error while connecting to ad server: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r2 = r2.getMessage()
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r2 = r2.toString()
            com.google.android.gms.internal.zzin.zzaK(r2)
            com.google.android.gms.ads.internal.request.AdResponseParcel r2 = new com.google.android.gms.ads.internal.request.AdResponseParcel
            r3 = 2
            r2.<init>(r3)
            goto L_0x00f5
        L_0x0130:
            r3 = move-exception
            r4 = r5
        L_0x0132:
            com.google.android.gms.internal.zzna.zzb(r4)     // Catch:{ all -> 0x0100 }
            throw r3     // Catch:{ all -> 0x0100 }
        L_0x0136:
            java.lang.String r4 = r7.toString()     // Catch:{ all -> 0x0100 }
            r5 = 0
            zza(r4, r12, r5, r9)     // Catch:{ all -> 0x0100 }
            r4 = 300(0x12c, float:4.2E-43)
            if (r9 < r4) goto L_0x018f
            r4 = 400(0x190, float:5.6E-43)
            if (r9 >= r4) goto L_0x018f
            java.lang.String r4 = "Location"
            java.lang.String r4 = r2.getHeaderField(r4)     // Catch:{ all -> 0x0100 }
            boolean r5 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x0100 }
            if (r5 == 0) goto L_0x016b
            java.lang.String r3 = "No location header to follow redirect."
            com.google.android.gms.internal.zzin.zzaK(r3)     // Catch:{ all -> 0x0100 }
            com.google.android.gms.ads.internal.request.AdResponseParcel r3 = new com.google.android.gms.ads.internal.request.AdResponseParcel     // Catch:{ all -> 0x0100 }
            r4 = 0
            r3.<init>(r4)     // Catch:{ all -> 0x0100 }
            r2.disconnect()     // Catch:{ IOException -> 0x010e }
            if (r21 == 0) goto L_0x0169
            r0 = r21
            com.google.android.gms.internal.zzhm r2 = r0.zzIO     // Catch:{ IOException -> 0x010e }
            r2.zzgK()     // Catch:{ IOException -> 0x010e }
        L_0x0169:
            r2 = r3
            goto L_0x00f5
        L_0x016b:
            java.net.URL r5 = new java.net.URL     // Catch:{ all -> 0x0100 }
            r5.<init>(r4)     // Catch:{ all -> 0x0100 }
            int r4 = r6 + 1
            r6 = 5
            if (r4 <= r6) goto L_0x01ba
            java.lang.String r3 = "Too many redirects."
            com.google.android.gms.internal.zzin.zzaK(r3)     // Catch:{ all -> 0x0100 }
            com.google.android.gms.ads.internal.request.AdResponseParcel r3 = new com.google.android.gms.ads.internal.request.AdResponseParcel     // Catch:{ all -> 0x0100 }
            r4 = 0
            r3.<init>(r4)     // Catch:{ all -> 0x0100 }
            r2.disconnect()     // Catch:{ IOException -> 0x010e }
            if (r21 == 0) goto L_0x018c
            r0 = r21
            com.google.android.gms.internal.zzhm r2 = r0.zzIO     // Catch:{ IOException -> 0x010e }
            r2.zzgK()     // Catch:{ IOException -> 0x010e }
        L_0x018c:
            r2 = r3
            goto L_0x00f5
        L_0x018f:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0100 }
            r3.<init>()     // Catch:{ all -> 0x0100 }
            java.lang.String r4 = "Received error HTTP response code: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0100 }
            java.lang.StringBuilder r3 = r3.append(r9)     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0100 }
            com.google.android.gms.internal.zzin.zzaK(r3)     // Catch:{ all -> 0x0100 }
            com.google.android.gms.ads.internal.request.AdResponseParcel r3 = new com.google.android.gms.ads.internal.request.AdResponseParcel     // Catch:{ all -> 0x0100 }
            r4 = 0
            r3.<init>(r4)     // Catch:{ all -> 0x0100 }
            r2.disconnect()     // Catch:{ IOException -> 0x010e }
            if (r21 == 0) goto L_0x01b7
            r0 = r21
            com.google.android.gms.internal.zzhm r2 = r0.zzIO     // Catch:{ IOException -> 0x010e }
            r2.zzgK()     // Catch:{ IOException -> 0x010e }
        L_0x01b7:
            r2 = r3
            goto L_0x00f5
        L_0x01ba:
            r8.zzj(r12)     // Catch:{ all -> 0x0100 }
            r2.disconnect()     // Catch:{ IOException -> 0x010e }
            if (r21 == 0) goto L_0x01c9
            r0 = r21
            com.google.android.gms.internal.zzhm r2 = r0.zzIO     // Catch:{ IOException -> 0x010e }
            r2.zzgK()     // Catch:{ IOException -> 0x010e }
        L_0x01c9:
            r6 = r4
            r7 = r5
            goto L_0x0036
        L_0x01cd:
            r3 = move-exception
            goto L_0x0132
        L_0x01d0:
            r3 = move-exception
            goto L_0x00fc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzhd.zza(com.google.android.gms.ads.internal.request.AdRequestInfoParcel, android.content.Context, java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.google.android.gms.internal.zzhi, com.google.android.gms.internal.zzcb, com.google.android.gms.internal.zzhc):com.google.android.gms.ads.internal.request.AdResponseParcel");
    }

    public static zzhd zza(Context context, zzbm zzbm, zzhc zzhc) {
        zzhd zzhd;
        synchronized (zzqy) {
            if (zzIQ == null) {
                if (context.getApplicationContext() != null) {
                    context = context.getApplicationContext();
                }
                zzIQ = new zzhd(context, zzbm, zzhc);
            }
            zzhd = zzIQ;
        }
        return zzhd;
    }

    /* access modifiers changed from: private */
    public static zzjq.zza zza(final String str, final zzcb zzcb, final zzbz zzbz) {
        return new zzjq.zza() {
            public void zza(zzjp zzjp, boolean z) {
                zzcb.zza(zzbz, "jsf");
                zzcb.zzdC();
                zzjp.zze("AFMA_buildAdURL", str);
            }
        };
    }

    private static void zza(String str, Map<String, List<String>> map, String str2, int i) {
        if (zzin.zzQ(2)) {
            zzin.v("Http Response: {\n  URL:\n    " + str + "\n  Headers:");
            if (map != null) {
                for (String next : map.keySet()) {
                    zzin.v("    " + next + ":");
                    for (String str3 : map.get(next)) {
                        zzin.v("      " + str3);
                    }
                }
            }
            zzin.v("  Body:");
            if (str2 != null) {
                for (int i2 = 0; i2 < Math.min(str2.length(), 100000); i2 += 1000) {
                    zzin.v(str2.substring(i2, Math.min(str2.length(), i2 + 1000)));
                }
            } else {
                zzin.v("    null");
            }
            zzin.v("  Response Code:\n    " + i + "\n}");
        }
    }

    public void zza(final AdRequestInfoParcel adRequestInfoParcel, final zzk zzk) {
        zzr.zzbF().zzb(this.mContext, adRequestInfoParcel.zzrl);
        zziq.zza(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.google.android.gms.internal.zzih.zzb(java.lang.Throwable, boolean):void
             arg types: [java.lang.Exception, int]
             candidates:
              com.google.android.gms.internal.zzih.zzb(android.content.Context, com.google.android.gms.ads.internal.util.client.VersionInfoParcel):void
              com.google.android.gms.internal.zzih.zzb(java.lang.Throwable, boolean):void */
            public void run() {
                AdResponseParcel adResponseParcel;
                try {
                    adResponseParcel = zzhd.this.zzd(adRequestInfoParcel);
                } catch (Exception e) {
                    zzr.zzbF().zzb((Throwable) e, true);
                    zzin.zzd("Could not fetch ad response due to an Exception.", e);
                    adResponseParcel = null;
                }
                if (adResponseParcel == null) {
                    adResponseParcel = new AdResponseParcel(0);
                }
                try {
                    zzk.zzb(adResponseParcel);
                } catch (RemoteException e2) {
                    zzin.zzd("Fail to forward ad response.", e2);
                }
            }
        });
    }

    public AdResponseParcel zzd(AdRequestInfoParcel adRequestInfoParcel) {
        return zza(this.mContext, this.zzIT, this.zzIS, this.zzIR, adRequestInfoParcel);
    }
}
