package com.tjeannin.apprate;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import com.jcraft.jzlib.JZlib;
import java.lang.Thread;

public class AppRate implements DialogInterface.OnClickListener, DialogInterface.OnCancelListener {
    private static final String TAG = "AppRater";
    private DialogInterface.OnClickListener clickListener;
    private AlertDialog.Builder dialogBuilder = null;
    private Activity hostActivity;
    private long minDaysUntilPrompt = 0;
    private long minLaunchesUntilPrompt = 0;
    private SharedPreferences preferences;
    private boolean showIfHasCrashed = true;

    public AppRate(Activity hostActivity2) {
        this.hostActivity = hostActivity2;
        this.preferences = hostActivity2.getSharedPreferences(PrefsContract.SHARED_PREFS_NAME, 0);
    }

    public AppRate setMinLaunchesUntilPrompt(long minLaunchesUntilPrompt2) {
        this.minLaunchesUntilPrompt = minLaunchesUntilPrompt2;
        return this;
    }

    public AppRate setMinDaysUntilPrompt(long minDaysUntilPrompt2) {
        this.minDaysUntilPrompt = minDaysUntilPrompt2;
        return this;
    }

    public AppRate setShowIfAppHasCrashed(boolean showIfCrash) {
        this.showIfHasCrashed = showIfCrash;
        return this;
    }

    public AppRate setCustomDialog(AlertDialog.Builder customBuilder) {
        this.dialogBuilder = customBuilder;
        return this;
    }

    public static void reset(Context context) {
        context.getSharedPreferences(PrefsContract.SHARED_PREFS_NAME, 0).edit().clear().commit();
        Log.d(TAG, "Cleared AppRate shared preferences.");
    }

    public void init() {
        Log.d(TAG, "Init AppRate");
        if (this.preferences.getBoolean(PrefsContract.PREF_DONT_SHOW_AGAIN, false)) {
            return;
        }
        if (!this.preferences.getBoolean(PrefsContract.PREF_APP_HAS_CRASHED, false) || this.showIfHasCrashed) {
            if (!this.showIfHasCrashed) {
                initExceptionHandler();
            }
            SharedPreferences.Editor editor = this.preferences.edit();
            long launch_count = this.preferences.getLong(PrefsContract.PREF_LAUNCH_COUNT, 0) + 1;
            editor.putLong(PrefsContract.PREF_LAUNCH_COUNT, launch_count);
            Long date_firstLaunch = Long.valueOf(this.preferences.getLong(PrefsContract.PREF_DATE_FIRST_LAUNCH, 0));
            if (date_firstLaunch.longValue() == 0) {
                date_firstLaunch = Long.valueOf(System.currentTimeMillis());
                editor.putLong(PrefsContract.PREF_DATE_FIRST_LAUNCH, date_firstLaunch.longValue());
            }
            if (launch_count >= this.minLaunchesUntilPrompt && System.currentTimeMillis() >= date_firstLaunch.longValue() + (this.minDaysUntilPrompt * 86400000)) {
                if (this.dialogBuilder != null) {
                    showDialog(this.dialogBuilder);
                } else {
                    showDefaultDialog();
                }
            }
            editor.commit();
        }
    }

    private void initExceptionHandler() {
        Log.d(TAG, "Init AppRate ExceptionHandler");
        Thread.UncaughtExceptionHandler currentHandler = Thread.getDefaultUncaughtExceptionHandler();
        if (!(currentHandler instanceof ExceptionHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(currentHandler, this.hostActivity));
        }
    }

    private void showDefaultDialog() {
        Log.d(TAG, "Create default dialog.");
        new AlertDialog.Builder(this.hostActivity).setTitle("Rate " + getApplicationName(this.hostActivity.getApplicationContext())).setMessage("If you enjoy using " + getApplicationName(this.hostActivity.getApplicationContext()) + ", please take a moment to rate it. Thanks for your support!").setPositiveButton("Rate it !", this).setNegativeButton("No thanks", this).setNeutralButton("Remind me later", this).setOnCancelListener(this).create().show();
    }

    private void showDialog(AlertDialog.Builder builder) {
        Log.d(TAG, "Create custom dialog.");
        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.setButton(-1, (String) dialog.getButton(-1).getText(), this);
        dialog.setButton(-3, (String) dialog.getButton(-3).getText(), this);
        dialog.setButton(-2, (String) dialog.getButton(-2).getText(), this);
        dialog.setOnCancelListener(this);
    }

    public void onCancel(DialogInterface dialog) {
        SharedPreferences.Editor editor = this.preferences.edit();
        editor.putLong(PrefsContract.PREF_DATE_FIRST_LAUNCH, System.currentTimeMillis());
        editor.putLong(PrefsContract.PREF_LAUNCH_COUNT, 0);
        editor.commit();
    }

    public AppRate setOnClickListener(DialogInterface.OnClickListener onClickListener) {
        this.clickListener = onClickListener;
        return this;
    }

    public void onClick(DialogInterface dialog, int which) {
        SharedPreferences.Editor editor = this.preferences.edit();
        switch (which) {
            case JZlib.Z_DATA_ERROR:
                editor.putLong(PrefsContract.PREF_DATE_FIRST_LAUNCH, System.currentTimeMillis());
                editor.putLong(PrefsContract.PREF_LAUNCH_COUNT, 0);
                break;
            case -2:
                editor.putBoolean(PrefsContract.PREF_DONT_SHOW_AGAIN, true);
                break;
            case -1:
                this.hostActivity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + this.hostActivity.getPackageName())));
                editor.putBoolean(PrefsContract.PREF_DONT_SHOW_AGAIN, true);
                break;
        }
        editor.commit();
        dialog.dismiss();
        if (this.clickListener != null) {
            this.clickListener.onClick(dialog, which);
        }
    }

    private static final String getApplicationName(Context context) {
        ApplicationInfo applicationInfo;
        PackageManager packageManager = context.getPackageManager();
        try {
            applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            applicationInfo = null;
        }
        return (String) (applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo) : "(unknown)");
    }
}
