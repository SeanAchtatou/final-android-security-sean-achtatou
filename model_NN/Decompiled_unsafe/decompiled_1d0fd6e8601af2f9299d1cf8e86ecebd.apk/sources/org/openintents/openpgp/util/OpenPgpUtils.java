package org.openintents.openpgp.util;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.fsck.k9.Account;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OpenPgpUtils {
    public static final int PARSE_RESULT_MESSAGE = 0;
    public static final int PARSE_RESULT_NO_PGP = -1;
    public static final int PARSE_RESULT_SIGNED_MESSAGE = 1;
    public static final String PGP_MARKER_CLEARSIGN_BEGIN_MESSAGE = "-----BEGIN PGP SIGNED MESSAGE-----";
    public static final String PGP_MARKER_CLEARSIGN_BEGIN_SIGNATURE = "-----BEGIN PGP SIGNATURE-----";
    public static final Pattern PGP_MESSAGE = Pattern.compile("(-----BEGIN PGP MESSAGE-----.*?-----END PGP MESSAGE-----).*", 32);
    public static final Pattern PGP_SIGNED_MESSAGE = Pattern.compile("(-----BEGIN PGP SIGNED MESSAGE-----.*?-----BEGIN PGP SIGNATURE-----.*?-----END PGP SIGNATURE-----).*", 32);
    private static final Pattern USER_ID_PATTERN = Pattern.compile("^(.*?)(?: \\((.*)\\))?(?: <(.*)>)?$");

    public static int parseMessage(String message) {
        return parseMessage(message, false);
    }

    public static int parseMessage(String message, boolean anchorToStart) {
        Matcher matcherSigned = PGP_SIGNED_MESSAGE.matcher(message);
        Matcher matcherMessage = PGP_MESSAGE.matcher(message);
        if (!anchorToStart ? matcherMessage.find() : matcherMessage.matches()) {
            return 0;
        }
        if (!anchorToStart ? matcherSigned.find() : matcherSigned.matches()) {
            return 1;
        }
        return -1;
    }

    public static boolean isAvailable(Context context) {
        if (!context.getPackageManager().queryIntentServices(new Intent(OpenPgpApi.SERVICE_INTENT_2), 0).isEmpty()) {
            return true;
        }
        return false;
    }

    public static String convertKeyIdToHex(long keyId) {
        return "0x" + convertKeyIdToHex32bit(keyId >> 32) + convertKeyIdToHex32bit(keyId);
    }

    private static String convertKeyIdToHex32bit(long keyId) {
        String hexString = Long.toHexString(4294967295L & keyId).toLowerCase(Locale.ENGLISH);
        while (hexString.length() < 8) {
            hexString = "0" + hexString;
        }
        return hexString;
    }

    public static String extractClearsignedMessage(String text) {
        int endOfHeader;
        if (!text.startsWith("-----BEGIN PGP SIGNED MESSAGE-----") || (endOfHeader = text.indexOf("\r\n\r\n") + 4) < 0) {
            return null;
        }
        int endOfCleartext = text.indexOf(PGP_MARKER_CLEARSIGN_BEGIN_SIGNATURE);
        if (endOfCleartext < 0) {
            endOfCleartext = text.length();
        }
        return text.substring(endOfHeader, endOfCleartext);
    }

    public static UserId splitUserId(String userId) {
        if (!TextUtils.isEmpty(userId)) {
            Matcher matcher = USER_ID_PATTERN.matcher(userId);
            if (matcher.matches()) {
                return new UserId(matcher.group(1), matcher.group(3), matcher.group(2));
            }
        }
        return new UserId(null, null, null);
    }

    public static String createUserId(UserId userId) {
        String userIdString = userId.name;
        if (userIdString != null && !TextUtils.isEmpty(userId.comment)) {
            userIdString = userIdString + " (" + userId.comment + ")";
        }
        if (userIdString == null || TextUtils.isEmpty(userId.email)) {
            return userIdString;
        }
        return userIdString + " <" + userId.email + Account.DEFAULT_QUOTE_PREFIX;
    }

    public static class UserId {
        public final String comment;
        public final String email;
        public final String name;

        public UserId(String name2, String email2, String comment2) {
            this.name = name2;
            this.email = email2;
            this.comment = comment2;
        }
    }
}
