package org.openintents.openpgp.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import org.openintents.openpgp.IOpenPgpService2;

public class OpenPgpServiceConnection {
    private Context mApplicationContext;
    /* access modifiers changed from: private */
    public OnBound mOnBoundListener;
    private String mProviderPackageName;
    /* access modifiers changed from: private */
    public IOpenPgpService2 mService;
    private ServiceConnection mServiceConnection;

    public interface OnBound {
        void onBound(IOpenPgpService2 iOpenPgpService2);

        void onError(Exception exc);
    }

    public OpenPgpServiceConnection(Context context, String providerPackageName) {
        this.mServiceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder service) {
                IOpenPgpService2 unused = OpenPgpServiceConnection.this.mService = IOpenPgpService2.Stub.asInterface(service);
                if (OpenPgpServiceConnection.this.mOnBoundListener != null) {
                    OpenPgpServiceConnection.this.mOnBoundListener.onBound(OpenPgpServiceConnection.this.mService);
                }
            }

            public void onServiceDisconnected(ComponentName name) {
                IOpenPgpService2 unused = OpenPgpServiceConnection.this.mService = null;
            }
        };
        this.mApplicationContext = context.getApplicationContext();
        this.mProviderPackageName = providerPackageName;
    }

    public OpenPgpServiceConnection(Context context, String providerPackageName, OnBound onBoundListener) {
        this(context, providerPackageName);
        this.mOnBoundListener = onBoundListener;
    }

    public IOpenPgpService2 getService() {
        return this.mService;
    }

    public boolean isBound() {
        return this.mService != null;
    }

    public void bindToService() {
        if (this.mService == null) {
            try {
                Intent serviceIntent = new Intent(OpenPgpApi.SERVICE_INTENT_2);
                serviceIntent.setPackage(this.mProviderPackageName);
                if (!this.mApplicationContext.bindService(serviceIntent, this.mServiceConnection, 1)) {
                    throw new Exception("bindService() returned false!");
                }
            } catch (Exception e) {
                if (this.mOnBoundListener != null) {
                    this.mOnBoundListener.onError(e);
                }
            }
        } else if (this.mOnBoundListener != null) {
            this.mOnBoundListener.onBound(this.mService);
        }
    }

    public void unbindFromService() {
        this.mApplicationContext.unbindService(this.mServiceConnection);
    }
}
