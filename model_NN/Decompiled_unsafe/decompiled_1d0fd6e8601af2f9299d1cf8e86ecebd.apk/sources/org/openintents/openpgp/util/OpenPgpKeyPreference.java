package org.openintents.openpgp.util;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.Preference;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import java.io.InputStream;
import java.io.OutputStream;
import org.openintents.openpgp.IOpenPgpService2;
import org.openintents.openpgp.OpenPgpError;
import org.openintents.openpgp.R;
import org.openintents.openpgp.util.OpenPgpApi;
import org.openintents.openpgp.util.OpenPgpServiceConnection;

public class OpenPgpKeyPreference extends Preference {
    private static final int NO_KEY = 0;
    public static final int REQUEST_CODE_KEY_PREFERENCE = 9999;
    private String mDefaultUserId;
    private long mKeyId;
    private String mOpenPgpProvider;
    private OpenPgpServiceConnection mServiceConnection;

    public OpenPgpKeyPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CharSequence getSummary() {
        if (this.mKeyId == 0) {
            return getContext().getString(R.string.openpgp_no_key_selected);
        }
        return getContext().getString(R.string.openpgp_key_selected);
    }

    private void updateEnabled() {
        if (TextUtils.isEmpty(this.mOpenPgpProvider)) {
            setEnabled(false);
        } else {
            setEnabled(true);
        }
    }

    public void setOpenPgpProvider(String packageName) {
        this.mOpenPgpProvider = packageName;
        updateEnabled();
    }

    public void setDefaultUserId(String userId) {
        this.mDefaultUserId = userId;
    }

    /* access modifiers changed from: protected */
    public void onClick() {
        this.mServiceConnection = new OpenPgpServiceConnection(getContext().getApplicationContext(), this.mOpenPgpProvider, new OpenPgpServiceConnection.OnBound() {
            public void onBound(IOpenPgpService2 service) {
                OpenPgpKeyPreference.this.getSignKeyId(new Intent());
            }

            public void onError(Exception e) {
                Log.e(OpenPgpApi.TAG, "exception on binding!", e);
            }
        });
        this.mServiceConnection.bindToService();
    }

    /* access modifiers changed from: private */
    public void getSignKeyId(Intent data) {
        data.setAction(OpenPgpApi.ACTION_GET_SIGN_KEY_ID);
        data.putExtra(OpenPgpApi.EXTRA_USER_ID, this.mDefaultUserId);
        new OpenPgpApi(getContext(), this.mServiceConnection.getService()).executeApiAsync(data, (InputStream) null, (OutputStream) null, new MyCallback(REQUEST_CODE_KEY_PREFERENCE));
    }

    private class MyCallback implements OpenPgpApi.IOpenPgpCallback {
        int requestCode;

        private MyCallback(int requestCode2) {
            this.requestCode = requestCode2;
        }

        public void onReturn(Intent result) {
            switch (result.getIntExtra(OpenPgpApi.RESULT_CODE, 0)) {
                case 0:
                    Log.e(OpenPgpApi.TAG, "RESULT_CODE_ERROR: " + ((OpenPgpError) result.getParcelableExtra(OpenPgpApi.RESULT_ERROR)).getMessage());
                    return;
                case 1:
                    OpenPgpKeyPreference.this.save(result.getLongExtra(OpenPgpApi.EXTRA_SIGN_KEY_ID, 0));
                    return;
                case 2:
                    PendingIntent pi = (PendingIntent) result.getParcelableExtra(OpenPgpApi.RESULT_INTENT);
                    try {
                        Activity act = (Activity) OpenPgpKeyPreference.this.getContext();
                        act.startIntentSenderFromChild(act, pi.getIntentSender(), this.requestCode, null, 0, 0, 0);
                        return;
                    } catch (IntentSender.SendIntentException e) {
                        Log.e(OpenPgpApi.TAG, "SendIntentException", e);
                        return;
                    }
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void save(long newValue) {
        if (callChangeListener(Long.valueOf(newValue))) {
            setAndPersist(newValue);
        }
    }

    public void setValue(long keyId) {
        setAndPersist(keyId);
    }

    public long getValue() {
        return this.mKeyId;
    }

    private void setAndPersist(long newValue) {
        this.mKeyId = newValue;
        persistLong(this.mKeyId);
        notifyChanged();
        setSummary(getSummary());
    }

    /* access modifiers changed from: protected */
    public Object onGetDefaultValue(TypedArray a, int index) {
        return Long.valueOf((long) a.getInteger(index, 0));
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        if (restoreValue) {
            this.mKeyId = getPersistedLong(this.mKeyId);
        } else {
            setAndPersist(((Long) defaultValue).longValue());
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        if (isPersistent()) {
            return superState;
        }
        SavedState myState = new SavedState(superState);
        myState.keyId = this.mKeyId;
        myState.openPgpProvider = this.mOpenPgpProvider;
        myState.defaultUserId = this.mDefaultUserId;
        return myState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable state) {
        if (!state.getClass().equals(SavedState.class)) {
            super.onRestoreInstanceState(state);
            return;
        }
        SavedState myState = (SavedState) state;
        super.onRestoreInstanceState(myState.getSuperState());
        this.mKeyId = myState.keyId;
        this.mOpenPgpProvider = myState.openPgpProvider;
        this.mDefaultUserId = myState.defaultUserId;
        notifyChanged();
    }

    private static class SavedState extends Preference.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
        String defaultUserId;
        long keyId;
        String openPgpProvider;

        public SavedState(Parcel source) {
            super(source);
            this.keyId = (long) source.readInt();
            this.openPgpProvider = source.readString();
            this.defaultUserId = source.readString();
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeLong(this.keyId);
            dest.writeString(this.openPgpProvider);
            dest.writeString(this.defaultUserId);
        }

        public SavedState(Parcelable superState) {
            super(superState);
        }
    }

    public boolean handleOnActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != 9999 || resultCode != -1) {
            return false;
        }
        getSignKeyId(data);
        return true;
    }
}
