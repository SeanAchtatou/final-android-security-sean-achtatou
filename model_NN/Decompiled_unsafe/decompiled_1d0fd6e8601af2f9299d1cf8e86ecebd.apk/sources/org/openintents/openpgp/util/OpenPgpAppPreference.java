package org.openintents.openpgp.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.openintents.openpgp.R;

public class OpenPgpAppPreference extends DialogPreference {
    private static final Intent MARKET_INTENT = new Intent("android.intent.action.VIEW", Uri.parse(String.format(MARKET_INTENT_URI_BASE, OPENKEYCHAIN_PACKAGE)));
    private static final String MARKET_INTENT_URI_BASE = "market://details?id=%s";
    private static final String OPENKEYCHAIN_PACKAGE = "org.sufficientlysecure.keychain";
    private static final ArrayList<String> PROVIDER_BLACKLIST = new ArrayList<>();
    private ArrayList<OpenPgpProviderEntry> mLegacyList;
    /* access modifiers changed from: private */
    public ArrayList<OpenPgpProviderEntry> mList;
    /* access modifiers changed from: private */
    public String mSelectedPackage;

    static {
        PROVIDER_BLACKLIST.add("org.thialfihar.android.apg");
    }

    public OpenPgpAppPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mLegacyList = new ArrayList<>();
        this.mList = new ArrayList<>();
        populateAppList();
    }

    public OpenPgpAppPreference(Context context) {
        this(context, null);
    }

    public void addLegacyProvider(int position, String packageName, String simpleName, Drawable icon) {
        this.mLegacyList.add(position, new OpenPgpProviderEntry(packageName, simpleName, icon));
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        populateAppList();
        builder.setSingleChoiceItems(new ArrayAdapter<OpenPgpProviderEntry>(getContext(), 17367058, 16908308, this.mList) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                TextView tv = (TextView) v.findViewById(16908308);
                tv.setCompoundDrawablesWithIntrinsicBounds(((OpenPgpProviderEntry) OpenPgpAppPreference.this.mList.get(position)).icon, (Drawable) null, (Drawable) null, (Drawable) null);
                tv.setCompoundDrawablePadding((int) ((10.0f * getContext().getResources().getDisplayMetrics().density) + 0.5f));
                return v;
            }
        }, getIndexOfProviderList(this.mSelectedPackage), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                OpenPgpProviderEntry entry = (OpenPgpProviderEntry) OpenPgpAppPreference.this.mList.get(which);
                if (entry.intent != null) {
                    OpenPgpAppPreference.this.getContext().startActivity(entry.intent);
                    return;
                }
                String unused = OpenPgpAppPreference.this.mSelectedPackage = entry.packageName;
                OpenPgpAppPreference.this.onClick(dialog, -1);
                dialog.dismiss();
            }
        });
        builder.setPositiveButton((CharSequence) null, (DialogInterface.OnClickListener) null);
    }

    /* access modifiers changed from: protected */
    public void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult && this.mSelectedPackage != null) {
            save();
        }
    }

    private void save() {
        if (callChangeListener(this.mSelectedPackage)) {
            setAndPersist(this.mSelectedPackage);
        }
    }

    private void setAndPersist(String packageName) {
        this.mSelectedPackage = packageName;
        persistString(this.mSelectedPackage);
        notifyChanged();
        updateSummary(this.mSelectedPackage);
    }

    private void updateSummary(String packageName) {
        setSummary(getEntryByValue(packageName));
    }

    public CharSequence getSummary() {
        return getEntryByValue(this.mSelectedPackage);
    }

    private int getIndexOfProviderList(String packageName) {
        Iterator<OpenPgpProviderEntry> it = this.mList.iterator();
        while (it.hasNext()) {
            OpenPgpProviderEntry app = it.next();
            if (app.packageName.equals(packageName)) {
                return this.mList.indexOf(app);
            }
        }
        return 0;
    }

    public String getEntry() {
        return getEntryByValue(this.mSelectedPackage);
    }

    public String getValue() {
        return this.mSelectedPackage;
    }

    public void setValue(String packageName) {
        setAndPersist(packageName);
    }

    /* access modifiers changed from: protected */
    public Object onGetDefaultValue(TypedArray a, int index) {
        return a.getString(index);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        if (restoreValue) {
            this.mSelectedPackage = getPersistedString(this.mSelectedPackage);
            updateSummary(this.mSelectedPackage);
            return;
        }
        String value = (String) defaultValue;
        setAndPersist(value);
        updateSummary(value);
    }

    public String getEntryByValue(String packageName) {
        Iterator<OpenPgpProviderEntry> it = this.mList.iterator();
        while (it.hasNext()) {
            OpenPgpProviderEntry app = it.next();
            if (app.packageName.equals(packageName) && app.intent == null) {
                return app.simpleName;
            }
        }
        return getContext().getString(R.string.openpgp_list_preference_none);
    }

    private void populateAppList() {
        this.mList.clear();
        this.mList.add(0, new OpenPgpProviderEntry("", getContext().getString(R.string.openpgp_list_preference_none), getContext().getResources().getDrawable(R.drawable.ic_action_cancel_launchersize)));
        this.mList.addAll(this.mLegacyList);
        ArrayList<OpenPgpProviderEntry> providerList = new ArrayList<>();
        List<ResolveInfo> resInfo = getContext().getPackageManager().queryIntentServices(new Intent(OpenPgpApi.SERVICE_INTENT_2), 0);
        if (!resInfo.isEmpty()) {
            for (ResolveInfo resolveInfo : resInfo) {
                if (resolveInfo.serviceInfo != null) {
                    String packageName = resolveInfo.serviceInfo.packageName;
                    String simpleName = String.valueOf(resolveInfo.serviceInfo.loadLabel(getContext().getPackageManager()));
                    Drawable icon = resolveInfo.serviceInfo.loadIcon(getContext().getPackageManager());
                    if (!PROVIDER_BLACKLIST.contains(packageName)) {
                        providerList.add(new OpenPgpProviderEntry(packageName, simpleName, icon));
                    }
                }
            }
        }
        if (providerList.isEmpty()) {
            for (ResolveInfo resolveInfo2 : getContext().getPackageManager().queryIntentActivities(MARKET_INTENT, 0)) {
                Intent marketIntent = new Intent(MARKET_INTENT);
                marketIntent.setPackage(resolveInfo2.activityInfo.packageName);
                Drawable icon2 = resolveInfo2.activityInfo.loadIcon(getContext().getPackageManager());
                String marketName = String.valueOf(resolveInfo2.activityInfo.applicationInfo.loadLabel(getContext().getPackageManager()));
                this.mList.add(new OpenPgpProviderEntry(OPENKEYCHAIN_PACKAGE, String.format(getContext().getString(R.string.openpgp_install_openkeychain_via), marketName), icon2, marketIntent));
            }
            return;
        }
        this.mList.addAll(providerList);
    }

    private static class OpenPgpProviderEntry {
        /* access modifiers changed from: private */
        public Drawable icon;
        /* access modifiers changed from: private */
        public Intent intent;
        /* access modifiers changed from: private */
        public String packageName;
        /* access modifiers changed from: private */
        public String simpleName;

        public OpenPgpProviderEntry(String packageName2, String simpleName2, Drawable icon2) {
            this.packageName = packageName2;
            this.simpleName = simpleName2;
            this.icon = icon2;
        }

        public OpenPgpProviderEntry(String packageName2, String simpleName2, Drawable icon2, Intent intent2) {
            this(packageName2, simpleName2, icon2);
            this.intent = intent2;
        }

        public String toString() {
            return this.simpleName;
        }
    }
}
