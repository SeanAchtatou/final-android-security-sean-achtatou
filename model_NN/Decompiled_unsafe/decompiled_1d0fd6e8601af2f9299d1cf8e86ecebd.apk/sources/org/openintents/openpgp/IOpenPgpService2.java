package org.openintents.openpgp;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;

public interface IOpenPgpService2 extends IInterface {
    ParcelFileDescriptor createOutputPipe(int i) throws RemoteException;

    Intent execute(Intent intent, ParcelFileDescriptor parcelFileDescriptor, int i) throws RemoteException;

    public static abstract class Stub extends Binder implements IOpenPgpService2 {
        private static final String DESCRIPTOR = "org.openintents.openpgp.IOpenPgpService2";
        static final int TRANSACTION_createOutputPipe = 1;
        static final int TRANSACTION_execute = 2;

        public Stub() {
            attachInterface(this, "org.openintents.openpgp.IOpenPgpService2");
        }

        public static IOpenPgpService2 asInterface(IBinder obj) {
            if (obj == null) {
                return null;
            }
            IInterface iin = obj.queryLocalInterface("org.openintents.openpgp.IOpenPgpService2");
            if (iin == null || !(iin instanceof IOpenPgpService2)) {
                return new Proxy(obj);
            }
            return (IOpenPgpService2) iin;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            Intent _arg0;
            ParcelFileDescriptor _arg1;
            switch (code) {
                case 1:
                    data.enforceInterface("org.openintents.openpgp.IOpenPgpService2");
                    ParcelFileDescriptor _result = createOutputPipe(data.readInt());
                    reply.writeNoException();
                    if (_result != null) {
                        reply.writeInt(1);
                        _result.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 2:
                    data.enforceInterface("org.openintents.openpgp.IOpenPgpService2");
                    if (data.readInt() != 0) {
                        _arg0 = (Intent) Intent.CREATOR.createFromParcel(data);
                    } else {
                        _arg0 = null;
                    }
                    if (data.readInt() != 0) {
                        _arg1 = (ParcelFileDescriptor) ParcelFileDescriptor.CREATOR.createFromParcel(data);
                    } else {
                        _arg1 = null;
                    }
                    Intent _result2 = execute(_arg0, _arg1, data.readInt());
                    reply.writeNoException();
                    if (_result2 != null) {
                        reply.writeInt(1);
                        _result2.writeToParcel(reply, 1);
                        return true;
                    }
                    reply.writeInt(0);
                    return true;
                case 1598968902:
                    reply.writeString("org.openintents.openpgp.IOpenPgpService2");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }

        private static class Proxy implements IOpenPgpService2 {
            private IBinder mRemote;

            Proxy(IBinder remote) {
                this.mRemote = remote;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return "org.openintents.openpgp.IOpenPgpService2";
            }

            public ParcelFileDescriptor createOutputPipe(int pipeId) throws RemoteException {
                ParcelFileDescriptor _result;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("org.openintents.openpgp.IOpenPgpService2");
                    _data.writeInt(pipeId);
                    this.mRemote.transact(1, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = (ParcelFileDescriptor) ParcelFileDescriptor.CREATOR.createFromParcel(_reply);
                    } else {
                        _result = null;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }

            public Intent execute(Intent data, ParcelFileDescriptor input, int pipeId) throws RemoteException {
                Intent _result;
                Parcel _data = Parcel.obtain();
                Parcel _reply = Parcel.obtain();
                try {
                    _data.writeInterfaceToken("org.openintents.openpgp.IOpenPgpService2");
                    if (data != null) {
                        _data.writeInt(1);
                        data.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    if (input != null) {
                        _data.writeInt(1);
                        input.writeToParcel(_data, 0);
                    } else {
                        _data.writeInt(0);
                    }
                    _data.writeInt(pipeId);
                    this.mRemote.transact(2, _data, _reply, 0);
                    _reply.readException();
                    if (_reply.readInt() != 0) {
                        _result = (Intent) Intent.CREATOR.createFromParcel(_reply);
                    } else {
                        _result = null;
                    }
                    return _result;
                } finally {
                    _reply.recycle();
                    _data.recycle();
                }
            }
        }
    }
}
