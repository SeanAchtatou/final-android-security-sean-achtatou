package org.openintents.openpgp;

import android.os.Parcel;
import android.os.Parcelable;

public class OpenPgpMetadata implements Parcelable {
    public static final Parcelable.Creator<OpenPgpMetadata> CREATOR = new Parcelable.Creator<OpenPgpMetadata>() {
        public OpenPgpMetadata createFromParcel(Parcel source) {
            int version = source.readInt();
            int parcelableSize = source.readInt();
            int startPosition = source.dataPosition();
            OpenPgpMetadata vr = new OpenPgpMetadata();
            vr.filename = source.readString();
            vr.mimeType = source.readString();
            vr.modificationTime = source.readLong();
            vr.originalSize = source.readLong();
            if (version >= 2) {
                vr.charset = source.readString();
            }
            source.setDataPosition(startPosition + parcelableSize);
            return vr;
        }

        public OpenPgpMetadata[] newArray(int size) {
            return new OpenPgpMetadata[size];
        }
    };
    public static final int PARCELABLE_VERSION = 2;
    String charset;
    String filename;
    String mimeType;
    long modificationTime;
    long originalSize;

    public String getFilename() {
        return this.filename;
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public long getModificationTime() {
        return this.modificationTime;
    }

    public long getOriginalSize() {
        return this.originalSize;
    }

    public String getCharset() {
        return this.charset;
    }

    public OpenPgpMetadata() {
    }

    public OpenPgpMetadata(String filename2, String mimeType2, long modificationTime2, long originalSize2, String charset2) {
        this.filename = filename2;
        this.mimeType = mimeType2;
        this.modificationTime = modificationTime2;
        this.originalSize = originalSize2;
        this.charset = charset2;
    }

    public OpenPgpMetadata(String filename2, String mimeType2, long modificationTime2, long originalSize2) {
        this.filename = filename2;
        this.mimeType = mimeType2;
        this.modificationTime = modificationTime2;
        this.originalSize = originalSize2;
    }

    public OpenPgpMetadata(OpenPgpMetadata b) {
        this.filename = b.filename;
        this.mimeType = b.mimeType;
        this.modificationTime = b.modificationTime;
        this.originalSize = b.originalSize;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(2);
        int sizePosition = dest.dataPosition();
        dest.writeInt(0);
        int startPosition = dest.dataPosition();
        dest.writeString(this.filename);
        dest.writeString(this.mimeType);
        dest.writeLong(this.modificationTime);
        dest.writeLong(this.originalSize);
        dest.writeString(this.charset);
        int parcelableSize = dest.dataPosition() - startPosition;
        dest.setDataPosition(sizePosition);
        dest.writeInt(parcelableSize);
        dest.setDataPosition(startPosition + parcelableSize);
    }

    public String toString() {
        return (((("\nfilename: " + this.filename) + "\nmimeType: " + this.mimeType) + "\nmodificationTime: " + this.modificationTime) + "\noriginalSize: " + this.originalSize) + "\ncharset: " + this.charset;
    }
}
