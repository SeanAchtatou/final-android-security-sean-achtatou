package org.jdom2;

import java.util.Iterator;
import java.util.List;
import org.htmlcleaner.CData;

public final class Verifier {
    private static final int CHARCNT = 65536;
    private static final byte[] CHARFLAGS = buildBitFlags();
    private static final int[] LENCONST = {9, 2, 2, 1, 18, 1, 1, 2, 9, 2, 1, 10, 1, 2, 1, 1, 2, 26, 4, 1, 1, 26, 3, 1, 56, 1, 8, 23, 1, 31, 1, 58, 2, 11, 2, 8, 1, 53, 1, 68, 9, 36, 3, 2, 4, 30, 56, 89, 18, 7, 14, 2, 46, 70, 26, 2, 36, 1, 1, 3, 1, 1, 1, 20, 1, 44, 1, 7, 3, 1, 1, 1, 1, 1, 1, 1, 1, 18, 13, 12, 1, 66, 1, 12, 1, 36, 1, 4, 9, 53, 2, 2, 2, 2, 3, 28, 2, 8, 2, 2, 55, 38, 2, 1, 7, 38, 10, 17, 1, 23, 1, 3, 1, 1, 1, 2, 1, 1, 11, 27, 5, 3, 46, 26, 5, 1, 10, 8, 13, 10, 6, 1, 71, 2, 5, 1, 15, 1, 4, 1, 1, 15, 2, 2, 1, 4, 2, 10, 519, 3, 1, 53, 2, 1, 1, 16, 3, 4, 3, 10, 2, 2, 10, 17, 3, 1, 8, 2, 2, 2, 22, 1, 7, 1, 1, 3, 4, 2, 1, 1, 7, 2, 2, 2, 3, 9, 1, 4, 2, 1, 3, 2, 2, 10, 2, 16, 1, 2, 6, 4, 2, 2, 22, 1, 7, 1, 2, 1, 2, 1, 2, 2, 1, 1, 5, 4, 2, 2, 3, 11, 4, 1, 1, 7, 10, 2, 3, 12, 3, 1, 7, 1, 1, 1, 3, 1, 22, 1, 7, 1, 2, 1, 5, 2, 1, 1, 8, 1, 3, 1, 3, 18, 1, 5, 10, 17, 3, 1, 8, 2, 2, 2, 22, 1, 7, 1, 2, 2, 4, 2, 1, 1, 6, 3, 2, 2, 3, 8, 2, 4, 2, 1, 3, 4, 10, 18, 2, 1, 6, 3, 3, 1, 4, 3, 2, 1, 1, 1, 2, 3, 2, 3, 3, 3, 8, 1, 3, 4, 5, 3, 3, 1, 4, 9, 1, 15, 9, 17, 3, 1, 8, 1, 3, 1, 23, 1, 10, 1, 5, 4, 7, 1, 3, 1, 4, 7, 2, 9, 2, 4, 10, 18, 2, 1, 8, 1, 3, 1, 23, 1, 10, 1, 5, 4, 7, 1, 3, 1, 4, 7, 2, 7, 1, 1, 2, 4, 10, 18, 2, 1, 8, 1, 3, 1, 23, 1, 16, 4, 6, 2, 3, 1, 4, 9, 1, 8, 2, 4, 10, 145, 46, 1, 1, 1, 2, 7, 5, 6, 1, 8, 1, 10, 39, 2, 1, 1, 2, 2, 1, 1, 2, 1, 6, 4, 1, 7, 1, 3, 1, 1, 1, 1, 2, 2, 1, 2, 1, 1, 1, 2, 6, 1, 2, 1, 2, 5, 1, 1, 1, 6, 2, 10, 62, 2, 6, 10, 11, 1, 1, 1, 1, 1, 4, 2, 8, 1, 33, 7, 20, 1, 6, 4, 6, 1, 1, 1, 21, 3, 7, 1, 1, 230, 38, 10, 39, 9, 1, 1, 2, 1, 3, 1, 1, 1, 2, 1, 5, 41, 1, 1, 1, 1, 1, 11, 1, 1, 1, 1, 1, 3, 2, 3, 1, 5, 3, 1, 1, 1, 1, 1, 1, 1, 1, 3, 2, 3, 2, 1, 1, 40, 1, 9, 1, 2, 1, 2, 2, 7, 2, 1, 1, 1, 7, 40, 1, 4, 1, 8, 1, 3078, 156, 4, 90, 6, 22, 2, 6, 2, 38, 2, 6, 2, 8, 1, 1, 1, 1, 1, 1, 1, 31, 2, 53, 1, 7, 1, 1, 3, 3, 1, 7, 3, 4, 2, 6, 4, 13, 5, 3, 1, 7, 211, 13, 4, 1, 68, 1, 3, 2, 2, 1, 81, 3, 3714, 1, 1, 1, 25, 9, 6, 1, 5, 11, 84, 4, 2, 2, 2, 2, 90, 1, 3, 6, 40, 7379, 20902, 3162, 11172, 92, 2048, 8190, 2};
    private static final byte MASKURICHAR = 64;
    private static final byte MASKXMLCHARACTER = 1;
    private static final byte MASKXMLCOMBINING = 32;
    private static final byte MASKXMLDIGIT = 16;
    private static final byte MASKXMLLETTER = 2;
    private static final byte MASKXMLLETTERORDIGIT = 18;
    private static final byte MASKXMLNAMECHAR = 8;
    private static final byte MASKXMLSTARTCHAR = 4;
    private static final byte[] VALCONST = {0, MASKXMLCHARACTER, 0, MASKXMLCHARACTER, 0, MASKXMLCHARACTER, 65, MASKXMLCHARACTER, 65, 73, 65, 89, 65, MASKXMLCHARACTER, 65, MASKXMLCHARACTER, 65, 79, MASKXMLCHARACTER, 77, MASKXMLCHARACTER, 79, MASKXMLCHARACTER, 65, MASKXMLCHARACTER, 9, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 9, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, 9, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 9, 15, 41, MASKXMLCHARACTER, 25, MASKXMLCHARACTER, 41, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, 41, 15, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 25, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 41, 15, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, 41, MASKXMLCHARACTER, 25, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, 41, MASKXMLCHARACTER, 25, 15, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 25, 41, 15, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 41, 15, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 25, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 41, 15, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 25, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 25, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 25, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 25, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 25, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, 41, 15, 41, MASKXMLCHARACTER, 15, 9, 41, MASKXMLCHARACTER, 25, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, 41, 15, 41, MASKXMLCHARACTER, 41, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 9, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 25, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 25, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 9, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, 41, MASKXMLCHARACTER, 9, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 41, MASKXMLCHARACTER, 9, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 9, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 15, MASKXMLCHARACTER, 0, MASKXMLCHARACTER, 0};

    private static final byte[] buildBitFlags() {
        int index;
        byte[] ret = new byte[65536];
        int index2 = 0;
        int i = 0;
        while (i < VALCONST.length) {
            byte v = VALCONST[i];
            int l = LENCONST[i];
            while (true) {
                index = index2;
                l--;
                if (l < 0) {
                    break;
                }
                index2 = index + 1;
                ret[index] = v;
            }
            i++;
            index2 = index;
        }
        return ret;
    }

    private Verifier() {
    }

    private static final String checkJDOMName(String name) {
        if (name == null) {
            return "XML names cannot be null";
        }
        if (name.length() == 0) {
            return "XML names cannot be empty";
        }
        if ((CHARFLAGS[name.charAt(0)] & MASKXMLSTARTCHAR) == 0) {
            return "XML name '" + name + "' cannot begin with the character \"" + name.charAt(0) + "\"";
        }
        for (int i = name.length() - 1; i >= 1; i--) {
            if (((byte) (CHARFLAGS[name.charAt(i)] & MASKXMLNAMECHAR)) == 0) {
                return "XML name '" + name + "' cannot contain the character \"" + name.charAt(i) + "\"";
            }
        }
        return null;
    }

    public static String checkElementName(String name) {
        return checkJDOMName(name);
    }

    public static String checkAttributeName(String name) {
        if ("xmlns".equals(name)) {
            return "An Attribute name may not be \"xmlns\"; use the Namespace class to manage namespaces";
        }
        return checkJDOMName(name);
    }

    public static String checkCharacterData(String text) {
        if (text == null) {
            return "A null is not a legal XML value";
        }
        int len = text.length();
        int i = 0;
        while (i < len) {
            while (CHARFLAGS[text.charAt(i)] != 0) {
                i++;
                if (i == len) {
                    return null;
                }
            }
            if (isHighSurrogate(text.charAt(i))) {
                int i2 = i + 1;
                if (i2 >= len) {
                    return String.format("Truncated Surrogate Pair 0x%04x????", Integer.valueOf(text.charAt(i2 - 1)));
                } else if (!isLowSurrogate(text.charAt(i2))) {
                    return String.format("Illegal Surrogate Pair 0x%04x%04x", Integer.valueOf(text.charAt(i2 - 1)), Integer.valueOf(text.charAt(i2)));
                } else if (!isXMLCharacter(decodeSurrogatePair(text.charAt(i2 - 1), text.charAt(i2)))) {
                    return String.format("0x%06x is not a legal XML character", Integer.valueOf(decodeSurrogatePair(text.charAt(i2 - 1), text.charAt(i2))));
                } else {
                    i = i2 + 1;
                }
            } else {
                return String.format("0x%04x is not a legal XML character", Integer.valueOf(text.charAt(i)));
            }
        }
        return null;
    }

    public static String checkCDATASection(String data) {
        String reason = checkCharacterData(data);
        if (reason != null) {
            return reason;
        }
        if (data.indexOf(CData.END_CDATA) != -1) {
            return "CDATA cannot internally contain a CDATA ending delimiter (]]>)";
        }
        return null;
    }

    public static String checkNamespacePrefix(String prefix) {
        if (prefix == null || prefix.equals("")) {
            return null;
        }
        if (checkJDOMName(prefix) != null) {
            return checkJDOMName(prefix);
        }
        if (prefix.length() < 3) {
            return null;
        }
        if (prefix.charAt(0) != 'x' && prefix.charAt(0) != 'X') {
            return null;
        }
        if (prefix.charAt(1) != 'm' && prefix.charAt(1) != 'M') {
            return null;
        }
        if (prefix.charAt(2) == 'l' || prefix.charAt(2) == 'L') {
            return "Namespace prefixes cannot begin with \"xml\" in any combination of case";
        }
        return null;
    }

    public static String checkNamespaceURI(String uri) {
        if (uri == null || uri.equals("")) {
            return null;
        }
        char first = uri.charAt(0);
        if (Character.isDigit(first)) {
            return "Namespace URIs cannot begin with a number";
        }
        if (first == '$') {
            return "Namespace URIs cannot begin with a dollar sign ($)";
        }
        if (first == '-') {
            return "Namespace URIs cannot begin with a hyphen (-)";
        }
        if (isXMLWhitespace(first)) {
            return "Namespace URIs cannot begin with white-space";
        }
        return null;
    }

    public static String checkNamespaceCollision(Namespace namespace, Namespace other) {
        String p1 = namespace.getPrefix();
        String u1 = namespace.getURI();
        String p2 = other.getPrefix();
        String u2 = other.getURI();
        if (!p1.equals(p2) || u1.equals(u2)) {
            return null;
        }
        return "The namespace prefix \"" + p1 + "\" collides";
    }

    public static String checkNamespaceCollision(Attribute attribute, Element element) {
        return checkNamespaceCollision(attribute, element, -1);
    }

    public static String checkNamespaceCollision(Attribute attribute, Element element, int ignoreatt) {
        Namespace namespace = attribute.getNamespace();
        if ("".equals(namespace.getPrefix())) {
            return null;
        }
        return checkNamespaceCollision(namespace, element, ignoreatt);
    }

    public static String checkNamespaceCollision(Namespace namespace, Element element) {
        return checkNamespaceCollision(namespace, element, -1);
    }

    public static String checkNamespaceCollision(Namespace namespace, Element element, int ignoreatt) {
        String reason;
        String reason2;
        String reason3 = checkNamespaceCollision(namespace, element.getNamespace());
        if (reason3 != null) {
            return reason3 + " with the element namespace prefix";
        }
        if (element.hasAdditionalNamespaces() && (reason2 = checkNamespaceCollision(namespace, element.getAdditionalNamespaces())) != null) {
            return reason2;
        }
        if (!element.hasAttributes() || (reason = checkNamespaceCollision(namespace, element.getAttributes(), ignoreatt)) == null) {
            return null;
        }
        return reason;
    }

    public static String checkNamespaceCollision(Namespace namespace, Attribute attribute) {
        if (attribute.getNamespace().equals(Namespace.NO_NAMESPACE)) {
            return null;
        }
        String reason = checkNamespaceCollision(namespace, attribute.getNamespace());
        if (reason != null) {
            return reason + " with an attribute namespace prefix on the element";
        }
        return reason;
    }

    public static String checkNamespaceCollision(Namespace namespace, List<?> list) {
        return checkNamespaceCollision(namespace, list, -1);
    }

    public static String checkNamespaceCollision(Namespace namespace, List<?> list, int ignoreatt) {
        if (list == null) {
            return null;
        }
        String reason = null;
        Iterator<?> i = list.iterator();
        int cnt = -1;
        while (reason == null && i.hasNext()) {
            Object obj = i.next();
            cnt++;
            if (obj instanceof Attribute) {
                if (cnt != ignoreatt) {
                    reason = checkNamespaceCollision(namespace, (Attribute) obj);
                }
            } else if (obj instanceof Element) {
                reason = checkNamespaceCollision(namespace, (Element) obj);
            } else if ((obj instanceof Namespace) && (reason = checkNamespaceCollision(namespace, (Namespace) obj)) != null) {
                reason = reason + " with an additional namespace declared by the element";
            }
        }
        return reason;
    }

    public static String checkProcessingInstructionTarget(String target) {
        String reason = checkXMLName(target);
        if (reason != null) {
            return reason;
        }
        if (target.indexOf(":") != -1) {
            return "Processing instruction targets cannot contain colons";
        }
        if (target.equalsIgnoreCase(JDOMConstants.NS_PREFIX_XML)) {
            return "Processing instructions cannot have a target of \"xml\" in any combination of case. (Note that the \"<?xml ... ?>\" declaration at the beginning of a document is not a processing instruction and should not be added as one; it is written automatically during output, e.g. by XMLOutputter.)";
        }
        return null;
    }

    public static String checkProcessingInstructionData(String data) {
        String reason = checkCharacterData(data);
        if (reason != null || data.indexOf("?>") < 0) {
            return reason;
        }
        return "Processing instructions cannot contain the string \"?>\"";
    }

    public static String checkCommentData(String data) {
        String reason = checkCharacterData(data);
        if (reason != null) {
            return reason;
        }
        if (data.indexOf("--") != -1) {
            return "Comments cannot contain double hyphens (--)";
        }
        if (data.endsWith("-")) {
            return "Comment data cannot end with a hyphen.";
        }
        return null;
    }

    public static int decodeSurrogatePair(char high, char low) {
        return 65536 + ((high - 55296) * 1024) + (low - 56320);
    }

    public static boolean isXMLPublicIDCharacter(char c) {
        if (c >= 'a' && c <= 'z') {
            return true;
        }
        if (c >= '?' && c <= 'Z') {
            return true;
        }
        if ((c >= '\'' && c <= ';') || c == ' ' || c == '!' || c == '=' || c == '#' || c == '$' || c == '_' || c == '%' || c == 10 || c == 13 || c == 9) {
            return true;
        }
        return false;
    }

    public static String checkPublicID(String publicID) {
        String reason = null;
        if (publicID == null) {
            return null;
        }
        int i = 0;
        while (true) {
            if (i >= publicID.length()) {
                break;
            }
            char c = publicID.charAt(i);
            if (!isXMLPublicIDCharacter(c)) {
                reason = c + " is not a legal character in public IDs";
                break;
            }
            i++;
        }
        return reason;
    }

    public static String checkSystemLiteral(String systemLiteral) {
        String reason;
        if (systemLiteral == null) {
            return null;
        }
        if (systemLiteral.indexOf(39) == -1 || systemLiteral.indexOf(34) == -1) {
            reason = checkCharacterData(systemLiteral);
        } else {
            reason = "System literals cannot simultaneously contain both single and double quotes.";
        }
        return reason;
    }

    public static String checkXMLName(String name) {
        if (name == null) {
            return "XML names cannot be null";
        }
        int len = name.length();
        if (len == 0) {
            return "XML names cannot be empty";
        }
        if (!isXMLNameStartCharacter(name.charAt(0))) {
            return "XML names cannot begin with the character \"" + name.charAt(0) + "\"";
        }
        for (int i = 1; i < len; i++) {
            if (!isXMLNameCharacter(name.charAt(i))) {
                return "XML names cannot contain the character \"" + name.charAt(i) + "\"";
            }
        }
        return null;
    }

    public static String checkURI(String uri) {
        if (uri == null || uri.equals("")) {
            return null;
        }
        for (int i = 0; i < uri.length(); i++) {
            char test = uri.charAt(i);
            if (!isURICharacter(test)) {
                String msgNumber = "0x" + Integer.toHexString(test);
                if (test <= 9) {
                    msgNumber = "0x0" + Integer.toHexString(test);
                }
                return "URIs cannot contain " + msgNumber;
            }
            if (test == '%') {
                try {
                    char firstDigit = uri.charAt(i + 1);
                    char secondDigit = uri.charAt(i + 2);
                    if (!isHexDigit(firstDigit) || !isHexDigit(secondDigit)) {
                        return "Percent signs in URIs must be followed by exactly two hexadecimal digits.";
                    }
                } catch (StringIndexOutOfBoundsException e) {
                    return "Percent signs in URIs must be followed by exactly two hexadecimal digits.";
                }
            }
        }
        return null;
    }

    public static boolean isHexDigit(char c) {
        if (c >= '0' && c <= '9') {
            return true;
        }
        if (c >= 'A' && c <= 'F') {
            return true;
        }
        if (c < 'a' || c > 'f') {
            return false;
        }
        return true;
    }

    public static boolean isHighSurrogate(char ch) {
        return 54 == (ch >>> 10);
    }

    public static boolean isLowSurrogate(char ch) {
        return 55 == (ch >>> 10);
    }

    public static boolean isURICharacter(char c) {
        return ((byte) (CHARFLAGS[c] & MASKURICHAR)) != 0;
    }

    public static boolean isXMLCharacter(int c) {
        if (c >= 65536) {
            if (c <= 1114111) {
                return true;
            }
            return false;
        } else if (((byte) (CHARFLAGS[c] & MASKXMLCHARACTER)) == 0) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean isXMLNameCharacter(char c) {
        return ((byte) (CHARFLAGS[c] & MASKXMLNAMECHAR)) != 0 || c == ':';
    }

    public static boolean isXMLNameStartCharacter(char c) {
        return ((byte) (CHARFLAGS[c] & MASKXMLSTARTCHAR)) != 0 || c == ':';
    }

    public static boolean isXMLLetterOrDigit(char c) {
        return ((byte) (CHARFLAGS[c] & MASKXMLLETTERORDIGIT)) != 0;
    }

    public static boolean isXMLLetter(char c) {
        return ((byte) (CHARFLAGS[c] & MASKXMLLETTER)) != 0;
    }

    public static boolean isXMLCombiningChar(char c) {
        return ((byte) (CHARFLAGS[c] & MASKXMLCOMBINING)) != 0;
    }

    public static boolean isXMLExtender(char c) {
        if (c < 182) {
            return false;
        }
        if (c == 183) {
            return true;
        }
        if (c == 720) {
            return true;
        }
        if (c == 721) {
            return true;
        }
        if (c == 903) {
            return true;
        }
        if (c == 1600) {
            return true;
        }
        if (c == 3654) {
            return true;
        }
        if (c == 3782) {
            return true;
        }
        if (c == 12293) {
            return true;
        }
        if (c < 12337) {
            return false;
        }
        if (c <= 12341) {
            return true;
        }
        if (c < 12445) {
            return false;
        }
        if (c <= 12446) {
            return true;
        }
        if (c < 12540 || c > 12542) {
            return false;
        }
        return true;
    }

    public static boolean isXMLDigit(char c) {
        return ((byte) (CHARFLAGS[c] & MASKXMLDIGIT)) != 0;
    }

    public static boolean isXMLWhitespace(char c) {
        if (c == ' ' || c == 10 || c == 9 || c == 13) {
            return true;
        }
        return false;
    }

    public static final boolean isAllXMLWhitespace(String value) {
        int i = value.length();
        do {
            i--;
            if (i < 0) {
                return true;
            }
        } while (isXMLWhitespace(value.charAt(i)));
        return false;
    }
}
