package org.jdom2.filter;

import org.jdom2.Content;

final class OrFilter extends AbstractFilter<Content> {
    private static final long serialVersionUID = 200;
    private final Filter<?> left;
    private final Filter<?> right;

    public OrFilter(Filter<?> left2, Filter<?> right2) {
        if (left2 == null || right2 == null) {
            throw new IllegalArgumentException("null filter not allowed");
        }
        this.left = left2;
        this.right = right2;
    }

    public Content filter(Object obj) {
        if (this.left.matches(obj) || this.right.matches(obj)) {
            return (Content) obj;
        }
        return null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof OrFilter) {
            OrFilter filter = (OrFilter) obj;
            if (this.left.equals(filter.left) && this.right.equals(filter.right)) {
                return true;
            }
            if (!this.left.equals(filter.right) || !this.right.equals(filter.left)) {
                return false;
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (this.left.hashCode() ^ -1) ^ this.right.hashCode();
    }

    public String toString() {
        return new StringBuilder(64).append("[OrFilter: ").append(this.left.toString()).append(",\n").append("           ").append(this.right.toString()).append("]").toString();
    }
}
