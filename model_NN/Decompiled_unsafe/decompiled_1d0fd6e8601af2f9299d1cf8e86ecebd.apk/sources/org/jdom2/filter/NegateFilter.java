package org.jdom2.filter;

final class NegateFilter extends AbstractFilter<Object> {
    private static final long serialVersionUID = 200;
    private final Filter<?> filter;

    public NegateFilter(Filter<?> filter2) {
        this.filter = filter2;
    }

    public Object filter(Object content) {
        if (this.filter.matches(content)) {
            return null;
        }
        return content;
    }

    /* access modifiers changed from: package-private */
    public Filter<?> getBaseFilter() {
        return this.filter;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof NegateFilter) {
            return this.filter.equals(((NegateFilter) obj).filter);
        }
        return false;
    }

    public int hashCode() {
        return this.filter.hashCode() ^ -1;
    }

    public String toString() {
        return new StringBuilder(64).append("[NegateFilter: ").append(this.filter.toString()).append("]").toString();
    }
}
