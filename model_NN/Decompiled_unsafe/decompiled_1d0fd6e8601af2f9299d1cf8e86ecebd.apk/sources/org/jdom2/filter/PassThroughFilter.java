package org.jdom2.filter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;

final class PassThroughFilter extends AbstractFilter<Object> {
    private static final long serialVersionUID = 200;

    PassThroughFilter() {
    }

    public Object filter(Object content) {
        return content;
    }

    public List<Object> filter(List<?> content) {
        if (content == null || content.isEmpty()) {
            return Collections.emptyList();
        }
        if (content instanceof RandomAccess) {
            return Collections.unmodifiableList(content);
        }
        ArrayList<Object> ret = new ArrayList<>();
        for (Object add : content) {
            ret.add(add);
        }
        return Collections.unmodifiableList(ret);
    }
}
