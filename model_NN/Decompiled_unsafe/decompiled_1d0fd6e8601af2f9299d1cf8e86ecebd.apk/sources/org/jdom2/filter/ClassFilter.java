package org.jdom2.filter;

final class ClassFilter<T> extends AbstractFilter<T> {
    private static final long serialVersionUID = 200;
    private final Class<? extends T> fclass;

    public ClassFilter(Class<? extends T> tclass) {
        this.fclass = tclass;
    }

    public T filter(Object content) {
        if (this.fclass.isInstance(content)) {
            return this.fclass.cast(content);
        }
        return null;
    }

    public String toString() {
        return "[ClassFilter: Class " + this.fclass.getName() + "]";
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ClassFilter) {
            return this.fclass.equals(((ClassFilter) obj).fclass);
        }
        return false;
    }

    public int hashCode() {
        return this.fclass.hashCode();
    }
}
