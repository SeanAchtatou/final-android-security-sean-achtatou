package org.jdom2.filter;

import org.jdom2.Attribute;
import org.jdom2.Namespace;

public class AttributeFilter extends AbstractFilter<Attribute> {
    private static final long serialVersionUID = 200;
    private final String name;
    private final Namespace namespace;

    public AttributeFilter() {
        this(null, null);
    }

    public AttributeFilter(String name2) {
        this(name2, null);
    }

    public AttributeFilter(Namespace namespace2) {
        this(null, namespace2);
    }

    public AttributeFilter(String name2, Namespace namespace2) {
        this.name = name2;
        this.namespace = namespace2;
    }

    public Attribute filter(Object content) {
        if (!(content instanceof Attribute)) {
            return null;
        }
        Attribute att = (Attribute) content;
        if (this.name == null) {
            if (this.namespace != null && !this.namespace.equals(att.getNamespace())) {
                return null;
            }
            return att;
        } else if (!this.name.equals(att.getName())) {
            return null;
        } else {
            if (this.namespace == null || this.namespace.equals(att.getNamespace())) {
                return att;
            }
            return null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AttributeFilter)) {
            return false;
        }
        AttributeFilter filter = (AttributeFilter) obj;
        if (this.name == null ? filter.name != null : !this.name.equals(filter.name)) {
            return false;
        }
        if (this.namespace != null) {
            if (this.namespace.equals(filter.namespace)) {
                return true;
            }
        } else if (filter.namespace == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (this.name != null) {
            result = this.name.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 29;
        if (this.namespace != null) {
            i = this.namespace.hashCode();
        }
        return i2 + i;
    }
}
