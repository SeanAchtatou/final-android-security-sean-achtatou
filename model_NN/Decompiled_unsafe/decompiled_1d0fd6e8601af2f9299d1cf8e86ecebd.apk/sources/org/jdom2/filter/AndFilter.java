package org.jdom2.filter;

final class AndFilter<T> extends AbstractFilter<T> {
    private static final long serialVersionUID = 200;
    private final Filter<?> base;
    private final Filter<T> refiner;

    public AndFilter(Filter<?> base2, Filter<T> refiner2) {
        if (base2 == null || refiner2 == null) {
            throw new NullPointerException("Cannot have a null base or refiner filter");
        }
        this.base = base2;
        this.refiner = refiner2;
    }

    public T filter(Object content) {
        if (this.base.filter(content) != null) {
            return this.refiner.filter(content);
        }
        return null;
    }

    public int hashCode() {
        return this.base.hashCode() ^ this.refiner.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof AndFilter)) {
            return false;
        }
        AndFilter<?> them = (AndFilter) obj;
        if ((!this.base.equals(them.base) || !this.refiner.equals(them.refiner)) && (!this.refiner.equals(them.base) || !this.base.equals(them.refiner))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return new StringBuilder(64).append("[AndFilter: ").append(this.base.toString()).append(",\n").append("            ").append(this.refiner.toString()).append("]").toString();
    }
}
