package org.jdom2.filter;

import org.jdom2.Element;
import org.jdom2.Namespace;

public class ElementFilter extends AbstractFilter<Element> {
    private static final long serialVersionUID = 200;
    private String name;
    private Namespace namespace;

    public ElementFilter() {
    }

    public ElementFilter(String name2) {
        this.name = name2;
    }

    public ElementFilter(Namespace namespace2) {
        this.namespace = namespace2;
    }

    public ElementFilter(String name2, Namespace namespace2) {
        this.name = name2;
        this.namespace = namespace2;
    }

    public Element filter(Object content) {
        if (!(content instanceof Element)) {
            return null;
        }
        Element el = (Element) content;
        if (this.name == null) {
            if (this.namespace != null && !this.namespace.equals(el.getNamespace())) {
                return null;
            }
            return el;
        } else if (!this.name.equals(el.getName())) {
            return null;
        } else {
            if (this.namespace == null || this.namespace.equals(el.getNamespace())) {
                return el;
            }
            return null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ElementFilter)) {
            return false;
        }
        ElementFilter filter = (ElementFilter) obj;
        if (this.name == null ? filter.name != null : !this.name.equals(filter.name)) {
            return false;
        }
        if (this.namespace != null) {
            if (this.namespace.equals(filter.namespace)) {
                return true;
            }
        } else if (filter.namespace == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (this.name != null) {
            result = this.name.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 29;
        if (this.namespace != null) {
            i = this.namespace.hashCode();
        }
        return i2 + i;
    }

    public String toString() {
        return "[ElementFilter: Name " + (this.name == null ? "*any*" : this.name) + " with Namespace " + this.namespace + "]";
    }
}
