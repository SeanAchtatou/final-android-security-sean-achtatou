package org.jdom2.transform;

import com.fsck.k9.provider.EmailProvider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.transform.sax.SAXResult;
import org.jdom2.Content;
import org.jdom2.DefaultJDOMFactory;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMFactory;
import org.jdom2.input.sax.SAXHandler;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.XMLFilterImpl;

public class JDOMResult extends SAXResult {
    public static final String JDOM_FEATURE = "http://jdom.org/jdom2/transform/JDOMResult/feature";
    private JDOMFactory factory = null;
    private boolean queried = false;
    private Document resultdoc = null;
    private List<Content> resultlist = null;

    public JDOMResult() {
        DocumentBuilder builder = new DocumentBuilder();
        super.setHandler(builder);
        super.setLexicalHandler(builder);
    }

    public void setResult(List<Content> result) {
        this.resultlist = result;
        this.queried = false;
    }

    public List<Content> getResult() {
        List<Content> nodes = Collections.emptyList();
        retrieveResult();
        if (this.resultlist != null) {
            nodes = this.resultlist;
        } else if (this.resultdoc != null && !this.queried) {
            List<Content> content = this.resultdoc.getContent();
            nodes = new ArrayList<>(content.size());
            while (content.size() != 0) {
                nodes.add(content.remove(0));
            }
            this.resultlist = nodes;
            this.resultdoc = null;
        }
        this.queried = true;
        return nodes;
    }

    public void setDocument(Document document) {
        this.resultdoc = document;
        this.resultlist = null;
        this.queried = false;
    }

    public Document getDocument() {
        Document doc = null;
        retrieveResult();
        if (this.resultdoc != null) {
            doc = this.resultdoc;
        } else if (this.resultlist != null && !this.queried) {
            try {
                JDOMFactory f = getFactory();
                if (f == null) {
                    f = new DefaultJDOMFactory();
                }
                doc = f.document(null);
                doc.setContent(this.resultlist);
                this.resultdoc = doc;
                this.resultlist = null;
            } catch (RuntimeException e) {
                return null;
            }
        }
        this.queried = true;
        return doc;
    }

    public void setFactory(JDOMFactory factory2) {
        this.factory = factory2;
    }

    public JDOMFactory getFactory() {
        return this.factory;
    }

    private void retrieveResult() {
        if (this.resultlist == null && this.resultdoc == null) {
            setResult(((DocumentBuilder) getHandler()).getResult());
        }
    }

    public void setHandler(ContentHandler handler) {
    }

    public void setLexicalHandler(LexicalHandler handler) {
    }

    private static class FragmentHandler extends SAXHandler {
        private Element dummyRoot = new Element(EmailProvider.ThreadColumns.ROOT, null, null);

        public FragmentHandler(JDOMFactory factory) {
            super(factory);
            pushElement(this.dummyRoot);
        }

        public List<Content> getResult() {
            try {
                flushCharacters();
            } catch (SAXException e) {
            }
            return getDetachedContent(this.dummyRoot);
        }

        private List<Content> getDetachedContent(Element elt) {
            List<Content> content = elt.getContent();
            List<Content> nodes = new ArrayList<>(content.size());
            while (content.size() != 0) {
                nodes.add(content.remove(0));
            }
            return nodes;
        }
    }

    private class DocumentBuilder extends XMLFilterImpl implements LexicalHandler {
        private FragmentHandler saxHandler = null;
        private boolean startDocumentReceived = false;

        public DocumentBuilder() {
        }

        public List<Content> getResult() {
            if (this.saxHandler == null) {
                return null;
            }
            List<Content> mresult = this.saxHandler.getResult();
            this.saxHandler = null;
            this.startDocumentReceived = false;
            return mresult;
        }

        private void ensureInitialization() throws SAXException {
            if (!this.startDocumentReceived) {
                startDocument();
            }
        }

        public void startDocument() throws SAXException {
            this.startDocumentReceived = true;
            JDOMResult.this.setResult(null);
            this.saxHandler = new FragmentHandler(JDOMResult.this.getFactory());
            super.setContentHandler(this.saxHandler);
            super.startDocument();
        }

        public void startElement(String nsURI, String localName, String qName, Attributes atts) throws SAXException {
            ensureInitialization();
            super.startElement(nsURI, localName, qName, atts);
        }

        public void startPrefixMapping(String prefix, String uri) throws SAXException {
            ensureInitialization();
            super.startPrefixMapping(prefix, uri);
        }

        public void characters(char[] ch, int start, int length) throws SAXException {
            ensureInitialization();
            super.characters(ch, start, length);
        }

        public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
            ensureInitialization();
            super.ignorableWhitespace(ch, start, length);
        }

        public void processingInstruction(String target, String data) throws SAXException {
            ensureInitialization();
            super.processingInstruction(target, data);
        }

        public void skippedEntity(String name) throws SAXException {
            ensureInitialization();
            super.skippedEntity(name);
        }

        public void startDTD(String name, String publicId, String systemId) throws SAXException {
            ensureInitialization();
            this.saxHandler.startDTD(name, publicId, systemId);
        }

        public void endDTD() throws SAXException {
            this.saxHandler.endDTD();
        }

        public void startEntity(String name) throws SAXException {
            ensureInitialization();
            this.saxHandler.startEntity(name);
        }

        public void endEntity(String name) throws SAXException {
            this.saxHandler.endEntity(name);
        }

        public void startCDATA() throws SAXException {
            ensureInitialization();
            this.saxHandler.startCDATA();
        }

        public void endCDATA() throws SAXException {
            this.saxHandler.endCDATA();
        }

        public void comment(char[] ch, int start, int length) throws SAXException {
            ensureInitialization();
            this.saxHandler.comment(ch, start, length);
        }
    }
}
