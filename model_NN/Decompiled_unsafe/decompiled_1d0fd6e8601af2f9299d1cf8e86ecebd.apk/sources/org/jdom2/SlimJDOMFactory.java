package org.jdom2;

import java.util.Map;

public class SlimJDOMFactory extends DefaultJDOMFactory {
    private StringBin cache;
    private final boolean cachetext;

    public SlimJDOMFactory() {
        this(true);
    }

    public SlimJDOMFactory(boolean cachetext2) {
        this.cache = new StringBin();
        this.cachetext = cachetext2;
    }

    public void clearCache() {
        this.cache = new StringBin();
    }

    public Attribute attribute(String name, String value, Namespace namespace) {
        String reuse = this.cache.reuse(name);
        if (this.cachetext) {
            value = this.cache.reuse(value);
        }
        return super.attribute(reuse, value, namespace);
    }

    @Deprecated
    public Attribute attribute(String name, String value, int type, Namespace namespace) {
        String reuse = this.cache.reuse(name);
        if (this.cachetext) {
            value = this.cache.reuse(value);
        }
        return super.attribute(reuse, value, type, namespace);
    }

    public Attribute attribute(String name, String value, AttributeType type, Namespace namespace) {
        String reuse = this.cache.reuse(name);
        if (this.cachetext) {
            value = this.cache.reuse(value);
        }
        return super.attribute(reuse, value, type, namespace);
    }

    public Attribute attribute(String name, String value) {
        String reuse = this.cache.reuse(name);
        if (this.cachetext) {
            value = this.cache.reuse(value);
        }
        return super.attribute(reuse, value);
    }

    @Deprecated
    public Attribute attribute(String name, String value, int type) {
        String reuse = this.cache.reuse(name);
        if (this.cachetext) {
            value = this.cache.reuse(value);
        }
        return super.attribute(reuse, value, type);
    }

    public Attribute attribute(String name, String value, AttributeType type) {
        String reuse = this.cache.reuse(name);
        if (this.cachetext) {
            value = this.cache.reuse(value);
        }
        return super.attribute(reuse, value, type);
    }

    public CDATA cdata(int line, int col, String str) {
        if (this.cachetext) {
            str = this.cache.reuse(str);
        }
        return super.cdata(line, col, str);
    }

    public Text text(int line, int col, String str) {
        if (this.cachetext) {
            str = this.cache.reuse(str);
        }
        return super.text(line, col, str);
    }

    public Comment comment(int line, int col, String text) {
        if (this.cachetext) {
            text = this.cache.reuse(text);
        }
        return super.comment(line, col, text);
    }

    public DocType docType(int line, int col, String elementName, String publicID, String systemID) {
        return super.docType(line, col, this.cache.reuse(elementName), publicID, systemID);
    }

    public DocType docType(int line, int col, String elementName, String systemID) {
        return super.docType(line, col, this.cache.reuse(elementName), systemID);
    }

    public DocType docType(int line, int col, String elementName) {
        return super.docType(line, col, this.cache.reuse(elementName));
    }

    public Element element(int line, int col, String name, Namespace namespace) {
        return super.element(line, col, this.cache.reuse(name), namespace);
    }

    public Element element(int line, int col, String name) {
        return super.element(line, col, this.cache.reuse(name));
    }

    public Element element(int line, int col, String name, String uri) {
        return super.element(line, col, this.cache.reuse(name), uri);
    }

    public Element element(int line, int col, String name, String prefix, String uri) {
        return super.element(line, col, this.cache.reuse(name), prefix, uri);
    }

    public ProcessingInstruction processingInstruction(int line, int col, String target, Map<String, String> data) {
        return super.processingInstruction(line, col, this.cache.reuse(target), data);
    }

    public ProcessingInstruction processingInstruction(int line, int col, String target, String data) {
        return super.processingInstruction(line, col, this.cache.reuse(target), data);
    }

    public ProcessingInstruction processingInstruction(int line, int col, String target) {
        return super.processingInstruction(line, col, this.cache.reuse(target));
    }

    public EntityRef entityRef(int line, int col, String name) {
        return super.entityRef(line, col, this.cache.reuse(name));
    }

    public EntityRef entityRef(int line, int col, String name, String publicID, String systemID) {
        return super.entityRef(line, col, this.cache.reuse(name), publicID, systemID);
    }

    public EntityRef entityRef(int line, int col, String name, String systemID) {
        return super.entityRef(line, col, this.cache.reuse(name), systemID);
    }
}
