package org.jdom2.xpath.jaxen;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.jaxen.BaseXPath;
import org.jaxen.JaxenException;
import org.jaxen.NamespaceContext;
import org.jaxen.UnresolvableException;
import org.jaxen.VariableContext;
import org.jaxen.XPath;
import org.jdom2.Namespace;
import org.jdom2.filter.Filter;
import org.jdom2.xpath.util.AbstractXPathCompiled;

class JaxenCompiled<T> extends AbstractXPathCompiled<T> implements NamespaceContext, VariableContext {
    private final JDOM2Navigator navigator = new JDOM2Navigator();
    private final XPath xPath;

    private static final Object unWrapNS(Object o) {
        if (o instanceof NamespaceContainer) {
            return ((NamespaceContainer) o).getNamespace();
        }
        return o;
    }

    private static final List<Object> unWrap(List<?> results) {
        ArrayList<Object> ret = new ArrayList<>(results.size());
        for (Object unWrapNS : results) {
            ret.add(unWrapNS(unWrapNS));
        }
        return ret;
    }

    public JaxenCompiled(String expression, Filter<T> filter, Map<String, Object> variables, Namespace[] namespaces) {
        super(expression, filter, variables, namespaces);
        try {
            this.xPath = new BaseXPath(expression, this.navigator);
            this.xPath.setNamespaceContext(this);
            this.xPath.setVariableContext(this);
        } catch (JaxenException e) {
            throw new IllegalArgumentException("Unable to compile '" + expression + "'. See Cause.", e);
        }
    }

    public String translateNamespacePrefixToUri(String prefix) {
        return getNamespace(prefix).getURI();
    }

    public Object getVariableValue(String namespaceURI, String prefix, String localName) throws UnresolvableException {
        if (namespaceURI == null) {
            namespaceURI = "";
        }
        if (prefix == null) {
            prefix = "";
        }
        try {
            if ("".equals(namespaceURI)) {
                namespaceURI = getNamespace(prefix).getURI();
            }
            return getVariable(localName, Namespace.getNamespace(namespaceURI));
        } catch (IllegalArgumentException e) {
            throw new UnresolvableException("Unable to resolve variable " + localName + " in namespace '" + namespaceURI + "' to a vaulue.");
        }
    }

    /* access modifiers changed from: protected */
    public List<?> evaluateRawAll(Object context) {
        try {
            return unWrap(this.xPath.selectNodes(context));
        } catch (JaxenException e) {
            throw new IllegalStateException("Unable to evaluate expression. See cause", e);
        }
    }

    /* access modifiers changed from: protected */
    public Object evaluateRawFirst(Object context) {
        try {
            return unWrapNS(this.xPath.selectSingleNode(context));
        } catch (JaxenException e) {
            throw new IllegalStateException("Unable to evaluate expression. See cause", e);
        }
    }
}
