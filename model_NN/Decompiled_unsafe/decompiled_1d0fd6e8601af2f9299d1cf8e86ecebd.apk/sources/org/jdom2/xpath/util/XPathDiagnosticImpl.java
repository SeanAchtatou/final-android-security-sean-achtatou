package org.jdom2.xpath.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.jdom2.filter.Filter;
import org.jdom2.xpath.XPathDiagnostic;
import org.jdom2.xpath.XPathExpression;

public class XPathDiagnosticImpl<T> implements XPathDiagnostic<T> {
    private final Object dcontext;
    private final List<Object> dfiltered;
    private final boolean dfirstonly;
    private final List<Object> draw;
    private final List<T> dresult;
    private final XPathExpression<T> dxpath;

    public XPathDiagnosticImpl(Object dcontext2, XPathExpression<T> dxpath2, List<?> inraw, boolean dfirstonly2) {
        int sz = inraw.size();
        List<Object> raw = new ArrayList<>(sz);
        List<Object> filtered = new ArrayList<>(sz);
        List<T> result = new ArrayList<>(sz);
        Filter<T> filter = dxpath2.getFilter();
        for (Object o : inraw) {
            raw.add(o);
            T t = filter.filter(o);
            if (t == null) {
                filtered.add(o);
            } else {
                result.add(t);
            }
        }
        this.dcontext = dcontext2;
        this.dxpath = dxpath2;
        this.dfirstonly = dfirstonly2;
        this.dfiltered = Collections.unmodifiableList(filtered);
        this.draw = Collections.unmodifiableList(raw);
        this.dresult = Collections.unmodifiableList(result);
    }

    public Object getContext() {
        return this.dcontext;
    }

    public XPathExpression<T> getXPathExpression() {
        return this.dxpath;
    }

    public List<T> getResult() {
        return this.dresult;
    }

    public List<Object> getFilteredResults() {
        return this.dfiltered;
    }

    public List<Object> getRawResults() {
        return this.draw;
    }

    public boolean isFirstOnly() {
        return this.dfirstonly;
    }

    public String toString() {
        Object[] objArr = new Object[6];
        objArr[0] = this.dxpath.getExpression();
        objArr[1] = this.dfirstonly ? "first" : "all";
        objArr[2] = this.dcontext.getClass().getName();
        objArr[3] = Integer.valueOf(this.draw.size());
        objArr[4] = Integer.valueOf(this.dfiltered.size());
        objArr[5] = Integer.valueOf(this.dresult.size());
        return String.format("[XPathDiagnostic: '%s' evaluated (%s) against %s produced  raw=%d discarded=%d returned=%d]", objArr);
    }
}
