package org.jdom2.xpath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.jdom2.Attribute;
import org.jdom2.Comment;
import org.jdom2.Content;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.NamespaceAware;
import org.jdom2.Parent;
import org.jdom2.ProcessingInstruction;
import org.jdom2.Text;
import org.jdom2.filter.Filters;

public final class XPathHelper {
    private XPathHelper() {
    }

    private static StringBuilder getPositionPath(Object node, List<?> siblings, String pathToken, StringBuilder buffer) {
        buffer.append(pathToken);
        if (siblings != null) {
            int position = 0;
            Iterator<?> i = siblings.iterator();
            while (i.hasNext()) {
                position++;
                if (i.next() == node) {
                    break;
                }
            }
            if (position > 1 || i.hasNext()) {
                buffer.append('[').append(position).append(']');
            }
        }
        return buffer;
    }

    private static final StringBuilder getSingleStep(NamespaceAware nsa, StringBuilder buffer) {
        List<?> sibs = null;
        if (nsa instanceof Content) {
            Content content = (Content) nsa;
            Parent pnt = content.getParent();
            if (content instanceof Text) {
                if (pnt != null) {
                    sibs = pnt.getContent(Filters.text());
                }
                return getPositionPath(content, sibs, "text()", buffer);
            } else if (content instanceof Comment) {
                if (pnt != null) {
                    sibs = pnt.getContent(Filters.comment());
                }
                return getPositionPath(content, sibs, "comment()", buffer);
            } else if (content instanceof ProcessingInstruction) {
                if (pnt != null) {
                    sibs = pnt.getContent(Filters.processinginstruction());
                }
                return getPositionPath(content, sibs, "processing-instruction()", buffer);
            } else if ((content instanceof Element) && ((Element) content).getNamespace() == Namespace.NO_NAMESPACE) {
                String ename = ((Element) content).getName();
                if (pnt instanceof Element) {
                    sibs = ((Element) pnt).getChildren(ename);
                }
                return getPositionPath(content, sibs, ename, buffer);
            } else if (content instanceof Element) {
                Element emt = (Element) content;
                if (pnt instanceof Element) {
                    sibs = ((Element) pnt).getChildren(emt.getName(), emt.getNamespace());
                }
                return getPositionPath(content, sibs, "*[local-name() = '" + emt.getName() + "' and namespace-uri() = '" + emt.getNamespaceURI() + "']", buffer);
            } else {
                return getPositionPath(content, pnt == null ? Collections.singletonList(nsa) : pnt.getContent(), "node()", buffer);
            }
        } else if (!(nsa instanceof Attribute)) {
            return buffer;
        } else {
            Attribute att = (Attribute) nsa;
            if (att.getNamespace() == Namespace.NO_NAMESPACE) {
                buffer.append("@").append(att.getName());
                return buffer;
            }
            buffer.append("@*[local-name() = '").append(att.getName());
            buffer.append("' and namespace-uri() = '");
            buffer.append(att.getNamespaceURI()).append("']");
            return buffer;
        }
    }

    private static StringBuilder getRelativeElementPath(Element from, Parent to, StringBuilder sb) {
        if (from == to) {
            sb.append(".");
        } else {
            ArrayList<Parent> tostack = new ArrayList<>();
            Parent p = to;
            while (p != null && p != from) {
                tostack.add(p);
                p = p.getParent();
            }
            int pos = tostack.size();
            if (p != from) {
                Parent f = from;
                int fcnt = 0;
                while (f != null) {
                    pos = locate(f, tostack);
                    if (pos >= 0) {
                        break;
                    }
                    fcnt++;
                    f = f.getParent();
                }
                if (f != null) {
                    while (true) {
                        fcnt--;
                        if (fcnt < 0) {
                            break;
                        }
                        sb.append("../");
                    }
                } else {
                    throw new IllegalArgumentException("The 'from' and 'to' Element have no common ancestor.");
                }
            }
            while (true) {
                pos--;
                if (pos < 0) {
                    break;
                }
                getSingleStep((NamespaceAware) tostack.get(pos), sb);
                sb.append("/");
            }
            sb.setLength(sb.length() - 1);
        }
        return sb;
    }

    private static int locate(Parent f, List<Parent> tostack) {
        int ret = tostack.size();
        do {
            ret--;
            if (ret < 0) {
                return -1;
            }
        } while (f != tostack.get(ret));
        return ret;
    }

    public static String getRelativePath(Content from, Content to) {
        if (from == null) {
            throw new NullPointerException("Cannot create a path from a null target");
        } else if (to == null) {
            throw new NullPointerException("Cannot create a path to a null target");
        } else {
            StringBuilder sb = new StringBuilder();
            if (from == to) {
                return ".";
            }
            Element efrom = from instanceof Element ? (Element) from : from.getParentElement();
            if (from != efrom) {
                sb.append("../");
            }
            if (to instanceof Element) {
                getRelativeElementPath(efrom, (Element) to, sb);
            } else {
                Parent telement = to.getParent();
                if (telement == null) {
                    throw new IllegalArgumentException("Cannot get a relative XPath to detached content.");
                }
                getRelativeElementPath(efrom, telement, sb);
                sb.append("/");
                getSingleStep(to, sb);
            }
            return sb.toString();
        }
    }

    public static String getRelativePath(Content from, Attribute to) {
        if (from == null) {
            throw new NullPointerException("Cannot create a path from a null Content");
        } else if (to == null) {
            throw new NullPointerException("Cannot create a path to a null Attribute");
        } else {
            Element t = to.getParent();
            if (t == null) {
                throw new IllegalArgumentException("Cannot create a path to detached Attribute");
            }
            StringBuilder sb = new StringBuilder(getRelativePath(from, t));
            sb.append("/");
            getSingleStep(to, sb);
            return sb.toString();
        }
    }

    public static String getRelativePath(Attribute from, Attribute to) {
        if (from == null) {
            throw new NullPointerException("Cannot create a path from a null 'from'");
        } else if (to == null) {
            throw new NullPointerException("Cannot create a path to a null target");
        } else if (from == to) {
            return ".";
        } else {
            Element f = from.getParent();
            if (f != null) {
                return "../" + getRelativePath(f, to);
            }
            throw new IllegalArgumentException("Cannot create a path from a detached attrbibute");
        }
    }

    public static String getRelativePath(Attribute from, Content to) {
        if (from == null) {
            throw new NullPointerException("Cannot create a path from a null 'from'");
        } else if (to == null) {
            throw new NullPointerException("Cannot create a path to a null target");
        } else {
            Element f = from.getParent();
            if (f == null) {
                throw new IllegalArgumentException("Cannot create a path from a detached attrbibute");
            } else if (f == to) {
                return "..";
            } else {
                return "../" + getRelativePath(f, to);
            }
        }
    }

    public static String getAbsolutePath(Content to) {
        if (to == null) {
            throw new NullPointerException("Cannot create a path to a null target");
        }
        StringBuilder sb = new StringBuilder();
        Element t = to instanceof Element ? (Element) to : to.getParentElement();
        if (t != null) {
            Element r = t;
            while (r.getParentElement() != null) {
                r = r.getParentElement();
            }
            sb.append("/");
            getSingleStep(r, sb);
            if (r != t) {
                sb.append("/");
                getRelativeElementPath(r, t, sb);
            }
            if (t != to) {
                sb.append("/");
                getSingleStep(to, sb);
            }
            return sb.toString();
        } else if (to.getParent() == null) {
            throw new IllegalArgumentException("Cannot create a path to detached target");
        } else {
            sb.append("/");
            getSingleStep(to, sb);
            return sb.toString();
        }
    }

    public static String getAbsolutePath(Attribute to) {
        if (to == null) {
            throw new NullPointerException("Cannot create a path to a null target");
        }
        Element t = to.getParent();
        if (t == null) {
            throw new IllegalArgumentException("Cannot create a path to detached target");
        }
        Element r = t;
        while (r.getParentElement() != null) {
            r = r.getParentElement();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("/");
        getSingleStep(r, sb);
        if (t != r) {
            sb.append("/");
            getRelativeElementPath(r, t, sb);
        }
        sb.append("/");
        getSingleStep(to, sb);
        return sb.toString();
    }
}
