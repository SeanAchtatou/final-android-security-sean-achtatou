package org.jdom2.xpath;

import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.List;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.internal.SystemProperty;

@Deprecated
public abstract class XPath implements Serializable {
    private static final String DEFAULT_XPATH_CLASS = "org.jdom2.xpath.jaxen.JDOMXPath";
    public static final String JDOM_OBJECT_MODEL_URI = "http://jdom.org/jaxp/xpath/jdom";
    private static final String XPATH_CLASS_PROPERTY = "org.jdom2.xpath.class";
    private static Constructor<? extends XPath> constructor = null;
    private static final long serialVersionUID = 200;

    public abstract void addNamespace(Namespace namespace);

    public abstract String getXPath();

    public abstract Number numberValueOf(Object obj) throws JDOMException;

    public abstract List<?> selectNodes(Object obj) throws JDOMException;

    public abstract Object selectSingleNode(Object obj) throws JDOMException;

    public abstract void setVariable(String str, Object obj);

    public abstract String valueOf(Object obj) throws JDOMException;

    public static XPath newInstance(String path) throws JDOMException {
        String className;
        try {
            if (constructor == null) {
                try {
                    className = SystemProperty.get(XPATH_CLASS_PROPERTY, DEFAULT_XPATH_CLASS);
                } catch (SecurityException e) {
                    className = DEFAULT_XPATH_CLASS;
                }
                Class<?> cls = Class.forName(className);
                if (!XPath.class.isAssignableFrom(cls)) {
                    throw new JDOMException("Unable to create a JDOMXPath from class '" + className + "'.");
                }
                setXPathClass(cls);
            }
            return (XPath) constructor.newInstance(path);
        } catch (JDOMException ex1) {
            throw ex1;
        } catch (InvocationTargetException ex2) {
            Throwable t = ex2.getTargetException();
            throw (t instanceof JDOMException ? (JDOMException) t : new JDOMException(t.toString(), t));
        } catch (Exception ex3) {
            throw new JDOMException(ex3.toString(), ex3);
        }
    }

    public static void setXPathClass(Class<? extends XPath> aClass) throws JDOMException {
        if (aClass == null) {
            throw new IllegalArgumentException("aClass");
        }
        try {
            if (!XPath.class.isAssignableFrom(aClass) || Modifier.isAbstract(aClass.getModifiers())) {
                throw new JDOMException(aClass.getName() + " is not a concrete JDOM XPath implementation");
            }
            constructor = aClass.getConstructor(String.class);
        } catch (JDOMException ex1) {
            throw ex1;
        } catch (Exception ex2) {
            throw new JDOMException(ex2.toString(), ex2);
        }
    }

    public void addNamespace(String prefix, String uri) {
        addNamespace(Namespace.getNamespace(prefix, uri));
    }

    public static List<?> selectNodes(Object context, String path) throws JDOMException {
        return newInstance(path).selectNodes(context);
    }

    public static Object selectSingleNode(Object context, String path) throws JDOMException {
        return newInstance(path).selectSingleNode(context);
    }

    /* access modifiers changed from: protected */
    public final Object writeReplace() throws ObjectStreamException {
        return new XPathString(getXPath());
    }

    private static final class XPathString implements Serializable {
        private static final long serialVersionUID = 200;
        private String xPath = null;

        public XPathString(String xpath) {
            this.xPath = xpath;
        }

        private Object readResolve() throws ObjectStreamException {
            try {
                return XPath.newInstance(this.xPath);
            } catch (JDOMException ex1) {
                throw new InvalidObjectException("Can't create XPath object for expression \"" + this.xPath + "\": " + ex1.toString());
            }
        }
    }
}
