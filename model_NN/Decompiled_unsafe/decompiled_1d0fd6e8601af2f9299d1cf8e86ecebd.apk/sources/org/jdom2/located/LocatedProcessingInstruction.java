package org.jdom2.located;

import java.util.Map;
import org.jdom2.ProcessingInstruction;

public class LocatedProcessingInstruction extends ProcessingInstruction implements Located {
    private static final long serialVersionUID = 200;
    private int col;
    private int line;

    public LocatedProcessingInstruction(String target) {
        super(target);
    }

    public LocatedProcessingInstruction(String target, Map<String, String> data) {
        super(target, data);
    }

    public LocatedProcessingInstruction(String target, String data) {
        super(target, data);
    }

    public int getLine() {
        return this.line;
    }

    public int getColumn() {
        return this.col;
    }

    public void setLine(int line2) {
        this.line = line2;
    }

    public void setColumn(int col2) {
        this.col = col2;
    }
}
