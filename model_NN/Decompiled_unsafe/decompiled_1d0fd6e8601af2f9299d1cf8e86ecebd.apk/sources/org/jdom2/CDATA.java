package org.jdom2;

import org.jdom2.Content;

public class CDATA extends Text {
    private static final long serialVersionUID = 200;

    protected CDATA() {
        super(Content.CType.CDATA);
    }

    public CDATA(String string) {
        super(Content.CType.CDATA);
        setText(string);
    }

    public CDATA setText(String str) {
        if (str == null || "".equals(str)) {
            this.value = "";
        } else {
            String reason = Verifier.checkCDATASection(str);
            if (reason != null) {
                throw new IllegalDataException(str, "CDATA section", reason);
            }
            this.value = str;
        }
        return this;
    }

    public void append(String str) {
        String tmpValue;
        if (str != null && !"".equals(str)) {
            if (this.value == "") {
                tmpValue = str;
            } else {
                tmpValue = this.value + str;
            }
            String reason = Verifier.checkCDATASection(tmpValue);
            if (reason != null) {
                throw new IllegalDataException(str, "CDATA section", reason);
            }
            this.value = tmpValue;
        }
    }

    public void append(Text text) {
        if (text != null) {
            append(text.getText());
        }
    }

    public String toString() {
        return new StringBuilder(64).append("[CDATA: ").append(getText()).append("]").toString();
    }

    public CDATA clone() {
        return (CDATA) super.clone();
    }

    public CDATA detach() {
        return (CDATA) super.detach();
    }

    /* access modifiers changed from: protected */
    public CDATA setParent(Parent parent) {
        return (CDATA) super.setParent(parent);
    }
}
