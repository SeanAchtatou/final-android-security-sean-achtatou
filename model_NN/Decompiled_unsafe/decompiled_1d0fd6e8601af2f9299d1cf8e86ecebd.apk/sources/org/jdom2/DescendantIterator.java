package org.jdom2;

import java.util.Iterator;
import org.jdom2.internal.ArrayCopy;
import org.jdom2.util.IteratorIterable;

final class DescendantIterator implements IteratorIterable<Content> {
    private Iterator<Content> ascending = null;
    private Iterator<Content> current = null;
    private Iterator<Content> descending = null;
    private boolean hasnext = true;
    private final Parent parent;
    private int ssize = 0;
    private Object[] stack = new Object[16];

    DescendantIterator(Parent parent2) {
        this.parent = parent2;
        this.current = parent2.getContent().iterator();
        this.hasnext = this.current.hasNext();
    }

    public DescendantIterator iterator() {
        return new DescendantIterator(this.parent);
    }

    public boolean hasNext() {
        return this.hasnext;
    }

    public Content next() {
        if (this.descending != null) {
            this.current = this.descending;
            this.descending = null;
        } else if (this.ascending != null) {
            this.current = this.ascending;
            this.ascending = null;
        }
        Content ret = this.current.next();
        if ((ret instanceof Element) && ((Element) ret).getContentSize() > 0) {
            this.descending = ((Element) ret).getContent().iterator();
            if (this.ssize >= this.stack.length) {
                this.stack = ArrayCopy.copyOf(this.stack, this.ssize + 16);
            }
            Object[] objArr = this.stack;
            int i = this.ssize;
            this.ssize = i + 1;
            objArr[i] = this.current;
        } else if (!this.current.hasNext()) {
            while (true) {
                if (this.ssize <= 0) {
                    this.ascending = null;
                    this.hasnext = false;
                    break;
                }
                Object[] objArr2 = this.stack;
                int i2 = this.ssize - 1;
                this.ssize = i2;
                this.ascending = (Iterator) objArr2[i2];
                this.stack[this.ssize] = null;
                if (this.ascending.hasNext()) {
                    break;
                }
            }
        }
        return ret;
    }

    public void remove() {
        this.current.remove();
        this.descending = null;
        if (!this.current.hasNext() && this.ascending == null) {
            while (this.ssize > 0) {
                Object[] objArr = this.stack;
                int i = this.ssize - 1;
                this.ssize = i;
                this.stack[this.ssize] = null;
                this.ascending = (Iterator) objArr[i];
                if (this.ascending.hasNext()) {
                    return;
                }
            }
            this.ascending = null;
            this.hasnext = false;
        }
    }
}
