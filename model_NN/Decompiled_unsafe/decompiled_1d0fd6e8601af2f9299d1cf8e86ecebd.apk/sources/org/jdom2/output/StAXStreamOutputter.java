package org.jdom2.output;

import java.util.List;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import org.jdom2.CDATA;
import org.jdom2.Comment;
import org.jdom2.Content;
import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.EntityRef;
import org.jdom2.ProcessingInstruction;
import org.jdom2.Text;
import org.jdom2.output.support.AbstractStAXStreamProcessor;
import org.jdom2.output.support.StAXStreamProcessor;

public final class StAXStreamOutputter implements Cloneable {
    private static final DefaultStAXStreamProcessor DEFAULTPROCESSOR = new DefaultStAXStreamProcessor();
    private Format myFormat;
    private StAXStreamProcessor myProcessor;

    private static final class DefaultStAXStreamProcessor extends AbstractStAXStreamProcessor {
        private DefaultStAXStreamProcessor() {
        }
    }

    public StAXStreamOutputter(Format format, StAXStreamProcessor processor) {
        this.myFormat = null;
        this.myProcessor = null;
        this.myFormat = format == null ? Format.getRawFormat() : format.clone();
        this.myProcessor = processor == null ? DEFAULTPROCESSOR : processor;
    }

    public StAXStreamOutputter() {
        this(null, null);
    }

    public StAXStreamOutputter(Format format) {
        this(format, null);
    }

    public StAXStreamOutputter(StAXStreamProcessor processor) {
        this(null, processor);
    }

    public void setFormat(Format newFormat) {
        this.myFormat = newFormat.clone();
    }

    public Format getFormat() {
        return this.myFormat;
    }

    public StAXStreamProcessor getStAXStream() {
        return this.myProcessor;
    }

    public void setStAXStreamProcessor(StAXStreamProcessor processor) {
        this.myProcessor = processor;
    }

    public final void output(Document doc, XMLStreamWriter out) throws XMLStreamException {
        this.myProcessor.process(out, this.myFormat, doc);
        out.flush();
    }

    public final void output(DocType doctype, XMLStreamWriter out) throws XMLStreamException {
        this.myProcessor.process(out, this.myFormat, doctype);
        out.flush();
    }

    public final void output(Element element, XMLStreamWriter out) throws XMLStreamException {
        this.myProcessor.process(out, this.myFormat, element);
        out.flush();
    }

    public final void outputElementContent(Element element, XMLStreamWriter out) throws XMLStreamException {
        this.myProcessor.process(out, this.myFormat, element.getContent());
        out.flush();
    }

    public final void output(List<? extends Content> list, XMLStreamWriter out) throws XMLStreamException {
        this.myProcessor.process(out, this.myFormat, list);
        out.flush();
    }

    public final void output(CDATA cdata, XMLStreamWriter out) throws XMLStreamException {
        this.myProcessor.process(out, this.myFormat, cdata);
        out.flush();
    }

    public final void output(Text text, XMLStreamWriter out) throws XMLStreamException {
        this.myProcessor.process(out, this.myFormat, text);
        out.flush();
    }

    public final void output(Comment comment, XMLStreamWriter out) throws XMLStreamException {
        this.myProcessor.process(out, this.myFormat, comment);
        out.flush();
    }

    public final void output(ProcessingInstruction pi, XMLStreamWriter out) throws XMLStreamException {
        this.myProcessor.process(out, this.myFormat, pi);
        out.flush();
    }

    public final void output(EntityRef entity, XMLStreamWriter out) throws XMLStreamException {
        this.myProcessor.process(out, this.myFormat, entity);
        out.flush();
    }

    public StAXStreamOutputter clone() {
        try {
            return (StAXStreamOutputter) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e.toString());
        }
    }

    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("StAXStreamOutputter[omitDeclaration = ");
        buffer.append(this.myFormat.omitDeclaration);
        buffer.append(", ");
        buffer.append("encoding = ");
        buffer.append(this.myFormat.encoding);
        buffer.append(", ");
        buffer.append("omitEncoding = ");
        buffer.append(this.myFormat.omitEncoding);
        buffer.append(", ");
        buffer.append("indent = '");
        buffer.append(this.myFormat.indent);
        buffer.append("'");
        buffer.append(", ");
        buffer.append("expandEmptyElements = ");
        buffer.append(this.myFormat.expandEmptyElements);
        buffer.append(", ");
        buffer.append("lineSeparator = '");
        for (char ch : this.myFormat.lineSeparator.toCharArray()) {
            switch (ch) {
                case 9:
                    buffer.append("\\t");
                    break;
                case 10:
                    buffer.append("\\n");
                    break;
                case 11:
                case 12:
                default:
                    buffer.append("[" + ((int) ch) + "]");
                    break;
                case 13:
                    buffer.append("\\r");
                    break;
            }
        }
        buffer.append("', ");
        buffer.append("textMode = ");
        buffer.append(this.myFormat.mode + "]");
        return buffer.toString();
    }
}
