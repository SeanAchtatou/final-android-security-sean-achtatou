package org.jdom2.output.support;

import java.io.IOException;
import java.io.StringReader;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.jdom2.Attribute;
import org.jdom2.AttributeType;
import org.jdom2.CDATA;
import org.jdom2.Comment;
import org.jdom2.Content;
import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.EntityRef;
import org.jdom2.JDOMConstants;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.ProcessingInstruction;
import org.jdom2.Text;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jdom2.util.NamespaceStack;
import org.xml.sax.ContentHandler;
import org.xml.sax.DTDHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.ext.DeclHandler;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.AttributesImpl;
import org.xml.sax.helpers.DefaultHandler;

public class AbstractSAXOutputProcessor extends AbstractOutputProcessor implements SAXOutputProcessor {
    private static void locate(SAXTarget out) {
        out.getContentHandler().setDocumentLocator(out.getLocator());
    }

    public void process(SAXTarget out, Format format, Document doc) throws JDOMException {
        try {
            locate(out);
            printDocument(out, new FormatStack(format), new NamespaceStack(), doc);
        } catch (SAXException se) {
            throw new JDOMException("Encountered a SAX exception processing the Document: ", se);
        }
    }

    public void process(SAXTarget out, Format format, DocType doctype) throws JDOMException {
        try {
            locate(out);
            printDocType(out, new FormatStack(format), doctype);
        } catch (SAXException se) {
            throw new JDOMException("Encountered a SAX exception processing the DocType: ", se);
        }
    }

    public void process(SAXTarget out, Format format, Element element) throws JDOMException {
        try {
            locate(out);
            printElement(out, new FormatStack(format), new NamespaceStack(), element);
        } catch (SAXException se) {
            throw new JDOMException("Encountered a SAX exception processing the Element: ", se);
        }
    }

    public void process(SAXTarget out, Format format, List<? extends Content> list) throws JDOMException {
        try {
            locate(out);
            FormatStack fstack = new FormatStack(format);
            printContent(out, fstack, new NamespaceStack(), buildWalker(fstack, list, false));
        } catch (SAXException se) {
            throw new JDOMException("Encountered a SAX exception processing the List: ", se);
        }
    }

    public void process(SAXTarget out, Format format, CDATA cdata) throws JDOMException {
        try {
            locate(out);
            List<CDATA> list = Collections.singletonList(cdata);
            FormatStack fstack = new FormatStack(format);
            printContent(out, fstack, new NamespaceStack(), buildWalker(fstack, list, false));
        } catch (SAXException se) {
            throw new JDOMException("Encountered a SAX exception processing the CDATA: ", se);
        }
    }

    public void process(SAXTarget out, Format format, Text text) throws JDOMException {
        try {
            locate(out);
            List<Text> list = Collections.singletonList(text);
            FormatStack fstack = new FormatStack(format);
            printContent(out, fstack, new NamespaceStack(), buildWalker(fstack, list, false));
        } catch (SAXException se) {
            throw new JDOMException("Encountered a SAX exception processing the Text: ", se);
        }
    }

    public void process(SAXTarget out, Format format, Comment comment) throws JDOMException {
        try {
            locate(out);
            printComment(out, new FormatStack(format), comment);
        } catch (SAXException se) {
            throw new JDOMException("Encountered a SAX exception processing the Comment: ", se);
        }
    }

    public void process(SAXTarget out, Format format, ProcessingInstruction pi) throws JDOMException {
        try {
            locate(out);
            printProcessingInstruction(out, new FormatStack(format), pi);
        } catch (SAXException se) {
            throw new JDOMException("Encountered a SAX exception processing the ProcessingInstruction: ", se);
        }
    }

    public void process(SAXTarget out, Format format, EntityRef entity) throws JDOMException {
        try {
            locate(out);
            printEntityRef(out, new FormatStack(format), entity);
        } catch (SAXException se) {
            throw new JDOMException("Encountered a SAX exception processing the EntityRef: ", se);
        }
    }

    public void processAsDocument(SAXTarget out, Format format, List<? extends Content> nodes) throws JDOMException {
        if (nodes != null) {
            try {
                if (nodes.size() != 0) {
                    locate(out);
                    out.getContentHandler().startDocument();
                    FormatStack fstack = new FormatStack(format);
                    if (out.isReportDTDEvents()) {
                        Iterator i$ = nodes.iterator();
                        while (true) {
                            if (!i$.hasNext()) {
                                break;
                            }
                            Content c = (Content) i$.next();
                            if (c instanceof DocType) {
                                printDocType(out, fstack, (DocType) c);
                                break;
                            }
                        }
                    }
                    printContent(out, fstack, new NamespaceStack(), buildWalker(fstack, nodes, false));
                    out.getContentHandler().endDocument();
                }
            } catch (SAXException se) {
                throw new JDOMException("Encountered a SAX exception processing the List: ", se);
            }
        }
    }

    public void processAsDocument(SAXTarget out, Format format, Element node) throws JDOMException {
        if (node != null) {
            try {
                locate(out);
                out.getContentHandler().startDocument();
                printElement(out, new FormatStack(format), new NamespaceStack(), node);
                out.getContentHandler().endDocument();
            } catch (SAXException se) {
                throw new JDOMException("Encountered a SAX exception processing the Element: ", se);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void printDocument(SAXTarget out, FormatStack fstack, NamespaceStack nstack, Document document) throws SAXException {
        if (document != null) {
            out.getContentHandler().startDocument();
            if (out.isReportDTDEvents()) {
                printDocType(out, fstack, document.getDocType());
            }
            int sz = document.getContentSize();
            if (sz > 0) {
                for (int i = 0; i < sz; i++) {
                    Content c = document.getContent(i);
                    out.getLocator().setNode(c);
                    switch (c.getCType()) {
                        case Comment:
                            printComment(out, fstack, (Comment) c);
                            break;
                        case Element:
                            printElement(out, fstack, nstack, (Element) c);
                            break;
                        case ProcessingInstruction:
                            printProcessingInstruction(out, fstack, (ProcessingInstruction) c);
                            break;
                    }
                }
            }
            out.getContentHandler().endDocument();
        }
    }

    /* access modifiers changed from: protected */
    public void printDocType(SAXTarget out, FormatStack fstack, DocType docType) throws SAXException {
        DTDHandler dtdHandler = out.getDTDHandler();
        DeclHandler declHandler = out.getDeclHandler();
        if (docType == null) {
            return;
        }
        if (dtdHandler != null || declHandler != null) {
            try {
                createDTDParser(out).parse(new InputSource(new StringReader(new XMLOutputter().outputString(docType))));
            } catch (SAXParseException e) {
            } catch (IOException e2) {
                throw new SAXException("DTD parsing error", e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void printProcessingInstruction(SAXTarget out, FormatStack fstack, ProcessingInstruction pi) throws SAXException {
        out.getContentHandler().processingInstruction(pi.getTarget(), pi.getData());
    }

    /* access modifiers changed from: protected */
    public void printComment(SAXTarget out, FormatStack fstack, Comment comment) throws SAXException {
        if (out.getLexicalHandler() != null) {
            char[] c = comment.getText().toCharArray();
            out.getLexicalHandler().comment(c, 0, c.length);
        }
    }

    /* access modifiers changed from: protected */
    public void printEntityRef(SAXTarget out, FormatStack fstack, EntityRef entity) throws SAXException {
        out.getContentHandler().skippedEntity(entity.getName());
    }

    /* access modifiers changed from: protected */
    public void printCDATA(SAXTarget out, FormatStack fstack, CDATA cdata) throws SAXException {
        LexicalHandler lexicalHandler = out.getLexicalHandler();
        char[] chars = cdata.getText().toCharArray();
        if (lexicalHandler != null) {
            lexicalHandler.startCDATA();
            out.getContentHandler().characters(chars, 0, chars.length);
            lexicalHandler.endCDATA();
            return;
        }
        out.getContentHandler().characters(chars, 0, chars.length);
    }

    /* access modifiers changed from: protected */
    public void printText(SAXTarget out, FormatStack fstack, Text text) throws SAXException {
        char[] chars = text.getText().toCharArray();
        out.getContentHandler().characters(chars, 0, chars.length);
    }

    /* access modifiers changed from: protected */
    public void printElement(SAXTarget out, FormatStack fstack, NamespaceStack nstack, Element element) throws SAXException {
        ContentHandler ch = out.getContentHandler();
        Object origloc = out.getLocator().getNode();
        nstack.push(element);
        try {
            out.getLocator().setNode(element);
            AttributesImpl atts = new AttributesImpl();
            for (Namespace ns : nstack.addedForward()) {
                ch.startPrefixMapping(ns.getPrefix(), ns.getURI());
                if (out.isDeclareNamespaces()) {
                    if (ns.getPrefix().equals("")) {
                        atts.addAttribute("", "", "xmlns", "CDATA", ns.getURI());
                    } else {
                        atts.addAttribute("", "", "xmlns:" + ns.getPrefix(), "CDATA", ns.getURI());
                    }
                }
            }
            if (element.hasAttributes()) {
                for (Attribute a : element.getAttributes()) {
                    if (a.isSpecified() || !fstack.isSpecifiedAttributesOnly()) {
                        atts.addAttribute(a.getNamespaceURI(), a.getName(), a.getQualifiedName(), getAttributeTypeName(a.getAttributeType()), a.getValue());
                    }
                }
            }
            ch.startElement(element.getNamespaceURI(), element.getName(), element.getQualifiedName(), atts);
            List<Content> content = element.getContent();
            if (!content.isEmpty()) {
                Format.TextMode textmode = fstack.getTextMode();
                String space = element.getAttributeValue("space", Namespace.XML_NAMESPACE);
                if ("default".equals(space)) {
                    textmode = fstack.getDefaultMode();
                } else if ("preserve".equals(space)) {
                    textmode = Format.TextMode.PRESERVE;
                }
                fstack.push();
                fstack.setTextMode(textmode);
                Walker walker = buildWalker(fstack, content, false);
                if (walker.hasNext()) {
                    if (!walker.isAllText() && fstack.getPadBetween() != null) {
                        printText(out, fstack, new Text(fstack.getPadBetween()));
                    }
                    printContent(out, fstack, nstack, walker);
                    if (!walker.isAllText() && fstack.getPadLast() != null) {
                        printText(out, fstack, new Text(fstack.getPadLast()));
                    }
                }
                fstack.pop();
            }
            out.getContentHandler().endElement(element.getNamespaceURI(), element.getName(), element.getQualifiedName());
            for (Namespace ns2 : nstack.addedReverse()) {
                ch.endPrefixMapping(ns2.getPrefix());
            }
            nstack.pop();
            out.getLocator().setNode(origloc);
        } catch (Throwable th) {
            nstack.pop();
            out.getLocator().setNode(origloc);
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void printContent(SAXTarget out, FormatStack fstack, NamespaceStack nstack, Walker walker) throws SAXException {
        while (walker.hasNext()) {
            Content c = walker.next();
            if (c == null) {
                String text = walker.text();
                if (walker.isCDATA()) {
                    printCDATA(out, fstack, new CDATA(text));
                } else {
                    printText(out, fstack, new Text(text));
                }
            } else {
                switch (c.getCType()) {
                    case Comment:
                        printComment(out, fstack, (Comment) c);
                        continue;
                    case Element:
                        printElement(out, fstack, nstack, (Element) c);
                        continue;
                    case ProcessingInstruction:
                        printProcessingInstruction(out, fstack, (ProcessingInstruction) c);
                        continue;
                    case CDATA:
                        printCDATA(out, fstack, (CDATA) c);
                        continue;
                    case EntityRef:
                        printEntityRef(out, fstack, (EntityRef) c);
                        continue;
                    case Text:
                        printText(out, fstack, (Text) c);
                        continue;
                }
            }
        }
    }

    private static String getAttributeTypeName(AttributeType type) {
        switch (type) {
            case UNDECLARED:
                return "CDATA";
            default:
                return type.name();
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v11, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: org.xml.sax.XMLReader} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.xml.sax.XMLReader createParser() throws java.lang.Exception {
        /*
            r11 = this;
            r7 = 0
            java.lang.String r9 = "javax.xml.parsers.SAXParserFactory"
            java.lang.Class r2 = java.lang.Class.forName(r9)     // Catch:{ ClassNotFoundException -> 0x004f, InvocationTargetException -> 0x004d, NoSuchMethodException -> 0x004b, IllegalAccessException -> 0x0049 }
            java.lang.String r9 = "newInstance"
            r10 = 0
            java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ ClassNotFoundException -> 0x004f, InvocationTargetException -> 0x004d, NoSuchMethodException -> 0x004b, IllegalAccessException -> 0x0049 }
            java.lang.reflect.Method r5 = r2.getMethod(r9, r10)     // Catch:{ ClassNotFoundException -> 0x004f, InvocationTargetException -> 0x004d, NoSuchMethodException -> 0x004b, IllegalAccessException -> 0x0049 }
            r9 = 0
            r10 = 0
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ ClassNotFoundException -> 0x004f, InvocationTargetException -> 0x004d, NoSuchMethodException -> 0x004b, IllegalAccessException -> 0x0049 }
            java.lang.Object r1 = r5.invoke(r9, r10)     // Catch:{ ClassNotFoundException -> 0x004f, InvocationTargetException -> 0x004d, NoSuchMethodException -> 0x004b, IllegalAccessException -> 0x0049 }
            java.lang.String r9 = "newSAXParser"
            r10 = 0
            java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ ClassNotFoundException -> 0x004f, InvocationTargetException -> 0x004d, NoSuchMethodException -> 0x004b, IllegalAccessException -> 0x0049 }
            java.lang.reflect.Method r6 = r2.getMethod(r9, r10)     // Catch:{ ClassNotFoundException -> 0x004f, InvocationTargetException -> 0x004d, NoSuchMethodException -> 0x004b, IllegalAccessException -> 0x0049 }
            r9 = 0
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ ClassNotFoundException -> 0x004f, InvocationTargetException -> 0x004d, NoSuchMethodException -> 0x004b, IllegalAccessException -> 0x0049 }
            java.lang.Object r4 = r6.invoke(r1, r9)     // Catch:{ ClassNotFoundException -> 0x004f, InvocationTargetException -> 0x004d, NoSuchMethodException -> 0x004b, IllegalAccessException -> 0x0049 }
            java.lang.Class r8 = r4.getClass()     // Catch:{ ClassNotFoundException -> 0x004f, InvocationTargetException -> 0x004d, NoSuchMethodException -> 0x004b, IllegalAccessException -> 0x0049 }
            java.lang.String r9 = "getXMLReader"
            r10 = 0
            java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ ClassNotFoundException -> 0x004f, InvocationTargetException -> 0x004d, NoSuchMethodException -> 0x004b, IllegalAccessException -> 0x0049 }
            java.lang.reflect.Method r3 = r8.getMethod(r9, r10)     // Catch:{ ClassNotFoundException -> 0x004f, InvocationTargetException -> 0x004d, NoSuchMethodException -> 0x004b, IllegalAccessException -> 0x0049 }
            r9 = 0
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ ClassNotFoundException -> 0x004f, InvocationTargetException -> 0x004d, NoSuchMethodException -> 0x004b, IllegalAccessException -> 0x0049 }
            java.lang.Object r9 = r3.invoke(r4, r9)     // Catch:{ ClassNotFoundException -> 0x004f, InvocationTargetException -> 0x004d, NoSuchMethodException -> 0x004b, IllegalAccessException -> 0x0049 }
            r0 = r9
            org.xml.sax.XMLReader r0 = (org.xml.sax.XMLReader) r0     // Catch:{ ClassNotFoundException -> 0x004f, InvocationTargetException -> 0x004d, NoSuchMethodException -> 0x004b, IllegalAccessException -> 0x0049 }
            r7 = r0
        L_0x0040:
            if (r7 != 0) goto L_0x0048
            java.lang.String r9 = "org.apache.xerces.parsers.SAXParser"
            org.xml.sax.XMLReader r7 = org.xml.sax.helpers.XMLReaderFactory.createXMLReader(r9)
        L_0x0048:
            return r7
        L_0x0049:
            r9 = move-exception
            goto L_0x0040
        L_0x004b:
            r9 = move-exception
            goto L_0x0040
        L_0x004d:
            r9 = move-exception
            goto L_0x0040
        L_0x004f:
            r9 = move-exception
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jdom2.output.support.AbstractSAXOutputProcessor.createParser():org.xml.sax.XMLReader");
    }

    private XMLReader createDTDParser(SAXTarget out) throws SAXException {
        try {
            XMLReader parser = createParser();
            if (out.getDTDHandler() != null) {
                parser.setDTDHandler(out.getDTDHandler());
            }
            if (out.getEntityResolver() != null) {
                parser.setEntityResolver(out.getEntityResolver());
            }
            if (out.getLexicalHandler() != null) {
                try {
                    parser.setProperty(JDOMConstants.SAX_PROPERTY_LEXICAL_HANDLER, out.getLexicalHandler());
                } catch (SAXException e) {
                    try {
                        parser.setProperty(JDOMConstants.SAX_PROPERTY_LEXICAL_HANDLER_ALT, out.getLexicalHandler());
                    } catch (SAXException e2) {
                    }
                }
            }
            if (out.getDeclHandler() != null) {
                try {
                    parser.setProperty(JDOMConstants.SAX_PROPERTY_DECLARATION_HANDLER, out.getDeclHandler());
                } catch (SAXException e3) {
                    try {
                        parser.setProperty(JDOMConstants.SAX_PROPERTY_DECLARATION_HANDLER_ALT, out.getDeclHandler());
                    } catch (SAXException e4) {
                    }
                }
            }
            parser.setErrorHandler(new DefaultHandler());
            return parser;
        } catch (Exception ex1) {
            throw new SAXException("Error in SAX parser allocation", ex1);
        }
    }
}
