package org.jdom2.output.support;

import java.util.List;
import org.jdom2.Content;
import org.jdom2.Text;
import org.jdom2.Verifier;
import org.jdom2.output.support.AbstractFormattedWalker;

public class WalkerTRIM_FULL_WHITE extends AbstractFormattedWalker {
    public WalkerTRIM_FULL_WHITE(List<? extends Content> content, FormatStack fstack, boolean escape) {
        super(content, fstack, escape);
    }

    /* access modifiers changed from: protected */
    public void analyzeMultiText(AbstractFormattedWalker.MultiText mtext, int offset, int len) {
        Content c;
        int ln = len;
        do {
            ln--;
            if (ln < 0) {
                break;
            }
            c = get(offset + ln);
            if (!(c instanceof Text)) {
                break;
            }
        } while (Verifier.isAllXMLWhitespace(c.getValue()));
        if (ln >= 0) {
            for (int i = 0; i < len; i++) {
                Content c2 = get(offset + i);
                switch (c2.getCType()) {
                    case Text:
                        mtext.appendText(AbstractFormattedWalker.Trim.NONE, c2.getValue());
                        break;
                    case CDATA:
                        mtext.appendCDATA(AbstractFormattedWalker.Trim.NONE, c2.getValue());
                        break;
                    default:
                        mtext.appendRaw(c2);
                        break;
                }
            }
        }
    }
}
