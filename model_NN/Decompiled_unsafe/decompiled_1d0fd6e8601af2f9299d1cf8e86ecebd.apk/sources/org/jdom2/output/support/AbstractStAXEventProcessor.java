package org.jdom2.output.support;

import com.fsck.k9.Account;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EntityDeclaration;
import javax.xml.stream.events.Namespace;
import javax.xml.stream.util.XMLEventConsumer;
import org.jdom2.CDATA;
import org.jdom2.Comment;
import org.jdom2.Content;
import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.EntityRef;
import org.jdom2.ProcessingInstruction;
import org.jdom2.Text;
import org.jdom2.Verifier;
import org.jdom2.output.Format;
import org.jdom2.util.NamespaceStack;

public abstract class AbstractStAXEventProcessor extends AbstractOutputProcessor implements StAXEventProcessor {

    private static final class NSIterator implements Iterator<Namespace> {
        private final XMLEventFactory fac;
        private final Iterator<org.jdom2.Namespace> source;

        public NSIterator(Iterator<org.jdom2.Namespace> source2, XMLEventFactory fac2) {
            this.source = source2;
            this.fac = fac2;
        }

        public boolean hasNext() {
            return this.source.hasNext();
        }

        public Namespace next() {
            org.jdom2.Namespace ns = this.source.next();
            return this.fac.createNamespace(ns.getPrefix(), ns.getURI());
        }

        public void remove() {
            throw new UnsupportedOperationException("Cannot remove Namespaces");
        }
    }

    private static final class AttIterator implements Iterator<Attribute> {
        private final XMLEventFactory fac;
        private final Iterator<org.jdom2.Attribute> source;

        public AttIterator(Iterator<org.jdom2.Attribute> source2, XMLEventFactory fac2, boolean specifiedAttributesOnly) {
            this.source = specifiedAttributesOnly ? specified(source2) : source2;
            this.fac = fac2;
        }

        private Iterator<org.jdom2.Attribute> specified(Iterator<org.jdom2.Attribute> src) {
            if (src == null) {
                return null;
            }
            ArrayList<org.jdom2.Attribute> al = new ArrayList<>();
            while (src.hasNext()) {
                org.jdom2.Attribute att = src.next();
                if (att.isSpecified()) {
                    al.add(att);
                }
            }
            if (!al.isEmpty()) {
                return al.iterator();
            }
            return null;
        }

        public boolean hasNext() {
            return this.source != null && this.source.hasNext();
        }

        public Attribute next() {
            org.jdom2.Attribute att = this.source.next();
            org.jdom2.Namespace ns = att.getNamespace();
            if (ns == org.jdom2.Namespace.NO_NAMESPACE) {
                return this.fac.createAttribute(att.getName(), att.getValue());
            }
            return this.fac.createAttribute(ns.getPrefix(), ns.getURI(), att.getName(), att.getValue());
        }

        public void remove() {
            throw new UnsupportedOperationException("Cannot remove attributes");
        }
    }

    public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, Document doc) throws XMLStreamException {
        printDocument(out, new FormatStack(format), new NamespaceStack(), eventfactory, doc);
    }

    public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, DocType doctype) throws XMLStreamException {
        printDocType(out, new FormatStack(format), eventfactory, doctype);
    }

    public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, Element element) throws XMLStreamException {
        printElement(out, new FormatStack(format), new NamespaceStack(), eventfactory, element);
    }

    public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, List<? extends Content> list) throws XMLStreamException {
        XMLEventConsumer xMLEventConsumer = out;
        printContent(xMLEventConsumer, new FormatStack(format), new NamespaceStack(), eventfactory, buildWalker(new FormatStack(format), list, false));
    }

    public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, CDATA cdata) throws XMLStreamException {
        List<CDATA> list = Collections.singletonList(cdata);
        FormatStack fstack = new FormatStack(format);
        Walker walker = buildWalker(fstack, list, false);
        if (walker.hasNext()) {
            Content c = walker.next();
            if (c == null) {
                printCDATA(out, fstack, eventfactory, new CDATA(walker.text()));
            } else if (c.getCType() == Content.CType.CDATA) {
                printCDATA(out, fstack, eventfactory, (CDATA) c);
            }
        }
    }

    public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, Text text) throws XMLStreamException {
        List<Text> list = Collections.singletonList(text);
        FormatStack fstack = new FormatStack(format);
        Walker walker = buildWalker(fstack, list, false);
        if (walker.hasNext()) {
            Content c = walker.next();
            if (c == null) {
                printText(out, fstack, eventfactory, new Text(walker.text()));
            } else if (c.getCType() == Content.CType.Text) {
                printText(out, fstack, eventfactory, (Text) c);
            }
        }
    }

    public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, Comment comment) throws XMLStreamException {
        printComment(out, new FormatStack(format), eventfactory, comment);
    }

    public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, ProcessingInstruction pi) throws XMLStreamException {
        FormatStack fstack = new FormatStack(format);
        fstack.setIgnoreTrAXEscapingPIs(true);
        printProcessingInstruction(out, fstack, eventfactory, pi);
    }

    public void process(XMLEventConsumer out, Format format, XMLEventFactory eventfactory, EntityRef entity) throws XMLStreamException {
        printEntityRef(out, new FormatStack(format), eventfactory, entity);
    }

    /* access modifiers changed from: protected */
    public void printDocument(XMLEventConsumer out, FormatStack fstack, NamespaceStack nstack, XMLEventFactory eventfactory, Document doc) throws XMLStreamException {
        if (fstack.isOmitDeclaration()) {
            out.add(eventfactory.createStartDocument((String) null, (String) null));
        } else if (fstack.isOmitEncoding()) {
            out.add(eventfactory.createStartDocument((String) null, "1.0"));
            if (fstack.getLineSeparator() != null) {
                out.add(eventfactory.createCharacters(fstack.getLineSeparator()));
            }
        } else {
            out.add(eventfactory.createStartDocument(fstack.getEncoding(), "1.0"));
            if (fstack.getLineSeparator() != null) {
                out.add(eventfactory.createCharacters(fstack.getLineSeparator()));
            }
        }
        List<Content> list = doc.hasRootElement() ? doc.getContent() : new ArrayList<>(doc.getContentSize());
        if (list.isEmpty()) {
            int sz = doc.getContentSize();
            for (int i = 0; i < sz; i++) {
                list.add(doc.getContent(i));
            }
        }
        Walker walker = buildWalker(fstack, list, false);
        if (walker.hasNext()) {
            while (walker.hasNext()) {
                Content c = walker.next();
                if (c == null) {
                    String padding = walker.text();
                    if (padding != null && Verifier.isAllXMLWhitespace(padding) && !walker.isCDATA()) {
                        out.add(eventfactory.createCharacters(padding));
                    }
                } else {
                    switch (c.getCType()) {
                        case Comment:
                            printComment(out, fstack, eventfactory, (Comment) c);
                            continue;
                        case DocType:
                            printDocType(out, fstack, eventfactory, (DocType) c);
                            continue;
                        case Element:
                            printElement(out, fstack, nstack, eventfactory, (Element) c);
                            continue;
                        case ProcessingInstruction:
                            printProcessingInstruction(out, fstack, eventfactory, (ProcessingInstruction) c);
                            continue;
                    }
                }
            }
            if (fstack.getLineSeparator() != null) {
                out.add(eventfactory.createCharacters(fstack.getLineSeparator()));
            }
        }
        out.add(eventfactory.createEndDocument());
    }

    /* access modifiers changed from: protected */
    public void printDocType(XMLEventConsumer out, FormatStack fstack, XMLEventFactory eventfactory, DocType docType) throws XMLStreamException {
        String publicID = docType.getPublicID();
        String systemID = docType.getSystemID();
        String internalSubset = docType.getInternalSubset();
        boolean hasPublic = false;
        StringWriter sw = new StringWriter();
        sw.write("<!DOCTYPE ");
        sw.write(docType.getElementName());
        if (publicID != null) {
            sw.write(" PUBLIC \"");
            sw.write(publicID);
            sw.write("\"");
            hasPublic = true;
        }
        if (systemID != null) {
            if (!hasPublic) {
                sw.write(" SYSTEM");
            }
            sw.write(" \"");
            sw.write(systemID);
            sw.write("\"");
        }
        if (internalSubset != null && !internalSubset.equals("")) {
            sw.write(" [");
            sw.write(fstack.getLineSeparator());
            sw.write(docType.getInternalSubset());
            sw.write("]");
        }
        sw.write(Account.DEFAULT_QUOTE_PREFIX);
        out.add(eventfactory.createDTD(sw.toString()));
    }

    /* access modifiers changed from: protected */
    public void printProcessingInstruction(XMLEventConsumer out, FormatStack fstack, XMLEventFactory eventfactory, ProcessingInstruction pi) throws XMLStreamException {
        String target = pi.getTarget();
        String rawData = pi.getData();
        if (rawData == null || rawData.trim().length() <= 0) {
            out.add(eventfactory.createProcessingInstruction(target, ""));
        } else {
            out.add(eventfactory.createProcessingInstruction(target, rawData));
        }
    }

    /* access modifiers changed from: protected */
    public void printComment(XMLEventConsumer out, FormatStack fstack, XMLEventFactory eventfactory, Comment comment) throws XMLStreamException {
        out.add(eventfactory.createComment(comment.getText()));
    }

    /* access modifiers changed from: protected */
    public void printEntityRef(XMLEventConsumer out, FormatStack fstack, XMLEventFactory eventfactory, EntityRef entity) throws XMLStreamException {
        out.add(eventfactory.createEntityReference(entity.getName(), (EntityDeclaration) null));
    }

    /* access modifiers changed from: protected */
    public void printCDATA(XMLEventConsumer out, FormatStack fstack, XMLEventFactory eventfactory, CDATA cdata) throws XMLStreamException {
        out.add(eventfactory.createCData(cdata.getText()));
    }

    /* access modifiers changed from: protected */
    public void printText(XMLEventConsumer out, FormatStack fstack, XMLEventFactory eventfactory, Text text) throws XMLStreamException {
        out.add(eventfactory.createCharacters(text.getText()));
    }

    /* access modifiers changed from: protected */
    public void printElement(XMLEventConsumer out, FormatStack fstack, NamespaceStack nstack, XMLEventFactory eventfactory, Element element) throws XMLStreamException {
        nstack.push(element);
        try {
            org.jdom2.Namespace ns = element.getNamespace();
            Iterator<org.jdom2.Attribute> ait = element.hasAttributes() ? element.getAttributes().iterator() : null;
            if (ns == org.jdom2.Namespace.NO_NAMESPACE) {
                out.add(eventfactory.createStartElement("", "", element.getName(), new AttIterator(ait, eventfactory, fstack.isSpecifiedAttributesOnly()), new NSIterator(nstack.addedForward().iterator(), eventfactory)));
            } else if ("".equals(ns.getPrefix())) {
                out.add(eventfactory.createStartElement("", ns.getURI(), element.getName(), new AttIterator(ait, eventfactory, fstack.isSpecifiedAttributesOnly()), new NSIterator(nstack.addedForward().iterator(), eventfactory)));
            } else {
                out.add(eventfactory.createStartElement(ns.getPrefix(), ns.getURI(), element.getName(), new AttIterator(ait, eventfactory, fstack.isSpecifiedAttributesOnly()), new NSIterator(nstack.addedForward().iterator(), eventfactory)));
            }
            List<Content> content = element.getContent();
            if (!content.isEmpty()) {
                Format.TextMode textmode = fstack.getTextMode();
                String space = element.getAttributeValue("space", org.jdom2.Namespace.XML_NAMESPACE);
                if ("default".equals(space)) {
                    textmode = fstack.getDefaultMode();
                } else if ("preserve".equals(space)) {
                    textmode = Format.TextMode.PRESERVE;
                }
                fstack.push();
                fstack.setTextMode(textmode);
                Walker walker = buildWalker(fstack, content, false);
                if (walker.hasNext()) {
                    if (!walker.isAllText() && fstack.getPadBetween() != null) {
                        printText(out, fstack, eventfactory, new Text(fstack.getPadBetween()));
                    }
                    printContent(out, fstack, nstack, eventfactory, walker);
                    if (!walker.isAllText() && fstack.getPadLast() != null) {
                        printText(out, fstack, eventfactory, new Text(fstack.getPadLast()));
                    }
                }
                fstack.pop();
            }
            out.add(eventfactory.createEndElement(element.getNamespacePrefix(), element.getNamespaceURI(), element.getName(), new NSIterator(nstack.addedReverse().iterator(), eventfactory)));
            nstack.pop();
        } catch (Throwable th) {
            nstack.pop();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void printContent(XMLEventConsumer out, FormatStack fstack, NamespaceStack nstack, XMLEventFactory eventfactory, Walker walker) throws XMLStreamException {
        while (walker.hasNext()) {
            Content content = walker.next();
            if (content != null) {
                switch (content.getCType()) {
                    case Comment:
                        printComment(out, fstack, eventfactory, (Comment) content);
                        continue;
                    case DocType:
                        printDocType(out, fstack, eventfactory, (DocType) content);
                        continue;
                    case Element:
                        printElement(out, fstack, nstack, eventfactory, (Element) content);
                        continue;
                    case ProcessingInstruction:
                        printProcessingInstruction(out, fstack, eventfactory, (ProcessingInstruction) content);
                        continue;
                    case CDATA:
                        printCDATA(out, fstack, eventfactory, (CDATA) content);
                        continue;
                    case EntityRef:
                        printEntityRef(out, fstack, eventfactory, (EntityRef) content);
                        continue;
                    case Text:
                        printText(out, fstack, eventfactory, (Text) content);
                        continue;
                    default:
                        throw new IllegalStateException("Unexpected Content " + content.getCType());
                }
            } else if (walker.isCDATA()) {
                printCDATA(out, fstack, eventfactory, new CDATA(walker.text()));
            } else {
                printText(out, fstack, eventfactory, new Text(walker.text()));
            }
        }
    }
}
