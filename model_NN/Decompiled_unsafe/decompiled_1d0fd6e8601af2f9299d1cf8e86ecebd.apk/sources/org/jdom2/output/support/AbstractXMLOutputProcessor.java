package org.jdom2.output.support;

import com.fsck.k9.Account;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.jdom2.Attribute;
import org.jdom2.CDATA;
import org.jdom2.Comment;
import org.jdom2.Content;
import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.EntityRef;
import org.jdom2.Namespace;
import org.jdom2.ProcessingInstruction;
import org.jdom2.Text;
import org.jdom2.Verifier;
import org.jdom2.output.Format;
import org.jdom2.util.NamespaceStack;

public abstract class AbstractXMLOutputProcessor extends AbstractOutputProcessor implements XMLOutputProcessor {
    protected static final String CDATAPOST = "]]>";
    protected static final String CDATAPRE = "<![CDATA[";

    public void process(Writer out, Format format, Document doc) throws IOException {
        printDocument(out, new FormatStack(format), new NamespaceStack(), doc);
        out.flush();
    }

    public void process(Writer out, Format format, DocType doctype) throws IOException {
        printDocType(out, new FormatStack(format), doctype);
        out.flush();
    }

    public void process(Writer out, Format format, Element element) throws IOException {
        printElement(out, new FormatStack(format), new NamespaceStack(), element);
        out.flush();
    }

    public void process(Writer out, Format format, List<? extends Content> list) throws IOException {
        FormatStack fstack = new FormatStack(format);
        printContent(out, fstack, new NamespaceStack(), buildWalker(fstack, list, true));
        out.flush();
    }

    public void process(Writer out, Format format, CDATA cdata) throws IOException {
        List<CDATA> list = Collections.singletonList(cdata);
        FormatStack fstack = new FormatStack(format);
        Walker walker = buildWalker(fstack, list, true);
        if (walker.hasNext()) {
            printContent(out, fstack, new NamespaceStack(), walker);
        }
        out.flush();
    }

    public void process(Writer out, Format format, Text text) throws IOException {
        List<Text> list = Collections.singletonList(text);
        FormatStack fstack = new FormatStack(format);
        Walker walker = buildWalker(fstack, list, true);
        if (walker.hasNext()) {
            printContent(out, fstack, new NamespaceStack(), walker);
        }
        out.flush();
    }

    public void process(Writer out, Format format, Comment comment) throws IOException {
        printComment(out, new FormatStack(format), comment);
        out.flush();
    }

    public void process(Writer out, Format format, ProcessingInstruction pi) throws IOException {
        FormatStack fstack = new FormatStack(format);
        fstack.setIgnoreTrAXEscapingPIs(true);
        printProcessingInstruction(out, fstack, pi);
        out.flush();
    }

    public void process(Writer out, Format format, EntityRef entity) throws IOException {
        printEntityRef(out, new FormatStack(format), entity);
        out.flush();
    }

    /* access modifiers changed from: protected */
    public void write(Writer out, String str) throws IOException {
        if (str != null) {
            out.write(str);
        }
    }

    /* access modifiers changed from: protected */
    public void write(Writer out, char c) throws IOException {
        out.write(c);
    }

    /* access modifiers changed from: protected */
    public void attributeEscapedEntitiesFilter(Writer out, FormatStack fstack, String value) throws IOException {
        if (!fstack.getEscapeOutput()) {
            write(out, value);
        } else {
            write(out, Format.escapeAttribute(fstack.getEscapeStrategy(), value));
        }
    }

    /* access modifiers changed from: protected */
    public void textRaw(Writer out, String str) throws IOException {
        write(out, str);
    }

    /* access modifiers changed from: protected */
    public void textRaw(Writer out, char ch) throws IOException {
        write(out, ch);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jdom2.output.support.AbstractXMLOutputProcessor.textRaw(java.io.Writer, char):void
     arg types: [java.io.Writer, int]
     candidates:
      org.jdom2.output.support.AbstractXMLOutputProcessor.textRaw(java.io.Writer, java.lang.String):void
      org.jdom2.output.support.AbstractXMLOutputProcessor.textRaw(java.io.Writer, char):void */
    /* access modifiers changed from: protected */
    public void textEntityRef(Writer out, String name) throws IOException {
        textRaw(out, '&');
        textRaw(out, name);
        textRaw(out, ';');
    }

    /* access modifiers changed from: protected */
    public void textCDATA(Writer out, String text) throws IOException {
        textRaw(out, "<![CDATA[");
        textRaw(out, text);
        textRaw(out, "]]>");
    }

    /* access modifiers changed from: protected */
    public void printDocument(Writer out, FormatStack fstack, NamespaceStack nstack, Document doc) throws IOException {
        List<Content> list = doc.hasRootElement() ? doc.getContent() : new ArrayList<>(doc.getContentSize());
        if (list.isEmpty()) {
            int sz = doc.getContentSize();
            for (int i = 0; i < sz; i++) {
                list.add(doc.getContent(i));
            }
        }
        printDeclaration(out, fstack);
        Walker walker = buildWalker(fstack, list, true);
        if (walker.hasNext()) {
            while (walker.hasNext()) {
                Content c = walker.next();
                if (c != null) {
                    switch (c.getCType()) {
                        case Comment:
                            printComment(out, fstack, (Comment) c);
                            continue;
                        case DocType:
                            printDocType(out, fstack, (DocType) c);
                            continue;
                        case Element:
                            printElement(out, fstack, nstack, (Element) c);
                            continue;
                        case ProcessingInstruction:
                            printProcessingInstruction(out, fstack, (ProcessingInstruction) c);
                            continue;
                        case Text:
                            String padding = ((Text) c).getText();
                            if (padding != null && Verifier.isAllXMLWhitespace(padding)) {
                                write(out, padding);
                                break;
                            }
                    }
                } else {
                    String padding2 = walker.text();
                    if (padding2 != null && Verifier.isAllXMLWhitespace(padding2) && !walker.isCDATA()) {
                        write(out, padding2);
                    }
                }
            }
            if (fstack.getLineSeparator() != null) {
                write(out, fstack.getLineSeparator());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void printDeclaration(Writer out, FormatStack fstack) throws IOException {
        if (!fstack.isOmitDeclaration()) {
            if (fstack.isOmitEncoding()) {
                write(out, "<?xml version=\"1.0\"?>");
            } else {
                write(out, "<?xml version=\"1.0\"");
                write(out, " encoding=\"");
                write(out, fstack.getEncoding());
                write(out, "\"?>");
            }
            write(out, fstack.getLineSeparator());
        }
    }

    /* access modifiers changed from: protected */
    public void printDocType(Writer out, FormatStack fstack, DocType docType) throws IOException {
        String publicID = docType.getPublicID();
        String systemID = docType.getSystemID();
        String internalSubset = docType.getInternalSubset();
        boolean hasPublic = false;
        write(out, "<!DOCTYPE ");
        write(out, docType.getElementName());
        if (publicID != null) {
            write(out, " PUBLIC \"");
            write(out, publicID);
            write(out, "\"");
            hasPublic = true;
        }
        if (systemID != null) {
            if (!hasPublic) {
                write(out, " SYSTEM");
            }
            write(out, " \"");
            write(out, systemID);
            write(out, "\"");
        }
        if (internalSubset != null && !internalSubset.equals("")) {
            write(out, " [");
            write(out, fstack.getLineSeparator());
            write(out, docType.getInternalSubset());
            write(out, "]");
        }
        write(out, Account.DEFAULT_QUOTE_PREFIX);
    }

    /* access modifiers changed from: protected */
    public void printProcessingInstruction(Writer out, FormatStack fstack, ProcessingInstruction pi) throws IOException {
        String target = pi.getTarget();
        boolean piProcessed = false;
        if (!fstack.isIgnoreTrAXEscapingPIs()) {
            if (target.equals("javax.xml.transform.disable-output-escaping")) {
                fstack.setEscapeOutput(false);
                piProcessed = true;
            } else if (target.equals("javax.xml.transform.enable-output-escaping")) {
                fstack.setEscapeOutput(true);
                piProcessed = true;
            }
        }
        if (!piProcessed) {
            String rawData = pi.getData();
            if (!"".equals(rawData)) {
                write(out, "<?");
                write(out, target);
                write(out, " ");
                write(out, rawData);
                write(out, "?>");
                return;
            }
            write(out, "<?");
            write(out, target);
            write(out, "?>");
        }
    }

    /* access modifiers changed from: protected */
    public void printComment(Writer out, FormatStack fstack, Comment comment) throws IOException {
        write(out, "<!--");
        write(out, comment.getText());
        write(out, "-->");
    }

    /* access modifiers changed from: protected */
    public void printEntityRef(Writer out, FormatStack fstack, EntityRef entity) throws IOException {
        textEntityRef(out, entity.getName());
    }

    /* access modifiers changed from: protected */
    public void printCDATA(Writer out, FormatStack fstack, CDATA cdata) throws IOException {
        textCDATA(out, cdata.getText());
    }

    /* access modifiers changed from: protected */
    public void printText(Writer out, FormatStack fstack, Text text) throws IOException {
        if (fstack.getEscapeOutput()) {
            textRaw(out, Format.escapeText(fstack.getEscapeStrategy(), fstack.getLineSeparator(), text.getText()));
        } else {
            textRaw(out, text.getText());
        }
    }

    /* access modifiers changed from: protected */
    public void printElement(Writer out, FormatStack fstack, NamespaceStack nstack, Element element) throws IOException {
        nstack.push(element);
        try {
            List<Content> content = element.getContent();
            write(out, "<");
            write(out, element.getQualifiedName());
            for (Namespace ns : nstack.addedForward()) {
                printNamespace(out, fstack, ns);
            }
            if (element.hasAttributes()) {
                for (Attribute attribute : element.getAttributes()) {
                    printAttribute(out, fstack, attribute);
                }
            }
            if (!content.isEmpty()) {
                fstack.push();
                String space = element.getAttributeValue("space", Namespace.XML_NAMESPACE);
                if ("default".equals(space)) {
                    fstack.setTextMode(fstack.getDefaultMode());
                } else if ("preserve".equals(space)) {
                    fstack.setTextMode(Format.TextMode.PRESERVE);
                }
                Walker walker = buildWalker(fstack, content, true);
                if (!walker.hasNext()) {
                    if (fstack.isExpandEmptyElements()) {
                        write(out, "></");
                        write(out, element.getQualifiedName());
                        write(out, Account.DEFAULT_QUOTE_PREFIX);
                    } else {
                        write(out, " />");
                    }
                    fstack.pop();
                } else {
                    write(out, Account.DEFAULT_QUOTE_PREFIX);
                    if (!walker.isAllText()) {
                        textRaw(out, fstack.getPadBetween());
                    }
                    printContent(out, fstack, nstack, walker);
                    if (!walker.isAllText()) {
                        textRaw(out, fstack.getPadLast());
                    }
                    write(out, "</");
                    write(out, element.getQualifiedName());
                    write(out, Account.DEFAULT_QUOTE_PREFIX);
                    fstack.pop();
                }
            } else if (fstack.isExpandEmptyElements()) {
                write(out, "></");
                write(out, element.getQualifiedName());
                write(out, Account.DEFAULT_QUOTE_PREFIX);
            } else {
                write(out, " />");
            }
            nstack.pop();
        } catch (Throwable th) {
            nstack.pop();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void printContent(Writer out, FormatStack fstack, NamespaceStack nstack, Walker walker) throws IOException {
        while (walker.hasNext()) {
            Content c = walker.next();
            if (c == null) {
                String t = walker.text();
                if (walker.isCDATA()) {
                    textCDATA(out, t);
                } else {
                    textRaw(out, t);
                }
            } else {
                switch (c.getCType()) {
                    case Comment:
                        printComment(out, fstack, (Comment) c);
                        continue;
                    case DocType:
                        printDocType(out, fstack, (DocType) c);
                        continue;
                    case Element:
                        printElement(out, fstack, nstack, (Element) c);
                        continue;
                    case ProcessingInstruction:
                        printProcessingInstruction(out, fstack, (ProcessingInstruction) c);
                        continue;
                    case Text:
                        printText(out, fstack, (Text) c);
                        continue;
                    case CDATA:
                        printCDATA(out, fstack, (CDATA) c);
                        continue;
                    case EntityRef:
                        printEntityRef(out, fstack, (EntityRef) c);
                        continue;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void printNamespace(Writer out, FormatStack fstack, Namespace ns) throws IOException {
        String prefix = ns.getPrefix();
        String uri = ns.getURI();
        write(out, " xmlns");
        if (!prefix.equals("")) {
            write(out, ":");
            write(out, prefix);
        }
        write(out, "=\"");
        attributeEscapedEntitiesFilter(out, fstack, uri);
        write(out, "\"");
    }

    /* access modifiers changed from: protected */
    public void printAttribute(Writer out, FormatStack fstack, Attribute attribute) throws IOException {
        if (attribute.isSpecified() || !fstack.isSpecifiedAttributesOnly()) {
            write(out, " ");
            write(out, attribute.getQualifiedName());
            write(out, "=");
            write(out, "\"");
            attributeEscapedEntitiesFilter(out, fstack, attribute.getValue());
            write(out, "\"");
        }
    }
}
