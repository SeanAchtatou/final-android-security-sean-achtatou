package org.jdom2.output;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import org.jdom2.IllegalDataException;
import org.jdom2.Verifier;

public class Format implements Cloneable {
    private static final EscapeStrategy Bits7EscapeStrategy = new EscapeStrategy7Bits();
    private static final EscapeStrategy Bits8EscapeStrategy = new EscapeStrategy8Bits();
    private static final EscapeStrategy DefaultEscapeStrategy = new EscapeStrategy() {
        public boolean shouldEscape(char ch) {
            if (Verifier.isHighSurrogate(ch)) {
                return true;
            }
            return false;
        }
    };
    private static final String STANDARD_ENCODING = "UTF-8";
    private static final String STANDARD_INDENT = "  ";
    private static final String STANDARD_LINE_SEPARATOR = LineSeparator.DEFAULT.value();
    private static final EscapeStrategy UTFEscapeStrategy = new EscapeStrategyUTF();
    String encoding = "UTF-8";
    EscapeStrategy escapeStrategy = DefaultEscapeStrategy;
    boolean expandEmptyElements = false;
    boolean ignoreTrAXEscapingPIs = false;
    String indent = null;
    String lineSeparator = STANDARD_LINE_SEPARATOR;
    TextMode mode = TextMode.PRESERVE;
    boolean omitDeclaration = false;
    boolean omitEncoding = false;
    boolean specifiedAttributesOnly = false;

    public enum TextMode {
        PRESERVE,
        TRIM,
        NORMALIZE,
        TRIM_FULL_WHITE
    }

    private static final class EscapeStrategyUTF implements EscapeStrategy {
        private EscapeStrategyUTF() {
        }

        public final boolean shouldEscape(char ch) {
            return Verifier.isHighSurrogate(ch);
        }
    }

    private static final class EscapeStrategy8Bits implements EscapeStrategy {
        private EscapeStrategy8Bits() {
        }

        public boolean shouldEscape(char ch) {
            return (ch >>> 8) != 0;
        }
    }

    private static final class EscapeStrategy7Bits implements EscapeStrategy {
        private EscapeStrategy7Bits() {
        }

        public boolean shouldEscape(char ch) {
            return (ch >>> 7) != 0;
        }
    }

    private static final class DefaultCharsetEscapeStrategy implements EscapeStrategy {
        private final CharsetEncoder encoder;

        public DefaultCharsetEscapeStrategy(CharsetEncoder cse) {
            this.encoder = cse;
        }

        public boolean shouldEscape(char ch) {
            if (!Verifier.isHighSurrogate(ch) && this.encoder.canEncode(ch)) {
                return false;
            }
            return true;
        }
    }

    public static Format getRawFormat() {
        return new Format();
    }

    public static Format getPrettyFormat() {
        Format f = new Format();
        f.setIndent(STANDARD_INDENT);
        f.setTextMode(TextMode.TRIM);
        return f;
    }

    public static Format getCompactFormat() {
        Format f = new Format();
        f.setTextMode(TextMode.NORMALIZE);
        return f;
    }

    public static final String compact(String str) {
        int right = str.length() - 1;
        int left = 0;
        while (left <= right && Verifier.isXMLWhitespace(str.charAt(left))) {
            left++;
        }
        while (right > left && Verifier.isXMLWhitespace(str.charAt(right))) {
            right--;
        }
        if (left > right) {
            return "";
        }
        boolean space = true;
        StringBuilder buffer = new StringBuilder((right - left) + 1);
        while (left <= right) {
            char c = str.charAt(left);
            if (!Verifier.isXMLWhitespace(c)) {
                buffer.append(c);
                space = true;
            } else if (space) {
                buffer.append(' ');
                space = false;
            }
            left++;
        }
        return buffer.toString();
    }

    public static final String trimRight(String str) {
        int right = str.length() - 1;
        while (right >= 0 && Verifier.isXMLWhitespace(str.charAt(right))) {
            right--;
        }
        if (right < 0) {
            return "";
        }
        return str.substring(0, right + 1);
    }

    public static final String trimLeft(String str) {
        int right = str.length();
        int left = 0;
        while (left < right && Verifier.isXMLWhitespace(str.charAt(left))) {
            left++;
        }
        if (left >= right) {
            return "";
        }
        return str.substring(left);
    }

    public static final String trimBoth(String str) {
        int right = str.length() - 1;
        while (right > 0 && Verifier.isXMLWhitespace(str.charAt(right))) {
            right--;
        }
        int left = 0;
        while (left <= right && Verifier.isXMLWhitespace(str.charAt(left))) {
            left++;
        }
        if (left > right) {
            return "";
        }
        return str.substring(left, right + 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder}
     arg types: [java.lang.String, int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(char[], int, int):java.lang.StringBuilder}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder} */
    public static final String escapeAttribute(EscapeStrategy strategy, String value) {
        int len = value.length();
        int idx = 0;
        while (idx < len) {
            char ch = value.charAt(idx);
            if (ch == '<' || ch == '>' || ch == '&' || ch == 13 || ch == 10 || ch == '\"' || ch == 9 || strategy.shouldEscape(ch)) {
                break;
            }
            idx++;
        }
        if (idx == len) {
            return value;
        }
        char highsurrogate = 0;
        StringBuilder sb = new StringBuilder(len + 5);
        sb.append((CharSequence) value, 0, idx);
        int idx2 = idx;
        while (idx2 < len) {
            int idx3 = idx2 + 1;
            char ch2 = value.charAt(idx2);
            if (highsurrogate <= 0) {
                switch (ch2) {
                    case 9:
                        sb.append("&#x9;");
                        break;
                    case 10:
                        sb.append("&#xA;");
                        break;
                    case 13:
                        sb.append("&#xD;");
                        break;
                    case '\"':
                        sb.append("&quot;");
                        break;
                    case '&':
                        sb.append("&amp;");
                        break;
                    case '<':
                        sb.append("&lt;");
                        break;
                    case '>':
                        sb.append("&gt;");
                        break;
                    default:
                        if (strategy.shouldEscape(ch2)) {
                            if (!Verifier.isHighSurrogate(ch2)) {
                                sb.append("&#x");
                                sb.append(Integer.toHexString(ch2));
                                sb.append(';');
                                break;
                            } else {
                                highsurrogate = ch2;
                                break;
                            }
                        } else {
                            sb.append(ch2);
                            break;
                        }
                }
                idx2 = idx3;
            } else if (!Verifier.isLowSurrogate(ch2)) {
                throw new IllegalDataException("Could not decode surrogate pair 0x" + Integer.toHexString(highsurrogate) + " / 0x" + Integer.toHexString(ch2));
            } else {
                int chp = Verifier.decodeSurrogatePair(highsurrogate, ch2);
                sb.append("&#x");
                sb.append(Integer.toHexString(chp));
                sb.append(';');
                highsurrogate = 0;
                idx2 = idx3;
            }
        }
        if (highsurrogate <= 0) {
            return sb.toString();
        }
        throw new IllegalDataException("Surrogate pair 0x" + Integer.toHexString(highsurrogate) + "truncated");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder}
     arg types: [java.lang.String, int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(char[], int, int):java.lang.StringBuilder}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder} */
    public static final String escapeText(EscapeStrategy strategy, String eol, String value) {
        int right = value.length();
        int idx = 0;
        while (idx < right) {
            char ch = value.charAt(idx);
            if (ch == '<' || ch == '>' || ch == '&' || ch == 13 || ch == 10 || strategy.shouldEscape(ch)) {
                break;
            }
            idx++;
        }
        if (idx == right) {
            return value;
        }
        StringBuilder sb = new StringBuilder();
        if (idx > 0) {
            sb.append((CharSequence) value, 0, idx);
        }
        char highsurrogate = 0;
        int idx2 = idx;
        while (idx2 < right) {
            int idx3 = idx2 + 1;
            char ch2 = value.charAt(idx2);
            if (highsurrogate <= 0) {
                switch (ch2) {
                    case 10:
                        if (eol == null) {
                            sb.append(10);
                            break;
                        } else {
                            sb.append(eol);
                            break;
                        }
                    case 13:
                        sb.append("&#xD;");
                        break;
                    case '&':
                        sb.append("&amp;");
                        break;
                    case '<':
                        sb.append("&lt;");
                        break;
                    case '>':
                        sb.append("&gt;");
                        break;
                    default:
                        if (strategy.shouldEscape(ch2)) {
                            if (!Verifier.isHighSurrogate(ch2)) {
                                sb.append("&#x" + Integer.toHexString(ch2) + ";");
                                break;
                            } else {
                                highsurrogate = ch2;
                                break;
                            }
                        } else {
                            sb.append(ch2);
                            break;
                        }
                }
                idx2 = idx3;
            } else if (!Verifier.isLowSurrogate(ch2)) {
                throw new IllegalDataException("Could not decode surrogate pair 0x" + Integer.toHexString(highsurrogate) + " / 0x" + Integer.toHexString(ch2));
            } else {
                sb.append("&#x" + Integer.toHexString(Verifier.decodeSurrogatePair(highsurrogate, ch2)) + ";");
                highsurrogate = 0;
                idx2 = idx3;
            }
        }
        if (highsurrogate <= 0) {
            return sb.toString();
        }
        throw new IllegalDataException("Surrogate pair 0x" + Integer.toHexString(highsurrogate) + "truncated");
    }

    private static final EscapeStrategy chooseStrategy(String encoding2) {
        if ("UTF-8".equalsIgnoreCase(encoding2) || "UTF-16".equalsIgnoreCase(encoding2)) {
            return UTFEscapeStrategy;
        }
        if (encoding2.toUpperCase().startsWith("ISO-8859-") || "Latin1".equalsIgnoreCase(encoding2)) {
            return Bits8EscapeStrategy;
        }
        if ("US-ASCII".equalsIgnoreCase(encoding2) || "ASCII".equalsIgnoreCase(encoding2)) {
            return Bits7EscapeStrategy;
        }
        try {
            return new DefaultCharsetEscapeStrategy(Charset.forName(encoding2).newEncoder());
        } catch (Exception e) {
            return DefaultEscapeStrategy;
        }
    }

    private Format() {
        setEncoding("UTF-8");
    }

    public Format setEscapeStrategy(EscapeStrategy strategy) {
        this.escapeStrategy = strategy;
        return this;
    }

    public EscapeStrategy getEscapeStrategy() {
        return this.escapeStrategy;
    }

    public Format setLineSeparator(String separator) {
        if ("".equals(separator)) {
            separator = null;
        }
        this.lineSeparator = separator;
        return this;
    }

    public Format setLineSeparator(LineSeparator separator) {
        return setLineSeparator(separator == null ? STANDARD_LINE_SEPARATOR : separator.value());
    }

    public String getLineSeparator() {
        return this.lineSeparator;
    }

    public Format setOmitEncoding(boolean omitEncoding2) {
        this.omitEncoding = omitEncoding2;
        return this;
    }

    public boolean getOmitEncoding() {
        return this.omitEncoding;
    }

    public Format setOmitDeclaration(boolean omitDeclaration2) {
        this.omitDeclaration = omitDeclaration2;
        return this;
    }

    public boolean getOmitDeclaration() {
        return this.omitDeclaration;
    }

    public Format setExpandEmptyElements(boolean expandEmptyElements2) {
        this.expandEmptyElements = expandEmptyElements2;
        return this;
    }

    public boolean getExpandEmptyElements() {
        return this.expandEmptyElements;
    }

    public void setIgnoreTrAXEscapingPIs(boolean ignoreTrAXEscapingPIs2) {
        this.ignoreTrAXEscapingPIs = ignoreTrAXEscapingPIs2;
    }

    public boolean getIgnoreTrAXEscapingPIs() {
        return this.ignoreTrAXEscapingPIs;
    }

    public Format setTextMode(TextMode mode2) {
        this.mode = mode2;
        return this;
    }

    public TextMode getTextMode() {
        return this.mode;
    }

    public Format setIndent(String indent2) {
        this.indent = indent2;
        return this;
    }

    public String getIndent() {
        return this.indent;
    }

    public Format setEncoding(String encoding2) {
        this.encoding = encoding2;
        this.escapeStrategy = chooseStrategy(encoding2);
        return this;
    }

    public String getEncoding() {
        return this.encoding;
    }

    public boolean isSpecifiedAttributesOnly() {
        return this.specifiedAttributesOnly;
    }

    public void setSpecifiedAttributesOnly(boolean specifiedAttributesOnly2) {
        this.specifiedAttributesOnly = specifiedAttributesOnly2;
    }

    public Format clone() {
        try {
            return (Format) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
