package org.jdom2;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import org.jdom2.filter.Filter;
import org.jdom2.internal.ArrayCopy;

final class ContentList extends AbstractList<Content> implements RandomAccess {
    private static final int INITIAL_ARRAY_SIZE = 4;
    private transient int dataModiCount = Integer.MIN_VALUE;
    /* access modifiers changed from: private */
    public Content[] elementData = null;
    private final Parent parent;
    /* access modifiers changed from: private */
    public int size;
    private transient int sizeModCount = Integer.MIN_VALUE;

    ContentList(Parent parent2) {
        this.parent = parent2;
    }

    /* access modifiers changed from: package-private */
    public final void uncheckedAddContent(Content c) {
        c.parent = this.parent;
        ensureCapacity(this.size + 1);
        Content[] contentArr = this.elementData;
        int i = this.size;
        this.size = i + 1;
        contentArr[i] = c;
        incModCount();
    }

    /* access modifiers changed from: private */
    public final void setModCount(int sizemod, int datamod) {
        this.sizeModCount = sizemod;
        this.dataModiCount = datamod;
    }

    /* access modifiers changed from: private */
    public final int getModCount() {
        return this.sizeModCount;
    }

    private final void incModCount() {
        this.dataModiCount++;
        this.sizeModCount++;
    }

    private final void incDataModOnly() {
        this.dataModiCount++;
    }

    /* access modifiers changed from: private */
    public final int getDataModCount() {
        return this.dataModiCount;
    }

    /* access modifiers changed from: private */
    public final void checkIndex(int index, boolean excludes) {
        int max = excludes ? this.size - 1 : this.size;
        if (index < 0 || index > max) {
            throw new IndexOutOfBoundsException("Index: " + index + " Size: " + this.size);
        }
    }

    private final void checkPreConditions(Content child, int index, boolean replace) {
        if (child == null) {
            throw new NullPointerException("Cannot add null object");
        }
        checkIndex(index, replace);
        if (child.getParent() != null) {
            Parent p = child.getParent();
            if (p instanceof Document) {
                throw new IllegalAddException((Element) child, "The Content already has an existing parent document");
            }
            throw new IllegalAddException("The Content already has an existing parent \"" + ((Element) p).getQualifiedName() + "\"");
        } else if (child == this.parent) {
            throw new IllegalAddException("The Element cannot be added to itself");
        } else if ((this.parent instanceof Element) && (child instanceof Element) && ((Element) child).isAncestor((Element) this.parent)) {
            throw new IllegalAddException("The Element cannot be added as a descendent of itself");
        }
    }

    public void add(int index, Content child) {
        checkPreConditions(child, index, false);
        this.parent.canContainContent(child, index, false);
        child.setParent(this.parent);
        ensureCapacity(this.size + 1);
        if (index == this.size) {
            Content[] contentArr = this.elementData;
            int i = this.size;
            this.size = i + 1;
            contentArr[i] = child;
        } else {
            System.arraycopy(this.elementData, index, this.elementData, index + 1, this.size - index);
            this.elementData[index] = child;
            this.size++;
        }
        incModCount();
    }

    public boolean addAll(Collection<? extends Content> collection) {
        return addAll(this.size, collection);
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public boolean addAll(int r11, java.util.Collection<? extends org.jdom2.Content> r12) {
        /*
            r10 = this;
            r7 = 0
            r8 = 1
            if (r12 != 0) goto L_0x000c
            java.lang.NullPointerException r7 = new java.lang.NullPointerException
            java.lang.String r8 = "Can not add a null collection to the ContentList"
            r7.<init>(r8)
            throw r7
        L_0x000c:
            r10.checkIndex(r11, r7)
            boolean r9 = r12.isEmpty()
            if (r9 == 0) goto L_0x0016
        L_0x0015:
            return r7
        L_0x0016:
            int r0 = r12.size()
            if (r0 != r8) goto L_0x002b
            java.util.Iterator r7 = r12.iterator()
            java.lang.Object r7 = r7.next()
            org.jdom2.Content r7 = (org.jdom2.Content) r7
            r10.add(r11, r7)
            r7 = r8
            goto L_0x0015
        L_0x002b:
            int r7 = r10.size()
            int r7 = r7 + r0
            r10.ensureCapacity(r7)
            int r6 = r10.getModCount()
            int r5 = r10.getDataModCount()
            r4 = 0
            r2 = 0
            java.util.Iterator r3 = r12.iterator()     // Catch:{ all -> 0x0067 }
        L_0x0041:
            boolean r7 = r3.hasNext()     // Catch:{ all -> 0x0067 }
            if (r7 == 0) goto L_0x0055
            java.lang.Object r1 = r3.next()     // Catch:{ all -> 0x0067 }
            org.jdom2.Content r1 = (org.jdom2.Content) r1     // Catch:{ all -> 0x0067 }
            int r7 = r11 + r2
            r10.add(r7, r1)     // Catch:{ all -> 0x0067 }
            int r2 = r2 + 1
            goto L_0x0041
        L_0x0055:
            r4 = 1
            if (r4 != 0) goto L_0x0065
        L_0x0058:
            int r2 = r2 + -1
            if (r2 < 0) goto L_0x0062
            int r7 = r11 + r2
            r10.remove(r7)
            goto L_0x0058
        L_0x0062:
            r10.setModCount(r6, r5)
        L_0x0065:
            r7 = r8
            goto L_0x0015
        L_0x0067:
            r7 = move-exception
            if (r4 != 0) goto L_0x0077
        L_0x006a:
            int r2 = r2 + -1
            if (r2 < 0) goto L_0x0074
            int r8 = r11 + r2
            r10.remove(r8)
            goto L_0x006a
        L_0x0074:
            r10.setModCount(r6, r5)
        L_0x0077:
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jdom2.ContentList.addAll(int, java.util.Collection):boolean");
    }

    public void clear() {
        if (this.elementData != null) {
            for (int i = 0; i < this.size; i++) {
                removeParent(this.elementData[i]);
            }
            this.elementData = null;
            this.size = 0;
        }
        incModCount();
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    void clearAndSet(java.util.Collection<? extends org.jdom2.Content> r10) {
        /*
            r9 = this;
            r7 = 0
            r6 = 0
            if (r10 == 0) goto L_0x000a
            boolean r5 = r10.isEmpty()
            if (r5 == 0) goto L_0x000e
        L_0x000a:
            r9.clear()
        L_0x000d:
            return
        L_0x000e:
            org.jdom2.Content[] r1 = r9.elementData
            int r4 = r9.size
            int r3 = r9.getModCount()
            int r2 = r9.getDataModCount()
        L_0x001a:
            int r5 = r9.size
            if (r5 <= 0) goto L_0x002a
            int r5 = r9.size
            int r5 = r5 + -1
            r9.size = r5
            r5 = r1[r5]
            r5.setParent(r7)
            goto L_0x001a
        L_0x002a:
            r9.size = r6
            r9.elementData = r7
            r0 = 0
            r5 = 0
            r9.addAll(r5, r10)     // Catch:{ all -> 0x0050 }
            r0 = 1
            if (r0 != 0) goto L_0x000d
            r9.elementData = r1
        L_0x0038:
            int r5 = r9.size
            if (r5 >= r4) goto L_0x004c
            org.jdom2.Content[] r5 = r9.elementData
            int r6 = r9.size
            int r7 = r6 + 1
            r9.size = r7
            r5 = r5[r6]
            org.jdom2.Parent r6 = r9.parent
            r5.setParent(r6)
            goto L_0x0038
        L_0x004c:
            r9.setModCount(r3, r2)
            goto L_0x000d
        L_0x0050:
            r5 = move-exception
            if (r0 != 0) goto L_0x006c
            r9.elementData = r1
        L_0x0055:
            int r6 = r9.size
            if (r6 >= r4) goto L_0x0069
            org.jdom2.Content[] r6 = r9.elementData
            int r7 = r9.size
            int r8 = r7 + 1
            r9.size = r8
            r6 = r6[r7]
            org.jdom2.Parent r7 = r9.parent
            r6.setParent(r7)
            goto L_0x0055
        L_0x0069:
            r9.setModCount(r3, r2)
        L_0x006c:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jdom2.ContentList.clearAndSet(java.util.Collection):void");
    }

    /* access modifiers changed from: package-private */
    public void ensureCapacity(int minCapacity) {
        if (this.elementData == null) {
            this.elementData = new Content[Math.max(minCapacity, 4)];
        } else if (minCapacity >= this.elementData.length) {
            int newcap = ((this.size * 3) / 2) + 1;
            Content[] contentArr = this.elementData;
            if (newcap >= minCapacity) {
                minCapacity = newcap;
            }
            this.elementData = (Content[]) ArrayCopy.copyOf(contentArr, minCapacity);
        }
    }

    public Content get(int index) {
        checkIndex(index, true);
        return this.elementData[index];
    }

    /* access modifiers changed from: package-private */
    public <E extends Content> List<E> getView(Filter<E> filter) {
        return new FilterList(filter);
    }

    /* access modifiers changed from: package-private */
    public int indexOfFirstElement() {
        if (this.elementData != null) {
            for (int i = 0; i < this.size; i++) {
                if (this.elementData[i] instanceof Element) {
                    return i;
                }
            }
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public int indexOfDocType() {
        if (this.elementData != null) {
            for (int i = 0; i < this.size; i++) {
                if (this.elementData[i] instanceof DocType) {
                    return i;
                }
            }
        }
        return -1;
    }

    public Content remove(int index) {
        checkIndex(index, true);
        Content old = this.elementData[index];
        removeParent(old);
        System.arraycopy(this.elementData, index + 1, this.elementData, index, (this.size - index) - 1);
        Content[] contentArr = this.elementData;
        int i = this.size - 1;
        this.size = i;
        contentArr[i] = null;
        incModCount();
        return old;
    }

    private static void removeParent(Content c) {
        c.setParent(null);
    }

    public Content set(int index, Content child) {
        checkPreConditions(child, index, true);
        this.parent.canContainContent(child, index, true);
        Content old = this.elementData[index];
        removeParent(old);
        child.setParent(this.parent);
        this.elementData[index] = child;
        incDataModOnly();
        return old;
    }

    public int size() {
        return this.size;
    }

    public Iterator<Content> iterator() {
        return new CLIterator();
    }

    public ListIterator<Content> listIterator() {
        return new CLListIterator(0);
    }

    public ListIterator<Content> listIterator(int start) {
        return new CLListIterator(start);
    }

    public String toString() {
        return super.toString();
    }

    /* access modifiers changed from: private */
    public void sortInPlace(int[] indexes) {
        int[] unsorted = ArrayCopy.copyOf(indexes, indexes.length);
        Arrays.sort(unsorted);
        Content[] usc = new Content[unsorted.length];
        for (int i = 0; i < usc.length; i++) {
            usc[i] = this.elementData[indexes[i]];
        }
        for (int i2 = 0; i2 < indexes.length; i2++) {
            this.elementData[unsorted[i2]] = usc[i2];
        }
    }

    private final int binarySearch(int[] indexes, int len, int val, Comparator<? super Content> comp) {
        int left = 0;
        int right = len - 1;
        Content base = this.elementData[val];
        while (left <= right) {
            int mid = (left + right) >>> 1;
            int cmp = comp.compare(base, this.elementData[indexes[mid]]);
            if (cmp == 0) {
                while (cmp == 0 && mid < right && comp.compare(base, this.elementData[indexes[mid + 1]]) == 0) {
                    mid++;
                }
                return mid + 1;
            } else if (cmp < 0) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return left;
    }

    /* access modifiers changed from: package-private */
    public final void sort(Comparator<? super Content> comp) {
        int sz = this.size;
        int[] indexes = new int[sz];
        for (int i = 0; i < sz; i++) {
            int ip = binarySearch(indexes, i, i, comp);
            if (ip < i) {
                System.arraycopy(indexes, ip, indexes, ip + 1, i - ip);
            }
            indexes[ip] = i;
        }
        sortInPlace(indexes);
    }

    private final class CLIterator implements Iterator<Content> {
        private boolean canremove;
        private int cursor;
        private int expect;

        private CLIterator() {
            this.expect = -1;
            this.cursor = 0;
            this.canremove = false;
            this.expect = ContentList.this.getModCount();
        }

        public boolean hasNext() {
            return this.cursor < ContentList.this.size;
        }

        public Content next() {
            if (ContentList.this.getModCount() != this.expect) {
                throw new ConcurrentModificationException("ContentList was modified outside of this Iterator");
            } else if (this.cursor >= ContentList.this.size) {
                throw new NoSuchElementException("Iterated beyond the end of the ContentList.");
            } else {
                this.canremove = true;
                Content[] access$300 = ContentList.this.elementData;
                int i = this.cursor;
                this.cursor = i + 1;
                return access$300[i];
            }
        }

        public void remove() {
            if (ContentList.this.getModCount() != this.expect) {
                throw new ConcurrentModificationException("ContentList was modified outside of this Iterator");
            } else if (!this.canremove) {
                throw new IllegalStateException("Can only remove() content after a call to next()");
            } else {
                this.canremove = false;
                ContentList contentList = ContentList.this;
                int i = this.cursor - 1;
                this.cursor = i;
                contentList.remove(i);
                this.expect = ContentList.this.getModCount();
            }
        }
    }

    private final class CLListIterator implements ListIterator<Content> {
        private boolean canremove = false;
        private boolean canset = false;
        private int cursor = -1;
        private int expectedmod = -1;
        private boolean forward = false;

        CLListIterator(int start) {
            this.expectedmod = ContentList.this.getModCount();
            this.forward = false;
            ContentList.this.checkIndex(start, false);
            this.cursor = start;
        }

        private void checkConcurrent() {
            if (this.expectedmod != ContentList.this.getModCount()) {
                throw new ConcurrentModificationException("The ContentList supporting this iterator has been modified bysomething other than this Iterator.");
            }
        }

        public boolean hasNext() {
            return (this.forward ? this.cursor + 1 : this.cursor) < ContentList.this.size;
        }

        public boolean hasPrevious() {
            return (this.forward ? this.cursor : this.cursor + -1) >= 0;
        }

        public int nextIndex() {
            return this.forward ? this.cursor + 1 : this.cursor;
        }

        public int previousIndex() {
            return this.forward ? this.cursor : this.cursor - 1;
        }

        public Content next() {
            checkConcurrent();
            int next = this.forward ? this.cursor + 1 : this.cursor;
            if (next >= ContentList.this.size) {
                throw new NoSuchElementException("next() is beyond the end of the Iterator");
            }
            this.cursor = next;
            this.forward = true;
            this.canremove = true;
            this.canset = true;
            return ContentList.this.elementData[this.cursor];
        }

        public Content previous() {
            checkConcurrent();
            int prev = this.forward ? this.cursor : this.cursor - 1;
            if (prev < 0) {
                throw new NoSuchElementException("previous() is beyond the beginning of the Iterator");
            }
            this.cursor = prev;
            this.forward = false;
            this.canremove = true;
            this.canset = true;
            return ContentList.this.elementData[this.cursor];
        }

        public void add(Content obj) {
            checkConcurrent();
            int next = this.forward ? this.cursor + 1 : this.cursor;
            ContentList.this.add(next, obj);
            this.expectedmod = ContentList.this.getModCount();
            this.canset = false;
            this.canremove = false;
            this.cursor = next;
            this.forward = true;
        }

        public void remove() {
            checkConcurrent();
            if (!this.canremove) {
                throw new IllegalStateException("Can not remove an element unless either next() or previous() has been called since the last remove()");
            }
            ContentList.this.remove(this.cursor);
            this.forward = false;
            this.expectedmod = ContentList.this.getModCount();
            this.canremove = false;
            this.canset = false;
        }

        public void set(Content obj) {
            checkConcurrent();
            if (!this.canset) {
                throw new IllegalStateException("Can not set an element unless either next() or previous() has been called since the last remove() or set()");
            }
            ContentList.this.set(this.cursor, obj);
            this.expectedmod = ContentList.this.getModCount();
        }
    }

    class FilterList<F extends Content> extends AbstractList<F> {
        int[] backingpos = new int[(ContentList.this.size + 4)];
        int backingsize = 0;
        final Filter<F> filter;
        int xdata = -1;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.jdom2.ContentList.FilterList.set(int, org.jdom2.Content):F
         arg types: [int, java.lang.Object]
         candidates:
          org.jdom2.ContentList.FilterList.set(int, java.lang.Object):java.lang.Object
          ClspMth{java.util.AbstractList.set(int, java.lang.Object):E}
          ClspMth{java.util.List.set(int, java.lang.Object):E}
          org.jdom2.ContentList.FilterList.set(int, org.jdom2.Content):F */
        public /* bridge */ /* synthetic */ Object set(int x0, Object x1) {
            return set(x0, (Content) ((Content) x1));
        }

        FilterList(Filter<F> filter2) {
            this.filter = filter2;
        }

        public boolean isEmpty() {
            return resync(0) == ContentList.this.size;
        }

        /* access modifiers changed from: private */
        public final int resync(int index) {
            if (this.xdata != ContentList.this.getDataModCount()) {
                this.xdata = ContentList.this.getDataModCount();
                this.backingsize = 0;
                if (ContentList.this.size >= this.backingpos.length) {
                    this.backingpos = new int[(ContentList.this.size + 1)];
                }
            }
            if (index >= 0 && index < this.backingsize) {
                return this.backingpos[index];
            }
            int bpi = 0;
            if (this.backingsize > 0) {
                bpi = this.backingpos[this.backingsize - 1] + 1;
            }
            while (bpi < ContentList.this.size) {
                if (((Content) this.filter.filter(ContentList.this.elementData[bpi])) != null) {
                    this.backingpos[this.backingsize] = bpi;
                    int i = this.backingsize;
                    this.backingsize = i + 1;
                    if (i == index) {
                        return bpi;
                    }
                }
                bpi++;
            }
            return ContentList.this.size;
        }

        public void add(int index, Content obj) {
            if (index < 0) {
                throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
            }
            int adj = resync(index);
            if (adj == ContentList.this.size && index > size()) {
                throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
            } else if (this.filter.matches(obj)) {
                ContentList.this.add(adj, obj);
                if (this.backingpos.length <= ContentList.this.size) {
                    this.backingpos = ArrayCopy.copyOf(this.backingpos, this.backingpos.length + 1);
                }
                this.backingpos[index] = adj;
                this.backingsize = index + 1;
                this.xdata = ContentList.this.getDataModCount();
            } else {
                throw new IllegalAddException("Filter won't allow the " + obj.getClass().getName() + " '" + obj + "' to be added to the list");
            }
        }

        /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
            */
        public boolean addAll(int r12, java.util.Collection<? extends F> r13) {
            /*
                r11 = this;
                if (r13 != 0) goto L_0x000a
                java.lang.NullPointerException r8 = new java.lang.NullPointerException
                java.lang.String r9 = "Cannot add a null collection"
                r8.<init>(r9)
                throw r8
            L_0x000a:
                if (r12 >= 0) goto L_0x0033
                java.lang.IndexOutOfBoundsException r8 = new java.lang.IndexOutOfBoundsException
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                java.lang.String r10 = "Index: "
                java.lang.StringBuilder r9 = r9.append(r10)
                java.lang.StringBuilder r9 = r9.append(r12)
                java.lang.String r10 = " Size: "
                java.lang.StringBuilder r9 = r9.append(r10)
                int r10 = r11.size()
                java.lang.StringBuilder r9 = r9.append(r10)
                java.lang.String r9 = r9.toString()
                r8.<init>(r9)
                throw r8
            L_0x0033:
                int r1 = r11.resync(r12)
                org.jdom2.ContentList r8 = org.jdom2.ContentList.this
                int r8 = r8.size
                if (r1 != r8) goto L_0x006c
                int r8 = r11.size()
                if (r12 <= r8) goto L_0x006c
                java.lang.IndexOutOfBoundsException r8 = new java.lang.IndexOutOfBoundsException
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                java.lang.String r10 = "Index: "
                java.lang.StringBuilder r9 = r9.append(r10)
                java.lang.StringBuilder r9 = r9.append(r12)
                java.lang.String r10 = " Size: "
                java.lang.StringBuilder r9 = r9.append(r10)
                int r10 = r11.size()
                java.lang.StringBuilder r9 = r9.append(r10)
                java.lang.String r9 = r9.toString()
                r8.<init>(r9)
                throw r8
            L_0x006c:
                int r0 = r13.size()
                if (r0 != 0) goto L_0x0074
                r8 = 0
            L_0x0073:
                return r8
            L_0x0074:
                org.jdom2.ContentList r8 = org.jdom2.ContentList.this
                org.jdom2.ContentList r9 = org.jdom2.ContentList.this
                int r9 = r9.size()
                int r9 = r9 + r0
                r8.ensureCapacity(r9)
                org.jdom2.ContentList r8 = org.jdom2.ContentList.this
                int r7 = r8.getModCount()
                org.jdom2.ContentList r8 = org.jdom2.ContentList.this
                int r6 = r8.getDataModCount()
                r5 = 0
                r3 = 0
                java.util.Iterator r4 = r13.iterator()     // Catch:{ all -> 0x00a8 }
            L_0x0092:
                boolean r8 = r4.hasNext()     // Catch:{ all -> 0x00a8 }
                if (r8 == 0) goto L_0x0127
                java.lang.Object r2 = r4.next()     // Catch:{ all -> 0x00a8 }
                org.jdom2.Content r2 = (org.jdom2.Content) r2     // Catch:{ all -> 0x00a8 }
                if (r2 != 0) goto L_0x00b7
                java.lang.NullPointerException r8 = new java.lang.NullPointerException     // Catch:{ all -> 0x00a8 }
                java.lang.String r9 = "Cannot add null content"
                r8.<init>(r9)     // Catch:{ all -> 0x00a8 }
                throw r8     // Catch:{ all -> 0x00a8 }
            L_0x00a8:
                r8 = move-exception
                if (r5 != 0) goto L_0x014b
            L_0x00ab:
                int r3 = r3 + -1
                if (r3 < 0) goto L_0x0142
                org.jdom2.ContentList r9 = org.jdom2.ContentList.this
                int r10 = r1 + r3
                r9.remove(r10)
                goto L_0x00ab
            L_0x00b7:
                org.jdom2.filter.Filter<F> r8 = r11.filter     // Catch:{ all -> 0x00a8 }
                boolean r8 = r8.matches(r2)     // Catch:{ all -> 0x00a8 }
                if (r8 == 0) goto L_0x00f6
                org.jdom2.ContentList r8 = org.jdom2.ContentList.this     // Catch:{ all -> 0x00a8 }
                int r9 = r1 + r3
                r8.add(r9, r2)     // Catch:{ all -> 0x00a8 }
                int[] r8 = r11.backingpos     // Catch:{ all -> 0x00a8 }
                int r8 = r8.length     // Catch:{ all -> 0x00a8 }
                org.jdom2.ContentList r9 = org.jdom2.ContentList.this     // Catch:{ all -> 0x00a8 }
                int r9 = r9.size     // Catch:{ all -> 0x00a8 }
                if (r8 > r9) goto L_0x00dd
                int[] r8 = r11.backingpos     // Catch:{ all -> 0x00a8 }
                int[] r9 = r11.backingpos     // Catch:{ all -> 0x00a8 }
                int r9 = r9.length     // Catch:{ all -> 0x00a8 }
                int r9 = r9 + r0
                int[] r8 = org.jdom2.internal.ArrayCopy.copyOf(r8, r9)     // Catch:{ all -> 0x00a8 }
                r11.backingpos = r8     // Catch:{ all -> 0x00a8 }
            L_0x00dd:
                int[] r8 = r11.backingpos     // Catch:{ all -> 0x00a8 }
                int r9 = r12 + r3
                int r10 = r1 + r3
                r8[r9] = r10     // Catch:{ all -> 0x00a8 }
                int r8 = r12 + r3
                int r8 = r8 + 1
                r11.backingsize = r8     // Catch:{ all -> 0x00a8 }
                org.jdom2.ContentList r8 = org.jdom2.ContentList.this     // Catch:{ all -> 0x00a8 }
                int r8 = r8.getDataModCount()     // Catch:{ all -> 0x00a8 }
                r11.xdata = r8     // Catch:{ all -> 0x00a8 }
                int r3 = r3 + 1
                goto L_0x0092
            L_0x00f6:
                org.jdom2.IllegalAddException r8 = new org.jdom2.IllegalAddException     // Catch:{ all -> 0x00a8 }
                java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a8 }
                r9.<init>()     // Catch:{ all -> 0x00a8 }
                java.lang.String r10 = "Filter won't allow the "
                java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x00a8 }
                java.lang.Class r10 = r2.getClass()     // Catch:{ all -> 0x00a8 }
                java.lang.String r10 = r10.getName()     // Catch:{ all -> 0x00a8 }
                java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x00a8 }
                java.lang.String r10 = " '"
                java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x00a8 }
                java.lang.StringBuilder r9 = r9.append(r2)     // Catch:{ all -> 0x00a8 }
                java.lang.String r10 = "' to be added to the list"
                java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ all -> 0x00a8 }
                java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x00a8 }
                r8.<init>(r9)     // Catch:{ all -> 0x00a8 }
                throw r8     // Catch:{ all -> 0x00a8 }
            L_0x0127:
                r5 = 1
                if (r5 != 0) goto L_0x013f
            L_0x012a:
                int r3 = r3 + -1
                if (r3 < 0) goto L_0x0136
                org.jdom2.ContentList r8 = org.jdom2.ContentList.this
                int r9 = r1 + r3
                r8.remove(r9)
                goto L_0x012a
            L_0x0136:
                org.jdom2.ContentList r8 = org.jdom2.ContentList.this
                r8.setModCount(r7, r6)
                r11.backingsize = r12
                r11.xdata = r7
            L_0x013f:
                r8 = 1
                goto L_0x0073
            L_0x0142:
                org.jdom2.ContentList r9 = org.jdom2.ContentList.this
                r9.setModCount(r7, r6)
                r11.backingsize = r12
                r11.xdata = r7
            L_0x014b:
                throw r8
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jdom2.ContentList.FilterList.addAll(int, java.util.Collection):boolean");
        }

        public F get(int index) {
            if (index < 0) {
                throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
            }
            int adj = resync(index);
            if (adj != ContentList.this.size) {
                return (Content) this.filter.filter(ContentList.this.get(adj));
            }
            throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
        }

        public Iterator<F> iterator() {
            return new FilterListIterator(this, 0);
        }

        public ListIterator<F> listIterator() {
            return new FilterListIterator(this, 0);
        }

        public ListIterator<F> listIterator(int index) {
            return new FilterListIterator(this, index);
        }

        public F remove(int index) {
            if (index < 0) {
                throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
            }
            int adj = resync(index);
            if (adj == ContentList.this.size) {
                throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
            }
            Content oldc = ContentList.this.remove(adj);
            this.backingsize = index;
            this.xdata = ContentList.this.getDataModCount();
            return (Content) this.filter.filter(oldc);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.jdom2.ContentList.set(int, org.jdom2.Content):org.jdom2.Content
         arg types: [int, F]
         candidates:
          org.jdom2.ContentList.set(int, java.lang.Object):java.lang.Object
          ClspMth{java.util.AbstractList.set(int, java.lang.Object):E}
          ClspMth{java.util.List.set(int, java.lang.Object):E}
          org.jdom2.ContentList.set(int, org.jdom2.Content):org.jdom2.Content */
        public F set(int index, F obj) {
            if (index < 0) {
                throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
            }
            int adj = resync(index);
            if (adj == ContentList.this.size) {
                throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
            }
            F ins = (Content) this.filter.filter((Object) obj);
            if (ins != null) {
                F oldc = (Content) this.filter.filter(ContentList.this.set(adj, (Content) ins));
                this.xdata = ContentList.this.getDataModCount();
                return oldc;
            }
            throw new IllegalAddException("Filter won't allow index " + index + " to be set to " + obj.getClass().getName());
        }

        public int size() {
            resync(-1);
            return this.backingsize;
        }

        private final int fbinarySearch(int[] indexes, int len, int val, Comparator<? super F> comp) {
            int left = 0;
            int right = len - 1;
            F base = ContentList.this.elementData[this.backingpos[val]];
            while (left <= right) {
                int mid = (left + right) >>> 1;
                int cmp = comp.compare(base, ContentList.this.elementData[indexes[mid]]);
                if (cmp == 0) {
                    while (cmp == 0 && mid < right && comp.compare(base, ContentList.this.elementData[indexes[mid + 1]]) == 0) {
                        mid++;
                    }
                    return mid + 1;
                } else if (cmp < 0) {
                    right = mid - 1;
                } else {
                    left = mid + 1;
                }
            }
            return left;
        }

        /* access modifiers changed from: package-private */
        public final void sort(Comparator<? super F> comp) {
            int sz = size();
            int[] indexes = new int[sz];
            for (int i = 0; i < sz; i++) {
                int ip = fbinarySearch(indexes, i, i, comp);
                if (ip < i) {
                    System.arraycopy(indexes, ip, indexes, ip + 1, i - ip);
                }
                indexes[ip] = this.backingpos[i];
            }
            ContentList.this.sortInPlace(indexes);
        }
    }

    final class FilterListIterator<F extends Content> implements ListIterator<F> {
        private boolean canremove = false;
        private boolean canset = false;
        private int cursor = -1;
        private int expectedmod = -1;
        private final FilterList<F> filterlist;
        private boolean forward = false;

        public /* bridge */ /* synthetic */ void set(Object x0) {
            set((Content) ((Content) x0));
        }

        FilterListIterator(FilterList<F> flist, int start) {
            this.filterlist = flist;
            this.expectedmod = ContentList.this.getModCount();
            this.forward = false;
            if (start < 0) {
                throw new IndexOutOfBoundsException("Index: " + start + " Size: " + this.filterlist.size());
            } else if (this.filterlist.resync(start) != ContentList.this.size || start <= this.filterlist.size()) {
                this.cursor = start;
            } else {
                throw new IndexOutOfBoundsException("Index: " + start + " Size: " + this.filterlist.size());
            }
        }

        private void checkConcurrent() {
            if (this.expectedmod != ContentList.this.getModCount()) {
                throw new ConcurrentModificationException("The ContentList supporting the FilterList this iterator is processing has been modified by something other than this Iterator.");
            }
        }

        public boolean hasNext() {
            return this.filterlist.resync(this.forward ? this.cursor + 1 : this.cursor) < ContentList.this.size;
        }

        public boolean hasPrevious() {
            return (this.forward ? this.cursor : this.cursor + -1) >= 0;
        }

        public int nextIndex() {
            return this.forward ? this.cursor + 1 : this.cursor;
        }

        public int previousIndex() {
            return this.forward ? this.cursor : this.cursor - 1;
        }

        public F next() {
            checkConcurrent();
            int next = this.forward ? this.cursor + 1 : this.cursor;
            if (this.filterlist.resync(next) >= ContentList.this.size) {
                throw new NoSuchElementException("next() is beyond the end of the Iterator");
            }
            this.cursor = next;
            this.forward = true;
            this.canremove = true;
            this.canset = true;
            return this.filterlist.get(this.cursor);
        }

        public F previous() {
            checkConcurrent();
            int prev = this.forward ? this.cursor : this.cursor - 1;
            if (prev < 0) {
                throw new NoSuchElementException("previous() is beyond the beginning of the Iterator");
            }
            this.cursor = prev;
            this.forward = false;
            this.canremove = true;
            this.canset = true;
            return this.filterlist.get(this.cursor);
        }

        public void add(Content obj) {
            checkConcurrent();
            int next = this.forward ? this.cursor + 1 : this.cursor;
            this.filterlist.add(next, obj);
            this.expectedmod = ContentList.this.getModCount();
            this.canset = false;
            this.canremove = false;
            this.cursor = next;
            this.forward = true;
        }

        public void remove() {
            checkConcurrent();
            if (!this.canremove) {
                throw new IllegalStateException("Can not remove an element unless either next() or previous() has been called since the last remove()");
            }
            this.filterlist.remove(this.cursor);
            this.forward = false;
            this.expectedmod = ContentList.this.getModCount();
            this.canremove = false;
            this.canset = false;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.jdom2.ContentList.FilterList.set(int, org.jdom2.Content):F
         arg types: [int, F]
         candidates:
          org.jdom2.ContentList.FilterList.set(int, java.lang.Object):java.lang.Object
          ClspMth{java.util.AbstractList.set(int, java.lang.Object):E}
          ClspMth{java.util.List.set(int, java.lang.Object):E}
          org.jdom2.ContentList.FilterList.set(int, org.jdom2.Content):F */
        public void set(F obj) {
            checkConcurrent();
            if (!this.canset) {
                throw new IllegalStateException("Can not set an element unless either next() or previous() has been called since the last remove() or set()");
            }
            this.filterlist.set(this.cursor, (Content) obj);
            this.expectedmod = ContentList.this.getModCount();
        }
    }
}
