package org.jdom2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import org.jdom2.Content;
import org.jdom2.ContentList;
import org.jdom2.filter.ElementFilter;
import org.jdom2.filter.Filter;
import org.jdom2.util.IteratorIterable;

public class Element extends Content implements Parent {
    private static final int INITIAL_ARRAY_SIZE = 5;
    private static final long serialVersionUID = 200;
    transient List<Namespace> additionalNamespaces;
    transient AttributeList attributes;
    transient ContentList content;
    protected String name;
    protected Namespace namespace;

    protected Element() {
        super(Content.CType.Element);
        this.additionalNamespaces = null;
        this.attributes = null;
        this.content = new ContentList(this);
    }

    public Element(String name2, Namespace namespace2) {
        super(Content.CType.Element);
        this.additionalNamespaces = null;
        this.attributes = null;
        this.content = new ContentList(this);
        setName(name2);
        setNamespace(namespace2);
    }

    public Element(String name2) {
        this(name2, (Namespace) null);
    }

    public Element(String name2, String uri) {
        this(name2, Namespace.getNamespace("", uri));
    }

    public Element(String name2, String prefix, String uri) {
        this(name2, Namespace.getNamespace(prefix, uri));
    }

    public String getName() {
        return this.name;
    }

    public Element setName(String name2) {
        String reason = Verifier.checkElementName(name2);
        if (reason != null) {
            throw new IllegalNameException(name2, "element", reason);
        }
        this.name = name2;
        return this;
    }

    public Namespace getNamespace() {
        return this.namespace;
    }

    public Element setNamespace(Namespace namespace2) {
        String reason;
        if (namespace2 == null) {
            namespace2 = Namespace.NO_NAMESPACE;
        }
        if (this.additionalNamespaces == null || (reason = Verifier.checkNamespaceCollision(namespace2, getAdditionalNamespaces())) == null) {
            if (hasAttributes()) {
                for (Attribute a : getAttributes()) {
                    String reason2 = Verifier.checkNamespaceCollision(namespace2, a);
                    if (reason2 != null) {
                        throw new IllegalAddException(this, namespace2, reason2);
                    }
                }
            }
            this.namespace = namespace2;
            return this;
        }
        throw new IllegalAddException(this, namespace2, reason);
    }

    public String getNamespacePrefix() {
        return this.namespace.getPrefix();
    }

    public String getNamespaceURI() {
        return this.namespace.getURI();
    }

    public Namespace getNamespace(String prefix) {
        if (prefix == null) {
            return null;
        }
        if (JDOMConstants.NS_PREFIX_XML.equals(prefix)) {
            return Namespace.XML_NAMESPACE;
        }
        if (prefix.equals(getNamespacePrefix())) {
            return getNamespace();
        }
        if (this.additionalNamespaces != null) {
            for (int i = 0; i < this.additionalNamespaces.size(); i++) {
                Namespace ns = this.additionalNamespaces.get(i);
                if (prefix.equals(ns.getPrefix())) {
                    return ns;
                }
            }
        }
        if (this.attributes != null) {
            Iterator i$ = this.attributes.iterator();
            while (i$.hasNext()) {
                Attribute a = i$.next();
                if (prefix.equals(a.getNamespacePrefix())) {
                    return a.getNamespace();
                }
            }
        }
        if (this.parent instanceof Element) {
            return ((Element) this.parent).getNamespace(prefix);
        }
        return null;
    }

    public String getQualifiedName() {
        if ("".equals(this.namespace.getPrefix())) {
            return getName();
        }
        return this.namespace.getPrefix() + ':' + this.name;
    }

    public boolean addNamespaceDeclaration(Namespace additionalNamespace) {
        if (this.additionalNamespaces == null) {
            this.additionalNamespaces = new ArrayList(5);
        }
        for (Namespace ns : this.additionalNamespaces) {
            if (ns == additionalNamespace) {
                return false;
            }
        }
        String reason = Verifier.checkNamespaceCollision(additionalNamespace, this);
        if (reason == null) {
            return this.additionalNamespaces.add(additionalNamespace);
        }
        throw new IllegalAddException(this, additionalNamespace, reason);
    }

    public void removeNamespaceDeclaration(Namespace additionalNamespace) {
        if (this.additionalNamespaces != null) {
            this.additionalNamespaces.remove(additionalNamespace);
        }
    }

    public List<Namespace> getAdditionalNamespaces() {
        if (this.additionalNamespaces == null) {
            return Collections.emptyList();
        }
        return Collections.unmodifiableList(this.additionalNamespaces);
    }

    public String getValue() {
        StringBuilder buffer = new StringBuilder();
        for (Content child : getContent()) {
            if ((child instanceof Element) || (child instanceof Text)) {
                buffer.append(child.getValue());
            }
        }
        return buffer.toString();
    }

    public boolean isRootElement() {
        return this.parent instanceof Document;
    }

    public int getContentSize() {
        return this.content.size();
    }

    public int indexOf(Content child) {
        return this.content.indexOf(child);
    }

    public String getText() {
        if (this.content.size() == 0) {
            return "";
        }
        if (this.content.size() == 1) {
            Content obj = this.content.get(0);
            if (obj instanceof Text) {
                return ((Text) obj).getText();
            }
            return "";
        }
        StringBuilder textContent = new StringBuilder();
        boolean hasText = false;
        for (int i = 0; i < this.content.size(); i++) {
            Content obj2 = this.content.get(i);
            if (obj2 instanceof Text) {
                textContent.append(((Text) obj2).getText());
                hasText = true;
            }
        }
        if (!hasText) {
            return "";
        }
        return textContent.toString();
    }

    public String getTextTrim() {
        return getText().trim();
    }

    public String getTextNormalize() {
        return Text.normalizeString(getText());
    }

    public String getChildText(String cname) {
        Element child = getChild(cname);
        if (child == null) {
            return null;
        }
        return child.getText();
    }

    public String getChildTextTrim(String cname) {
        Element child = getChild(cname);
        if (child == null) {
            return null;
        }
        return child.getTextTrim();
    }

    public String getChildTextNormalize(String cname) {
        Element child = getChild(cname);
        if (child == null) {
            return null;
        }
        return child.getTextNormalize();
    }

    public String getChildText(String cname, Namespace ns) {
        Element child = getChild(cname, ns);
        if (child == null) {
            return null;
        }
        return child.getText();
    }

    public String getChildTextTrim(String cname, Namespace ns) {
        Element child = getChild(cname, ns);
        if (child == null) {
            return null;
        }
        return child.getTextTrim();
    }

    public String getChildTextNormalize(String cname, Namespace ns) {
        Element child = getChild(cname, ns);
        if (child == null) {
            return null;
        }
        return child.getTextNormalize();
    }

    public Element setText(String text) {
        this.content.clear();
        if (text != null) {
            addContent((Content) new Text(text));
        }
        return this;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: org.jdom2.Content} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: org.jdom2.Text} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean coalesceText(boolean r8) {
        /*
            r7 = this;
            if (r8 == 0) goto L_0x0030
            org.jdom2.util.IteratorIterable r2 = r7.getDescendants()
        L_0x0006:
            r4 = 0
            r1 = 0
        L_0x0008:
            boolean r5 = r2.hasNext()
            if (r5 == 0) goto L_0x0053
            java.lang.Object r0 = r2.next()
            org.jdom2.Content r0 = (org.jdom2.Content) r0
            org.jdom2.Content$CType r5 = r0.getCType()
            org.jdom2.Content$CType r6 = org.jdom2.Content.CType.Text
            if (r5 != r6) goto L_0x0051
            r3 = r0
            org.jdom2.Text r3 = (org.jdom2.Text) r3
            java.lang.String r5 = ""
            java.lang.String r6 = r3.getValue()
            boolean r5 = r5.equals(r6)
            if (r5 == 0) goto L_0x0037
            r2.remove()
            r1 = 1
            goto L_0x0008
        L_0x0030:
            org.jdom2.ContentList r5 = r7.content
            java.util.Iterator r2 = r5.iterator()
            goto L_0x0006
        L_0x0037:
            if (r4 == 0) goto L_0x0043
            org.jdom2.Element r5 = r4.getParent()
            org.jdom2.Element r6 = r3.getParent()
            if (r5 == r6) goto L_0x0045
        L_0x0043:
            r4 = r3
            goto L_0x0008
        L_0x0045:
            java.lang.String r5 = r3.getValue()
            r4.append(r5)
            r2.remove()
            r1 = 1
            goto L_0x0008
        L_0x0051:
            r4 = 0
            goto L_0x0008
        L_0x0053:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jdom2.Element.coalesceText(boolean):boolean");
    }

    public List<Content> getContent() {
        return this.content;
    }

    public <E extends Content> List<E> getContent(Filter<E> filter) {
        return this.content.getView(filter);
    }

    public List<Content> removeContent() {
        List<Content> old = new ArrayList<>(this.content);
        this.content.clear();
        return old;
    }

    public <F extends Content> List<F> removeContent(Filter<F> filter) {
        List<F> old = new ArrayList<>();
        Iterator<E> it = this.content.getView(filter).iterator();
        while (it.hasNext()) {
            old.add((Content) it.next());
            it.remove();
        }
        return old;
    }

    public Element setContent(Collection<? extends Content> newContent) {
        this.content.clearAndSet(newContent);
        return this;
    }

    public Element setContent(int index, Content child) {
        this.content.set(index, child);
        return this;
    }

    public Parent setContent(int index, Collection<? extends Content> newContent) {
        this.content.remove(index);
        this.content.addAll(index, newContent);
        return this;
    }

    public Element addContent(String str) {
        return addContent((Content) new Text(str));
    }

    public Element addContent(Content child) {
        this.content.add(child);
        return this;
    }

    public Element addContent(Collection<? extends Content> newContent) {
        this.content.addAll(newContent);
        return this;
    }

    public Element addContent(int index, Content child) {
        this.content.add(index, child);
        return this;
    }

    public Element addContent(int index, Collection<? extends Content> newContent) {
        this.content.addAll(index, newContent);
        return this;
    }

    public List<Content> cloneContent() {
        int size = getContentSize();
        List<Content> list = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            list.add(getContent(i).clone());
        }
        return list;
    }

    public Content getContent(int index) {
        return this.content.get(index);
    }

    public boolean removeContent(Content child) {
        return this.content.remove(child);
    }

    public Content removeContent(int index) {
        return this.content.remove(index);
    }

    public Element setContent(Content child) {
        this.content.clear();
        this.content.add(child);
        return this;
    }

    public boolean isAncestor(Element element) {
        for (Parent p = element.getParent(); p instanceof Element; p = p.getParent()) {
            if (p == this) {
                return true;
            }
        }
        return false;
    }

    public boolean hasAttributes() {
        return this.attributes != null && !this.attributes.isEmpty();
    }

    public boolean hasAdditionalNamespaces() {
        return this.additionalNamespaces != null && !this.additionalNamespaces.isEmpty();
    }

    /* access modifiers changed from: package-private */
    public AttributeList getAttributeList() {
        if (this.attributes == null) {
            this.attributes = new AttributeList(this);
        }
        return this.attributes;
    }

    public List<Attribute> getAttributes() {
        return getAttributeList();
    }

    public Attribute getAttribute(String attname) {
        return getAttribute(attname, Namespace.NO_NAMESPACE);
    }

    public Attribute getAttribute(String attname, Namespace ns) {
        if (this.attributes == null) {
            return null;
        }
        return getAttributeList().get(attname, ns);
    }

    public String getAttributeValue(String attname) {
        if (this.attributes == null) {
            return null;
        }
        return getAttributeValue(attname, Namespace.NO_NAMESPACE);
    }

    public String getAttributeValue(String attname, String def) {
        return this.attributes == null ? def : getAttributeValue(attname, Namespace.NO_NAMESPACE, def);
    }

    public String getAttributeValue(String attname, Namespace ns) {
        if (this.attributes == null) {
            return null;
        }
        return getAttributeValue(attname, ns, null);
    }

    public String getAttributeValue(String attname, Namespace ns, String def) {
        Attribute attribute;
        return (this.attributes == null || (attribute = getAttributeList().get(attname, ns)) == null) ? def : attribute.getValue();
    }

    public Element setAttributes(Collection<? extends Attribute> newAttributes) {
        getAttributeList().clearAndSet(newAttributes);
        return this;
    }

    public Element setAttribute(String name2, String value) {
        Attribute attribute = getAttribute(name2);
        if (attribute == null) {
            setAttribute(new Attribute(name2, value));
        } else {
            attribute.setValue(value);
        }
        return this;
    }

    public Element setAttribute(String name2, String value, Namespace ns) {
        Attribute attribute = getAttribute(name2, ns);
        if (attribute == null) {
            setAttribute(new Attribute(name2, value, ns));
        } else {
            attribute.setValue(value);
        }
        return this;
    }

    public Element setAttribute(Attribute attribute) {
        getAttributeList().add(attribute);
        return this;
    }

    public boolean removeAttribute(String attname) {
        return removeAttribute(attname, Namespace.NO_NAMESPACE);
    }

    public boolean removeAttribute(String attname, Namespace ns) {
        if (this.attributes == null) {
            return false;
        }
        return getAttributeList().remove(attname, ns);
    }

    public boolean removeAttribute(Attribute attribute) {
        if (this.attributes == null) {
            return false;
        }
        return getAttributeList().remove(attribute);
    }

    public String toString() {
        StringBuilder stringForm = new StringBuilder(64).append("[Element: <").append(getQualifiedName());
        String nsuri = getNamespaceURI();
        if (!"".equals(nsuri)) {
            stringForm.append(" [Namespace: ").append(nsuri).append("]");
        }
        stringForm.append("/>]");
        return stringForm.toString();
    }

    public Element clone() {
        AttributeList attributeList;
        Element element = (Element) super.clone();
        element.content = new ContentList(element);
        if (this.attributes == null) {
            attributeList = null;
        } else {
            attributeList = new AttributeList(element);
        }
        element.attributes = attributeList;
        if (this.attributes != null) {
            for (int i = 0; i < this.attributes.size(); i++) {
                element.attributes.add(this.attributes.get(i).clone());
            }
        }
        if (this.additionalNamespaces != null) {
            element.additionalNamespaces = new ArrayList(this.additionalNamespaces);
        }
        for (int i2 = 0; i2 < this.content.size(); i2++) {
            element.content.add(this.content.get(i2).clone());
        }
        return element;
    }

    public IteratorIterable<Content> getDescendants() {
        return new DescendantIterator(this);
    }

    public <F extends Content> IteratorIterable<F> getDescendants(Filter<F> filter) {
        return new FilterIterator(new DescendantIterator(this), filter);
    }

    public List<Element> getChildren() {
        return this.content.getView(new ElementFilter());
    }

    public List<Element> getChildren(String cname) {
        return getChildren(cname, Namespace.NO_NAMESPACE);
    }

    public List<Element> getChildren(String cname, Namespace ns) {
        return this.content.getView(new ElementFilter(cname, ns));
    }

    public Element getChild(String cname, Namespace ns) {
        Iterator<Element> iter = this.content.getView(new ElementFilter(cname, ns)).iterator();
        if (iter.hasNext()) {
            return (Element) iter.next();
        }
        return null;
    }

    public Element getChild(String cname) {
        return getChild(cname, Namespace.NO_NAMESPACE);
    }

    public boolean removeChild(String cname) {
        return removeChild(cname, Namespace.NO_NAMESPACE);
    }

    public boolean removeChild(String cname, Namespace ns) {
        Iterator<Element> iter = this.content.getView(new ElementFilter(cname, ns)).iterator();
        if (!iter.hasNext()) {
            return false;
        }
        iter.next();
        iter.remove();
        return true;
    }

    public boolean removeChildren(String cname) {
        return removeChildren(cname, Namespace.NO_NAMESPACE);
    }

    public boolean removeChildren(String cname, Namespace ns) {
        boolean deletedSome = false;
        Iterator<Element> iter = this.content.getView(new ElementFilter(cname, ns)).iterator();
        while (iter.hasNext()) {
            iter.next();
            iter.remove();
            deletedSome = true;
        }
        return deletedSome;
    }

    public List<Namespace> getNamespacesInScope() {
        TreeMap<String, Namespace> namespaces = new TreeMap<>();
        namespaces.put(Namespace.XML_NAMESPACE.getPrefix(), Namespace.XML_NAMESPACE);
        namespaces.put(getNamespacePrefix(), getNamespace());
        if (this.additionalNamespaces != null) {
            for (Namespace ns : getAdditionalNamespaces()) {
                if (!namespaces.containsKey(ns.getPrefix())) {
                    namespaces.put(ns.getPrefix(), ns);
                }
            }
        }
        if (this.attributes != null) {
            for (Attribute att : getAttributes()) {
                Namespace ns2 = att.getNamespace();
                if (!namespaces.containsKey(ns2.getPrefix())) {
                    namespaces.put(ns2.getPrefix(), ns2);
                }
            }
        }
        Element pnt = getParentElement();
        if (pnt != null) {
            for (Namespace ns3 : pnt.getNamespacesInScope()) {
                if (!namespaces.containsKey(ns3.getPrefix())) {
                    namespaces.put(ns3.getPrefix(), ns3);
                }
            }
        }
        if (pnt == null && !namespaces.containsKey("")) {
            namespaces.put(Namespace.NO_NAMESPACE.getPrefix(), Namespace.NO_NAMESPACE);
        }
        ArrayList<Namespace> al = new ArrayList<>(namespaces.size());
        al.add(getNamespace());
        namespaces.remove(getNamespacePrefix());
        al.addAll(namespaces.values());
        return Collections.unmodifiableList(al);
    }

    public List<Namespace> getNamespacesInherited() {
        if (getParentElement() == null) {
            ArrayList<Namespace> ret = new ArrayList<>(getNamespacesInScope());
            Iterator<Namespace> it = ret.iterator();
            while (it.hasNext()) {
                Namespace ns = (Namespace) it.next();
                if (!(ns == Namespace.NO_NAMESPACE || ns == Namespace.XML_NAMESPACE)) {
                    it.remove();
                }
            }
            return Collections.unmodifiableList(ret);
        }
        HashMap<String, Namespace> parents = new HashMap<>();
        for (Namespace ns2 : getParentElement().getNamespacesInScope()) {
            parents.put(ns2.getPrefix(), ns2);
        }
        ArrayList<Namespace> al = new ArrayList<>();
        for (Namespace ns3 : getNamespacesInScope()) {
            if (ns3 == parents.get(ns3.getPrefix())) {
                al.add(ns3);
            }
        }
        return Collections.unmodifiableList(al);
    }

    public List<Namespace> getNamespacesIntroduced() {
        if (getParentElement() == null) {
            List<Namespace> ret = new ArrayList<>(getNamespacesInScope());
            Iterator<Namespace> it = ret.iterator();
            while (it.hasNext()) {
                Namespace ns = (Namespace) it.next();
                if (ns == Namespace.XML_NAMESPACE || ns == Namespace.NO_NAMESPACE) {
                    it.remove();
                }
            }
            return Collections.unmodifiableList(ret);
        }
        HashMap<String, Namespace> parents = new HashMap<>();
        for (Namespace ns2 : getParentElement().getNamespacesInScope()) {
            parents.put(ns2.getPrefix(), ns2);
        }
        ArrayList<Namespace> al = new ArrayList<>();
        for (Namespace ns3 : getNamespacesInScope()) {
            if (!parents.containsKey(ns3.getPrefix()) || ns3 != parents.get(ns3.getPrefix())) {
                al.add(ns3);
            }
        }
        return Collections.unmodifiableList(al);
    }

    public Element detach() {
        return (Element) super.detach();
    }

    public void canContainContent(Content child, int index, boolean replace) throws IllegalAddException {
        if (child instanceof DocType) {
            throw new IllegalAddException("A DocType is not allowed except at the document level");
        }
    }

    public void sortContent(Comparator<? super Content> comparator) {
        this.content.sort(comparator);
    }

    public void sortChildren(Comparator<? super Element> comparator) {
        ((ContentList.FilterList) getChildren()).sort(comparator);
    }

    public void sortAttributes(Comparator<? super Attribute> comparator) {
        if (this.attributes != null) {
            this.attributes.sort(comparator);
        }
    }

    public <E extends Content> void sortContent(Filter<E> filter, Comparator<? super E> comparator) {
        ((ContentList.FilterList) getContent(filter)).sort(comparator);
    }

    private final URI resolve(String uri, URI relative) throws URISyntaxException {
        if (uri == null) {
            return relative;
        }
        URI n = new URI(uri);
        if (relative == null) {
            return n;
        }
        return n.resolve(relative);
    }

    public URI getXMLBaseURI() throws URISyntaxException {
        URI ret = null;
        for (Parent p = this; p != null; p = p.getParent()) {
            if (p instanceof Element) {
                ret = resolve(((Element) p).getAttributeValue("base", Namespace.XML_NAMESPACE), ret);
            } else {
                ret = resolve(((Document) p).getBaseURI(), ret);
            }
            if (ret != null && ret.isAbsolute()) {
                return ret;
            }
        }
        return ret;
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        if (hasAdditionalNamespaces()) {
            int ans = this.additionalNamespaces.size();
            out.writeInt(ans);
            for (int i = 0; i < ans; i++) {
                out.writeObject(this.additionalNamespaces.get(i));
            }
        } else {
            out.writeInt(0);
        }
        if (hasAttributes()) {
            int ans2 = this.attributes.size();
            out.writeInt(ans2);
            for (int i2 = 0; i2 < ans2; i2++) {
                out.writeObject(this.attributes.get(i2));
            }
        } else {
            out.writeInt(0);
        }
        int cs = this.content.size();
        out.writeInt(cs);
        for (int i3 = 0; i3 < cs; i3++) {
            out.writeObject(this.content.get(i3));
        }
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.content = new ContentList(this);
        int nss = in.readInt();
        while (true) {
            nss--;
            if (nss < 0) {
                break;
            }
            addNamespaceDeclaration((Namespace) in.readObject());
        }
        int ats = in.readInt();
        while (true) {
            ats--;
            if (ats < 0) {
                break;
            }
            setAttribute((Attribute) in.readObject());
        }
        int cs = in.readInt();
        while (true) {
            cs--;
            if (cs >= 0) {
                addContent((Content) in.readObject());
            } else {
                return;
            }
        }
    }
}
