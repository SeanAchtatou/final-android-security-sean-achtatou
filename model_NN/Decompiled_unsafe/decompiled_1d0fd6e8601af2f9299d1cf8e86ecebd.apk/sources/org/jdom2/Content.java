package org.jdom2;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public abstract class Content extends CloneBase implements Serializable, NamespaceAware {
    private static final long serialVersionUID = 200;
    protected final CType ctype;
    protected transient Parent parent = null;

    public enum CType {
        Comment,
        Element,
        ProcessingInstruction,
        EntityRef,
        Text,
        CDATA,
        DocType
    }

    public abstract String getValue();

    protected Content(CType type) {
        this.ctype = type;
    }

    public final CType getCType() {
        return this.ctype;
    }

    public Content detach() {
        if (this.parent != null) {
            this.parent.removeContent(this);
        }
        return this;
    }

    public Parent getParent() {
        return this.parent;
    }

    public final Element getParentElement() {
        Parent pnt = getParent();
        return pnt instanceof Element ? pnt : null;
    }

    /* access modifiers changed from: protected */
    public Content setParent(Parent parent2) {
        this.parent = parent2;
        return this;
    }

    public Document getDocument() {
        if (this.parent == null) {
            return null;
        }
        return this.parent.getDocument();
    }

    public Content clone() {
        Content c = (Content) super.clone();
        c.parent = null;
        return c;
    }

    public final boolean equals(Object ob) {
        return ob == this;
    }

    public final int hashCode() {
        return super.hashCode();
    }

    public List<Namespace> getNamespacesInScope() {
        Element emt = getParentElement();
        if (emt == null) {
            return Collections.singletonList(Namespace.XML_NAMESPACE);
        }
        return emt.getNamespacesInScope();
    }

    public List<Namespace> getNamespacesIntroduced() {
        return Collections.emptyList();
    }

    public List<Namespace> getNamespacesInherited() {
        return getNamespacesInScope();
    }
}
