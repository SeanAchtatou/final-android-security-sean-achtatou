package org.jdom2;

import org.jdom2.Content;
import org.jdom2.output.Format;

public class Text extends Content {
    static final String EMPTY_STRING = "";
    private static final long serialVersionUID = 200;
    protected String value;

    protected Text(Content.CType ctype) {
        super(ctype);
    }

    protected Text() {
        this(Content.CType.Text);
    }

    public Text(String str) {
        this(Content.CType.Text);
        setText(str);
    }

    public String getText() {
        return this.value;
    }

    public String getTextTrim() {
        return Format.trimBoth(getText());
    }

    public String getTextNormalize() {
        return normalizeString(getText());
    }

    public static String normalizeString(String str) {
        if (str == null) {
            return "";
        }
        return Format.compact(str);
    }

    public Text setText(String str) {
        if (str == null) {
            this.value = "";
        } else {
            String reason = Verifier.checkCharacterData(str);
            if (reason != null) {
                throw new IllegalDataException(str, "character content", reason);
            }
            this.value = str;
        }
        return this;
    }

    public void append(String str) {
        if (str != null) {
            String reason = Verifier.checkCharacterData(str);
            if (reason != null) {
                throw new IllegalDataException(str, "character content", reason);
            } else if (str.length() > 0) {
                this.value += str;
            }
        }
    }

    public void append(Text text) {
        if (text != null) {
            this.value += text.getText();
        }
    }

    public String getValue() {
        return this.value;
    }

    public String toString() {
        return new StringBuilder(64).append("[Text: ").append(getText()).append("]").toString();
    }

    public Text clone() {
        Text text = (Text) super.clone();
        text.value = this.value;
        return text;
    }

    public Text detach() {
        return (Text) super.detach();
    }

    /* access modifiers changed from: protected */
    public Text setParent(Parent parent) {
        return (Text) super.setParent(parent);
    }

    public Element getParent() {
        return (Element) super.getParent();
    }
}
