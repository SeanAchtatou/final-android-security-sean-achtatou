package org.jdom2;

public class IllegalAddException extends IllegalArgumentException {
    private static final long serialVersionUID = 200;

    IllegalAddException(Element base, Attribute added, String reason) {
        super("The attribute \"" + added.getQualifiedName() + "\" could not be added to the element \"" + base.getQualifiedName() + "\": " + reason);
    }

    IllegalAddException(Element base, Element added, String reason) {
        super("The element \"" + added.getQualifiedName() + "\" could not be added as a child of \"" + base.getQualifiedName() + "\": " + reason);
    }

    IllegalAddException(Element added, String reason) {
        super("The element \"" + added.getQualifiedName() + "\" could not be added as the root of the document: " + reason);
    }

    IllegalAddException(Element base, ProcessingInstruction added, String reason) {
        super("The PI \"" + added.getTarget() + "\" could not be added as content to \"" + base.getQualifiedName() + "\": " + reason);
    }

    IllegalAddException(ProcessingInstruction added, String reason) {
        super("The PI \"" + added.getTarget() + "\" could not be added to the top level of the document: " + reason);
    }

    IllegalAddException(Element base, Comment added, String reason) {
        super("The comment \"" + added.getText() + "\" could not be added as content to \"" + base.getQualifiedName() + "\": " + reason);
    }

    IllegalAddException(Element base, CDATA added, String reason) {
        super("The CDATA \"" + added.getText() + "\" could not be added as content to \"" + base.getQualifiedName() + "\": " + reason);
    }

    IllegalAddException(Element base, Text added, String reason) {
        super("The Text \"" + added.getText() + "\" could not be added as content to \"" + base.getQualifiedName() + "\": " + reason);
    }

    IllegalAddException(Comment added, String reason) {
        super("The comment \"" + added.getText() + "\" could not be added to the top level of the document: " + reason);
    }

    IllegalAddException(Element base, EntityRef added, String reason) {
        super("The entity reference\"" + added.getName() + "\" could not be added as content to \"" + base.getQualifiedName() + "\": " + reason);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    IllegalAddException(org.jdom2.Element r4, org.jdom2.Namespace r5, java.lang.String r6) {
        /*
            r3 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "The namespace xmlns"
            java.lang.StringBuilder r1 = r0.append(r1)
            java.lang.String r0 = r5.getPrefix()
            java.lang.String r2 = ""
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x004b
            java.lang.String r0 = "="
        L_0x0019:
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = "\""
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r5.getURI()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "\" could not be added as a namespace to \""
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r4.getQualifiedName()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "\": "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r0 = r0.toString()
            r3.<init>(r0)
            return
        L_0x004b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = ":"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = r5.getPrefix()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "="
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jdom2.IllegalAddException.<init>(org.jdom2.Element, org.jdom2.Namespace, java.lang.String):void");
    }

    IllegalAddException(DocType added, String reason) {
        super("The DOCTYPE " + added.toString() + " could not be added to the document: " + reason);
    }

    public IllegalAddException(String reason) {
        super(reason);
    }
}
