package org.jdom2.input.sax;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.jdom2.Attribute;
import org.jdom2.AttributeType;
import org.jdom2.Comment;
import org.jdom2.DefaultJDOMFactory;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMConstants;
import org.jdom2.JDOMFactory;
import org.jdom2.Namespace;
import org.jdom2.Parent;
import org.jdom2.ProcessingInstruction;
import org.xml.sax.Attributes;
import org.xml.sax.DTDHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.ext.Attributes2;
import org.xml.sax.ext.DeclHandler;
import org.xml.sax.ext.LexicalHandler;
import org.xml.sax.helpers.DefaultHandler;

public class SAXHandler extends DefaultHandler implements LexicalHandler, DeclHandler, DTDHandler {
    private boolean atRoot;
    private Document currentDocument;
    private Element currentElement;
    private Locator currentLocator;
    private final List<Namespace> declaredNamespaces;
    private int entityDepth;
    private boolean expand;
    private final Map<String, String[]> externalEntities;
    private final JDOMFactory factory;
    private boolean ignoringBoundaryWhite;
    private boolean ignoringWhite;
    private boolean inCDATA;
    private boolean inDTD;
    private boolean inInternalSubset;
    private final StringBuilder internalSubset;
    private int lastcol;
    private int lastline;
    private boolean previousCDATA;
    private boolean suppress;
    private final TextBuffer textBuffer;

    public SAXHandler() {
        this(null);
    }

    public SAXHandler(JDOMFactory factory2) {
        this.declaredNamespaces = new ArrayList(32);
        this.internalSubset = new StringBuilder();
        this.textBuffer = new TextBuffer();
        this.externalEntities = new HashMap();
        this.currentDocument = null;
        this.currentElement = null;
        this.currentLocator = null;
        this.atRoot = true;
        this.inDTD = false;
        this.inInternalSubset = false;
        this.previousCDATA = false;
        this.inCDATA = false;
        this.expand = true;
        this.suppress = false;
        this.entityDepth = 0;
        this.ignoringWhite = false;
        this.ignoringBoundaryWhite = false;
        this.lastline = 0;
        this.lastcol = 0;
        this.factory = factory2 == null ? new DefaultJDOMFactory() : factory2;
        reset();
    }

    /* access modifiers changed from: protected */
    public void resetSubCLass() {
    }

    public final void reset() {
        this.currentLocator = null;
        this.currentDocument = this.factory.document(null);
        this.currentElement = null;
        this.atRoot = true;
        this.inDTD = false;
        this.inInternalSubset = false;
        this.previousCDATA = false;
        this.inCDATA = false;
        this.expand = true;
        this.suppress = false;
        this.entityDepth = 0;
        this.declaredNamespaces.clear();
        this.internalSubset.setLength(0);
        this.textBuffer.clear();
        this.externalEntities.clear();
        this.ignoringWhite = false;
        this.ignoringBoundaryWhite = false;
        resetSubCLass();
    }

    /* access modifiers changed from: protected */
    public void pushElement(Element element) {
        if (this.atRoot) {
            this.currentDocument.setRootElement(element);
            this.atRoot = false;
        } else {
            this.factory.addContent(this.currentElement, element);
        }
        this.currentElement = element;
    }

    public Document getDocument() {
        return this.currentDocument;
    }

    public JDOMFactory getFactory() {
        return this.factory;
    }

    public void setExpandEntities(boolean expand2) {
        this.expand = expand2;
    }

    public boolean getExpandEntities() {
        return this.expand;
    }

    public void setIgnoringElementContentWhitespace(boolean ignoringWhite2) {
        this.ignoringWhite = ignoringWhite2;
    }

    public void setIgnoringBoundaryWhitespace(boolean ignoringBoundaryWhite2) {
        this.ignoringBoundaryWhite = ignoringBoundaryWhite2;
    }

    public boolean getIgnoringBoundaryWhitespace() {
        return this.ignoringBoundaryWhite;
    }

    public boolean getIgnoringElementContentWhitespace() {
        return this.ignoringWhite;
    }

    public void startDocument() {
        if (this.currentLocator != null) {
            this.currentDocument.setBaseURI(this.currentLocator.getSystemId());
        }
    }

    public void externalEntityDecl(String name, String publicID, String systemID) throws SAXException {
        this.externalEntities.put(name, new String[]{publicID, systemID});
        if (this.inInternalSubset) {
            this.internalSubset.append("  <!ENTITY ").append(name);
            appendExternalId(publicID, systemID);
            this.internalSubset.append(">\n");
        }
    }

    public void attributeDecl(String eName, String aName, String type, String valueDefault, String value) {
        if (this.inInternalSubset) {
            this.internalSubset.append("  <!ATTLIST ").append(eName).append(' ').append(aName).append(' ').append(type).append(' ');
            if (valueDefault != null) {
                this.internalSubset.append(valueDefault);
            } else {
                this.internalSubset.append('\"').append(value).append('\"');
            }
            if (valueDefault != null && valueDefault.equals("#FIXED")) {
                this.internalSubset.append(" \"").append(value).append('\"');
            }
            this.internalSubset.append(">\n");
        }
    }

    public void elementDecl(String name, String model) {
        if (this.inInternalSubset) {
            this.internalSubset.append("  <!ELEMENT ").append(name).append(' ').append(model).append(">\n");
        }
    }

    public void internalEntityDecl(String name, String value) {
        if (this.inInternalSubset) {
            this.internalSubset.append("  <!ENTITY ");
            if (name.startsWith("%")) {
                this.internalSubset.append("% ").append(name.substring(1));
            } else {
                this.internalSubset.append(name);
            }
            this.internalSubset.append(" \"").append(value).append("\">\n");
        }
    }

    public void processingInstruction(String target, String data) throws SAXException {
        if (!this.suppress) {
            flushCharacters();
            ProcessingInstruction pi = this.currentLocator == null ? this.factory.processingInstruction(target, data) : this.factory.processingInstruction(this.currentLocator.getLineNumber(), this.currentLocator.getColumnNumber(), target, data);
            if (this.atRoot) {
                this.factory.addContent(this.currentDocument, pi);
            } else {
                this.factory.addContent(getCurrentElement(), pi);
            }
        }
    }

    public void skippedEntity(String name) throws SAXException {
        if (!name.startsWith("%")) {
            flushCharacters();
            this.factory.addContent(getCurrentElement(), this.currentLocator == null ? this.factory.entityRef(name) : this.factory.entityRef(this.currentLocator.getLineNumber(), this.currentLocator.getColumnNumber(), name));
        }
    }

    public void startPrefixMapping(String prefix, String uri) throws SAXException {
        if (!this.suppress) {
            this.declaredNamespaces.add(Namespace.getNamespace(prefix, uri));
        }
    }

    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        Element element;
        if (!this.suppress) {
            String prefix = "";
            if (!"".equals(qName)) {
                int colon = qName.indexOf(58);
                if (colon > 0) {
                    prefix = qName.substring(0, colon);
                }
                if (localName == null || localName.equals("")) {
                    localName = qName.substring(colon + 1);
                }
            }
            Namespace namespace = Namespace.getNamespace(prefix, namespaceURI);
            if (this.currentLocator == null) {
                element = this.factory.element(localName, namespace);
            } else {
                element = this.factory.element(this.currentLocator.getLineNumber(), this.currentLocator.getColumnNumber(), localName, namespace);
            }
            if (this.declaredNamespaces.size() > 0) {
                transferNamespaces(element);
            }
            flushCharacters();
            if (this.atRoot) {
                this.factory.setRoot(this.currentDocument, element);
                this.atRoot = false;
            } else {
                this.factory.addContent(getCurrentElement(), element);
            }
            this.currentElement = element;
            int len = atts.getLength();
            for (int i = 0; i < len; i++) {
                String attPrefix = "";
                String attLocalName = atts.getLocalName(i);
                String attQName = atts.getQName(i);
                boolean specified = atts instanceof Attributes2 ? ((Attributes2) atts).isSpecified(i) : true;
                if (!attQName.equals("")) {
                    if (!attQName.startsWith("xmlns:") && !attQName.equals("xmlns")) {
                        int attColon = attQName.indexOf(58);
                        if (attColon > 0) {
                            attPrefix = attQName.substring(0, attColon);
                        }
                        if ("".equals(attLocalName)) {
                            attLocalName = attQName.substring(attColon + 1);
                        }
                    }
                }
                AttributeType attType = AttributeType.getAttributeType(atts.getType(i));
                String attValue = atts.getValue(i);
                String attURI = atts.getURI(i);
                if (!"xmlns".equals(attLocalName) && !"xmlns".equals(attPrefix) && !JDOMConstants.NS_URI_XMLNS.equals(attURI)) {
                    if (!"".equals(attURI) && "".equals(attPrefix)) {
                        HashMap<String, Namespace> tmpmap = new HashMap<>();
                        Iterator i$ = element.getNamespacesInScope().iterator();
                        while (true) {
                            if (!i$.hasNext()) {
                                break;
                            }
                            Namespace nss = i$.next();
                            if (nss.getPrefix().length() > 0 && nss.getURI().equals(attURI)) {
                                attPrefix = nss.getPrefix();
                                break;
                            }
                            tmpmap.put(nss.getPrefix(), nss);
                        }
                        if ("".equals(attPrefix)) {
                            int cnt = 0;
                            String pfx = "attns" + 0;
                            while (tmpmap.containsKey(pfx)) {
                                cnt++;
                                pfx = "attns" + cnt;
                            }
                            attPrefix = pfx;
                        }
                    }
                    Attribute attribute = this.factory.attribute(attLocalName, attValue, attType, Namespace.getNamespace(attPrefix, attURI));
                    if (!specified) {
                        attribute.setSpecified(false);
                    }
                    this.factory.setAttribute(element, attribute);
                }
            }
        }
    }

    private void transferNamespaces(Element element) {
        for (Namespace ns : this.declaredNamespaces) {
            if (ns != element.getNamespace()) {
                element.addNamespaceDeclaration(ns);
            }
        }
        this.declaredNamespaces.clear();
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        if (this.suppress) {
            return;
        }
        if (length != 0 || this.inCDATA) {
            if (this.previousCDATA != this.inCDATA) {
                flushCharacters();
            }
            this.textBuffer.append(ch, start, length);
            if (this.currentLocator != null) {
                this.lastline = this.currentLocator.getLineNumber();
                this.lastcol = this.currentLocator.getColumnNumber();
            }
        }
    }

    public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
        if (!this.ignoringWhite) {
            characters(ch, start, length);
        }
    }

    /* access modifiers changed from: protected */
    public void flushCharacters() throws SAXException {
        if (!this.ignoringBoundaryWhite) {
            flushCharacters(this.textBuffer.toString());
        } else if (!this.textBuffer.isAllWhitespace()) {
            flushCharacters(this.textBuffer.toString());
        }
        this.textBuffer.clear();
    }

    /* access modifiers changed from: protected */
    public void flushCharacters(String data) throws SAXException {
        if (data.length() != 0 || this.inCDATA) {
            if (this.previousCDATA) {
                this.factory.addContent(getCurrentElement(), this.currentLocator == null ? this.factory.cdata(data) : this.factory.cdata(this.lastline, this.lastcol, data));
            } else {
                this.factory.addContent(getCurrentElement(), this.currentLocator == null ? this.factory.text(data) : this.factory.text(this.lastline, this.lastcol, data));
            }
            this.previousCDATA = this.inCDATA;
            return;
        }
        this.previousCDATA = this.inCDATA;
    }

    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
        if (!this.suppress) {
            flushCharacters();
            if (!this.atRoot) {
                Parent p = this.currentElement.getParent();
                if (p instanceof Document) {
                    this.atRoot = true;
                } else {
                    this.currentElement = (Element) p;
                }
            } else {
                throw new SAXException("Ill-formed XML document (missing opening tag for " + localName + ")");
            }
        }
    }

    public void startDTD(String name, String publicID, String systemID) throws SAXException {
        flushCharacters();
        this.factory.addContent(this.currentDocument, this.currentLocator == null ? this.factory.docType(name, publicID, systemID) : this.factory.docType(this.currentLocator.getLineNumber(), this.currentLocator.getColumnNumber(), name, publicID, systemID));
        this.inDTD = true;
        this.inInternalSubset = true;
    }

    public void endDTD() {
        this.currentDocument.getDocType().setInternalSubset(this.internalSubset.toString());
        this.inDTD = false;
        this.inInternalSubset = false;
    }

    public void startEntity(String name) throws SAXException {
        this.entityDepth++;
        if (!this.expand && this.entityDepth <= 1) {
            if (name.equals("[dtd]")) {
                this.inInternalSubset = false;
            } else if (!this.inDTD && !name.equals("amp") && !name.equals("lt") && !name.equals("gt") && !name.equals("apos") && !name.equals("quot") && !this.expand) {
                String pub = null;
                String sys = null;
                String[] ids = this.externalEntities.get(name);
                if (ids != null) {
                    pub = ids[0];
                    sys = ids[1];
                }
                if (!this.atRoot) {
                    flushCharacters();
                    this.factory.addContent(getCurrentElement(), this.currentLocator == null ? this.factory.entityRef(name, pub, sys) : this.factory.entityRef(this.currentLocator.getLineNumber(), this.currentLocator.getColumnNumber(), name, pub, sys));
                }
                this.suppress = true;
            }
        }
    }

    public void endEntity(String name) throws SAXException {
        this.entityDepth--;
        if (this.entityDepth == 0) {
            this.suppress = false;
        }
        if (name.equals("[dtd]")) {
            this.inInternalSubset = true;
        }
    }

    public void startCDATA() {
        if (!this.suppress) {
            this.inCDATA = true;
        }
    }

    public void endCDATA() throws SAXException {
        if (!this.suppress) {
            this.previousCDATA = true;
            flushCharacters();
            this.previousCDATA = false;
            this.inCDATA = false;
        }
    }

    public void comment(char[] ch, int start, int length) throws SAXException {
        if (!this.suppress) {
            flushCharacters();
            String commentText = new String(ch, start, length);
            if (this.inDTD && this.inInternalSubset && !this.expand) {
                this.internalSubset.append("  <!--").append(commentText).append("-->\n");
            } else if (!this.inDTD && !commentText.equals("")) {
                Comment comment = this.currentLocator == null ? this.factory.comment(commentText) : this.factory.comment(this.currentLocator.getLineNumber(), this.currentLocator.getColumnNumber(), commentText);
                if (this.atRoot) {
                    this.factory.addContent(this.currentDocument, comment);
                } else {
                    this.factory.addContent(getCurrentElement(), comment);
                }
            }
        }
    }

    public void notationDecl(String name, String publicID, String systemID) throws SAXException {
        if (this.inInternalSubset) {
            this.internalSubset.append("  <!NOTATION ").append(name);
            appendExternalId(publicID, systemID);
            this.internalSubset.append(">\n");
        }
    }

    public void unparsedEntityDecl(String name, String publicID, String systemID, String notationName) {
        if (this.inInternalSubset) {
            this.internalSubset.append("  <!ENTITY ").append(name);
            appendExternalId(publicID, systemID);
            this.internalSubset.append(" NDATA ").append(notationName);
            this.internalSubset.append(">\n");
        }
    }

    private void appendExternalId(String publicID, String systemID) {
        if (publicID != null) {
            this.internalSubset.append(" PUBLIC \"").append(publicID).append('\"');
        }
        if (systemID != null) {
            if (publicID == null) {
                this.internalSubset.append(" SYSTEM ");
            } else {
                this.internalSubset.append(' ');
            }
            this.internalSubset.append('\"').append(systemID).append('\"');
        }
    }

    public Element getCurrentElement() throws SAXException {
        if (this.currentElement != null) {
            return this.currentElement;
        }
        throw new SAXException("Ill-formed XML document (multiple root elements detected)");
    }

    public void setDocumentLocator(Locator locator) {
        this.currentLocator = locator;
    }

    public Locator getDocumentLocator() {
        return this.currentLocator;
    }
}
