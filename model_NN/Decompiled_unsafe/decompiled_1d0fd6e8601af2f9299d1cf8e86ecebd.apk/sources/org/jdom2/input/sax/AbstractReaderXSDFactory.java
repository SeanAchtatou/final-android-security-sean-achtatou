package org.jdom2.input.sax;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.jdom2.JDOMException;
import org.xml.sax.SAXException;

public class AbstractReaderXSDFactory extends AbstractReaderSchemaFactory {
    private static final ThreadLocal<SchemaFactory> schemafactl = new ThreadLocal<>();

    protected interface SchemaFactoryProvider {
        SchemaFactory getSchemaFactory();
    }

    private static final Schema getSchemaFromString(SchemaFactoryProvider sfp, String... systemID) throws JDOMException {
        if (systemID == null) {
            throw new NullPointerException("Cannot specify a null input array");
        } else if (systemID.length == 0) {
            throw new IllegalArgumentException("You need at least one XSD source for an XML Schema validator");
        } else {
            Source[] urls = new Source[systemID.length];
            for (int i = 0; i < systemID.length; i++) {
                if (systemID[i] == null) {
                    throw new NullPointerException("Cannot specify a null SystemID");
                }
                urls[i] = new StreamSource(systemID[i]);
            }
            return getSchemaFromSource(sfp, urls);
        }
    }

    private static final Schema getSchemaFromFile(SchemaFactoryProvider sfp, File... systemID) throws JDOMException {
        if (systemID == null) {
            throw new NullPointerException("Cannot specify a null input array");
        } else if (systemID.length == 0) {
            throw new IllegalArgumentException("You need at least one XSD source for an XML Schema validator");
        } else {
            Source[] sources = new Source[systemID.length];
            for (int i = 0; i < systemID.length; i++) {
                if (systemID[i] == null) {
                    throw new NullPointerException("Cannot specify a null SystemID");
                }
                sources[i] = new StreamSource(systemID[i]);
            }
            return getSchemaFromSource(sfp, sources);
        }
    }

    private static final Schema getSchemaFromURL(SchemaFactoryProvider sfp, URL... systemID) throws JDOMException {
        int i;
        if (systemID == null) {
            throw new NullPointerException("Cannot specify a null input array");
        } else if (systemID.length == 0) {
            throw new IllegalArgumentException("You need at least one XSD source for an XML Schema validator");
        } else {
            InputStream[] streams = new InputStream[systemID.length];
            try {
                Source[] sources = new Source[systemID.length];
                i = 0;
                while (i < systemID.length) {
                    if (systemID[i] == null) {
                        throw new NullPointerException("Cannot specify a null SystemID");
                    }
                    InputStream is = systemID[i].openStream();
                    streams[i] = is;
                    sources[i] = new StreamSource(is, systemID[i].toString());
                    i++;
                }
                Schema schemaFromSource = getSchemaFromSource(sfp, sources);
                for (InputStream is2 : streams) {
                    if (is2 != null) {
                        try {
                            is2.close();
                        } catch (IOException e) {
                        }
                    }
                }
                return schemaFromSource;
            } catch (IOException e2) {
                throw new JDOMException("Unable to read Schema URL " + systemID[i].toString(), e2);
            } catch (Throwable th) {
                for (InputStream is3 : streams) {
                    if (is3 != null) {
                        try {
                            is3.close();
                        } catch (IOException e3) {
                        }
                    }
                }
                throw th;
            }
        }
    }

    private static final Schema getSchemaFromSource(SchemaFactoryProvider sfp, Source... sources) throws JDOMException {
        if (sources == null) {
            throw new NullPointerException("Cannot specify a null input array");
        } else if (sources.length == 0) {
            throw new IllegalArgumentException("You need at least one XSD Source for an XML Schema validator");
        } else {
            try {
                SchemaFactory sfac = schemafactl.get();
                if (sfac == null) {
                    sfac = sfp.getSchemaFactory();
                    schemafactl.set(sfac);
                }
                if (sfac != null) {
                    return sfac.newSchema(sources);
                }
                throw new JDOMException("Unable to create XSDSchema validator.");
            } catch (SAXException e) {
                throw new JDOMException("Unable to create a Schema for Sources " + Arrays.toString(sources), e);
            }
        }
    }

    public AbstractReaderXSDFactory(SAXParserFactory fac, SchemaFactoryProvider sfp, String... systemid) throws JDOMException {
        super(fac, getSchemaFromString(sfp, systemid));
    }

    public AbstractReaderXSDFactory(SAXParserFactory fac, SchemaFactoryProvider sfp, URL... systemid) throws JDOMException {
        super(fac, getSchemaFromURL(sfp, systemid));
    }

    public AbstractReaderXSDFactory(SAXParserFactory fac, SchemaFactoryProvider sfp, File... systemid) throws JDOMException {
        super(fac, getSchemaFromFile(sfp, systemid));
    }

    public AbstractReaderXSDFactory(SAXParserFactory fac, SchemaFactoryProvider sfp, Source... sources) throws JDOMException {
        super(fac, getSchemaFromSource(sfp, sources));
    }
}
