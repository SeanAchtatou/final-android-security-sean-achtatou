package org.jdom2;

import com.fsck.k9.remotecontrol.K9RemoteControl;
import exts.whats.Constants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

public class Attribute extends CloneBase implements NamespaceAware, Serializable, Cloneable {
    public static final AttributeType CDATA_TYPE = AttributeType.CDATA;
    public static final AttributeType ENTITIES_TYPE = AttributeType.ENTITIES;
    public static final AttributeType ENTITY_TYPE = AttributeType.ENTITY;
    public static final AttributeType ENUMERATED_TYPE = AttributeType.ENUMERATION;
    public static final AttributeType IDREFS_TYPE = AttributeType.IDREFS;
    public static final AttributeType IDREF_TYPE = AttributeType.IDREF;
    public static final AttributeType ID_TYPE = AttributeType.ID;
    public static final AttributeType NMTOKENS_TYPE = AttributeType.NMTOKENS;
    public static final AttributeType NMTOKEN_TYPE = AttributeType.NMTOKEN;
    public static final AttributeType NOTATION_TYPE = AttributeType.NOTATION;
    public static final AttributeType UNDECLARED_TYPE = AttributeType.UNDECLARED;
    private static final long serialVersionUID = 200;
    protected String name;
    protected Namespace namespace;
    protected transient Element parent;
    protected boolean specified;
    protected AttributeType type;
    protected String value;

    protected Attribute() {
        this.type = AttributeType.UNDECLARED;
        this.specified = true;
    }

    public Attribute(String name2, String value2, Namespace namespace2) {
        this(name2, value2, AttributeType.UNDECLARED, namespace2);
    }

    @Deprecated
    public Attribute(String name2, String value2, int type2, Namespace namespace2) {
        this(name2, value2, AttributeType.byIndex(type2), namespace2);
    }

    public Attribute(String name2, String value2, AttributeType type2, Namespace namespace2) {
        this.type = AttributeType.UNDECLARED;
        this.specified = true;
        setName(name2);
        setValue(value2);
        setAttributeType(type2);
        setNamespace(namespace2);
    }

    public Attribute(String name2, String value2) {
        this(name2, value2, AttributeType.UNDECLARED, Namespace.NO_NAMESPACE);
    }

    public Attribute(String name2, String value2, AttributeType type2) {
        this(name2, value2, type2, Namespace.NO_NAMESPACE);
    }

    @Deprecated
    public Attribute(String name2, String value2, int type2) {
        this(name2, value2, type2, Namespace.NO_NAMESPACE);
    }

    public Element getParent() {
        return this.parent;
    }

    public Document getDocument() {
        if (this.parent == null) {
            return null;
        }
        return this.parent.getDocument();
    }

    public String getName() {
        return this.name;
    }

    public Attribute setName(String name2) {
        if (name2 == null) {
            throw new NullPointerException("Can not set a null name for an Attribute.");
        }
        String reason = Verifier.checkAttributeName(name2);
        if (reason != null) {
            throw new IllegalNameException(name2, "attribute", reason);
        }
        this.name = name2;
        this.specified = true;
        return this;
    }

    public String getQualifiedName() {
        String prefix = this.namespace.getPrefix();
        if ("".equals(prefix)) {
            return getName();
        }
        return prefix + ':' + getName();
    }

    public String getNamespacePrefix() {
        return this.namespace.getPrefix();
    }

    public String getNamespaceURI() {
        return this.namespace.getURI();
    }

    public Namespace getNamespace() {
        return this.namespace;
    }

    public Attribute setNamespace(Namespace namespace2) {
        if (namespace2 == null) {
            namespace2 = Namespace.NO_NAMESPACE;
        }
        if (namespace2 == Namespace.NO_NAMESPACE || !"".equals(namespace2.getPrefix())) {
            this.namespace = namespace2;
            this.specified = true;
            return this;
        }
        throw new IllegalNameException("", "attribute namespace", "An attribute namespace without a prefix can only be the NO_NAMESPACE namespace");
    }

    public String getValue() {
        return this.value;
    }

    public Attribute setValue(String value2) {
        if (value2 == null) {
            throw new NullPointerException("Can not set a null value for an Attribute");
        }
        String reason = Verifier.checkCharacterData(value2);
        if (reason != null) {
            throw new IllegalDataException(value2, "attribute", reason);
        }
        this.value = value2;
        this.specified = true;
        return this;
    }

    public AttributeType getAttributeType() {
        return this.type;
    }

    public Attribute setAttributeType(AttributeType type2) {
        if (type2 == null) {
            type2 = AttributeType.UNDECLARED;
        }
        this.type = type2;
        this.specified = true;
        return this;
    }

    @Deprecated
    public Attribute setAttributeType(int type2) {
        setAttributeType(AttributeType.byIndex(type2));
        return this;
    }

    public boolean isSpecified() {
        return this.specified;
    }

    public void setSpecified(boolean specified2) {
        this.specified = specified2;
    }

    public String toString() {
        return "[Attribute: " + getQualifiedName() + "=\"" + this.value + "\"" + "]";
    }

    public Attribute clone() {
        Attribute clone = (Attribute) super.clone();
        clone.parent = null;
        return clone;
    }

    public Attribute detach() {
        if (this.parent != null) {
            this.parent.removeAttribute(this);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public Attribute setParent(Element parent2) {
        this.parent = parent2;
        return this;
    }

    public int getIntValue() throws DataConversionException {
        try {
            return Integer.parseInt(this.value.trim());
        } catch (NumberFormatException e) {
            throw new DataConversionException(this.name, "int");
        }
    }

    public long getLongValue() throws DataConversionException {
        try {
            return Long.parseLong(this.value.trim());
        } catch (NumberFormatException e) {
            throw new DataConversionException(this.name, "long");
        }
    }

    public float getFloatValue() throws DataConversionException {
        try {
            return Float.valueOf(this.value.trim()).floatValue();
        } catch (NumberFormatException e) {
            throw new DataConversionException(this.name, "float");
        }
    }

    public double getDoubleValue() throws DataConversionException {
        try {
            return Double.valueOf(this.value.trim()).doubleValue();
        } catch (NumberFormatException e) {
            String v = this.value.trim();
            if ("INF".equals(v)) {
                return Double.POSITIVE_INFINITY;
            }
            if ("-INF".equals(v)) {
                return Double.NEGATIVE_INFINITY;
            }
            throw new DataConversionException(this.name, "double");
        }
    }

    public boolean getBooleanValue() throws DataConversionException {
        String valueTrim = this.value.trim();
        if (valueTrim.equalsIgnoreCase("true") || valueTrim.equalsIgnoreCase("on") || valueTrim.equalsIgnoreCase(Constants.INSTALL_ID) || valueTrim.equalsIgnoreCase("yes")) {
            return true;
        }
        if (valueTrim.equalsIgnoreCase(K9RemoteControl.K9_DISABLED) || valueTrim.equalsIgnoreCase("off") || valueTrim.equalsIgnoreCase("0") || valueTrim.equalsIgnoreCase("no")) {
            return false;
        }
        throw new DataConversionException(this.name, "boolean");
    }

    public List<Namespace> getNamespacesInScope() {
        if (getParent() != null) {
            return orderFirst(getNamespace(), getParent().getNamespacesInScope());
        }
        ArrayList<Namespace> ret = new ArrayList<>(3);
        ret.add(getNamespace());
        ret.add(Namespace.XML_NAMESPACE);
        return Collections.unmodifiableList(ret);
    }

    public List<Namespace> getNamespacesIntroduced() {
        if (getParent() == null) {
            return Collections.singletonList(getNamespace());
        }
        return Collections.emptyList();
    }

    public List<Namespace> getNamespacesInherited() {
        if (getParent() == null) {
            return Collections.singletonList(Namespace.XML_NAMESPACE);
        }
        return orderFirst(getNamespace(), getParent().getNamespacesInScope());
    }

    private static final List<Namespace> orderFirst(Namespace nsa, List<Namespace> nsl) {
        if (nsl.get(0) == nsa) {
            return nsl;
        }
        TreeMap<String, Namespace> tm = new TreeMap<>();
        for (Namespace ns : nsl) {
            if (ns != nsa) {
                tm.put(ns.getPrefix(), ns);
            }
        }
        ArrayList<Namespace> ret = new ArrayList<>(tm.size() + 1);
        ret.add(nsa);
        ret.addAll(tm.values());
        return Collections.unmodifiableList(ret);
    }
}
