package org.jdom2.adapters;

import java.lang.reflect.InvocationTargetException;
import org.jdom2.DocType;
import org.jdom2.JDOMException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;

public abstract class AbstractDOMAdapter implements DOMAdapter {
    public Document createDocument(DocType doctype) throws JDOMException {
        if (doctype == null) {
            return createDocument();
        }
        DOMImplementation domImpl = createDocument().getImplementation();
        DocumentType domDocType = domImpl.createDocumentType(doctype.getElementName(), doctype.getPublicID(), doctype.getSystemID());
        setInternalSubset(domDocType, doctype.getInternalSubset());
        Document ret = domImpl.createDocument("http://temporary", doctype.getElementName(), domDocType);
        Element root = ret.getDocumentElement();
        if (root == null) {
            return ret;
        }
        ret.removeChild(root);
        return ret;
    }

    /* access modifiers changed from: protected */
    public void setInternalSubset(DocumentType dt, String s) {
        if (dt != null && s != null) {
            try {
                dt.getClass().getMethod("setInternalSubset", String.class).invoke(dt, s);
            } catch (IllegalAccessException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
            }
        }
    }
}
