package org.htmlcleaner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HtmlTokenizer {
    private static final int WORKING_BUFFER_SIZE = 1024;
    private boolean _asExpected = true;
    private transient int _col = 1;
    private transient TagToken _currentTagToken;
    private transient DoctypeToken _docType;
    private transient boolean _isLateForDoctype;
    private boolean _isSpecialContext;
    private String _isSpecialContextName;
    private transient int _len = -1;
    private transient Set<String> _namespacePrefixes = new HashSet();
    private transient int _pos;
    private BufferedReader _reader;
    private transient int _row = 1;
    private transient StringBuffer _saved = new StringBuffer(512);
    private transient List<BaseToken> _tokenList = new ArrayList();
    private char[] _working = new char[1024];
    private CleanTimeValues cleanTimeValues;
    private HtmlCleaner cleaner;
    private CleanerProperties props;
    private CleanerTransformations transformations;

    public HtmlTokenizer(HtmlCleaner cleaner2, Reader reader, CleanTimeValues cleanTimeValues2) {
        this._reader = new BufferedReader(reader);
        this.cleaner = cleaner2;
        this.props = cleaner2.getProperties();
        this.transformations = cleaner2.getTransformations();
        this.cleanTimeValues = cleanTimeValues2;
    }

    private void addToken(BaseToken token) {
        token.setRow(this._row);
        token.setCol(this._col);
        this._tokenList.add(token);
        this.cleaner.makeTree(this._tokenList, this._tokenList.listIterator(this._tokenList.size() - 1), this.cleanTimeValues);
    }

    private void readIfNeeded(int neededChars) throws IOException {
        if (this._len == -1 && this._pos + neededChars >= 1024) {
            int numToCopy = 1024 - this._pos;
            System.arraycopy(this._working, this._pos, this._working, 0, numToCopy);
            this._pos = 0;
            int expected = 1024 - numToCopy;
            int size = 0;
            int offset = numToCopy;
            do {
                int charsRead = this._reader.read(this._working, offset, expected);
                if (charsRead >= 0) {
                    size += charsRead;
                    offset += charsRead;
                    expected -= charsRead;
                }
                if (charsRead < 0) {
                    break;
                }
            } while (expected > 0);
            if (expected > 0) {
                this._len = size + numToCopy;
            }
            int i = 0;
            while (true) {
                if (i < (this._len >= 0 ? this._len : 1024)) {
                    char c = this._working[i];
                    if (c >= 1 && c <= ' ' && c != 10 && c != 13) {
                        this._working[i] = ' ';
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public List<BaseToken> getTokenList() {
        return this._tokenList;
    }

    /* access modifiers changed from: package-private */
    public Set<String> getNamespacePrefixes() {
        return this._namespacePrefixes;
    }

    private void go() throws IOException {
        go(1);
    }

    private void go(int step) throws IOException {
        this._pos += step;
        readIfNeeded(step - 1);
    }

    private boolean startsWith(String value) throws IOException {
        int valueLen = value.length();
        readIfNeeded(valueLen);
        if (this._len >= 0 && this._pos + valueLen > this._len) {
            return false;
        }
        for (int i = 0; i < valueLen; i++) {
            if (Character.toLowerCase(value.charAt(i)) != Character.toLowerCase(this._working[this._pos + i])) {
                return false;
            }
        }
        return true;
    }

    private boolean isWhitespace(int position) {
        if (this._len < 0 || position < this._len) {
            return Character.isWhitespace(this._working[position]);
        }
        return false;
    }

    private boolean isWhitespace() {
        return isWhitespace(this._pos);
    }

    private boolean isChar(int position, char ch) {
        if ((this._len < 0 || position < this._len) && Character.toLowerCase(ch) == Character.toLowerCase(this._working[position])) {
            return true;
        }
        return false;
    }

    private boolean isChar(char ch) {
        return isChar(this._pos, ch);
    }

    private boolean isIdentifierStartChar(int position) {
        if (this._len < 0 || position < this._len) {
            return Character.isUnicodeIdentifierStart(this._working[position]);
        }
        return false;
    }

    private boolean isIdentifierStartChar() {
        return isIdentifierStartChar(this._pos);
    }

    private boolean isIdentifierChar() {
        if (this._len >= 0 && this._pos >= this._len) {
            return false;
        }
        char ch = this._working[this._pos];
        if (Character.isUnicodeIdentifierStart(ch) || Character.isDigit(ch) || Utils.isIdentifierHelperChar(ch)) {
            return true;
        }
        return false;
    }

    private boolean isAllRead() {
        return this._len >= 0 && this._pos >= this._len;
    }

    private void save(char ch) {
        updateCoordinates(ch);
        this._saved.append(ch);
    }

    private void updateCoordinates(char ch) {
        if (ch == 10) {
            this._row++;
            this._col = 1;
            return;
        }
        this._col++;
    }

    private void saveCurrent() {
        if (!isAllRead()) {
            save(this._working[this._pos]);
        }
    }

    private void saveCurrent(int size) throws IOException {
        readIfNeeded(size);
        int pos = this._pos;
        while (!isAllRead() && size > 0) {
            save(this._working[pos]);
            pos++;
            size--;
        }
    }

    private void skipWhitespaces() throws IOException {
        while (!isAllRead() && isWhitespace()) {
            saveCurrent();
            go();
        }
    }

    private boolean addSavedAsContent() {
        if (this._saved.length() <= 0) {
            return false;
        }
        addToken(new ContentNode(this.props.isDeserializeEntities() ? deserializeEntitiesInSaved() : this._saved.toString()));
        this._saved.delete(0, this._saved.length());
        return true;
    }

    private String deserializeEntitiesInSaved() {
        SpecialEntities entities = SpecialEntities.INSTANCE;
        int entityStart = -1;
        boolean numericEntity = false;
        boolean hexEntity = false;
        int maxEntityLength = entities.getMaxEntityLength();
        int i = 0;
        int length = this._saved.length();
        while (i < length) {
            if (this._saved.charAt(i) == '&') {
                entityStart = i;
                numericEntity = false;
                hexEntity = false;
                i++;
            } else if (entityStart == -1) {
                i++;
            } else if (this._saved.charAt(i) == ';') {
                int entityValue = -1;
                if (numericEntity) {
                    try {
                        entityValue = Integer.parseInt(this._saved.substring((hexEntity ? 3 : 2) + entityStart, i), hexEntity ? 16 : 10);
                    } catch (NumberFormatException e) {
                        entityValue = -1;
                    }
                    SpecialEntity entity = entities.getSpecialEntityByUnicode(entityValue);
                    if (entity != null) {
                        entityValue = entity.intValue();
                    } else if (!this.props.isRecognizeUnicodeChars()) {
                        entityValue = -1;
                    }
                } else {
                    SpecialEntity entity2 = entities.getSpecialEntity(this._saved.substring(entityStart + 1, i));
                    if (entity2 != null) {
                        entityValue = entity2.intValue();
                    }
                }
                if (entityValue >= 0) {
                    char[] decodedEntity = Character.toChars(entityValue);
                    this._saved.replace(entityStart, i + 1, new String(decodedEntity));
                    length = this._saved.length();
                    i = entityStart + decodedEntity.length;
                } else {
                    i++;
                }
                entityStart = -1;
            } else {
                if (i == entityStart + 1 && this._saved.charAt(i) == '#') {
                    numericEntity = true;
                } else if (i == entityStart + 2 && numericEntity && this._saved.charAt(i) == 'x') {
                    hexEntity = true;
                } else if (i - entityStart > maxEntityLength) {
                    entityStart = -1;
                }
                i++;
            }
        }
        return this._saved.toString();
    }

    /* access modifiers changed from: package-private */
    public void start() throws IOException {
        BaseToken lastToken;
        String lastTokenAsString;
        this._currentTagToken = null;
        this._tokenList.clear();
        this._asExpected = true;
        this._isSpecialContext = false;
        this._isLateForDoctype = false;
        this._namespacePrefixes.clear();
        this._pos = 1024;
        readIfNeeded(0);
        boolean isSpecialEmpty = true;
        while (!isAllRead()) {
            if (Thread.currentThread().isInterrupted()) {
                handleInterruption();
                this._tokenList.clear();
                this._namespacePrefixes.clear();
                this._reader.close();
                return;
            }
            this._saved.delete(0, this._saved.length());
            this._currentTagToken = null;
            this._asExpected = true;
            readIfNeeded(10);
            if (this._isSpecialContext) {
                int nameLen = this._isSpecialContextName.length();
                if (startsWith("</" + this._isSpecialContextName) && (isWhitespace(this._pos + nameLen + 2) || isChar(this._pos + nameLen + 2, '>'))) {
                    tagEnd();
                } else if (isSpecialEmpty && startsWith("<!--")) {
                    comment();
                } else if (startsWith(CData.SAFE_BEGIN_CDATA) || startsWith(CData.BEGIN_CDATA) || startsWith(CData.SAFE_BEGIN_CDATA_ALT)) {
                    cdata();
                } else {
                    boolean isTokenAdded = content();
                    if (isSpecialEmpty && isTokenAdded && (lastToken = this._tokenList.get(this._tokenList.size() - 1)) != null && (lastTokenAsString = lastToken.toString()) != null && lastTokenAsString.trim().length() > 0) {
                        isSpecialEmpty = false;
                    }
                }
                if (!this._isSpecialContext) {
                    isSpecialEmpty = true;
                }
            } else if (startsWith("<!doctype")) {
                if (!this._isLateForDoctype) {
                    doctype();
                    this._isLateForDoctype = true;
                } else {
                    ignoreUntil('<');
                }
            } else if (startsWith("</") && isIdentifierStartChar(this._pos + 2)) {
                this._isLateForDoctype = true;
                tagEnd();
            } else if (startsWith(CData.SAFE_BEGIN_CDATA) || startsWith(CData.BEGIN_CDATA) || startsWith(CData.SAFE_BEGIN_CDATA_ALT)) {
                cdata();
            } else if (startsWith("<!--")) {
                comment();
            } else if (startsWith("<") && isIdentifierStartChar(this._pos + 1)) {
                this._isLateForDoctype = true;
                tagStart();
            } else if (this.props.isIgnoreQuestAndExclam() && (startsWith("<!") || startsWith("<?"))) {
                ignoreUntil('<');
                if (isChar('>')) {
                    go();
                }
            } else if (startsWith("<?xml")) {
                ignoreUntil('<');
            } else {
                content();
            }
        }
        this._reader.close();
    }

    private boolean isReservedTag(String tagName) {
        return "html".equalsIgnoreCase(tagName) || "head".equalsIgnoreCase(tagName) || "body".equalsIgnoreCase(tagName);
    }

    private void tagStart() throws IOException {
        TagInfo tagInfo;
        saveCurrent();
        go();
        if (!isAllRead()) {
            String originalTagName = identifier();
            String tagName = this.transformations.getTagName(originalTagName);
            if (tagName == null || (((tagInfo = this.cleaner.getTagInfoProvider().getTagInfo(tagName)) != null || this.props.isOmitUnknownTags() || !this.props.isTreatUnknownTagsAsContent() || isReservedTag(tagName) || this.props.isNamespacesAware()) && (tagInfo == null || !tagInfo.isDeprecated() || this.props.isOmitDeprecatedTags() || !this.props.isTreatDeprecatedTagsAsContent()))) {
                TagNode tagNode = new TagNode(tagName);
                tagNode.setTrimAttributeValues(this.props.isTrimAttributeValues());
                this._currentTagToken = tagNode;
                if (this._asExpected) {
                    skipWhitespaces();
                    tagAttributes();
                    if (tagName != null) {
                        if (this.transformations != null) {
                            tagNode.setAttributes(this.transformations.transformAttributes(originalTagName, tagNode.getAttributesInLowerCase()));
                        }
                        addToken(this._currentTagToken);
                    }
                    if (isChar('>')) {
                        go();
                        if (this.props.isUseCdataFor(tagName)) {
                            this._isSpecialContext = true;
                            this._isSpecialContextName = tagName;
                        }
                    } else if (startsWith("/>")) {
                        go(2);
                        addToken(new EndTagToken(tagName));
                    }
                    this._currentTagToken = null;
                    return;
                }
                addSavedAsContent();
                return;
            }
            content();
        }
    }

    private void tagEnd() throws IOException {
        TagInfo tagInfo;
        TagTransformation tagTransformation;
        saveCurrent(2);
        go(2);
        this._col += 2;
        if (!isAllRead()) {
            String tagName = identifier();
            if (!(this.transformations == null || !this.transformations.hasTransformationForTag(tagName) || (tagTransformation = this.transformations.getTransformation(tagName)) == null)) {
                tagName = tagTransformation.getDestTag();
            }
            if (tagName == null || (((tagInfo = this.cleaner.getTagInfoProvider().getTagInfo(tagName)) != null || this.props.isOmitUnknownTags() || !this.props.isTreatUnknownTagsAsContent() || isReservedTag(tagName) || this.props.isNamespacesAware()) && (tagInfo == null || !tagInfo.isDeprecated() || this.props.isOmitDeprecatedTags() || !this.props.isTreatDeprecatedTagsAsContent()))) {
                this._currentTagToken = new EndTagToken(tagName);
                if (this._asExpected) {
                    skipWhitespaces();
                    tagAttributes();
                    if (tagName != null) {
                        addToken(this._currentTagToken);
                    }
                    if (isChar('>')) {
                        go();
                    }
                    if (this.props.isUseCdataFor(tagName)) {
                        this._isSpecialContext = false;
                        this._isSpecialContextName = tagName;
                    }
                    if (tagName != null && tagName.equalsIgnoreCase("html")) {
                        skipWhitespaces();
                    }
                    this._currentTagToken = null;
                    return;
                }
                addSavedAsContent();
                return;
            }
            content();
        }
    }

    private String identifier() throws IOException {
        this._asExpected = true;
        if (!isIdentifierStartChar()) {
            this._asExpected = false;
            return null;
        }
        StringBuffer identifierValue = new StringBuffer();
        while (!isAllRead() && isIdentifierChar()) {
            saveCurrent();
            identifierValue.append(this._working[this._pos]);
            go();
        }
        while (identifierValue.length() > 0 && Utils.isIdentifierHelperChar(identifierValue.charAt(identifierValue.length() - 1))) {
            identifierValue.deleteCharAt(identifierValue.length() - 1);
        }
        if (identifierValue.length() == 0) {
            return null;
        }
        String id = identifierValue.toString();
        int columnIndex = id.indexOf(58);
        if (columnIndex < 0) {
            return id;
        }
        String prefix = id.substring(0, columnIndex);
        String suffix = id.substring(columnIndex + 1);
        int nextColumnIndex = suffix.indexOf(58);
        if (nextColumnIndex >= 0) {
            suffix = suffix.substring(0, nextColumnIndex);
        }
        if (!this.props.isNamespacesAware()) {
            return suffix;
        }
        String id2 = prefix + ":" + suffix;
        if ("xmlns".equalsIgnoreCase(prefix)) {
            return id2;
        }
        this._namespacePrefixes.add(prefix.toLowerCase());
        return id2;
    }

    private void tagAttributes() throws IOException {
        String attValue;
        while (!isAllRead() && this._asExpected && !isChar('>') && !startsWith("/>")) {
            if (Thread.currentThread().isInterrupted()) {
                handleInterruption();
                return;
            }
            skipWhitespaces();
            String attName = identifier();
            if (!this._asExpected) {
                if (!isChar('<') && !isChar('>') && !startsWith("/>")) {
                    saveCurrent();
                    go();
                }
                if (!isChar('<')) {
                    this._asExpected = true;
                }
            } else {
                skipWhitespaces();
                if (isChar('=')) {
                    saveCurrent();
                    go();
                    attValue = attributeValue();
                } else if ("empty".equals(this.props.getBooleanAttributeValues())) {
                    attValue = "";
                } else if ("true".equals(this.props.getBooleanAttributeValues())) {
                    attValue = "true";
                } else {
                    attValue = attName;
                }
                if (this._asExpected) {
                    this._currentTagToken.addAttribute(attName, attValue);
                }
            }
        }
    }

    private String attributeValue() throws IOException {
        skipWhitespaces();
        if (isChar('<') || isChar('>') || startsWith("/>")) {
            return "";
        }
        boolean isQuoteMode = false;
        boolean isAposMode = false;
        StringBuffer result = new StringBuffer();
        if (isChar('\'')) {
            isAposMode = true;
            saveCurrent();
            go();
        } else if (isChar('\"')) {
            isQuoteMode = true;
            saveCurrent();
            go();
        }
        boolean isMultiWord = this.props.isAllowMultiWordAttributes();
        boolean allowHtml = this.props.isAllowHtmlInsideAttributes();
        while (!isAllRead() && ((isAposMode && !isChar('\'') && ((allowHtml || (!isChar('>') && !isChar('<'))) && (isMultiWord || !isWhitespace()))) || ((isQuoteMode && !isChar('\"') && ((allowHtml || (!isChar('>') && !isChar('<'))) && (isMultiWord || !isWhitespace()))) || (!isAposMode && !isQuoteMode && !isWhitespace() && !isChar('>') && !isChar('<'))))) {
            result.append(this._working[this._pos]);
            saveCurrent();
            go();
        }
        if (isChar('\'') && isAposMode) {
            saveCurrent();
            go();
        } else if (isChar('\"') && isQuoteMode) {
            saveCurrent();
            go();
        }
        return result.toString();
    }

    private boolean content() throws IOException {
        while (!isAllRead()) {
            saveCurrent();
            go();
            if (!startsWith(CData.SAFE_BEGIN_CDATA)) {
                if (!startsWith(CData.BEGIN_CDATA)) {
                    if (!startsWith(CData.SAFE_BEGIN_CDATA_ALT)) {
                        if (isTagStartOrEnd()) {
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        return addSavedAsContent();
    }

    private boolean isTagStartOrEnd() throws IOException {
        return startsWith("</") || startsWith("<!") || startsWith("<?") || (startsWith("<") && isIdentifierStartChar(this._pos + 1));
    }

    private void ignoreUntil(char ch) throws IOException {
        while (!isAllRead()) {
            go();
            updateCoordinates(this._working[this._pos]);
            if (isChar(ch)) {
                return;
            }
        }
    }

    private void comment() throws IOException {
        go(4);
        while (!isAllRead() && !startsWith("-->")) {
            saveCurrent();
            go();
        }
        if (startsWith("-->")) {
            go(3);
        }
        if (this._saved.length() > 0) {
            if (!this.props.isOmitComments()) {
                String hyphenRepl = this.props.getHyphenReplacementInComment();
                String comment = this._saved.toString().replaceAll("--", hyphenRepl + hyphenRepl);
                if (comment.length() > 0 && comment.charAt(0) == '-') {
                    comment = hyphenRepl + comment.substring(1);
                }
                int len = comment.length();
                if (len > 0 && comment.charAt(len - 1) == '-') {
                    comment = comment.substring(0, len - 1) + hyphenRepl;
                }
                addToken(new CommentNode(comment));
            }
            this._saved.delete(0, this._saved.length());
        }
    }

    private void cdata() throws IOException {
        boolean preserveComments = false;
        if (this._isSpecialContext || this.props.isOmitCdataOutsideScriptAndStyle()) {
            if (startsWith(CData.SAFE_BEGIN_CDATA)) {
                go(CData.SAFE_BEGIN_CDATA.length());
            } else if (startsWith(CData.SAFE_BEGIN_CDATA_ALT)) {
                preserveComments = true;
                go(CData.SAFE_BEGIN_CDATA_ALT.length());
            } else {
                go(CData.BEGIN_CDATA.length());
            }
            int cdataStart = this._saved.length();
            while (!isAllRead() && !startsWith(CData.SAFE_END_CDATA) && !startsWith(CData.END_CDATA) && !startsWith(CData.SAFE_END_CDATA_ALT)) {
                saveCurrent();
                go();
            }
            if (startsWith(CData.SAFE_END_CDATA)) {
                go(CData.SAFE_END_CDATA.length());
            } else if (startsWith(CData.SAFE_END_CDATA_ALT)) {
                go(CData.SAFE_END_CDATA_ALT.length());
            } else if (startsWith(CData.END_CDATA)) {
                go(CData.END_CDATA.length());
            }
            if (this._saved.length() > 0 && (this._isSpecialContext || !this.props.isOmitCdataOutsideScriptAndStyle())) {
                if (preserveComments) {
                    addToken(new ContentNode("//"));
                }
                addToken(new CData(this._saved.toString().substring(cdataStart)));
            }
            this._saved.delete(cdataStart, this._saved.length());
            return;
        }
        content();
    }

    private void doctype() throws IOException {
        go(9);
        skipWhitespaces();
        String part1 = identifier();
        skipWhitespaces();
        String part2 = identifier();
        skipWhitespaces();
        String part3 = attributeValue();
        skipWhitespaces();
        String part4 = attributeValue();
        skipWhitespaces();
        String part5 = attributeValue();
        ignoreUntil('<');
        if (part5 == null || part5.length() == 0) {
            this._docType = new DoctypeToken(part1, part2, part3, part4);
        } else {
            this._docType = new DoctypeToken(part1, part2, part3, part4, part5);
        }
    }

    public DoctypeToken getDocType() {
        return this._docType;
    }

    private void handleInterruption() {
    }
}
