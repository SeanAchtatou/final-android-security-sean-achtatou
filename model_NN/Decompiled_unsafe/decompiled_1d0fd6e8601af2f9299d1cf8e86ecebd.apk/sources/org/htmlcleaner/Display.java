package org.htmlcleaner;

public enum Display {
    block(true, false),
    inline(false, true),
    any(true, false),
    none(true, false);
    
    private boolean afterTagLineBreakNeeded;
    private boolean leadingAndEndWhitespacesAllowed;

    private Display(boolean afterTagLineBreakNeeded2, boolean leadingAndEndWhitespacesAllowed2) {
        this.afterTagLineBreakNeeded = afterTagLineBreakNeeded2;
        this.leadingAndEndWhitespacesAllowed = leadingAndEndWhitespacesAllowed2;
    }

    public boolean isAfterTagLineBreakNeeded() {
        return this.afterTagLineBreakNeeded;
    }

    public boolean isLeadingAndEndWhitespacesAllowed() {
        return this.leadingAndEndWhitespacesAllowed;
    }
}
