package org.htmlcleaner;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class TagTransformation {
    public static String VAR_END = "}";
    public static String VAR_START = "${";
    private List<AttributeTransformation> attributePatternTransformations;
    private Map<String, String> attributeTransformations;
    private String destTag;
    private boolean preserveSourceAttributes;
    private String sourceTag;

    public TagTransformation() {
        this.attributeTransformations = new LinkedHashMap();
        this.attributePatternTransformations = new ArrayList();
        this.preserveSourceAttributes = true;
    }

    public TagTransformation(String sourceTag2, String destTag2, boolean preserveSourceAttributes2) {
        this.attributeTransformations = new LinkedHashMap();
        this.attributePatternTransformations = new ArrayList();
        this.sourceTag = sourceTag2.toLowerCase();
        if (destTag2 == null) {
            this.destTag = null;
        } else {
            this.destTag = Utils.isValidXmlIdentifier(destTag2) ? destTag2.toLowerCase() : sourceTag2;
        }
        this.preserveSourceAttributes = preserveSourceAttributes2;
    }

    public TagTransformation(String sourceTag2, String destTag2) {
        this(sourceTag2, destTag2, true);
    }

    public TagTransformation(String sourceTag2) {
        this(sourceTag2, null);
    }

    public void addAttributeTransformation(String targetAttName, String transformationDesc) {
        this.attributeTransformations.put(targetAttName.toLowerCase(), transformationDesc);
    }

    public void addAttributePatternTransformation(Pattern attNamePattern, String transformationDesc) {
        this.attributePatternTransformations.add(new AttributeTransformationPatternImpl(attNamePattern, (Pattern) null, transformationDesc));
    }

    public void addAttributePatternTransformation(Pattern attNamePattern, Pattern attValuePattern, String transformationDesc) {
        addAttributePatternTransformation(new AttributeTransformationPatternImpl(attNamePattern, attValuePattern, transformationDesc));
    }

    public void addAttributePatternTransformation(AttributeTransformation attributeTransformation) {
        if (this.attributePatternTransformations == null) {
            this.attributePatternTransformations = new ArrayList();
        }
        this.attributePatternTransformations.add(attributeTransformation);
    }

    public void addAttributeTransformation(String targetAttName) {
        addAttributeTransformation(targetAttName, null);
    }

    /* access modifiers changed from: package-private */
    public boolean hasAttributeTransformations() {
        return (this.attributeTransformations == null && this.attributePatternTransformations == null) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public String getSourceTag() {
        return this.sourceTag;
    }

    /* access modifiers changed from: package-private */
    public String getDestTag() {
        return this.destTag;
    }

    /* access modifiers changed from: package-private */
    public boolean isPreserveSourceAttributes() {
        return this.preserveSourceAttributes;
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> getAttributeTransformations() {
        return this.attributeTransformations;
    }

    public Map<String, String> applyTagTransformations(Map<String, String> attributes) {
        Map<String, String> newAttributes;
        boolean isPreserveSourceAtts = isPreserveSourceAttributes();
        boolean hasAttTransforms = hasAttributeTransformations();
        if (!hasAttTransforms && isPreserveSourceAtts) {
            return attributes;
        }
        if (isPreserveSourceAtts) {
            newAttributes = new LinkedHashMap<>(attributes);
        } else {
            newAttributes = new LinkedHashMap<>();
        }
        if (hasAttTransforms) {
            for (Map.Entry<String, String> entry : getAttributeTransformations().entrySet()) {
                String attName = (String) entry.getKey();
                String template = (String) entry.getValue();
                if (template == null) {
                    newAttributes.remove(attName);
                } else {
                    newAttributes.put(attName, evaluateTemplate(template, attributes));
                }
            }
            for (AttributeTransformation attributeTransformation : this.attributePatternTransformations) {
                for (Map.Entry<String, String> entry1 : attributes.entrySet()) {
                    String attName2 = (String) entry1.getKey();
                    if (attributeTransformation.satisfy(attName2, (String) entry1.getValue())) {
                        String template2 = attributeTransformation.getTemplate();
                        if (template2 == null) {
                            newAttributes.remove(attName2);
                        } else {
                            newAttributes.put(attName2, evaluateTemplate(template2, attributes));
                        }
                    }
                }
            }
        }
        return newAttributes;
    }

    public String evaluateTemplate(String template, Map<String, String> variables) {
        if (template == null) {
            return template;
        }
        StringBuffer result = new StringBuffer();
        int startIndex = template.indexOf(VAR_START);
        int endIndex = -1;
        while (startIndex >= 0 && startIndex < template.length()) {
            result.append(template.substring(endIndex + 1, startIndex));
            endIndex = template.indexOf(VAR_END, startIndex);
            if (endIndex > startIndex) {
                String resultObj = variables != null ? variables.get(template.substring(VAR_START.length() + startIndex, endIndex).toLowerCase()) : "";
                result.append(resultObj == null ? "" : resultObj.toString());
            }
            startIndex = template.indexOf(VAR_START, Math.max(VAR_END.length() + endIndex, startIndex + 1));
        }
        result.append(template.substring(endIndex + 1));
        return result.toString();
    }
}
