package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;

public class ContentNode extends BaseTokenImpl implements HtmlNode {
    protected final boolean blank = Utils.isEmptyString(this.content);
    protected final String content;

    public ContentNode(String content2) {
        this.content = content2;
    }

    public String getContent() {
        return this.content;
    }

    public String toString() {
        return getContent();
    }

    public void serialize(Serializer serializer, Writer writer) throws IOException {
        writer.write(this.content);
    }

    public boolean isBlank() {
        return this.blank;
    }
}
