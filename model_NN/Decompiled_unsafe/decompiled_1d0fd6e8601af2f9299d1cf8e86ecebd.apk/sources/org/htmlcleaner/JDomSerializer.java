package org.htmlcleaner;

import java.util.List;
import java.util.Map;
import org.jdom2.Content;
import org.jdom2.DefaultJDOMFactory;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMConstants;
import org.jdom2.Namespace;

public class JDomSerializer {
    protected boolean escapeXml;
    private DefaultJDOMFactory factory;
    protected CleanerProperties props;

    public JDomSerializer(CleanerProperties props2, boolean escapeXml2) {
        this.escapeXml = true;
        this.props = props2;
        this.escapeXml = escapeXml2;
    }

    public JDomSerializer(CleanerProperties props2) {
        this(props2, true);
    }

    public Document createJDom(TagNode rootNode) {
        this.factory = new DefaultJDOMFactory();
        if (rootNode.getName() == null) {
            return null;
        }
        Element rootElement = createElement(rootNode);
        Document document = this.factory.document(rootElement);
        setAttributes(rootNode, rootElement);
        createSubnodes(rootElement, rootNode.getAllChildren());
        return document;
    }

    private Element createElement(TagNode node) {
        Element element;
        String name = node.getName();
        boolean nsAware = this.props.isNamespacesAware();
        String prefix = Utils.getXmlNSPrefix(name);
        Map<String, String> nsDeclarations = node.getNamespaceDeclarations();
        String nsURI = null;
        if (prefix != null) {
            name = Utils.getXmlName(name);
            if (nsAware) {
                if (nsDeclarations != null) {
                    nsURI = nsDeclarations.get(prefix);
                }
                if (nsURI == null) {
                    nsURI = node.getNamespaceURIOnPath(prefix);
                }
                if (nsURI == null) {
                    nsURI = prefix;
                }
            }
        } else if (nsAware) {
            if (nsDeclarations != null) {
                nsURI = nsDeclarations.get("");
            }
            if (nsURI == null) {
                nsURI = node.getNamespaceURIOnPath(prefix);
            }
        }
        if (!nsAware || nsURI == null) {
            element = this.factory.element(name);
        } else {
            element = this.factory.element(name, prefix == null ? Namespace.getNamespace(nsURI) : Namespace.getNamespace(prefix, nsURI));
        }
        if (nsAware) {
            defineNamespaceDeclarations(node, element);
        }
        return element;
    }

    private void defineNamespaceDeclarations(TagNode node, Element element) {
        Namespace ns;
        Map<String, String> nsDeclarations = node.getNamespaceDeclarations();
        if (nsDeclarations != null) {
            for (Map.Entry<String, String> nsEntry : nsDeclarations.entrySet()) {
                String nsPrefix = (String) nsEntry.getKey();
                String nsURI = (String) nsEntry.getValue();
                if (nsPrefix == null || "".equals(nsPrefix)) {
                    ns = Namespace.getNamespace(nsURI);
                } else {
                    ns = Namespace.getNamespace(nsPrefix, nsURI);
                }
                element.addNamespaceDeclaration(ns);
            }
        }
    }

    private void setAttributes(TagNode node, Element element) {
        for (Map.Entry<String, String> entry : node.getAttributes().entrySet()) {
            String attrName = (String) entry.getKey();
            String attrValue = (String) entry.getValue();
            if (this.escapeXml) {
                attrValue = Utils.escapeXml(attrValue, this.props, true);
            }
            String attPrefix = Utils.getXmlNSPrefix(attrName);
            Namespace ns = null;
            if (attPrefix != null) {
                attrName = Utils.getXmlName(attrName);
                if (this.props.isNamespacesAware()) {
                    String nsURI = node.getNamespaceURIOnPath(attPrefix);
                    if (nsURI == null) {
                        nsURI = attPrefix;
                    }
                    if (!attPrefix.startsWith(JDOMConstants.NS_PREFIX_XML)) {
                        ns = Namespace.getNamespace(attPrefix, nsURI);
                    }
                }
            }
            if (!attrName.equals("xmlns")) {
                if (ns == null) {
                    element.setAttribute(attrName, attrValue);
                } else {
                    element.setAttribute(attrName, attrValue, ns);
                }
            }
        }
    }

    private void createSubnodes(Element element, List<? extends BaseToken> tagChildren) {
        if (tagChildren != null) {
            for (Object next : tagChildren) {
                if (next instanceof CommentNode) {
                    element.addContent((Content) this.factory.comment(((CommentNode) next).getContent().toString()));
                } else if (next instanceof ContentNode) {
                    String nodeName = element.getName();
                    String content = next.toString();
                    boolean specialCase = this.props.isUseCdataFor(nodeName);
                    if (this.escapeXml && !specialCase) {
                        content = Utils.escapeXml(content, this.props, true);
                    }
                    if (specialCase && (next instanceof CData)) {
                        content = ((CData) next).getContentWithoutStartAndEndTokens();
                    }
                    element.addContent((Content) (specialCase ? this.factory.cdata(content) : this.factory.text(content)));
                } else if (next instanceof TagNode) {
                    TagNode subTagNode = (TagNode) next;
                    Element subelement = createElement(subTagNode);
                    setAttributes(subTagNode, subelement);
                    createSubnodes(subelement, subTagNode.getAllChildren());
                    element.addContent((Content) subelement);
                } else if (next instanceof List) {
                    createSubnodes(element, (List) next);
                }
            }
        }
    }
}
