package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;

public class SimpleHtmlSerializer extends HtmlSerializer {
    public SimpleHtmlSerializer(CleanerProperties props) {
        super(props);
    }

    /* access modifiers changed from: protected */
    public void serialize(TagNode tagNode, Writer writer) throws IOException {
        serializeOpenTag(tagNode, writer, false);
        if (!isMinimizedTagSyntax(tagNode)) {
            for (Object item : tagNode.getAllChildren()) {
                if (item instanceof ContentNode) {
                    String content = item.toString();
                    if (!dontEscape(tagNode)) {
                        content = escapeText(content);
                    }
                    writer.write(content);
                } else if (item instanceof BaseToken) {
                    ((BaseToken) item).serialize(this, writer);
                }
            }
            serializeEndTag(tagNode, writer, false);
        }
    }
}
