package org.htmlcleaner;

public enum ContentType {
    all("all"),
    none("none"),
    text("text");
    
    private final String dbCode;

    private ContentType(String dbCode2) {
        this.dbCode = dbCode2;
    }

    public String getDbCode() {
        return this.dbCode;
    }

    public static ContentType toValue(Object value) {
        if (value instanceof ContentType) {
            return (ContentType) value;
        }
        if (value == null) {
            return null;
        }
        String dbCode2 = value.toString().trim();
        for (ContentType contentType : values()) {
            if (contentType.getDbCode().equalsIgnoreCase(dbCode2) || contentType.name().equalsIgnoreCase(dbCode2)) {
                return contentType;
            }
        }
        return null;
    }
}
