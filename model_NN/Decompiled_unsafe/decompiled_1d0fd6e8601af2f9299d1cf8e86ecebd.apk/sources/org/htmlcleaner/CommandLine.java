package org.htmlcleaner;

import java.util.logging.Logger;

public class CommandLine {
    private static final String OMITXMLDECL = "omitxmldecl";
    private static final String className = CommandLine.class.getName();
    private static final Logger logger = Logger.getLogger(className);

    private static boolean getSwitchArgument(String[] args, String name) {
        boolean value = false;
        for (String curr : args) {
            int eqIndex = curr.indexOf(61);
            if (eqIndex >= 0) {
                String argName = curr.substring(0, eqIndex).trim();
                String argValue = curr.substring(eqIndex + 1).trim();
                if (argName.toLowerCase().startsWith(name.toLowerCase())) {
                    value = toBoolean(argValue);
                }
            } else {
                value = true;
            }
        }
        return value;
    }

    private static String getArgValue(String[] args, String name, String defaultValue) {
        for (String curr : args) {
            int eqIndex = curr.indexOf(61);
            if (eqIndex >= 0) {
                String argName = curr.substring(0, eqIndex).trim();
                String argValue = curr.substring(eqIndex + 1).trim();
                if (argName.toLowerCase().startsWith(name.toLowerCase())) {
                    return argValue;
                }
            }
        }
        return defaultValue;
    }

    private static boolean toBoolean(String s) {
        return s != null && ("on".equalsIgnoreCase(s) || "true".equalsIgnoreCase(s) || "yes".equalsIgnoreCase(s));
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r28v0 */
    /* JADX WARN: Type inference failed for: r28v2 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void main(java.lang.String[] r54) throws java.io.IOException, org.htmlcleaner.XPatherException {
        /*
            java.lang.String r50 = "src"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r35 = getArgValue(r0, r1, r2)
            java.util.Scanner r34 = new java.util.Scanner
            java.io.InputStream r50 = java.lang.System.in
            r0 = r34
            r1 = r50
            r0.<init>(r1)
            java.lang.String r33 = ""
            java.lang.String r50 = ""
            r0 = r50
            r1 = r35
            boolean r50 = r0.equals(r1)
            if (r50 == 0) goto L_0x005a
        L_0x0027:
            boolean r50 = r34.hasNext()
            if (r50 == 0) goto L_0x0047
            java.lang.StringBuilder r50 = new java.lang.StringBuilder
            r50.<init>()
            r0 = r50
            r1 = r33
            java.lang.StringBuilder r50 = r0.append(r1)
            java.lang.String r51 = r34.nextLine()
            java.lang.StringBuilder r50 = r50.append(r51)
            java.lang.String r33 = r50.toString()
            goto L_0x0027
        L_0x0047:
            java.lang.String r50 = ""
            r0 = r33
            r1 = r50
            int r50 = r0.compareTo(r1)
            if (r50 == 0) goto L_0x0457
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "Output:"
            r50.println(r51)
        L_0x005a:
            java.lang.String r50 = "incharset"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r16 = getArgValue(r0, r1, r2)
            java.lang.String r50 = ""
            r0 = r50
            r1 = r16
            boolean r50 = r0.equals(r1)
            if (r50 == 0) goto L_0x0076
            java.lang.String r16 = "UTF-8"
        L_0x0076:
            java.lang.String r50 = "outcharset"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r29 = getArgValue(r0, r1, r2)
            java.lang.String r50 = ""
            r0 = r50
            r1 = r29
            boolean r50 = r0.equals(r1)
            if (r50 == 0) goto L_0x0092
            java.lang.String r29 = "UTF-8"
        L_0x0092:
            java.lang.String r50 = "htmlver"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r13 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "dest"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r12 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "outputtype"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r30 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "advancedxmlescape"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r4 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "usecdata"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r45 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "usecdatafor"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r46 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "specialentities"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r41 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "unicodechars"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r44 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "omitunknowntags"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r26 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "treatunknowntagsascontent"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r43 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "omitdeprtags"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r23 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "treatdeprtagsascontent"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r42 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "omitcomments"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r22 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "omitxmldecl"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r27 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "omitdoctypedecl"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r24 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "omithtmlenvelope"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r25 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "useemptyelementtags"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r47 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "allowmultiwordattributes"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r6 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "allowhtmlinsideattributes"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r5 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "ignoreqe"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r15 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "namespacesaware"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r19 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "hyphenreplacement"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r11 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "prunetags"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r32 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "booleanatts"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r9 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "nodebyxpath"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r21 = getArgValue(r0, r1, r2)
            java.lang.String r50 = "taginfofile"
            java.lang.String r51 = ""
            r0 = r54
            r1 = r50
            r2 = r51
            java.lang.String r37 = getArgValue(r0, r1, r2)
            java.lang.String r50 = ""
            r0 = r50
            r1 = r37
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x0529
            org.htmlcleaner.HtmlCleaner r10 = new org.htmlcleaner.HtmlCleaner
            org.htmlcleaner.ConfigFileTagProvider r50 = new org.htmlcleaner.ConfigFileTagProvider
            java.io.File r51 = new java.io.File
            r0 = r51
            r1 = r37
            r0.<init>(r1)
            r50.<init>(r51)
            r0 = r50
            r10.<init>(r0)
        L_0x021f:
            org.htmlcleaner.CleanerProperties r31 = r10.getProperties()
            java.lang.String r50 = "quiet"
            r0 = r54
            r1 = r50
            boolean r50 = getSwitchArgument(r0, r1)
            if (r50 != 0) goto L_0x023d
            org.htmlcleaner.audit.HtmlModificationListenerLogger r50 = new org.htmlcleaner.audit.HtmlModificationListenerLogger
            java.util.logging.Logger r51 = org.htmlcleaner.CommandLine.logger
            r50.<init>(r51)
            r0 = r31
            r1 = r50
            r0.addHtmlModificationListener(r1)
        L_0x023d:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r26
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x0254
            boolean r50 = toBoolean(r26)
            r0 = r31
            r1 = r50
            r0.setOmitUnknownTags(r1)
        L_0x0254:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r43
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x026b
            boolean r50 = toBoolean(r43)
            r0 = r31
            r1 = r50
            r0.setTreatUnknownTagsAsContent(r1)
        L_0x026b:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r23
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x0282
            boolean r50 = toBoolean(r23)
            r0 = r31
            r1 = r50
            r0.setOmitDeprecatedTags(r1)
        L_0x0282:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r42
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x0299
            boolean r50 = toBoolean(r42)
            r0 = r31
            r1 = r50
            r0.setTreatDeprecatedTagsAsContent(r1)
        L_0x0299:
            java.lang.String r50 = ""
            r0 = r50
            boolean r50 = r0.equals(r4)
            if (r50 != 0) goto L_0x02ae
            boolean r50 = toBoolean(r4)
            r0 = r31
            r1 = r50
            r0.setAdvancedXmlEscape(r1)
        L_0x02ae:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r45
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x02d1
            java.lang.String r50 = ""
            r0 = r50
            r1 = r46
            boolean r50 = r0.equals(r1)
            if (r50 == 0) goto L_0x02d1
            boolean r50 = toBoolean(r45)
            r0 = r31
            r1 = r50
            r0.setUseCdataForScriptAndStyle(r1)
        L_0x02d1:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r46
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x02e4
            r0 = r31
            r1 = r46
            r0.setUseCdataFor(r1)
        L_0x02e4:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r41
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x02fb
            boolean r50 = toBoolean(r41)
            r0 = r31
            r1 = r50
            r0.setTranslateSpecialEntities(r1)
        L_0x02fb:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r44
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x0312
            boolean r50 = toBoolean(r44)
            r0 = r31
            r1 = r50
            r0.setRecognizeUnicodeChars(r1)
        L_0x0312:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r22
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x0329
            boolean r50 = toBoolean(r22)
            r0 = r31
            r1 = r50
            r0.setOmitComments(r1)
        L_0x0329:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r27
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x0340
            boolean r50 = toBoolean(r27)
            r0 = r31
            r1 = r50
            r0.setOmitXmlDeclaration(r1)
        L_0x0340:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r24
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x0357
            boolean r50 = toBoolean(r24)
            r0 = r31
            r1 = r50
            r0.setOmitDoctypeDeclaration(r1)
        L_0x0357:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r25
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x036e
            boolean r50 = toBoolean(r25)
            r0 = r31
            r1 = r50
            r0.setOmitHtmlEnvelope(r1)
        L_0x036e:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r47
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x0385
            boolean r50 = toBoolean(r47)
            r0 = r31
            r1 = r50
            r0.setUseEmptyElementTags(r1)
        L_0x0385:
            java.lang.String r50 = ""
            r0 = r50
            boolean r50 = r0.equals(r6)
            if (r50 != 0) goto L_0x039a
            boolean r50 = toBoolean(r6)
            r0 = r31
            r1 = r50
            r0.setAllowMultiWordAttributes(r1)
        L_0x039a:
            java.lang.String r50 = ""
            r0 = r50
            boolean r50 = r0.equals(r5)
            if (r50 != 0) goto L_0x03af
            boolean r50 = toBoolean(r5)
            r0 = r31
            r1 = r50
            r0.setAllowHtmlInsideAttributes(r1)
        L_0x03af:
            java.lang.String r50 = ""
            r0 = r50
            boolean r50 = r0.equals(r15)
            if (r50 != 0) goto L_0x03c4
            boolean r50 = toBoolean(r15)
            r0 = r31
            r1 = r50
            r0.setIgnoreQuestAndExclam(r1)
        L_0x03c4:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r19
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x03db
            boolean r50 = toBoolean(r19)
            r0 = r31
            r1 = r50
            r0.setNamespacesAware(r1)
        L_0x03db:
            java.lang.String r50 = ""
            r0 = r50
            boolean r50 = r0.equals(r11)
            if (r50 != 0) goto L_0x03ea
            r0 = r31
            r0.setHyphenReplacementInComment(r11)
        L_0x03ea:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r32
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x03f9
            r31.setPruneTags(r32)
        L_0x03f9:
            java.lang.String r50 = ""
            r0 = r50
            boolean r50 = r0.equals(r9)
            if (r50 != 0) goto L_0x0408
            r0 = r31
            r0.setBooleanAttributeValues(r9)
        L_0x0408:
            java.util.TreeMap r40 = new java.util.TreeMap
            r40.<init>()
            r0 = r54
            int r0 = r0.length
            r51 = r0
            r50 = 0
        L_0x0414:
            r0 = r50
            r1 = r51
            if (r0 >= r1) goto L_0x055f
            r8 = r54[r50]
            r7 = r8
            java.lang.String r52 = "t:"
            r0 = r52
            boolean r52 = r7.startsWith(r0)
            if (r52 == 0) goto L_0x0454
            int r52 = r7.length()
            r53 = 2
            r0 = r52
            r1 = r53
            if (r0 <= r1) goto L_0x0454
            r52 = 2
            r0 = r52
            java.lang.String r7 = r7.substring(r0)
            r52 = 61
            r0 = r52
            int r17 = r7.indexOf(r0)
            if (r17 > 0) goto L_0x0549
            r18 = r7
        L_0x0447:
            if (r17 > 0) goto L_0x0555
            r48 = 0
        L_0x044b:
            r0 = r40
            r1 = r18
            r2 = r48
            r0.put(r1, r2)
        L_0x0454:
            int r50 = r50 + 1
            goto L_0x0414
        L_0x0457:
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "Usage: java -jar htmlcleanerXX.jar src=<url | file> [htmlver=4] [incharset=<charset>] [dest=<file>] [outcharset=<charset>] [taginfofile=<file>] [options...]"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "Alternative: java -jar htmlcleanerXX.jar (reads the input from console)"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = ""
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "where options include:"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    outputtype=simple* | compact | browser-compact | pretty"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    advancedxmlescape=true* | false"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    usecdata=true* | false"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    usecdatafor=<string value> [script,style]"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    specialentities=true* | false"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    unicodechars=true* | false"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    omitunknowntags=true | false*"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    treatunknowntagsascontent=true | false*"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    omitdeprtags=true | false*"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    treatdeprtagsascontent=true | false*"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    omitcomments=true | false*"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    omitxmldecl=true* | false"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    omitdoctypedecl=true* | false"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    omithtmlenvelope=true | false*"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    useemptyelementtags=true* | false"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    allowmultiwordattributes=true* | false"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    allowhtmlinsideattributes=true | false*"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    ignoreqe=true | false*"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    namespacesaware=true* | false"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    hyphenreplacement=<string value> [=]"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    prunetags=<string value> []"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    booleanatts=self* | empty | true"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    nodebyxpath=<xpath expression>"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    t:<sourcetagX>[=<desttag>[,<preserveatts>]]"
            r50.println(r51)
            java.io.PrintStream r50 = java.lang.System.err
            java.lang.String r51 = "    t:<sourcetagX>.<destattrY>[=<template>]"
            r50.println(r51)
            r50 = 1
            java.lang.System.exit(r50)
            goto L_0x005a
        L_0x0529:
            java.lang.String r50 = "4"
            r0 = r50
            int r50 = r13.compareTo(r0)
            if (r50 != 0) goto L_0x053e
            org.htmlcleaner.HtmlCleaner r10 = new org.htmlcleaner.HtmlCleaner
            org.htmlcleaner.Html4TagProvider r50 = org.htmlcleaner.Html4TagProvider.INSTANCE
            r0 = r50
            r10.<init>(r0)
            goto L_0x021f
        L_0x053e:
            org.htmlcleaner.HtmlCleaner r10 = new org.htmlcleaner.HtmlCleaner
            org.htmlcleaner.Html5TagProvider r50 = org.htmlcleaner.Html5TagProvider.INSTANCE
            r0 = r50
            r10.<init>(r0)
            goto L_0x021f
        L_0x0549:
            r52 = 0
            r0 = r52
            r1 = r17
            java.lang.String r18 = r7.substring(r0, r1)
            goto L_0x0447
        L_0x0555:
            int r52 = r17 + 1
            r0 = r52
            java.lang.String r48 = r7.substring(r0)
            goto L_0x044b
        L_0x055f:
            r0 = r40
            r10.initCleanerTransformations(r0)
            long r38 = java.lang.System.currentTimeMillis()
            java.lang.String r36 = r35.toLowerCase()
            java.lang.String r50 = "http://"
            r0 = r36
            r1 = r50
            boolean r50 = r0.startsWith(r1)
            if (r50 != 0) goto L_0x0584
            java.lang.String r50 = "https://"
            r0 = r36
            r1 = r50
            boolean r50 = r0.startsWith(r1)
            if (r50 == 0) goto L_0x0635
        L_0x0584:
            java.net.URL r50 = new java.net.URL
            r0 = r50
            r1 = r36
            r0.<init>(r1)
            r0 = r50
            r1 = r16
            org.htmlcleaner.TagNode r20 = r10.clean(r0, r1)
        L_0x0595:
            java.lang.String r50 = ""
            r0 = r50
            r1 = r21
            boolean r50 = r0.equals(r1)
            if (r50 != 0) goto L_0x05d4
            java.lang.Object[] r49 = r20.evaluateXPath(r21)
            r14 = 0
        L_0x05a6:
            r0 = r49
            int r0 = r0.length
            r50 = r0
            r0 = r50
            if (r14 >= r0) goto L_0x05c4
            r50 = r49[r14]
            r0 = r50
            boolean r0 = r0 instanceof org.htmlcleaner.TagNode
            r50 = r0
            if (r50 == 0) goto L_0x0656
            r20 = r49[r14]
            org.htmlcleaner.TagNode r20 = (org.htmlcleaner.TagNode) r20
            java.io.PrintStream r50 = java.lang.System.out
            java.lang.String r51 = "Node successfully found by XPath."
            r50.println(r51)
        L_0x05c4:
            r0 = r49
            int r0 = r0.length
            r50 = r0
            r0 = r50
            if (r14 != r0) goto L_0x05d4
            java.io.PrintStream r50 = java.lang.System.out
            java.lang.String r51 = "Node not found by XPath expression - whole html tree is going to be serialized!"
            r50.println(r51)
        L_0x05d4:
            if (r12 == 0) goto L_0x05e2
            java.lang.String r50 = ""
            java.lang.String r51 = r12.trim()
            boolean r50 = r50.equals(r51)
            if (r50 == 0) goto L_0x065a
        L_0x05e2:
            java.io.PrintStream r28 = java.lang.System.out
        L_0x05e4:
            java.lang.String r50 = "compact"
            r0 = r50
            r1 = r30
            boolean r50 = r0.equals(r1)
            if (r50 == 0) goto L_0x0662
            org.htmlcleaner.CompactXmlSerializer r50 = new org.htmlcleaner.CompactXmlSerializer
            r0 = r50
            r1 = r31
            r0.<init>(r1)
            r0 = r50
            r1 = r20
            r2 = r28
            r3 = r29
            r0.writeToStream(r1, r2, r3)
        L_0x0604:
            java.lang.String r50 = "quiet"
            r0 = r54
            r1 = r50
            boolean r50 = getSwitchArgument(r0, r1)
            if (r50 != 0) goto L_0x0634
            java.io.PrintStream r50 = java.lang.System.out
            java.lang.StringBuilder r51 = new java.lang.StringBuilder
            r51.<init>()
            java.lang.String r52 = "Finished successfully in "
            java.lang.StringBuilder r51 = r51.append(r52)
            long r52 = java.lang.System.currentTimeMillis()
            long r52 = r52 - r38
            java.lang.StringBuilder r51 = r51.append(r52)
            java.lang.String r52 = "ms."
            java.lang.StringBuilder r51 = r51.append(r52)
            java.lang.String r51 = r51.toString()
            r50.println(r51)
        L_0x0634:
            return
        L_0x0635:
            boolean r50 = r35.isEmpty()
            if (r50 != 0) goto L_0x064e
            java.io.File r50 = new java.io.File
            r0 = r50
            r1 = r35
            r0.<init>(r1)
            r0 = r50
            r1 = r16
            org.htmlcleaner.TagNode r20 = r10.clean(r0, r1)
            goto L_0x0595
        L_0x064e:
            r0 = r33
            org.htmlcleaner.TagNode r20 = r10.clean(r0)
            goto L_0x0595
        L_0x0656:
            int r14 = r14 + 1
            goto L_0x05a6
        L_0x065a:
            java.io.FileOutputStream r28 = new java.io.FileOutputStream
            r0 = r28
            r0.<init>(r12)
            goto L_0x05e4
        L_0x0662:
            java.lang.String r50 = "browser-compact"
            r0 = r50
            r1 = r30
            boolean r50 = r0.equals(r1)
            if (r50 == 0) goto L_0x0683
            org.htmlcleaner.BrowserCompactXmlSerializer r50 = new org.htmlcleaner.BrowserCompactXmlSerializer
            r0 = r50
            r1 = r31
            r0.<init>(r1)
            r0 = r50
            r1 = r20
            r2 = r28
            r3 = r29
            r0.writeToStream(r1, r2, r3)
            goto L_0x0604
        L_0x0683:
            java.lang.String r50 = "pretty"
            r0 = r50
            r1 = r30
            boolean r50 = r0.equals(r1)
            if (r50 == 0) goto L_0x06a5
            org.htmlcleaner.PrettyXmlSerializer r50 = new org.htmlcleaner.PrettyXmlSerializer
            r0 = r50
            r1 = r31
            r0.<init>(r1)
            r0 = r50
            r1 = r20
            r2 = r28
            r3 = r29
            r0.writeToStream(r1, r2, r3)
            goto L_0x0604
        L_0x06a5:
            java.lang.String r50 = "htmlsimple"
            r0 = r50
            r1 = r30
            boolean r50 = r0.equals(r1)
            if (r50 == 0) goto L_0x06c7
            org.htmlcleaner.SimpleHtmlSerializer r50 = new org.htmlcleaner.SimpleHtmlSerializer
            r0 = r50
            r1 = r31
            r0.<init>(r1)
            r0 = r50
            r1 = r20
            r2 = r28
            r3 = r29
            r0.writeToStream(r1, r2, r3)
            goto L_0x0604
        L_0x06c7:
            java.lang.String r50 = "htmlpretty"
            r0 = r50
            r1 = r30
            boolean r50 = r0.equals(r1)
            if (r50 == 0) goto L_0x06e9
            org.htmlcleaner.PrettyHtmlSerializer r50 = new org.htmlcleaner.PrettyHtmlSerializer
            r0 = r50
            r1 = r31
            r0.<init>(r1)
            r0 = r50
            r1 = r20
            r2 = r28
            r3 = r29
            r0.writeToStream(r1, r2, r3)
            goto L_0x0604
        L_0x06e9:
            java.lang.String r50 = "htmlcompact"
            r0 = r50
            r1 = r30
            boolean r50 = r0.equals(r1)
            if (r50 == 0) goto L_0x070b
            org.htmlcleaner.CompactHtmlSerializer r50 = new org.htmlcleaner.CompactHtmlSerializer
            r0 = r50
            r1 = r31
            r0.<init>(r1)
            r0 = r50
            r1 = r20
            r2 = r28
            r3 = r29
            r0.writeToStream(r1, r2, r3)
            goto L_0x0604
        L_0x070b:
            org.htmlcleaner.SimpleXmlSerializer r50 = new org.htmlcleaner.SimpleXmlSerializer
            r0 = r50
            r1 = r31
            r0.<init>(r1)
            r0 = r50
            r1 = r20
            r2 = r28
            r3 = r29
            r0.writeToStream(r1, r2, r3)
            goto L_0x0604
        */
        throw new UnsupportedOperationException("Method not decompiled: org.htmlcleaner.CommandLine.main(java.lang.String[]):void");
    }
}
