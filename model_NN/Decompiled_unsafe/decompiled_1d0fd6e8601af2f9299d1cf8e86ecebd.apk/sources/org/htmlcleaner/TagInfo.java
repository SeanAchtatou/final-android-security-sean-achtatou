package org.htmlcleaner;

import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

public class TagInfo {
    private BelongsTo belongsTo = BelongsTo.BODY;
    private Set<String> childTags = new HashSet();
    private CloseTag closeTag;
    private ContentType contentType;
    private Set<String> continueAfterTags = new HashSet();
    private Set<String> copyTags = new HashSet();
    private boolean deprecated;
    private Display display;
    private Set<String> fatalTags = new HashSet();
    private Set<String> higherTags = new HashSet();
    private boolean ignorePermitted;
    private Set<String> mustCloseTags = new HashSet();
    private String name;
    private Set<String> permittedTags = new HashSet();
    private Set<String> requiredParentTags = new HashSet();
    private boolean unique;

    public TagInfo(String name2, ContentType contentType2, BelongsTo belongsTo2, boolean deprecated2, boolean unique2, boolean ignorePermitted2, CloseTag closeTag2, Display display2) {
        this.name = name2;
        this.contentType = contentType2;
        this.belongsTo = belongsTo2;
        this.deprecated = deprecated2;
        this.unique = unique2;
        this.ignorePermitted = ignorePermitted2;
        this.closeTag = closeTag2;
        this.display = display2;
    }

    public void defineFatalTags(String commaSeparatedListOfTags) {
        StringTokenizer tokenizer = new StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            String currTag = tokenizer.nextToken();
            this.fatalTags.add(currTag);
            this.higherTags.add(currTag);
        }
    }

    public void defineRequiredEnclosingTags(String commaSeparatedListOfTags) {
        StringTokenizer tokenizer = new StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            String currTag = tokenizer.nextToken();
            this.requiredParentTags.add(currTag);
            this.higherTags.add(currTag);
        }
    }

    public void defineForbiddenTags(String commaSeparatedListOfTags) {
        StringTokenizer tokenizer = new StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            this.permittedTags.add(tokenizer.nextToken());
        }
    }

    public void defineAllowedChildrenTags(String commaSeparatedListOfTags) {
        StringTokenizer tokenizer = new StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            this.childTags.add(tokenizer.nextToken());
        }
    }

    public void defineHigherLevelTags(String commaSeparatedListOfTags) {
        StringTokenizer tokenizer = new StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            this.higherTags.add(tokenizer.nextToken());
        }
    }

    public void defineCloseBeforeCopyInsideTags(String commaSeparatedListOfTags) {
        StringTokenizer tokenizer = new StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            String currTag = tokenizer.nextToken();
            this.copyTags.add(currTag);
            this.mustCloseTags.add(currTag);
        }
    }

    public void defineCloseInsideCopyAfterTags(String commaSeparatedListOfTags) {
        StringTokenizer tokenizer = new StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            this.continueAfterTags.add(tokenizer.nextToken());
        }
    }

    public void defineCloseBeforeTags(String commaSeparatedListOfTags) {
        StringTokenizer tokenizer = new StringTokenizer(commaSeparatedListOfTags.toLowerCase(), ",");
        while (tokenizer.hasMoreTokens()) {
            this.mustCloseTags.add(tokenizer.nextToken());
        }
    }

    public Display getDisplay() {
        return this.display;
    }

    public void setDisplay(Display display2) {
        this.display = display2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public ContentType getContentType() {
        return this.contentType;
    }

    public Set<String> getMustCloseTags() {
        return this.mustCloseTags;
    }

    public void setMustCloseTags(Set<String> mustCloseTags2) {
        this.mustCloseTags = mustCloseTags2;
    }

    public Set<String> getHigherTags() {
        return this.higherTags;
    }

    public void setHigherTags(Set<String> higherTags2) {
        this.higherTags = higherTags2;
    }

    public Set<String> getChildTags() {
        return this.childTags;
    }

    public void setChildTags(Set<String> childTags2) {
        this.childTags = childTags2;
    }

    public Set<String> getPermittedTags() {
        return this.permittedTags;
    }

    public void setPermittedTags(Set<String> permittedTags2) {
        this.permittedTags = permittedTags2;
    }

    public Set<String> getCopyTags() {
        return this.copyTags;
    }

    public void setCopyTags(Set<String> copyTags2) {
        this.copyTags = copyTags2;
    }

    public Set<String> getContinueAfterTags() {
        return this.continueAfterTags;
    }

    public void setContinueAfterTags(Set<String> continueAfterTags2) {
        this.continueAfterTags = continueAfterTags2;
    }

    public Set<String> getRequiredParentTags() {
        return this.requiredParentTags;
    }

    public void setRequiredParent(String requiredParent) {
        this.requiredParentTags.add(requiredParent);
    }

    public BelongsTo getBelongsTo() {
        return this.belongsTo;
    }

    public void setBelongsTo(BelongsTo belongsTo2) {
        this.belongsTo = belongsTo2;
    }

    public Set<String> getFatalTags() {
        return this.fatalTags;
    }

    public boolean isFatalTag(String tag) {
        for (String fatalTag : this.fatalTags) {
            if (tag.equals(fatalTag)) {
                return true;
            }
        }
        return false;
    }

    public void setFatalTag(String fatalTag) {
        this.fatalTags.add(fatalTag);
    }

    public boolean isDeprecated() {
        return this.deprecated;
    }

    public void setDeprecated(boolean deprecated2) {
        this.deprecated = deprecated2;
    }

    public boolean isUnique() {
        return this.unique;
    }

    public void setUnique(boolean unique2) {
        this.unique = unique2;
    }

    public boolean isIgnorePermitted() {
        return this.ignorePermitted;
    }

    public boolean isEmptyTag() {
        return ContentType.none == this.contentType;
    }

    public void setIgnorePermitted(boolean ignorePermitted2) {
        this.ignorePermitted = ignorePermitted2;
    }

    /* access modifiers changed from: package-private */
    public boolean allowsBody() {
        return ContentType.none != this.contentType;
    }

    /* access modifiers changed from: package-private */
    public boolean isHigher(String tagName) {
        return this.higherTags.contains(tagName);
    }

    /* access modifiers changed from: package-private */
    public boolean isCopy(String tagName) {
        return this.copyTags.contains(tagName);
    }

    /* access modifiers changed from: package-private */
    public boolean hasCopyTags() {
        return !this.copyTags.isEmpty();
    }

    /* access modifiers changed from: package-private */
    public boolean isContinueAfter(String tagName) {
        return this.continueAfterTags.contains(tagName);
    }

    /* access modifiers changed from: package-private */
    public boolean hasPermittedTags() {
        return !this.permittedTags.isEmpty();
    }

    /* access modifiers changed from: package-private */
    public boolean isHeadTag() {
        return this.belongsTo == BelongsTo.HEAD;
    }

    /* access modifiers changed from: package-private */
    public boolean isHeadAndBodyTag() {
        return this.belongsTo == BelongsTo.HEAD || this.belongsTo == BelongsTo.HEAD_AND_BODY;
    }

    /* access modifiers changed from: package-private */
    public boolean isMustCloseTag(TagInfo tagInfo) {
        if (tagInfo == null) {
            return false;
        }
        if (this.mustCloseTags.contains(tagInfo.getName()) || tagInfo.contentType == ContentType.text) {
            return true;
        }
        return false;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: package-private */
    public boolean allowsItem(BaseToken token) {
        if (this.contentType != ContentType.none && (token instanceof TagToken) && "script".equals(((TagToken) token).getName())) {
            return true;
        }
        switch (this.contentType) {
            case all:
                if (!this.childTags.isEmpty()) {
                    if (token instanceof TagToken) {
                        return this.childTags.contains(((TagToken) token).getName());
                    }
                    return true;
                } else if (this.permittedTags.isEmpty() || !(token instanceof TagToken) || !this.permittedTags.contains(((TagToken) token).getName())) {
                    return true;
                } else {
                    return false;
                }
            case text:
                if (token instanceof TagToken) {
                    return false;
                }
                return true;
            case none:
                if (token instanceof ContentNode) {
                    return ((ContentNode) token).isBlank();
                }
                if (!(token instanceof TagToken)) {
                    return true;
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean allowsAnything() {
        return ContentType.all == this.contentType && this.childTags.isEmpty();
    }

    public boolean isMinimizedTagPermitted() {
        return this.closeTag.isMinimizedTagPermitted();
    }
}
