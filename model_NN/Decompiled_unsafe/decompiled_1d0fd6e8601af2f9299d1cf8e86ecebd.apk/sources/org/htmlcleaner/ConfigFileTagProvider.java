package org.htmlcleaner;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ConfigFileTagProvider extends HashMap implements ITagInfoProvider {
    static SAXParserFactory parserFactory = SAXParserFactory.newInstance();
    /* access modifiers changed from: private */
    public boolean generateCode = false;

    static {
        parserFactory.setValidating(false);
        parserFactory.setNamespaceAware(false);
    }

    private ConfigFileTagProvider() {
    }

    public ConfigFileTagProvider(InputSource inputSource) {
        try {
            new ConfigParser(this).parse(inputSource);
        } catch (Exception e) {
            throw new HtmlCleanerException("Error parsing tag configuration file!", e);
        }
    }

    public ConfigFileTagProvider(File file) {
        try {
            new ConfigParser(this).parse(new InputSource(new FileReader(file)));
        } catch (Exception e) {
            throw new HtmlCleanerException("Error parsing tag configuration file!", e);
        }
    }

    public ConfigFileTagProvider(URL url) {
        try {
            Object content = url.getContent();
            if (content instanceof InputStream) {
                new ConfigParser(this).parse(new InputSource(new InputStreamReader((InputStream) content)));
            }
        } catch (Exception e) {
            throw new HtmlCleanerException("Error parsing tag configuration file!", e);
        }
    }

    public TagInfo getTagInfo(String tagName) {
        return (TagInfo) get(tagName);
    }

    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {
        ConfigFileTagProvider provider = new ConfigFileTagProvider();
        provider.generateCode = true;
        String fileName = "default.xml";
        if (args != null && args.length > 0) {
            fileName = args[0];
        }
        File configFile = new File(fileName);
        provider.getClass();
        ConfigParser parser = new ConfigParser(provider);
        System.out.println("package " + "org.htmlcleaner" + ";");
        System.out.println("import java.util.HashMap;");
        System.out.println("public class " + "CustomTagProvider" + " extends HashMap implements ITagInfoProvider {");
        System.out.println("private ConcurrentMap<String, TagInfo> tagInfoMap = new ConcurrentHashMap<String, TagInfo>();");
        System.out.println("// singleton instance, used if no other TagInfoProvider is specified");
        System.out.println("public final static " + "CustomTagProvider" + " INSTANCE= new " + "CustomTagProvider" + "();");
        System.out.println("public " + "CustomTagProvider" + "() {");
        System.out.println("TagInfo tagInfo;");
        parser.parse(new InputSource(new FileReader(configFile)));
        System.out.println("}");
        System.out.println("}");
    }

    private class ConfigParser extends DefaultHandler {
        private String dependencyName = null;
        private TagInfo tagInfo = null;
        private Map tagInfoMap;

        ConfigParser(Map tagInfoMap2) {
            this.tagInfoMap = tagInfoMap2;
        }

        public void parse(InputSource in) throws ParserConfigurationException, SAXException, IOException {
            ConfigFileTagProvider.parserFactory.newSAXParser().parse(in, this);
        }

        public void characters(char[] ch, int start, int length) throws SAXException {
            if (this.tagInfo != null) {
                String value = new String(ch, start, length).trim();
                if ("fatal-tags".equals(this.dependencyName)) {
                    this.tagInfo.defineFatalTags(value);
                    if (ConfigFileTagProvider.this.generateCode) {
                        System.out.println("tagInfo.defineFatalTags(\"" + value + "\");");
                    }
                } else if ("req-enclosing-tags".equals(this.dependencyName)) {
                    this.tagInfo.defineRequiredEnclosingTags(value);
                    if (ConfigFileTagProvider.this.generateCode) {
                        System.out.println("tagInfo.defineRequiredEnclosingTags(\"" + value + "\");");
                    }
                } else if ("forbidden-tags".equals(this.dependencyName)) {
                    this.tagInfo.defineForbiddenTags(value);
                    if (ConfigFileTagProvider.this.generateCode) {
                        System.out.println("tagInfo.defineForbiddenTags(\"" + value + "\");");
                    }
                } else if ("allowed-children-tags".equals(this.dependencyName)) {
                    this.tagInfo.defineAllowedChildrenTags(value);
                    if (ConfigFileTagProvider.this.generateCode) {
                        System.out.println("tagInfo.defineAllowedChildrenTags(\"" + value + "\");");
                    }
                } else if ("higher-level-tags".equals(this.dependencyName)) {
                    this.tagInfo.defineHigherLevelTags(value);
                    if (ConfigFileTagProvider.this.generateCode) {
                        System.out.println("tagInfo.defineHigherLevelTags(\"" + value + "\");");
                    }
                } else if ("close-before-copy-inside-tags".equals(this.dependencyName)) {
                    this.tagInfo.defineCloseBeforeCopyInsideTags(value);
                    if (ConfigFileTagProvider.this.generateCode) {
                        System.out.println("tagInfo.defineCloseBeforeCopyInsideTags(\"" + value + "\");");
                    }
                } else if ("close-inside-copy-after-tags".equals(this.dependencyName)) {
                    this.tagInfo.defineCloseInsideCopyAfterTags(value);
                    if (ConfigFileTagProvider.this.generateCode) {
                        System.out.println("tagInfo.defineCloseInsideCopyAfterTags(\"" + value + "\");");
                    }
                } else if ("close-before-tags".equals(this.dependencyName)) {
                    this.tagInfo.defineCloseBeforeTags(value);
                    if (ConfigFileTagProvider.this.generateCode) {
                        System.out.println("tagInfo.defineCloseBeforeTags(\"" + value + "\");");
                    }
                }
            }
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if ("tag".equals(qName)) {
                String name = attributes.getValue("name");
                String content = attributes.getValue("content");
                String section = attributes.getValue("section");
                String deprecated = attributes.getValue("deprecated");
                String unique = attributes.getValue("unique");
                String ignorePermitted = attributes.getValue("ignore-permitted");
                ContentType contentType = ContentType.toValue(content);
                BelongsTo belongsTo = BelongsTo.toValue(section);
                this.tagInfo = new TagInfo(name, contentType, belongsTo, deprecated != null && "true".equals(deprecated), unique != null && "true".equals(unique), ignorePermitted != null && "true".equals(ignorePermitted), CloseTag.required, Display.any);
                if (ConfigFileTagProvider.this.generateCode) {
                    System.out.println("tagInfo = new TagInfo(\"#1\", #2, #3, #4, #5, #6);".replaceAll("#1", name).replaceAll("#2", ContentType.class.getCanonicalName() + "." + contentType.name()).replaceAll("#3", BelongsTo.class.getCanonicalName() + "." + belongsTo.name()).replaceAll("#4", Boolean.toString(deprecated != null && "true".equals(deprecated))).replaceAll("#5", Boolean.toString(unique != null && "true".equals(unique))).replaceAll("#6", Boolean.toString(ignorePermitted != null && "true".equals(ignorePermitted))));
                }
            } else if (!"tags".equals(qName)) {
                this.dependencyName = qName;
            }
        }

        public void endElement(String uri, String localName, String qName) throws SAXException {
            if ("tag".equals(qName)) {
                if (this.tagInfo != null) {
                    this.tagInfoMap.put(this.tagInfo.getName(), this.tagInfo);
                    if (ConfigFileTagProvider.this.generateCode) {
                        System.out.println("this.put(\"" + this.tagInfo.getName() + "\", tagInfo);\n");
                    }
                }
                this.tagInfo = null;
            } else if (!"tags".equals(qName)) {
                this.dependencyName = null;
            }
        }
    }
}
