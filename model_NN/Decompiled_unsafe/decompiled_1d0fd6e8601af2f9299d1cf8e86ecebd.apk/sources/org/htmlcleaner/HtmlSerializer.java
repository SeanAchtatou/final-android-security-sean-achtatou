package org.htmlcleaner;

import com.fsck.k9.Account;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;
import org.apache.commons.io.IOUtils;

public abstract class HtmlSerializer extends Serializer {
    protected HtmlSerializer(CleanerProperties props) {
        super(props);
    }

    /* access modifiers changed from: protected */
    public boolean isMinimizedTagSyntax(TagNode tagNode) {
        TagInfo tagInfo = this.props.getTagInfoProvider().getTagInfo(tagNode.getName());
        return tagInfo != null && !tagNode.hasChildren() && tagInfo.isEmptyTag();
    }

    /* access modifiers changed from: protected */
    public boolean dontEscape(TagNode tagNode) {
        return isScriptOrStyle(tagNode);
    }

    /* access modifiers changed from: protected */
    public String escapeText(String content) {
        return Utils.escapeHtml(content, this.props);
    }

    /* access modifiers changed from: protected */
    public void serializeOpenTag(TagNode tagNode, Writer writer, boolean newLine) throws IOException {
        Map<String, String> nsDeclarations;
        String tagName = tagNode.getName();
        if (!Utils.isEmptyString(tagName)) {
            boolean nsAware = this.props.isNamespacesAware();
            if (!nsAware && Utils.getXmlNSPrefix(tagName) != null) {
                tagName = Utils.getXmlName(tagName);
            }
            writer.write("<" + tagName);
            for (Map.Entry<String, String> entry : tagNode.getAttributes().entrySet()) {
                String attName = (String) entry.getKey();
                if (!nsAware && Utils.getXmlNSPrefix(attName) != null) {
                    attName = Utils.getXmlName(attName);
                }
                if (!nsAware || !attName.equalsIgnoreCase("xmlns")) {
                    writer.write(" " + attName + "=\"" + escapeText((String) entry.getValue()) + "\"");
                }
            }
            if (nsAware && (nsDeclarations = tagNode.getNamespaceDeclarations()) != null) {
                for (Map.Entry<String, String> entry2 : nsDeclarations.entrySet()) {
                    String prefix = (String) entry2.getKey();
                    String att = "xmlns";
                    if (prefix.length() > 0) {
                        att = att + ":" + prefix;
                    }
                    writer.write(" " + att + "=\"" + escapeText((String) entry2.getValue()) + "\"");
                }
            }
            if (isMinimizedTagSyntax(tagNode)) {
                writer.write(" />");
                if (newLine) {
                    writer.write(IOUtils.LINE_SEPARATOR_UNIX);
                    return;
                }
                return;
            }
            writer.write(Account.DEFAULT_QUOTE_PREFIX);
        }
    }

    /* access modifiers changed from: protected */
    public void serializeEndTag(TagNode tagNode, Writer writer, boolean newLine) throws IOException {
        String tagName = tagNode.getName();
        if (!Utils.isEmptyString(tagName)) {
            if (Utils.getXmlNSPrefix(tagName) != null && !this.props.isNamespacesAware()) {
                tagName = Utils.getXmlName(tagName);
            }
            writer.write("</" + tagName + Account.DEFAULT_QUOTE_PREFIX);
            if (newLine) {
                writer.write(IOUtils.LINE_SEPARATOR_UNIX);
            }
        }
    }
}
