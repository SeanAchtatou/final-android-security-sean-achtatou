package org.htmlcleaner;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    private static final Pattern ASCII_CHAR = Pattern.compile("\\p{Print}");
    public static Pattern DECIMAL = Pattern.compile("^([\\p{Digit}]+)(;?)");
    public static Pattern HEX_RELAXED = Pattern.compile("^0*([x|X][\\p{XDigit}]+)(;?)");
    public static Pattern HEX_STRICT = Pattern.compile("^([x|X][\\p{XDigit}]+)(;?)");
    private static String ampNcr;

    @Deprecated
    static CharSequence readUrl(URL url, String charset) throws IOException {
        int charsRead;
        StringBuilder buffer = new StringBuilder(1024);
        InputStream inputStream = url.openStream();
        try {
            InputStreamReader reader = new InputStreamReader(inputStream, charset);
            char[] charArray = new char[1024];
            do {
                charsRead = reader.read(charArray);
                if (charsRead >= 0) {
                    buffer.append(charArray, 0, charsRead);
                    continue;
                }
            } while (charsRead > 0);
            return buffer;
        } finally {
            inputStream.close();
        }
    }

    public static String escapeHtml(String s, CleanerProperties props) {
        return escapeXml(s, props.isAdvancedXmlEscape(), props.isRecognizeUnicodeChars(), props.isTranslateSpecialEntities(), false, props.isTransResCharsToNCR(), props.isTransSpecialEntitiesToNCR(), true);
    }

    public static String escapeXml(String s, CleanerProperties props, boolean isDomCreation) {
        return escapeXml(s, props.isAdvancedXmlEscape(), props.isRecognizeUnicodeChars(), props.isTranslateSpecialEntities(), isDomCreation, props.isTransResCharsToNCR(), props.isTransSpecialEntitiesToNCR(), false);
    }

    public static String escapeXml(String s, boolean advanced, boolean recognizeUnicodeChars, boolean translateSpecialEntities, boolean isDomCreation, boolean transResCharsToNCR, boolean translateSpecialEntitiesToNCR) {
        return escapeXml(s, advanced, recognizeUnicodeChars, translateSpecialEntities, isDomCreation, transResCharsToNCR, translateSpecialEntitiesToNCR, false);
    }

    public static String escapeXml(String s, boolean advanced, boolean recognizeUnicodeChars, boolean translateSpecialEntities, boolean isDomCreation, boolean transResCharsToNCR, boolean translateSpecialEntitiesToNCR, boolean isHtmlOutput) {
        SpecialEntity code;
        String escaped;
        if (s == null) {
            return null;
        }
        int len = s.length();
        StringBuilder result = new StringBuilder(len);
        int i = 0;
        while (i < len) {
            char ch = s.charAt(i);
            if (ch != '&') {
                SpecialEntity code2 = SpecialEntities.INSTANCE.getSpecialEntityByUnicode(ch);
                if (code2 == null) {
                    result.append(ch);
                } else if (!isHtmlOutput) {
                    result.append(transResCharsToNCR ? code2.getDecimalNCR() : code2.getEscaped(isDomCreation));
                } else if ("apos".equals(code2.getKey())) {
                    result.append(ch);
                } else {
                    result.append(isDomCreation ? code2.getHtmlString() : code2.getEscapedValue());
                }
            } else if ((advanced || recognizeUnicodeChars) && i < len - 1 && s.charAt(i + 1) == '#') {
                i = convertToUnicode(s, isDomCreation, recognizeUnicodeChars, translateSpecialEntitiesToNCR, result, i + 2);
            } else if ((translateSpecialEntities || advanced) && (code = SpecialEntities.INSTANCE.getSpecialEntity(s.substring(i, Math.min(10, len - i) + i))) != null) {
                if (translateSpecialEntities && code.isHtmlSpecialEntity()) {
                    if (recognizeUnicodeChars) {
                        result.append((char) code.intValue());
                    } else {
                        result.append(code.getDecimalNCR());
                    }
                    i += code.getKey().length() + 1;
                } else if (advanced) {
                    if (transResCharsToNCR) {
                        escaped = code.getDecimalNCR();
                    } else {
                        escaped = code.getEscaped(isHtmlOutput || isDomCreation);
                    }
                    result.append(escaped);
                    i += code.getKey().length() + 1;
                } else {
                    result.append(transResCharsToNCR ? getAmpNcr() : "&amp;");
                }
            } else if (isHtmlOutput) {
                SpecialEntity code3 = SpecialEntities.INSTANCE.getSpecialEntity(s.substring(i, Math.min(10, len - i) + i));
                if (code3 != null) {
                    result.append(code3.getEscapedValue());
                    i += code3.getKey().length() + 1;
                } else if (i >= len - 1 || s.charAt(i + 1) != '#') {
                    result.append(transResCharsToNCR ? getAmpNcr() : "&amp;");
                } else {
                    i = convert_To_Entity_Name(s, false, false, false, result, i + 2);
                }
            } else {
                result.append(transResCharsToNCR ? getAmpNcr() : "&amp;");
            }
            i++;
        }
        return result.toString();
    }

    private static String getAmpNcr() {
        if (ampNcr == null) {
            ampNcr = SpecialEntities.INSTANCE.getSpecialEntityByUnicode(38).getDecimalNCR();
        }
        return ampNcr;
    }

    private static int convert_To_Entity_Name(String s, boolean domCreation, boolean recognizeUnicodeChars, boolean translateSpecialEntitiesToNCR, StringBuilder result, int i) {
        int codePoint;
        char[] unicodeChar;
        String htmlString;
        String escapedValue;
        StringBuilder unicode = new StringBuilder();
        int charIndex = extractCharCode(s, i, true, unicode);
        if (unicode.length() > 0) {
            try {
                boolean isHex = unicode.substring(0, 1).equals("x");
                if (isHex) {
                    codePoint = Integer.parseInt(unicode.substring(1), 16);
                    unicodeChar = Character.toChars(codePoint);
                } else {
                    codePoint = Integer.parseInt(unicode.toString());
                    unicodeChar = Character.toChars(codePoint);
                }
                SpecialEntity specialEntity = SpecialEntities.INSTANCE.getSpecialEntityByUnicode(codePoint);
                if (unicodeChar.length == 1 && unicodeChar[0] == 0) {
                    result.append("&amp;");
                } else if (specialEntity != null) {
                    if (specialEntity.isHtmlSpecialEntity()) {
                        if (domCreation) {
                            escapedValue = specialEntity.getHtmlString();
                        } else {
                            escapedValue = specialEntity.getEscapedValue();
                        }
                        result.append(escapedValue);
                    } else {
                        if (domCreation) {
                            htmlString = specialEntity.getHtmlString();
                        } else if (translateSpecialEntitiesToNCR) {
                            htmlString = isHex ? specialEntity.getHexNCR() : specialEntity.getDecimalNCR();
                        } else {
                            htmlString = specialEntity.getHtmlString();
                        }
                        result.append(htmlString);
                    }
                } else if (recognizeUnicodeChars) {
                    result.append(String.valueOf(unicodeChar));
                } else if (ASCII_CHAR.matcher(new String(unicodeChar)).find()) {
                    result.append(String.valueOf(unicodeChar));
                } else {
                    result.append("&#").append((CharSequence) unicode).append(";");
                }
            } catch (NumberFormatException e) {
                result.append("&amp;#").append((CharSequence) unicode).append(";");
            }
        } else {
            result.append("&amp;");
        }
        return charIndex;
    }

    private static int convertToUnicode(String s, boolean domCreation, boolean recognizeUnicodeChars, boolean translateSpecialEntitiesToNCR, StringBuilder result, int i) {
        int codePoint;
        char[] unicodeChar;
        String escapedXmlString;
        StringBuilder unicode = new StringBuilder();
        int charIndex = extractCharCode(s, i, true, unicode);
        if (unicode.length() > 0) {
            try {
                boolean isHex = unicode.substring(0, 1).equals("x");
                if (isHex) {
                    codePoint = Integer.parseInt(unicode.substring(1), 16);
                    unicodeChar = Character.toChars(codePoint);
                } else {
                    codePoint = Integer.parseInt(unicode.toString());
                    unicodeChar = Character.toChars(codePoint);
                }
                SpecialEntity specialEntity = SpecialEntities.INSTANCE.getSpecialEntityByUnicode(codePoint);
                if (unicodeChar.length == 1 && unicodeChar[0] == 0) {
                    result.append("&amp;");
                } else if (specialEntity != null && (!specialEntity.isHtmlSpecialEntity() || !recognizeUnicodeChars)) {
                    if (domCreation) {
                        escapedXmlString = specialEntity.getHtmlString();
                    } else if (translateSpecialEntitiesToNCR) {
                        escapedXmlString = isHex ? specialEntity.getHexNCR() : specialEntity.getDecimalNCR();
                    } else {
                        escapedXmlString = specialEntity.getEscapedXmlString();
                    }
                    result.append(escapedXmlString);
                } else if (recognizeUnicodeChars) {
                    result.append(String.valueOf(unicodeChar));
                } else if (ASCII_CHAR.matcher(new String(unicodeChar)).find()) {
                    result.append(String.valueOf(unicodeChar));
                } else {
                    result.append("&#").append((CharSequence) unicode).append(";");
                }
            } catch (NumberFormatException e) {
                result.append("&amp;#").append((CharSequence) unicode).append(";");
            }
        } else {
            result.append("&amp;");
        }
        return charIndex;
    }

    private static int extractCharCode(String s, int charIndex, boolean relaxedUnicode, StringBuilder unicode) {
        Matcher matcher;
        CharSequence subSequence = s.subSequence(charIndex, Math.min(s.length(), charIndex + 15));
        if (relaxedUnicode) {
            matcher = HEX_RELAXED.matcher(subSequence);
        } else {
            matcher = HEX_STRICT.matcher(subSequence);
        }
        if (!matcher.find()) {
            matcher = DECIMAL.matcher(subSequence);
            if (!matcher.find()) {
                return charIndex;
            }
        }
        int charIndex2 = charIndex + (matcher.end() - 1);
        unicode.append(matcher.group(1));
        return charIndex2;
    }

    public static boolean isIdentifierHelperChar(char ch) {
        return ':' == ch || '.' == ch || '-' == ch || '_' == ch;
    }

    public static boolean isValidXmlIdentifier(String s) {
        int len;
        if (s == null || (len = s.length()) == 0) {
            return false;
        }
        for (int i = 0; i < len; i++) {
            char ch = s.charAt(i);
            if (i == 0 && !Character.isUnicodeIdentifierStart(ch)) {
                return false;
            }
            if (!Character.isUnicodeIdentifierStart(ch) && !Character.isDigit(ch) && !isIdentifierHelperChar(ch)) {
                return false;
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [?, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static boolean isEmptyString(Object o) {
        if (o == null || escapeXml(o.toString(), true, false, false, false, false, false, false).replace((char) SpecialEntities.NON_BREAKABLE_SPACE, ' ').trim().length() == 0) {
            return true;
        }
        return false;
    }

    public static String[] tokenize(String s, String delimiters) {
        if (s == null) {
            return new String[0];
        }
        StringTokenizer tokenizer = new StringTokenizer(s, delimiters);
        String[] result = new String[tokenizer.countTokens()];
        int index = 0;
        while (tokenizer.hasMoreTokens()) {
            result[index] = tokenizer.nextToken();
            index++;
        }
        return result;
    }

    public static String getXmlNSPrefix(String name) {
        int colIndex = name.indexOf(58);
        if (colIndex > 0) {
            return name.substring(0, colIndex);
        }
        return null;
    }

    public static String getXmlName(String name) {
        int colIndex = name.indexOf(58);
        if (colIndex <= 0 || colIndex >= name.length() - 1) {
            return name;
        }
        return name.substring(colIndex + 1);
    }

    static boolean isValidInt(String s, int radix) {
        try {
            Integer.parseInt(s, radix);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    static boolean isValidXmlChar(char ch) {
        return (ch >= ' ' && ch <= 55295) || ch == 9 || ch == 10 || ch == 13 || (ch >= 57344 && ch <= 65533) || (ch >= 0 && ch <= 65535);
    }

    public static String ltrim(String s) {
        if (s == null) {
            return null;
        }
        int index = 0;
        int len = s.length();
        while (index < len && Character.isWhitespace(s.charAt(index))) {
            index++;
        }
        return index >= len ? "" : s.substring(index);
    }

    public static String rtrim(String s) {
        if (s == null) {
            return null;
        }
        int index = s.length();
        while (index > 0 && Character.isWhitespace(s.charAt(index - 1))) {
            index--;
        }
        return index <= 0 ? "" : s.substring(0, index);
    }

    public static boolean isWhitespaceString(Object object) {
        String s;
        if (object == null || (s = object.toString()) == null || !"".equals(s.trim())) {
            return false;
        }
        return true;
    }
}
