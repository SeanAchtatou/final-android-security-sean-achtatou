package org.htmlcleaner;

import java.util.regex.Pattern;

public class AttributeTransformationPatternImpl implements AttributeTransformation {
    private final Pattern attNamePattern;
    private final Pattern attValuePattern;
    private final String template;

    public AttributeTransformationPatternImpl(Pattern attNamePattern2, Pattern attValuePattern2, String template2) {
        this.attNamePattern = attNamePattern2;
        this.attValuePattern = attValuePattern2;
        this.template = template2;
    }

    public AttributeTransformationPatternImpl(String attNamePattern2, String attValuePattern2, String template2) {
        Pattern pattern = null;
        this.attNamePattern = attNamePattern2 == null ? null : Pattern.compile(attNamePattern2);
        this.attValuePattern = attValuePattern2 != null ? Pattern.compile(attValuePattern2) : pattern;
        this.template = template2;
    }

    public boolean satisfy(String attName, String attValue) {
        if ((this.attNamePattern == null || this.attNamePattern.matcher(attName).find()) && (this.attValuePattern == null || this.attValuePattern.matcher(attValue).find())) {
            return true;
        }
        return false;
    }

    public String getTemplate() {
        return this.template;
    }
}
