package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.ListIterator;
import org.apache.commons.io.IOUtils;

public class CompactHtmlSerializer extends HtmlSerializer {
    private int openPreTags = 0;

    public CompactHtmlSerializer(CleanerProperties props) {
        super(props);
    }

    /* access modifiers changed from: protected */
    public void serialize(TagNode tagNode, Writer writer) throws IOException {
        boolean startsWithSpace;
        boolean endsWithSpace;
        String content;
        boolean isPreTag = "pre".equalsIgnoreCase(tagNode.getName());
        if (isPreTag) {
            this.openPreTags++;
        }
        serializeOpenTag(tagNode, writer, false);
        List<? extends BaseToken> tagChildren = tagNode.getAllChildren();
        if (!isMinimizedTagSyntax(tagNode)) {
            ListIterator<? extends BaseToken> childrenIt = tagChildren.listIterator();
            while (childrenIt.hasNext()) {
                Object item = childrenIt.next();
                if (item instanceof ContentNode) {
                    String content2 = item.toString();
                    if (this.openPreTags > 0) {
                        writer.write(content2);
                    } else {
                        if (content2.length() <= 0 || !Character.isWhitespace(content2.charAt(0))) {
                            startsWithSpace = false;
                        } else {
                            startsWithSpace = true;
                        }
                        if (content2.length() <= 1 || !Character.isWhitespace(content2.charAt(content2.length() - 1))) {
                            endsWithSpace = false;
                        } else {
                            endsWithSpace = true;
                        }
                        if (dontEscape(tagNode)) {
                            content = content2.trim();
                        } else {
                            content = escapeText(content2.trim());
                        }
                        if (startsWithSpace) {
                            writer.write(32);
                        }
                        if (content.length() != 0) {
                            writer.write(content);
                            if (endsWithSpace) {
                                writer.write(32);
                            }
                        }
                        if (childrenIt.hasNext()) {
                            if (!Utils.isWhitespaceString(childrenIt.next())) {
                                writer.write(IOUtils.LINE_SEPARATOR_UNIX);
                            }
                            childrenIt.previous();
                        }
                    }
                } else if (item instanceof CommentNode) {
                    writer.write(((CommentNode) item).getCommentedContent().trim());
                } else if (item instanceof BaseToken) {
                    ((BaseToken) item).serialize(this, writer);
                }
            }
            serializeEndTag(tagNode, writer, false);
            if (isPreTag) {
                this.openPreTags--;
            }
        }
    }
}
