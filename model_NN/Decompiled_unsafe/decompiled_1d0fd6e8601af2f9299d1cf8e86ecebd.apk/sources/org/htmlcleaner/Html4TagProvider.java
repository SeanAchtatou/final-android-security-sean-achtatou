package org.htmlcleaner;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.jdom2.JDOMConstants;

public class Html4TagProvider implements ITagInfoProvider {
    private static final String CLOSE_BEFORE_COPY_INSIDE_TAGS = "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font";
    private static final String CLOSE_BEFORE_TAGS = "p,details,summary,menuitem,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml";
    public static final Html4TagProvider INSTANCE = new Html4TagProvider();
    private static final String PHRASING_TAGS = "a,abbr,area,b,bdi,bdo,br,button,canvas,cite,code,command,data,datalist,del,dfn,em,embed,i,iframe,img,input,ins,kbd,keygen,label,link,map,mark,math,meta,meter,noscript,object,output,progress,q,s,samp,script,select,small,span,strong,sub,sup,svg,template,text,textarea,time,u,var,wbr";
    private static final String STRONG = "strong";
    private ConcurrentMap<String, TagInfo> tagInfoMap = new ConcurrentHashMap();

    public Html4TagProvider() {
        basicElements(null);
        formattingElements(null);
        formElements(null);
        imgElements(null);
        listElements(null);
        linkElements(null);
        tableElements(null);
        styleElements(null);
        olderElements(null);
        scriptElements(null);
    }

    public void basicElements(TagInfo tagInfo) {
        put("title", new TagInfo("title", ContentType.text, BelongsTo.HEAD, false, true, false, CloseTag.required, Display.none));
        TagInfo tagInfo2 = new TagInfo("h1", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo2.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo2.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("h1", tagInfo2);
        TagInfo tagInfo3 = new TagInfo("h2", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo3.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo3.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("h2", tagInfo3);
        TagInfo tagInfo4 = new TagInfo("h3", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo4.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo4.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("h3", tagInfo4);
        TagInfo tagInfo5 = new TagInfo("h4", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo5.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo5.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("h4", tagInfo5);
        TagInfo tagInfo6 = new TagInfo("h5", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo6.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo6.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("h5", tagInfo6);
        TagInfo tagInfo7 = new TagInfo("h6", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo7.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo7.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("h6", tagInfo7);
        TagInfo tagInfo8 = new TagInfo("p", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo8.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo8.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("p", tagInfo8);
        put("br", new TagInfo("br", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.none));
        TagInfo tagInfo9 = new TagInfo("hr", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.block);
        tagInfo9.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo9.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("hr", tagInfo9);
        TagInfo tagInfo10 = new TagInfo("div", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo10.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo10.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("div", tagInfo10);
    }

    public void formattingElements(TagInfo tagInfo) {
        put("abbr", new TagInfo("abbr", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("acronym", new TagInfo("acronym", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        TagInfo tagInfo2 = new TagInfo("address", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo2.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo2.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("address", tagInfo2);
        TagInfo tagInfo3 = new TagInfo("b", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo3.defineCloseInsideCopyAfterTags("u,i,tt,sub,sup,big,small,strike,blink,s");
        put("b", tagInfo3);
        put("bdo", new TagInfo("bdo", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        TagInfo tagInfo4 = new TagInfo("blockquote", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo4.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo4.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("blockquote", tagInfo4);
        put("cite", new TagInfo("cite", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("q", new TagInfo("q", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("code", new TagInfo("code", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("ins", new TagInfo("ins", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any));
        TagInfo tagInfo5 = new TagInfo("i", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo5.defineCloseInsideCopyAfterTags("b,u,tt,sub,sup,big,small,strike,blink,s");
        put("i", tagInfo5);
        TagInfo tagInfo6 = new TagInfo("u", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.inline);
        tagInfo6.defineCloseInsideCopyAfterTags("b,i,tt,sub,sup,big,small,strike,blink,s");
        put("u", tagInfo6);
        TagInfo tagInfo7 = new TagInfo("tt", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo7.defineCloseInsideCopyAfterTags("b,u,i,sub,sup,big,small,strike,blink,s");
        put("tt", tagInfo7);
        TagInfo tagInfo8 = new TagInfo("sub", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo8.defineCloseInsideCopyAfterTags("b,u,i,tt,sup,big,small,strike,blink,s");
        put("sub", tagInfo8);
        TagInfo tagInfo9 = new TagInfo("sup", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo9.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,big,small,strike,blink,s");
        put("sup", tagInfo9);
        TagInfo tagInfo10 = new TagInfo("big", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo10.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,small,strike,blink,s");
        put("big", tagInfo10);
        TagInfo tagInfo11 = new TagInfo("small", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo11.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,strike,blink,s");
        put("small", tagInfo11);
        TagInfo tagInfo12 = new TagInfo("strike", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.inline);
        tagInfo12.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,small,blink,s");
        put("strike", tagInfo12);
        TagInfo tagInfo13 = new TagInfo("blink", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo13.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,small,strike,s");
        put("blink", tagInfo13);
        TagInfo tagInfo14 = new TagInfo("marquee", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo14.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo14.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("marquee", tagInfo14);
        TagInfo tagInfo15 = new TagInfo("s", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.inline);
        tagInfo15.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,small,strike,blink");
        put("s", tagInfo15);
        put("font", new TagInfo("font", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.inline));
        put("basefont", new TagInfo("basefont", ContentType.none, BelongsTo.BODY, true, false, false, CloseTag.forbidden, Display.none));
        TagInfo tagInfo16 = new TagInfo("center", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.block);
        tagInfo16.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo16.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("center", tagInfo16);
        put("del", new TagInfo("del", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any));
        put("dfn", new TagInfo("dfn", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("kbd", new TagInfo("kbd", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        TagInfo tagInfo17 = new TagInfo("pre", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo17.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo17.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("pre", tagInfo17);
        put("samp", new TagInfo("samp", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put(STRONG, new TagInfo(STRONG, ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("em", new TagInfo("em", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("var", new TagInfo("var", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("wbr", new TagInfo("wbr", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.none));
    }

    public void formElements(TagInfo tagInfo) {
        TagInfo tagInfo2 = new TagInfo("form", ContentType.all, BelongsTo.BODY, false, false, true, CloseTag.required, Display.block);
        tagInfo2.defineForbiddenTags("form");
        tagInfo2.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo2.defineCloseBeforeTags("option,optgroup,textarea,select,fieldset,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("form", tagInfo2);
        TagInfo tagInfo3 = new TagInfo("input", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.inline);
        tagInfo3.defineCloseBeforeTags("select,optgroup,option");
        put("input", tagInfo3);
        TagInfo tagInfo4 = new TagInfo("textarea", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo4.defineCloseBeforeTags("select,optgroup,option");
        put("textarea", tagInfo4);
        TagInfo tagInfo5 = new TagInfo("select", ContentType.all, BelongsTo.BODY, false, false, true, CloseTag.required, Display.inline);
        tagInfo5.defineAllowedChildrenTags("option,optgroup");
        tagInfo5.defineCloseBeforeTags("option,optgroup,select");
        put("select", tagInfo5);
        TagInfo tagInfo6 = new TagInfo("option", ContentType.text, BelongsTo.BODY, false, false, true, CloseTag.optional, Display.inline);
        tagInfo6.defineFatalTags("select");
        tagInfo6.defineCloseBeforeTags("option");
        put("option", tagInfo6);
        TagInfo tagInfo7 = new TagInfo("optgroup", ContentType.all, BelongsTo.BODY, false, false, true, CloseTag.required, Display.inline);
        tagInfo7.defineFatalTags("select");
        tagInfo7.defineAllowedChildrenTags("option");
        tagInfo7.defineCloseBeforeTags("optgroup");
        put("optgroup", tagInfo7);
        TagInfo tagInfo8 = new TagInfo("button", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo8.defineCloseBeforeTags("select,optgroup,option");
        put("button", tagInfo8);
        put("label", new TagInfo("label", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        TagInfo tagInfo9 = new TagInfo("legend", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo9.defineAllowedChildrenTags(PHRASING_TAGS);
        put("legend", tagInfo9);
        TagInfo tagInfo10 = new TagInfo("fieldset", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo10.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo10.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("fieldset", tagInfo10);
    }

    public void listElements(TagInfo tagInfo) {
        TagInfo tagInfo2 = new TagInfo("ul", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo2.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo2.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("ul", tagInfo2);
        TagInfo tagInfo3 = new TagInfo("ol", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo3.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo3.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("ol", tagInfo3);
        TagInfo tagInfo4 = new TagInfo("li", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo4.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo4.defineCloseBeforeTags("li,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("li", tagInfo4);
        TagInfo tagInfo5 = new TagInfo("dl", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo5.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo5.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("dl", tagInfo5);
        TagInfo tagInfo6 = new TagInfo("dt", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo6.defineCloseBeforeTags("dt,dd");
        put("dt", tagInfo6);
        TagInfo tagInfo7 = new TagInfo("dd", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo7.defineCloseBeforeTags("dt,dd");
        put("dd", tagInfo7);
        TagInfo tagInfo8 = new TagInfo("menu", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.block);
        tagInfo8.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo8.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("menu", tagInfo8);
        TagInfo tagInfo9 = new TagInfo("dir", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.block);
        tagInfo9.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo9.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("dir", tagInfo9);
    }

    public void linkElements(TagInfo tagInfo) {
        put("link", new TagInfo("link", ContentType.none, BelongsTo.HEAD, false, false, false, CloseTag.forbidden, Display.none));
        TagInfo tagInfo2 = new TagInfo("a", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo2.defineCloseBeforeTags("a");
        put("a", tagInfo2);
    }

    public void tableElements(TagInfo tagInfo) {
        TagInfo tagInfo2 = new TagInfo("table", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo2.defineAllowedChildrenTags("tr,tbody,thead,tfoot,colgroup,caption");
        tagInfo2.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo2.defineCloseBeforeTags("tr,thead,tbody,tfoot,caption,colgroup,table,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("table", tagInfo2);
        TagInfo tagInfo3 = new TagInfo("tr", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo3.defineFatalTags("table");
        tagInfo3.defineRequiredEnclosingTags("tbody");
        tagInfo3.defineAllowedChildrenTags("td,th");
        tagInfo3.defineHigherLevelTags("thead,tfoot");
        tagInfo3.defineCloseBeforeTags("tr,td,th,caption,colgroup");
        put("tr", tagInfo3);
        TagInfo tagInfo4 = new TagInfo("td", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo4.defineFatalTags("table");
        tagInfo4.defineRequiredEnclosingTags("tr");
        tagInfo4.defineCloseBeforeTags("td,th,caption,colgroup");
        put("td", tagInfo4);
        TagInfo tagInfo5 = new TagInfo("th", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo5.defineFatalTags("table");
        tagInfo5.defineRequiredEnclosingTags("tr");
        tagInfo5.defineCloseBeforeTags("td,th,caption,colgroup");
        put("th", tagInfo5);
        TagInfo tagInfo6 = new TagInfo("tbody", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo6.defineFatalTags("table");
        tagInfo6.defineAllowedChildrenTags("tr,form");
        tagInfo6.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        put("tbody", tagInfo6);
        TagInfo tagInfo7 = new TagInfo("thead", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo7.defineFatalTags("table");
        tagInfo7.defineAllowedChildrenTags("tr,form");
        tagInfo7.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        put("thead", tagInfo7);
        TagInfo tagInfo8 = new TagInfo("tfoot", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo8.defineFatalTags("table");
        tagInfo8.defineAllowedChildrenTags("tr,form");
        tagInfo8.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        put("tfoot", tagInfo8);
        TagInfo tagInfo9 = new TagInfo("col", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.block);
        tagInfo9.defineFatalTags("colgroup");
        put("col", tagInfo9);
        TagInfo tagInfo10 = new TagInfo("colgroup", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo10.defineFatalTags("table");
        tagInfo10.defineAllowedChildrenTags("col");
        tagInfo10.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        put("colgroup", tagInfo10);
        TagInfo tagInfo11 = new TagInfo("caption", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo11.defineFatalTags("table");
        tagInfo11.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        put("caption", tagInfo11);
    }

    public void styleElements(TagInfo tagInfo) {
        put("span", new TagInfo("span", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("style", new TagInfo("style", ContentType.text, BelongsTo.HEAD, false, false, false, CloseTag.required, Display.none));
        put("bgsound", new TagInfo("bgsound", ContentType.none, BelongsTo.HEAD, false, false, false, CloseTag.forbidden, Display.none));
        put("meta", new TagInfo("meta", ContentType.none, BelongsTo.HEAD, false, false, false, CloseTag.forbidden, Display.none));
        put("base", new TagInfo("base", ContentType.none, BelongsTo.HEAD, false, false, false, CloseTag.forbidden, Display.none));
    }

    public void scriptElements(TagInfo tagInfo) {
        put("script", new TagInfo("script", ContentType.all, BelongsTo.HEAD_AND_BODY, false, false, false, CloseTag.required, Display.none));
        put("noscript", new TagInfo("noscript", ContentType.all, BelongsTo.HEAD_AND_BODY, false, false, false, CloseTag.required, Display.block));
        put("applet", new TagInfo("applet", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.any));
        put("object", new TagInfo("object", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any));
        TagInfo tagInfo2 = new TagInfo("param", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.none);
        tagInfo2.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo2.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("param", tagInfo2);
    }

    public void imgElements(TagInfo tagInfo) {
        put("img", new TagInfo("img", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.inline));
        TagInfo tagInfo2 = new TagInfo("area", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.none);
        tagInfo2.defineFatalTags("map");
        tagInfo2.defineCloseBeforeTags("area");
        put("area", tagInfo2);
        TagInfo tagInfo3 = new TagInfo("map", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo3.defineCloseBeforeTags("map");
        put("map", tagInfo3);
    }

    public void olderElements(TagInfo tagInfo) {
        TagInfo tagInfo2 = new TagInfo("listing", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo2.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo2.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("listing", tagInfo2);
        TagInfo tagInfo3 = new TagInfo("nobr", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo3.defineCloseBeforeTags("nobr");
        put("nobr", tagInfo3);
        put("xmp", new TagInfo("xmp", ContentType.text, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put(JDOMConstants.NS_PREFIX_XML, new TagInfo(JDOMConstants.NS_PREFIX_XML, ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.none));
        TagInfo tagInfo4 = new TagInfo("isindex", ContentType.none, BelongsTo.BODY, true, false, false, CloseTag.forbidden, Display.block);
        tagInfo4.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo4.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("isindex", tagInfo4);
        put("comment", new TagInfo("comment", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.none));
        put("server", new TagInfo("server", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.none));
        put("iframe", new TagInfo("iframe", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any));
    }

    /* access modifiers changed from: protected */
    public void put(String tagName, TagInfo tagInfo) {
        this.tagInfoMap.put(tagName, tagInfo);
    }

    public TagInfo getTagInfo(String tagName) {
        if (tagName == null) {
            return null;
        }
        return this.tagInfoMap.get(tagName);
    }
}
