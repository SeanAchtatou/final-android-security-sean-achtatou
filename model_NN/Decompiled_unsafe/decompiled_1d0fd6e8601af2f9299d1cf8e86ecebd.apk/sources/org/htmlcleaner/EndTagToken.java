package org.htmlcleaner;

import java.io.Writer;

public class EndTagToken extends TagToken {
    public EndTagToken() {
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public EndTagToken(String name) {
        super(name == null ? null : name.toLowerCase());
    }

    /* access modifiers changed from: package-private */
    public void addAttribute(String attName, String attValue) {
    }

    public void serialize(Serializer serializer, Writer writer) {
    }

    public String toString() {
        return "endtoken" + super.toString();
    }
}
