package org.htmlcleaner;

public enum CloseTag {
    required(false, true),
    optional(true, true),
    forbidden(true, false);
    
    private final boolean endTagPermitted;
    private final boolean minimizedTagPermitted;

    private CloseTag(boolean minimizedTagPermitted2, boolean endTagPermitted2) {
        this.minimizedTagPermitted = minimizedTagPermitted2;
        this.endTagPermitted = endTagPermitted2;
    }

    public boolean isMinimizedTagPermitted() {
        return this.minimizedTagPermitted;
    }

    public boolean isEndTagPermitted() {
        return this.endTagPermitted;
    }
}
