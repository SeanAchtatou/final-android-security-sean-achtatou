package org.htmlcleaner;

public enum BelongsTo {
    HEAD_AND_BODY("all"),
    HEAD("head"),
    BODY("body");
    
    private final String dbCode;

    private BelongsTo(String dbCode2) {
        this.dbCode = dbCode2;
    }

    public String getDbCode() {
        return this.dbCode;
    }

    public static BelongsTo toValue(Object value) {
        if (value instanceof BelongsTo) {
            return (BelongsTo) value;
        }
        if (value == null) {
            return null;
        }
        String dbCode2 = value.toString().trim();
        for (BelongsTo belongsTo : values()) {
            if (belongsTo.getDbCode().equalsIgnoreCase(dbCode2) || belongsTo.name().equalsIgnoreCase(dbCode2)) {
                return belongsTo;
            }
        }
        return null;
    }
}
