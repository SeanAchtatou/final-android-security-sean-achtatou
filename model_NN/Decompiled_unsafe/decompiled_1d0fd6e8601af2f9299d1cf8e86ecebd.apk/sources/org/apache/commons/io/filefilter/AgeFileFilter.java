package org.apache.commons.io.filefilter;

import com.fsck.k9.Account;
import java.io.File;
import java.io.Serializable;
import java.util.Date;
import org.apache.commons.io.FileUtils;

public class AgeFileFilter extends AbstractFileFilter implements Serializable {
    private final boolean acceptOlder;
    private final long cutoff;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.filefilter.AgeFileFilter.<init>(long, boolean):void
     arg types: [long, int]
     candidates:
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.io.File, boolean):void
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.util.Date, boolean):void
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(long, boolean):void */
    public AgeFileFilter(long cutoff2) {
        this(cutoff2, true);
    }

    public AgeFileFilter(long cutoff2, boolean acceptOlder2) {
        this.acceptOlder = acceptOlder2;
        this.cutoff = cutoff2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.util.Date, boolean):void
     arg types: [java.util.Date, int]
     candidates:
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(long, boolean):void
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.io.File, boolean):void
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.util.Date, boolean):void */
    public AgeFileFilter(Date cutoffDate) {
        this(cutoffDate, true);
    }

    public AgeFileFilter(Date cutoffDate, boolean acceptOlder2) {
        this(cutoffDate.getTime(), acceptOlder2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.io.File, boolean):void
     arg types: [java.io.File, int]
     candidates:
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(long, boolean):void
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.util.Date, boolean):void
      org.apache.commons.io.filefilter.AgeFileFilter.<init>(java.io.File, boolean):void */
    public AgeFileFilter(File cutoffReference) {
        this(cutoffReference, true);
    }

    public AgeFileFilter(File cutoffReference, boolean acceptOlder2) {
        this(cutoffReference.lastModified(), acceptOlder2);
    }

    public boolean accept(File file) {
        boolean newer = FileUtils.isFileNewer(file, this.cutoff);
        if (this.acceptOlder) {
            return !newer;
        }
        return newer;
    }

    public String toString() {
        return super.toString() + "(" + (this.acceptOlder ? "<=" : Account.DEFAULT_QUOTE_PREFIX) + this.cutoff + ")";
    }
}
