package org.apache.commons.io.input;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class Tailer implements Runnable {
    private static final int DEFAULT_BUFSIZE = 4096;
    private static final int DEFAULT_DELAY_MILLIS = 1000;
    private static final String RAF_MODE = "r";
    private final long delayMillis;
    private final boolean end;
    private final File file;
    private final byte[] inbuf;
    private final TailerListener listener;
    private final boolean reOpen;
    private volatile boolean run;

    public Tailer(File file2, TailerListener listener2) {
        this(file2, listener2, 1000);
    }

    public Tailer(File file2, TailerListener listener2, long delayMillis2) {
        this(file2, listener2, delayMillis2, false);
    }

    public Tailer(File file2, TailerListener listener2, long delayMillis2, boolean end2) {
        this(file2, listener2, delayMillis2, end2, 4096);
    }

    public Tailer(File file2, TailerListener listener2, long delayMillis2, boolean end2, boolean reOpen2) {
        this(file2, listener2, delayMillis2, end2, reOpen2, 4096);
    }

    public Tailer(File file2, TailerListener listener2, long delayMillis2, boolean end2, int bufSize) {
        this(file2, listener2, delayMillis2, end2, false, bufSize);
    }

    public Tailer(File file2, TailerListener listener2, long delayMillis2, boolean end2, boolean reOpen2, int bufSize) {
        this.run = true;
        this.file = file2;
        this.delayMillis = delayMillis2;
        this.end = end2;
        this.inbuf = new byte[bufSize];
        this.listener = listener2;
        listener2.init(this);
        this.reOpen = reOpen2;
    }

    public static Tailer create(File file2, TailerListener listener2, long delayMillis2, boolean end2, int bufSize) {
        Tailer tailer = new Tailer(file2, listener2, delayMillis2, end2, bufSize);
        Thread thread = new Thread(tailer);
        thread.setDaemon(true);
        thread.start();
        return tailer;
    }

    public static Tailer create(File file2, TailerListener listener2, long delayMillis2, boolean end2, boolean reOpen2, int bufSize) {
        Tailer tailer = new Tailer(file2, listener2, delayMillis2, end2, reOpen2, bufSize);
        Thread thread = new Thread(tailer);
        thread.setDaemon(true);
        thread.start();
        return tailer;
    }

    public static Tailer create(File file2, TailerListener listener2, long delayMillis2, boolean end2) {
        return create(file2, listener2, delayMillis2, end2, 4096);
    }

    public static Tailer create(File file2, TailerListener listener2, long delayMillis2, boolean end2, boolean reOpen2) {
        return create(file2, listener2, delayMillis2, end2, reOpen2, 4096);
    }

    public static Tailer create(File file2, TailerListener listener2, long delayMillis2) {
        return create(file2, listener2, delayMillis2, false);
    }

    public static Tailer create(File file2, TailerListener listener2) {
        return create(file2, listener2, 1000, false);
    }

    public File getFile() {
        return this.file;
    }

    public long getDelay() {
        return this.delayMillis;
    }

    public void run() {
        RandomAccessFile reader;
        RandomAccessFile reader2;
        RandomAccessFile reader3 = null;
        long last = 0;
        long position = 0;
        while (true) {
            try {
                reader = reader3;
                if (!this.run || reader != null) {
                    break;
                }
                try {
                    reader3 = new RandomAccessFile(this.file, RAF_MODE);
                } catch (FileNotFoundException e) {
                    this.listener.fileNotFound();
                    reader3 = reader;
                }
                if (reader3 == null) {
                    try {
                        Thread.sleep(this.delayMillis);
                    } catch (InterruptedException e2) {
                    }
                } else {
                    try {
                        position = this.end ? this.file.length() : 0;
                        last = System.currentTimeMillis();
                        reader3.seek(position);
                    } catch (Exception e3) {
                        e = e3;
                        try {
                            this.listener.handle(e);
                            IOUtils.closeQuietly(reader3);
                            return;
                        } catch (Throwable th) {
                            th = th;
                            IOUtils.closeQuietly(reader3);
                            throw th;
                        }
                    }
                }
            } catch (Exception e4) {
                e = e4;
                reader3 = reader;
            } catch (Throwable th2) {
                th = th2;
                reader3 = reader;
                IOUtils.closeQuietly(reader3);
                throw th;
            }
        }
        if (this.run || !this.reOpen) {
            reader3 = reader;
        } else {
            reader3 = new RandomAccessFile(this.file, RAF_MODE);
            reader3.seek(position);
        }
        reader = reader3;
        while (this.run) {
            boolean newer = FileUtils.isFileNewer(this.file, last);
            long length = this.file.length();
            if (length < position) {
                this.listener.fileRotated();
                RandomAccessFile save = reader;
                try {
                    reader2 = new RandomAccessFile(this.file, RAF_MODE);
                    position = 0;
                    try {
                        IOUtils.closeQuietly(save);
                        reader = reader2;
                    } catch (FileNotFoundException e5) {
                        this.listener.fileNotFound();
                        reader = reader3;
                    }
                } catch (FileNotFoundException e6) {
                    reader2 = reader;
                    this.listener.fileNotFound();
                    reader = reader3;
                }
            } else {
                if (length > position) {
                    position = readLines(reader);
                    last = System.currentTimeMillis();
                } else if (newer) {
                    reader.seek(0);
                    position = readLines(reader);
                    last = System.currentTimeMillis();
                }
                if (this.reOpen) {
                    IOUtils.closeQuietly(reader);
                }
                try {
                    Thread.sleep(this.delayMillis);
                } catch (InterruptedException e7) {
                }
                if (this.run) {
                }
                reader3 = reader;
                reader = reader3;
            }
        }
        IOUtils.closeQuietly(reader);
    }

    public void stop() {
        this.run = false;
    }

    private long readLines(RandomAccessFile reader) throws IOException {
        int num;
        StringBuilder sb = new StringBuilder();
        long pos = reader.getFilePointer();
        long rePos = pos;
        boolean seenCR = false;
        while (this.run && (num = reader.read(this.inbuf)) != -1) {
            for (int i = 0; i < num; i++) {
                byte ch = this.inbuf[i];
                switch (ch) {
                    case 10:
                        seenCR = false;
                        this.listener.handle(sb.toString());
                        sb.setLength(0);
                        rePos = ((long) i) + pos + 1;
                        break;
                    case 11:
                    case 12:
                    default:
                        if (seenCR) {
                            seenCR = false;
                            this.listener.handle(sb.toString());
                            sb.setLength(0);
                            rePos = ((long) i) + pos + 1;
                        }
                        sb.append((char) ch);
                        break;
                    case 13:
                        if (seenCR) {
                            sb.append(13);
                        }
                        seenCR = true;
                        break;
                }
            }
            pos = reader.getFilePointer();
        }
        reader.seek(rePos);
        return rePos;
    }
}
