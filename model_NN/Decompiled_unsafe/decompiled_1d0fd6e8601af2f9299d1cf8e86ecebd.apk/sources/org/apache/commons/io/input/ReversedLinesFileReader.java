package org.apache.commons.io.input;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import org.apache.commons.io.Charsets;

public class ReversedLinesFileReader implements Closeable {
    /* access modifiers changed from: private */
    public final int avoidNewlineSplitBufferSize;
    /* access modifiers changed from: private */
    public final int blockSize;
    /* access modifiers changed from: private */
    public final int byteDecrement;
    private FilePart currentFilePart;
    /* access modifiers changed from: private */
    public final Charset encoding;
    /* access modifiers changed from: private */
    public final byte[][] newLineSequences;
    /* access modifiers changed from: private */
    public final RandomAccessFile randomAccessFile;
    private final long totalBlockCount;
    private final long totalByteLength;
    private boolean trailingNewlineOfFileSkipped;

    public ReversedLinesFileReader(File file) throws IOException {
        this(file, 4096, Charset.defaultCharset().toString());
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public ReversedLinesFileReader(java.io.File r11, int r12, java.nio.charset.Charset r13) throws java.io.IOException {
        /*
            r10 = this;
            r10.<init>()
            r0 = 0
            r10.trailingNewlineOfFileSkipped = r0
            r10.blockSize = r12
            r10.encoding = r13
            java.io.RandomAccessFile r0 = new java.io.RandomAccessFile
            java.lang.String r1 = "r"
            r0.<init>(r11, r1)
            r10.randomAccessFile = r0
            java.io.RandomAccessFile r0 = r10.randomAccessFile
            long r0 = r0.length()
            r10.totalByteLength = r0
            long r0 = r10.totalByteLength
            long r2 = (long) r12
            long r0 = r0 % r2
            int r4 = (int) r0
            if (r4 <= 0) goto L_0x0075
            long r0 = r10.totalByteLength
            long r2 = (long) r12
            long r0 = r0 / r2
            r2 = 1
            long r0 = r0 + r2
            r10.totalBlockCount = r0
        L_0x002b:
            org.apache.commons.io.input.ReversedLinesFileReader$FilePart r0 = new org.apache.commons.io.input.ReversedLinesFileReader$FilePart
            long r2 = r10.totalBlockCount
            r5 = 0
            r6 = 0
            r1 = r10
            r0.<init>(r2, r4, r5)
            r10.currentFilePart = r0
            java.nio.charset.Charset r7 = org.apache.commons.io.Charsets.toCharset(r13)
            java.nio.charset.CharsetEncoder r8 = r7.newEncoder()
            float r9 = r8.maxBytesPerChar()
            r0 = 1065353216(0x3f800000, float:1.0)
            int r0 = (r9 > r0 ? 1 : (r9 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x0085
            r0 = 1
            r10.byteDecrement = r0
        L_0x004c:
            r0 = 3
            byte[][] r0 = new byte[r0][]
            r1 = 0
            java.lang.String r2 = "\r\n"
            byte[] r2 = r2.getBytes(r13)
            r0[r1] = r2
            r1 = 1
            java.lang.String r2 = "\n"
            byte[] r2 = r2.getBytes(r13)
            r0[r1] = r2
            r1 = 2
            java.lang.String r2 = "\r"
            byte[] r2 = r2.getBytes(r13)
            r0[r1] = r2
            r10.newLineSequences = r0
            byte[][] r0 = r10.newLineSequences
            r1 = 0
            r0 = r0[r1]
            int r0 = r0.length
            r10.avoidNewlineSplitBufferSize = r0
            return
        L_0x0075:
            long r0 = r10.totalByteLength
            long r2 = (long) r12
            long r0 = r0 / r2
            r10.totalBlockCount = r0
            long r0 = r10.totalByteLength
            r2 = 0
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x002b
            r4 = r12
            goto L_0x002b
        L_0x0085:
            java.lang.String r0 = "UTF-8"
            java.nio.charset.Charset r0 = java.nio.charset.Charset.forName(r0)
            if (r7 != r0) goto L_0x0091
            r0 = 1
            r10.byteDecrement = r0
            goto L_0x004c
        L_0x0091:
            java.lang.String r0 = "Shift_JIS"
            java.nio.charset.Charset r0 = java.nio.charset.Charset.forName(r0)
            if (r7 != r0) goto L_0x009d
            r0 = 1
            r10.byteDecrement = r0
            goto L_0x004c
        L_0x009d:
            java.lang.String r0 = "UTF-16BE"
            java.nio.charset.Charset r0 = java.nio.charset.Charset.forName(r0)
            if (r7 == r0) goto L_0x00ad
            java.lang.String r0 = "UTF-16LE"
            java.nio.charset.Charset r0 = java.nio.charset.Charset.forName(r0)
            if (r7 != r0) goto L_0x00b1
        L_0x00ad:
            r0 = 2
            r10.byteDecrement = r0
            goto L_0x004c
        L_0x00b1:
            java.lang.String r0 = "UTF-16"
            java.nio.charset.Charset r0 = java.nio.charset.Charset.forName(r0)
            if (r7 != r0) goto L_0x00c1
            java.io.UnsupportedEncodingException r0 = new java.io.UnsupportedEncodingException
            java.lang.String r1 = "For UTF-16, you need to specify the byte order (use UTF-16BE or UTF-16LE)"
            r0.<init>(r1)
            throw r0
        L_0x00c1:
            java.io.UnsupportedEncodingException r0 = new java.io.UnsupportedEncodingException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Encoding "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r13)
            java.lang.String r2 = " is not supported yet (feel free to submit a patch)"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.commons.io.input.ReversedLinesFileReader.<init>(java.io.File, int, java.nio.charset.Charset):void");
    }

    public ReversedLinesFileReader(File file, int blockSize2, String encoding2) throws IOException {
        this(file, blockSize2, Charsets.toCharset(encoding2));
    }

    public String readLine() throws IOException {
        String line = this.currentFilePart.readLine();
        while (line == null) {
            this.currentFilePart = this.currentFilePart.rollOver();
            if (this.currentFilePart == null) {
                break;
            }
            line = this.currentFilePart.readLine();
        }
        if (!"".equals(line) || this.trailingNewlineOfFileSkipped) {
            return line;
        }
        this.trailingNewlineOfFileSkipped = true;
        return readLine();
    }

    public void close() throws IOException {
        this.randomAccessFile.close();
    }

    private class FilePart {
        private int currentLastBytePos;
        private final byte[] data;
        private byte[] leftOver;
        private final long no;

        private FilePart(long no2, int length, byte[] leftOverOfLastFilePart) throws IOException {
            int i;
            this.no = no2;
            if (leftOverOfLastFilePart != null) {
                i = leftOverOfLastFilePart.length;
            } else {
                i = 0;
            }
            this.data = new byte[(length + i)];
            long off = (no2 - 1) * ((long) ReversedLinesFileReader.this.blockSize);
            if (no2 > 0) {
                ReversedLinesFileReader.this.randomAccessFile.seek(off);
                if (ReversedLinesFileReader.this.randomAccessFile.read(this.data, 0, length) != length) {
                    throw new IllegalStateException("Count of requested bytes and actually read bytes don't match");
                }
            }
            if (leftOverOfLastFilePart != null) {
                System.arraycopy(leftOverOfLastFilePart, 0, this.data, length, leftOverOfLastFilePart.length);
            }
            this.currentLastBytePos = this.data.length - 1;
            this.leftOver = null;
        }

        /* access modifiers changed from: private */
        public FilePart rollOver() throws IOException {
            if (this.currentLastBytePos > -1) {
                throw new IllegalStateException("Current currentLastCharPos unexpectedly positive... last readLine() should have returned something! currentLastCharPos=" + this.currentLastBytePos);
            } else if (this.no > 1) {
                return new FilePart(this.no - 1, ReversedLinesFileReader.this.blockSize, this.leftOver);
            } else {
                if (this.leftOver == null) {
                    return null;
                }
                throw new IllegalStateException("Unexpected leftover of the last block: leftOverOfThisFilePart=" + new String(this.leftOver, ReversedLinesFileReader.this.encoding));
            }
        }

        /* access modifiers changed from: private */
        public String readLine() throws IOException {
            boolean isLastFilePart;
            String line = null;
            if (this.no == 1) {
                isLastFilePart = true;
            } else {
                isLastFilePart = false;
            }
            int i = this.currentLastBytePos;
            while (true) {
                if (i > -1) {
                    if (!isLastFilePart && i < ReversedLinesFileReader.this.avoidNewlineSplitBufferSize) {
                        createLeftOver();
                        break;
                    }
                    int newLineMatchByteCount = getNewLineMatchByteCount(this.data, i);
                    if (newLineMatchByteCount <= 0) {
                        i -= ReversedLinesFileReader.this.byteDecrement;
                        if (i < 0) {
                            createLeftOver();
                            break;
                        }
                    } else {
                        int lineStart = i + 1;
                        int lineLengthBytes = (this.currentLastBytePos - lineStart) + 1;
                        if (lineLengthBytes < 0) {
                            throw new IllegalStateException("Unexpected negative line length=" + lineLengthBytes);
                        }
                        byte[] lineData = new byte[lineLengthBytes];
                        System.arraycopy(this.data, lineStart, lineData, 0, lineLengthBytes);
                        line = new String(lineData, ReversedLinesFileReader.this.encoding);
                        this.currentLastBytePos = i - newLineMatchByteCount;
                    }
                } else {
                    break;
                }
            }
            if (!isLastFilePart || this.leftOver == null) {
                return line;
            }
            String line2 = new String(this.leftOver, ReversedLinesFileReader.this.encoding);
            this.leftOver = null;
            return line2;
        }

        private void createLeftOver() {
            int lineLengthBytes = this.currentLastBytePos + 1;
            if (lineLengthBytes > 0) {
                this.leftOver = new byte[lineLengthBytes];
                System.arraycopy(this.data, 0, this.leftOver, 0, lineLengthBytes);
            } else {
                this.leftOver = null;
            }
            this.currentLastBytePos = -1;
        }

        private int getNewLineMatchByteCount(byte[] data2, int i) {
            boolean z;
            for (byte[] newLineSequence : ReversedLinesFileReader.this.newLineSequences) {
                boolean match = true;
                for (int j = newLineSequence.length - 1; j >= 0; j--) {
                    int k = (i + j) - (newLineSequence.length - 1);
                    if (k < 0 || data2[k] != newLineSequence[j]) {
                        z = false;
                    } else {
                        z = true;
                    }
                    match &= z;
                }
                if (match) {
                    return newLineSequence.length;
                }
            }
            return 0;
        }
    }
}
