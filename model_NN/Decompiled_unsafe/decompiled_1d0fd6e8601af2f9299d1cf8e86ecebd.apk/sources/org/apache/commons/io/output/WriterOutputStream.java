package org.apache.commons.io.output;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;

public class WriterOutputStream extends OutputStream {
    private static final int DEFAULT_BUFFER_SIZE = 1024;
    private final CharsetDecoder decoder;
    private final ByteBuffer decoderIn;
    private final CharBuffer decoderOut;
    private final boolean writeImmediately;
    private final Writer writer;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.nio.charset.CharsetDecoder, int, boolean):void
     arg types: [java.io.Writer, java.nio.charset.CharsetDecoder, int, int]
     candidates:
      org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.lang.String, int, boolean):void
      org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.nio.charset.Charset, int, boolean):void
      org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.nio.charset.CharsetDecoder, int, boolean):void */
    public WriterOutputStream(Writer writer2, CharsetDecoder decoder2) {
        this(writer2, decoder2, 1024, false);
    }

    public WriterOutputStream(Writer writer2, CharsetDecoder decoder2, int bufferSize, boolean writeImmediately2) {
        this.decoderIn = ByteBuffer.allocate(128);
        this.writer = writer2;
        this.decoder = decoder2;
        this.writeImmediately = writeImmediately2;
        this.decoderOut = CharBuffer.allocate(bufferSize);
    }

    public WriterOutputStream(Writer writer2, Charset charset, int bufferSize, boolean writeImmediately2) {
        this(writer2, charset.newDecoder().onMalformedInput(CodingErrorAction.REPLACE).onUnmappableCharacter(CodingErrorAction.REPLACE).replaceWith("?"), bufferSize, writeImmediately2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.nio.charset.Charset, int, boolean):void
     arg types: [java.io.Writer, java.nio.charset.Charset, int, int]
     candidates:
      org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.lang.String, int, boolean):void
      org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.nio.charset.CharsetDecoder, int, boolean):void
      org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.nio.charset.Charset, int, boolean):void */
    public WriterOutputStream(Writer writer2, Charset charset) {
        this(writer2, charset, 1024, false);
    }

    public WriterOutputStream(Writer writer2, String charsetName, int bufferSize, boolean writeImmediately2) {
        this(writer2, Charset.forName(charsetName), bufferSize, writeImmediately2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.lang.String, int, boolean):void
     arg types: [java.io.Writer, java.lang.String, int, int]
     candidates:
      org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.nio.charset.Charset, int, boolean):void
      org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.nio.charset.CharsetDecoder, int, boolean):void
      org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.lang.String, int, boolean):void */
    public WriterOutputStream(Writer writer2, String charsetName) {
        this(writer2, charsetName, 1024, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.nio.charset.Charset, int, boolean):void
     arg types: [java.io.Writer, java.nio.charset.Charset, int, int]
     candidates:
      org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.lang.String, int, boolean):void
      org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.nio.charset.CharsetDecoder, int, boolean):void
      org.apache.commons.io.output.WriterOutputStream.<init>(java.io.Writer, java.nio.charset.Charset, int, boolean):void */
    public WriterOutputStream(Writer writer2) {
        this(writer2, Charset.defaultCharset(), 1024, false);
    }

    public void write(byte[] b, int off, int len) throws IOException {
        while (len > 0) {
            int c = Math.min(len, this.decoderIn.remaining());
            this.decoderIn.put(b, off, c);
            processInput(false);
            len -= c;
            off += c;
        }
        if (this.writeImmediately) {
            flushOutput();
        }
    }

    public void write(byte[] b) throws IOException {
        write(b, 0, b.length);
    }

    public void write(int b) throws IOException {
        write(new byte[]{(byte) b}, 0, 1);
    }

    public void flush() throws IOException {
        flushOutput();
        this.writer.flush();
    }

    public void close() throws IOException {
        processInput(true);
        flushOutput();
        this.writer.close();
    }

    private void processInput(boolean endOfInput) throws IOException {
        CoderResult coderResult;
        this.decoderIn.flip();
        while (true) {
            coderResult = this.decoder.decode(this.decoderIn, this.decoderOut, endOfInput);
            if (!coderResult.isOverflow()) {
                break;
            }
            flushOutput();
        }
        if (coderResult.isUnderflow()) {
            this.decoderIn.compact();
            return;
        }
        throw new IOException("Unexpected coder result");
    }

    private void flushOutput() throws IOException {
        if (this.decoderOut.position() > 0) {
            this.writer.write(this.decoderOut.array(), 0, this.decoderOut.position());
            this.decoderOut.rewind();
        }
    }
}
