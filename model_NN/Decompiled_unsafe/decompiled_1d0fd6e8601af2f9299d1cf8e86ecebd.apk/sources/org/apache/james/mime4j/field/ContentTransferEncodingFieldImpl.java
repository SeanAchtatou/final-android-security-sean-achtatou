package org.apache.james.mime4j.field;

import java.util.Locale;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentTransferEncodingField;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.util.MimeUtil;

public class ContentTransferEncodingFieldImpl extends AbstractField implements ContentTransferEncodingField {
    public static final FieldParser<ContentTransferEncodingField> PARSER = new FieldParser<ContentTransferEncodingField>() {
        public ContentTransferEncodingField parse(Field rawField, DecodeMonitor monitor) {
            return new ContentTransferEncodingFieldImpl(rawField, monitor);
        }
    };
    private String encoding;
    private boolean parsed = false;

    ContentTransferEncodingFieldImpl(Field rawField, DecodeMonitor monitor) {
        super(rawField, monitor);
    }

    private void parse() {
        this.parsed = true;
        String body = getBody();
        if (body != null) {
            this.encoding = body.trim().toLowerCase(Locale.US);
        } else {
            this.encoding = null;
        }
    }

    public String getEncoding() {
        if (!this.parsed) {
            parse();
        }
        return this.encoding;
    }

    public static String getEncoding(ContentTransferEncodingField f) {
        if (f == null || f.getEncoding().length() == 0) {
            return MimeUtil.ENC_7BIT;
        }
        return f.getEncoding();
    }
}
