package org.apache.james.mime4j.dom.field;

import java.util.Date;
import java.util.Map;

public interface ContentDispositionField extends ParsedField {
    public static final String DISPOSITION_TYPE_ATTACHMENT = "attachment";
    public static final String DISPOSITION_TYPE_INLINE = "inline";
    public static final String PARAM_CREATION_DATE = "creation-date";
    public static final String PARAM_FILENAME = "filename";
    public static final String PARAM_MODIFICATION_DATE = "modification-date";
    public static final String PARAM_READ_DATE = "read-date";
    public static final String PARAM_SIZE = "size";

    Date getCreationDate();

    String getDispositionType();

    String getFilename();

    Date getModificationDate();

    String getParameter(String str);

    Map<String, String> getParameters();

    Date getReadDate();

    long getSize();

    boolean isAttachment();

    boolean isDispositionType(String str);

    boolean isInline();
}
