package org.apache.james.mime4j.message;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.james.mime4j.dom.field.ContentDescriptionField;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.dom.field.ContentIdField;
import org.apache.james.mime4j.dom.field.ContentLanguageField;
import org.apache.james.mime4j.dom.field.ContentLengthField;
import org.apache.james.mime4j.dom.field.ContentLocationField;
import org.apache.james.mime4j.dom.field.ContentMD5Field;
import org.apache.james.mime4j.dom.field.ContentTransferEncodingField;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.dom.field.FieldName;
import org.apache.james.mime4j.dom.field.MimeVersionField;
import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.stream.BodyDescriptor;
import org.apache.james.mime4j.util.MimeUtil;

public class MaximalBodyDescriptor implements BodyDescriptor {
    private static final String CONTENT_DESCRIPTION = FieldName.CONTENT_DESCRIPTION.toLowerCase(Locale.US);
    private static final String CONTENT_DISPOSITION = "Content-Disposition".toLowerCase(Locale.US);
    private static final String CONTENT_ID = "Content-ID".toLowerCase(Locale.US);
    private static final String CONTENT_LANGUAGE = FieldName.CONTENT_LANGUAGE.toLowerCase(Locale.US);
    private static final String CONTENT_LENGTH = FieldName.CONTENT_LENGTH.toLowerCase(Locale.US);
    private static final String CONTENT_LOCATION = FieldName.CONTENT_LOCATION.toLowerCase(Locale.US);
    private static final String CONTENT_MD5 = FieldName.CONTENT_MD5.toLowerCase(Locale.US);
    private static final String CONTENT_TRANSFER_ENCODING = "Content-Transfer-Encoding".toLowerCase(Locale.US);
    private static final String CONTENT_TYPE = "Content-Type".toLowerCase(Locale.US);
    private static final String MIME_VERSION = FieldName.MIME_VERSION.toLowerCase(Locale.US);
    private final String boundary;
    private final String charset;
    private final Map<String, ParsedField> fields;
    private final String mediaType;
    private final String mimeType;
    private final String subType;

    MaximalBodyDescriptor(String mimeType2, String mediaType2, String subType2, String boundary2, String charset2, Map<String, ParsedField> fields2) {
        this.mimeType = mimeType2;
        this.mediaType = mediaType2;
        this.subType = subType2;
        this.boundary = boundary2;
        this.charset = charset2;
        this.fields = fields2 != null ? new HashMap<>(fields2) : Collections.emptyMap();
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public String getBoundary() {
        return this.boundary;
    }

    public String getCharset() {
        return this.charset;
    }

    public String getMediaType() {
        return this.mediaType;
    }

    public String getSubType() {
        return this.subType;
    }

    public Map<String, String> getContentTypeParameters() {
        ContentTypeField contentTypeField = (ContentTypeField) this.fields.get(CONTENT_TYPE);
        return contentTypeField != null ? contentTypeField.getParameters() : Collections.emptyMap();
    }

    public String getTransferEncoding() {
        ContentTransferEncodingField contentTransferEncodingField = (ContentTransferEncodingField) this.fields.get(CONTENT_TRANSFER_ENCODING);
        return contentTransferEncodingField != null ? contentTransferEncodingField.getEncoding() : MimeUtil.ENC_7BIT;
    }

    public long getContentLength() {
        ContentLengthField contentLengthField = (ContentLengthField) this.fields.get(CONTENT_LENGTH);
        if (contentLengthField != null) {
            return contentLengthField.getContentLength();
        }
        return -1;
    }

    public int getMimeMajorVersion() {
        MimeVersionField mimeVersionField = (MimeVersionField) this.fields.get(MIME_VERSION);
        if (mimeVersionField != null) {
            return mimeVersionField.getMajorVersion();
        }
        return 1;
    }

    public int getMimeMinorVersion() {
        MimeVersionField mimeVersionField = (MimeVersionField) this.fields.get(MIME_VERSION);
        if (mimeVersionField != null) {
            return mimeVersionField.getMinorVersion();
        }
        return 0;
    }

    public String getContentDescription() {
        ContentDescriptionField contentDescriptionField = (ContentDescriptionField) this.fields.get(CONTENT_DESCRIPTION);
        if (contentDescriptionField != null) {
            return contentDescriptionField.getDescription();
        }
        return null;
    }

    public String getContentId() {
        ContentIdField contentIdField = (ContentIdField) this.fields.get(CONTENT_ID);
        if (contentIdField != null) {
            return contentIdField.getId();
        }
        return null;
    }

    public String getContentDispositionType() {
        ContentDispositionField contentDispositionField = (ContentDispositionField) this.fields.get(CONTENT_DISPOSITION);
        if (contentDispositionField != null) {
            return contentDispositionField.getDispositionType();
        }
        return null;
    }

    public Map<String, String> getContentDispositionParameters() {
        ContentDispositionField contentDispositionField = (ContentDispositionField) this.fields.get(CONTENT_DISPOSITION);
        return contentDispositionField != null ? contentDispositionField.getParameters() : Collections.emptyMap();
    }

    public String getContentDispositionFilename() {
        ContentDispositionField contentDispositionField = (ContentDispositionField) this.fields.get(CONTENT_DISPOSITION);
        if (contentDispositionField != null) {
            return contentDispositionField.getFilename();
        }
        return null;
    }

    public Date getContentDispositionModificationDate() {
        ContentDispositionField contentDispositionField = (ContentDispositionField) this.fields.get(CONTENT_DISPOSITION);
        if (contentDispositionField != null) {
            return contentDispositionField.getModificationDate();
        }
        return null;
    }

    public Date getContentDispositionCreationDate() {
        ContentDispositionField contentDispositionField = (ContentDispositionField) this.fields.get(CONTENT_DISPOSITION);
        if (contentDispositionField != null) {
            return contentDispositionField.getCreationDate();
        }
        return null;
    }

    public Date getContentDispositionReadDate() {
        ContentDispositionField contentDispositionField = (ContentDispositionField) this.fields.get(CONTENT_DISPOSITION);
        if (contentDispositionField != null) {
            return contentDispositionField.getReadDate();
        }
        return null;
    }

    public long getContentDispositionSize() {
        ContentDispositionField contentDispositionField = (ContentDispositionField) this.fields.get(CONTENT_DISPOSITION);
        if (contentDispositionField != null) {
            return contentDispositionField.getSize();
        }
        return -1;
    }

    public List<String> getContentLanguage() {
        ContentLanguageField contentLanguageField = (ContentLanguageField) this.fields.get(CONTENT_LANGUAGE);
        return contentLanguageField != null ? contentLanguageField.getLanguages() : Collections.emptyList();
    }

    public String getContentLocation() {
        ContentLocationField contentLocationField = (ContentLocationField) this.fields.get(CONTENT_LOCATION);
        if (contentLocationField != null) {
            return contentLocationField.getLocation();
        }
        return null;
    }

    public String getContentMD5Raw() {
        ContentMD5Field contentMD5Field = (ContentMD5Field) this.fields.get(CONTENT_MD5);
        if (contentMD5Field != null) {
            return contentMD5Field.getMD5Raw();
        }
        return null;
    }

    public String toString() {
        return "[mimeType=" + this.mimeType + ", mediaType=" + this.mediaType + ", subType=" + this.subType + ", boundary=" + this.boundary + ", charset=" + this.charset + "]";
    }
}
