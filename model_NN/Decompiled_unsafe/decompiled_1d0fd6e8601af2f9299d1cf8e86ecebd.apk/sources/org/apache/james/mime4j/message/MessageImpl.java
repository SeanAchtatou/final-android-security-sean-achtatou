package org.apache.james.mime4j.message;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.address.Address;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.field.AddressListField;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.dom.field.ContentTransferEncodingField;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.dom.field.DateTimeField;
import org.apache.james.mime4j.dom.field.FieldName;
import org.apache.james.mime4j.dom.field.MailboxField;
import org.apache.james.mime4j.dom.field.MailboxListField;
import org.apache.james.mime4j.dom.field.UnstructuredField;
import org.apache.james.mime4j.field.ContentTransferEncodingFieldImpl;
import org.apache.james.mime4j.field.ContentTypeFieldImpl;
import org.apache.james.mime4j.field.Fields;
import org.apache.james.mime4j.field.MimeVersionFieldLenientImpl;
import org.apache.james.mime4j.stream.RawField;
import org.apache.james.mime4j.util.MimeUtil;

public class MessageImpl extends AbstractMessage {
    public MessageImpl() {
        obtainHeader().addField(MimeVersionFieldLenientImpl.PARSER.parse(new RawField(FieldName.MIME_VERSION, "1.0"), DecodeMonitor.SILENT));
    }

    /* access modifiers changed from: protected */
    public String newUniqueBoundary() {
        return MimeUtil.createUniqueBoundary();
    }

    /* access modifiers changed from: protected */
    public UnstructuredField newMessageId(String hostname) {
        return Fields.messageId(hostname);
    }

    /* access modifiers changed from: protected */
    public DateTimeField newDate(Date date, TimeZone zone) {
        return Fields.date(FieldName.DATE, date, zone);
    }

    /* access modifiers changed from: protected */
    public MailboxField newMailbox(String fieldName, Mailbox mailbox) {
        return Fields.mailbox(fieldName, mailbox);
    }

    /* access modifiers changed from: protected */
    public MailboxListField newMailboxList(String fieldName, Collection<Mailbox> mailboxes) {
        return Fields.mailboxList(fieldName, mailboxes);
    }

    /* access modifiers changed from: protected */
    public AddressListField newAddressList(String fieldName, Collection<? extends Address> addresses) {
        return Fields.addressList(fieldName, addresses);
    }

    /* access modifiers changed from: protected */
    public UnstructuredField newSubject(String subject) {
        return Fields.subject(subject);
    }

    /* access modifiers changed from: protected */
    public ContentDispositionField newContentDisposition(String dispositionType, String filename, long size, Date creationDate, Date modificationDate, Date readDate) {
        return Fields.contentDisposition(dispositionType, filename, size, creationDate, modificationDate, readDate);
    }

    /* access modifiers changed from: protected */
    public ContentDispositionField newContentDisposition(String dispositionType, Map<String, String> parameters) {
        return Fields.contentDisposition(dispositionType, parameters);
    }

    /* access modifiers changed from: protected */
    public ContentTypeField newContentType(String mimeType, Map<String, String> parameters) {
        return Fields.contentType(mimeType, parameters);
    }

    /* access modifiers changed from: protected */
    public ContentTransferEncodingField newContentTransferEncoding(String contentTransferEncoding) {
        return Fields.contentTransferEncoding(contentTransferEncoding);
    }

    /* access modifiers changed from: protected */
    public String calcTransferEncoding(ContentTransferEncodingField f) {
        return ContentTransferEncodingFieldImpl.getEncoding(f);
    }

    /* access modifiers changed from: protected */
    public String calcMimeType(ContentTypeField child, ContentTypeField parent) {
        return ContentTypeFieldImpl.getMimeType(child, parent);
    }

    /* access modifiers changed from: protected */
    public String calcCharset(ContentTypeField contentType) {
        return ContentTypeFieldImpl.getCharset(contentType);
    }
}
