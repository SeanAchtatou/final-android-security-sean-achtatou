package org.apache.james.mime4j.stream;

import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.util.ByteArrayBuffer;

public interface FieldBuilder {
    void append(ByteArrayBuffer byteArrayBuffer) throws MimeException;

    RawField build() throws MimeException;

    ByteArrayBuffer getRaw();

    void reset();
}
