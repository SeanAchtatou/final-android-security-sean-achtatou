package org.apache.james.mime4j.stream;

public final class MimeConfig implements Cloneable {
    private boolean countLineNumbers = false;
    private String headlessParsing = null;
    private boolean malformedHeaderStartsBody = false;
    private long maxContentLen = -1;
    private int maxHeaderCount = 1000;
    private int maxHeaderLen = 10000;
    private int maxLineLen = 1000;
    private boolean strictParsing = false;

    public boolean isMalformedHeaderStartsBody() {
        return this.malformedHeaderStartsBody;
    }

    public void setMalformedHeaderStartsBody(boolean malformedHeaderStartsBody2) {
        this.malformedHeaderStartsBody = malformedHeaderStartsBody2;
    }

    public boolean isStrictParsing() {
        return this.strictParsing;
    }

    public void setStrictParsing(boolean strictParsing2) {
        this.strictParsing = strictParsing2;
    }

    public int getMaxLineLen() {
        return this.maxLineLen;
    }

    public void setMaxLineLen(int maxLineLen2) {
        this.maxLineLen = maxLineLen2;
    }

    public int getMaxHeaderCount() {
        return this.maxHeaderCount;
    }

    public void setMaxHeaderCount(int maxHeaderCount2) {
        this.maxHeaderCount = maxHeaderCount2;
    }

    public int getMaxHeaderLen() {
        return this.maxHeaderLen;
    }

    public void setMaxHeaderLen(int maxHeaderLen2) {
        this.maxHeaderLen = maxHeaderLen2;
    }

    public long getMaxContentLen() {
        return this.maxContentLen;
    }

    public void setMaxContentLen(long maxContentLen2) {
        this.maxContentLen = maxContentLen2;
    }

    public boolean isCountLineNumbers() {
        return this.countLineNumbers;
    }

    public void setCountLineNumbers(boolean countLineNumbers2) {
        this.countLineNumbers = countLineNumbers2;
    }

    public String getHeadlessParsing() {
        return this.headlessParsing;
    }

    public void setHeadlessParsing(String contentType) {
        this.headlessParsing = contentType;
    }

    public MimeConfig clone() {
        try {
            return (MimeConfig) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }

    public String toString() {
        return "[strict parsing: " + this.strictParsing + ", max line length: " + this.maxLineLen + ", max header count: " + this.maxHeaderCount + ", max content length: " + this.maxContentLen + ", count line numbers: " + this.countLineNumbers + "]";
    }
}
