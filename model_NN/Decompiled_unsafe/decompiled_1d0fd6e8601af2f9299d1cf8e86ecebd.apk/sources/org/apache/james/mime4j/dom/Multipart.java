package org.apache.james.mime4j.dom;

import java.util.List;

public interface Multipart extends Body {
    void addBodyPart(Entity entity);

    void addBodyPart(Entity entity, int i);

    List<Entity> getBodyParts();

    int getCount();

    String getEpilogue();

    String getPreamble();

    String getSubType();

    Entity removeBodyPart(int i);

    Entity replaceBodyPart(Entity entity, int i);

    void setBodyParts(List<Entity> list);

    void setEpilogue(String str);

    void setPreamble(String str);
}
