package org.apache.james.mime4j.field;

import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.AddressListField;
import org.apache.james.mime4j.dom.field.DateTimeField;
import org.apache.james.mime4j.dom.field.FieldName;
import org.apache.james.mime4j.dom.field.MailboxField;
import org.apache.james.mime4j.dom.field.MailboxListField;
import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.stream.RawFieldParser;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.ContentUtil;

public class LenientFieldParser extends DelegatingFieldParser {
    private static final FieldParser<ParsedField> PARSER = new LenientFieldParser();

    public static FieldParser<ParsedField> getParser() {
        return PARSER;
    }

    public static ParsedField parse(ByteSequence raw, DecodeMonitor monitor) throws MimeException {
        return PARSER.parse(RawFieldParser.DEFAULT.parseField(raw), monitor);
    }

    public static ParsedField parse(String rawStr, DecodeMonitor monitor) throws MimeException {
        return PARSER.parse(RawFieldParser.DEFAULT.parseField(ContentUtil.encode(rawStr)), monitor);
    }

    public static ParsedField parse(String rawStr) throws MimeException {
        return parse(rawStr, DecodeMonitor.SILENT);
    }

    public LenientFieldParser() {
        super(UnstructuredFieldImpl.PARSER);
        setFieldParser("Content-Type", ContentTypeFieldLenientImpl.PARSER);
        setFieldParser(FieldName.CONTENT_LENGTH, ContentLengthFieldImpl.PARSER);
        setFieldParser("Content-Transfer-Encoding", ContentTransferEncodingFieldImpl.PARSER);
        setFieldParser("Content-Disposition", ContentDispositionFieldLenientImpl.PARSER);
        setFieldParser("Content-ID", ContentIdFieldImpl.PARSER);
        setFieldParser(FieldName.CONTENT_MD5, ContentMD5FieldImpl.PARSER);
        setFieldParser(FieldName.CONTENT_DESCRIPTION, ContentDescriptionFieldImpl.PARSER);
        setFieldParser(FieldName.CONTENT_LANGUAGE, ContentLanguageFieldLenientImpl.PARSER);
        setFieldParser(FieldName.CONTENT_LOCATION, ContentLocationFieldLenientImpl.PARSER);
        setFieldParser(FieldName.MIME_VERSION, MimeVersionFieldImpl.PARSER);
        FieldParser<DateTimeField> dateTimeParser = DateTimeFieldLenientImpl.PARSER;
        setFieldParser(FieldName.DATE, dateTimeParser);
        setFieldParser(FieldName.RESENT_DATE, dateTimeParser);
        FieldParser<MailboxListField> mailboxListParser = MailboxListFieldLenientImpl.PARSER;
        setFieldParser(FieldName.FROM, mailboxListParser);
        setFieldParser(FieldName.RESENT_FROM, mailboxListParser);
        FieldParser<MailboxField> mailboxParser = MailboxFieldLenientImpl.PARSER;
        setFieldParser(FieldName.SENDER, mailboxParser);
        setFieldParser(FieldName.RESENT_SENDER, mailboxParser);
        FieldParser<AddressListField> addressListParser = AddressListFieldLenientImpl.PARSER;
        setFieldParser(FieldName.TO, addressListParser);
        setFieldParser(FieldName.RESENT_TO, addressListParser);
        setFieldParser(FieldName.CC, addressListParser);
        setFieldParser(FieldName.RESENT_CC, addressListParser);
        setFieldParser(FieldName.BCC, addressListParser);
        setFieldParser(FieldName.RESENT_BCC, addressListParser);
        setFieldParser(FieldName.REPLY_TO, addressListParser);
    }
}
