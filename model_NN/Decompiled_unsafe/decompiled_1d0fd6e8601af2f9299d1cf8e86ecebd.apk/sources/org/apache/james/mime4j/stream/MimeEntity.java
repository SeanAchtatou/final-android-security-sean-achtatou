package org.apache.james.mime4j.stream;

import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.codec.Base64InputStream;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.codec.QuotedPrintableInputStream;
import org.apache.james.mime4j.io.BufferedLineReaderInputStream;
import org.apache.james.mime4j.io.LimitedInputStream;
import org.apache.james.mime4j.io.LineNumberSource;
import org.apache.james.mime4j.io.LineReaderInputStream;
import org.apache.james.mime4j.io.LineReaderInputStreamAdaptor;
import org.apache.james.mime4j.io.MaxHeaderLimitException;
import org.apache.james.mime4j.io.MaxLineLimitException;
import org.apache.james.mime4j.io.MimeBoundaryInputStream;
import org.apache.james.mime4j.util.ByteArrayBuffer;
import org.apache.james.mime4j.util.MimeUtil;

class MimeEntity implements EntityStateMachine {
    private BodyDescriptor body;
    private final BodyDescriptorBuilder bodyDescBuilder;
    private final MimeConfig config;
    private MimeBoundaryInputStream currentMimePartStream;
    private LineReaderInputStreamAdaptor dataStream;
    private boolean endOfHeader;
    private final EntityState endState;
    private Field field;
    private final FieldBuilder fieldBuilder;
    private int headerCount;
    private final BufferedLineReaderInputStream inbuffer;
    private int lineCount;
    private final LineNumberSource lineSource;
    private final ByteArrayBuffer linebuf;
    private final DecodeMonitor monitor;
    private RecursionMode recursionMode;
    private EntityState state;
    private byte[] tmpbuf;

    MimeEntity(LineNumberSource lineSource2, InputStream instream, MimeConfig config2, EntityState startState, EntityState endState2, DecodeMonitor monitor2, FieldBuilder fieldBuilder2, BodyDescriptorBuilder bodyDescBuilder2) {
        this.config = config2;
        this.state = startState;
        this.endState = endState2;
        this.monitor = monitor2;
        this.fieldBuilder = fieldBuilder2;
        this.bodyDescBuilder = bodyDescBuilder2;
        this.linebuf = new ByteArrayBuffer(64);
        this.lineCount = 0;
        this.endOfHeader = false;
        this.headerCount = 0;
        this.lineSource = lineSource2;
        this.inbuffer = new BufferedLineReaderInputStream(instream, 4096, config2.getMaxLineLen());
        this.dataStream = new LineReaderInputStreamAdaptor(this.inbuffer, config2.getMaxLineLen());
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    MimeEntity(LineNumberSource lineSource2, InputStream instream, MimeConfig config2, EntityState startState, EntityState endState2, BodyDescriptorBuilder bodyDescBuilder2) {
        this(lineSource2, instream, config2, startState, endState2, config2.isStrictParsing() ? DecodeMonitor.STRICT : DecodeMonitor.SILENT, new DefaultFieldBuilder(config2.getMaxHeaderLen()), bodyDescBuilder2);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    MimeEntity(LineNumberSource lineSource2, InputStream instream, MimeConfig config2, BodyDescriptorBuilder bodyDescBuilder2) {
        this(lineSource2, instream, config2, EntityState.T_START_MESSAGE, EntityState.T_END_MESSAGE, config2.isStrictParsing() ? DecodeMonitor.STRICT : DecodeMonitor.SILENT, new DefaultFieldBuilder(config2.getMaxHeaderLen()), bodyDescBuilder2);
    }

    MimeEntity(LineNumberSource lineSource2, InputStream instream, FieldBuilder fieldBuilder2, BodyDescriptorBuilder bodyDescBuilder2) {
        this(lineSource2, instream, new MimeConfig(), EntityState.T_START_MESSAGE, EntityState.T_END_MESSAGE, DecodeMonitor.SILENT, fieldBuilder2, bodyDescBuilder2);
    }

    MimeEntity(LineNumberSource lineSource2, InputStream instream, BodyDescriptorBuilder bodyDescBuilder2) {
        this(lineSource2, instream, new MimeConfig(), EntityState.T_START_MESSAGE, EntityState.T_END_MESSAGE, DecodeMonitor.SILENT, new DefaultFieldBuilder(-1), bodyDescBuilder2);
    }

    public EntityState getState() {
        return this.state;
    }

    public RecursionMode getRecursionMode() {
        return this.recursionMode;
    }

    public void setRecursionMode(RecursionMode recursionMode2) {
        this.recursionMode = recursionMode2;
    }

    public void stop() {
        this.inbuffer.truncate();
    }

    private int getLineNumber() {
        if (this.lineSource == null) {
            return -1;
        }
        return this.lineSource.getLineNumber();
    }

    private LineReaderInputStream getDataStream() {
        return this.dataStream;
    }

    /* access modifiers changed from: protected */
    public String message(Event event) {
        String message;
        if (event == null) {
            message = "Event is unexpectedly null.";
        } else {
            message = event.toString();
        }
        int lineNumber = getLineNumber();
        return lineNumber <= 0 ? message : "Line " + lineNumber + ": " + message;
    }

    /* access modifiers changed from: protected */
    public void monitor(Event event) throws MimeException, IOException {
        if (this.monitor.isListening()) {
            if (this.monitor.warn(message(event), "ignoring")) {
                throw new MimeParseEventException(event);
            }
        }
    }

    private void readRawField() throws IOException, MimeException {
        if (this.endOfHeader) {
            throw new IllegalStateException();
        }
        LineReaderInputStream instream = getDataStream();
        while (true) {
            try {
                if (this.linebuf.length() > 0) {
                    this.fieldBuilder.append(this.linebuf);
                }
                this.linebuf.clear();
                if (instream.readLine(this.linebuf) == -1) {
                    monitor(Event.HEADERS_PREMATURE_END);
                    this.endOfHeader = true;
                    return;
                }
                int len = this.linebuf.length();
                if (len > 0 && this.linebuf.byteAt(len - 1) == 10) {
                    len--;
                }
                if (len > 0 && this.linebuf.byteAt(len - 1) == 13) {
                    len--;
                }
                if (len == 0) {
                    this.endOfHeader = true;
                    return;
                }
                this.lineCount++;
                if (this.lineCount > 1) {
                    int ch = this.linebuf.byteAt(0);
                    if (!(ch == 32 || ch == 9)) {
                        return;
                    }
                }
            } catch (MaxLineLimitException e) {
                throw new MimeException(e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean nextField() throws MimeException, IOException {
        int maxHeaderCount = this.config.getMaxHeaderCount();
        while (!this.endOfHeader) {
            if (maxHeaderCount <= 0 || this.headerCount < maxHeaderCount) {
                this.headerCount++;
                this.fieldBuilder.reset();
                readRawField();
                try {
                    RawField rawfield = this.fieldBuilder.build();
                    if (rawfield != null) {
                        if (rawfield.getDelimiterIdx() != rawfield.getName().length()) {
                            monitor(Event.OBSOLETE_HEADER);
                        }
                        Field parsedField = this.bodyDescBuilder.addField(rawfield);
                        if (parsedField == null) {
                            parsedField = rawfield;
                        }
                        this.field = parsedField;
                        return true;
                    }
                } catch (MimeException e) {
                    monitor(Event.INVALID_HEADER);
                    if (this.config.isMalformedHeaderStartsBody()) {
                        LineReaderInputStream instream = getDataStream();
                        ByteArrayBuffer buf = this.fieldBuilder.getRaw();
                        if (buf != null && instream.unread(buf)) {
                            return false;
                        }
                        throw new MimeParseEventException(Event.INVALID_HEADER);
                    }
                }
            } else {
                throw new MaxHeaderLimitException("Maximum header limit exceeded");
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0094, code lost:
        if (r5.currentMimePartStream.isEmptyStream() != false) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00bb, code lost:
        if (r0 != false) goto L_0x00bd;
     */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0033  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.james.mime4j.stream.EntityStateMachine advance() throws java.io.IOException, org.apache.james.mime4j.MimeException {
        /*
            r5 = this;
            int[] r2 = org.apache.james.mime4j.stream.MimeEntity.AnonymousClass1.$SwitchMap$org$apache$james$mime4j$stream$EntityState
            org.apache.james.mime4j.stream.EntityState r3 = r5.state
            int r3 = r3.ordinal()
            r2 = r2[r3]
            switch(r2) {
                case 1: goto L_0x0019;
                case 2: goto L_0x001e;
                case 3: goto L_0x0023;
                case 4: goto L_0x0028;
                case 5: goto L_0x0036;
                case 6: goto L_0x0077;
                case 7: goto L_0x0096;
                case 8: goto L_0x00bd;
                case 9: goto L_0x00d7;
                case 10: goto L_0x00d7;
                default: goto L_0x000d;
            }
        L_0x000d:
            org.apache.james.mime4j.stream.EntityState r2 = r5.state
            org.apache.james.mime4j.stream.EntityState r3 = r5.endState
            if (r2 != r3) goto L_0x00dd
            org.apache.james.mime4j.stream.EntityState r2 = org.apache.james.mime4j.stream.EntityState.T_END_OF_STREAM
            r5.state = r2
        L_0x0017:
            r2 = 0
        L_0x0018:
            return r2
        L_0x0019:
            org.apache.james.mime4j.stream.EntityState r2 = org.apache.james.mime4j.stream.EntityState.T_START_HEADER
            r5.state = r2
            goto L_0x0017
        L_0x001e:
            org.apache.james.mime4j.stream.EntityState r2 = org.apache.james.mime4j.stream.EntityState.T_START_HEADER
            r5.state = r2
            goto L_0x0017
        L_0x0023:
            org.apache.james.mime4j.stream.BodyDescriptorBuilder r2 = r5.bodyDescBuilder
            r2.reset()
        L_0x0028:
            boolean r2 = r5.nextField()
            if (r2 == 0) goto L_0x0033
            org.apache.james.mime4j.stream.EntityState r2 = org.apache.james.mime4j.stream.EntityState.T_FIELD
        L_0x0030:
            r5.state = r2
            goto L_0x0017
        L_0x0033:
            org.apache.james.mime4j.stream.EntityState r2 = org.apache.james.mime4j.stream.EntityState.T_END_HEADER
            goto L_0x0030
        L_0x0036:
            org.apache.james.mime4j.stream.BodyDescriptorBuilder r2 = r5.bodyDescBuilder
            org.apache.james.mime4j.stream.BodyDescriptor r2 = r2.build()
            r5.body = r2
            org.apache.james.mime4j.stream.BodyDescriptor r2 = r5.body
            java.lang.String r1 = r2.getMimeType()
            org.apache.james.mime4j.stream.RecursionMode r2 = r5.recursionMode
            org.apache.james.mime4j.stream.RecursionMode r3 = org.apache.james.mime4j.stream.RecursionMode.M_FLAT
            if (r2 != r3) goto L_0x004f
            org.apache.james.mime4j.stream.EntityState r2 = org.apache.james.mime4j.stream.EntityState.T_BODY
            r5.state = r2
            goto L_0x0017
        L_0x004f:
            boolean r2 = org.apache.james.mime4j.util.MimeUtil.isMultipart(r1)
            if (r2 == 0) goto L_0x005d
            org.apache.james.mime4j.stream.EntityState r2 = org.apache.james.mime4j.stream.EntityState.T_START_MULTIPART
            r5.state = r2
            r5.clearMimePartStream()
            goto L_0x0017
        L_0x005d:
            org.apache.james.mime4j.stream.RecursionMode r2 = r5.recursionMode
            org.apache.james.mime4j.stream.RecursionMode r3 = org.apache.james.mime4j.stream.RecursionMode.M_NO_RECURSE
            if (r2 == r3) goto L_0x0072
            boolean r2 = org.apache.james.mime4j.util.MimeUtil.isMessage(r1)
            if (r2 == 0) goto L_0x0072
            org.apache.james.mime4j.stream.EntityState r2 = org.apache.james.mime4j.stream.EntityState.T_BODY
            r5.state = r2
            org.apache.james.mime4j.stream.EntityStateMachine r2 = r5.nextMessage()
            goto L_0x0018
        L_0x0072:
            org.apache.james.mime4j.stream.EntityState r2 = org.apache.james.mime4j.stream.EntityState.T_BODY
            r5.state = r2
            goto L_0x0017
        L_0x0077:
            org.apache.james.mime4j.io.LineReaderInputStreamAdaptor r2 = r5.dataStream
            boolean r2 = r2.isUsed()
            if (r2 == 0) goto L_0x0087
            r5.advanceToBoundary()
            org.apache.james.mime4j.stream.EntityState r2 = org.apache.james.mime4j.stream.EntityState.T_END_MULTIPART
            r5.state = r2
            goto L_0x0017
        L_0x0087:
            r5.createMimePartStream()
            org.apache.james.mime4j.stream.EntityState r2 = org.apache.james.mime4j.stream.EntityState.T_PREAMBLE
            r5.state = r2
            org.apache.james.mime4j.io.MimeBoundaryInputStream r2 = r5.currentMimePartStream
            boolean r0 = r2.isEmptyStream()
            if (r0 == 0) goto L_0x0017
        L_0x0096:
            r5.advanceToBoundary()
            org.apache.james.mime4j.io.MimeBoundaryInputStream r2 = r5.currentMimePartStream
            boolean r2 = r2.eof()
            if (r2 == 0) goto L_0x00c3
            org.apache.james.mime4j.io.MimeBoundaryInputStream r2 = r5.currentMimePartStream
            boolean r2 = r2.isLastPart()
            if (r2 != 0) goto L_0x00c3
            org.apache.james.mime4j.stream.Event r2 = org.apache.james.mime4j.stream.Event.MIME_BODY_PREMATURE_END
            r5.monitor(r2)
        L_0x00ae:
            org.apache.james.mime4j.io.MimeBoundaryInputStream r2 = r5.currentMimePartStream
            boolean r0 = r2.isFullyConsumed()
            r5.clearMimePartStream()
            org.apache.james.mime4j.stream.EntityState r2 = org.apache.james.mime4j.stream.EntityState.T_EPILOGUE
            r5.state = r2
            if (r0 == 0) goto L_0x0017
        L_0x00bd:
            org.apache.james.mime4j.stream.EntityState r2 = org.apache.james.mime4j.stream.EntityState.T_END_MULTIPART
            r5.state = r2
            goto L_0x0017
        L_0x00c3:
            org.apache.james.mime4j.io.MimeBoundaryInputStream r2 = r5.currentMimePartStream
            boolean r2 = r2.isLastPart()
            if (r2 != 0) goto L_0x00ae
            r5.clearMimePartStream()
            r5.createMimePartStream()
            org.apache.james.mime4j.stream.EntityStateMachine r2 = r5.nextMimeEntity()
            goto L_0x0018
        L_0x00d7:
            org.apache.james.mime4j.stream.EntityState r2 = r5.endState
            r5.state = r2
            goto L_0x0017
        L_0x00dd:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Invalid state: "
            java.lang.StringBuilder r3 = r3.append(r4)
            org.apache.james.mime4j.stream.EntityState r4 = r5.state
            java.lang.String r4 = stateToString(r4)
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.stream.MimeEntity.advance():org.apache.james.mime4j.stream.EntityStateMachine");
    }

    private void createMimePartStream() throws MimeException, IOException {
        try {
            this.currentMimePartStream = new MimeBoundaryInputStream(this.inbuffer, this.body.getBoundary(), this.config.isStrictParsing());
            this.dataStream = new LineReaderInputStreamAdaptor(this.currentMimePartStream, this.config.getMaxLineLen());
        } catch (IllegalArgumentException e) {
            throw new MimeException(e.getMessage(), e);
        }
    }

    private void clearMimePartStream() {
        this.currentMimePartStream = null;
        this.dataStream = new LineReaderInputStreamAdaptor(this.inbuffer, this.config.getMaxLineLen());
    }

    private void advanceToBoundary() throws IOException {
        if (!this.dataStream.eof()) {
            if (this.tmpbuf == null) {
                this.tmpbuf = new byte[2048];
            }
            do {
            } while (getLimitedContentStream().read(this.tmpbuf) != -1);
        }
    }

    private EntityStateMachine nextMessage() {
        return nextMimeEntity(EntityState.T_START_MESSAGE, EntityState.T_END_MESSAGE, decodedStream(this.currentMimePartStream != null ? this.currentMimePartStream : this.inbuffer));
    }

    private InputStream decodedStream(InputStream instream) {
        String transferEncoding = this.body.getTransferEncoding();
        if (MimeUtil.isBase64Encoding(transferEncoding)) {
            return new Base64InputStream(instream, this.monitor);
        }
        if (MimeUtil.isQuotedPrintableEncoded(transferEncoding)) {
            return new QuotedPrintableInputStream(instream, this.monitor);
        }
        return instream;
    }

    private EntityStateMachine nextMimeEntity() {
        return nextMimeEntity(EntityState.T_START_BODYPART, EntityState.T_END_BODYPART, this.currentMimePartStream);
    }

    private EntityStateMachine nextMimeEntity(EntityState startState, EntityState endState2, InputStream instream) {
        if (this.recursionMode == RecursionMode.M_RAW) {
            return new RawEntity(instream);
        }
        MimeEntity mimeentity = new MimeEntity(this.lineSource, instream, this.config, startState, endState2, this.monitor, this.fieldBuilder, this.bodyDescBuilder.newChild());
        mimeentity.setRecursionMode(this.recursionMode);
        return mimeentity;
    }

    private InputStream getLimitedContentStream() {
        long maxContentLimit = this.config.getMaxContentLen();
        if (maxContentLimit >= 0) {
            return new LimitedInputStream(this.dataStream, maxContentLimit);
        }
        return this.dataStream;
    }

    public BodyDescriptor getBodyDescriptor() {
        switch (getState()) {
            case T_START_MULTIPART:
            case T_PREAMBLE:
            case T_EPILOGUE:
            case T_BODY:
            case T_END_OF_STREAM:
                break;
            case T_END_MULTIPART:
            default:
                throw new IllegalStateException("Invalid state :" + stateToString(this.state));
        }
        return this.body;
    }

    public Field getField() {
        switch (getState()) {
            case T_FIELD:
                break;
            default:
                throw new IllegalStateException("Invalid state :" + stateToString(this.state));
        }
        return this.field;
    }

    public InputStream getContentStream() {
        switch (this.state) {
            case T_START_MULTIPART:
            case T_PREAMBLE:
            case T_EPILOGUE:
            case T_BODY:
                break;
            default:
                throw new IllegalStateException("Invalid state: " + stateToString(this.state));
        }
        return getLimitedContentStream();
    }

    public InputStream getDecodedContentStream() throws IllegalStateException {
        return decodedStream(getContentStream());
    }

    public String toString() {
        return getClass().getName() + " [" + stateToString(this.state) + "][" + this.body.getMimeType() + "][" + this.body.getBoundary() + "]";
    }

    public static final String stateToString(EntityState state2) {
        switch (state2) {
            case T_START_MESSAGE:
                return "Start message";
            case T_START_BODYPART:
                return "Start bodypart";
            case T_START_HEADER:
                return "Start header";
            case T_FIELD:
                return "Field";
            case T_END_HEADER:
                return "End header";
            case T_START_MULTIPART:
                return "Start multipart";
            case T_PREAMBLE:
                return "Preamble";
            case T_EPILOGUE:
                return "Epilogue";
            case T_BODY:
                return "Body";
            case T_END_MULTIPART:
                return "End multipart";
            case T_END_OF_STREAM:
                return "End of stream";
            case T_END_MESSAGE:
                return "End message";
            case T_RAW_ENTITY:
                return "Raw entity";
            case T_END_BODYPART:
                return "End bodypart";
            default:
                return "Unknown";
        }
    }
}
