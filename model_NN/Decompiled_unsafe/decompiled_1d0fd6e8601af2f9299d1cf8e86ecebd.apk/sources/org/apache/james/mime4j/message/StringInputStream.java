package org.apache.james.mime4j.message;

import com.fsck.k9.ui.crypto.MessageCryptoHelper;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;

class StringInputStream extends InputStream {
    private final ByteBuffer bbuf;
    private final CharBuffer cbuf;
    private final CharsetEncoder encoder;
    private int mark;

    StringInputStream(CharSequence s, Charset charset, int bufferSize) {
        this.encoder = charset.newEncoder().onMalformedInput(CodingErrorAction.REPLACE).onUnmappableCharacter(CodingErrorAction.REPLACE);
        this.bbuf = ByteBuffer.allocate(MessageCryptoHelper.REQUEST_CODE_USER_INTERACTION);
        this.bbuf.flip();
        this.cbuf = CharBuffer.wrap(s);
        this.mark = -1;
    }

    StringInputStream(CharSequence s, Charset charset) {
        this(s, charset, 2048);
    }

    private void fillBuffer() throws IOException {
        this.bbuf.compact();
        CoderResult result = this.encoder.encode(this.cbuf, this.bbuf, true);
        if (result.isError()) {
            result.throwException();
        }
        this.bbuf.flip();
    }

    public int read(byte[] b, int off, int len) throws IOException {
        if (b == null) {
            throw new NullPointerException("Byte array is null");
        } else if (len < 0 || off + len > b.length) {
            throw new IndexOutOfBoundsException("Array Size=" + b.length + ", offset=" + off + ", length=" + len);
        } else if (!this.bbuf.hasRemaining() && !this.cbuf.hasRemaining()) {
            return -1;
        } else {
            int bytesRead = 0;
            while (len > 0) {
                if (!this.bbuf.hasRemaining()) {
                    fillBuffer();
                    if (!this.bbuf.hasRemaining() && !this.cbuf.hasRemaining()) {
                        break;
                    }
                } else {
                    int chunk = Math.min(this.bbuf.remaining(), len);
                    this.bbuf.get(b, off, chunk);
                    off += chunk;
                    len -= chunk;
                    bytesRead += chunk;
                }
            }
            if (bytesRead == 0 && !this.cbuf.hasRemaining()) {
                bytesRead = -1;
            }
            return bytesRead;
        }
    }

    public int read() throws IOException {
        while (!this.bbuf.hasRemaining()) {
            fillBuffer();
            if (!this.bbuf.hasRemaining() && !this.cbuf.hasRemaining()) {
                return -1;
            }
        }
        return this.bbuf.get() & 255;
    }

    public int read(byte[] b) throws IOException {
        return read(b, 0, b.length);
    }

    public long skip(long n) throws IOException {
        int skipped = 0;
        while (n > 0 && this.cbuf.hasRemaining()) {
            this.cbuf.get();
            n--;
            skipped++;
        }
        return (long) skipped;
    }

    public int available() throws IOException {
        return this.cbuf.remaining();
    }

    public void close() throws IOException {
    }

    public void mark(int readlimit) {
        this.mark = this.cbuf.position();
    }

    public void reset() throws IOException {
        if (this.mark != -1) {
            this.cbuf.position(this.mark);
            this.mark = -1;
        }
    }

    public boolean markSupported() {
        return true;
    }
}
