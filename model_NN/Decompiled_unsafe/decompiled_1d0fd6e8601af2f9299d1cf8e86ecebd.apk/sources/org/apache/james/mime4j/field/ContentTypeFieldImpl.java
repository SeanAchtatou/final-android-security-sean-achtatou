package org.apache.james.mime4j.field;

import java.io.StringReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser;
import org.apache.james.mime4j.field.contenttype.parser.ParseException;
import org.apache.james.mime4j.field.contenttype.parser.TokenMgrError;
import org.apache.james.mime4j.stream.Field;

public class ContentTypeFieldImpl extends AbstractField implements ContentTypeField {
    public static final FieldParser<ContentTypeField> PARSER = new FieldParser<ContentTypeField>() {
        public ContentTypeField parse(Field rawField, DecodeMonitor monitor) {
            return new ContentTypeFieldImpl(rawField, monitor);
        }
    };
    private String mediaType = null;
    private String mimeType = null;
    private Map<String, String> parameters = new HashMap();
    private ParseException parseException;
    private boolean parsed = false;
    private String subType = null;

    ContentTypeFieldImpl(Field rawField, DecodeMonitor monitor) {
        super(rawField, monitor);
    }

    public ParseException getParseException() {
        if (!this.parsed) {
            parse();
        }
        return this.parseException;
    }

    public String getMimeType() {
        if (!this.parsed) {
            parse();
        }
        return this.mimeType;
    }

    public String getMediaType() {
        if (!this.parsed) {
            parse();
        }
        return this.mediaType;
    }

    public String getSubType() {
        if (!this.parsed) {
            parse();
        }
        return this.subType;
    }

    public String getParameter(String name) {
        if (!this.parsed) {
            parse();
        }
        return this.parameters.get(name.toLowerCase());
    }

    public Map<String, String> getParameters() {
        if (!this.parsed) {
            parse();
        }
        return Collections.unmodifiableMap(this.parameters);
    }

    public boolean isMimeType(String mimeType2) {
        if (!this.parsed) {
            parse();
        }
        return this.mimeType != null && this.mimeType.equalsIgnoreCase(mimeType2);
    }

    public boolean isMultipart() {
        if (!this.parsed) {
            parse();
        }
        return this.mimeType != null && this.mimeType.startsWith(ContentTypeField.TYPE_MULTIPART_PREFIX);
    }

    public String getBoundary() {
        return getParameter(ContentTypeField.PARAM_BOUNDARY);
    }

    public String getCharset() {
        return getParameter("charset");
    }

    public static String getMimeType(ContentTypeField child, ContentTypeField parent) {
        if (child != null && child.getMimeType() != null && (!child.isMultipart() || child.getBoundary() != null)) {
            return child.getMimeType();
        }
        if (parent == null || !parent.isMimeType(ContentTypeField.TYPE_MULTIPART_DIGEST)) {
            return ContentTypeField.TYPE_TEXT_PLAIN;
        }
        return ContentTypeField.TYPE_MESSAGE_RFC822;
    }

    public static String getCharset(ContentTypeField f) {
        String charset;
        return (f == null || (charset = f.getCharset()) == null || charset.length() <= 0) ? "us-ascii" : charset;
    }

    private void parse() {
        ContentTypeParser parser = new ContentTypeParser(new StringReader(getBody()));
        try {
            parser.parseAll();
        } catch (ParseException e) {
            this.parseException = e;
        } catch (TokenMgrError e2) {
            this.parseException = new ParseException(e2.getMessage());
        }
        this.mediaType = parser.getType();
        this.subType = parser.getSubType();
        if (!(this.mediaType == null || this.subType == null)) {
            this.mimeType = (this.mediaType + "/" + this.subType).toLowerCase();
            List<String> paramNames = parser.getParamNames();
            List<String> paramValues = parser.getParamValues();
            if (!(paramNames == null || paramValues == null)) {
                int len = Math.min(paramNames.size(), paramValues.size());
                for (int i = 0; i < len; i++) {
                    this.parameters.put(paramNames.get(i).toLowerCase(), paramValues.get(i));
                }
            }
        }
        this.parsed = true;
    }
}
