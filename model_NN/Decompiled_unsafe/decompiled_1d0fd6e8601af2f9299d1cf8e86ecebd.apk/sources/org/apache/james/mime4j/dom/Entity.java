package org.apache.james.mime4j.dom;

public interface Entity extends Disposable {
    Body getBody();

    String getCharset();

    String getContentTransferEncoding();

    String getDispositionType();

    String getFilename();

    Header getHeader();

    String getMimeType();

    Entity getParent();

    boolean isMultipart();

    Body removeBody();

    void setBody(Body body);

    void setHeader(Header header);

    void setParent(Entity entity);
}
