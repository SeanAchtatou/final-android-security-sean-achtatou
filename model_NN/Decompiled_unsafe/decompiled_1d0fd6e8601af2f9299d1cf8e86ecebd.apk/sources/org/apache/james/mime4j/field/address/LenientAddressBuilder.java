package org.apache.james.mime4j.field.address;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.codec.DecoderUtil;
import org.apache.james.mime4j.dom.address.Address;
import org.apache.james.mime4j.dom.address.AddressList;
import org.apache.james.mime4j.dom.address.DomainList;
import org.apache.james.mime4j.dom.address.Group;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.stream.ParserCursor;
import org.apache.james.mime4j.stream.RawFieldParser;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.CharsetUtil;
import org.apache.james.mime4j.util.ContentUtil;

public class LenientAddressBuilder {
    private static final int AT = 64;
    private static final BitSet AT_AND_CLOSING_BRACKET = RawFieldParser.INIT_BITSET(64, 62);
    private static final int CLOSING_BRACKET = 62;
    private static final BitSet CLOSING_BRACKET_ONLY = RawFieldParser.INIT_BITSET(62);
    private static final int COLON = 58;
    private static final BitSet COLON_ONLY = RawFieldParser.INIT_BITSET(58);
    private static final int COMMA = 44;
    private static final BitSet COMMA_ONLY = RawFieldParser.INIT_BITSET(44);
    public static final LenientAddressBuilder DEFAULT = new LenientAddressBuilder(DecodeMonitor.SILENT);
    private static final int OPENING_BRACKET = 60;
    private static final int SEMICOLON = 59;
    private static final BitSet SEMICOLON_ONLY = RawFieldParser.INIT_BITSET(59);
    private final DecodeMonitor monitor;
    private final RawFieldParser parser = new RawFieldParser();

    protected LenientAddressBuilder(DecodeMonitor monitor2) {
        this.monitor = monitor2;
    }

    /* access modifiers changed from: package-private */
    public String parseDomain(ByteSequence buf, ParserCursor cursor, BitSet delimiters) {
        StringBuilder dst = new StringBuilder();
        while (!cursor.atEnd()) {
            char current = (char) (buf.byteAt(cursor.getPos()) & 255);
            if (delimiters != null && delimiters.get(current)) {
                break;
            } else if (CharsetUtil.isWhitespace(current)) {
                this.parser.skipWhiteSpace(buf, cursor);
            } else if (current == '(') {
                this.parser.skipComment(buf, cursor);
            } else {
                this.parser.copyContent(buf, cursor, delimiters, dst);
            }
        }
        return dst.toString();
    }

    /* access modifiers changed from: package-private */
    public DomainList parseRoute(ByteSequence buf, ParserCursor cursor, BitSet delimiters) {
        BitSet bitset = RawFieldParser.INIT_BITSET(44, 58);
        if (delimiters != null) {
            bitset.or(delimiters);
        }
        List<String> domains = null;
        while (true) {
            this.parser.skipAllWhiteSpace(buf, cursor);
            if (!cursor.atEnd()) {
                int pos = cursor.getPos();
                if (((char) (buf.byteAt(pos) & 255)) != 64) {
                    break;
                }
                cursor.updatePos(pos + 1);
                String s = parseDomain(buf, cursor, bitset);
                if (s != null && s.length() > 0) {
                    if (domains == null) {
                        domains = new ArrayList<>();
                    }
                    domains.add(s);
                }
                if (cursor.atEnd()) {
                    break;
                }
                int pos2 = cursor.getPos();
                int current = (char) (buf.byteAt(pos2) & 255);
                if (current == 44) {
                    cursor.updatePos(pos2 + 1);
                } else if (current == 58) {
                    cursor.updatePos(pos2 + 1);
                }
            } else {
                break;
            }
        }
        if (domains != null) {
            return new DomainList(domains, true);
        }
        return null;
    }

    private Mailbox createMailbox(String name, DomainList route, String localPart, String domain) {
        return new Mailbox(name != null ? DecoderUtil.decodeEncodedWords(name, this.monitor) : null, route, localPart, domain);
    }

    /* access modifiers changed from: package-private */
    public Mailbox parseMailboxAddress(String openingText, ByteSequence buf, ParserCursor cursor) {
        if (cursor.atEnd()) {
            return createMailbox(null, null, openingText, null);
        }
        int pos = cursor.getPos();
        if (((char) (buf.byteAt(pos) & 255)) != '<') {
            return createMailbox(null, null, openingText, null);
        }
        cursor.updatePos(pos + 1);
        DomainList domainList = parseRoute(buf, cursor, CLOSING_BRACKET_ONLY);
        String localPart = this.parser.parseValue(buf, cursor, AT_AND_CLOSING_BRACKET);
        if (cursor.atEnd()) {
            return createMailbox(openingText, domainList, localPart, null);
        }
        int pos2 = cursor.getPos();
        if (((char) (buf.byteAt(pos2) & 255)) != '@') {
            return createMailbox(openingText, domainList, localPart, null);
        }
        cursor.updatePos(pos2 + 1);
        String domain = parseDomain(buf, cursor, CLOSING_BRACKET_ONLY);
        if (cursor.atEnd()) {
            return createMailbox(openingText, domainList, localPart, domain);
        }
        int pos3 = cursor.getPos();
        if (((char) (buf.byteAt(pos3) & 255)) != '>') {
            return createMailbox(openingText, domainList, localPart, domain);
        }
        cursor.updatePos(pos3 + 1);
        while (!cursor.atEnd()) {
            char current = (char) (buf.byteAt(cursor.getPos()) & 255);
            if (!CharsetUtil.isWhitespace(current)) {
                if (current != '(') {
                    break;
                }
                this.parser.skipComment(buf, cursor);
            } else {
                this.parser.skipWhiteSpace(buf, cursor);
            }
        }
        return createMailbox(openingText, domainList, localPart, domain);
    }

    private Mailbox createMailbox(String localPart) {
        if (localPart == null || localPart.length() <= 0) {
            return null;
        }
        return new Mailbox(null, null, localPart, null);
    }

    public Mailbox parseMailbox(ByteSequence buf, ParserCursor cursor, BitSet delimiters) {
        BitSet bitset = RawFieldParser.INIT_BITSET(64, 60);
        if (delimiters != null) {
            bitset.or(delimiters);
        }
        String openingText = this.parser.parseValue(buf, cursor, bitset);
        if (cursor.atEnd()) {
            return createMailbox(openingText);
        }
        int pos = cursor.getPos();
        char current = (char) (buf.byteAt(pos) & 255);
        if (current == '<') {
            return parseMailboxAddress(openingText, buf, cursor);
        }
        if (current != '@') {
            return createMailbox(openingText);
        }
        cursor.updatePos(pos + 1);
        return new Mailbox(null, null, openingText, parseDomain(buf, cursor, delimiters));
    }

    public Mailbox parseMailbox(String text) {
        return parseMailbox(ContentUtil.encode(text), new ParserCursor(0, text.length()), null);
    }

    /* access modifiers changed from: package-private */
    public List<Mailbox> parseMailboxes(ByteSequence buf, ParserCursor cursor, BitSet delimiters) {
        BitSet bitset = RawFieldParser.INIT_BITSET(44);
        if (delimiters != null) {
            bitset.or(delimiters);
        }
        List<Mailbox> mboxes = new ArrayList<>();
        while (!cursor.atEnd()) {
            int pos = cursor.getPos();
            int current = (char) (buf.byteAt(pos) & 255);
            if (delimiters != null && delimiters.get(current)) {
                break;
            } else if (current == 44) {
                cursor.updatePos(pos + 1);
            } else {
                Mailbox mbox = parseMailbox(buf, cursor, bitset);
                if (mbox != null) {
                    mboxes.add(mbox);
                }
            }
        }
        return mboxes;
    }

    public Group parseGroup(ByteSequence buf, ParserCursor cursor) {
        String name = this.parser.parseToken(buf, cursor, COLON_ONLY);
        if (cursor.atEnd()) {
            return new Group(name, Collections.emptyList());
        }
        int pos = cursor.getPos();
        if (((char) (buf.byteAt(pos) & 255)) == 58) {
            cursor.updatePos(pos + 1);
        }
        return new Group(name, parseMailboxes(buf, cursor, SEMICOLON_ONLY));
    }

    public Group parseGroup(String text) {
        return parseGroup(ContentUtil.encode(text), new ParserCursor(0, text.length()));
    }

    public Address parseAddress(ByteSequence buf, ParserCursor cursor, BitSet delimiters) {
        BitSet bitset = RawFieldParser.INIT_BITSET(58, 64, 60);
        if (delimiters != null) {
            bitset.or(delimiters);
        }
        String openingText = this.parser.parseValue(buf, cursor, bitset);
        if (cursor.atEnd()) {
            return createMailbox(openingText);
        }
        int pos = cursor.getPos();
        char current = (char) (buf.byteAt(pos) & 255);
        if (current == '<') {
            return parseMailboxAddress(openingText, buf, cursor);
        }
        if (current == '@') {
            cursor.updatePos(pos + 1);
            return new Mailbox(null, null, openingText, parseDomain(buf, cursor, delimiters));
        } else if (current != ':') {
            return createMailbox(openingText);
        } else {
            cursor.updatePos(pos + 1);
            String name = openingText;
            List<Mailbox> mboxes = parseMailboxes(buf, cursor, SEMICOLON_ONLY);
            if (!cursor.atEnd()) {
                int pos2 = cursor.getPos();
                if (((char) (buf.byteAt(pos2) & 255)) == ';') {
                    cursor.updatePos(pos2 + 1);
                }
            }
            return new Group(name, mboxes);
        }
    }

    public Address parseAddress(String text) {
        return parseAddress(ContentUtil.encode(text), new ParserCursor(0, text.length()), null);
    }

    public AddressList parseAddressList(ByteSequence buf, ParserCursor cursor) {
        List<Address> addresses = new ArrayList<>();
        while (!cursor.atEnd()) {
            int pos = cursor.getPos();
            if (((char) (buf.byteAt(pos) & 255)) == 44) {
                cursor.updatePos(pos + 1);
            } else {
                Address address = parseAddress(buf, cursor, COMMA_ONLY);
                if (address != null) {
                    addresses.add(address);
                }
            }
        }
        return new AddressList(addresses, false);
    }

    public AddressList parseAddressList(String text) {
        return parseAddressList(ContentUtil.encode(text), new ParserCursor(0, text.length()));
    }
}
