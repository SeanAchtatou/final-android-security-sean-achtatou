package org.apache.james.mime4j.dom;

class ServiceLoader {
    private ServiceLoader() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r5.close();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static <T> T load(java.lang.Class<T> r14) {
        /*
            r12 = 0
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r13 = "META-INF/services/"
            java.lang.StringBuilder r11 = r11.append(r13)
            java.lang.String r13 = r14.getName()
            java.lang.StringBuilder r11 = r11.append(r13)
            java.lang.String r10 = r11.toString()
            java.lang.ClassLoader r0 = r14.getClassLoader()
            java.util.Enumeration r9 = r0.getResources(r10)     // Catch:{ IOException -> 0x0079, ClassNotFoundException -> 0x0085, IllegalAccessException -> 0x00ab, InstantiationException -> 0x00ae }
        L_0x0020:
            boolean r11 = r9.hasMoreElements()     // Catch:{ IOException -> 0x0079, ClassNotFoundException -> 0x0085, IllegalAccessException -> 0x00ab, InstantiationException -> 0x00ae }
            if (r11 == 0) goto L_0x00a9
            java.lang.Object r8 = r9.nextElement()     // Catch:{ IOException -> 0x0079, ClassNotFoundException -> 0x0085, IllegalAccessException -> 0x00ab, InstantiationException -> 0x00ae }
            java.net.URL r8 = (java.net.URL) r8     // Catch:{ IOException -> 0x0079, ClassNotFoundException -> 0x0085, IllegalAccessException -> 0x00ab, InstantiationException -> 0x00ae }
            java.io.InputStream r5 = r8.openStream()     // Catch:{ IOException -> 0x0079, ClassNotFoundException -> 0x0085, IllegalAccessException -> 0x00ab, InstantiationException -> 0x00ae }
            java.io.BufferedReader r7 = new java.io.BufferedReader     // Catch:{ all -> 0x0080 }
            java.io.InputStreamReader r11 = new java.io.InputStreamReader     // Catch:{ all -> 0x0080 }
            r11.<init>(r5)     // Catch:{ all -> 0x0080 }
            r7.<init>(r11)     // Catch:{ all -> 0x0080 }
        L_0x003a:
            java.lang.String r6 = r7.readLine()     // Catch:{ all -> 0x0080 }
            if (r6 == 0) goto L_0x0072
            java.lang.String r6 = r6.trim()     // Catch:{ all -> 0x0080 }
            r11 = 35
            int r1 = r6.indexOf(r11)     // Catch:{ all -> 0x0080 }
            r11 = -1
            if (r1 == r11) goto L_0x0056
            r11 = 0
            java.lang.String r6 = r6.substring(r11, r1)     // Catch:{ all -> 0x0080 }
            java.lang.String r6 = r6.trim()     // Catch:{ all -> 0x0080 }
        L_0x0056:
            int r11 = r6.length()     // Catch:{ all -> 0x0080 }
            if (r11 == 0) goto L_0x003a
            java.lang.Class r4 = r0.loadClass(r6)     // Catch:{ all -> 0x0080 }
            boolean r11 = r14.isAssignableFrom(r4)     // Catch:{ all -> 0x0080 }
            if (r11 == 0) goto L_0x003a
            java.lang.Object r3 = r4.newInstance()     // Catch:{ all -> 0x0080 }
            java.lang.Object r11 = r14.cast(r3)     // Catch:{ all -> 0x0080 }
            r5.close()     // Catch:{ IOException -> 0x0079, ClassNotFoundException -> 0x0085, IllegalAccessException -> 0x00ab, InstantiationException -> 0x00ae }
        L_0x0071:
            return r11
        L_0x0072:
            r7.close()     // Catch:{ all -> 0x0080 }
            r5.close()     // Catch:{ IOException -> 0x0079, ClassNotFoundException -> 0x0085, IllegalAccessException -> 0x00ab, InstantiationException -> 0x00ae }
            goto L_0x0020
        L_0x0079:
            r2 = move-exception
            org.apache.james.mime4j.dom.ServiceLoaderException r11 = new org.apache.james.mime4j.dom.ServiceLoaderException
            r11.<init>(r2)
            throw r11
        L_0x0080:
            r11 = move-exception
            r5.close()     // Catch:{ IOException -> 0x0079, ClassNotFoundException -> 0x0085, IllegalAccessException -> 0x00ab, InstantiationException -> 0x00ae }
            throw r11     // Catch:{ IOException -> 0x0079, ClassNotFoundException -> 0x0085, IllegalAccessException -> 0x00ab, InstantiationException -> 0x00ae }
        L_0x0085:
            r2 = move-exception
            org.apache.james.mime4j.dom.ServiceLoaderException r11 = new org.apache.james.mime4j.dom.ServiceLoaderException
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "Unknown SPI class '"
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r13 = r14.getName()
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r13 = "'"
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r12 = r12.toString()
            r11.<init>(r12, r2)
            throw r11
        L_0x00a9:
            r11 = r12
            goto L_0x0071
        L_0x00ab:
            r2 = move-exception
            r11 = r12
            goto L_0x0071
        L_0x00ae:
            r2 = move-exception
            org.apache.james.mime4j.dom.ServiceLoaderException r11 = new org.apache.james.mime4j.dom.ServiceLoaderException
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "SPI class '"
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r13 = r14.getName()
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r13 = "' cannot be instantiated"
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r12 = r12.toString()
            r11.<init>(r12, r2)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.dom.ServiceLoader.load(java.lang.Class):java.lang.Object");
    }
}
