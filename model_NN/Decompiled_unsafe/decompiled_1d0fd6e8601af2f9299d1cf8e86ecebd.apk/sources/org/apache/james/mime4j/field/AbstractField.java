package org.apache.james.mime4j.field;

import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.field.ParseException;
import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.stream.RawField;
import org.apache.james.mime4j.util.ByteSequence;

public abstract class AbstractField implements ParsedField {
    protected final DecodeMonitor monitor;
    protected final Field rawField;

    protected AbstractField(Field rawField2, DecodeMonitor monitor2) {
        this.rawField = rawField2;
        this.monitor = monitor2 == null ? DecodeMonitor.SILENT : monitor2;
    }

    public String getName() {
        return this.rawField.getName();
    }

    public String getBody() {
        return this.rawField.getBody();
    }

    public ByteSequence getRaw() {
        return this.rawField.getRaw();
    }

    public boolean isValidField() {
        return getParseException() == null;
    }

    public ParseException getParseException() {
        return null;
    }

    /* access modifiers changed from: protected */
    public RawField getRawField() {
        if (this.rawField instanceof RawField) {
            return (RawField) this.rawField;
        }
        return new RawField(this.rawField.getName(), this.rawField.getBody());
    }

    public String toString() {
        return this.rawField.toString();
    }
}
