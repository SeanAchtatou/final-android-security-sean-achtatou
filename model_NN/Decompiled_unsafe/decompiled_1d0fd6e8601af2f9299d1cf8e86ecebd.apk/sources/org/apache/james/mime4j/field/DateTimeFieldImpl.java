package org.apache.james.mime4j.field;

import java.io.StringReader;
import java.util.Date;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.DateTimeField;
import org.apache.james.mime4j.field.datetime.parser.DateTimeParser;
import org.apache.james.mime4j.field.datetime.parser.ParseException;
import org.apache.james.mime4j.field.datetime.parser.TokenMgrError;
import org.apache.james.mime4j.stream.Field;

public class DateTimeFieldImpl extends AbstractField implements DateTimeField {
    public static final FieldParser<DateTimeField> PARSER = new FieldParser<DateTimeField>() {
        public DateTimeField parse(Field rawField, DecodeMonitor monitor) {
            return new DateTimeFieldImpl(rawField, monitor);
        }
    };
    private Date date;
    private ParseException parseException;
    private boolean parsed = false;

    DateTimeFieldImpl(Field rawField, DecodeMonitor monitor) {
        super(rawField, monitor);
    }

    public Date getDate() {
        if (!this.parsed) {
            parse();
        }
        return this.date;
    }

    public ParseException getParseException() {
        if (!this.parsed) {
            parse();
        }
        return this.parseException;
    }

    private void parse() {
        try {
            this.date = new DateTimeParser(new StringReader(getBody())).parseAll().getDate();
        } catch (ParseException e) {
            this.parseException = e;
        } catch (TokenMgrError e2) {
            this.parseException = new ParseException(e2.getMessage());
        }
        this.parsed = true;
    }
}
