package org.apache.james.mime4j.field;

import java.io.StringReader;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentLocationField;
import org.apache.james.mime4j.field.structured.parser.ParseException;
import org.apache.james.mime4j.field.structured.parser.StructuredFieldParser;
import org.apache.james.mime4j.stream.Field;

public class ContentLocationFieldImpl extends AbstractField implements ContentLocationField {
    public static final FieldParser<ContentLocationField> PARSER = new FieldParser<ContentLocationField>() {
        public ContentLocationField parse(Field rawField, DecodeMonitor monitor) {
            return new ContentLocationFieldImpl(rawField, monitor);
        }
    };
    private String location;
    private ParseException parseException;
    private boolean parsed = false;

    ContentLocationFieldImpl(Field rawField, DecodeMonitor monitor) {
        super(rawField, monitor);
    }

    private void parse() {
        this.parsed = true;
        String body = getBody();
        this.location = null;
        if (body != null) {
            try {
                this.location = new StructuredFieldParser(new StringReader(body)).parse().replaceAll("\\s", "");
            } catch (ParseException ex) {
                this.parseException = ex;
            }
        }
    }

    public String getLocation() {
        if (!this.parsed) {
            parse();
        }
        return this.location;
    }

    public org.apache.james.mime4j.dom.field.ParseException getParseException() {
        return this.parseException;
    }
}
