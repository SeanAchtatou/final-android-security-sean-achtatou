package org.apache.james.mime4j.field;

import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.stream.NameValuePair;
import org.apache.james.mime4j.stream.RawBody;
import org.apache.james.mime4j.stream.RawFieldParser;

public class ContentTypeFieldLenientImpl extends AbstractField implements ContentTypeField {
    public static final FieldParser<ContentTypeField> PARSER = new FieldParser<ContentTypeField>() {
        public ContentTypeField parse(Field rawField, DecodeMonitor monitor) {
            return new ContentTypeFieldLenientImpl(rawField, monitor);
        }
    };
    private String mediaType = null;
    private String mimeType = null;
    private Map<String, String> parameters = new HashMap();
    private boolean parsed = false;
    private String subType = null;

    ContentTypeFieldLenientImpl(Field rawField, DecodeMonitor monitor) {
        super(rawField, monitor);
    }

    public String getMimeType() {
        if (!this.parsed) {
            parse();
        }
        return this.mimeType;
    }

    public String getMediaType() {
        if (!this.parsed) {
            parse();
        }
        return this.mediaType;
    }

    public String getSubType() {
        if (!this.parsed) {
            parse();
        }
        return this.subType;
    }

    public String getParameter(String name) {
        if (!this.parsed) {
            parse();
        }
        return this.parameters.get(name.toLowerCase());
    }

    public Map<String, String> getParameters() {
        if (!this.parsed) {
            parse();
        }
        return Collections.unmodifiableMap(this.parameters);
    }

    public boolean isMimeType(String mimeType2) {
        if (!this.parsed) {
            parse();
        }
        return this.mimeType != null && this.mimeType.equalsIgnoreCase(mimeType2);
    }

    public boolean isMultipart() {
        if (!this.parsed) {
            parse();
        }
        return this.mimeType != null && this.mimeType.startsWith(ContentTypeField.TYPE_MULTIPART_PREFIX);
    }

    public String getBoundary() {
        return getParameter(ContentTypeField.PARAM_BOUNDARY);
    }

    public String getCharset() {
        return getParameter("charset");
    }

    private void parse() {
        this.parsed = true;
        RawBody body = RawFieldParser.DEFAULT.parseRawBody(getRawField());
        String main = body.getValue();
        String type = null;
        String subtype = null;
        if (main != null) {
            main = main.toLowerCase().trim();
            int index = main.indexOf(47);
            boolean valid = false;
            if (index != -1) {
                type = main.substring(0, index).trim();
                subtype = main.substring(index + 1).trim();
                if (type.length() > 0 && subtype.length() > 0) {
                    main = type + "/" + subtype;
                    valid = true;
                }
            }
            if (!valid) {
                if (this.monitor.isListening()) {
                    this.monitor.warn("Invalid Content-Type: " + body, "Content-Type value ignored");
                }
                main = null;
                type = null;
                subtype = null;
            }
        }
        this.mimeType = main;
        this.mediaType = type;
        this.subType = subtype;
        this.parameters.clear();
        for (NameValuePair nmp : body.getParams()) {
            this.parameters.put(nmp.getName().toLowerCase(Locale.US), nmp.getValue());
        }
    }
}
