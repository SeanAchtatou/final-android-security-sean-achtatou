package org.apache.james.mime4j.field;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.DateTimeField;
import org.apache.james.mime4j.stream.Field;

public class DateTimeFieldLenientImpl extends AbstractField implements DateTimeField {
    private static final String[] DEFAULT_DATE_FORMATS = {"EEE, dd MMM yyyy HH:mm:ss ZZZZ", "dd MMM yyyy HH:mm:ss ZZZZ"};
    public static final FieldParser<DateTimeField> PARSER = new FieldParser<DateTimeField>() {
        public DateTimeField parse(Field rawField, DecodeMonitor monitor) {
            return new DateTimeFieldLenientImpl(rawField, null, monitor);
        }
    };
    private Date date;
    private final List<String> datePatterns = new ArrayList();
    private boolean parsed = false;

    DateTimeFieldLenientImpl(Field rawField, Collection<String> dateParsers, DecodeMonitor monitor) {
        super(rawField, monitor);
        if (dateParsers != null) {
            this.datePatterns.addAll(dateParsers);
            return;
        }
        for (String pattern : DEFAULT_DATE_FORMATS) {
            this.datePatterns.add(pattern);
        }
    }

    public Date getDate() {
        if (!this.parsed) {
            parse();
        }
        return this.date;
    }

    private void parse() {
        this.parsed = true;
        this.date = null;
        String body = getBody();
        for (String datePattern : this.datePatterns) {
            try {
                SimpleDateFormat parser = new SimpleDateFormat(datePattern, Locale.US);
                parser.setTimeZone(TimeZone.getTimeZone("GMT"));
                parser.setLenient(true);
                this.date = parser.parse(body);
                return;
            } catch (ParseException e) {
            }
        }
    }

    public static FieldParser<DateTimeField> createParser(final Collection<String> dateParsers) {
        return new FieldParser<DateTimeField>() {
            public DateTimeField parse(Field rawField, DecodeMonitor monitor) {
                return new DateTimeFieldLenientImpl(rawField, dateParsers, monitor);
            }
        };
    }
}
