package org.apache.james.mime4j.stream;

import org.apache.james.mime4j.util.LangUtils;

public final class NameValuePair {
    private final String name;
    private final String value;

    public NameValuePair(String name2, String value2) {
        if (name2 == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        this.name = name2;
        this.value = value2;
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }

    public String toString() {
        if (this.value == null) {
            return this.name;
        }
        return this.name + "=" + "\"" + this.value + "\"";
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof NameValuePair)) {
            return false;
        }
        NameValuePair that = (NameValuePair) object;
        if (!this.name.equals(that.name) || !LangUtils.equals(this.value, that.value)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return LangUtils.hashCode(LangUtils.hashCode(17, this.name), this.value);
    }
}
