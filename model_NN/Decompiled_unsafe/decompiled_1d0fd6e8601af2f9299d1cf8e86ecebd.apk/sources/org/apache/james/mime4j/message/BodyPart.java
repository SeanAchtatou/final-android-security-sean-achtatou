package org.apache.james.mime4j.message;

import java.util.Date;
import java.util.Map;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.dom.field.ContentTransferEncodingField;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.field.ContentTransferEncodingFieldImpl;
import org.apache.james.mime4j.field.ContentTypeFieldImpl;
import org.apache.james.mime4j.field.Fields;
import org.apache.james.mime4j.util.MimeUtil;

public class BodyPart extends AbstractEntity {
    /* access modifiers changed from: protected */
    public String newUniqueBoundary() {
        return MimeUtil.createUniqueBoundary();
    }

    /* access modifiers changed from: protected */
    public ContentDispositionField newContentDisposition(String dispositionType, String filename, long size, Date creationDate, Date modificationDate, Date readDate) {
        return Fields.contentDisposition(dispositionType, filename, size, creationDate, modificationDate, readDate);
    }

    /* access modifiers changed from: protected */
    public ContentDispositionField newContentDisposition(String dispositionType, Map<String, String> parameters) {
        return Fields.contentDisposition(dispositionType, parameters);
    }

    /* access modifiers changed from: protected */
    public ContentTypeField newContentType(String mimeType, Map<String, String> parameters) {
        return Fields.contentType(mimeType, parameters);
    }

    /* access modifiers changed from: protected */
    public ContentTransferEncodingField newContentTransferEncoding(String contentTransferEncoding) {
        return Fields.contentTransferEncoding(contentTransferEncoding);
    }

    /* access modifiers changed from: protected */
    public String calcTransferEncoding(ContentTransferEncodingField f) {
        return ContentTransferEncodingFieldImpl.getEncoding(f);
    }

    /* access modifiers changed from: protected */
    public String calcMimeType(ContentTypeField child, ContentTypeField parent) {
        return ContentTypeFieldImpl.getMimeType(child, parent);
    }

    /* access modifiers changed from: protected */
    public String calcCharset(ContentTypeField contentType) {
        return ContentTypeFieldImpl.getCharset(contentType);
    }
}
