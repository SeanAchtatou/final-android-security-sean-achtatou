package org.apache.james.mime4j.field;

import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.codec.DecoderUtil;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.UnstructuredField;
import org.apache.james.mime4j.stream.Field;

public class UnstructuredFieldImpl extends AbstractField implements UnstructuredField {
    public static final FieldParser<UnstructuredField> PARSER = new FieldParser<UnstructuredField>() {
        public UnstructuredField parse(Field rawField, DecodeMonitor monitor) {
            return new UnstructuredFieldImpl(rawField, monitor);
        }
    };
    private boolean parsed = false;
    private String value;

    UnstructuredFieldImpl(Field rawField, DecodeMonitor monitor) {
        super(rawField, monitor);
    }

    public String getValue() {
        if (!this.parsed) {
            parse();
        }
        return this.value;
    }

    private void parse() {
        this.value = DecoderUtil.decodeEncodedWords(getBody(), this.monitor);
        this.parsed = true;
    }
}
