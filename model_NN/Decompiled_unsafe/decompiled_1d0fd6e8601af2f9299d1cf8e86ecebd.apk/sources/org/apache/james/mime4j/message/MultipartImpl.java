package org.apache.james.mime4j.message;

import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.ContentUtil;

public class MultipartImpl extends AbstractMultipart {
    private ByteSequence epilogue;
    private transient boolean epilogueComputed;
    private transient String epilogueStrCache;
    private ByteSequence preamble;
    private transient boolean preambleComputed;
    private transient String preambleStrCache;

    public MultipartImpl(String subType) {
        super(subType);
        this.preambleComputed = false;
        this.epilogueComputed = false;
        this.preamble = null;
        this.preambleStrCache = null;
        this.preambleComputed = true;
        this.epilogue = null;
        this.epilogueStrCache = null;
        this.epilogueComputed = true;
    }

    public ByteSequence getPreambleRaw() {
        return this.preamble;
    }

    public void setPreambleRaw(ByteSequence preamble2) {
        this.preamble = preamble2;
        this.preambleStrCache = null;
        this.preambleComputed = false;
    }

    public String getPreamble() {
        if (!this.preambleComputed) {
            this.preambleStrCache = this.preamble != null ? ContentUtil.decode(this.preamble) : null;
            this.preambleComputed = true;
        }
        return this.preambleStrCache;
    }

    public void setPreamble(String preamble2) {
        this.preamble = preamble2 != null ? ContentUtil.encode(preamble2) : null;
        this.preambleStrCache = preamble2;
        this.preambleComputed = true;
    }

    public ByteSequence getEpilogueRaw() {
        return this.epilogue;
    }

    public void setEpilogueRaw(ByteSequence epilogue2) {
        this.epilogue = epilogue2;
        this.epilogueStrCache = null;
        this.epilogueComputed = false;
    }

    public String getEpilogue() {
        if (!this.epilogueComputed) {
            this.epilogueStrCache = this.epilogue != null ? ContentUtil.decode(this.epilogue) : null;
            this.epilogueComputed = true;
        }
        return this.epilogueStrCache;
    }

    public void setEpilogue(String epilogue2) {
        this.epilogue = epilogue2 != null ? ContentUtil.encode(epilogue2) : null;
        this.epilogueStrCache = epilogue2;
        this.epilogueComputed = true;
    }
}
