package org.apache.james.mime4j.dom.address;

import java.io.Serializable;
import java.util.List;

public abstract class Address implements Serializable {
    private static final long serialVersionUID = 634090661990433426L;

    /* access modifiers changed from: protected */
    public abstract void doAddMailboxesTo(List<Mailbox> list);

    /* access modifiers changed from: package-private */
    public final void addMailboxesTo(List<Mailbox> results) {
        doAddMailboxesTo(results);
    }
}
