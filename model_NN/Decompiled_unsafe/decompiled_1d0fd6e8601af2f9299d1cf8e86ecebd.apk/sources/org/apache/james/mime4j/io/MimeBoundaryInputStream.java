package org.apache.james.mime4j.io;

import java.io.IOException;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.MimeIOException;
import org.apache.james.mime4j.util.ByteArrayBuffer;
import org.apache.james.mime4j.util.CharsetUtil;

public class MimeBoundaryInputStream extends LineReaderInputStream {
    private boolean atBoundary;
    private final byte[] boundary;
    private int boundaryLen;
    private BufferedLineReaderInputStream buffer;
    private boolean completed;
    private boolean eof;
    private int initialLength;
    private boolean lastPart;
    private int limit;
    private final boolean strict;

    public MimeBoundaryInputStream(BufferedLineReaderInputStream inbuffer, String boundary2, boolean strict2) throws IOException {
        super(inbuffer);
        int bufferSize = boundary2.length() * 2;
        inbuffer.ensureCapacity(bufferSize < 4096 ? 4096 : bufferSize);
        this.buffer = inbuffer;
        this.eof = false;
        this.limit = -1;
        this.atBoundary = false;
        this.boundaryLen = 0;
        this.lastPart = false;
        this.initialLength = -1;
        this.completed = false;
        this.strict = strict2;
        this.boundary = new byte[(boundary2.length() + 2)];
        this.boundary[0] = 45;
        this.boundary[1] = 45;
        for (int i = 0; i < boundary2.length(); i++) {
            this.boundary[i + 2] = (byte) boundary2.charAt(i);
        }
        fillBuffer();
    }

    public MimeBoundaryInputStream(BufferedLineReaderInputStream inbuffer, String boundary2) throws IOException {
        this(inbuffer, boundary2, false);
    }

    public void close() throws IOException {
    }

    public boolean markSupported() {
        return false;
    }

    public boolean readAllowed() throws IOException {
        if (this.completed) {
            return false;
        }
        if (!endOfStream() || hasData()) {
            return true;
        }
        skipBoundary();
        verifyEndOfStream();
        return false;
    }

    public int read() throws IOException {
        while (readAllowed()) {
            if (hasData()) {
                return this.buffer.read();
            }
            fillBuffer();
        }
        return -1;
    }

    public int read(byte[] b, int off, int len) throws IOException {
        while (readAllowed()) {
            if (hasData()) {
                return this.buffer.read(b, off, Math.min(len, this.limit - this.buffer.pos()));
            }
            fillBuffer();
        }
        return -1;
    }

    public int readLine(ByteArrayBuffer dst) throws IOException {
        int chunk;
        if (dst == null) {
            throw new IllegalArgumentException("Destination buffer may not be null");
        } else if (!readAllowed()) {
            return -1;
        } else {
            int total = 0;
            boolean found = false;
            int bytesRead = 0;
            while (true) {
                if (found) {
                    break;
                }
                if (!hasData()) {
                    bytesRead = fillBuffer();
                    if (endOfStream() && !hasData()) {
                        skipBoundary();
                        verifyEndOfStream();
                        bytesRead = -1;
                        break;
                    }
                }
                int len = this.limit - this.buffer.pos();
                int i = this.buffer.indexOf((byte) 10, this.buffer.pos(), len);
                if (i != -1) {
                    found = true;
                    chunk = (i + 1) - this.buffer.pos();
                } else {
                    chunk = len;
                }
                if (chunk > 0) {
                    dst.append(this.buffer.buf(), this.buffer.pos(), chunk);
                    this.buffer.skip(chunk);
                    total += chunk;
                }
            }
            if (total == 0 && bytesRead == -1) {
                return -1;
            }
            return total;
        }
    }

    private void verifyEndOfStream() throws IOException {
        if (this.strict && this.eof && !this.atBoundary) {
            throw new MimeIOException(new MimeException("Unexpected end of stream"));
        }
    }

    private boolean endOfStream() {
        return this.eof || this.atBoundary;
    }

    private boolean hasData() {
        return this.limit > this.buffer.pos() && this.limit <= this.buffer.limit();
    }

    private int fillBuffer() throws IOException {
        int bytesRead;
        int i;
        if (this.eof) {
            return -1;
        }
        if (!hasData()) {
            bytesRead = this.buffer.fillBuffer();
            if (bytesRead == -1) {
                this.eof = true;
            }
        } else {
            bytesRead = 0;
        }
        int off = this.buffer.pos();
        while (true) {
            i = this.buffer.indexOf(this.boundary, off, this.buffer.limit() - off);
            if (i == -1) {
                break;
            }
            if (i == this.buffer.pos() || this.buffer.byteAt(i - 1) == 10) {
                int pos = i + this.boundary.length;
                if (this.buffer.limit() - pos <= 0) {
                    break;
                }
                char ch = (char) this.buffer.byteAt(pos);
                if (CharsetUtil.isWhitespace(ch) || ch == '-') {
                    break;
                }
            }
            off = i + this.boundary.length;
        }
        if (i != -1) {
            this.limit = i;
            this.atBoundary = true;
            calculateBoundaryLen();
            return bytesRead;
        } else if (this.eof) {
            this.limit = this.buffer.limit();
            return bytesRead;
        } else {
            this.limit = this.buffer.limit() - (this.boundary.length + 2);
            return bytesRead;
        }
    }

    public boolean isEmptyStream() {
        return this.initialLength == 0;
    }

    public boolean isFullyConsumed() {
        return this.completed && !this.buffer.hasBufferedData();
    }

    private void calculateBoundaryLen() throws IOException {
        this.boundaryLen = this.boundary.length;
        int len = this.limit - this.buffer.pos();
        if (len >= 0 && this.initialLength == -1) {
            this.initialLength = len;
        }
        if (len > 0 && this.buffer.byteAt(this.limit - 1) == 10) {
            this.boundaryLen++;
            this.limit--;
        }
        if (len > 1 && this.buffer.byteAt(this.limit - 1) == 13) {
            this.boundaryLen++;
            this.limit--;
        }
    }

    private void skipBoundary() throws IOException {
        if (!this.completed) {
            this.completed = true;
            this.buffer.skip(this.boundaryLen);
            boolean checkForLastPart = true;
            while (true) {
                if (this.buffer.length() > 1) {
                    int ch1 = this.buffer.byteAt(this.buffer.pos());
                    int ch2 = this.buffer.byteAt(this.buffer.pos() + 1);
                    if (checkForLastPart && ch1 == 45 && ch2 == 45) {
                        this.lastPart = true;
                        this.buffer.skip(2);
                        checkForLastPart = false;
                    } else if (ch1 == 13 && ch2 == 10) {
                        this.buffer.skip(2);
                        return;
                    } else if (ch1 == 10) {
                        this.buffer.skip(1);
                        return;
                    } else {
                        this.buffer.skip(1);
                    }
                } else if (!this.eof) {
                    fillBuffer();
                } else {
                    return;
                }
            }
        }
    }

    public boolean isLastPart() {
        return this.lastPart;
    }

    public boolean eof() {
        return this.eof && !this.buffer.hasBufferedData();
    }

    public String toString() {
        StringBuilder buffer2 = new StringBuilder("MimeBoundaryInputStream, boundary ");
        for (byte b : this.boundary) {
            buffer2.append((char) b);
        }
        return buffer2.toString();
    }

    public boolean unread(ByteArrayBuffer buf) {
        return false;
    }
}
