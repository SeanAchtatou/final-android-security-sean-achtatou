package org.apache.james.mime4j.message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import org.apache.james.mime4j.dom.BinaryBody;
import org.apache.james.mime4j.dom.TextBody;
import org.apache.james.mime4j.util.CharsetUtil;

public class BasicBodyFactory implements BodyFactory {
    public BinaryBody binaryBody(InputStream is) throws IOException {
        return new BasicBinaryBody(bufferContent(is));
    }

    public TextBody textBody(InputStream is, String mimeCharset) throws IOException {
        return new BasicTextBody(bufferContent(is), mimeCharset);
    }

    private static byte[] bufferContent(InputStream is) throws IOException {
        if (is == null) {
            throw new IllegalArgumentException("Input stream may not be null");
        }
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        byte[] tmp = new byte[2048];
        while (true) {
            int l = is.read(tmp);
            if (l == -1) {
                return buf.toByteArray();
            }
            buf.write(tmp, 0, l);
        }
    }

    public TextBody textBody(String text, String mimeCharset) throws UnsupportedEncodingException {
        if (text == null) {
            throw new IllegalArgumentException("Text may not be null");
        }
        try {
            return new StringBody(text, Charset.forName(mimeCharset));
        } catch (UnsupportedCharsetException ex) {
            throw new UnsupportedEncodingException(ex.getMessage());
        }
    }

    public TextBody textBody(String text, Charset charset) {
        if (text != null) {
            return new StringBody(text, charset);
        }
        throw new IllegalArgumentException("Text may not be null");
    }

    public TextBody textBody(String text) {
        return textBody(text, CharsetUtil.DEFAULT_CHARSET);
    }

    public BinaryBody binaryBody(byte[] buf) {
        return new BasicBinaryBody(buf);
    }
}
