package org.apache.james.mime4j.stream;

import java.util.ArrayList;
import java.util.List;

public final class RawBody {
    private final List<NameValuePair> params;
    private final String value;

    RawBody(String value2, List<NameValuePair> params2) {
        if (value2 == null) {
            throw new IllegalArgumentException("Field value not be null");
        }
        this.value = value2;
        this.params = params2 == null ? new ArrayList<>() : params2;
    }

    public String getValue() {
        return this.value;
    }

    public List<NameValuePair> getParams() {
        return new ArrayList(this.params);
    }

    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append(this.value);
        buf.append("; ");
        for (NameValuePair param : this.params) {
            buf.append("; ");
            buf.append(param);
        }
        return buf.toString();
    }
}
