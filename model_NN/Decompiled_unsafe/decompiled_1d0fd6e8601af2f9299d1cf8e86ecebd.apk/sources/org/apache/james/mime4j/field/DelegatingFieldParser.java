package org.apache.james.mime4j.field;

import java.util.HashMap;
import java.util.Map;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.stream.Field;

public class DelegatingFieldParser implements FieldParser<ParsedField> {
    private final FieldParser<? extends ParsedField> defaultParser;
    private final Map<String, FieldParser<? extends ParsedField>> parsers = new HashMap();

    public DelegatingFieldParser(FieldParser<? extends ParsedField> defaultParser2) {
        this.defaultParser = defaultParser2;
    }

    public void setFieldParser(String name, FieldParser<? extends ParsedField> parser) {
        this.parsers.put(name.toLowerCase(), parser);
    }

    public FieldParser<? extends ParsedField> getParser(String name) {
        FieldParser<? extends ParsedField> field = this.parsers.get(name.toLowerCase());
        if (field == null) {
            return this.defaultParser;
        }
        return field;
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [org.apache.james.mime4j.dom.field.ParsedField] */
    public ParsedField parse(Field rawField, DecodeMonitor monitor) {
        return getParser(rawField.getName()).parse(rawField, monitor);
    }
}
