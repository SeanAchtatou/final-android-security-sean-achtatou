package org.apache.james.mime4j.field;

import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentLengthField;
import org.apache.james.mime4j.stream.Field;

public class ContentLengthFieldImpl extends AbstractField implements ContentLengthField {
    public static final FieldParser<ContentLengthField> PARSER = new FieldParser<ContentLengthField>() {
        public ContentLengthField parse(Field rawField, DecodeMonitor monitor) {
            return new ContentLengthFieldImpl(rawField, monitor);
        }
    };
    private long contentLength;
    private boolean parsed = false;

    ContentLengthFieldImpl(Field rawField, DecodeMonitor monitor) {
        super(rawField, monitor);
    }

    private void parse() {
        this.parsed = true;
        this.contentLength = -1;
        String body = getBody();
        if (body != null) {
            try {
                this.contentLength = Long.parseLong(body);
                if (this.contentLength < 0) {
                    this.contentLength = -1;
                    if (this.monitor.isListening()) {
                        this.monitor.warn("Negative content length: " + body, "ignoring Content-Length header");
                    }
                }
            } catch (NumberFormatException e) {
                if (this.monitor.isListening()) {
                    this.monitor.warn("Invalid content length: " + body, "ignoring Content-Length header");
                }
            }
        }
    }

    public long getContentLength() {
        if (!this.parsed) {
            parse();
        }
        return this.contentLength;
    }
}
