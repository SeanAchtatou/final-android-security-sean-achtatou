package org.apache.james.mime4j.message;

import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.Header;
import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.field.LenientFieldParser;
import org.apache.james.mime4j.parser.AbstractContentHandler;

public abstract class SimpleContentHandler extends AbstractContentHandler {
    private Header currHeader;
    private final FieldParser<? extends ParsedField> fieldParser;
    private final DecodeMonitor monitor;

    public abstract void headers(Header header);

    public SimpleContentHandler(FieldParser<? extends ParsedField> fieldParser2, DecodeMonitor monitor2) {
        this.fieldParser = fieldParser2 == null ? LenientFieldParser.getParser() : fieldParser2;
        this.monitor = monitor2 == null ? DecodeMonitor.SILENT : monitor2;
    }

    public SimpleContentHandler() {
        this(null, null);
    }

    public final void startHeader() {
        this.currHeader = new HeaderImpl();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v1, types: [org.apache.james.mime4j.stream.Field] */
    /* JADX WARN: Type inference failed for: r0v4 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void field(org.apache.james.mime4j.stream.Field r4) throws org.apache.james.mime4j.MimeException {
        /*
            r3 = this;
            boolean r1 = r4 instanceof org.apache.james.mime4j.dom.field.ParsedField
            if (r1 == 0) goto L_0x000d
            r0 = r4
            org.apache.james.mime4j.dom.field.ParsedField r0 = (org.apache.james.mime4j.dom.field.ParsedField) r0
        L_0x0007:
            org.apache.james.mime4j.dom.Header r1 = r3.currHeader
            r1.addField(r0)
            return
        L_0x000d:
            org.apache.james.mime4j.dom.FieldParser<? extends org.apache.james.mime4j.dom.field.ParsedField> r1 = r3.fieldParser
            org.apache.james.mime4j.codec.DecodeMonitor r2 = r3.monitor
            org.apache.james.mime4j.dom.field.ParsedField r0 = r1.parse(r4, r2)
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.message.SimpleContentHandler.field(org.apache.james.mime4j.stream.Field):void");
    }

    public final void endHeader() {
        Header tmp = this.currHeader;
        this.currHeader = null;
        headers(tmp);
    }
}
