package org.apache.james.mime4j.field;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.stream.NameValuePair;
import org.apache.james.mime4j.stream.RawBody;
import org.apache.james.mime4j.stream.RawFieldParser;

public class ContentDispositionFieldLenientImpl extends AbstractField implements ContentDispositionField {
    private static final String DEFAULT_DATE_FORMAT = "EEE, dd MMM yyyy hh:mm:ss ZZZZ";
    public static final FieldParser<ContentDispositionField> PARSER = new FieldParser<ContentDispositionField>() {
        public ContentDispositionField parse(Field rawField, DecodeMonitor monitor) {
            return new ContentDispositionFieldLenientImpl(rawField, null, monitor);
        }
    };
    private Date creationDate;
    private boolean creationDateParsed;
    private final List<String> datePatterns = new ArrayList();
    private String dispositionType = "";
    private Date modificationDate;
    private boolean modificationDateParsed;
    private Map<String, String> parameters = new HashMap();
    private boolean parsed = false;
    private Date readDate;
    private boolean readDateParsed;

    ContentDispositionFieldLenientImpl(Field rawField, Collection<String> dateParsers, DecodeMonitor monitor) {
        super(rawField, monitor);
        if (dateParsers != null) {
            this.datePatterns.addAll(dateParsers);
        } else {
            this.datePatterns.add(DEFAULT_DATE_FORMAT);
        }
    }

    public String getDispositionType() {
        if (!this.parsed) {
            parse();
        }
        return this.dispositionType;
    }

    public String getParameter(String name) {
        if (!this.parsed) {
            parse();
        }
        return this.parameters.get(name.toLowerCase());
    }

    public Map<String, String> getParameters() {
        if (!this.parsed) {
            parse();
        }
        return Collections.unmodifiableMap(this.parameters);
    }

    public boolean isDispositionType(String dispositionType2) {
        if (!this.parsed) {
            parse();
        }
        return this.dispositionType.equalsIgnoreCase(dispositionType2);
    }

    public boolean isInline() {
        if (!this.parsed) {
            parse();
        }
        return this.dispositionType.equals(ContentDispositionField.DISPOSITION_TYPE_INLINE);
    }

    public boolean isAttachment() {
        if (!this.parsed) {
            parse();
        }
        return this.dispositionType.equals(ContentDispositionField.DISPOSITION_TYPE_ATTACHMENT);
    }

    public String getFilename() {
        return getParameter(ContentDispositionField.PARAM_FILENAME);
    }

    public Date getCreationDate() {
        if (!this.creationDateParsed) {
            this.creationDate = parseDate(ContentDispositionField.PARAM_CREATION_DATE);
            this.creationDateParsed = true;
        }
        return this.creationDate;
    }

    public Date getModificationDate() {
        if (!this.modificationDateParsed) {
            this.modificationDate = parseDate(ContentDispositionField.PARAM_MODIFICATION_DATE);
            this.modificationDateParsed = true;
        }
        return this.modificationDate;
    }

    public Date getReadDate() {
        if (!this.readDateParsed) {
            this.readDate = parseDate(ContentDispositionField.PARAM_READ_DATE);
            this.readDateParsed = true;
        }
        return this.readDate;
    }

    public long getSize() {
        String value = getParameter(ContentDispositionField.PARAM_SIZE);
        if (value == null) {
            return -1;
        }
        try {
            long size = Long.parseLong(value);
            if (size < 0) {
                size = -1;
            }
            return size;
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    private void parse() {
        this.parsed = true;
        RawBody body = RawFieldParser.DEFAULT.parseRawBody(getRawField());
        String main = body.getValue();
        if (main != null) {
            this.dispositionType = main.toLowerCase(Locale.US);
        } else {
            this.dispositionType = null;
        }
        this.parameters.clear();
        for (NameValuePair nmp : body.getParams()) {
            this.parameters.put(nmp.getName().toLowerCase(Locale.US), nmp.getValue());
        }
    }

    private Date parseDate(String paramName) {
        Date date = null;
        String value = getParameter(paramName);
        if (value != null) {
            Iterator i$ = this.datePatterns.iterator();
            while (true) {
                if (i$.hasNext()) {
                    try {
                        SimpleDateFormat parser = new SimpleDateFormat(i$.next(), Locale.US);
                        parser.setTimeZone(TimeZone.getTimeZone("GMT"));
                        parser.setLenient(true);
                        date = parser.parse(value);
                        break;
                    } catch (ParseException e) {
                    }
                } else if (this.monitor.isListening()) {
                    this.monitor.warn(paramName + " parameter is invalid: " + value, paramName + " parameter is ignored");
                }
            }
        }
        return date;
    }
}
