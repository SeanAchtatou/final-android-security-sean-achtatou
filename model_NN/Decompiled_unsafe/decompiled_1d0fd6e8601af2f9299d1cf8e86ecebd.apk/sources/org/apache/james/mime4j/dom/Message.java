package org.apache.james.mime4j.dom;

import java.util.Collection;
import java.util.Date;
import java.util.TimeZone;
import org.apache.james.mime4j.dom.address.Address;
import org.apache.james.mime4j.dom.address.AddressList;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.address.MailboxList;

public interface Message extends Entity, Body {
    void createMessageId(String str);

    AddressList getBcc();

    AddressList getCc();

    Date getDate();

    MailboxList getFrom();

    String getMessageId();

    AddressList getReplyTo();

    Mailbox getSender();

    String getSubject();

    AddressList getTo();

    void setBcc(Collection<? extends Address> collection);

    void setBcc(Address address);

    void setBcc(Address... addressArr);

    void setCc(Collection<? extends Address> collection);

    void setCc(Address address);

    void setCc(Address... addressArr);

    void setDate(Date date);

    void setDate(Date date, TimeZone timeZone);

    void setFrom(Collection<Mailbox> collection);

    void setFrom(Mailbox mailbox);

    void setFrom(Mailbox... mailboxArr);

    void setReplyTo(Collection<? extends Address> collection);

    void setReplyTo(Address address);

    void setReplyTo(Address... addressArr);

    void setSender(Mailbox mailbox);

    void setSubject(String str);

    void setTo(Collection<? extends Address> collection);

    void setTo(Address address);

    void setTo(Address... addressArr);
}
