package org.apache.james.mime4j.field.address;

import java.io.StringReader;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.address.Address;
import org.apache.james.mime4j.dom.address.AddressList;
import org.apache.james.mime4j.dom.address.Group;
import org.apache.james.mime4j.dom.address.Mailbox;

public class AddressBuilder {
    public static final AddressBuilder DEFAULT = new AddressBuilder();

    protected AddressBuilder() {
    }

    public Address parseAddress(String rawAddressString, DecodeMonitor monitor) throws ParseException {
        return Builder.getInstance().buildAddress(new AddressListParser(new StringReader(rawAddressString)).parseAddress(), monitor);
    }

    public Address parseAddress(String rawAddressString) throws ParseException {
        return parseAddress(rawAddressString, DecodeMonitor.STRICT);
    }

    public AddressList parseAddressList(String rawAddressList, DecodeMonitor monitor) throws ParseException {
        return Builder.getInstance().buildAddressList(new AddressListParser(new StringReader(rawAddressList)).parseAddressList(), monitor);
    }

    public AddressList parseAddressList(String rawAddressList) throws ParseException {
        return parseAddressList(rawAddressList, DecodeMonitor.STRICT);
    }

    public Mailbox parseMailbox(String rawMailboxString, DecodeMonitor monitor) throws ParseException {
        return Builder.getInstance().buildMailbox(new AddressListParser(new StringReader(rawMailboxString)).parseMailbox(), monitor);
    }

    public Mailbox parseMailbox(String rawMailboxString) throws ParseException {
        return parseMailbox(rawMailboxString, DecodeMonitor.STRICT);
    }

    public Group parseGroup(String rawGroupString, DecodeMonitor monitor) throws ParseException {
        Address address = parseAddress(rawGroupString, monitor);
        if (address instanceof Group) {
            return (Group) address;
        }
        throw new ParseException("Not a group address");
    }

    public Group parseGroup(String rawGroupString) throws ParseException {
        return parseGroup(rawGroupString, DecodeMonitor.STRICT);
    }
}
