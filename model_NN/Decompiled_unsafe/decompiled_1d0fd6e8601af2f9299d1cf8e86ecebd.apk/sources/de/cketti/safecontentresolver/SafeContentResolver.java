package de.cketti.safecontentresolver;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public abstract class SafeContentResolver {
    private final Blacklist blacklist;
    private final ContentResolver contentResolver;

    /* access modifiers changed from: protected */
    public abstract int getFileUidOrThrow(@NonNull FileDescriptor fileDescriptor) throws FileNotFoundException;

    @NonNull
    public static SafeContentResolver newInstance(@NonNull Context context) {
        if (context != null) {
            return new SafeContentResolverApi21(context);
        }
        throw new NullPointerException("Argument 'context' must not be null.");
    }

    protected SafeContentResolver(@NonNull Context context) {
        this.contentResolver = context.getContentResolver();
        this.blacklist = new Blacklist(context);
    }

    @Nullable
    public InputStream openInputStream(@NonNull Uri uri) throws FileNotFoundException {
        if (uri == null) {
            throw new NullPointerException("Argument 'uri' must not be null");
        }
        String scheme = uri.getScheme();
        if ("content".equals(scheme)) {
            String authority = uri.getAuthority();
            if (this.blacklist.isBlacklisted(authority)) {
                throw new FileNotFoundException("content URI is owned by the application itself. Content provider is not whitelisted: " + authority);
            }
        }
        if (!"file".equals(scheme)) {
            return this.contentResolver.openInputStream(uri);
        }
        ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.open(new File(uri.getPath()), 268435456);
        if (getFileUidOrThrow(parcelFileDescriptor.getFileDescriptor()) == Process.myUid()) {
            throw new FileNotFoundException("File is owned by the application itself");
        }
        try {
            return new AssetFileDescriptor(parcelFileDescriptor, 0, -1).createInputStream();
        } catch (IOException e) {
            throw new FileNotFoundException("Unable to create stream");
        }
    }
}
