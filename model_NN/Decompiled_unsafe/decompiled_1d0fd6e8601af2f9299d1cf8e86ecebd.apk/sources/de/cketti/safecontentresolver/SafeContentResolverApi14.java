package de.cketti.safecontentresolver;

import android.content.Context;
import android.support.annotation.NonNull;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;

class SafeContentResolverApi14 extends SafeContentResolver {
    SafeContentResolverApi14(Context context) {
        super(context);
        Os.init(context);
    }

    /* access modifiers changed from: protected */
    public int getFileUidOrThrow(@NonNull FileDescriptor fileDescriptor) throws FileNotFoundException {
        try {
            return Os.fstat(extractSystemFileDescriptor(fileDescriptor));
        } catch (ErrnoException | UnsupportedOperationException e) {
            throw new FileNotFoundException(e.getMessage());
        }
    }

    private int extractSystemFileDescriptor(FileDescriptor fileDescriptor) throws FileNotFoundException {
        try {
            Field descriptor = fileDescriptor.getClass().getDeclaredField("descriptor");
            descriptor.setAccessible(true);
            try {
                return descriptor.getInt(fileDescriptor);
            } catch (IllegalAccessException e) {
                throw new FileNotFoundException("Couldn't read system file descriptor");
            }
        } catch (NoSuchFieldException e2) {
            throw new FileNotFoundException("Couldn't find field that holds system file descriptor");
        }
    }
}
