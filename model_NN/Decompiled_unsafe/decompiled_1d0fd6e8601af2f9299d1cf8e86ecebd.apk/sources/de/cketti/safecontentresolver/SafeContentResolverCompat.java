package de.cketti.safecontentresolver;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;

public final class SafeContentResolverCompat {
    @NonNull
    public static SafeContentResolver newInstance(@NonNull Context context) {
        if (context == null) {
            throw new NullPointerException("Argument 'context' must not be null");
        } else if (Build.VERSION.SDK_INT < 21) {
            return new SafeContentResolverApi14(context);
        } else {
            return new SafeContentResolverApi21(context);
        }
    }
}
