package de.cketti.safecontentresolver;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.os.Bundle;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

class Blacklist {
    private static final String META_DATA_KEY_ALLOW_INTERNAL_ACCESS = "de.cketti.safecontentresolver.ALLOW_INTERNAL_ACCESS";
    private Set<String> blacklistedAuthorities;
    private final Context context;

    Blacklist(Context context2) {
        this.context = context2;
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean isBlacklisted(String authority) {
        if (this.blacklistedAuthorities == null) {
            this.blacklistedAuthorities = findBlacklistedContentProviderAuthorities();
        }
        return this.blacklistedAuthorities.contains(authority);
    }

    private Set<String> findBlacklistedContentProviderAuthorities() {
        ProviderInfo[] providers = getProviderInfo(this.context);
        Set<String> blacklistedAuthorities2 = new HashSet<>(providers.length);
        for (ProviderInfo providerInfo : providers) {
            if (!isContentProviderWhitelisted(providerInfo)) {
                Collections.addAll(blacklistedAuthorities2, providerInfo.authority.split(";"));
            }
        }
        return blacklistedAuthorities2;
    }

    private ProviderInfo[] getProviderInfo(Context context2) {
        try {
            ProviderInfo[] providers = context2.getPackageManager().getPackageInfo(context2.getPackageName(), 136).providers;
            return providers != null ? providers : new ProviderInfo[0];
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean isContentProviderWhitelisted(ProviderInfo providerInfo) {
        Bundle metaData = providerInfo.metaData;
        if (metaData == null || !metaData.getBoolean(META_DATA_KEY_ALLOW_INTERNAL_ACCESS, false)) {
            return false;
        }
        return true;
    }
}
