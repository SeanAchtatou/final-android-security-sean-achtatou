package android.support.v7.recyclerview;

public final class R {

    public static final class attr {
        public static final int layoutManager = 2130772290;
        public static final int reverseLayout = 2130772292;
        public static final int spanCount = 2130772291;
        public static final int stackFromEnd = 2130772293;
    }

    public static final class dimen {
        public static final int item_touch_helper_max_drag_scroll_per_frame = 2131361921;
        public static final int item_touch_helper_swipe_escape_max_velocity = 2131361922;
        public static final int item_touch_helper_swipe_escape_velocity = 2131361923;
    }

    public static final class id {
        public static final int item_touch_helper_previous_elevation = 2131755019;
    }

    public static final class styleable {
        public static final int[] RecyclerView = {16842948, 16842993, yahoo.mail.app.R.attr.layoutManager, yahoo.mail.app.R.attr.spanCount, yahoo.mail.app.R.attr.reverseLayout, yahoo.mail.app.R.attr.stackFromEnd};
        public static final int RecyclerView_android_descendantFocusability = 1;
        public static final int RecyclerView_android_orientation = 0;
        public static final int RecyclerView_layoutManager = 2;
        public static final int RecyclerView_reverseLayout = 4;
        public static final int RecyclerView_spanCount = 3;
        public static final int RecyclerView_stackFromEnd = 5;
    }
}
