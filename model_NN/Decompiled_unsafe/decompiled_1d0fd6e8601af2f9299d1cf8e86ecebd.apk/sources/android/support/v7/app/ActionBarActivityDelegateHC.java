package android.support.v7.app;

class ActionBarActivityDelegateHC extends ActionBarActivityDelegateBase {
    ActionBarActivityDelegateHC(ActionBarActivity activity) {
        super(activity);
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.support.v7.app.ActionBarActivity, android.support.v7.app.ActionBar$Callback] */
    public ActionBar createSupportActionBar() {
        ensureSubDecor();
        return new ActionBarImplHC(this.mActivity, this.mActivity);
    }
}
