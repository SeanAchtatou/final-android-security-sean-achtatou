package android.support.v7.app;

class ActionBarActivityDelegateJBMR2 extends ActionBarActivityDelegateJB {
    ActionBarActivityDelegateJBMR2(ActionBarActivity activity) {
        super(activity);
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.support.v7.app.ActionBarActivity, android.support.v7.app.ActionBar$Callback] */
    public ActionBar createSupportActionBar() {
        return new ActionBarImplJBMR2(this.mActivity, this.mActivity);
    }
}
