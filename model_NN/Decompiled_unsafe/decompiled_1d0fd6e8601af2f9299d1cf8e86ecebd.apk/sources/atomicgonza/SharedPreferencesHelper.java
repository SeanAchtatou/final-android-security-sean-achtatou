package atomicgonza;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

public class SharedPreferencesHelper {
    public static SharedPreferences getSharedPreferences(String name, Context ctx) {
        if (Build.VERSION.SDK_INT < 11) {
            return lessApi11(ctx, name);
        }
        return fromApi11(ctx, name);
    }

    @TargetApi(11)
    private static SharedPreferences fromApi11(Context ctx, String name) {
        return ctx.getSharedPreferences(name, 4);
    }

    private static SharedPreferences lessApi11(Context ctx, String name) {
        return ctx.getSharedPreferences(name, 0);
    }
}
