package atomicgonza;

import android.util.Patterns;

public class HotmailUtils {
    public static boolean isHotmailccount(String text) {
        return Patterns.EMAIL_ADDRESS.matcher(text).matches() && text.toLowerCase().endsWith("@hotmail.com");
    }
}
