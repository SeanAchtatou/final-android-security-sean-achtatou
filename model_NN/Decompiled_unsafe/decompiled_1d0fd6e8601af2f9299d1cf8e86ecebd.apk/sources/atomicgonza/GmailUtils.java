package atomicgonza;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fsck.k9.Account;
import com.fsck.k9.activity.setup.AccountSetupCheckSettings;
import com.fsck.k9.helper.UrlEncodingHelper;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import yahoo.mail.app.R;

public class GmailUtils {

    public interface CallBackAccessToken {
        void error(String str);

        void sucess();
    }

    public static boolean isGmailAccount(String text) {
        return Patterns.EMAIL_ADDRESS.matcher(text).matches() && text.toLowerCase().endsWith("@gmail.com");
    }

    public static void queryAccessToken(String code, final Context act, Account mAccount, boolean refreshToken, boolean isNewAccount, CallBackAccessToken callbackAccessToken) {
        Log.e("AtomicGonza", "okAG query accesToken for code: " + code + " " + new Exception().getStackTrace()[0].toString());
        final boolean z = refreshToken;
        final Context context = act;
        final Account account = mAccount;
        final String str = code;
        final CallBackAccessToken callBackAccessToken = callbackAccessToken;
        final boolean z2 = isNewAccount;
        AnonymousClass1 r2 = new Response.Listener<String>() {
            public void onResponse(String response) {
                Log.e("AtomicGonza", "okAG OAUTH TOKEN RESPONSE:" + response + " " + new Exception().getStackTrace()[0].toString());
                try {
                    JSONObject obj = new JSONObject(response);
                    String token = obj.getString("access_token");
                    if (!TextUtils.isEmpty(token)) {
                        if (z) {
                            App.setTokenGmailAccount(context, account.getEmail(), token, str, obj.getLong("expires_in"));
                            if (callBackAccessToken != null) {
                                callBackAccessToken.sucess();
                            }
                        } else {
                            App.setTokenGmailAccount(context, account.getEmail(), token, obj.getString("refresh_token"), obj.getLong("expires_in"));
                        }
                        if (!z && (context instanceof Activity)) {
                            AccountSetupCheckSettings.actionCheckSettings((Activity) context, account, AccountSetupCheckSettings.CheckDirection.INCOMING, z2);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        final boolean z3 = refreshToken;
        final CallBackAccessToken callBackAccessToken2 = callbackAccessToken;
        final Context context2 = act;
        final boolean z4 = refreshToken;
        final String str2 = code;
        Volley.newRequestQueue(act).add(new StringRequest(1, "https://www.googleapis.com/oauth2/v4/token", r2, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (!z3) {
                    Toast.makeText(act, "Error " + error.getMessage(), 0).show();
                } else if (callBackAccessToken2 != null) {
                    callBackAccessToken2.error(error.getMessage());
                }
            }
        }) {
            /* access modifiers changed from: protected */
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("client_id", context2.getString(R.string.oauth_gmailclient_id));
                if (z4) {
                    params.put("refresh_token", str2);
                    params.put("grant_type", "refresh_token");
                } else {
                    params.put("code", str2);
                    params.put("grant_type", "authorization_code");
                    params.put("redirect_uri", "yahoo.mail.app:" + context2.getString(R.string.path_oauth2));
                }
                return params;
            }
        });
    }

    public static void parseUriCallback(Activity act, String uri, String callbackUri, Account mAccount, boolean isNewAccount) {
        Uri uriData = Uri.parse(uri);
        if (uriData != null) {
            String code = uriData.getQueryParameter("code");
            if (code != null) {
                queryAccessToken(code, act, mAccount, false, isNewAccount, null);
            } else {
                Toast.makeText(act, "Error " + uriData.toString(), 0).show();
            }
        }
    }

    public static void displayDialogOauth2(Activity act, Account mAccount, boolean isNewAccount) {
        final String callBackUri = "yahoo.mail.app:" + act.getString(R.string.path_oauth2);
        final Dialog dialog = new Dialog(act, 16973829);
        final WebView webView = new WebView(act);
        webView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        WebSettings websettings = webView.getSettings();
        websettings.setLoadWithOverviewMode(true);
        websettings.setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
            String title = "";

            public void onProgressChanged(WebView view, int progress) {
                if (progress == 100) {
                    dialog.setTitle(this.title);
                } else {
                    dialog.setTitle("wait " + progress + "%");
                }
            }

            public void onReceivedTitle(WebView view, String title2) {
                super.onReceivedTitle(view, title2);
                dialog.setTitle(title2);
                this.title = title2;
            }
        });
        final Activity activity = act;
        final Account account = mAccount;
        final boolean z = isNewAccount;
        webView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                dialog.setTitle(description);
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (TextUtils.isEmpty(url) || !url.startsWith(callBackUri)) {
                    return false;
                }
                GmailUtils.parseUriCallback(activity, url, callBackUri, account, z);
                webView.destroy();
                dialog.dismiss();
                return true;
            }
        });
        new Uri.Builder().appendQueryParameter("", "");
        webView.loadUrl("https://accounts.google.com/o/oauth2/v2/auth?scope=https://mail.google.com/&redirect_uri=" + callBackUri + "&response_type=code&client_id=" + act.getString(R.string.oauth_gmailclient_id) + "&login_hint=" + UrlEncodingHelper.encodeUtf8(mAccount.getEmail()));
        LinearLayout layout = new LinearLayout(act);
        layout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        layout.addView(webView);
        dialog.setContentView(layout);
        dialog.show();
    }
}
