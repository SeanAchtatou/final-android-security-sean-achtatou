package atomicgonza;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v7.app.NotificationCompat;
import java.util.Random;

public class Notif {
    public static void show(Context ctx, String title) {
        ((NotificationManager) ctx.getSystemService("notification")).notify(new Random().nextInt(), new NotificationCompat.Builder(ctx).setTicker(title).setSmallIcon(17301579).setContentTitle(title).setContentText(title).setAutoCancel(true).build());
    }
}
