package atomicgonza;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.fsck.k9.K9;
import java.util.Date;
import java.util.Map;

public class App {
    public static Map<String, String> ACCOUNTS_TOKENS = null;
    public static final int DAYS_FREQ_TO_CHECK_PURCHASE = 7;
    public static final int DAYS_FREQ_TO_OFFER_PURCHASE = 7;
    public static final boolean DEBUG = false;
    public static final boolean IS_BILLING_ENABLED = false;
    public static final boolean IS_DEBUG = false;
    public static final boolean IS_DEBUG_SENSITIVE = false;
    public static final boolean IS_SIMPLE_WIDGET_CONFIG = true;
    public static final String KEY_APP_PURCHASED = "isAppPurchased";
    public static final String KEY_ASK_WIDGET_BEFORE = "asked_widget";
    public static final String KEY_REFRESH_TOKEN = ".refreshToken";
    public static final String KEY_TIME_EXPIRE_TOKEN = ".expireToken";
    public static final String KEY_TOKEN = ".token";
    public static final long LAUNCH_FREQ_TO_OFFER_PURCHASE = 70;
    public static final long LAUNCH_FREQ_TO_TO_CHECK_PURCHASE = 70;
    public static final int MIN_DAYS_PROMPT_RATE = 3;
    public static final int MIN_DAYS_UNTIL_BANNER_ADS = 0;
    public static final int MIN_DAYS_UNTIL_INTERSTITIAL_ADS = 0;
    public static final int MIN_LAUNCH_PROMPT_RATE = 0;
    public static final long MIN_LAUNCH_UNTIL_BANNER_ADS = 0;
    public static final long MIN_LAUNCH_UNTIL_INTERSTITIAL_ADS = 0;
    public static final String PCKG = "";
    public static final String PREFNAME_ATOMICGONZA = "PREF_ATG";
    public static final String PREFNAME_ATOMICGONZA_ACCOUNTS = "PREF_ATG_ACC";
    public static final String PURCHASE_PRODUCT_ID = "";
    public static final String PURCHASE_PRODUCT_NAME = "";
    public static final String VENDOR = "Atomicgonza";

    public static String getBase64EncodedPublicKey() {
        return "public" + "Key..";
    }

    public static String getTokenGmailAccount(Context ctx, String account) {
        return getPrefToken(ctx).getString(account + KEY_TOKEN, null);
    }

    public static String getRefreshTokenGmailAccount(Context ctx, String account) {
        return getPrefToken(ctx).getString(account + KEY_REFRESH_TOKEN, null);
    }

    public static long getTimeExpireTokenGmailAccount(Context ctx, String account) {
        return getPrefToken(ctx).getLong(account + KEY_TIME_EXPIRE_TOKEN, 0);
    }

    public static String getTokenGmailAccount(SharedPreferences pref, String account) {
        return pref.getString(account + KEY_TOKEN, null);
    }

    public static String getRefreshTokenGmailAccount(SharedPreferences pref, String account) {
        return pref.getString(account + KEY_REFRESH_TOKEN, null);
    }

    public static long getTimeExpireTokenGmailAccount(SharedPreferences pref, String account) {
        return pref.getLong(account + KEY_TIME_EXPIRE_TOKEN, 0);
    }

    public static void setTokenGmailAccount(Context ctx, String account, String token, String refreshToken, long timeExpireToken) {
        long timeExpirationToken = System.currentTimeMillis() + ((1000 * timeExpireToken) / 2);
        getPrefToken(ctx).edit().putString(account + KEY_TOKEN, token).putString(account + KEY_REFRESH_TOKEN, refreshToken).putLong(account + KEY_TIME_EXPIRE_TOKEN, timeExpirationToken).commit();
        if (K9.DEBUG) {
            Log.e("AtomicGonza", "okAG setToken on prefs " + account + "   " + token + " timeExpiration: " + new Date(timeExpirationToken).toString() + " " + new Exception().getStackTrace()[0].toString());
        }
    }

    public static boolean isExpired(SharedPreferences prefs, int days, long launches) {
        if (days == 0 || launches == 0) {
            return true;
        }
        long daysLimitInmillis = (long) (days * 24 * 60 * 60 * 1000);
        if (!prefs.contains("key_first_run") || !prefs.contains("key_run_count")) {
            prefs.edit().putLong("key_run_count", 1).putLong("key_first_run", System.currentTimeMillis()).commit();
        } else {
            long timeFirstRunMillis = prefs.getLong("key_first_run", 0);
            long timeNow = System.currentTimeMillis();
            long runCounts = prefs.getLong("key_run_count", 0);
            if (timeNow < timeFirstRunMillis || timeNow > timeFirstRunMillis + daysLimitInmillis || runCounts >= launches) {
                return true;
            }
            prefs.edit().putLong("key_run_count", runCounts + 1).commit();
        }
        return false;
    }

    public static boolean isAppPurchased(Activity act, SharedPreferences pref) {
        if (pref == null) {
            pref = getPref(act);
        }
        return pref.getBoolean(KEY_APP_PURCHASED, false);
    }

    public static SharedPreferences getPrefToken(Context ctx) {
        return SharedPreferencesHelper.getSharedPreferences(PREFNAME_ATOMICGONZA_ACCOUNTS, ctx);
    }

    public static SharedPreferences getPref(Context ctx) {
        return SharedPreferencesHelper.getSharedPreferences(PREFNAME_ATOMICGONZA, ctx);
    }
}
