package atomicgonza;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class Views {
    public static void hide(ViewGroup layout) {
        layout.setVisibility(4);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                hide((ViewGroup) child);
            } else {
                hide(child);
            }
        }
    }

    public static void hide(View layout) {
        layout.setVisibility(4);
    }

    public static void show(ViewGroup layout) {
        layout.setVisibility(0);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                show((ViewGroup) child);
            } else {
                show(child);
            }
        }
    }

    public static void show(View layout) {
        layout.setVisibility(0);
    }

    public static void gone(ViewGroup layout) {
        layout.setVisibility(8);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                gone((ViewGroup) child);
            } else {
                gone(child);
            }
        }
    }

    public static void gone(View layout) {
        layout.setVisibility(8);
    }

    public static void decorView(Activity act, int visibility) {
        switch (visibility) {
            case 0:
                show((ViewGroup) act.getWindow().getDecorView());
                return;
            case 4:
                hide((ViewGroup) act.getWindow().getDecorView());
                return;
            case 8:
                gone((ViewGroup) act.getWindow().getDecorView());
                return;
            default:
                return;
        }
    }

    public static void disable(ViewGroup layout) {
        layout.setEnabled(false);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                disable((ViewGroup) child);
            } else {
                child.setEnabled(false);
            }
        }
    }

    public static void disable(View view) {
        view.setEnabled(false);
    }

    public static void enable(ViewGroup layout) {
        layout.setEnabled(true);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                enable((ViewGroup) child);
            } else {
                child.setEnabled(true);
            }
        }
    }

    public static void enable(View view) {
        view.setEnabled(true);
    }

    public static boolean isEditText(ViewGroup layout) {
        boolean isEditable = false;
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                isEditable = isEditText((ViewGroup) child);
            } else {
                isEditable = isViewEditText(child);
            }
            if (isEditable) {
                return true;
            }
        }
        return isEditable;
    }

    public static boolean isViewEditText(View view) {
        return view instanceof EditText;
    }

    public static void setMargins(View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ((ViewGroup.MarginLayoutParams) v.getLayoutParams()).setMargins(l, t, r, b);
            v.requestLayout();
        }
    }

    public static View findViewByTag(ViewGroup layout, String tag) {
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (tag.equals(child.getTag())) {
                return child;
            }
            if (child instanceof ViewGroup) {
                child = findViewByTag((ViewGroup) child, tag);
            }
            if (child != null && tag.equals(child.getTag())) {
                return child;
            }
        }
        return null;
    }
}
