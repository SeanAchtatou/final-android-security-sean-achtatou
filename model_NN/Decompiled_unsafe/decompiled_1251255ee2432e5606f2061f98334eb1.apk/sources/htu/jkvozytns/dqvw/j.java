package htu.jkvozytns.dqvw;

import android.os.AsyncTask;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

class j extends AsyncTask<Object, Void, Void> {
    final /* synthetic */ i a;

    j(i iVar) {
        this.a = iVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Object... objArr) {
        try {
            Object a2 = a.a(d.a(1048920));
            Method method = a.b(839242).getMethod(d.a(1048812), new Class[0]);
            Method method2 = a.b(839242).getMethod(d.a(1048813), d.n);
            d.p.invoke(method, true);
            d.p.invoke(method2, true);
            Object invoke = method.invoke(a2, new Object[0]);
            method2.invoke(a2, 0);
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
            }
            Object newInstance = a.b(839151).newInstance();
            Method method3 = a.b(839151).getMethod(d.a(1048712), a.b(839141), a.b(839143));
            d.p.invoke(method3, true);
            method3.invoke(newInstance, d.a(1048623), objArr[0]);
            method3.invoke(newInstance, d.a(1048622), objArr[1]);
            method3.invoke(newInstance, d.a(1048624), false);
            b.a(2, newInstance);
            if (a.c()) {
                a.b(d.a(1048620));
                a.b(d.a(1048931));
            }
            method2.invoke(a2, invoke);
            return null;
        } catch (Exception e2) {
            a.a((Throwable) e2);
            return null;
        }
    }
}
