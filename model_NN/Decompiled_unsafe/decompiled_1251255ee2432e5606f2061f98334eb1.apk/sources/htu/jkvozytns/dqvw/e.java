package htu.jkvozytns.dqvw;

final class e {
    private static byte[] a(String str) {
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        if (length < 1 || length > 256) {
            throw new IllegalArgumentException();
        }
        byte[] bArr = new byte[256];
        byte[] bArr2 = new byte[256];
        for (int i = 0; i < 256; i++) {
            bArr[i] = (byte) i;
            bArr2[i] = bytes[i % length];
        }
        byte b = 0;
        for (int i2 = 0; i2 < 256; i2++) {
            b = (b + bArr[i2] + bArr2[i2]) & 255;
            byte b2 = bArr[b];
            bArr[b] = bArr[i2];
            bArr[i2] = b2;
        }
        return bArr;
    }

    static byte[] a(byte[] bArr, String str) {
        int length = bArr.length;
        byte[] bArr2 = new byte[length];
        byte[] a = a(str);
        byte b = 0;
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            i = (i + 1) & 255;
            b = (b + a[i]) & 255;
            byte b2 = a[b];
            a[b] = a[i];
            a[i] = b2;
            bArr2[i2] = (byte) (a[(a[i] + a[b]) & 255] ^ bArr[i2]);
        }
        return bArr2;
    }
}
