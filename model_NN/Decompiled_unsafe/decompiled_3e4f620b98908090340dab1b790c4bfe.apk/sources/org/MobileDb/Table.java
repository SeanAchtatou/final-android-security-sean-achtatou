package org.MobileDb;

import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public class Table {
    private long _offset;
    private Vector fields;
    private InputStream inputStream;
    private int last_find_index;
    private Row last_find_row;
    private boolean loadAllDataInMemory;
    public String name;
    private Field[] opt_fields;
    private Row[] opt_rows;
    private boolean optimized;
    private String pathToDb;
    private Vector rows;
    private int rowsCount;
    private boolean transaction;

    public Table() {
        this.optimized = false;
        this.opt_fields = null;
        this.opt_rows = null;
        this.last_find_index = -1;
        this.last_find_row = null;
        this.loadAllDataInMemory = true;
        this.pathToDb = null;
        this.transaction = false;
        this.inputStream = null;
        this.rowsCount = 0;
        this._offset = -1;
        this.name = "";
        this.fields = new Vector();
        this.rows = new Vector();
    }

    public Table(String str) {
        this.optimized = false;
        this.opt_fields = null;
        this.opt_rows = null;
        this.last_find_index = -1;
        this.last_find_row = null;
        this.loadAllDataInMemory = true;
        this.pathToDb = null;
        this.transaction = false;
        this.inputStream = null;
        this.rowsCount = 0;
        this._offset = -1;
        this.name = str;
        this.fields = new Vector();
        this.rows = new Vector();
    }

    public Table(String str, boolean z, String str2) {
        this.optimized = false;
        this.opt_fields = null;
        this.opt_rows = null;
        this.last_find_index = -1;
        this.last_find_row = null;
        this.loadAllDataInMemory = true;
        this.pathToDb = null;
        this.transaction = false;
        this.inputStream = null;
        this.rowsCount = 0;
        this._offset = -1;
        this.name = str;
        this.fields = new Vector();
        this.loadAllDataInMemory = z;
        if (z) {
            this.rows = new Vector();
            this.pathToDb = null;
            return;
        }
        this.pathToDb = str2;
    }

    public void addField(Field field) {
        if (!this.optimized) {
            this.fields.addElement(field);
        }
    }

    public void addRow() {
        this.rowsCount++;
    }

    public void addRow(Row row) {
        if (!this.optimized) {
            this.rows.addElement(row);
        }
    }

    public Row createRow() {
        if (this.optimized) {
            return null;
        }
        int[] iArr = new int[this.fields.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i >= this.fields.size()) {
                return new Row(iArr);
            }
            iArr[i2] = ((Field) this.fields.elementAt(i2)).type;
            i = i2 + 1;
        }
    }

    public int fieldsCount() {
        return this.optimized ? this.opt_fields.length : this.fields.size();
    }

    public Field getField(int i) {
        if (this.optimized) {
            if (i >= 0 && i < this.opt_fields.length) {
                return this.opt_fields[i];
            }
        } else if (i >= 0 && i < this.fields.size()) {
            return (Field) this.fields.elementAt(i);
        }
        return null;
    }

    public Object getFieldValueByName(String str, int i) {
        int i2 = 0;
        if (this.loadAllDataInMemory) {
            if (this.optimized) {
                if (this.last_find_index == i || getRow(i) != null) {
                    while (true) {
                        int i3 = i2;
                        if (i2 >= this.opt_fields.length) {
                            break;
                        } else if (str.equals(this.opt_fields[i3].name)) {
                            return this.last_find_row.getValue(i3);
                        } else {
                            i2 = i3 + 1;
                        }
                    }
                } else {
                    return null;
                }
            } else if (this.last_find_index == i || getRow(i) != null) {
                while (true) {
                    int i4 = i2;
                    if (i2 >= this.fields.size()) {
                        break;
                    } else if (str.equals(((Field) this.fields.elementAt(i4)).name)) {
                        return this.last_find_row.getValue(i4);
                    } else {
                        i2 = i4 + 1;
                    }
                }
            } else {
                return null;
            }
        } else if (this.last_find_index == i || getRow(i) != null) {
            while (true) {
                int i5 = i2;
                if (i2 >= this.fields.size()) {
                    break;
                } else if (str.equals(((Field) this.fields.elementAt(i5)).name)) {
                    return this.last_find_row.getValue(i5);
                } else {
                    i2 = i5 + 1;
                }
            }
        } else {
            return null;
        }
        return null;
    }

    public long getOffset() {
        return this._offset;
    }

    public Row getRow(int i) {
        if (this.loadAllDataInMemory) {
            if (this.optimized) {
                if (i >= 0 && i < this.opt_rows.length) {
                    this.last_find_row = this.opt_rows[i];
                    this.last_find_index = i;
                    return this.last_find_row;
                }
            } else if (i >= 0 && i < this.rows.size()) {
                this.last_find_row = (Row) this.rows.elementAt(i);
                this.last_find_index = i;
                return this.last_find_row;
            }
        } else if (i >= 0 && i < this.rowsCount) {
            this.last_find_index = i;
            if (this.transaction) {
                InputStream inputStream2 = this.inputStream;
                try {
                    if (inputStream2.read() == 11) {
                        Row createRow = createRow();
                        int i2 = 0;
                        int i3 = 0;
                        while (i3 < createRow.fieldsCount()) {
                            int fieldType = createRow.getFieldType(i2);
                            if (fieldType == Field.SMALL_INT) {
                                createRow.setValue(i2, new Integer(inputStream2.read()));
                            } else if (fieldType == Field.SHORT_INT) {
                                byte[] bArr = new byte[2];
                                MobileDatabase.readDataFromStream(inputStream2, bArr);
                                createRow.setValue(i2, new Integer(MobileDatabase.shortIntFromBytes(bArr)));
                            } else if (fieldType == Field.INT) {
                                byte[] bArr2 = new byte[4];
                                MobileDatabase.readDataFromStream(inputStream2, bArr2);
                                createRow.setValue(i2, new Integer(MobileDatabase.intFromBytes(bArr2)));
                            } else if (fieldType == Field.TIME) {
                                byte[] bArr3 = new byte[4];
                                MobileDatabase.readDataFromStream(inputStream2, bArr3);
                                createRow.setValue(i2, new Integer(MobileDatabase.intFromBytes(bArr3)));
                            } else if (fieldType == Field.NAME) {
                                byte[] bArr4 = new byte[inputStream2.read()];
                                MobileDatabase.readDataFromStream(inputStream2, bArr4);
                                createRow.setValue(i2, MobileDatabase.getUtf8String(bArr4));
                            } else if (fieldType == Field.TEXT) {
                                byte[] bArr5 = new byte[2];
                                MobileDatabase.readDataFromStream(inputStream2, bArr5);
                                byte[] bArr6 = new byte[MobileDatabase.shortIntFromBytes(bArr5)];
                                MobileDatabase.readDataFromStream(inputStream2, bArr6);
                                createRow.setValue(i2, MobileDatabase.getUtf8String(bArr6));
                            } else if (fieldType == Field.BINARY) {
                                byte[] bArr7 = new byte[4];
                                MobileDatabase.readDataFromStream(inputStream2, bArr7);
                                byte[] bArr8 = new byte[MobileDatabase.intFromBytes(bArr7)];
                                MobileDatabase.readDataFromStream(inputStream2, bArr8);
                                createRow.setValue(i2, bArr8);
                            }
                            i2++;
                            i3 = i2;
                        }
                        this.last_find_row = createRow;
                        return this.last_find_row;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    public boolean isOptimized() {
        return this.optimized;
    }

    public void optimize() {
        if (this.loadAllDataInMemory && !this.optimized) {
            this.opt_fields = new Field[this.fields.size()];
            this.fields.copyInto(this.opt_fields);
            this.opt_rows = new Row[this.rows.size()];
            this.rows.copyInto(this.opt_rows);
            this.optimized = true;
            this.fields = null;
            this.rows = null;
            System.gc();
        }
    }

    public void removeAllFields() {
        if (!this.optimized) {
            this.fields.removeAllElements();
        }
    }

    public void removeAllRows() {
        if (this.loadAllDataInMemory && !this.optimized) {
            this.rows.removeAllElements();
        }
    }

    public void removeField(Field field) {
        if (!this.optimized) {
            this.fields.removeElement(field);
        }
    }

    public void removeRow(Row row) {
        if (!this.optimized) {
            this.rows.removeElement(row);
        }
    }

    public int rowsCount() {
        return this.loadAllDataInMemory ? this.optimized ? this.opt_rows.length : this.rows.size() : this.rowsCount;
    }

    public void setOffset(long j) {
        this._offset = j - 1;
    }

    public void startTransaction() {
        if (!this.loadAllDataInMemory && this.rowsCount != 0) {
            try {
                this.inputStream = getClass().getResourceAsStream(this.pathToDb);
                this.inputStream.skip(this._offset);
                this.transaction = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void stopTransaction() {
        if (!this.loadAllDataInMemory) {
            try {
                this.inputStream.close();
            } catch (IOException e) {
            }
            this.transaction = true;
        }
    }
}
