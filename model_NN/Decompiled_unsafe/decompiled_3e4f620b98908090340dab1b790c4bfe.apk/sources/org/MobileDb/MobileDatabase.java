package org.MobileDb;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Vector;

public class MobileDatabase {
    private static boolean useNativeUtf8Decoder = true;
    public int Version = 0;
    private boolean loadAllDataInMemory = true;
    private String path = null;
    private Vector tables = new Vector();

    public MobileDatabase() {
        useNativeUtf8Decoder = isSupportUtf8();
    }

    public static String getUtf8String(byte[] bArr) {
        if (useNativeUtf8Decoder) {
            try {
                return new String(bArr, "utf-8");
            } catch (UnsupportedEncodingException e) {
                return "";
            }
        } else {
            Utf8StringBuffer utf8StringBuffer = new Utf8StringBuffer();
            utf8StringBuffer.append(bArr, 0, bArr.length);
            return utf8StringBuffer.toString();
        }
    }

    public static int intFromBytes(byte[] bArr) {
        return ((bArr[3] & 255) << 24) + ((bArr[2] & 255) << 16) + ((bArr[1] & 255) << 8) + (bArr[0] & 255);
    }

    public static boolean isSupportUtf8() {
        try {
            new String("23".getBytes(), "utf-8");
            return true;
        } catch (UnsupportedEncodingException e) {
            return false;
        }
    }

    public static void readDataFromStream(InputStream inputStream, byte[] bArr) throws IOException {
        int i = 0;
        int length = bArr.length;
        while (true) {
            int i2 = i;
            int i3 = length;
            int read = inputStream.read(bArr, i2, i3);
            length = i3 - read;
            if (length != 0) {
                i = i2 + read;
            } else {
                return;
            }
        }
    }

    public static int shortIntFromBytes(byte[] bArr) {
        return ((bArr[1] & 255) << 8) + (bArr[0] & 255);
    }

    public Table getTable(int i) {
        if (i < 0 || i >= this.tables.size()) {
            return null;
        }
        return (Table) this.tables.elementAt(i);
    }

    public Table getTableByName(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i >= this.tables.size()) {
                return null;
            }
            Table table = (Table) this.tables.elementAt(i2);
            if (table.name.equals(str)) {
                return table;
            }
            i = i2 + 1;
        }
    }

    public void loadFrom(InputStream inputStream) throws IOException {
        long j;
        long j2;
        byte[] bArr = new byte[4];
        readDataFromStream(inputStream, bArr);
        this.Version = inputStream.read();
        long j3 = 0 + 4 + 1;
        Table table = null;
        while (true) {
            Table table2 = table;
            byte[] bArr2 = bArr;
            while (true) {
                long j4 = j;
                long read = (long) inputStream.read();
                if (read != -1) {
                    j2 = 1 + j4;
                    if (read != 9) {
                        if (read != 10) {
                            if (read != 11) {
                                break;
                            }
                            Row createRow = table2.createRow();
                            j = j2;
                            for (int i = 0; i < createRow.fieldsCount(); i++) {
                                int fieldType = createRow.getFieldType(i);
                                if (fieldType == Field.SMALL_INT) {
                                    j++;
                                    createRow.setValue(i, new Integer(inputStream.read()));
                                } else if (fieldType == Field.SHORT_INT) {
                                    byte[] bArr3 = new byte[2];
                                    readDataFromStream(inputStream, bArr3);
                                    j += 2;
                                    createRow.setValue(i, new Integer(shortIntFromBytes(bArr3)));
                                } else if (fieldType == Field.INT) {
                                    byte[] bArr4 = new byte[4];
                                    readDataFromStream(inputStream, bArr4);
                                    j += 4;
                                    createRow.setValue(i, new Integer(intFromBytes(bArr4)));
                                } else if (fieldType == Field.TIME) {
                                    byte[] bArr5 = new byte[4];
                                    readDataFromStream(inputStream, bArr5);
                                    j += 4;
                                    createRow.setValue(i, new Integer(intFromBytes(bArr5)));
                                } else if (fieldType == Field.NAME) {
                                    int read2 = inputStream.read();
                                    byte[] bArr6 = new byte[read2];
                                    readDataFromStream(inputStream, bArr6);
                                    j = j + 1 + ((long) read2);
                                    createRow.setValue(i, getUtf8String(bArr6));
                                } else if (fieldType == Field.TEXT) {
                                    byte[] bArr7 = new byte[2];
                                    readDataFromStream(inputStream, bArr7);
                                    int shortIntFromBytes = shortIntFromBytes(bArr7);
                                    byte[] bArr8 = new byte[shortIntFromBytes];
                                    readDataFromStream(inputStream, bArr8);
                                    j = j + 2 + ((long) shortIntFromBytes);
                                    createRow.setValue(i, getUtf8String(bArr8));
                                } else if (fieldType == Field.BINARY) {
                                    byte[] bArr9 = new byte[4];
                                    readDataFromStream(inputStream, bArr9);
                                    int intFromBytes = intFromBytes(bArr9);
                                    readDataFromStream(inputStream, bArr2);
                                    j = j + 4 + ((long) intFromBytes);
                                    createRow.setValue(i, new byte[intFromBytes]);
                                }
                            }
                            if (this.loadAllDataInMemory) {
                                table2.addRow(createRow);
                            } else {
                                if (table2.getOffset() == -1) {
                                    table2.setOffset(j2);
                                }
                                table2.addRow();
                            }
                        } else {
                            int read3 = inputStream.read();
                            int read4 = inputStream.read();
                            byte[] bArr10 = new byte[read4];
                            readDataFromStream(inputStream, bArr10);
                            j = ((long) read4) + j2 + 1 + 1;
                            table2.addField(new Field(read3, getUtf8String(bArr10)));
                            bArr2 = bArr10;
                        }
                    } else {
                        int read5 = inputStream.read();
                        byte[] bArr11 = new byte[read5];
                        readDataFromStream(inputStream, bArr11);
                        j = ((long) read5) + 1 + j2;
                        Table table3 = new Table(getUtf8String(bArr11), this.loadAllDataInMemory, this.path);
                        this.tables.addElement(table3);
                        table2 = table3;
                        bArr2 = bArr11;
                    }
                } else {
                    inputStream.close();
                    return;
                }
            }
            j3 = j2;
            table = table2;
            bArr = bArr2;
        }
    }

    public void loadFrom(String str) throws IOException {
        this.loadAllDataInMemory = true;
        this.path = str;
        loadFrom(getClass().getResourceAsStream(str));
    }

    public void loadFrom(String str, boolean z) throws IOException {
        this.loadAllDataInMemory = z;
        this.path = str;
        loadFrom(getClass().getResourceAsStream(str));
    }

    public void optimize() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i < this.tables.size()) {
                ((Table) this.tables.elementAt(i2)).optimize();
                i = i2 + 1;
            } else {
                System.gc();
                return;
            }
        }
    }

    public int tablesCount() {
        return this.tables.size();
    }
}
