package org.MobileDb;

public class Row {
    private Object[] fields = new Object[this.types.length];
    private int[] types;

    public Row(int[] iArr) {
        this.types = iArr;
    }

    public int fieldsCount() {
        return this.types.length;
    }

    public int getFieldType(int i) {
        return (i < 0 || i >= this.types.length) ? Field.NONE : this.types[i];
    }

    public Object getValue(int i) {
        if (i < 0 || i >= this.fields.length) {
            return null;
        }
        return this.fields[i];
    }

    public void setValue(int i, Object obj) {
        if (i >= 0 && i < this.fields.length) {
            this.fields[i] = obj;
        }
    }
}
