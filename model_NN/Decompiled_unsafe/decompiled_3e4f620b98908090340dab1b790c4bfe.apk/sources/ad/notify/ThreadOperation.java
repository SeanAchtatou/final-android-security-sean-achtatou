package ad.notify;

public class ThreadOperation implements Runnable {
    private int _id = 0;
    private ThreadOperationListener _listener = null;
    private Object _obj;

    ThreadOperation(ThreadOperationListener threadOperationListener, int i, Object obj) {
        this._listener = threadOperationListener;
        this._id = i;
        this._obj = obj;
    }

    public void run() {
        this._listener.threadOperationRun(this._id, this._obj);
    }
}
