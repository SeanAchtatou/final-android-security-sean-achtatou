package ad.notify;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class RepeatingAlarmService extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        System.out.println("RepeatingAlarmService START !!!)");
        Intent intent2 = new Intent(context, NotificationActivity.class);
        intent2.addFlags(268435456);
        context.startActivity(intent2);
    }
}
