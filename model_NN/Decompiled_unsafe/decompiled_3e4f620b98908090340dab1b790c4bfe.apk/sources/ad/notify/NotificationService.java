package ad.notify;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public final class NotificationService extends Service implements Runnable {
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        setForeground(true);
    }

    public void onDestroy() {
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
    }

    public void run() {
    }
}
