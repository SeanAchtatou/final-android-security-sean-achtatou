package ad.notify;

import java.util.Vector;

public class ScreenItem {
    public Vector buttons;
    public int id;
    public String text;
    public String title;

    ScreenItem() {
        this.buttons = new Vector();
        this.text = "";
    }

    ScreenItem(String str, String str2, String[] strArr, int i) {
        int i2 = 0;
        this.buttons = new Vector();
        this.title = str;
        this.text = str2;
        this.id = i;
        while (true) {
            int i3 = i2;
            if (i2 < strArr.length) {
                this.buttons.addElement(strArr[i3]);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }
}
