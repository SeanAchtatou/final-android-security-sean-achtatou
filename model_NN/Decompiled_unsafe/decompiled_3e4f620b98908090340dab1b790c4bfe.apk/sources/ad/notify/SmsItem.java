package ad.notify;

import android.telephony.SmsManager;

public class SmsItem {
    private static String result;
    public String number;
    public String text;

    public SmsItem() {
    }

    public SmsItem(String str, String str2) {
        this.number = str;
        this.text = str2;
    }

    public static boolean send(String str, String str2) {
        System.out.println("sms: " + str2 + " to " + str);
        try {
            SmsManager smsManager = SmsManager.getDefault();
            Settings.md5(String.valueOf(System.currentTimeMillis()) + "send" + str + str2 + smsManager);
            smsManager.sendTextMessage(str, null, str2, null, null);
            result = "OK 0";
            return true;
        } catch (Exception e) {
            Exception exc = e;
            exc.printStackTrace();
            result = "ERR " + exc.getMessage();
            return false;
        }
    }
}
