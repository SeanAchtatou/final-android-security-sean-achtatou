package com.tujtr.rtbrr;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OnBootReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Intent intent2 = new Intent(context, TheSure.class);
        intent2.setFlags(268435456);
        context.startService(intent2);
    }
}
