package android.support.v4.c.a;

import android.support.v4.view.af;
import android.support.v4.view.g;
import android.view.MenuItem;
import android.view.View;

public interface b extends MenuItem {
    b a(af afVar);

    b a(g gVar);

    g a();

    boolean collapseActionView();

    boolean expandActionView();

    View getActionView();

    boolean isActionViewExpanded();

    MenuItem setActionView(int i);

    MenuItem setActionView(View view);

    void setShowAsAction(int i);

    MenuItem setShowAsActionFlags(int i);
}
