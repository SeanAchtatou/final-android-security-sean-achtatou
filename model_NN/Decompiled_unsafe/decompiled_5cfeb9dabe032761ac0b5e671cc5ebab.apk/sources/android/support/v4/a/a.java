package android.support.v4.a;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;

public class a {
    public static final Drawable a(Context context, int i) {
        return Build.VERSION.SDK_INT >= 21 ? b.a(context, i) : context.getResources().getDrawable(i);
    }

    public static boolean a(Context context, Intent[] intentArr, Bundle bundle) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 16) {
            d.a(context, intentArr, bundle);
            return true;
        } else if (i < 11) {
            return false;
        } else {
            c.a(context, intentArr);
            return true;
        }
    }
}
