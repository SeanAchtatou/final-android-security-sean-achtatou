package android.support.v4.a;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private static final f f1a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 15) {
            f1a = new i();
        } else if (i >= 11) {
            f1a = new h();
        } else {
            f1a = new g();
        }
    }

    public static Intent a(ComponentName componentName) {
        return f1a.a(componentName);
    }
}
