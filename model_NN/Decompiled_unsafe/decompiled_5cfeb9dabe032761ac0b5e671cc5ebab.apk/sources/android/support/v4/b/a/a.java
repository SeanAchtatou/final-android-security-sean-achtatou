package android.support.v4.b.a;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;

public class a {

    /* renamed from: a  reason: collision with root package name */
    static final c f33a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            f33a = new f();
        } else if (i >= 19) {
            f33a = new e();
        } else if (i >= 11) {
            f33a = new d();
        } else {
            f33a = new b();
        }
    }

    public static void a(Drawable drawable) {
        f33a.a(drawable);
    }

    public static void a(Drawable drawable, float f, float f2) {
        f33a.a(drawable, f, f2);
    }

    public static void a(Drawable drawable, int i) {
        f33a.a(drawable, i);
    }

    public static void a(Drawable drawable, int i, int i2, int i3, int i4) {
        f33a.a(drawable, i, i2, i3, i4);
    }

    public static void a(Drawable drawable, ColorStateList colorStateList) {
        f33a.a(drawable, colorStateList);
    }

    public static void a(Drawable drawable, PorterDuff.Mode mode) {
        f33a.a(drawable, mode);
    }

    public static void a(Drawable drawable, boolean z) {
        f33a.a(drawable, z);
    }

    public static boolean b(Drawable drawable) {
        return f33a.b(drawable);
    }
}
