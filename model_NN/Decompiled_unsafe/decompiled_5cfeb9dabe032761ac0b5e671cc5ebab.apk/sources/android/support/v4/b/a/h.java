package android.support.v4.b.a;

import android.graphics.drawable.Drawable;

class h {
    public static void a(Drawable drawable, boolean z) {
        drawable.setAutoMirrored(z);
    }

    public static boolean a(Drawable drawable) {
        return drawable.isAutoMirrored();
    }
}
