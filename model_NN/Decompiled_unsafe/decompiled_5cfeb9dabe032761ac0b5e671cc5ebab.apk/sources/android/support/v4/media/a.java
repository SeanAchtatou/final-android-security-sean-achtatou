package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable;

final class a implements Parcelable.Creator {
    a() {
    }

    /* renamed from: a */
    public MediaMetadataCompat createFromParcel(Parcel parcel) {
        return new MediaMetadataCompat(parcel, null);
    }

    /* renamed from: a */
    public MediaMetadataCompat[] newArray(int i) {
        return new MediaMetadataCompat[i];
    }
}
