package android.support.v4.media;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.f.a;

public final class MediaMetadataCompat implements Parcelable {
    public static final Parcelable.Creator CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    private static final a f50a = new a();
    private final Bundle b;

    static {
        f50a.put("android.media.metadata.TITLE", 1);
        f50a.put("android.media.metadata.ARTIST", 1);
        f50a.put("android.media.metadata.DURATION", 0);
        f50a.put("android.media.metadata.ALBUM", 1);
        f50a.put("android.media.metadata.AUTHOR", 1);
        f50a.put("android.media.metadata.WRITER", 1);
        f50a.put("android.media.metadata.COMPOSER", 1);
        f50a.put("android.media.metadata.COMPILATION", 1);
        f50a.put("android.media.metadata.DATE", 1);
        f50a.put("android.media.metadata.YEAR", 0);
        f50a.put("android.media.metadata.GENRE", 1);
        f50a.put("android.media.metadata.TRACK_NUMBER", 0);
        f50a.put("android.media.metadata.NUM_TRACKS", 0);
        f50a.put("android.media.metadata.DISC_NUMBER", 0);
        f50a.put("android.media.metadata.ALBUM_ARTIST", 1);
        f50a.put("android.media.metadata.ART", 2);
        f50a.put("android.media.metadata.ART_URI", 1);
        f50a.put("android.media.metadata.ALBUM_ART", 2);
        f50a.put("android.media.metadata.ALBUM_ART_URI", 1);
        f50a.put("android.media.metadata.USER_RATING", 3);
        f50a.put("android.media.metadata.RATING", 3);
        f50a.put("android.media.metadata.DISPLAY_TITLE", 1);
        f50a.put("android.media.metadata.DISPLAY_SUBTITLE", 1);
        f50a.put("android.media.metadata.DISPLAY_DESCRIPTION", 1);
        f50a.put("android.media.metadata.DISPLAY_ICON", 2);
        f50a.put("android.media.metadata.DISPLAY_ICON_URI", 1);
    }

    private MediaMetadataCompat(Parcel parcel) {
        this.b = parcel.readBundle();
    }

    /* synthetic */ MediaMetadataCompat(Parcel parcel, a aVar) {
        this(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeBundle(this.b);
    }
}
