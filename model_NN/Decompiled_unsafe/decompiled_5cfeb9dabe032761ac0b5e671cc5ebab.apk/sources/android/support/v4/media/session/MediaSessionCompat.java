package android.support.v4.media.session;

import android.os.Parcel;
import android.os.Parcelable;

public class MediaSessionCompat {

    public final class Token implements Parcelable {
        public static final Parcelable.Creator CREATOR = new a();

        /* renamed from: a  reason: collision with root package name */
        private final Parcelable f52a;

        Token(Parcelable parcelable) {
            this.f52a = parcelable;
        }

        public int describeContents() {
            return this.f52a.describeContents();
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeParcelable(this.f52a, i);
        }
    }
}
