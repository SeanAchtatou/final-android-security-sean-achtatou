package android.support.v4.media.session;

import android.os.Parcel;
import android.os.Parcelable;

final class b implements Parcelable.Creator {
    b() {
    }

    /* renamed from: a */
    public PlaybackStateCompat createFromParcel(Parcel parcel) {
        return new PlaybackStateCompat(parcel, null);
    }

    /* renamed from: a */
    public PlaybackStateCompat[] newArray(int i) {
        return new PlaybackStateCompat[i];
    }
}
