package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable;

final class b implements Parcelable.Creator {
    b() {
    }

    /* renamed from: a */
    public RatingCompat createFromParcel(Parcel parcel) {
        return new RatingCompat(parcel.readInt(), parcel.readFloat(), null);
    }

    /* renamed from: a */
    public RatingCompat[] newArray(int i) {
        return new RatingCompat[i];
    }
}
