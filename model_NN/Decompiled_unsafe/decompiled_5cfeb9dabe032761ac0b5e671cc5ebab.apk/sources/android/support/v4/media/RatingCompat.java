package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable;

public final class RatingCompat implements Parcelable {
    public static final Parcelable.Creator CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private final int f51a;
    private final float b;

    private RatingCompat(int i, float f) {
        this.f51a = i;
        this.b = f;
    }

    /* synthetic */ RatingCompat(int i, float f, b bVar) {
        this(i, f);
    }

    public int describeContents() {
        return this.f51a;
    }

    public String toString() {
        return "Rating:style=" + this.f51a + " rating=" + (this.b < 0.0f ? "unrated" : String.valueOf(this.b));
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f51a);
        parcel.writeFloat(this.b);
    }
}
