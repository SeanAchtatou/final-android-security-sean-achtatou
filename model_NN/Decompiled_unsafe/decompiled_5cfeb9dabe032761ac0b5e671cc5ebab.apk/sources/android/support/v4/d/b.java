package android.support.v4.d;

import android.os.Parcel;
import android.os.Parcelable;

class b implements Parcelable.Creator {

    /* renamed from: a  reason: collision with root package name */
    final c f34a;

    public b(c cVar) {
        this.f34a = cVar;
    }

    public Object createFromParcel(Parcel parcel) {
        return this.f34a.a(parcel, null);
    }

    public Object[] newArray(int i) {
        return this.f34a.a(i);
    }
}
