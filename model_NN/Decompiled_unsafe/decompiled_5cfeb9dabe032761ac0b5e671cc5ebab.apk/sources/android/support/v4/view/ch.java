package android.support.v4.view;

import android.view.View;
import android.view.animation.Interpolator;
import java.util.WeakHashMap;

class ch implements co {

    /* renamed from: a  reason: collision with root package name */
    WeakHashMap f69a = null;

    ch() {
    }

    private void a(View view) {
        Runnable runnable;
        if (this.f69a != null && (runnable = (Runnable) this.f69a.get(view)) != null) {
            view.removeCallbacks(runnable);
        }
    }

    /* access modifiers changed from: private */
    public void c(cf cfVar, View view) {
        Object tag = view.getTag(2113929216);
        cv cvVar = tag instanceof cv ? (cv) tag : null;
        Runnable a2 = cfVar.c;
        Runnable b = cfVar.d;
        if (a2 != null) {
            a2.run();
        }
        if (cvVar != null) {
            cvVar.a(view);
            cvVar.b(view);
        }
        if (b != null) {
            b.run();
        }
        if (this.f69a != null) {
            this.f69a.remove(view);
        }
    }

    private void d(cf cfVar, View view) {
        Runnable runnable = this.f69a != null ? (Runnable) this.f69a.get(view) : null;
        if (runnable == null) {
            runnable = new ci(this, cfVar, view);
            if (this.f69a == null) {
                this.f69a = new WeakHashMap();
            }
            this.f69a.put(view, runnable);
        }
        view.removeCallbacks(runnable);
        view.post(runnable);
    }

    public void a(cf cfVar, View view) {
        d(cfVar, view);
    }

    public void a(cf cfVar, View view, float f) {
        d(cfVar, view);
    }

    public void a(cf cfVar, View view, long j) {
    }

    public void a(cf cfVar, View view, cv cvVar) {
        view.setTag(2113929216, cvVar);
    }

    public void a(cf cfVar, View view, cx cxVar) {
    }

    public void a(cf cfVar, View view, Interpolator interpolator) {
    }

    public void b(cf cfVar, View view) {
        a(view);
        c(cfVar, view);
    }

    public void b(cf cfVar, View view, float f) {
        d(cfVar, view);
    }

    public void c(cf cfVar, View view, float f) {
        d(cfVar, view);
    }

    public void d(cf cfVar, View view, float f) {
        d(cfVar, view);
    }
}
