package android.support.v4.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

final class cq extends AnimatorListenerAdapter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cv f72a;
    final /* synthetic */ View b;

    cq(cv cvVar, View view) {
        this.f72a = cvVar;
        this.b = view;
    }

    public void onAnimationCancel(Animator animator) {
        this.f72a.c(this.b);
    }

    public void onAnimationEnd(Animator animator) {
        this.f72a.b(this.b);
    }

    public void onAnimationStart(Animator animator) {
        this.f72a.a(this.b);
    }
}
