package android.support.v4.view;

import android.database.DataSetObserver;

class cc extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ViewPager f67a;

    private cc(ViewPager viewPager) {
        this.f67a = viewPager;
    }

    /* synthetic */ cc(ViewPager viewPager, bu buVar) {
        this(viewPager);
    }

    public void onChanged() {
        this.f67a.a();
    }

    public void onInvalidated() {
        this.f67a.a();
    }
}
