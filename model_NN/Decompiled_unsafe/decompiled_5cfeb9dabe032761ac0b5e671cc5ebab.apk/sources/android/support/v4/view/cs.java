package android.support.v4.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

final class cs extends AnimatorListenerAdapter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cv f73a;
    final /* synthetic */ View b;

    cs(cv cvVar, View view) {
        this.f73a = cvVar;
        this.b = view;
    }

    public void onAnimationCancel(Animator animator) {
        this.f73a.c(this.b);
    }

    public void onAnimationEnd(Animator animator) {
        this.f73a.b(this.b);
    }

    public void onAnimationStart(Animator animator) {
        this.f73a.a(this.b);
    }
}
