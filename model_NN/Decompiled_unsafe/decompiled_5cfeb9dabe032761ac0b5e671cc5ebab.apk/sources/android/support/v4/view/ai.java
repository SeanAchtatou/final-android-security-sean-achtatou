package android.support.v4.view;

import android.os.Build;
import android.view.MotionEvent;

public class ai {

    /* renamed from: a  reason: collision with root package name */
    static final al f58a;

    static {
        if (Build.VERSION.SDK_INT >= 5) {
            f58a = new ak();
        } else {
            f58a = new aj();
        }
    }

    public static int a(MotionEvent motionEvent) {
        return motionEvent.getAction() & 255;
    }

    public static int a(MotionEvent motionEvent, int i) {
        return f58a.a(motionEvent, i);
    }

    public static int b(MotionEvent motionEvent) {
        return (motionEvent.getAction() & 65280) >> 8;
    }

    public static int b(MotionEvent motionEvent, int i) {
        return f58a.b(motionEvent, i);
    }

    public static float c(MotionEvent motionEvent, int i) {
        return f58a.c(motionEvent, i);
    }

    public static int c(MotionEvent motionEvent) {
        return f58a.a(motionEvent);
    }

    public static float d(MotionEvent motionEvent, int i) {
        return f58a.d(motionEvent, i);
    }
}
