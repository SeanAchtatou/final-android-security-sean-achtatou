package android.support.v4.view;

import android.animation.ValueAnimator;
import android.view.View;

final class cu implements ValueAnimator.AnimatorUpdateListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ cx f74a;
    final /* synthetic */ View b;

    cu(cx cxVar, View view) {
        this.f74a = cxVar;
        this.b = view;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f74a.a(this.b);
    }
}
