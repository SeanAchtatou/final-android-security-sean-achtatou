package android.support.v4.view;

import android.view.View;
import android.view.animation.Interpolator;
import java.util.WeakHashMap;

class cj extends ch {
    WeakHashMap b = null;

    cj() {
    }

    public void a(cf cfVar, View view) {
        cp.a(view);
    }

    public void a(cf cfVar, View view, float f) {
        cp.a(view, f);
    }

    public void a(cf cfVar, View view, long j) {
        cp.a(view, j);
    }

    public void a(cf cfVar, View view, cv cvVar) {
        view.setTag(2113929216, cvVar);
        cp.a(view, new ck(cfVar));
    }

    public void a(cf cfVar, View view, Interpolator interpolator) {
        cp.a(view, interpolator);
    }

    public void b(cf cfVar, View view) {
        cp.b(view);
    }

    public void b(cf cfVar, View view, float f) {
        cp.b(view, f);
    }

    public void c(cf cfVar, View view, float f) {
        cp.c(view, f);
    }

    public void d(cf cfVar, View view, float f) {
        cp.d(view, f);
    }
}
