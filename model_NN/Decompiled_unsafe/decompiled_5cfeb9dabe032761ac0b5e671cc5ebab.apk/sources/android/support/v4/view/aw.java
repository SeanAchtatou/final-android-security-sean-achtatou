package android.support.v4.view;

import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.view.View;
import java.util.WeakHashMap;

class aw implements be {

    /* renamed from: a  reason: collision with root package name */
    WeakHashMap f62a = null;

    aw() {
    }

    public int a(int i, int i2, int i3) {
        return View.resolveSize(i, i2);
    }

    /* access modifiers changed from: package-private */
    public long a() {
        return 10;
    }

    public void a(View view) {
    }

    public void a(View view, float f) {
    }

    public void a(View view, int i, int i2, int i3, int i4) {
        view.invalidate(i, i2, i3, i4);
    }

    public void a(View view, int i, Paint paint) {
    }

    public void a(View view, Paint paint) {
    }

    public void a(View view, a aVar) {
    }

    public void a(View view, an anVar) {
    }

    public void a(View view, Runnable runnable) {
        view.postDelayed(runnable, a());
    }

    public void a(View view, Runnable runnable, long j) {
        view.postDelayed(runnable, a() + j);
    }

    public boolean a(View view, int i) {
        return false;
    }

    public int b(View view) {
        return 2;
    }

    public void b(View view, float f) {
    }

    public void b(View view, int i) {
    }

    public void c(View view) {
        view.invalidate();
    }

    public void c(View view, float f) {
    }

    public int d(View view) {
        return 0;
    }

    public void d(View view, float f) {
    }

    public int e(View view) {
        return 0;
    }

    public void e(View view, float f) {
    }

    public boolean f(View view) {
        Drawable background = view.getBackground();
        return background != null && background.getOpacity() == -1;
    }

    public int g(View view) {
        return 0;
    }

    public float h(View view) {
        return 0.0f;
    }

    public int i(View view) {
        return 0;
    }

    public cf j(View view) {
        return new cf(view);
    }

    public int k(View view) {
        return 0;
    }

    public boolean l(View view) {
        return false;
    }

    public void m(View view) {
    }
}
