package android.support.v4.view;

import android.os.Build;
import android.support.v4.c.a.b;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

public class aa {

    /* renamed from: a  reason: collision with root package name */
    static final ae f57a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 14) {
            f57a = new ad();
        } else if (i >= 11) {
            f57a = new ac();
        } else {
            f57a = new ab();
        }
    }

    public static MenuItem a(MenuItem menuItem, g gVar) {
        if (menuItem instanceof b) {
            return ((b) menuItem).a(gVar);
        }
        Log.w("MenuItemCompat", "setActionProvider: item does not implement SupportMenuItem; ignoring");
        return menuItem;
    }

    public static MenuItem a(MenuItem menuItem, View view) {
        return menuItem instanceof b ? ((b) menuItem).setActionView(view) : f57a.a(menuItem, view);
    }

    public static View a(MenuItem menuItem) {
        return menuItem instanceof b ? ((b) menuItem).getActionView() : f57a.a(menuItem);
    }

    public static void a(MenuItem menuItem, int i) {
        if (menuItem instanceof b) {
            ((b) menuItem).setShowAsAction(i);
        } else {
            f57a.a(menuItem, i);
        }
    }

    public static MenuItem b(MenuItem menuItem, int i) {
        return menuItem instanceof b ? ((b) menuItem).setActionView(i) : f57a.b(menuItem, i);
    }

    public static boolean b(MenuItem menuItem) {
        return menuItem instanceof b ? ((b) menuItem).expandActionView() : f57a.b(menuItem);
    }

    public static boolean c(MenuItem menuItem) {
        return menuItem instanceof b ? ((b) menuItem).isActionViewExpanded() : f57a.c(menuItem);
    }
}
