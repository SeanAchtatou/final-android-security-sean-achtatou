package android.support.v4.view;

import android.view.VelocityTracker;

class aq implements as {
    aq() {
    }

    public float a(VelocityTracker velocityTracker, int i) {
        return velocityTracker.getXVelocity();
    }

    public float b(VelocityTracker velocityTracker, int i) {
        return velocityTracker.getYVelocity();
    }
}
