package android.support.v4.view;

import android.os.Build;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;

public class cf {

    /* renamed from: a  reason: collision with root package name */
    static final co f68a;
    private WeakReference b;
    /* access modifiers changed from: private */
    public Runnable c = null;
    /* access modifiers changed from: private */
    public Runnable d = null;
    /* access modifiers changed from: private */
    public int e = -1;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19) {
            f68a = new cn();
        } else if (i >= 18) {
            f68a = new cl();
        } else if (i >= 16) {
            f68a = new cm();
        } else if (i >= 14) {
            f68a = new cj();
        } else {
            f68a = new ch();
        }
    }

    cf(View view) {
        this.b = new WeakReference(view);
    }

    public cf a(float f) {
        View view = (View) this.b.get();
        if (view != null) {
            f68a.a(this, view, f);
        }
        return this;
    }

    public cf a(long j) {
        View view = (View) this.b.get();
        if (view != null) {
            f68a.a(this, view, j);
        }
        return this;
    }

    public cf a(cv cvVar) {
        View view = (View) this.b.get();
        if (view != null) {
            f68a.a(this, view, cvVar);
        }
        return this;
    }

    public cf a(cx cxVar) {
        View view = (View) this.b.get();
        if (view != null) {
            f68a.a(this, view, cxVar);
        }
        return this;
    }

    public cf a(Interpolator interpolator) {
        View view = (View) this.b.get();
        if (view != null) {
            f68a.a(this, view, interpolator);
        }
        return this;
    }

    public void a() {
        View view = (View) this.b.get();
        if (view != null) {
            f68a.a(this, view);
        }
    }

    public cf b(float f) {
        View view = (View) this.b.get();
        if (view != null) {
            f68a.b(this, view, f);
        }
        return this;
    }

    public void b() {
        View view = (View) this.b.get();
        if (view != null) {
            f68a.b(this, view);
        }
    }

    public cf c(float f) {
        View view = (View) this.b.get();
        if (view != null) {
            f68a.c(this, view, f);
        }
        return this;
    }

    public cf d(float f) {
        View view = (View) this.b.get();
        if (view != null) {
            f68a.d(this, view, f);
        }
        return this;
    }
}
