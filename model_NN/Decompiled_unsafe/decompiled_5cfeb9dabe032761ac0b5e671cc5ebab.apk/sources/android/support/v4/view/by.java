package android.support.v4.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class by extends ViewGroup.LayoutParams {

    /* renamed from: a  reason: collision with root package name */
    public boolean f66a;
    public int b;
    float c = 0.0f;
    boolean d;
    int e;
    int f;

    public by() {
        super(-1, -1);
    }

    public by(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, ViewPager.f54a);
        this.b = obtainStyledAttributes.getInteger(0, 48);
        obtainStyledAttributes.recycle();
    }
}
