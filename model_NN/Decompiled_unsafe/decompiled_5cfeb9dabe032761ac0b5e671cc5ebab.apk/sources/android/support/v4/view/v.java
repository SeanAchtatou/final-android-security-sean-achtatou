package android.support.v4.view;

import android.os.Build;
import android.view.ViewGroup;

public class v {

    /* renamed from: a  reason: collision with root package name */
    static final w f79a;

    static {
        if (Build.VERSION.SDK_INT >= 17) {
            f79a = new y();
        } else {
            f79a = new x();
        }
    }

    public static int a(ViewGroup.MarginLayoutParams marginLayoutParams) {
        return f79a.a(marginLayoutParams);
    }

    public static int b(ViewGroup.MarginLayoutParams marginLayoutParams) {
        return f79a.b(marginLayoutParams);
    }
}
