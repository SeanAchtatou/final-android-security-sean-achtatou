package android.support.v4.view;

import android.view.View;
import android.view.WindowInsets;

final class bg implements View.OnApplyWindowInsetsListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ an f63a;

    bg(an anVar) {
        this.f63a = anVar;
    }

    public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
        return ((cz) this.f63a.a(view, new cz(windowInsets))).e();
    }
}
