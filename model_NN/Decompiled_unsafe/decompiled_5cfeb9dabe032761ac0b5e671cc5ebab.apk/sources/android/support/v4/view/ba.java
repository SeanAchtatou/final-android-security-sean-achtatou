package android.support.v4.view;

import android.view.View;
import java.util.WeakHashMap;

class ba extends az {
    static boolean b = false;

    ba() {
    }

    public void a(View view, a aVar) {
        bk.a(view, aVar.a());
    }

    public boolean a(View view, int i) {
        return bk.a(view, i);
    }

    public cf j(View view) {
        if (this.f62a == null) {
            this.f62a = new WeakHashMap();
        }
        cf cfVar = (cf) this.f62a.get(view);
        if (cfVar != null) {
            return cfVar;
        }
        cf cfVar2 = new cf(view);
        this.f62a.put(view, cfVar2);
        return cfVar2;
    }
}
