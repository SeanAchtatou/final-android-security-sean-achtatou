package android.support.v4.view;

import android.view.View;
import android.view.animation.Interpolator;

class cp {
    public static void a(View view) {
        view.animate().cancel();
    }

    public static void a(View view, float f) {
        view.animate().alpha(f);
    }

    public static void a(View view, long j) {
        view.animate().setDuration(j);
    }

    public static void a(View view, cv cvVar) {
        if (cvVar != null) {
            view.animate().setListener(new cq(cvVar, view));
        } else {
            view.animate().setListener(null);
        }
    }

    public static void a(View view, Interpolator interpolator) {
        view.animate().setInterpolator(interpolator);
    }

    public static void b(View view) {
        view.animate().start();
    }

    public static void b(View view, float f) {
        view.animate().translationX(f);
    }

    public static void c(View view, float f) {
        view.animate().translationY(f);
    }

    public static void d(View view, float f) {
        view.animate().scaleY(f);
    }
}
