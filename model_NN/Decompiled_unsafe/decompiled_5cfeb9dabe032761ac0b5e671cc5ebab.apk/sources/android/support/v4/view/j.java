package android.support.v4.view;

import android.os.Build;

public class j {

    /* renamed from: a  reason: collision with root package name */
    static final k f77a;

    static {
        if (Build.VERSION.SDK_INT >= 17) {
            f77a = new m();
        } else {
            f77a = new l();
        }
    }

    public static int a(int i, int i2) {
        return f77a.a(i, i2);
    }
}
