package android.support.v4.view;

import android.os.Build;
import android.view.VelocityTracker;

public class ap {

    /* renamed from: a  reason: collision with root package name */
    static final as f60a;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            f60a = new ar();
        } else {
            f60a = new aq();
        }
    }

    public static float a(VelocityTracker velocityTracker, int i) {
        return f60a.a(velocityTracker, i);
    }

    public static float b(VelocityTracker velocityTracker, int i) {
        return f60a.b(velocityTracker, i);
    }
}
