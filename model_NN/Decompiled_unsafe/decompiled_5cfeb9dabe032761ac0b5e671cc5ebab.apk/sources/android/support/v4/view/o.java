package android.support.v4.view;

import android.os.Build;
import android.view.KeyEvent;

public class o {

    /* renamed from: a  reason: collision with root package name */
    static final s f78a;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            f78a = new r();
        } else {
            f78a = new p();
        }
    }

    public static boolean a(KeyEvent keyEvent) {
        return f78a.b(keyEvent.getMetaState());
    }

    public static boolean a(KeyEvent keyEvent, int i) {
        return f78a.a(keyEvent.getMetaState(), i);
    }

    public static void b(KeyEvent keyEvent) {
        f78a.a(keyEvent);
    }
}
