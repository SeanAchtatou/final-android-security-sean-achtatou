package android.support.v4.view;

import android.os.Build;
import android.view.ViewConfiguration;

public class bn {

    /* renamed from: a  reason: collision with root package name */
    static final bs f64a;

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            f64a = new br();
        } else if (Build.VERSION.SDK_INT >= 11) {
            f64a = new bq();
        } else if (Build.VERSION.SDK_INT >= 8) {
            f64a = new bp();
        } else {
            f64a = new bo();
        }
    }

    public static boolean a(ViewConfiguration viewConfiguration) {
        return f64a.a(viewConfiguration);
    }
}
