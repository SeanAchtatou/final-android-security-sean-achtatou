package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;

class az extends ay {
    az() {
    }

    public int a(int i, int i2, int i3) {
        return bj.a(i, i2, i3);
    }

    /* access modifiers changed from: package-private */
    public long a() {
        return bj.a();
    }

    public void a(View view, int i, Paint paint) {
        bj.a(view, i, paint);
    }

    public void a(View view, Paint paint) {
        a(view, d(view), paint);
        view.invalidate();
    }

    public void b(View view, float f) {
        bj.a(view, f);
    }

    public void c(View view, float f) {
        bj.b(view, f);
    }

    public int d(View view) {
        return bj.a(view);
    }

    public void d(View view, float f) {
        bj.c(view, f);
    }

    public void e(View view, float f) {
        bj.d(view, f);
    }

    public int g(View view) {
        return bj.b(view);
    }

    public float h(View view) {
        return bj.c(view);
    }

    public void m(View view) {
        bj.d(view);
    }
}
