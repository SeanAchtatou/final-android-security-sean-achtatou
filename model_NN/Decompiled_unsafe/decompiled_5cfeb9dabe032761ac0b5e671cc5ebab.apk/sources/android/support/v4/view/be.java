package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;

interface be {
    int a(int i, int i2, int i3);

    void a(View view);

    void a(View view, float f);

    void a(View view, int i, int i2, int i3, int i4);

    void a(View view, int i, Paint paint);

    void a(View view, Paint paint);

    void a(View view, a aVar);

    void a(View view, an anVar);

    void a(View view, Runnable runnable);

    void a(View view, Runnable runnable, long j);

    boolean a(View view, int i);

    int b(View view);

    void b(View view, float f);

    void b(View view, int i);

    void c(View view);

    void c(View view, float f);

    int d(View view);

    void d(View view, float f);

    int e(View view);

    void e(View view, float f);

    boolean f(View view);

    int g(View view);

    float h(View view);

    int i(View view);

    cf j(View view);

    int k(View view);

    boolean l(View view);

    void m(View view);
}
