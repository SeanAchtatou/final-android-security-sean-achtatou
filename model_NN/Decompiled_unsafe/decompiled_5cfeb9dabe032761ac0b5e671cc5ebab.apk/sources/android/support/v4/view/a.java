package android.support.v4.view;

import android.os.Build;

public class a {
    private static final c b;
    private static final Object c = b.a();

    /* renamed from: a  reason: collision with root package name */
    final Object f56a;

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            b = new d();
        } else if (Build.VERSION.SDK_INT >= 14) {
            b = new b();
        } else {
            b = new e();
        }
    }

    /* access modifiers changed from: package-private */
    public Object a() {
        return this.f56a;
    }
}
