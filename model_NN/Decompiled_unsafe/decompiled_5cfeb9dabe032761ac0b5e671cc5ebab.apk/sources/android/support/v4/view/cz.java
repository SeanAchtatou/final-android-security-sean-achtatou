package android.support.v4.view;

import android.view.WindowInsets;

class cz extends cy {

    /* renamed from: a  reason: collision with root package name */
    private final WindowInsets f75a;

    cz(WindowInsets windowInsets) {
        this.f75a = windowInsets;
    }

    public int a() {
        return this.f75a.getSystemWindowInsetLeft();
    }

    public cy a(int i, int i2, int i3, int i4) {
        return new cz(this.f75a.replaceSystemWindowInsets(i, i2, i3, i4));
    }

    public int b() {
        return this.f75a.getSystemWindowInsetTop();
    }

    public int c() {
        return this.f75a.getSystemWindowInsetRight();
    }

    public int d() {
        return this.f75a.getSystemWindowInsetBottom();
    }

    /* access modifiers changed from: package-private */
    public WindowInsets e() {
        return this.f75a;
    }
}
