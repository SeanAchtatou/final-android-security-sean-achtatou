package android.support.v4.view;

import android.view.View;
import java.util.Comparator;

class ce implements Comparator {
    ce() {
    }

    /* renamed from: a */
    public int compare(View view, View view2) {
        by byVar = (by) view.getLayoutParams();
        by byVar2 = (by) view2.getLayoutParams();
        return byVar.f66a != byVar2.f66a ? byVar.f66a ? 1 : -1 : byVar.e - byVar2.e;
    }
}
