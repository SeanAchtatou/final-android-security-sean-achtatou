package android.support.v4.view;

import android.graphics.Paint;
import android.os.Build;
import android.view.View;

public class au {

    /* renamed from: a  reason: collision with root package name */
    static final be f61a;

    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            f61a = new av();
        } else if (i >= 19) {
            f61a = new bd();
        } else if (i >= 17) {
            f61a = new bc();
        } else if (i >= 16) {
            f61a = new bb();
        } else if (i >= 14) {
            f61a = new ba();
        } else if (i >= 11) {
            f61a = new az();
        } else if (i >= 9) {
            f61a = new ay();
        } else if (i >= 7) {
            f61a = new ax();
        } else {
            f61a = new aw();
        }
    }

    public static int a(int i, int i2, int i3) {
        return f61a.a(i, i2, i3);
    }

    public static int a(View view) {
        return f61a.b(view);
    }

    public static void a(View view, float f) {
        f61a.b(view, f);
    }

    public static void a(View view, int i, int i2, int i3, int i4) {
        f61a.a(view, i, i2, i3, i4);
    }

    public static void a(View view, int i, Paint paint) {
        f61a.a(view, i, paint);
    }

    public static void a(View view, Paint paint) {
        f61a.a(view, paint);
    }

    public static void a(View view, a aVar) {
        f61a.a(view, aVar);
    }

    public static void a(View view, an anVar) {
        f61a.a(view, anVar);
    }

    public static void a(View view, Runnable runnable) {
        f61a.a(view, runnable);
    }

    public static void a(View view, Runnable runnable, long j) {
        f61a.a(view, runnable, j);
    }

    public static boolean a(View view, int i) {
        return f61a.a(view, i);
    }

    public static void b(View view) {
        f61a.c(view);
    }

    public static void b(View view, float f) {
        f61a.c(view, f);
    }

    public static void b(View view, int i) {
        f61a.b(view, i);
    }

    public static int c(View view) {
        return f61a.d(view);
    }

    public static void c(View view, float f) {
        f61a.d(view, f);
    }

    public static int d(View view) {
        return f61a.e(view);
    }

    public static void d(View view, float f) {
        f61a.e(view, f);
    }

    public static void e(View view, float f) {
        f61a.a(view, f);
    }

    public static boolean e(View view) {
        return f61a.f(view);
    }

    public static int f(View view) {
        return f61a.g(view);
    }

    public static float g(View view) {
        return f61a.h(view);
    }

    public static int h(View view) {
        return f61a.i(view);
    }

    public static cf i(View view) {
        return f61a.j(view);
    }

    public static int j(View view) {
        return f61a.k(view);
    }

    public static void k(View view) {
        f61a.a(view);
    }

    public static boolean l(View view) {
        return f61a.l(view);
    }

    public static void m(View view) {
        f61a.m(view);
    }
}
