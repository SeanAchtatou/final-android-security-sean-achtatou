package android.support.v4.view;

import android.graphics.Paint;
import android.view.View;

class ck implements cv {

    /* renamed from: a  reason: collision with root package name */
    cf f71a;

    ck(cf cfVar) {
        this.f71a = cfVar;
    }

    public void a(View view) {
        if (this.f71a.e >= 0) {
            au.a(view, 2, (Paint) null);
        }
        if (this.f71a.c != null) {
            this.f71a.c.run();
        }
        Object tag = view.getTag(2113929216);
        cv cvVar = tag instanceof cv ? (cv) tag : null;
        if (cvVar != null) {
            cvVar.a(view);
        }
    }

    public void b(View view) {
        if (this.f71a.e >= 0) {
            au.a(view, this.f71a.e, (Paint) null);
            int unused = this.f71a.e = -1;
        }
        if (this.f71a.d != null) {
            this.f71a.d.run();
        }
        Object tag = view.getTag(2113929216);
        cv cvVar = tag instanceof cv ? (cv) tag : null;
        if (cvVar != null) {
            cvVar.b(view);
        }
    }

    public void c(View view) {
        Object tag = view.getTag(2113929216);
        cv cvVar = tag instanceof cv ? (cv) tag : null;
        if (cvVar != null) {
            cvVar.c(view);
        }
    }
}
