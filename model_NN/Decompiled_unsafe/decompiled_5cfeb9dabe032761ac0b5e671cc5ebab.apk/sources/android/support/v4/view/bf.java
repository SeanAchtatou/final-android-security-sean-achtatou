package android.support.v4.view;

import android.view.View;

class bf {
    public static void a(View view) {
        view.requestApplyInsets();
    }

    public static void a(View view, float f) {
        view.setElevation(f);
    }

    public static void a(View view, an anVar) {
        view.setOnApplyWindowInsetsListener(new bg(anVar));
    }
}
