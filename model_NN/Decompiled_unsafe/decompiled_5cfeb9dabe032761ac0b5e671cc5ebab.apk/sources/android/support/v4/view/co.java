package android.support.v4.view;

import android.view.View;
import android.view.animation.Interpolator;

interface co {
    void a(cf cfVar, View view);

    void a(cf cfVar, View view, float f);

    void a(cf cfVar, View view, long j);

    void a(cf cfVar, View view, cv cvVar);

    void a(cf cfVar, View view, cx cxVar);

    void a(cf cfVar, View view, Interpolator interpolator);

    void b(cf cfVar, View view);

    void b(cf cfVar, View view, float f);

    void c(cf cfVar, View view, float f);

    void d(cf cfVar, View view, float f);
}
