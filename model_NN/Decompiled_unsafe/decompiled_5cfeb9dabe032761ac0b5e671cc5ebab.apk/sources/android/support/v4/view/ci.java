package android.support.v4.view;

import android.view.View;
import java.lang.ref.WeakReference;

class ci implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    WeakReference f70a;
    cf b;
    final /* synthetic */ ch c;

    private ci(ch chVar, cf cfVar, View view) {
        this.c = chVar;
        this.f70a = new WeakReference(view);
        this.b = cfVar;
    }

    public void run() {
        this.c.c(this.b, (View) this.f70a.get());
    }
}
