package android.support.v4.f;

import java.util.Collection;
import java.util.Iterator;

final class l implements Collection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f48a;

    l(g gVar) {
        this.f48a = gVar;
    }

    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        this.f48a.c();
    }

    public boolean contains(Object obj) {
        return this.f48a.b(obj) >= 0;
    }

    public boolean containsAll(Collection collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    public boolean isEmpty() {
        return this.f48a.a() == 0;
    }

    public Iterator iterator() {
        return new h(this.f48a, 1);
    }

    public boolean remove(Object obj) {
        int b = this.f48a.b(obj);
        if (b < 0) {
            return false;
        }
        this.f48a.a(b);
        return true;
    }

    public boolean removeAll(Collection collection) {
        int i = 0;
        int a2 = this.f48a.a();
        boolean z = false;
        while (i < a2) {
            if (collection.contains(this.f48a.a(i, 1))) {
                this.f48a.a(i);
                i--;
                a2--;
                z = true;
            }
            i++;
        }
        return z;
    }

    public boolean retainAll(Collection collection) {
        int i = 0;
        int a2 = this.f48a.a();
        boolean z = false;
        while (i < a2) {
            if (!collection.contains(this.f48a.a(i, 1))) {
                this.f48a.a(i);
                i--;
                a2--;
                z = true;
            }
            i++;
        }
        return z;
    }

    public int size() {
        return this.f48a.a();
    }

    public Object[] toArray() {
        return this.f48a.b(1);
    }

    public Object[] toArray(Object[] objArr) {
        return this.f48a.a(objArr, 1);
    }
}
