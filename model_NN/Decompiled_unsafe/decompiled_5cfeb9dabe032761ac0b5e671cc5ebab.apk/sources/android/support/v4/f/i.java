package android.support.v4.f;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

final class i implements Set {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f45a;

    i(g gVar) {
        this.f45a = gVar;
    }

    /* renamed from: a */
    public boolean add(Map.Entry entry) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection collection) {
        int a2 = this.f45a.a();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            this.f45a.a(entry.getKey(), entry.getValue());
        }
        return a2 != this.f45a.a();
    }

    public void clear() {
        this.f45a.c();
    }

    public boolean contains(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        int a2 = this.f45a.a(entry.getKey());
        if (a2 >= 0) {
            return c.a(this.f45a.a(a2, 1), entry.getValue());
        }
        return false;
    }

    public boolean containsAll(Collection collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.f.g.a(java.util.Set, java.lang.Object):boolean
     arg types: [android.support.v4.f.i, java.lang.Object]
     candidates:
      android.support.v4.f.g.a(java.util.Map, java.util.Collection):boolean
      android.support.v4.f.g.a(int, int):java.lang.Object
      android.support.v4.f.g.a(int, java.lang.Object):java.lang.Object
      android.support.v4.f.g.a(java.lang.Object, java.lang.Object):void
      android.support.v4.f.g.a(java.lang.Object[], int):java.lang.Object[]
      android.support.v4.f.g.a(java.util.Set, java.lang.Object):boolean */
    public boolean equals(Object obj) {
        return g.a((Set) this, obj);
    }

    public int hashCode() {
        int a2 = this.f45a.a() - 1;
        int i = 0;
        while (a2 >= 0) {
            Object a3 = this.f45a.a(a2, 0);
            Object a4 = this.f45a.a(a2, 1);
            a2--;
            i += (a4 == null ? 0 : a4.hashCode()) ^ (a3 == null ? 0 : a3.hashCode());
        }
        return i;
    }

    public boolean isEmpty() {
        return this.f45a.a() == 0;
    }

    public Iterator iterator() {
        return new k(this.f45a);
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public int size() {
        return this.f45a.a();
    }

    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    public Object[] toArray(Object[] objArr) {
        throw new UnsupportedOperationException();
    }
}
