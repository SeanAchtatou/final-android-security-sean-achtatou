package android.support.v4.f;

import java.util.Iterator;
import java.util.Map;

final class k implements Iterator, Map.Entry {

    /* renamed from: a  reason: collision with root package name */
    int f47a;
    int b;
    boolean c = false;
    final /* synthetic */ g d;

    k(g gVar) {
        this.d = gVar;
        this.f47a = gVar.a() - 1;
        this.b = -1;
    }

    /* renamed from: a */
    public Map.Entry next() {
        this.b++;
        this.c = true;
        return this;
    }

    public final boolean equals(Object obj) {
        boolean z = true;
        if (!this.c) {
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        } else if (!(obj instanceof Map.Entry)) {
            return false;
        } else {
            Map.Entry entry = (Map.Entry) obj;
            if (!c.a(entry.getKey(), this.d.a(this.b, 0)) || !c.a(entry.getValue(), this.d.a(this.b, 1))) {
                z = false;
            }
            return z;
        }
    }

    public Object getKey() {
        if (this.c) {
            return this.d.a(this.b, 0);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public Object getValue() {
        if (this.c) {
            return this.d.a(this.b, 1);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public boolean hasNext() {
        return this.b < this.f47a;
    }

    public final int hashCode() {
        int i = 0;
        if (!this.c) {
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }
        Object a2 = this.d.a(this.b, 0);
        Object a3 = this.d.a(this.b, 1);
        int hashCode = a2 == null ? 0 : a2.hashCode();
        if (a3 != null) {
            i = a3.hashCode();
        }
        return i ^ hashCode;
    }

    public void remove() {
        if (!this.c) {
            throw new IllegalStateException();
        }
        this.d.a(this.b);
        this.b--;
        this.f47a--;
        this.c = false;
    }

    public Object setValue(Object obj) {
        if (this.c) {
            return this.d.a(this.b, obj);
        }
        throw new IllegalStateException("This container does not support retaining Map.Entry objects");
    }

    public final String toString() {
        return getKey() + "=" + getValue();
    }
}
