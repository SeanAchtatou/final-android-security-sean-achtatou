package android.support.v4.f;

import java.util.Iterator;

final class h implements Iterator {

    /* renamed from: a  reason: collision with root package name */
    final int f44a;
    int b;
    int c;
    boolean d = false;
    final /* synthetic */ g e;

    h(g gVar, int i) {
        this.e = gVar;
        this.f44a = i;
        this.b = gVar.a();
    }

    public boolean hasNext() {
        return this.c < this.b;
    }

    public Object next() {
        Object a2 = this.e.a(this.c, this.f44a);
        this.c++;
        this.d = true;
        return a2;
    }

    public void remove() {
        if (!this.d) {
            throw new IllegalStateException();
        }
        this.c--;
        this.b--;
        this.d = false;
        this.e.a(this.c);
    }
}
