package android.support.v4.f;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

final class j implements Set {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f46a;

    j(g gVar) {
        this.f46a = gVar;
    }

    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        this.f46a.c();
    }

    public boolean contains(Object obj) {
        return this.f46a.a(obj) >= 0;
    }

    public boolean containsAll(Collection collection) {
        return g.a(this.f46a.b(), collection);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.f.g.a(java.util.Set, java.lang.Object):boolean
     arg types: [android.support.v4.f.j, java.lang.Object]
     candidates:
      android.support.v4.f.g.a(java.util.Map, java.util.Collection):boolean
      android.support.v4.f.g.a(int, int):java.lang.Object
      android.support.v4.f.g.a(int, java.lang.Object):java.lang.Object
      android.support.v4.f.g.a(java.lang.Object, java.lang.Object):void
      android.support.v4.f.g.a(java.lang.Object[], int):java.lang.Object[]
      android.support.v4.f.g.a(java.util.Set, java.lang.Object):boolean */
    public boolean equals(Object obj) {
        return g.a((Set) this, obj);
    }

    public int hashCode() {
        int i = 0;
        for (int a2 = this.f46a.a() - 1; a2 >= 0; a2--) {
            Object a3 = this.f46a.a(a2, 0);
            i += a3 == null ? 0 : a3.hashCode();
        }
        return i;
    }

    public boolean isEmpty() {
        return this.f46a.a() == 0;
    }

    public Iterator iterator() {
        return new h(this.f46a, 0);
    }

    public boolean remove(Object obj) {
        int a2 = this.f46a.a(obj);
        if (a2 < 0) {
            return false;
        }
        this.f46a.a(a2);
        return true;
    }

    public boolean removeAll(Collection collection) {
        return g.b(this.f46a.b(), collection);
    }

    public boolean retainAll(Collection collection) {
        return g.c(this.f46a.b(), collection);
    }

    public int size() {
        return this.f46a.a();
    }

    public Object[] toArray() {
        return this.f46a.b(0);
    }

    public Object[] toArray(Object[] objArr) {
        return this.f46a.a(objArr, 0);
    }
}
