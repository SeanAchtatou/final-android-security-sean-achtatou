package android.support.v4.f;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class a extends m implements Map {

    /* renamed from: a  reason: collision with root package name */
    g f39a;

    private g b() {
        if (this.f39a == null) {
            this.f39a = new b(this);
        }
        return this.f39a;
    }

    public boolean a(Collection collection) {
        return g.c(this, collection);
    }

    public Set entrySet() {
        return b().d();
    }

    public Set keySet() {
        return b().e();
    }

    public void putAll(Map map) {
        a(this.h + map.size());
        for (Map.Entry entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    public Collection values() {
        return b().f();
    }
}
