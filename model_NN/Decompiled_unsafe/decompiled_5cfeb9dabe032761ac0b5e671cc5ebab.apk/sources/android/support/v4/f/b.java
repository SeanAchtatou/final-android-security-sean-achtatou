package android.support.v4.f;

import java.util.Map;

class b extends g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f40a;

    b(a aVar) {
        this.f40a = aVar;
    }

    /* access modifiers changed from: protected */
    public int a() {
        return this.f40a.h;
    }

    /* access modifiers changed from: protected */
    public int a(Object obj) {
        return this.f40a.a(obj);
    }

    /* access modifiers changed from: protected */
    public Object a(int i, int i2) {
        return this.f40a.g[(i << 1) + i2];
    }

    /* access modifiers changed from: protected */
    public Object a(int i, Object obj) {
        return this.f40a.a(i, obj);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        this.f40a.d(i);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, Object obj2) {
        this.f40a.put(obj, obj2);
    }

    /* access modifiers changed from: protected */
    public int b(Object obj) {
        return this.f40a.b(obj);
    }

    /* access modifiers changed from: protected */
    public Map b() {
        return this.f40a;
    }

    /* access modifiers changed from: protected */
    public void c() {
        this.f40a.clear();
    }
}
