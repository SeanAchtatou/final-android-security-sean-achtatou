package android.support.v4.app;

import android.view.animation.Animation;

class v implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Fragment f31a;
    final /* synthetic */ t b;

    v(t tVar, Fragment fragment) {
        this.b = tVar;
        this.f31a = fragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.t.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void onAnimationEnd(Animation animation) {
        if (this.f31a.c != null) {
            this.f31a.c = null;
            this.b.a(this.f31a, this.f31a.d, 0, 0, false);
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
