package android.support.v4.app;

import android.support.v4.f.d;
import android.support.v4.f.n;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;

class am extends ak {

    /* renamed from: a  reason: collision with root package name */
    static boolean f16a = false;
    final n b = new n();
    final n c = new n();
    final String d;
    o e;
    boolean f;
    boolean g;

    am(String str, o oVar, boolean z) {
        this.d = str;
        this.e = oVar;
        this.f = z;
    }

    /* access modifiers changed from: package-private */
    public void a(o oVar) {
        this.e = oVar;
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (this.b.b() > 0) {
            printWriter.print(str);
            printWriter.println("Active Loaders:");
            String str2 = str + "    ";
            for (int i = 0; i < this.b.b(); i++) {
                an anVar = (an) this.b.b(i);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.b.a(i));
                printWriter.print(": ");
                printWriter.println(anVar.toString());
                anVar.a(str2, fileDescriptor, printWriter, strArr);
            }
        }
        if (this.c.b() > 0) {
            printWriter.print(str);
            printWriter.println("Inactive Loaders:");
            String str3 = str + "    ";
            for (int i2 = 0; i2 < this.c.b(); i2++) {
                an anVar2 = (an) this.c.b(i2);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(this.c.a(i2));
                printWriter.print(": ");
                printWriter.println(anVar2.toString());
                anVar2.a(str3, fileDescriptor, printWriter, strArr);
            }
        }
    }

    public boolean a() {
        int b2 = this.b.b();
        boolean z = false;
        for (int i = 0; i < b2; i++) {
            an anVar = (an) this.b.b(i);
            z |= anVar.h && !anVar.f;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (f16a) {
            Log.v("LoaderManager", "Starting in " + this);
        }
        if (this.f) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStart when already started: " + this, runtimeException);
            return;
        }
        this.f = true;
        for (int b2 = this.b.b() - 1; b2 >= 0; b2--) {
            ((an) this.b.b(b2)).a();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (f16a) {
            Log.v("LoaderManager", "Stopping in " + this);
        }
        if (!this.f) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doStop when not started: " + this, runtimeException);
            return;
        }
        for (int b2 = this.b.b() - 1; b2 >= 0; b2--) {
            ((an) this.b.b(b2)).e();
        }
        this.f = false;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (f16a) {
            Log.v("LoaderManager", "Retaining in " + this);
        }
        if (!this.f) {
            RuntimeException runtimeException = new RuntimeException("here");
            runtimeException.fillInStackTrace();
            Log.w("LoaderManager", "Called doRetain when not started: " + this, runtimeException);
            return;
        }
        this.g = true;
        this.f = false;
        for (int b2 = this.b.b() - 1; b2 >= 0; b2--) {
            ((an) this.b.b(b2)).b();
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (this.g) {
            if (f16a) {
                Log.v("LoaderManager", "Finished Retaining in " + this);
            }
            this.g = false;
            for (int b2 = this.b.b() - 1; b2 >= 0; b2--) {
                ((an) this.b.b(b2)).c();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        for (int b2 = this.b.b() - 1; b2 >= 0; b2--) {
            ((an) this.b.b(b2)).k = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        for (int b2 = this.b.b() - 1; b2 >= 0; b2--) {
            ((an) this.b.b(b2)).d();
        }
    }

    /* access modifiers changed from: package-private */
    public void h() {
        if (!this.g) {
            if (f16a) {
                Log.v("LoaderManager", "Destroying Active in " + this);
            }
            for (int b2 = this.b.b() - 1; b2 >= 0; b2--) {
                ((an) this.b.b(b2)).f();
            }
            this.b.c();
        }
        if (f16a) {
            Log.v("LoaderManager", "Destroying Inactive in " + this);
        }
        for (int b3 = this.c.b() - 1; b3 >= 0; b3--) {
            ((an) this.c.b(b3)).f();
        }
        this.c.c();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        d.a(this.e, sb);
        sb.append("}}");
        return sb.toString();
    }
}
