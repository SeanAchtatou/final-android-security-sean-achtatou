package android.support.v4.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;

class ar extends aq {
    ar() {
    }

    public Intent a(Activity activity) {
        Intent a2 = as.a(activity);
        return a2 == null ? b(activity) : a2;
    }

    public String a(Context context, ActivityInfo activityInfo) {
        String a2 = as.a(activityInfo);
        return a2 == null ? super.a(context, activityInfo) : a2;
    }

    public boolean a(Activity activity, Intent intent) {
        return as.a(activity, intent);
    }

    /* access modifiers changed from: package-private */
    public Intent b(Activity activity) {
        return super.a(activity);
    }

    public void b(Activity activity, Intent intent) {
        as.b(activity, intent);
    }
}
