package android.support.v4.app;

import android.view.View;
import android.view.ViewTreeObserver;

class h implements ViewTreeObserver.OnPreDrawListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f23a;
    final /* synthetic */ j b;
    final /* synthetic */ int c;
    final /* synthetic */ Object d;
    final /* synthetic */ e e;

    h(e eVar, View view, j jVar, int i, Object obj) {
        this.e = eVar;
        this.f23a = view;
        this.b = jVar;
        this.c = i;
        this.d = obj;
    }

    public boolean onPreDraw() {
        this.f23a.getViewTreeObserver().removeOnPreDrawListener(this);
        this.e.a(this.b, this.c, this.d);
        return true;
    }
}
