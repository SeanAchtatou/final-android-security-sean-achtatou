package android.support.v4.app;

import android.view.View;

class l implements q {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Fragment f26a;

    l(Fragment fragment) {
        this.f26a = fragment;
    }

    public View a(int i) {
        if (this.f26a.J != null) {
            return this.f26a.J.findViewById(i);
        }
        throw new IllegalStateException("Fragment does not have a view");
    }

    public boolean a() {
        return this.f26a.J != null;
    }
}
