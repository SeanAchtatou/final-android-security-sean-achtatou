package android.support.v4.app;

import android.os.Build;
import android.support.v4.f.a;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

final class e extends ac implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final t f20a;
    i b;
    i c;
    int d;
    int e;
    int f;
    int g;
    int h;
    int i;
    int j;
    boolean k;
    boolean l = true;
    String m;
    boolean n;
    int o = -1;
    int p;
    CharSequence q;
    int r;
    CharSequence s;
    ArrayList t;
    ArrayList u;

    public e(t tVar) {
        this.f20a = tVar;
    }

    private j a(SparseArray sparseArray, SparseArray sparseArray2, boolean z) {
        j jVar = new j(this);
        jVar.d = new View(this.f20a.o);
        int i2 = 0;
        boolean z2 = false;
        while (i2 < sparseArray.size()) {
            boolean z3 = a(sparseArray.keyAt(i2), jVar, z, sparseArray, sparseArray2) ? true : z2;
            i2++;
            z2 = z3;
        }
        for (int i3 = 0; i3 < sparseArray2.size(); i3++) {
            int keyAt = sparseArray2.keyAt(i3);
            if (sparseArray.get(keyAt) == null && a(keyAt, jVar, z, sparseArray, sparseArray2)) {
                z2 = true;
            }
        }
        if (!z2) {
            return null;
        }
        return jVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.ad.a(java.util.Map, android.view.View):void
     arg types: [android.support.v4.f.a, android.view.View]
     candidates:
      android.support.v4.app.ad.a(android.transition.Transition, android.support.v4.app.ai):void
      android.support.v4.app.ad.a(android.view.ViewGroup, java.lang.Object):void
      android.support.v4.app.ad.a(java.lang.Object, android.view.View):void
      android.support.v4.app.ad.a(java.lang.Object, java.util.ArrayList):void
      android.support.v4.app.ad.a(java.util.ArrayList, android.view.View):void
      android.support.v4.app.ad.a(java.util.Map, android.view.View):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.support.v4.app.j, android.support.v4.f.a, boolean):void
     arg types: [android.support.v4.app.j, android.support.v4.f.a, int]
     candidates:
      android.support.v4.app.e.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.j
      android.support.v4.app.e.a(android.support.v4.app.j, android.support.v4.app.Fragment, boolean):android.support.v4.f.a
      android.support.v4.app.e.a(android.support.v4.app.j, boolean, android.support.v4.app.Fragment):android.support.v4.f.a
      android.support.v4.app.e.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.f.a):android.support.v4.f.a
      android.support.v4.app.e.a(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean):java.lang.Object
      android.support.v4.app.e.a(android.support.v4.app.e, android.support.v4.f.a, android.support.v4.app.j):void
      android.support.v4.app.e.a(android.support.v4.app.j, int, java.lang.Object):void
      android.support.v4.app.e.a(android.support.v4.app.j, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.e.a(android.support.v4.f.a, java.lang.String, java.lang.String):void
      android.support.v4.app.e.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.ac
      android.support.v4.app.e.a(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.ac.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.ac
      android.support.v4.app.e.a(android.support.v4.app.j, android.support.v4.f.a, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.b(android.support.v4.app.j, android.support.v4.f.a, boolean):void
     arg types: [android.support.v4.app.j, android.support.v4.f.a, int]
     candidates:
      android.support.v4.app.e.b(android.support.v4.app.j, android.support.v4.app.Fragment, boolean):android.support.v4.f.a
      android.support.v4.app.e.b(android.support.v4.app.j, android.support.v4.f.a, boolean):void */
    private a a(j jVar, Fragment fragment, boolean z) {
        a aVar = new a();
        if (this.t != null) {
            ad.a((Map) aVar, fragment.h());
            if (z) {
                aVar.a((Collection) this.u);
            } else {
                aVar = a(this.t, this.u, aVar);
            }
        }
        if (z) {
            if (fragment.Y != null) {
                fragment.Y.a(this.u, aVar);
            }
            a(jVar, aVar, false);
        } else {
            if (fragment.Z != null) {
                fragment.Z.a(this.u, aVar);
            }
            b(jVar, aVar, false);
        }
        return aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.support.v4.app.j, android.support.v4.f.a, boolean):void
     arg types: [android.support.v4.app.j, android.support.v4.f.a, int]
     candidates:
      android.support.v4.app.e.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.j
      android.support.v4.app.e.a(android.support.v4.app.j, android.support.v4.app.Fragment, boolean):android.support.v4.f.a
      android.support.v4.app.e.a(android.support.v4.app.j, boolean, android.support.v4.app.Fragment):android.support.v4.f.a
      android.support.v4.app.e.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.f.a):android.support.v4.f.a
      android.support.v4.app.e.a(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean):java.lang.Object
      android.support.v4.app.e.a(android.support.v4.app.e, android.support.v4.f.a, android.support.v4.app.j):void
      android.support.v4.app.e.a(android.support.v4.app.j, int, java.lang.Object):void
      android.support.v4.app.e.a(android.support.v4.app.j, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.e.a(android.support.v4.f.a, java.lang.String, java.lang.String):void
      android.support.v4.app.e.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.ac
      android.support.v4.app.e.a(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.ac.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.ac
      android.support.v4.app.e.a(android.support.v4.app.j, android.support.v4.f.a, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.b(android.support.v4.app.j, android.support.v4.f.a, boolean):void
     arg types: [android.support.v4.app.j, android.support.v4.f.a, int]
     candidates:
      android.support.v4.app.e.b(android.support.v4.app.j, android.support.v4.app.Fragment, boolean):android.support.v4.f.a
      android.support.v4.app.e.b(android.support.v4.app.j, android.support.v4.f.a, boolean):void */
    /* access modifiers changed from: private */
    public a a(j jVar, boolean z, Fragment fragment) {
        a b2 = b(jVar, fragment, z);
        if (z) {
            if (fragment.Z != null) {
                fragment.Z.a(this.u, b2);
            }
            a(jVar, b2, true);
        } else {
            if (fragment.Y != null) {
                fragment.Y.a(this.u, b2);
            }
            b(jVar, b2, true);
        }
        return b2;
    }

    private static a a(ArrayList arrayList, ArrayList arrayList2, a aVar) {
        if (aVar.isEmpty()) {
            return aVar;
        }
        a aVar2 = new a();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            View view = (View) aVar.get(arrayList.get(i2));
            if (view != null) {
                aVar2.put(arrayList2.get(i2), view);
            }
        }
        return aVar2;
    }

    private static Object a(Fragment fragment, Fragment fragment2, boolean z) {
        if (fragment == null || fragment2 == null) {
            return null;
        }
        return ad.a(z ? fragment2.w() : fragment.v());
    }

    private static Object a(Fragment fragment, boolean z) {
        if (fragment == null) {
            return null;
        }
        return ad.a(z ? fragment.u() : fragment.r());
    }

    private static Object a(Object obj, Fragment fragment, ArrayList arrayList, a aVar) {
        return obj != null ? ad.a(obj, fragment.h(), arrayList, aVar) : obj;
    }

    private void a(int i2, Fragment fragment, String str, int i3) {
        fragment.t = this.f20a;
        if (str != null) {
            if (fragment.z == null || str.equals(fragment.z)) {
                fragment.z = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.z + " now " + str);
            }
        }
        if (i2 != 0) {
            if (fragment.x == 0 || fragment.x == i2) {
                fragment.x = i2;
                fragment.y = i2;
            } else {
                throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.x + " now " + i2);
            }
        }
        i iVar = new i();
        iVar.c = i3;
        iVar.d = fragment;
        a(iVar);
    }

    /* access modifiers changed from: private */
    public void a(j jVar, int i2, Object obj) {
        if (this.f20a.g != null) {
            for (int i3 = 0; i3 < this.f20a.g.size(); i3++) {
                Fragment fragment = (Fragment) this.f20a.g.get(i3);
                if (!(fragment.J == null || fragment.I == null || fragment.y != i2)) {
                    if (!fragment.A) {
                        ad.a(obj, fragment.J, false);
                        jVar.b.remove(fragment.J);
                    } else if (!jVar.b.contains(fragment.J)) {
                        ad.a(obj, fragment.J, true);
                        jVar.b.add(fragment.J);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(j jVar, Fragment fragment, Fragment fragment2, boolean z, a aVar) {
        au auVar = z ? fragment2.Y : fragment.Y;
        if (auVar != null) {
            auVar.b(new ArrayList(aVar.keySet()), new ArrayList(aVar.values()), null);
        }
    }

    private void a(j jVar, a aVar, boolean z) {
        int size = this.u == null ? 0 : this.u.size();
        for (int i2 = 0; i2 < size; i2++) {
            String str = (String) this.t.get(i2);
            View view = (View) aVar.get((String) this.u.get(i2));
            if (view != null) {
                String a2 = ad.a(view);
                if (z) {
                    a(jVar.f25a, str, a2);
                } else {
                    a(jVar.f25a, a2, str);
                }
            }
        }
    }

    private void a(j jVar, View view, Object obj, Fragment fragment, Fragment fragment2, boolean z, ArrayList arrayList) {
        view.getViewTreeObserver().addOnPreDrawListener(new g(this, view, obj, arrayList, jVar, z, fragment, fragment2));
    }

    private static void a(j jVar, ArrayList arrayList, ArrayList arrayList2) {
        if (arrayList != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < arrayList.size()) {
                    a(jVar.f25a, (String) arrayList.get(i3), (String) arrayList2.get(i3));
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(a aVar, j jVar) {
        View view;
        if (this.u != null && !aVar.isEmpty() && (view = (View) aVar.get(this.u.get(0))) != null) {
            jVar.c.f15a = view;
        }
    }

    private static void a(a aVar, String str, String str2) {
        if (str != null && str2 != null && !str.equals(str2)) {
            for (int i2 = 0; i2 < aVar.size(); i2++) {
                if (str.equals(aVar.c(i2))) {
                    aVar.a(i2, str2);
                    return;
                }
            }
            aVar.put(str, str2);
        }
    }

    private static void a(SparseArray sparseArray, Fragment fragment) {
        int i2;
        if (fragment != null && (i2 = fragment.y) != 0 && !fragment.g() && fragment.e() && fragment.h() != null && sparseArray.get(i2) == null) {
            sparseArray.put(i2, fragment);
        }
    }

    private void a(View view, j jVar, int i2, Object obj) {
        view.getViewTreeObserver().addOnPreDrawListener(new h(this, view, jVar, i2, obj));
    }

    private boolean a(int i2, j jVar, boolean z, SparseArray sparseArray, SparseArray sparseArray2) {
        View view;
        ViewGroup viewGroup = (ViewGroup) this.f20a.p.a(i2);
        if (viewGroup == null) {
            return false;
        }
        Fragment fragment = (Fragment) sparseArray2.get(i2);
        Fragment fragment2 = (Fragment) sparseArray.get(i2);
        Object a2 = a(fragment, z);
        Object a3 = a(fragment, fragment2, z);
        Object b2 = b(fragment2, z);
        if (a2 == null && a3 == null && b2 == null) {
            return false;
        }
        a aVar = null;
        ArrayList arrayList = new ArrayList();
        if (a3 != null) {
            aVar = a(jVar, fragment2, z);
            if (aVar.isEmpty()) {
                arrayList.add(jVar.d);
            } else {
                arrayList.addAll(aVar.values());
            }
            au auVar = z ? fragment2.Y : fragment.Y;
            if (auVar != null) {
                auVar.a(new ArrayList(aVar.keySet()), new ArrayList(aVar.values()), null);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        Object a4 = a(b2, fragment2, arrayList2, aVar);
        if (!(this.u == null || aVar == null || (view = (View) aVar.get(this.u.get(0))) == null)) {
            if (a4 != null) {
                ad.a(a4, view);
            }
            if (a3 != null) {
                ad.a(a3, view);
            }
        }
        f fVar = new f(this, fragment);
        if (a3 != null) {
            a(jVar, viewGroup, a3, fragment, fragment2, z, arrayList);
        }
        ArrayList arrayList3 = new ArrayList();
        a aVar2 = new a();
        Object a5 = ad.a(a2, a4, a3, z ? fragment.y() : fragment.x());
        if (a5 != null) {
            ad.a(a2, a3, viewGroup, fVar, jVar.d, jVar.c, jVar.f25a, arrayList3, aVar2, arrayList);
            a(viewGroup, jVar, i2, a5);
            ad.a(a5, jVar.d, true);
            a(jVar, i2, a5);
            ad.a(viewGroup, a5);
            ad.a(viewGroup, jVar.d, a2, arrayList3, a4, arrayList2, a3, arrayList, a5, jVar.b, aVar2);
        }
        return a5 != null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.ad.a(java.util.Map, android.view.View):void
     arg types: [android.support.v4.f.a, android.view.View]
     candidates:
      android.support.v4.app.ad.a(android.transition.Transition, android.support.v4.app.ai):void
      android.support.v4.app.ad.a(android.view.ViewGroup, java.lang.Object):void
      android.support.v4.app.ad.a(java.lang.Object, android.view.View):void
      android.support.v4.app.ad.a(java.lang.Object, java.util.ArrayList):void
      android.support.v4.app.ad.a(java.util.ArrayList, android.view.View):void
      android.support.v4.app.ad.a(java.util.Map, android.view.View):void */
    private a b(j jVar, Fragment fragment, boolean z) {
        a aVar = new a();
        View h2 = fragment.h();
        if (h2 == null || this.t == null) {
            return aVar;
        }
        ad.a((Map) aVar, h2);
        if (z) {
            return a(this.t, this.u, aVar);
        }
        aVar.a((Collection) this.u);
        return aVar;
    }

    private static Object b(Fragment fragment, boolean z) {
        if (fragment == null) {
            return null;
        }
        return ad.a(z ? fragment.s() : fragment.t());
    }

    private void b(j jVar, a aVar, boolean z) {
        int size = aVar.size();
        for (int i2 = 0; i2 < size; i2++) {
            String str = (String) aVar.b(i2);
            String a2 = ad.a((View) aVar.c(i2));
            if (z) {
                a(jVar.f25a, str, a2);
            } else {
                a(jVar.f25a, a2, str);
            }
        }
    }

    private void b(SparseArray sparseArray, Fragment fragment) {
        int i2;
        if (fragment != null && (i2 = fragment.y) != 0) {
            sparseArray.put(i2, fragment);
        }
    }

    private void b(SparseArray sparseArray, SparseArray sparseArray2) {
        Fragment fragment;
        if (this.f20a.p.a()) {
            for (i iVar = this.b; iVar != null; iVar = iVar.f24a) {
                switch (iVar.c) {
                    case 1:
                        b(sparseArray2, iVar.d);
                        break;
                    case 2:
                        Fragment fragment2 = iVar.d;
                        if (this.f20a.g != null) {
                            int i2 = 0;
                            fragment = fragment2;
                            while (true) {
                                int i3 = i2;
                                if (i3 < this.f20a.g.size()) {
                                    Fragment fragment3 = (Fragment) this.f20a.g.get(i3);
                                    if (fragment == null || fragment3.y == fragment.y) {
                                        if (fragment3 == fragment) {
                                            fragment = null;
                                        } else {
                                            a(sparseArray, fragment3);
                                        }
                                    }
                                    i2 = i3 + 1;
                                }
                            }
                        } else {
                            fragment = fragment2;
                        }
                        b(sparseArray2, fragment);
                        break;
                    case 3:
                        a(sparseArray, iVar.d);
                        break;
                    case 4:
                        a(sparseArray, iVar.d);
                        break;
                    case 5:
                        b(sparseArray2, iVar.d);
                        break;
                    case 6:
                        a(sparseArray, iVar.d);
                        break;
                    case 7:
                        b(sparseArray2, iVar.d);
                        break;
                }
            }
        }
    }

    public int a() {
        return a(false);
    }

    /* access modifiers changed from: package-private */
    public int a(boolean z) {
        if (this.n) {
            throw new IllegalStateException("commit already called");
        }
        if (t.f29a) {
            Log.v("FragmentManager", "Commit: " + this);
            a("  ", (FileDescriptor) null, new PrintWriter(new android.support.v4.f.e("FragmentManager")), (String[]) null);
        }
        this.n = true;
        if (this.k) {
            this.o = this.f20a.a(this);
        } else {
            this.o = -1;
        }
        this.f20a.a(this, z);
        return this.o;
    }

    public ac a(int i2, Fragment fragment, String str) {
        a(i2, fragment, str, 1);
        return this;
    }

    public ac a(Fragment fragment) {
        i iVar = new i();
        iVar.c = 6;
        iVar.d = fragment;
        a(iVar);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.j
     arg types: [android.util.SparseArray, android.util.SparseArray, int]
     candidates:
      android.support.v4.app.e.a(android.support.v4.app.j, android.support.v4.app.Fragment, boolean):android.support.v4.f.a
      android.support.v4.app.e.a(android.support.v4.app.j, boolean, android.support.v4.app.Fragment):android.support.v4.f.a
      android.support.v4.app.e.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.f.a):android.support.v4.f.a
      android.support.v4.app.e.a(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean):java.lang.Object
      android.support.v4.app.e.a(android.support.v4.app.e, android.support.v4.f.a, android.support.v4.app.j):void
      android.support.v4.app.e.a(android.support.v4.app.j, int, java.lang.Object):void
      android.support.v4.app.e.a(android.support.v4.app.j, android.support.v4.f.a, boolean):void
      android.support.v4.app.e.a(android.support.v4.app.j, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.e.a(android.support.v4.f.a, java.lang.String, java.lang.String):void
      android.support.v4.app.e.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.ac
      android.support.v4.app.e.a(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.ac.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.ac
      android.support.v4.app.e.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.j */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.t.a(int, android.support.v4.app.e):void
      android.support.v4.app.t.a(int, boolean):void
      android.support.v4.app.t.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, int, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.t.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.t.a(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.t.a(int, int, int, boolean):void */
    public j a(boolean z, j jVar, SparseArray sparseArray, SparseArray sparseArray2) {
        if (t.f29a) {
            Log.v("FragmentManager", "popFromBackStack: " + this);
            a("  ", (FileDescriptor) null, new PrintWriter(new android.support.v4.f.e("FragmentManager")), (String[]) null);
        }
        if (jVar == null) {
            if (!(sparseArray.size() == 0 && sparseArray2.size() == 0)) {
                jVar = a(sparseArray, sparseArray2, true);
            }
        } else if (!z) {
            a(jVar, this.u, this.t);
        }
        a(-1);
        int i2 = jVar != null ? 0 : this.j;
        int i3 = jVar != null ? 0 : this.i;
        for (i iVar = this.c; iVar != null; iVar = iVar.b) {
            int i4 = jVar != null ? 0 : iVar.g;
            int i5 = jVar != null ? 0 : iVar.h;
            switch (iVar.c) {
                case 1:
                    Fragment fragment = iVar.d;
                    fragment.H = i5;
                    this.f20a.a(fragment, t.c(i3), i2);
                    break;
                case 2:
                    Fragment fragment2 = iVar.d;
                    if (fragment2 != null) {
                        fragment2.H = i5;
                        this.f20a.a(fragment2, t.c(i3), i2);
                    }
                    if (iVar.i == null) {
                        break;
                    } else {
                        for (int i6 = 0; i6 < iVar.i.size(); i6++) {
                            Fragment fragment3 = (Fragment) iVar.i.get(i6);
                            fragment3.H = i4;
                            this.f20a.a(fragment3, false);
                        }
                        break;
                    }
                case 3:
                    Fragment fragment4 = iVar.d;
                    fragment4.H = i4;
                    this.f20a.a(fragment4, false);
                    break;
                case 4:
                    Fragment fragment5 = iVar.d;
                    fragment5.H = i4;
                    this.f20a.c(fragment5, t.c(i3), i2);
                    break;
                case 5:
                    Fragment fragment6 = iVar.d;
                    fragment6.H = i5;
                    this.f20a.b(fragment6, t.c(i3), i2);
                    break;
                case 6:
                    Fragment fragment7 = iVar.d;
                    fragment7.H = i4;
                    this.f20a.e(fragment7, t.c(i3), i2);
                    break;
                case 7:
                    Fragment fragment8 = iVar.d;
                    fragment8.H = i4;
                    this.f20a.d(fragment8, t.c(i3), i2);
                    break;
                default:
                    throw new IllegalArgumentException("Unknown cmd: " + iVar.c);
            }
        }
        if (z) {
            this.f20a.a(this.f20a.n, t.c(i3), i2, true);
            jVar = null;
        }
        if (this.o >= 0) {
            this.f20a.b(this.o);
            this.o = -1;
        }
        return jVar;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (this.k) {
            if (t.f29a) {
                Log.v("FragmentManager", "Bump nesting in " + this + " by " + i2);
            }
            for (i iVar = this.b; iVar != null; iVar = iVar.f24a) {
                if (iVar.d != null) {
                    iVar.d.s += i2;
                    if (t.f29a) {
                        Log.v("FragmentManager", "Bump nesting of " + iVar.d + " to " + iVar.d.s);
                    }
                }
                if (iVar.i != null) {
                    for (int size = iVar.i.size() - 1; size >= 0; size--) {
                        Fragment fragment = (Fragment) iVar.i.get(size);
                        fragment.s += i2;
                        if (t.f29a) {
                            Log.v("FragmentManager", "Bump nesting of " + fragment + " to " + fragment.s);
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(i iVar) {
        if (this.b == null) {
            this.c = iVar;
            this.b = iVar;
        } else {
            iVar.b = this.c;
            this.c.f24a = iVar;
            this.c = iVar;
        }
        iVar.e = this.e;
        iVar.f = this.f;
        iVar.g = this.g;
        iVar.h = this.h;
        this.d++;
    }

    public void a(SparseArray sparseArray, SparseArray sparseArray2) {
        if (this.f20a.p.a()) {
            for (i iVar = this.b; iVar != null; iVar = iVar.f24a) {
                switch (iVar.c) {
                    case 1:
                        a(sparseArray, iVar.d);
                        break;
                    case 2:
                        if (iVar.i != null) {
                            for (int size = iVar.i.size() - 1; size >= 0; size--) {
                                b(sparseArray2, (Fragment) iVar.i.get(size));
                            }
                        }
                        a(sparseArray, iVar.d);
                        break;
                    case 3:
                        b(sparseArray2, iVar.d);
                        break;
                    case 4:
                        b(sparseArray2, iVar.d);
                        break;
                    case 5:
                        a(sparseArray, iVar.d);
                        break;
                    case 6:
                        b(sparseArray2, iVar.d);
                        break;
                    case 7:
                        a(sparseArray, iVar.d);
                        break;
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      android.support.v4.app.e.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.j
      android.support.v4.app.e.a(android.support.v4.app.j, android.support.v4.app.Fragment, boolean):android.support.v4.f.a
      android.support.v4.app.e.a(android.support.v4.app.j, boolean, android.support.v4.app.Fragment):android.support.v4.f.a
      android.support.v4.app.e.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.f.a):android.support.v4.f.a
      android.support.v4.app.e.a(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean):java.lang.Object
      android.support.v4.app.e.a(android.support.v4.app.e, android.support.v4.f.a, android.support.v4.app.j):void
      android.support.v4.app.e.a(android.support.v4.app.j, int, java.lang.Object):void
      android.support.v4.app.e.a(android.support.v4.app.j, android.support.v4.f.a, boolean):void
      android.support.v4.app.e.a(android.support.v4.app.j, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.e.a(android.support.v4.f.a, java.lang.String, java.lang.String):void
      android.support.v4.app.e.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.ac
      android.support.v4.app.ac.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.ac
      android.support.v4.app.e.a(java.lang.String, java.io.PrintWriter, boolean):void */
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        a(str, printWriter, true);
    }

    public void a(String str, PrintWriter printWriter, boolean z) {
        String str2;
        if (z) {
            printWriter.print(str);
            printWriter.print("mName=");
            printWriter.print(this.m);
            printWriter.print(" mIndex=");
            printWriter.print(this.o);
            printWriter.print(" mCommitted=");
            printWriter.println(this.n);
            if (this.i != 0) {
                printWriter.print(str);
                printWriter.print("mTransition=#");
                printWriter.print(Integer.toHexString(this.i));
                printWriter.print(" mTransitionStyle=#");
                printWriter.println(Integer.toHexString(this.j));
            }
            if (!(this.e == 0 && this.f == 0)) {
                printWriter.print(str);
                printWriter.print("mEnterAnim=#");
                printWriter.print(Integer.toHexString(this.e));
                printWriter.print(" mExitAnim=#");
                printWriter.println(Integer.toHexString(this.f));
            }
            if (!(this.g == 0 && this.h == 0)) {
                printWriter.print(str);
                printWriter.print("mPopEnterAnim=#");
                printWriter.print(Integer.toHexString(this.g));
                printWriter.print(" mPopExitAnim=#");
                printWriter.println(Integer.toHexString(this.h));
            }
            if (!(this.p == 0 && this.q == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbTitleRes=#");
                printWriter.print(Integer.toHexString(this.p));
                printWriter.print(" mBreadCrumbTitleText=");
                printWriter.println(this.q);
            }
            if (!(this.r == 0 && this.s == null)) {
                printWriter.print(str);
                printWriter.print("mBreadCrumbShortTitleRes=#");
                printWriter.print(Integer.toHexString(this.r));
                printWriter.print(" mBreadCrumbShortTitleText=");
                printWriter.println(this.s);
            }
        }
        if (this.b != null) {
            printWriter.print(str);
            printWriter.println("Operations:");
            String str3 = str + "    ";
            int i2 = 0;
            i iVar = this.b;
            while (iVar != null) {
                switch (iVar.c) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    default:
                        str2 = "cmd=" + iVar.c;
                        break;
                }
                printWriter.print(str);
                printWriter.print("  Op #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.print(str2);
                printWriter.print(" ");
                printWriter.println(iVar.d);
                if (z) {
                    if (!(iVar.e == 0 && iVar.f == 0)) {
                        printWriter.print(str);
                        printWriter.print("enterAnim=#");
                        printWriter.print(Integer.toHexString(iVar.e));
                        printWriter.print(" exitAnim=#");
                        printWriter.println(Integer.toHexString(iVar.f));
                    }
                    if (!(iVar.g == 0 && iVar.h == 0)) {
                        printWriter.print(str);
                        printWriter.print("popEnterAnim=#");
                        printWriter.print(Integer.toHexString(iVar.g));
                        printWriter.print(" popExitAnim=#");
                        printWriter.println(Integer.toHexString(iVar.h));
                    }
                }
                if (iVar.i != null && iVar.i.size() > 0) {
                    for (int i3 = 0; i3 < iVar.i.size(); i3++) {
                        printWriter.print(str3);
                        if (iVar.i.size() == 1) {
                            printWriter.print("Removed: ");
                        } else {
                            if (i3 == 0) {
                                printWriter.println("Removed:");
                            }
                            printWriter.print(str3);
                            printWriter.print("  #");
                            printWriter.print(i3);
                            printWriter.print(": ");
                        }
                        printWriter.println(iVar.i.get(i3));
                    }
                }
                iVar = iVar.f24a;
                i2++;
            }
        }
    }

    public ac b(Fragment fragment) {
        i iVar = new i();
        iVar.c = 7;
        iVar.d = fragment;
        a(iVar);
        return this;
    }

    public String b() {
        return this.m;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.j
     arg types: [android.util.SparseArray, android.util.SparseArray, int]
     candidates:
      android.support.v4.app.e.a(android.support.v4.app.j, android.support.v4.app.Fragment, boolean):android.support.v4.f.a
      android.support.v4.app.e.a(android.support.v4.app.j, boolean, android.support.v4.app.Fragment):android.support.v4.f.a
      android.support.v4.app.e.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.f.a):android.support.v4.f.a
      android.support.v4.app.e.a(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean):java.lang.Object
      android.support.v4.app.e.a(android.support.v4.app.e, android.support.v4.f.a, android.support.v4.app.j):void
      android.support.v4.app.e.a(android.support.v4.app.j, int, java.lang.Object):void
      android.support.v4.app.e.a(android.support.v4.app.j, android.support.v4.f.a, boolean):void
      android.support.v4.app.e.a(android.support.v4.app.j, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.e.a(android.support.v4.f.a, java.lang.String, java.lang.String):void
      android.support.v4.app.e.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.ac
      android.support.v4.app.e.a(java.lang.String, java.io.PrintWriter, boolean):void
      android.support.v4.app.ac.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.ac
      android.support.v4.app.e.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.j */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.t.a(int, android.support.v4.app.e):void
      android.support.v4.app.t.a(int, boolean):void
      android.support.v4.app.t.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, int, int, boolean):void
     arg types: [int, int, int, int]
     candidates:
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
      android.support.v4.app.t.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.t.a(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.t.a(int, int, int, boolean):void */
    public void run() {
        j jVar;
        Fragment fragment;
        if (t.f29a) {
            Log.v("FragmentManager", "Run: " + this);
        }
        if (!this.k || this.o >= 0) {
            a(1);
            if (Build.VERSION.SDK_INT >= 21) {
                SparseArray sparseArray = new SparseArray();
                SparseArray sparseArray2 = new SparseArray();
                b(sparseArray, sparseArray2);
                jVar = a(sparseArray, sparseArray2, false);
            } else {
                jVar = null;
            }
            int i2 = jVar != null ? 0 : this.j;
            int i3 = jVar != null ? 0 : this.i;
            for (i iVar = this.b; iVar != null; iVar = iVar.f24a) {
                int i4 = jVar != null ? 0 : iVar.e;
                int i5 = jVar != null ? 0 : iVar.f;
                switch (iVar.c) {
                    case 1:
                        Fragment fragment2 = iVar.d;
                        fragment2.H = i4;
                        this.f20a.a(fragment2, false);
                        break;
                    case 2:
                        Fragment fragment3 = iVar.d;
                        if (this.f20a.g != null) {
                            fragment = fragment3;
                            for (int i6 = 0; i6 < this.f20a.g.size(); i6++) {
                                Fragment fragment4 = (Fragment) this.f20a.g.get(i6);
                                if (t.f29a) {
                                    Log.v("FragmentManager", "OP_REPLACE: adding=" + fragment + " old=" + fragment4);
                                }
                                if (fragment == null || fragment4.y == fragment.y) {
                                    if (fragment4 == fragment) {
                                        iVar.d = null;
                                        fragment = null;
                                    } else {
                                        if (iVar.i == null) {
                                            iVar.i = new ArrayList();
                                        }
                                        iVar.i.add(fragment4);
                                        fragment4.H = i5;
                                        if (this.k) {
                                            fragment4.s++;
                                            if (t.f29a) {
                                                Log.v("FragmentManager", "Bump nesting of " + fragment4 + " to " + fragment4.s);
                                            }
                                        }
                                        this.f20a.a(fragment4, i3, i2);
                                    }
                                }
                            }
                        } else {
                            fragment = fragment3;
                        }
                        if (fragment == null) {
                            break;
                        } else {
                            fragment.H = i4;
                            this.f20a.a(fragment, false);
                            break;
                        }
                    case 3:
                        Fragment fragment5 = iVar.d;
                        fragment5.H = i5;
                        this.f20a.a(fragment5, i3, i2);
                        break;
                    case 4:
                        Fragment fragment6 = iVar.d;
                        fragment6.H = i5;
                        this.f20a.b(fragment6, i3, i2);
                        break;
                    case 5:
                        Fragment fragment7 = iVar.d;
                        fragment7.H = i4;
                        this.f20a.c(fragment7, i3, i2);
                        break;
                    case 6:
                        Fragment fragment8 = iVar.d;
                        fragment8.H = i5;
                        this.f20a.d(fragment8, i3, i2);
                        break;
                    case 7:
                        Fragment fragment9 = iVar.d;
                        fragment9.H = i4;
                        this.f20a.e(fragment9, i3, i2);
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown cmd: " + iVar.c);
                }
            }
            this.f20a.a(this.f20a.n, i3, i2, true);
            if (this.k) {
                this.f20a.b(this);
                return;
            }
            return;
        }
        throw new IllegalStateException("addToBackStack() called after commit()");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("BackStackEntry{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.o >= 0) {
            sb.append(" #");
            sb.append(this.o);
        }
        if (this.m != null) {
            sb.append(" ");
            sb.append(this.m);
        }
        sb.append("}");
        return sb.toString();
    }
}
