package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.a.a;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

public class aw implements Iterable {

    /* renamed from: a  reason: collision with root package name */
    private static final ay f19a;
    private final ArrayList b = new ArrayList();
    private final Context c;

    static {
        if (Build.VERSION.SDK_INT >= 11) {
            f19a = new ba();
        } else {
            f19a = new az();
        }
    }

    private aw(Context context) {
        this.c = context;
    }

    public static aw a(Context context) {
        return new aw(context);
    }

    public aw a(Activity activity) {
        Intent intent = null;
        if (activity instanceof ax) {
            intent = ((ax) activity).a();
        }
        Intent a2 = intent == null ? ao.a(activity) : intent;
        if (a2 != null) {
            ComponentName component = a2.getComponent();
            if (component == null) {
                component = a2.resolveActivity(this.c.getPackageManager());
            }
            a(component);
            a(a2);
        }
        return this;
    }

    public aw a(ComponentName componentName) {
        int size = this.b.size();
        try {
            Intent a2 = ao.a(this.c, componentName);
            while (a2 != null) {
                this.b.add(size, a2);
                a2 = ao.a(this.c, a2.getComponent());
            }
            return this;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
            throw new IllegalArgumentException(e);
        }
    }

    public aw a(Intent intent) {
        this.b.add(intent);
        return this;
    }

    public void a() {
        a((Bundle) null);
    }

    public void a(Bundle bundle) {
        if (this.b.isEmpty()) {
            throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
        }
        Intent[] intentArr = (Intent[]) this.b.toArray(new Intent[this.b.size()]);
        intentArr[0] = new Intent(intentArr[0]).addFlags(268484608);
        if (!a.a(this.c, intentArr, bundle)) {
            Intent intent = new Intent(intentArr[intentArr.length - 1]);
            intent.addFlags(268435456);
            this.c.startActivity(intent);
        }
    }

    public Iterator iterator() {
        return this.b.iterator();
    }
}
