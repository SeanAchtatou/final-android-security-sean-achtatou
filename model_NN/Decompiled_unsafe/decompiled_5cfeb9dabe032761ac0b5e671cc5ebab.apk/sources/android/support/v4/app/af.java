package android.support.v4.app;

import android.transition.Transition;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;
import java.util.Map;

final class af implements ViewTreeObserver.OnPreDrawListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f12a;
    final /* synthetic */ aj b;
    final /* synthetic */ Map c;
    final /* synthetic */ Map d;
    final /* synthetic */ Transition e;
    final /* synthetic */ ArrayList f;

    af(View view, aj ajVar, Map map, Map map2, Transition transition, ArrayList arrayList) {
        this.f12a = view;
        this.b = ajVar;
        this.c = map;
        this.d = map2;
        this.e = transition;
        this.f = arrayList;
    }

    public boolean onPreDraw() {
        this.f12a.getViewTreeObserver().removeOnPreDrawListener(this);
        View a2 = this.b.a();
        if (a2 == null) {
            return true;
        }
        if (!this.c.isEmpty()) {
            ad.a(this.d, a2);
            this.d.keySet().retainAll(this.c.values());
            for (Map.Entry entry : this.c.entrySet()) {
                View view = (View) this.d.get((String) entry.getValue());
                if (view != null) {
                    view.setTransitionName((String) entry.getKey());
                }
            }
        }
        if (this.e == null) {
            return true;
        }
        ad.b(this.f, a2);
        this.f.removeAll(this.d.values());
        ad.b(this.e, this.f);
        return true;
    }
}
