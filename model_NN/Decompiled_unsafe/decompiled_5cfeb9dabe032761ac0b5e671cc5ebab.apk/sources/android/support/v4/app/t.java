package android.support.v4.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.f.d;
import android.support.v4.f.e;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

final class t extends r implements LayoutInflater.Factory {
    static final Interpolator A = new DecelerateInterpolator(1.5f);
    static final Interpolator B = new AccelerateInterpolator(2.5f);
    static final Interpolator C = new AccelerateInterpolator(1.5f);

    /* renamed from: a  reason: collision with root package name */
    static boolean f29a = false;
    static final boolean b;
    static final Interpolator z = new DecelerateInterpolator(2.5f);
    ArrayList c;
    Runnable[] d;
    boolean e;
    ArrayList f;
    ArrayList g;
    ArrayList h;
    ArrayList i;
    ArrayList j;
    ArrayList k;
    ArrayList l;
    ArrayList m;
    int n = 0;
    o o;
    q p;
    Fragment q;
    boolean r;
    boolean s;
    boolean t;
    String u;
    boolean v;
    Bundle w = null;
    SparseArray x = null;
    Runnable y = new u(this);

    static {
        boolean z2 = false;
        if (Build.VERSION.SDK_INT >= 11) {
            z2 = true;
        }
        b = z2;
    }

    t() {
    }

    static Animation a(Context context, float f2, float f3) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(f2, f3);
        alphaAnimation.setInterpolator(A);
        alphaAnimation.setDuration(220);
        return alphaAnimation;
    }

    static Animation a(Context context, float f2, float f3, float f4, float f5) {
        AnimationSet animationSet = new AnimationSet(false);
        ScaleAnimation scaleAnimation = new ScaleAnimation(f2, f3, f2, f3, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(z);
        scaleAnimation.setDuration(220);
        animationSet.addAnimation(scaleAnimation);
        AlphaAnimation alphaAnimation = new AlphaAnimation(f4, f5);
        alphaAnimation.setInterpolator(A);
        alphaAnimation.setDuration(220);
        animationSet.addAnimation(alphaAnimation);
        return animationSet;
    }

    private void a(RuntimeException runtimeException) {
        Log.e("FragmentManager", runtimeException.getMessage());
        Log.e("FragmentManager", "Activity state:");
        PrintWriter printWriter = new PrintWriter(new e("FragmentManager"));
        if (this.o != null) {
            try {
                this.o.dump("  ", null, printWriter, new String[0]);
            } catch (Exception e2) {
                Log.e("FragmentManager", "Failed dumping state", e2);
            }
        } else {
            try {
                a("  ", (FileDescriptor) null, printWriter, new String[0]);
            } catch (Exception e3) {
                Log.e("FragmentManager", "Failed dumping state", e3);
            }
        }
        throw runtimeException;
    }

    public static int b(int i2, boolean z2) {
        switch (i2) {
            case 4097:
                return z2 ? 1 : 2;
            case 4099:
                return z2 ? 5 : 6;
            case 8194:
                return z2 ? 3 : 4;
            default:
                return -1;
        }
    }

    public static int c(int i2) {
        switch (i2) {
            case 4097:
                return 8194;
            case 4099:
                return 4099;
            case 8194:
                return 4097;
            default:
                return 0;
        }
    }

    private void u() {
        if (this.s) {
            throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
        } else if (this.u != null) {
            throw new IllegalStateException("Can not perform this action inside of " + this.u);
        }
    }

    public int a(e eVar) {
        int i2;
        synchronized (this) {
            if (this.l == null || this.l.size() <= 0) {
                if (this.k == null) {
                    this.k = new ArrayList();
                }
                i2 = this.k.size();
                if (f29a) {
                    Log.v("FragmentManager", "Setting back stack index " + i2 + " to " + eVar);
                }
                this.k.add(eVar);
            } else {
                i2 = ((Integer) this.l.remove(this.l.size() - 1)).intValue();
                if (f29a) {
                    Log.v("FragmentManager", "Adding back stack index " + i2 + " with " + eVar);
                }
                this.k.set(i2, eVar);
            }
        }
        return i2;
    }

    public Fragment a(int i2) {
        if (this.g != null) {
            for (int size = this.g.size() - 1; size >= 0; size--) {
                Fragment fragment = (Fragment) this.g.get(size);
                if (fragment != null && fragment.x == i2) {
                    return fragment;
                }
            }
        }
        if (this.f != null) {
            for (int size2 = this.f.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = (Fragment) this.f.get(size2);
                if (fragment2 != null && fragment2.x == i2) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    public Fragment a(Bundle bundle, String str) {
        int i2 = bundle.getInt(str, -1);
        if (i2 == -1) {
            return null;
        }
        if (i2 >= this.f.size()) {
            a(new IllegalStateException("Fragment no longer exists for key " + str + ": index " + i2));
        }
        Fragment fragment = (Fragment) this.f.get(i2);
        if (fragment != null) {
            return fragment;
        }
        a(new IllegalStateException("Fragment no longer exists for key " + str + ": index " + i2));
        return fragment;
    }

    public Fragment a(String str) {
        if (!(this.g == null || str == null)) {
            for (int size = this.g.size() - 1; size >= 0; size--) {
                Fragment fragment = (Fragment) this.g.get(size);
                if (fragment != null && str.equals(fragment.z)) {
                    return fragment;
                }
            }
        }
        if (!(this.f == null || str == null)) {
            for (int size2 = this.f.size() - 1; size2 >= 0; size2--) {
                Fragment fragment2 = (Fragment) this.f.get(size2);
                if (fragment2 != null && str.equals(fragment2.z)) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    public ac a() {
        return new e(this);
    }

    /* access modifiers changed from: package-private */
    public Animation a(Fragment fragment, int i2, boolean z2, int i3) {
        Animation loadAnimation;
        Animation a2 = fragment.a(i2, z2, fragment.H);
        if (a2 != null) {
            return a2;
        }
        if (fragment.H != 0 && (loadAnimation = AnimationUtils.loadAnimation(this.o, fragment.H)) != null) {
            return loadAnimation;
        }
        if (i2 == 0) {
            return null;
        }
        int b2 = b(i2, z2);
        if (b2 < 0) {
            return null;
        }
        switch (b2) {
            case 1:
                return a(this.o, 1.125f, 1.0f, 0.0f, 1.0f);
            case 2:
                return a(this.o, 1.0f, 0.975f, 1.0f, 0.0f);
            case 3:
                return a(this.o, 0.975f, 1.0f, 0.0f, 1.0f);
            case 4:
                return a(this.o, 1.0f, 1.075f, 1.0f, 0.0f);
            case 5:
                return a(this.o, 0.0f, 1.0f);
            case 6:
                return a(this.o, 1.0f, 0.0f);
            default:
                if (i3 == 0 && this.o.getWindow() != null) {
                    i3 = this.o.getWindow().getAttributes().windowAnimations;
                }
                return i3 == 0 ? null : null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.t.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4, boolean z2) {
        if (this.o == null && i2 != 0) {
            throw new IllegalStateException("No activity");
        } else if (z2 || this.n != i2) {
            this.n = i2;
            if (this.f != null) {
                int i5 = 0;
                boolean z3 = false;
                while (i5 < this.f.size()) {
                    Fragment fragment = (Fragment) this.f.get(i5);
                    if (fragment != null) {
                        a(fragment, i2, i3, i4, false);
                        if (fragment.N != null) {
                            z3 |= fragment.N.a();
                        }
                    }
                    i5++;
                    z3 = z3;
                }
                if (!z3) {
                    d();
                }
                if (this.r && this.o != null && this.n == 5) {
                    this.o.d();
                    this.r = false;
                }
            }
        }
    }

    public void a(int i2, e eVar) {
        synchronized (this) {
            if (this.k == null) {
                this.k = new ArrayList();
            }
            int size = this.k.size();
            if (i2 < size) {
                if (f29a) {
                    Log.v("FragmentManager", "Setting back stack index " + i2 + " to " + eVar);
                }
                this.k.set(i2, eVar);
            } else {
                while (size < i2) {
                    this.k.add(null);
                    if (this.l == null) {
                        this.l = new ArrayList();
                    }
                    if (f29a) {
                        Log.v("FragmentManager", "Adding available back stack index " + size);
                    }
                    this.l.add(Integer.valueOf(size));
                    size++;
                }
                if (f29a) {
                    Log.v("FragmentManager", "Adding back stack index " + i2 + " with " + eVar);
                }
                this.k.add(eVar);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, boolean z2) {
        a(i2, 0, 0, z2);
    }

    public void a(Configuration configuration) {
        if (this.g != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.g.size()) {
                    Fragment fragment = (Fragment) this.g.get(i3);
                    if (fragment != null) {
                        fragment.a(configuration);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void a(Bundle bundle, String str, Fragment fragment) {
        if (fragment.g < 0) {
            a(new IllegalStateException("Fragment " + fragment + " is not currently in the FragmentManager"));
        }
        bundle.putInt(str, fragment.g);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(java.lang.String, java.io.PrintWriter, boolean):void
     arg types: [java.lang.String, java.io.PrintWriter, int]
     candidates:
      android.support.v4.app.e.a(android.util.SparseArray, android.util.SparseArray, boolean):android.support.v4.app.j
      android.support.v4.app.e.a(android.support.v4.app.j, android.support.v4.app.Fragment, boolean):android.support.v4.f.a
      android.support.v4.app.e.a(android.support.v4.app.j, boolean, android.support.v4.app.Fragment):android.support.v4.f.a
      android.support.v4.app.e.a(java.util.ArrayList, java.util.ArrayList, android.support.v4.f.a):android.support.v4.f.a
      android.support.v4.app.e.a(android.support.v4.app.Fragment, android.support.v4.app.Fragment, boolean):java.lang.Object
      android.support.v4.app.e.a(android.support.v4.app.e, android.support.v4.f.a, android.support.v4.app.j):void
      android.support.v4.app.e.a(android.support.v4.app.j, int, java.lang.Object):void
      android.support.v4.app.e.a(android.support.v4.app.j, android.support.v4.f.a, boolean):void
      android.support.v4.app.e.a(android.support.v4.app.j, java.util.ArrayList, java.util.ArrayList):void
      android.support.v4.app.e.a(android.support.v4.f.a, java.lang.String, java.lang.String):void
      android.support.v4.app.e.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.ac
      android.support.v4.app.ac.a(int, android.support.v4.app.Fragment, java.lang.String):android.support.v4.app.ac
      android.support.v4.app.e.a(java.lang.String, java.io.PrintWriter, boolean):void */
    /* access modifiers changed from: package-private */
    public void a(Parcelable parcelable, ArrayList arrayList) {
        if (parcelable != null) {
            FragmentManagerState fragmentManagerState = (FragmentManagerState) parcelable;
            if (fragmentManagerState.f6a != null) {
                if (arrayList != null) {
                    for (int i2 = 0; i2 < arrayList.size(); i2++) {
                        Fragment fragment = (Fragment) arrayList.get(i2);
                        if (f29a) {
                            Log.v("FragmentManager", "restoreAllState: re-attaching retained " + fragment);
                        }
                        FragmentState fragmentState = fragmentManagerState.f6a[fragment.g];
                        fragmentState.k = fragment;
                        fragment.f = null;
                        fragment.s = 0;
                        fragment.q = false;
                        fragment.m = false;
                        fragment.j = null;
                        if (fragmentState.j != null) {
                            fragmentState.j.setClassLoader(this.o.getClassLoader());
                            fragment.f = fragmentState.j.getSparseParcelableArray("android:view_state");
                            fragment.e = fragmentState.j;
                        }
                    }
                }
                this.f = new ArrayList(fragmentManagerState.f6a.length);
                if (this.h != null) {
                    this.h.clear();
                }
                for (int i3 = 0; i3 < fragmentManagerState.f6a.length; i3++) {
                    FragmentState fragmentState2 = fragmentManagerState.f6a[i3];
                    if (fragmentState2 != null) {
                        Fragment a2 = fragmentState2.a(this.o, this.q);
                        if (f29a) {
                            Log.v("FragmentManager", "restoreAllState: active #" + i3 + ": " + a2);
                        }
                        this.f.add(a2);
                        fragmentState2.k = null;
                    } else {
                        this.f.add(null);
                        if (this.h == null) {
                            this.h = new ArrayList();
                        }
                        if (f29a) {
                            Log.v("FragmentManager", "restoreAllState: avail #" + i3);
                        }
                        this.h.add(Integer.valueOf(i3));
                    }
                }
                if (arrayList != null) {
                    for (int i4 = 0; i4 < arrayList.size(); i4++) {
                        Fragment fragment2 = (Fragment) arrayList.get(i4);
                        if (fragment2.k >= 0) {
                            if (fragment2.k < this.f.size()) {
                                fragment2.j = (Fragment) this.f.get(fragment2.k);
                            } else {
                                Log.w("FragmentManager", "Re-attaching retained fragment " + fragment2 + " target no longer exists: " + fragment2.k);
                                fragment2.j = null;
                            }
                        }
                    }
                }
                if (fragmentManagerState.b != null) {
                    this.g = new ArrayList(fragmentManagerState.b.length);
                    for (int i5 = 0; i5 < fragmentManagerState.b.length; i5++) {
                        Fragment fragment3 = (Fragment) this.f.get(fragmentManagerState.b[i5]);
                        if (fragment3 == null) {
                            a(new IllegalStateException("No instantiated fragment for index #" + fragmentManagerState.b[i5]));
                        }
                        fragment3.m = true;
                        if (f29a) {
                            Log.v("FragmentManager", "restoreAllState: added #" + i5 + ": " + fragment3);
                        }
                        if (this.g.contains(fragment3)) {
                            throw new IllegalStateException("Already added!");
                        }
                        this.g.add(fragment3);
                    }
                } else {
                    this.g = null;
                }
                if (fragmentManagerState.c != null) {
                    this.i = new ArrayList(fragmentManagerState.c.length);
                    for (int i6 = 0; i6 < fragmentManagerState.c.length; i6++) {
                        e a3 = fragmentManagerState.c[i6].a(this);
                        if (f29a) {
                            Log.v("FragmentManager", "restoreAllState: back stack #" + i6 + " (index " + a3.o + "): " + a3);
                            a3.a("  ", new PrintWriter(new e("FragmentManager")), false);
                        }
                        this.i.add(a3);
                        if (a3.o >= 0) {
                            a(a3.o, a3);
                        }
                    }
                    return;
                }
                this.i = null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.t.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void a(Fragment fragment) {
        if (!fragment.L) {
            return;
        }
        if (this.e) {
            this.v = true;
            return;
        }
        fragment.L = false;
        a(fragment, this.n, 0, 0, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.t.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void a(Fragment fragment, int i2, int i3) {
        if (f29a) {
            Log.v("FragmentManager", "remove: " + fragment + " nesting=" + fragment.s);
        }
        boolean z2 = !fragment.a();
        if (!fragment.B || z2) {
            if (this.g != null) {
                this.g.remove(fragment);
            }
            if (fragment.E && fragment.F) {
                this.r = true;
            }
            fragment.m = false;
            fragment.n = true;
            a(fragment, z2 ? 0 : 1, i2, i3, false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.t.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.t.a(int, int, int, boolean):void
      android.support.v4.app.t.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.t.a(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    /* access modifiers changed from: package-private */
    public void a(Fragment fragment, int i2, int i3, int i4, boolean z2) {
        ViewGroup viewGroup;
        if ((!fragment.m || fragment.B) && i2 > 1) {
            i2 = 1;
        }
        if (fragment.n && i2 > fragment.b) {
            i2 = fragment.b;
        }
        if (fragment.L && fragment.b < 4 && i2 > 3) {
            i2 = 3;
        }
        if (fragment.b >= i2) {
            if (fragment.b > i2) {
                switch (fragment.b) {
                    case 5:
                        if (i2 < 5) {
                            if (f29a) {
                                Log.v("FragmentManager", "movefrom RESUMED: " + fragment);
                            }
                            fragment.D();
                            fragment.o = false;
                        }
                    case 4:
                        if (i2 < 4) {
                            if (f29a) {
                                Log.v("FragmentManager", "movefrom STARTED: " + fragment);
                            }
                            fragment.E();
                        }
                    case 3:
                        if (i2 < 3) {
                            if (f29a) {
                                Log.v("FragmentManager", "movefrom STOPPED: " + fragment);
                            }
                            fragment.F();
                        }
                    case 2:
                        if (i2 < 2) {
                            if (f29a) {
                                Log.v("FragmentManager", "movefrom ACTIVITY_CREATED: " + fragment);
                            }
                            if (fragment.J != null && !this.o.isFinishing() && fragment.f == null) {
                                e(fragment);
                            }
                            fragment.G();
                            if (!(fragment.J == null || fragment.I == null)) {
                                Animation a2 = (this.n <= 0 || this.t) ? null : a(fragment, i3, false, i4);
                                if (a2 != null) {
                                    fragment.c = fragment.J;
                                    fragment.d = i2;
                                    a2.setAnimationListener(new v(this, fragment));
                                    fragment.J.startAnimation(a2);
                                }
                                fragment.I.removeView(fragment.J);
                            }
                            fragment.I = null;
                            fragment.J = null;
                            fragment.K = null;
                        }
                        break;
                    case 1:
                        if (i2 < 1) {
                            if (this.t && fragment.c != null) {
                                View view = fragment.c;
                                fragment.c = null;
                                view.clearAnimation();
                            }
                            if (fragment.c == null) {
                                if (f29a) {
                                    Log.v("FragmentManager", "movefrom CREATED: " + fragment);
                                }
                                if (!fragment.D) {
                                    fragment.H();
                                }
                                fragment.G = false;
                                fragment.p();
                                if (fragment.G) {
                                    if (!z2) {
                                        if (fragment.D) {
                                            fragment.u = null;
                                            fragment.w = null;
                                            fragment.t = null;
                                            fragment.v = null;
                                            break;
                                        } else {
                                            d(fragment);
                                            break;
                                        }
                                    }
                                } else {
                                    throw new av("Fragment " + fragment + " did not call through to super.onDetach()");
                                }
                            } else {
                                fragment.d = i2;
                                i2 = 1;
                                break;
                            }
                        }
                        break;
                }
            }
        } else if (!fragment.p || fragment.q) {
            if (fragment.c != null) {
                fragment.c = null;
                a(fragment, fragment.d, 0, 0, true);
            }
            switch (fragment.b) {
                case 0:
                    if (f29a) {
                        Log.v("FragmentManager", "moveto CREATED: " + fragment);
                    }
                    if (fragment.e != null) {
                        fragment.e.setClassLoader(this.o.getClassLoader());
                        fragment.f = fragment.e.getSparseParcelableArray("android:view_state");
                        fragment.j = a(fragment.e, "android:target_state");
                        if (fragment.j != null) {
                            fragment.l = fragment.e.getInt("android:target_req_state", 0);
                        }
                        fragment.M = fragment.e.getBoolean("android:user_visible_hint", true);
                        if (!fragment.M) {
                            fragment.L = true;
                            if (i2 > 3) {
                                i2 = 3;
                            }
                        }
                    }
                    fragment.u = this.o;
                    fragment.w = this.q;
                    fragment.t = this.q != null ? this.q.v : this.o.b;
                    fragment.G = false;
                    fragment.a(this.o);
                    if (!fragment.G) {
                        throw new av("Fragment " + fragment + " did not call through to super.onAttach()");
                    }
                    if (fragment.w == null) {
                        this.o.a(fragment);
                    }
                    if (!fragment.D) {
                        fragment.g(fragment.e);
                    }
                    fragment.D = false;
                    if (fragment.p) {
                        fragment.J = fragment.b(fragment.b(fragment.e), null, fragment.e);
                        if (fragment.J != null) {
                            fragment.K = fragment.J;
                            fragment.J = at.a(fragment.J);
                            if (fragment.A) {
                                fragment.J.setVisibility(8);
                            }
                            fragment.a(fragment.J, fragment.e);
                        } else {
                            fragment.K = null;
                        }
                    }
                case 1:
                    if (i2 > 1) {
                        if (f29a) {
                            Log.v("FragmentManager", "moveto ACTIVITY_CREATED: " + fragment);
                        }
                        if (!fragment.p) {
                            if (fragment.y != 0) {
                                viewGroup = (ViewGroup) this.p.a(fragment.y);
                                if (viewGroup == null && !fragment.r) {
                                    a(new IllegalArgumentException("No view found for id 0x" + Integer.toHexString(fragment.y) + " (" + fragment.c().getResourceName(fragment.y) + ") for fragment " + fragment));
                                }
                            } else {
                                viewGroup = null;
                            }
                            fragment.I = viewGroup;
                            fragment.J = fragment.b(fragment.b(fragment.e), viewGroup, fragment.e);
                            if (fragment.J != null) {
                                fragment.K = fragment.J;
                                fragment.J = at.a(fragment.J);
                                if (viewGroup != null) {
                                    Animation a3 = a(fragment, i3, true, i4);
                                    if (a3 != null) {
                                        fragment.J.startAnimation(a3);
                                    }
                                    viewGroup.addView(fragment.J);
                                }
                                if (fragment.A) {
                                    fragment.J.setVisibility(8);
                                }
                                fragment.a(fragment.J, fragment.e);
                            } else {
                                fragment.K = null;
                            }
                        }
                        fragment.h(fragment.e);
                        if (fragment.J != null) {
                            fragment.a(fragment.e);
                        }
                        fragment.e = null;
                    }
                case 2:
                case 3:
                    if (i2 > 3) {
                        if (f29a) {
                            Log.v("FragmentManager", "moveto STARTED: " + fragment);
                        }
                        fragment.A();
                    }
                case 4:
                    if (i2 > 4) {
                        if (f29a) {
                            Log.v("FragmentManager", "moveto RESUMED: " + fragment);
                        }
                        fragment.o = true;
                        fragment.B();
                        fragment.e = null;
                        fragment.f = null;
                        break;
                    }
                    break;
            }
        } else {
            return;
        }
        fragment.b = i2;
    }

    public void a(Fragment fragment, boolean z2) {
        if (this.g == null) {
            this.g = new ArrayList();
        }
        if (f29a) {
            Log.v("FragmentManager", "add: " + fragment);
        }
        c(fragment);
        if (fragment.B) {
            return;
        }
        if (this.g.contains(fragment)) {
            throw new IllegalStateException("Fragment already added: " + fragment);
        }
        this.g.add(fragment);
        fragment.m = true;
        fragment.n = false;
        if (fragment.E && fragment.F) {
            this.r = true;
        }
        if (z2) {
            b(fragment);
        }
    }

    public void a(o oVar, q qVar, Fragment fragment) {
        if (this.o != null) {
            throw new IllegalStateException("Already attached");
        }
        this.o = oVar;
        this.p = qVar;
        this.q = fragment;
    }

    public void a(Runnable runnable, boolean z2) {
        if (!z2) {
            u();
        }
        synchronized (this) {
            if (this.t || this.o == null) {
                throw new IllegalStateException("Activity has been destroyed");
            }
            if (this.c == null) {
                this.c = new ArrayList();
            }
            this.c.add(runnable);
            if (this.c.size() == 1) {
                this.o.f27a.removeCallbacks(this.y);
                this.o.f27a.post(this.y);
            }
        }
    }

    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int size;
        int size2;
        int size3;
        int size4;
        int size5;
        int size6;
        String str2 = str + "    ";
        if (this.f != null && (size6 = this.f.size()) > 0) {
            printWriter.print(str);
            printWriter.print("Active Fragments in ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this)));
            printWriter.println(":");
            for (int i2 = 0; i2 < size6; i2++) {
                Fragment fragment = (Fragment) this.f.get(i2);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.println(fragment);
                if (fragment != null) {
                    fragment.a(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }
        if (this.g != null && (size5 = this.g.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i3 = 0; i3 < size5; i3++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i3);
                printWriter.print(": ");
                printWriter.println(((Fragment) this.g.get(i3)).toString());
            }
        }
        if (this.j != null && (size4 = this.j.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Fragments Created Menus:");
            for (int i4 = 0; i4 < size4; i4++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i4);
                printWriter.print(": ");
                printWriter.println(((Fragment) this.j.get(i4)).toString());
            }
        }
        if (this.i != null && (size3 = this.i.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Back Stack:");
            for (int i5 = 0; i5 < size3; i5++) {
                e eVar = (e) this.i.get(i5);
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i5);
                printWriter.print(": ");
                printWriter.println(eVar.toString());
                eVar.a(str2, fileDescriptor, printWriter, strArr);
            }
        }
        synchronized (this) {
            if (this.k != null && (size2 = this.k.size()) > 0) {
                printWriter.print(str);
                printWriter.println("Back Stack Indices:");
                for (int i6 = 0; i6 < size2; i6++) {
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(i6);
                    printWriter.print(": ");
                    printWriter.println((e) this.k.get(i6));
                }
            }
            if (this.l != null && this.l.size() > 0) {
                printWriter.print(str);
                printWriter.print("mAvailBackStackIndices: ");
                printWriter.println(Arrays.toString(this.l.toArray()));
            }
        }
        if (this.c != null && (size = this.c.size()) > 0) {
            printWriter.print(str);
            printWriter.println("Pending Actions:");
            for (int i7 = 0; i7 < size; i7++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i7);
                printWriter.print(": ");
                printWriter.println((Runnable) this.c.get(i7));
            }
        }
        printWriter.print(str);
        printWriter.println("FragmentManager misc state:");
        printWriter.print(str);
        printWriter.print("  mActivity=");
        printWriter.println(this.o);
        printWriter.print(str);
        printWriter.print("  mContainer=");
        printWriter.println(this.p);
        if (this.q != null) {
            printWriter.print(str);
            printWriter.print("  mParent=");
            printWriter.println(this.q);
        }
        printWriter.print(str);
        printWriter.print("  mCurState=");
        printWriter.print(this.n);
        printWriter.print(" mStateSaved=");
        printWriter.print(this.s);
        printWriter.print(" mDestroyed=");
        printWriter.println(this.t);
        if (this.r) {
            printWriter.print(str);
            printWriter.print("  mNeedMenuInvalidate=");
            printWriter.println(this.r);
        }
        if (this.u != null) {
            printWriter.print(str);
            printWriter.print("  mNoTransactionsBecause=");
            printWriter.println(this.u);
        }
        if (this.h != null && this.h.size() > 0) {
            printWriter.print(str);
            printWriter.print("  mAvailIndices: ");
            printWriter.println(Arrays.toString(this.h.toArray()));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.e.a(boolean, android.support.v4.app.j, android.util.SparseArray, android.util.SparseArray):android.support.v4.app.j
     arg types: [int, ?[OBJECT, ARRAY], android.util.SparseArray, android.util.SparseArray]
     candidates:
      android.support.v4.app.e.a(android.support.v4.app.e, android.support.v4.app.j, boolean, android.support.v4.app.Fragment):android.support.v4.f.a
      android.support.v4.app.e.a(java.lang.Object, android.support.v4.app.Fragment, java.util.ArrayList, android.support.v4.f.a):java.lang.Object
      android.support.v4.app.e.a(int, android.support.v4.app.Fragment, java.lang.String, int):void
      android.support.v4.app.e.a(android.support.v4.app.e, android.support.v4.app.j, int, java.lang.Object):void
      android.support.v4.app.e.a(android.view.View, android.support.v4.app.j, int, java.lang.Object):void
      android.support.v4.app.e.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.e.a(boolean, android.support.v4.app.j, android.util.SparseArray, android.util.SparseArray):android.support.v4.app.j */
    /* access modifiers changed from: package-private */
    public boolean a(Handler handler, String str, int i2, int i3) {
        int i4;
        if (this.i == null) {
            return false;
        }
        if (str == null && i2 < 0 && (i3 & 1) == 0) {
            int size = this.i.size() - 1;
            if (size < 0) {
                return false;
            }
            e eVar = (e) this.i.remove(size);
            SparseArray sparseArray = new SparseArray();
            SparseArray sparseArray2 = new SparseArray();
            eVar.a(sparseArray, sparseArray2);
            eVar.a(true, (j) null, sparseArray, sparseArray2);
            f();
        } else {
            int i5 = -1;
            if (str != null || i2 >= 0) {
                int size2 = this.i.size() - 1;
                while (i4 >= 0) {
                    e eVar2 = (e) this.i.get(i4);
                    if ((str != null && str.equals(eVar2.b())) || (i2 >= 0 && i2 == eVar2.o)) {
                        break;
                    }
                    size2 = i4 - 1;
                }
                if (i4 < 0) {
                    return false;
                }
                if ((i3 & 1) != 0) {
                    i4--;
                    while (i4 >= 0) {
                        e eVar3 = (e) this.i.get(i4);
                        if ((str == null || !str.equals(eVar3.b())) && (i2 < 0 || i2 != eVar3.o)) {
                            break;
                        }
                        i4--;
                    }
                }
                i5 = i4;
            }
            if (i5 == this.i.size() - 1) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            for (int size3 = this.i.size() - 1; size3 > i5; size3--) {
                arrayList.add(this.i.remove(size3));
            }
            int size4 = arrayList.size() - 1;
            SparseArray sparseArray3 = new SparseArray();
            SparseArray sparseArray4 = new SparseArray();
            for (int i6 = 0; i6 <= size4; i6++) {
                ((e) arrayList.get(i6)).a(sparseArray3, sparseArray4);
            }
            j jVar = null;
            int i7 = 0;
            while (i7 <= size4) {
                if (f29a) {
                    Log.v("FragmentManager", "Popping back stack state: " + arrayList.get(i7));
                }
                i7++;
                jVar = ((e) arrayList.get(i7)).a(i7 == size4, jVar, sparseArray3, sparseArray4);
            }
            f();
        }
        return true;
    }

    public boolean a(Menu menu) {
        if (this.g == null) {
            return false;
        }
        boolean z2 = false;
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            Fragment fragment = (Fragment) this.g.get(i2);
            if (fragment != null && fragment.c(menu)) {
                z2 = true;
            }
        }
        return z2;
    }

    public boolean a(Menu menu, MenuInflater menuInflater) {
        boolean z2;
        ArrayList arrayList = null;
        if (this.g != null) {
            int i2 = 0;
            z2 = false;
            while (i2 < this.g.size()) {
                Fragment fragment = (Fragment) this.g.get(i2);
                if (fragment != null && fragment.b(menu, menuInflater)) {
                    z2 = true;
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(fragment);
                }
                i2++;
                z2 = z2;
            }
        } else {
            z2 = false;
        }
        if (this.j != null) {
            for (int i3 = 0; i3 < this.j.size(); i3++) {
                Fragment fragment2 = (Fragment) this.j.get(i3);
                if (arrayList == null || !arrayList.contains(fragment2)) {
                    fragment2.q();
                }
            }
        }
        this.j = arrayList;
        return z2;
    }

    public boolean a(MenuItem menuItem) {
        if (this.g == null) {
            return false;
        }
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            Fragment fragment = (Fragment) this.g.get(i2);
            if (fragment != null && fragment.c(menuItem)) {
                return true;
            }
        }
        return false;
    }

    public void b(int i2) {
        synchronized (this) {
            this.k.set(i2, null);
            if (this.l == null) {
                this.l = new ArrayList();
            }
            if (f29a) {
                Log.v("FragmentManager", "Freeing back stack index " + i2);
            }
            this.l.add(Integer.valueOf(i2));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.t.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public void b(Fragment fragment) {
        a(fragment, this.n, 0, 0, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.t.a(int, int, int, boolean):void
      android.support.v4.app.t.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.t.a(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    public void b(Fragment fragment, int i2, int i3) {
        if (f29a) {
            Log.v("FragmentManager", "hide: " + fragment);
        }
        if (!fragment.A) {
            fragment.A = true;
            if (fragment.J != null) {
                Animation a2 = a(fragment, i2, false, i3);
                if (a2 != null) {
                    fragment.J.startAnimation(a2);
                }
                fragment.J.setVisibility(8);
            }
            if (fragment.m && fragment.E && fragment.F) {
                this.r = true;
            }
            fragment.a(true);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(e eVar) {
        if (this.i == null) {
            this.i = new ArrayList();
        }
        this.i.add(eVar);
        f();
    }

    public void b(Menu menu) {
        if (this.g != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.g.size()) {
                    Fragment fragment = (Fragment) this.g.get(i3);
                    if (fragment != null) {
                        fragment.d(menu);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public boolean b() {
        return e();
    }

    public boolean b(MenuItem menuItem) {
        if (this.g == null) {
            return false;
        }
        for (int i2 = 0; i2 < this.g.size(); i2++) {
            Fragment fragment = (Fragment) this.g.get(i2);
            if (fragment != null && fragment.d(menuItem)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void c(Fragment fragment) {
        if (fragment.g < 0) {
            if (this.h == null || this.h.size() <= 0) {
                if (this.f == null) {
                    this.f = new ArrayList();
                }
                fragment.a(this.f.size(), this.q);
                this.f.add(fragment);
            } else {
                fragment.a(((Integer) this.h.remove(this.h.size() - 1)).intValue(), this.q);
                this.f.set(fragment.g, fragment);
            }
            if (f29a) {
                Log.v("FragmentManager", "Allocated fragment index " + fragment);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation
     arg types: [android.support.v4.app.Fragment, int, int, int]
     candidates:
      android.support.v4.app.t.a(int, int, int, boolean):void
      android.support.v4.app.t.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      android.support.v4.app.t.a(android.os.Handler, java.lang.String, int, int):boolean
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, boolean, int):android.view.animation.Animation */
    public void c(Fragment fragment, int i2, int i3) {
        if (f29a) {
            Log.v("FragmentManager", "show: " + fragment);
        }
        if (fragment.A) {
            fragment.A = false;
            if (fragment.J != null) {
                Animation a2 = a(fragment, i2, true, i3);
                if (a2 != null) {
                    fragment.J.startAnimation(a2);
                }
                fragment.J.setVisibility(0);
            }
            if (fragment.m && fragment.E && fragment.F) {
                this.r = true;
            }
            fragment.a(false);
        }
    }

    public boolean c() {
        u();
        b();
        return a(this.o.f27a, (String) null, -1, 0);
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (this.f != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.f.size()) {
                    Fragment fragment = (Fragment) this.f.get(i3);
                    if (fragment != null) {
                        a(fragment);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d(Fragment fragment) {
        if (fragment.g >= 0) {
            if (f29a) {
                Log.v("FragmentManager", "Freeing fragment index " + fragment);
            }
            this.f.set(fragment.g, null);
            if (this.h == null) {
                this.h = new ArrayList();
            }
            this.h.add(Integer.valueOf(fragment.g));
            this.o.a(fragment.h);
            fragment.o();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.t.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void d(Fragment fragment, int i2, int i3) {
        if (f29a) {
            Log.v("FragmentManager", "detach: " + fragment);
        }
        if (!fragment.B) {
            fragment.B = true;
            if (fragment.m) {
                if (this.g != null) {
                    if (f29a) {
                        Log.v("FragmentManager", "remove from detach: " + fragment);
                    }
                    this.g.remove(fragment);
                }
                if (fragment.E && fragment.F) {
                    this.r = true;
                }
                fragment.m = false;
                a(fragment, 1, i2, i3, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void e(Fragment fragment) {
        if (fragment.K != null) {
            if (this.x == null) {
                this.x = new SparseArray();
            } else {
                this.x.clear();
            }
            fragment.K.saveHierarchyState(this.x);
            if (this.x.size() > 0) {
                fragment.f = this.x;
                this.x = null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.t.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public void e(Fragment fragment, int i2, int i3) {
        if (f29a) {
            Log.v("FragmentManager", "attach: " + fragment);
        }
        if (fragment.B) {
            fragment.B = false;
            if (!fragment.m) {
                if (this.g == null) {
                    this.g = new ArrayList();
                }
                if (this.g.contains(fragment)) {
                    throw new IllegalStateException("Fragment already added: " + fragment);
                }
                if (f29a) {
                    Log.v("FragmentManager", "add from attach: " + fragment);
                }
                this.g.add(fragment);
                fragment.m = true;
                if (fragment.E && fragment.F) {
                    this.r = true;
                }
                a(fragment, this.n, i2, i3, false);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0083, code lost:
        r6.e = true;
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0086, code lost:
        if (r1 >= r3) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0088, code lost:
        r6.d[r1].run();
        r6.d[r1] = null;
        r1 = r1 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean e() {
        /*
            r6 = this;
            r0 = 1
            r2 = 0
            boolean r1 = r6.e
            if (r1 == 0) goto L_0x000e
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Recursive entry to executePendingTransactions"
            r0.<init>(r1)
            throw r0
        L_0x000e:
            android.os.Looper r1 = android.os.Looper.myLooper()
            android.support.v4.app.o r3 = r6.o
            android.os.Handler r3 = r3.f27a
            android.os.Looper r3 = r3.getLooper()
            if (r1 == r3) goto L_0x0024
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Must be called from main thread of process"
            r0.<init>(r1)
            throw r0
        L_0x0024:
            r1 = r2
        L_0x0025:
            monitor-enter(r6)
            java.util.ArrayList r3 = r6.c     // Catch:{ all -> 0x0097 }
            if (r3 == 0) goto L_0x0032
            java.util.ArrayList r3 = r6.c     // Catch:{ all -> 0x0097 }
            int r3 = r3.size()     // Catch:{ all -> 0x0097 }
            if (r3 != 0) goto L_0x005a
        L_0x0032:
            monitor-exit(r6)     // Catch:{ all -> 0x0097 }
            boolean r0 = r6.v
            if (r0 == 0) goto L_0x00a5
            r3 = r2
            r4 = r2
        L_0x0039:
            java.util.ArrayList r0 = r6.f
            int r0 = r0.size()
            if (r3 >= r0) goto L_0x009e
            java.util.ArrayList r0 = r6.f
            java.lang.Object r0 = r0.get(r3)
            android.support.v4.app.Fragment r0 = (android.support.v4.app.Fragment) r0
            if (r0 == 0) goto L_0x0056
            android.support.v4.app.am r5 = r0.N
            if (r5 == 0) goto L_0x0056
            android.support.v4.app.am r0 = r0.N
            boolean r0 = r0.a()
            r4 = r4 | r0
        L_0x0056:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0039
        L_0x005a:
            java.util.ArrayList r1 = r6.c     // Catch:{ all -> 0x0097 }
            int r3 = r1.size()     // Catch:{ all -> 0x0097 }
            java.lang.Runnable[] r1 = r6.d     // Catch:{ all -> 0x0097 }
            if (r1 == 0) goto L_0x0069
            java.lang.Runnable[] r1 = r6.d     // Catch:{ all -> 0x0097 }
            int r1 = r1.length     // Catch:{ all -> 0x0097 }
            if (r1 >= r3) goto L_0x006d
        L_0x0069:
            java.lang.Runnable[] r1 = new java.lang.Runnable[r3]     // Catch:{ all -> 0x0097 }
            r6.d = r1     // Catch:{ all -> 0x0097 }
        L_0x006d:
            java.util.ArrayList r1 = r6.c     // Catch:{ all -> 0x0097 }
            java.lang.Runnable[] r4 = r6.d     // Catch:{ all -> 0x0097 }
            r1.toArray(r4)     // Catch:{ all -> 0x0097 }
            java.util.ArrayList r1 = r6.c     // Catch:{ all -> 0x0097 }
            r1.clear()     // Catch:{ all -> 0x0097 }
            android.support.v4.app.o r1 = r6.o     // Catch:{ all -> 0x0097 }
            android.os.Handler r1 = r1.f27a     // Catch:{ all -> 0x0097 }
            java.lang.Runnable r4 = r6.y     // Catch:{ all -> 0x0097 }
            r1.removeCallbacks(r4)     // Catch:{ all -> 0x0097 }
            monitor-exit(r6)     // Catch:{ all -> 0x0097 }
            r6.e = r0
            r1 = r2
        L_0x0086:
            if (r1 >= r3) goto L_0x009a
            java.lang.Runnable[] r4 = r6.d
            r4 = r4[r1]
            r4.run()
            java.lang.Runnable[] r4 = r6.d
            r5 = 0
            r4[r1] = r5
            int r1 = r1 + 1
            goto L_0x0086
        L_0x0097:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0097 }
            throw r0
        L_0x009a:
            r6.e = r2
            r1 = r0
            goto L_0x0025
        L_0x009e:
            if (r4 != 0) goto L_0x00a5
            r6.v = r2
            r6.d()
        L_0x00a5:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.t.e():boolean");
    }

    /* access modifiers changed from: package-private */
    public Bundle f(Fragment fragment) {
        Bundle bundle;
        if (this.w == null) {
            this.w = new Bundle();
        }
        fragment.i(this.w);
        if (!this.w.isEmpty()) {
            bundle = this.w;
            this.w = null;
        } else {
            bundle = null;
        }
        if (fragment.J != null) {
            e(fragment);
        }
        if (fragment.f != null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putSparseParcelableArray("android:view_state", fragment.f);
        }
        if (!fragment.M) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            bundle.putBoolean("android:user_visible_hint", fragment.M);
        }
        return bundle;
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.m != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.m.size()) {
                    ((s) this.m.get(i3)).a();
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public ArrayList g() {
        ArrayList arrayList = null;
        if (this.f != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= this.f.size()) {
                    break;
                }
                Fragment fragment = (Fragment) this.f.get(i3);
                if (fragment != null && fragment.C) {
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(fragment);
                    fragment.D = true;
                    fragment.k = fragment.j != null ? fragment.j.g : -1;
                    if (f29a) {
                        Log.v("FragmentManager", "retainNonConfig: keeping retained " + fragment);
                    }
                }
                i2 = i3 + 1;
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public Parcelable h() {
        int[] iArr;
        int size;
        int size2;
        boolean z2;
        BackStackState[] backStackStateArr = null;
        e();
        if (b) {
            this.s = true;
        }
        if (this.f == null || this.f.size() <= 0) {
            return null;
        }
        int size3 = this.f.size();
        FragmentState[] fragmentStateArr = new FragmentState[size3];
        int i2 = 0;
        boolean z3 = false;
        while (i2 < size3) {
            Fragment fragment = (Fragment) this.f.get(i2);
            if (fragment != null) {
                if (fragment.g < 0) {
                    a(new IllegalStateException("Failure saving state: active " + fragment + " has cleared index: " + fragment.g));
                }
                FragmentState fragmentState = new FragmentState(fragment);
                fragmentStateArr[i2] = fragmentState;
                if (fragment.b <= 0 || fragmentState.j != null) {
                    fragmentState.j = fragment.e;
                } else {
                    fragmentState.j = f(fragment);
                    if (fragment.j != null) {
                        if (fragment.j.g < 0) {
                            a(new IllegalStateException("Failure saving state: " + fragment + " has target not in fragment manager: " + fragment.j));
                        }
                        if (fragmentState.j == null) {
                            fragmentState.j = new Bundle();
                        }
                        a(fragmentState.j, "android:target_state", fragment.j);
                        if (fragment.l != 0) {
                            fragmentState.j.putInt("android:target_req_state", fragment.l);
                        }
                    }
                }
                if (f29a) {
                    Log.v("FragmentManager", "Saved state of " + fragment + ": " + fragmentState.j);
                }
                z2 = true;
            } else {
                z2 = z3;
            }
            i2++;
            z3 = z2;
        }
        if (z3) {
            if (this.g == null || (size2 = this.g.size()) <= 0) {
                iArr = null;
            } else {
                iArr = new int[size2];
                for (int i3 = 0; i3 < size2; i3++) {
                    iArr[i3] = ((Fragment) this.g.get(i3)).g;
                    if (iArr[i3] < 0) {
                        a(new IllegalStateException("Failure saving state: active " + this.g.get(i3) + " has cleared index: " + iArr[i3]));
                    }
                    if (f29a) {
                        Log.v("FragmentManager", "saveAllState: adding fragment #" + i3 + ": " + this.g.get(i3));
                    }
                }
            }
            if (this.i != null && (size = this.i.size()) > 0) {
                backStackStateArr = new BackStackState[size];
                for (int i4 = 0; i4 < size; i4++) {
                    backStackStateArr[i4] = new BackStackState(this, (e) this.i.get(i4));
                    if (f29a) {
                        Log.v("FragmentManager", "saveAllState: adding back stack #" + i4 + ": " + this.i.get(i4));
                    }
                }
            }
            FragmentManagerState fragmentManagerState = new FragmentManagerState();
            fragmentManagerState.f6a = fragmentStateArr;
            fragmentManagerState.b = iArr;
            fragmentManagerState.c = backStackStateArr;
            return fragmentManagerState;
        } else if (!f29a) {
            return null;
        } else {
            Log.v("FragmentManager", "saveAllState: no fragments!");
            return null;
        }
    }

    public void i() {
        this.s = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.t.a(int, android.support.v4.app.e):void
      android.support.v4.app.t.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(int, boolean):void */
    public void j() {
        this.s = false;
        a(1, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.t.a(int, android.support.v4.app.e):void
      android.support.v4.app.t.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(int, boolean):void */
    public void k() {
        this.s = false;
        a(2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.t.a(int, android.support.v4.app.e):void
      android.support.v4.app.t.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(int, boolean):void */
    public void l() {
        this.s = false;
        a(4, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.t.a(int, android.support.v4.app.e):void
      android.support.v4.app.t.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(int, boolean):void */
    public void m() {
        this.s = false;
        a(5, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.t.a(int, android.support.v4.app.e):void
      android.support.v4.app.t.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(int, boolean):void */
    public void n() {
        a(4, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.t.a(int, android.support.v4.app.e):void
      android.support.v4.app.t.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(int, boolean):void */
    public void o() {
        this.s = true;
        a(3, false);
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [android.view.View, java.lang.String] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void
     arg types: [android.support.v4.app.Fragment, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.t.a(int, android.support.v4.app.e):void
      android.support.v4.app.t.a(int, boolean):void
      android.support.v4.app.t.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void
     arg types: [android.support.v4.app.Fragment, int, int, int, int]
     candidates:
      android.support.v4.app.t.a(android.content.Context, float, float, float, float):android.view.animation.Animation
      android.support.v4.app.t.a(android.support.v4.app.Fragment, int, int, int, boolean):void */
    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        Fragment fragment;
        ? r4 = 0;
        if (!"fragment".equals(str)) {
            return r4;
        }
        String attributeValue = attributeSet.getAttributeValue(r4, "class");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, w.f32a);
        String string = attributeValue == null ? obtainStyledAttributes.getString(0) : attributeValue;
        int resourceId = obtainStyledAttributes.getResourceId(1, -1);
        String string2 = obtainStyledAttributes.getString(2);
        obtainStyledAttributes.recycle();
        if (!Fragment.b(this.o, string)) {
            return r4;
        }
        int id = r4 != 0 ? r4.getId() : 0;
        if (id == -1 && resourceId == -1 && string2 == null) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Must specify unique android:id, android:tag, or have a parent with an id for " + string);
        }
        Fragment a2 = resourceId != -1 ? a(resourceId) : r4;
        if (a2 == null && string2 != null) {
            a2 = a(string2);
        }
        if (a2 == null && id != -1) {
            a2 = a(id);
        }
        if (f29a) {
            Log.v("FragmentManager", "onCreateView: id=0x" + Integer.toHexString(resourceId) + " fname=" + string + " existing=" + a2);
        }
        if (a2 == null) {
            Fragment a3 = Fragment.a(context, string);
            a3.p = true;
            a3.x = resourceId != 0 ? resourceId : id;
            a3.y = id;
            a3.z = string2;
            a3.q = true;
            a3.t = this;
            a3.a(this.o, attributeSet, a3.e);
            a(a3, true);
            fragment = a3;
        } else if (a2.q) {
            throw new IllegalArgumentException(attributeSet.getPositionDescription() + ": Duplicate id 0x" + Integer.toHexString(resourceId) + ", tag " + string2 + ", or parent id 0x" + Integer.toHexString(id) + " with another fragment for " + string);
        } else {
            a2.q = true;
            if (!a2.D) {
                a2.a(this.o, attributeSet, a2.e);
            }
            fragment = a2;
        }
        if (this.n >= 1 || !fragment.p) {
            b(fragment);
        } else {
            a(fragment, 1, 0, 0, false);
        }
        if (fragment.J == null) {
            throw new IllegalStateException("Fragment " + string + " did not create a view.");
        }
        if (resourceId != 0) {
            fragment.J.setId(resourceId);
        }
        if (fragment.J.getTag() == null) {
            fragment.J.setTag(string2);
        }
        return fragment.J;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.t.a(int, android.support.v4.app.e):void
      android.support.v4.app.t.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(int, boolean):void */
    public void p() {
        a(2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.t.a(int, android.support.v4.app.e):void
      android.support.v4.app.t.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(int, boolean):void */
    public void q() {
        a(1, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.t.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v4.app.t.a(android.os.Bundle, java.lang.String):android.support.v4.app.Fragment
      android.support.v4.app.t.a(int, android.support.v4.app.e):void
      android.support.v4.app.t.a(android.os.Parcelable, java.util.ArrayList):void
      android.support.v4.app.t.a(android.support.v4.app.Fragment, boolean):void
      android.support.v4.app.t.a(java.lang.Runnable, boolean):void
      android.support.v4.app.t.a(android.view.Menu, android.view.MenuInflater):boolean
      android.support.v4.app.t.a(int, boolean):void */
    public void r() {
        this.t = true;
        e();
        a(0, false);
        this.o = null;
        this.p = null;
        this.q = null;
    }

    public void s() {
        if (this.g != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < this.g.size()) {
                    Fragment fragment = (Fragment) this.g.get(i3);
                    if (fragment != null) {
                        fragment.C();
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public LayoutInflater.Factory t() {
        return this;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("FragmentManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        if (this.q != null) {
            d.a(this.q, sb);
        } else {
            d.a(this.o, sb);
        }
        sb.append("}}");
        return sb.toString();
    }
}
