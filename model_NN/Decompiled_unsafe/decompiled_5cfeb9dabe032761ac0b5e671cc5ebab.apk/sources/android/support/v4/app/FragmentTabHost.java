package android.support.v4.app;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.TabHost;
import java.util.ArrayList;

public class FragmentTabHost extends TabHost implements TabHost.OnTabChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList f8a;
    private Context b;
    private r c;
    private int d;
    private TabHost.OnTabChangeListener e;
    private ab f;
    private boolean g;

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new aa();

        /* renamed from: a  reason: collision with root package name */
        String f9a;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f9a = parcel.readString();
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentTabHost.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " curTab=" + this.f9a + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.f9a);
        }
    }

    private ac a(String str, ac acVar) {
        ab abVar = null;
        int i = 0;
        while (i < this.f8a.size()) {
            ab abVar2 = (ab) this.f8a.get(i);
            if (!abVar2.f10a.equals(str)) {
                abVar2 = abVar;
            }
            i++;
            abVar = abVar2;
        }
        if (abVar == null) {
            throw new IllegalStateException("No tab known for tag " + str);
        }
        if (this.f != abVar) {
            if (acVar == null) {
                acVar = this.c.a();
            }
            if (!(this.f == null || this.f.d == null)) {
                acVar.a(this.f.d);
            }
            if (abVar != null) {
                if (abVar.d == null) {
                    Fragment unused = abVar.d = Fragment.a(this.b, abVar.b.getName(), abVar.c);
                    acVar.a(this.d, abVar.d, abVar.f10a);
                } else {
                    acVar.b(abVar.d);
                }
            }
            this.f = abVar;
        }
        return acVar;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String currentTabTag = getCurrentTabTag();
        ac acVar = null;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f8a.size()) {
                break;
            }
            ab abVar = (ab) this.f8a.get(i2);
            Fragment unused = abVar.d = this.c.a(abVar.f10a);
            if (abVar.d != null && !abVar.d.f()) {
                if (abVar.f10a.equals(currentTabTag)) {
                    this.f = abVar;
                } else {
                    if (acVar == null) {
                        acVar = this.c.a();
                    }
                    acVar.a(abVar.d);
                }
            }
            i = i2 + 1;
        }
        this.g = true;
        ac a2 = a(currentTabTag, acVar);
        if (a2 != null) {
            a2.a();
            this.c.b();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.g = false;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCurrentTabByTag(savedState.f9a);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f9a = getCurrentTabTag();
        return savedState;
    }

    public void onTabChanged(String str) {
        ac a2;
        if (this.g && (a2 = a(str, null)) != null) {
            a2.a();
        }
        if (this.e != null) {
            this.e.onTabChanged(str);
        }
    }

    public void setOnTabChangedListener(TabHost.OnTabChangeListener onTabChangeListener) {
        this.e = onTabChangeListener;
    }

    @Deprecated
    public void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }
}
