package android.support.v4.app;

import android.graphics.Rect;
import android.transition.Transition;

final class ag extends Transition.EpicenterCallback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ai f13a;
    private Rect b;

    ag(ai aiVar) {
        this.f13a = aiVar;
    }

    public Rect onGetEpicenter(Transition transition) {
        if (this.b == null && this.f13a.f15a != null) {
            this.b = ad.c(this.f13a.f15a);
        }
        return this.b;
    }
}
