package android.support.v4.app;

import android.support.v4.f.a;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;

class g implements ViewTreeObserver.OnPreDrawListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f22a;
    final /* synthetic */ Object b;
    final /* synthetic */ ArrayList c;
    final /* synthetic */ j d;
    final /* synthetic */ boolean e;
    final /* synthetic */ Fragment f;
    final /* synthetic */ Fragment g;
    final /* synthetic */ e h;

    g(e eVar, View view, Object obj, ArrayList arrayList, j jVar, boolean z, Fragment fragment, Fragment fragment2) {
        this.h = eVar;
        this.f22a = view;
        this.b = obj;
        this.c = arrayList;
        this.d = jVar;
        this.e = z;
        this.f = fragment;
        this.g = fragment2;
    }

    public boolean onPreDraw() {
        this.f22a.getViewTreeObserver().removeOnPreDrawListener(this);
        if (this.b == null) {
            return true;
        }
        ad.a(this.b, this.c);
        this.c.clear();
        a a2 = this.h.a(this.d, this.e, this.f);
        if (a2.isEmpty()) {
            this.c.add(this.d.d);
        } else {
            this.c.addAll(a2.values());
        }
        ad.b(this.b, this.c);
        this.h.a(a2, this.d);
        this.h.a(this.d, this.f, this.g, this.e, a2);
        return true;
    }
}
