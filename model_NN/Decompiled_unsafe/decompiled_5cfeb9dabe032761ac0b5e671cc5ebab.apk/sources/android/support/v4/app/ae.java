package android.support.v4.app;

import android.graphics.Rect;
import android.transition.Transition;

final class ae extends Transition.EpicenterCallback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Rect f11a;

    ae(Rect rect) {
        this.f11a = rect;
    }

    public Rect onGetEpicenter(Transition transition) {
        return this.f11a;
    }
}
