package android.support.v4.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.a.e;

public class ao {

    /* renamed from: a  reason: collision with root package name */
    private static final ap f18a;

    static {
        if (Build.VERSION.SDK_INT >= 16) {
            f18a = new ar();
        } else {
            f18a = new aq();
        }
    }

    public static Intent a(Activity activity) {
        return f18a.a(activity);
    }

    public static Intent a(Context context, ComponentName componentName) {
        String b = b(context, componentName);
        if (b == null) {
            return null;
        }
        ComponentName componentName2 = new ComponentName(componentName.getPackageName(), b);
        return b(context, componentName2) == null ? e.a(componentName2) : new Intent().setComponent(componentName2);
    }

    public static boolean a(Activity activity, Intent intent) {
        return f18a.a(activity, intent);
    }

    public static String b(Activity activity) {
        try {
            return b(activity, activity.getComponentName());
        } catch (PackageManager.NameNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static String b(Context context, ComponentName componentName) {
        return f18a.a(context, context.getPackageManager().getActivityInfo(componentName, 128));
    }

    public static void b(Activity activity, Intent intent) {
        f18a.b(activity, intent);
    }
}
