package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;

final class BackStackState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new k();

    /* renamed from: a  reason: collision with root package name */
    final int[] f3a;
    final int b;
    final int c;
    final String d;
    final int e;
    final int f;
    final CharSequence g;
    final int h;
    final CharSequence i;
    final ArrayList j;
    final ArrayList k;

    public BackStackState(Parcel parcel) {
        this.f3a = parcel.createIntArray();
        this.b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readString();
        this.e = parcel.readInt();
        this.f = parcel.readInt();
        this.g = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.h = parcel.readInt();
        this.i = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.j = parcel.createStringArrayList();
        this.k = parcel.createStringArrayList();
    }

    public BackStackState(t tVar, e eVar) {
        int i2 = 0;
        for (i iVar = eVar.b; iVar != null; iVar = iVar.f24a) {
            if (iVar.i != null) {
                i2 += iVar.i.size();
            }
        }
        this.f3a = new int[(i2 + (eVar.d * 7))];
        if (!eVar.k) {
            throw new IllegalStateException("Not on back stack");
        }
        int i3 = 0;
        for (i iVar2 = eVar.b; iVar2 != null; iVar2 = iVar2.f24a) {
            int i4 = i3 + 1;
            this.f3a[i3] = iVar2.c;
            int i5 = i4 + 1;
            this.f3a[i4] = iVar2.d != null ? iVar2.d.g : -1;
            int i6 = i5 + 1;
            this.f3a[i5] = iVar2.e;
            int i7 = i6 + 1;
            this.f3a[i6] = iVar2.f;
            int i8 = i7 + 1;
            this.f3a[i7] = iVar2.g;
            int i9 = i8 + 1;
            this.f3a[i8] = iVar2.h;
            if (iVar2.i != null) {
                int size = iVar2.i.size();
                int i10 = i9 + 1;
                this.f3a[i9] = size;
                int i11 = 0;
                while (i11 < size) {
                    this.f3a[i10] = ((Fragment) iVar2.i.get(i11)).g;
                    i11++;
                    i10++;
                }
                i3 = i10;
            } else {
                i3 = i9 + 1;
                this.f3a[i9] = 0;
            }
        }
        this.b = eVar.i;
        this.c = eVar.j;
        this.d = eVar.m;
        this.e = eVar.o;
        this.f = eVar.p;
        this.g = eVar.q;
        this.h = eVar.r;
        this.i = eVar.s;
        this.j = eVar.t;
        this.k = eVar.u;
    }

    public e a(t tVar) {
        e eVar = new e(tVar);
        int i2 = 0;
        int i3 = 0;
        while (i3 < this.f3a.length) {
            i iVar = new i();
            int i4 = i3 + 1;
            iVar.c = this.f3a[i3];
            if (t.f29a) {
                Log.v("FragmentManager", "Instantiate " + eVar + " op #" + i2 + " base fragment #" + this.f3a[i4]);
            }
            int i5 = i4 + 1;
            int i6 = this.f3a[i4];
            if (i6 >= 0) {
                iVar.d = (Fragment) tVar.f.get(i6);
            } else {
                iVar.d = null;
            }
            int i7 = i5 + 1;
            iVar.e = this.f3a[i5];
            int i8 = i7 + 1;
            iVar.f = this.f3a[i7];
            int i9 = i8 + 1;
            iVar.g = this.f3a[i8];
            int i10 = i9 + 1;
            iVar.h = this.f3a[i9];
            int i11 = i10 + 1;
            int i12 = this.f3a[i10];
            if (i12 > 0) {
                iVar.i = new ArrayList(i12);
                int i13 = 0;
                while (i13 < i12) {
                    if (t.f29a) {
                        Log.v("FragmentManager", "Instantiate " + eVar + " set remove fragment #" + this.f3a[i11]);
                    }
                    iVar.i.add((Fragment) tVar.f.get(this.f3a[i11]));
                    i13++;
                    i11++;
                }
            }
            eVar.a(iVar);
            i2++;
            i3 = i11;
        }
        eVar.i = this.b;
        eVar.j = this.c;
        eVar.m = this.d;
        eVar.o = this.e;
        eVar.k = true;
        eVar.p = this.f;
        eVar.q = this.g;
        eVar.r = this.h;
        eVar.s = this.i;
        eVar.t = this.j;
        eVar.u = this.k;
        eVar.a(1);
        return eVar;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeIntArray(this.f3a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        parcel.writeString(this.d);
        parcel.writeInt(this.e);
        parcel.writeInt(this.f);
        TextUtils.writeToParcel(this.g, parcel, 0);
        parcel.writeInt(this.h);
        TextUtils.writeToParcel(this.i, parcel, 0);
        parcel.writeStringList(this.j);
        parcel.writeStringList(this.k);
    }
}
