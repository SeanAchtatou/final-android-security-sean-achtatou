package android.support.v4.app;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

final class FragmentState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new y();

    /* renamed from: a  reason: collision with root package name */
    final String f7a;
    final int b;
    final boolean c;
    final int d;
    final int e;
    final String f;
    final boolean g;
    final boolean h;
    final Bundle i;
    Bundle j;
    Fragment k;

    public FragmentState(Parcel parcel) {
        boolean z = true;
        this.f7a = parcel.readString();
        this.b = parcel.readInt();
        this.c = parcel.readInt() != 0;
        this.d = parcel.readInt();
        this.e = parcel.readInt();
        this.f = parcel.readString();
        this.g = parcel.readInt() != 0;
        this.h = parcel.readInt() == 0 ? false : z;
        this.i = parcel.readBundle();
        this.j = parcel.readBundle();
    }

    public FragmentState(Fragment fragment) {
        this.f7a = fragment.getClass().getName();
        this.b = fragment.g;
        this.c = fragment.p;
        this.d = fragment.x;
        this.e = fragment.y;
        this.f = fragment.z;
        this.g = fragment.C;
        this.h = fragment.B;
        this.i = fragment.i;
    }

    public Fragment a(o oVar, Fragment fragment) {
        if (this.k != null) {
            return this.k;
        }
        if (this.i != null) {
            this.i.setClassLoader(oVar.getClassLoader());
        }
        this.k = Fragment.a(oVar, this.f7a, this.i);
        if (this.j != null) {
            this.j.setClassLoader(oVar.getClassLoader());
            this.k.e = this.j;
        }
        this.k.a(this.b, fragment);
        this.k.p = this.c;
        this.k.r = true;
        this.k.x = this.d;
        this.k.y = this.e;
        this.k.z = this.f;
        this.k.C = this.g;
        this.k.B = this.h;
        this.k.t = oVar.b;
        if (t.f29a) {
            Log.v("FragmentManager", "Instantiated fragment " + this.k);
        }
        return this.k;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int i3 = 1;
        parcel.writeString(this.f7a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c ? 1 : 0);
        parcel.writeInt(this.d);
        parcel.writeInt(this.e);
        parcel.writeString(this.f);
        parcel.writeInt(this.g ? 1 : 0);
        if (!this.h) {
            i3 = 0;
        }
        parcel.writeInt(i3);
        parcel.writeBundle(this.i);
        parcel.writeBundle(this.j);
    }
}
