package android.support.v4.widget;

import android.content.Context;
import android.os.Build;
import android.view.animation.Interpolator;

public class af {

    /* renamed from: a  reason: collision with root package name */
    Object f85a;
    ag b;

    private af(int i, Context context, Interpolator interpolator) {
        if (i >= 14) {
            this.b = new aj();
        } else if (i >= 9) {
            this.b = new ai();
        } else {
            this.b = new ah();
        }
        this.f85a = this.b.a(context, interpolator);
    }

    af(Context context, Interpolator interpolator) {
        this(Build.VERSION.SDK_INT, context, interpolator);
    }

    public static af a(Context context) {
        return a(context, null);
    }

    public static af a(Context context, Interpolator interpolator) {
        return new af(context, interpolator);
    }

    public int a() {
        return this.b.a(this.f85a);
    }

    public void a(int i, int i2, int i3, int i4, int i5) {
        this.b.a(this.f85a, i, i2, i3, i4, i5);
    }

    public void a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.b.a(this.f85a, i, i2, i3, i4, i5, i6, i7, i8);
    }

    public int b() {
        return this.b.b(this.f85a);
    }

    public int c() {
        return this.b.e(this.f85a);
    }

    public int d() {
        return this.b.f(this.f85a);
    }

    public boolean e() {
        return this.b.c(this.f85a);
    }

    public void f() {
        this.b.d(this.f85a);
    }
}
