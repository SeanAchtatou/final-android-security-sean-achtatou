package android.support.v4.widget;

import android.graphics.Canvas;
import android.os.Build;

public class t {
    private static final w b;

    /* renamed from: a  reason: collision with root package name */
    private Object f98a;

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            b = new v();
        } else {
            b = new u();
        }
    }

    public void a(int i, int i2) {
        b.a(this.f98a, i, i2);
    }

    public boolean a() {
        return b.a(this.f98a);
    }

    public boolean a(float f) {
        return b.a(this.f98a, f);
    }

    public boolean a(Canvas canvas) {
        return b.a(this.f98a, canvas);
    }

    public void b() {
        b.b(this.f98a);
    }

    public boolean c() {
        return b.c(this.f98a);
    }
}
