package android.support.v4.widget;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ai;
import android.support.v4.view.au;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public class SlidingPaneLayout extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    static final aq f82a;
    private int b;
    private int c;
    private Drawable d;
    private Drawable e;
    private final int f;
    private boolean g;
    private View h;
    private float i;
    private float j;
    private int k;
    private boolean l;
    private int m;
    private float n;
    private float o;
    private ao p;
    private final au q;
    private boolean r;
    private boolean s;
    private final Rect t;
    /* access modifiers changed from: private */
    public final ArrayList u;

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new ap();

        /* renamed from: a  reason: collision with root package name */
        boolean f83a;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f83a = parcel.readInt() != 0;
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f83a ? 1 : 0);
        }
    }

    static {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 17) {
            f82a = new at();
        } else if (i2 >= 16) {
            f82a = new as();
        } else {
            f82a = new ar();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:8:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(float r10) {
        /*
            r9 = this;
            r1 = 0
            r8 = 1065353216(0x3f800000, float:1.0)
            boolean r3 = r9.f()
            android.view.View r0 = r9.h
            android.view.ViewGroup$LayoutParams r0 = r0.getLayoutParams()
            android.support.v4.widget.an r0 = (android.support.v4.widget.an) r0
            boolean r2 = r0.c
            if (r2 == 0) goto L_0x0030
            if (r3 == 0) goto L_0x002d
            int r0 = r0.rightMargin
        L_0x0017:
            if (r0 > 0) goto L_0x0030
            r0 = 1
        L_0x001a:
            int r4 = r9.getChildCount()
            r2 = r1
        L_0x001f:
            if (r2 >= r4) goto L_0x005d
            android.view.View r5 = r9.getChildAt(r2)
            android.view.View r1 = r9.h
            if (r5 != r1) goto L_0x0032
        L_0x0029:
            int r1 = r2 + 1
            r2 = r1
            goto L_0x001f
        L_0x002d:
            int r0 = r0.leftMargin
            goto L_0x0017
        L_0x0030:
            r0 = r1
            goto L_0x001a
        L_0x0032:
            float r1 = r9.j
            float r1 = r8 - r1
            int r6 = r9.m
            float r6 = (float) r6
            float r1 = r1 * r6
            int r1 = (int) r1
            r9.j = r10
            float r6 = r8 - r10
            int r7 = r9.m
            float r7 = (float) r7
            float r6 = r6 * r7
            int r6 = (int) r6
            int r1 = r1 - r6
            if (r3 == 0) goto L_0x0048
            int r1 = -r1
        L_0x0048:
            r5.offsetLeftAndRight(r1)
            if (r0 == 0) goto L_0x0029
            if (r3 == 0) goto L_0x0058
            float r1 = r9.j
            float r1 = r1 - r8
        L_0x0052:
            int r6 = r9.c
            r9.a(r5, r1, r6)
            goto L_0x0029
        L_0x0058:
            float r1 = r9.j
            float r1 = r8 - r1
            goto L_0x0052
        L_0x005d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.SlidingPaneLayout.a(float):void");
    }

    private void a(View view, float f2, int i2) {
        an anVar = (an) view.getLayoutParams();
        if (f2 > 0.0f && i2 != 0) {
            int i3 = (((int) (((float) ((-16777216 & i2) >>> 24)) * f2)) << 24) | (16777215 & i2);
            if (anVar.d == null) {
                anVar.d = new Paint();
            }
            anVar.d.setColorFilter(new PorterDuffColorFilter(i3, PorterDuff.Mode.SRC_OVER));
            if (au.c(view) != 2) {
                au.a(view, 2, anVar.d);
            }
            d(view);
        } else if (au.c(view) != 0) {
            if (anVar.d != null) {
                anVar.d.setColorFilter(null);
            }
            am amVar = new am(this, view);
            this.u.add(amVar);
            au.a(this, amVar);
        }
    }

    private boolean a(View view, int i2) {
        if (!this.s && !a(0.0f, i2)) {
            return false;
        }
        this.r = false;
        return true;
    }

    private boolean b(View view, int i2) {
        if (!this.s && !a(1.0f, i2)) {
            return false;
        }
        this.r = true;
        return true;
    }

    private static boolean c(View view) {
        if (au.e(view)) {
            return true;
        }
        if (Build.VERSION.SDK_INT >= 18) {
            return false;
        }
        Drawable background = view.getBackground();
        if (background != null) {
            return background.getOpacity() == -1;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void d(View view) {
        f82a.a(this, view);
    }

    private boolean f() {
        return au.d(this) == 1;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (childAt.getVisibility() == 4) {
                childAt.setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view) {
        int i2;
        int i3;
        int i4;
        int i5;
        boolean f2 = f();
        int width = f2 ? getWidth() - getPaddingRight() : getPaddingLeft();
        int paddingLeft = f2 ? getPaddingLeft() : getWidth() - getPaddingRight();
        int paddingTop = getPaddingTop();
        int height = getHeight() - getPaddingBottom();
        if (view == null || !c(view)) {
            i2 = 0;
            i3 = 0;
            i4 = 0;
            i5 = 0;
        } else {
            i5 = view.getLeft();
            i4 = view.getRight();
            i3 = view.getTop();
            i2 = view.getBottom();
        }
        int childCount = getChildCount();
        int i6 = 0;
        while (i6 < childCount) {
            View childAt = getChildAt(i6);
            if (childAt != view) {
                childAt.setVisibility((Math.max(f2 ? paddingLeft : width, childAt.getLeft()) < i5 || Math.max(paddingTop, childAt.getTop()) < i3 || Math.min(f2 ? width : paddingLeft, childAt.getRight()) > i4 || Math.min(height, childAt.getBottom()) > i2) ? 0 : 4);
                i6++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(float f2, int i2) {
        int paddingLeft;
        if (!this.g) {
            return false;
        }
        boolean f3 = f();
        an anVar = (an) this.h.getLayoutParams();
        if (f3) {
            paddingLeft = (int) (((float) getWidth()) - ((((float) (anVar.rightMargin + getPaddingRight())) + (((float) this.k) * f2)) + ((float) this.h.getWidth())));
        } else {
            paddingLeft = (int) (((float) (anVar.leftMargin + getPaddingLeft())) + (((float) this.k) * f2));
        }
        if (!this.q.a(this.h, paddingLeft, this.h.getTop())) {
            return false;
        }
        a();
        au.b(this);
        return true;
    }

    public boolean b() {
        return b(this.h, 0);
    }

    /* access modifiers changed from: package-private */
    public boolean b(View view) {
        if (view == null) {
            return false;
        }
        return this.g && ((an) view.getLayoutParams()).c && this.i > 0.0f;
    }

    public boolean c() {
        return a(this.h, 0);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof an) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        if (!this.q.a(true)) {
            return;
        }
        if (!this.g) {
            this.q.f();
        } else {
            au.b(this);
        }
    }

    public boolean d() {
        return !this.g || this.i == 1.0f;
    }

    public void draw(Canvas canvas) {
        int left;
        int i2;
        super.draw(canvas);
        Drawable drawable = f() ? this.e : this.d;
        View childAt = getChildCount() > 1 ? getChildAt(1) : null;
        if (childAt != null && drawable != null) {
            int top = childAt.getTop();
            int bottom = childAt.getBottom();
            int intrinsicWidth = drawable.getIntrinsicWidth();
            if (f()) {
                i2 = childAt.getRight();
                left = i2 + intrinsicWidth;
            } else {
                left = childAt.getLeft();
                i2 = left - intrinsicWidth;
            }
            drawable.setBounds(i2, top, left, bottom);
            drawable.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        boolean drawChild;
        an anVar = (an) view.getLayoutParams();
        int save = canvas.save(2);
        if (this.g && !anVar.b && this.h != null) {
            canvas.getClipBounds(this.t);
            if (f()) {
                this.t.left = Math.max(this.t.left, this.h.getRight());
            } else {
                this.t.right = Math.min(this.t.right, this.h.getLeft());
            }
            canvas.clipRect(this.t);
        }
        if (Build.VERSION.SDK_INT >= 11) {
            drawChild = super.drawChild(canvas, view, j2);
        } else if (!anVar.c || this.i <= 0.0f) {
            if (view.isDrawingCacheEnabled()) {
                view.setDrawingCacheEnabled(false);
            }
            drawChild = super.drawChild(canvas, view, j2);
        } else {
            if (!view.isDrawingCacheEnabled()) {
                view.setDrawingCacheEnabled(true);
            }
            Bitmap drawingCache = view.getDrawingCache();
            if (drawingCache != null) {
                canvas.drawBitmap(drawingCache, (float) view.getLeft(), (float) view.getTop(), anVar.d);
                drawChild = false;
            } else {
                Log.e("SlidingPaneLayout", "drawChild: child view " + view + " returned null drawing cache");
                drawChild = super.drawChild(canvas, view, j2);
            }
        }
        canvas.restoreToCount(save);
        return drawChild;
    }

    public boolean e() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new an();
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new an(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof ViewGroup.MarginLayoutParams ? new an((ViewGroup.MarginLayoutParams) layoutParams) : new an(layoutParams);
    }

    public int getCoveredFadeColor() {
        return this.c;
    }

    public int getParallaxDistance() {
        return this.m;
    }

    public int getSliderFadeColor() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.s = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.s = true;
        int size = this.u.size();
        for (int i2 = 0; i2 < size; i2++) {
            ((am) this.u.get(i2)).run();
        }
        this.u.clear();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View childAt;
        int a2 = ai.a(motionEvent);
        if (!this.g && a2 == 0 && getChildCount() > 1 && (childAt = getChildAt(1)) != null) {
            this.r = !this.q.b(childAt, (int) motionEvent.getX(), (int) motionEvent.getY());
        }
        if (!this.g || (this.l && a2 != 0)) {
            this.q.e();
            return super.onInterceptTouchEvent(motionEvent);
        } else if (a2 == 3 || a2 == 1) {
            this.q.e();
            return false;
        } else {
            switch (a2) {
                case 0:
                    this.l = false;
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    this.n = x;
                    this.o = y;
                    if (this.q.b(this.h, (int) x, (int) y) && b(this.h)) {
                        z = true;
                        break;
                    }
                    z = false;
                    break;
                case 1:
                default:
                    z = false;
                    break;
                case 2:
                    float x2 = motionEvent.getX();
                    float y2 = motionEvent.getY();
                    float abs = Math.abs(x2 - this.n);
                    float abs2 = Math.abs(y2 - this.o);
                    if (abs > ((float) this.q.d()) && abs2 > abs) {
                        this.q.e();
                        this.l = true;
                        return false;
                    }
                    z = false;
                    break;
            }
            return this.q.a(motionEvent) || z;
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        int width;
        int i10;
        boolean f2 = f();
        if (f2) {
            this.q.a(2);
        } else {
            this.q.a(1);
        }
        int i11 = i4 - i2;
        int paddingRight = f2 ? getPaddingRight() : getPaddingLeft();
        int paddingLeft = f2 ? getPaddingLeft() : getPaddingRight();
        int paddingTop = getPaddingTop();
        int childCount = getChildCount();
        if (this.s) {
            this.i = (!this.g || !this.r) ? 0.0f : 1.0f;
        }
        int i12 = 0;
        int i13 = paddingRight;
        while (i12 < childCount) {
            View childAt = getChildAt(i12);
            if (childAt.getVisibility() == 8) {
                width = paddingRight;
                i10 = i13;
            } else {
                an anVar = (an) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                if (anVar.b) {
                    int min = (Math.min(paddingRight, (i11 - paddingLeft) - this.f) - i13) - (anVar.leftMargin + anVar.rightMargin);
                    this.k = min;
                    int i14 = f2 ? anVar.rightMargin : anVar.leftMargin;
                    anVar.c = ((i13 + i14) + min) + (measuredWidth / 2) > i11 - paddingLeft;
                    int i15 = (int) (((float) min) * this.i);
                    i7 = i13 + i14 + i15;
                    this.i = ((float) i15) / ((float) this.k);
                    i6 = 0;
                } else if (!this.g || this.m == 0) {
                    i6 = 0;
                    i7 = paddingRight;
                } else {
                    i6 = (int) ((1.0f - this.i) * ((float) this.m));
                    i7 = paddingRight;
                }
                if (f2) {
                    i9 = (i11 - i7) + i6;
                    i8 = i9 - measuredWidth;
                } else {
                    i8 = i7 - i6;
                    i9 = i8 + measuredWidth;
                }
                childAt.layout(i8, paddingTop, i9, childAt.getMeasuredHeight() + paddingTop);
                width = childAt.getWidth() + paddingRight;
                i10 = i7;
            }
            i12++;
            paddingRight = width;
            i13 = i10;
        }
        if (this.s) {
            if (this.g) {
                if (this.m != 0) {
                    a(this.i);
                }
                if (((an) this.h.getLayoutParams()).c) {
                    a(this.h, this.i, this.b);
                }
            } else {
                for (int i16 = 0; i16 < childCount; i16++) {
                    a(getChildAt(i16), 0.0f, this.b);
                }
            }
            a(this.h);
        }
        this.s = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int i7;
        int paddingTop;
        int i8;
        int i9;
        boolean z;
        float f2;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        if (mode == 1073741824) {
            if (mode2 == 0) {
                if (!isInEditMode()) {
                    throw new IllegalStateException("Height must not be UNSPECIFIED");
                } else if (mode2 == 0) {
                    i4 = Integer.MIN_VALUE;
                    i5 = size;
                    i6 = 300;
                }
            }
            i4 = mode2;
            i5 = size;
            i6 = size2;
        } else if (!isInEditMode()) {
            throw new IllegalStateException("Width must have an exact value or MATCH_PARENT");
        } else if (mode == Integer.MIN_VALUE) {
            i4 = mode2;
            i5 = size;
            i6 = size2;
        } else {
            if (mode == 0) {
                i4 = mode2;
                i5 = 300;
                i6 = size2;
            }
            i4 = mode2;
            i5 = size;
            i6 = size2;
        }
        switch (i4) {
            case Integer.MIN_VALUE:
                i7 = 0;
                paddingTop = (i6 - getPaddingTop()) - getPaddingBottom();
                break;
            case 1073741824:
                i7 = (i6 - getPaddingTop()) - getPaddingBottom();
                paddingTop = i7;
                break;
            default:
                i7 = 0;
                paddingTop = -1;
                break;
        }
        boolean z2 = false;
        int paddingLeft = (i5 - getPaddingLeft()) - getPaddingRight();
        int childCount = getChildCount();
        if (childCount > 2) {
            Log.e("SlidingPaneLayout", "onMeasure: More than two child views are not supported.");
        }
        this.h = null;
        int i10 = 0;
        int i11 = paddingLeft;
        int i12 = i7;
        float f3 = 0.0f;
        while (i10 < childCount) {
            View childAt = getChildAt(i10);
            an anVar = (an) childAt.getLayoutParams();
            if (childAt.getVisibility() == 8) {
                anVar.c = false;
                i8 = i11;
                f2 = f3;
                i9 = i12;
                z = z2;
            } else {
                if (anVar.f87a > 0.0f) {
                    f3 += anVar.f87a;
                    if (anVar.width == 0) {
                        i8 = i11;
                        f2 = f3;
                        i9 = i12;
                        z = z2;
                    }
                }
                int i13 = anVar.leftMargin + anVar.rightMargin;
                childAt.measure(anVar.width == -2 ? View.MeasureSpec.makeMeasureSpec(paddingLeft - i13, Integer.MIN_VALUE) : anVar.width == -1 ? View.MeasureSpec.makeMeasureSpec(paddingLeft - i13, 1073741824) : View.MeasureSpec.makeMeasureSpec(anVar.width, 1073741824), anVar.height == -2 ? View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE) : anVar.height == -1 ? View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824) : View.MeasureSpec.makeMeasureSpec(anVar.height, 1073741824));
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                if (i4 == Integer.MIN_VALUE && measuredHeight > i12) {
                    i12 = Math.min(measuredHeight, paddingTop);
                }
                int i14 = i11 - measuredWidth;
                boolean z3 = i14 < 0;
                anVar.b = z3;
                boolean z4 = z3 | z2;
                if (anVar.b) {
                    this.h = childAt;
                }
                i8 = i14;
                i9 = i12;
                float f4 = f3;
                z = z4;
                f2 = f4;
            }
            i10++;
            z2 = z;
            i12 = i9;
            f3 = f2;
            i11 = i8;
        }
        if (z2 || f3 > 0.0f) {
            int i15 = paddingLeft - this.f;
            for (int i16 = 0; i16 < childCount; i16++) {
                View childAt2 = getChildAt(i16);
                if (childAt2.getVisibility() != 8) {
                    an anVar2 = (an) childAt2.getLayoutParams();
                    if (childAt2.getVisibility() != 8) {
                        boolean z5 = anVar2.width == 0 && anVar2.f87a > 0.0f;
                        int measuredWidth2 = z5 ? 0 : childAt2.getMeasuredWidth();
                        if (!z2 || childAt2 == this.h) {
                            if (anVar2.f87a > 0.0f) {
                                int makeMeasureSpec = anVar2.width == 0 ? anVar2.height == -2 ? View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE) : anVar2.height == -1 ? View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824) : View.MeasureSpec.makeMeasureSpec(anVar2.height, 1073741824) : View.MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824);
                                if (z2) {
                                    int i17 = paddingLeft - (anVar2.rightMargin + anVar2.leftMargin);
                                    int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i17, 1073741824);
                                    if (measuredWidth2 != i17) {
                                        childAt2.measure(makeMeasureSpec2, makeMeasureSpec);
                                    }
                                } else {
                                    childAt2.measure(View.MeasureSpec.makeMeasureSpec(((int) ((anVar2.f87a * ((float) Math.max(0, i11))) / f3)) + measuredWidth2, 1073741824), makeMeasureSpec);
                                }
                            }
                        } else if (anVar2.width < 0 && (measuredWidth2 > i15 || anVar2.f87a > 0.0f)) {
                            childAt2.measure(View.MeasureSpec.makeMeasureSpec(i15, 1073741824), z5 ? anVar2.height == -2 ? View.MeasureSpec.makeMeasureSpec(paddingTop, Integer.MIN_VALUE) : anVar2.height == -1 ? View.MeasureSpec.makeMeasureSpec(paddingTop, 1073741824) : View.MeasureSpec.makeMeasureSpec(anVar2.height, 1073741824) : View.MeasureSpec.makeMeasureSpec(childAt2.getMeasuredHeight(), 1073741824));
                        }
                    }
                }
            }
        }
        setMeasuredDimension(i5, getPaddingTop() + i12 + getPaddingBottom());
        this.g = z2;
        if (this.q.a() != 0 && !z2) {
            this.q.f();
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.f83a) {
            b();
        } else {
            c();
        }
        this.r = savedState.f83a;
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f83a = e() ? d() : this.r;
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        if (i2 != i4) {
            this.s = true;
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.g) {
            return super.onTouchEvent(motionEvent);
        }
        this.q.b(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.n = x;
                this.o = y;
                return true;
            case 1:
                if (!b(this.h)) {
                    return true;
                }
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                float f2 = x2 - this.n;
                float f3 = y2 - this.o;
                int d2 = this.q.d();
                if ((f2 * f2) + (f3 * f3) >= ((float) (d2 * d2)) || !this.q.b(this.h, (int) x2, (int) y2)) {
                    return true;
                }
                a(this.h, 0);
                return true;
            default:
                return true;
        }
    }

    public void requestChildFocus(View view, View view2) {
        super.requestChildFocus(view, view2);
        if (!isInTouchMode() && !this.g) {
            this.r = view == this.h;
        }
    }

    public void setCoveredFadeColor(int i2) {
        this.c = i2;
    }

    public void setPanelSlideListener(ao aoVar) {
        this.p = aoVar;
    }

    public void setParallaxDistance(int i2) {
        this.m = i2;
        requestLayout();
    }

    @Deprecated
    public void setShadowDrawable(Drawable drawable) {
        setShadowDrawableLeft(drawable);
    }

    public void setShadowDrawableLeft(Drawable drawable) {
        this.d = drawable;
    }

    public void setShadowDrawableRight(Drawable drawable) {
        this.e = drawable;
    }

    @Deprecated
    public void setShadowResource(int i2) {
        setShadowDrawable(getResources().getDrawable(i2));
    }

    public void setShadowResourceLeft(int i2) {
        setShadowDrawableLeft(getResources().getDrawable(i2));
    }

    public void setShadowResourceRight(int i2) {
        setShadowDrawableRight(getResources().getDrawable(i2));
    }

    public void setSliderFadeColor(int i2) {
        this.b = i2;
    }
}
