package android.support.v4.widget;

import android.os.Build;
import android.view.View;
import android.widget.PopupWindow;

public class z {

    /* renamed from: a  reason: collision with root package name */
    static final ac f100a;

    static {
        if (Build.VERSION.SDK_INT >= 19) {
            f100a = new ab();
        } else {
            f100a = new aa();
        }
    }

    public static void a(PopupWindow popupWindow, View view, int i, int i2, int i3) {
        f100a.a(popupWindow, view, i, i2, i3);
    }
}
