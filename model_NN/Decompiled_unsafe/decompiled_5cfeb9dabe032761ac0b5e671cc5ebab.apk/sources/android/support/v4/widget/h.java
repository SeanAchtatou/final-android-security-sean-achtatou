package android.support.v4.widget;

import android.database.DataSetObserver;

class h extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f94a;

    private h(e eVar) {
        this.f94a = eVar;
    }

    public void onChanged() {
        this.f94a.f92a = true;
        this.f94a.notifyDataSetChanged();
    }

    public void onInvalidated() {
        this.f94a.f92a = false;
        this.f94a.notifyDataSetInvalidated();
    }
}
