package android.support.v4.widget;

import android.content.Context;
import android.view.animation.Interpolator;

class ai implements ag {
    ai() {
    }

    public int a(Object obj) {
        return ak.a(obj);
    }

    public Object a(Context context, Interpolator interpolator) {
        return ak.a(context, interpolator);
    }

    public void a(Object obj, int i, int i2, int i3, int i4, int i5) {
        ak.a(obj, i, i2, i3, i4, i5);
    }

    public void a(Object obj, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        ak.a(obj, i, i2, i3, i4, i5, i6, i7, i8);
    }

    public int b(Object obj) {
        return ak.b(obj);
    }

    public boolean c(Object obj) {
        return ak.c(obj);
    }

    public void d(Object obj) {
        ak.d(obj);
    }

    public int e(Object obj) {
        return ak.e(obj);
    }

    public int f(Object obj) {
        return ak.f(obj);
    }
}
