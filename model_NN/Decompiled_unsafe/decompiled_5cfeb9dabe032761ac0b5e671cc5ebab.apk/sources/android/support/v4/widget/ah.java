package android.support.v4.widget;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;

class ah implements ag {
    ah() {
    }

    public int a(Object obj) {
        return ((Scroller) obj).getCurrX();
    }

    public Object a(Context context, Interpolator interpolator) {
        return interpolator != null ? new Scroller(context, interpolator) : new Scroller(context);
    }

    public void a(Object obj, int i, int i2, int i3, int i4, int i5) {
        ((Scroller) obj).startScroll(i, i2, i3, i4, i5);
    }

    public void a(Object obj, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        ((Scroller) obj).fling(i, i2, i3, i4, i5, i6, i7, i8);
    }

    public int b(Object obj) {
        return ((Scroller) obj).getCurrY();
    }

    public boolean c(Object obj) {
        return ((Scroller) obj).computeScrollOffset();
    }

    public void d(Object obj) {
        ((Scroller) obj).abortAnimation();
    }

    public int e(Object obj) {
        return ((Scroller) obj).getFinalX();
    }

    public int f(Object obj) {
        return ((Scroller) obj).getFinalY();
    }
}
