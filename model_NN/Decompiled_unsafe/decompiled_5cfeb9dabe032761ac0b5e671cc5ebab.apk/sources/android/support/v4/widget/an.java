package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class an extends ViewGroup.MarginLayoutParams {
    private static final int[] e = {16843137};

    /* renamed from: a  reason: collision with root package name */
    public float f87a = 0.0f;
    boolean b;
    boolean c;
    Paint d;

    public an() {
        super(-1, -1);
    }

    public an(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, e);
        this.f87a = obtainStyledAttributes.getFloat(0, 0.0f);
        obtainStyledAttributes.recycle();
    }

    public an(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public an(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
    }
}
