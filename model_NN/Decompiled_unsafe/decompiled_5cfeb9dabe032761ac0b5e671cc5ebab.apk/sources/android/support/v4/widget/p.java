package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class p extends ViewGroup.MarginLayoutParams {

    /* renamed from: a  reason: collision with root package name */
    public int f96a = 0;
    float b;
    boolean c;
    boolean d;

    public p(int i, int i2) {
        super(i, i2);
    }

    public p(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, DrawerLayout.b);
        this.f96a = obtainStyledAttributes.getInt(0, 0);
        obtainStyledAttributes.recycle();
    }

    public p(p pVar) {
        super((ViewGroup.MarginLayoutParams) pVar);
        this.f96a = pVar.f96a;
    }

    public p(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public p(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
    }
}
