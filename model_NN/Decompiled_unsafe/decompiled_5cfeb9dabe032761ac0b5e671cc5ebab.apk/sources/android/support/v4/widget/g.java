package android.support.v4.widget;

import android.database.ContentObserver;
import android.os.Handler;

class g extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f93a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g(e eVar) {
        super(new Handler());
        this.f93a = eVar;
    }

    public boolean deliverSelfNotifications() {
        return true;
    }

    public void onChange(boolean z) {
        this.f93a.b();
    }
}
