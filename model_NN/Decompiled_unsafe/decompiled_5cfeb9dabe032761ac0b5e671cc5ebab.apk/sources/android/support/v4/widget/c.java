package android.support.v4.widget;

import android.view.animation.AnimationUtils;

class c {

    /* renamed from: a  reason: collision with root package name */
    private int f90a;
    private int b;
    private float c;
    private float d;
    private long e = Long.MIN_VALUE;
    private long f = 0;
    private int g = 0;
    private int h = 0;
    private long i = -1;
    private float j;
    private int k;

    private float a(float f2) {
        return (-4.0f * f2 * f2) + (4.0f * f2);
    }

    private float a(long j2) {
        if (j2 < this.e) {
            return 0.0f;
        }
        if (this.i < 0 || j2 < this.i) {
            return a.b(((float) (j2 - this.e)) / ((float) this.f90a), 0.0f, 1.0f) * 0.5f;
        }
        return (a.b(((float) (j2 - this.i)) / ((float) this.k), 0.0f, 1.0f) * this.j) + (1.0f - this.j);
    }

    public void a() {
        this.e = AnimationUtils.currentAnimationTimeMillis();
        this.i = -1;
        this.f = this.e;
        this.j = 0.5f;
        this.g = 0;
        this.h = 0;
    }

    public void a(float f2, float f3) {
        this.c = f2;
        this.d = f3;
    }

    public void a(int i2) {
        this.f90a = i2;
    }

    public void b() {
        long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
        this.k = a.b((int) (currentAnimationTimeMillis - this.e), 0, this.b);
        this.j = a(currentAnimationTimeMillis);
        this.i = currentAnimationTimeMillis;
    }

    public void b(int i2) {
        this.b = i2;
    }

    public boolean c() {
        return this.i > 0 && AnimationUtils.currentAnimationTimeMillis() > this.i + ((long) this.k);
    }

    public void d() {
        if (this.f == 0) {
            throw new RuntimeException("Cannot compute scroll delta before calling start()");
        }
        long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
        float a2 = a(a(currentAnimationTimeMillis));
        long j2 = currentAnimationTimeMillis - this.f;
        this.f = currentAnimationTimeMillis;
        this.g = (int) (((float) j2) * a2 * this.c);
        this.h = (int) (((float) j2) * a2 * this.d);
    }

    public int e() {
        return (int) (this.c / Math.abs(this.c));
    }

    public int f() {
        return (int) (this.d / Math.abs(this.d));
    }

    public int g() {
        return this.g;
    }

    public int h() {
        return this.h;
    }
}
