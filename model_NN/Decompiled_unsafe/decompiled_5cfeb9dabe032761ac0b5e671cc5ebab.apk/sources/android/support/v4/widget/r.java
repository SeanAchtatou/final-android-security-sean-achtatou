package android.support.v4.widget;

import android.view.View;

class r extends aw {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DrawerLayout f97a;
    private final int b;
    private au c;
    private final Runnable d;

    private void b() {
        int i = 3;
        if (this.b == 3) {
            i = 5;
        }
        View a2 = this.f97a.a(i);
        if (a2 != null) {
            this.f97a.i(a2);
        }
    }

    public int a(View view) {
        if (this.f97a.g(view)) {
            return view.getWidth();
        }
        return 0;
    }

    public int a(View view, int i, int i2) {
        if (this.f97a.a(view, 3)) {
            return Math.max(-view.getWidth(), Math.min(i, 0));
        }
        int width = this.f97a.getWidth();
        return Math.max(width - view.getWidth(), Math.min(i, width));
    }

    public void a() {
        this.f97a.removeCallbacks(this.d);
    }

    public void a(int i) {
        this.f97a.a(this.b, i, this.c.c());
    }

    public void a(int i, int i2) {
        this.f97a.postDelayed(this.d, 160);
    }

    public void a(View view, float f, float f2) {
        int width;
        float d2 = this.f97a.d(view);
        int width2 = view.getWidth();
        if (this.f97a.a(view, 3)) {
            width = (f > 0.0f || (f == 0.0f && d2 > 0.5f)) ? 0 : -width2;
        } else {
            width = this.f97a.getWidth();
            if (f < 0.0f || (f == 0.0f && d2 > 0.5f)) {
                width -= width2;
            }
        }
        this.c.a(width, view.getTop());
        this.f97a.invalidate();
    }

    public void a(View view, int i, int i2, int i3, int i4) {
        int width = view.getWidth();
        float width2 = this.f97a.a(view, 3) ? ((float) (width + i)) / ((float) width) : ((float) (this.f97a.getWidth() - i)) / ((float) width);
        this.f97a.b(view, width2);
        view.setVisibility(width2 == 0.0f ? 4 : 0);
        this.f97a.invalidate();
    }

    public boolean a(View view, int i) {
        return this.f97a.g(view) && this.f97a.a(view, this.b) && this.f97a.a(view) == 0;
    }

    public int b(View view, int i, int i2) {
        return view.getTop();
    }

    public void b(int i, int i2) {
        View a2 = (i & 1) == 1 ? this.f97a.a(3) : this.f97a.a(5);
        if (a2 != null && this.f97a.a(a2) == 0) {
            this.c.a(a2, i2);
        }
    }

    public void b(View view, int i) {
        ((p) view.getLayoutParams()).c = false;
        b();
    }

    public boolean b(int i) {
        return false;
    }
}
