package android.support.v4.widget;

import android.graphics.Paint;
import android.support.v4.view.au;
import android.view.View;

class am implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final View f86a;
    final /* synthetic */ SlidingPaneLayout b;

    am(SlidingPaneLayout slidingPaneLayout, View view) {
        this.b = slidingPaneLayout;
        this.f86a = view;
    }

    public void run() {
        if (this.f86a.getParent() == this.b) {
            au.a(this.f86a, 0, (Paint) null);
            this.b.d(this.f86a);
        }
        this.b.u.remove(this);
    }
}
