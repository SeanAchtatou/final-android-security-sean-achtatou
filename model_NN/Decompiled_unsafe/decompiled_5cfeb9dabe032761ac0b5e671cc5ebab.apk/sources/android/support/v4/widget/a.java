package android.support.v4.widget;

import android.content.res.Resources;
import android.os.SystemClock;
import android.support.v4.view.ai;
import android.support.v4.view.au;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;

public abstract class a implements View.OnTouchListener {
    private static final int r = ViewConfiguration.getTapTimeout();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final c f84a = new c();
    private final Interpolator b = new AccelerateInterpolator();
    /* access modifiers changed from: private */
    public final View c;
    private Runnable d;
    private float[] e = {0.0f, 0.0f};
    private float[] f = {Float.MAX_VALUE, Float.MAX_VALUE};
    private int g;
    private int h;
    private float[] i = {0.0f, 0.0f};
    private float[] j = {0.0f, 0.0f};
    private float[] k = {Float.MAX_VALUE, Float.MAX_VALUE};
    private boolean l;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public boolean o;
    private boolean p;
    private boolean q;

    public a(View view) {
        this.c = view;
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        int i2 = (int) ((1575.0f * displayMetrics.density) + 0.5f);
        int i3 = (int) ((displayMetrics.density * 315.0f) + 0.5f);
        a((float) i2, (float) i2);
        b((float) i3, (float) i3);
        a(1);
        e(Float.MAX_VALUE, Float.MAX_VALUE);
        d(0.2f, 0.2f);
        c(1.0f, 1.0f);
        b(r);
        c(500);
        d(500);
    }

    private float a(float f2, float f3, float f4, float f5) {
        float interpolation;
        float b2 = b(f2 * f3, 0.0f, f4);
        float f6 = f(f3 - f5, b2) - f(f5, b2);
        if (f6 < 0.0f) {
            interpolation = -this.b.getInterpolation(-f6);
        } else if (f6 <= 0.0f) {
            return 0.0f;
        } else {
            interpolation = this.b.getInterpolation(f6);
        }
        return b(interpolation, -1.0f, 1.0f);
    }

    private float a(int i2, float f2, float f3, float f4) {
        float a2 = a(this.e[i2], f3, this.f[i2], f2);
        if (a2 == 0.0f) {
            return 0.0f;
        }
        float f5 = this.i[i2];
        float f6 = this.j[i2];
        float f7 = this.k[i2];
        float f8 = f5 * f4;
        return a2 > 0.0f ? b(a2 * f8, f6, f7) : -b((-a2) * f8, f6, f7);
    }

    /* access modifiers changed from: private */
    public boolean a() {
        c cVar = this.f84a;
        int f2 = cVar.f();
        int e2 = cVar.e();
        return (f2 != 0 && f(f2)) || (e2 != 0 && e(e2));
    }

    /* access modifiers changed from: private */
    public static float b(float f2, float f3, float f4) {
        return f2 > f4 ? f4 : f2 < f3 ? f3 : f2;
    }

    /* access modifiers changed from: private */
    public static int b(int i2, int i3, int i4) {
        return i2 > i4 ? i4 : i2 < i3 ? i3 : i2;
    }

    private void b() {
        if (this.d == null) {
            this.d = new d(this);
        }
        this.o = true;
        this.m = true;
        if (this.l || this.h <= 0) {
            this.d.run();
        } else {
            au.a(this.c, this.d, (long) this.h);
        }
        this.l = true;
    }

    private void c() {
        if (this.m) {
            this.o = false;
        } else {
            this.f84a.b();
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        long uptimeMillis = SystemClock.uptimeMillis();
        MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
        this.c.onTouchEvent(obtain);
        obtain.recycle();
    }

    private float f(float f2, float f3) {
        if (f3 == 0.0f) {
            return 0.0f;
        }
        switch (this.g) {
            case 0:
            case 1:
                if (f2 < f3) {
                    return f2 >= 0.0f ? 1.0f - (f2 / f3) : (!this.o || this.g != 1) ? 0.0f : 1.0f;
                }
                return 0.0f;
            case 2:
                if (f2 < 0.0f) {
                    return f2 / (-f3);
                }
                return 0.0f;
            default:
                return 0.0f;
        }
    }

    public a a(float f2, float f3) {
        this.k[0] = f2 / 1000.0f;
        this.k[1] = f3 / 1000.0f;
        return this;
    }

    public a a(int i2) {
        this.g = i2;
        return this;
    }

    public a a(boolean z) {
        if (this.p && !z) {
            c();
        }
        this.p = z;
        return this;
    }

    public abstract void a(int i2, int i3);

    public a b(float f2, float f3) {
        this.j[0] = f2 / 1000.0f;
        this.j[1] = f3 / 1000.0f;
        return this;
    }

    public a b(int i2) {
        this.h = i2;
        return this;
    }

    public a c(float f2, float f3) {
        this.i[0] = f2 / 1000.0f;
        this.i[1] = f3 / 1000.0f;
        return this;
    }

    public a c(int i2) {
        this.f84a.a(i2);
        return this;
    }

    public a d(float f2, float f3) {
        this.e[0] = f2;
        this.e[1] = f3;
        return this;
    }

    public a d(int i2) {
        this.f84a.b(i2);
        return this;
    }

    public a e(float f2, float f3) {
        this.f[0] = f2;
        this.f[1] = f3;
        return this;
    }

    public abstract boolean e(int i2);

    public abstract boolean f(int i2);

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean z = true;
        if (!this.p) {
            return false;
        }
        switch (ai.a(motionEvent)) {
            case 0:
                this.n = true;
                this.l = false;
                this.f84a.a(a(0, motionEvent.getX(), (float) view.getWidth(), (float) this.c.getWidth()), a(1, motionEvent.getY(), (float) view.getHeight(), (float) this.c.getHeight()));
                if (!this.o && a()) {
                    b();
                    break;
                }
            case 1:
            case 3:
                c();
                break;
            case 2:
                this.f84a.a(a(0, motionEvent.getX(), (float) view.getWidth(), (float) this.c.getWidth()), a(1, motionEvent.getY(), (float) view.getHeight(), (float) this.c.getHeight()));
                b();
                break;
        }
        if (!this.q || !this.o) {
            z = false;
        }
        return z;
    }
}
