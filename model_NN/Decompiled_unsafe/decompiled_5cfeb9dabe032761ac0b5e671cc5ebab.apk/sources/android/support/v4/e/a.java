package android.support.v4.e;

import android.os.Build;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final b f36a;

    static {
        if (Build.VERSION.SDK_INT >= 14) {
            f36a = new d();
        } else {
            f36a = new c();
        }
    }

    public static String a(String str) {
        return f36a.a(str);
    }

    public static String b(String str) {
        return f36a.b(str);
    }
}
