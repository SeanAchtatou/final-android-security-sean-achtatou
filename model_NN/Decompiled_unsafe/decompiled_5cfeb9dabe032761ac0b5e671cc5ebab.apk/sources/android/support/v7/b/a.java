package android.support.v7.b;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    private Object f113a;
    private boolean b;

    public abstract MenuInflater a();

    public abstract void a(int i);

    public abstract void a(View view);

    public abstract void a(CharSequence charSequence);

    public void a(Object obj) {
        this.f113a = obj;
    }

    public void a(boolean z) {
        this.b = z;
    }

    public abstract Menu b();

    public abstract void b(int i);

    public abstract void b(CharSequence charSequence);

    public abstract void c();

    public abstract void d();

    public abstract CharSequence f();

    public abstract CharSequence g();

    public boolean h() {
        return false;
    }

    public abstract View i();

    public Object j() {
        return this.f113a;
    }

    public boolean k() {
        return this.b;
    }
}
