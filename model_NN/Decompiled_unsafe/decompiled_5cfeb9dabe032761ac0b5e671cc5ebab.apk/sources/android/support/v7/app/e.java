package android.support.v7.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ao;
import android.support.v4.app.aw;
import android.support.v4.app.ax;
import android.support.v4.app.o;
import android.support.v7.b.a;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class e extends o implements ax {
    private f n;

    private f i() {
        if (this.n == null) {
            this.n = f.a(this);
        }
        return this.n;
    }

    public Intent a() {
        return ao.a(this);
    }

    public void a(aw awVar) {
        awVar.a((Activity) this);
    }

    public void a(a aVar) {
    }

    /* access modifiers changed from: package-private */
    public void a(View view) {
        super.setContentView(view);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, Menu menu) {
        return super.onCreatePanelMenu(i, menu);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, View view, Menu menu) {
        return super.onPreparePanel(i, view, menu);
    }

    public boolean a(Intent intent) {
        return ao.a(this, intent);
    }

    /* access modifiers changed from: protected */
    public boolean a(View view, Menu menu) {
        return i().a(view, menu);
    }

    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        i().b(view, layoutParams);
    }

    /* access modifiers changed from: package-private */
    public void b(int i, Menu menu) {
        super.onPanelClosed(i, menu);
    }

    public void b(Intent intent) {
        ao.b(this, intent);
    }

    public void b(aw awVar) {
    }

    public void b(a aVar) {
    }

    /* access modifiers changed from: package-private */
    public boolean b(View view, Menu menu) {
        return super.a(view, menu);
    }

    /* access modifiers changed from: package-private */
    public boolean c(int i, Menu menu) {
        return super.onMenuOpened(i, menu);
    }

    public void d() {
        i().g();
    }

    public a f() {
        return i().b();
    }

    public boolean g() {
        Intent a2 = a();
        if (a2 == null) {
            return false;
        }
        if (a(a2)) {
            aw a3 = aw.a((Context) this);
            a(a3);
            b(a3);
            a3.a();
            try {
                android.support.v4.app.a.a(this);
            } catch (IllegalStateException e) {
                finish();
            }
        } else {
            b(a2);
        }
        return true;
    }

    public MenuInflater getMenuInflater() {
        return i().d();
    }

    public void h() {
    }

    public void invalidateOptionsMenu() {
        i().g();
    }

    public void onBackPressed() {
        if (!i().h()) {
            super.onBackPressed();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        i().a(configuration);
    }

    public final void onContentChanged() {
        i().i();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        i().a(bundle);
    }

    public boolean onCreatePanelMenu(int i, Menu menu) {
        return i().c(i, menu);
    }

    public View onCreatePanelView(int i) {
        return i == 0 ? i().b(i) : super.onCreatePanelView(i);
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        View onCreateView = super.onCreateView(str, context, attributeSet);
        return onCreateView != null ? onCreateView : i().a(str, context, attributeSet);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        i().l();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (super.onKeyDown(i, keyEvent)) {
            return true;
        }
        return i().a(i, keyEvent);
    }

    public boolean onKeyShortcut(int i, KeyEvent keyEvent) {
        return i().b(i, keyEvent);
    }

    public final boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (super.onMenuItemSelected(i, menuItem)) {
            return true;
        }
        a f = f();
        if (menuItem.getItemId() != 16908332 || f == null || (f.a() & 4) == 0) {
            return false;
        }
        return g();
    }

    public boolean onMenuOpened(int i, Menu menu) {
        return i().b(i, menu);
    }

    public void onPanelClosed(int i, Menu menu) {
        i().a(i, menu);
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        i().f();
    }

    public boolean onPreparePanel(int i, View view, Menu menu) {
        return i().a(i, view, menu);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        i().e();
    }

    /* access modifiers changed from: protected */
    public void onTitleChanged(CharSequence charSequence, int i) {
        super.onTitleChanged(charSequence, i);
        i().a(charSequence);
    }

    public void setContentView(int i) {
        i().a(i);
    }

    public void setContentView(View view) {
        i().a(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        i().a(view, layoutParams);
    }
}
