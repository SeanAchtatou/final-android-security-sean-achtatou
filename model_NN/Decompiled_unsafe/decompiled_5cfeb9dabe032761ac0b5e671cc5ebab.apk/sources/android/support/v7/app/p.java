package android.support.v7.app;

import android.annotation.TargetApi;
import android.support.v7.b.a;
import android.support.v7.internal.view.c;
import android.support.v7.internal.view.d;
import android.support.v7.internal.widget.NativeActionModeAwareLayout;
import android.support.v7.internal.widget.ad;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.View;

@TargetApi(11)
class p extends ActionBarActivityDelegateBase implements ad {
    private NativeActionModeAwareLayout k;

    p(e eVar) {
        super(eVar);
    }

    public ActionMode a(View view, ActionMode.Callback callback) {
        a a2 = a(new d(view.getContext(), callback));
        if (a2 != null) {
            return new c(this.f104a, a2);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i, KeyEvent keyEvent) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void o() {
        this.k = (NativeActionModeAwareLayout) this.f104a.findViewById(16908290);
        if (this.k != null) {
            this.k.setActionModeForChildListener(this);
        }
    }
}
