package android.support.v7.app;

import android.support.v7.internal.a.a;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

class g implements a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f105a;

    g(f fVar) {
        this.f105a = fVar;
    }

    public View a(int i) {
        return null;
    }

    public boolean a(int i, Menu menu) {
        return this.f105a.f104a.a(i, menu);
    }

    public boolean a(int i, MenuItem menuItem) {
        return this.f105a.f104a.onMenuItemSelected(i, menuItem);
    }

    public boolean a(int i, View view, Menu menu) {
        return this.f105a.f104a.a(i, view, menu);
    }

    public void b(int i, Menu menu) {
        this.f105a.f104a.onPanelClosed(i, menu);
    }

    public boolean c(int i, Menu menu) {
        return this.f105a.f104a.onMenuOpened(i, menu);
    }
}
