package android.support.v7.app;

import android.support.v7.internal.a.a;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.y;

final class l implements y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ActionBarActivityDelegateBase f110a;

    private l(ActionBarActivityDelegateBase actionBarActivityDelegateBase) {
        this.f110a = actionBarActivityDelegateBase;
    }

    /* synthetic */ l(ActionBarActivityDelegateBase actionBarActivityDelegateBase, h hVar) {
        this(actionBarActivityDelegateBase);
    }

    public void a(i iVar, boolean z) {
        this.f110a.b(iVar);
    }

    public boolean a(i iVar) {
        a k = this.f110a.k();
        if (k == null) {
            return true;
        }
        k.c(8, iVar);
        return true;
    }
}
