package android.support.v7.app;

import android.support.v4.view.an;
import android.support.v4.view.cy;
import android.view.View;

class i implements an {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ActionBarActivityDelegateBase f107a;

    i(ActionBarActivityDelegateBase actionBarActivityDelegateBase) {
        this.f107a = actionBarActivityDelegateBase;
    }

    public cy a(View view, cy cyVar) {
        int b = cyVar.b();
        int c = this.f107a.e(b);
        return b != c ? cyVar.a(cyVar.a(), c, cyVar.c(), cyVar.d()) : cyVar;
    }
}
