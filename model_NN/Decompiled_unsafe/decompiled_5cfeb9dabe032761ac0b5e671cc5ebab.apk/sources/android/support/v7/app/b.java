package android.support.v7.app;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.a.l;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class b extends ViewGroup.MarginLayoutParams {

    /* renamed from: a  reason: collision with root package name */
    public int f103a;

    public b(int i, int i2) {
        super(i, i2);
        this.f103a = 0;
        this.f103a = 8388627;
    }

    public b(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f103a = 0;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.ActionBarLayout);
        this.f103a = obtainStyledAttributes.getInt(l.ActionBarLayout_android_layout_gravity, 0);
        obtainStyledAttributes.recycle();
    }

    public b(b bVar) {
        super((ViewGroup.MarginLayoutParams) bVar);
        this.f103a = 0;
        this.f103a = bVar.f103a;
    }

    public b(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
        this.f103a = 0;
    }
}
