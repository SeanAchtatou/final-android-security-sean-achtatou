package android.support.v7.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.ao;
import android.support.v4.view.au;
import android.support.v4.view.bn;
import android.support.v7.a.b;
import android.support.v7.a.d;
import android.support.v7.a.k;
import android.support.v7.b.a;
import android.support.v7.internal.view.menu.g;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.j;
import android.support.v7.internal.view.menu.y;
import android.support.v7.internal.view.menu.z;
import android.support.v7.internal.widget.ActionBarContextView;
import android.support.v7.internal.widget.ViewStubCompat;
import android.support.v7.internal.widget.as;
import android.support.v7.internal.widget.at;
import android.support.v7.internal.widget.av;
import android.support.v7.internal.widget.ay;
import android.support.v7.internal.widget.ba;
import android.support.v7.internal.widget.bh;
import android.support.v7.internal.widget.w;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.PopupWindow;

class ActionBarActivityDelegateBase extends f implements j {
    private boolean A;
    private g B;
    private Rect C;
    private Rect D;
    a g;
    ActionBarContextView h;
    PopupWindow i;
    Runnable j;
    private w k;
    private l l;
    private o m;
    private boolean n;
    private ViewGroup o;
    private ViewGroup p;
    private View q;
    private CharSequence r;
    private boolean s;
    private boolean t;
    private boolean u;
    private PanelFeatureState[] v;
    private PanelFeatureState w;
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public int y;
    private final Runnable z = new h(this);

    final class PanelFeatureState {

        /* renamed from: a  reason: collision with root package name */
        int f101a;
        ViewGroup b;
        View c;
        i d;
        g e;
        Context f;
        boolean g;
        boolean h;
        boolean i;
        public boolean j;
        boolean k = false;
        boolean l;
        Bundle m;

        class SavedState implements Parcelable {
            public static final Parcelable.Creator CREATOR = new n();

            /* renamed from: a  reason: collision with root package name */
            int f102a;
            boolean b;
            Bundle c;

            private SavedState() {
            }

            /* access modifiers changed from: private */
            public static SavedState b(Parcel parcel) {
                boolean z = true;
                SavedState savedState = new SavedState();
                savedState.f102a = parcel.readInt();
                if (parcel.readInt() != 1) {
                    z = false;
                }
                savedState.b = z;
                if (savedState.b) {
                    savedState.c = parcel.readBundle();
                }
                return savedState;
            }

            public int describeContents() {
                return 0;
            }

            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(this.f102a);
                parcel.writeInt(this.b ? 1 : 0);
                if (this.b) {
                    parcel.writeBundle(this.c);
                }
            }
        }

        PanelFeatureState(int i2) {
            this.f101a = i2;
        }

        /* access modifiers changed from: package-private */
        public z a(y yVar) {
            if (this.d == null) {
                return null;
            }
            if (this.e == null) {
                this.e = new g(this.f, android.support.v7.a.i.abc_list_menu_item_layout);
                this.e.a(yVar);
                this.d.a(this.e);
            }
            return this.e.a(this.b);
        }

        /* access modifiers changed from: package-private */
        public void a(Context context) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme newTheme = context.getResources().newTheme();
            newTheme.setTo(context.getTheme());
            newTheme.resolveAttribute(b.actionBarPopupTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            }
            newTheme.resolveAttribute(b.panelMenuListTheme, typedValue, true);
            if (typedValue.resourceId != 0) {
                newTheme.applyStyle(typedValue.resourceId, true);
            } else {
                newTheme.applyStyle(k.Theme_AppCompat_CompactMenu, true);
            }
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, 0);
            contextThemeWrapper.getTheme().setTo(newTheme);
            this.f = contextThemeWrapper;
        }

        /* access modifiers changed from: package-private */
        public void a(i iVar) {
            if (iVar != this.d) {
                if (this.d != null) {
                    this.d.b(this.e);
                }
                this.d = iVar;
                if (iVar != null && this.e != null) {
                    iVar.a(this.e);
                }
            }
        }

        public boolean a() {
            return this.c != null && this.e.a().getCount() > 0;
        }
    }

    ActionBarActivityDelegateBase(e eVar) {
        super(eVar);
    }

    private PanelFeatureState a(int i2, boolean z2) {
        PanelFeatureState[] panelFeatureStateArr = this.v;
        if (panelFeatureStateArr == null || panelFeatureStateArr.length <= i2) {
            PanelFeatureState[] panelFeatureStateArr2 = new PanelFeatureState[(i2 + 1)];
            if (panelFeatureStateArr != null) {
                System.arraycopy(panelFeatureStateArr, 0, panelFeatureStateArr2, 0, panelFeatureStateArr.length);
            }
            this.v = panelFeatureStateArr2;
            panelFeatureStateArr = panelFeatureStateArr2;
        }
        PanelFeatureState panelFeatureState = panelFeatureStateArr[i2];
        if (panelFeatureState != null) {
            return panelFeatureState;
        }
        PanelFeatureState panelFeatureState2 = new PanelFeatureState(i2);
        panelFeatureStateArr[i2] = panelFeatureState2;
        return panelFeatureState2;
    }

    /* access modifiers changed from: private */
    public PanelFeatureState a(Menu menu) {
        PanelFeatureState[] panelFeatureStateArr = this.v;
        int length = panelFeatureStateArr != null ? panelFeatureStateArr.length : 0;
        for (int i2 = 0; i2 < length; i2++) {
            PanelFeatureState panelFeatureState = panelFeatureStateArr[i2];
            if (panelFeatureState != null && panelFeatureState.d == menu) {
                return panelFeatureState;
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void a(int i2, PanelFeatureState panelFeatureState, Menu menu) {
        if (menu == null) {
            if (panelFeatureState == null && i2 >= 0 && i2 < this.v.length) {
                panelFeatureState = this.v[i2];
            }
            if (panelFeatureState != null) {
                menu = panelFeatureState.d;
            }
        }
        if (panelFeatureState == null || panelFeatureState.i) {
            k().b(i2, menu);
        }
    }

    private void a(PanelFeatureState panelFeatureState) {
        panelFeatureState.b = this.o;
        panelFeatureState.a(j());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, int]
     candidates:
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.view.Menu):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, int):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, boolean):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.Menu):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.f.a(int, android.view.Menu):void
      android.support.v7.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.f.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.f.a(android.view.View, android.view.Menu):boolean
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void */
    private void a(PanelFeatureState panelFeatureState, KeyEvent keyEvent) {
        if (!panelFeatureState.i && !m()) {
            if (panelFeatureState.f101a == 0) {
                e eVar = this.f104a;
                boolean z2 = (eVar.getResources().getConfiguration().screenLayout & 15) == 4;
                boolean z3 = eVar.getApplicationInfo().targetSdkVersion >= 11;
                if (z2 && z3) {
                    return;
                }
            }
            android.support.v7.internal.a.a k2 = k();
            if (k2 != null && !k2.c(panelFeatureState.f101a, panelFeatureState.d)) {
                a(panelFeatureState, true);
            } else if (b(panelFeatureState, keyEvent)) {
                if (panelFeatureState.b == null || panelFeatureState.k) {
                    a(panelFeatureState);
                }
                if (c(panelFeatureState) && panelFeatureState.a()) {
                    panelFeatureState.h = false;
                    panelFeatureState.i = true;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(PanelFeatureState panelFeatureState, boolean z2) {
        if (!z2 || panelFeatureState.f101a != 0 || this.k == null || !this.k.e()) {
            if (panelFeatureState.i && z2) {
                a(panelFeatureState.f101a, panelFeatureState, (Menu) null);
            }
            panelFeatureState.g = false;
            panelFeatureState.h = false;
            panelFeatureState.i = false;
            panelFeatureState.c = null;
            panelFeatureState.k = true;
            if (this.w == panelFeatureState) {
                this.w = null;
                return;
            }
            return;
        }
        b(panelFeatureState.d);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.view.Menu):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, int):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, boolean):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.Menu):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.f.a(int, android.view.Menu):void
      android.support.v7.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.f.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.f.a(android.view.View, android.view.Menu):boolean
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, int]
     candidates:
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.view.Menu):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, int):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, boolean):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.Menu):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.f.a(int, android.view.Menu):void
      android.support.v7.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.f.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.f.a(android.view.View, android.view.Menu):boolean
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void */
    private void a(i iVar, boolean z2) {
        if (this.k == null || !this.k.d() || (bn.a(ViewConfiguration.get(this.f104a)) && !this.k.f())) {
            PanelFeatureState a2 = a(0, true);
            a2.k = true;
            a(a2, false);
            a(a2, (KeyEvent) null);
            return;
        }
        android.support.v7.internal.a.a k2 = k();
        if (this.k.e() && z2) {
            this.k.h();
            if (!m()) {
                this.f104a.onPanelClosed(8, a(0, true).d);
            }
        } else if (k2 != null && !m()) {
            if (this.x && (this.y & 1) != 0) {
                this.o.removeCallbacks(this.z);
                this.z.run();
            }
            PanelFeatureState a3 = a(0, true);
            if (a3.d != null && !a3.l && k2.a(0, null, a3.d)) {
                k2.c(8, a3.d);
                this.k.g();
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(i iVar) {
        if (!this.u) {
            this.u = true;
            this.k.j();
            android.support.v7.internal.a.a k2 = k();
            if (k2 != null && !m()) {
                k2.b(8, iVar);
            }
            this.u = false;
        }
    }

    private boolean b(PanelFeatureState panelFeatureState) {
        ContextThemeWrapper contextThemeWrapper;
        e eVar = this.f104a;
        if ((panelFeatureState.f101a == 0 || panelFeatureState.f101a == 8) && this.k != null) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = eVar.getTheme();
            theme.resolveAttribute(b.actionBarTheme, typedValue, true);
            Resources.Theme theme2 = null;
            if (typedValue.resourceId != 0) {
                theme2 = eVar.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue.resourceId, true);
                theme2.resolveAttribute(b.actionBarWidgetTheme, typedValue, true);
            } else {
                theme.resolveAttribute(b.actionBarWidgetTheme, typedValue, true);
            }
            if (typedValue.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = eVar.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue.resourceId, true);
            }
            Resources.Theme theme3 = theme2;
            if (theme3 != null) {
                contextThemeWrapper = new ContextThemeWrapper(eVar, 0);
                contextThemeWrapper.getTheme().setTo(theme3);
                i iVar = new i(contextThemeWrapper);
                iVar.a(this);
                panelFeatureState.a(iVar);
                return true;
            }
        }
        contextThemeWrapper = eVar;
        i iVar2 = new i(contextThemeWrapper);
        iVar2.a(this);
        panelFeatureState.a(iVar2);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, int]
     candidates:
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.view.Menu):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, int):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, boolean):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.Menu):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.f.a(int, android.view.Menu):void
      android.support.v7.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.f.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.f.a(android.view.View, android.view.Menu):boolean
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void */
    private boolean b(PanelFeatureState panelFeatureState, KeyEvent keyEvent) {
        if (m()) {
            return false;
        }
        if (panelFeatureState.g) {
            return true;
        }
        if (!(this.w == null || this.w == panelFeatureState)) {
            a(this.w, false);
        }
        boolean z2 = panelFeatureState.f101a == 0 || panelFeatureState.f101a == 8;
        if (z2 && this.k != null) {
            this.k.i();
        }
        if (panelFeatureState.d == null || panelFeatureState.l) {
            if (panelFeatureState.d == null && (!b(panelFeatureState) || panelFeatureState.d == null)) {
                return false;
            }
            if (z2 && this.k != null) {
                if (this.l == null) {
                    this.l = new l(this, null);
                }
                this.k.a(panelFeatureState.d, this.l);
            }
            panelFeatureState.d.g();
            if (!k().a(panelFeatureState.f101a, panelFeatureState.d)) {
                panelFeatureState.a((i) null);
                if (!z2 || this.k == null) {
                    return false;
                }
                this.k.a(null, this.l);
                return false;
            }
            panelFeatureState.l = false;
        }
        panelFeatureState.d.g();
        if (panelFeatureState.m != null) {
            panelFeatureState.d.b(panelFeatureState.m);
            panelFeatureState.m = null;
        }
        if (!k().a(0, null, panelFeatureState.d)) {
            if (z2 && this.k != null) {
                this.k.a(null, this.l);
            }
            panelFeatureState.d.h();
            return false;
        }
        panelFeatureState.j = KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1;
        panelFeatureState.d.setQwertyMode(panelFeatureState.j);
        panelFeatureState.d.h();
        panelFeatureState.g = true;
        panelFeatureState.h = false;
        this.w = panelFeatureState;
        return true;
    }

    private void c(int i2) {
        this.y |= 1 << i2;
        if (!this.x && this.o != null) {
            au.a(this.o, this.z);
            this.x = true;
        }
    }

    private boolean c(PanelFeatureState panelFeatureState) {
        if (panelFeatureState.d == null) {
            return false;
        }
        if (this.m == null) {
            this.m = new o(this, null);
        }
        panelFeatureState.c = (View) panelFeatureState.a(this.m);
        return panelFeatureState.c != null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.view.Menu):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, int):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, boolean):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.Menu):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.f.a(int, android.view.Menu):void
      android.support.v7.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.f.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.f.a(android.view.View, android.view.Menu):boolean
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState */
    /* access modifiers changed from: private */
    public void d(int i2) {
        PanelFeatureState a2;
        PanelFeatureState a3 = a(i2, true);
        if (a3.d != null) {
            Bundle bundle = new Bundle();
            a3.d.a(bundle);
            if (bundle.size() > 0) {
                a3.m = bundle;
            }
            a3.d.g();
            a3.d.clear();
        }
        a3.l = true;
        a3.k = true;
        if ((i2 == 8 || i2 == 0) && this.k != null && (a2 = a(0, false)) != null) {
            a2.g = false;
            b(a2, (KeyEvent) null);
        }
    }

    /* access modifiers changed from: private */
    public int e(int i2) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5 = true;
        int i3 = 0;
        if (this.h == null || !(this.h.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            z2 = false;
        } else {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.h.getLayoutParams();
            if (this.h.isShown()) {
                if (this.C == null) {
                    this.C = new Rect();
                    this.D = new Rect();
                }
                Rect rect = this.C;
                Rect rect2 = this.D;
                rect.set(0, i2, 0, 0);
                bh.a(this.p, rect, rect2);
                if (marginLayoutParams.topMargin != (rect2.top == 0 ? i2 : 0)) {
                    marginLayoutParams.topMargin = i2;
                    if (this.q == null) {
                        this.q = new View(this.f104a);
                        this.q.setBackgroundColor(this.f104a.getResources().getColor(d.abc_input_method_navigation_guard));
                        this.p.addView(this.q, -1, new ViewGroup.LayoutParams(-1, i2));
                        z4 = true;
                    } else {
                        ViewGroup.LayoutParams layoutParams = this.q.getLayoutParams();
                        if (layoutParams.height != i2) {
                            layoutParams.height = i2;
                            this.q.setLayoutParams(layoutParams);
                        }
                        z4 = true;
                    }
                } else {
                    z4 = false;
                }
                if (this.q == null) {
                    z5 = false;
                }
                if (!this.d && z5) {
                    i2 = 0;
                }
                boolean z6 = z4;
                z3 = z5;
                z5 = z6;
            } else if (marginLayoutParams.topMargin != 0) {
                marginLayoutParams.topMargin = 0;
                z3 = false;
            } else {
                z5 = false;
                z3 = false;
            }
            if (z5) {
                this.h.setLayoutParams(marginLayoutParams);
            }
            z2 = z3;
        }
        if (this.q != null) {
            View view = this.q;
            if (!z2) {
                i3 = 8;
            }
            view.setVisibility(i3);
        }
        return i2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00b8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void p() {
        /*
            r11 = this;
            r10 = 6
            r9 = 5
            r1 = 0
            r4 = -1
            android.support.v7.app.e r0 = r11.f104a
            int[] r2 = android.support.v7.a.l.Theme
            android.content.res.TypedArray r6 = r0.obtainStyledAttributes(r2)
            int r0 = android.support.v7.a.l.Theme_windowFixedWidthMajor
            boolean r0 = r6.hasValue(r0)
            if (r0 == 0) goto L_0x00db
            if (r1 != 0) goto L_0x00d8
            android.util.TypedValue r0 = new android.util.TypedValue
            r0.<init>()
        L_0x001b:
            int r2 = android.support.v7.a.l.Theme_windowFixedWidthMajor
            r6.getValue(r2, r0)
        L_0x0020:
            int r2 = android.support.v7.a.l.Theme_windowFixedWidthMinor
            boolean r2 = r6.hasValue(r2)
            if (r2 == 0) goto L_0x00d5
            if (r1 != 0) goto L_0x00d2
            android.util.TypedValue r2 = new android.util.TypedValue
            r2.<init>()
        L_0x002f:
            int r3 = android.support.v7.a.l.Theme_windowFixedWidthMinor
            r6.getValue(r3, r2)
        L_0x0034:
            int r3 = android.support.v7.a.l.Theme_windowFixedHeightMajor
            boolean r3 = r6.hasValue(r3)
            if (r3 == 0) goto L_0x00cf
            if (r1 != 0) goto L_0x00cc
            android.util.TypedValue r3 = new android.util.TypedValue
            r3.<init>()
        L_0x0043:
            int r5 = android.support.v7.a.l.Theme_windowFixedHeightMajor
            r6.getValue(r5, r3)
        L_0x0048:
            int r5 = android.support.v7.a.l.Theme_windowFixedHeightMinor
            boolean r5 = r6.hasValue(r5)
            if (r5 == 0) goto L_0x005c
            if (r1 != 0) goto L_0x0057
            android.util.TypedValue r1 = new android.util.TypedValue
            r1.<init>()
        L_0x0057:
            int r5 = android.support.v7.a.l.Theme_windowFixedHeightMinor
            r6.getValue(r5, r1)
        L_0x005c:
            android.support.v7.app.e r5 = r11.f104a
            android.content.res.Resources r5 = r5.getResources()
            android.util.DisplayMetrics r7 = r5.getDisplayMetrics()
            int r5 = r7.widthPixels
            int r8 = r7.heightPixels
            if (r5 >= r8) goto L_0x00a1
            r5 = 1
        L_0x006d:
            if (r5 == 0) goto L_0x00a3
        L_0x006f:
            if (r2 == 0) goto L_0x00ca
            int r0 = r2.type
            if (r0 == 0) goto L_0x00ca
            int r0 = r2.type
            if (r0 != r9) goto L_0x00a5
            float r0 = r2.getDimension(r7)
            int r0 = (int) r0
            r2 = r0
        L_0x007f:
            if (r5 == 0) goto L_0x00b6
        L_0x0081:
            if (r3 == 0) goto L_0x00c8
            int r0 = r3.type
            if (r0 == 0) goto L_0x00c8
            int r0 = r3.type
            if (r0 != r9) goto L_0x00b8
            float r0 = r3.getDimension(r7)
            int r0 = (int) r0
        L_0x0090:
            if (r2 != r4) goto L_0x0094
            if (r0 == r4) goto L_0x009d
        L_0x0094:
            android.support.v7.app.e r1 = r11.f104a
            android.view.Window r1 = r1.getWindow()
            r1.setLayout(r2, r0)
        L_0x009d:
            r6.recycle()
            return
        L_0x00a1:
            r5 = 0
            goto L_0x006d
        L_0x00a3:
            r2 = r0
            goto L_0x006f
        L_0x00a5:
            int r0 = r2.type
            if (r0 != r10) goto L_0x00ca
            int r0 = r7.widthPixels
            float r0 = (float) r0
            int r8 = r7.widthPixels
            float r8 = (float) r8
            float r0 = r2.getFraction(r0, r8)
            int r0 = (int) r0
            r2 = r0
            goto L_0x007f
        L_0x00b6:
            r3 = r1
            goto L_0x0081
        L_0x00b8:
            int r0 = r3.type
            if (r0 != r10) goto L_0x00c8
            int r0 = r7.heightPixels
            float r0 = (float) r0
            int r1 = r7.heightPixels
            float r1 = (float) r1
            float r0 = r3.getFraction(r0, r1)
            int r0 = (int) r0
            goto L_0x0090
        L_0x00c8:
            r0 = r4
            goto L_0x0090
        L_0x00ca:
            r2 = r4
            goto L_0x007f
        L_0x00cc:
            r3 = r1
            goto L_0x0043
        L_0x00cf:
            r3 = r1
            goto L_0x0048
        L_0x00d2:
            r2 = r1
            goto L_0x002f
        L_0x00d5:
            r2 = r1
            goto L_0x0034
        L_0x00d8:
            r0 = r1
            goto L_0x001b
        L_0x00db:
            r0 = r1
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.ActionBarActivityDelegateBase.p():void");
    }

    public a a() {
        n();
        android.support.v7.internal.a.b bVar = new android.support.v7.internal.a.b(this.f104a, this.c);
        bVar.c(this.A);
        return bVar;
    }

    public a a(android.support.v7.b.b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("ActionMode callback can not be null.");
        }
        if (this.g != null) {
            this.g.c();
        }
        m mVar = new m(this, bVar);
        a b = b();
        if (b != null) {
            this.g = b.a(mVar);
            if (this.g != null) {
                this.f104a.a(this.g);
            }
        }
        if (this.g == null) {
            this.g = b(mVar);
        }
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public View a(String str, Context context, AttributeSet attributeSet) {
        if (Build.VERSION.SDK_INT < 21) {
            char c = 65535;
            switch (str.hashCode()) {
                case -1455429095:
                    if (str.equals("CheckedTextView")) {
                        c = 4;
                        break;
                    }
                    break;
                case -339785223:
                    if (str.equals("Spinner")) {
                        c = 1;
                        break;
                    }
                    break;
                case 776382189:
                    if (str.equals("RadioButton")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1601505219:
                    if (str.equals("CheckBox")) {
                        c = 2;
                        break;
                    }
                    break;
                case 1666676343:
                    if (str.equals("EditText")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    return new av(context, attributeSet);
                case 1:
                    return new ba(context, attributeSet);
                case 2:
                    return new as(context, attributeSet);
                case 3:
                    return new ay(context, attributeSet);
                case 4:
                    return new at(context, attributeSet);
            }
        }
        return null;
    }

    public void a(int i2) {
        n();
        ViewGroup viewGroup = (ViewGroup) this.f104a.findViewById(16908290);
        viewGroup.removeAllViews();
        this.f104a.getLayoutInflater().inflate(i2, viewGroup);
        this.f104a.h();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.view.Menu):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, int):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, boolean):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.Menu):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.f.a(int, android.view.Menu):void
      android.support.v7.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.f.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.f.a(android.view.View, android.view.Menu):boolean
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, int]
     candidates:
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.view.Menu):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, int):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, boolean):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.Menu):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.f.a(int, android.view.Menu):void
      android.support.v7.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.f.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.f.a(android.view.View, android.view.Menu):boolean
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void */
    public void a(int i2, Menu menu) {
        PanelFeatureState a2 = a(i2, false);
        if (a2 != null) {
            a(a2, false);
        }
        if (i2 == 8) {
            a b = b();
            if (b != null) {
                b.e(false);
            }
        } else if (!m()) {
            this.f104a.b(i2, menu);
        }
    }

    public void a(Configuration configuration) {
        a b;
        if (this.b && this.n && (b = b()) != null) {
            b.a(configuration);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Bundle bundle) {
        super.a(bundle);
        this.o = (ViewGroup) this.f104a.getWindow().getDecorView();
        if (ao.b(this.f104a) != null) {
            a c = c();
            if (c == null) {
                this.A = true;
            } else {
                c.c(true);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, boolean):void
     arg types: [android.support.v7.internal.view.menu.i, int]
     candidates:
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.view.Menu):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, int):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, boolean):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.Menu):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.f.a(int, android.view.Menu):void
      android.support.v7.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.f.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.f.a(android.view.View, android.view.Menu):boolean
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, boolean):void */
    public void a(i iVar) {
        a(iVar, true);
    }

    public void a(View view) {
        n();
        ViewGroup viewGroup = (ViewGroup) this.f104a.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.f104a.h();
    }

    public void a(View view, ViewGroup.LayoutParams layoutParams) {
        n();
        ViewGroup viewGroup = (ViewGroup) this.f104a.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.f104a.h();
    }

    public void a(CharSequence charSequence) {
        if (this.k != null) {
            this.k.setWindowTitle(charSequence);
        } else if (b() != null) {
            b().a(charSequence);
        } else {
            this.r = charSequence;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2, KeyEvent keyEvent) {
        return b(i2, keyEvent);
    }

    public boolean a(int i2, View view, Menu menu) {
        if (i2 != 0) {
            return k().a(i2, view, menu);
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, int]
     candidates:
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.view.Menu):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, int):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, boolean):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.Menu):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.f.a(int, android.view.Menu):void
      android.support.v7.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.f.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.f.a(android.view.View, android.view.Menu):boolean
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void */
    /* access modifiers changed from: package-private */
    public final boolean a(PanelFeatureState panelFeatureState, int i2, KeyEvent keyEvent, int i3) {
        boolean z2 = false;
        if (!keyEvent.isSystem()) {
            if ((panelFeatureState.g || b(panelFeatureState, keyEvent)) && panelFeatureState.d != null) {
                z2 = panelFeatureState.d.performShortcut(i2, keyEvent, i3);
            }
            if (z2 && (i3 & 1) == 0 && this.k == null) {
                a(panelFeatureState, true);
            }
        }
        return z2;
    }

    public boolean a(i iVar, MenuItem menuItem) {
        PanelFeatureState a2;
        android.support.v7.internal.a.a k2 = k();
        if (k2 == null || m() || (a2 = a((Menu) iVar.p())) == null) {
            return false;
        }
        return k2.a(a2.f101a, menuItem);
    }

    /* access modifiers changed from: package-private */
    public a b(android.support.v7.b.b bVar) {
        if (this.g != null) {
            this.g.c();
        }
        m mVar = new m(this, bVar);
        Context j2 = j();
        if (this.h == null) {
            if (this.e) {
                this.h = new ActionBarContextView(j2);
                this.i = new PopupWindow(j2, (AttributeSet) null, b.actionModePopupWindowStyle);
                this.i.setContentView(this.h);
                this.i.setWidth(-1);
                TypedValue typedValue = new TypedValue();
                this.f104a.getTheme().resolveAttribute(b.actionBarSize, typedValue, true);
                this.h.setContentHeight(TypedValue.complexToDimensionPixelSize(typedValue.data, this.f104a.getResources().getDisplayMetrics()));
                this.i.setHeight(-2);
                this.j = new k(this);
            } else {
                ViewStubCompat viewStubCompat = (ViewStubCompat) this.f104a.findViewById(android.support.v7.a.g.action_mode_bar_stub);
                if (viewStubCompat != null) {
                    viewStubCompat.setLayoutInflater(LayoutInflater.from(j2));
                    this.h = (ActionBarContextView) viewStubCompat.a();
                }
            }
        }
        if (this.h != null) {
            this.h.c();
            android.support.v7.internal.view.b bVar2 = new android.support.v7.internal.view.b(j2, this.h, mVar, this.i == null);
            if (bVar.a(bVar2, bVar2.b())) {
                bVar2.d();
                this.h.a(bVar2);
                this.h.setVisibility(0);
                this.g = bVar2;
                if (this.i != null) {
                    this.f104a.getWindow().getDecorView().post(this.j);
                }
                this.h.sendAccessibilityEvent(32);
                if (this.h.getParent() != null) {
                    au.k((View) this.h.getParent());
                }
            } else {
                this.g = null;
            }
        }
        if (!(this.g == null || this.f104a == null)) {
            this.f104a.a(this.g);
        }
        return this.g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.view.Menu):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, int):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, boolean):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.Menu):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.f.a(int, android.view.Menu):void
      android.support.v7.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.f.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.f.a(android.view.View, android.view.Menu):boolean
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState */
    public View b(int i2) {
        if (this.g != null) {
            return null;
        }
        android.support.v7.internal.a.a k2 = k();
        View a2 = k2 != null ? k2.a(i2) : null;
        if (a2 != null || this.B != null) {
            return a2;
        }
        PanelFeatureState a3 = a(i2, true);
        a(a3, (KeyEvent) null);
        return a3.i ? a3.c : a2;
    }

    public void b(View view, ViewGroup.LayoutParams layoutParams) {
        n();
        ((ViewGroup) this.f104a.findViewById(16908290)).addView(view, layoutParams);
        this.f104a.h();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.view.Menu):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, int):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, boolean):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.Menu):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.f.a(int, android.view.Menu):void
      android.support.v7.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.f.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.f.a(android.view.View, android.view.Menu):boolean
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState */
    /* access modifiers changed from: package-private */
    public boolean b(int i2, KeyEvent keyEvent) {
        if (this.w == null || !a(this.w, keyEvent.getKeyCode(), keyEvent, 1)) {
            if (this.w == null) {
                PanelFeatureState a2 = a(0, true);
                b(a2, keyEvent);
                boolean a3 = a(a2, keyEvent.getKeyCode(), keyEvent, 1);
                a2.g = false;
                if (a3) {
                    return true;
                }
            }
            return false;
        } else if (this.w == null) {
            return true;
        } else {
            this.w.h = true;
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b(int i2, Menu menu) {
        if (i2 != 8) {
            return this.f104a.c(i2, menu);
        }
        a b = b();
        if (b == null) {
            return true;
        }
        b.e(true);
        return true;
    }

    public boolean c(int i2, Menu menu) {
        if (i2 != 0) {
            return k().a(i2, menu);
        }
        return false;
    }

    public void e() {
        a b = b();
        if (b != null) {
            b.d(false);
        }
    }

    public void f() {
        a b = b();
        if (b != null) {
            b.d(true);
        }
    }

    public void g() {
        a b = b();
        if (b == null || !b.c()) {
            c(0);
        }
    }

    public boolean h() {
        if (this.g != null) {
            this.g.c();
            return true;
        }
        a b = b();
        return b != null && b.d();
    }

    public void i() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
     arg types: [int, int]
     candidates:
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.view.Menu):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, int):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, boolean):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.Menu):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.f.a(int, android.view.Menu):void
      android.support.v7.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.f.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.f.a(android.view.View, android.view.Menu):boolean
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState */
    /* access modifiers changed from: package-private */
    public final void n() {
        if (!this.n) {
            if (this.b) {
                TypedValue typedValue = new TypedValue();
                this.f104a.getTheme().resolveAttribute(b.actionBarTheme, typedValue, true);
                this.p = (ViewGroup) LayoutInflater.from(typedValue.resourceId != 0 ? new ContextThemeWrapper(this.f104a, typedValue.resourceId) : this.f104a).inflate(android.support.v7.a.i.abc_screen_toolbar, (ViewGroup) null);
                this.k = (w) this.p.findViewById(android.support.v7.a.g.decor_content_parent);
                this.k.setWindowCallback(k());
                if (this.c) {
                    this.k.a(9);
                }
                if (this.s) {
                    this.k.a(2);
                }
                if (this.t) {
                    this.k.a(5);
                }
            } else {
                if (this.d) {
                    this.p = (ViewGroup) LayoutInflater.from(this.f104a).inflate(android.support.v7.a.i.abc_screen_simple_overlay_action_mode, (ViewGroup) null);
                } else {
                    this.p = (ViewGroup) LayoutInflater.from(this.f104a).inflate(android.support.v7.a.i.abc_screen_simple, (ViewGroup) null);
                }
                if (Build.VERSION.SDK_INT >= 21) {
                    au.a(this.p, new i(this));
                } else {
                    ((android.support.v7.internal.widget.z) this.p).setOnFitSystemWindowsListener(new j(this));
                }
            }
            bh.b(this.p);
            this.f104a.a((View) this.p);
            View findViewById = this.f104a.findViewById(16908290);
            findViewById.setId(-1);
            this.f104a.findViewById(android.support.v7.a.g.action_bar_activity_content).setId(16908290);
            if (findViewById instanceof FrameLayout) {
                ((FrameLayout) findViewById).setForeground(null);
            }
            if (!(this.r == null || this.k == null)) {
                this.k.setWindowTitle(this.r);
                this.r = null;
            }
            p();
            o();
            this.n = true;
            PanelFeatureState a2 = a(0, false);
            if (m()) {
                return;
            }
            if (a2 == null || a2.d == null) {
                c(8);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void o() {
    }
}
