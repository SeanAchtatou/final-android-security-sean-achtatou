package android.support.v7.app;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.a.l;
import android.support.v7.internal.a.a;
import android.support.v7.internal.view.e;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

abstract class f {

    /* renamed from: a  reason: collision with root package name */
    final e f104a;
    boolean b;
    boolean c;
    boolean d;
    boolean e;
    final a f = new g(this);
    private a g;
    private MenuInflater h;
    private a i;
    private boolean j;

    f(e eVar) {
        this.f104a = eVar;
        this.i = this.f;
    }

    static f a(e eVar) {
        return Build.VERSION.SDK_INT >= 11 ? new p(eVar) : new ActionBarActivityDelegateBase(eVar);
    }

    /* access modifiers changed from: package-private */
    public abstract a a();

    /* access modifiers changed from: package-private */
    public abstract View a(String str, Context context, AttributeSet attributeSet);

    /* access modifiers changed from: package-private */
    public abstract void a(int i2);

    /* access modifiers changed from: package-private */
    public abstract void a(int i2, Menu menu);

    /* access modifiers changed from: package-private */
    public abstract void a(Configuration configuration);

    /* access modifiers changed from: package-private */
    public void a(Bundle bundle) {
        TypedArray obtainStyledAttributes = this.f104a.obtainStyledAttributes(l.Theme);
        if (!obtainStyledAttributes.hasValue(l.Theme_windowActionBar)) {
            obtainStyledAttributes.recycle();
            throw new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
        }
        if (obtainStyledAttributes.getBoolean(l.Theme_windowActionBar, false)) {
            this.b = true;
        }
        if (obtainStyledAttributes.getBoolean(l.Theme_windowActionBarOverlay, false)) {
            this.c = true;
        }
        if (obtainStyledAttributes.getBoolean(l.Theme_windowActionModeOverlay, false)) {
            this.d = true;
        }
        this.e = obtainStyledAttributes.getBoolean(l.Theme_android_windowIsFloating, false);
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: package-private */
    public abstract void a(View view);

    /* access modifiers changed from: package-private */
    public abstract void a(View view, ViewGroup.LayoutParams layoutParams);

    /* access modifiers changed from: package-private */
    public abstract void a(CharSequence charSequence);

    /* access modifiers changed from: package-private */
    public boolean a(int i2, KeyEvent keyEvent) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public abstract boolean a(int i2, View view, Menu menu);

    /* access modifiers changed from: package-private */
    public boolean a(View view, Menu menu) {
        return Build.VERSION.SDK_INT < 16 ? this.f104a.onPrepareOptionsMenu(menu) : this.f104a.b(view, menu);
    }

    /* access modifiers changed from: package-private */
    public final a b() {
        if (this.b && this.g == null) {
            this.g = a();
        }
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public abstract View b(int i2);

    /* access modifiers changed from: package-private */
    public abstract void b(View view, ViewGroup.LayoutParams layoutParams);

    /* access modifiers changed from: package-private */
    public abstract boolean b(int i2, KeyEvent keyEvent);

    /* access modifiers changed from: package-private */
    public abstract boolean b(int i2, Menu menu);

    /* access modifiers changed from: package-private */
    public final a c() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public abstract boolean c(int i2, Menu menu);

    /* access modifiers changed from: package-private */
    public MenuInflater d() {
        if (this.h == null) {
            this.h = new e(j());
        }
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public abstract void e();

    /* access modifiers changed from: package-private */
    public abstract void f();

    /* access modifiers changed from: package-private */
    public abstract void g();

    /* access modifiers changed from: package-private */
    public abstract boolean h();

    /* access modifiers changed from: package-private */
    public abstract void i();

    /* access modifiers changed from: protected */
    public final Context j() {
        Context context = null;
        a b2 = b();
        if (b2 != null) {
            context = b2.b();
        }
        return context == null ? this.f104a : context;
    }

    /* access modifiers changed from: package-private */
    public final a k() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public final void l() {
        this.j = true;
    }

    /* access modifiers changed from: package-private */
    public final boolean m() {
        return this.j;
    }
}
