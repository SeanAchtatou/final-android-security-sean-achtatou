package android.support.v7.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivityDelegateBase;

final class n implements Parcelable.Creator {
    n() {
    }

    /* renamed from: a */
    public ActionBarActivityDelegateBase.PanelFeatureState.SavedState createFromParcel(Parcel parcel) {
        return ActionBarActivityDelegateBase.PanelFeatureState.SavedState.b(parcel);
    }

    /* renamed from: a */
    public ActionBarActivityDelegateBase.PanelFeatureState.SavedState[] newArray(int i) {
        return new ActionBarActivityDelegateBase.PanelFeatureState.SavedState[i];
    }
}
