package android.support.v7.app;

import android.support.v7.app.ActionBarActivityDelegateBase;
import android.support.v7.internal.a.a;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.y;
import android.view.Menu;

final class o implements y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ActionBarActivityDelegateBase f112a;

    private o(ActionBarActivityDelegateBase actionBarActivityDelegateBase) {
        this.f112a = actionBarActivityDelegateBase;
    }

    /* synthetic */ o(ActionBarActivityDelegateBase actionBarActivityDelegateBase, h hVar) {
        this(actionBarActivityDelegateBase);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.view.Menu):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
     arg types: [android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.internal.view.menu.i]
     candidates:
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, boolean):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, android.view.KeyEvent):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, int):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.internal.view.menu.i):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, boolean):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.Menu):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.f.a(int, android.view.Menu):void
      android.support.v7.app.f.a(android.view.View, android.view.ViewGroup$LayoutParams):void
      android.support.v7.app.f.a(int, android.view.KeyEvent):boolean
      android.support.v7.app.f.a(android.view.View, android.view.Menu):boolean
      android.support.v7.internal.view.menu.j.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.view.Menu):android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void
     arg types: [android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, int]
     candidates:
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, android.view.Menu):void
      android.support.v7.app.ActionBarActivityDelegateBase.a(java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v7.app.ActionBarActivityDelegateBase.a(int, android.view.View, android.view.Menu):boolean
      android.support.v7.app.f.a(java.lang.String, android.content.Context, android.util.AttributeSet):android.view.View
      android.support.v7.app.f.a(int, android.view.View, android.view.Menu):boolean
      android.support.v7.app.ActionBarActivityDelegateBase.a(android.support.v7.app.ActionBarActivityDelegateBase, android.support.v7.app.ActionBarActivityDelegateBase$PanelFeatureState, boolean):void */
    public void a(i iVar, boolean z) {
        i p = iVar.p();
        boolean z2 = p != iVar;
        ActionBarActivityDelegateBase actionBarActivityDelegateBase = this.f112a;
        if (z2) {
            iVar = p;
        }
        ActionBarActivityDelegateBase.PanelFeatureState a2 = actionBarActivityDelegateBase.a((Menu) iVar);
        if (a2 == null) {
            return;
        }
        if (z2) {
            this.f112a.a(a2.f101a, a2, p);
            this.f112a.a(a2, true);
            return;
        }
        this.f112a.f104a.closeOptionsMenu();
        this.f112a.a(a2, z);
    }

    public boolean a(i iVar) {
        a k;
        if (iVar != null || !this.f112a.b || (k = this.f112a.k()) == null || this.f112a.m()) {
            return true;
        }
        k.c(8, iVar);
        return true;
    }
}
