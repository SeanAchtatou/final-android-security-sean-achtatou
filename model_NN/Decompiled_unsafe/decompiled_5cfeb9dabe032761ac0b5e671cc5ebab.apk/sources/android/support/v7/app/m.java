package android.support.v7.app;

import android.support.v4.view.au;
import android.support.v7.b.a;
import android.support.v7.b.b;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

class m implements b {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ActionBarActivityDelegateBase f111a;
    private b b;

    public m(ActionBarActivityDelegateBase actionBarActivityDelegateBase, b bVar) {
        this.f111a = actionBarActivityDelegateBase;
        this.b = bVar;
    }

    public void a(a aVar) {
        this.b.a(aVar);
        if (this.f111a.i != null) {
            this.f111a.f104a.getWindow().getDecorView().removeCallbacks(this.f111a.j);
            this.f111a.i.dismiss();
        } else if (this.f111a.h != null) {
            this.f111a.h.setVisibility(8);
            if (this.f111a.h.getParent() != null) {
                au.k((View) this.f111a.h.getParent());
            }
        }
        if (this.f111a.h != null) {
            this.f111a.h.removeAllViews();
        }
        if (this.f111a.f104a != null) {
            try {
                this.f111a.f104a.b(this.f111a.g);
            } catch (AbstractMethodError e) {
            }
        }
        this.f111a.g = null;
    }

    public boolean a(a aVar, Menu menu) {
        return this.b.a(aVar, menu);
    }

    public boolean a(a aVar, MenuItem menuItem) {
        return this.b.a(aVar, menuItem);
    }

    public boolean b(a aVar, Menu menu) {
        return this.b.b(aVar, menu);
    }
}
