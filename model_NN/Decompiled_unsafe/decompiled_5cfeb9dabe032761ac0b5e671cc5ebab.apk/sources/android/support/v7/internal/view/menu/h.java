package android.support.v7.internal.view.menu;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

class h extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f137a;
    private int b = -1;

    public h(g gVar) {
        this.f137a = gVar;
        a();
    }

    /* renamed from: a */
    public m getItem(int i) {
        ArrayList l = this.f137a.c.l();
        int a2 = this.f137a.h + i;
        if (this.b >= 0 && a2 >= this.b) {
            a2++;
        }
        return (m) l.get(a2);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        m r = this.f137a.c.r();
        if (r != null) {
            ArrayList l = this.f137a.c.l();
            int size = l.size();
            for (int i = 0; i < size; i++) {
                if (((m) l.get(i)) == r) {
                    this.b = i;
                    return;
                }
            }
        }
        this.b = -1;
    }

    public int getCount() {
        int size = this.f137a.c.l().size() - this.f137a.h;
        return this.b < 0 ? size : size - 1;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.f137a.b.inflate(this.f137a.f, viewGroup, false) : view;
        ((aa) inflate).a(getItem(i), 0);
        return inflate;
    }

    public void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }
}
