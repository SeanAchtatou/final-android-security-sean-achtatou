package android.support.v7.internal.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.a.b;
import android.support.v7.a.e;
import android.support.v7.a.i;
import android.support.v7.widget.q;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.PopupWindow;

public class v implements x, View.OnKeyListener, ViewTreeObserver.OnGlobalLayoutListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    static final int f146a = i.abc_popup_menu_item_layout;
    boolean b;
    private final Context c;
    /* access modifiers changed from: private */
    public final LayoutInflater d;
    /* access modifiers changed from: private */
    public final i e;
    private final w f;
    /* access modifiers changed from: private */
    public final boolean g;
    private final int h;
    private final int i;
    private final int j;
    private View k;
    private q l;
    private ViewTreeObserver m;
    private y n;
    private ViewGroup o;
    private boolean p;
    private int q;
    private int r;

    public v(Context context, i iVar, View view) {
        this(context, iVar, view, false, b.popupMenuStyle);
    }

    public v(Context context, i iVar, View view, boolean z, int i2) {
        this(context, iVar, view, z, i2, 0);
    }

    public v(Context context, i iVar, View view, boolean z, int i2, int i3) {
        this.r = 0;
        this.c = context;
        this.d = LayoutInflater.from(context);
        this.e = iVar;
        this.f = new w(this, this.e);
        this.g = z;
        this.i = i2;
        this.j = i3;
        Resources resources = context.getResources();
        this.h = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(e.abc_config_prefDialogWidth));
        this.k = view;
        iVar.a(this, context);
    }

    private int g() {
        View view;
        w wVar = this.f;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
        int count = wVar.getCount();
        int i2 = 0;
        int i3 = 0;
        View view2 = null;
        int i4 = 0;
        while (i2 < count) {
            int itemViewType = wVar.getItemViewType(i2);
            if (itemViewType != i3) {
                i3 = itemViewType;
                view = null;
            } else {
                view = view2;
            }
            if (this.o == null) {
                this.o = new FrameLayout(this.c);
            }
            view2 = wVar.getView(i2, view, this.o);
            view2.measure(makeMeasureSpec, makeMeasureSpec2);
            int measuredWidth = view2.getMeasuredWidth();
            if (measuredWidth >= this.h) {
                return this.h;
            }
            if (measuredWidth <= i4) {
                measuredWidth = i4;
            }
            i2++;
            i4 = measuredWidth;
        }
        return i4;
    }

    public void a() {
        if (!d()) {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    public void a(int i2) {
        this.r = i2;
    }

    public void a(Context context, i iVar) {
    }

    public void a(i iVar, boolean z) {
        if (iVar == this.e) {
            e();
            if (this.n != null) {
                this.n.a(iVar, z);
            }
        }
    }

    public void a(y yVar) {
        this.n = yVar;
    }

    public void a(View view) {
        this.k = view;
    }

    public void a(boolean z) {
        this.p = false;
        if (this.f != null) {
            this.f.notifyDataSetChanged();
        }
    }

    public boolean a(ad adVar) {
        boolean z;
        if (adVar.hasVisibleItems()) {
            v vVar = new v(this.c, adVar, this.k);
            vVar.a(this.n);
            int size = adVar.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    z = false;
                    break;
                }
                MenuItem item = adVar.getItem(i2);
                if (item.isVisible() && item.getIcon() != null) {
                    z = true;
                    break;
                }
                i2++;
            }
            vVar.b(z);
            if (vVar.d()) {
                if (this.n == null) {
                    return true;
                }
                this.n.a(adVar);
                return true;
            }
        }
        return false;
    }

    public boolean a(i iVar, m mVar) {
        return false;
    }

    public void b(boolean z) {
        this.b = z;
    }

    public boolean b() {
        return false;
    }

    public boolean b(i iVar, m mVar) {
        return false;
    }

    public q c() {
        return this.l;
    }

    public boolean d() {
        boolean z = false;
        this.l = new q(this.c, null, this.i, this.j);
        this.l.a((PopupWindow.OnDismissListener) this);
        this.l.a((AdapterView.OnItemClickListener) this);
        this.l.a(this.f);
        this.l.a(true);
        View view = this.k;
        if (view == null) {
            return false;
        }
        if (this.m == null) {
            z = true;
        }
        this.m = view.getViewTreeObserver();
        if (z) {
            this.m.addOnGlobalLayoutListener(this);
        }
        this.l.a(view);
        this.l.b(this.r);
        if (!this.p) {
            this.q = g();
            this.p = true;
        }
        this.l.d(this.q);
        this.l.e(2);
        this.l.c();
        this.l.g().setOnKeyListener(this);
        return true;
    }

    public void e() {
        if (f()) {
            this.l.a();
        }
    }

    public boolean f() {
        return this.l != null && this.l.b();
    }

    public void onDismiss() {
        this.l = null;
        this.e.close();
        if (this.m != null) {
            if (!this.m.isAlive()) {
                this.m = this.k.getViewTreeObserver();
            }
            this.m.removeGlobalOnLayoutListener(this);
            this.m = null;
        }
    }

    public void onGlobalLayout() {
        if (f()) {
            View view = this.k;
            if (view == null || !view.isShown()) {
                e();
            } else if (f()) {
                this.l.c();
            }
        }
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        w wVar = this.f;
        wVar.b.a(wVar.getItem(i2), 0);
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        e();
        return true;
    }
}
