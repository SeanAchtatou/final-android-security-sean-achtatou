package android.support.v7.internal.view.menu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.IBinder;
import android.support.v7.a.i;
import android.support.v7.a.k;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class l implements DialogInterface.OnClickListener, DialogInterface.OnDismissListener, DialogInterface.OnKeyListener, y {

    /* renamed from: a  reason: collision with root package name */
    g f139a;
    private i b;
    private AlertDialog c;
    private y d;

    public l(i iVar) {
        this.b = iVar;
    }

    public void a() {
        if (this.c != null) {
            this.c.dismiss();
        }
    }

    public void a(IBinder iBinder) {
        i iVar = this.b;
        AlertDialog.Builder builder = new AlertDialog.Builder(iVar.e());
        this.f139a = new g(i.abc_list_menu_item_layout, k.Theme_AppCompat_CompactMenu);
        this.f139a.a(this);
        this.b.a(this.f139a);
        builder.setAdapter(this.f139a.a(), this);
        View o = iVar.o();
        if (o != null) {
            builder.setCustomTitle(o);
        } else {
            builder.setIcon(iVar.n()).setTitle(iVar.m());
        }
        builder.setOnKeyListener(this);
        this.c = builder.create();
        this.c.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.c.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder != null) {
            attributes.token = iBinder;
        }
        attributes.flags |= 131072;
        this.c.show();
    }

    public void a(i iVar, boolean z) {
        if (z || iVar == this.b) {
            a();
        }
        if (this.d != null) {
            this.d.a(iVar, z);
        }
    }

    public boolean a(i iVar) {
        if (this.d != null) {
            return this.d.a(iVar);
        }
        return false;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.b.a((m) this.f139a.a().getItem(i), 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.view.menu.g.a(android.support.v7.internal.view.menu.i, boolean):void
     arg types: [android.support.v7.internal.view.menu.i, int]
     candidates:
      android.support.v7.internal.view.menu.g.a(android.content.Context, android.support.v7.internal.view.menu.i):void
      android.support.v7.internal.view.menu.g.a(android.support.v7.internal.view.menu.i, android.support.v7.internal.view.menu.m):boolean
      android.support.v7.internal.view.menu.x.a(android.content.Context, android.support.v7.internal.view.menu.i):void
      android.support.v7.internal.view.menu.x.a(android.support.v7.internal.view.menu.i, android.support.v7.internal.view.menu.m):boolean
      android.support.v7.internal.view.menu.g.a(android.support.v7.internal.view.menu.i, boolean):void */
    public void onDismiss(DialogInterface dialogInterface) {
        this.f139a.a(this.b, true);
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        if (i == 82 || i == 4) {
            if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                Window window2 = this.c.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent, this);
                    return true;
                }
            } else if (keyEvent.getAction() == 1 && !keyEvent.isCanceled() && (window = this.c.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent)) {
                this.b.a(true);
                dialogInterface.dismiss();
                return true;
            }
        }
        return this.b.performShortcut(i, keyEvent, 0);
    }
}
