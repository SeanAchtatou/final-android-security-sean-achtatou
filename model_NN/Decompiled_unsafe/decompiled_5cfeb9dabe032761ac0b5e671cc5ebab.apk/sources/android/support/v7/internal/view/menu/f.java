package android.support.v7.internal.view.menu;

class f {
    final Object b;

    f(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("Wrapped Object can not be null.");
        }
        this.b = obj;
    }
}
