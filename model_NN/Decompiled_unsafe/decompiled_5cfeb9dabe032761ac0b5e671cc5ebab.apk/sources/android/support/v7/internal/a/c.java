package android.support.v7.internal.a;

import android.support.v4.view.au;
import android.support.v4.view.cw;
import android.support.v7.internal.view.h;
import android.view.View;

class c extends cw {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f115a;

    c(b bVar) {
        this.f115a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.au.b(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.au.b(android.view.View, int):void
      android.support.v4.view.au.b(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.au.b(android.view.View, float):void
     arg types: [android.support.v7.internal.widget.ActionBarContainer, int]
     candidates:
      android.support.v4.view.au.b(android.view.View, int):void
      android.support.v4.view.au.b(android.view.View, float):void */
    public void b(View view) {
        if (this.f115a.B && this.f115a.r != null) {
            au.b(this.f115a.r, 0.0f);
            au.b((View) this.f115a.n, 0.0f);
        }
        if (this.f115a.q != null && this.f115a.y == 1) {
            this.f115a.q.setVisibility(8);
        }
        this.f115a.n.setVisibility(8);
        this.f115a.n.setTransitioning(false);
        h unused = this.f115a.G = (h) null;
        this.f115a.e();
        if (this.f115a.m != null) {
            au.k(this.f115a.m);
        }
    }
}
