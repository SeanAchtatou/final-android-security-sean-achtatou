package android.support.v7.internal.widget;

import android.support.v4.view.cf;
import android.support.v4.view.cv;
import android.view.View;

public class b implements cv {

    /* renamed from: a  reason: collision with root package name */
    int f179a;
    final /* synthetic */ a b;
    private boolean c = false;

    protected b(a aVar) {
        this.b = aVar;
    }

    public b a(cf cfVar, int i) {
        this.b.i = cfVar;
        this.f179a = i;
        return this;
    }

    public void a(View view) {
        this.b.setVisibility(0);
        this.c = false;
    }

    public void b(View view) {
        if (!this.c) {
            this.b.i = null;
            this.b.setVisibility(this.f179a);
            if (this.b.e != null && this.b.c != null) {
                this.b.c.setVisibility(this.f179a);
            }
        }
    }

    public void c(View view) {
        this.c = true;
    }
}
