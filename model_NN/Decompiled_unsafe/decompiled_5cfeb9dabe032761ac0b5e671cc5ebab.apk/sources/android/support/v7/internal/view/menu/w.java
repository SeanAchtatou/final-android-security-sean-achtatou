package android.support.v7.internal.view.menu;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;

class w extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ v f147a;
    /* access modifiers changed from: private */
    public i b;
    private int c = -1;

    public w(v vVar, i iVar) {
        this.f147a = vVar;
        this.b = iVar;
        a();
    }

    /* renamed from: a */
    public m getItem(int i) {
        ArrayList l = this.f147a.g ? this.b.l() : this.b.i();
        if (this.c >= 0 && i >= this.c) {
            i++;
        }
        return (m) l.get(i);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        m r = this.f147a.e.r();
        if (r != null) {
            ArrayList l = this.f147a.e.l();
            int size = l.size();
            for (int i = 0; i < size; i++) {
                if (((m) l.get(i)) == r) {
                    this.c = i;
                    return;
                }
            }
        }
        this.c = -1;
    }

    public int getCount() {
        ArrayList l = this.f147a.g ? this.b.l() : this.b.i();
        return this.c < 0 ? l.size() : l.size() - 1;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = view == null ? this.f147a.d.inflate(v.f146a, viewGroup, false) : view;
        aa aaVar = (aa) inflate;
        if (this.f147a.b) {
            ((ListMenuItemView) inflate).setForceShowIcon(true);
        }
        aaVar.a(getItem(i), 0);
        return inflate;
    }

    public void notifyDataSetChanged() {
        a();
        super.notifyDataSetChanged();
    }
}
