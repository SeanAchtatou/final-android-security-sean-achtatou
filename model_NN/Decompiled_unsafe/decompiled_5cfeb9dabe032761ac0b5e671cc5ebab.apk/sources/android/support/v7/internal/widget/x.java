package android.support.v7.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.internal.a.a;
import android.support.v7.internal.view.menu.y;
import android.view.Menu;
import android.view.ViewGroup;

public interface x {
    ViewGroup a();

    void a(int i);

    void a(Drawable drawable);

    void a(a aVar);

    void a(af afVar);

    void a(Menu menu, y yVar);

    void a(CharSequence charSequence);

    void a(boolean z);

    Context b();

    void b(int i);

    void b(boolean z);

    void c(int i);

    boolean c();

    void d(int i);

    boolean d();

    void e();

    CharSequence f();

    void g();

    void h();

    boolean i();

    boolean j();

    boolean k();

    boolean l();

    boolean m();

    void n();

    void o();

    int p();

    int q();
}
