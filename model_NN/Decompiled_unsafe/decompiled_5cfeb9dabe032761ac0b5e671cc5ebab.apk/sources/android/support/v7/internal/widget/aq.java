package android.support.v7.internal.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.internal.widget.SpinnerCompat;

final class aq implements Parcelable.Creator {
    aq() {
    }

    /* renamed from: a */
    public SpinnerCompat.SavedState createFromParcel(Parcel parcel) {
        return new SpinnerCompat.SavedState(parcel, null);
    }

    /* renamed from: a */
    public SpinnerCompat.SavedState[] newArray(int i) {
        return new SpinnerCompat.SavedState[i];
    }
}
