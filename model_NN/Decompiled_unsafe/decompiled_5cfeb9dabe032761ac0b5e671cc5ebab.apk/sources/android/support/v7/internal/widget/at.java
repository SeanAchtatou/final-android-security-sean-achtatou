package android.support.v7.internal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckedTextView;

public class at extends CheckedTextView {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f173a = {16843016};
    private final aw b;

    public at(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16843720);
    }

    public at(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        bb a2 = bb.a(context, attributeSet, f173a, i, 0);
        setCheckMarkDrawable(a2.a(0));
        a2.b();
        this.b = a2.c();
    }

    public void setCheckMarkDrawable(int i) {
        setCheckMarkDrawable(this.b.a(i));
    }
}
