package android.support.v7.internal.view.menu;

import android.support.v4.view.af;
import android.view.MenuItem;

class r extends f implements af {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f144a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    r(o oVar, MenuItem.OnActionExpandListener onActionExpandListener) {
        super(onActionExpandListener);
        this.f144a = oVar;
    }

    public boolean a(MenuItem menuItem) {
        return ((MenuItem.OnActionExpandListener) this.b).onMenuItemActionExpand(this.f144a.a(menuItem));
    }

    public boolean b(MenuItem menuItem) {
        return ((MenuItem.OnActionExpandListener) this.b).onMenuItemActionCollapse(this.f144a.a(menuItem));
    }
}
