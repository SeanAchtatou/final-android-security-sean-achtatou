package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.a.l;
import android.support.v7.internal.b.a;
import android.util.AttributeSet;
import android.widget.TextView;

public class v extends TextView {
    public v(Context context) {
        this(context, null);
    }

    public v(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public v(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.CompatTextView, i, 0);
        boolean z = obtainStyledAttributes.getBoolean(l.CompatTextView_textAllCaps, false);
        obtainStyledAttributes.recycle();
        if (z) {
            setTransformationMethod(new a(context));
        }
    }
}
