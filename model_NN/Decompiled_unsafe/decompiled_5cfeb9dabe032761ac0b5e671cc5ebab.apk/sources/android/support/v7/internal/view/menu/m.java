package android.support.v7.internal.view.menu;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.c.a.b;
import android.support.v4.view.af;
import android.support.v4.view.g;
import android.support.v4.view.i;
import android.support.v7.a.l;
import android.support.v7.internal.widget.aw;
import android.util.Log;
import android.view.ActionProvider;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public final class m implements b {
    private static String w;
    private static String x;
    private static String y;
    private static String z;

    /* renamed from: a  reason: collision with root package name */
    private final int f140a;
    private final int b;
    private final int c;
    private final int d;
    private CharSequence e;
    private CharSequence f;
    private Intent g;
    private char h;
    private char i;
    private Drawable j;
    private int k = 0;
    /* access modifiers changed from: private */
    public i l;
    private ad m;
    private Runnable n;
    private MenuItem.OnMenuItemClickListener o;
    private int p = 16;
    private int q = 0;
    private View r;
    private g s;
    private af t;
    private boolean u = false;
    private ContextMenu.ContextMenuInfo v;

    m(i iVar, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6) {
        this.l = iVar;
        this.f140a = i3;
        this.b = i2;
        this.c = i4;
        this.d = i5;
        this.e = charSequence;
        this.q = i6;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: a */
    public b setActionView(int i2) {
        Context e2 = this.l.e();
        setActionView(LayoutInflater.from(e2).inflate(i2, (ViewGroup) new LinearLayout(e2), false));
        return this;
    }

    public b a(af afVar) {
        this.t = afVar;
        return this;
    }

    public b a(g gVar) {
        if (this.s != null) {
            this.s.a((i) null);
        }
        this.r = null;
        this.s = gVar;
        this.l.b(true);
        if (this.s != null) {
            this.s.a(new n(this));
        }
        return this;
    }

    /* renamed from: a */
    public b setActionView(View view) {
        this.r = view;
        this.s = null;
        if (view != null && view.getId() == -1 && this.f140a > 0) {
            view.setId(this.f140a);
        }
        this.l.b(this);
        return this;
    }

    public g a() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public CharSequence a(aa aaVar) {
        return (aaVar == null || !aaVar.a()) ? getTitle() : getTitleCondensed();
    }

    /* access modifiers changed from: package-private */
    public void a(ad adVar) {
        this.m = adVar;
        adVar.setHeaderTitle(getTitle());
    }

    /* access modifiers changed from: package-private */
    public void a(ContextMenu.ContextMenuInfo contextMenuInfo) {
        this.v = contextMenuInfo;
    }

    public void a(boolean z2) {
        this.p = (z2 ? 4 : 0) | (this.p & -5);
    }

    /* renamed from: b */
    public b setShowAsActionFlags(int i2) {
        setShowAsAction(i2);
        return this;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        int i2 = this.p;
        this.p = (z2 ? 2 : 0) | (this.p & -3);
        if (i2 != this.p) {
            this.l.b(false);
        }
    }

    public boolean b() {
        if ((this.o != null && this.o.onMenuItemClick(this)) || this.l.a(this.l.p(), this)) {
            return true;
        }
        if (this.n != null) {
            this.n.run();
            return true;
        }
        if (this.g != null) {
            try {
                this.l.e().startActivity(this.g);
                return true;
            } catch (ActivityNotFoundException e2) {
                Log.e("MenuItemImpl", "Can't find activity to handle intent; ignoring", e2);
            }
        }
        return this.s != null && this.s.d();
    }

    public int c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public boolean c(boolean z2) {
        int i2 = this.p;
        this.p = (z2 ? 0 : 8) | (this.p & -9);
        return i2 != this.p;
    }

    public boolean collapseActionView() {
        if ((this.q & 8) == 0) {
            return false;
        }
        if (this.r == null) {
            return true;
        }
        if (this.t == null || this.t.b(this)) {
            return this.l.d(this);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public char d() {
        return this.l.b() ? this.i : this.h;
    }

    public void d(boolean z2) {
        if (z2) {
            this.p |= 32;
        } else {
            this.p &= -33;
        }
    }

    /* access modifiers changed from: package-private */
    public String e() {
        char d2 = d();
        if (d2 == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(w);
        switch (d2) {
            case 8:
                sb.append(y);
                break;
            case 10:
                sb.append(x);
                break;
            case l.Theme_actionModeShareDrawable:
                sb.append(z);
                break;
            default:
                sb.append(d2);
                break;
        }
        return sb.toString();
    }

    public void e(boolean z2) {
        this.u = z2;
        this.l.b(false);
    }

    public boolean expandActionView() {
        if (!n()) {
            return false;
        }
        if (this.t == null || this.t.a(this)) {
            return this.l.c(this);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return this.l.c() && d() != 0;
    }

    public boolean g() {
        return (this.p & 4) != 0;
    }

    public ActionProvider getActionProvider() {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.getActionProvider()");
    }

    public View getActionView() {
        if (this.r != null) {
            return this.r;
        }
        if (this.s == null) {
            return null;
        }
        this.r = this.s.a(this);
        return this.r;
    }

    public char getAlphabeticShortcut() {
        return this.i;
    }

    public int getGroupId() {
        return this.b;
    }

    public Drawable getIcon() {
        if (this.j != null) {
            return this.j;
        }
        if (this.k == 0) {
            return null;
        }
        Drawable a2 = aw.a(this.l.e(), this.k);
        this.k = 0;
        this.j = a2;
        return a2;
    }

    public Intent getIntent() {
        return this.g;
    }

    @ViewDebug.CapturedViewProperty
    public int getItemId() {
        return this.f140a;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return this.v;
    }

    public char getNumericShortcut() {
        return this.h;
    }

    public int getOrder() {
        return this.c;
    }

    public SubMenu getSubMenu() {
        return this.m;
    }

    @ViewDebug.CapturedViewProperty
    public CharSequence getTitle() {
        return this.e;
    }

    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.f != null ? this.f : this.e;
        return (Build.VERSION.SDK_INT >= 18 || charSequence == null || (charSequence instanceof String)) ? charSequence : charSequence.toString();
    }

    public void h() {
        this.l.b(this);
    }

    public boolean hasSubMenu() {
        return this.m != null;
    }

    public boolean i() {
        return this.l.q();
    }

    public boolean isActionViewExpanded() {
        return this.u;
    }

    public boolean isCheckable() {
        return (this.p & 1) == 1;
    }

    public boolean isChecked() {
        return (this.p & 2) == 2;
    }

    public boolean isEnabled() {
        return (this.p & 16) != 0;
    }

    public boolean isVisible() {
        return (this.s == null || !this.s.b()) ? (this.p & 8) == 0 : (this.p & 8) == 0 && this.s.c();
    }

    public boolean j() {
        return (this.p & 32) == 32;
    }

    public boolean k() {
        return (this.q & 1) == 1;
    }

    public boolean l() {
        return (this.q & 2) == 2;
    }

    public boolean m() {
        return (this.q & 4) == 4;
    }

    public boolean n() {
        if ((this.q & 8) == 0) {
            return false;
        }
        if (this.r == null && this.s != null) {
            this.r = this.s.a(this);
        }
        return this.r != null;
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setActionProvider()");
    }

    public MenuItem setAlphabeticShortcut(char c2) {
        if (this.i != c2) {
            this.i = Character.toLowerCase(c2);
            this.l.b(false);
        }
        return this;
    }

    public MenuItem setCheckable(boolean z2) {
        int i2 = this.p;
        this.p = (z2 ? 1 : 0) | (this.p & -2);
        if (i2 != this.p) {
            this.l.b(false);
        }
        return this;
    }

    public MenuItem setChecked(boolean z2) {
        if ((this.p & 4) != 0) {
            this.l.a((MenuItem) this);
        } else {
            b(z2);
        }
        return this;
    }

    public MenuItem setEnabled(boolean z2) {
        if (z2) {
            this.p |= 16;
        } else {
            this.p &= -17;
        }
        this.l.b(false);
        return this;
    }

    public MenuItem setIcon(int i2) {
        this.j = null;
        this.k = i2;
        this.l.b(false);
        return this;
    }

    public MenuItem setIcon(Drawable drawable) {
        this.k = 0;
        this.j = drawable;
        this.l.b(false);
        return this;
    }

    public MenuItem setIntent(Intent intent) {
        this.g = intent;
        return this;
    }

    public MenuItem setNumericShortcut(char c2) {
        if (this.h != c2) {
            this.h = c2;
            this.l.b(false);
        }
        return this;
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setOnActionExpandListener()");
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.o = onMenuItemClickListener;
        return this;
    }

    public MenuItem setShortcut(char c2, char c3) {
        this.h = c2;
        this.i = Character.toLowerCase(c3);
        this.l.b(false);
        return this;
    }

    public void setShowAsAction(int i2) {
        switch (i2 & 3) {
            case 0:
            case 1:
            case 2:
                break;
            default:
                throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
        }
        this.q = i2;
        this.l.b(this);
    }

    public MenuItem setTitle(int i2) {
        return setTitle(this.l.e().getString(i2));
    }

    public MenuItem setTitle(CharSequence charSequence) {
        this.e = charSequence;
        this.l.b(false);
        if (this.m != null) {
            this.m.setHeaderTitle(charSequence);
        }
        return this;
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.f = charSequence;
        if (charSequence == null) {
            CharSequence charSequence2 = this.e;
        }
        this.l.b(false);
        return this;
    }

    public MenuItem setVisible(boolean z2) {
        if (c(z2)) {
            this.l.a(this);
        }
        return this;
    }

    public String toString() {
        return this.e.toString();
    }
}
