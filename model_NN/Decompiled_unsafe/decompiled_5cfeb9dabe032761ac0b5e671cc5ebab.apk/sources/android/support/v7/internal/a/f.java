package android.support.v7.internal.a;

import android.support.v7.b.a;
import android.support.v7.b.b;
import android.support.v7.internal.view.e;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.j;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.lang.ref.WeakReference;

public class f extends a implements j {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f118a;
    private b b;
    private i c;
    private WeakReference d;

    public f(b bVar, b bVar2) {
        this.f118a = bVar;
        this.b = bVar2;
        this.c = new i(bVar.b()).a(1);
        this.c.a(this);
    }

    public MenuInflater a() {
        return new e(this.f118a.b());
    }

    public void a(int i) {
        b(this.f118a.j.getResources().getString(i));
    }

    public void a(i iVar) {
        if (this.b != null) {
            d();
            this.f118a.p.a();
        }
    }

    public void a(View view) {
        this.f118a.p.setCustomView(view);
        this.d = new WeakReference(view);
    }

    public void a(CharSequence charSequence) {
        this.f118a.p.setSubtitle(charSequence);
    }

    public void a(boolean z) {
        super.a(z);
        this.f118a.p.setTitleOptional(z);
    }

    public boolean a(i iVar, MenuItem menuItem) {
        if (this.b != null) {
            return this.b.a(this, menuItem);
        }
        return false;
    }

    public Menu b() {
        return this.c;
    }

    public void b(int i) {
        a((CharSequence) this.f118a.j.getResources().getString(i));
    }

    public void b(CharSequence charSequence) {
        this.f118a.p.setTitle(charSequence);
    }

    public void c() {
        if (this.f118a.f114a == this) {
            if (!b.b(this.f118a.C, this.f118a.D, false)) {
                this.f118a.b = this;
                this.f118a.c = this.b;
            } else {
                this.b.a(this);
            }
            this.b = null;
            this.f118a.j(false);
            this.f118a.p.b();
            this.f118a.o.a().sendAccessibilityEvent(32);
            this.f118a.m.setHideOnContentScrollEnabled(this.f118a.d);
            this.f118a.f114a = null;
        }
    }

    public void d() {
        this.c.g();
        try {
            this.b.b(this, this.c);
        } finally {
            this.c.h();
        }
    }

    public boolean e() {
        this.c.g();
        try {
            return this.b.a(this, this.c);
        } finally {
            this.c.h();
        }
    }

    public CharSequence f() {
        return this.f118a.p.getTitle();
    }

    public CharSequence g() {
        return this.f118a.p.getSubtitle();
    }

    public boolean h() {
        return this.f118a.p.d();
    }

    public View i() {
        if (this.d != null) {
            return (View) this.d.get();
        }
        return null;
    }
}
