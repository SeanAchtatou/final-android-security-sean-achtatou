package android.support.v7.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.a.b;
import android.support.v7.app.d;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.p;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

class aj extends LinearLayoutCompat implements View.OnLongClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ af f165a;
    private final int[] b = {16842964};
    private d c;
    private TextView d;
    private ImageView e;
    private View f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aj(af afVar, Context context, d dVar, boolean z) {
        super(context, null, b.actionBarTabStyle);
        this.f165a = afVar;
        this.c = dVar;
        bb a2 = bb.a(context, null, this.b, b.actionBarTabStyle, 0);
        if (a2.d(0)) {
            setBackgroundDrawable(a2.a(0));
        }
        a2.b();
        if (z) {
            setGravity(8388627);
        }
        a();
    }

    public void a() {
        d dVar = this.c;
        View c2 = dVar.c();
        if (c2 != null) {
            ViewParent parent = c2.getParent();
            if (parent != this) {
                if (parent != null) {
                    ((ViewGroup) parent).removeView(c2);
                }
                addView(c2);
            }
            this.f = c2;
            if (this.d != null) {
                this.d.setVisibility(8);
            }
            if (this.e != null) {
                this.e.setVisibility(8);
                this.e.setImageDrawable(null);
                return;
            }
            return;
        }
        if (this.f != null) {
            removeView(this.f);
            this.f = null;
        }
        Drawable a2 = dVar.a();
        CharSequence b2 = dVar.b();
        if (a2 != null) {
            if (this.e == null) {
                ImageView imageView = new ImageView(getContext());
                p pVar = new p(-2, -2);
                pVar.h = 16;
                imageView.setLayoutParams(pVar);
                addView(imageView, 0);
                this.e = imageView;
            }
            this.e.setImageDrawable(a2);
            this.e.setVisibility(0);
        } else if (this.e != null) {
            this.e.setVisibility(8);
            this.e.setImageDrawable(null);
        }
        boolean z = !TextUtils.isEmpty(b2);
        if (z) {
            if (this.d == null) {
                v vVar = new v(getContext(), null, b.actionBarTabTextStyle);
                vVar.setEllipsize(TextUtils.TruncateAt.END);
                p pVar2 = new p(-2, -2);
                pVar2.h = 16;
                vVar.setLayoutParams(pVar2);
                addView(vVar);
                this.d = vVar;
            }
            this.d.setText(b2);
            this.d.setVisibility(0);
        } else if (this.d != null) {
            this.d.setVisibility(8);
            this.d.setText((CharSequence) null);
        }
        if (this.e != null) {
            this.e.setContentDescription(dVar.e());
        }
        if (z || TextUtils.isEmpty(dVar.e())) {
            setOnLongClickListener(null);
            setLongClickable(false);
            return;
        }
        setOnLongClickListener(this);
    }

    public void a(d dVar) {
        this.c = dVar;
        a();
    }

    public d b() {
        return this.c;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(d.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        if (Build.VERSION.SDK_INT >= 14) {
            accessibilityNodeInfo.setClassName(d.class.getName());
        }
    }

    public boolean onLongClick(View view) {
        int[] iArr = new int[2];
        getLocationOnScreen(iArr);
        Context context = getContext();
        int width = getWidth();
        int height = getHeight();
        int i = context.getResources().getDisplayMetrics().widthPixels;
        Toast makeText = Toast.makeText(context, this.c.e(), 0);
        makeText.setGravity(49, (iArr[0] + (width / 2)) - (i / 2), height);
        makeText.show();
        return true;
    }

    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.f165a.b > 0 && getMeasuredWidth() > this.f165a.b) {
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(this.f165a.b, 1073741824), i2);
        }
    }

    public void setSelected(boolean z) {
        boolean z2 = isSelected() != z;
        super.setSelected(z);
        if (z2 && z) {
            sendAccessibilityEvent(4);
        }
    }
}
