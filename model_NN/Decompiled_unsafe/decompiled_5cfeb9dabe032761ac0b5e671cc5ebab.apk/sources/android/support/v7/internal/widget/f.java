package android.support.v7.internal.widget;

import android.graphics.Outline;

class f extends e {
    public f(ActionBarContainer actionBarContainer) {
        super(actionBarContainer);
    }

    public void getOutline(Outline outline) {
        if (this.f188a.d) {
            if (this.f188a.c != null) {
                this.f188a.c.getOutline(outline);
            }
        } else if (this.f188a.f150a != null) {
            this.f188a.f150a.getOutline(outline);
        }
    }
}
