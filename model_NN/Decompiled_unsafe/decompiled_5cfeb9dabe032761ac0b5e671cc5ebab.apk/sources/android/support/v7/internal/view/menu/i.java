package android.support.v7.internal.view.menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.c.a.a;
import android.support.v4.view.aa;
import android.support.v4.view.g;
import android.support.v7.a.c;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class i implements a {
    private static final int[] d = {1, 4, 5, 3, 2, 0};

    /* renamed from: a  reason: collision with root package name */
    CharSequence f138a;
    Drawable b;
    View c;
    private final Context e;
    private final Resources f;
    private boolean g;
    private boolean h;
    private j i;
    private ArrayList j;
    private ArrayList k;
    private boolean l;
    private ArrayList m;
    private ArrayList n;
    private boolean o;
    private int p = 0;
    private ContextMenu.ContextMenuInfo q;
    private boolean r = false;
    private boolean s = false;
    private boolean t = false;
    private boolean u = false;
    private ArrayList v = new ArrayList();
    private CopyOnWriteArrayList w = new CopyOnWriteArrayList();
    private m x;

    public i(Context context) {
        this.e = context;
        this.f = context.getResources();
        this.j = new ArrayList();
        this.k = new ArrayList();
        this.l = true;
        this.m = new ArrayList();
        this.n = new ArrayList();
        this.o = true;
        d(true);
    }

    private static int a(ArrayList arrayList, int i2) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (((m) arrayList.get(size)).c() <= i2) {
                return size + 1;
            }
        }
        return 0;
    }

    private m a(int i2, int i3, int i4, int i5, CharSequence charSequence, int i6) {
        return new m(this, i2, i3, i4, i5, charSequence, i6);
    }

    private MenuItem a(int i2, int i3, int i4, CharSequence charSequence) {
        int d2 = d(i4);
        m a2 = a(i2, i3, i4, d2, charSequence, this.p);
        if (this.q != null) {
            a2.a(this.q);
        }
        this.j.add(a(this.j, d2), a2);
        b(true);
        return a2;
    }

    private void a(int i2, CharSequence charSequence, int i3, Drawable drawable, View view) {
        Resources d2 = d();
        if (view != null) {
            this.c = view;
            this.f138a = null;
            this.b = null;
        } else {
            if (i2 > 0) {
                this.f138a = d2.getText(i2);
            } else if (charSequence != null) {
                this.f138a = charSequence;
            }
            if (i3 > 0) {
                this.b = android.support.v4.a.a.a(e(), i3);
            } else if (drawable != null) {
                this.b = drawable;
            }
            this.c = null;
        }
        b(false);
    }

    private void a(int i2, boolean z) {
        if (i2 >= 0 && i2 < this.j.size()) {
            this.j.remove(i2);
            if (z) {
                b(true);
            }
        }
    }

    private boolean a(ad adVar, x xVar) {
        boolean z = false;
        if (this.w.isEmpty()) {
            return false;
        }
        if (xVar != null) {
            z = xVar.a(adVar);
        }
        Iterator it = this.w.iterator();
        while (true) {
            boolean z2 = z;
            if (!it.hasNext()) {
                return z2;
            }
            WeakReference weakReference = (WeakReference) it.next();
            x xVar2 = (x) weakReference.get();
            if (xVar2 == null) {
                this.w.remove(weakReference);
            } else if (!z2) {
                z2 = xVar2.a(adVar);
            }
            z = z2;
        }
    }

    private void c(boolean z) {
        if (!this.w.isEmpty()) {
            g();
            Iterator it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                x xVar = (x) weakReference.get();
                if (xVar == null) {
                    this.w.remove(weakReference);
                } else {
                    xVar.a(z);
                }
            }
            h();
        }
    }

    private static int d(int i2) {
        int i3 = (-65536 & i2) >> 16;
        if (i3 >= 0 && i3 < d.length) {
            return (d[i3] << 16) | (65535 & i2);
        }
        throw new IllegalArgumentException("order does not contain a valid category.");
    }

    private void d(boolean z) {
        boolean z2 = true;
        if (!z || this.f.getConfiguration().keyboard == 1 || !this.f.getBoolean(c.abc_config_showMenuShortcutsWhenKeyboardPresent)) {
            z2 = false;
        }
        this.h = z2;
    }

    public int a(int i2, int i3) {
        int size = size();
        if (i3 < 0) {
            i3 = 0;
        }
        for (int i4 = i3; i4 < size; i4++) {
            if (((m) this.j.get(i4)).getGroupId() == i2) {
                return i4;
            }
        }
        return -1;
    }

    public i a(int i2) {
        this.p = i2;
        return this;
    }

    /* access modifiers changed from: protected */
    public i a(Drawable drawable) {
        a(0, null, 0, drawable, null);
        return this;
    }

    /* access modifiers changed from: protected */
    public i a(View view) {
        a(0, null, 0, null, view);
        return this;
    }

    /* access modifiers changed from: protected */
    public i a(CharSequence charSequence) {
        a(0, charSequence, 0, null, null);
        return this;
    }

    /* access modifiers changed from: package-private */
    public m a(int i2, KeyEvent keyEvent) {
        ArrayList arrayList = this.v;
        arrayList.clear();
        a(arrayList, i2, keyEvent);
        if (arrayList.isEmpty()) {
            return null;
        }
        int metaState = keyEvent.getMetaState();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        keyEvent.getKeyData(keyData);
        int size = arrayList.size();
        if (size == 1) {
            return (m) arrayList.get(0);
        }
        boolean b2 = b();
        for (int i3 = 0; i3 < size; i3++) {
            m mVar = (m) arrayList.get(i3);
            char alphabeticShortcut = b2 ? mVar.getAlphabeticShortcut() : mVar.getNumericShortcut();
            if (alphabeticShortcut == keyData.meta[0] && (metaState & 2) == 0) {
                return mVar;
            }
            if (alphabeticShortcut == keyData.meta[2] && (metaState & 2) != 0) {
                return mVar;
            }
            if (b2 && alphabeticShortcut == 8 && i2 == 67) {
                return mVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public String a() {
        return "android:menu:actionviewstates";
    }

    public void a(Bundle bundle) {
        int size = size();
        int i2 = 0;
        SparseArray sparseArray = null;
        while (i2 < size) {
            MenuItem item = getItem(i2);
            View a2 = aa.a(item);
            if (!(a2 == null || a2.getId() == -1)) {
                if (sparseArray == null) {
                    sparseArray = new SparseArray();
                }
                a2.saveHierarchyState(sparseArray);
                if (aa.c(item)) {
                    bundle.putInt("android:menu:expandedactionview", item.getItemId());
                }
            }
            SparseArray sparseArray2 = sparseArray;
            if (item.hasSubMenu()) {
                ((ad) item.getSubMenu()).a(bundle);
            }
            i2++;
            sparseArray = sparseArray2;
        }
        if (sparseArray != null) {
            bundle.putSparseParcelableArray(a(), sparseArray);
        }
    }

    public void a(j jVar) {
        this.i = jVar;
    }

    /* access modifiers changed from: package-private */
    public void a(m mVar) {
        this.l = true;
        b(true);
    }

    public void a(x xVar) {
        a(xVar, this.e);
    }

    public void a(x xVar, Context context) {
        this.w.add(new WeakReference(xVar));
        xVar.a(context, this);
        this.o = true;
    }

    /* access modifiers changed from: package-private */
    public void a(MenuItem menuItem) {
        int groupId = menuItem.getGroupId();
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            m mVar = (m) this.j.get(i2);
            if (mVar.getGroupId() == groupId && mVar.g() && mVar.isCheckable()) {
                mVar.b(mVar == menuItem);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(List list, int i2, KeyEvent keyEvent) {
        boolean b2 = b();
        int metaState = keyEvent.getMetaState();
        KeyCharacterMap.KeyData keyData = new KeyCharacterMap.KeyData();
        if (keyEvent.getKeyData(keyData) || i2 == 67) {
            int size = this.j.size();
            for (int i3 = 0; i3 < size; i3++) {
                m mVar = (m) this.j.get(i3);
                if (mVar.hasSubMenu()) {
                    ((i) mVar.getSubMenu()).a(list, i2, keyEvent);
                }
                char alphabeticShortcut = b2 ? mVar.getAlphabeticShortcut() : mVar.getNumericShortcut();
                if ((metaState & 5) == 0 && alphabeticShortcut != 0 && ((alphabeticShortcut == keyData.meta[0] || alphabeticShortcut == keyData.meta[2] || (b2 && alphabeticShortcut == 8 && i2 == 67)) && mVar.isEnabled())) {
                    list.add(mVar);
                }
            }
        }
    }

    public final void a(boolean z) {
        if (!this.u) {
            this.u = true;
            Iterator it = this.w.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                x xVar = (x) weakReference.get();
                if (xVar == null) {
                    this.w.remove(weakReference);
                } else {
                    xVar.a(this, z);
                }
            }
            this.u = false;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(i iVar, MenuItem menuItem) {
        return this.i != null && this.i.a(iVar, menuItem);
    }

    public boolean a(MenuItem menuItem, int i2) {
        return a(menuItem, (x) null, i2);
    }

    public boolean a(MenuItem menuItem, x xVar, int i2) {
        m mVar = (m) menuItem;
        if (mVar == null || !mVar.isEnabled()) {
            return false;
        }
        boolean b2 = mVar.b();
        g a2 = mVar.a();
        boolean z = a2 != null && a2.e();
        if (mVar.n()) {
            boolean expandActionView = mVar.expandActionView() | b2;
            if (!expandActionView) {
                return expandActionView;
            }
            a(true);
            return expandActionView;
        } else if (mVar.hasSubMenu() || z) {
            a(false);
            if (!mVar.hasSubMenu()) {
                mVar.a(new ad(e(), this, mVar));
            }
            ad adVar = (ad) mVar.getSubMenu();
            if (z) {
                a2.a(adVar);
            }
            boolean a3 = a(adVar, xVar) | b2;
            if (a3) {
                return a3;
            }
            a(true);
            return a3;
        } else {
            if ((i2 & 1) == 0) {
                a(true);
            }
            return b2;
        }
    }

    public MenuItem add(int i2) {
        return a(0, 0, 0, this.f.getString(i2));
    }

    public MenuItem add(int i2, int i3, int i4, int i5) {
        return a(i2, i3, i4, this.f.getString(i5));
    }

    public MenuItem add(int i2, int i3, int i4, CharSequence charSequence) {
        return a(i2, i3, i4, charSequence);
    }

    public MenuItem add(CharSequence charSequence) {
        return a(0, 0, 0, charSequence);
    }

    public int addIntentOptions(int i2, int i3, int i4, ComponentName componentName, Intent[] intentArr, Intent intent, int i5, MenuItem[] menuItemArr) {
        PackageManager packageManager = this.e.getPackageManager();
        List<ResolveInfo> queryIntentActivityOptions = packageManager.queryIntentActivityOptions(componentName, intentArr, intent, 0);
        int size = queryIntentActivityOptions != null ? queryIntentActivityOptions.size() : 0;
        if ((i5 & 1) == 0) {
            removeGroup(i2);
        }
        for (int i6 = 0; i6 < size; i6++) {
            ResolveInfo resolveInfo = queryIntentActivityOptions.get(i6);
            Intent intent2 = new Intent(resolveInfo.specificIndex < 0 ? intent : intentArr[resolveInfo.specificIndex]);
            intent2.setComponent(new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name));
            MenuItem intent3 = add(i2, i3, i4, resolveInfo.loadLabel(packageManager)).setIcon(resolveInfo.loadIcon(packageManager)).setIntent(intent2);
            if (menuItemArr != null && resolveInfo.specificIndex >= 0) {
                menuItemArr[resolveInfo.specificIndex] = intent3;
            }
        }
        return size;
    }

    public SubMenu addSubMenu(int i2) {
        return addSubMenu(0, 0, 0, this.f.getString(i2));
    }

    public SubMenu addSubMenu(int i2, int i3, int i4, int i5) {
        return addSubMenu(i2, i3, i4, this.f.getString(i5));
    }

    public SubMenu addSubMenu(int i2, int i3, int i4, CharSequence charSequence) {
        m mVar = (m) a(i2, i3, i4, charSequence);
        ad adVar = new ad(this.e, this, mVar);
        mVar.a(adVar);
        return adVar;
    }

    public SubMenu addSubMenu(CharSequence charSequence) {
        return addSubMenu(0, 0, 0, charSequence);
    }

    public int b(int i2) {
        int size = size();
        for (int i3 = 0; i3 < size; i3++) {
            if (((m) this.j.get(i3)).getItemId() == i2) {
                return i3;
            }
        }
        return -1;
    }

    public void b(Bundle bundle) {
        MenuItem findItem;
        if (bundle != null) {
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray(a());
            int size = size();
            for (int i2 = 0; i2 < size; i2++) {
                MenuItem item = getItem(i2);
                View a2 = aa.a(item);
                if (!(a2 == null || a2.getId() == -1)) {
                    a2.restoreHierarchyState(sparseParcelableArray);
                }
                if (item.hasSubMenu()) {
                    ((ad) item.getSubMenu()).b(bundle);
                }
            }
            int i3 = bundle.getInt("android:menu:expandedactionview");
            if (i3 > 0 && (findItem = findItem(i3)) != null) {
                aa.b(findItem);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(m mVar) {
        this.o = true;
        b(true);
    }

    public void b(x xVar) {
        Iterator it = this.w.iterator();
        while (it.hasNext()) {
            WeakReference weakReference = (WeakReference) it.next();
            x xVar2 = (x) weakReference.get();
            if (xVar2 == null || xVar2 == xVar) {
                this.w.remove(weakReference);
            }
        }
    }

    public void b(boolean z) {
        if (!this.r) {
            if (z) {
                this.l = true;
                this.o = true;
            }
            c(z);
            return;
        }
        this.s = true;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.g;
    }

    public int c(int i2) {
        return a(i2, 0);
    }

    public boolean c() {
        return this.h;
    }

    public boolean c(m mVar) {
        boolean z = false;
        if (!this.w.isEmpty()) {
            g();
            Iterator it = this.w.iterator();
            while (true) {
                boolean z2 = z;
                if (!it.hasNext()) {
                    z = z2;
                    break;
                }
                WeakReference weakReference = (WeakReference) it.next();
                x xVar = (x) weakReference.get();
                if (xVar == null) {
                    this.w.remove(weakReference);
                    z = z2;
                } else {
                    z = xVar.a(this, mVar);
                    if (z) {
                        break;
                    }
                }
            }
            h();
            if (z) {
                this.x = mVar;
            }
        }
        return z;
    }

    public void clear() {
        if (this.x != null) {
            d(this.x);
        }
        this.j.clear();
        b(true);
    }

    public void clearHeader() {
        this.b = null;
        this.f138a = null;
        this.c = null;
        b(false);
    }

    public void close() {
        a(true);
    }

    /* access modifiers changed from: package-private */
    public Resources d() {
        return this.f;
    }

    public boolean d(m mVar) {
        boolean z = false;
        if (!this.w.isEmpty() && this.x == mVar) {
            g();
            Iterator it = this.w.iterator();
            while (true) {
                boolean z2 = z;
                if (!it.hasNext()) {
                    z = z2;
                    break;
                }
                WeakReference weakReference = (WeakReference) it.next();
                x xVar = (x) weakReference.get();
                if (xVar == null) {
                    this.w.remove(weakReference);
                    z = z2;
                } else {
                    z = xVar.b(this, mVar);
                    if (z) {
                        break;
                    }
                }
            }
            h();
            if (z) {
                this.x = null;
            }
        }
        return z;
    }

    public Context e() {
        return this.e;
    }

    public void f() {
        if (this.i != null) {
            this.i.a(this);
        }
    }

    public MenuItem findItem(int i2) {
        MenuItem findItem;
        int size = size();
        for (int i3 = 0; i3 < size; i3++) {
            m mVar = (m) this.j.get(i3);
            if (mVar.getItemId() == i2) {
                return mVar;
            }
            if (mVar.hasSubMenu() && (findItem = mVar.getSubMenu().findItem(i2)) != null) {
                return findItem;
            }
        }
        return null;
    }

    public void g() {
        if (!this.r) {
            this.r = true;
            this.s = false;
        }
    }

    public MenuItem getItem(int i2) {
        return (MenuItem) this.j.get(i2);
    }

    public void h() {
        this.r = false;
        if (this.s) {
            this.s = false;
            b(true);
        }
    }

    public boolean hasVisibleItems() {
        int size = size();
        for (int i2 = 0; i2 < size; i2++) {
            if (((m) this.j.get(i2)).isVisible()) {
                return true;
            }
        }
        return false;
    }

    public ArrayList i() {
        if (!this.l) {
            return this.k;
        }
        this.k.clear();
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            m mVar = (m) this.j.get(i2);
            if (mVar.isVisible()) {
                this.k.add(mVar);
            }
        }
        this.l = false;
        this.o = true;
        return this.k;
    }

    public boolean isShortcutKey(int i2, KeyEvent keyEvent) {
        return a(i2, keyEvent) != null;
    }

    public void j() {
        boolean b2;
        ArrayList i2 = i();
        if (this.o) {
            Iterator it = this.w.iterator();
            boolean z = false;
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                x xVar = (x) weakReference.get();
                if (xVar == null) {
                    this.w.remove(weakReference);
                    b2 = z;
                } else {
                    b2 = xVar.b() | z;
                }
                z = b2;
            }
            if (z) {
                this.m.clear();
                this.n.clear();
                int size = i2.size();
                for (int i3 = 0; i3 < size; i3++) {
                    m mVar = (m) i2.get(i3);
                    if (mVar.j()) {
                        this.m.add(mVar);
                    } else {
                        this.n.add(mVar);
                    }
                }
            } else {
                this.m.clear();
                this.n.clear();
                this.n.addAll(i());
            }
            this.o = false;
        }
    }

    public ArrayList k() {
        j();
        return this.m;
    }

    public ArrayList l() {
        j();
        return this.n;
    }

    public CharSequence m() {
        return this.f138a;
    }

    public Drawable n() {
        return this.b;
    }

    public View o() {
        return this.c;
    }

    public i p() {
        return this;
    }

    public boolean performIdentifierAction(int i2, int i3) {
        return a(findItem(i2), i3);
    }

    public boolean performShortcut(int i2, KeyEvent keyEvent, int i3) {
        m a2 = a(i2, keyEvent);
        boolean z = false;
        if (a2 != null) {
            z = a(a2, i3);
        }
        if ((i3 & 2) != 0) {
            a(true);
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public boolean q() {
        return this.t;
    }

    public m r() {
        return this.x;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.view.menu.i.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.internal.view.menu.i.a(java.util.ArrayList, int):int
      android.support.v7.internal.view.menu.i.a(android.support.v7.internal.view.menu.ad, android.support.v7.internal.view.menu.x):boolean
      android.support.v7.internal.view.menu.i.a(int, int):int
      android.support.v7.internal.view.menu.i.a(int, android.view.KeyEvent):android.support.v7.internal.view.menu.m
      android.support.v7.internal.view.menu.i.a(android.support.v7.internal.view.menu.x, android.content.Context):void
      android.support.v7.internal.view.menu.i.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.internal.view.menu.i.a(android.view.MenuItem, int):boolean
      android.support.v7.internal.view.menu.i.a(int, boolean):void */
    public void removeGroup(int i2) {
        int c2 = c(i2);
        if (c2 >= 0) {
            int size = this.j.size() - c2;
            int i3 = 0;
            while (true) {
                int i4 = i3 + 1;
                if (i3 >= size || ((m) this.j.get(c2)).getGroupId() != i2) {
                    b(true);
                } else {
                    a(c2, false);
                    i3 = i4;
                }
            }
            b(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.view.menu.i.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.internal.view.menu.i.a(java.util.ArrayList, int):int
      android.support.v7.internal.view.menu.i.a(android.support.v7.internal.view.menu.ad, android.support.v7.internal.view.menu.x):boolean
      android.support.v7.internal.view.menu.i.a(int, int):int
      android.support.v7.internal.view.menu.i.a(int, android.view.KeyEvent):android.support.v7.internal.view.menu.m
      android.support.v7.internal.view.menu.i.a(android.support.v7.internal.view.menu.x, android.content.Context):void
      android.support.v7.internal.view.menu.i.a(android.support.v7.internal.view.menu.i, android.view.MenuItem):boolean
      android.support.v7.internal.view.menu.i.a(android.view.MenuItem, int):boolean
      android.support.v7.internal.view.menu.i.a(int, boolean):void */
    public void removeItem(int i2) {
        a(b(i2), true);
    }

    public void setGroupCheckable(int i2, boolean z, boolean z2) {
        int size = this.j.size();
        for (int i3 = 0; i3 < size; i3++) {
            m mVar = (m) this.j.get(i3);
            if (mVar.getGroupId() == i2) {
                mVar.a(z2);
                mVar.setCheckable(z);
            }
        }
    }

    public void setGroupEnabled(int i2, boolean z) {
        int size = this.j.size();
        for (int i3 = 0; i3 < size; i3++) {
            m mVar = (m) this.j.get(i3);
            if (mVar.getGroupId() == i2) {
                mVar.setEnabled(z);
            }
        }
    }

    public void setGroupVisible(int i2, boolean z) {
        int size = this.j.size();
        int i3 = 0;
        boolean z2 = false;
        while (i3 < size) {
            m mVar = (m) this.j.get(i3);
            i3++;
            z2 = (mVar.getGroupId() != i2 || !mVar.c(z)) ? z2 : true;
        }
        if (z2) {
            b(true);
        }
    }

    public void setQwertyMode(boolean z) {
        this.g = z;
        b(false);
    }

    public int size() {
        return this.j.size();
    }
}
