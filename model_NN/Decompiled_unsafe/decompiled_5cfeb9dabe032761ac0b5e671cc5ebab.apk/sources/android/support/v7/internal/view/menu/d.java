package android.support.v7.internal.view.menu;

import android.content.Context;
import android.support.v4.view.au;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public abstract class d implements x {

    /* renamed from: a  reason: collision with root package name */
    protected Context f134a;
    protected Context b;
    protected i c;
    protected LayoutInflater d;
    protected LayoutInflater e;
    protected z f;
    private y g;
    private int h;
    private int i;
    private int j;

    public d(Context context, int i2, int i3) {
        this.f134a = context;
        this.d = LayoutInflater.from(context);
        this.h = i2;
        this.i = i3;
    }

    public y a() {
        return this.g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public z a(ViewGroup viewGroup) {
        if (this.f == null) {
            this.f = (z) this.d.inflate(this.h, viewGroup, false);
            this.f.a(this.c);
            a(true);
        }
        return this.f;
    }

    public View a(m mVar, View view, ViewGroup viewGroup) {
        aa b2 = view instanceof aa ? (aa) view : b(viewGroup);
        a(mVar, b2);
        return (View) b2;
    }

    public void a(int i2) {
        this.j = i2;
    }

    public void a(Context context, i iVar) {
        this.b = context;
        this.e = LayoutInflater.from(this.b);
        this.c = iVar;
    }

    public void a(i iVar, boolean z) {
        if (this.g != null) {
            this.g.a(iVar, z);
        }
    }

    public abstract void a(m mVar, aa aaVar);

    public void a(y yVar) {
        this.g = yVar;
    }

    /* access modifiers changed from: protected */
    public void a(View view, int i2) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.f).addView(view, i2);
    }

    public void a(boolean z) {
        int i2;
        int i3;
        ViewGroup viewGroup = (ViewGroup) this.f;
        if (viewGroup != null) {
            if (this.c != null) {
                this.c.j();
                ArrayList i4 = this.c.i();
                int size = i4.size();
                int i5 = 0;
                i2 = 0;
                while (i5 < size) {
                    m mVar = (m) i4.get(i5);
                    if (a(i2, mVar)) {
                        View childAt = viewGroup.getChildAt(i2);
                        m itemData = childAt instanceof aa ? ((aa) childAt).getItemData() : null;
                        View a2 = a(mVar, childAt, viewGroup);
                        if (mVar != itemData) {
                            a2.setPressed(false);
                            au.m(a2);
                        }
                        if (a2 != childAt) {
                            a(a2, i2);
                        }
                        i3 = i2 + 1;
                    } else {
                        i3 = i2;
                    }
                    i5++;
                    i2 = i3;
                }
            } else {
                i2 = 0;
            }
            while (i2 < viewGroup.getChildCount()) {
                if (!a(viewGroup, i2)) {
                    i2++;
                }
            }
        }
    }

    public boolean a(int i2, m mVar) {
        return true;
    }

    public boolean a(ad adVar) {
        if (this.g != null) {
            return this.g.a(adVar);
        }
        return false;
    }

    public boolean a(i iVar, m mVar) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean a(ViewGroup viewGroup, int i2) {
        viewGroup.removeViewAt(i2);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public aa b(ViewGroup viewGroup) {
        return (aa) this.d.inflate(this.i, viewGroup, false);
    }

    public boolean b() {
        return false;
    }

    public boolean b(i iVar, m mVar) {
        return false;
    }
}
