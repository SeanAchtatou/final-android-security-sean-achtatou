package android.support.v7.internal.widget;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

class ac extends y {

    /* renamed from: a  reason: collision with root package name */
    private boolean f159a = true;

    public ac(Drawable drawable) {
        super(drawable);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.f159a = z;
    }

    public void draw(Canvas canvas) {
        if (this.f159a) {
            super.draw(canvas);
        }
    }

    public void setHotspot(float f, float f2) {
        if (this.f159a) {
            super.setHotspot(f, f2);
        }
    }

    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        if (this.f159a) {
            super.setHotspotBounds(i, i2, i3, i4);
        }
    }

    public boolean setState(int[] iArr) {
        if (this.f159a) {
            return super.setState(iArr);
        }
        return false;
    }

    public boolean setVisible(boolean z, boolean z2) {
        if (this.f159a) {
            return super.setVisible(z, z2);
        }
        return false;
    }
}
