package android.support.v7.internal.widget;

import android.content.res.Configuration;
import android.os.Build;
import android.support.v7.a.b;
import android.support.v7.app.d;
import android.support.v7.internal.view.a;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.p;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AbsListView;
import android.widget.HorizontalScrollView;

public class af extends HorizontalScrollView implements q {
    private static final Interpolator j = new DecelerateInterpolator();

    /* renamed from: a  reason: collision with root package name */
    Runnable f161a;
    int b;
    int c;
    private ai d;
    /* access modifiers changed from: private */
    public LinearLayoutCompat e;
    private SpinnerCompat f;
    private boolean g;
    private int h;
    private int i;

    /* access modifiers changed from: private */
    public aj a(d dVar, boolean z) {
        aj ajVar = new aj(this, getContext(), dVar, z);
        if (z) {
            ajVar.setBackgroundDrawable(null);
            ajVar.setLayoutParams(new AbsListView.LayoutParams(-1, this.h));
        } else {
            ajVar.setFocusable(true);
            if (this.d == null) {
                this.d = new ai(this, null);
            }
            ajVar.setOnClickListener(this.d);
        }
        return ajVar;
    }

    private boolean a() {
        return this.f != null && this.f.getParent() == this;
    }

    private void b() {
        if (!a()) {
            if (this.f == null) {
                this.f = d();
            }
            removeView(this.e);
            addView(this.f, new ViewGroup.LayoutParams(-2, -1));
            if (this.f.getAdapter() == null) {
                this.f.setAdapter(new ah(this, null));
            }
            if (this.f161a != null) {
                removeCallbacks(this.f161a);
                this.f161a = null;
            }
            this.f.setSelection(this.i);
        }
    }

    private boolean c() {
        if (a()) {
            removeView(this.f);
            addView(this.e, new ViewGroup.LayoutParams(-2, -1));
            setTabSelected(this.f.getSelectedItemPosition());
        }
        return false;
    }

    private SpinnerCompat d() {
        SpinnerCompat spinnerCompat = new SpinnerCompat(getContext(), null, b.actionDropDownStyle);
        spinnerCompat.setLayoutParams(new p(-2, -1));
        spinnerCompat.a((q) this);
        return spinnerCompat;
    }

    public void a(int i2) {
        View childAt = this.e.getChildAt(i2);
        if (this.f161a != null) {
            removeCallbacks(this.f161a);
        }
        this.f161a = new ag(this, childAt);
        post(this.f161a);
    }

    public void a(n nVar, View view, int i2, long j2) {
        ((aj) view).b().d();
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f161a != null) {
            post(this.f161a);
        }
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration);
        }
        a a2 = a.a(getContext());
        setContentHeight(a2.e());
        this.c = a2.g();
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f161a != null) {
            removeCallbacks(this.f161a);
        }
    }

    public void onMeasure(int i2, int i3) {
        boolean z = true;
        int mode = View.MeasureSpec.getMode(i2);
        boolean z2 = mode == 1073741824;
        setFillViewport(z2);
        int childCount = this.e.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.b = -1;
        } else {
            if (childCount > 2) {
                this.b = (int) (((float) View.MeasureSpec.getSize(i2)) * 0.4f);
            } else {
                this.b = View.MeasureSpec.getSize(i2) / 2;
            }
            this.b = Math.min(this.b, this.c);
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.h, 1073741824);
        if (z2 || !this.g) {
            z = false;
        }
        if (z) {
            this.e.measure(0, makeMeasureSpec);
            if (this.e.getMeasuredWidth() > View.MeasureSpec.getSize(i2)) {
                b();
            } else {
                c();
            }
        } else {
            c();
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i2, makeMeasureSpec);
        int measuredWidth2 = getMeasuredWidth();
        if (z2 && measuredWidth != measuredWidth2) {
            setTabSelected(this.i);
        }
    }

    public void setAllowCollapse(boolean z) {
        this.g = z;
    }

    public void setContentHeight(int i2) {
        this.h = i2;
        requestLayout();
    }

    public void setTabSelected(int i2) {
        this.i = i2;
        int childCount = this.e.getChildCount();
        int i3 = 0;
        while (i3 < childCount) {
            View childAt = this.e.getChildAt(i3);
            boolean z = i3 == i2;
            childAt.setSelected(z);
            if (z) {
                a(i2);
            }
            i3++;
        }
        if (this.f != null && i2 >= 0) {
            this.f.setSelection(i2);
        }
    }
}
