package android.support.v7.internal.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.View;

@TargetApi(11)
public class NativeActionModeAwareLayout extends ContentFrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private ad f154a;

    public NativeActionModeAwareLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setActionModeForChildListener(ad adVar) {
        this.f154a = adVar;
    }

    public ActionMode startActionModeForChild(View view, ActionMode.Callback callback) {
        return this.f154a != null ? this.f154a.a(view, callback) : super.startActionModeForChild(view, callback);
    }
}
