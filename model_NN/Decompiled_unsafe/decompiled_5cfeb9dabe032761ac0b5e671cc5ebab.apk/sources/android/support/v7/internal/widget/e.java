package android.support.v7.internal.widget;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

class e extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    final ActionBarContainer f188a;

    public e(ActionBarContainer actionBarContainer) {
        this.f188a = actionBarContainer;
    }

    public void draw(Canvas canvas) {
        if (!this.f188a.d) {
            if (this.f188a.f150a != null) {
                this.f188a.f150a.draw(canvas);
            }
            if (this.f188a.b != null && this.f188a.e) {
                this.f188a.b.draw(canvas);
            }
        } else if (this.f188a.c != null) {
            this.f188a.c.draw(canvas);
        }
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
