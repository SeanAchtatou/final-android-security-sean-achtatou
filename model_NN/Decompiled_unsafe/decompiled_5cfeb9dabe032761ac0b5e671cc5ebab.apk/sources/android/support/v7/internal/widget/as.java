package android.support.v7.internal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

public class as extends CheckBox {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f172a = {16843015};
    private final aw b;

    public as(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842860);
    }

    public as(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        bb a2 = bb.a(context, attributeSet, f172a, i, 0);
        setButtonDrawable(a2.a(0));
        a2.b();
        this.b = a2.c();
    }

    public void setButtonDrawable(int i) {
        setButtonDrawable(this.b.a(i));
    }
}
