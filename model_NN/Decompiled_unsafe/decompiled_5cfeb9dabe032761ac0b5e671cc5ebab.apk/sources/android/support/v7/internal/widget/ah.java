package android.support.v7.internal.widget;

import android.support.v7.app.d;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

class ah extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ af f163a;

    private ah(af afVar) {
        this.f163a = afVar;
    }

    /* synthetic */ ah(af afVar, ag agVar) {
        this(afVar);
    }

    public int getCount() {
        return this.f163a.e.getChildCount();
    }

    public Object getItem(int i) {
        return ((aj) this.f163a.e.getChildAt(i)).b();
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            return this.f163a.a((d) getItem(i), true);
        }
        ((aj) view).a((d) getItem(i));
        return view;
    }
}
