package android.support.v7.internal.view;

import android.content.Context;
import android.support.v4.c.a.a;
import android.support.v4.f.m;
import android.support.v7.b.b;
import android.support.v7.internal.view.menu.ab;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

public class d implements b {

    /* renamed from: a  reason: collision with root package name */
    final ActionMode.Callback f123a;
    final Context b;
    final m c = new m();
    final m d = new m();

    public d(Context context, ActionMode.Callback callback) {
        this.b = context;
        this.f123a = callback;
    }

    private Menu a(Menu menu) {
        Menu menu2 = (Menu) this.d.get(menu);
        if (menu2 != null) {
            return menu2;
        }
        Menu a2 = ab.a(this.b, (a) menu);
        this.d.put(menu, a2);
        return a2;
    }

    private ActionMode b(android.support.v7.b.a aVar) {
        c cVar = (c) this.c.get(aVar);
        if (cVar != null) {
            return cVar;
        }
        c cVar2 = new c(this.b, aVar);
        this.c.put(aVar, cVar2);
        return cVar2;
    }

    public void a(android.support.v7.b.a aVar) {
        this.f123a.onDestroyActionMode(b(aVar));
    }

    public boolean a(android.support.v7.b.a aVar, Menu menu) {
        return this.f123a.onCreateActionMode(b(aVar), a(menu));
    }

    public boolean a(android.support.v7.b.a aVar, MenuItem menuItem) {
        return this.f123a.onActionItemClicked(b(aVar), ab.a(this.b, (android.support.v4.c.a.b) menuItem));
    }

    public boolean b(android.support.v7.b.a aVar, Menu menu) {
        return this.f123a.onPrepareActionMode(b(aVar), a(menu));
    }
}
