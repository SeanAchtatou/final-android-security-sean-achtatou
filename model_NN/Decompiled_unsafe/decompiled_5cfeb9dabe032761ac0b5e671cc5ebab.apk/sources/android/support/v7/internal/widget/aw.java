package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v4.a.a;
import android.support.v7.a.b;
import android.support.v7.a.f;
import android.util.TypedValue;

public class aw {

    /* renamed from: a  reason: collision with root package name */
    static final PorterDuff.Mode f176a = PorterDuff.Mode.SRC_IN;
    private static final String b = aw.class.getSimpleName();
    private static final ax c = new ax(6);
    private static final int[] d = {f.abc_ic_ab_back_mtrl_am_alpha, f.abc_ic_go_search_api_mtrl_alpha, f.abc_ic_search_api_mtrl_alpha, f.abc_ic_commit_search_api_mtrl_alpha, f.abc_ic_clear_mtrl_alpha, f.abc_ic_menu_share_mtrl_alpha, f.abc_ic_menu_copy_mtrl_am_alpha, f.abc_ic_menu_cut_mtrl_alpha, f.abc_ic_menu_selectall_mtrl_alpha, f.abc_ic_menu_paste_mtrl_am_alpha, f.abc_ic_menu_moreoverflow_mtrl_alpha, f.abc_ic_voice_search_api_mtrl_alpha, f.abc_textfield_search_default_mtrl_alpha, f.abc_textfield_default_mtrl_alpha};
    private static final int[] e = {f.abc_textfield_activated_mtrl_alpha, f.abc_textfield_search_activated_mtrl_alpha, f.abc_cab_background_top_mtrl_alpha};
    private static final int[] f = {f.abc_popup_background_mtrl_mult, f.abc_cab_background_internal_bg, f.abc_menu_hardkey_panel_mtrl_mult};
    private static final int[] g = {f.abc_edit_text_material, f.abc_tab_indicator_material, f.abc_textfield_search_material, f.abc_spinner_mtrl_am_alpha, f.abc_btn_check_material, f.abc_btn_radio_material};
    private static final int[] h = {f.abc_cab_background_top_material};
    private final Context i;
    private final Resources j;
    private final TypedValue k = new TypedValue();
    private ColorStateList l;
    private ColorStateList m;
    private ColorStateList n;

    public aw(Context context) {
        this.i = context;
        this.j = new az(context.getResources(), this);
    }

    private ColorStateList a() {
        if (this.l == null) {
            int b2 = b(b.colorControlNormal);
            int b3 = b(b.colorControlActivated);
            this.l = new ColorStateList(new int[][]{new int[]{-16842910}, new int[]{16842908}, new int[]{16843518}, new int[]{16842919}, new int[]{16842912}, new int[]{16842913}, new int[0]}, new int[]{c(b.colorControlNormal), b3, b3, b3, b3, b3, b2});
        }
        return this.l;
    }

    public static Drawable a(Context context, int i2) {
        return d(i2) ? new aw(context).a(i2) : a.a(context, i2);
    }

    private static boolean a(int[] iArr, int i2) {
        for (int i3 : iArr) {
            if (i3 == i2) {
                return true;
            }
        }
        return false;
    }

    private ColorStateList b() {
        if (this.n == null) {
            this.n = new ColorStateList(new int[][]{new int[]{-16842910}, new int[]{16842912}, new int[0]}, new int[]{a(16842800, 0.1f), a(b.colorControlActivated, 0.3f), a(16842800, 0.3f)});
        }
        return this.n;
    }

    private ColorStateList c() {
        if (this.m == null) {
            this.m = new ColorStateList(new int[][]{new int[]{-16842910}, new int[]{16842912}, new int[0]}, new int[]{c(b.colorSwitchThumbNormal), b(b.colorControlActivated), b(b.colorSwitchThumbNormal)});
        }
        return this.m;
    }

    private static boolean d(int i2) {
        return a(f, i2) || a(d, i2) || a(e, i2) || a(g, i2) || a(h, i2);
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, float f2) {
        int b2 = b(i2);
        return (b2 & 16777215) | (Math.round(((float) Color.alpha(b2)) * f2) << 24);
    }

    public Drawable a(int i2) {
        Drawable a2 = a.a(this.i, i2);
        if (a2 != null) {
            if (a(g, i2)) {
                return new au(a2, a());
            }
            if (i2 == f.abc_switch_track_mtrl_alpha) {
                return new au(a2, b());
            }
            if (i2 == f.abc_switch_thumb_material) {
                return new au(a2, c(), PorterDuff.Mode.MULTIPLY);
            }
            if (a(h, i2)) {
                return this.j.getDrawable(i2);
            }
            a(i2, a2);
        }
        return a2;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, Drawable drawable) {
        int i3;
        int i4;
        PorterDuff.Mode mode;
        boolean z;
        PorterDuffColorFilter porterDuffColorFilter;
        if (a(d, i2)) {
            i4 = b.colorControlNormal;
            mode = null;
            z = true;
            i3 = -1;
        } else if (a(e, i2)) {
            i4 = b.colorControlActivated;
            mode = null;
            z = true;
            i3 = -1;
        } else if (a(f, i2)) {
            z = true;
            i3 = -1;
            mode = PorterDuff.Mode.MULTIPLY;
            i4 = 16842801;
        } else if (i2 == f.abc_list_divider_mtrl_alpha) {
            i4 = 16842800;
            mode = null;
            z = true;
            i3 = Math.round(40.8f);
        } else {
            i3 = -1;
            i4 = 0;
            mode = null;
            z = false;
        }
        if (z) {
            if (mode == null) {
                mode = f176a;
            }
            int b2 = b(i4);
            PorterDuffColorFilter a2 = c.a(b2, mode);
            if (a2 == null) {
                PorterDuffColorFilter porterDuffColorFilter2 = new PorterDuffColorFilter(b2, mode);
                c.a(b2, mode, porterDuffColorFilter2);
                porterDuffColorFilter = porterDuffColorFilter2;
            } else {
                porterDuffColorFilter = a2;
            }
            drawable.setColorFilter(porterDuffColorFilter);
            if (i3 != -1) {
                drawable.setAlpha(i3);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int b(int i2) {
        if (this.i.getTheme().resolveAttribute(i2, this.k, true)) {
            if (this.k.type >= 16 && this.k.type <= 31) {
                return this.k.data;
            }
            if (this.k.type == 3) {
                return this.j.getColor(this.k.resourceId);
            }
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int c(int i2) {
        this.i.getTheme().resolveAttribute(16842803, this.k, true);
        return a(i2, this.k.getFloat());
    }
}
