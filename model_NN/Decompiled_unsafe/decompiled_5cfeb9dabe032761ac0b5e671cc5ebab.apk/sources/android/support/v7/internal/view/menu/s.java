package android.support.v7.internal.view.menu;

import android.view.MenuItem;

class s extends f implements MenuItem.OnMenuItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ o f145a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    s(o oVar, MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        super(onMenuItemClickListener);
        this.f145a = oVar;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        return ((MenuItem.OnMenuItemClickListener) this.b).onMenuItemClick(this.f145a.a(menuItem));
    }
}
