package android.support.v7.internal.widget;

import android.database.DataSetObserver;
import android.os.Parcelable;

class p extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ n f195a;
    private Parcelable b = null;

    p(n nVar) {
        this.f195a = nVar;
    }

    public void onChanged() {
        this.f195a.u = true;
        this.f195a.A = this.f195a.z;
        this.f195a.z = this.f195a.getAdapter().getCount();
        if (!this.f195a.getAdapter().hasStableIds() || this.b == null || this.f195a.A != 0 || this.f195a.z <= 0) {
            this.f195a.j();
        } else {
            this.f195a.onRestoreInstanceState(this.b);
            this.b = null;
        }
        this.f195a.e();
        this.f195a.requestLayout();
    }

    public void onInvalidated() {
        this.f195a.u = true;
        if (this.f195a.getAdapter().hasStableIds()) {
            this.b = this.f195a.onSaveInstanceState();
        }
        this.f195a.A = this.f195a.z;
        this.f195a.z = 0;
        this.f195a.x = -1;
        this.f195a.y = Long.MIN_VALUE;
        this.f195a.v = -1;
        this.f195a.w = Long.MIN_VALUE;
        this.f195a.o = false;
        this.f195a.e();
        this.f195a.requestLayout();
    }
}
