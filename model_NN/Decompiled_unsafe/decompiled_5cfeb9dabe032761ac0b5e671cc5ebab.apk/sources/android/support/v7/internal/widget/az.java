package android.support.v7.internal.widget;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

class az extends Resources {

    /* renamed from: a  reason: collision with root package name */
    private final aw f178a;

    public az(Resources resources, aw awVar) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.f178a = awVar;
    }

    public Drawable getDrawable(int i) {
        Drawable drawable = super.getDrawable(i);
        if (drawable != null) {
            this.f178a.a(i, drawable);
        }
        return drawable;
    }
}
