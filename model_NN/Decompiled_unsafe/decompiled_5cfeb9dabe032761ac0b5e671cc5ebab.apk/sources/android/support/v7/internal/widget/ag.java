package android.support.v7.internal.widget;

import android.view.View;

class ag implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f162a;
    final /* synthetic */ af b;

    ag(af afVar, View view) {
        this.b = afVar;
        this.f162a = view;
    }

    public void run() {
        this.b.smoothScrollTo(this.f162a.getLeft() - ((this.b.getWidth() - this.f162a.getWidth()) / 2), 0);
        this.b.f161a = null;
    }
}
