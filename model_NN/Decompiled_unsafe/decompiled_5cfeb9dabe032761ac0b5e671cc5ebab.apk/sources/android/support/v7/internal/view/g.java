package android.support.v7.internal.view;

import android.content.res.TypedArray;
import android.support.v4.view.aa;
import android.support.v7.a.l;
import android.support.v7.internal.view.menu.m;
import android.support.v7.internal.view.menu.o;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

class g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f126a;
    private Menu b;
    private int c;
    private int d;
    private int e;
    private int f;
    private boolean g;
    private boolean h;
    private boolean i;
    private int j;
    private int k;
    private CharSequence l;
    private CharSequence m;
    private int n;
    private char o;
    private char p;
    private int q;
    private boolean r;
    private boolean s;
    private boolean t;
    private int u;
    private int v;
    private String w;
    private String x;
    private String y;
    /* access modifiers changed from: private */
    public android.support.v4.view.g z;

    public g(e eVar, Menu menu) {
        this.f126a = eVar;
        this.b = menu;
        a();
    }

    private char a(String str) {
        if (str == null) {
            return 0;
        }
        return str.charAt(0);
    }

    private Object a(String str, Class[] clsArr, Object[] objArr) {
        try {
            return this.f126a.e.getClassLoader().loadClass(str).getConstructor(clsArr).newInstance(objArr);
        } catch (Exception e2) {
            Log.w("SupportMenuInflater", "Cannot instantiate class: " + str, e2);
            return null;
        }
    }

    private void a(MenuItem menuItem) {
        boolean z2 = true;
        menuItem.setChecked(this.r).setVisible(this.s).setEnabled(this.t).setCheckable(this.q >= 1).setTitleCondensed(this.m).setIcon(this.n).setAlphabeticShortcut(this.o).setNumericShortcut(this.p);
        if (this.u >= 0) {
            aa.a(menuItem, this.u);
        }
        if (this.y != null) {
            if (this.f126a.e.isRestricted()) {
                throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
            }
            menuItem.setOnMenuItemClickListener(new f(this.f126a.c(), this.y));
        }
        if (menuItem instanceof m) {
            m mVar = (m) menuItem;
        }
        if (this.q >= 2) {
            if (menuItem instanceof m) {
                ((m) menuItem).a(true);
            } else if (menuItem instanceof o) {
                ((o) menuItem).a(true);
            }
        }
        if (this.w != null) {
            aa.a(menuItem, (View) a(this.w, e.f124a, this.f126a.c));
        } else {
            z2 = false;
        }
        if (this.v > 0) {
            if (!z2) {
                aa.b(menuItem, this.v);
            } else {
                Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
            }
        }
        if (this.z != null) {
            aa.a(menuItem, this.z);
        }
    }

    public void a() {
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.f = 0;
        this.g = true;
        this.h = true;
    }

    public void a(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = this.f126a.e.obtainStyledAttributes(attributeSet, l.MenuGroup);
        this.c = obtainStyledAttributes.getResourceId(l.MenuGroup_android_id, 0);
        this.d = obtainStyledAttributes.getInt(l.MenuGroup_android_menuCategory, 0);
        this.e = obtainStyledAttributes.getInt(l.MenuGroup_android_orderInCategory, 0);
        this.f = obtainStyledAttributes.getInt(l.MenuGroup_android_checkableBehavior, 0);
        this.g = obtainStyledAttributes.getBoolean(l.MenuGroup_android_visible, true);
        this.h = obtainStyledAttributes.getBoolean(l.MenuGroup_android_enabled, true);
        obtainStyledAttributes.recycle();
    }

    public void b() {
        this.i = true;
        a(this.b.add(this.c, this.j, this.k, this.l));
    }

    public void b(AttributeSet attributeSet) {
        boolean z2 = true;
        TypedArray obtainStyledAttributes = this.f126a.e.obtainStyledAttributes(attributeSet, l.MenuItem);
        this.j = obtainStyledAttributes.getResourceId(l.MenuItem_android_id, 0);
        this.k = (obtainStyledAttributes.getInt(l.MenuItem_android_menuCategory, this.d) & -65536) | (obtainStyledAttributes.getInt(l.MenuItem_android_orderInCategory, this.e) & 65535);
        this.l = obtainStyledAttributes.getText(l.MenuItem_android_title);
        this.m = obtainStyledAttributes.getText(l.MenuItem_android_titleCondensed);
        this.n = obtainStyledAttributes.getResourceId(l.MenuItem_android_icon, 0);
        this.o = a(obtainStyledAttributes.getString(l.MenuItem_android_alphabeticShortcut));
        this.p = a(obtainStyledAttributes.getString(l.MenuItem_android_numericShortcut));
        if (obtainStyledAttributes.hasValue(l.MenuItem_android_checkable)) {
            this.q = obtainStyledAttributes.getBoolean(l.MenuItem_android_checkable, false) ? 1 : 0;
        } else {
            this.q = this.f;
        }
        this.r = obtainStyledAttributes.getBoolean(l.MenuItem_android_checked, false);
        this.s = obtainStyledAttributes.getBoolean(l.MenuItem_android_visible, this.g);
        this.t = obtainStyledAttributes.getBoolean(l.MenuItem_android_enabled, this.h);
        this.u = obtainStyledAttributes.getInt(l.MenuItem_showAsAction, -1);
        this.y = obtainStyledAttributes.getString(l.MenuItem_android_onClick);
        this.v = obtainStyledAttributes.getResourceId(l.MenuItem_actionLayout, 0);
        this.w = obtainStyledAttributes.getString(l.MenuItem_actionViewClass);
        this.x = obtainStyledAttributes.getString(l.MenuItem_actionProviderClass);
        if (this.x == null) {
            z2 = false;
        }
        if (z2 && this.v == 0 && this.w == null) {
            this.z = (android.support.v4.view.g) a(this.x, e.b, this.f126a.d);
        } else {
            if (z2) {
                Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
            }
            this.z = null;
        }
        obtainStyledAttributes.recycle();
        this.i = false;
    }

    public SubMenu c() {
        this.i = true;
        SubMenu addSubMenu = this.b.addSubMenu(this.c, this.j, this.k, this.l);
        a(addSubMenu.getItem());
        return addSubMenu;
    }

    public boolean d() {
        return this.i;
    }
}
