package android.support.v7.internal.widget;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.support.v4.b.a.a;

class y extends Drawable implements Drawable.Callback {

    /* renamed from: a  reason: collision with root package name */
    private final Drawable f198a;

    public y(Drawable drawable) {
        this.f198a = drawable;
        this.f198a.setCallback(this);
    }

    public void draw(Canvas canvas) {
        this.f198a.draw(canvas);
    }

    public int getChangingConfigurations() {
        return this.f198a.getChangingConfigurations();
    }

    public Drawable getCurrent() {
        return this.f198a.getCurrent();
    }

    public int getIntrinsicHeight() {
        return this.f198a.getIntrinsicHeight();
    }

    public int getIntrinsicWidth() {
        return this.f198a.getIntrinsicWidth();
    }

    public int getMinimumHeight() {
        return this.f198a.getMinimumHeight();
    }

    public int getMinimumWidth() {
        return this.f198a.getMinimumWidth();
    }

    public int getOpacity() {
        return this.f198a.getOpacity();
    }

    public boolean getPadding(Rect rect) {
        return this.f198a.getPadding(rect);
    }

    public int[] getState() {
        return this.f198a.getState();
    }

    public Region getTransparentRegion() {
        return this.f198a.getTransparentRegion();
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public boolean isAutoMirrored() {
        return a.b(this.f198a);
    }

    public boolean isStateful() {
        return this.f198a.isStateful();
    }

    public void jumpToCurrentState() {
        a.a(this.f198a);
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        return this.f198a.setLevel(i);
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    public void setAlpha(int i) {
        this.f198a.setAlpha(i);
    }

    public void setAutoMirrored(boolean z) {
        a.a(this.f198a, z);
    }

    public void setBounds(int i, int i2, int i3, int i4) {
        super.setBounds(i, i2, i3, i4);
        this.f198a.setBounds(i, i2, i3, i4);
    }

    public void setChangingConfigurations(int i) {
        this.f198a.setChangingConfigurations(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f198a.setColorFilter(colorFilter);
    }

    public void setDither(boolean z) {
        this.f198a.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.f198a.setFilterBitmap(z);
    }

    public void setHotspot(float f, float f2) {
        a.a(this.f198a, f, f2);
    }

    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        a.a(this.f198a, i, i2, i3, i4);
    }

    public boolean setState(int[] iArr) {
        return this.f198a.setState(iArr);
    }

    public void setTint(int i) {
        a.a(this.f198a, i);
    }

    public void setTintList(ColorStateList colorStateList) {
        a.a(this.f198a, colorStateList);
    }

    public void setTintMode(PorterDuff.Mode mode) {
        a.a(this.f198a, mode);
    }

    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.f198a.setVisible(z, z2);
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }
}
