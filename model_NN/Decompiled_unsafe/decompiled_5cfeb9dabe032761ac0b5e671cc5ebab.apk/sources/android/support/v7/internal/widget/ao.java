package android.support.v7.internal.widget;

import android.content.Context;
import android.support.v7.widget.q;
import android.util.AttributeSet;
import android.widget.ListAdapter;

class ao extends q implements ar {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpinnerCompat f170a;
    private CharSequence c;
    /* access modifiers changed from: private */
    public ListAdapter d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ao(SpinnerCompat spinnerCompat, Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f170a = spinnerCompat;
        a(spinnerCompat);
        a(true);
        a(0);
        a(new ap(this, spinnerCompat));
    }

    public void a(ListAdapter listAdapter) {
        super.a(listAdapter);
        this.d = listAdapter;
    }

    public void a(CharSequence charSequence) {
        this.c = charSequence;
    }
}
