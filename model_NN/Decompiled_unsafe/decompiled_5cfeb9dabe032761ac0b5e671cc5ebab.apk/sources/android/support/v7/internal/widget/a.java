package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.view.au;
import android.support.v4.view.cf;
import android.support.v7.a.b;
import android.support.v7.a.l;
import android.support.v7.internal.view.h;
import android.support.v7.widget.ActionMenuPresenter;
import android.support.v7.widget.ActionMenuView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

abstract class a extends ViewGroup {
    private static final Interpolator j = new DecelerateInterpolator();

    /* renamed from: a  reason: collision with root package name */
    protected final b f157a;
    protected final Context b;
    protected ActionMenuView c;
    protected ActionMenuPresenter d;
    protected ViewGroup e;
    protected boolean f;
    protected boolean g;
    protected int h;
    protected cf i;

    a(Context context) {
        this(context, null);
    }

    a(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    a(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f157a = new b(this);
        TypedValue typedValue = new TypedValue();
        if (!context.getTheme().resolveAttribute(b.actionBarPopupTheme, typedValue, true) || typedValue.resourceId == 0) {
            this.b = context;
        } else {
            this.b = new ContextThemeWrapper(context, typedValue.resourceId);
        }
    }

    protected static int a(int i2, int i3, boolean z) {
        return z ? i2 - i3 : i2 + i3;
    }

    /* access modifiers changed from: protected */
    public int a(View view, int i2, int i3, int i4) {
        view.measure(View.MeasureSpec.makeMeasureSpec(i2, Integer.MIN_VALUE), i3);
        return Math.max(0, (i2 - view.getMeasuredWidth()) - i4);
    }

    /* access modifiers changed from: protected */
    public int a(View view, int i2, int i3, int i4, boolean z) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i5 = ((i4 - measuredHeight) / 2) + i3;
        if (z) {
            view.layout(i2 - measuredWidth, i5, i2, measuredHeight + i5);
        } else {
            view.layout(i2, i5, i2 + measuredWidth, measuredHeight + i5);
        }
        return z ? -measuredWidth : measuredWidth;
    }

    public void a(int i2) {
        if (this.i != null) {
            this.i.a();
        }
        if (i2 == 0) {
            if (getVisibility() != 0) {
                au.c(this, 0.0f);
                if (!(this.e == null || this.c == null)) {
                    au.c(this.c, 0.0f);
                }
            }
            cf a2 = au.i(this).a(1.0f);
            a2.a(200L);
            a2.a(j);
            if (this.e == null || this.c == null) {
                a2.a(this.f157a.a(a2, i2));
                a2.b();
                return;
            }
            h hVar = new h();
            cf a3 = au.i(this.c).a(1.0f);
            a3.a(200L);
            hVar.a(this.f157a.a(a2, i2));
            hVar.a(a2).a(a3);
            hVar.a();
            return;
        }
        cf a4 = au.i(this).a(0.0f);
        a4.a(200L);
        a4.a(j);
        if (this.e == null || this.c == null) {
            a4.a(this.f157a.a(a4, i2));
            a4.b();
            return;
        }
        h hVar2 = new h();
        cf a5 = au.i(this.c).a(0.0f);
        a5.a(200L);
        hVar2.a(this.f157a.a(a4, i2));
        hVar2.a(a4).a(a5);
        hVar2.a();
    }

    public boolean a() {
        if (this.d != null) {
            return this.d.c();
        }
        return false;
    }

    public int getAnimatedVisibility() {
        return this.i != null ? this.f157a.f179a : getVisibility();
    }

    public int getContentHeight() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration);
        }
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(null, l.ActionBar, b.actionBarStyle, 0);
        setContentHeight(obtainStyledAttributes.getLayoutDimension(l.ActionBar_height, 0));
        obtainStyledAttributes.recycle();
        if (this.d != null) {
            this.d.a(configuration);
        }
    }

    public void setContentHeight(int i2) {
        this.h = i2;
        requestLayout();
    }

    public void setSplitToolbar(boolean z) {
        this.f = z;
    }

    public void setSplitView(ViewGroup viewGroup) {
        this.e = viewGroup;
    }

    public void setSplitWhenNarrow(boolean z) {
        this.g = z;
    }
}
