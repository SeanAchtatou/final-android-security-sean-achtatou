package android.support.v7.internal.view;

import android.content.Context;
import android.support.v7.b.a;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.j;
import android.support.v7.internal.widget.ActionBarContextView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.lang.ref.WeakReference;

public class b extends a implements j {

    /* renamed from: a  reason: collision with root package name */
    private Context f121a;
    private ActionBarContextView b;
    private android.support.v7.b.b c;
    private WeakReference d;
    private boolean e;
    private boolean f;
    private i g;

    public b(Context context, ActionBarContextView actionBarContextView, android.support.v7.b.b bVar, boolean z) {
        this.f121a = context;
        this.b = actionBarContextView;
        this.c = bVar;
        this.g = new i(context).a(1);
        this.g.a(this);
        this.f = z;
    }

    public MenuInflater a() {
        return new MenuInflater(this.f121a);
    }

    public void a(int i) {
        b(this.f121a.getString(i));
    }

    public void a(i iVar) {
        d();
        this.b.a();
    }

    public void a(View view) {
        this.b.setCustomView(view);
        this.d = view != null ? new WeakReference(view) : null;
    }

    public void a(CharSequence charSequence) {
        this.b.setSubtitle(charSequence);
    }

    public void a(boolean z) {
        super.a(z);
        this.b.setTitleOptional(z);
    }

    public boolean a(i iVar, MenuItem menuItem) {
        return this.c.a(this, menuItem);
    }

    public Menu b() {
        return this.g;
    }

    public void b(int i) {
        a((CharSequence) this.f121a.getString(i));
    }

    public void b(CharSequence charSequence) {
        this.b.setTitle(charSequence);
    }

    public void c() {
        if (!this.e) {
            this.e = true;
            this.b.sendAccessibilityEvent(32);
            this.c.a(this);
        }
    }

    public void d() {
        this.c.b(this, this.g);
    }

    public CharSequence f() {
        return this.b.getTitle();
    }

    public CharSequence g() {
        return this.b.getSubtitle();
    }

    public boolean h() {
        return this.b.d();
    }

    public View i() {
        if (this.d != null) {
            return (View) this.d.get();
        }
        return null;
    }
}
