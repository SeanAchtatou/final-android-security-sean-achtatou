package android.support.v7.internal.widget;

import android.support.v7.widget.q;
import android.support.v7.widget.v;
import android.view.View;

class ak extends v {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ao f166a;
    final /* synthetic */ SpinnerCompat b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ak(SpinnerCompat spinnerCompat, View view, ao aoVar) {
        super(view);
        this.b = spinnerCompat;
        this.f166a = aoVar;
    }

    public q a() {
        return this.f166a;
    }

    public boolean b() {
        if (this.b.G.b()) {
            return true;
        }
        this.b.G.c();
        return true;
    }
}
