package android.support.v7.internal.widget;

import android.view.View;
import android.widget.AdapterView;

class ap implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpinnerCompat f171a;
    final /* synthetic */ ao b;

    ap(ao aoVar, SpinnerCompat spinnerCompat) {
        this.b = aoVar;
        this.f171a = spinnerCompat;
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        this.b.f170a.setSelection(i);
        if (this.b.f170a.s != null) {
            this.b.f170a.a(view, i, this.b.d.getItemId(i));
        }
        this.b.a();
    }
}
