package android.support.v7.internal.view;

import android.support.v4.view.cw;
import android.view.View;

class i extends cw {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f128a;
    private boolean b = false;
    private int c = 0;

    i(h hVar) {
        this.f128a = hVar;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.c = 0;
        this.b = false;
        this.f128a.c();
    }

    public void a(View view) {
        if (!this.b) {
            this.b = true;
            if (this.f128a.d != null) {
                this.f128a.d.a(null);
            }
        }
    }

    public void b(View view) {
        int i = this.c + 1;
        this.c = i;
        if (i == this.f128a.f127a.size()) {
            if (this.f128a.d != null) {
                this.f128a.d.b(null);
            }
            a();
        }
    }
}
