package android.support.v7.internal.widget;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;

abstract class AbsSpinnerCompat extends n {
    private DataSetObserver E;

    /* renamed from: a  reason: collision with root package name */
    SpinnerAdapter f148a;
    int b;
    int c;
    int d = 0;
    int e = 0;
    int f = 0;
    int g = 0;
    final Rect h = new Rect();
    final c i = new c(this);

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new d();

        /* renamed from: a  reason: collision with root package name */
        long f149a;
        int b;

        SavedState(Parcel parcel) {
            super(parcel);
            this.f149a = parcel.readLong();
            this.b = parcel.readInt();
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "AbsSpinner.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " selectedId=" + this.f149a + " position=" + this.b + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeLong(this.f149a);
            parcel.writeInt(this.b);
        }
    }

    AbsSpinnerCompat(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        k();
    }

    private void k() {
        setFocusable(true);
        setWillNotDraw(false);
    }

    /* access modifiers changed from: package-private */
    public int a(View view) {
        return view.getMeasuredHeight();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.u = false;
        this.o = false;
        removeAllViewsInLayout();
        this.B = -1;
        this.C = Long.MIN_VALUE;
        setSelectedPositionInt(-1);
        setNextSelectedPositionInt(-1);
        invalidate();
    }

    /* renamed from: a */
    public void setAdapter(SpinnerAdapter spinnerAdapter) {
        int i2 = -1;
        if (this.f148a != null) {
            this.f148a.unregisterDataSetObserver(this.E);
            a();
        }
        this.f148a = spinnerAdapter;
        this.B = -1;
        this.C = Long.MIN_VALUE;
        if (this.f148a != null) {
            this.A = this.z;
            this.z = this.f148a.getCount();
            e();
            this.E = new p(this);
            this.f148a.registerDataSetObserver(this.E);
            if (this.z > 0) {
                i2 = 0;
            }
            setSelectedPositionInt(i2);
            setNextSelectedPositionInt(i2);
            if (this.z == 0) {
                h();
            }
        } else {
            e();
            a();
            h();
        }
        requestLayout();
    }

    /* access modifiers changed from: package-private */
    public int b(View view) {
        return view.getMeasuredWidth();
    }

    /* access modifiers changed from: package-private */
    public void b() {
        int childCount = getChildCount();
        c cVar = this.i;
        int i2 = this.j;
        for (int i3 = 0; i3 < childCount; i3++) {
            cVar.a(i2 + i3, getChildAt(i3));
        }
    }

    /* renamed from: c */
    public SpinnerAdapter getAdapter() {
        return this.f148a;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.LayoutParams(-1, -2);
    }

    public int getCount() {
        return this.z;
    }

    public View getSelectedView() {
        if (this.z <= 0 || this.x < 0) {
            return null;
        }
        return getChildAt(this.x - this.j);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x009b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r10, int r11) {
        /*
            r9 = this;
            r5 = 1
            r4 = 0
            int r6 = android.view.View.MeasureSpec.getMode(r10)
            int r0 = r9.getPaddingLeft()
            int r1 = r9.getPaddingTop()
            int r2 = r9.getPaddingRight()
            int r3 = r9.getPaddingBottom()
            android.graphics.Rect r7 = r9.h
            int r8 = r9.d
            if (r0 <= r8) goto L_0x00cf
        L_0x001c:
            r7.left = r0
            android.graphics.Rect r7 = r9.h
            int r0 = r9.e
            if (r1 <= r0) goto L_0x00d3
            r0 = r1
        L_0x0025:
            r7.top = r0
            android.graphics.Rect r1 = r9.h
            int r0 = r9.f
            if (r2 <= r0) goto L_0x00d7
            r0 = r2
        L_0x002e:
            r1.right = r0
            android.graphics.Rect r1 = r9.h
            int r0 = r9.g
            if (r3 <= r0) goto L_0x00db
            r0 = r3
        L_0x0037:
            r1.bottom = r0
            boolean r0 = r9.u
            if (r0 == 0) goto L_0x0040
            r9.g()
        L_0x0040:
            int r1 = r9.getSelectedItemPosition()
            if (r1 < 0) goto L_0x00df
            android.widget.SpinnerAdapter r0 = r9.f148a
            if (r0 == 0) goto L_0x00df
            android.widget.SpinnerAdapter r0 = r9.f148a
            int r0 = r0.getCount()
            if (r1 >= r0) goto L_0x00df
            android.support.v7.internal.widget.c r0 = r9.i
            android.view.View r0 = r0.a(r1)
            if (r0 != 0) goto L_0x0061
            android.widget.SpinnerAdapter r0 = r9.f148a
            r2 = 0
            android.view.View r0 = r0.getView(r1, r2, r9)
        L_0x0061:
            if (r0 == 0) goto L_0x00df
            android.support.v7.internal.widget.c r2 = r9.i
            r2.a(r1, r0)
            android.view.ViewGroup$LayoutParams r1 = r0.getLayoutParams()
            if (r1 != 0) goto L_0x0079
            r9.D = r5
            android.view.ViewGroup$LayoutParams r1 = r9.generateDefaultLayoutParams()
            r0.setLayoutParams(r1)
            r9.D = r4
        L_0x0079:
            r9.measureChild(r0, r10, r11)
            int r1 = r9.a(r0)
            android.graphics.Rect r2 = r9.h
            int r2 = r2.top
            int r1 = r1 + r2
            android.graphics.Rect r2 = r9.h
            int r2 = r2.bottom
            int r1 = r1 + r2
            int r0 = r9.b(r0)
            android.graphics.Rect r2 = r9.h
            int r2 = r2.left
            int r0 = r0 + r2
            android.graphics.Rect r2 = r9.h
            int r2 = r2.right
            int r0 = r0 + r2
            r2 = r4
        L_0x0099:
            if (r2 == 0) goto L_0x00af
            android.graphics.Rect r1 = r9.h
            int r1 = r1.top
            android.graphics.Rect r2 = r9.h
            int r2 = r2.bottom
            int r1 = r1 + r2
            if (r6 != 0) goto L_0x00af
            android.graphics.Rect r0 = r9.h
            int r0 = r0.left
            android.graphics.Rect r2 = r9.h
            int r2 = r2.right
            int r0 = r0 + r2
        L_0x00af:
            int r2 = r9.getSuggestedMinimumHeight()
            int r1 = java.lang.Math.max(r1, r2)
            int r2 = r9.getSuggestedMinimumWidth()
            int r0 = java.lang.Math.max(r0, r2)
            int r1 = android.support.v4.view.au.a(r1, r11, r4)
            int r0 = android.support.v4.view.au.a(r0, r10, r4)
            r9.setMeasuredDimension(r0, r1)
            r9.b = r11
            r9.c = r10
            return
        L_0x00cf:
            int r0 = r9.d
            goto L_0x001c
        L_0x00d3:
            int r0 = r9.e
            goto L_0x0025
        L_0x00d7:
            int r0 = r9.f
            goto L_0x002e
        L_0x00db:
            int r0 = r9.g
            goto L_0x0037
        L_0x00df:
            r2 = r5
            r0 = r4
            r1 = r4
            goto L_0x0099
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.AbsSpinnerCompat.onMeasure(int, int):void");
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.f149a >= 0) {
            this.u = true;
            this.o = true;
            this.m = savedState.f149a;
            this.l = savedState.b;
            this.p = 0;
            requestLayout();
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f149a = getSelectedItemId();
        if (savedState.f149a >= 0) {
            savedState.b = getSelectedItemPosition();
        } else {
            savedState.b = -1;
        }
        return savedState;
    }

    public void requestLayout() {
        if (!this.D) {
            super.requestLayout();
        }
    }

    public void setSelection(int i2) {
        setNextSelectedPositionInt(i2);
        requestLayout();
        invalidate();
    }
}
