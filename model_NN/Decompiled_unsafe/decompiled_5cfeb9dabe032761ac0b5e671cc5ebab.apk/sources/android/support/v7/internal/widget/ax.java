package android.support.v7.internal.widget;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.v4.f.f;

class ax extends f {
    public ax(int i) {
        super(i);
    }

    private static int b(int i, PorterDuff.Mode mode) {
        return ((i + 31) * 31) + mode.hashCode();
    }

    /* access modifiers changed from: package-private */
    public PorterDuffColorFilter a(int i, PorterDuff.Mode mode) {
        return (PorterDuffColorFilter) a(Integer.valueOf(b(i, mode)));
    }

    /* access modifiers changed from: package-private */
    public PorterDuffColorFilter a(int i, PorterDuff.Mode mode, PorterDuffColorFilter porterDuffColorFilter) {
        return (PorterDuffColorFilter) a(Integer.valueOf(b(i, mode)), porterDuffColorFilter);
    }
}
