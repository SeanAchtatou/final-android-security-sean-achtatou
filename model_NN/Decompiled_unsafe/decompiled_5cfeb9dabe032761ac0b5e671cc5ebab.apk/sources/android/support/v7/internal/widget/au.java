package android.support.v7.internal.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;

class au extends y {

    /* renamed from: a  reason: collision with root package name */
    private final ColorStateList f174a;
    private final PorterDuff.Mode b;
    private int c;

    public au(Drawable drawable, ColorStateList colorStateList) {
        this(drawable, colorStateList, aw.f176a);
    }

    public au(Drawable drawable, ColorStateList colorStateList, PorterDuff.Mode mode) {
        super(drawable);
        this.f174a = colorStateList;
        this.b = mode;
    }

    private boolean a(int[] iArr) {
        int colorForState;
        if (this.f174a == null || (colorForState = this.f174a.getColorForState(iArr, this.c)) == this.c) {
            return false;
        }
        if (colorForState != 0) {
            setColorFilter(colorForState, this.b);
        } else {
            clearColorFilter();
        }
        this.c = colorForState;
        return true;
    }

    public boolean isStateful() {
        return (this.f174a != null && this.f174a.isStateful()) || super.isStateful();
    }

    public boolean setState(int[] iArr) {
        return a(iArr) || super.setState(iArr);
    }
}
