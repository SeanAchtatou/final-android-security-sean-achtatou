package android.support.v7.internal.view.menu;

import android.content.Context;
import android.support.v4.view.i;
import android.view.ActionProvider;
import android.view.MenuItem;
import android.view.View;

class u extends p implements ActionProvider.VisibilityListener {
    i c;
    final /* synthetic */ t d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public u(t tVar, Context context, ActionProvider actionProvider) {
        super(tVar, context, actionProvider);
        this.d = tVar;
    }

    public View a(MenuItem menuItem) {
        return this.f142a.onCreateActionView(menuItem);
    }

    public void a(i iVar) {
        this.c = iVar;
        ActionProvider actionProvider = this.f142a;
        if (iVar == null) {
            this = null;
        }
        actionProvider.setVisibilityListener(this);
    }

    public boolean b() {
        return this.f142a.overridesItemVisibility();
    }

    public boolean c() {
        return this.f142a.isVisible();
    }

    public void onActionProviderVisibilityChanged(boolean z) {
        if (this.c != null) {
            this.c.a(z);
        }
    }
}
