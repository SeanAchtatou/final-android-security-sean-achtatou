package android.support.v7.internal.widget;

import android.view.View;

class ai implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ af f164a;

    private ai(af afVar) {
        this.f164a = afVar;
    }

    /* synthetic */ ai(af afVar, ag agVar) {
        this(afVar);
    }

    public void onClick(View view) {
        ((aj) view).b().d();
        int childCount = this.f164a.e.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = this.f164a.e.getChildAt(i);
            childAt.setSelected(childAt == view);
        }
    }
}
