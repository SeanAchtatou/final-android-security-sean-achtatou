package android.support.v7.internal.widget;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ListAdapter;

class am implements DialogInterface.OnClickListener, ar {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpinnerCompat f168a;
    private AlertDialog b;
    private ListAdapter c;
    private CharSequence d;

    private am(SpinnerCompat spinnerCompat) {
        this.f168a = spinnerCompat;
    }

    /* synthetic */ am(SpinnerCompat spinnerCompat, ak akVar) {
        this(spinnerCompat);
    }

    public void a() {
        if (this.b != null) {
            this.b.dismiss();
            this.b = null;
        }
    }

    public void a(ListAdapter listAdapter) {
        this.c = listAdapter;
    }

    public void a(CharSequence charSequence) {
        this.d = charSequence;
    }

    public boolean b() {
        if (this.b != null) {
            return this.b.isShowing();
        }
        return false;
    }

    public void c() {
        if (this.c != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.f168a.getContext());
            if (this.d != null) {
                builder.setTitle(this.d);
            }
            this.b = builder.setSingleChoiceItems(this.c, this.f168a.getSelectedItemPosition(), this).create();
            this.b.show();
        }
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f168a.setSelection(i);
        if (this.f168a.s != null) {
            this.f168a.a((View) null, i, this.c.getItemId(i));
        }
        a();
    }
}
