package android.support.v7.internal.view;

import android.support.v4.view.cf;
import android.support.v4.view.cv;
import android.support.v4.view.cw;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;

public class h {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final ArrayList f127a = new ArrayList();
    private long b = -1;
    private Interpolator c;
    /* access modifiers changed from: private */
    public cv d;
    private boolean e;
    private final cw f = new i(this);

    /* access modifiers changed from: private */
    public void c() {
        this.e = false;
    }

    public h a(long j) {
        if (!this.e) {
            this.b = j;
        }
        return this;
    }

    public h a(cf cfVar) {
        if (!this.e) {
            this.f127a.add(cfVar);
        }
        return this;
    }

    public h a(cv cvVar) {
        if (!this.e) {
            this.d = cvVar;
        }
        return this;
    }

    public h a(Interpolator interpolator) {
        if (!this.e) {
            this.c = interpolator;
        }
        return this;
    }

    public void a() {
        if (!this.e) {
            Iterator it = this.f127a.iterator();
            while (it.hasNext()) {
                cf cfVar = (cf) it.next();
                if (this.b >= 0) {
                    cfVar.a(this.b);
                }
                if (this.c != null) {
                    cfVar.a(this.c);
                }
                if (this.d != null) {
                    cfVar.a(this.f);
                }
                cfVar.b();
            }
            this.e = true;
        }
    }

    public void b() {
        if (this.e) {
            Iterator it = this.f127a.iterator();
            while (it.hasNext()) {
                ((cf) it.next()).a();
            }
            this.e = false;
        }
    }
}
