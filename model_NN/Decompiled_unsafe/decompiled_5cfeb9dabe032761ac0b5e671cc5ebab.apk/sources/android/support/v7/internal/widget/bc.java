package android.support.v7.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.au;
import android.support.v7.a.b;
import android.support.v7.a.f;
import android.support.v7.a.g;
import android.support.v7.a.j;
import android.support.v7.a.l;
import android.support.v7.internal.a.a;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.y;
import android.support.v7.widget.ActionMenuPresenter;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.an;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

public class bc implements x {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Toolbar f182a;
    private int b;
    private View c;
    private View d;
    private Drawable e;
    private Drawable f;
    private Drawable g;
    private boolean h;
    /* access modifiers changed from: private */
    public CharSequence i;
    private CharSequence j;
    private CharSequence k;
    /* access modifiers changed from: private */
    public a l;
    /* access modifiers changed from: private */
    public boolean m;
    private ActionMenuPresenter n;
    private int o;
    private final aw p;
    private int q;
    private Drawable r;

    public bc(Toolbar toolbar, boolean z) {
        this(toolbar, z, j.abc_action_bar_up_description, f.abc_ic_ab_back_mtrl_am_alpha);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.widget.Toolbar, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public bc(Toolbar toolbar, boolean z, int i2, int i3) {
        this.o = 0;
        this.q = 0;
        this.f182a = toolbar;
        this.i = toolbar.getTitle();
        this.j = toolbar.getSubtitle();
        this.h = this.i != null;
        if (z) {
            bb a2 = bb.a(toolbar.getContext(), null, l.ActionBar, b.actionBarStyle, 0);
            CharSequence b2 = a2.b(l.ActionBar_title);
            if (!TextUtils.isEmpty(b2)) {
                b(b2);
            }
            CharSequence b3 = a2.b(l.ActionBar_subtitle);
            if (!TextUtils.isEmpty(b3)) {
                c(b3);
            }
            Drawable a3 = a2.a(l.ActionBar_logo);
            if (a3 != null) {
                c(a3);
            }
            Drawable a4 = a2.a(l.ActionBar_icon);
            if (a4 != null) {
                a(a4);
            }
            Drawable a5 = a2.a(l.ActionBar_homeAsUpIndicator);
            if (a5 != null) {
                d(a5);
            }
            c(a2.a(l.ActionBar_displayOptions, 0));
            int f2 = a2.f(l.ActionBar_customNavigationLayout, 0);
            if (f2 != 0) {
                a(LayoutInflater.from(this.f182a.getContext()).inflate(f2, (ViewGroup) this.f182a, false));
                c(this.b | 16);
            }
            int e2 = a2.e(l.ActionBar_height, 0);
            if (e2 > 0) {
                ViewGroup.LayoutParams layoutParams = this.f182a.getLayoutParams();
                layoutParams.height = e2;
                this.f182a.setLayoutParams(layoutParams);
            }
            int c2 = a2.c(l.ActionBar_contentInsetStart, -1);
            int c3 = a2.c(l.ActionBar_contentInsetEnd, -1);
            if (c2 >= 0 || c3 >= 0) {
                this.f182a.a(Math.max(c2, 0), Math.max(c3, 0));
            }
            int f3 = a2.f(l.ActionBar_titleTextStyle, 0);
            if (f3 != 0) {
                this.f182a.a(this.f182a.getContext(), f3);
            }
            int f4 = a2.f(l.ActionBar_subtitleTextStyle, 0);
            if (f4 != 0) {
                this.f182a.b(this.f182a.getContext(), f4);
            }
            int f5 = a2.f(l.ActionBar_popupTheme, 0);
            if (f5 != 0) {
                this.f182a.setPopupTheme(f5);
            }
            a2.b();
            this.p = a2.c();
        } else {
            this.b = r();
            this.p = new aw(toolbar.getContext());
        }
        e(i2);
        this.k = this.f182a.getNavigationContentDescription();
        b(this.p.a(i3));
        this.f182a.setNavigationOnClickListener(new bd(this));
    }

    private void e(CharSequence charSequence) {
        this.i = charSequence;
        if ((this.b & 8) != 0) {
            this.f182a.setTitle(charSequence);
        }
    }

    private int r() {
        return this.f182a.getNavigationIcon() != null ? 15 : 11;
    }

    private void s() {
        Drawable drawable = null;
        if ((this.b & 2) != 0) {
            drawable = (this.b & 1) != 0 ? this.f != null ? this.f : this.e : this.e;
        }
        this.f182a.setLogo(drawable);
    }

    private void t() {
        if ((this.b & 4) == 0) {
            return;
        }
        if (TextUtils.isEmpty(this.k)) {
            this.f182a.setNavigationContentDescription(this.q);
        } else {
            this.f182a.setNavigationContentDescription(this.k);
        }
    }

    private void u() {
        if ((this.b & 4) != 0) {
            this.f182a.setNavigationIcon(this.g != null ? this.g : this.r);
        }
    }

    public ViewGroup a() {
        return this.f182a;
    }

    public void a(int i2) {
        a(i2 != 0 ? this.p.a(i2) : null);
    }

    public void a(Drawable drawable) {
        this.e = drawable;
        s();
    }

    public void a(a aVar) {
        this.l = aVar;
    }

    public void a(af afVar) {
        if (this.c != null && this.c.getParent() == this.f182a) {
            this.f182a.removeView(this.c);
        }
        this.c = afVar;
        if (afVar != null && this.o == 2) {
            this.f182a.addView(this.c, 0);
            an anVar = (an) this.c.getLayoutParams();
            anVar.width = -2;
            anVar.height = -2;
            anVar.f103a = 8388691;
            afVar.setAllowCollapse(true);
        }
    }

    public void a(Menu menu, y yVar) {
        if (this.n == null) {
            this.n = new ActionMenuPresenter(this.f182a.getContext());
            this.n.a(g.action_menu_presenter);
        }
        this.n.a(yVar);
        this.f182a.a((i) menu, this.n);
    }

    public void a(View view) {
        if (!(this.d == null || (this.b & 16) == 0)) {
            this.f182a.removeView(this.d);
        }
        this.d = view;
        if (view != null && (this.b & 16) != 0) {
            this.f182a.addView(this.d);
        }
    }

    public void a(CharSequence charSequence) {
        if (!this.h) {
            e(charSequence);
        }
    }

    public void a(boolean z) {
        this.f182a.setCollapsible(z);
    }

    public Context b() {
        return this.f182a.getContext();
    }

    public void b(int i2) {
        c(i2 != 0 ? this.p.a(i2) : null);
    }

    public void b(Drawable drawable) {
        if (this.r != drawable) {
            this.r = drawable;
            u();
        }
    }

    public void b(CharSequence charSequence) {
        this.h = true;
        e(charSequence);
    }

    public void b(boolean z) {
    }

    public void c(int i2) {
        int i3 = this.b ^ i2;
        this.b = i2;
        if (i3 != 0) {
            if ((i3 & 4) != 0) {
                if ((i2 & 4) != 0) {
                    u();
                    t();
                } else {
                    this.f182a.setNavigationIcon((Drawable) null);
                }
            }
            if ((i3 & 3) != 0) {
                s();
            }
            if ((i3 & 8) != 0) {
                if ((i2 & 8) != 0) {
                    this.f182a.setTitle(this.i);
                    this.f182a.setSubtitle(this.j);
                } else {
                    this.f182a.setTitle((CharSequence) null);
                    this.f182a.setSubtitle((CharSequence) null);
                }
            }
            if ((i3 & 16) != 0 && this.d != null) {
                if ((i2 & 16) != 0) {
                    this.f182a.addView(this.d);
                } else {
                    this.f182a.removeView(this.d);
                }
            }
        }
    }

    public void c(Drawable drawable) {
        this.f = drawable;
        s();
    }

    public void c(CharSequence charSequence) {
        this.j = charSequence;
        if ((this.b & 8) != 0) {
            this.f182a.setSubtitle(charSequence);
        }
    }

    public boolean c() {
        return false;
    }

    public void d(int i2) {
        if (i2 == 8) {
            au.i(this.f182a).a(0.0f).a(new be(this));
        } else if (i2 == 0) {
            au.i(this.f182a).a(1.0f).a(new bf(this));
        }
    }

    public void d(Drawable drawable) {
        this.g = drawable;
        u();
    }

    public void d(CharSequence charSequence) {
        this.k = charSequence;
        t();
    }

    public boolean d() {
        return this.f182a.g();
    }

    public void e() {
        this.f182a.h();
    }

    public void e(int i2) {
        if (i2 != this.q) {
            this.q = i2;
            if (TextUtils.isEmpty(this.f182a.getNavigationContentDescription())) {
                f(this.q);
            }
        }
    }

    public CharSequence f() {
        return this.f182a.getTitle();
    }

    public void f(int i2) {
        d(i2 == 0 ? null : b().getString(i2));
    }

    public void g() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public void h() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public boolean i() {
        return this.f182a.a();
    }

    public boolean j() {
        return this.f182a.b();
    }

    public boolean k() {
        return this.f182a.c();
    }

    public boolean l() {
        return this.f182a.d();
    }

    public boolean m() {
        return this.f182a.e();
    }

    public void n() {
        this.m = true;
    }

    public void o() {
        this.f182a.f();
    }

    public int p() {
        return this.b;
    }

    public int q() {
        return this.o;
    }
}
