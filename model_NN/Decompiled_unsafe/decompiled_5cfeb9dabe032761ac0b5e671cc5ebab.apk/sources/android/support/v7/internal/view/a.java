package android.support.v7.internal.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.view.bn;
import android.support.v7.a.b;
import android.support.v7.a.c;
import android.support.v7.a.e;
import android.support.v7.a.h;
import android.support.v7.a.l;
import android.view.ViewConfiguration;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private Context f120a;

    private a(Context context) {
        this.f120a = context;
    }

    public static a a(Context context) {
        return new a(context);
    }

    public int a() {
        return this.f120a.getResources().getInteger(h.abc_max_action_buttons);
    }

    public boolean b() {
        return Build.VERSION.SDK_INT >= 19 || !bn.a(ViewConfiguration.get(this.f120a));
    }

    public int c() {
        return this.f120a.getResources().getDisplayMetrics().widthPixels / 2;
    }

    public boolean d() {
        return this.f120a.getApplicationInfo().targetSdkVersion >= 16 ? this.f120a.getResources().getBoolean(c.abc_action_bar_embed_tabs) : this.f120a.getResources().getBoolean(c.abc_action_bar_embed_tabs_pre_jb);
    }

    public int e() {
        TypedArray obtainStyledAttributes = this.f120a.obtainStyledAttributes(null, l.ActionBar, b.actionBarStyle, 0);
        int layoutDimension = obtainStyledAttributes.getLayoutDimension(l.ActionBar_height, 0);
        Resources resources = this.f120a.getResources();
        if (!d()) {
            layoutDimension = Math.min(layoutDimension, resources.getDimensionPixelSize(e.abc_action_bar_stacked_max_height));
        }
        obtainStyledAttributes.recycle();
        return layoutDimension;
    }

    public boolean f() {
        return this.f120a.getApplicationInfo().targetSdkVersion < 14;
    }

    public int g() {
        return this.f120a.getResources().getDimensionPixelSize(e.abc_action_bar_stacked_tab_max_width);
    }
}
