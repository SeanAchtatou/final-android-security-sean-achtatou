package android.support.v7.internal.view.menu;

import android.content.Context;
import android.support.v4.view.g;
import android.view.ActionProvider;
import android.view.SubMenu;
import android.view.View;

class p extends g {

    /* renamed from: a  reason: collision with root package name */
    final ActionProvider f142a;
    final /* synthetic */ o b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p(o oVar, Context context, ActionProvider actionProvider) {
        super(context);
        this.b = oVar;
        this.f142a = actionProvider;
    }

    public View a() {
        return this.f142a.onCreateActionView();
    }

    public void a(SubMenu subMenu) {
        this.f142a.onPrepareSubMenu(this.b.a(subMenu));
    }

    public boolean d() {
        return this.f142a.onPerformDefaultAction();
    }

    public boolean e() {
        return this.f142a.hasSubMenu();
    }
}
