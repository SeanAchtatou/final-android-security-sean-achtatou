package android.support.v7.internal.view;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.XmlResourceParser;
import android.support.v4.c.a.a;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class e extends MenuInflater {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Class[] f124a = {Context.class};
    /* access modifiers changed from: private */
    public static final Class[] b = f124a;
    /* access modifiers changed from: private */
    public final Object[] c;
    /* access modifiers changed from: private */
    public final Object[] d = this.c;
    /* access modifiers changed from: private */
    public Context e;
    private Object f;

    public e(Context context) {
        super(context);
        this.e = context;
        this.c = new Object[]{context};
    }

    private Object a(Object obj) {
        return (!(obj instanceof Activity) && (obj instanceof ContextWrapper)) ? a(((ContextWrapper) obj).getBaseContext()) : obj;
    }

    private void a(XmlPullParser xmlPullParser, AttributeSet attributeSet, Menu menu) {
        boolean z;
        g gVar = new g(this, menu);
        int eventType = xmlPullParser.getEventType();
        while (true) {
            if (eventType != 2) {
                eventType = xmlPullParser.next();
                if (eventType == 1) {
                    break;
                }
            } else {
                String name = xmlPullParser.getName();
                if (name.equals("menu")) {
                    eventType = xmlPullParser.next();
                } else {
                    throw new RuntimeException("Expecting menu, got " + name);
                }
            }
        }
        String str = null;
        boolean z2 = false;
        int i = eventType;
        boolean z3 = false;
        while (!z3) {
            switch (i) {
                case 1:
                    throw new RuntimeException("Unexpected end of document");
                case 2:
                    if (z2) {
                        z = z2;
                        continue;
                    } else {
                        String name2 = xmlPullParser.getName();
                        if (name2.equals("group")) {
                            gVar.a(attributeSet);
                            z = z2;
                        } else if (name2.equals("item")) {
                            gVar.b(attributeSet);
                            z = z2;
                        } else if (name2.equals("menu")) {
                            a(xmlPullParser, attributeSet, gVar.c());
                            z = z2;
                        } else {
                            str = name2;
                            z = true;
                        }
                    }
                    boolean z4 = z;
                    i = xmlPullParser.next();
                    z2 = z4;
                case 3:
                    String name3 = xmlPullParser.getName();
                    if (!z2 || !name3.equals(str)) {
                        if (name3.equals("group")) {
                            gVar.a();
                            z = z2;
                        } else if (name3.equals("item")) {
                            if (!gVar.d()) {
                                if (gVar.z == null || !gVar.z.e()) {
                                    gVar.b();
                                    z = z2;
                                } else {
                                    gVar.c();
                                    z = z2;
                                }
                            }
                        } else if (name3.equals("menu")) {
                            z3 = true;
                            z = z2;
                        }
                        boolean z42 = z;
                        i = xmlPullParser.next();
                        z2 = z42;
                    } else {
                        str = null;
                        z = false;
                        continue;
                        boolean z422 = z;
                        i = xmlPullParser.next();
                        z2 = z422;
                    }
                    break;
            }
            z = z2;
            boolean z4222 = z;
            i = xmlPullParser.next();
            z2 = z4222;
        }
    }

    /* access modifiers changed from: private */
    public Object c() {
        if (this.f == null) {
            this.f = a(this.e);
        }
        return this.f;
    }

    public void inflate(int i, Menu menu) {
        if (!(menu instanceof a)) {
            super.inflate(i, menu);
            return;
        }
        XmlResourceParser xmlResourceParser = null;
        try {
            xmlResourceParser = this.e.getResources().getLayout(i);
            a(xmlResourceParser, Xml.asAttributeSet(xmlResourceParser), menu);
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
        } catch (XmlPullParserException e2) {
            throw new InflateException("Error inflating menu XML", e2);
        } catch (IOException e3) {
            throw new InflateException("Error inflating menu XML", e3);
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }
}
