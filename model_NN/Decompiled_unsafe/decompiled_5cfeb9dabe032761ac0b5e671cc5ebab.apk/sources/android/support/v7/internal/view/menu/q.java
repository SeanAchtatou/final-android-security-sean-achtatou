package android.support.v7.internal.view.menu;

import android.support.v7.b.c;
import android.view.CollapsibleActionView;
import android.view.View;
import android.widget.FrameLayout;

class q extends FrameLayout implements c {

    /* renamed from: a  reason: collision with root package name */
    final CollapsibleActionView f143a;

    q(View view) {
        super(view.getContext());
        this.f143a = (CollapsibleActionView) view;
        addView(view);
    }

    public void a() {
        this.f143a.onActionViewExpanded();
    }

    public void b() {
        this.f143a.onActionViewCollapsed();
    }

    /* access modifiers changed from: package-private */
    public View c() {
        return (View) this.f143a;
    }
}
