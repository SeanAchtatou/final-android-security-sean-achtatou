package android.support.v7.internal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class m extends ViewGroup.MarginLayoutParams {
    public m(int i, int i2) {
        super(i, i2);
    }

    public m(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public m(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }
}
