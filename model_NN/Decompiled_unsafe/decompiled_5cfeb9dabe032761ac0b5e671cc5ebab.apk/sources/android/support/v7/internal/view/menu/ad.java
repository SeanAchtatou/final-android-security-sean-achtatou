package android.support.v7.internal.view.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.a.a;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public class ad extends i implements SubMenu {
    private i d;
    private m e;

    public ad(Context context, i iVar, m mVar) {
        super(context);
        this.d = iVar;
        this.e = mVar;
    }

    public String a() {
        int itemId = this.e != null ? this.e.getItemId() : 0;
        if (itemId == 0) {
            return null;
        }
        return super.a() + ":" + itemId;
    }

    public void a(j jVar) {
        this.d.a(jVar);
    }

    /* access modifiers changed from: package-private */
    public boolean a(i iVar, MenuItem menuItem) {
        return super.a(iVar, menuItem) || this.d.a(iVar, menuItem);
    }

    public boolean b() {
        return this.d.b();
    }

    public boolean c() {
        return this.d.c();
    }

    public boolean c(m mVar) {
        return this.d.c(mVar);
    }

    public boolean d(m mVar) {
        return this.d.d(mVar);
    }

    public MenuItem getItem() {
        return this.e;
    }

    public i p() {
        return this.d;
    }

    public Menu s() {
        return this.d;
    }

    public SubMenu setHeaderIcon(int i) {
        super.a(a.a(e(), i));
        return this;
    }

    public SubMenu setHeaderIcon(Drawable drawable) {
        super.a(drawable);
        return this;
    }

    public SubMenu setHeaderTitle(int i) {
        super.a(e().getResources().getString(i));
        return this;
    }

    public SubMenu setHeaderTitle(CharSequence charSequence) {
        super.a(charSequence);
        return this;
    }

    public SubMenu setHeaderView(View view) {
        super.a(view);
        return this;
    }

    public SubMenu setIcon(int i) {
        this.e.setIcon(i);
        return this;
    }

    public SubMenu setIcon(Drawable drawable) {
        this.e.setIcon(drawable);
        return this;
    }

    public void setQwertyMode(boolean z) {
        this.d.setQwertyMode(z);
    }
}
