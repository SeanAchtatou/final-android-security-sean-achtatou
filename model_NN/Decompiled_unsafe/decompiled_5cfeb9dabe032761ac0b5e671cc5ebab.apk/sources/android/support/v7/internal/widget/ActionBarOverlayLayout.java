package android.support.v7.internal.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.au;
import android.support.v4.view.cf;
import android.support.v4.view.cv;
import android.support.v4.widget.af;
import android.support.v7.a.b;
import android.support.v7.a.g;
import android.support.v7.internal.a;
import android.support.v7.internal.view.menu.y;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

public class ActionBarOverlayLayout extends ViewGroup implements w {

    /* renamed from: a  reason: collision with root package name */
    static final int[] f151a = {b.actionBarSize, 16842841};
    /* access modifiers changed from: private */
    public final cv A = new h(this);
    /* access modifiers changed from: private */
    public final cv B = new i(this);
    private final Runnable C = new j(this);
    private final Runnable D = new k(this);
    private int b;
    private int c = 0;
    private ContentFrameLayout d;
    /* access modifiers changed from: private */
    public ActionBarContainer e;
    /* access modifiers changed from: private */
    public ActionBarContainer f;
    private x g;
    private Drawable h;
    private boolean i;
    private boolean j;
    private boolean k;
    private boolean l;
    /* access modifiers changed from: private */
    public boolean m;
    private int n;
    private int o;
    private final Rect p = new Rect();
    private final Rect q = new Rect();
    private final Rect r = new Rect();
    private final Rect s = new Rect();
    private final Rect t = new Rect();
    private final Rect u = new Rect();
    private l v;
    private final int w = 600;
    private af x;
    /* access modifiers changed from: private */
    public cf y;
    /* access modifiers changed from: private */
    public cf z;

    public ActionBarOverlayLayout(Context context) {
        super(context);
        a(context);
    }

    public ActionBarOverlayLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    private x a(View view) {
        if (view instanceof x) {
            return (x) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        throw new IllegalStateException("Can't make a decor toolbar out of " + view.getClass().getSimpleName());
    }

    private void a(Context context) {
        boolean z2 = true;
        TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(f151a);
        this.b = obtainStyledAttributes.getDimensionPixelSize(0, 0);
        this.h = obtainStyledAttributes.getDrawable(1);
        setWillNotDraw(this.h == null);
        obtainStyledAttributes.recycle();
        if (context.getApplicationInfo().targetSdkVersion >= 19) {
            z2 = false;
        }
        this.i = z2;
        this.x = af.a(context);
    }

    private boolean a(float f2, float f3) {
        this.x.a(0, 0, 0, (int) f3, 0, 0, Integer.MIN_VALUE, Integer.MAX_VALUE);
        return this.x.d() > this.f.getHeight();
    }

    private boolean a(View view, Rect rect, boolean z2, boolean z3, boolean z4, boolean z5) {
        boolean z6 = false;
        m mVar = (m) view.getLayoutParams();
        if (z2 && mVar.leftMargin != rect.left) {
            mVar.leftMargin = rect.left;
            z6 = true;
        }
        if (z3 && mVar.topMargin != rect.top) {
            mVar.topMargin = rect.top;
            z6 = true;
        }
        if (z5 && mVar.rightMargin != rect.right) {
            mVar.rightMargin = rect.right;
            z6 = true;
        }
        if (!z4 || mVar.bottomMargin == rect.bottom) {
            return z6;
        }
        mVar.bottomMargin = rect.bottom;
        return true;
    }

    /* access modifiers changed from: private */
    public void k() {
        removeCallbacks(this.C);
        removeCallbacks(this.D);
        if (this.y != null) {
            this.y.a();
        }
        if (this.z != null) {
            this.z.a();
        }
    }

    private void l() {
        k();
        postDelayed(this.C, 600);
    }

    private void m() {
        k();
        postDelayed(this.D, 600);
    }

    private void n() {
        k();
        this.C.run();
    }

    private void o() {
        k();
        this.D.run();
    }

    /* renamed from: a */
    public m generateLayoutParams(AttributeSet attributeSet) {
        return new m(getContext(), attributeSet);
    }

    public void a(int i2) {
        c();
        switch (i2) {
            case 2:
                this.g.g();
                return;
            case 5:
                this.g.h();
                return;
            case 9:
                setOverlayMode(true);
                return;
            default:
                return;
        }
    }

    public void a(Menu menu, y yVar) {
        c();
        this.g.a(menu, yVar);
    }

    public boolean a() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public m generateDefaultLayoutParams() {
        return new m(-1, -1);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.d == null) {
            this.d = (ContentFrameLayout) findViewById(g.action_bar_activity_content);
            this.f = (ActionBarContainer) findViewById(g.action_bar_container);
            this.g = a(findViewById(g.action_bar));
            this.e = (ActionBarContainer) findViewById(g.split_action_bar);
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof m;
    }

    public boolean d() {
        c();
        return this.g.i();
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.h != null && !this.i) {
            int bottom = this.f.getVisibility() == 0 ? (int) (((float) this.f.getBottom()) + au.g(this.f) + 0.5f) : 0;
            this.h.setBounds(0, bottom, getWidth(), this.h.getIntrinsicHeight() + bottom);
            this.h.draw(canvas);
        }
    }

    public boolean e() {
        c();
        return this.g.j();
    }

    public boolean f() {
        c();
        return this.g.k();
    }

    /* access modifiers changed from: protected */
    public boolean fitSystemWindows(Rect rect) {
        c();
        if ((au.j(this) & 256) != 0) {
        }
        boolean a2 = a(this.f, rect, true, true, false, true);
        if (this.e != null) {
            a2 |= a(this.e, rect, true, false, true, true);
        }
        this.s.set(rect);
        bh.a(this, this.s, this.p);
        if (!this.q.equals(this.p)) {
            this.q.set(this.p);
            a2 = true;
        }
        if (a2) {
            requestLayout();
        }
        return true;
    }

    public boolean g() {
        c();
        return this.g.l();
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return new m(layoutParams);
    }

    public int getActionBarHideOffset() {
        if (this.f != null) {
            return -((int) au.g(this.f));
        }
        return 0;
    }

    public CharSequence getTitle() {
        c();
        return this.g.f();
    }

    public boolean h() {
        c();
        return this.g.m();
    }

    public void i() {
        c();
        this.g.n();
    }

    public void j() {
        c();
        this.g.o();
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        if (Build.VERSION.SDK_INT >= 8) {
            super.onConfigurationChanged(configuration);
        }
        a(getContext());
        au.k(this);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        k();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        int paddingLeft = getPaddingLeft();
        int paddingRight = (i4 - i2) - getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = (i5 - i3) - getPaddingBottom();
        for (int i6 = 0; i6 < childCount; i6++) {
            View childAt = getChildAt(i6);
            if (childAt.getVisibility() != 8) {
                m mVar = (m) childAt.getLayoutParams();
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                int i7 = mVar.leftMargin + paddingLeft;
                int i8 = childAt == this.e ? (paddingBottom - measuredHeight) - mVar.bottomMargin : mVar.topMargin + paddingTop;
                childAt.layout(i7, i8, measuredWidth + i7, measuredHeight + i8);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int measuredHeight;
        c();
        measureChildWithMargins(this.f, i2, 0, i3, 0);
        m mVar = (m) this.f.getLayoutParams();
        int max = Math.max(0, this.f.getMeasuredWidth() + mVar.leftMargin + mVar.rightMargin);
        int max2 = Math.max(0, mVar.bottomMargin + this.f.getMeasuredHeight() + mVar.topMargin);
        int a2 = bh.a(0, au.f(this.f));
        if (this.e != null) {
            measureChildWithMargins(this.e, i2, 0, i3, 0);
            m mVar2 = (m) this.e.getLayoutParams();
            int max3 = Math.max(max, this.e.getMeasuredWidth() + mVar2.leftMargin + mVar2.rightMargin);
            int max4 = Math.max(max2, mVar2.bottomMargin + this.e.getMeasuredHeight() + mVar2.topMargin);
            i6 = bh.a(a2, au.f(this.e));
            i5 = max3;
            i4 = max4;
        } else {
            i4 = max2;
            i5 = max;
            i6 = a2;
        }
        boolean z2 = (au.j(this) & 256) != 0;
        if (z2) {
            measuredHeight = this.b;
            if (this.k && this.f.getTabContainer() != null) {
                measuredHeight += this.b;
            }
        } else {
            measuredHeight = this.f.getVisibility() != 8 ? this.f.getMeasuredHeight() : 0;
        }
        int measuredHeight2 = (!this.g.c() || this.e == null) ? 0 : z2 ? this.b : this.e.getMeasuredHeight();
        this.r.set(this.p);
        this.t.set(this.s);
        if (this.j || z2) {
            Rect rect = this.t;
            rect.top = measuredHeight + rect.top;
            Rect rect2 = this.t;
            rect2.bottom = measuredHeight2 + rect2.bottom;
        } else {
            Rect rect3 = this.r;
            rect3.top = measuredHeight + rect3.top;
            Rect rect4 = this.r;
            rect4.bottom = measuredHeight2 + rect4.bottom;
        }
        a(this.d, this.r, true, true, true, true);
        if (!this.u.equals(this.t)) {
            this.u.set(this.t);
            this.d.a(this.t);
        }
        measureChildWithMargins(this.d, i2, 0, i3, 0);
        m mVar3 = (m) this.d.getLayoutParams();
        int max5 = Math.max(i5, this.d.getMeasuredWidth() + mVar3.leftMargin + mVar3.rightMargin);
        int max6 = Math.max(i4, mVar3.bottomMargin + this.d.getMeasuredHeight() + mVar3.topMargin);
        int a3 = bh.a(i6, au.f(this.d));
        setMeasuredDimension(au.a(Math.max(max5 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i2, a3), au.a(Math.max(max6 + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight()), i3, a3 << 16));
    }

    public boolean onNestedFling(View view, float f2, float f3, boolean z2) {
        if (!this.l || !z2) {
            return false;
        }
        if (a(f2, f3)) {
            o();
        } else {
            n();
        }
        this.m = true;
        return true;
    }

    public void onNestedScroll(View view, int i2, int i3, int i4, int i5) {
        this.n += i3;
        setActionBarHideOffset(this.n);
    }

    public void onNestedScrollAccepted(View view, View view2, int i2) {
        super.onNestedScrollAccepted(view, view2, i2);
        this.n = getActionBarHideOffset();
        k();
        if (this.v != null) {
            this.v.i();
        }
    }

    public boolean onStartNestedScroll(View view, View view2, int i2) {
        if ((i2 & 2) == 0 || this.f.getVisibility() != 0) {
            return false;
        }
        return this.l;
    }

    public void onStopNestedScroll(View view) {
        super.onStopNestedScroll(view);
        if (this.l && !this.m) {
            if (this.n <= this.f.getHeight()) {
                l();
            } else {
                m();
            }
        }
        if (this.v != null) {
            this.v.j();
        }
    }

    public void onWindowSystemUiVisibilityChanged(int i2) {
        boolean z2 = true;
        if (Build.VERSION.SDK_INT >= 16) {
            super.onWindowSystemUiVisibilityChanged(i2);
        }
        c();
        int i3 = this.o ^ i2;
        this.o = i2;
        boolean z3 = (i2 & 4) == 0;
        boolean z4 = (i2 & 256) != 0;
        if (this.v != null) {
            l lVar = this.v;
            if (z4) {
                z2 = false;
            }
            lVar.g(z2);
            if (z3 || !z4) {
                this.v.g();
            } else {
                this.v.h();
            }
        }
        if ((i3 & 256) != 0 && this.v != null) {
            au.k(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        this.c = i2;
        if (this.v != null) {
            this.v.a(i2);
        }
    }

    public void setActionBarHideOffset(int i2) {
        k();
        int height = this.f.getHeight();
        int max = Math.max(0, Math.min(i2, height));
        au.b(this.f, (float) (-max));
        if (this.e != null && this.e.getVisibility() != 8) {
            au.b(this.e, (float) ((int) ((((float) max) / ((float) height)) * ((float) this.e.getHeight()))));
        }
    }

    public void setActionBarVisibilityCallback(l lVar) {
        this.v = lVar;
        if (getWindowToken() != null) {
            this.v.a(this.c);
            if (this.o != 0) {
                onWindowSystemUiVisibilityChanged(this.o);
                au.k(this);
            }
        }
    }

    public void setHasNonEmbeddedTabs(boolean z2) {
        this.k = z2;
    }

    public void setHideOnContentScrollEnabled(boolean z2) {
        if (z2 != this.l) {
            this.l = z2;
            if (!z2) {
                if (a.a()) {
                    stopNestedScroll();
                }
                k();
                setActionBarHideOffset(0);
            }
        }
    }

    public void setIcon(int i2) {
        c();
        this.g.a(i2);
    }

    public void setIcon(Drawable drawable) {
        c();
        this.g.a(drawable);
    }

    public void setLogo(int i2) {
        c();
        this.g.b(i2);
    }

    public void setOverlayMode(boolean z2) {
        this.j = z2;
        this.i = z2 && getContext().getApplicationInfo().targetSdkVersion < 19;
    }

    public void setShowingForActionMode(boolean z2) {
    }

    public void setUiOptions(int i2) {
    }

    public void setWindowCallback(android.support.v7.internal.a.a aVar) {
        c();
        this.g.a(aVar);
    }

    public void setWindowTitle(CharSequence charSequence) {
        c();
        this.g.a(charSequence);
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }
}
