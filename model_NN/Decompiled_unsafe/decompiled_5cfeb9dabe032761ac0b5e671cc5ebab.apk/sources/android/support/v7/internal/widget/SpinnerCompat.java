package android.support.v7.internal.widget;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.au;
import android.support.v4.view.j;
import android.support.v7.a.l;
import android.support.v7.internal.widget.AbsSpinnerCompat;
import android.support.v7.widget.v;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.SpinnerAdapter;

class SpinnerCompat extends AbsSpinnerCompat implements DialogInterface.OnClickListener {
    int E;
    private v F;
    /* access modifiers changed from: private */
    public ar G;
    private an H;
    private int I;
    private boolean J;
    private Rect K;
    private final aw L;

    class SavedState extends AbsSpinnerCompat.SavedState {
        public static final Parcelable.Creator CREATOR = new aq();
        boolean c;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.c = parcel.readByte() != 0;
        }

        /* synthetic */ SavedState(Parcel parcel, ak akVar) {
            this(parcel);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeByte((byte) (this.c ? 1 : 0));
        }
    }

    SpinnerCompat(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.bb.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.internal.widget.bb.a(int, float):float
      android.support.v7.internal.widget.bb.a(int, int):int
      android.support.v7.internal.widget.bb.a(int, boolean):boolean */
    SpinnerCompat(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i);
        this.K = new Rect();
        bb a2 = bb.a(context, attributeSet, l.Spinner, i, 0);
        setBackgroundDrawable(a2.a(l.Spinner_android_background));
        switch (i2 == -1 ? a2.a(l.Spinner_spinnerMode, 0) : i2) {
            case 0:
                this.G = new am(this, null);
                break;
            case 1:
                ao aoVar = new ao(this, context, attributeSet, i);
                this.E = a2.e(l.Spinner_android_dropDownWidth, -2);
                aoVar.a(a2.a(l.Spinner_android_popupBackground));
                this.G = aoVar;
                this.F = new ak(this, this, aoVar);
                break;
        }
        this.I = a2.a(l.Spinner_android_gravity, 17);
        this.G.a(a2.c(l.Spinner_prompt));
        this.J = a2.a(l.Spinner_disableChildrenWhenDisabled, false);
        a2.b();
        if (this.H != null) {
            this.G.a(this.H);
            this.H = null;
        }
        this.L = a2.c();
    }

    private void a(View view, boolean z) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = generateDefaultLayoutParams();
        }
        if (z) {
            addViewInLayout(view, 0, layoutParams);
        }
        view.setSelected(hasFocus());
        if (this.J) {
            view.setEnabled(isEnabled());
        }
        view.measure(ViewGroup.getChildMeasureSpec(this.c, this.h.left + this.h.right, layoutParams.width), ViewGroup.getChildMeasureSpec(this.b, this.h.top + this.h.bottom, layoutParams.height));
        int measuredHeight = this.h.top + ((((getMeasuredHeight() - this.h.bottom) - this.h.top) - view.getMeasuredHeight()) / 2);
        view.layout(0, measuredHeight, view.getMeasuredWidth() + 0, view.getMeasuredHeight() + measuredHeight);
    }

    private View c(int i, boolean z) {
        View a2;
        if (this.u || (a2 = this.i.a(i)) == null) {
            View view = this.f148a.getView(i, null, this);
            a(view, z);
            return view;
        }
        a(a2, z);
        return a2;
    }

    /* access modifiers changed from: package-private */
    public int a(SpinnerAdapter spinnerAdapter, Drawable drawable) {
        View view;
        if (spinnerAdapter == null) {
            return 0;
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
        int max = Math.max(0, getSelectedItemPosition());
        int min = Math.min(spinnerAdapter.getCount(), max + 15);
        int max2 = Math.max(0, max - (15 - (min - max)));
        View view2 = null;
        int i = 0;
        int i2 = 0;
        while (max2 < min) {
            int itemViewType = spinnerAdapter.getItemViewType(max2);
            if (itemViewType != i2) {
                view = null;
            } else {
                itemViewType = i2;
                view = view2;
            }
            view2 = spinnerAdapter.getView(max2, view, this);
            if (view2.getLayoutParams() == null) {
                view2.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            }
            view2.measure(makeMeasureSpec, makeMeasureSpec2);
            i = Math.max(i, view2.getMeasuredWidth());
            max2++;
            i2 = itemViewType;
        }
        if (drawable == null) {
            return i;
        }
        drawable.getPadding(this.K);
        return this.K.left + this.K.right + i;
    }

    /* access modifiers changed from: package-private */
    public void a(q qVar) {
        super.setOnItemClickListener(qVar);
    }

    /* renamed from: a */
    public void setAdapter(SpinnerAdapter spinnerAdapter) {
        super.setAdapter(spinnerAdapter);
        this.i.a();
        if (getContext().getApplicationInfo().targetSdkVersion >= 21 && spinnerAdapter != null && spinnerAdapter.getViewTypeCount() != 1) {
            throw new IllegalArgumentException("Spinner adapter view type count must be 1");
        } else if (this.G != null) {
            this.G.a(new an(spinnerAdapter));
        } else {
            this.H = new an(spinnerAdapter);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(int i, boolean z) {
        int i2 = this.h.left;
        int right = ((getRight() - getLeft()) - this.h.left) - this.h.right;
        if (this.u) {
            g();
        }
        if (this.z == 0) {
            a();
            return;
        }
        if (this.v >= 0) {
            setSelectedPositionInt(this.v);
        }
        b();
        removeAllViewsInLayout();
        this.j = this.x;
        if (this.f148a != null) {
            View c = c(this.x, true);
            int measuredWidth = c.getMeasuredWidth();
            switch (j.a(this.I, au.d(this)) & 7) {
                case 1:
                    i2 = (i2 + (right / 2)) - (measuredWidth / 2);
                    break;
                case 5:
                    i2 = (i2 + right) - measuredWidth;
                    break;
            }
            c.offsetLeftAndRight(i2);
        }
        this.i.a();
        invalidate();
        h();
        this.u = false;
        this.o = false;
        setNextSelectedPositionInt(this.x);
    }

    public int getBaseline() {
        int baseline;
        View view = null;
        if (getChildCount() > 0) {
            view = getChildAt(0);
        } else if (this.f148a != null && this.f148a.getCount() > 0) {
            view = c(0, false);
            this.i.a(0, view);
        }
        if (view == null || (baseline = view.getBaseline()) < 0) {
            return -1;
        }
        return view.getTop() + baseline;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        setSelection(i);
        dialogInterface.dismiss();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.G != null && this.G.b()) {
            this.G.a();
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.q = true;
        b(0, false);
        this.q = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.G != null && View.MeasureSpec.getMode(i) == Integer.MIN_VALUE) {
            setMeasuredDimension(Math.min(Math.max(getMeasuredWidth(), a(getAdapter(), getBackground())), View.MeasureSpec.getSize(i)), getMeasuredHeight());
        }
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        ViewTreeObserver viewTreeObserver;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (savedState.c && (viewTreeObserver = getViewTreeObserver()) != null) {
            viewTreeObserver.addOnGlobalLayoutListener(new al(this));
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.c = this.G != null && this.G.b();
        return savedState;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.F == null || !this.F.onTouch(this, motionEvent)) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    public boolean performClick() {
        boolean performClick = super.performClick();
        if (!performClick) {
            performClick = true;
            if (!this.G.b()) {
                this.G.c();
            }
        }
        return performClick;
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (this.J) {
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                getChildAt(i).setEnabled(z);
            }
        }
    }

    public void setOnItemClickListener(q qVar) {
        throw new RuntimeException("setOnItemClickListener cannot be used with a spinner.");
    }
}
