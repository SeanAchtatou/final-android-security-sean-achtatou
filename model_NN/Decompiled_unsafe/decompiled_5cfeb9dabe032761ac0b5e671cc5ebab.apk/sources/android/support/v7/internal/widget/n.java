package android.support.v7.internal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Adapter;

public abstract class n extends ViewGroup {
    int A;
    int B = -1;
    long C = Long.MIN_VALUE;
    boolean D = false;

    /* renamed from: a  reason: collision with root package name */
    private int f194a;
    private View b;
    private boolean c;
    private boolean d;
    private t e;
    @ViewDebug.ExportedProperty(category = "scrolling")
    int j = 0;
    int k;
    int l;
    long m = Long.MIN_VALUE;
    long n;
    boolean o = false;
    int p;
    boolean q = false;
    s r;
    q s;
    r t;
    boolean u;
    @ViewDebug.ExportedProperty(category = "list")
    int v = -1;
    long w = Long.MIN_VALUE;
    @ViewDebug.ExportedProperty(category = "list")
    int x = -1;
    long y = Long.MIN_VALUE;
    @ViewDebug.ExportedProperty(category = "list")
    int z;

    n(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: private */
    public void a() {
        if (this.r != null) {
            int selectedItemPosition = getSelectedItemPosition();
            if (selectedItemPosition >= 0) {
                this.r.a(this, getSelectedView(), selectedItemPosition, getAdapter().getItemId(selectedItemPosition));
                return;
            }
            this.r.a(this);
        }
    }

    private void a(boolean z2) {
        if (d()) {
            z2 = false;
        }
        if (z2) {
            if (this.b != null) {
                this.b.setVisibility(0);
                setVisibility(8);
            } else {
                setVisibility(0);
            }
            if (this.u) {
                onLayout(false, getLeft(), getTop(), getRight(), getBottom());
                return;
            }
            return;
        }
        if (this.b != null) {
            this.b.setVisibility(8);
        }
        setVisibility(0);
    }

    /* access modifiers changed from: package-private */
    public int a(int i, boolean z2) {
        return i;
    }

    public long a(int i) {
        Adapter adapter = getAdapter();
        if (adapter == null || i < 0) {
            return Long.MIN_VALUE;
        }
        return adapter.getItemId(i);
    }

    public boolean a(View view, int i, long j2) {
        if (this.s == null) {
            return false;
        }
        playSoundEffect(0);
        if (view != null) {
            view.sendAccessibilityEvent(1);
        }
        this.s.a(this, view, i, j2);
        return true;
    }

    public void addView(View view) {
        throw new UnsupportedOperationException("addView(View) is not supported in AdapterView");
    }

    public void addView(View view, int i) {
        throw new UnsupportedOperationException("addView(View, int) is not supported in AdapterView");
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        throw new UnsupportedOperationException("addView(View, int, LayoutParams) is not supported in AdapterView");
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        throw new UnsupportedOperationException("addView(View, LayoutParams) is not supported in AdapterView");
    }

    /* access modifiers changed from: protected */
    public boolean canAnimate() {
        return super.canAnimate() && this.z > 0;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return false;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        View selectedView = getSelectedView();
        return selectedView != null && selectedView.getVisibility() == 0 && selectedView.dispatchPopulateAccessibilityEvent(accessibilityEvent);
    }

    /* access modifiers changed from: protected */
    public void dispatchRestoreInstanceState(SparseArray sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    /* access modifiers changed from: protected */
    public void dispatchSaveInstanceState(SparseArray sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    /* access modifiers changed from: package-private */
    public void e() {
        boolean z2 = false;
        Adapter adapter = getAdapter();
        boolean z3 = !(adapter == null || adapter.getCount() == 0) || d();
        super.setFocusableInTouchMode(z3 && this.d);
        super.setFocusable(z3 && this.c);
        if (this.b != null) {
            if (adapter == null || adapter.isEmpty()) {
                z2 = true;
            }
            a(z2);
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.r != null) {
            if (this.q || this.D) {
                if (this.e == null) {
                    this.e = new t(this);
                }
                post(this.e);
            } else {
                a();
            }
        }
        if (this.x != -1 && isShown() && !isInTouchMode()) {
            sendAccessibilityEvent(4);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.n.a(int, boolean):int
     arg types: [int, int]
     candidates:
      android.support.v7.internal.widget.n.a(android.support.v7.internal.widget.n, android.os.Parcelable):void
      android.support.v7.internal.widget.n.a(int, boolean):int */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void g() {
        /*
            r8 = this;
            r6 = -9223372036854775808
            r5 = -1
            r2 = 1
            r1 = 0
            int r4 = r8.z
            if (r4 <= 0) goto L_0x0055
            boolean r0 = r8.o
            if (r0 == 0) goto L_0x0053
            r8.o = r1
            int r0 = r8.i()
            if (r0 < 0) goto L_0x0053
            int r3 = r8.a(r0, r2)
            if (r3 != r0) goto L_0x0053
            r8.setNextSelectedPositionInt(r0)
            r3 = r2
        L_0x001f:
            if (r3 != 0) goto L_0x004f
            int r0 = r8.getSelectedItemPosition()
            if (r0 < r4) goto L_0x0029
            int r0 = r4 + -1
        L_0x0029:
            if (r0 >= 0) goto L_0x002c
            r0 = r1
        L_0x002c:
            int r4 = r8.a(r0, r2)
            if (r4 >= 0) goto L_0x0051
            int r0 = r8.a(r0, r1)
        L_0x0036:
            if (r0 < 0) goto L_0x004f
            r8.setNextSelectedPositionInt(r0)
            r8.h()
            r0 = r2
        L_0x003f:
            if (r0 != 0) goto L_0x004e
            r8.x = r5
            r8.y = r6
            r8.v = r5
            r8.w = r6
            r8.o = r1
            r8.h()
        L_0x004e:
            return
        L_0x004f:
            r0 = r3
            goto L_0x003f
        L_0x0051:
            r0 = r4
            goto L_0x0036
        L_0x0053:
            r3 = r1
            goto L_0x001f
        L_0x0055:
            r0 = r1
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.n.g():void");
    }

    public abstract Adapter getAdapter();

    @ViewDebug.CapturedViewProperty
    public int getCount() {
        return this.z;
    }

    public View getEmptyView() {
        return this.b;
    }

    public int getFirstVisiblePosition() {
        return this.j;
    }

    public int getLastVisiblePosition() {
        return (this.j + getChildCount()) - 1;
    }

    public final q getOnItemClickListener() {
        return this.s;
    }

    public final r getOnItemLongClickListener() {
        return this.t;
    }

    public final s getOnItemSelectedListener() {
        return this.r;
    }

    public Object getSelectedItem() {
        Adapter adapter = getAdapter();
        int selectedItemPosition = getSelectedItemPosition();
        if (adapter == null || adapter.getCount() <= 0 || selectedItemPosition < 0) {
            return null;
        }
        return adapter.getItem(selectedItemPosition);
    }

    @ViewDebug.CapturedViewProperty
    public long getSelectedItemId() {
        return this.w;
    }

    @ViewDebug.CapturedViewProperty
    public int getSelectedItemPosition() {
        return this.v;
    }

    public abstract View getSelectedView();

    /* access modifiers changed from: package-private */
    public void h() {
        if (this.x != this.B || this.y != this.C) {
            f();
            this.B = this.x;
            this.C = this.y;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: CFG modification limit reached, blocks count: 140 */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005c, code lost:
        if (r5 != false) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005e, code lost:
        if (r0 != false) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0060, code lost:
        if (r4 != false) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0062, code lost:
        r2 = r2 - 1;
        r0 = true;
        r3 = r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int i() {
        /*
            r12 = this;
            int r6 = r12.z
            if (r6 != 0) goto L_0x0006
            r3 = -1
        L_0x0005:
            return r3
        L_0x0006:
            long r8 = r12.m
            int r0 = r12.l
            r2 = -9223372036854775808
            int r1 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r1 != 0) goto L_0x0012
            r3 = -1
            goto L_0x0005
        L_0x0012:
            r1 = 0
            int r0 = java.lang.Math.max(r1, r0)
            int r1 = r6 + -1
            int r1 = java.lang.Math.min(r1, r0)
            long r2 = android.os.SystemClock.uptimeMillis()
            r4 = 100
            long r10 = r2 + r4
            r0 = 0
            android.widget.Adapter r7 = r12.getAdapter()
            if (r7 != 0) goto L_0x0067
            r3 = -1
            goto L_0x0005
        L_0x002e:
            if (r4 != 0) goto L_0x0034
            if (r0 == 0) goto L_0x005c
            if (r5 != 0) goto L_0x005c
        L_0x0034:
            int r1 = r1 + 1
            r0 = 0
            r3 = r1
        L_0x0038:
            long r4 = android.os.SystemClock.uptimeMillis()
            int r4 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r4 > 0) goto L_0x0055
            long r4 = r7.getItemId(r3)
            int r4 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r4 == 0) goto L_0x0005
            int r4 = r6 + -1
            if (r1 != r4) goto L_0x0057
            r4 = 1
            r5 = r4
        L_0x004e:
            if (r2 != 0) goto L_0x005a
            r4 = 1
        L_0x0051:
            if (r5 == 0) goto L_0x002e
            if (r4 == 0) goto L_0x002e
        L_0x0055:
            r3 = -1
            goto L_0x0005
        L_0x0057:
            r4 = 0
            r5 = r4
            goto L_0x004e
        L_0x005a:
            r4 = 0
            goto L_0x0051
        L_0x005c:
            if (r5 != 0) goto L_0x0062
            if (r0 != 0) goto L_0x0038
            if (r4 != 0) goto L_0x0038
        L_0x0062:
            int r2 = r2 + -1
            r0 = 1
            r3 = r2
            goto L_0x0038
        L_0x0067:
            r2 = r1
            r3 = r1
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.internal.widget.n.i():int");
    }

    /* access modifiers changed from: package-private */
    public void j() {
        if (getChildCount() > 0) {
            this.o = true;
            this.n = (long) this.f194a;
            if (this.x >= 0) {
                View childAt = getChildAt(this.x - this.j);
                this.m = this.w;
                this.l = this.v;
                if (childAt != null) {
                    this.k = childAt.getTop();
                }
                this.p = 0;
                return;
            }
            View childAt2 = getChildAt(0);
            Adapter adapter = getAdapter();
            if (this.j < 0 || this.j >= adapter.getCount()) {
                this.m = -1;
            } else {
                this.m = adapter.getItemId(this.j);
            }
            this.l = this.j;
            if (childAt2 != null) {
                this.k = childAt2.getTop();
            }
            this.p = 1;
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.e);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        this.f194a = getHeight();
    }

    public void removeAllViews() {
        throw new UnsupportedOperationException("removeAllViews() is not supported in AdapterView");
    }

    public void removeView(View view) {
        throw new UnsupportedOperationException("removeView(View) is not supported in AdapterView");
    }

    public void removeViewAt(int i) {
        throw new UnsupportedOperationException("removeViewAt(int) is not supported in AdapterView");
    }

    public abstract void setAdapter(Adapter adapter);

    public void setEmptyView(View view) {
        this.b = view;
        Adapter adapter = getAdapter();
        a(adapter == null || adapter.isEmpty());
    }

    public void setFocusable(boolean z2) {
        boolean z3 = true;
        Adapter adapter = getAdapter();
        boolean z4 = adapter == null || adapter.getCount() == 0;
        this.c = z2;
        if (!z2) {
            this.d = false;
        }
        if (!z2 || (z4 && !d())) {
            z3 = false;
        }
        super.setFocusable(z3);
    }

    public void setFocusableInTouchMode(boolean z2) {
        boolean z3 = true;
        Adapter adapter = getAdapter();
        boolean z4 = adapter == null || adapter.getCount() == 0;
        this.d = z2;
        if (z2) {
            this.c = true;
        }
        if (!z2 || (z4 && !d())) {
            z3 = false;
        }
        super.setFocusableInTouchMode(z3);
    }

    /* access modifiers changed from: package-private */
    public void setNextSelectedPositionInt(int i) {
        this.v = i;
        this.w = a(i);
        if (this.o && this.p == 0 && i >= 0) {
            this.l = i;
            this.m = this.w;
        }
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        throw new RuntimeException("Don't call setOnClickListener for an AdapterView. You probably want setOnItemClickListener instead");
    }

    public void setOnItemClickListener(q qVar) {
        this.s = qVar;
    }

    public void setOnItemLongClickListener(r rVar) {
        if (!isLongClickable()) {
            setLongClickable(true);
        }
        this.t = rVar;
    }

    public void setOnItemSelectedListener(s sVar) {
        this.r = sVar;
    }

    /* access modifiers changed from: package-private */
    public void setSelectedPositionInt(int i) {
        this.x = i;
        this.y = a(i);
    }

    public abstract void setSelection(int i);
}
