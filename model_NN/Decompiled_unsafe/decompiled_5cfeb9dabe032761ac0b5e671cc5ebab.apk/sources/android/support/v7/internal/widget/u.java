package android.support.v7.internal.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.a.l;
import android.util.AttributeSet;
import android.view.View;
import android.widget.PopupWindow;

public class u extends PopupWindow {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f197a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.internal.widget.bb.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      android.support.v7.internal.widget.bb.a(int, float):float
      android.support.v7.internal.widget.bb.a(int, int):int
      android.support.v7.internal.widget.bb.a(int, boolean):boolean */
    public u(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        bb a2 = bb.a(context, attributeSet, l.PopupWindow, i, 0);
        this.f197a = a2.a(l.PopupWindow_overlapAnchor, false);
        setBackgroundDrawable(a2.a(l.PopupWindow_android_popupBackground));
        a2.b();
    }

    public void showAsDropDown(View view, int i, int i2) {
        if (Build.VERSION.SDK_INT < 21 && this.f197a) {
            i2 -= view.getHeight();
        }
        super.showAsDropDown(view, i, i2);
    }

    @TargetApi(19)
    public void showAsDropDown(View view, int i, int i2, int i3) {
        if (Build.VERSION.SDK_INT < 21 && this.f197a) {
            i2 -= view.getHeight();
        }
        super.showAsDropDown(view, i, i2, i3);
    }

    public void update(View view, int i, int i2, int i3, int i4) {
        super.update(view, i, (Build.VERSION.SDK_INT >= 21 || !this.f197a) ? i2 : i2 - view.getHeight(), i3, i4);
    }
}
