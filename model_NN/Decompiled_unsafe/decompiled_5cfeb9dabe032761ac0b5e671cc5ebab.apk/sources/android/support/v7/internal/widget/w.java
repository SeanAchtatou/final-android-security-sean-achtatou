package android.support.v7.internal.widget;

import android.support.v7.internal.a.a;
import android.support.v7.internal.view.menu.y;
import android.view.Menu;

public interface w {
    void a(int i);

    void a(Menu menu, y yVar);

    boolean d();

    boolean e();

    boolean f();

    boolean g();

    boolean h();

    void i();

    void j();

    void setWindowCallback(a aVar);

    void setWindowTitle(CharSequence charSequence);
}
