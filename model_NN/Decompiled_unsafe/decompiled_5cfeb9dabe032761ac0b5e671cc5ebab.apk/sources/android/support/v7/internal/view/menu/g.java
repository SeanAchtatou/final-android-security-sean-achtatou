package android.support.v7.internal.view.menu;

import android.content.Context;
import android.os.IBinder;
import android.support.v7.a.i;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;

public class g implements x, AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    Context f136a;
    LayoutInflater b;
    i c;
    ExpandedMenuView d;
    int e;
    int f;
    h g;
    /* access modifiers changed from: private */
    public int h;
    private y i;

    public g(int i2, int i3) {
        this.f = i2;
        this.e = i3;
    }

    public g(Context context, int i2) {
        this(i2, 0);
        this.f136a = context;
        this.b = LayoutInflater.from(this.f136a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public z a(ViewGroup viewGroup) {
        if (this.d == null) {
            this.d = (ExpandedMenuView) this.b.inflate(i.abc_expanded_menu_layout, viewGroup, false);
            if (this.g == null) {
                this.g = new h(this);
            }
            this.d.setAdapter((ListAdapter) this.g);
            this.d.setOnItemClickListener(this);
        }
        return this.d;
    }

    public ListAdapter a() {
        if (this.g == null) {
            this.g = new h(this);
        }
        return this.g;
    }

    public void a(Context context, i iVar) {
        if (this.e != 0) {
            this.f136a = new ContextThemeWrapper(context, this.e);
            this.b = LayoutInflater.from(this.f136a);
        } else if (this.f136a != null) {
            this.f136a = context;
            if (this.b == null) {
                this.b = LayoutInflater.from(this.f136a);
            }
        }
        this.c = iVar;
        if (this.g != null) {
            this.g.notifyDataSetChanged();
        }
    }

    public void a(i iVar, boolean z) {
        if (this.i != null) {
            this.i.a(iVar, z);
        }
    }

    public void a(y yVar) {
        this.i = yVar;
    }

    public void a(boolean z) {
        if (this.g != null) {
            this.g.notifyDataSetChanged();
        }
    }

    public boolean a(ad adVar) {
        if (!adVar.hasVisibleItems()) {
            return false;
        }
        new l(adVar).a((IBinder) null);
        if (this.i != null) {
            this.i.a(adVar);
        }
        return true;
    }

    public boolean a(i iVar, m mVar) {
        return false;
    }

    public boolean b() {
        return false;
    }

    public boolean b(i iVar, m mVar) {
        return false;
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j) {
        this.c.a(this.g.getItem(i2), this, 0);
    }
}
