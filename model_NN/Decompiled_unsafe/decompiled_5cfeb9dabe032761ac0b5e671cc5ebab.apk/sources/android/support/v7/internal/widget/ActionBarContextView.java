package android.support.v7.internal.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.au;
import android.support.v4.view.cf;
import android.support.v4.view.cv;
import android.support.v7.a.b;
import android.support.v7.a.g;
import android.support.v7.a.i;
import android.support.v7.a.l;
import android.support.v7.b.a;
import android.support.v7.internal.view.h;
import android.support.v7.widget.ActionMenuPresenter;
import android.support.v7.widget.ActionMenuView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ActionBarContextView extends a implements cv {
    private CharSequence j;
    private CharSequence k;
    private View l;
    private View m;
    private LinearLayout n;
    private TextView o;
    private TextView p;
    private int q;
    private int r;
    private Drawable s;
    private boolean t;
    private int u;
    private h v;
    private boolean w;
    private int x;

    public ActionBarContextView(Context context) {
        this(context, null);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, b.actionModeStyle);
    }

    public ActionBarContextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        bb a2 = bb.a(context, attributeSet, l.ActionMode, i, 0);
        setBackgroundDrawable(a2.a(l.ActionMode_background));
        this.q = a2.f(l.ActionMode_titleTextStyle, 0);
        this.r = a2.f(l.ActionMode_subtitleTextStyle, 0);
        this.h = a2.e(l.ActionMode_height, 0);
        this.s = a2.a(l.ActionMode_backgroundSplit);
        this.u = a2.f(l.ActionMode_closeItemLayout, i.abc_action_mode_close_item_material);
        a2.b();
    }

    private void e() {
        int i = 8;
        boolean z = true;
        if (this.n == null) {
            LayoutInflater.from(getContext()).inflate(i.abc_action_bar_title_item, this);
            this.n = (LinearLayout) getChildAt(getChildCount() - 1);
            this.o = (TextView) this.n.findViewById(g.action_bar_title);
            this.p = (TextView) this.n.findViewById(g.action_bar_subtitle);
            if (this.q != 0) {
                this.o.setTextAppearance(getContext(), this.q);
            }
            if (this.r != 0) {
                this.p.setTextAppearance(getContext(), this.r);
            }
        }
        this.o.setText(this.j);
        this.p.setText(this.k);
        boolean z2 = !TextUtils.isEmpty(this.j);
        if (TextUtils.isEmpty(this.k)) {
            z = false;
        }
        this.p.setVisibility(z ? 0 : 8);
        LinearLayout linearLayout = this.n;
        if (z2 || z) {
            i = 0;
        }
        linearLayout.setVisibility(i);
        if (this.n.getParent() == null) {
            addView(this.n);
        }
    }

    private void f() {
        h hVar = this.v;
        if (hVar != null) {
            this.v = null;
            hVar.b();
        }
    }

    private h g() {
        int childCount;
        au.a(this.l, (float) ((-this.l.getWidth()) - ((ViewGroup.MarginLayoutParams) this.l.getLayoutParams()).leftMargin));
        cf b = au.i(this.l).b(0.0f);
        b.a(200L);
        b.a(this);
        b.a(new DecelerateInterpolator());
        h hVar = new h();
        hVar.a(b);
        if (this.c != null && (childCount = this.c.getChildCount()) > 0) {
            int i = childCount - 1;
            int i2 = 0;
            while (i >= 0) {
                View childAt = this.c.getChildAt(i);
                au.d(childAt, 0.0f);
                cf d = au.i(childAt).d(1.0f);
                d.a(300L);
                hVar.a(d);
                i--;
                i2++;
            }
        }
        return hVar;
    }

    private h h() {
        cf b = au.i(this.l).b((float) ((-this.l.getWidth()) - ((ViewGroup.MarginLayoutParams) this.l.getLayoutParams()).leftMargin));
        b.a(200L);
        b.a(this);
        b.a(new DecelerateInterpolator());
        h hVar = new h();
        hVar.a(b);
        if (this.c == null || this.c.getChildCount() > 0) {
        }
        return hVar;
    }

    public /* bridge */ /* synthetic */ void a(int i) {
        super.a(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.internal.widget.ActionBarContextView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ActionMenuPresenter.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.widget.ActionMenuPresenter.a(android.support.v7.widget.ActionMenuPresenter, android.support.v7.widget.b):android.support.v7.widget.b
      android.support.v7.widget.ActionMenuPresenter.a(android.support.v7.widget.ActionMenuPresenter, android.support.v7.widget.d):android.support.v7.widget.d
      android.support.v7.widget.ActionMenuPresenter.a(android.support.v7.widget.ActionMenuPresenter, android.support.v7.widget.g):android.support.v7.widget.g
      android.support.v7.widget.ActionMenuPresenter.a(android.content.Context, android.support.v7.internal.view.menu.i):void
      android.support.v7.widget.ActionMenuPresenter.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.widget.ActionMenuPresenter.a(android.support.v7.internal.view.menu.m, android.support.v7.internal.view.menu.aa):void
      android.support.v7.widget.ActionMenuPresenter.a(int, android.support.v7.internal.view.menu.m):boolean
      android.support.v7.widget.ActionMenuPresenter.a(android.view.ViewGroup, int):boolean
      android.support.v7.internal.view.menu.d.a(android.content.Context, android.support.v7.internal.view.menu.i):void
      android.support.v7.internal.view.menu.d.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.internal.view.menu.d.a(android.support.v7.internal.view.menu.m, android.support.v7.internal.view.menu.aa):void
      android.support.v7.internal.view.menu.d.a(android.view.View, int):void
      android.support.v7.internal.view.menu.d.a(int, android.support.v7.internal.view.menu.m):boolean
      android.support.v7.internal.view.menu.d.a(android.support.v7.internal.view.menu.i, android.support.v7.internal.view.menu.m):boolean
      android.support.v7.internal.view.menu.d.a(android.view.ViewGroup, int):boolean
      android.support.v7.internal.view.menu.x.a(android.content.Context, android.support.v7.internal.view.menu.i):void
      android.support.v7.internal.view.menu.x.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.internal.view.menu.x.a(android.support.v7.internal.view.menu.i, android.support.v7.internal.view.menu.m):boolean
      android.support.v7.widget.ActionMenuPresenter.a(int, boolean):void */
    public void a(a aVar) {
        if (this.l == null) {
            this.l = LayoutInflater.from(getContext()).inflate(this.u, (ViewGroup) this, false);
            addView(this.l);
        } else if (this.l.getParent() == null) {
            addView(this.l);
        }
        this.l.findViewById(g.action_mode_close_button).setOnClickListener(new g(this, aVar));
        android.support.v7.internal.view.menu.i iVar = (android.support.v7.internal.view.menu.i) aVar.b();
        if (this.d != null) {
            this.d.e();
        }
        this.d = new ActionMenuPresenter(getContext());
        this.d.b(true);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -1);
        if (!this.f) {
            iVar.a(this.d, this.b);
            this.c = (ActionMenuView) this.d.a(this);
            this.c.setBackgroundDrawable(null);
            addView(this.c, layoutParams);
        } else {
            this.d.a(getContext().getResources().getDisplayMetrics().widthPixels, true);
            this.d.b(Integer.MAX_VALUE);
            layoutParams.width = -1;
            layoutParams.height = this.h;
            iVar.a(this.d, this.b);
            this.c = (ActionMenuView) this.d.a(this);
            this.c.setBackgroundDrawable(this.s);
            this.e.addView(this.c, layoutParams);
        }
        this.w = true;
    }

    public void a(View view) {
    }

    public boolean a() {
        if (this.d != null) {
            return this.d.c();
        }
        return false;
    }

    public void b() {
        if (this.x != 2) {
            if (this.l == null) {
                c();
                return;
            }
            f();
            this.x = 2;
            this.v = h();
            this.v.a();
        }
    }

    public void b(View view) {
        if (this.x == 2) {
            c();
        }
        this.x = 0;
    }

    public void c() {
        f();
        removeAllViews();
        if (this.e != null) {
            this.e.removeView(this.c);
        }
        this.m = null;
        this.c = null;
        this.w = false;
    }

    public void c(View view) {
    }

    public boolean d() {
        return this.t;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ViewGroup.MarginLayoutParams(-1, -2);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new ViewGroup.MarginLayoutParams(getContext(), attributeSet);
    }

    public /* bridge */ /* synthetic */ int getAnimatedVisibility() {
        return super.getAnimatedVisibility();
    }

    public /* bridge */ /* synthetic */ int getContentHeight() {
        return super.getContentHeight();
    }

    public CharSequence getSubtitle() {
        return this.k;
    }

    public CharSequence getTitle() {
        return this.j;
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.d != null) {
            this.d.d();
            this.d.f();
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        if (Build.VERSION.SDK_INT < 14) {
            return;
        }
        if (accessibilityEvent.getEventType() == 32) {
            accessibilityEvent.setSource(this);
            accessibilityEvent.setClassName(getClass().getName());
            accessibilityEvent.setPackageName(getContext().getPackageName());
            accessibilityEvent.setContentDescription(this.j);
            return;
        }
        super.onInitializeAccessibilityEvent(accessibilityEvent);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        boolean a2 = bh.a(this);
        int paddingRight = a2 ? (i3 - i) - getPaddingRight() : getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingTop2 = ((i4 - i2) - getPaddingTop()) - getPaddingBottom();
        if (!(this.l == null || this.l.getVisibility() == 8)) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.l.getLayoutParams();
            int i5 = a2 ? marginLayoutParams.rightMargin : marginLayoutParams.leftMargin;
            int i6 = a2 ? marginLayoutParams.leftMargin : marginLayoutParams.rightMargin;
            int a3 = a(paddingRight, i5, a2);
            paddingRight = a(a(this.l, a3, paddingTop, paddingTop2, a2) + a3, i6, a2);
            if (this.w) {
                this.x = 1;
                this.v = g();
                this.v.a();
                this.w = false;
            }
        }
        int i7 = paddingRight;
        if (!(this.n == null || this.m != null || this.n.getVisibility() == 8)) {
            i7 += a(this.n, i7, paddingTop, paddingTop2, a2);
        }
        if (this.m != null) {
            int a4 = a(this.m, i7, paddingTop, paddingTop2, a2) + i7;
        }
        int paddingLeft = a2 ? getPaddingLeft() : (i3 - i) - getPaddingRight();
        if (this.c != null) {
            int a5 = a(this.c, paddingLeft, paddingTop, paddingTop2, !a2) + paddingLeft;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = 1073741824;
        int i4 = 0;
        if (View.MeasureSpec.getMode(i) != 1073741824) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with android:layout_width=\"match_parent\" (or fill_parent)");
        } else if (View.MeasureSpec.getMode(i2) == 0) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with android:layout_height=\"wrap_content\"");
        } else {
            int size = View.MeasureSpec.getSize(i);
            int size2 = this.h > 0 ? this.h : View.MeasureSpec.getSize(i2);
            int paddingTop = getPaddingTop() + getPaddingBottom();
            int paddingLeft = (size - getPaddingLeft()) - getPaddingRight();
            int i5 = size2 - paddingTop;
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE);
            if (this.l != null) {
                int a2 = a(this.l, paddingLeft, makeMeasureSpec, 0);
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.l.getLayoutParams();
                paddingLeft = a2 - (marginLayoutParams.rightMargin + marginLayoutParams.leftMargin);
            }
            if (this.c != null && this.c.getParent() == this) {
                paddingLeft = a(this.c, paddingLeft, makeMeasureSpec, 0);
            }
            if (this.n != null && this.m == null) {
                if (this.t) {
                    this.n.measure(View.MeasureSpec.makeMeasureSpec(0, 0), makeMeasureSpec);
                    int measuredWidth = this.n.getMeasuredWidth();
                    boolean z = measuredWidth <= paddingLeft;
                    if (z) {
                        paddingLeft -= measuredWidth;
                    }
                    this.n.setVisibility(z ? 0 : 8);
                } else {
                    paddingLeft = a(this.n, paddingLeft, makeMeasureSpec, 0);
                }
            }
            if (this.m != null) {
                ViewGroup.LayoutParams layoutParams = this.m.getLayoutParams();
                int i6 = layoutParams.width != -2 ? 1073741824 : Integer.MIN_VALUE;
                if (layoutParams.width >= 0) {
                    paddingLeft = Math.min(layoutParams.width, paddingLeft);
                }
                if (layoutParams.height == -2) {
                    i3 = Integer.MIN_VALUE;
                }
                this.m.measure(View.MeasureSpec.makeMeasureSpec(paddingLeft, i6), View.MeasureSpec.makeMeasureSpec(layoutParams.height >= 0 ? Math.min(layoutParams.height, i5) : i5, i3));
            }
            if (this.h <= 0) {
                int childCount = getChildCount();
                int i7 = 0;
                while (i4 < childCount) {
                    int measuredHeight = getChildAt(i4).getMeasuredHeight() + paddingTop;
                    if (measuredHeight <= i7) {
                        measuredHeight = i7;
                    }
                    i4++;
                    i7 = measuredHeight;
                }
                setMeasuredDimension(size, i7);
                return;
            }
            setMeasuredDimension(size, size2);
        }
    }

    public void setContentHeight(int i) {
        this.h = i;
    }

    public void setCustomView(View view) {
        if (this.m != null) {
            removeView(this.m);
        }
        this.m = view;
        if (this.n != null) {
            removeView(this.n);
            this.n = null;
        }
        if (view != null) {
            addView(view);
        }
        requestLayout();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ActionMenuPresenter.a(int, boolean):void
     arg types: [int, int]
     candidates:
      android.support.v7.widget.ActionMenuPresenter.a(android.support.v7.widget.ActionMenuPresenter, android.support.v7.widget.b):android.support.v7.widget.b
      android.support.v7.widget.ActionMenuPresenter.a(android.support.v7.widget.ActionMenuPresenter, android.support.v7.widget.d):android.support.v7.widget.d
      android.support.v7.widget.ActionMenuPresenter.a(android.support.v7.widget.ActionMenuPresenter, android.support.v7.widget.g):android.support.v7.widget.g
      android.support.v7.widget.ActionMenuPresenter.a(android.content.Context, android.support.v7.internal.view.menu.i):void
      android.support.v7.widget.ActionMenuPresenter.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.widget.ActionMenuPresenter.a(android.support.v7.internal.view.menu.m, android.support.v7.internal.view.menu.aa):void
      android.support.v7.widget.ActionMenuPresenter.a(int, android.support.v7.internal.view.menu.m):boolean
      android.support.v7.widget.ActionMenuPresenter.a(android.view.ViewGroup, int):boolean
      android.support.v7.internal.view.menu.d.a(android.content.Context, android.support.v7.internal.view.menu.i):void
      android.support.v7.internal.view.menu.d.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.internal.view.menu.d.a(android.support.v7.internal.view.menu.m, android.support.v7.internal.view.menu.aa):void
      android.support.v7.internal.view.menu.d.a(android.view.View, int):void
      android.support.v7.internal.view.menu.d.a(int, android.support.v7.internal.view.menu.m):boolean
      android.support.v7.internal.view.menu.d.a(android.support.v7.internal.view.menu.i, android.support.v7.internal.view.menu.m):boolean
      android.support.v7.internal.view.menu.d.a(android.view.ViewGroup, int):boolean
      android.support.v7.internal.view.menu.x.a(android.content.Context, android.support.v7.internal.view.menu.i):void
      android.support.v7.internal.view.menu.x.a(android.support.v7.internal.view.menu.i, boolean):void
      android.support.v7.internal.view.menu.x.a(android.support.v7.internal.view.menu.i, android.support.v7.internal.view.menu.m):boolean
      android.support.v7.widget.ActionMenuPresenter.a(int, boolean):void */
    public void setSplitToolbar(boolean z) {
        if (this.f != z) {
            if (this.d != null) {
                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -1);
                if (!z) {
                    this.c = (ActionMenuView) this.d.a(this);
                    this.c.setBackgroundDrawable(null);
                    ViewGroup viewGroup = (ViewGroup) this.c.getParent();
                    if (viewGroup != null) {
                        viewGroup.removeView(this.c);
                    }
                    addView(this.c, layoutParams);
                } else {
                    this.d.a(getContext().getResources().getDisplayMetrics().widthPixels, true);
                    this.d.b(Integer.MAX_VALUE);
                    layoutParams.width = -1;
                    layoutParams.height = this.h;
                    this.c = (ActionMenuView) this.d.a(this);
                    this.c.setBackgroundDrawable(this.s);
                    ViewGroup viewGroup2 = (ViewGroup) this.c.getParent();
                    if (viewGroup2 != null) {
                        viewGroup2.removeView(this.c);
                    }
                    this.e.addView(this.c, layoutParams);
                }
            }
            super.setSplitToolbar(z);
        }
    }

    public /* bridge */ /* synthetic */ void setSplitView(ViewGroup viewGroup) {
        super.setSplitView(viewGroup);
    }

    public /* bridge */ /* synthetic */ void setSplitWhenNarrow(boolean z) {
        super.setSplitWhenNarrow(z);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.k = charSequence;
        e();
    }

    public void setTitle(CharSequence charSequence) {
        this.j = charSequence;
        e();
    }

    public void setTitleOptional(boolean z) {
        if (z != this.t) {
            requestLayout();
        }
        this.t = z;
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }
}
