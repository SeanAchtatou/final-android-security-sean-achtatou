package android.support.v7.internal.a;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.app.o;
import android.support.v4.view.au;
import android.support.v4.view.cf;
import android.support.v4.view.cv;
import android.support.v4.view.cx;
import android.support.v7.a.g;
import android.support.v7.app.a;
import android.support.v7.app.c;
import android.support.v7.app.e;
import android.support.v7.internal.view.h;
import android.support.v7.internal.widget.ActionBarContainer;
import android.support.v7.internal.widget.ActionBarContextView;
import android.support.v7.internal.widget.ActionBarOverlayLayout;
import android.support.v7.internal.widget.af;
import android.support.v7.internal.widget.l;
import android.support.v7.internal.widget.x;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.animation.AnimationUtils;
import java.util.ArrayList;

public class b extends a implements l {
    static final /* synthetic */ boolean h = (!b.class.desiredAssertionStatus());
    private static final boolean i;
    private int A = 0;
    /* access modifiers changed from: private */
    public boolean B = true;
    /* access modifiers changed from: private */
    public boolean C;
    /* access modifiers changed from: private */
    public boolean D;
    private boolean E;
    private boolean F = true;
    /* access modifiers changed from: private */
    public h G;
    private boolean H;

    /* renamed from: a  reason: collision with root package name */
    f f114a;
    android.support.v7.b.a b;
    android.support.v7.b.b c;
    boolean d;
    final cv e = new c(this);
    final cv f = new d(this);
    final cx g = new e(this);
    /* access modifiers changed from: private */
    public Context j;
    private Context k;
    private o l;
    /* access modifiers changed from: private */
    public ActionBarOverlayLayout m;
    /* access modifiers changed from: private */
    public ActionBarContainer n;
    /* access modifiers changed from: private */
    public x o;
    /* access modifiers changed from: private */
    public ActionBarContextView p;
    /* access modifiers changed from: private */
    public ActionBarContainer q;
    /* access modifiers changed from: private */
    public View r;
    private af s;
    private ArrayList t = new ArrayList();
    private int u = -1;
    private boolean v;
    private boolean w;
    private ArrayList x = new ArrayList();
    /* access modifiers changed from: private */
    public int y;
    private boolean z;

    static {
        boolean z2 = true;
        if (Build.VERSION.SDK_INT < 14) {
            z2 = false;
        }
        i = z2;
    }

    public b(e eVar, boolean z2) {
        this.l = eVar;
        View decorView = eVar.getWindow().getDecorView();
        a(decorView);
        if (!z2) {
            this.r = decorView.findViewById(16908290);
        }
    }

    private void a(View view) {
        this.m = (ActionBarOverlayLayout) view.findViewById(g.decor_content_parent);
        if (this.m != null) {
            this.m.setActionBarVisibilityCallback(this);
        }
        this.o = b(view.findViewById(g.action_bar));
        this.p = (ActionBarContextView) view.findViewById(g.action_context_bar);
        this.n = (ActionBarContainer) view.findViewById(g.action_bar_container);
        this.q = (ActionBarContainer) view.findViewById(g.split_action_bar);
        if (this.o == null || this.p == null || this.n == null) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with a compatible window decor layout");
        }
        this.j = this.o.b();
        this.y = this.o.c() ? 1 : 0;
        boolean z2 = (this.o.p() & 4) != 0;
        if (z2) {
            this.v = true;
        }
        android.support.v7.internal.view.a a2 = android.support.v7.internal.view.a.a(this.j);
        a(a2.f() || z2);
        k(a2.d());
        TypedArray obtainStyledAttributes = this.j.obtainStyledAttributes(null, android.support.v7.a.l.ActionBar, android.support.v7.a.b.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(android.support.v7.a.l.ActionBar_hideOnContentScroll, false)) {
            b(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(android.support.v7.a.l.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            a((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    private x b(View view) {
        if (view instanceof x) {
            return (x) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        throw new IllegalStateException("Can't make a decor toolbar out of " + view.getClass().getSimpleName());
    }

    /* access modifiers changed from: private */
    public static boolean b(boolean z2, boolean z3, boolean z4) {
        if (z4) {
            return true;
        }
        return !z2 && !z3;
    }

    private void k() {
        if (!this.E) {
            this.E = true;
            if (this.m != null) {
                this.m.setShowingForActionMode(true);
            }
            l(false);
        }
    }

    private void k(boolean z2) {
        boolean z3 = true;
        this.z = z2;
        if (!this.z) {
            this.o.a((af) null);
            this.n.setTabContainer(this.s);
        } else {
            this.n.setTabContainer(null);
            this.o.a(this.s);
        }
        boolean z4 = f() == 2;
        if (this.s != null) {
            if (z4) {
                this.s.setVisibility(0);
                if (this.m != null) {
                    au.k(this.m);
                }
            } else {
                this.s.setVisibility(8);
            }
        }
        this.o.a(!this.z && z4);
        ActionBarOverlayLayout actionBarOverlayLayout = this.m;
        if (this.z || !z4) {
            z3 = false;
        }
        actionBarOverlayLayout.setHasNonEmbeddedTabs(z3);
    }

    private void l() {
        if (this.E) {
            this.E = false;
            if (this.m != null) {
                this.m.setShowingForActionMode(false);
            }
            l(false);
        }
    }

    private void l(boolean z2) {
        if (b(this.C, this.D, this.E)) {
            if (!this.F) {
                this.F = true;
                h(z2);
            }
        } else if (this.F) {
            this.F = false;
            i(z2);
        }
    }

    public int a() {
        return this.o.p();
    }

    public android.support.v7.b.a a(android.support.v7.b.b bVar) {
        if (this.f114a != null) {
            this.f114a.c();
        }
        this.m.setHideOnContentScrollEnabled(false);
        this.p.c();
        f fVar = new f(this, bVar);
        if (!fVar.e()) {
            return null;
        }
        fVar.d();
        this.p.a(fVar);
        j(true);
        if (!(this.q == null || this.y != 1 || this.q.getVisibility() == 0)) {
            this.q.setVisibility(0);
            if (this.m != null) {
                au.k(this.m);
            }
        }
        this.p.sendAccessibilityEvent(32);
        this.f114a = fVar;
        return fVar;
    }

    public void a(float f2) {
        au.e(this.n, f2);
        if (this.q != null) {
            au.e(this.q, f2);
        }
    }

    public void a(int i2) {
        this.A = i2;
    }

    public void a(int i2, int i3) {
        int p2 = this.o.p();
        if ((i3 & 4) != 0) {
            this.v = true;
        }
        this.o.c((p2 & (i3 ^ -1)) | (i2 & i3));
    }

    public void a(Configuration configuration) {
        k(android.support.v7.internal.view.a.a(this.j).d());
    }

    public void a(CharSequence charSequence) {
        this.o.a(charSequence);
    }

    public void a(boolean z2) {
        this.o.b(z2);
    }

    public Context b() {
        if (this.k == null) {
            TypedValue typedValue = new TypedValue();
            this.j.getTheme().resolveAttribute(android.support.v7.a.b.actionBarWidgetTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                this.k = new ContextThemeWrapper(this.j, i2);
            } else {
                this.k = this.j;
            }
        }
        return this.k;
    }

    public void b(boolean z2) {
        if (!z2 || this.m.a()) {
            this.d = z2;
            this.m.setHideOnContentScrollEnabled(z2);
            return;
        }
        throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }

    public void c(boolean z2) {
        if (!this.v) {
            f(z2);
        }
    }

    public void d(boolean z2) {
        this.H = z2;
        if (!z2 && this.G != null) {
            this.G.b();
        }
    }

    public boolean d() {
        if (this.o == null || !this.o.d()) {
            return false;
        }
        this.o.e();
        return true;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (this.c != null) {
            this.c.a(this.b);
            this.b = null;
            this.c = null;
        }
    }

    public void e(boolean z2) {
        if (z2 != this.w) {
            this.w = z2;
            int size = this.x.size();
            for (int i2 = 0; i2 < size; i2++) {
                ((c) this.x.get(i2)).a(z2);
            }
        }
    }

    public int f() {
        return this.o.q();
    }

    public void f(boolean z2) {
        a(z2 ? 4 : 0, 4);
    }

    public void g() {
        if (this.D) {
            this.D = false;
            l(true);
        }
    }

    public void g(boolean z2) {
        this.B = z2;
    }

    public void h() {
        if (!this.D) {
            this.D = true;
            l(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.au.b(android.view.View, float):void
     arg types: [android.support.v7.internal.widget.ActionBarContainer, int]
     candidates:
      android.support.v4.view.au.b(android.view.View, int):void
      android.support.v4.view.au.b(android.view.View, float):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.au.b(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.view.au.b(android.view.View, int):void
      android.support.v4.view.au.b(android.view.View, float):void */
    public void h(boolean z2) {
        if (this.G != null) {
            this.G.b();
        }
        this.n.setVisibility(0);
        if (this.A != 0 || !i || (!this.H && !z2)) {
            au.c(this.n, 1.0f);
            au.b((View) this.n, 0.0f);
            if (this.B && this.r != null) {
                au.b(this.r, 0.0f);
            }
            if (this.q != null && this.y == 1) {
                au.c(this.q, 1.0f);
                au.b((View) this.q, 0.0f);
                this.q.setVisibility(0);
            }
            this.f.b(null);
        } else {
            au.b((View) this.n, 0.0f);
            float f2 = (float) (-this.n.getHeight());
            if (z2) {
                int[] iArr = {0, 0};
                this.n.getLocationInWindow(iArr);
                f2 -= (float) iArr[1];
            }
            au.b(this.n, f2);
            h hVar = new h();
            cf c2 = au.i(this.n).c(0.0f);
            c2.a(this.g);
            hVar.a(c2);
            if (this.B && this.r != null) {
                au.b(this.r, f2);
                hVar.a(au.i(this.r).c(0.0f));
            }
            if (this.q != null && this.y == 1) {
                au.b(this.q, (float) this.q.getHeight());
                this.q.setVisibility(0);
                hVar.a(au.i(this.q).c(0.0f));
            }
            hVar.a(AnimationUtils.loadInterpolator(this.j, 17432582));
            hVar.a(250);
            hVar.a(this.f);
            this.G = hVar;
            hVar.a();
        }
        if (this.m != null) {
            au.k(this.m);
        }
    }

    public void i() {
        if (this.G != null) {
            this.G.b();
            this.G = null;
        }
    }

    public void i(boolean z2) {
        if (this.G != null) {
            this.G.b();
        }
        if (this.A != 0 || !i || (!this.H && !z2)) {
            this.e.b(null);
            return;
        }
        au.c(this.n, 1.0f);
        this.n.setTransitioning(true);
        h hVar = new h();
        float f2 = (float) (-this.n.getHeight());
        if (z2) {
            int[] iArr = {0, 0};
            this.n.getLocationInWindow(iArr);
            f2 -= (float) iArr[1];
        }
        cf c2 = au.i(this.n).c(f2);
        c2.a(this.g);
        hVar.a(c2);
        if (this.B && this.r != null) {
            hVar.a(au.i(this.r).c(f2));
        }
        if (this.q != null && this.q.getVisibility() == 0) {
            au.c(this.q, 1.0f);
            hVar.a(au.i(this.q).c((float) this.q.getHeight()));
        }
        hVar.a(AnimationUtils.loadInterpolator(this.j, 17432581));
        hVar.a(250);
        hVar.a(this.e);
        this.G = hVar;
        hVar.a();
    }

    public void j() {
    }

    public void j(boolean z2) {
        int i2 = 0;
        if (z2) {
            k();
        } else {
            l();
        }
        this.o.d(z2 ? 8 : 0);
        ActionBarContextView actionBarContextView = this.p;
        if (!z2) {
            i2 = 8;
        }
        actionBarContextView.a(i2);
    }
}
