package android.support.v7.internal.view.menu;

import android.support.v7.widget.q;
import android.support.v7.widget.v;

class b extends v {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ActionMenuItemView f133a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(ActionMenuItemView actionMenuItemView) {
        super(actionMenuItemView);
        this.f133a = actionMenuItemView;
    }

    public q a() {
        if (this.f133a.f != null) {
            return this.f133a.f.a();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        q a2;
        return this.f133a.d != null && this.f133a.d.a(this.f133a.f129a) && (a2 = a()) != null && a2.b();
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        q a2 = a();
        if (a2 == null) {
            return false;
        }
        a2.a();
        return true;
    }
}
