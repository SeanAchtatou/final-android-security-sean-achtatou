package android.support.v7.internal.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

public class av extends EditText {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f175a = {16842964};

    public av(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842862);
    }

    public av(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        bb a2 = bb.a(context, attributeSet, f175a, i, 0);
        setBackgroundDrawable(a2.a(0));
        a2.b();
    }
}
