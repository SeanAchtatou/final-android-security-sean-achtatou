package android.support.v7.internal.widget;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.internal.widget.AbsSpinnerCompat;

final class d implements Parcelable.Creator {
    d() {
    }

    /* renamed from: a */
    public AbsSpinnerCompat.SavedState createFromParcel(Parcel parcel) {
        return new AbsSpinnerCompat.SavedState(parcel);
    }

    /* renamed from: a */
    public AbsSpinnerCompat.SavedState[] newArray(int i) {
        return new AbsSpinnerCompat.SavedState[i];
    }
}
