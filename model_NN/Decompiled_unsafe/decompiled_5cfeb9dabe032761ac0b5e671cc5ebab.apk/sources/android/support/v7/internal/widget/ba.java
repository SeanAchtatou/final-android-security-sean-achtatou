package android.support.v7.internal.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ListPopupWindow;
import android.widget.Spinner;
import java.lang.reflect.Field;

public class ba extends Spinner {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f180a = {16842964, 16843126};

    public ba(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842881);
    }

    public ba(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        bb a2 = bb.a(context, attributeSet, f180a, i, 0);
        setBackgroundDrawable(a2.a(0));
        if (a2.d(1)) {
            Drawable a3 = a2.a(1);
            if (Build.VERSION.SDK_INT >= 16) {
                setPopupBackgroundDrawable(a3);
            } else if (Build.VERSION.SDK_INT >= 11) {
                a(this, a3);
            }
        }
        a2.b();
    }

    @TargetApi(11)
    private static void a(Spinner spinner, Drawable drawable) {
        try {
            Field declaredField = Spinner.class.getDeclaredField("mPopup");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(spinner);
            if (obj instanceof ListPopupWindow) {
                ((ListPopupWindow) obj).setBackgroundDrawable(drawable);
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
    }
}
