package android.support.v7.internal.widget;

import android.view.ViewTreeObserver;

class al implements ViewTreeObserver.OnGlobalLayoutListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SpinnerCompat f167a;

    al(SpinnerCompat spinnerCompat) {
        this.f167a = spinnerCompat;
    }

    public void onGlobalLayout() {
        if (!this.f167a.G.b()) {
            this.f167a.G.c();
        }
        ViewTreeObserver viewTreeObserver = this.f167a.getViewTreeObserver();
        if (viewTreeObserver != null) {
            viewTreeObserver.removeGlobalOnLayoutListener(this);
        }
    }
}
