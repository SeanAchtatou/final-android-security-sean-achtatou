package android.support.v7.widget;

import android.content.Context;
import android.support.v7.b.c;
import android.support.v7.internal.view.menu.ad;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.m;
import android.support.v7.internal.view.menu.x;

class am implements x {

    /* renamed from: a  reason: collision with root package name */
    i f214a;
    m b;
    final /* synthetic */ Toolbar c;

    private am(Toolbar toolbar) {
        this.c = toolbar;
    }

    /* synthetic */ am(Toolbar toolbar, aj ajVar) {
        this(toolbar);
    }

    public void a(Context context, i iVar) {
        if (!(this.f214a == null || this.b == null)) {
            this.f214a.d(this.b);
        }
        this.f214a = iVar;
    }

    public void a(i iVar, boolean z) {
    }

    public void a(boolean z) {
        boolean z2 = false;
        if (this.b != null) {
            if (this.f214a != null) {
                int size = this.f214a.size();
                int i = 0;
                while (true) {
                    if (i >= size) {
                        break;
                    } else if (this.f214a.getItem(i) == this.b) {
                        z2 = true;
                        break;
                    } else {
                        i++;
                    }
                }
            }
            if (!z2) {
                b(this.f214a, this.b);
            }
        }
    }

    public boolean a(ad adVar) {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.Toolbar.a(android.support.v7.widget.Toolbar, boolean):void
     arg types: [android.support.v7.widget.Toolbar, int]
     candidates:
      android.support.v7.widget.Toolbar.a(android.view.View, int):int
      android.support.v7.widget.Toolbar.a(java.util.List, int[]):int
      android.support.v7.widget.Toolbar.a(java.util.List, int):void
      android.support.v7.widget.Toolbar.a(int, int):void
      android.support.v7.widget.Toolbar.a(android.content.Context, int):void
      android.support.v7.widget.Toolbar.a(android.support.v7.internal.view.menu.i, android.support.v7.widget.ActionMenuPresenter):void
      android.support.v7.widget.Toolbar.a(android.support.v7.widget.Toolbar, boolean):void */
    public boolean a(i iVar, m mVar) {
        this.c.n();
        if (this.c.i.getParent() != this.c) {
            this.c.addView(this.c.i);
        }
        this.c.f204a = mVar.getActionView();
        this.b = mVar;
        if (this.c.f204a.getParent() != this.c) {
            an i = this.c.generateDefaultLayoutParams();
            i.f103a = 8388611 | (this.c.n & 112);
            i.b = 2;
            this.c.f204a.setLayoutParams(i);
            this.c.addView(this.c.f204a);
        }
        this.c.setChildVisibilityForExpandedActionView(true);
        this.c.requestLayout();
        mVar.e(true);
        if (this.c.f204a instanceof c) {
            ((c) this.c.f204a).a();
        }
        return true;
    }

    public boolean b() {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.Toolbar.a(android.support.v7.widget.Toolbar, boolean):void
     arg types: [android.support.v7.widget.Toolbar, int]
     candidates:
      android.support.v7.widget.Toolbar.a(android.view.View, int):int
      android.support.v7.widget.Toolbar.a(java.util.List, int[]):int
      android.support.v7.widget.Toolbar.a(java.util.List, int):void
      android.support.v7.widget.Toolbar.a(int, int):void
      android.support.v7.widget.Toolbar.a(android.content.Context, int):void
      android.support.v7.widget.Toolbar.a(android.support.v7.internal.view.menu.i, android.support.v7.widget.ActionMenuPresenter):void
      android.support.v7.widget.Toolbar.a(android.support.v7.widget.Toolbar, boolean):void */
    public boolean b(i iVar, m mVar) {
        if (this.c.f204a instanceof c) {
            ((c) this.c.f204a).b();
        }
        this.c.removeView(this.c.f204a);
        this.c.removeView(this.c.i);
        this.c.f204a = null;
        this.c.setChildVisibilityForExpandedActionView(false);
        this.b = null;
        this.c.requestLayout();
        mVar.e(false);
        return true;
    }
}
