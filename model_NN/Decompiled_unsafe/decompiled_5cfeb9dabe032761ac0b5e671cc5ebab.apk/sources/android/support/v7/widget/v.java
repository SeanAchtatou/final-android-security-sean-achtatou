package android.support.v7.widget;

import android.os.SystemClock;
import android.support.v4.view.ai;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

public abstract class v implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private final float f227a;
    private final int b;
    private final int c;
    /* access modifiers changed from: private */
    public final View d;
    private Runnable e;
    private Runnable f;
    private boolean g;
    private boolean h;
    private int i;
    private final int[] j = new int[2];

    public v(View view) {
        this.d = view;
        this.f227a = (float) ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
        this.b = ViewConfiguration.getTapTimeout();
        this.c = (this.b + ViewConfiguration.getLongPressTimeout()) / 2;
    }

    private boolean a(MotionEvent motionEvent) {
        View view = this.d;
        if (!view.isEnabled()) {
            return false;
        }
        switch (ai.a(motionEvent)) {
            case 0:
                this.i = motionEvent.getPointerId(0);
                this.h = false;
                if (this.e == null) {
                    this.e = new w(this, null);
                }
                view.postDelayed(this.e, (long) this.b);
                if (this.f == null) {
                    this.f = new x(this, null);
                }
                view.postDelayed(this.f, (long) this.c);
                return false;
            case 1:
            case 3:
                d();
                return false;
            case 2:
                int findPointerIndex = motionEvent.findPointerIndex(this.i);
                if (findPointerIndex < 0 || a(view, motionEvent.getX(findPointerIndex), motionEvent.getY(findPointerIndex), this.f227a)) {
                    return false;
                }
                d();
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return true;
            default:
                return false;
        }
    }

    private static boolean a(View view, float f2, float f3, float f4) {
        return f2 >= (-f4) && f3 >= (-f4) && f2 < ((float) (view.getRight() - view.getLeft())) + f4 && f3 < ((float) (view.getBottom() - view.getTop())) + f4;
    }

    private boolean a(View view, MotionEvent motionEvent) {
        int[] iArr = this.j;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) (-iArr[0]), (float) (-iArr[1]));
        return true;
    }

    private boolean b(MotionEvent motionEvent) {
        u a2;
        boolean z = true;
        View view = this.d;
        q a3 = a();
        if (a3 == null || !a3.b() || (a2 = a3.f) == null || !a2.isShown()) {
            return false;
        }
        MotionEvent obtainNoHistory = MotionEvent.obtainNoHistory(motionEvent);
        b(view, obtainNoHistory);
        a(a2, obtainNoHistory);
        boolean a4 = a2.a(obtainNoHistory, this.i);
        obtainNoHistory.recycle();
        int a5 = ai.a(motionEvent);
        boolean z2 = (a5 == 1 || a5 == 3) ? false : true;
        if (!a4 || !z2) {
            z = false;
        }
        return z;
    }

    private boolean b(View view, MotionEvent motionEvent) {
        int[] iArr = this.j;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) iArr[0], (float) iArr[1]);
        return true;
    }

    private void d() {
        if (this.f != null) {
            this.d.removeCallbacks(this.f);
        }
        if (this.e != null) {
            this.d.removeCallbacks(this.e);
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        d();
        if (this.d.isEnabled() && b()) {
            this.d.getParent().requestDisallowInterceptTouchEvent(true);
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
            this.d.onTouchEvent(obtain);
            obtain.recycle();
            this.g = true;
            this.h = true;
        }
    }

    public abstract q a();

    /* access modifiers changed from: protected */
    public boolean b() {
        q a2 = a();
        if (a2 == null || a2.b()) {
            return true;
        }
        a2.c();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        q a2 = a();
        if (a2 == null || !a2.b()) {
            return true;
        }
        a2.a();
        return true;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean z;
        boolean z2 = this.g;
        if (z2) {
            z = this.h ? b(motionEvent) : b(motionEvent) || !c();
        } else {
            boolean z3 = a(motionEvent) && b();
            if (z3) {
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                this.d.onTouchEvent(obtain);
                obtain.recycle();
            }
            z = z3;
        }
        this.g = z;
        return z || z2;
    }
}
