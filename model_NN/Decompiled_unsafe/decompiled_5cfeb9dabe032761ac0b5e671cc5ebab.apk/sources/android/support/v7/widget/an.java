package android.support.v7.widget;

import android.content.Context;
import android.support.v7.app.b;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class an extends b {
    int b = 0;

    public an(int i, int i2) {
        super(i, i2);
        this.f103a = 8388627;
    }

    public an(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public an(b bVar) {
        super(bVar);
    }

    public an(an anVar) {
        super((b) anVar);
        this.b = anVar.b;
    }

    public an(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public an(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
        a(marginLayoutParams);
    }

    /* access modifiers changed from: package-private */
    public void a(ViewGroup.MarginLayoutParams marginLayoutParams) {
        this.leftMargin = marginLayoutParams.leftMargin;
        this.topMargin = marginLayoutParams.topMargin;
        this.rightMargin = marginLayoutParams.rightMargin;
        this.bottomMargin = marginLayoutParams.bottomMargin;
    }
}
