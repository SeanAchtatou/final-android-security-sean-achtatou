package android.support.v7.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewDebug;
import android.view.ViewGroup;

public class m extends p {
    @ViewDebug.ExportedProperty

    /* renamed from: a  reason: collision with root package name */
    public boolean f221a;
    @ViewDebug.ExportedProperty
    public int b;
    @ViewDebug.ExportedProperty
    public int c;
    @ViewDebug.ExportedProperty
    public boolean d;
    @ViewDebug.ExportedProperty
    public boolean e;
    boolean f;

    public m(int i, int i2) {
        super(i, i2);
        this.f221a = false;
    }

    public m(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public m(m mVar) {
        super(mVar);
        this.f221a = mVar.f221a;
    }

    public m(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }
}
