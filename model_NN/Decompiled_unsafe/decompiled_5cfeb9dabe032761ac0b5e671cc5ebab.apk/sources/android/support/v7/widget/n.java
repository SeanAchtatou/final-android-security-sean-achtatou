package android.support.v7.widget;

import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.j;
import android.view.MenuItem;

class n implements j {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ActionMenuView f222a;

    private n(ActionMenuView actionMenuView) {
        this.f222a = actionMenuView;
    }

    public void a(i iVar) {
        if (this.f222a.h != null) {
            this.f222a.h.a(iVar);
        }
    }

    public boolean a(i iVar, MenuItem menuItem) {
        return this.f222a.m != null && this.f222a.m.a(menuItem);
    }
}
