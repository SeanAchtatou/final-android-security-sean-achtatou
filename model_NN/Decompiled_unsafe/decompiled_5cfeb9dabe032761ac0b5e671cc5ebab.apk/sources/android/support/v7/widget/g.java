package android.support.v7.widget;

import android.content.Context;
import android.support.v7.a.b;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.v;
import android.view.View;

class g extends v {
    final /* synthetic */ ActionMenuPresenter c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g(ActionMenuPresenter actionMenuPresenter, Context context, i iVar, View view, boolean z) {
        super(context, iVar, view, z, b.actionOverflowMenuStyle);
        this.c = actionMenuPresenter;
        a(8388613);
        a(actionMenuPresenter.g);
    }

    public void onDismiss() {
        super.onDismiss();
        this.c.c.close();
        g unused = this.c.v = (g) null;
    }
}
