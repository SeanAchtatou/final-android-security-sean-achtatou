package android.support.v7.widget;

import android.os.ResultReceiver;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import java.lang.reflect.Method;

class ad {

    /* renamed from: a  reason: collision with root package name */
    private Method f209a;
    private Method b;
    private Method c;
    private Method d;

    ad() {
        try {
            this.f209a = AutoCompleteTextView.class.getDeclaredMethod("doBeforeTextChanged", new Class[0]);
            this.f209a.setAccessible(true);
        } catch (NoSuchMethodException e) {
        }
        try {
            this.b = AutoCompleteTextView.class.getDeclaredMethod("doAfterTextChanged", new Class[0]);
            this.b.setAccessible(true);
        } catch (NoSuchMethodException e2) {
        }
        Class<AutoCompleteTextView> cls = AutoCompleteTextView.class;
        try {
            this.c = cls.getMethod("ensureImeVisible", Boolean.TYPE);
            this.c.setAccessible(true);
        } catch (NoSuchMethodException e3) {
        }
        Class<InputMethodManager> cls2 = InputMethodManager.class;
        try {
            this.d = cls2.getMethod("showSoftInputUnchecked", Integer.TYPE, ResultReceiver.class);
            this.d.setAccessible(true);
        } catch (NoSuchMethodException e4) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(AutoCompleteTextView autoCompleteTextView) {
        if (this.f209a != null) {
            try {
                this.f209a.invoke(autoCompleteTextView, new Object[0]);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(AutoCompleteTextView autoCompleteTextView, boolean z) {
        if (this.c != null) {
            try {
                this.c.invoke(autoCompleteTextView, Boolean.valueOf(z));
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(AutoCompleteTextView autoCompleteTextView) {
        if (this.b != null) {
            try {
                this.b.invoke(autoCompleteTextView, new Object[0]);
            } catch (Exception e) {
            }
        }
    }
}
