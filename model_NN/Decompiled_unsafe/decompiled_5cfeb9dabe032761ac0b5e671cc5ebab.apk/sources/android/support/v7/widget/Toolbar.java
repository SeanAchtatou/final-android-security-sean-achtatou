package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.aa;
import android.support.v4.view.ai;
import android.support.v4.view.au;
import android.support.v4.view.v;
import android.support.v7.a.b;
import android.support.v7.a.l;
import android.support.v7.internal.view.e;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.j;
import android.support.v7.internal.view.menu.m;
import android.support.v7.internal.view.menu.y;
import android.support.v7.internal.widget.ae;
import android.support.v7.internal.widget.aw;
import android.support.v7.internal.widget.bb;
import android.support.v7.internal.widget.bc;
import android.support.v7.internal.widget.bh;
import android.support.v7.internal.widget.x;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class Toolbar extends ViewGroup {
    private final ArrayList A;
    private final int[] B;
    /* access modifiers changed from: private */
    public ao C;
    private final o D;
    private bc E;
    private ActionMenuPresenter F;
    private am G;
    private y H;
    private j I;
    private boolean J;
    private int K;
    private final Runnable L;
    private final aw M;

    /* renamed from: a  reason: collision with root package name */
    View f204a;
    private ActionMenuView b;
    private TextView c;
    private TextView d;
    private ImageButton e;
    private ImageView f;
    private Drawable g;
    private CharSequence h;
    /* access modifiers changed from: private */
    public ImageButton i;
    private Context j;
    private int k;
    private int l;
    private int m;
    /* access modifiers changed from: private */
    public int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private final ae t;
    private int u;
    private CharSequence v;
    private CharSequence w;
    private int x;
    private int y;
    private boolean z;

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new ap();

        /* renamed from: a  reason: collision with root package name */
        public int f205a;
        public boolean b;

        public SavedState(Parcel parcel) {
            super(parcel);
            this.f205a = parcel.readInt();
            this.b = parcel.readInt() != 0;
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f205a);
            parcel.writeInt(this.b ? 1 : 0);
        }
    }

    public Toolbar(Context context) {
        this(context, null);
    }

    public Toolbar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, b.toolbarStyle);
    }

    public Toolbar(Context context, AttributeSet attributeSet, int i2) {
        super(a(context, attributeSet, i2), attributeSet, i2);
        this.t = new ae();
        this.u = 8388627;
        this.A = new ArrayList();
        this.B = new int[2];
        this.D = new aj(this);
        this.L = new ak(this);
        bb a2 = bb.a(getContext(), attributeSet, l.Toolbar, i2, 0);
        this.l = a2.f(l.Toolbar_titleTextAppearance, 0);
        this.m = a2.f(l.Toolbar_subtitleTextAppearance, 0);
        this.u = a2.b(l.Toolbar_android_gravity, this.u);
        this.n = 48;
        int c2 = a2.c(l.Toolbar_titleMargins, 0);
        this.s = c2;
        this.r = c2;
        this.q = c2;
        this.p = c2;
        int c3 = a2.c(l.Toolbar_titleMarginStart, -1);
        if (c3 >= 0) {
            this.p = c3;
        }
        int c4 = a2.c(l.Toolbar_titleMarginEnd, -1);
        if (c4 >= 0) {
            this.q = c4;
        }
        int c5 = a2.c(l.Toolbar_titleMarginTop, -1);
        if (c5 >= 0) {
            this.r = c5;
        }
        int c6 = a2.c(l.Toolbar_titleMarginBottom, -1);
        if (c6 >= 0) {
            this.s = c6;
        }
        this.o = a2.d(l.Toolbar_maxButtonHeight, -1);
        int c7 = a2.c(l.Toolbar_contentInsetStart, Integer.MIN_VALUE);
        int c8 = a2.c(l.Toolbar_contentInsetEnd, Integer.MIN_VALUE);
        this.t.b(a2.d(l.Toolbar_contentInsetLeft, 0), a2.d(l.Toolbar_contentInsetRight, 0));
        if (!(c7 == Integer.MIN_VALUE && c8 == Integer.MIN_VALUE)) {
            this.t.a(c7, c8);
        }
        this.g = a2.a(l.Toolbar_collapseIcon);
        this.h = a2.b(l.Toolbar_collapseContentDescription);
        CharSequence b2 = a2.b(l.Toolbar_title);
        if (!TextUtils.isEmpty(b2)) {
            setTitle(b2);
        }
        CharSequence b3 = a2.b(l.Toolbar_subtitle);
        if (!TextUtils.isEmpty(b3)) {
            setSubtitle(b3);
        }
        this.j = getContext();
        setPopupTheme(a2.f(l.Toolbar_popupTheme, 0));
        Drawable a3 = a2.a(l.Toolbar_navigationIcon);
        if (a3 != null) {
            setNavigationIcon(a3);
        }
        CharSequence b4 = a2.b(l.Toolbar_navigationContentDescription);
        if (!TextUtils.isEmpty(b4)) {
            setNavigationContentDescription(b4);
        }
        this.K = a2.d(l.Toolbar_android_minHeight, 0);
        a2.b();
        this.M = a2.c();
    }

    private int a(int i2) {
        int i3 = i2 & 112;
        switch (i3) {
            case 16:
            case l.Theme_dividerVertical:
            case l.Theme_colorControlHighlight:
                return i3;
            default:
                return this.u & 112;
        }
    }

    private int a(View view, int i2) {
        int max;
        an anVar = (an) view.getLayoutParams();
        int measuredHeight = view.getMeasuredHeight();
        int i3 = i2 > 0 ? (measuredHeight - i2) / 2 : 0;
        switch (a(anVar.f103a)) {
            case l.Theme_dividerVertical:
                return getPaddingTop() - i3;
            case l.Theme_colorControlHighlight:
                return (((getHeight() - getPaddingBottom()) - measuredHeight) - anVar.bottomMargin) - i3;
            default:
                int paddingTop = getPaddingTop();
                int paddingBottom = getPaddingBottom();
                int height = getHeight();
                int i4 = (((height - paddingTop) - paddingBottom) - measuredHeight) / 2;
                if (i4 < anVar.topMargin) {
                    max = anVar.topMargin;
                } else {
                    int i5 = (((height - paddingBottom) - measuredHeight) - i4) - paddingTop;
                    max = i5 < anVar.bottomMargin ? Math.max(0, i4 - (anVar.bottomMargin - i5)) : i4;
                }
                return max + paddingTop;
        }
    }

    private int a(View view, int i2, int i3, int i4, int i5, int[] iArr) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int i6 = marginLayoutParams.leftMargin - iArr[0];
        int i7 = marginLayoutParams.rightMargin - iArr[1];
        int max = Math.max(0, i6) + Math.max(0, i7);
        iArr[0] = Math.max(0, -i6);
        iArr[1] = Math.max(0, -i7);
        view.measure(getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight() + max + i3, marginLayoutParams.width), getChildMeasureSpec(i4, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i5, marginLayoutParams.height));
        return view.getMeasuredWidth() + max;
    }

    private int a(View view, int i2, int[] iArr, int i3) {
        an anVar = (an) view.getLayoutParams();
        int i4 = anVar.leftMargin - iArr[0];
        int max = Math.max(0, i4) + i2;
        iArr[0] = Math.max(0, -i4);
        int a2 = a(view, i3);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max, a2, max + measuredWidth, view.getMeasuredHeight() + a2);
        return anVar.rightMargin + measuredWidth + max;
    }

    private int a(List list, int[] iArr) {
        int i2 = iArr[0];
        int i3 = iArr[1];
        int size = list.size();
        int i4 = 0;
        int i5 = 0;
        int i6 = i3;
        int i7 = i2;
        while (i4 < size) {
            View view = (View) list.get(i4);
            an anVar = (an) view.getLayoutParams();
            int i8 = anVar.leftMargin - i7;
            int i9 = anVar.rightMargin - i6;
            int max = Math.max(0, i8);
            int max2 = Math.max(0, i9);
            i7 = Math.max(0, -i8);
            i6 = Math.max(0, -i9);
            i4++;
            i5 += view.getMeasuredWidth() + max + max2;
        }
        return i5;
    }

    private static Context a(Context context, AttributeSet attributeSet, int i2) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.Toolbar, i2, 0);
        int resourceId = obtainStyledAttributes.getResourceId(l.Toolbar_theme, 0);
        if (resourceId != 0) {
            context = new ContextThemeWrapper(context, resourceId);
        }
        obtainStyledAttributes.recycle();
        return context;
    }

    private void a(View view) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        an i2 = layoutParams == null ? generateDefaultLayoutParams() : !checkLayoutParams(layoutParams) ? generateLayoutParams(layoutParams) : (an) layoutParams;
        i2.b = 1;
        addView(view, i2);
    }

    private void a(View view, int i2, int i3, int i4, int i5, int i6) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int childMeasureSpec = getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i3, marginLayoutParams.width);
        int childMeasureSpec2 = getChildMeasureSpec(i4, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i5, marginLayoutParams.height);
        int mode = View.MeasureSpec.getMode(childMeasureSpec2);
        if (mode != 1073741824 && i6 >= 0) {
            if (mode != 0) {
                i6 = Math.min(View.MeasureSpec.getSize(childMeasureSpec2), i6);
            }
            childMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i6, 1073741824);
        }
        view.measure(childMeasureSpec, childMeasureSpec2);
    }

    private void a(List list, int i2) {
        boolean z2 = true;
        if (au.d(this) != 1) {
            z2 = false;
        }
        int childCount = getChildCount();
        int a2 = android.support.v4.view.j.a(i2, au.d(this));
        list.clear();
        if (z2) {
            for (int i3 = childCount - 1; i3 >= 0; i3--) {
                View childAt = getChildAt(i3);
                an anVar = (an) childAt.getLayoutParams();
                if (anVar.b == 0 && b(childAt) && b(anVar.f103a) == a2) {
                    list.add(childAt);
                }
            }
            return;
        }
        for (int i4 = 0; i4 < childCount; i4++) {
            View childAt2 = getChildAt(i4);
            an anVar2 = (an) childAt2.getLayoutParams();
            if (anVar2.b == 0 && b(childAt2) && b(anVar2.f103a) == a2) {
                list.add(childAt2);
            }
        }
    }

    private int b(int i2) {
        int d2 = au.d(this);
        int a2 = android.support.v4.view.j.a(i2, d2) & 7;
        switch (a2) {
            case 1:
            case 3:
            case 5:
                return a2;
            case 2:
            case 4:
            default:
                return d2 == 1 ? 5 : 3;
        }
    }

    private int b(View view, int i2, int[] iArr, int i3) {
        an anVar = (an) view.getLayoutParams();
        int i4 = anVar.rightMargin - iArr[1];
        int max = i2 - Math.max(0, i4);
        iArr[1] = Math.max(0, -i4);
        int a2 = a(view, i3);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max - measuredWidth, a2, max, view.getMeasuredHeight() + a2);
        return max - (anVar.leftMargin + measuredWidth);
    }

    private boolean b(View view) {
        return (view == null || view.getParent() != this || view.getVisibility() == 8) ? false : true;
    }

    private int c(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return v.b(marginLayoutParams) + v.a(marginLayoutParams);
    }

    private int d(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return marginLayoutParams.bottomMargin + marginLayoutParams.topMargin;
    }

    private void e(View view) {
        if (((an) view.getLayoutParams()).b != 2 && view != this.b) {
            view.setVisibility(this.f204a != null ? 8 : 0);
        }
    }

    private MenuInflater getMenuInflater() {
        return new e(getContext());
    }

    private int getMinimumHeightCompat() {
        return Build.VERSION.SDK_INT >= 16 ? au.h(this) : this.K;
    }

    private void j() {
        if (this.f == null) {
            this.f = new ImageView(getContext());
        }
    }

    private void k() {
        l();
        if (this.b.d() == null) {
            i iVar = (i) this.b.getMenu();
            if (this.G == null) {
                this.G = new am(this, null);
            }
            this.b.setExpandedActionViewsExclusive(true);
            iVar.a(this.G, this.j);
        }
    }

    private void l() {
        if (this.b == null) {
            this.b = new ActionMenuView(getContext());
            this.b.setPopupTheme(this.k);
            this.b.setOnMenuItemClickListener(this.D);
            this.b.a(this.H, this.I);
            an i2 = generateDefaultLayoutParams();
            i2.f103a = 8388613 | (this.n & 112);
            this.b.setLayoutParams(i2);
            a(this.b);
        }
    }

    private void m() {
        if (this.e == null) {
            this.e = new ImageButton(getContext(), null, b.toolbarNavigationButtonStyle);
            an i2 = generateDefaultLayoutParams();
            i2.f103a = 8388611 | (this.n & 112);
            this.e.setLayoutParams(i2);
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        if (this.i == null) {
            this.i = new ImageButton(getContext(), null, b.toolbarNavigationButtonStyle);
            this.i.setImageDrawable(this.g);
            this.i.setContentDescription(this.h);
            an i2 = generateDefaultLayoutParams();
            i2.f103a = 8388611 | (this.n & 112);
            i2.b = 2;
            this.i.setLayoutParams(i2);
            this.i.setOnClickListener(new al(this));
        }
    }

    private void o() {
        removeCallbacks(this.L);
        post(this.L);
    }

    private boolean p() {
        if (!this.J) {
            return false;
        }
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (b(childAt) && childAt.getMeasuredWidth() > 0 && childAt.getMeasuredHeight() > 0) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void setChildVisibilityForExpandedActionView(boolean z2) {
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (!(((an) childAt.getLayoutParams()).b == 2 || childAt == this.b)) {
                childAt.setVisibility(z2 ? 8 : 0);
            }
        }
    }

    /* renamed from: a */
    public an generateLayoutParams(AttributeSet attributeSet) {
        return new an(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public an generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof an ? new an((an) layoutParams) : layoutParams instanceof android.support.v7.app.b ? new an((android.support.v7.app.b) layoutParams) : layoutParams instanceof ViewGroup.MarginLayoutParams ? new an((ViewGroup.MarginLayoutParams) layoutParams) : new an(layoutParams);
    }

    public void a(int i2, int i3) {
        this.t.a(i2, i3);
    }

    public void a(Context context, int i2) {
        this.l = i2;
        if (this.c != null) {
            this.c.setTextAppearance(context, i2);
        }
    }

    public void a(i iVar, ActionMenuPresenter actionMenuPresenter) {
        if (iVar != null || this.b != null) {
            l();
            i d2 = this.b.d();
            if (d2 != iVar) {
                if (d2 != null) {
                    d2.b(this.F);
                    d2.b(this.G);
                }
                if (this.G == null) {
                    this.G = new am(this, null);
                }
                actionMenuPresenter.c(true);
                if (iVar != null) {
                    iVar.a(actionMenuPresenter, this.j);
                    iVar.a(this.G, this.j);
                } else {
                    actionMenuPresenter.a(this.j, (i) null);
                    this.G.a(this.j, (i) null);
                    actionMenuPresenter.a(true);
                    this.G.a(true);
                }
                this.b.setPopupTheme(this.k);
                this.b.setPresenter(actionMenuPresenter);
                this.F = actionMenuPresenter;
            }
        }
    }

    public boolean a() {
        return getVisibility() == 0 && this.b != null && this.b.a();
    }

    public void b(Context context, int i2) {
        this.m = i2;
        if (this.d != null) {
            this.d.setTextAppearance(context, i2);
        }
    }

    public boolean b() {
        return this.b != null && this.b.g();
    }

    public boolean c() {
        return this.b != null && this.b.h();
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return super.checkLayoutParams(layoutParams) && (layoutParams instanceof an);
    }

    public boolean d() {
        return this.b != null && this.b.e();
    }

    public boolean e() {
        return this.b != null && this.b.f();
    }

    public void f() {
        if (this.b != null) {
            this.b.i();
        }
    }

    public boolean g() {
        return (this.G == null || this.G.b == null) ? false : true;
    }

    public int getContentInsetEnd() {
        return this.t.d();
    }

    public int getContentInsetLeft() {
        return this.t.a();
    }

    public int getContentInsetRight() {
        return this.t.b();
    }

    public int getContentInsetStart() {
        return this.t.c();
    }

    public Drawable getLogo() {
        if (this.f != null) {
            return this.f.getDrawable();
        }
        return null;
    }

    public CharSequence getLogoDescription() {
        if (this.f != null) {
            return this.f.getContentDescription();
        }
        return null;
    }

    public Menu getMenu() {
        k();
        return this.b.getMenu();
    }

    public CharSequence getNavigationContentDescription() {
        if (this.e != null) {
            return this.e.getContentDescription();
        }
        return null;
    }

    public Drawable getNavigationIcon() {
        if (this.e != null) {
            return this.e.getDrawable();
        }
        return null;
    }

    public int getPopupTheme() {
        return this.k;
    }

    public CharSequence getSubtitle() {
        return this.w;
    }

    public CharSequence getTitle() {
        return this.v;
    }

    public x getWrapper() {
        if (this.E == null) {
            this.E = new bc(this, true);
        }
        return this.E;
    }

    public void h() {
        m mVar = this.G == null ? null : this.G.b;
        if (mVar != null) {
            mVar.collapseActionView();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: i */
    public an generateDefaultLayoutParams() {
        return new an(-2, -2);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.L);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int max;
        boolean z3 = au.d(this) == 1;
        int width = getWidth();
        int height = getHeight();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int i16 = width - paddingRight;
        int[] iArr = this.B;
        iArr[1] = 0;
        iArr[0] = 0;
        int minimumHeightCompat = getMinimumHeightCompat();
        if (!b(this.e)) {
            i6 = paddingLeft;
        } else if (z3) {
            i16 = b(this.e, i16, iArr, minimumHeightCompat);
            i6 = paddingLeft;
        } else {
            i6 = a(this.e, paddingLeft, iArr, minimumHeightCompat);
        }
        if (b(this.i)) {
            if (z3) {
                i16 = b(this.i, i16, iArr, minimumHeightCompat);
            } else {
                i6 = a(this.i, i6, iArr, minimumHeightCompat);
            }
        }
        if (b(this.b)) {
            if (z3) {
                i6 = a(this.b, i6, iArr, minimumHeightCompat);
            } else {
                i16 = b(this.b, i16, iArr, minimumHeightCompat);
            }
        }
        iArr[0] = Math.max(0, getContentInsetLeft() - i6);
        iArr[1] = Math.max(0, getContentInsetRight() - ((width - paddingRight) - i16));
        int max2 = Math.max(i6, getContentInsetLeft());
        int min = Math.min(i16, (width - paddingRight) - getContentInsetRight());
        if (b(this.f204a)) {
            if (z3) {
                min = b(this.f204a, min, iArr, minimumHeightCompat);
            } else {
                max2 = a(this.f204a, max2, iArr, minimumHeightCompat);
            }
        }
        if (!b(this.f)) {
            i7 = min;
            i8 = max2;
        } else if (z3) {
            i7 = b(this.f, min, iArr, minimumHeightCompat);
            i8 = max2;
        } else {
            i7 = min;
            i8 = a(this.f, max2, iArr, minimumHeightCompat);
        }
        boolean b2 = b(this.c);
        boolean b3 = b(this.d);
        int i17 = 0;
        if (b2) {
            an anVar = (an) this.c.getLayoutParams();
            i17 = 0 + anVar.bottomMargin + anVar.topMargin + this.c.getMeasuredHeight();
        }
        if (b3) {
            an anVar2 = (an) this.d.getLayoutParams();
            i9 = anVar2.bottomMargin + anVar2.topMargin + this.d.getMeasuredHeight() + i17;
        } else {
            i9 = i17;
        }
        if (b2 || b3) {
            TextView textView = b2 ? this.c : this.d;
            TextView textView2 = b3 ? this.d : this.c;
            an anVar3 = (an) textView.getLayoutParams();
            an anVar4 = (an) textView2.getLayoutParams();
            boolean z4 = (b2 && this.c.getMeasuredWidth() > 0) || (b3 && this.d.getMeasuredWidth() > 0);
            switch (this.u & 112) {
                case l.Theme_dividerVertical:
                    i10 = anVar3.topMargin + getPaddingTop() + this.r;
                    break;
                case l.Theme_colorControlHighlight:
                    i10 = (((height - paddingBottom) - anVar4.bottomMargin) - this.s) - i9;
                    break;
                default:
                    int i18 = (((height - paddingTop) - paddingBottom) - i9) / 2;
                    if (i18 < anVar3.topMargin + this.r) {
                        max = anVar3.topMargin + this.r;
                    } else {
                        int i19 = (((height - paddingBottom) - i9) - i18) - paddingTop;
                        max = i19 < anVar3.bottomMargin + this.s ? Math.max(0, i18 - ((anVar4.bottomMargin + this.s) - i19)) : i18;
                    }
                    i10 = paddingTop + max;
                    break;
            }
            if (z3) {
                int i20 = (z4 ? this.p : 0) - iArr[1];
                int max3 = i7 - Math.max(0, i20);
                iArr[1] = Math.max(0, -i20);
                if (b2) {
                    int measuredWidth = max3 - this.c.getMeasuredWidth();
                    int measuredHeight = this.c.getMeasuredHeight() + i10;
                    this.c.layout(measuredWidth, i10, max3, measuredHeight);
                    int i21 = measuredWidth - this.q;
                    i10 = measuredHeight + ((an) this.c.getLayoutParams()).bottomMargin;
                    i14 = i21;
                } else {
                    i14 = max3;
                }
                if (b3) {
                    an anVar5 = (an) this.d.getLayoutParams();
                    int i22 = anVar5.topMargin + i10;
                    int measuredHeight2 = this.d.getMeasuredHeight() + i22;
                    this.d.layout(max3 - this.d.getMeasuredWidth(), i22, max3, measuredHeight2);
                    int i23 = anVar5.bottomMargin + measuredHeight2;
                    i15 = max3 - this.q;
                } else {
                    i15 = max3;
                }
                i7 = z4 ? Math.min(i14, i15) : max3;
            } else {
                int i24 = (z4 ? this.p : 0) - iArr[0];
                i8 += Math.max(0, i24);
                iArr[0] = Math.max(0, -i24);
                if (b2) {
                    int measuredWidth2 = this.c.getMeasuredWidth() + i8;
                    int measuredHeight3 = this.c.getMeasuredHeight() + i10;
                    this.c.layout(i8, i10, measuredWidth2, measuredHeight3);
                    int i25 = ((an) this.c.getLayoutParams()).bottomMargin + measuredHeight3;
                    i11 = measuredWidth2 + this.q;
                    i12 = i25;
                } else {
                    i11 = i8;
                    i12 = i10;
                }
                if (b3) {
                    an anVar6 = (an) this.d.getLayoutParams();
                    int i26 = i12 + anVar6.topMargin;
                    int measuredWidth3 = this.d.getMeasuredWidth() + i8;
                    int measuredHeight4 = this.d.getMeasuredHeight() + i26;
                    this.d.layout(i8, i26, measuredWidth3, measuredHeight4);
                    int i27 = anVar6.bottomMargin + measuredHeight4;
                    i13 = this.q + measuredWidth3;
                } else {
                    i13 = i8;
                }
                if (z4) {
                    i8 = Math.max(i11, i13);
                }
            }
        }
        a(this.A, 3);
        int size = this.A.size();
        int i28 = i8;
        for (int i29 = 0; i29 < size; i29++) {
            i28 = a((View) this.A.get(i29), i28, iArr, minimumHeightCompat);
        }
        a(this.A, 5);
        int size2 = this.A.size();
        for (int i30 = 0; i30 < size2; i30++) {
            i7 = b((View) this.A.get(i30), i7, iArr, minimumHeightCompat);
        }
        a(this.A, 1);
        int a2 = a(this.A, iArr);
        int i31 = ((((width - paddingLeft) - paddingRight) / 2) + paddingLeft) - (a2 / 2);
        int i32 = a2 + i31;
        if (i31 < i28) {
            i31 = i28;
        } else if (i32 > i7) {
            i31 -= i32 - i7;
        }
        int size3 = this.A.size();
        int i33 = i31;
        for (int i34 = 0; i34 < size3; i34++) {
            i33 = a((View) this.A.get(i34), i33, iArr, minimumHeightCompat);
        }
        this.A.clear();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        char c2;
        char c3;
        int i4;
        int i5;
        int i6 = 0;
        int i7 = 0;
        int[] iArr = this.B;
        if (bh.a(this)) {
            c2 = 0;
            c3 = 1;
        } else {
            c2 = 1;
            c3 = 0;
        }
        int i8 = 0;
        if (b(this.e)) {
            a(this.e, i2, 0, i3, 0, this.o);
            i8 = this.e.getMeasuredWidth() + c(this.e);
            int max = Math.max(0, this.e.getMeasuredHeight() + d(this.e));
            i7 = bh.a(0, au.f(this.e));
            i6 = max;
        }
        if (b(this.i)) {
            a(this.i, i2, 0, i3, 0, this.o);
            i8 = this.i.getMeasuredWidth() + c(this.i);
            i6 = Math.max(i6, this.i.getMeasuredHeight() + d(this.i));
            i7 = bh.a(i7, au.f(this.i));
        }
        int contentInsetStart = getContentInsetStart();
        int max2 = 0 + Math.max(contentInsetStart, i8);
        iArr[c3] = Math.max(0, contentInsetStart - i8);
        int i9 = 0;
        if (b(this.b)) {
            a(this.b, i2, max2, i3, 0, this.o);
            i9 = this.b.getMeasuredWidth() + c(this.b);
            i6 = Math.max(i6, this.b.getMeasuredHeight() + d(this.b));
            i7 = bh.a(i7, au.f(this.b));
        }
        int contentInsetEnd = getContentInsetEnd();
        int max3 = max2 + Math.max(contentInsetEnd, i9);
        iArr[c2] = Math.max(0, contentInsetEnd - i9);
        if (b(this.f204a)) {
            max3 += a(this.f204a, i2, max3, i3, 0, iArr);
            i6 = Math.max(i6, this.f204a.getMeasuredHeight() + d(this.f204a));
            i7 = bh.a(i7, au.f(this.f204a));
        }
        if (b(this.f)) {
            max3 += a(this.f, i2, max3, i3, 0, iArr);
            i6 = Math.max(i6, this.f.getMeasuredHeight() + d(this.f));
            i7 = bh.a(i7, au.f(this.f));
        }
        int childCount = getChildCount();
        int i10 = 0;
        int i11 = i6;
        int i12 = i7;
        while (i10 < childCount) {
            View childAt = getChildAt(i10);
            if (((an) childAt.getLayoutParams()).b != 0) {
                i4 = i12;
                i5 = i11;
            } else if (!b(childAt)) {
                i4 = i12;
                i5 = i11;
            } else {
                max3 += a(childAt, i2, max3, i3, 0, iArr);
                int max4 = Math.max(i11, childAt.getMeasuredHeight() + d(childAt));
                i4 = bh.a(i12, au.f(childAt));
                i5 = max4;
            }
            i10++;
            i12 = i4;
            i11 = i5;
        }
        int i13 = 0;
        int i14 = 0;
        int i15 = this.r + this.s;
        int i16 = this.p + this.q;
        if (b(this.c)) {
            a(this.c, i2, max3 + i16, i3, i15, iArr);
            i13 = c(this.c) + this.c.getMeasuredWidth();
            i14 = this.c.getMeasuredHeight() + d(this.c);
            i12 = bh.a(i12, au.f(this.c));
        }
        if (b(this.d)) {
            i13 = Math.max(i13, a(this.d, i2, max3 + i16, i3, i15 + i14, iArr));
            i14 += this.d.getMeasuredHeight() + d(this.d);
            i12 = bh.a(i12, au.f(this.d));
        }
        int max5 = Math.max(i11, i14);
        int paddingLeft = i13 + max3 + getPaddingLeft() + getPaddingRight();
        int paddingTop = max5 + getPaddingTop() + getPaddingBottom();
        int a2 = au.a(Math.max(paddingLeft, getSuggestedMinimumWidth()), i2, -16777216 & i12);
        int a3 = au.a(Math.max(paddingTop, getSuggestedMinimumHeight()), i3, i12 << 16);
        if (p()) {
            a3 = 0;
        }
        setMeasuredDimension(a2, a3);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        MenuItem findItem;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        i d2 = this.b != null ? this.b.d() : null;
        if (!(savedState.f205a == 0 || this.G == null || d2 == null || (findItem = d2.findItem(savedState.f205a)) == null)) {
            aa.b(findItem);
        }
        if (savedState.b) {
            o();
        }
    }

    public void onRtlPropertiesChanged(int i2) {
        boolean z2 = true;
        if (Build.VERSION.SDK_INT >= 17) {
            super.onRtlPropertiesChanged(i2);
        }
        ae aeVar = this.t;
        if (i2 != 1) {
            z2 = false;
        }
        aeVar.a(z2);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        if (!(this.G == null || this.G.b == null)) {
            savedState.f205a = this.G.b.getItemId();
        }
        savedState.b = b();
        return savedState;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int a2 = ai.a(motionEvent);
        if (a2 == 0) {
            this.z = false;
        }
        if (!this.z) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (a2 == 0 && !onTouchEvent) {
                this.z = true;
            }
        }
        if (a2 == 1 || a2 == 3) {
            this.z = false;
        }
        return true;
    }

    public void setCollapsible(boolean z2) {
        this.J = z2;
        requestLayout();
    }

    public void setLogo(int i2) {
        setLogo(this.M.a(i2));
    }

    public void setLogo(Drawable drawable) {
        if (drawable != null) {
            j();
            if (this.f.getParent() == null) {
                a(this.f);
                e(this.f);
            }
        } else if (!(this.f == null || this.f.getParent() == null)) {
            removeView(this.f);
        }
        if (this.f != null) {
            this.f.setImageDrawable(drawable);
        }
    }

    public void setLogoDescription(int i2) {
        setLogoDescription(getContext().getText(i2));
    }

    public void setLogoDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            j();
        }
        if (this.f != null) {
            this.f.setContentDescription(charSequence);
        }
    }

    public void setMinimumHeight(int i2) {
        this.K = i2;
        super.setMinimumHeight(i2);
    }

    public void setNavigationContentDescription(int i2) {
        setNavigationContentDescription(i2 != 0 ? getContext().getText(i2) : null);
    }

    public void setNavigationContentDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            m();
        }
        if (this.e != null) {
            this.e.setContentDescription(charSequence);
        }
    }

    public void setNavigationIcon(int i2) {
        setNavigationIcon(this.M.a(i2));
    }

    public void setNavigationIcon(Drawable drawable) {
        if (drawable != null) {
            m();
            if (this.e.getParent() == null) {
                a(this.e);
                e(this.e);
            }
        } else if (!(this.e == null || this.e.getParent() == null)) {
            removeView(this.e);
        }
        if (this.e != null) {
            this.e.setImageDrawable(drawable);
        }
    }

    public void setNavigationOnClickListener(View.OnClickListener onClickListener) {
        m();
        this.e.setOnClickListener(onClickListener);
    }

    public void setOnMenuItemClickListener(ao aoVar) {
        this.C = aoVar;
    }

    public void setPopupTheme(int i2) {
        if (this.k != i2) {
            this.k = i2;
            if (i2 == 0) {
                this.j = getContext();
            } else {
                this.j = new ContextThemeWrapper(getContext(), i2);
            }
        }
    }

    public void setSubtitle(int i2) {
        setSubtitle(getContext().getText(i2));
    }

    public void setSubtitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.d == null) {
                Context context = getContext();
                this.d = new TextView(context);
                this.d.setSingleLine();
                this.d.setEllipsize(TextUtils.TruncateAt.END);
                if (this.m != 0) {
                    this.d.setTextAppearance(context, this.m);
                }
                if (this.y != 0) {
                    this.d.setTextColor(this.y);
                }
            }
            if (this.d.getParent() == null) {
                a(this.d);
                e(this.d);
            }
        } else if (!(this.d == null || this.d.getParent() == null)) {
            removeView(this.d);
        }
        if (this.d != null) {
            this.d.setText(charSequence);
        }
        this.w = charSequence;
    }

    public void setSubtitleTextColor(int i2) {
        this.y = i2;
        if (this.d != null) {
            this.d.setTextColor(i2);
        }
    }

    public void setTitle(int i2) {
        setTitle(getContext().getText(i2));
    }

    public void setTitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.c == null) {
                Context context = getContext();
                this.c = new TextView(context);
                this.c.setSingleLine();
                this.c.setEllipsize(TextUtils.TruncateAt.END);
                if (this.l != 0) {
                    this.c.setTextAppearance(context, this.l);
                }
                if (this.x != 0) {
                    this.c.setTextColor(this.x);
                }
            }
            if (this.c.getParent() == null) {
                a(this.c);
                e(this.c);
            }
        } else if (!(this.c == null || this.c.getParent() == null)) {
            removeView(this.c);
        }
        if (this.c != null) {
            this.c.setText(charSequence);
        }
        this.v = charSequence;
    }

    public void setTitleTextColor(int i2) {
        this.x = i2;
        if (this.c != null) {
            this.c.setTextColor(i2);
        }
    }
}
