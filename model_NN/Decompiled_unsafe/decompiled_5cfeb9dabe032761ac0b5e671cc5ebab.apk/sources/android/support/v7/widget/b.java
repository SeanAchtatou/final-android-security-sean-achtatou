package android.support.v7.widget;

import android.content.Context;
import android.support.v7.internal.view.menu.ad;
import android.support.v7.internal.view.menu.m;
import android.support.v7.internal.view.menu.v;
import android.view.MenuItem;
import android.view.View;

class b extends v {
    final /* synthetic */ ActionMenuPresenter c;
    private ad d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(ActionMenuPresenter actionMenuPresenter, Context context, ad adVar) {
        super(context, adVar, null, false, android.support.v7.a.b.actionOverflowMenuStyle);
        boolean z = false;
        this.c = actionMenuPresenter;
        this.d = adVar;
        if (!((m) adVar.getItem()).j()) {
            a(actionMenuPresenter.i == null ? (View) actionMenuPresenter.f : actionMenuPresenter.i);
        }
        a(actionMenuPresenter.g);
        int size = adVar.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                break;
            }
            MenuItem item = adVar.getItem(i);
            if (item.isVisible() && item.getIcon() != null) {
                z = true;
                break;
            }
            i++;
        }
        b(z);
    }

    public void onDismiss() {
        super.onDismiss();
        b unused = this.c.w = (b) null;
        this.c.h = 0;
    }
}
