package android.support.v7.widget;

import android.support.v7.internal.view.menu.ad;
import android.support.v7.internal.view.menu.i;
import android.support.v7.internal.view.menu.y;

class h implements y {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ActionMenuPresenter f219a;

    private h(ActionMenuPresenter actionMenuPresenter) {
        this.f219a = actionMenuPresenter;
    }

    public void a(i iVar, boolean z) {
        if (iVar instanceof ad) {
            ((ad) iVar).p().a(false);
        }
        y a2 = this.f219a.a();
        if (a2 != null) {
            a2.a(iVar, z);
        }
    }

    public boolean a(i iVar) {
        if (iVar == null) {
            return false;
        }
        this.f219a.h = ((ad) iVar).getItem().getItemId();
        y a2 = this.f219a.a();
        return a2 != null ? a2.a(iVar) : false;
    }
}
