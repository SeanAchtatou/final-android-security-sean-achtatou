package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.g;
import android.support.v4.view.h;
import android.support.v7.a.i;
import android.support.v7.internal.view.a;
import android.support.v7.internal.view.menu.ActionMenuItemView;
import android.support.v7.internal.view.menu.aa;
import android.support.v7.internal.view.menu.ad;
import android.support.v7.internal.view.menu.d;
import android.support.v7.internal.view.menu.m;
import android.support.v7.internal.view.menu.z;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public class ActionMenuPresenter extends d implements h {
    final h g = new h(this);
    int h;
    /* access modifiers changed from: private */
    public View i;
    private boolean j;
    private boolean k;
    private int l;
    private int m;
    private int n;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    private int s;
    private final SparseBooleanArray t = new SparseBooleanArray();
    private View u;
    /* access modifiers changed from: private */
    public g v;
    /* access modifiers changed from: private */
    public b w;
    /* access modifiers changed from: private */
    public d x;
    private c y;

    class SavedState implements Parcelable {
        public static final Parcelable.Creator CREATOR = new i();

        /* renamed from: a  reason: collision with root package name */
        public int f199a;

        SavedState() {
        }

        SavedState(Parcel parcel) {
            this.f199a = parcel.readInt();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f199a);
        }
    }

    public ActionMenuPresenter(Context context) {
        super(context, i.abc_action_menu_layout, i.abc_action_menu_item_layout);
    }

    private View a(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) this.f;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = viewGroup.getChildAt(i2);
            if ((childAt instanceof aa) && ((aa) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    public z a(ViewGroup viewGroup) {
        z a2 = super.a(viewGroup);
        ((ActionMenuView) a2).setPresenter(this);
        return a2;
    }

    public View a(m mVar, View view, ViewGroup viewGroup) {
        View actionView = mVar.getActionView();
        if (actionView == null || mVar.n()) {
            actionView = super.a(mVar, view, viewGroup);
        }
        actionView.setVisibility(mVar.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    public void a(int i2, boolean z) {
        this.l = i2;
        this.p = z;
        this.q = true;
    }

    public void a(Context context, android.support.v7.internal.view.menu.i iVar) {
        super.a(context, iVar);
        Resources resources = context.getResources();
        a a2 = a.a(context);
        if (!this.k) {
            this.j = a2.b();
        }
        if (!this.q) {
            this.l = a2.c();
        }
        if (!this.o) {
            this.n = a2.a();
        }
        int i2 = this.l;
        if (this.j) {
            if (this.i == null) {
                this.i = new e(this, this.f134a);
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.i.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i2 -= this.i.getMeasuredWidth();
        } else {
            this.i = null;
        }
        this.m = i2;
        this.s = (int) (56.0f * resources.getDisplayMetrics().density);
        this.u = null;
    }

    public void a(Configuration configuration) {
        if (!this.o) {
            this.n = this.b.getResources().getInteger(android.support.v7.a.h.abc_max_action_buttons);
        }
        if (this.c != null) {
            this.c.b(true);
        }
    }

    public void a(android.support.v7.internal.view.menu.i iVar, boolean z) {
        e();
        super.a(iVar, z);
    }

    public void a(m mVar, aa aaVar) {
        aaVar.a(mVar, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) aaVar;
        actionMenuItemView.setItemInvoker((ActionMenuView) this.f);
        if (this.y == null) {
            this.y = new c(this);
        }
        actionMenuItemView.setPopupCallback(this.y);
    }

    public void a(ActionMenuView actionMenuView) {
        this.f = actionMenuView;
        actionMenuView.a(this.c);
    }

    public void a(boolean z) {
        boolean z2 = true;
        boolean z3 = false;
        ViewGroup viewGroup = (ViewGroup) ((View) this.f).getParent();
        if (viewGroup != null) {
            android.support.v7.internal.c.a.a(viewGroup);
        }
        super.a(z);
        ((View) this.f).requestLayout();
        if (this.c != null) {
            ArrayList k2 = this.c.k();
            int size = k2.size();
            for (int i2 = 0; i2 < size; i2++) {
                g a2 = ((m) k2.get(i2)).a();
                if (a2 != null) {
                    a2.a(this);
                }
            }
        }
        ArrayList l2 = this.c != null ? this.c.l() : null;
        if (this.j && l2 != null) {
            int size2 = l2.size();
            if (size2 == 1) {
                z3 = !((m) l2.get(0)).isActionViewExpanded();
            } else {
                if (size2 <= 0) {
                    z2 = false;
                }
                z3 = z2;
            }
        }
        if (z3) {
            if (this.i == null) {
                this.i = new e(this, this.f134a);
            }
            ViewGroup viewGroup2 = (ViewGroup) this.i.getParent();
            if (viewGroup2 != this.f) {
                if (viewGroup2 != null) {
                    viewGroup2.removeView(this.i);
                }
                ActionMenuView actionMenuView = (ActionMenuView) this.f;
                actionMenuView.addView(this.i, actionMenuView.c());
            }
        } else if (this.i != null && this.i.getParent() == this.f) {
            ((ViewGroup) this.f).removeView(this.i);
        }
        ((ActionMenuView) this.f).setOverflowReserved(this.j);
    }

    public boolean a(int i2, m mVar) {
        return mVar.j();
    }

    public boolean a(ad adVar) {
        if (!adVar.hasVisibleItems()) {
            return false;
        }
        ad adVar2 = adVar;
        while (adVar2.s() != this.c) {
            adVar2 = (ad) adVar2.s();
        }
        View a2 = a(adVar2.getItem());
        if (a2 == null) {
            if (this.i == null) {
                return false;
            }
            a2 = this.i;
        }
        this.h = adVar.getItem().getItemId();
        this.w = new b(this, this.b, adVar);
        this.w.a(a2);
        this.w.a();
        super.a(adVar);
        return true;
    }

    public boolean a(ViewGroup viewGroup, int i2) {
        if (viewGroup.getChildAt(i2) == this.i) {
            return false;
        }
        return super.a(viewGroup, i2);
    }

    public void b(int i2) {
        this.n = i2;
        this.o = true;
    }

    public void b(boolean z) {
        this.j = z;
        this.k = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.ActionMenuView.a(android.view.View, int, int, int, int):int
     arg types: [android.view.View, int, int, int, int]
     candidates:
      android.support.v7.widget.LinearLayoutCompat.a(android.view.View, int, int, int, int):void
      android.support.v7.widget.ActionMenuView.a(android.view.View, int, int, int, int):int */
    public boolean b() {
        int i2;
        int i3;
        int i4;
        int i5;
        boolean z;
        int i6;
        int i7;
        int i8;
        int i9;
        boolean z2;
        ArrayList i10 = this.c.i();
        int size = i10.size();
        int i11 = this.n;
        int i12 = this.m;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) this.f;
        int i13 = 0;
        int i14 = 0;
        boolean z3 = false;
        int i15 = 0;
        while (i15 < size) {
            m mVar = (m) i10.get(i15);
            if (mVar.l()) {
                i13++;
            } else if (mVar.k()) {
                i14++;
            } else {
                z3 = true;
            }
            i15++;
            i11 = (!this.r || !mVar.isActionViewExpanded()) ? i11 : 0;
        }
        if (this.j && (z3 || i13 + i14 > i11)) {
            i11--;
        }
        int i16 = i11 - i13;
        SparseBooleanArray sparseBooleanArray = this.t;
        sparseBooleanArray.clear();
        int i17 = 0;
        if (this.p) {
            i17 = i12 / this.s;
            i2 = ((i12 % this.s) / i17) + this.s;
        } else {
            i2 = 0;
        }
        int i18 = 0;
        int i19 = 0;
        int i20 = i17;
        while (i18 < size) {
            m mVar2 = (m) i10.get(i18);
            if (mVar2.l()) {
                View a2 = a(mVar2, this.u, viewGroup);
                if (this.u == null) {
                    this.u = a2;
                }
                if (this.p) {
                    i20 -= ActionMenuView.a(a2, i2, i20, makeMeasureSpec, 0);
                } else {
                    a2.measure(makeMeasureSpec, makeMeasureSpec);
                }
                i3 = a2.getMeasuredWidth();
                int i21 = i12 - i3;
                if (i19 != 0) {
                    i3 = i19;
                }
                int groupId = mVar2.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                mVar2.d(true);
                i4 = i21;
                i5 = i16;
            } else if (mVar2.k()) {
                int groupId2 = mVar2.getGroupId();
                boolean z4 = sparseBooleanArray.get(groupId2);
                boolean z5 = (i16 > 0 || z4) && i12 > 0 && (!this.p || i20 > 0);
                if (z5) {
                    View a3 = a(mVar2, this.u, viewGroup);
                    if (this.u == null) {
                        this.u = a3;
                    }
                    if (this.p) {
                        int a4 = ActionMenuView.a(a3, i2, i20, makeMeasureSpec, 0);
                        int i22 = i20 - a4;
                        z2 = a4 == 0 ? false : z5;
                        i9 = i22;
                    } else {
                        a3.measure(makeMeasureSpec, makeMeasureSpec);
                        boolean z6 = z5;
                        i9 = i20;
                        z2 = z6;
                    }
                    int measuredWidth = a3.getMeasuredWidth();
                    i12 -= measuredWidth;
                    if (i19 == 0) {
                        i19 = measuredWidth;
                    }
                    if (this.p) {
                        z = z2 & (i12 >= 0);
                        i6 = i19;
                        i7 = i9;
                    } else {
                        z = z2 & (i12 + i19 > 0);
                        i6 = i19;
                        i7 = i9;
                    }
                } else {
                    z = z5;
                    i6 = i19;
                    i7 = i20;
                }
                if (z && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                    i8 = i16;
                } else if (z4) {
                    sparseBooleanArray.put(groupId2, false);
                    int i23 = i16;
                    for (int i24 = 0; i24 < i18; i24++) {
                        m mVar3 = (m) i10.get(i24);
                        if (mVar3.getGroupId() == groupId2) {
                            if (mVar3.j()) {
                                i23++;
                            }
                            mVar3.d(false);
                        }
                    }
                    i8 = i23;
                } else {
                    i8 = i16;
                }
                if (z) {
                    i8--;
                }
                mVar2.d(z);
                i3 = i6;
                i4 = i12;
                int i25 = i7;
                i5 = i8;
                i20 = i25;
            } else {
                mVar2.d(false);
                i3 = i19;
                i4 = i12;
                i5 = i16;
            }
            i18++;
            i12 = i4;
            i16 = i5;
            i19 = i3;
        }
        return true;
    }

    public void c(boolean z) {
        this.r = z;
    }

    public boolean c() {
        if (!this.j || g() || this.c == null || this.f == null || this.x != null || this.c.l().isEmpty()) {
            return false;
        }
        this.x = new d(this, new g(this, this.b, this.c, this.i, true));
        ((View) this.f).post(this.x);
        super.a((ad) null);
        return true;
    }

    public boolean d() {
        if (this.x == null || this.f == null) {
            g gVar = this.v;
            if (gVar == null) {
                return false;
            }
            gVar.e();
            return true;
        }
        ((View) this.f).removeCallbacks(this.x);
        this.x = null;
        return true;
    }

    public boolean e() {
        return d() | f();
    }

    public boolean f() {
        if (this.w == null) {
            return false;
        }
        this.w.e();
        return true;
    }

    public boolean g() {
        return this.v != null && this.v.f();
    }

    public boolean h() {
        return this.x != null || g();
    }
}
