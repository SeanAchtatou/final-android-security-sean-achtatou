package android.support.v7.widget;

import android.annotation.TargetApi;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.e;
import android.support.v7.b.c;
import android.support.v7.internal.widget.aw;
import android.support.v7.internal.widget.bb;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import java.util.WeakHashMap;

public class SearchView extends LinearLayoutCompat implements c {

    /* renamed from: a  reason: collision with root package name */
    static final ad f202a = new ad();
    private static final boolean b = (Build.VERSION.SDK_INT >= 8);
    private boolean A;
    private int B;
    private boolean C;
    private CharSequence D;
    private boolean E;
    private int F;
    private SearchableInfo G;
    private Bundle H;
    private final aw I;
    private Runnable J;
    private final Runnable K;
    private Runnable L;
    private final WeakHashMap M;
    private final SearchAutoComplete c;
    private final View d;
    private final View e;
    private final ImageView f;
    private final ImageView g;
    private final ImageView h;
    private final ImageView i;
    private final ImageView j;
    private final int k;
    private final int l;
    private final int m;
    private final Intent n;
    private final Intent o;
    private af p;
    private ae q;
    private View.OnFocusChangeListener r;
    private ag s;
    private View.OnClickListener t;
    private boolean u;
    private boolean v;
    private e w;
    private boolean x;
    private CharSequence y;
    private boolean z;

    public class SearchAutoComplete extends AutoCompleteTextView {

        /* renamed from: a  reason: collision with root package name */
        private final int[] f203a;
        private int b;
        private SearchView c;
        private final aw d;

        public SearchAutoComplete(Context context) {
            this(context, null);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet) {
            this(context, attributeSet, 16842859);
        }

        public SearchAutoComplete(Context context, AttributeSet attributeSet, int i) {
            super(context, attributeSet, i);
            this.f203a = new int[]{16843126};
            this.b = getThreshold();
            bb a2 = bb.a(context, attributeSet, this.f203a, i, 0);
            if (a2.d(0)) {
                setDropDownBackgroundDrawable(a2.a(0));
            }
            a2.b();
            this.d = a2.c();
        }

        public boolean enoughToFilter() {
            return this.b <= 0 || super.enoughToFilter();
        }

        /* access modifiers changed from: protected */
        public void onFocusChanged(boolean z, int i, Rect rect) {
            super.onFocusChanged(z, i, rect);
            this.c.d();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.widget.SearchView.a(android.support.v7.widget.SearchView, boolean):void
         arg types: [android.support.v7.widget.SearchView, int]
         candidates:
          android.support.v7.widget.SearchView.a(java.lang.CharSequence, boolean):void
          android.support.v7.widget.LinearLayoutCompat.a(android.view.View, int):int
          android.support.v7.widget.LinearLayoutCompat.a(int, int):void
          android.support.v7.widget.LinearLayoutCompat.a(android.graphics.Canvas, int):void
          android.support.v7.widget.SearchView.a(android.support.v7.widget.SearchView, boolean):void */
        public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
            if (i == 4) {
                if (keyEvent.getAction() == 0 && keyEvent.getRepeatCount() == 0) {
                    KeyEvent.DispatcherState keyDispatcherState = getKeyDispatcherState();
                    if (keyDispatcherState == null) {
                        return true;
                    }
                    keyDispatcherState.startTracking(keyEvent, this);
                    return true;
                } else if (keyEvent.getAction() == 1) {
                    KeyEvent.DispatcherState keyDispatcherState2 = getKeyDispatcherState();
                    if (keyDispatcherState2 != null) {
                        keyDispatcherState2.handleUpEvent(keyEvent);
                    }
                    if (keyEvent.isTracking() && !keyEvent.isCanceled()) {
                        this.c.clearFocus();
                        this.c.setImeVisibility(false);
                        return true;
                    }
                }
            }
            return super.onKeyPreIme(i, keyEvent);
        }

        public void onWindowFocusChanged(boolean z) {
            super.onWindowFocusChanged(z);
            if (z && this.c.hasFocus() && getVisibility() == 0) {
                ((InputMethodManager) getContext().getSystemService("input_method")).showSoftInput(this, 0);
                if (SearchView.a(getContext())) {
                    SearchView.f202a.a(this, true);
                }
            }
        }

        public void performCompletion() {
        }

        /* access modifiers changed from: protected */
        public void replaceText(CharSequence charSequence) {
        }

        public void setDropDownBackgroundResource(int i) {
            setDropDownBackgroundDrawable(this.d.a(i));
        }

        /* access modifiers changed from: package-private */
        public void setSearchView(SearchView searchView) {
            this.c = searchView;
        }

        public void setThreshold(int i) {
            super.setThreshold(i);
            this.b = i;
        }
    }

    private Intent a(String str, Uri uri, String str2, String str3, int i2, String str4) {
        Intent intent = new Intent(str);
        intent.addFlags(268435456);
        if (uri != null) {
            intent.setData(uri);
        }
        intent.putExtra("user_query", this.D);
        if (str3 != null) {
            intent.putExtra("query", str3);
        }
        if (str2 != null) {
            intent.putExtra("intent_extra_data_key", str2);
        }
        if (this.H != null) {
            intent.putExtra("app_data", this.H);
        }
        if (i2 != 0) {
            intent.putExtra("action_key", i2);
            intent.putExtra("action_msg", str4);
        }
        if (b) {
            intent.setComponent(this.G.getSearchActivity());
        }
        return intent;
    }

    private void a(int i2, String str, String str2) {
        getContext().startActivity(a("android.intent.action.SEARCH", (Uri) null, (String) null, str2, i2, str));
    }

    private void a(boolean z2) {
        boolean z3 = true;
        int i2 = 8;
        this.v = z2;
        int i3 = z2 ? 0 : 8;
        boolean z4 = !TextUtils.isEmpty(this.c.getText());
        this.f.setVisibility(i3);
        b(z4);
        this.d.setVisibility(z2 ? 8 : 0);
        ImageView imageView = this.j;
        if (!this.u) {
            i2 = 0;
        }
        imageView.setVisibility(i2);
        h();
        if (z4) {
            z3 = false;
        }
        c(z3);
        g();
    }

    static boolean a(Context context) {
        return context.getResources().getConfiguration().orientation == 2;
    }

    private CharSequence b(CharSequence charSequence) {
        if (!this.u) {
            return charSequence;
        }
        Drawable a2 = this.I.a(this.k);
        int textSize = (int) (((double) this.c.getTextSize()) * 1.25d);
        a2.setBounds(0, 0, textSize, textSize);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder("   ");
        spannableStringBuilder.append(charSequence);
        spannableStringBuilder.setSpan(new ImageSpan(a2), 1, 2, 33);
        return spannableStringBuilder;
    }

    private void b(boolean z2) {
        int i2 = 8;
        if (this.x && f() && hasFocus() && (z2 || !this.C)) {
            i2 = 0;
        }
        this.g.setVisibility(i2);
    }

    private void c(boolean z2) {
        int i2;
        if (!this.C || c() || !z2) {
            i2 = 8;
        } else {
            i2 = 0;
            this.g.setVisibility(8);
        }
        this.i.setVisibility(i2);
    }

    @TargetApi(8)
    private boolean e() {
        if (this.G == null || !this.G.getVoiceSearchEnabled()) {
            return false;
        }
        Intent intent = null;
        if (this.G.getVoiceSearchLaunchWebSearch()) {
            intent = this.n;
        } else if (this.G.getVoiceSearchLaunchRecognizer()) {
            intent = this.o;
        }
        return (intent == null || getContext().getPackageManager().resolveActivity(intent, 65536) == null) ? false : true;
    }

    private boolean f() {
        return (this.x || this.C) && !c();
    }

    private void g() {
        int i2 = 8;
        if (f() && (this.g.getVisibility() == 0 || this.i.getVisibility() == 0)) {
            i2 = 0;
        }
        this.e.setVisibility(i2);
    }

    private int getPreferredWidth() {
        return getContext().getResources().getDimensionPixelSize(android.support.v7.a.e.abc_search_view_preferred_width);
    }

    private void h() {
        boolean z2 = true;
        int i2 = 0;
        boolean z3 = !TextUtils.isEmpty(this.c.getText());
        if (!z3 && (!this.u || this.E)) {
            z2 = false;
        }
        ImageView imageView = this.h;
        if (!z2) {
            i2 = 8;
        }
        imageView.setVisibility(i2);
        this.h.getDrawable().setState(z3 ? ENABLED_STATE_SET : EMPTY_STATE_SET);
    }

    private void i() {
        post(this.K);
    }

    private void k() {
        if (this.y != null) {
            this.c.setHint(b(this.y));
        } else if (!b || this.G == null) {
            this.c.setHint(b(""));
        } else {
            String str = null;
            int hintId = this.G.getHintId();
            if (hintId != 0) {
                str = getContext().getString(hintId);
            }
            if (str != null) {
                this.c.setHint(b(str));
            }
        }
    }

    @TargetApi(8)
    private void l() {
        int i2 = 1;
        this.c.setThreshold(this.G.getSuggestThreshold());
        this.c.setImeOptions(this.G.getImeOptions());
        int inputType = this.G.getInputType();
        if ((inputType & 15) == 1) {
            inputType &= -65537;
            if (this.G.getSuggestAuthority() != null) {
                inputType = inputType | 65536 | 524288;
            }
        }
        this.c.setInputType(inputType);
        if (this.w != null) {
            this.w.a((Cursor) null);
        }
        if (this.G.getSuggestAuthority() != null) {
            this.w = new ah(getContext(), this, this.G, this.M);
            this.c.setAdapter(this.w);
            ah ahVar = (ah) this.w;
            if (this.z) {
                i2 = 2;
            }
            ahVar.a(i2);
        }
    }

    private void m() {
        Editable text = this.c.getText();
        if (text != null && TextUtils.getTrimmedLength(text) > 0) {
            if (this.p == null || !this.p.a(text.toString())) {
                if (this.G != null) {
                    a(0, null, text.toString());
                }
                setImeVisibility(false);
                n();
            }
        }
    }

    private void n() {
        this.c.dismissDropDown();
    }

    private void o() {
        if (!TextUtils.isEmpty(this.c.getText())) {
            this.c.setText("");
            this.c.requestFocus();
            setImeVisibility(true);
        } else if (!this.u) {
        } else {
            if (this.q == null || !this.q.a()) {
                clearFocus();
                a(true);
            }
        }
    }

    private void p() {
        a(false);
        this.c.requestFocus();
        setImeVisibility(true);
        if (this.t != null) {
            this.t.onClick(this);
        }
    }

    private void q() {
        f202a.a(this.c);
        f202a.b(this.c);
    }

    /* access modifiers changed from: private */
    public void setImeVisibility(boolean z2) {
        if (z2) {
            post(this.J);
            return;
        }
        removeCallbacks(this.J);
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
        }
    }

    private void setQuery(CharSequence charSequence) {
        this.c.setText(charSequence);
        this.c.setSelection(TextUtils.isEmpty(charSequence) ? 0 : charSequence.length());
    }

    public void a() {
        if (!this.E) {
            this.E = true;
            this.F = this.c.getImeOptions();
            this.c.setImeOptions(this.F | 33554432);
            this.c.setText("");
            setIconified(false);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(CharSequence charSequence) {
        setQuery(charSequence);
    }

    public void a(CharSequence charSequence, boolean z2) {
        this.c.setText(charSequence);
        if (charSequence != null) {
            this.c.setSelection(this.c.length());
            this.D = charSequence;
        }
        if (z2 && !TextUtils.isEmpty(charSequence)) {
            m();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.SearchView.a(java.lang.CharSequence, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      android.support.v7.widget.SearchView.a(android.support.v7.widget.SearchView, boolean):void
      android.support.v7.widget.LinearLayoutCompat.a(android.view.View, int):int
      android.support.v7.widget.LinearLayoutCompat.a(int, int):void
      android.support.v7.widget.LinearLayoutCompat.a(android.graphics.Canvas, int):void
      android.support.v7.widget.SearchView.a(java.lang.CharSequence, boolean):void */
    public void b() {
        a((CharSequence) "", false);
        clearFocus();
        a(true);
        this.c.setImeOptions(this.F);
        this.E = false;
    }

    public boolean c() {
        return this.v;
    }

    public void clearFocus() {
        this.A = true;
        setImeVisibility(false);
        super.clearFocus();
        this.c.clearFocus();
        this.A = false;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        a(c());
        i();
        if (this.c.hasFocus()) {
            q();
        }
    }

    public int getImeOptions() {
        return this.c.getImeOptions();
    }

    public int getInputType() {
        return this.c.getInputType();
    }

    public int getMaxWidth() {
        return this.B;
    }

    public CharSequence getQuery() {
        return this.c.getText();
    }

    public CharSequence getQueryHint() {
        int hintId;
        if (this.y != null) {
            return this.y;
        }
        if (!b || this.G == null || (hintId = this.G.getHintId()) == 0) {
            return null;
        }
        return getContext().getString(hintId);
    }

    /* access modifiers changed from: package-private */
    public int getSuggestionCommitIconResId() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public int getSuggestionRowLayout() {
        return this.l;
    }

    public e getSuggestionsAdapter() {
        return this.w;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        removeCallbacks(this.K);
        post(this.L);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        if (c()) {
            super.onMeasure(i2, i3);
            return;
        }
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        switch (mode) {
            case Integer.MIN_VALUE:
                if (this.B <= 0) {
                    size = Math.min(getPreferredWidth(), size);
                    break;
                } else {
                    size = Math.min(this.B, size);
                    break;
                }
            case 0:
                if (this.B <= 0) {
                    size = getPreferredWidth();
                    break;
                } else {
                    size = this.B;
                    break;
                }
            case 1073741824:
                if (this.B > 0) {
                    size = Math.min(this.B, size);
                    break;
                }
                break;
        }
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(size, 1073741824), i3);
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        i();
    }

    public boolean requestFocus(int i2, Rect rect) {
        if (this.A || !isFocusable()) {
            return false;
        }
        if (c()) {
            return super.requestFocus(i2, rect);
        }
        boolean requestFocus = this.c.requestFocus(i2, rect);
        if (requestFocus) {
            a(false);
        }
        return requestFocus;
    }

    public void setAppSearchData(Bundle bundle) {
        this.H = bundle;
    }

    public void setIconified(boolean z2) {
        if (z2) {
            o();
        } else {
            p();
        }
    }

    public void setIconifiedByDefault(boolean z2) {
        if (this.u != z2) {
            this.u = z2;
            a(z2);
            k();
        }
    }

    public void setImeOptions(int i2) {
        this.c.setImeOptions(i2);
    }

    public void setInputType(int i2) {
        this.c.setInputType(i2);
    }

    public void setMaxWidth(int i2) {
        this.B = i2;
        requestLayout();
    }

    public void setOnCloseListener(ae aeVar) {
        this.q = aeVar;
    }

    public void setOnQueryTextFocusChangeListener(View.OnFocusChangeListener onFocusChangeListener) {
        this.r = onFocusChangeListener;
    }

    public void setOnQueryTextListener(af afVar) {
        this.p = afVar;
    }

    public void setOnSearchClickListener(View.OnClickListener onClickListener) {
        this.t = onClickListener;
    }

    public void setOnSuggestionListener(ag agVar) {
        this.s = agVar;
    }

    public void setQueryHint(CharSequence charSequence) {
        this.y = charSequence;
        k();
    }

    public void setQueryRefinementEnabled(boolean z2) {
        this.z = z2;
        if (this.w instanceof ah) {
            ((ah) this.w).a(z2 ? 2 : 1);
        }
    }

    public void setSearchableInfo(SearchableInfo searchableInfo) {
        this.G = searchableInfo;
        if (this.G != null) {
            if (b) {
                l();
            }
            k();
        }
        this.C = b && e();
        if (this.C) {
            this.c.setPrivateImeOptions("nm");
        }
        a(c());
    }

    public void setSubmitButtonEnabled(boolean z2) {
        this.x = z2;
        a(c());
    }

    public void setSuggestionsAdapter(e eVar) {
        this.w = eVar;
        this.c.setAdapter(this.w);
    }
}
