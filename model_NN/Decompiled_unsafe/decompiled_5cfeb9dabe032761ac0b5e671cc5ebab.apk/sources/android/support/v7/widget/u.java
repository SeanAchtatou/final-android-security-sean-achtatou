package android.support.v7.widget;

import android.content.Context;
import android.support.v4.view.cf;
import android.support.v4.widget.y;
import android.support.v7.a.b;
import android.support.v7.internal.widget.ab;
import android.view.View;

class u extends ab {
    /* access modifiers changed from: private */
    public boolean f;
    private boolean g;
    private boolean h;
    private cf i;
    private y j;

    public u(Context context, boolean z) {
        super(context, null, b.dropDownListViewStyle);
        this.g = z;
        setCacheColorHint(0);
    }

    private void a(View view, int i2) {
        performItemClick(view, i2, getItemIdAtPosition(i2));
    }

    private void a(View view, int i2, float f2, float f3) {
        this.h = true;
        setPressed(true);
        layoutChildren();
        setSelection(i2);
        a(i2, view, f2, f3);
        setSelectorEnabled(false);
        refreshDrawableState();
    }

    private void d() {
        this.h = false;
        setPressed(false);
        drawableStateChanged();
        if (this.i != null) {
            this.i.a();
            this.i = null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0037  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.view.MotionEvent r9, int r10) {
        /*
            r8 = this;
            r2 = 1
            r1 = 0
            int r3 = android.support.v4.view.ai.a(r9)
            switch(r3) {
                case 1: goto L_0x002d;
                case 2: goto L_0x006a;
                case 3: goto L_0x002a;
                default: goto L_0x0009;
            }
        L_0x0009:
            r0 = r1
            r3 = r2
        L_0x000b:
            if (r3 == 0) goto L_0x000f
            if (r0 == 0) goto L_0x0012
        L_0x000f:
            r8.d()
        L_0x0012:
            if (r3 == 0) goto L_0x0060
            android.support.v4.widget.y r0 = r8.j
            if (r0 != 0) goto L_0x001f
            android.support.v4.widget.y r0 = new android.support.v4.widget.y
            r0.<init>(r8)
            r8.j = r0
        L_0x001f:
            android.support.v4.widget.y r0 = r8.j
            r0.a(r2)
            android.support.v4.widget.y r0 = r8.j
            r0.onTouch(r8, r9)
        L_0x0029:
            return r3
        L_0x002a:
            r0 = r1
            r3 = r1
            goto L_0x000b
        L_0x002d:
            r0 = r1
        L_0x002e:
            int r4 = r9.findPointerIndex(r10)
            if (r4 >= 0) goto L_0x0037
            r0 = r1
            r3 = r1
            goto L_0x000b
        L_0x0037:
            float r5 = r9.getX(r4)
            int r5 = (int) r5
            float r4 = r9.getY(r4)
            int r4 = (int) r4
            int r6 = r8.pointToPosition(r5, r4)
            r7 = -1
            if (r6 != r7) goto L_0x004b
            r3 = r0
            r0 = r2
            goto L_0x000b
        L_0x004b:
            int r0 = r8.getFirstVisiblePosition()
            int r0 = r6 - r0
            android.view.View r0 = r8.getChildAt(r0)
            float r5 = (float) r5
            float r4 = (float) r4
            r8.a(r0, r6, r5, r4)
            if (r3 != r2) goto L_0x0009
            r8.a(r0, r6)
            goto L_0x0009
        L_0x0060:
            android.support.v4.widget.y r0 = r8.j
            if (r0 == 0) goto L_0x0029
            android.support.v4.widget.y r0 = r8.j
            r0.a(r1)
            goto L_0x0029
        L_0x006a:
            r0 = r2
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.u.a(android.view.MotionEvent, int):boolean");
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return this.h || super.c();
    }

    public boolean hasFocus() {
        return this.g || super.hasFocus();
    }

    public boolean hasWindowFocus() {
        return this.g || super.hasWindowFocus();
    }

    public boolean isFocused() {
        return this.g || super.isFocused();
    }

    public boolean isInTouchMode() {
        return (this.g && this.f) || super.isInTouchMode();
    }
}
