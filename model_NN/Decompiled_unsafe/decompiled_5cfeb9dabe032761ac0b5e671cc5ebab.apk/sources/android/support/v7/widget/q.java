package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.e.f;
import android.support.v4.widget.z;
import android.support.v7.a.l;
import android.support.v7.internal.widget.u;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import java.lang.reflect.Method;

public class q {

    /* renamed from: a  reason: collision with root package name */
    private static Method f223a;
    /* access modifiers changed from: private */
    public Handler A;
    private Rect B;
    private boolean C;
    private int D;
    int b;
    private Context c;
    /* access modifiers changed from: private */
    public PopupWindow d;
    private ListAdapter e;
    /* access modifiers changed from: private */
    public u f;
    private int g;
    private int h;
    private int i;
    private int j;
    private boolean k;
    private int l;
    private boolean m;
    private boolean n;
    private View o;
    private int p;
    private DataSetObserver q;
    private View r;
    private Drawable s;
    private AdapterView.OnItemClickListener t;
    private AdapterView.OnItemSelectedListener u;
    /* access modifiers changed from: private */
    public final ac v;
    private final ab w;
    private final aa x;
    private final y y;
    private Runnable z;

    static {
        Class<PopupWindow> cls = PopupWindow.class;
        try {
            f223a = cls.getDeclaredMethod("setClipToScreenEnabled", Boolean.TYPE);
        } catch (NoSuchMethodException e2) {
            Log.i("ListPopupWindow", "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well.");
        }
    }

    public q(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, 0);
    }

    public q(Context context, AttributeSet attributeSet, int i2, int i3) {
        this.g = -2;
        this.h = -2;
        this.l = 0;
        this.m = false;
        this.n = false;
        this.b = Integer.MAX_VALUE;
        this.p = 0;
        this.v = new ac(this, null);
        this.w = new ab(this, null);
        this.x = new aa(this, null);
        this.y = new y(this, null);
        this.A = new Handler();
        this.B = new Rect();
        this.c = context;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.ListPopupWindow, i2, i3);
        this.i = obtainStyledAttributes.getDimensionPixelOffset(l.ListPopupWindow_android_dropDownHorizontalOffset, 0);
        this.j = obtainStyledAttributes.getDimensionPixelOffset(l.ListPopupWindow_android_dropDownVerticalOffset, 0);
        if (this.j != 0) {
            this.k = true;
        }
        obtainStyledAttributes.recycle();
        this.d = new u(context, attributeSet, i2);
        this.d.setInputMethodMode(1);
        this.D = f.a(this.c.getResources().getConfiguration().locale);
    }

    private void b(boolean z2) {
        if (f223a != null) {
            try {
                f223a.invoke(this.d, Boolean.valueOf(z2));
            } catch (Exception e2) {
                Log.i("ListPopupWindow", "Could not call setClipToScreenEnabled() on PopupWindow. Oh well.");
            }
        }
    }

    private void h() {
        if (this.o != null) {
            ViewParent parent = this.o.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.o);
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v23, resolved type: android.support.v7.widget.u} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v24, resolved type: android.support.v7.widget.u} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v10, resolved type: android.widget.LinearLayout} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v28, resolved type: android.support.v7.widget.u} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int i() {
        /*
            r10 = this;
            r9 = 1073741824(0x40000000, float:2.0)
            r8 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = 1
            r3 = -1
            r2 = 0
            android.support.v7.widget.u r0 = r10.f
            if (r0 != 0) goto L_0x0106
            android.content.Context r5 = r10.c
            android.support.v7.widget.s r0 = new android.support.v7.widget.s
            r0.<init>(r10)
            r10.z = r0
            android.support.v7.widget.u r4 = new android.support.v7.widget.u
            boolean r0 = r10.C
            if (r0 != 0) goto L_0x00f5
            r0 = r1
        L_0x001b:
            r4.<init>(r5, r0)
            r10.f = r4
            android.graphics.drawable.Drawable r0 = r10.s
            if (r0 == 0) goto L_0x002b
            android.support.v7.widget.u r0 = r10.f
            android.graphics.drawable.Drawable r4 = r10.s
            r0.setSelector(r4)
        L_0x002b:
            android.support.v7.widget.u r0 = r10.f
            android.widget.ListAdapter r4 = r10.e
            r0.setAdapter(r4)
            android.support.v7.widget.u r0 = r10.f
            android.widget.AdapterView$OnItemClickListener r4 = r10.t
            r0.setOnItemClickListener(r4)
            android.support.v7.widget.u r0 = r10.f
            r0.setFocusable(r1)
            android.support.v7.widget.u r0 = r10.f
            r0.setFocusableInTouchMode(r1)
            android.support.v7.widget.u r0 = r10.f
            android.support.v7.widget.t r4 = new android.support.v7.widget.t
            r4.<init>(r10)
            r0.setOnItemSelectedListener(r4)
            android.support.v7.widget.u r0 = r10.f
            android.support.v7.widget.aa r4 = r10.x
            r0.setOnScrollListener(r4)
            android.widget.AdapterView$OnItemSelectedListener r0 = r10.u
            if (r0 == 0) goto L_0x005f
            android.support.v7.widget.u r0 = r10.f
            android.widget.AdapterView$OnItemSelectedListener r4 = r10.u
            r0.setOnItemSelectedListener(r4)
        L_0x005f:
            android.support.v7.widget.u r0 = r10.f
            android.view.View r6 = r10.o
            if (r6 == 0) goto L_0x017f
            android.widget.LinearLayout r4 = new android.widget.LinearLayout
            r4.<init>(r5)
            r4.setOrientation(r1)
            android.widget.LinearLayout$LayoutParams r1 = new android.widget.LinearLayout$LayoutParams
            r5 = 1065353216(0x3f800000, float:1.0)
            r1.<init>(r3, r2, r5)
            int r5 = r10.p
            switch(r5) {
                case 0: goto L_0x00ff;
                case 1: goto L_0x00f8;
                default: goto L_0x0079;
            }
        L_0x0079:
            java.lang.String r0 = "ListPopupWindow"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r5 = "Invalid hint position "
            java.lang.StringBuilder r1 = r1.append(r5)
            int r5 = r10.p
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r0, r1)
        L_0x0093:
            int r0 = r10.h
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r8)
            r6.measure(r0, r2)
            android.view.ViewGroup$LayoutParams r0 = r6.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r0 = (android.widget.LinearLayout.LayoutParams) r0
            int r1 = r6.getMeasuredHeight()
            int r5 = r0.topMargin
            int r1 = r1 + r5
            int r0 = r0.bottomMargin
            int r0 = r0 + r1
            r1 = r4
        L_0x00ad:
            android.widget.PopupWindow r4 = r10.d
            r4.setContentView(r1)
            r6 = r0
        L_0x00b3:
            android.widget.PopupWindow r0 = r10.d
            android.graphics.drawable.Drawable r0 = r0.getBackground()
            if (r0 == 0) goto L_0x0124
            android.graphics.Rect r1 = r10.B
            r0.getPadding(r1)
            android.graphics.Rect r0 = r10.B
            int r0 = r0.top
            android.graphics.Rect r1 = r10.B
            int r1 = r1.bottom
            int r0 = r0 + r1
            boolean r1 = r10.k
            if (r1 != 0) goto L_0x0179
            android.graphics.Rect r1 = r10.B
            int r1 = r1.top
            int r1 = -r1
            r10.j = r1
            r7 = r0
        L_0x00d5:
            android.widget.PopupWindow r0 = r10.d
            int r0 = r0.getInputMethodMode()
            r1 = 2
            if (r0 != r1) goto L_0x00de
        L_0x00de:
            android.widget.PopupWindow r0 = r10.d
            android.view.View r1 = r10.d()
            int r4 = r10.j
            int r4 = r0.getMaxAvailableHeight(r1, r4)
            boolean r0 = r10.m
            if (r0 != 0) goto L_0x00f2
            int r0 = r10.g
            if (r0 != r3) goto L_0x012b
        L_0x00f2:
            int r0 = r4 + r7
        L_0x00f4:
            return r0
        L_0x00f5:
            r0 = r2
            goto L_0x001b
        L_0x00f8:
            r4.addView(r0, r1)
            r4.addView(r6)
            goto L_0x0093
        L_0x00ff:
            r4.addView(r6)
            r4.addView(r0, r1)
            goto L_0x0093
        L_0x0106:
            android.widget.PopupWindow r0 = r10.d
            android.view.View r0 = r0.getContentView()
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            android.view.View r1 = r10.o
            if (r1 == 0) goto L_0x017c
            android.view.ViewGroup$LayoutParams r0 = r1.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r0 = (android.widget.LinearLayout.LayoutParams) r0
            int r1 = r1.getMeasuredHeight()
            int r4 = r0.topMargin
            int r1 = r1 + r4
            int r0 = r0.bottomMargin
            int r0 = r0 + r1
            r6 = r0
            goto L_0x00b3
        L_0x0124:
            android.graphics.Rect r0 = r10.B
            r0.setEmpty()
            r7 = r2
            goto L_0x00d5
        L_0x012b:
            int r0 = r10.h
            switch(r0) {
                case -2: goto L_0x0143;
                case -1: goto L_0x015e;
                default: goto L_0x0130;
            }
        L_0x0130:
            int r0 = r10.h
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r9)
        L_0x0136:
            android.support.v7.widget.u r0 = r10.f
            int r4 = r4 - r6
            r5 = r3
            int r0 = r0.a(r1, r2, r3, r4, r5)
            if (r0 <= 0) goto L_0x0141
            int r6 = r6 + r7
        L_0x0141:
            int r0 = r0 + r6
            goto L_0x00f4
        L_0x0143:
            android.content.Context r0 = r10.c
            android.content.res.Resources r0 = r0.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            int r0 = r0.widthPixels
            android.graphics.Rect r1 = r10.B
            int r1 = r1.left
            android.graphics.Rect r5 = r10.B
            int r5 = r5.right
            int r1 = r1 + r5
            int r0 = r0 - r1
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r8)
            goto L_0x0136
        L_0x015e:
            android.content.Context r0 = r10.c
            android.content.res.Resources r0 = r0.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            int r0 = r0.widthPixels
            android.graphics.Rect r1 = r10.B
            int r1 = r1.left
            android.graphics.Rect r5 = r10.B
            int r5 = r5.right
            int r1 = r1 + r5
            int r0 = r0 - r1
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r9)
            goto L_0x0136
        L_0x0179:
            r7 = r0
            goto L_0x00d5
        L_0x017c:
            r6 = r2
            goto L_0x00b3
        L_0x017f:
            r1 = r0
            r0 = r2
            goto L_0x00ad
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.q.i():int");
    }

    public void a() {
        this.d.dismiss();
        h();
        this.d.setContentView(null);
        this.f = null;
        this.A.removeCallbacks(this.v);
    }

    public void a(int i2) {
        this.p = i2;
    }

    public void a(Drawable drawable) {
        this.d.setBackgroundDrawable(drawable);
    }

    public void a(View view) {
        this.r = view;
    }

    public void a(AdapterView.OnItemClickListener onItemClickListener) {
        this.t = onItemClickListener;
    }

    public void a(ListAdapter listAdapter) {
        if (this.q == null) {
            this.q = new z(this, null);
        } else if (this.e != null) {
            this.e.unregisterDataSetObserver(this.q);
        }
        this.e = listAdapter;
        if (this.e != null) {
            listAdapter.registerDataSetObserver(this.q);
        }
        if (this.f != null) {
            this.f.setAdapter(this.e);
        }
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.d.setOnDismissListener(onDismissListener);
    }

    public void a(boolean z2) {
        this.C = z2;
        this.d.setFocusable(z2);
    }

    public void b(int i2) {
        this.l = i2;
    }

    public boolean b() {
        return this.d.isShowing();
    }

    public void c() {
        int i2;
        int i3;
        boolean z2 = true;
        boolean z3 = false;
        int i4 = -1;
        int i5 = i();
        boolean f2 = f();
        if (this.d.isShowing()) {
            int width = this.h == -1 ? -1 : this.h == -2 ? d().getWidth() : this.h;
            if (this.g == -1) {
                if (!f2) {
                    i5 = -1;
                }
                if (f2) {
                    PopupWindow popupWindow = this.d;
                    if (this.h != -1) {
                        i4 = 0;
                    }
                    popupWindow.setWindowLayoutMode(i4, 0);
                } else {
                    this.d.setWindowLayoutMode(this.h == -1 ? -1 : 0, -1);
                }
            } else if (this.g != -2) {
                i5 = this.g;
            }
            PopupWindow popupWindow2 = this.d;
            if (!this.n && !this.m) {
                z3 = true;
            }
            popupWindow2.setOutsideTouchable(z3);
            this.d.update(d(), this.i, this.j, width, i5);
            return;
        }
        if (this.h == -1) {
            i2 = -1;
        } else if (this.h == -2) {
            this.d.setWidth(d().getWidth());
            i2 = 0;
        } else {
            this.d.setWidth(this.h);
            i2 = 0;
        }
        if (this.g == -1) {
            i3 = -1;
        } else if (this.g == -2) {
            this.d.setHeight(i5);
            i3 = 0;
        } else {
            this.d.setHeight(this.g);
            i3 = 0;
        }
        this.d.setWindowLayoutMode(i2, i3);
        b(true);
        PopupWindow popupWindow3 = this.d;
        if (this.n || this.m) {
            z2 = false;
        }
        popupWindow3.setOutsideTouchable(z2);
        this.d.setTouchInterceptor(this.w);
        z.a(this.d, d(), this.i, this.j, this.l);
        this.f.setSelection(-1);
        if (!this.C || this.f.isInTouchMode()) {
            e();
        }
        if (!this.C) {
            this.A.post(this.y);
        }
    }

    public void c(int i2) {
        this.h = i2;
    }

    public View d() {
        return this.r;
    }

    public void d(int i2) {
        Drawable background = this.d.getBackground();
        if (background != null) {
            background.getPadding(this.B);
            this.h = this.B.left + this.B.right + i2;
            return;
        }
        c(i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.u.a(android.support.v7.widget.u, boolean):boolean
     arg types: [android.support.v7.widget.u, int]
     candidates:
      android.support.v7.widget.u.a(android.view.View, int):void
      android.support.v7.widget.u.a(android.view.MotionEvent, int):boolean
      android.support.v7.internal.widget.ab.a(int, android.view.View):void
      android.support.v7.widget.u.a(android.support.v7.widget.u, boolean):boolean */
    public void e() {
        u uVar = this.f;
        if (uVar != null) {
            boolean unused = uVar.f = true;
            uVar.requestLayout();
        }
    }

    public void e(int i2) {
        this.d.setInputMethodMode(i2);
    }

    public boolean f() {
        return this.d.getInputMethodMode() == 2;
    }

    public ListView g() {
        return this.f;
    }
}
