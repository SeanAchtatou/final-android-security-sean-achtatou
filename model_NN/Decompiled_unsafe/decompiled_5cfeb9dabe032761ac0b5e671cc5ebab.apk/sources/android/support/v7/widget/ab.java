package android.support.v7.widget;

import android.view.MotionEvent;
import android.view.View;

class ab implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f207a;

    private ab(q qVar) {
        this.f207a = qVar;
    }

    /* synthetic */ ab(q qVar, r rVar) {
        this(qVar);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (action == 0 && this.f207a.d != null && this.f207a.d.isShowing() && x >= 0 && x < this.f207a.d.getWidth() && y >= 0 && y < this.f207a.d.getHeight()) {
            this.f207a.A.postDelayed(this.f207a.v, 250);
            return false;
        } else if (action != 1) {
            return false;
        } else {
            this.f207a.A.removeCallbacks(this.f207a.v);
            return false;
        }
    }
}
