package android.support.v7.widget;

import android.support.v7.a.g;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

final class ai {

    /* renamed from: a  reason: collision with root package name */
    public final TextView f210a;
    public final TextView b;
    public final ImageView c;
    public final ImageView d;
    public final ImageView e;

    public ai(View view) {
        this.f210a = (TextView) view.findViewById(16908308);
        this.b = (TextView) view.findViewById(16908309);
        this.c = (ImageView) view.findViewById(16908295);
        this.d = (ImageView) view.findViewById(16908296);
        this.e = (ImageView) view.findViewById(g.edit_query);
    }
}
