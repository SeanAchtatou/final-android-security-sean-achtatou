package android.support.v7.widget;

import android.view.View;

class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ActionMenuPresenter f216a;
    private g b;

    public d(ActionMenuPresenter actionMenuPresenter, g gVar) {
        this.f216a = actionMenuPresenter;
        this.b = gVar;
    }

    public void run() {
        this.f216a.c.f();
        View view = (View) this.f216a.f;
        if (!(view == null || view.getWindowToken() == null || !this.b.d())) {
            g unused = this.f216a.v = this.b;
        }
        d unused2 = this.f216a.x = (d) null;
    }
}
