package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.b.a.a;
import android.support.v7.a.b;
import android.support.v7.internal.widget.TintImageView;

class e extends TintImageView implements k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ActionMenuPresenter f217a;
    private final float[] b = new float[2];

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(ActionMenuPresenter actionMenuPresenter, Context context) {
        super(context, null, b.actionOverflowButtonStyle);
        this.f217a = actionMenuPresenter;
        setClickable(true);
        setFocusable(true);
        setVisibility(0);
        setEnabled(true);
        setOnTouchListener(new f(this, this, actionMenuPresenter));
    }

    public boolean c() {
        return false;
    }

    public boolean d() {
        return false;
    }

    public boolean performClick() {
        if (!super.performClick()) {
            playSoundEffect(0);
            this.f217a.c();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean setFrame(int i, int i2, int i3, int i4) {
        boolean frame = super.setFrame(i, i2, i3, i4);
        Drawable drawable = getDrawable();
        Drawable background = getBackground();
        if (!(drawable == null || background == null)) {
            float[] fArr = this.b;
            fArr[0] = (float) drawable.getBounds().centerX();
            getImageMatrix().mapPoints(fArr);
            int width = ((int) fArr[0]) - (getWidth() / 2);
            a.a(background, width, 0, getWidth() + width, getHeight());
        }
        return frame;
    }
}
