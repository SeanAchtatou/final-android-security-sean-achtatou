package android.support.v7.widget;

import android.database.DataSetObserver;

class z extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ q f231a;

    private z(q qVar) {
        this.f231a = qVar;
    }

    /* synthetic */ z(q qVar, r rVar) {
        this(qVar);
    }

    public void onChanged() {
        if (this.f231a.b()) {
            this.f231a.c();
        }
    }

    public void onInvalidated() {
        this.f231a.a();
    }
}
