package android.support.v7.widget;

import android.view.View;

class f extends v {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ActionMenuPresenter f218a;
    final /* synthetic */ e b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    f(e eVar, View view, ActionMenuPresenter actionMenuPresenter) {
        super(view);
        this.b = eVar;
        this.f218a = actionMenuPresenter;
    }

    public q a() {
        if (this.b.f217a.v == null) {
            return null;
        }
        return this.b.f217a.v.c();
    }

    public boolean b() {
        this.b.f217a.c();
        return true;
    }

    public boolean c() {
        if (this.b.f217a.x != null) {
            return false;
        }
        this.b.f217a.d();
        return true;
    }
}
