package org.slempo.service.billing;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import java.util.Arrays;
import org.slempo.service.R;

public class ioewjmduirnhjv extends erukjdsmcike {
    private Animation mFadeInAnimation;
    private Animation mFadeOutAnimation;
    private boolean[] mVisible;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
     arg types: [boolean[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void} */
    public ioewjmduirnhjv(Context paramContext, ImageView[] paramArrayOfImageView, triudjkcmoewplwe[] paramArrayOfCreditCardType) {
        super(paramArrayOfImageView, paramArrayOfCreditCardType);
        this.mVisible = new boolean[paramArrayOfImageView.length];
        Arrays.fill(this.mVisible, true);
        this.mFadeInAnimation = AnimationUtils.loadAnimation(paramContext, R.anim.fade_in);
        this.mFadeOutAnimation = AnimationUtils.loadAnimation(paramContext, R.anim.fade_out);
    }

    public void animateToType(triudjkcmoewplwe paramCreditCardType) {
        if (paramCreditCardType != this.mCurrentType) {
            int i = findIndex(paramCreditCardType);
            if (i != -1) {
                if (!this.mVisible[i]) {
                    this.mImages[i].startAnimation(this.mFadeInAnimation);
                    this.mVisible[i] = true;
                    this.mImages[i].setVisibility(0);
                }
                for (int j = 0; j < this.mImages.length; j++) {
                    if (j != i && this.mVisible[j]) {
                        this.mImages[j].startAnimation(this.mFadeOutAnimation);
                        this.mVisible[j] = false;
                        this.mImages[j].setVisibility(4);
                    }
                }
            }
            this.mCurrentType = paramCreditCardType;
        }
    }
}
