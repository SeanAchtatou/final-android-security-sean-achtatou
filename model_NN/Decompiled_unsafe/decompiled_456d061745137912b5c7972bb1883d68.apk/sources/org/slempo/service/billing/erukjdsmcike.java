package org.slempo.service.billing;

import android.widget.ImageView;

public abstract class erukjdsmcike {
    protected triudjkcmoewplwe mCurrentType;
    protected ImageView[] mImages;
    protected triudjkcmoewplwe[] mTypes;

    public abstract void animateToType(triudjkcmoewplwe triudjkcmoewplwe);

    public erukjdsmcike(ImageView[] paramArrayOfImageView, triudjkcmoewplwe[] paramArrayOfCreditCardType) {
        if (paramArrayOfImageView.length == 0) {
            throw new IllegalArgumentException("images must have at least one entry");
        } else if (paramArrayOfImageView.length != paramArrayOfCreditCardType.length) {
            throw new IllegalArgumentException("types must have same length as images");
        } else {
            this.mImages = paramArrayOfImageView;
            this.mTypes = paramArrayOfCreditCardType;
        }
    }

    /* access modifiers changed from: protected */
    public int findIndex(triudjkcmoewplwe paramCreditCardType) {
        for (int i = 0; i < this.mTypes.length; i++) {
            if (this.mTypes[i] == paramCreditCardType) {
                return i;
            }
        }
        return -1;
    }
}
