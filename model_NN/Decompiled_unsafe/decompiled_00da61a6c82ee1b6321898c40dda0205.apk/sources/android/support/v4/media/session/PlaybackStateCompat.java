package android.support.v4.media.session;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public final class PlaybackStateCompat implements Parcelable {
    public static final Parcelable.Creator CREATOR = new fxug2rdnfo();
    private final long cehyzt7dw;
    private final long e8kxjqktk9t;
    private final CharSequence fxug2rdnfo;
    private final long ozpoxuz523b2;
    private final int ttmhx7;
    private final float uin6g3d5rqgcbs;
    private final long usuayu88rw4;

    public final class CustomAction implements Parcelable {
        public static final Parcelable.Creator CREATOR = new e8kxjqktk9t();
        private final int cehyzt7dw;
        private final CharSequence ozpoxuz523b2;
        private final String ttmhx7;
        private final Bundle uin6g3d5rqgcbs;

        private CustomAction(Parcel parcel) {
            this.ttmhx7 = parcel.readString();
            this.ozpoxuz523b2 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
            this.cehyzt7dw = parcel.readInt();
            this.uin6g3d5rqgcbs = parcel.readBundle();
        }

        /* synthetic */ CustomAction(Parcel parcel, fxug2rdnfo fxug2rdnfo) {
            this(parcel);
        }

        public int describeContents() {
            return 0;
        }

        public String toString() {
            return "Action:mName='" + ((Object) this.ozpoxuz523b2) + ", mIcon=" + this.cehyzt7dw + ", mExtras=" + this.uin6g3d5rqgcbs;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.ttmhx7);
            TextUtils.writeToParcel(this.ozpoxuz523b2, parcel, i);
            parcel.writeInt(this.cehyzt7dw);
            parcel.writeBundle(this.uin6g3d5rqgcbs);
        }
    }

    private PlaybackStateCompat(Parcel parcel) {
        this.ttmhx7 = parcel.readInt();
        this.ozpoxuz523b2 = parcel.readLong();
        this.uin6g3d5rqgcbs = parcel.readFloat();
        this.e8kxjqktk9t = parcel.readLong();
        this.cehyzt7dw = parcel.readLong();
        this.usuayu88rw4 = parcel.readLong();
        this.fxug2rdnfo = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
    }

    /* synthetic */ PlaybackStateCompat(Parcel parcel, fxug2rdnfo fxug2rdnfo2) {
        this(parcel);
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("PlaybackState {");
        sb.append("state=").append(this.ttmhx7);
        sb.append(", position=").append(this.ozpoxuz523b2);
        sb.append(", buffered position=").append(this.cehyzt7dw);
        sb.append(", speed=").append(this.uin6g3d5rqgcbs);
        sb.append(", updated=").append(this.e8kxjqktk9t);
        sb.append(", actions=").append(this.usuayu88rw4);
        sb.append(", error=").append(this.fxug2rdnfo);
        sb.append("}");
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.ttmhx7);
        parcel.writeLong(this.ozpoxuz523b2);
        parcel.writeFloat(this.uin6g3d5rqgcbs);
        parcel.writeLong(this.e8kxjqktk9t);
        parcel.writeLong(this.cehyzt7dw);
        parcel.writeLong(this.usuayu88rw4);
        TextUtils.writeToParcel(this.fxug2rdnfo, parcel, i);
    }
}
