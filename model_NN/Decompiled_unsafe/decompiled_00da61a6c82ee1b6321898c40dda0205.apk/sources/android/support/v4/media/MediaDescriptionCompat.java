package android.support.v4.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public final class MediaDescriptionCompat implements Parcelable {
    public static final Parcelable.Creator CREATOR = new ttmhx7();
    private final CharSequence cehyzt7dw;
    private final Bundle e8kxjqktk9t;
    private final Uri fxug2rdnfo;
    private Object lg71ytkvzw;
    private final CharSequence ozpoxuz523b2;
    private final String ttmhx7;
    private final CharSequence uin6g3d5rqgcbs;
    private final Bitmap usuayu88rw4;

    private MediaDescriptionCompat(Parcel parcel) {
        this.ttmhx7 = parcel.readString();
        this.ozpoxuz523b2 = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.cehyzt7dw = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.uin6g3d5rqgcbs = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.usuayu88rw4 = (Bitmap) parcel.readParcelable(null);
        this.fxug2rdnfo = (Uri) parcel.readParcelable(null);
        this.e8kxjqktk9t = parcel.readBundle();
    }

    /* synthetic */ MediaDescriptionCompat(Parcel parcel, ttmhx7 ttmhx72) {
        this(parcel);
    }

    private MediaDescriptionCompat(String str, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, Bitmap bitmap, Uri uri, Bundle bundle) {
        this.ttmhx7 = str;
        this.ozpoxuz523b2 = charSequence;
        this.cehyzt7dw = charSequence2;
        this.uin6g3d5rqgcbs = charSequence3;
        this.usuayu88rw4 = bitmap;
        this.fxug2rdnfo = uri;
        this.e8kxjqktk9t = bundle;
    }

    /* synthetic */ MediaDescriptionCompat(String str, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, Bitmap bitmap, Uri uri, Bundle bundle, ttmhx7 ttmhx72) {
        this(str, charSequence, charSequence2, charSequence3, bitmap, uri, bundle);
    }

    public static MediaDescriptionCompat ttmhx7(Object obj) {
        if (obj == null || Build.VERSION.SDK_INT < 21) {
            return null;
        }
        ozpoxuz523b2 ozpoxuz523b22 = new ozpoxuz523b2();
        ozpoxuz523b22.ttmhx7(cehyzt7dw.ttmhx7(obj));
        ozpoxuz523b22.ttmhx7(cehyzt7dw.ozpoxuz523b2(obj));
        ozpoxuz523b22.ozpoxuz523b2(cehyzt7dw.cehyzt7dw(obj));
        ozpoxuz523b22.cehyzt7dw(cehyzt7dw.uin6g3d5rqgcbs(obj));
        ozpoxuz523b22.ttmhx7(cehyzt7dw.usuayu88rw4(obj));
        ozpoxuz523b22.ttmhx7(cehyzt7dw.fxug2rdnfo(obj));
        ozpoxuz523b22.ttmhx7(cehyzt7dw.e8kxjqktk9t(obj));
        MediaDescriptionCompat ttmhx72 = ozpoxuz523b22.ttmhx7();
        ttmhx72.lg71ytkvzw = obj;
        return ttmhx72;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return ((Object) this.ozpoxuz523b2) + ", " + ((Object) this.cehyzt7dw) + ", " + ((Object) this.uin6g3d5rqgcbs);
    }

    public Object ttmhx7() {
        if (this.lg71ytkvzw != null || Build.VERSION.SDK_INT < 21) {
            return this.lg71ytkvzw;
        }
        Object ttmhx72 = uin6g3d5rqgcbs.ttmhx7();
        uin6g3d5rqgcbs.ttmhx7(ttmhx72, this.ttmhx7);
        uin6g3d5rqgcbs.ttmhx7(ttmhx72, this.ozpoxuz523b2);
        uin6g3d5rqgcbs.ozpoxuz523b2(ttmhx72, this.cehyzt7dw);
        uin6g3d5rqgcbs.cehyzt7dw(ttmhx72, this.uin6g3d5rqgcbs);
        uin6g3d5rqgcbs.ttmhx7(ttmhx72, this.usuayu88rw4);
        uin6g3d5rqgcbs.ttmhx7(ttmhx72, this.fxug2rdnfo);
        uin6g3d5rqgcbs.ttmhx7(ttmhx72, this.e8kxjqktk9t);
        this.lg71ytkvzw = uin6g3d5rqgcbs.ttmhx7(ttmhx72);
        return this.lg71ytkvzw;
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (Build.VERSION.SDK_INT < 21) {
            parcel.writeString(this.ttmhx7);
            TextUtils.writeToParcel(this.ozpoxuz523b2, parcel, i);
            TextUtils.writeToParcel(this.cehyzt7dw, parcel, i);
            TextUtils.writeToParcel(this.uin6g3d5rqgcbs, parcel, i);
            parcel.writeParcelable(this.usuayu88rw4, i);
            parcel.writeParcelable(this.fxug2rdnfo, i);
            parcel.writeBundle(this.e8kxjqktk9t);
            return;
        }
        cehyzt7dw.ttmhx7(ttmhx7(), parcel, i);
    }
}
