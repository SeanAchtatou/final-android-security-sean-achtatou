package android.support.v4.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.c.ay6ebym1yp0qgk;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;

public class oc9mgl157cp extends Activity {
    ay6ebym1yp0qgk ay6ebym1yp0qgk;
    boolean b5zlaptmyxarl;
    final ca2ssr26fefu cehyzt7dw;
    boolean e8kxjqktk9t;
    boolean ef5tn1cvshg414;
    boolean fxug2rdnfo;
    boolean iux03f6yieb;
    boolean lg71ytkvzw;
    kld4qxthnxo5uo mhtc4dliin7r;
    final rulrdod1midre ozpoxuz523b2;
    final Handler ttmhx7;
    boolean uin6g3d5rqgcbs;
    boolean usuayu88rw4;

    private static String ttmhx7(View view) {
        String str;
        char c = 'F';
        char c2 = '.';
        StringBuilder sb = new StringBuilder(128);
        sb.append(view.getClass().getName());
        sb.append('{');
        sb.append(Integer.toHexString(System.identityHashCode(view)));
        sb.append(' ');
        switch (view.getVisibility()) {
            case 0:
                sb.append('V');
                break;
            case 4:
                sb.append('I');
                break;
            case 8:
                sb.append('G');
                break;
            default:
                sb.append('.');
                break;
        }
        sb.append(view.isFocusable() ? 'F' : '.');
        sb.append(view.isEnabled() ? 'E' : '.');
        sb.append(view.willNotDraw() ? '.' : 'D');
        sb.append(view.isHorizontalScrollBarEnabled() ? 'H' : '.');
        sb.append(view.isVerticalScrollBarEnabled() ? 'V' : '.');
        sb.append(view.isClickable() ? 'C' : '.');
        sb.append(view.isLongClickable() ? 'L' : '.');
        sb.append(' ');
        if (!view.isFocused()) {
            c = '.';
        }
        sb.append(c);
        sb.append(view.isSelected() ? 'S' : '.');
        if (view.isPressed()) {
            c2 = 'P';
        }
        sb.append(c2);
        sb.append(' ');
        sb.append(view.getLeft());
        sb.append(',');
        sb.append(view.getTop());
        sb.append('-');
        sb.append(view.getRight());
        sb.append(',');
        sb.append(view.getBottom());
        int id = view.getId();
        if (id != -1) {
            sb.append(" #");
            sb.append(Integer.toHexString(id));
            Resources resources = view.getResources();
            if (!(id == 0 || resources == null)) {
                switch (-16777216 & id) {
                    case 16777216:
                        str = "android";
                        String resourceTypeName = resources.getResourceTypeName(id);
                        String resourceEntryName = resources.getResourceEntryName(id);
                        sb.append(" ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(resourceTypeName);
                        sb.append("/");
                        sb.append(resourceEntryName);
                        break;
                    case 2130706432:
                        str = "app";
                        String resourceTypeName2 = resources.getResourceTypeName(id);
                        String resourceEntryName2 = resources.getResourceEntryName(id);
                        sb.append(" ");
                        sb.append(str);
                        sb.append(":");
                        sb.append(resourceTypeName2);
                        sb.append("/");
                        sb.append(resourceEntryName2);
                        break;
                    default:
                        try {
                            str = resources.getResourcePackageName(id);
                            String resourceTypeName22 = resources.getResourceTypeName(id);
                            String resourceEntryName22 = resources.getResourceEntryName(id);
                            sb.append(" ");
                            sb.append(str);
                            sb.append(":");
                            sb.append(resourceTypeName22);
                            sb.append("/");
                            sb.append(resourceEntryName22);
                            break;
                        } catch (Resources.NotFoundException e) {
                            break;
                        }
                }
            }
        }
        sb.append("}");
        return sb.toString();
    }

    private void ttmhx7(String str, PrintWriter printWriter, View view) {
        ViewGroup viewGroup;
        int childCount;
        printWriter.print(str);
        if (view == null) {
            printWriter.println("null");
            return;
        }
        printWriter.println(ttmhx7(view));
        if ((view instanceof ViewGroup) && (childCount = (viewGroup = (ViewGroup) view).getChildCount()) > 0) {
            String str2 = str + "  ";
            for (int i = 0; i < childCount; i++) {
                ttmhx7(str2, printWriter, viewGroup.getChildAt(i));
            }
        }
    }

    public Object cehyzt7dw() {
        return null;
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        if (Build.VERSION.SDK_INT >= 11) {
        }
        printWriter.print(str);
        printWriter.print("Local FragmentActivity ");
        printWriter.print(Integer.toHexString(System.identityHashCode(this)));
        printWriter.println(" State:");
        String str2 = str + "  ";
        printWriter.print(str2);
        printWriter.print("mCreated=");
        printWriter.print(this.uin6g3d5rqgcbs);
        printWriter.print("mResumed=");
        printWriter.print(this.usuayu88rw4);
        printWriter.print(" mStopped=");
        printWriter.print(this.fxug2rdnfo);
        printWriter.print(" mReallyStopped=");
        printWriter.println(this.e8kxjqktk9t);
        printWriter.print(str2);
        printWriter.print("mLoadersStarted=");
        printWriter.println(this.iux03f6yieb);
        if (this.mhtc4dliin7r != null) {
            printWriter.print(str);
            printWriter.print("Loader Manager ");
            printWriter.print(Integer.toHexString(System.identityHashCode(this.mhtc4dliin7r)));
            printWriter.println(":");
            this.mhtc4dliin7r.ttmhx7(str + "  ", fileDescriptor, printWriter, strArr);
        }
        this.ozpoxuz523b2.ttmhx7(str, fileDescriptor, printWriter, strArr);
        printWriter.print(str);
        printWriter.println("View Hierarchy:");
        ttmhx7(str + "  ", printWriter, getWindow().getDecorView());
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        this.ozpoxuz523b2.ef5tn1cvshg414();
        int i3 = i >> 16;
        if (i3 != 0) {
            int i4 = i3 - 1;
            if (this.ozpoxuz523b2.fxug2rdnfo == null || i4 < 0 || i4 >= this.ozpoxuz523b2.fxug2rdnfo.size()) {
                Log.w("FragmentActivity", "Activity result fragment index out of range: 0x" + Integer.toHexString(i));
                return;
            }
            Fragment fragment = (Fragment) this.ozpoxuz523b2.fxug2rdnfo.get(i4);
            if (fragment == null) {
                Log.w("FragmentActivity", "Activity result no fragment exists for index: 0x" + Integer.toHexString(i));
            } else {
                fragment.ttmhx7(65535 & i, i2, intent);
            }
        } else {
            super.onActivityResult(i, i2, intent);
        }
    }

    public void onBackPressed() {
        if (!this.ozpoxuz523b2.cehyzt7dw()) {
            ttmhx7();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.ozpoxuz523b2.ttmhx7(configuration);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        this.ozpoxuz523b2.ttmhx7(this, this.cehyzt7dw, (Fragment) null);
        if (getLayoutInflater().getFactory() == null) {
            getLayoutInflater().setFactory(this);
        }
        super.onCreate(bundle);
        bpogj6 bpogj6 = (bpogj6) getLastNonConfigurationInstance();
        if (bpogj6 != null) {
            this.ay6ebym1yp0qgk = bpogj6.usuayu88rw4;
        }
        if (bundle != null) {
            this.ozpoxuz523b2.ttmhx7(bundle.getParcelable("android:support:fragments"), bpogj6 != null ? bpogj6.uin6g3d5rqgcbs : null);
        }
        this.ozpoxuz523b2.b5zlaptmyxarl();
    }

    public boolean onCreatePanelMenu(int i, Menu menu) {
        if (i != 0) {
            return super.onCreatePanelMenu(i, menu);
        }
        boolean onCreatePanelMenu = super.onCreatePanelMenu(i, menu) | this.ozpoxuz523b2.ttmhx7(menu, getMenuInflater());
        if (Build.VERSION.SDK_INT >= 11) {
            return onCreatePanelMenu;
        }
        return true;
    }

    public View onCreateView(String str, Context context, AttributeSet attributeSet) {
        if (!"fragment".equals(str)) {
            return super.onCreateView(str, context, attributeSet);
        }
        View onCreateView = this.ozpoxuz523b2.onCreateView(str, context, attributeSet);
        return onCreateView == null ? super.onCreateView(str, context, attributeSet) : onCreateView;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        ttmhx7(false);
        this.ozpoxuz523b2.k3jokks5k5();
        if (this.mhtc4dliin7r != null) {
            this.mhtc4dliin7r.lg71ytkvzw();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT >= 5 || i != 4 || keyEvent.getRepeatCount() != 0) {
            return super.onKeyDown(i, keyEvent);
        }
        onBackPressed();
        return true;
    }

    public void onLowMemory() {
        super.onLowMemory();
        this.ozpoxuz523b2.rulrdod1midre();
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (super.onMenuItemSelected(i, menuItem)) {
            return true;
        }
        switch (i) {
            case 0:
                return this.ozpoxuz523b2.ttmhx7(menuItem);
            case 6:
                return this.ozpoxuz523b2.ozpoxuz523b2(menuItem);
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.ozpoxuz523b2.ef5tn1cvshg414();
    }

    public void onPanelClosed(int i, Menu menu) {
        switch (i) {
            case 0:
                this.ozpoxuz523b2.ozpoxuz523b2(menu);
                break;
        }
        super.onPanelClosed(i, menu);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.usuayu88rw4 = false;
        if (this.ttmhx7.hasMessages(2)) {
            this.ttmhx7.removeMessages(2);
            ozpoxuz523b2();
        }
        this.ozpoxuz523b2.oc9mgl157cp();
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        this.ttmhx7.removeMessages(2);
        ozpoxuz523b2();
        this.ozpoxuz523b2.usuayu88rw4();
    }

    public boolean onPreparePanel(int i, View view, Menu menu) {
        if (i != 0 || menu == null) {
            return super.onPreparePanel(i, view, menu);
        }
        if (this.ef5tn1cvshg414) {
            this.ef5tn1cvshg414 = false;
            menu.clear();
            onCreatePanelMenu(i, menu);
        }
        return ttmhx7(view, menu) | this.ozpoxuz523b2.ttmhx7(menu);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.ttmhx7.sendEmptyMessage(2);
        this.usuayu88rw4 = true;
        this.ozpoxuz523b2.usuayu88rw4();
    }

    public final Object onRetainNonConfigurationInstance() {
        boolean z;
        if (this.fxug2rdnfo) {
            ttmhx7(true);
        }
        Object cehyzt7dw2 = cehyzt7dw();
        ArrayList e8kxjqktk9t2 = this.ozpoxuz523b2.e8kxjqktk9t();
        if (this.ay6ebym1yp0qgk != null) {
            int size = this.ay6ebym1yp0qgk.size();
            kld4qxthnxo5uo[] kld4qxthnxo5uoArr = new kld4qxthnxo5uo[size];
            for (int i = size - 1; i >= 0; i--) {
                kld4qxthnxo5uoArr[i] = (kld4qxthnxo5uo) this.ay6ebym1yp0qgk.cehyzt7dw(i);
            }
            z = false;
            for (int i2 = 0; i2 < size; i2++) {
                kld4qxthnxo5uo kld4qxthnxo5uo = kld4qxthnxo5uoArr[i2];
                if (kld4qxthnxo5uo.e8kxjqktk9t) {
                    z = true;
                } else {
                    kld4qxthnxo5uo.lg71ytkvzw();
                    this.ay6ebym1yp0qgk.remove(kld4qxthnxo5uo.uin6g3d5rqgcbs);
                }
            }
        } else {
            z = false;
        }
        if (e8kxjqktk9t2 == null && !z && cehyzt7dw2 == null) {
            return null;
        }
        bpogj6 bpogj6 = new bpogj6();
        bpogj6.ttmhx7 = null;
        bpogj6.ozpoxuz523b2 = cehyzt7dw2;
        bpogj6.cehyzt7dw = null;
        bpogj6.uin6g3d5rqgcbs = e8kxjqktk9t2;
        bpogj6.usuayu88rw4 = this.ay6ebym1yp0qgk;
        return bpogj6;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Parcelable lg71ytkvzw2 = this.ozpoxuz523b2.lg71ytkvzw();
        if (lg71ytkvzw2 != null) {
            bundle.putParcelable("android:support:fragments", lg71ytkvzw2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.oc9mgl157cp.ttmhx7(java.lang.String, boolean, boolean):android.support.v4.app.kld4qxthnxo5uo
     arg types: [java.lang.String, boolean, int]
     candidates:
      android.support.v4.app.oc9mgl157cp.ttmhx7(java.lang.String, java.io.PrintWriter, android.view.View):void
      android.support.v4.app.oc9mgl157cp.ttmhx7(java.lang.String, boolean, boolean):android.support.v4.app.kld4qxthnxo5uo */
    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.fxug2rdnfo = false;
        this.e8kxjqktk9t = false;
        this.ttmhx7.removeMessages(1);
        if (!this.uin6g3d5rqgcbs) {
            this.uin6g3d5rqgcbs = true;
            this.ozpoxuz523b2.iux03f6yieb();
        }
        this.ozpoxuz523b2.ef5tn1cvshg414();
        this.ozpoxuz523b2.usuayu88rw4();
        if (!this.iux03f6yieb) {
            this.iux03f6yieb = true;
            if (this.mhtc4dliin7r != null) {
                this.mhtc4dliin7r.ozpoxuz523b2();
            } else if (!this.b5zlaptmyxarl) {
                this.mhtc4dliin7r = ttmhx7("(root)", this.iux03f6yieb, false);
                if (this.mhtc4dliin7r != null && !this.mhtc4dliin7r.fxug2rdnfo) {
                    this.mhtc4dliin7r.ozpoxuz523b2();
                }
            }
            this.b5zlaptmyxarl = true;
        }
        this.ozpoxuz523b2.ay6ebym1yp0qgk();
        if (this.ay6ebym1yp0qgk != null) {
            int size = this.ay6ebym1yp0qgk.size();
            kld4qxthnxo5uo[] kld4qxthnxo5uoArr = new kld4qxthnxo5uo[size];
            for (int i = size - 1; i >= 0; i--) {
                kld4qxthnxo5uoArr[i] = (kld4qxthnxo5uo) this.ay6ebym1yp0qgk.cehyzt7dw(i);
            }
            for (int i2 = 0; i2 < size; i2++) {
                kld4qxthnxo5uo kld4qxthnxo5uo = kld4qxthnxo5uoArr[i2];
                kld4qxthnxo5uo.usuayu88rw4();
                kld4qxthnxo5uo.e8kxjqktk9t();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.fxug2rdnfo = true;
        this.ttmhx7.sendEmptyMessage(1);
        this.ozpoxuz523b2.bpogj6();
    }

    /* access modifiers changed from: protected */
    public void ozpoxuz523b2() {
        this.ozpoxuz523b2.mhtc4dliin7r();
    }

    public void startActivityForResult(Intent intent, int i) {
        if (i == -1 || (-65536 & i) == 0) {
            super.startActivityForResult(intent, i);
            return;
        }
        throw new IllegalArgumentException("Can only use lower 16 bits for requestCode");
    }

    /* access modifiers changed from: package-private */
    public kld4qxthnxo5uo ttmhx7(String str, boolean z, boolean z2) {
        if (this.ay6ebym1yp0qgk == null) {
            this.ay6ebym1yp0qgk = new ay6ebym1yp0qgk();
        }
        kld4qxthnxo5uo kld4qxthnxo5uo = (kld4qxthnxo5uo) this.ay6ebym1yp0qgk.get(str);
        if (kld4qxthnxo5uo != null) {
            kld4qxthnxo5uo.ttmhx7(this);
            return kld4qxthnxo5uo;
        } else if (!z2) {
            return kld4qxthnxo5uo;
        } else {
            kld4qxthnxo5uo kld4qxthnxo5uo2 = new kld4qxthnxo5uo(str, this, z);
            this.ay6ebym1yp0qgk.put(str, kld4qxthnxo5uo2);
            return kld4qxthnxo5uo2;
        }
    }

    public void ttmhx7() {
        ttmhx7.ttmhx7(this);
    }

    public void ttmhx7(Fragment fragment) {
    }

    /* access modifiers changed from: package-private */
    public void ttmhx7(String str) {
        kld4qxthnxo5uo kld4qxthnxo5uo;
        if (this.ay6ebym1yp0qgk != null && (kld4qxthnxo5uo = (kld4qxthnxo5uo) this.ay6ebym1yp0qgk.get(str)) != null && !kld4qxthnxo5uo.e8kxjqktk9t) {
            kld4qxthnxo5uo.lg71ytkvzw();
            this.ay6ebym1yp0qgk.remove(str);
        }
    }

    /* access modifiers changed from: package-private */
    public void ttmhx7(boolean z) {
        if (!this.e8kxjqktk9t) {
            this.e8kxjqktk9t = true;
            this.lg71ytkvzw = z;
            this.ttmhx7.removeMessages(1);
            usuayu88rw4();
        }
    }

    /* access modifiers changed from: protected */
    public boolean ttmhx7(View view, Menu menu) {
        return super.onPreparePanel(0, view, menu);
    }

    public void uin6g3d5rqgcbs() {
        if (Build.VERSION.SDK_INT >= 11) {
            cehyzt7dw.ttmhx7(this);
        } else {
            this.ef5tn1cvshg414 = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void usuayu88rw4() {
        if (this.iux03f6yieb) {
            this.iux03f6yieb = false;
            if (this.mhtc4dliin7r != null) {
                if (!this.lg71ytkvzw) {
                    this.mhtc4dliin7r.cehyzt7dw();
                } else {
                    this.mhtc4dliin7r.uin6g3d5rqgcbs();
                }
            }
        }
        this.ozpoxuz523b2.ca2ssr26fefu();
    }
}
