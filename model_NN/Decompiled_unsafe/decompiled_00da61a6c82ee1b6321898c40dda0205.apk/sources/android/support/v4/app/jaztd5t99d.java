package android.support.v4.app;

import android.transition.Transition;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;
import java.util.Map;

final class jaztd5t99d implements ViewTreeObserver.OnPreDrawListener {
    final /* synthetic */ ArrayList b5zlaptmyxarl;
    final /* synthetic */ View cehyzt7dw;
    final /* synthetic */ Transition e8kxjqktk9t;
    final /* synthetic */ Map ef5tn1cvshg414;
    final /* synthetic */ ArrayList fxug2rdnfo;
    final /* synthetic */ Transition iux03f6yieb;
    final /* synthetic */ ArrayList lg71ytkvzw;
    final /* synthetic */ Transition ozpoxuz523b2;
    final /* synthetic */ View ttmhx7;
    final /* synthetic */ ArrayList uin6g3d5rqgcbs;
    final /* synthetic */ Transition usuayu88rw4;

    jaztd5t99d(View view, Transition transition, View view2, ArrayList arrayList, Transition transition2, ArrayList arrayList2, Transition transition3, ArrayList arrayList3, Map map, ArrayList arrayList4, Transition transition4) {
        this.ttmhx7 = view;
        this.ozpoxuz523b2 = transition;
        this.cehyzt7dw = view2;
        this.uin6g3d5rqgcbs = arrayList;
        this.usuayu88rw4 = transition2;
        this.fxug2rdnfo = arrayList2;
        this.e8kxjqktk9t = transition3;
        this.lg71ytkvzw = arrayList3;
        this.ef5tn1cvshg414 = map;
        this.b5zlaptmyxarl = arrayList4;
        this.iux03f6yieb = transition4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.transition.Transition.excludeTarget(android.view.View, boolean):android.transition.Transition}
     arg types: [android.view.View, int]
     candidates:
      ClspMth{android.transition.Transition.excludeTarget(int, boolean):android.transition.Transition}
      ClspMth{android.transition.Transition.excludeTarget(java.lang.Class, boolean):android.transition.Transition}
      ClspMth{android.transition.Transition.excludeTarget(java.lang.String, boolean):android.transition.Transition}
      ClspMth{android.transition.Transition.excludeTarget(android.view.View, boolean):android.transition.Transition} */
    public boolean onPreDraw() {
        this.ttmhx7.getViewTreeObserver().removeOnPreDrawListener(this);
        if (this.ozpoxuz523b2 != null) {
            this.ozpoxuz523b2.removeTarget(this.cehyzt7dw);
            ftlyjgoncub6q.ttmhx7(this.ozpoxuz523b2, this.uin6g3d5rqgcbs);
        }
        if (this.usuayu88rw4 != null) {
            ftlyjgoncub6q.ttmhx7(this.usuayu88rw4, this.fxug2rdnfo);
        }
        if (this.e8kxjqktk9t != null) {
            ftlyjgoncub6q.ttmhx7(this.e8kxjqktk9t, this.lg71ytkvzw);
        }
        for (Map.Entry entry : this.ef5tn1cvshg414.entrySet()) {
            ((View) entry.getValue()).setTransitionName((String) entry.getKey());
        }
        int size = this.b5zlaptmyxarl.size();
        for (int i = 0; i < size; i++) {
            this.iux03f6yieb.excludeTarget((View) this.b5zlaptmyxarl.get(i), false);
        }
        this.iux03f6yieb.excludeTarget(this.cehyzt7dw, false);
        return true;
    }
}
