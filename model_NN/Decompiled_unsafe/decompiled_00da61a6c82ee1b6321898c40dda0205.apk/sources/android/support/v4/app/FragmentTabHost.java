package android.support.v4.app;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.TabHost;
import java.util.ArrayList;

public class FragmentTabHost extends TabHost implements TabHost.OnTabChangeListener {
    private flawb66z00q cehyzt7dw;
    private boolean e8kxjqktk9t;
    private zs1ge47fq1dgv5 fxug2rdnfo;
    private Context ozpoxuz523b2;
    private final ArrayList ttmhx7;
    private int uin6g3d5rqgcbs;
    private TabHost.OnTabChangeListener usuayu88rw4;

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new aecbla89ntoa8();
        String ttmhx7;

        private SavedState(Parcel parcel) {
            super(parcel);
            this.ttmhx7 = parcel.readString();
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public String toString() {
            return "FragmentTabHost.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " curTab=" + this.ttmhx7 + "}";
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.ttmhx7);
        }
    }

    private xbcow1jyae ttmhx7(String str, xbcow1jyae xbcow1jyae) {
        zs1ge47fq1dgv5 zs1ge47fq1dgv5 = null;
        int i = 0;
        while (i < this.ttmhx7.size()) {
            zs1ge47fq1dgv5 zs1ge47fq1dgv52 = (zs1ge47fq1dgv5) this.ttmhx7.get(i);
            if (!zs1ge47fq1dgv52.ttmhx7.equals(str)) {
                zs1ge47fq1dgv52 = zs1ge47fq1dgv5;
            }
            i++;
            zs1ge47fq1dgv5 = zs1ge47fq1dgv52;
        }
        if (zs1ge47fq1dgv5 == null) {
            throw new IllegalStateException("No tab known for tag " + str);
        }
        if (this.fxug2rdnfo != zs1ge47fq1dgv5) {
            if (xbcow1jyae == null) {
                xbcow1jyae = this.cehyzt7dw.ttmhx7();
            }
            if (!(this.fxug2rdnfo == null || this.fxug2rdnfo.uin6g3d5rqgcbs == null)) {
                xbcow1jyae.ttmhx7(this.fxug2rdnfo.uin6g3d5rqgcbs);
            }
            if (zs1ge47fq1dgv5 != null) {
                if (zs1ge47fq1dgv5.uin6g3d5rqgcbs == null) {
                    Fragment unused = zs1ge47fq1dgv5.uin6g3d5rqgcbs = Fragment.ttmhx7(this.ozpoxuz523b2, zs1ge47fq1dgv5.ozpoxuz523b2.getName(), zs1ge47fq1dgv5.cehyzt7dw);
                    xbcow1jyae.ttmhx7(this.uin6g3d5rqgcbs, zs1ge47fq1dgv5.uin6g3d5rqgcbs, zs1ge47fq1dgv5.ttmhx7);
                } else {
                    xbcow1jyae.ozpoxuz523b2(zs1ge47fq1dgv5.uin6g3d5rqgcbs);
                }
            }
            this.fxug2rdnfo = zs1ge47fq1dgv5;
        }
        return xbcow1jyae;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String currentTabTag = getCurrentTabTag();
        xbcow1jyae xbcow1jyae = null;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.ttmhx7.size()) {
                break;
            }
            zs1ge47fq1dgv5 zs1ge47fq1dgv5 = (zs1ge47fq1dgv5) this.ttmhx7.get(i2);
            Fragment unused = zs1ge47fq1dgv5.uin6g3d5rqgcbs = this.cehyzt7dw.ttmhx7(zs1ge47fq1dgv5.ttmhx7);
            if (zs1ge47fq1dgv5.uin6g3d5rqgcbs != null && !zs1ge47fq1dgv5.uin6g3d5rqgcbs.fxug2rdnfo()) {
                if (zs1ge47fq1dgv5.ttmhx7.equals(currentTabTag)) {
                    this.fxug2rdnfo = zs1ge47fq1dgv5;
                } else {
                    if (xbcow1jyae == null) {
                        xbcow1jyae = this.cehyzt7dw.ttmhx7();
                    }
                    xbcow1jyae.ttmhx7(zs1ge47fq1dgv5.uin6g3d5rqgcbs);
                }
            }
            i = i2 + 1;
        }
        this.e8kxjqktk9t = true;
        xbcow1jyae ttmhx72 = ttmhx7(currentTabTag, xbcow1jyae);
        if (ttmhx72 != null) {
            ttmhx72.ttmhx7();
            this.cehyzt7dw.ozpoxuz523b2();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.e8kxjqktk9t = false;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setCurrentTabByTag(savedState.ttmhx7);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.ttmhx7 = getCurrentTabTag();
        return savedState;
    }

    public void onTabChanged(String str) {
        xbcow1jyae ttmhx72;
        if (this.e8kxjqktk9t && (ttmhx72 = ttmhx7(str, null)) != null) {
            ttmhx72.ttmhx7();
        }
        if (this.usuayu88rw4 != null) {
            this.usuayu88rw4.onTabChanged(str);
        }
    }

    public void setOnTabChangedListener(TabHost.OnTabChangeListener onTabChangeListener) {
        this.usuayu88rw4 = onTabChangeListener;
    }

    @Deprecated
    public void setup() {
        throw new IllegalStateException("Must call setup() that takes a Context and FragmentManager");
    }
}
