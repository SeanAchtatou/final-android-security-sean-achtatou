package android.support.v4.app;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

final class FragmentState implements Parcelable {
    public static final Parcelable.Creator CREATOR = new eyli1ymagd3o();
    Bundle b5zlaptmyxarl;
    final boolean cehyzt7dw;
    final boolean e8kxjqktk9t;
    final Bundle ef5tn1cvshg414;
    final String fxug2rdnfo;
    Fragment iux03f6yieb;
    final boolean lg71ytkvzw;
    final int ozpoxuz523b2;
    final String ttmhx7;
    final int uin6g3d5rqgcbs;
    final int usuayu88rw4;

    public FragmentState(Parcel parcel) {
        boolean z = true;
        this.ttmhx7 = parcel.readString();
        this.ozpoxuz523b2 = parcel.readInt();
        this.cehyzt7dw = parcel.readInt() != 0;
        this.uin6g3d5rqgcbs = parcel.readInt();
        this.usuayu88rw4 = parcel.readInt();
        this.fxug2rdnfo = parcel.readString();
        this.e8kxjqktk9t = parcel.readInt() != 0;
        this.lg71ytkvzw = parcel.readInt() == 0 ? false : z;
        this.ef5tn1cvshg414 = parcel.readBundle();
        this.b5zlaptmyxarl = parcel.readBundle();
    }

    public FragmentState(Fragment fragment) {
        this.ttmhx7 = fragment.getClass().getName();
        this.ozpoxuz523b2 = fragment.e8kxjqktk9t;
        this.cehyzt7dw = fragment.ca2ssr26fefu;
        this.uin6g3d5rqgcbs = fragment.eyli1ymagd3o;
        this.usuayu88rw4 = fragment.sgnd7s4;
        this.fxug2rdnfo = fragment.aecbla89ntoa8;
        this.e8kxjqktk9t = fragment.ftlyjgoncub6q;
        this.lg71ytkvzw = fragment.xbcow1jyae;
        this.ef5tn1cvshg414 = fragment.ef5tn1cvshg414;
    }

    public int describeContents() {
        return 0;
    }

    public Fragment ttmhx7(oc9mgl157cp oc9mgl157cp, Fragment fragment) {
        if (this.iux03f6yieb != null) {
            return this.iux03f6yieb;
        }
        if (this.ef5tn1cvshg414 != null) {
            this.ef5tn1cvshg414.setClassLoader(oc9mgl157cp.getClassLoader());
        }
        this.iux03f6yieb = Fragment.ttmhx7(oc9mgl157cp, this.ttmhx7, this.ef5tn1cvshg414);
        if (this.b5zlaptmyxarl != null) {
            this.b5zlaptmyxarl.setClassLoader(oc9mgl157cp.getClassLoader());
            this.iux03f6yieb.usuayu88rw4 = this.b5zlaptmyxarl;
        }
        this.iux03f6yieb.ttmhx7(this.ozpoxuz523b2, fragment);
        this.iux03f6yieb.ca2ssr26fefu = this.cehyzt7dw;
        this.iux03f6yieb.k3jokks5k5 = true;
        this.iux03f6yieb.eyli1ymagd3o = this.uin6g3d5rqgcbs;
        this.iux03f6yieb.sgnd7s4 = this.usuayu88rw4;
        this.iux03f6yieb.aecbla89ntoa8 = this.fxug2rdnfo;
        this.iux03f6yieb.ftlyjgoncub6q = this.e8kxjqktk9t;
        this.iux03f6yieb.xbcow1jyae = this.lg71ytkvzw;
        this.iux03f6yieb.cpgyvt8o4r3 = oc9mgl157cp.ozpoxuz523b2;
        if (rulrdod1midre.ttmhx7) {
            Log.v("FragmentManager", "Instantiated fragment " + this.iux03f6yieb);
        }
        return this.iux03f6yieb;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int i2 = 1;
        parcel.writeString(this.ttmhx7);
        parcel.writeInt(this.ozpoxuz523b2);
        parcel.writeInt(this.cehyzt7dw ? 1 : 0);
        parcel.writeInt(this.uin6g3d5rqgcbs);
        parcel.writeInt(this.usuayu88rw4);
        parcel.writeString(this.fxug2rdnfo);
        parcel.writeInt(this.e8kxjqktk9t ? 1 : 0);
        if (!this.lg71ytkvzw) {
            i2 = 0;
        }
        parcel.writeInt(i2);
        parcel.writeBundle(this.ef5tn1cvshg414);
        parcel.writeBundle(this.b5zlaptmyxarl);
    }
}
