package android.support.v4.app;

import android.transition.Transition;
import android.view.View;
import android.view.ViewTreeObserver;
import java.util.ArrayList;
import java.util.Map;

final class s6o869vduri implements ViewTreeObserver.OnPreDrawListener {
    final /* synthetic */ Map cehyzt7dw;
    final /* synthetic */ View e8kxjqktk9t;
    final /* synthetic */ ArrayList fxug2rdnfo;
    final /* synthetic */ b6p1j7hoons8 ozpoxuz523b2;
    final /* synthetic */ View ttmhx7;
    final /* synthetic */ Map uin6g3d5rqgcbs;
    final /* synthetic */ Transition usuayu88rw4;

    s6o869vduri(View view, b6p1j7hoons8 b6p1j7hoons8, Map map, Map map2, Transition transition, ArrayList arrayList, View view2) {
        this.ttmhx7 = view;
        this.ozpoxuz523b2 = b6p1j7hoons8;
        this.cehyzt7dw = map;
        this.uin6g3d5rqgcbs = map2;
        this.usuayu88rw4 = transition;
        this.fxug2rdnfo = arrayList;
        this.e8kxjqktk9t = view2;
    }

    public boolean onPreDraw() {
        this.ttmhx7.getViewTreeObserver().removeOnPreDrawListener(this);
        View ttmhx72 = this.ozpoxuz523b2.ttmhx7();
        if (ttmhx72 == null) {
            return true;
        }
        if (!this.cehyzt7dw.isEmpty()) {
            ftlyjgoncub6q.ttmhx7(this.uin6g3d5rqgcbs, ttmhx72);
            this.uin6g3d5rqgcbs.keySet().retainAll(this.cehyzt7dw.values());
            for (Map.Entry entry : this.cehyzt7dw.entrySet()) {
                View view = (View) this.uin6g3d5rqgcbs.get((String) entry.getValue());
                if (view != null) {
                    view.setTransitionName((String) entry.getKey());
                }
            }
        }
        if (this.usuayu88rw4 == null) {
            return true;
        }
        ftlyjgoncub6q.ozpoxuz523b2(this.fxug2rdnfo, ttmhx72);
        this.fxug2rdnfo.removeAll(this.uin6g3d5rqgcbs.values());
        this.fxug2rdnfo.add(this.e8kxjqktk9t);
        this.usuayu88rw4.removeTarget(this.e8kxjqktk9t);
        ftlyjgoncub6q.ozpoxuz523b2(this.usuayu88rw4, this.fxug2rdnfo);
        return true;
    }
}
