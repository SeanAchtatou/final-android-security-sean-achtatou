package com.a.a;

public final class b {
    public byte[] a;
    public int b;
    public int c;
    public long d;
    public byte[] e;
    public int f;
    public int g;
    public long h;
    public String i;
    h j;
    public long k;
    d l = new d();

    public final int a(int i2) {
        if (this.j == null) {
            return -2;
        }
        return this.j.a(this, i2);
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        int i2 = this.j.c;
        if (i2 > this.g) {
            i2 = this.g;
        }
        if (i2 != 0) {
            if (this.j.a.length <= this.j.b || this.e.length <= this.f || this.j.a.length < this.j.b + i2 || this.e.length < this.f + i2) {
                System.out.println(this.j.a.length + ", " + this.j.b + ", " + this.e.length + ", " + this.f + ", " + i2);
                System.out.println("avail_out=" + this.g);
            }
            System.arraycopy(this.j.a, this.j.b, this.e, this.f, i2);
            this.f += i2;
            this.j.b += i2;
            this.h += (long) i2;
            this.g -= i2;
            this.j.c -= i2;
            if (this.j.c == 0) {
                this.j.b = 0;
            }
        }
    }
}
