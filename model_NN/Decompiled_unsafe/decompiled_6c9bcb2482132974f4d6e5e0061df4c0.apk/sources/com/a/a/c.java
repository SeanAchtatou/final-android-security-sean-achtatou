package com.a.a;

import java.io.IOException;
import java.io.OutputStream;

public final class c extends OutputStream {
    private b a;
    private int b;
    private int c;
    private byte[] d;
    private byte[] e;
    private boolean f;
    private OutputStream g;

    public c(OutputStream outputStream, int i) {
        this(outputStream, 9, false);
    }

    private c(OutputStream outputStream, int i, boolean z) {
        this.a = new b();
        this.b = 512;
        this.c = 0;
        this.d = new byte[this.b];
        this.e = new byte[1];
        this.g = outputStream;
        b bVar = this.a;
        bVar.j = new h();
        bVar.j.a(bVar, i, 0 != 0 ? -15 : 15);
        this.f = true;
    }

    private void a() {
        if (this.a != null) {
            if (this.f) {
                b bVar = this.a;
                if (bVar.j != null) {
                    bVar.j.a();
                    bVar.j = null;
                }
            } else {
                b bVar2 = this.a;
            }
            b bVar3 = this.a;
            bVar3.a = null;
            bVar3.e = null;
            bVar3.i = null;
            bVar3.l = null;
            this.a = null;
        }
    }

    public final void close() {
        int i;
        while (true) {
            try {
                this.a.e = this.d;
                this.a.f = 0;
                this.a.g = this.b;
                if (this.f) {
                    i = this.a.a(4);
                } else {
                    b bVar = this.a;
                    i = -2;
                }
                if (i == 1 || i == 0) {
                    if (this.b - this.a.g > 0) {
                        this.g.write(this.d, 0, this.b - this.a.g);
                    }
                    if (this.a.c <= 0 && this.a.g != 0) {
                        flush();
                        break;
                    }
                } else {
                    throw new a((this.f ? "de" : "in") + "flating: " + this.a.i);
                }
            } catch (IOException e2) {
            } catch (Throwable th) {
                a();
                this.g.close();
                this.g = null;
                throw th;
            }
        }
        a();
        this.g.close();
        this.g = null;
    }

    public final void flush() {
        this.g.flush();
    }

    public final void write(int i) {
        this.e[0] = (byte) i;
        write(this.e, 0, 1);
    }

    public final void write(byte[] bArr, int i, int i2) {
        int i3;
        if (i2 != 0) {
            this.a.a = bArr;
            this.a.b = i;
            this.a.c = i2;
            while (true) {
                this.a.e = this.d;
                this.a.f = 0;
                this.a.g = this.b;
                if (this.f) {
                    i3 = this.a.a(0);
                } else {
                    b bVar = this.a;
                    i3 = -2;
                }
                if (i3 != 0) {
                    throw new a((this.f ? "de" : "in") + "flating: " + this.a.i);
                }
                this.g.write(this.d, 0, this.b - this.a.g);
                if (this.a.c <= 0 && this.a.g != 0) {
                    return;
                }
            }
        }
    }
}
