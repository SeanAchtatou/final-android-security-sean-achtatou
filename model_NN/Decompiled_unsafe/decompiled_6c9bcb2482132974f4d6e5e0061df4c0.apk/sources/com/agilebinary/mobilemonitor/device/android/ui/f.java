package com.agilebinary.mobilemonitor.device.android.ui;

import android.os.AsyncTask;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.i.b;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;

final class f extends AsyncTask {
    private /* synthetic */ MainActivity a;

    f(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.e.b(java.lang.String, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.b.b(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.d.e.b(java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        String str = ((String[]) objArr)[0];
        c cVar = new c(this.a);
        int a2 = this.a.y.a(str, this.a.x.v());
        if (a2 == 0) {
            e.c().b((String) null, true);
            cVar.a = true;
            BackgroundService.a(this.a, "EXTRA_DEACTIVATE_FROM_GUI");
        } else {
            cVar.a = false;
            cVar.b = b.a("ACTIVATION_ERROR_" + a2);
        }
        return cVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        c cVar = (c) obj;
        super.onPostExecute(cVar);
        this.a.a();
        this.a.p.hide();
        if (cVar.a) {
            this.a.b();
        } else {
            this.a.h.setText(b.a("COMMONS_DEACTIVATION_FAILED", cVar.b));
        }
    }
}
