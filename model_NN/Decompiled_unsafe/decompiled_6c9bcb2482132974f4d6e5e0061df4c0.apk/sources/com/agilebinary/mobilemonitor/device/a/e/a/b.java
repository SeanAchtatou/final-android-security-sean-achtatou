package com.agilebinary.mobilemonitor.device.a.e.a;

import com.agilebinary.mobilemonitor.device.a.e.f;

public final class b extends c {
    private static final String a = f.a();
    /* access modifiers changed from: private */
    public a b;
    private d c;
    private boolean d;
    private boolean e;
    /* access modifiers changed from: private */
    public com.agilebinary.mobilemonitor.device.a.f.f f;

    public b(a aVar, d dVar, boolean z, boolean z2, com.agilebinary.mobilemonitor.device.a.f.f fVar) {
        super(aVar.b());
        this.b = aVar;
        this.c = dVar;
        this.d = z;
        this.e = z2;
        this.f = fVar;
    }

    public final void a() {
        b();
        if (this.e) {
            this.c.a(new e(this, this.b), this.d);
        } else {
            this.c.a(this.b, this.d);
        }
    }

    public final String b() {
        return super.b() + " (wrapped)";
    }

    public final Object c() {
        return this.b.c();
    }
}
