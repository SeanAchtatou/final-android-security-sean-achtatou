package com.agilebinary.mobilemonitor.device.android.ui;

import android.os.AsyncTask;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.i.b;

final class p extends AsyncTask {
    private /* synthetic */ MainActivity a;

    p(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.c.c.a(boolean, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.c.c.a(java.lang.String, long, byte[]):com.agilebinary.mobilemonitor.device.a.c.b
      com.agilebinary.mobilemonitor.device.a.c.c.a(boolean, boolean, boolean):void */
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        this.a.y.a(true, true, true);
        int r = this.a.y.r();
        c cVar = new c(this.a);
        cVar.a = r != 0;
        cVar.b = b.a("CONNECTION_TYPE_" + r);
        return cVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        c cVar = (c) obj;
        super.onPostExecute(cVar);
        if (cVar.a) {
            this.a.w.add(new r(b.a("CONNECTION_TEST_SUCCESS", cVar.b)));
            if (e.c().Z()) {
                new t(this.a).execute(new Void[0]);
            }
        } else {
            this.a.w.add(new r(b.a("CONNECTION_TEST_FAILED")));
        }
        this.a.q.cancel();
    }
}
