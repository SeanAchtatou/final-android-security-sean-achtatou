package com.agilebinary.mobilemonitor.device.a.b;

import com.agilebinary.mobilemonitor.device.a.d.c;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.e.f;

public final class b {
    private static int a = -1;
    private long b;
    private int c;
    private int d;
    private int e;
    private long f;
    private byte[] g;
    private int h;
    private int i;
    private int j;
    private int k;
    private int l;
    private boolean m;
    private boolean n;
    private boolean o;

    static {
        f.a();
    }

    public b(int i2, byte[] bArr, long j2) {
        this((long) a, i2, 0, 0, j2, bArr.length);
        a(bArr);
    }

    public b(long j2, int i2, int i3, int i4, long j3, int i5) {
        this.b = j2;
        this.c = i3;
        this.d = i2;
        this.e = i4;
        this.f = j3;
        this.h = i5;
        this.i = 0;
    }

    public final int a(c cVar) {
        if (this.c >= cVar.g(this.d)) {
            return Integer.MAX_VALUE;
        }
        return cVar.h(this.d);
    }

    public final int a(e eVar, c cVar) {
        if (this.i != 0) {
            return 2;
        }
        return ((this.c >= cVar.g(this.d)) || eVar.a(this.d) == 0) ? 1 : 2;
    }

    public final void a(int i2) {
        this.e = i2;
    }

    public final void a(boolean z) {
        this.m = true;
    }

    public final void a(byte[] bArr) {
        this.g = bArr;
        this.h = bArr.length;
    }

    public final boolean a() {
        return this.b == ((long) a);
    }

    public final void b(int i2) {
        this.c = 1;
    }

    public final void b(boolean z) {
        this.n = z;
    }

    public final boolean b() {
        return this.g != null;
    }

    public final int c() {
        return this.h - this.e;
    }

    public final void c(int i2) {
        this.i = i2;
    }

    public final void c(boolean z) {
        this.o = z;
    }

    public final void d(int i2) {
        this.j = i2;
    }

    public final boolean d() {
        return this.h == this.e;
    }

    public final void e(int i2) {
        this.k = i2;
    }

    public final byte[] e() {
        return this.g;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof b) {
            return ((b) obj).f == this.f;
        }
        return false;
    }

    public final int f() {
        return this.e;
    }

    public final void f(int i2) {
        this.l = i2;
    }

    public final long g() {
        return this.b;
    }

    public final int h() {
        return this.c;
    }

    public final int hashCode() {
        return (int) this.f;
    }

    public final int i() {
        return this.d;
    }

    public final long j() {
        return this.f;
    }

    public final int k() {
        return this.h;
    }

    public final int l() {
        return this.i;
    }

    public final int m() {
        return this.j;
    }

    public final int n() {
        return this.k;
    }

    public final int o() {
        return this.l;
    }

    public final boolean p() {
        return this.m;
    }

    public final boolean q() {
        return this.n;
    }

    public final boolean r() {
        return this.o;
    }

    public final String toString() {
        return this.d + " / " + this.f + " / chunkeduploadActive: " + this.n;
    }
}
