package com.agilebinary.mobilemonitor.device.android.b.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

final class a extends SQLiteOpenHelper {
    public a(b bVar, Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        String format = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMARY KEY, %3$s INTEGER, %4$s INTEGER, %5$s INTEGER, %6$s INTEGER, %7$s BLOB,%8$s INTEGER);", c.a, c.b, c.c, c.d, c.e, c.f, c.g, c.h);
        String.format("Executing SQL: %1$s", format);
        sQLiteDatabase.execSQL(format);
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        Log.d(b.e, String.format("Upgrading database from version %1$s to %2$s, destroying all existing data", Integer.valueOf(i), Integer.valueOf(i2)));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", c.a));
        onCreate(sQLiteDatabase);
    }
}
