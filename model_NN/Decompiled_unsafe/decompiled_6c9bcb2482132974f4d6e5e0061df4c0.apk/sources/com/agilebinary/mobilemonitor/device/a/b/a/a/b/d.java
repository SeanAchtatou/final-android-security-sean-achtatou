package com.agilebinary.mobilemonitor.device.a.b.a.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;

public final class d extends g {
    private int a = -1;

    public d(a aVar) {
        super(aVar);
        this.a = aVar.e();
    }

    public final int a() {
        return this.a;
    }

    public final String a(com.agilebinary.mobilemonitor.device.a.b.a.a.d dVar) {
        return super.a(dVar) + "\nCellId: " + this.a;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
    }

    public final byte e() {
        return 12;
    }
}
