package com.agilebinary.mobilemonitor.device.android.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class e extends BroadcastReceiver {
    final /* synthetic */ MainActivity a;

    e(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent.hasExtra("EXTRA_LAST_EVENT_UPLOAD_CONTYPE") && intent.hasExtra("EXTRA_LAST_EVENT_UPLOAD_TIME")) {
            this.a.runOnUiThread(new o(this, intent.getLongExtra("EXTRA_LAST_EVENT_UPLOAD_TIME", 0), intent.getIntExtra("EXTRA_LAST_EVENT_UPLOAD_CONTYPE", 0)));
        }
    }
}
