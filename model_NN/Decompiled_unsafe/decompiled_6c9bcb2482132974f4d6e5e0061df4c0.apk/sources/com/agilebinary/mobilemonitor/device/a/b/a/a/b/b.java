package com.agilebinary.mobilemonitor.device.a.b.a.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.e;

public final class b extends f {
    private long a;
    private int b;

    public b(a aVar) {
        super(aVar);
        this.a = aVar.f();
        this.b = aVar.e();
    }

    public b(String str, String str2, e eVar, long j, int i) {
        super(str, str2, eVar);
        this.a = j;
        this.b = i;
    }

    public final String a(d dVar) {
        return super.a(dVar) + "\nTime: " + dVar.a(this.a) + "\nKind: " + this.b;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        aVar.a(this.b);
    }

    public final byte e() {
        return 10;
    }
}
