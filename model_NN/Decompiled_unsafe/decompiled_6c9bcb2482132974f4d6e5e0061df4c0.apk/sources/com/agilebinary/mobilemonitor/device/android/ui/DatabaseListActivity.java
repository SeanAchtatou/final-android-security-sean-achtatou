package com.agilebinary.mobilemonitor.device.android.ui;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.Button;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.d.c;
import com.agilebinary.mobilemonitor.device.android.b.a.b;
import com.agilebinary.mobilemonitor.device.b.a.a.a;
import com.agilebinary.phonebeagle.R;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DatabaseListActivity extends ListActivity {
    private Button a;
    /* access modifiers changed from: private */
    public b b;

    private static String a(int i) {
        Field[] fields = com.agilebinary.mobilemonitor.device.a.b.a.a.b.class.getFields();
        int length = fields.length;
        int i2 = 0;
        while (i2 < length) {
            Field field = fields[i2];
            try {
                if (field.getInt(null) == i) {
                    return field.getName();
                }
                i2++;
            } catch (Exception e) {
                return "N/A";
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void a() {
        d dVar = new d();
        q qVar = new q(this);
        if (this.b != null) {
            try {
                com.agilebinary.mobilemonitor.device.a.b.b[] o = this.b.o();
                for (com.agilebinary.mobilemonitor.device.a.b.b bVar : o) {
                    this.b.a(bVar);
                    byte[] e = bVar.e();
                    c.c();
                    qVar.add(new r(SimpleDateFormat.getTimeInstance().format(new Date(bVar.j())) + "\n" + a(bVar.i()), a.a(e).a(dVar)));
                }
            } catch (com.agilebinary.mobilemonitor.device.a.a.a e2) {
                e2.printStackTrace();
            } catch (com.agilebinary.mobilemonitor.device.a.b.a.a.a.a.b e3) {
                e3.printStackTrace();
            }
        }
        setListAdapter(qVar);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = b.a;
        setContentView((int) R.layout.dblist);
        this.a = (Button) findViewById(R.id.dblist_Button_Clear);
        this.a.setOnClickListener(new s(this));
        a();
    }
}
