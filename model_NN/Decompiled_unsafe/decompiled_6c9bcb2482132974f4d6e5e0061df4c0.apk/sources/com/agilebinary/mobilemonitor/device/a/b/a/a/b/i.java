package com.agilebinary.mobilemonitor.device.a.b.a.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.e;

public final class i extends f {
    private long a;
    private int b;
    private String c;

    public i(a aVar) {
        super(aVar);
        this.a = aVar.f();
        this.b = aVar.e();
        this.c = aVar.i();
    }

    public i(String str, String str2, e eVar, long j, int i, String str3) {
        super(str, str2, eVar);
        this.a = j;
        this.b = i;
        this.c = str3 == null ? "" : str3;
    }

    public final String a(d dVar) {
        return super.a(dVar) + "\nTime: " + dVar.a(this.a) + "\nKind: " + this.b + "\nName: " + this.c;
    }

    public final byte e() {
        return 11;
    }
}
