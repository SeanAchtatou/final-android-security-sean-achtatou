package com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a;

public final class k {
    private static final l[] l;
    private static final String[] m = {"need dictionary", "stream end", "", "file error", "stream error", "data error", "insufficient memory", "buffer error", "incompatible version", ""};
    private int A;
    private int B;
    private int C;
    private int D;
    private int E;
    private int F;
    private int G;
    private int H;
    private int I;
    private int J;
    private int K;
    private int L;
    private int M;
    private int N;
    private int O;
    private int P;
    private int Q;
    private int R;
    private short[] S = new short[1146];
    private short[] T = new short[122];
    private short[] U = new short[78];
    private d V = new d();
    private d W = new d();
    private d X = new d();
    private int Y;
    private int Z;
    byte[] a;
    private int aa;
    private int ab;
    private int ac;
    private int ad;
    private short ae;
    private int af;
    int b;
    int c;
    int d;
    short[] e = new short[16];
    int[] f = new int[573];
    int g;
    int h;
    byte[] i = new byte[573];
    int j;
    int k;
    private a n;
    private int o;
    private int p;
    private byte q;
    private int r;
    private int s;
    private int t;
    private int u;
    private byte[] v;
    private int w;
    private short[] x;
    private short[] y;
    private int z;

    static {
        l[] lVarArr = new l[10];
        l = lVarArr;
        lVarArr[0] = new l(0, 0, 0, 0, 0);
        l[1] = new l(4, 4, 8, 4, 1);
        l[2] = new l(4, 5, 16, 8, 1);
        l[3] = new l(4, 6, 32, 32, 1);
        l[4] = new l(4, 4, 16, 16, 2);
        l[5] = new l(8, 16, 32, 32, 2);
        l[6] = new l(8, 16, 128, 128, 2);
        l[7] = new l(8, 32, 128, 256, 2);
        l[8] = new l(32, 128, 258, 1024, 2);
        l[9] = new l(32, 258, 258, 4096, 2);
    }

    k() {
    }

    private void a(byte b2) {
        byte[] bArr = this.a;
        int i2 = this.c;
        this.c = i2 + 1;
        bArr[i2] = b2;
    }

    private void a(int i2) {
        a((byte) i2);
        a((byte) (i2 >>> 8));
    }

    private void a(int i2, int i3) {
        if (this.af > 16 - i3) {
            this.ae = (short) (this.ae | ((i2 << this.af) & 65535));
            a(this.ae);
            this.ae = (short) (i2 >>> (16 - this.af));
            this.af += i3 - 16;
            return;
        }
        this.ae = (short) (this.ae | ((i2 << this.af) & 65535));
        this.af += i3;
    }

    private void a(int i2, int i3, boolean z2) {
        a((z2 ? 1 : 0) + 0, 3);
        d();
        this.ad = 8;
        a((short) i3);
        a((short) (i3 ^ -1));
        System.arraycopy(this.v, i2, this.a, this.c, i3);
        this.c += i3;
    }

    private void a(int i2, short[] sArr) {
        int i3 = i2 * 2;
        a(sArr[i3] & 65535, sArr[i3 + 1] & 65535);
    }

    private void a(boolean z2) {
        int i2;
        int i3;
        int i4;
        int i5 = this.E >= 0 ? this.E : -1;
        int i6 = this.I - this.E;
        if (this.O > 0) {
            if (this.q == 2) {
                int i7 = 0;
                int i8 = 0;
                while (i8 < 7) {
                    i7 += this.S[i8 * 2];
                    i8++;
                }
                int i9 = i8;
                int i10 = 0;
                while (i9 < 128) {
                    i10 += this.S[i9 * 2];
                    i9++;
                }
                while (i9 < 256) {
                    i7 += this.S[i9 * 2];
                    i9++;
                }
                this.q = (byte) (i7 > (i10 >>> 2) ? 0 : 1);
            }
            this.V.a(this);
            this.W.a(this);
            b(this.S, this.V.i);
            b(this.T, this.W.i);
            this.X.a(this);
            i4 = 18;
            while (i4 >= 3 && this.U[(d.d[i4] * 2) + 1] == 0) {
                i4--;
            }
            this.j += ((i4 + 1) * 3) + 5 + 5 + 4;
            int i11 = ((this.j + 3) + 7) >>> 3;
            i3 = ((this.k + 3) + 7) >>> 3;
            if (i3 <= i11) {
                i2 = i3;
            } else {
                int i12 = i3;
                i3 = i11;
                i2 = i12;
            }
        } else {
            int i13 = i6 + 5;
            i2 = i13;
            i3 = i13;
            i4 = 0;
        }
        if (i6 + 4 <= i3 && i5 != -1) {
            a(i5, i6, z2);
        } else if (i2 == i3) {
            a((z2 ? 1 : 0) + 2, 3);
            a(h.a, h.b);
        } else {
            a((z2 ? 1 : 0) + 4, 3);
            int i14 = this.V.i + 1;
            int i15 = this.W.i + 1;
            int i16 = i4 + 1;
            a(i14 - 257, 5);
            a(i15 - 1, 5);
            a(i16 - 4, 4);
            for (int i17 = 0; i17 < i16; i17++) {
                a(this.U[(d.d[i17] * 2) + 1], 3);
            }
            c(this.S, i14 - 1);
            c(this.T, i15 - 1);
            a(this.S, this.T);
        }
        b();
        if (z2) {
            d();
        }
        this.E = this.I;
        this.n.a();
    }

    private void a(short[] sArr, short[] sArr2) {
        int i2 = 0;
        if (this.aa != 0) {
            do {
                byte b2 = ((this.a[this.ab + (i2 * 2)] << 8) & 65280) | (this.a[this.ab + (i2 * 2) + 1] & 255);
                byte b3 = this.a[this.Y + i2] & 255;
                i2++;
                if (b2 == 0) {
                    a(b3, sArr);
                } else {
                    byte b4 = d.e[b3];
                    a(b4 + 256 + 1, sArr);
                    int i3 = d.a[b4];
                    if (i3 != 0) {
                        a(b3 - d.f[b4], i3);
                    }
                    int i4 = b2 - 1;
                    int a2 = d.a(i4);
                    a(a2, sArr2);
                    int i5 = d.b[a2];
                    if (i5 != 0) {
                        a(i4 - d.g[a2], i5);
                    }
                }
            } while (i2 < this.aa);
        }
        a(256, sArr);
        this.ad = sArr[513];
    }

    private static boolean a(short[] sArr, int i2, int i3, byte[] bArr) {
        short s2 = sArr[i2 * 2];
        short s3 = sArr[i3 * 2];
        return s2 < s3 || (s2 == s3 && bArr[i2] <= bArr[i3]);
    }

    private void b() {
        for (int i2 = 0; i2 < 286; i2++) {
            this.S[i2 * 2] = 0;
        }
        for (int i3 = 0; i3 < 30; i3++) {
            this.T[i3 * 2] = 0;
        }
        for (int i4 = 0; i4 < 19; i4++) {
            this.U[i4 * 2] = 0;
        }
        this.S[512] = 1;
        this.k = 0;
        this.j = 0;
        this.ac = 0;
        this.aa = 0;
    }

    private void b(int i2) {
        a((byte) (i2 >> 8));
        a((byte) i2);
    }

    private void b(short[] sArr, int i2) {
        int i3;
        int i4;
        short s2 = -1;
        short s3 = sArr[1];
        if (s3 == 0) {
            i4 = 138;
            i3 = 3;
        } else {
            i3 = 4;
            i4 = 7;
        }
        sArr[((i2 + 1) * 2) + 1] = -1;
        short s4 = s3;
        int i5 = 0;
        int i6 = i3;
        int i7 = i4;
        int i8 = 0;
        while (i5 <= i2) {
            short s5 = sArr[((i5 + 1) * 2) + 1];
            i8++;
            if (i8 >= i7 || s4 != s5) {
                if (i8 < i6) {
                    short[] sArr2 = this.U;
                    int i9 = s4 * 2;
                    sArr2[i9] = (short) (i8 + sArr2[i9]);
                } else if (s4 != 0) {
                    if (s4 != s2) {
                        short[] sArr3 = this.U;
                        int i10 = s4 * 2;
                        sArr3[i10] = (short) (sArr3[i10] + 1);
                    }
                    short[] sArr4 = this.U;
                    sArr4[32] = (short) (sArr4[32] + 1);
                } else if (i8 <= 10) {
                    short[] sArr5 = this.U;
                    sArr5[34] = (short) (sArr5[34] + 1);
                } else {
                    short[] sArr6 = this.U;
                    sArr6[36] = (short) (sArr6[36] + 1);
                }
                if (s5 == 0) {
                    i7 = 138;
                    i8 = 0;
                    i6 = 3;
                } else if (s4 == s5) {
                    i7 = 6;
                    i8 = 0;
                    i6 = 3;
                } else {
                    i6 = 4;
                    i7 = 7;
                    i8 = 0;
                }
            } else {
                s4 = s2;
            }
            i5++;
            s2 = s4;
            s4 = s5;
        }
    }

    private boolean b(int i2, int i3) {
        this.a[this.ab + (this.aa * 2)] = (byte) (i2 >>> 8);
        this.a[this.ab + (this.aa * 2) + 1] = (byte) i2;
        this.a[this.Y + this.aa] = (byte) i3;
        this.aa++;
        if (i2 == 0) {
            short[] sArr = this.S;
            int i4 = i3 * 2;
            sArr[i4] = (short) (sArr[i4] + 1);
        } else {
            this.ac++;
            short[] sArr2 = this.S;
            int i5 = (d.e[i3] + 256 + 1) * 2;
            sArr2[i5] = (short) (sArr2[i5] + 1);
            short[] sArr3 = this.T;
            int a2 = d.a(i2 - 1) * 2;
            sArr3[a2] = (short) (sArr3[a2] + 1);
        }
        if ((this.aa & 8191) == 0 && this.O > 2) {
            int i6 = this.I - this.E;
            int i7 = this.aa * 8;
            for (int i8 = 0; i8 < 30; i8++) {
                i7 = (int) (((long) i7) + (((long) this.T[i8 * 2]) * (5 + ((long) d.b[i8]))));
            }
            int i9 = i7 >>> 3;
            if (this.ac < this.aa / 2 && i9 < i6 / 2) {
                return true;
            }
        }
        return this.aa == this.Z - 1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x00b8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int c(int r20) {
        /*
            r19 = this;
            r0 = r19
            int r0 = r0.M
            r2 = r0
            r0 = r19
            int r0 = r0.I
            r3 = r0
            r0 = r19
            int r0 = r0.L
            r4 = r0
            r0 = r19
            int r0 = r0.I
            r5 = r0
            r0 = r19
            int r0 = r0.s
            r6 = r0
            r7 = 262(0x106, float:3.67E-43)
            int r6 = r6 - r7
            if (r5 <= r6) goto L_0x01ad
            r0 = r19
            int r0 = r0.I
            r5 = r0
            r0 = r19
            int r0 = r0.s
            r6 = r0
            r7 = 262(0x106, float:3.67E-43)
            int r6 = r6 - r7
            int r5 = r5 - r6
        L_0x002c:
            r0 = r19
            int r0 = r0.R
            r6 = r0
            r0 = r19
            int r0 = r0.u
            r7 = r0
            r0 = r19
            int r0 = r0.I
            r8 = r0
            int r8 = r8 + 258
            r0 = r19
            byte[] r0 = r0.v
            r9 = r0
            int r10 = r3 + r4
            r11 = 1
            int r10 = r10 - r11
            byte r9 = r9[r10]
            r0 = r19
            byte[] r0 = r0.v
            r10 = r0
            int r11 = r3 + r4
            byte r10 = r10[r11]
            r0 = r19
            int r0 = r0.L
            r11 = r0
            r0 = r19
            int r0 = r0.Q
            r12 = r0
            if (r11 < r12) goto L_0x005f
            int r2 = r2 >> 2
        L_0x005f:
            r0 = r19
            int r0 = r0.K
            r11 = r0
            if (r6 <= r11) goto L_0x01ba
            r0 = r19
            int r0 = r0.K
            r6 = r0
            r11 = r20
            r16 = r9
            r9 = r3
            r3 = r16
            r17 = r4
            r4 = r6
            r6 = r17
            r18 = r2
            r2 = r10
            r10 = r18
        L_0x007c:
            r0 = r19
            byte[] r0 = r0.v
            r12 = r0
            int r13 = r11 + r6
            byte r12 = r12[r13]
            if (r12 != r2) goto L_0x0192
            r0 = r19
            byte[] r0 = r0.v
            r12 = r0
            int r13 = r11 + r6
            r14 = 1
            int r13 = r13 - r14
            byte r12 = r12[r13]
            if (r12 != r3) goto L_0x0192
            r0 = r19
            byte[] r0 = r0.v
            r12 = r0
            byte r12 = r12[r11]
            r0 = r19
            byte[] r0 = r0.v
            r13 = r0
            byte r13 = r13[r9]
            if (r12 != r13) goto L_0x0192
            r0 = r19
            byte[] r0 = r0.v
            r12 = r0
            int r13 = r11 + 1
            byte r12 = r12[r13]
            r0 = r19
            byte[] r0 = r0.v
            r14 = r0
            int r15 = r9 + 1
            byte r14 = r14[r15]
            if (r12 != r14) goto L_0x0192
            int r9 = r9 + 2
            int r12 = r13 + 1
            r16 = r12
            r12 = r9
            r9 = r16
        L_0x00c1:
            r0 = r19
            byte[] r0 = r0.v
            r13 = r0
            int r12 = r12 + 1
            byte r13 = r13[r12]
            r0 = r19
            byte[] r0 = r0.v
            r14 = r0
            int r9 = r9 + 1
            byte r14 = r14[r9]
            if (r13 != r14) goto L_0x0163
            r0 = r19
            byte[] r0 = r0.v
            r13 = r0
            int r12 = r12 + 1
            byte r13 = r13[r12]
            r0 = r19
            byte[] r0 = r0.v
            r14 = r0
            int r9 = r9 + 1
            byte r14 = r14[r9]
            if (r13 != r14) goto L_0x0163
            r0 = r19
            byte[] r0 = r0.v
            r13 = r0
            int r12 = r12 + 1
            byte r13 = r13[r12]
            r0 = r19
            byte[] r0 = r0.v
            r14 = r0
            int r9 = r9 + 1
            byte r14 = r14[r9]
            if (r13 != r14) goto L_0x0163
            r0 = r19
            byte[] r0 = r0.v
            r13 = r0
            int r12 = r12 + 1
            byte r13 = r13[r12]
            r0 = r19
            byte[] r0 = r0.v
            r14 = r0
            int r9 = r9 + 1
            byte r14 = r14[r9]
            if (r13 != r14) goto L_0x0163
            r0 = r19
            byte[] r0 = r0.v
            r13 = r0
            int r12 = r12 + 1
            byte r13 = r13[r12]
            r0 = r19
            byte[] r0 = r0.v
            r14 = r0
            int r9 = r9 + 1
            byte r14 = r14[r9]
            if (r13 != r14) goto L_0x0163
            r0 = r19
            byte[] r0 = r0.v
            r13 = r0
            int r12 = r12 + 1
            byte r13 = r13[r12]
            r0 = r19
            byte[] r0 = r0.v
            r14 = r0
            int r9 = r9 + 1
            byte r14 = r14[r9]
            if (r13 != r14) goto L_0x0163
            r0 = r19
            byte[] r0 = r0.v
            r13 = r0
            int r12 = r12 + 1
            byte r13 = r13[r12]
            r0 = r19
            byte[] r0 = r0.v
            r14 = r0
            int r9 = r9 + 1
            byte r14 = r14[r9]
            if (r13 != r14) goto L_0x0163
            r0 = r19
            byte[] r0 = r0.v
            r13 = r0
            int r12 = r12 + 1
            byte r13 = r13[r12]
            r0 = r19
            byte[] r0 = r0.v
            r14 = r0
            int r9 = r9 + 1
            byte r14 = r14[r9]
            if (r13 != r14) goto L_0x0163
            if (r12 < r8) goto L_0x00c1
        L_0x0163:
            r9 = r12
            r12 = 258(0x102, float:3.62E-43)
            int r9 = r8 - r9
            int r9 = r12 - r9
            r12 = 258(0x102, float:3.62E-43)
            int r12 = r8 - r12
            if (r9 <= r6) goto L_0x01b8
            r0 = r11
            r1 = r19
            r1.J = r0
            if (r9 >= r4) goto L_0x01b6
            r0 = r19
            byte[] r0 = r0.v
            r2 = r0
            int r3 = r12 + r9
            r6 = 1
            int r3 = r3 - r6
            byte r2 = r2[r3]
            r0 = r19
            byte[] r0 = r0.v
            r3 = r0
            int r6 = r12 + r9
            byte r3 = r3[r6]
            r6 = r9
            r9 = r12
            r16 = r3
            r3 = r2
            r2 = r16
        L_0x0192:
            r0 = r19
            short[] r0 = r0.x
            r12 = r0
            r11 = r11 & r7
            short r11 = r12[r11]
            r12 = 65535(0xffff, float:9.1834E-41)
            r11 = r11 & r12
            if (r11 <= r5) goto L_0x01a4
            int r10 = r10 + -1
            if (r10 != 0) goto L_0x007c
        L_0x01a4:
            r2 = r6
        L_0x01a5:
            r0 = r19
            int r0 = r0.K
            r3 = r0
            if (r2 > r3) goto L_0x01b0
        L_0x01ac:
            return r2
        L_0x01ad:
            r5 = 0
            goto L_0x002c
        L_0x01b0:
            r0 = r19
            int r0 = r0.K
            r2 = r0
            goto L_0x01ac
        L_0x01b6:
            r2 = r9
            goto L_0x01a5
        L_0x01b8:
            r9 = r12
            goto L_0x0192
        L_0x01ba:
            r11 = r20
            r16 = r9
            r9 = r3
            r3 = r16
            r17 = r4
            r4 = r6
            r6 = r17
            r18 = r2
            r2 = r10
            r10 = r18
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.k.c(int):int");
    }

    private void c() {
        if (this.af == 16) {
            a(this.ae);
            this.ae = 0;
            this.af = 0;
        } else if (this.af >= 8) {
            a((byte) this.ae);
            this.ae = (short) (this.ae >>> 8);
            this.af -= 8;
        }
    }

    private void c(short[] sArr, int i2) {
        int i3;
        int i4;
        int i5;
        short s2 = sArr[1];
        if (s2 == 0) {
            i3 = 3;
            i4 = 138;
        } else {
            i3 = 4;
            i4 = 7;
        }
        short s3 = -1;
        int i6 = 0;
        int i7 = i3;
        int i8 = 0;
        short s4 = s2;
        int i9 = i4;
        short s5 = s4;
        while (i6 <= i2) {
            short s6 = sArr[((i6 + 1) * 2) + 1];
            i8++;
            if (i8 >= i9 || s5 != s6) {
                if (i8 < i7) {
                    int i10 = i8;
                    do {
                        a(s5, this.U);
                        i10--;
                    } while (i10 != 0);
                } else if (s5 != 0) {
                    if (s5 != s3) {
                        a(s5, this.U);
                        i5 = i8 - 1;
                    } else {
                        i5 = i8;
                    }
                    a(16, this.U);
                    a(i5 - 3, 2);
                } else if (i8 <= 10) {
                    a(17, this.U);
                    a(i8 - 3, 3);
                } else {
                    a(18, this.U);
                    a(i8 - 11, 7);
                }
                if (s6 == 0) {
                    i7 = 3;
                    i9 = 138;
                    i8 = 0;
                } else if (s5 == s6) {
                    i9 = 6;
                    i8 = 0;
                    i7 = 3;
                } else {
                    i7 = 4;
                    i9 = 7;
                    i8 = 0;
                }
            } else {
                s5 = s3;
            }
            i6++;
            s3 = s5;
            s5 = s6;
        }
    }

    private void d() {
        if (this.af > 8) {
            a(this.ae);
        } else if (this.af > 0) {
            a((byte) this.ae);
        }
        this.ae = 0;
        this.af = 0;
    }

    private void e() {
        do {
            int i2 = (this.w - this.K) - this.I;
            if (i2 == 0 && this.I == 0 && this.K == 0) {
                i2 = this.s;
            } else if (i2 == -1) {
                i2--;
            } else if (this.I >= (this.s + this.s) - 262) {
                System.arraycopy(this.v, this.s, this.v, 0, this.s);
                this.J -= this.s;
                this.I -= this.s;
                this.E -= this.s;
                int i3 = this.A;
                int i4 = i3;
                do {
                    i3--;
                    short s2 = this.y[i3] & 65535;
                    this.y[i3] = s2 >= this.s ? (short) (s2 - this.s) : 0;
                    i4--;
                } while (i4 != 0);
                int i5 = this.s;
                int i6 = i5;
                do {
                    i5--;
                    short s3 = this.x[i5] & 65535;
                    this.x[i5] = s3 >= this.s ? (short) (s3 - this.s) : 0;
                    i6--;
                } while (i6 != 0);
                i2 += this.s;
            }
            if (this.n.c != 0) {
                a aVar = this.n;
                byte[] bArr = this.v;
                int i7 = this.I + this.K;
                int i8 = aVar.c;
                if (i8 <= i2) {
                    i2 = i8;
                }
                if (i2 == 0) {
                    i2 = 0;
                } else {
                    aVar.c -= i2;
                    if (aVar.j.d == 0) {
                        aVar.l = c.a(aVar.l, aVar.a, aVar.b, i2);
                    }
                    System.arraycopy(aVar.a, aVar.b, bArr, i7, i2);
                    aVar.b += i2;
                    aVar.d += (long) i2;
                }
                this.K = i2 + this.K;
                if (this.K >= 3) {
                    this.z = this.v[this.I] & 255;
                    this.z = ((this.z << this.D) ^ (this.v[this.I + 1] & 255)) & this.C;
                }
                if (this.K >= 262) {
                    return;
                }
            } else {
                return;
            }
        } while (this.n.c != 0);
    }

    /* access modifiers changed from: package-private */
    public final int a() {
        if (this.o != 42 && this.o != 113 && this.o != 666) {
            return -2;
        }
        this.a = null;
        this.y = null;
        this.x = null;
        this.v = null;
        return this.o == 113 ? -3 : 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.k.a(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.k.a(com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.a, int, int):int
      com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.k.a(int, int, boolean):void */
    /* access modifiers changed from: package-private */
    public final int a(a aVar, int i2) {
        int i3;
        short s2;
        boolean z2;
        short s3;
        int i4;
        if (i2 > 4 || i2 < 0) {
            return -2;
        }
        if (aVar.e == null || ((aVar.a == null && aVar.c != 0) || (this.o == 666 && i2 != 4))) {
            aVar.i = m[4];
            return -2;
        } else if (aVar.g == 0) {
            aVar.i = m[7];
            return -5;
        } else {
            this.n = aVar;
            int i5 = this.r;
            this.r = i2;
            if (this.o == 42) {
                int i6 = (((this.t - 8) << 4) + 8) << 8;
                int i7 = ((this.O - 1) & 255) >> 1;
                if (i7 > 3) {
                    i7 = 3;
                }
                int i8 = i6 | (i7 << 6);
                if (this.I != 0) {
                    i8 |= 32;
                }
                this.o = 113;
                b(i8 + (31 - (i8 % 31)));
                if (this.I != 0) {
                    b((int) (aVar.l >>> 16));
                    b((int) (aVar.l & 65535));
                }
                aVar.l = c.a(0, null, 0, 0);
            }
            if (this.c != 0) {
                aVar.a();
                if (aVar.g == 0) {
                    this.r = -1;
                    return 0;
                }
            } else if (aVar.c == 0 && i2 <= i5 && i2 != 4) {
                aVar.i = m[7];
                return -5;
            }
            if (this.o != 666 || aVar.c == 0) {
                if (!(aVar.c == 0 && this.K == 0 && (i2 == 0 || this.o == 666))) {
                    char c2 = 65535;
                    switch (l[this.O].e) {
                        case 0:
                            int i9 = 65535;
                            if (65535 > this.p - 5) {
                                i9 = this.p - 5;
                            }
                            while (true) {
                                if (this.K <= 1) {
                                    e();
                                    if (this.K != 0 || i2 != 0) {
                                        if (this.K == 0) {
                                            a(i2 == 4);
                                            if (this.n.g != 0) {
                                                if (i2 != 4) {
                                                    c2 = 1;
                                                    break;
                                                } else {
                                                    c2 = 3;
                                                    break;
                                                }
                                            } else if (i2 != 4) {
                                                c2 = 0;
                                                break;
                                            } else {
                                                c2 = 2;
                                                break;
                                            }
                                        }
                                    } else {
                                        c2 = 0;
                                        break;
                                    }
                                }
                                this.I += this.K;
                                this.K = 0;
                                int i10 = this.E + i9;
                                if (this.I == 0 || this.I >= i10) {
                                    this.K = this.I - i10;
                                    this.I = i10;
                                    a(false);
                                    if (this.n.g == 0) {
                                        c2 = 0;
                                        break;
                                    }
                                }
                                if (this.I - this.E >= this.s - 262) {
                                    a(false);
                                    if (this.n.g == 0) {
                                        c2 = 0;
                                        break;
                                    }
                                }
                            }
                        case 1:
                            short s4 = 0;
                            while (true) {
                                if (this.K < 262) {
                                    e();
                                    if (this.K >= 262 || i2 != 0) {
                                        if (this.K == 0) {
                                            a(i2 == 4);
                                            if (this.n.g != 0) {
                                                if (i2 != 4) {
                                                    c2 = 1;
                                                    break;
                                                } else {
                                                    c2 = 3;
                                                    break;
                                                }
                                            } else if (i2 != 4) {
                                                c2 = 0;
                                                break;
                                            } else {
                                                c2 = 2;
                                                break;
                                            }
                                        }
                                    } else {
                                        c2 = 0;
                                        break;
                                    }
                                }
                                if (this.K >= 3) {
                                    this.z = ((this.z << this.D) ^ (this.v[this.I + 2] & 255)) & this.C;
                                    s4 = this.y[this.z] & 65535;
                                    this.x[this.I & this.u] = this.y[this.z];
                                    this.y[this.z] = (short) this.I;
                                }
                                if (!(((long) s4) == 0 || ((this.I - s4) & 65535) > this.s - 262 || this.P == 2)) {
                                    this.F = c(s4);
                                }
                                if (this.F >= 3) {
                                    boolean b2 = b(this.I - this.J, this.F - 3);
                                    this.K -= this.F;
                                    if (this.F > this.N || this.K < 3) {
                                        this.I += this.F;
                                        this.F = 0;
                                        this.z = this.v[this.I] & 255;
                                        this.z = ((this.z << this.D) ^ (this.v[this.I + 1] & 255)) & this.C;
                                        boolean z3 = b2;
                                        s2 = s4;
                                        z2 = z3;
                                    } else {
                                        this.F--;
                                        do {
                                            this.I++;
                                            this.z = ((this.z << this.D) ^ (this.v[this.I + 2] & 255)) & this.C;
                                            s3 = this.y[this.z] & 65535;
                                            this.x[this.I & this.u] = this.y[this.z];
                                            this.y[this.z] = (short) this.I;
                                            i4 = this.F - 1;
                                            this.F = i4;
                                        } while (i4 != 0);
                                        this.I++;
                                        boolean z4 = b2;
                                        s2 = s3;
                                        z2 = z4;
                                    }
                                } else {
                                    boolean b3 = b(0, this.v[this.I] & 255);
                                    this.K--;
                                    this.I++;
                                    boolean z5 = b3;
                                    s2 = s4;
                                    z2 = z5;
                                }
                                if (z2) {
                                    a(false);
                                    if (this.n.g == 0) {
                                        c2 = 0;
                                        break;
                                    }
                                }
                                s4 = s2;
                            }
                            break;
                        case 2:
                            short s5 = 0;
                            while (true) {
                                if (this.K < 262) {
                                    e();
                                    if (this.K >= 262 || i2 != 0) {
                                        if (this.K == 0) {
                                            if (this.H != 0) {
                                                b(0, this.v[this.I - 1] & 255);
                                                this.H = 0;
                                            }
                                            a(i2 == 4);
                                            if (this.n.g != 0) {
                                                if (i2 != 4) {
                                                    c2 = 1;
                                                    break;
                                                } else {
                                                    c2 = 3;
                                                    break;
                                                }
                                            } else if (i2 != 4) {
                                                c2 = 0;
                                                break;
                                            } else {
                                                c2 = 2;
                                                break;
                                            }
                                        }
                                    } else {
                                        c2 = 0;
                                        break;
                                    }
                                }
                                if (this.K >= 3) {
                                    this.z = ((this.z << this.D) ^ (this.v[this.I + 2] & 255)) & this.C;
                                    s5 = this.y[this.z] & 65535;
                                    this.x[this.I & this.u] = this.y[this.z];
                                    this.y[this.z] = (short) this.I;
                                }
                                this.L = this.F;
                                this.G = this.J;
                                this.F = 2;
                                if (s5 != 0 && this.L < this.N && ((this.I - s5) & 65535) <= this.s - 262) {
                                    if (this.P != 2) {
                                        this.F = c(s5);
                                    }
                                    if (this.F <= 5 && (this.P == 1 || (this.F == 3 && this.I - this.J > 4096))) {
                                        this.F = 2;
                                    }
                                }
                                if (this.L >= 3 && this.F <= this.L) {
                                    int i11 = (this.I + this.K) - 3;
                                    boolean b4 = b((this.I - 1) - this.G, this.L - 3);
                                    this.K -= this.L - 1;
                                    this.L -= 2;
                                    do {
                                        int i12 = this.I + 1;
                                        this.I = i12;
                                        if (i12 <= i11) {
                                            this.z = ((this.z << this.D) ^ (this.v[this.I + 2] & 255)) & this.C;
                                            s5 = this.y[this.z] & 65535;
                                            this.x[this.I & this.u] = this.y[this.z];
                                            this.y[this.z] = (short) this.I;
                                        }
                                        i3 = this.L - 1;
                                        this.L = i3;
                                    } while (i3 != 0);
                                    this.H = 0;
                                    this.F = 2;
                                    this.I++;
                                    if (b4) {
                                        a(false);
                                        if (this.n.g == 0) {
                                            c2 = 0;
                                            break;
                                        }
                                    } else {
                                        continue;
                                    }
                                } else if (this.H != 0) {
                                    if (b(0, this.v[this.I - 1] & 255)) {
                                        a(false);
                                    }
                                    this.I++;
                                    this.K--;
                                    if (this.n.g == 0) {
                                        c2 = 0;
                                        break;
                                    }
                                } else {
                                    this.H = 1;
                                    this.I++;
                                    this.K--;
                                }
                            }
                            break;
                    }
                    if (c2 == 2 || c2 == 3) {
                        this.o = 666;
                    }
                    if (c2 == 0 || c2 == 2) {
                        if (aVar.g == 0) {
                            this.r = -1;
                        }
                        return 0;
                    } else if (c2 == 1) {
                        if (i2 == 1) {
                            a(2, 3);
                            a(256, h.a);
                            c();
                            if (((this.ad + 1) + 10) - this.af < 9) {
                                a(2, 3);
                                a(256, h.a);
                                c();
                            }
                            this.ad = 7;
                        } else {
                            a(0, 0, false);
                            if (i2 == 3) {
                                for (int i13 = 0; i13 < this.A; i13++) {
                                    this.y[i13] = 0;
                                }
                            }
                        }
                        aVar.a();
                        if (aVar.g == 0) {
                            this.r = -1;
                            return 0;
                        }
                    }
                }
                if (i2 != 4) {
                    return 0;
                }
                if (this.d != 0) {
                    return 1;
                }
                b((int) (aVar.l >>> 16));
                b((int) (aVar.l & 65535));
                aVar.a();
                this.d = -1;
                return this.c != 0 ? 0 : 1;
            }
            aVar.i = m[7];
            return -5;
        }
    }

    /* access modifiers changed from: package-private */
    public final int a(a aVar, int i2, int i3) {
        int i4;
        int i5;
        aVar.i = null;
        int i6 = i2 == -1 ? 6 : i2;
        if (i3 < 0) {
            i5 = -i3;
            i4 = 1;
        } else {
            i4 = 0;
            i5 = i3;
        }
        if (i5 < 9 || i5 > 15 || i6 < 0 || i6 > 9) {
            return -2;
        }
        aVar.j = this;
        this.d = i4;
        this.t = i5;
        this.s = 1 << this.t;
        this.u = this.s - 1;
        this.B = 15;
        this.A = 1 << this.B;
        this.C = this.A - 1;
        this.D = ((this.B + 3) - 1) / 3;
        this.v = new byte[(this.s * 2)];
        this.x = new short[this.s];
        this.y = new short[this.A];
        this.Z = 16384;
        this.a = new byte[(this.Z * 4)];
        this.p = this.Z * 4;
        this.ab = this.Z / 2;
        this.Y = this.Z * 3;
        this.O = i6;
        this.P = 0;
        aVar.h = 0;
        aVar.d = 0;
        aVar.i = null;
        this.c = 0;
        this.b = 0;
        if (this.d < 0) {
            this.d = 0;
        }
        this.o = this.d != 0 ? 113 : 42;
        aVar.l = c.a(0, null, 0, 0);
        this.r = 0;
        this.V.h = this.S;
        this.V.j = h.c;
        this.W.h = this.T;
        this.W.j = h.d;
        this.X.h = this.U;
        this.X.j = h.e;
        this.ae = 0;
        this.af = 0;
        this.ad = 8;
        b();
        this.w = this.s * 2;
        this.y[this.A - 1] = 0;
        for (int i7 = 0; i7 < this.A - 1; i7++) {
            this.y[i7] = 0;
        }
        this.N = l[this.O].b;
        this.Q = l[this.O].a;
        this.R = l[this.O].c;
        this.M = l[this.O].d;
        this.I = 0;
        this.E = 0;
        this.K = 0;
        this.L = 2;
        this.F = 2;
        this.H = 0;
        this.z = 0;
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final void a(short[] sArr, int i2) {
        int i3 = this.f[i2];
        int i4 = i2 << 1;
        int i5 = i2;
        while (i4 <= this.g) {
            if (i4 < this.g && a(sArr, this.f[i4 + 1], this.f[i4], this.i)) {
                i4++;
            }
            if (a(sArr, i3, this.f[i4], this.i)) {
                break;
            }
            this.f[i5] = this.f[i4];
            i5 = i4;
            i4 <<= 1;
        }
        this.f[i5] = i3;
    }
}
