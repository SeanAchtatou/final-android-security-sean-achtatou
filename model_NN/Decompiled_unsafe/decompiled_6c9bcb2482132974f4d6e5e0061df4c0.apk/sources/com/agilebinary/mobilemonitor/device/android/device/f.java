package com.agilebinary.mobilemonitor.device.android.device;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.agilebinary.mobilemonitor.device.a.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.c;
import com.agilebinary.mobilemonitor.device.a.c.b.h;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.d.g;
import com.agilebinary.mobilemonitor.device.a.f.b;
import com.agilebinary.mobilemonitor.device.android.e.a;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;
import com.agilebinary.phonebeagle.R;
import java.util.Locale;

public final class f extends b implements d, c, h {
    private static g e;
    private Context c;
    private BackgroundService d;

    public f(Context context, BackgroundService backgroundService) {
        this.c = context;
        this.d = backgroundService;
        a(context);
    }

    public static void a(Context context) {
        new a();
        if (e == null) {
            e = new com.agilebinary.mobilemonitor.device.android.d.a();
        }
        com.agilebinary.mobilemonitor.device.a.i.b.a(e, Locale.getDefault().getLanguage(), Locale.getDefault().getCountry(), new com.agilebinary.mobilemonitor.device.a.e.b());
        com.agilebinary.mobilemonitor.device.a.d.c.a(e);
        e.a(e, new com.agilebinary.mobilemonitor.device.android.b.b.a(context), new com.agilebinary.mobilemonitor.device.a.d.d(), com.agilebinary.mobilemonitor.device.a.d.c.c(), new com.agilebinary.mobilemonitor.device.a.b.a.a.c(context));
    }

    private void b(boolean z) {
        NotificationManager notificationManager = (NotificationManager) this.c.getSystemService("notification");
        if (!this.b || !z) {
            notificationManager.cancelAll();
            return;
        }
        String a = com.agilebinary.mobilemonitor.device.a.i.b.a("STATUSBAR_ACTIVATED_TITLE");
        String a2 = com.agilebinary.mobilemonitor.device.a.i.b.a("STATUSBAR_ACTIVATED_TEXT");
        Notification notification = new Notification(R.drawable.paw_25, a2, System.currentTimeMillis());
        notification.flags = 34;
        notification.setLatestEventInfo(this.c, a, a2, PendingIntent.getBroadcast(this.c, 0, new Intent(), 0));
        notificationManager.notify(0, notification);
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.mobilemonitor.device.a.a.a.b a(com.agilebinary.mobilemonitor.device.a.f.f fVar, e eVar, com.agilebinary.mobilemonitor.device.a.d.c cVar) {
        return new com.agilebinary.mobilemonitor.device.android.b.a.b(this.c, fVar, eVar, cVar);
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.mobilemonitor.device.a.c.b.e a(com.agilebinary.mobilemonitor.device.a.f.f fVar, com.agilebinary.mobilemonitor.device.a.a.a.b bVar, com.agilebinary.mobilemonitor.device.a.d.c cVar, e eVar) {
        return new com.agilebinary.mobilemonitor.device.android.a.a.a(this, fVar, bVar, cVar, eVar);
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.mobilemonitor.device.a.f.g a(com.agilebinary.mobilemonitor.device.a.d.c cVar, e eVar) {
        return new a(this.c, cVar, eVar, com.agilebinary.mobilemonitor.device.a.e.f.a());
    }

    public final void a() {
        a(this.c);
        super.a();
    }

    public final void a(long j) {
        if (this.d != null) {
            this.d.a(j);
        }
    }

    public final void a(long j, int i) {
        if (this.d != null) {
            this.d.a(j, i);
        }
    }

    public final void a(boolean z) {
        b(z);
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.mobilemonitor.device.a.c.a.e b(com.agilebinary.mobilemonitor.device.a.f.f fVar, com.agilebinary.mobilemonitor.device.a.a.a.b bVar, com.agilebinary.mobilemonitor.device.a.d.c cVar, e eVar) {
        return new com.agilebinary.mobilemonitor.device.android.a.c.a(this, fVar, bVar, cVar, eVar);
    }

    public final void b() {
        super.b();
        g().a(this);
        i().a(this);
        b(this.a.r());
        this.a.a(this);
    }

    public final void c() {
        this.a.a((c) null);
        g().b(this);
        i().b(this);
        super.c();
        b(this.a.r());
    }

    public final void j() {
        super.j();
        if (this.d != null) {
            this.d.a();
            this.d.stopSelf();
        }
    }
}
