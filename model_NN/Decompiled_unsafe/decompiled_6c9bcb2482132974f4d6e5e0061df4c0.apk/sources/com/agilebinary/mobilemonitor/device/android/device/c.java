package com.agilebinary.mobilemonitor.device.android.device;

import android.net.wifi.WifiManager;
import android.os.PowerManager;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.k;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.l;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.n;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.p;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.r;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.u;
import com.agilebinary.mobilemonitor.device.a.i.a;
import com.agilebinary.mobilemonitor.device.a.i.b;

public class c {
    private PowerManager.WakeLock a;
    private WifiManager.WifiLock b;
    private int c;
    private String d;

    public c() {
    }

    public c(i iVar, String str, PowerManager.WakeLock wakeLock, WifiManager.WifiLock wifiLock) {
        this.a = wakeLock;
        this.b = wifiLock;
        this.d = str;
    }

    public static String a(r[] rVarArr) {
        String str;
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < rVarArr.length; i++) {
            r rVar = rVarArr[i];
            if (rVar instanceof k) {
                str = b.a("LOCATIONSMS.CELL_BROADCASTNAME_LINE", ((k) rVar).a());
            } else if (rVar instanceof p) {
                p pVar = (p) rVar;
                str = b.a("LOCATIONSMS.CELL_CDMA_LINE", new String[]{new Integer(pVar.a()).toString(), new Integer(pVar.b()).toString(), new Integer(pVar.c()).toString(), new a(pVar.d(), 4).toString(), new a(pVar.f(), 4).toString()});
            } else if (rVar instanceof com.agilebinary.mobilemonitor.device.a.b.a.a.b.a) {
                com.agilebinary.mobilemonitor.device.a.b.a.a.b.a aVar = (com.agilebinary.mobilemonitor.device.a.b.a.a.b.a) rVar;
                str = b.a("LOCATIONSMS.CELL_GSM_LINE", new String[]{new Integer(aVar.a()).toString(), new Integer(aVar.b()).toString(), new Integer(aVar.c()).toString(), new Integer(aVar.d()).toString()});
            } else if (rVar instanceof d) {
                str = b.a("LOCATIONSMS.CELL_IDEN_LINE", new String[]{new Integer(((d) rVar).a()).toString()});
            } else if (rVar instanceof u) {
                u uVar = (u) rVar;
                if (!uVar.h()) {
                    str = b.a("LOCATIONSMS.COMBINED_LINE_NOFIX");
                } else {
                    str = b.a("LOCATIONSMS.COMBINED_LINE", new String[]{new a(uVar.c(), 4).toString(), new a(uVar.d(), 4).toString(), new a(uVar.f(), 0).toString()});
                }
            } else if (rVar instanceof l) {
                l lVar = (l) rVar;
                if (!lVar.h()) {
                    str = b.a("LOCATIONSMS.GPS_LINE_NOFIX");
                } else {
                    str = b.a("LOCATIONSMS.GPS_LINE", new String[]{new a(lVar.c(), 4).toString(), new a(lVar.d(), 4).toString(), new a(lVar.a(), 0).toString(), new a(lVar.f(), 0).toString(), new a(lVar.b(), 0).toString()});
                }
            } else if (rVar instanceof n) {
                n nVar = (n) rVar;
                if (!nVar.h()) {
                    str = b.a("LOCATIONSMS.TOWERTRIANGULATION_LINE_NOFIX");
                } else {
                    str = b.a("LOCATIONSMS.TOWERTRIANGULATION_LINE", new String[]{new a(nVar.c(), 4).toString(), new a(nVar.d(), 4).toString(), new a(nVar.f(), 0).toString()});
                }
            } else {
                rVar.getClass().getName();
                str = null;
            }
            if (str != null) {
                stringBuffer.append(str);
                if (i < rVarArr.length - 1) {
                    stringBuffer.append("\n");
                }
            }
        }
        return stringBuffer.toString();
    }

    public final boolean a() {
        "" + this.c;
        this.c++;
        this.a.acquire();
        this.b.acquire();
        "" + this.c;
        return this.c == 1;
    }

    public final boolean a(boolean z) {
        "WakeLockAndWifiLock.release " + this.d + ", refCount=";
        "" + this.c;
        "" + z;
        if (z) {
            if (this.c > 0) {
                "release WifiLock " + this.d;
                this.b.release();
                "release WakeLock " + this.d;
                this.a.release();
            }
            this.c = 0;
        } else if (this.c > 0) {
            this.c--;
            if (this.c == 0) {
                "release WifiLock " + this.d;
                this.b.release();
                "release WakeLock " + this.d;
                this.a.release();
            }
        }
        "" + this.c;
        return this.c == 0;
    }
}
