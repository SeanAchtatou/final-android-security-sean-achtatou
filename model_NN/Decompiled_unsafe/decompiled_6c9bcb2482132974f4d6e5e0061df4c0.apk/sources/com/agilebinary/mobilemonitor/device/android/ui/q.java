package com.agilebinary.mobilemonitor.device.android.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.agilebinary.phonebeagle.R;
import java.io.Serializable;

public final class q extends ArrayAdapter implements Serializable {
    public q(Context context) {
        super(context, (int) R.layout.loglistitem, (int) R.id.tvMsg);
    }

    /* renamed from: a */
    public final void add(r rVar) {
        super.insert(rVar, 0);
        if (getCount() > 250) {
            super.remove(getItem(getCount() - 1));
        }
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = super.getView(i, view, viewGroup);
        ((TextView) view2.findViewById(R.id.tvTime)).setText(((r) getItem(i)).a);
        return view2;
    }
}
