package com.agilebinary.mobilemonitor.device.android.ui;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class r {
    String a;
    private String b;

    public r(String str) {
        this.b = str;
        this.a = SimpleDateFormat.getTimeInstance().format(new Date());
    }

    public r(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    public final String toString() {
        return this.b;
    }
}
