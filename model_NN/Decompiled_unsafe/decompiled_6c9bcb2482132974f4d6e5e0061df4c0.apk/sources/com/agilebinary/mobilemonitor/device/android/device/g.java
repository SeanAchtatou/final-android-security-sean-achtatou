package com.agilebinary.mobilemonitor.device.android.device;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.agilebinary.mobilemonitor.device.a.e.a.b;
import com.agilebinary.mobilemonitor.device.a.h.a;

final class g extends BroadcastReceiver {
    private String a;
    private b b;
    private PendingIntent c;
    private boolean d;
    private /* synthetic */ i e;

    public g(i iVar, String str, b bVar, PendingIntent pendingIntent, boolean z) {
        this.e = iVar;
        this.a = str;
        this.b = bVar;
        this.c = pendingIntent;
        this.d = z;
        iVar.d.registerReceiver(this, new IntentFilter(str));
    }

    public final PendingIntent a() {
        return this.c;
    }

    public final void onReceive(Context context, Intent intent) {
        if (this.d) {
            this.e.d.unregisterReceiver(this);
            synchronized (this.e.q) {
                this.e.q.remove(this.a);
            }
        }
        try {
            this.b.a();
        } catch (Throwable th) {
            a.b(th);
        }
    }
}
