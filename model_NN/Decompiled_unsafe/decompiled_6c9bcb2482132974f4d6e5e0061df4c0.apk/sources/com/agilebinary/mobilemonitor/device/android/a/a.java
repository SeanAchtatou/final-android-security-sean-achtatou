package com.agilebinary.mobilemonitor.device.android.a;

import com.agilebinary.mobilemonitor.device.a.c.b;
import com.agilebinary.mobilemonitor.device.a.e.f;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;

public final class a extends b {
    private static String a = f.a();
    private HttpResponse b;
    private byte[] c;
    private int d;

    public a(HttpResponse httpResponse) {
        this.b = httpResponse;
        this.d = httpResponse.getStatusLine().getStatusCode();
        try {
            this.c = a(httpResponse.getEntity());
        } catch (IOException e) {
            com.agilebinary.mobilemonitor.device.a.h.a.a(e);
        }
    }

    private synchronized byte[] a(HttpEntity httpEntity) {
        ByteArrayOutputStream byteArrayOutputStream;
        byteArrayOutputStream = new ByteArrayOutputStream();
        httpEntity.writeTo(byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public final String a(String str) {
        Header[] headers = this.b.getHeaders(str);
        if (headers.length == 0) {
            return null;
        }
        return headers[0].getValue();
    }

    public final byte[] a() {
        return this.c;
    }

    public final int b() {
        return this.d;
    }
}
