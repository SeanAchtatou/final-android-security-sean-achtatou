package com.agilebinary.mobilemonitor.device.a.b.a.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;

public final class k extends g {
    private String a;

    public k(a aVar) {
        super(aVar);
        this.a = aVar.i();
    }

    public final String a() {
        return this.a;
    }

    public final String a(d dVar) {
        return super.a(dVar) + "\nCellName: " + this.a;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
    }

    public final byte e() {
        return 9;
    }
}
