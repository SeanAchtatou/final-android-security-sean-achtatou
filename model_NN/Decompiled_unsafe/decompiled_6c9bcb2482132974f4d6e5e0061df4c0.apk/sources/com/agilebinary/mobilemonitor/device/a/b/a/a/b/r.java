package com.agilebinary.mobilemonitor.device.a.b.a.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;

public abstract class r extends m {
    private long a;

    public r(a aVar) {
        super(aVar);
        this.a = aVar.f();
    }

    public r(String str, String str2, long j) {
        super(str, str2);
        this.a = j;
    }

    public String a(d dVar) {
        return super.a(dVar) + "\nTime: " + dVar.a(this.a);
    }

    public void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
    }

    public final long g() {
        return this.a;
    }
}
