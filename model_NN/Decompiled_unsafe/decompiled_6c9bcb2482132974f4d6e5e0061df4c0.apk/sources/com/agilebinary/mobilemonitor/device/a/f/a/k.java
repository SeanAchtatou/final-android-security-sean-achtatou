package com.agilebinary.mobilemonitor.device.a.f.a;

import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.e.f;

public class k implements i {
    private static final String b = f.a();
    protected e a;
    private String c = null;
    private int d = -1;
    private com.agilebinary.mobilemonitor.device.a.f.f e;

    public k(e eVar, com.agilebinary.mobilemonitor.device.a.f.f fVar) {
        this.e = fVar;
        this.a = eVar;
    }

    private String e() {
        return this.e.D();
    }

    private int h() {
        return this.e.E();
    }

    public final void a() {
    }

    public void b() {
        f();
    }

    public void c() {
    }

    public final void d() {
    }

    public void f() {
        this.c = e();
        this.d = h();
    }

    public synchronized a g() {
        a aVar;
        int u = this.a.u();
        String e2 = e();
        e();
        "" + this.d;
        "" + h();
        if (this.c == null && e2 == null) {
            aVar = new a(0, b + ": no current and reference AP");
        } else if (this.c == null || !this.c.equals(e2)) {
            aVar = new a(1, b + ": changed connected AP");
        } else {
            int h = h();
            if (h == -1 || this.d == -1) {
                aVar = new a(0, b + ": rssi unknown");
            } else {
                int abs = Math.abs(this.d - h);
                aVar = abs > u ? new a(1, b + ": rssi change=" + abs) : new a(2, b + ": rssi change=" + abs);
            }
        }
        if (aVar.a != 2) {
            f();
        }
        "" + aVar;
        return aVar;
    }
}
