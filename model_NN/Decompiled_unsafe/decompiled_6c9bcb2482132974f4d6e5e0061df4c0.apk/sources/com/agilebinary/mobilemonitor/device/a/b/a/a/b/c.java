package com.agilebinary.mobilemonitor.device.a.b.a.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.e;

public final class c extends f {
    private long a;
    private String b;
    private String c;
    private boolean d;
    private int e;

    public c(a aVar) {
        super(aVar);
        this.a = aVar.f();
        this.b = aVar.i();
        this.c = aVar.i();
        this.d = aVar.b();
        this.e = aVar.e();
    }

    public c(String str, String str2, e eVar, long j, String str3, String str4, boolean z, int i) {
        super(str, str2, eVar);
        this.a = j;
        this.b = str3;
        this.c = str4;
        this.d = z;
        this.e = i;
    }

    public final String a(d dVar) {
        return super.a(dVar) + "\nTime: " + dVar.a(this.a) + "\nUrl: " + this.b + "\nTitle: " + this.c + "\nIsBookmark: " + this.d + "\nNumVisits: " + this.e;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        aVar.a(this.b);
        aVar.a(this.c);
        aVar.a(this.d);
        aVar.a(this.e);
    }

    public final byte e() {
        return 14;
    }
}
