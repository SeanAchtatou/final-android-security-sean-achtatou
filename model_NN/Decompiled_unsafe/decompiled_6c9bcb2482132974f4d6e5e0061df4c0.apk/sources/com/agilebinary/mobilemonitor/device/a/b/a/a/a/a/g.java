package com.agilebinary.mobilemonitor.device.a.b.a.a.a.a;

public abstract class g {
    public void a() {
    }

    public void a(byte[] bArr) {
        a(bArr, 0, bArr.length);
    }

    public void a(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new NullPointerException();
        } else if (i < 0 || i > bArr.length || i2 < 0 || i + i2 > bArr.length || i + i2 < 0) {
            throw new IndexOutOfBoundsException();
        } else if (i2 != 0) {
            for (int i3 = 0; i3 < i2; i3++) {
                c(bArr[i + i3]);
            }
        }
    }

    public abstract void c(int i);
}
