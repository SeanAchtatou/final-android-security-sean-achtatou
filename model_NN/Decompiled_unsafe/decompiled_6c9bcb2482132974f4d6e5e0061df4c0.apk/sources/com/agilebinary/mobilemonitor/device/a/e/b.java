package com.agilebinary.mobilemonitor.device.a.e;

import java.text.MessageFormat;
import java.text.NumberFormat;

public class b {
    public String a(double d, int i) {
        NumberFormat numberInstance = NumberFormat.getNumberInstance();
        numberInstance.setMaximumFractionDigits(i);
        numberInstance.setMinimumFractionDigits(i);
        return numberInstance.format(d);
    }

    public String a(String str, String[] strArr) {
        return MessageFormat.format(str, (Object[]) strArr);
    }
}
