package com.agilebinary.mobilemonitor.device.android.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.agilebinary.mobilemonitor.device.a.i.b;

final class m extends BroadcastReceiver {
    private /* synthetic */ MainActivity a;

    m(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public final void onReceive(Context context, Intent intent) {
        if (this.a.p != null && this.a.p.isShowing()) {
            this.a.p.cancel();
        }
        if (!intent.hasExtra("EXTRA_SERVICE_STATUS")) {
            return;
        }
        if (intent.getBooleanExtra("EXTRA_SERVICE_STATUS", false)) {
            this.a.w.add(new r(b.a("COMMON_SERVICE_STARTED")));
        } else {
            this.a.w.add(new r(b.a("COMMON_SERVICE_STOPPED")));
        }
    }
}
