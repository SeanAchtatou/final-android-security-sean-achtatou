package com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.a.a.a;

public final class f extends a {
    private a b;
    private int c;
    private int d;
    private byte[] e;
    private byte[] f;
    private boolean g;
    private com.agilebinary.mobilemonitor.device.a.b.a.a.a.a.f h;
    private boolean i;

    public f(com.agilebinary.mobilemonitor.device.a.b.a.a.a.a.f fVar) {
        this(fVar, false);
    }

    private f(com.agilebinary.mobilemonitor.device.a.b.a.a.a.a.f fVar, boolean z) {
        super(fVar);
        this.b = new a();
        this.c = 512;
        this.d = 0;
        this.e = new byte[this.c];
        this.f = new byte[1];
        this.h = null;
        this.i = false;
        this.h = fVar;
        a aVar = this.b;
        aVar.k = new m();
        aVar.k.a(aVar, 0 != 0 ? -15 : 15);
        this.g = false;
        this.b.a = this.e;
        this.b.b = 0;
        this.b.c = 0;
    }

    public final int a() {
        if (a(this.f, 0, 1) == -1) {
            return -1;
        }
        return this.f[0] & 255;
    }

    public final int a(byte[] bArr, int i2, int i3) {
        int a;
        if (i3 == 0) {
            return 0;
        }
        this.b.e = bArr;
        this.b.f = i2;
        this.b.g = i3;
        do {
            if (this.b.c == 0 && !this.i) {
                this.b.b = 0;
                this.b.c = this.h.a(this.e, 0, this.c);
                if (this.b.c == -1) {
                    this.b.c = 0;
                    this.i = true;
                }
            }
            a = this.b.a(0);
            if (this.i && a == -5) {
                return -1;
            }
            if (a == 0 || a == 1) {
                if ((!this.i && a != 1) || this.b.g != i3) {
                    if (this.b.g != i3) {
                        break;
                    }
                } else {
                    return -1;
                }
            } else {
                throw new b("in" + "flating: " + this.b.i);
            }
        } while (a == 0);
        return i3 - this.b.g;
    }
}
