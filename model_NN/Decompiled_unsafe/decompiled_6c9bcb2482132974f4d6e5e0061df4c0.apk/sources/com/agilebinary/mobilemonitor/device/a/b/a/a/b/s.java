package com.agilebinary.mobilemonitor.device.a.b.a.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;

public final class s {
    protected String a;
    protected String b;
    protected String c;
    protected String d;
    protected boolean e;
    private byte[] f;

    public s(a aVar) {
        this.a = aVar.i();
        this.b = aVar.i();
        this.c = aVar.i();
        this.e = aVar.b();
        if (this.e) {
            this.d = aVar.i();
            return;
        }
        this.f = new byte[aVar.e()];
        aVar.a(this.f);
    }

    public s(String str, String str2, String str3, String str4) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = true;
    }

    public s(String str, String str2, String str3, byte[] bArr) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.f = bArr;
        this.e = false;
    }

    public final int a() {
        if (this.e) {
            if (this.d == null) {
                return 0;
            }
            return this.d.length();
        } else if (this.f == null) {
            return 0;
        } else {
            return this.f.length;
        }
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        aVar.a(this.a);
        aVar.a(this.b);
        aVar.a(this.c);
        aVar.a(this.e);
        if (this.e) {
            aVar.a(this.d);
            return;
        }
        aVar.a(this.f.length);
        aVar.a(this.f);
    }
}
