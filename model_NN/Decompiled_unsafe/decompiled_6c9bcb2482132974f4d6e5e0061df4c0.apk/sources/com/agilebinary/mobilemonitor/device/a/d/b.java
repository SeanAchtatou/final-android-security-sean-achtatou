package com.agilebinary.mobilemonitor.device.a.d;

import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.h.a;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public abstract class b {
    public static final String a = f.a();
    protected f b = new f();
    protected g c;
    private boolean d = false;

    protected b(g gVar) {
        this.c = gVar;
    }

    /* access modifiers changed from: protected */
    public final int a(String str) {
        return Integer.parseInt(this.b.a(str));
    }

    /* access modifiers changed from: protected */
    public final int a(String str, int i) {
        return a(str, i, false);
    }

    /* access modifiers changed from: protected */
    public final int a(String str, int i, boolean z) {
        if (z && !this.b.containsKey(str)) {
            return i;
        }
        try {
            return a(str);
        } catch (RuntimeException e) {
            "" + i;
            e.getMessage();
            return i;
        }
    }

    /* access modifiers changed from: package-private */
    public abstract String a();

    /* access modifiers changed from: protected */
    public final String a(String str, String str2) {
        String b2 = b(str);
        return b2 != null ? b2 : str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.f.a(com.agilebinary.mobilemonitor.device.a.d.f, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.a.d.f, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.f.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.d.f.a(java.io.OutputStream, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.e.d.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.d.f.a(com.agilebinary.mobilemonitor.device.a.d.f, boolean):void */
    public final void a(f fVar, boolean z) {
        this.b.a(fVar, true);
    }

    /* access modifiers changed from: protected */
    public final boolean a(String str, boolean z) {
        try {
            return "true".equalsIgnoreCase(this.b.a(str));
        } catch (RuntimeException e) {
            "" + z;
            e.getMessage();
            return z;
        }
    }

    /* access modifiers changed from: protected */
    public final String b(String str) {
        String a2 = this.b.a(str);
        return a2 != null ? a2.trim() : a2;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        try {
            this.b.clear();
            InputStream resourceAsStream = getClass().getResourceAsStream(a());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr = new byte[512];
            while (true) {
                int read = resourceAsStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    this.b.a(new ByteArrayInputStream(this.c.a(byteArrayOutputStream.toByteArray())));
                    return;
                }
            }
        } catch (IOException e) {
            a.c(e);
            System.exit(0);
        }
    }

    /* access modifiers changed from: protected */
    public final void b(String str, String str2) {
        if (str2 == null) {
            this.b.remove(str);
        } else {
            this.b.put(str, str2.trim());
        }
    }
}
