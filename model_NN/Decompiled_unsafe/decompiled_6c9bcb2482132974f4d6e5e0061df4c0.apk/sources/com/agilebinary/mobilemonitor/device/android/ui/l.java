package com.agilebinary.mobilemonitor.device.android.ui;

import android.view.View;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;

final class l implements View.OnClickListener {
    private /* synthetic */ MainActivity a;

    l(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public final void onClick(View view) {
        this.a.m.setEnabled(false);
        try {
            if (e.c().Z()) {
                BackgroundService.a(this.a, "EXTRA_SYNC_NOW");
            }
        } finally {
            this.a.m.setEnabled(true);
        }
    }
}
