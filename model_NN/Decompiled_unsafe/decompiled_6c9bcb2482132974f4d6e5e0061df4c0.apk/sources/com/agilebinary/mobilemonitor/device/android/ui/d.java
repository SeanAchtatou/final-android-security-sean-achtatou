package com.agilebinary.mobilemonitor.device.android.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class d extends BroadcastReceiver {
    final /* synthetic */ MainActivity a;

    d(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent.hasExtra("EXTRA_NUMBER_OF_EVENTS")) {
            this.a.runOnUiThread(new g(this, intent.getLongExtra("EXTRA_NUMBER_OF_EVENTS", 0)));
        }
    }
}
