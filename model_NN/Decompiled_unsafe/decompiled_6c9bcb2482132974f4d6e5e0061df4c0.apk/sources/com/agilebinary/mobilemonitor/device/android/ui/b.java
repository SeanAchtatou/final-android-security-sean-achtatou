package com.agilebinary.mobilemonitor.device.android.ui;

import android.os.AsyncTask;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.android.a.b.a;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;

final class b extends AsyncTask {
    private /* synthetic */ MainActivity a;

    b(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.e.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.b.b(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.d.e.b(java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        String str = ((String[]) objArr)[0];
        c cVar = new c(this.a);
        a f = this.a.y;
        this.a.x.v();
        int a2 = f.a(str);
        if (a2 == 0) {
            e.c().b(str, true);
            cVar.a = true;
            BackgroundService.a(this.a, "EXTRA_START_FROM_GUI");
        } else {
            cVar.a = false;
            cVar.b = com.agilebinary.mobilemonitor.device.a.i.b.a("ACTIVATION_ERROR_" + a2);
        }
        return cVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        c cVar = (c) obj;
        super.onPostExecute(cVar);
        this.a.a();
        this.a.p.hide();
        if (cVar.a) {
            this.a.f.setText("");
            this.a.h.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("COMMONS_ACTIVATED"));
            this.a.e.setVisibility(8);
            return;
        }
        this.a.h.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("COMMONS_ACTIVATION_FAILED", cVar.b));
        this.a.e.setVisibility(0);
    }
}
