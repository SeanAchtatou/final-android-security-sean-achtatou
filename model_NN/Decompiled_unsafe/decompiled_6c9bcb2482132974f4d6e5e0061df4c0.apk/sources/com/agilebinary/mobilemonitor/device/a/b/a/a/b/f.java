package com.agilebinary.mobilemonitor.device.a.b.a.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.e;

public abstract class f extends m {
    private boolean a;
    private double b;
    private double c;
    private boolean d;
    private double e;
    private boolean f;
    private double g;
    private double h;
    private boolean i;
    private double j;
    private boolean k;
    private double l;
    private long m;

    public f(a aVar) {
        super(aVar);
        this.a = aVar.b();
        if (this.a) {
            this.b = aVar.g();
            this.c = aVar.g();
            this.d = aVar.b();
            if (this.d) {
                this.e = aVar.g();
            }
            this.f = aVar.b();
            if (this.f) {
                this.g = aVar.g();
                this.h = aVar.g();
            }
            this.i = aVar.b();
            if (this.i) {
                this.j = aVar.g();
            }
            this.k = aVar.b();
            if (this.k) {
                this.l = aVar.g();
            }
            this.m = aVar.f();
        }
    }

    public f(String str, String str2, e eVar) {
        super(str, str2);
        if (eVar != null) {
            this.a = true;
            this.b = eVar.i();
            this.c = eVar.j();
            this.d = eVar.a();
            if (this.d) {
                this.e = eVar.b();
            }
            this.f = eVar.f();
            if (this.f) {
                this.g = eVar.h();
                this.h = eVar.g();
            }
            this.i = eVar.k();
            if (this.i) {
                this.j = eVar.l();
            }
            this.k = eVar.c();
            if (this.k) {
                this.l = eVar.d();
            }
            this.m = eVar.e();
        }
    }

    public String a(d dVar) {
        return super.a(dVar) + (this.a ? "\nLocation_Latitude: " + this.b + "\nLocation_Longitude: " + this.c + "\nLocation_Altitude: " + this.e + "\nLocation_HorizontalAccuracy: " + this.g + "\nLocation_VerticalAccuracy: " + this.h + "\nLocation_Speed: " + this.j + "\nLocation_Bearing: " + this.l + "\nLocation_fixTime: " + dVar.a(this.m) : "\nNo Location Info!");
    }

    public void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        if (this.a) {
            aVar.a(this.b);
            aVar.a(this.c);
            aVar.a(this.d);
            if (this.d) {
                aVar.a(this.e);
            }
            aVar.a(this.f);
            if (this.f) {
                aVar.a(this.g);
                aVar.a(this.h);
            }
            aVar.a(this.i);
            if (this.i) {
                aVar.a(this.j);
            }
            aVar.a(this.k);
            if (this.k) {
                aVar.a(this.l);
            }
            aVar.a(this.m);
        }
    }
}
