package com.agilebinary.mobilemonitor.device.a.f;

import com.agilebinary.mobilemonitor.device.a.e.a.a;
import com.agilebinary.mobilemonitor.device.a.e.f;

public class d implements a {
    private int a;
    private boolean b;
    private /* synthetic */ e c;

    public d() {
    }

    public d(e eVar, int i, boolean z) {
        this.c = eVar;
        this.a = i;
        this.b = z;
    }

    public static int a(Object obj, String str) {
        return ((Integer) obj.getClass().getMethod(str, new Class[0]).invoke(obj, new Object[0])).intValue();
    }

    public static double b(Object obj, String str) {
        return ((Double) obj.getClass().getMethod(str, new Class[0]).invoke(obj, new Object[0])).doubleValue();
    }

    public final void a() {
        this.c.b(this.a, this.b);
    }

    public final String b() {
        return f.a();
    }

    public final Object c() {
        return "CCX";
    }
}
