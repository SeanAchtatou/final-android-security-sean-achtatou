package com.agilebinary.mobilemonitor.device.a.b.a.a.a.a;

public final class c extends f {
    private byte[] a;
    private int b = 0;
    private int c;

    public c(byte[] bArr) {
        this.a = bArr;
        this.c = bArr.length;
    }

    public final synchronized int a() {
        byte b2;
        if (this.b < this.c) {
            byte[] bArr = this.a;
            int i = this.b;
            this.b = i + 1;
            b2 = bArr[i] & 255;
        } else {
            b2 = -1;
        }
        return b2;
    }

    public final synchronized int a(byte[] bArr, int i, int i2) {
        int i3;
        if (bArr == null) {
            throw new NullPointerException();
        }
        if (i >= 0) {
            if (i <= bArr.length && i2 >= 0 && i + i2 <= bArr.length && i + i2 >= 0) {
                if (this.b >= this.c) {
                    i3 = -1;
                } else {
                    i3 = this.b + i2 > this.c ? this.c - this.b : i2;
                    if (i3 <= 0) {
                        i3 = 0;
                    } else {
                        System.arraycopy(this.a, this.b, bArr, i, i3);
                        this.b += i3;
                    }
                }
            }
        }
        throw new IndexOutOfBoundsException();
        return i3;
    }
}
