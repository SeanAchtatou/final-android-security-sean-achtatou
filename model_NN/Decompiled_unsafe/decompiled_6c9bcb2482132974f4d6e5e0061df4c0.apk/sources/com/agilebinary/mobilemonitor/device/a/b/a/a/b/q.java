package com.agilebinary.mobilemonitor.device.a.b.a.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.e;

public abstract class q extends f {
    private long a;
    private long b;
    private byte c;
    private String d;

    public q(a aVar) {
        super(aVar);
        this.a = aVar.f();
        this.b = aVar.f();
        this.c = aVar.c();
        this.d = aVar.i();
    }

    public q(String str, String str2, e eVar, long j, long j2, byte b2, String str3) {
        super(str, str2, eVar);
        this.a = j;
        this.b = j2;
        this.c = b2;
        this.d = str3;
    }

    public String a(d dVar) {
        return super.a(dVar) + "\nTimeSent: " + dVar.a(this.a) + "\nTimeReceived: " + dVar.a(this.a) + "\nDirection: " + ((int) this.c) + "\nSMSC: " + this.d;
    }

    public void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        aVar.a(this.b);
        aVar.a(this.c);
        aVar.a(this.d);
    }
}
