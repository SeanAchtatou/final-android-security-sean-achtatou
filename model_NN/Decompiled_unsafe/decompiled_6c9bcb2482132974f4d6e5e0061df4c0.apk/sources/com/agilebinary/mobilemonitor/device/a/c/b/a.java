package com.agilebinary.mobilemonitor.device.a.c.b;

import com.agilebinary.mobilemonitor.device.a.b.b;
import com.agilebinary.mobilemonitor.device.a.e.f;
import java.util.Vector;

final class a {
    private String a = f.a();
    private Vector b = new Vector(32);
    private c c;

    a(c cVar, c cVar2) {
        this.c = cVar2;
    }

    /* access modifiers changed from: package-private */
    public final synchronized int a(b bVar) {
        int i;
        boolean z;
        boolean z2;
        "" + bVar.j();
        if (this.b.contains(bVar)) {
            "" + bVar.j();
            i = 0;
        } else {
            int c2 = this.c.c(bVar);
            int i2 = 0;
            boolean z3 = false;
            while (true) {
                if (i2 >= this.b.size()) {
                    i = 2;
                    z = z3;
                    break;
                }
                b bVar2 = (b) this.b.elementAt(i2);
                boolean z4 = this.c.d(bVar2) == 2;
                boolean z5 = this.c.d(bVar) == 2;
                int c3 = this.c.c(bVar2);
                if ((!z5 || !z4) && (z5 || z4)) {
                    if (z5 && !z4) {
                        z2 = true;
                    }
                    z2 = z3;
                } else {
                    if (c2 < c3) {
                        z2 = true;
                    }
                    z2 = z3;
                }
                if (z2) {
                    this.b.insertElementAt(bVar, i2);
                    if (i2 == 0) {
                        z = z2;
                        i = 1;
                    } else {
                        z = z2;
                        i = 2;
                    }
                } else {
                    i2++;
                    z3 = z2;
                }
            }
            if (!z) {
                this.b.addElement(bVar);
                if (this.b.size() == 1) {
                    i = 1;
                }
            }
            "" + bVar.j();
            "" + i;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public final synchronized b a(int i) {
        b bVar;
        if (this.b.size() == 0 || this.b.size() <= i) {
            bVar = null;
        } else {
            bVar = (b) this.b.elementAt(i);
            "" + bVar.j();
        }
        return bVar;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a() {
        this.b.setSize(0);
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean b() {
        return this.b.size() == 0;
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean b(b bVar) {
        return this.b.removeElement(bVar);
    }

    /* access modifiers changed from: package-private */
    public final synchronized int c() {
        return this.b.size();
    }
}
