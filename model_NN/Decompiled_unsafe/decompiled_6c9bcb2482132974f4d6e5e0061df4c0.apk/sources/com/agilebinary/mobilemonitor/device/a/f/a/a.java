package com.agilebinary.mobilemonitor.device.a.f.a;

public final class a {
    int a;
    String b;

    public a(int i, String str) {
        this.a = i;
        this.b = str;
    }

    public final int a() {
        return this.a;
    }

    public final String b() {
        return this.b;
    }

    public final String toString() {
        return "resultCode: " + this.a + ", reason: " + this.b;
    }
}
