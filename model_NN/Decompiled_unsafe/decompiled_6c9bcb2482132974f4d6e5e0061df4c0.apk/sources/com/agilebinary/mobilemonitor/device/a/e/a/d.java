package com.agilebinary.mobilemonitor.device.a.e.a;

import com.agilebinary.mobilemonitor.device.a.e.c;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.h.a;
import java.util.Enumeration;
import java.util.Vector;

public final class d extends Thread implements c {
    private static final String a = f.a();
    private boolean b;
    private Vector c;
    private com.agilebinary.mobilemonitor.device.a.f.f d;
    private String e;

    public d(String str, com.agilebinary.mobilemonitor.device.a.f.f fVar) {
        super(a + "_" + str);
        this.e = str;
        this.d = fVar;
    }

    public final void a() {
        this.c = new Vector();
    }

    public final void a(a aVar) {
        synchronized (this.c) {
            this.c.removeElement(aVar);
        }
    }

    public final void a(a aVar, boolean z) {
        aVar.b() + " [KEY: " + aVar.c() + "]";
        "" + z;
        if (this.c != null) {
            synchronized (this.c) {
                if (z) {
                    Enumeration elements = this.c.elements();
                    while (elements.hasMoreElements()) {
                        a aVar2 = (a) elements.nextElement();
                        "inQUeue: " + aVar2.c();
                        if (aVar.c().equals(aVar2.c())) {
                            aVar.b();
                            return;
                        }
                    }
                }
                this.c.addElement(aVar);
                this.d.c(this.e);
                this.c.notifyAll();
            }
        }
    }

    public final void b() {
        start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.e.a.a, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.r[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.e.d):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, boolean):void */
    public final void c() {
        this.b = true;
        synchronized (this.c) {
            this.c.setSize(0);
            this.c.notifyAll();
        }
        this.d.a(this.e, true);
    }

    public final void d() {
        this.c = null;
    }

    public final void e() {
        synchronized (this.c) {
            this.c.setSize(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.e.a.a, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.r[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.e.d):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, boolean):void */
    public final void run() {
        while (true) {
            this.d.c(this.e);
            while (true) {
                synchronized (this.c) {
                    if (this.c.size() != 0) {
                        a aVar = (a) this.c.elementAt(0);
                        try {
                            aVar.b();
                            aVar.a();
                            aVar.b();
                        } catch (Throwable th) {
                            aVar.b();
                            a.d(th);
                        }
                        if (!this.b) {
                            synchronized (this.c) {
                                if (this.c.size() > 0) {
                                    this.c.removeElementAt(0);
                                }
                            }
                        }
                    }
                }
                try {
                    if (this.b) {
                        break;
                    }
                    synchronized (this.c) {
                        try {
                            this.d.a(this.e, true);
                        } catch (Exception e2) {
                            a.b(e2);
                            e2.printStackTrace();
                        }
                        try {
                            this.c.wait();
                        } catch (InterruptedException e3) {
                            e3.printStackTrace();
                        }
                        if (!this.b) {
                            try {
                                this.d.c(this.e);
                            } catch (Exception e4) {
                                a.b(e4);
                                e4.printStackTrace();
                            }
                        }
                    }
                    if (this.b) {
                        break;
                    }
                } catch (Throwable th2) {
                    a.b(th2);
                    th2.printStackTrace();
                }
            }
            this.d.a(this.e, false);
            if (!this.b) {
                this.c = new Vector();
            } else {
                return;
            }
        }
    }
}
