package com.agilebinary.mobilemonitor.device.a.d;

import com.agilebinary.mobilemonitor.device.a.e.f;

public abstract class g {
    protected static final String a = f.a();
    protected byte[] b = new byte[8];

    public g() {
        this.b[0] = 84;
        this.b[1] = (byte) ((this.b[0] * 2) + 7);
        this.b[2] = (byte) ((this.b[1] - 9) + 13);
        this.b[3] = (byte) ((this.b[2] * 5) + (this.b[1] * 13));
        this.b[4] = (byte) ((this.b[0] * 7) + (this.b[3] * 15));
        this.b[5] = (byte) ((this.b[2] - 9) * 66);
        this.b[6] = 10;
        this.b[7] = (byte) ((this.b[5] * 5) + (this.b[3] * 13));
    }

    public abstract byte[] a(byte[] bArr);

    public abstract byte[] b(byte[] bArr);
}
