package com.agilebinary.mobilemonitor.device.a.f.a;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.k;
import com.agilebinary.mobilemonitor.device.a.e.f;

public final class h implements i {
    private static final String a = f.a();
    private k b;

    public h(k kVar) {
        this.b = kVar;
        if (this.b.b() == 0) {
            this.b.b(new c());
        }
    }

    public final void a() {
        a a2 = this.b.a();
        while (a2.a()) {
            ((i) a2.b()).a();
        }
    }

    public final void b() {
        a a2 = this.b.a();
        while (a2.a()) {
            ((i) a2.b()).b();
        }
    }

    public final void c() {
        a a2 = this.b.a();
        while (a2.a()) {
            ((i) a2.b()).c();
        }
    }

    public final void d() {
        a a2 = this.b.a();
        while (a2.a()) {
            ((i) a2.b()).d();
        }
        this.b.d();
    }

    public final a g() {
        StringBuffer stringBuffer = new StringBuffer();
        a a2 = this.b.a();
        boolean z = true;
        boolean z2 = true;
        while (a2.a()) {
            a g = ((i) a2.b()).g();
            stringBuffer.append(g.b).append(", ");
            if (g.a != 0) {
                if (g.a == 1) {
                    z = false;
                    z2 = false;
                } else {
                    z = false;
                }
            }
        }
        return z ? new a(0, stringBuffer.toString()) : z2 ? new a(2, stringBuffer.toString()) : new a(1, stringBuffer.toString());
    }
}
