package com.agilebinary.mobilemonitor.device.a.a.a.a;

import com.agilebinary.mobilemonitor.device.a.a.a.b;
import com.agilebinary.mobilemonitor.device.a.b.o;
import com.agilebinary.mobilemonitor.device.a.c.b.g;
import java.util.Hashtable;

public final class c {
    private static Hashtable a = new Hashtable();

    public static d a(int i, b bVar, g gVar, o oVar) {
        String valueOf = String.valueOf(i);
        d dVar = (d) a.get(valueOf);
        if (dVar == null) {
            switch (i) {
                case 0:
                    dVar = new f(bVar, gVar);
                    break;
                case 1:
                    dVar = new a(3, bVar, gVar, oVar);
                    break;
                case 2:
                    dVar = new a(2, bVar, gVar, oVar);
                    break;
                case 3:
                    dVar = new a(1, bVar, gVar, oVar);
                    break;
                case 4:
                    dVar = new b(3, bVar, gVar, oVar);
                    break;
                case 5:
                    dVar = new b(2, bVar, gVar, oVar);
                    break;
                case 6:
                    dVar = new b(1, bVar, gVar, oVar);
                    break;
            }
            a.put(valueOf, dVar);
        }
        return dVar;
    }
}
