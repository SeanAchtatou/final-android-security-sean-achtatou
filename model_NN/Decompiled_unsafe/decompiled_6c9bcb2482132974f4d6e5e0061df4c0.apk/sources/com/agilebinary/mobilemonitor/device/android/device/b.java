package com.agilebinary.mobilemonitor.device.android.device;

import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import com.agilebinary.a.a.a.k;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.f.a.g;
import java.util.List;

public final class b extends com.agilebinary.mobilemonitor.device.a.f.a.b {
    private e b;
    private /* synthetic */ i c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(i iVar, e eVar, i iVar2) {
        super(eVar, iVar2);
        this.c = iVar;
    }

    public final void b() {
        this.b = new e(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.wifi.SCAN_RESULTS");
        this.c.d.registerReceiver(this.b, intentFilter);
        super.b();
    }

    public final void c() {
        super.c();
        this.c.d.unregisterReceiver(this.b);
    }

    public final k e() {
        if (this.c.e.startScan()) {
            synchronized (this.b) {
                try {
                    this.b.wait(10000);
                } catch (InterruptedException e) {
                }
            }
        }
        List<ScanResult> scanResults = this.c.e.getScanResults();
        com.agilebinary.a.a.a.e eVar = new com.agilebinary.a.a.a.e();
        if (scanResults != null) {
            for (ScanResult next : scanResults) {
                eVar.b(new g(next.BSSID, next.level));
            }
        }
        return eVar;
    }
}
