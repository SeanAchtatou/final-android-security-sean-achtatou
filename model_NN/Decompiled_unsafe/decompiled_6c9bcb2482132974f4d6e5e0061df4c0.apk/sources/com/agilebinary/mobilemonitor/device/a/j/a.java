package com.agilebinary.mobilemonitor.device.a.j;

import com.agilebinary.mobilemonitor.device.a.e.f;
import java.util.Timer;

public final class a extends Timer {
    private static final String a = f.a();
    private static Timer b = null;

    private a() {
    }

    public static synchronized Timer a() {
        Timer timer;
        synchronized (a.class) {
            if (b == null) {
                b = new Timer();
            }
            timer = b;
        }
        return timer;
    }

    public static void b() {
        if (b != null) {
            b.cancel();
            b = null;
        }
    }
}
