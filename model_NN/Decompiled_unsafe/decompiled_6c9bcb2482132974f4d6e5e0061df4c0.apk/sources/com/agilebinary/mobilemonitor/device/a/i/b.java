package com.agilebinary.mobilemonitor.device.a.i;

import com.agilebinary.mobilemonitor.device.a.d.g;
import com.agilebinary.mobilemonitor.device.a.e.a;
import com.agilebinary.mobilemonitor.device.a.e.f;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class b {
    private static final String a = f.a();
    private static com.agilebinary.mobilemonitor.device.a.d.f b;
    private static g c;
    /* access modifiers changed from: private */
    public static com.agilebinary.mobilemonitor.device.a.e.b d;
    private static String e;
    private static String f;

    public static String a(String str) {
        if (b == null) {
            throw new a();
        }
        Object obj = b.get(str);
        return obj != null ? (String) obj : str;
    }

    public static String a(String str, String str2) {
        return a(str, new String[]{str2});
    }

    public static String a(String str, String[] strArr) {
        String a2 = a(str);
        if (a2 == null) {
            a2 = str;
        }
        return d.a(a2, strArr);
    }

    public static synchronized void a(g gVar, String str, String str2, com.agilebinary.mobilemonitor.device.a.e.b bVar) {
        synchronized (b.class) {
            c = gVar;
            d = bVar;
            if (b == null || !e.equals(str) || !f.equals(str2)) {
                b = new com.agilebinary.mobilemonitor.device.a.d.f();
                b(null, null);
                if (str.trim().length() > 0) {
                    b(str, null);
                    if (str2.trim().length() > 0) {
                        b(str, str2);
                    }
                }
                e = str;
                f = str2;
            }
        }
    }

    private static void b(String str, String str2) {
        String str3 = "/messages";
        if (str != null && str.trim().length() > 0) {
            str3 = str3 + "_" + str.trim();
        }
        if (str2 != null && str2.trim().length() > 0) {
            str3 = str3 + "_" + str2.trim();
        }
        InputStream resourceAsStream = b.class.getResourceAsStream(str3 + ".properties");
        if (resourceAsStream != null) {
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr = new byte[512];
                while (true) {
                    int read = resourceAsStream.read(bArr);
                    if (read != -1) {
                        byteArrayOutputStream.write(bArr, 0, read);
                    } else {
                        b.a(new ByteArrayInputStream(c.a(byteArrayOutputStream.toByteArray())));
                        return;
                    }
                }
            } catch (IOException e2) {
                com.agilebinary.mobilemonitor.device.a.h.a.c(e2);
            }
        }
    }
}
