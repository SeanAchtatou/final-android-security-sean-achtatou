package com.agilebinary.mobilemonitor.device.android.a;

import android.util.Log;
import com.agilebinary.mobilemonitor.device.a.c.c;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.h.a;
import java.io.IOException;
import java.util.TimerTask;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;

public final class d extends TimerTask {
    private static String a = f.a();
    private HttpUriRequest b;
    private long c;

    private d(long j) {
        this.c = j;
    }

    public static HttpResponse a(c cVar, HttpClient httpClient, HttpUriRequest httpUriRequest, long j) {
        return new d(j).a(httpClient, httpUriRequest, cVar);
    }

    private HttpResponse a(HttpClient httpClient, HttpUriRequest httpUriRequest, c cVar) {
        this.b = httpUriRequest;
        c.k().schedule(this, this.c);
        try {
            HttpResponse execute = httpClient.execute(httpUriRequest);
            this.b = null;
            cancel();
            return execute;
        } catch (IOException e) {
            a.a(e);
            cancel();
            return null;
        } catch (Exception e2) {
            a.a(e2);
            cancel();
            return null;
        }
    }

    public final void run() {
        if (this.b != null) {
            this.b.abort();
            Log.d(a, "RequestTimeoutTimerTask aborted a request due to timeout!");
        }
    }
}
