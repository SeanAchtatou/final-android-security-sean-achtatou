package com.agilebinary.mobilemonitor.device.a.b.a.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;

public final class e extends f {
    private long a;
    private String b;

    public e(a aVar) {
        super(aVar);
        this.a = aVar.f();
        this.b = aVar.i();
    }

    public e(String str, String str2, com.agilebinary.mobilemonitor.device.a.b.a.a.e eVar, long j, String str3) {
        super(str, str2, eVar);
        this.a = j;
        this.b = str3;
    }

    public final String a(d dVar) {
        return super.a(dVar) + "\nTime: " + dVar.a(this.a) + "\nKeyword: " + this.b;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        aVar.a(this.b);
    }

    public final byte e() {
        return 13;
    }
}
