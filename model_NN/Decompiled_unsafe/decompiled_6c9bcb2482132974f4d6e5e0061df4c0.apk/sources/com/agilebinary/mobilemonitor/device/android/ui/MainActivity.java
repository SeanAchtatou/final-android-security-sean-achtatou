package com.agilebinary.mobilemonitor.device.android.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.InputFilter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.device.a.d.c;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.g.b;
import com.agilebinary.mobilemonitor.device.android.a.b.a;
import com.agilebinary.mobilemonitor.device.android.device.i;
import com.agilebinary.mobilemonitor.device.android.services.BackgroundService;
import com.agilebinary.phonebeagle.R;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends Activity implements b {
    private static final String a = f.a();
    private Button b;
    private Button c;
    private TextView d;
    /* access modifiers changed from: private */
    public TextView e;
    /* access modifiers changed from: private */
    public EditText f;
    private TextView g;
    /* access modifiers changed from: private */
    public TextView h;
    private TextView i;
    private TextView j;
    private TextView k;
    private Button l;
    /* access modifiers changed from: private */
    public Button m;
    private TextView n;
    private TextView o;
    /* access modifiers changed from: private */
    public ProgressDialog p;
    /* access modifiers changed from: private */
    public ProgressDialog q;
    private BroadcastReceiver r;
    private BroadcastReceiver s;
    private BroadcastReceiver t;
    private BroadcastReceiver u;
    private ListView v;
    /* access modifiers changed from: private */
    public q w;
    /* access modifiers changed from: private */
    public i x;
    /* access modifiers changed from: private */
    public a y;

    static /* synthetic */ void a(MainActivity mainActivity, long j2, int i2) {
        String a2;
        if (j2 == 0) {
            a2 = com.agilebinary.mobilemonitor.device.a.i.b.a("COMMONS_UNKNOWN");
        } else {
            a2 = com.agilebinary.mobilemonitor.device.a.i.b.a("EVENT_UPLOAD_STATUS", new String[]{SimpleDateFormat.getDateTimeInstance().format(new Date(j2)), com.agilebinary.mobilemonitor.device.a.i.b.a("CONNECTION_TYPE_" + i2)});
        }
        mainActivity.j.setText(a2);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        boolean z = this.f.getText().toString().trim().length() == 16;
        this.b.setEnabled(z);
        this.c.setEnabled(z);
        this.m.setEnabled(e.c().Z());
    }

    public final void a(String str, String str2) {
        runOnUiThread(new n(this, com.agilebinary.mobilemonitor.device.a.i.b.a("CONNECTIVITYMANAGERLISTENER." + str, str2 == null ? "" : str2)));
    }

    public final void b() {
        this.f.setText("");
        this.o.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("COMMONS_UNKNOWN"));
        this.j.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("COMMONS_UNKNOWN"));
        this.h.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("COMMONS_NOT_ACTIVATED"));
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        com.agilebinary.mobilemonitor.device.android.device.f.a(this);
        this.x = new i(this, c.c(), e.c(), f.a());
        this.x.a();
        this.x.b();
        this.y = new a(null, this.x, c.c(), e.c());
        this.y.a();
        this.y.b();
        setContentView((int) R.layout.main);
        this.b = (Button) findViewById(R.id.Button_Activate);
        this.c = (Button) findViewById(R.id.Button_Deactivate);
        this.d = (TextView) findViewById(R.id.TextView_productId);
        this.e = (TextView) findViewById(R.id.TextView_buyKey);
        this.h = (TextView) findViewById(R.id.TextView_statusActivationText);
        this.g = (TextView) findViewById(R.id.TextView_statusActivationLabel);
        this.j = (TextView) findViewById(R.id.TextView_statusUploadText);
        this.i = (TextView) findViewById(R.id.TextView_statusUploadLabel);
        this.k = (TextView) findViewById(R.id.TextView_keyLabel);
        this.f = (EditText) findViewById(R.id.EditText_Key);
        this.l = (Button) findViewById(R.id.Button_TestConnection);
        this.m = (Button) findViewById(R.id.Button_uploadNextBatchOfEventsNow);
        this.n = (TextView) findViewById(R.id.TextView_numeventsLabel);
        this.o = (TextView) findViewById(R.id.TextView_numeventsText);
        this.v = (ListView) findViewById(R.id.ListView_StatusLog);
        this.f.setFilters(new InputFilter[]{new InputFilter.LengthFilter(16)});
        this.f.addTextChangedListener(new h(this));
        this.d.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("LABEL_HEADER", new String[]{e.c().m(), c.c().e()}));
        this.e.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("LABEL_BUY_KEY_AT"));
        this.b.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("BUTTON_ACTIVATE"));
        this.c.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("BUTTON_DEACTIVATE"));
        this.l.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("BUTTON_TEST_CONNECTION"));
        this.m.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("BUTTON_SYNCHRONIZE"));
        this.k.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("LABEL_KEY"));
        this.g.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("LABEL_STATUS_ACTIVATION"));
        this.i.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("LABEL_STATUS_UPLOAD"));
        this.n.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("LABEL_NUMBER_OF_EVENTS_IN_DATABASE"));
        this.o.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("COMMONS_UNKNOWN"));
        if (e.c().Z()) {
            this.h.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("COMMONS_ACTIVATED"));
            this.e.setVisibility(8);
        } else {
            this.h.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("COMMONS_NOT_ACTIVATED"));
            this.e.setVisibility(0);
        }
        this.j.setText(com.agilebinary.mobilemonitor.device.a.i.b.a("COMMONS_UNKNOWN"));
        this.w = new q(this);
        this.v.setAdapter((ListAdapter) this.w);
        this.b.setOnClickListener(new i(this));
        this.c.setOnClickListener(new k(this));
        this.l.setOnClickListener(new j(this));
        this.m.setOnClickListener(new l(this));
        this.p = new ProgressDialog(this);
        this.p.setMessage(com.agilebinary.mobilemonitor.device.a.i.b.a("COMMONS_PLEASE_WAIT"));
        this.p.setIndeterminate(true);
        this.p.setCancelable(false);
        this.q = new ProgressDialog(this);
        this.q.setMessage(com.agilebinary.mobilemonitor.device.a.i.b.a("COMMONS_PLEASE_WAIT"));
        this.q.setIndeterminate(true);
        this.q.setCancelable(false);
        this.r = new m(this);
        this.s = new d(this);
        this.t = new e(this);
        this.u = new a(this);
        a();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.q.cancel();
        this.x.c();
        this.x.d();
        this.y.c();
        this.y.d();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.y.h().b(this);
        unregisterReceiver(this.s);
        unregisterReceiver(this.r);
        unregisterReceiver(this.t);
        unregisterReceiver(this.u);
        this.w.clear();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        a();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.y.h().a(this);
        registerReceiver(this.r, new IntentFilter(BackgroundService.a));
        registerReceiver(this.s, new IntentFilter(BackgroundService.b));
        registerReceiver(this.t, new IntentFilter(BackgroundService.c));
        registerReceiver(this.u, new IntentFilter(BackgroundService.d));
        BackgroundService.a(this, "EXTRA_FORCE_BROADCASTS");
        super.onResume();
    }
}
