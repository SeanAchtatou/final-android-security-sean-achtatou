package com.agilebinary.mobilemonitor.device.a.a.a;

import com.agilebinary.mobilemonitor.device.a.b.b;
import com.agilebinary.mobilemonitor.device.a.e.d;

final class c implements d {
    private /* synthetic */ a a;

    c(a aVar) {
        this.a = aVar;
    }

    public final int a(Object obj, Object obj2) {
        b bVar = (b) obj;
        b bVar2 = (b) obj2;
        int i = 0;
        int a2 = bVar.a(this.a.d, this.a.c);
        int a3 = bVar2.a(this.a.d, this.a.c);
        if (a2 == 2 && a3 == 1) {
            i = -1;
        } else if (a2 == 1 && a3 == 2) {
            i = 1;
        }
        if (i != 0) {
            return i;
        }
        int a4 = bVar.a(this.a.c) - bVar2.a(this.a.c);
        if (a4 != 0) {
            return a4;
        }
        if (bVar.j() < bVar2.j()) {
            return -1;
        }
        if (bVar.j() > bVar2.j()) {
            return 1;
        }
        return a4;
    }
}
