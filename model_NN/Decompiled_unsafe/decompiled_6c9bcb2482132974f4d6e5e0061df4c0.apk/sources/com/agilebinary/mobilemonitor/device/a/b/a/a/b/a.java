package com.agilebinary.mobilemonitor.device.a.b.a.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.d;

public final class a extends g {
    private int a = -1;
    private int b = -1;
    private int c = -1;
    private int d = -1;

    public a(com.agilebinary.mobilemonitor.device.a.b.a.a.c.a aVar) {
        super(aVar);
        this.a = aVar.e();
        this.b = aVar.e();
        this.c = aVar.e();
        this.d = aVar.e();
    }

    public a(String str, String str2, long j, int i, int i2, int i3, int i4, String str3) {
        super(str, str2, j, str3);
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
    }

    public final int a() {
        return this.a;
    }

    public final String a(d dVar) {
        return super.a(dVar) + "\nCid: " + this.a + "\nLac: " + this.b + "\nMCC: " + this.c + "\nMNC: " + this.d;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        aVar.a(this.b);
        aVar.a(this.c);
        aVar.a(this.d);
    }

    public final int b() {
        return this.b;
    }

    public final int c() {
        return this.c;
    }

    public final int d() {
        return this.d;
    }

    public final byte e() {
        return 7;
    }
}
