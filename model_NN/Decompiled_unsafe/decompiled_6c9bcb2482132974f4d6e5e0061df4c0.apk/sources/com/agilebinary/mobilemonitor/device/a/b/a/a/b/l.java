package com.agilebinary.mobilemonitor.device.a.b.a.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.e;

public final class l extends t {
    private double b;
    private double c;

    public l(a aVar) {
        super(aVar);
        this.b = aVar.g();
        this.c = aVar.g();
    }

    public l(String str, String str2, long j, double d, double d2, double d3, double d4, double d5, long j2, boolean z, boolean z2, String str3, e eVar) {
        super(str, str2, j, d, d2, d4, j2, z, z2, str3, eVar);
        this.b = d3;
        this.c = d5;
    }

    public final double a() {
        return this.b;
    }

    public final String a(d dVar) {
        return super.a(dVar) + "\nFixTimeUTC: " + dVar.a(this.a) + "\nAltitude: " + this.b + "\nVerticalAccuracy: " + this.c;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.b);
        aVar.a(this.c);
    }

    public final double b() {
        return this.c;
    }

    public final byte e() {
        return 5;
    }
}
