package com.agilebinary.mobilemonitor.device.a.f.a;

public final class g {
    int a;
    private String b;

    public g(String str, int i) {
        this.b = str;
        this.a = i;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof g)) {
            return false;
        }
        return this.b.equals(((g) obj).b);
    }

    public final int hashCode() {
        return this.b.hashCode();
    }
}
