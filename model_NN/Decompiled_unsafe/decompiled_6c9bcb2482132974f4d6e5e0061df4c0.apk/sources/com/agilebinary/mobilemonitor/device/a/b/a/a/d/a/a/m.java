package com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a;

final class m {
    private int a;
    private int b;
    private long[] c = new long[1];
    private long d;
    private int e;
    private int f;
    private g g;

    static {
        byte[] bArr = {0, 0, -1, -1};
    }

    m() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:124:?, code lost:
        return 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int b(com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.a r13, int r14) {
        /*
            r1 = -5
            r12 = -2
            r11 = 13
            r9 = 1
            r8 = 1
            if (r13 == 0) goto L_0x0011
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r0 = r13.k
            if (r0 == 0) goto L_0x0011
            byte[] r0 = r13.a
            if (r0 != 0) goto L_0x0013
        L_0x0011:
            r0 = r12
        L_0x0012:
            return r0
        L_0x0013:
            r0 = 4
            if (r14 != r0) goto L_0x0020
            r0 = r1
        L_0x0017:
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r2 = r13.k
            int r2 = r2.a
            switch(r2) {
                case 0: goto L_0x0022;
                case 1: goto L_0x006d;
                case 2: goto L_0x00b1;
                case 3: goto L_0x00e1;
                case 4: goto L_0x0112;
                case 5: goto L_0x0299;
                case 6: goto L_0x0173;
                case 7: goto L_0x017e;
                case 8: goto L_0x01b8;
                case 9: goto L_0x01e9;
                case 10: goto L_0x021b;
                case 11: goto L_0x024d;
                case 12: goto L_0x0293;
                case 13: goto L_0x0296;
                default: goto L_0x001e;
            }
        L_0x001e:
            r0 = r12
            goto L_0x0012
        L_0x0020:
            r0 = 0
            goto L_0x0017
        L_0x0022:
            int r2 = r13.c
            if (r2 != 0) goto L_0x0028
            r0 = r1
            goto L_0x0012
        L_0x0028:
            int r1 = r13.c
            int r1 = r1 - r8
            r13.c = r1
            long r1 = r13.d
            long r1 = r1 + r9
            r13.d = r1
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            byte[] r2 = r13.a
            int r3 = r13.b
            int r4 = r3 + 1
            r13.b = r4
            byte r2 = r2[r3]
            r1.b = r2
            r1 = r2 & 15
            r2 = 8
            if (r1 == r2) goto L_0x0050
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            r1.a = r11
            java.lang.String r1 = "unknown compression method"
            r13.i = r1
            r1 = r0
            goto L_0x0017
        L_0x0050:
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            int r1 = r1.b
            int r1 = r1 >> 4
            int r1 = r1 + 8
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r2 = r13.k
            int r2 = r2.f
            if (r1 <= r2) goto L_0x0068
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            r1.a = r11
            java.lang.String r1 = "invalid window size"
            r13.i = r1
            r1 = r0
            goto L_0x0017
        L_0x0068:
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            r1.a = r8
            r1 = r0
        L_0x006d:
            int r2 = r13.c
            if (r2 != 0) goto L_0x0073
            r0 = r1
            goto L_0x0012
        L_0x0073:
            int r1 = r13.c
            int r1 = r1 - r8
            r13.c = r1
            long r1 = r13.d
            long r1 = r1 + r9
            r13.d = r1
            byte[] r1 = r13.a
            int r2 = r13.b
            int r3 = r2 + 1
            r13.b = r3
            byte r1 = r1[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r2 = r13.k
            int r2 = r2.b
            int r2 = r2 << 8
            int r2 = r2 + r1
            int r2 = r2 % 31
            if (r2 == 0) goto L_0x009f
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            r1.a = r11
            java.lang.String r1 = "incorrect header check"
            r13.i = r1
            r1 = r0
            goto L_0x0017
        L_0x009f:
            r1 = r1 & 32
            if (r1 != 0) goto L_0x00ab
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            r2 = 7
            r1.a = r2
            r1 = r0
            goto L_0x0017
        L_0x00ab:
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            r2 = 2
            r1.a = r2
            r1 = r0
        L_0x00b1:
            int r2 = r13.c
            if (r2 != 0) goto L_0x00b8
            r0 = r1
            goto L_0x0012
        L_0x00b8:
            int r1 = r13.c
            int r1 = r1 - r8
            r13.c = r1
            long r1 = r13.d
            long r1 = r1 + r9
            r13.d = r1
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            byte[] r2 = r13.a
            int r3 = r13.b
            int r4 = r3 + 1
            r13.b = r4
            byte r2 = r2[r3]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 << 24
            long r2 = (long) r2
            r4 = 4278190080(0xff000000, double:2.113706745E-314)
            long r2 = r2 & r4
            r1.d = r2
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            r2 = 3
            r1.a = r2
            r1 = r0
        L_0x00e1:
            int r2 = r13.c
            if (r2 != 0) goto L_0x00e8
            r0 = r1
            goto L_0x0012
        L_0x00e8:
            int r1 = r13.c
            int r1 = r1 - r8
            r13.c = r1
            long r1 = r13.d
            long r1 = r1 + r9
            r13.d = r1
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            long r2 = r1.d
            byte[] r4 = r13.a
            int r5 = r13.b
            int r6 = r5 + 1
            r13.b = r6
            byte r4 = r4[r5]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << 16
            long r4 = (long) r4
            r6 = 16711680(0xff0000, double:8.256667E-317)
            long r4 = r4 & r6
            long r2 = r2 + r4
            r1.d = r2
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            r2 = 4
            r1.a = r2
            r1 = r0
        L_0x0112:
            int r2 = r13.c
            if (r2 != 0) goto L_0x0119
            r0 = r1
            goto L_0x0012
        L_0x0119:
            int r1 = r13.c
            int r1 = r1 - r8
            r13.c = r1
            long r1 = r13.d
            long r1 = r1 + r9
            r13.d = r1
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            long r2 = r1.d
            byte[] r4 = r13.a
            int r5 = r13.b
            int r6 = r5 + 1
            r13.b = r6
            byte r4 = r4[r5]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << 8
            long r4 = (long) r4
            r6 = 65280(0xff00, double:3.22526E-319)
            long r4 = r4 & r6
            long r2 = r2 + r4
            r1.d = r2
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            r2 = 5
            r1.a = r2
        L_0x0142:
            int r1 = r13.c
            if (r1 == 0) goto L_0x0012
            int r0 = r13.c
            int r0 = r0 - r8
            r13.c = r0
            long r0 = r13.d
            long r0 = r0 + r9
            r13.d = r0
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r0 = r13.k
            long r1 = r0.d
            byte[] r3 = r13.a
            int r4 = r13.b
            int r5 = r4 + 1
            r13.b = r5
            byte r3 = r3[r4]
            long r3 = (long) r3
            r5 = 255(0xff, double:1.26E-321)
            long r3 = r3 & r5
            long r1 = r1 + r3
            r0.d = r1
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r0 = r13.k
            long r0 = r0.d
            r13.l = r0
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r0 = r13.k
            r1 = 6
            r0.a = r1
            r0 = 2
            goto L_0x0012
        L_0x0173:
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r0 = r13.k
            r0.a = r11
            java.lang.String r0 = "need dictionary"
            r13.i = r0
            r0 = r12
            goto L_0x0012
        L_0x017e:
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r2 = r13.k
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.g r2 = r2.g
            int r1 = r2.a(r13, r1)
            r2 = -3
            if (r1 != r2) goto L_0x018f
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r2 = r13.k
            r2.a = r11
            goto L_0x0017
        L_0x018f:
            if (r1 != 0) goto L_0x0192
            r1 = r0
        L_0x0192:
            if (r1 == r8) goto L_0x0197
            r0 = r1
            goto L_0x0012
        L_0x0197:
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.g r1 = r1.g
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r2 = r13.k
            long[] r2 = r2.c
            r1.a(r13, r2)
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            int r1 = r1.e
            if (r1 == 0) goto L_0x01b1
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            r2 = 12
            r1.a = r2
            r1 = r0
            goto L_0x0017
        L_0x01b1:
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            r2 = 8
            r1.a = r2
            r1 = r0
        L_0x01b8:
            int r2 = r13.c
            if (r2 != 0) goto L_0x01bf
            r0 = r1
            goto L_0x0012
        L_0x01bf:
            int r1 = r13.c
            int r1 = r1 - r8
            r13.c = r1
            long r1 = r13.d
            long r1 = r1 + r9
            r13.d = r1
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            byte[] r2 = r13.a
            int r3 = r13.b
            int r4 = r3 + 1
            r13.b = r4
            byte r2 = r2[r3]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 << 24
            long r2 = (long) r2
            r4 = 4278190080(0xff000000, double:2.113706745E-314)
            long r2 = r2 & r4
            r1.d = r2
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            r2 = 9
            r1.a = r2
            r1 = r0
        L_0x01e9:
            int r2 = r13.c
            if (r2 != 0) goto L_0x01f0
            r0 = r1
            goto L_0x0012
        L_0x01f0:
            int r1 = r13.c
            int r1 = r1 - r8
            r13.c = r1
            long r1 = r13.d
            long r1 = r1 + r9
            r13.d = r1
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            long r2 = r1.d
            byte[] r4 = r13.a
            int r5 = r13.b
            int r6 = r5 + 1
            r13.b = r6
            byte r4 = r4[r5]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << 16
            long r4 = (long) r4
            r6 = 16711680(0xff0000, double:8.256667E-317)
            long r4 = r4 & r6
            long r2 = r2 + r4
            r1.d = r2
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            r2 = 10
            r1.a = r2
            r1 = r0
        L_0x021b:
            int r2 = r13.c
            if (r2 != 0) goto L_0x0222
            r0 = r1
            goto L_0x0012
        L_0x0222:
            int r1 = r13.c
            int r1 = r1 - r8
            r13.c = r1
            long r1 = r13.d
            long r1 = r1 + r9
            r13.d = r1
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            long r2 = r1.d
            byte[] r4 = r13.a
            int r5 = r13.b
            int r6 = r5 + 1
            r13.b = r6
            byte r4 = r4[r5]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << 8
            long r4 = (long) r4
            r6 = 65280(0xff00, double:3.22526E-319)
            long r4 = r4 & r6
            long r2 = r2 + r4
            r1.d = r2
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            r2 = 11
            r1.a = r2
            r1 = r0
        L_0x024d:
            int r2 = r13.c
            if (r2 != 0) goto L_0x0254
            r0 = r1
            goto L_0x0012
        L_0x0254:
            int r1 = r13.c
            int r1 = r1 - r8
            r13.c = r1
            long r1 = r13.d
            long r1 = r1 + r9
            r13.d = r1
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            long r2 = r1.d
            byte[] r4 = r13.a
            int r5 = r13.b
            int r6 = r5 + 1
            r13.b = r6
            byte r4 = r4[r5]
            long r4 = (long) r4
            r6 = 255(0xff, double:1.26E-321)
            long r4 = r4 & r6
            long r2 = r2 + r4
            r1.d = r2
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            long[] r1 = r1.c
            r2 = 0
            r1 = r1[r2]
            int r1 = (int) r1
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r2 = r13.k
            long r2 = r2.d
            int r2 = (int) r2
            if (r1 == r2) goto L_0x028d
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r1 = r13.k
            r1.a = r11
            java.lang.String r1 = "incorrect data check"
            r13.i = r1
            r1 = r0
            goto L_0x0017
        L_0x028d:
            com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m r0 = r13.k
            r1 = 12
            r0.a = r1
        L_0x0293:
            r0 = r8
            goto L_0x0012
        L_0x0296:
            r0 = -3
            goto L_0x0012
        L_0x0299:
            r0 = r1
            goto L_0x0142
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.m.b(com.agilebinary.mobilemonitor.device.a.b.a.a.d.a.a.a, int):int");
    }

    /* access modifiers changed from: package-private */
    public final int a(a aVar) {
        if (this.g != null) {
            this.g.a(aVar);
        }
        this.g = null;
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final int a(a aVar, int i) {
        int i2;
        aVar.i = null;
        this.g = null;
        this.e = 0;
        if (i < 0) {
            i2 = -i;
            this.e = 1;
        } else {
            i2 = i;
        }
        if (i2 < 8 || i2 > 15) {
            a(aVar);
            return -2;
        }
        this.f = i2;
        aVar.k.g = new g(aVar, aVar.k.e != 0 ? null : this, 1 << i2);
        if (!(aVar == null || aVar.k == null)) {
            aVar.h = 0;
            aVar.d = 0;
            aVar.i = null;
            aVar.k.a = aVar.k.e != 0 ? 7 : 0;
            aVar.k.g.a(aVar, (long[]) null);
        }
        return 0;
    }
}
