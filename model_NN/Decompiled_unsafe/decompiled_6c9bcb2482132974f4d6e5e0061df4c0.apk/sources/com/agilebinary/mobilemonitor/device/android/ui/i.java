package com.agilebinary.mobilemonitor.device.android.ui;

import android.view.View;

final class i implements View.OnClickListener {
    private /* synthetic */ MainActivity a;

    i(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public final void onClick(View view) {
        String obj = this.a.f.getText().toString();
        if (obj.trim().length() == 16) {
            this.a.p.show();
            new b(this.a).execute(obj);
        }
    }
}
