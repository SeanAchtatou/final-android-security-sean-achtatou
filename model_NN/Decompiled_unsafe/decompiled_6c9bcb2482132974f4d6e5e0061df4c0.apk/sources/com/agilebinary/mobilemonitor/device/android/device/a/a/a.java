package com.agilebinary.mobilemonitor.device.android.device.a.a;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public class a {
    private static final int[] a = {0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 17, 106, 2026, 1000, 1015};
    private static final String[] b = {"*", "US-ASCII", "ISO-8859-1", "ISO-8859-2", "ISO-8859-3", "ISO-8859-4", "ISO-8859-5", "ISO-8859-6", "ISO-8859-7", "ISO-8859-8", "ISO-8859-9", "Shift_JIS", "UTF-8", "Big5", "iso-10646-ucs-2", "UTF-16"};
    private static final HashMap c = new HashMap();
    private static final HashMap d = new HashMap();
    private static /* synthetic */ boolean e = (!a.class.desiredAssertionStatus());

    static {
        if (e || a.length == b.length) {
            int length = a.length - 1;
            for (int i = 0; i <= length; i++) {
                c.put(Integer.valueOf(a[i]), b[i]);
                d.put(b[i], Integer.valueOf(a[i]));
            }
            return;
        }
        throw new AssertionError();
    }

    private a() {
    }

    public static String a(int i) {
        String str = (String) c.get(Integer.valueOf(i));
        if (str != null) {
            return str;
        }
        throw new UnsupportedEncodingException();
    }
}
