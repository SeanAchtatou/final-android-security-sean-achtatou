package com.agilebinary.mobilemonitor.device.android.ui;

import android.text.Editable;
import android.text.TextWatcher;

final class h implements TextWatcher {
    private /* synthetic */ MainActivity a;

    h(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public final void afterTextChanged(Editable editable) {
        this.a.a();
    }

    public final void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public final void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }
}
