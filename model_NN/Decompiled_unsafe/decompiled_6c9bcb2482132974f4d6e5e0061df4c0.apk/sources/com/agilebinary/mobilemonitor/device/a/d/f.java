package com.agilebinary.mobilemonitor.device.a.d;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.e;
import com.agilebinary.a.a.a.i;
import com.agilebinary.mobilemonitor.device.a.e.d;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Hashtable;

public final class f extends Hashtable implements d {
    private static char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    private static char a(int i) {
        return a[i & 15];
    }

    public final int a(Object obj, Object obj2) {
        return ((String) obj).compareTo((String) obj2);
    }

    public final String a(String str) {
        return (String) super.get(str);
    }

    public final void a(f fVar, boolean z) {
        Enumeration keys = keys();
        while (keys.hasMoreElements()) {
            String str = (String) keys.nextElement();
            if (z || !str.endsWith("_cmd")) {
                fVar.put(str, get(str));
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x00ca A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00d2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.io.InputStream r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            r0 = 80
            r1 = 80
            char[] r1 = new char[r1]     // Catch:{ all -> 0x012e }
            int r2 = r13.read()     // Catch:{ all -> 0x012e }
            r10 = r2
            r2 = r0
            r0 = r10
        L_0x000e:
            switch(r0) {
                case -1: goto L_0x012c;
                case 9: goto L_0x0057;
                case 10: goto L_0x0057;
                case 13: goto L_0x0057;
                case 32: goto L_0x0057;
                case 33: goto L_0x0048;
                case 35: goto L_0x0048;
                default: goto L_0x0011;
            }     // Catch:{ all -> 0x012e }
        L_0x0011:
            r3 = 0
            r10 = r3
            r3 = r2
            r2 = r1
            r1 = r10
        L_0x0016:
            if (r0 < 0) goto L_0x005c
            r4 = 61
            if (r0 == r4) goto L_0x005c
            r4 = 58
            if (r0 == r4) goto L_0x005c
            r4 = 32
            if (r0 == r4) goto L_0x005c
            r4 = 9
            if (r0 == r4) goto L_0x005c
            r4 = 10
            if (r0 == r4) goto L_0x005c
            r4 = 13
            if (r0 == r4) goto L_0x005c
            if (r1 < r3) goto L_0x003d
            int r3 = r3 * 2
            char[] r4 = new char[r3]     // Catch:{ all -> 0x012e }
            r5 = 0
            r6 = 0
            int r7 = r2.length     // Catch:{ all -> 0x012e }
            java.lang.System.arraycopy(r2, r5, r4, r6, r7)     // Catch:{ all -> 0x012e }
            r2 = r4
        L_0x003d:
            int r4 = r1 + 1
            char r0 = (char) r0     // Catch:{ all -> 0x012e }
            r2[r1] = r0     // Catch:{ all -> 0x012e }
            int r0 = r13.read()     // Catch:{ all -> 0x012e }
            r1 = r4
            goto L_0x0016
        L_0x0048:
            int r0 = r13.read()     // Catch:{ all -> 0x012e }
            if (r0 < 0) goto L_0x000e
            r3 = 10
            if (r0 == r3) goto L_0x000e
            r3 = 13
            if (r0 != r3) goto L_0x0048
            goto L_0x000e
        L_0x0057:
            int r0 = r13.read()     // Catch:{ all -> 0x012e }
            goto L_0x000e
        L_0x005c:
            r4 = 32
            if (r0 == r4) goto L_0x0064
            r4 = 9
            if (r0 != r4) goto L_0x0069
        L_0x0064:
            int r0 = r13.read()     // Catch:{ all -> 0x012e }
            goto L_0x005c
        L_0x0069:
            r4 = 61
            if (r0 == r4) goto L_0x0071
            r4 = 58
            if (r0 != r4) goto L_0x0075
        L_0x0071:
            int r0 = r13.read()     // Catch:{ all -> 0x012e }
        L_0x0075:
            r4 = 32
            if (r0 == r4) goto L_0x007d
            r4 = 9
            if (r0 != r4) goto L_0x0082
        L_0x007d:
            int r0 = r13.read()     // Catch:{ all -> 0x012e }
            goto L_0x0075
        L_0x0082:
            java.lang.String r4 = new java.lang.String     // Catch:{ all -> 0x012e }
            r5 = 0
            r4.<init>(r2, r5, r1)     // Catch:{ all -> 0x012e }
            r1 = 0
        L_0x0089:
            if (r0 < 0) goto L_0x011f
            r5 = 10
            if (r0 == r5) goto L_0x011f
            r5 = 13
            if (r0 == r5) goto L_0x011f
            r5 = 0
            r6 = 92
            if (r0 != r6) goto L_0x00d9
            int r0 = r13.read()     // Catch:{ all -> 0x012e }
            switch(r0) {
                case 10: goto L_0x00ca;
                case 13: goto L_0x00ba;
                case 110: goto L_0x00e1;
                case 114: goto L_0x00e4;
                case 116: goto L_0x00d7;
                case 117: goto L_0x00e7;
                default: goto L_0x009f;
            }     // Catch:{ all -> 0x012e }
        L_0x009f:
            int r5 = r13.read()     // Catch:{ all -> 0x012e }
            r10 = r5
            r5 = r0
            r0 = r10
        L_0x00a6:
            if (r1 < r3) goto L_0x00b3
            int r3 = r3 * 2
            char[] r6 = new char[r3]     // Catch:{ all -> 0x012e }
            r7 = 0
            r8 = 0
            int r9 = r2.length     // Catch:{ all -> 0x012e }
            java.lang.System.arraycopy(r2, r7, r6, r8, r9)     // Catch:{ all -> 0x012e }
            r2 = r6
        L_0x00b3:
            int r6 = r1 + 1
            char r5 = (char) r5     // Catch:{ all -> 0x012e }
            r2[r1] = r5     // Catch:{ all -> 0x012e }
            r1 = r6
            goto L_0x0089
        L_0x00ba:
            int r0 = r13.read()     // Catch:{ all -> 0x012e }
            r5 = 10
            if (r0 == r5) goto L_0x00ca
            r5 = 32
            if (r0 == r5) goto L_0x00ca
            r5 = 9
            if (r0 != r5) goto L_0x0089
        L_0x00ca:
            int r0 = r13.read()     // Catch:{ all -> 0x012e }
            r5 = 32
            if (r0 == r5) goto L_0x00ca
            r5 = 9
            if (r0 != r5) goto L_0x0089
            goto L_0x00ca
        L_0x00d7:
            r0 = 9
        L_0x00d9:
            int r5 = r13.read()     // Catch:{ all -> 0x012e }
            r10 = r5
            r5 = r0
            r0 = r10
            goto L_0x00a6
        L_0x00e1:
            r0 = 10
            goto L_0x00d9
        L_0x00e4:
            r0 = 13
            goto L_0x00d9
        L_0x00e7:
            int r0 = r13.read()     // Catch:{ all -> 0x012e }
            r6 = 117(0x75, float:1.64E-43)
            if (r0 == r6) goto L_0x00e7
            r6 = 0
            r7 = 0
            r10 = r7
            r7 = r0
            r0 = r10
            r11 = r5
            r5 = r6
            r6 = r11
        L_0x00f7:
            r8 = 4
            if (r0 >= r8) goto L_0x0131
            int r6 = r13.read()     // Catch:{ all -> 0x012e }
            switch(r7) {
                case 48: goto L_0x0103;
                case 49: goto L_0x0103;
                case 50: goto L_0x0103;
                case 51: goto L_0x0103;
                case 52: goto L_0x0103;
                case 53: goto L_0x0103;
                case 54: goto L_0x0103;
                case 55: goto L_0x0103;
                case 56: goto L_0x0103;
                case 57: goto L_0x0103;
                case 65: goto L_0x0116;
                case 66: goto L_0x0116;
                case 67: goto L_0x0116;
                case 68: goto L_0x0116;
                case 69: goto L_0x0116;
                case 70: goto L_0x0116;
                case 97: goto L_0x010d;
                case 98: goto L_0x010d;
                case 99: goto L_0x010d;
                case 100: goto L_0x010d;
                case 101: goto L_0x010d;
                case 102: goto L_0x010d;
                default: goto L_0x0101;
            }     // Catch:{ all -> 0x012e }
        L_0x0101:
            r0 = r6
            goto L_0x00a6
        L_0x0103:
            int r5 = r5 << 4
            int r5 = r5 + r7
            r7 = 48
            int r5 = r5 - r7
        L_0x0109:
            int r0 = r0 + 1
            r7 = r6
            goto L_0x00f7
        L_0x010d:
            int r5 = r5 << 4
            int r5 = r5 + 10
            int r5 = r5 + r7
            r7 = 97
            int r5 = r5 - r7
            goto L_0x0109
        L_0x0116:
            int r5 = r5 << 4
            int r5 = r5 + 10
            int r5 = r5 + r7
            r7 = 65
            int r5 = r5 - r7
            goto L_0x0109
        L_0x011f:
            java.lang.String r5 = new java.lang.String     // Catch:{ all -> 0x012e }
            r6 = 0
            r5.<init>(r2, r6, r1)     // Catch:{ all -> 0x012e }
            r12.put(r4, r5)     // Catch:{ all -> 0x012e }
            r1 = r2
            r2 = r3
            goto L_0x000e
        L_0x012c:
            monitor-exit(r12)
            return
        L_0x012e:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        L_0x0131:
            r0 = r6
            goto L_0x00a6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.device.a.d.f.a(java.io.InputStream):void");
    }

    public final synchronized void a(OutputStream outputStream, String str) {
        PrintStream printStream = new PrintStream(outputStream);
        printStream.write(35);
        printStream.println(System.currentTimeMillis());
        e eVar = new e(size());
        Enumeration keys = keys();
        while (keys.hasMoreElements()) {
            eVar.b((String) keys.nextElement());
        }
        i.a(eVar, this);
        a a2 = eVar.a();
        while (a2.a()) {
            String str2 = (String) a2.b();
            printStream.print(str2);
            printStream.write(61);
            String str3 = (String) get(str2);
            int length = str3.length();
            for (int i = 0; i < length; i++) {
                char charAt = str3.charAt(i);
                switch (charAt) {
                    case 9:
                        printStream.write(92);
                        printStream.write(116);
                        break;
                    case 10:
                        printStream.write(92);
                        printStream.write(110);
                        break;
                    case 13:
                        printStream.write(92);
                        printStream.write(114);
                        break;
                    case '\\':
                        printStream.write(92);
                        printStream.write(92);
                        break;
                    default:
                        if (charAt >= ' ' && charAt < 127) {
                            printStream.write(charAt);
                            break;
                        } else {
                            printStream.write(92);
                            printStream.write(117);
                            printStream.write(a((charAt >> 12) & 15));
                            printStream.write(a((charAt >> 8) & 15));
                            printStream.write(a((charAt >> 4) & 15));
                            printStream.write(a((charAt >> 0) & 15));
                            break;
                        }
                        break;
                }
            }
            printStream.write(10);
        }
    }
}
