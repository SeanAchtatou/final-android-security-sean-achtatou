package com.agilebinary.a.a.a;

public final class e extends s implements k {
    private transient Object[] b;
    private int c;

    public e() {
        this(10);
    }

    public e(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Illegal Capacity: " + i);
        }
        this.b = new Object[i];
    }

    private void c(int i) {
        this.a++;
        int length = this.b.length;
        if (i > length) {
            Object[] objArr = this.b;
            int i2 = ((length * 3) / 2) + 1;
            if (i2 < i) {
                i2 = i;
            }
            this.b = new Object[i2];
            System.arraycopy(objArr, 0, this.b, 0, this.c);
        }
    }

    private void d(int i) {
        if (i >= this.c || i < 0) {
            throw new IndexOutOfBoundsException("Index: " + i + ", Size: " + this.c);
        }
    }

    public final Object a(int i) {
        d(i);
        return this.b[i];
    }

    public final Object a(int i, Object obj) {
        d(i);
        Object obj2 = this.b[i];
        this.b[i] = obj;
        return obj2;
    }

    /* access modifiers changed from: protected */
    public final void a(int i, int i2) {
        this.a++;
        System.arraycopy(this.b, i2, this.b, 0, this.c - i2);
        int i3 = this.c - (i2 - 0);
        while (this.c != i3) {
            Object[] objArr = this.b;
            int i4 = this.c - 1;
            this.c = i4;
            objArr[i4] = null;
        }
    }

    public final boolean a(Object obj) {
        return c(obj) >= 0;
    }

    public final int b() {
        return this.c;
    }

    public final Object b(int i) {
        d(i);
        this.a++;
        Object obj = this.b[i];
        int i2 = (this.c - i) - 1;
        if (i2 > 0) {
            System.arraycopy(this.b, i + 1, this.b, i, i2);
        }
        Object[] objArr = this.b;
        int i3 = this.c - 1;
        this.c = i3;
        objArr[i3] = null;
        return obj;
    }

    public final void b(int i, Object obj) {
        if (i > this.c || i < 0) {
            throw new IndexOutOfBoundsException("Index: " + i + ", Size: " + this.c);
        }
        c(this.c + 1);
        System.arraycopy(this.b, i, this.b, i + 1, this.c - i);
        this.b[i] = obj;
        this.c++;
    }

    public final boolean b(Object obj) {
        c(this.c + 1);
        Object[] objArr = this.b;
        int i = this.c;
        this.c = i + 1;
        objArr[i] = obj;
        return true;
    }

    public final int c(Object obj) {
        int i = 0;
        if (obj == null) {
            while (i < this.c) {
                if (this.b[i] == null) {
                    return i;
                }
                i++;
            }
        } else {
            while (i < this.c) {
                if (obj.equals(this.b[i])) {
                    return i;
                }
                i++;
            }
        }
        return -1;
    }

    public final Object[] c() {
        Object[] objArr = new Object[this.c];
        System.arraycopy(this.b, 0, objArr, 0, this.c);
        return objArr;
    }

    public final Object clone() {
        e eVar = new e();
        eVar.b = new Object[this.c];
        System.arraycopy(this.b, 0, eVar.b, 0, this.c);
        eVar.a = 0;
        return eVar;
    }

    public final void d() {
        this.a++;
        for (int i = 0; i < this.c; i++) {
            this.b[i] = null;
        }
        this.c = 0;
    }
}
