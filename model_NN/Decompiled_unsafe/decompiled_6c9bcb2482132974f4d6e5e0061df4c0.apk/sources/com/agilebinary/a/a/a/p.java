package com.agilebinary.a.a.a;

public abstract class p extends b implements t {
    protected p() {
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof t)) {
            return false;
        }
        n nVar = (n) obj;
        if (nVar.b() != b()) {
            return false;
        }
        a a = nVar.a();
        while (a.a()) {
            if (!a(a.b())) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        a a = a();
        while (a.a()) {
            Object b = a.b();
            if (b != null) {
                i += b.hashCode();
            }
        }
        return i;
    }
}
