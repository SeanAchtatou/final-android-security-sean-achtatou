package com.agilebinary.a.a.a;

import com.agilebinary.mobilemonitor.device.b.a.a.b;

public abstract class l implements q {
    protected l() {
    }

    public int a() {
        return b().b();
    }

    public boolean a(Object obj) {
        a a = b().a();
        if (obj == null) {
            while (a.a()) {
                if (((b) a.b()).a() == null) {
                    return true;
                }
            }
        } else {
            while (a.a()) {
                if (obj.equals(((b) a.b()).a())) {
                    return true;
                }
            }
        }
        return false;
    }

    public abstract t b();

    public Object b(Object obj) {
        a a = b().a();
        if (obj == null) {
            while (a.a()) {
                b bVar = (b) a.b();
                if (bVar.a() == null) {
                    return bVar.b();
                }
            }
        } else {
            while (a.a()) {
                b bVar2 = (b) a.b();
                if (obj.equals(bVar2.a())) {
                    return bVar2.b();
                }
            }
        }
        return null;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof q)) {
            return false;
        }
        q qVar = (q) obj;
        if (qVar.a() != a()) {
            return false;
        }
        a a = b().a();
        while (a.a()) {
            b bVar = (b) a.b();
            Object a2 = bVar.a();
            Object b = bVar.b();
            if (b == null) {
                if (qVar.b(a2) != null || !qVar.a(a2)) {
                    return false;
                }
            } else if (!b.equals(qVar.b(a2))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int i = 0;
        a a = b().a();
        while (a.a()) {
            i += a.b().hashCode();
        }
        return i;
    }

    public String toString() {
        int a = a() - 1;
        StringBuffer stringBuffer = new StringBuffer();
        a a2 = b().a();
        stringBuffer.append("{");
        for (int i = 0; i <= a; i++) {
            b bVar = (b) a2.b();
            stringBuffer.append(bVar.a() + "=" + bVar.b());
            if (i < a) {
                stringBuffer.append(", ");
            }
        }
        stringBuffer.append("}");
        return stringBuffer.toString();
    }
}
