package com.agilebinary.a.a.a;

import com.agilebinary.mobilemonitor.device.a.e.d;
import java.util.Random;

public final class i {
    public static final t a = new r();

    static {
        new Random();
        new o();
        new m();
        new d();
    }

    private i() {
    }

    public static void a(k kVar, d dVar) {
        Object[] c = kVar.c();
        c.a(c, dVar);
        f e = kVar.e();
        for (Object a2 : c) {
            e.b();
            e.a(a2);
        }
    }
}
