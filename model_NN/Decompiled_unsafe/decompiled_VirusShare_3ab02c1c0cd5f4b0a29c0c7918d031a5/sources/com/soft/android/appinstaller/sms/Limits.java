package com.soft.android.appinstaller.sms;

import com.soft.android.appinstaller.core.SmsInfo;

public class Limits {
    private int dcSmsRest;
    private int moneyRest;
    private int smsRest;

    public Limits(int moneyRest2, int smsRest2, int dcSMSRest) {
        this.moneyRest = moneyRest2;
        this.smsRest = smsRest2;
        this.dcSmsRest = dcSMSRest;
    }

    public int expectedMoneyRest() {
        return this.moneyRest;
    }

    public int smsCountRest() {
        return this.smsRest;
    }

    public int dcSmsCountRest() {
        return this.dcSmsRest;
    }

    public void registerSuccessfulPayment(SmsInfo.SMS sms) {
        this.smsRest--;
        this.dcSmsRest--;
        this.moneyRest -= sms.getCost();
    }

    public void registerFailedPaymend(SmsInfo.SMS sms) {
        this.moneyRest = sms.getCost() - 1;
    }
}
