package com.soft.install;

import android.os.AsyncTask;

public class AsyncLoader extends AsyncTask<String, Integer, String> {
    private static final long NUM_SECONDS_TO_LOAD = 4;

    /* access modifiers changed from: protected */
    public String doInBackground(String... urls) {
        do {
        } while ((System.currentTimeMillis() - System.currentTimeMillis()) / 1000 <= NUM_SECONDS_TO_LOAD);
        return null;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String result) {
    }
}
