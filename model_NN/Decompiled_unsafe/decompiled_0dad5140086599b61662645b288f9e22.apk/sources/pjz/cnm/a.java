package pjz.cnm;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public class a {
    private static String strDefaultKey = "national";
    private Cipher decryptCipher;
    private Cipher encryptCipher;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: javax.crypto.Cipher.getInstance(java.lang.String):javax.crypto.Cipher in method: pjz.cnm.a.<init>(java.lang.String):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: javax.crypto.Cipher.getInstance(java.lang.String):javax.crypto.Cipher
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:528)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public a(java.lang.String r1) {
        /*
            r9 = this;
            r0 = r9
            r1 = r10
            r6 = r0
            r6.<init>()
            r6 = r0
            r7 = 0
            javax.crypto.Cipher r7 = (javax.crypto.Cipher) r7
            r6.encryptCipher = r7
            r6 = r0
            r7 = 0
            javax.crypto.Cipher r7 = (javax.crypto.Cipher) r7
            r6.decryptCipher = r7
            r6 = r0
            r7 = r1
            byte[] r7 = r7.getBytes()     // Catch:{ Exception -> 0x0040 }
            java.security.Key r6 = r6.getKey(r7)     // Catch:{ Exception -> 0x0040 }
            r3 = r6
            r6 = r0
            java.lang.String r7 = "DES"
            javax.crypto.Cipher r7 = javax.crypto.Cipher.getInstance(r7)     // Catch:{ Exception -> 0x0040 }
            r6.encryptCipher = r7     // Catch:{ Exception -> 0x0040 }
            r6 = r0
            javax.crypto.Cipher r6 = r6.encryptCipher     // Catch:{ Exception -> 0x0040 }
            r7 = 1
            r8 = r3
            r6.init(r7, r8)     // Catch:{ Exception -> 0x0040 }
            r6 = r0
            java.lang.String r7 = "DES"
            javax.crypto.Cipher r7 = javax.crypto.Cipher.getInstance(r7)     // Catch:{ Exception -> 0x0040 }
            r6.decryptCipher = r7     // Catch:{ Exception -> 0x0040 }
            r6 = r0
            javax.crypto.Cipher r6 = r6.decryptCipher     // Catch:{ Exception -> 0x0040 }
            r7 = 2
            r8 = r3
            r6.init(r7, r8)     // Catch:{ Exception -> 0x0040 }
        L_0x003f:
            return
        L_0x0040:
            r6 = move-exception
            r4 = r6
            r6 = r4
            r6.printStackTrace()
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: pjz.cnm.a.<init>(java.lang.String):void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public static String byteArr2HexStr(byte[] bArr) throws Exception {
        StringBuffer stringBuffer;
        int i;
        byte[] bArr2 = bArr;
        int length = bArr2.length;
        new StringBuffer(length * 2);
        StringBuffer stringBuffer2 = stringBuffer;
        for (int i2 = 0; i2 < length; i2++) {
            int i3 = bArr2[i2];
            while (true) {
                i = i3;
                if (i >= 0) {
                    break;
                }
                i3 = i + 256;
            }
            if (i < 16) {
                StringBuffer append = stringBuffer2.append('0');
            }
            StringBuffer append2 = stringBuffer2.append(Integer.toString(i, 16));
        }
        return stringBuffer2.toString();
    }

    public static byte[] hexStr2ByteArr(String str) throws Exception {
        String str2;
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        byte[] bArr = new byte[(length / 2)];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= length) {
                return bArr;
            }
            new String(bytes, i2, 2);
            bArr[i2 / 2] = (byte) Integer.parseInt(str2, 16);
            i = i2 + 2;
        }
    }

    public a() throws Exception {
        this(strDefaultKey);
    }

    public byte[] encrypt(byte[] bArr) throws Exception {
        return this.encryptCipher.doFinal(bArr);
    }

    public String encrypt(String str) throws Exception {
        return byteArr2HexStr(encrypt(str.getBytes()));
    }

    public byte[] decrypt(byte[] bArr) throws Exception {
        return this.decryptCipher.doFinal(bArr);
    }

    public String decrypt(String str) throws Exception {
        String str2;
        new String(decrypt(hexStr2ByteArr(str)));
        return str2;
    }

    private Key getKey(byte[] bArr) throws Exception {
        Key key;
        byte[] bArr2 = bArr;
        byte[] bArr3 = new byte[8];
        int i = 0;
        while (i < bArr2.length && i < bArr3.length) {
            bArr3[i] = bArr2[i];
            i++;
        }
        new SecretKeySpec(bArr3, "DES");
        return key;
    }
}
