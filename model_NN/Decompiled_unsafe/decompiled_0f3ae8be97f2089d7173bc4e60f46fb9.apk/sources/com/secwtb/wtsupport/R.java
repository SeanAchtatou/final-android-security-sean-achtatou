package com.secwtb.wtsupport;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int banrtgd = 2130837514;
        public static final int bmaijklwwrlu = 2130837515;
        public static final int cnuqasu = 2130837516;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837517;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837518;
        public static final int fbi_btn_default_focused_holo_dark = 2130837519;
        public static final int fbi_btn_default_normal_holo_dark = 2130837520;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837521;
        public static final int gapdlu = 2130837522;
        public static final int hikifvc = 2130837523;
        public static final int hkktp = 2130837524;
        public static final int htsojf = 2130837525;
        public static final int ic_launcher = 2130837526;
        public static final int kmpkgqdql = 2130837527;
        public static final int lqhrvp = 2130837528;
        public static final int nkasjtfw = 2130837529;
        public static final int oodjnou = 2130837530;
        public static final int sphckjpl = 2130837531;
        public static final int spinner_48_inner_holo = 2130837532;
    }

    public static final class id {
        public static final int aacicctjou = 2131165200;
        public static final int amluvskv = 2131165195;
        public static final int aogejv = 2131165187;
        public static final int aptcant = 2131165185;
        public static final int bweiook = 2131165228;
        public static final int causb = 2131165222;
        public static final int cgrmwb = 2131165226;
        public static final int cpvqws = 2131165190;
        public static final int dioms = 2131165235;
        public static final int eelurf = 2131165211;
        public static final int gjiuarkbf = 2131165186;
        public static final int gvcidp = 2131165210;
        public static final int hdidvogbos = 2131165202;
        public static final int hpvhato = 2131165198;
        public static final int ighnlwmf = 2131165229;
        public static final int ijnsemj = 2131165231;
        public static final int imjmucw = 2131165207;
        public static final int jbclqlba = 2131165233;
        public static final int jfoprjlg = 2131165199;
        public static final int jjipqdpns = 2131165188;
        public static final int jpemqrcg = 2131165245;
        public static final int kpatqrpr = 2131165209;
        public static final int kwwek = 2131165215;
        public static final int lvnpnvg = 2131165218;
        public static final int mdiwmr = 2131165208;
        public static final int mmdbl = 2131165192;
        public static final int mrvjl = 2131165221;
        public static final int mwikdtk = 2131165244;
        public static final int ndtce = 2131165240;
        public static final int nknbkab = 2131165227;
        public static final int novidfiui = 2131165213;
        public static final int oinoss = 2131165184;
        public static final int ompwp = 2131165241;
        public static final int otdvkr = 2131165246;
        public static final int pbvfqgg = 2131165194;
        public static final int pjlsna = 2131165212;
        public static final int pleasm = 2131165239;
        public static final int pmragnlh = 2131165216;
        public static final int pwgaitr = 2131165197;
        public static final int qaokofmqde = 2131165223;
        public static final int qfqbsoc = 2131165205;
        public static final int qngankcp = 2131165238;
        public static final int qpjboum = 2131165204;
        public static final int qvwqwri = 2131165193;
        public static final int rgpqfjna = 2131165237;
        public static final int rktabp = 2131165230;
        public static final int rnrnjrb = 2131165219;
        public static final int rwjdoi = 2131165191;
        public static final int sgkdarh = 2131165224;
        public static final int slstmjlcs = 2131165225;
        public static final int tfcliomw = 2131165242;
        public static final int tgqjsjw = 2131165236;
        public static final int thonl = 2131165214;
        public static final int uaiqhtvl = 2131165220;
        public static final int uhswfpke = 2131165217;
        public static final int uqtldwr = 2131165196;
        public static final int vbwsoe = 2131165206;
        public static final int vdfia = 2131165243;
        public static final int whiwdo = 2131165189;
        public static final int wjiiqj = 2131165201;
        public static final int wocvjq = 2131165234;
        public static final int wqjlrvrn = 2131165232;
        public static final int wvtrvtfaj = 2131165203;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int ieslad = 2131230723;
        public static final int klgwl = 2131230722;
        public static final int lfijtaum = 2131230725;
        public static final int mckslj = 2131230724;
        public static final int qjsqbue = 2131230721;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
