package com.sect.hsupport;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int bqbdshmoc = 2130837514;
        public static final int coteauvt = 2130837515;
        public static final int delqotda = 2130837516;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837517;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837518;
        public static final int fbi_btn_default_focused_holo_dark = 2130837519;
        public static final int fbi_btn_default_normal_holo_dark = 2130837520;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837521;
        public static final int hdsufjmd = 2130837522;
        public static final int heenmgnh = 2130837523;
        public static final int ic_launcher = 2130837524;
        public static final int iphcfe = 2130837525;
        public static final int kippbfjb = 2130837526;
        public static final int ksvmanq = 2130837527;
        public static final int nuidn = 2130837528;
        public static final int panvlouj = 2130837529;
        public static final int pegmidjav = 2130837530;
        public static final int skcgmh = 2130837531;
        public static final int spinner_48_inner_holo = 2130837532;
    }

    public static final class id {
        public static final int abgndf = 2131165227;
        public static final int afqjb = 2131165237;
        public static final int bagunfw = 2131165201;
        public static final int cuacgcqonb = 2131165202;
        public static final int cvuho = 2131165199;
        public static final int debgabu = 2131165224;
        public static final int dmjkdcr = 2131165196;
        public static final int eeiadpvr = 2131165188;
        public static final int epjfg = 2131165193;
        public static final int esnaw = 2131165200;
        public static final int euhcb = 2131165214;
        public static final int fncogarnl = 2131165231;
        public static final int fqbrrfckk = 2131165240;
        public static final int gbsmau = 2131165198;
        public static final int gfujtvhsod = 2131165186;
        public static final int gjjprv = 2131165185;
        public static final int gkplihmf = 2131165233;
        public static final int hhmsor = 2131165206;
        public static final int hjhwddn = 2131165243;
        public static final int huefvfbh = 2131165192;
        public static final int iajrtmmi = 2131165212;
        public static final int ifchaqg = 2131165189;
        public static final int jckcq = 2131165223;
        public static final int jqclfwkf = 2131165217;
        public static final int jvjamgs = 2131165191;
        public static final int kmefp = 2131165230;
        public static final int kpsmcwe = 2131165207;
        public static final int kucdud = 2131165245;
        public static final int kwtmwbla = 2131165221;
        public static final int lkuopkf = 2131165218;
        public static final int nhewtemn = 2131165204;
        public static final int nrrvauidks = 2131165209;
        public static final int nwfedse = 2131165205;
        public static final int omnmph = 2131165194;
        public static final int onpdehjfue = 2131165220;
        public static final int qolwjpkor = 2131165190;
        public static final int rgebupenj = 2131165219;
        public static final int rhjdcnggj = 2131165239;
        public static final int rmhudnp = 2131165208;
        public static final int rudiine = 2131165226;
        public static final int sefmo = 2131165234;
        public static final int servsfln = 2131165197;
        public static final int sfvcgekocu = 2131165187;
        public static final int shmrtrwq = 2131165216;
        public static final int stnhua = 2131165195;
        public static final int swwqturp = 2131165238;
        public static final int tehbgc = 2131165242;
        public static final int tlvfjll = 2131165184;
        public static final int tsuudkqqof = 2131165244;
        public static final int tummft = 2131165235;
        public static final int uahvwnmu = 2131165210;
        public static final int ufnwwunqidt = 2131165236;
        public static final int uiubkhdmu = 2131165232;
        public static final int ulifarvgt = 2131165222;
        public static final int ultmcj = 2131165246;
        public static final int uqwvagu = 2131165229;
        public static final int uwngng = 2131165228;
        public static final int vghgoqlr = 2131165203;
        public static final int vqauutito = 2131165225;
        public static final int vwbvmslo = 2131165215;
        public static final int wjgphl = 2131165211;
        public static final int wjwjruog = 2131165241;
        public static final int wojun = 2131165213;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int fuheqw = 2131230721;
        public static final int mgdldcsehs = 2131230724;
        public static final int njstvnw = 2131230725;
        public static final int ppcfii = 2131230722;
        public static final int wwjpmltkke = 2131230723;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
