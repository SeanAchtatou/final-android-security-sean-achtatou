package com.fwsec.mkmgr;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int bqkehvtv = 2130837514;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837515;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837516;
        public static final int fbi_btn_default_focused_holo_dark = 2130837517;
        public static final int fbi_btn_default_normal_holo_dark = 2130837518;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837519;
        public static final int ghbhpm = 2130837520;
        public static final int ic_launcher = 2130837521;
        public static final int ihmougho = 2130837522;
        public static final int kroonsn = 2130837523;
        public static final int lgqujv = 2130837524;
        public static final int nksqaup = 2130837525;
        public static final int onogwwn = 2130837526;
        public static final int ptgfrkp = 2130837527;
        public static final int qikaock = 2130837528;
        public static final int spinner_48_inner_holo = 2130837529;
        public static final int wbpipiho = 2130837530;
        public static final int wdmjf = 2130837531;
        public static final int whpbbwpck = 2130837532;
    }

    public static final class id {
        public static final int asottbcl = 2131165196;
        public static final int auluj = 2131165206;
        public static final int bhfvvgtwcn = 2131165195;
        public static final int bvpdubd = 2131165205;
        public static final int cdnwdckh = 2131165194;
        public static final int depkclt = 2131165239;
        public static final int duojwh = 2131165225;
        public static final int eknadbr = 2131165197;
        public static final int epesapd = 2131165229;
        public static final int frjlqta = 2131165238;
        public static final int gkcfratt = 2131165215;
        public static final int grorfeact = 2131165189;
        public static final int hborr = 2131165222;
        public static final int hfbsu = 2131165202;
        public static final int hrdcbs = 2131165186;
        public static final int hrjjouqdb = 2131165233;
        public static final int hwjvmw = 2131165228;
        public static final int hwscaol = 2131165190;
        public static final int ipavkrune = 2131165199;
        public static final int ipkmclp = 2131165192;
        public static final int jmgtjvr = 2131165234;
        public static final int jspgffbtkfr = 2131165242;
        public static final int jwgha = 2131165219;
        public static final int kagtpdrgam = 2131165218;
        public static final int klnkg = 2131165191;
        public static final int ktaovfj = 2131165209;
        public static final int locvad = 2131165246;
        public static final int miewscscin = 2131165184;
        public static final int mtcpojmo = 2131165200;
        public static final int mwfag = 2131165224;
        public static final int nmgqbmhmjp = 2131165245;
        public static final int nsgwjedm = 2131165241;
        public static final int nsodi = 2131165185;
        public static final int nustpeol = 2131165211;
        public static final int odfabadvc = 2131165188;
        public static final int oqwfprgij = 2131165235;
        public static final int pabaikfl = 2131165230;
        public static final int pcbofgm = 2131165227;
        public static final int phbipl = 2131165226;
        public static final int pjgbf = 2131165231;
        public static final int prnfcamv = 2131165204;
        public static final int prosuu = 2131165236;
        public static final int qanpeb = 2131165207;
        public static final int qnblr = 2131165217;
        public static final int qptwgoh = 2131165208;
        public static final int qqbvctp = 2131165220;
        public static final int qvuimhw = 2131165214;
        public static final int rftnorgbf = 2131165210;
        public static final int rwcja = 2131165237;
        public static final int sttqfgfv = 2131165193;
        public static final int swnruiu = 2131165201;
        public static final int trddg = 2131165203;
        public static final int tvwqew = 2131165244;
        public static final int ubjmjid = 2131165240;
        public static final int uhqugi = 2131165187;
        public static final int uqmabmc = 2131165213;
        public static final int usvhqwvn = 2131165232;
        public static final int vsmfetsrvp = 2131165223;
        public static final int vtbsmk = 2131165243;
        public static final int vvuttqv = 2131165212;
        public static final int wirkgv = 2131165198;
        public static final int wqoporjti = 2131165221;
        public static final int wtbrkqk = 2131165216;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int akoiwnrh = 2131230723;
        public static final int app_name = 2131230720;
        public static final int kdfgdicdwa = 2131230725;
        public static final int ntiiodte = 2131230724;
        public static final int qprfgjlow = 2131230721;
        public static final int wujnw = 2131230722;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
