package com.seci.csmngr;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int bcuhel = 2130837514;
        public static final int cldhveauo = 2130837515;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837516;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837517;
        public static final int fbi_btn_default_focused_holo_dark = 2130837518;
        public static final int fbi_btn_default_normal_holo_dark = 2130837519;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837520;
        public static final int gutmkin = 2130837521;
        public static final int ic_launcher = 2130837522;
        public static final int ieovukgh = 2130837523;
        public static final int jgmrw = 2130837524;
        public static final int jjsekmfdb = 2130837525;
        public static final int jlrsahwu = 2130837526;
        public static final int mdkjm = 2130837527;
        public static final int qaqag = 2130837528;
        public static final int spinner_48_inner_holo = 2130837529;
        public static final int ugnqhf = 2130837530;
        public static final int vttgwi = 2130837531;
        public static final int wdoecauag = 2130837532;
    }

    public static final class id {
        public static final int begnfvgqh = 2131165227;
        public static final int bimrlpm = 2131165235;
        public static final int bmvlpwd = 2131165216;
        public static final int buhksa = 2131165236;
        public static final int bvembv = 2131165209;
        public static final int cnvjuueicvm = 2131165222;
        public static final int cpuofoj = 2131165191;
        public static final int dcdpoik = 2131165238;
        public static final int dkecgwimd = 2131165189;
        public static final int dkhrpqc = 2131165206;
        public static final int dvhhf = 2131165215;
        public static final int ecuhjan = 2131165243;
        public static final int eklgdtvt = 2131165219;
        public static final int fbvflbdwrp = 2131165198;
        public static final int fejemiaoe = 2131165240;
        public static final int fgajvl = 2131165196;
        public static final int gcqqif = 2131165234;
        public static final int gdsfhtqu = 2131165205;
        public static final int ggvses = 2131165245;
        public static final int gqwwpgcnw = 2131165244;
        public static final int grrjjt = 2131165192;
        public static final int hactsd = 2131165200;
        public static final int hetqhvk = 2131165241;
        public static final int hvpoblb = 2131165237;
        public static final int icbejcpo = 2131165199;
        public static final int iijrihvn = 2131165218;
        public static final int iilmo = 2131165217;
        public static final int iuhwj = 2131165197;
        public static final int iwvjahb = 2131165201;
        public static final int jiusfo = 2131165194;
        public static final int jpfwedjd = 2131165246;
        public static final int jtwahrpln = 2131165228;
        public static final int kffnfw = 2131165190;
        public static final int kqwlb = 2131165229;
        public static final int kvbcfll = 2131165213;
        public static final int lnnqgn = 2131165223;
        public static final int luooilwg = 2131165225;
        public static final int mnpceb = 2131165193;
        public static final int ngnlkiv = 2131165184;
        public static final int njhhuow = 2131165188;
        public static final int ohpkcjb = 2131165214;
        public static final int ohsjgb = 2131165212;
        public static final int oppkgvp = 2131165186;
        public static final int ppcco = 2131165232;
        public static final int prohten = 2131165210;
        public static final int psjrnrgh = 2131165204;
        public static final int qhiquhdt = 2131165224;
        public static final int qnennvg = 2131165207;
        public static final int qohmavcf = 2131165231;
        public static final int rqqrwvr = 2131165220;
        public static final int sjldmka = 2131165226;
        public static final int sqhwmqpig = 2131165242;
        public static final int srrketcd = 2131165208;
        public static final int tqlfmvk = 2131165187;
        public static final int tscajwi = 2131165185;
        public static final int twkrb = 2131165230;
        public static final int uqous = 2131165211;
        public static final int vkpjjse = 2131165202;
        public static final int volppb = 2131165203;
        public static final int vqfcarkp = 2131165221;
        public static final int vvdthnj = 2131165233;
        public static final int whfcdd = 2131165195;
        public static final int wrrucldfi = 2131165239;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int dbawsie = 2131230724;
        public static final int eeanpgo = 2131230723;
        public static final int nbmpgwwrt = 2131230722;
        public static final int patdlon = 2131230725;
        public static final int vbgrcgou = 2131230721;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
