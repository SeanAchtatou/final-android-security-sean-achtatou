package com.androiddg.pgroute;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int anekfesn = 2130837504;
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837505;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837506;
        public static final int apptheme_btn_default_focused_holo_light = 2130837507;
        public static final int apptheme_btn_default_normal_holo_light = 2130837508;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837509;
        public static final int apptheme_textfield_activated_holo_light = 2130837510;
        public static final int apptheme_textfield_default_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837512;
        public static final int apptheme_textfield_disabled_holo_light = 2130837513;
        public static final int apptheme_textfield_focused_holo_light = 2130837514;
        public static final int bhtel = 2130837515;
        public static final int boham = 2130837516;
        public static final int dunfr = 2130837517;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837518;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837519;
        public static final int fbi_btn_default_focused_holo_dark = 2130837520;
        public static final int fbi_btn_default_normal_holo_dark = 2130837521;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837522;
        public static final int fglsu = 2130837523;
        public static final int gqeggbn = 2130837524;
        public static final int ic_launcher = 2130837525;
        public static final int kthqdnvg = 2130837526;
        public static final int mphrgrdfoj = 2130837527;
        public static final int msuimjr = 2130837528;
        public static final int rqptnqmuh = 2130837529;
        public static final int spinner_48_inner_holo = 2130837530;
        public static final int wkbagu = 2130837531;
        public static final int wnrcuac = 2130837532;
    }

    public static final class id {
        public static final int barjeamrm = 2131165198;
        public static final int bgbrrauo = 2131165201;
        public static final int blwrtrev = 2131165239;
        public static final int brgeeooqr = 2131165212;
        public static final int cblvbw = 2131165231;
        public static final int cdwvso = 2131165242;
        public static final int cibfwhng = 2131165226;
        public static final int cmjggfuj = 2131165245;
        public static final int cngbujjhir = 2131165230;
        public static final int cvarlm = 2131165223;
        public static final int detmphua = 2131165227;
        public static final int dinkbp = 2131165197;
        public static final int dqkuvon = 2131165186;
        public static final int egkgkoo = 2131165200;
        public static final int equlpsg = 2131165235;
        public static final int fartswn = 2131165207;
        public static final int fbodgq = 2131165202;
        public static final int fdttukn = 2131165229;
        public static final int ffvhqteo = 2131165232;
        public static final int fnsmbp = 2131165221;
        public static final int gfcowtjop = 2131165217;
        public static final int gotipsl = 2131165233;
        public static final int gushbdp = 2131165236;
        public static final int gvipqr = 2131165184;
        public static final int hmmhvioo = 2131165199;
        public static final int hmvtjkm = 2131165208;
        public static final int hppcatfgvt = 2131165185;
        public static final int hsqfegj = 2131165244;
        public static final int iigur = 2131165224;
        public static final int ijliehut = 2131165225;
        public static final int imovb = 2131165211;
        public static final int itcutu = 2131165220;
        public static final int jdiwo = 2131165215;
        public static final int jiqui = 2131165228;
        public static final int klawasmeo = 2131165193;
        public static final int kmwmlb = 2131165196;
        public static final int ljnqmpck = 2131165191;
        public static final int lkjvajttpv = 2131165204;
        public static final int mkdlnq = 2131165194;
        public static final int nulcbeoos = 2131165216;
        public static final int ohkfomus = 2131165219;
        public static final int ownagqtjb = 2131165192;
        public static final int palbrdtc = 2131165209;
        public static final int pbbeuv = 2131165237;
        public static final int pbjvihpelt = 2131165206;
        public static final int pddakf = 2131165222;
        public static final int pdwqigg = 2131165188;
        public static final int pnweh = 2131165189;
        public static final int puhgbc = 2131165240;
        public static final int qomeusqfha = 2131165214;
        public static final int qsqlq = 2131165205;
        public static final int rkhtkpjauc = 2131165190;
        public static final int sbntkdfju = 2131165203;
        public static final int schvvpdpm = 2131165187;
        public static final int scjivuws = 2131165234;
        public static final int swmrifp = 2131165213;
        public static final int tlpgclchw = 2131165238;
        public static final int trcgboej = 2131165210;
        public static final int utbteqp = 2131165195;
        public static final int vrnntl = 2131165243;
        public static final int vtekoui = 2131165218;
        public static final int wekgtssf = 2131165246;
        public static final int wqltubtgci = 2131165241;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int adbmken = 2131230721;
        public static final int app_name = 2131230720;
        public static final int flcfqfjis = 2131230722;
        public static final int kjbfbh = 2131230723;
        public static final int oraadgehbl = 2131230725;
        public static final int qqpcego = 2131230724;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
