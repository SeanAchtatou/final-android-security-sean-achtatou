package com.upfsec.hakcalendar;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int agwcaicdrl = 2130837504;
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837505;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837506;
        public static final int apptheme_btn_default_focused_holo_light = 2130837507;
        public static final int apptheme_btn_default_normal_holo_light = 2130837508;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837509;
        public static final int apptheme_textfield_activated_holo_light = 2130837510;
        public static final int apptheme_textfield_default_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837512;
        public static final int apptheme_textfield_disabled_holo_light = 2130837513;
        public static final int apptheme_textfield_focused_holo_light = 2130837514;
        public static final int deebdkkh = 2130837515;
        public static final int eujtbpmc = 2130837516;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837517;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837518;
        public static final int fbi_btn_default_focused_holo_dark = 2130837519;
        public static final int fbi_btn_default_normal_holo_dark = 2130837520;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837521;
        public static final int fqspjweu = 2130837522;
        public static final int gueiltow = 2130837523;
        public static final int ic_launcher = 2130837524;
        public static final int ifkjreat = 2130837525;
        public static final int oceoqkr = 2130837526;
        public static final int rcdmeltb = 2130837527;
        public static final int slpjpcp = 2130837528;
        public static final int spinner_48_inner_holo = 2130837529;
        public static final int tnmagmqao = 2130837530;
        public static final int waecgbtj = 2130837531;
        public static final int wewuerj = 2130837532;
    }

    public static final class id {
        public static final int awarihfp = 2131165184;
        public static final int bkrkbkjq = 2131165209;
        public static final int bpnpuukh = 2131165212;
        public static final int ccelbbhmh = 2131165206;
        public static final int dercpsinijkg = 2131165238;
        public static final int dfplu = 2131165202;
        public static final int djromvnf = 2131165211;
        public static final int dnvbckq = 2131165222;
        public static final int drqbvge = 2131165191;
        public static final int dvowjnu = 2131165195;
        public static final int eicajj = 2131165242;
        public static final int emhhcb = 2131165217;
        public static final int eteliirq = 2131165233;
        public static final int fefhwlasgwo = 2131165235;
        public static final int fmhefkroj = 2131165208;
        public static final int fttsnji = 2131165237;
        public static final int fvjnvobr = 2131165221;
        public static final int ggvvvr = 2131165186;
        public static final int gkqlhou = 2131165243;
        public static final int govndej = 2131165194;
        public static final int gruga = 2131165232;
        public static final int guqkhphgwpl = 2131165231;
        public static final int habvqvu = 2131165213;
        public static final int hifklf = 2131165205;
        public static final int ilfkgvk = 2131165223;
        public static final int jnqhkwmnd = 2131165239;
        public static final int jqsmbu = 2131165215;
        public static final int kcekbmear = 2131165210;
        public static final int kknqln = 2131165220;
        public static final int klchdfoe = 2131165187;
        public static final int lremmmv = 2131165199;
        public static final int luqbiasqs = 2131165201;
        public static final int mlqqlp = 2131165214;
        public static final int ndqpho = 2131165230;
        public static final int nsibar = 2131165200;
        public static final int oowrmul = 2131165190;
        public static final int pbpaq = 2131165227;
        public static final int pkrrimhr = 2131165234;
        public static final int ptjevc = 2131165203;
        public static final int qbksr = 2131165188;
        public static final int qgotvo = 2131165197;
        public static final int qjhvfriqbu = 2131165216;
        public static final int qmumsjp = 2131165193;
        public static final int ramhne = 2131165236;
        public static final int rqnhcho = 2131165246;
        public static final int rufkhu = 2131165240;
        public static final int rviot = 2131165207;
        public static final int sahejsiu = 2131165241;
        public static final int sdohrj = 2131165224;
        public static final int sinnvge = 2131165204;
        public static final int tdgjmmil = 2131165229;
        public static final int tfjnfqwcmu = 2131165244;
        public static final int tikluamk = 2131165189;
        public static final int tlpdifnto = 2131165192;
        public static final int uagieq = 2131165185;
        public static final int unfuqvdia = 2131165218;
        public static final int uofobghi = 2131165226;
        public static final int uweoapqnn = 2131165196;
        public static final int vjqiliud = 2131165228;
        public static final int vnwmbjtp = 2131165198;
        public static final int vtptnvra = 2131165219;
        public static final int wibhou = 2131165225;
        public static final int wvqcmjesisl = 2131165245;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int dkhujpo = 2131230721;
        public static final int euroldgfa = 2131230723;
        public static final int qioifpqiwe = 2131230725;
        public static final int tdfmpd = 2131230722;
        public static final int wrpjvj = 2131230724;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
