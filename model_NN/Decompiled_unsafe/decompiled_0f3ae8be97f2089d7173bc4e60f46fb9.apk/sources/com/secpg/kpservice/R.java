package com.secpg.kpservice;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int etjfd = 2130837514;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837515;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837516;
        public static final int fbi_btn_default_focused_holo_dark = 2130837517;
        public static final int fbi_btn_default_normal_holo_dark = 2130837518;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837519;
        public static final int fhpjskrj = 2130837520;
        public static final int hpego = 2130837521;
        public static final int ic_launcher = 2130837522;
        public static final int mftwnjs = 2130837523;
        public static final int oucussphm = 2130837524;
        public static final int pdtbwtmg = 2130837525;
        public static final int rldvekov = 2130837526;
        public static final int spinner_48_inner_holo = 2130837527;
        public static final int tjgabbgm = 2130837528;
        public static final int ttcmwpbp = 2130837529;
        public static final int ukdwchfci = 2130837530;
        public static final int upctj = 2130837531;
        public static final int vwabp = 2130837532;
    }

    public static final class id {
        public static final int aaaocuh = 2131165223;
        public static final int acqgjjnu = 2131165236;
        public static final int aoghdu = 2131165205;
        public static final int aqgsnrm = 2131165232;
        public static final int bifshn = 2131165238;
        public static final int bwegrmafplpl = 2131165241;
        public static final int cdwapqr = 2131165194;
        public static final int cjggpoi = 2131165222;
        public static final int cknak = 2131165199;
        public static final int clsid = 2131165226;
        public static final int dbjunapn = 2131165213;
        public static final int dbvgjdiet = 2131165212;
        public static final int dejvag = 2131165225;
        public static final int dmnfnk = 2131165243;
        public static final int dsdfujv = 2131165224;
        public static final int ecqssf = 2131165239;
        public static final int edsuauj = 2131165228;
        public static final int egfwttkt = 2131165185;
        public static final int eugje = 2131165220;
        public static final int gfgdno = 2131165242;
        public static final int gfrkpgfb = 2131165196;
        public static final int gwfmss = 2131165214;
        public static final int haatwrd = 2131165209;
        public static final int hctapvr = 2131165192;
        public static final int hdmnph = 2131165221;
        public static final int hlsujo = 2131165184;
        public static final int hocbpfs = 2131165198;
        public static final int hpjve = 2131165187;
        public static final int hpmktldm = 2131165200;
        public static final int ikqhpgdi = 2131165244;
        public static final int ivhgak = 2131165189;
        public static final int jtjtlo = 2131165211;
        public static final int kffawv = 2131165206;
        public static final int kgjrjfh = 2131165218;
        public static final int lcspm = 2131165240;
        public static final int lowgvjo = 2131165190;
        public static final int lqudqcbn = 2131165231;
        public static final int mrtmrnmg = 2131165186;
        public static final int mspwiftfp = 2131165234;
        public static final int npppoqhc = 2131165233;
        public static final int nuevwrpok = 2131165219;
        public static final int onsnbo = 2131165195;
        public static final int pckkktn = 2131165188;
        public static final int psdvlmmg = 2131165235;
        public static final int qaupoffmir = 2131165197;
        public static final int qmacjwh = 2131165229;
        public static final int rdvhpwjr = 2131165227;
        public static final int rnrltp = 2131165208;
        public static final int rvdjstqbt = 2131165217;
        public static final int tetkcvkn = 2131165193;
        public static final int tlhbobt = 2131165246;
        public static final int tninu = 2131165237;
        public static final int tskwljvnr = 2131165207;
        public static final int tthetm = 2131165210;
        public static final int ukebtl = 2131165230;
        public static final int uluotuou = 2131165215;
        public static final int vovmim = 2131165245;
        public static final int vtvddauwl = 2131165201;
        public static final int wnvoelwt = 2131165216;
        public static final int wqlfrdgq = 2131165191;
        public static final int wsirqpw = 2131165204;
        public static final int wthtswi = 2131165202;
        public static final int wwrvovjo = 2131165203;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int ekrmrt = 2131230724;
        public static final int kdnium = 2131230725;
        public static final int mmaoodl = 2131230723;
        public static final int nepnrpla = 2131230722;
        public static final int oihhfq = 2131230721;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
