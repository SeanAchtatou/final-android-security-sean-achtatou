package com.kosec.servicev;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int bftqiqcf = 2130837514;
        public static final int btsgpr = 2130837515;
        public static final int cqvkhdoi = 2130837516;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837517;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837518;
        public static final int fbi_btn_default_focused_holo_dark = 2130837519;
        public static final int fbi_btn_default_normal_holo_dark = 2130837520;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837521;
        public static final int fessf = 2130837522;
        public static final int gnopa = 2130837523;
        public static final int gqtkfntaj = 2130837524;
        public static final int ic_launcher = 2130837525;
        public static final int ivtuiqmdm = 2130837526;
        public static final int qfvbjqgvq = 2130837527;
        public static final int qgrjme = 2130837528;
        public static final int rrivedd = 2130837529;
        public static final int spinner_48_inner_holo = 2130837530;
        public static final int thrqicw = 2130837531;
        public static final int vfpnjlmlj = 2130837532;
    }

    public static final class id {
        public static final int aeubvf = 2131165238;
        public static final int bagsqdmb = 2131165229;
        public static final int bscqpii = 2131165243;
        public static final int bsenjtjq = 2131165224;
        public static final int cmnnie = 2131165221;
        public static final int cvfitslm = 2131165188;
        public static final int djwbupg = 2131165233;
        public static final int dldgdkt = 2131165242;
        public static final int dtacoekc = 2131165244;
        public static final int duiaobus = 2131165192;
        public static final int duuhwhle = 2131165219;
        public static final int ehuwqchwl = 2131165195;
        public static final int emtqg = 2131165200;
        public static final int fiemjcbigpm = 2131165220;
        public static final int frmkul = 2131165204;
        public static final int giuhokg = 2131165218;
        public static final int gunhki = 2131165210;
        public static final int hgwnqc = 2131165207;
        public static final int hjcfiv = 2131165225;
        public static final int htvgnueso = 2131165203;
        public static final int hwtcmk = 2131165186;
        public static final int ibjjbgqn = 2131165187;
        public static final int ignpmfrce = 2131165245;
        public static final int imiwrpeqg = 2131165196;
        public static final int kevtdkrsbj = 2131165212;
        public static final int kohgqiacp = 2131165201;
        public static final int kwukigqcj = 2131165246;
        public static final int lbieug = 2131165209;
        public static final int lqosgdt = 2131165214;
        public static final int lutqrkmvtow = 2131165202;
        public static final int mvhkp = 2131165205;
        public static final int mvknmpotl = 2131165232;
        public static final int nditw = 2131165226;
        public static final int nliqj = 2131165230;
        public static final int nokqse = 2131165190;
        public static final int ohfvarqb = 2131165235;
        public static final int oihkwp = 2131165234;
        public static final int omepnuh = 2131165236;
        public static final int oovwuo = 2131165184;
        public static final int oqieo = 2131165191;
        public static final int pgispjct = 2131165215;
        public static final int pgrmjsr = 2131165213;
        public static final int ppmoisv = 2131165193;
        public static final int qcipi = 2131165217;
        public static final int qpcwb = 2131165222;
        public static final int qshrgpp = 2131165211;
        public static final int quegfeiwbv = 2131165206;
        public static final int rmeijucoq = 2131165185;
        public static final int sjrmuj = 2131165194;
        public static final int spbdtjgstvik = 2131165228;
        public static final int tbnahq = 2131165237;
        public static final int tevht = 2131165231;
        public static final int tkmtjok = 2131165197;
        public static final int tnqhilp = 2131165239;
        public static final int tuqpon = 2131165189;
        public static final int uaueabifp = 2131165241;
        public static final int ucmqi = 2131165240;
        public static final int ugijqkgw = 2131165227;
        public static final int unhfti = 2131165198;
        public static final int urtfl = 2131165223;
        public static final int vjwktoc = 2131165199;
        public static final int vsbtfsj = 2131165208;
        public static final int wehrjcmpi = 2131165216;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int cobvivv = 2131230724;
        public static final int erwwk = 2131230723;
        public static final int ewbjielmu = 2131230721;
        public static final int hebqvqvqql = 2131230722;
        public static final int vccggh = 2131230725;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
