package com.wgomanager.ncalendar;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837514;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837515;
        public static final int fbi_btn_default_focused_holo_dark = 2130837516;
        public static final int fbi_btn_default_normal_holo_dark = 2130837517;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837518;
        public static final int ghhjir = 2130837519;
        public static final int hifiodowqm = 2130837520;
        public static final int hlofiuk = 2130837521;
        public static final int ic_launcher = 2130837522;
        public static final int iqwuls = 2130837523;
        public static final int jeawllef = 2130837524;
        public static final int jpudtpp = 2130837525;
        public static final int kmddfr = 2130837526;
        public static final int qeumoui = 2130837527;
        public static final int rjlhgvp = 2130837528;
        public static final int spinner_48_inner_holo = 2130837529;
        public static final int utrpt = 2130837530;
        public static final int vfcaqg = 2130837531;
        public static final int wkhklqudq = 2130837532;
    }

    public static final class id {
        public static final int aahsktth = 2131165241;
        public static final int abmcfdotgpj = 2131165189;
        public static final int akoiwnrh = 2131165223;
        public static final int atppv = 2131165185;
        public static final int bqkehvtv = 2131165232;
        public static final int cfigjdnj = 2131165193;
        public static final int depkclt = 2131165213;
        public static final int duojwh = 2131165199;
        public static final int ebbsvwtrn = 2131165239;
        public static final int epesapd = 2131165203;
        public static final int fbqvugwfm = 2131165245;
        public static final int frjlqta = 2131165212;
        public static final int ftwdptcf = 2131165190;
        public static final int gbfkw = 2131165186;
        public static final int ghbhpm = 2131165231;
        public static final int gurmbdbj = 2131165244;
        public static final int hawlqqbvct = 2131165194;
        public static final int hborr = 2131165196;
        public static final int hrjjouqdb = 2131165207;
        public static final int htdjtjt = 2131165242;
        public static final int hwjvmw = 2131165202;
        public static final int ihmougho = 2131165235;
        public static final int jfdbvg = 2131165243;
        public static final int jkuluo = 2131165246;
        public static final int jmgtjvr = 2131165208;
        public static final int jspgffbtkfr = 2131165216;
        public static final int kdfgdicdwa = 2131165225;
        public static final int kjtoaikag = 2131165192;
        public static final int kroonsn = 2131165226;
        public static final int lgqujv = 2131165234;
        public static final int locvad = 2131165220;
        public static final int mlkhdjd = 2131165191;
        public static final int mwfag = 2131165198;
        public static final int nksqaup = 2131165228;
        public static final int nmgqbmhmjp = 2131165219;
        public static final int nnrwkts = 2131165238;
        public static final int nsgwjedm = 2131165215;
        public static final int ntiiodte = 2131165224;
        public static final int onogwwn = 2131165236;
        public static final int oqwfprgij = 2131165209;
        public static final int pabaikfl = 2131165204;
        public static final int pcbofgm = 2131165201;
        public static final int phbipl = 2131165200;
        public static final int pjgbf = 2131165205;
        public static final int prosuu = 2131165210;
        public static final int ptgfrkp = 2131165227;
        public static final int qikaock = 2131165230;
        public static final int qprfgjlow = 2131165221;
        public static final int rhtujimi = 2131165184;
        public static final int rtutha = 2131165240;
        public static final int rwcja = 2131165211;
        public static final int stpeolii = 2131165187;
        public static final int tvwqew = 2131165218;
        public static final int ubjmjid = 2131165214;
        public static final int usvhqwvn = 2131165206;
        public static final int uttqvean = 2131165188;
        public static final int vsmfetsrvp = 2131165197;
        public static final int vtbsmk = 2131165217;
        public static final int wbpipiho = 2131165229;
        public static final int wdmjf = 2131165233;
        public static final int whpbbwpck = 2131165237;
        public static final int wujnw = 2131165222;
        public static final int wwqoporjti = 2131165195;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int beusoif = 2131230725;
        public static final int ejqdjgf = 2131230724;
        public static final int epworvd = 2131230722;
        public static final int rgknwnvo = 2131230721;
        public static final int trrehk = 2131230723;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
