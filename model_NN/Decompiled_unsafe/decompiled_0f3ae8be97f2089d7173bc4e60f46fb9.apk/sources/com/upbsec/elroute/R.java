package com.upbsec.elroute;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int atgtq = 2130837514;
        public static final int bieaanv = 2130837515;
        public static final int crucp = 2130837516;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837517;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837518;
        public static final int fbi_btn_default_focused_holo_dark = 2130837519;
        public static final int fbi_btn_default_normal_holo_dark = 2130837520;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837521;
        public static final int gmelbgmi = 2130837522;
        public static final int ic_launcher = 2130837523;
        public static final int ienejb = 2130837524;
        public static final int jushgp = 2130837525;
        public static final int maewon = 2130837526;
        public static final int mrwjidenn = 2130837527;
        public static final int pcwefbau = 2130837528;
        public static final int sfjlnlv = 2130837529;
        public static final int spinner_48_inner_holo = 2130837530;
        public static final int upqukus = 2130837531;
        public static final int vnosn = 2130837532;
    }

    public static final class id {
        public static final int awklpcl = 2131165234;
        public static final int bggbisb = 2131165213;
        public static final int bgofp = 2131165235;
        public static final int bmpjoavl = 2131165222;
        public static final int boqcf = 2131165186;
        public static final int bspsao = 2131165228;
        public static final int busqacerd = 2131165240;
        public static final int bwdgudq = 2131165198;
        public static final int crdlakjr = 2131165220;
        public static final int dqfisaema = 2131165216;
        public static final int dtiggqq = 2131165226;
        public static final int esjiosr = 2131165211;
        public static final int euwdobi = 2131165217;
        public static final int fomsvdeid = 2131165224;
        public static final int gcvto = 2131165212;
        public static final int ggkeaoj = 2131165191;
        public static final int gjfvt = 2131165193;
        public static final int gpgqpukvu = 2131165188;
        public static final int hdgjt = 2131165205;
        public static final int hfefrqk = 2131165207;
        public static final int hhdqalvem = 2131165192;
        public static final int hjguqpt = 2131165245;
        public static final int hqobie = 2131165203;
        public static final int iqbpcisf = 2131165206;
        public static final int japveguc = 2131165187;
        public static final int jjfqml = 2131165229;
        public static final int jvtrlcglfj = 2131165204;
        public static final int kblcn = 2131165185;
        public static final int kdlilhhaj = 2131165246;
        public static final int khflccp = 2131165230;
        public static final int lhcsvvr = 2131165218;
        public static final int mbfqgl = 2131165227;
        public static final int mvbvndrpe = 2131165210;
        public static final int ndllin = 2131165241;
        public static final int oaeqmo = 2131165232;
        public static final int obicfqa = 2131165194;
        public static final int obukuwo = 2131165238;
        public static final int odmcwips = 2131165225;
        public static final int oglgokj = 2131165209;
        public static final int olvbcj = 2131165197;
        public static final int pkdibgr = 2131165200;
        public static final int pvohnsfc = 2131165184;
        public static final int qbkksvhlk = 2131165242;
        public static final int qgimimaqpf = 2131165214;
        public static final int qhppak = 2131165196;
        public static final int qvjwun = 2131165243;
        public static final int skiiu = 2131165202;
        public static final int sllrhig = 2131165215;
        public static final int tbjigwj = 2131165189;
        public static final int tbjsavf = 2131165233;
        public static final int tuvuckg = 2131165231;
        public static final int ucgrrl = 2131165201;
        public static final int ujcmuuk = 2131165190;
        public static final int ujfwren = 2131165221;
        public static final int ulnfomcab = 2131165195;
        public static final int uqvtgcd = 2131165239;
        public static final int vkmkf = 2131165219;
        public static final int vlwvwluijpl = 2131165236;
        public static final int vqeeomfnps = 2131165208;
        public static final int wdacepid = 2131165244;
        public static final int wkocwc = 2131165237;
        public static final int wljrfl = 2131165223;
        public static final int wovbm = 2131165199;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int iuoaaigscm = 2131230721;
        public static final int iwujdfrw = 2131230724;
        public static final int quelka = 2131230722;
        public static final int rvnokhvcma = 2131230723;
        public static final int ssoopcbl = 2131230725;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
