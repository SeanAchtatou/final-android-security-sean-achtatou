package com.commonnw.widgetqw;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int asckavjer = 2130837514;
        public static final int bhfiwfid = 2130837515;
        public static final int cpwutuegdj = 2130837516;
        public static final int dfpch = 2130837517;
        public static final int dkasmnesc = 2130837518;
        public static final int drlhdibb = 2130837519;
        public static final int duqutja = 2130837520;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837521;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837522;
        public static final int fbi_btn_default_focused_holo_dark = 2130837523;
        public static final int fbi_btn_default_normal_holo_dark = 2130837524;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837525;
        public static final int ic_launcher = 2130837526;
        public static final int jaspdsirqha = 2130837527;
        public static final int jbeikwc = 2130837528;
        public static final int mdmeqiuhtp = 2130837529;
        public static final int mffvff = 2130837530;
        public static final int spinner_48_inner_holo = 2130837531;
        public static final int wrdbg = 2130837532;
    }

    public static final class id {
        public static final int aajnrju = 2131165186;
        public static final int afdqggev = 2131165206;
        public static final int agghwide = 2131165222;
        public static final int ahctdjun = 2131165187;
        public static final int bcruhehc = 2131165199;
        public static final int bndrnjttkq = 2131165242;
        public static final int bvwhfapgb = 2131165244;
        public static final int bwgbatgvlu = 2131165239;
        public static final int cembvjt = 2131165212;
        public static final int cfvofll = 2131165211;
        public static final int chhpcto = 2131165189;
        public static final int dfnhqiphfu = 2131165208;
        public static final int dvsqufn = 2131165243;
        public static final int eakpvc = 2131165225;
        public static final int eraue = 2131165207;
        public static final int esvbmdsg = 2131165245;
        public static final int evwwekbow = 2131165210;
        public static final int fjnkf = 2131165190;
        public static final int fnqsbk = 2131165230;
        public static final int gvotmuj = 2131165221;
        public static final int gwnomwkk = 2131165232;
        public static final int hfwagt = 2131165205;
        public static final int hqqrrj = 2131165235;
        public static final int hrjlcrs = 2131165193;
        public static final int hsacusae = 2131165203;
        public static final int ibsknu = 2131165236;
        public static final int icfofa = 2131165216;
        public static final int igqmdljj = 2131165240;
        public static final int ilhdpdg = 2131165217;
        public static final int itiuhbef = 2131165224;
        public static final int jhtkl = 2131165191;
        public static final int kkvmmbcgj = 2131165202;
        public static final int kmrokiv = 2131165223;
        public static final int kvdww = 2131165229;
        public static final int ltpmg = 2131165184;
        public static final int mabmrg = 2131165233;
        public static final int mcstjwolwt = 2131165213;
        public static final int mgffthh = 2131165200;
        public static final int mvqwn = 2131165197;
        public static final int ndckoepls = 2131165185;
        public static final int njnjf = 2131165194;
        public static final int ocpfuhb = 2131165220;
        public static final int olblwq = 2131165192;
        public static final int pdbifuskv = 2131165209;
        public static final int peoqnlf = 2131165237;
        public static final int pgrhuwdt = 2131165219;
        public static final int prejeklv = 2131165188;
        public static final int qjfftmtkg = 2131165198;
        public static final int qmhiwgic = 2131165214;
        public static final int qrnhrf = 2131165215;
        public static final int rklde = 2131165196;
        public static final int rvdhfrg = 2131165201;
        public static final int rvfrfwcb = 2131165238;
        public static final int sjgrbnjbq = 2131165228;
        public static final int skres = 2131165204;
        public static final int smjtu = 2131165195;
        public static final int sohiiu = 2131165226;
        public static final int spiukjgpjb = 2131165227;
        public static final int twoeku = 2131165234;
        public static final int vjhjm = 2131165218;
        public static final int vufwqjnf = 2131165231;
        public static final int wdfjskt = 2131165246;
        public static final int wqkgrvbrn = 2131165241;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int abqecww = 2131230721;
        public static final int app_name = 2131230720;
        public static final int qunkjtt = 2131230722;
        public static final int ssuruvijmv = 2131230723;
        public static final int vghldtj = 2131230724;
        public static final int withac = 2131230725;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
