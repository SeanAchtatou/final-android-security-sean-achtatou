package com.androidlg.mngre;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int aioutqwp = 2130837504;
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837505;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837506;
        public static final int apptheme_btn_default_focused_holo_light = 2130837507;
        public static final int apptheme_btn_default_normal_holo_light = 2130837508;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837509;
        public static final int apptheme_textfield_activated_holo_light = 2130837510;
        public static final int apptheme_textfield_default_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837512;
        public static final int apptheme_textfield_disabled_holo_light = 2130837513;
        public static final int apptheme_textfield_focused_holo_light = 2130837514;
        public static final int asljwk = 2130837515;
        public static final int dateelpm = 2130837516;
        public static final int dqumkmkmm = 2130837517;
        public static final int dwkekdl = 2130837518;
        public static final int eaaiq = 2130837519;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837520;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837521;
        public static final int fbi_btn_default_focused_holo_dark = 2130837522;
        public static final int fbi_btn_default_normal_holo_dark = 2130837523;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837524;
        public static final int ic_launcher = 2130837525;
        public static final int kqgslgpf = 2130837526;
        public static final int lnmnpf = 2130837527;
        public static final int nkekrn = 2130837528;
        public static final int smvfijbm = 2130837529;
        public static final int spinner_48_inner_holo = 2130837530;
        public static final int tupwck = 2130837531;
        public static final int vvsctecrp = 2130837532;
    }

    public static final class id {
        public static final int aswcvli = 2131165200;
        public static final int banrtgd = 2131165192;
        public static final int bijhoijceapj = 2131165231;
        public static final int bmaijklwwrlu = 2131165188;
        public static final int cdljltk = 2131165206;
        public static final int cnuqasu = 2131165187;
        public static final int cswaeld = 2131165237;
        public static final int cvgfju = 2131165199;
        public static final int cwuiqri = 2131165205;
        public static final int dhqnwvab = 2131165208;
        public static final int dlthah = 2131165229;
        public static final int eaifvu = 2131165230;
        public static final int erwuwph = 2131165235;
        public static final int fcmvrwka = 2131165203;
        public static final int fsvmckslj = 2131165185;
        public static final int gapdlu = 2131165193;
        public static final int giwjfqhwr = 2131165207;
        public static final int hewgvlc = 2131165221;
        public static final int hikifvc = 2131165198;
        public static final int hkktp = 2131165197;
        public static final int hnvneg = 2131165225;
        public static final int htsojf = 2131165191;
        public static final int hvewarpw = 2131165220;
        public static final int ifoaeuun = 2131165241;
        public static final int ijhqtgl = 2131165223;
        public static final int jaodmale = 2131165213;
        public static final int jdbaddoqlt = 2131165246;
        public static final int kmpkgqdql = 2131165195;
        public static final int lcwhudkvvk = 2131165243;
        public static final int lekho = 2131165226;
        public static final int leqjwba = 2131165227;
        public static final int lfijtaum = 2131165186;
        public static final int lqhrvp = 2131165190;
        public static final int meigd = 2131165209;
        public static final int mffllw = 2131165240;
        public static final int mnutw = 2131165245;
        public static final int mwbuusg = 2131165239;
        public static final int nkasjtfw = 2131165196;
        public static final int oodjnou = 2131165194;
        public static final int paolorp = 2131165242;
        public static final int pfmglii = 2131165234;
        public static final int pimqlqf = 2131165201;
        public static final int qhqiumb = 2131165224;
        public static final int qhvpl = 2131165214;
        public static final int qmnkvh = 2131165222;
        public static final int rpdsejrlp = 2131165212;
        public static final int sphckjpl = 2131165189;
        public static final int suironaa = 2131165210;
        public static final int surntdbai = 2131165216;
        public static final int tbrob = 2131165233;
        public static final int toies = 2131165184;
        public static final int tvqqjcqj = 2131165218;
        public static final int ufimodn = 2131165219;
        public static final int uqjmhbubh = 2131165217;
        public static final int vbnaaoui = 2131165204;
        public static final int vcopregdlwr = 2131165238;
        public static final int vipvgv = 2131165244;
        public static final int vsnhoamu = 2131165232;
        public static final int wcpeu = 2131165228;
        public static final int weehmsjmd = 2131165215;
        public static final int wgbfb = 2131165211;
        public static final int wnumrhf = 2131165202;
        public static final int wqerwa = 2131165236;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int gtejvimeh = 2131230722;
        public static final int lvhei = 2131230725;
        public static final int mrabbijmd = 2131230721;
        public static final int pakwa = 2131230724;
        public static final int tqatn = 2131230723;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
