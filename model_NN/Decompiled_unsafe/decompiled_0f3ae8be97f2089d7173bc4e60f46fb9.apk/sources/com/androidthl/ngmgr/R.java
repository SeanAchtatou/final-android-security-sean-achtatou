package com.androidthl.ngmgr;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int bcawpdmw = 2130837514;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837515;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837516;
        public static final int fbi_btn_default_focused_holo_dark = 2130837517;
        public static final int fbi_btn_default_normal_holo_dark = 2130837518;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837519;
        public static final int grsjgv = 2130837520;
        public static final int ic_launcher = 2130837521;
        public static final int jijjnnrtq = 2130837522;
        public static final int lngmgbdq = 2130837523;
        public static final int nnncnglo = 2130837524;
        public static final int ntnswut = 2130837525;
        public static final int obhcihqe = 2130837526;
        public static final int phpjawuaj = 2130837527;
        public static final int pvatbed = 2130837528;
        public static final int scblqmtr = 2130837529;
        public static final int spinner_48_inner_holo = 2130837530;
        public static final int tqrmcagr = 2130837531;
        public static final int vwicfaccu = 2130837532;
    }

    public static final class id {
        public static final int aakdmnn = 2131165208;
        public static final int aqombdteb = 2131165210;
        public static final int bkiqhrk = 2131165215;
        public static final int bulbm = 2131165217;
        public static final int cfflnraag = 2131165240;
        public static final int cougripwd = 2131165237;
        public static final int cslhmk = 2131165220;
        public static final int cukaf = 2131165206;
        public static final int dqaetwl = 2131165243;
        public static final int ebkrusjv = 2131165207;
        public static final int edwmio = 2131165201;
        public static final int ehebvd = 2131165244;
        public static final int enpte = 2131165226;
        public static final int esdbdvg = 2131165211;
        public static final int ferqso = 2131165230;
        public static final int fgblipo = 2131165204;
        public static final int fpfumgs = 2131165188;
        public static final int fprtokw = 2131165241;
        public static final int fqdrdkvaf = 2131165199;
        public static final int gelshf = 2131165187;
        public static final int gwhjmg = 2131165190;
        public static final int hiqwsavi = 2131165219;
        public static final int iaktga = 2131165228;
        public static final int igblnerhrea = 2131165194;
        public static final int isphh = 2131165221;
        public static final int iwuop = 2131165196;
        public static final int jgulkquttr = 2131165193;
        public static final int joduwe = 2131165232;
        public static final int kihnhrp = 2131165197;
        public static final int kmcivrft = 2131165246;
        public static final int kvgrcij = 2131165198;
        public static final int lahpsrj = 2131165202;
        public static final int luuaqrednn = 2131165233;
        public static final int narcskkh = 2131165235;
        public static final int nwioajh = 2131165205;
        public static final int orkvcagkv = 2131165242;
        public static final int pawkqefjt = 2131165214;
        public static final int pertkcie = 2131165200;
        public static final int pniqm = 2131165191;
        public static final int poljilbs = 2131165203;
        public static final int qmjaeoco = 2131165231;
        public static final int rbfhh = 2131165239;
        public static final int ruslqrvd = 2131165227;
        public static final int rwilihulc = 2131165192;
        public static final int sineberjo = 2131165209;
        public static final int smobadf = 2131165189;
        public static final int spvdlu = 2131165213;
        public static final int srnsng = 2131165184;
        public static final int stccgfdle = 2131165222;
        public static final int tfinqnrto = 2131165224;
        public static final int thvpqt = 2131165238;
        public static final int tifjli = 2131165225;
        public static final int tknrdnuen = 2131165195;
        public static final int tqqujs = 2131165186;
        public static final int tqtmlfch = 2131165223;
        public static final int uakkhbrok = 2131165212;
        public static final int udufkasf = 2131165245;
        public static final int ueohom = 2131165229;
        public static final int umjpfejw = 2131165185;
        public static final int vavtsjeejh = 2131165234;
        public static final int vhhsrfbp = 2131165236;
        public static final int vqpqfhc = 2131165216;
        public static final int wtgrblou = 2131165218;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int duadjqbr = 2131230721;
        public static final int jjdicildn = 2131230723;
        public static final int pjdqttnkd = 2131230724;
        public static final int ssdalv = 2131230722;
        public static final int wogflpj = 2131230725;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
