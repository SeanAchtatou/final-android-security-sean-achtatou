package com.androidb.troute;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int dquvho = 2130837514;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837515;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837516;
        public static final int fbi_btn_default_focused_holo_dark = 2130837517;
        public static final int fbi_btn_default_normal_holo_dark = 2130837518;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837519;
        public static final int fmpfc = 2130837520;
        public static final int fsjhogo = 2130837521;
        public static final int hnqqhk = 2130837522;
        public static final int ic_launcher = 2130837523;
        public static final int klmimvfl = 2130837524;
        public static final int ldgbppbni = 2130837525;
        public static final int ldqwoqt = 2130837526;
        public static final int ligprdls = 2130837527;
        public static final int qwplewowb = 2130837528;
        public static final int spinner_48_inner_holo = 2130837529;
        public static final int tsahabtt = 2130837530;
        public static final int ungpqv = 2130837531;
        public static final int uqganihsrc = 2130837532;
    }

    public static final class id {
        public static final int apgor = 2131165226;
        public static final int bdocpa = 2131165200;
        public static final int bhwdwfwhmcu = 2131165191;
        public static final int bupivp = 2131165209;
        public static final int cbjtk = 2131165206;
        public static final int cjdtsaf = 2131165194;
        public static final int ctntliwp = 2131165233;
        public static final int ddiqapf = 2131165216;
        public static final int dlaoracv = 2131165207;
        public static final int dmetc = 2131165210;
        public static final int dsckfd = 2131165223;
        public static final int dstdpb = 2131165196;
        public static final int edshrtvq = 2131165187;
        public static final int egtdvg = 2131165232;
        public static final int eletdv = 2131165231;
        public static final int enbpc = 2131165184;
        public static final int eoceew = 2131165205;
        public static final int eogbrms = 2131165239;
        public static final int frcjlslm = 2131165195;
        public static final int frpurcf = 2131165245;
        public static final int gadrpr = 2131165234;
        public static final int gamqogtd = 2131165202;
        public static final int gccdik = 2131165198;
        public static final int ggecru = 2131165229;
        public static final int gnddjojukie = 2131165228;
        public static final int gqdet = 2131165204;
        public static final int igkain = 2131165219;
        public static final int ionpiqdh = 2131165217;
        public static final int jbileimrqe = 2131165188;
        public static final int lfccg = 2131165189;
        public static final int loksfe = 2131165199;
        public static final int mtqlkbb = 2131165230;
        public static final int mugts = 2131165192;
        public static final int nidbvlog = 2131165221;
        public static final int olcitc = 2131165201;
        public static final int orsinjnh = 2131165241;
        public static final int owppda = 2131165243;
        public static final int pbdmhbkh = 2131165197;
        public static final int ppfpfp = 2131165244;
        public static final int pwbgacbs = 2131165218;
        public static final int pwfal = 2131165186;
        public static final int qbtow = 2131165212;
        public static final int qcdkmglh = 2131165237;
        public static final int qidmmps = 2131165242;
        public static final int qniuw = 2131165246;
        public static final int qsapbo = 2131165220;
        public static final int rjtdhjjo = 2131165185;
        public static final int rkkghgm = 2131165215;
        public static final int rpiaaathqv = 2131165208;
        public static final int sljahtkcd = 2131165238;
        public static final int smhbnwcmp = 2131165224;
        public static final int smqvronss = 2131165236;
        public static final int sufhrguu = 2131165211;
        public static final int swifbk = 2131165235;
        public static final int tulklpav = 2131165213;
        public static final int ulpaloaeao = 2131165203;
        public static final int vbecfw = 2131165193;
        public static final int vktpekdctgr = 2131165222;
        public static final int vnrfkop = 2131165214;
        public static final int vpterni = 2131165225;
        public static final int wevwst = 2131165227;
        public static final int wlpvb = 2131165190;
        public static final int wphgsa = 2131165240;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int asbumrqs = 2131230721;
        public static final int cgcsjr = 2131230723;
        public static final int fninnadgw = 2131230725;
        public static final int isjfwqhqdi = 2131230722;
        public static final int qalskn = 2131230724;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
