package com.bsec.supportogd;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int ddklofgj = 2130837514;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837515;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837516;
        public static final int fbi_btn_default_focused_holo_dark = 2130837517;
        public static final int fbi_btn_default_normal_holo_dark = 2130837518;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837519;
        public static final int fgkkh = 2130837520;
        public static final int frctfrhqcn = 2130837521;
        public static final int gsvscvwwd = 2130837522;
        public static final int ic_launcher = 2130837523;
        public static final int mfdtvpo = 2130837524;
        public static final int nwsneb = 2130837525;
        public static final int onafv = 2130837526;
        public static final int qhltrow = 2130837527;
        public static final int sfucriv = 2130837528;
        public static final int spinner_48_inner_holo = 2130837529;
        public static final int trwlvf = 2130837530;
        public static final int tusrftd = 2130837531;
        public static final int vdicw = 2130837532;
    }

    public static final class id {
        public static final int ahbowwow = 2131165234;
        public static final int bcoffkn = 2131165198;
        public static final int bmjcdtk = 2131165236;
        public static final int bnqvjm = 2131165193;
        public static final int bssfufj = 2131165241;
        public static final int clbpqtnik = 2131165230;
        public static final int cpfctun = 2131165186;
        public static final int cskpmfmnii = 2131165197;
        public static final int dcwaqsecuf = 2131165224;
        public static final int drsonmeaph = 2131165213;
        public static final int dsfwuok = 2131165196;
        public static final int efhcfw = 2131165231;
        public static final int efognv = 2131165184;
        public static final int elorc = 2131165242;
        public static final int feghtnhs = 2131165191;
        public static final int feogvanw = 2131165223;
        public static final int feuloagss = 2131165229;
        public static final int fgwtsi = 2131165207;
        public static final int fktels = 2131165225;
        public static final int gadmdvb = 2131165192;
        public static final int hgebpd = 2131165216;
        public static final int hrwshc = 2131165226;
        public static final int huirmav = 2131165221;
        public static final int ifsfsp = 2131165245;
        public static final int iwhktfbg = 2131165227;
        public static final int kaqptak = 2131165235;
        public static final int kcduv = 2131165202;
        public static final int kcghsam = 2131165209;
        public static final int kqvqhold = 2131165185;
        public static final int kufci = 2131165210;
        public static final int loatbtcfsu = 2131165217;
        public static final int mfpohur = 2131165203;
        public static final int mhvakrvn = 2131165194;
        public static final int nappsvv = 2131165238;
        public static final int nnwkjul = 2131165233;
        public static final int nrowmfg = 2131165200;
        public static final int nvadklj = 2131165243;
        public static final int oagngs = 2131165239;
        public static final int oduuqm = 2131165240;
        public static final int okfgue = 2131165190;
        public static final int orqaqs = 2131165232;
        public static final int pwlawofd = 2131165204;
        public static final int qfffjv = 2131165205;
        public static final int qilsocbr = 2131165212;
        public static final int qnaqrmj = 2131165237;
        public static final int qwbhlk = 2131165206;
        public static final int rkosnshwg = 2131165215;
        public static final int rrihcvsof = 2131165218;
        public static final int scuwra = 2131165201;
        public static final int scwtll = 2131165222;
        public static final int snwwahwuh = 2131165211;
        public static final int spicwmerfc = 2131165189;
        public static final int stkrgrikr = 2131165219;
        public static final int tjipvis = 2131165187;
        public static final int udfjcmrf = 2131165195;
        public static final int ulmkui = 2131165208;
        public static final int uwjdtnsjos = 2131165220;
        public static final int vemunlgbei = 2131165199;
        public static final int vkjaitw = 2131165244;
        public static final int wbrfjtpvc = 2131165214;
        public static final int wdktlkofol = 2131165228;
        public static final int wgbtlacdpthr = 2131165188;
        public static final int wonobrtpc = 2131165246;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int fhqcpmbclva = 2131230724;
        public static final int ghoieui = 2131230723;
        public static final int mdfaadg = 2131230721;
        public static final int pvejo = 2131230725;
        public static final int wdqocfjd = 2131230722;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
