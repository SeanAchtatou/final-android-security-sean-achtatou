package com.pcommon.routeoi;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int abwgq = 2130837504;
        public static final int annmm = 2130837505;
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837507;
        public static final int apptheme_btn_default_focused_holo_light = 2130837508;
        public static final int apptheme_btn_default_normal_holo_light = 2130837509;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837510;
        public static final int apptheme_textfield_activated_holo_light = 2130837511;
        public static final int apptheme_textfield_default_holo_light = 2130837512;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837513;
        public static final int apptheme_textfield_disabled_holo_light = 2130837514;
        public static final int apptheme_textfield_focused_holo_light = 2130837515;
        public static final int bhcjlngjmc = 2130837516;
        public static final int bomadh = 2130837517;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837518;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837519;
        public static final int fbi_btn_default_focused_holo_dark = 2130837520;
        public static final int fbi_btn_default_normal_holo_dark = 2130837521;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837522;
        public static final int fhankw = 2130837523;
        public static final int fjrpmuu = 2130837524;
        public static final int hvgrut = 2130837525;
        public static final int ic_launcher = 2130837526;
        public static final int kmneordoi = 2130837527;
        public static final int lisiv = 2130837528;
        public static final int sdlpppnm = 2130837529;
        public static final int spinner_48_inner_holo = 2130837530;
        public static final int tmhdflkweh = 2130837531;
        public static final int umlmwpr = 2130837532;
    }

    public static final class id {
        public static final int aamaoaa = 2131165230;
        public static final int agwcaicdrl = 2131165192;
        public static final int ahbmecm = 2131165225;
        public static final int altmh = 2131165218;
        public static final int antakslqg = 2131165197;
        public static final int bdkkh = 2131165189;
        public static final int bfvdivo = 2131165200;
        public static final int cvisbd = 2131165220;
        public static final int eatou = 2131165186;
        public static final int eiltowdbs = 2131165187;
        public static final int eprcvab = 2131165199;
        public static final int eujtbpmc = 2131165193;
        public static final int ffwrevll = 2131165202;
        public static final int fqpfmpbr = 2131165242;
        public static final int fqspjweu = 2131165191;
        public static final int fsikreu = 2131165221;
        public static final int geksktbm = 2131165210;
        public static final int gfatqmdvd = 2131165229;
        public static final int giqaurcc = 2131165233;
        public static final int gpsslakf = 2131165222;
        public static final int gsbfwfsci = 2131165219;
        public static final int hpifpuq = 2131165237;
        public static final int ipbauvo = 2131165235;
        public static final int iwendkikm = 2131165184;
        public static final int jgwnja = 2131165201;
        public static final int jpcpwnr = 2131165188;
        public static final int kbrlomih = 2131165243;
        public static final int kvlrqdnsmi = 2131165207;
        public static final int lecndqlwi = 2131165206;
        public static final int lewamddi = 2131165209;
        public static final int lfundaw = 2131165214;
        public static final int lkuten = 2131165234;
        public static final int lslpekw = 2131165216;
        public static final int lvkdtmqpk = 2131165244;
        public static final int nbwnfqbu = 2131165227;
        public static final int ndwuaoaev = 2131165203;
        public static final int nfttqsm = 2131165226;
        public static final int nqblpmuk = 2131165211;
        public static final int nqpkvtv = 2131165205;
        public static final int oceoqkr = 2131165190;
        public static final int oofwfuum = 2131165245;
        public static final int piqvslsd = 2131165208;
        public static final int plbdeg = 2131165228;
        public static final int qpqacwfaj = 2131165241;
        public static final int rcdmeltb = 2131165195;
        public static final int revirlaj = 2131165236;
        public static final int rtawehtw = 2131165204;
        public static final int saugkbnr = 2131165223;
        public static final int suhbrlq = 2131165231;
        public static final int tjmfbbp = 2131165185;
        public static final int tnmagmqao = 2131165194;
        public static final int ttvfuac = 2131165238;
        public static final int ubmussuti = 2131165224;
        public static final int ucvumdbp = 2131165215;
        public static final int uiabsth = 2131165213;
        public static final int ulrewglr = 2131165240;
        public static final int vmbpkfa = 2131165212;
        public static final int vqngfcg = 2131165246;
        public static final int vvlvbr = 2131165198;
        public static final int wewuerj = 2131165196;
        public static final int wfjivcevspc = 2131165217;
        public static final int wocndoas = 2131165232;
        public static final int wuuubik = 2131165239;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int cwkharrbwi = 2131230721;
        public static final int lvnnqt = 2131230723;
        public static final int mbssokhtm = 2131230725;
        public static final int ngasp = 2131230722;
        public static final int ougmtmb = 2131230724;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
