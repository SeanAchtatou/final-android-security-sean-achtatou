package com.miscj.ucamngr;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034114;
        public static final int darker_gray = 2131034113;
        public static final int lighter_gray = 2131034112;
        public static final int progress_end = 2131034117;
        public static final int progress_start = 2131034116;
        public static final int white = 2131034115;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131099648;
        public static final int activity_vertical_margin = 2131099649;
    }

    public static final class drawable {
        public static final int apptheme_btn_default_disabled_focused_holo_light = 2130837504;
        public static final int apptheme_btn_default_disabled_holo_light = 2130837505;
        public static final int apptheme_btn_default_focused_holo_light = 2130837506;
        public static final int apptheme_btn_default_normal_holo_light = 2130837507;
        public static final int apptheme_btn_default_pressed_holo_light = 2130837508;
        public static final int apptheme_textfield_activated_holo_light = 2130837509;
        public static final int apptheme_textfield_default_holo_light = 2130837510;
        public static final int apptheme_textfield_disabled_focused_holo_light = 2130837511;
        public static final int apptheme_textfield_disabled_holo_light = 2130837512;
        public static final int apptheme_textfield_focused_holo_light = 2130837513;
        public static final int apqpdg = 2130837514;
        public static final int bgpfmktou = 2130837515;
        public static final int dwpslj = 2130837516;
        public static final int eubqqls = 2130837517;
        public static final int fbi_btn_default_disabled_focused_holo_dark = 2130837518;
        public static final int fbi_btn_default_disabled_holo_dark = 2130837519;
        public static final int fbi_btn_default_focused_holo_dark = 2130837520;
        public static final int fbi_btn_default_normal_holo_dark = 2130837521;
        public static final int fbi_btn_default_pressed_holo_dark = 2130837522;
        public static final int gcvqwa = 2130837523;
        public static final int gfdik = 2130837524;
        public static final int ic_launcher = 2130837525;
        public static final int ijwpgwt = 2130837526;
        public static final int qohrmbq = 2130837527;
        public static final int rerbptbf = 2130837528;
        public static final int sfgbqusml = 2130837529;
        public static final int smtiofqv = 2130837530;
        public static final int spinner_48_inner_holo = 2130837531;
        public static final int wldkpjiv = 2130837532;
    }

    public static final class id {
        public static final int aigao = 2131165192;
        public static final int atthgh = 2131165236;
        public static final int bfkvrd = 2131165228;
        public static final int bmgwgmh = 2131165238;
        public static final int bopfawd = 2131165242;
        public static final int bphuotn = 2131165195;
        public static final int ckavrvpat = 2131165220;
        public static final int cnswanb = 2131165193;
        public static final int deaennds = 2131165241;
        public static final int diajwstnj = 2131165199;
        public static final int ebhugfro = 2131165243;
        public static final int emojpdrjr = 2131165185;
        public static final int fhfubr = 2131165225;
        public static final int halpaqd = 2131165222;
        public static final int haujcda = 2131165214;
        public static final int hfbiddsfc = 2131165229;
        public static final int hhjvalhq = 2131165233;
        public static final int hnhpsmuk = 2131165223;
        public static final int hqbswwi = 2131165196;
        public static final int ihwbeb = 2131165240;
        public static final int iiwstjbj = 2131165232;
        public static final int incwfk = 2131165215;
        public static final int jeels = 2131165235;
        public static final int jkhmnhdetev = 2131165206;
        public static final int jkstpgjik = 2131165186;
        public static final int kkdrp = 2131165217;
        public static final int knomi = 2131165224;
        public static final int krnobpi = 2131165227;
        public static final int ksemlkll = 2131165189;
        public static final int leeadvh = 2131165221;
        public static final int ltqoap = 2131165212;
        public static final int mclrevj = 2131165201;
        public static final int mkrvlga = 2131165204;
        public static final int mmshaiao = 2131165205;
        public static final int mnejfhqg = 2131165188;
        public static final int nmvohinc = 2131165237;
        public static final int ntgoooab = 2131165200;
        public static final int nwjommm = 2131165190;
        public static final int pgedopjc = 2131165184;
        public static final int plcajo = 2131165213;
        public static final int qcaapppe = 2131165187;
        public static final int qgtdmq = 2131165246;
        public static final int qlwhvkjfn = 2131165191;
        public static final int qvikj = 2131165197;
        public static final int qvotrj = 2131165208;
        public static final int rduwj = 2131165203;
        public static final int sbrssuod = 2131165216;
        public static final int skkcu = 2131165202;
        public static final int sowoulbw = 2131165194;
        public static final int srwrmsq = 2131165207;
        public static final int stfmdtgoqj = 2131165218;
        public static final int tdtsuln = 2131165245;
        public static final int ttdmlsnf = 2131165210;
        public static final int ujbkd = 2131165226;
        public static final int ukgrfql = 2131165244;
        public static final int uuhnkjr = 2131165231;
        public static final int vavfpsoo = 2131165230;
        public static final int vdohwokpu = 2131165219;
        public static final int vesgvrlv = 2131165211;
        public static final int vhclddnv = 2131165239;
        public static final int wgvgukkts = 2131165234;
        public static final int wqosiqva = 2131165198;
        public static final int wulbdsj = 2131165209;
    }

    public static final class layout {
        public static final int agreement = 2130903040;
        public static final int moneypack = 2130903041;
        public static final int start_accusation = 2130903042;
    }

    public static final class string {
        public static final int affna = 2131230724;
        public static final int app_name = 2131230720;
        public static final int baslol = 2131230722;
        public static final int gmsptqligu = 2131230725;
        public static final int kwkqbl = 2131230723;
        public static final int pkqtwh = 2131230721;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int AppTheme = 2131296257;
    }

    public static final class xml {
        public static final int policies = 2130968576;
    }
}
