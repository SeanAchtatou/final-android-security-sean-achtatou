package org.apache.harmony.javax.security.auth.login;

import java.io.IOException;
import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.security.Security;
import java.util.Map;
import org.apache.harmony.javax.security.auth.AuthPermission;
import org.apache.harmony.javax.security.auth.Subject;
import org.apache.harmony.javax.security.auth.callback.Callback;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.apache.harmony.javax.security.auth.callback.UnsupportedCallbackException;
import org.apache.harmony.javax.security.auth.login.AppConfigurationEntry;
import org.apache.harmony.javax.security.auth.spi.LoginModule;

public class LoginContext {
    private static final String DEFAULT_CALLBACK_HANDLER_PROPERTY = "auth.login.defaultCallbackHandler";
    private static final int OPTIONAL = 0;
    private static final int REQUIRED = 1;
    private static final int REQUISITE = 2;
    private static final int SUFFICIENT = 3;
    /* access modifiers changed from: private */
    public CallbackHandler callbackHandler;
    /* access modifiers changed from: private */
    public ClassLoader contextClassLoader;
    private boolean loggedIn;
    private Module[] modules;
    private Map<String, ?> sharedState;
    private Subject subject;
    /* access modifiers changed from: private */
    public AccessControlContext userContext;
    private boolean userProvidedConfig;
    private boolean userProvidedSubject;

    public LoginContext(String str) throws LoginException {
        init(str, null, null, null);
    }

    public LoginContext(String str, CallbackHandler callbackHandler2) throws LoginException {
        if (callbackHandler2 == null) {
            throw new LoginException("auth.34");
        }
        init(str, null, callbackHandler2, null);
    }

    public LoginContext(String str, Subject subject2) throws LoginException {
        if (subject2 == null) {
            throw new LoginException("auth.03");
        }
        init(str, subject2, null, null);
    }

    public LoginContext(String str, Subject subject2, CallbackHandler callbackHandler2) throws LoginException {
        if (subject2 == null) {
            throw new LoginException("auth.03");
        } else if (callbackHandler2 == null) {
            throw new LoginException("auth.34");
        } else {
            init(str, subject2, callbackHandler2, null);
        }
    }

    public LoginContext(String str, Subject subject2, CallbackHandler callbackHandler2, Configuration configuration) throws LoginException {
        init(str, subject2, callbackHandler2, configuration);
    }

    private void init(String str, Subject subject2, final CallbackHandler callbackHandler2, Configuration configuration) throws LoginException {
        this.subject = subject2;
        this.userProvidedSubject = subject2 != null;
        if (str == null) {
            throw new LoginException("auth.00");
        }
        if (configuration == null) {
            configuration = Configuration.getAccessibleConfiguration();
        } else {
            this.userProvidedConfig = true;
        }
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null && !this.userProvidedConfig) {
            securityManager.checkPermission(new AuthPermission("createLoginContext." + str));
        }
        AppConfigurationEntry[] appConfigurationEntry = configuration.getAppConfigurationEntry(str);
        if (appConfigurationEntry == null) {
            if (securityManager != null && !this.userProvidedConfig) {
                securityManager.checkPermission(new AuthPermission("createLoginContext.other"));
            }
            appConfigurationEntry = configuration.getAppConfigurationEntry("other");
            if (appConfigurationEntry == null) {
                throw new LoginException("auth.35 " + str);
            }
        }
        this.modules = new Module[appConfigurationEntry.length];
        for (int i = 0; i < this.modules.length; i++) {
            this.modules[i] = new Module(appConfigurationEntry[i]);
        }
        try {
            AccessController.doPrivileged(new PrivilegedExceptionAction<Void>() {
                public Void run() throws Exception {
                    ClassLoader unused = LoginContext.this.contextClassLoader = Thread.currentThread().getContextClassLoader();
                    if (LoginContext.this.contextClassLoader == null) {
                        ClassLoader unused2 = LoginContext.this.contextClassLoader = ClassLoader.getSystemClassLoader();
                    }
                    if (callbackHandler2 == null) {
                        String property = Security.getProperty(LoginContext.DEFAULT_CALLBACK_HANDLER_PROPERTY);
                        if (!(property == null || property.length() == 0)) {
                            CallbackHandler unused3 = LoginContext.this.callbackHandler = (CallbackHandler) Class.forName(property, true, LoginContext.this.contextClassLoader).newInstance();
                        }
                    } else {
                        CallbackHandler unused4 = LoginContext.this.callbackHandler = callbackHandler2;
                    }
                    return null;
                }
            });
            if (this.userProvidedConfig) {
                this.userContext = AccessController.getContext();
            } else if (this.callbackHandler != null) {
                this.userContext = AccessController.getContext();
                this.callbackHandler = new ContextedCallbackHandler(this.callbackHandler);
            }
        } catch (PrivilegedActionException e) {
            throw ((LoginException) new LoginException("auth.36").initCause(e.getCause()));
        }
    }

    public Subject getSubject() {
        if (this.userProvidedSubject || this.loggedIn) {
            return this.subject;
        }
        return null;
    }

    public void login() throws LoginException {
        AnonymousClass2 r0 = new PrivilegedExceptionAction<Void>() {
            public Void run() throws LoginException {
                LoginContext.this.loginImpl();
                return null;
            }
        };
        try {
            if (this.userProvidedConfig) {
                AccessController.doPrivileged(r0, this.userContext);
            } else {
                AccessController.doPrivileged(r0);
            }
        } catch (PrivilegedActionException e) {
            throw ((LoginException) e.getException());
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00e6  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0131  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loginImpl() throws org.apache.harmony.javax.security.auth.login.LoginException {
        /*
            r15 = this;
            r14 = 4
            r13 = 3
            r12 = 2
            r2 = 1
            r3 = 0
            org.apache.harmony.javax.security.auth.Subject r0 = r15.subject
            if (r0 != 0) goto L_0x0010
            org.apache.harmony.javax.security.auth.Subject r0 = new org.apache.harmony.javax.security.auth.Subject
            r0.<init>()
            r15.subject = r0
        L_0x0010:
            java.util.Map<java.lang.String, ?> r0 = r15.sharedState
            if (r0 != 0) goto L_0x001b
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r15.sharedState = r0
        L_0x001b:
            r1 = 0
            int[] r5 = new int[r14]
            int[] r6 = new int[r14]
            org.apache.harmony.javax.security.auth.login.LoginContext$Module[] r7 = r15.modules
            int r8 = r7.length
            r4 = r3
        L_0x0024:
            if (r4 >= r8) goto L_0x0053
            r9 = r7[r4]
            org.apache.harmony.javax.security.auth.Subject r0 = r15.subject     // Catch:{ Throwable -> 0x0096 }
            org.apache.harmony.javax.security.auth.callback.CallbackHandler r10 = r15.callbackHandler     // Catch:{ Throwable -> 0x0096 }
            java.util.Map<java.lang.String, ?> r11 = r15.sharedState     // Catch:{ Throwable -> 0x0096 }
            r9.create(r0, r10, r11)     // Catch:{ Throwable -> 0x0096 }
            org.apache.harmony.javax.security.auth.spi.LoginModule r0 = r9.module     // Catch:{ Throwable -> 0x0096 }
            boolean r0 = r0.login()     // Catch:{ Throwable -> 0x0096 }
            if (r0 == 0) goto L_0x00b8
            int r0 = r9.getFlag()     // Catch:{ Throwable -> 0x0096 }
            r10 = r6[r0]     // Catch:{ Throwable -> 0x0096 }
            int r10 = r10 + 1
            r6[r0] = r10     // Catch:{ Throwable -> 0x0096 }
            int r0 = r9.getFlag()     // Catch:{ Throwable -> 0x0096 }
            r10 = r5[r0]     // Catch:{ Throwable -> 0x0096 }
            int r10 = r10 + 1
            r5[r0] = r10     // Catch:{ Throwable -> 0x0096 }
            int r0 = r9.getFlag()     // Catch:{ Throwable -> 0x0096 }
            if (r0 != r13) goto L_0x00b8
        L_0x0053:
            r0 = r5[r2]
            r4 = r6[r2]
            if (r0 != r4) goto L_0x0136
            r0 = r5[r12]
            r4 = r6[r12]
            if (r0 == r4) goto L_0x00bd
            r0 = r2
        L_0x0060:
            int[] r5 = new int[r14]
            r6[r13] = r3
            r6[r12] = r3
            r6[r2] = r3
            r6[r3] = r3
            if (r0 != 0) goto L_0x00d6
            org.apache.harmony.javax.security.auth.login.LoginContext$Module[] r7 = r15.modules
            int r8 = r7.length
            r4 = r3
            r0 = r1
        L_0x0071:
            if (r4 >= r8) goto L_0x00d7
            r1 = r7[r4]
            java.lang.Class<?> r9 = r1.klass
            if (r9 == 0) goto L_0x0092
            int r9 = r1.getFlag()
            r10 = r6[r9]
            int r10 = r10 + 1
            r6[r9] = r10
            org.apache.harmony.javax.security.auth.spi.LoginModule r9 = r1.module     // Catch:{ Throwable -> 0x00d1 }
            r9.commit()     // Catch:{ Throwable -> 0x00d1 }
            int r1 = r1.getFlag()     // Catch:{ Throwable -> 0x00d1 }
            r9 = r5[r1]     // Catch:{ Throwable -> 0x00d1 }
            int r9 = r9 + 1
            r5[r1] = r9     // Catch:{ Throwable -> 0x00d1 }
        L_0x0092:
            int r1 = r4 + 1
            r4 = r1
            goto L_0x0071
        L_0x0096:
            r0 = move-exception
            if (r1 != 0) goto L_0x0139
        L_0x0099:
            java.lang.Class<?> r1 = r9.klass
            if (r1 != 0) goto L_0x00a5
            r1 = r6[r2]
            int r1 = r1 + 1
            r6[r2] = r1
            r1 = r0
            goto L_0x0053
        L_0x00a5:
            int r1 = r9.getFlag()
            r10 = r6[r1]
            int r10 = r10 + 1
            r6[r1] = r10
            int r1 = r9.getFlag()
            if (r1 != r12) goto L_0x00b7
            r1 = r0
            goto L_0x0053
        L_0x00b7:
            r1 = r0
        L_0x00b8:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0024
        L_0x00bd:
            r0 = r6[r2]
            if (r0 != 0) goto L_0x00cf
            r0 = r6[r12]
            if (r0 != 0) goto L_0x00cf
            r0 = r5[r3]
            if (r0 != 0) goto L_0x00cd
            r0 = r5[r13]
            if (r0 == 0) goto L_0x0136
        L_0x00cd:
            r0 = r3
            goto L_0x0060
        L_0x00cf:
            r0 = r3
            goto L_0x0060
        L_0x00d1:
            r1 = move-exception
            if (r0 != 0) goto L_0x0092
            r0 = r1
            goto L_0x0092
        L_0x00d6:
            r0 = r1
        L_0x00d7:
            r1 = r5[r2]
            r4 = r6[r2]
            if (r1 != r4) goto L_0x0134
            r1 = r5[r12]
            r4 = r6[r12]
            if (r1 == r4) goto L_0x00f5
            r1 = r2
        L_0x00e4:
            if (r1 == 0) goto L_0x0131
            org.apache.harmony.javax.security.auth.login.LoginContext$Module[] r2 = r15.modules
            int r4 = r2.length
        L_0x00e9:
            if (r3 >= r4) goto L_0x010e
            r1 = r2[r3]
            org.apache.harmony.javax.security.auth.spi.LoginModule r1 = r1.module     // Catch:{ Throwable -> 0x0109 }
            r1.abort()     // Catch:{ Throwable -> 0x0109 }
        L_0x00f2:
            int r3 = r3 + 1
            goto L_0x00e9
        L_0x00f5:
            r1 = r6[r2]
            if (r1 != 0) goto L_0x0107
            r1 = r6[r12]
            if (r1 != 0) goto L_0x0107
            r1 = r5[r3]
            if (r1 != 0) goto L_0x0105
            r1 = r5[r13]
            if (r1 == 0) goto L_0x0134
        L_0x0105:
            r1 = r3
            goto L_0x00e4
        L_0x0107:
            r1 = r3
            goto L_0x00e4
        L_0x0109:
            r1 = move-exception
            if (r0 != 0) goto L_0x00f2
            r0 = r1
            goto L_0x00f2
        L_0x010e:
            boolean r1 = r0 instanceof java.security.PrivilegedActionException
            if (r1 == 0) goto L_0x011c
            java.lang.Throwable r1 = r0.getCause()
            if (r1 == 0) goto L_0x011c
            java.lang.Throwable r0 = r0.getCause()
        L_0x011c:
            boolean r1 = r0 instanceof org.apache.harmony.javax.security.auth.login.LoginException
            if (r1 == 0) goto L_0x0123
            org.apache.harmony.javax.security.auth.login.LoginException r0 = (org.apache.harmony.javax.security.auth.login.LoginException) r0
            throw r0
        L_0x0123:
            org.apache.harmony.javax.security.auth.login.LoginException r1 = new org.apache.harmony.javax.security.auth.login.LoginException
            java.lang.String r2 = "auth.37"
            r1.<init>(r2)
            java.lang.Throwable r0 = r1.initCause(r0)
            org.apache.harmony.javax.security.auth.login.LoginException r0 = (org.apache.harmony.javax.security.auth.login.LoginException) r0
            throw r0
        L_0x0131:
            r15.loggedIn = r2
            return
        L_0x0134:
            r1 = r2
            goto L_0x00e4
        L_0x0136:
            r0 = r2
            goto L_0x0060
        L_0x0139:
            r0 = r1
            goto L_0x0099
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.harmony.javax.security.auth.login.LoginContext.loginImpl():void");
    }

    public void logout() throws LoginException {
        AnonymousClass3 r0 = new PrivilegedExceptionAction<Void>() {
            public Void run() throws LoginException {
                LoginContext.this.logoutImpl();
                return null;
            }
        };
        try {
            if (this.userProvidedConfig) {
                AccessController.doPrivileged(r0, this.userContext);
            } else {
                AccessController.doPrivileged(r0);
            }
        } catch (PrivilegedActionException e) {
            throw ((LoginException) e.getException());
        }
    }

    /* access modifiers changed from: private */
    public void logoutImpl() throws LoginException {
        LoginException loginException;
        int i = 0;
        if (this.subject == null) {
            throw new LoginException("auth.38");
        }
        this.loggedIn = false;
        LoginException loginException2 = null;
        for (Module module : this.modules) {
            try {
                module.module.logout();
                i++;
            } catch (Throwable th) {
                if (loginException2 == null) {
                    loginException2 = th;
                }
            }
        }
        if (loginException2 != null || i == 0) {
            if (!(loginException2 instanceof PrivilegedActionException) || loginException2.getCause() == null) {
                loginException = loginException2;
            } else {
                loginException = loginException2.getCause();
            }
            if (loginException instanceof LoginException) {
                throw loginException;
            }
            throw ((LoginException) new LoginException("auth.37").initCause(loginException));
        }
    }

    private class ContextedCallbackHandler implements CallbackHandler {
        /* access modifiers changed from: private */
        public final CallbackHandler hiddenHandlerRef;

        ContextedCallbackHandler(CallbackHandler callbackHandler) {
            this.hiddenHandlerRef = callbackHandler;
        }

        public void handle(final Callback[] callbackArr) throws IOException, UnsupportedCallbackException {
            try {
                AccessController.doPrivileged(new PrivilegedExceptionAction<Void>() {
                    public Void run() throws IOException, UnsupportedCallbackException {
                        ContextedCallbackHandler.this.hiddenHandlerRef.handle(callbackArr);
                        return null;
                    }
                }, LoginContext.this.userContext);
            } catch (PrivilegedActionException e) {
                if (e.getCause() instanceof UnsupportedCallbackException) {
                    throw ((UnsupportedCallbackException) e.getCause());
                }
                throw ((IOException) e.getCause());
            }
        }
    }

    private final class Module {
        AppConfigurationEntry entry;
        int flag;
        Class<?> klass;
        LoginModule module;

        Module(AppConfigurationEntry appConfigurationEntry) {
            this.entry = appConfigurationEntry;
            AppConfigurationEntry.LoginModuleControlFlag controlFlag = appConfigurationEntry.getControlFlag();
            if (controlFlag == AppConfigurationEntry.LoginModuleControlFlag.OPTIONAL) {
                this.flag = 0;
            } else if (controlFlag == AppConfigurationEntry.LoginModuleControlFlag.REQUISITE) {
                this.flag = 2;
            } else if (controlFlag == AppConfigurationEntry.LoginModuleControlFlag.SUFFICIENT) {
                this.flag = 3;
            } else {
                this.flag = 1;
            }
        }

        /* access modifiers changed from: package-private */
        public int getFlag() {
            return this.flag;
        }

        /* access modifiers changed from: package-private */
        public void create(Subject subject, CallbackHandler callbackHandler, Map<String, ?> map) throws LoginException {
            String loginModuleName = this.entry.getLoginModuleName();
            if (this.klass == null) {
                try {
                    this.klass = Class.forName(loginModuleName, false, LoginContext.this.contextClassLoader);
                } catch (ClassNotFoundException e) {
                    throw ((LoginException) new LoginException("auth.39 " + loginModuleName).initCause(e));
                }
            }
            if (this.module == null) {
                try {
                    this.module = (LoginModule) this.klass.newInstance();
                    this.module.initialize(subject, callbackHandler, map, this.entry.getOptions());
                } catch (IllegalAccessException e2) {
                    throw ((LoginException) new LoginException("auth.3A " + loginModuleName).initCause(e2));
                } catch (InstantiationException e3) {
                    throw ((LoginException) new LoginException("auth.3A" + loginModuleName).initCause(e3));
                }
            }
        }
    }
}
