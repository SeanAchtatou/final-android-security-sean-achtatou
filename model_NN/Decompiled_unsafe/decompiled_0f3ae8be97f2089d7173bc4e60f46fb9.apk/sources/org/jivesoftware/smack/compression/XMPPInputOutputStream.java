package org.jivesoftware.smack.compression;

import java.io.InputStream;
import java.io.OutputStream;

public abstract class XMPPInputOutputStream {
    protected static FlushMethod flushMethod;
    protected String compressionMethod;

    public enum FlushMethod {
        FULL_FLUSH,
        SYNC_FLUSH
    }

    public abstract InputStream getInputStream(InputStream inputStream) throws Exception;

    public abstract OutputStream getOutputStream(OutputStream outputStream) throws Exception;

    public abstract boolean isSupported();

    public static void setFlushMethod(FlushMethod flushMethod2) {
        flushMethod = flushMethod2;
    }

    public String getCompressionMethod() {
        return this.compressionMethod;
    }
}
