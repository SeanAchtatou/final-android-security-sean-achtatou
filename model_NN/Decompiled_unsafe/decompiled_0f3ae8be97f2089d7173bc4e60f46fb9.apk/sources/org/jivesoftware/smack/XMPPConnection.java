package org.jivesoftware.smack;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.harmony.javax.security.sasl.SaslException;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.compression.XMPPInputOutputStream;
import org.jivesoftware.smack.debugger.SmackDebugger;
import org.jivesoftware.smack.filter.IQReplyFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Bind;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Session;

public abstract class XMPPConnection {
    /* access modifiers changed from: private */
    public static final Logger LOGGER = Logger.getLogger(XMPPConnection.class.getName());
    private static final AtomicInteger connectionCounter = new AtomicInteger(0);
    private static final Set<ConnectionCreationListener> connectionEstablishedListeners = new CopyOnWriteArraySet();
    protected boolean authenticated = false;
    private AtomicBoolean bindingRequired = new AtomicBoolean(false);
    protected final Collection<PacketCollector> collectors = new ConcurrentLinkedQueue();
    protected XMPPInputOutputStream compressionHandler;
    protected final ConnectionConfiguration config;
    protected final int connectionCounterValue = connectionCounter.getAndIncrement();
    private IOException connectionException;
    protected final Collection<ConnectionListener> connectionListeners = new CopyOnWriteArrayList();
    protected SmackDebugger debugger = null;
    private final ScheduledExecutorService executorService = new ScheduledThreadPoolExecutor(1, new SmackExecutorThreadFactory(this.connectionCounterValue));
    private FromMode fromMode = FromMode.OMITTED;
    private String host;
    protected final Map<PacketInterceptor, InterceptorWrapper> interceptors = new ConcurrentHashMap();
    private long packetReplyTimeout = ((long) SmackConfiguration.getDefaultPacketReplyTimeout());
    private int port;
    protected Reader reader;
    protected final Map<PacketListener, ListenerWrapper> recvListeners = new ConcurrentHashMap();
    private Roster roster;
    private boolean rosterVersioningSupported = false;
    protected SASLAuthentication saslAuthentication = new SASLAuthentication(this);
    protected final Map<PacketListener, ListenerWrapper> sendListeners = new ConcurrentHashMap();
    private String serviceCapsNode;
    private boolean sessionSupported;
    protected boolean wasAuthenticated = false;
    protected Writer writer;

    public enum FromMode {
        UNCHANGED,
        OMITTED,
        USER
    }

    /* access modifiers changed from: protected */
    public abstract void connectInternal() throws SmackException, IOException, XMPPException;

    public abstract String getConnectionID();

    public abstract String getUser();

    public abstract boolean isAnonymous();

    public abstract boolean isAuthenticated();

    public abstract boolean isConnected();

    public abstract boolean isSecureConnection();

    public abstract boolean isUsingCompression();

    public abstract void login(String str, String str2, String str3) throws XMPPException, SmackException, SaslException, IOException;

    public abstract void loginAnonymously() throws XMPPException, SmackException, SaslException, IOException;

    /* access modifiers changed from: protected */
    public abstract void sendPacketInternal(Packet packet) throws SmackException.NotConnectedException;

    /* access modifiers changed from: protected */
    public abstract void shutdown();

    static {
        SmackConfiguration.getVersion();
    }

    private static final class SmackExecutorThreadFactory implements ThreadFactory {
        private final int connectionCounterValue;
        private int count;

        private SmackExecutorThreadFactory(int i) {
            this.count = 0;
            this.connectionCounterValue = i;
        }

        public Thread newThread(Runnable runnable) {
            StringBuilder append = new StringBuilder().append("Smack Executor Service ");
            int i = this.count;
            this.count = i + 1;
            Thread thread = new Thread(runnable, append.append(i).append(" (").append(this.connectionCounterValue).append(")").toString());
            thread.setDaemon(true);
            return thread;
        }
    }

    protected XMPPConnection(ConnectionConfiguration connectionConfiguration) {
        this.config = connectionConfiguration;
    }

    /* access modifiers changed from: protected */
    public ConnectionConfiguration getConfiguration() {
        return this.config;
    }

    public String getServiceName() {
        return this.config.getServiceName();
    }

    public String getHost() {
        return this.host;
    }

    public int getPort() {
        return this.port;
    }

    public void connect() throws SmackException, IOException, XMPPException {
        this.saslAuthentication.init();
        this.bindingRequired.set(false);
        this.sessionSupported = false;
        this.connectionException = null;
        connectInternal();
    }

    public void login(String str, String str2) throws XMPPException, SmackException, SaslException, IOException {
        login(str, str2, "Smack");
    }

    /* access modifiers changed from: protected */
    public void serverRequiresBinding() {
        synchronized (this.bindingRequired) {
            this.bindingRequired.set(true);
            this.bindingRequired.notify();
        }
    }

    /* access modifiers changed from: protected */
    public void serverSupportsSession() {
        this.sessionSupported = true;
    }

    /* access modifiers changed from: protected */
    public String bindResourceAndEstablishSession(String str) throws XMPPException.XMPPErrorException, SmackException.ResourceBindingNotOfferedException, SmackException.NoResponseException, SmackException.NotConnectedException {
        synchronized (this.bindingRequired) {
            if (!this.bindingRequired.get()) {
                try {
                    this.bindingRequired.wait(getPacketReplyTimeout());
                } catch (InterruptedException e) {
                }
                if (!this.bindingRequired.get()) {
                    throw new SmackException.ResourceBindingNotOfferedException();
                }
            }
        }
        Bind bind = new Bind();
        bind.setResource(str);
        String jid = ((Bind) createPacketCollectorAndSend(bind).nextResultOrThrow()).getJid();
        if (this.sessionSupported && !getConfiguration().isLegacySessionDisabled()) {
            createPacketCollectorAndSend(new Session()).nextResultOrThrow();
        }
        return jid;
    }

    /* access modifiers changed from: protected */
    public void setConnectionException(IOException iOException) {
        this.connectionException = iOException;
    }

    /* access modifiers changed from: protected */
    public void throwConnectionExceptionOrNoResponse() throws IOException, SmackException.NoResponseException {
        if (this.connectionException != null) {
            throw this.connectionException;
        }
        throw new SmackException.NoResponseException();
    }

    /* access modifiers changed from: protected */
    public Reader getReader() {
        return this.reader;
    }

    /* access modifiers changed from: protected */
    public Writer getWriter() {
        return this.writer;
    }

    /* access modifiers changed from: protected */
    public void setServiceName(String str) {
        this.config.setServiceName(str);
    }

    /* access modifiers changed from: protected */
    public void setLoginInfo(String str, String str2, String str3) {
        this.config.setLoginInfo(str, str2, str3);
    }

    /* access modifiers changed from: protected */
    public void serverSupportsAccountCreation() {
        AccountManager.getInstance(this).setSupportsAccountCreation(true);
    }

    /* access modifiers changed from: protected */
    public void maybeResolveDns() throws Exception {
        this.config.maybeResolveDns();
    }

    public void sendPacket(Packet packet) throws SmackException.NotConnectedException {
        if (!isConnected()) {
            throw new SmackException.NotConnectedException();
        } else if (packet == null) {
            throw new NullPointerException("Packet is null.");
        } else {
            switch (this.fromMode) {
                case OMITTED:
                    packet.setFrom(null);
                    break;
                case USER:
                    packet.setFrom(getUser());
                    break;
            }
            firePacketInterceptors(packet);
            sendPacketInternal(packet);
            firePacketSendingListeners(packet);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0029, code lost:
        if (r7.roster.rosterInitialized != false) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0031, code lost:
        if (r7.config.isRosterLoadedAtLogin() == false) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r6 = r7.roster;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0035, code lost:
        monitor-enter(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r2 = getPacketReplyTimeout();
        r0 = java.lang.System.currentTimeMillis();
        r4 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0043, code lost:
        if (r7.roster.rosterInitialized != false) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0049, code lost:
        if (r4 > 0) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004b, code lost:
        monitor-exit(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        r7.roster.wait(r4);
        r2 = java.lang.System.currentTimeMillis();
        r4 = r4 - (r2 - r0);
        r0 = r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.jivesoftware.smack.Roster getRoster() {
        /*
            r7 = this;
            boolean r0 = r7.isAnonymous()
            if (r0 == 0) goto L_0x000e
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Anonymous users can't have a roster"
            r0.<init>(r1)
            throw r0
        L_0x000e:
            monitor-enter(r7)
            org.jivesoftware.smack.Roster r0 = r7.roster     // Catch:{ all -> 0x004f }
            if (r0 != 0) goto L_0x001a
            org.jivesoftware.smack.Roster r0 = new org.jivesoftware.smack.Roster     // Catch:{ all -> 0x004f }
            r0.<init>(r7)     // Catch:{ all -> 0x004f }
            r7.roster = r0     // Catch:{ all -> 0x004f }
        L_0x001a:
            boolean r0 = r7.isAuthenticated()     // Catch:{ all -> 0x004f }
            if (r0 != 0) goto L_0x0024
            org.jivesoftware.smack.Roster r0 = r7.roster     // Catch:{ all -> 0x004f }
            monitor-exit(r7)     // Catch:{ all -> 0x004f }
        L_0x0023:
            return r0
        L_0x0024:
            monitor-exit(r7)     // Catch:{ all -> 0x004f }
            org.jivesoftware.smack.Roster r0 = r7.roster
            boolean r0 = r0.rosterInitialized
            if (r0 != 0) goto L_0x004c
            org.jivesoftware.smack.ConnectionConfiguration r0 = r7.config
            boolean r0 = r0.isRosterLoadedAtLogin()
            if (r0 == 0) goto L_0x004c
            org.jivesoftware.smack.Roster r6 = r7.roster     // Catch:{ InterruptedException -> 0x0065 }
            monitor-enter(r6)     // Catch:{ InterruptedException -> 0x0065 }
            long r2 = r7.getPacketReplyTimeout()     // Catch:{ all -> 0x0062 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0062 }
            r4 = r2
        L_0x003f:
            org.jivesoftware.smack.Roster r2 = r7.roster     // Catch:{ all -> 0x0062 }
            boolean r2 = r2.rosterInitialized     // Catch:{ all -> 0x0062 }
            if (r2 != 0) goto L_0x004b
            r2 = 0
            int r2 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r2 > 0) goto L_0x0052
        L_0x004b:
            monitor-exit(r6)     // Catch:{ all -> 0x0062 }
        L_0x004c:
            org.jivesoftware.smack.Roster r0 = r7.roster
            goto L_0x0023
        L_0x004f:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x004f }
            throw r0
        L_0x0052:
            org.jivesoftware.smack.Roster r2 = r7.roster     // Catch:{ all -> 0x0062 }
            r2.wait(r4)     // Catch:{ all -> 0x0062 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0062 }
            long r0 = r2 - r0
            long r0 = r4 - r0
            r4 = r0
            r0 = r2
            goto L_0x003f
        L_0x0062:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0062 }
            throw r0     // Catch:{ InterruptedException -> 0x0065 }
        L_0x0065:
            r0 = move-exception
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.XMPPConnection.getRoster():org.jivesoftware.smack.Roster");
    }

    /* access modifiers changed from: protected */
    public SASLAuthentication getSASLAuthentication() {
        return this.saslAuthentication;
    }

    public void disconnect() throws SmackException.NotConnectedException {
        disconnect(new Presence(Presence.Type.unavailable));
    }

    public synchronized void disconnect(Presence presence) throws SmackException.NotConnectedException {
        if (isConnected()) {
            sendPacket(presence);
            shutdown();
            callConnectionClosedListener();
        }
    }

    public static void addConnectionCreationListener(ConnectionCreationListener connectionCreationListener) {
        connectionEstablishedListeners.add(connectionCreationListener);
    }

    public static void removeConnectionCreationListener(ConnectionCreationListener connectionCreationListener) {
        connectionEstablishedListeners.remove(connectionCreationListener);
    }

    protected static Collection<ConnectionCreationListener> getConnectionCreationListeners() {
        return Collections.unmodifiableCollection(connectionEstablishedListeners);
    }

    public void addConnectionListener(ConnectionListener connectionListener) {
        if (connectionListener != null && !this.connectionListeners.contains(connectionListener)) {
            this.connectionListeners.add(connectionListener);
        }
    }

    public void removeConnectionListener(ConnectionListener connectionListener) {
        this.connectionListeners.remove(connectionListener);
    }

    /* access modifiers changed from: protected */
    public Collection<ConnectionListener> getConnectionListeners() {
        return this.connectionListeners;
    }

    public PacketCollector createPacketCollectorAndSend(IQ iq) throws SmackException.NotConnectedException {
        PacketCollector createPacketCollector = createPacketCollector(new IQReplyFilter(iq, this));
        sendPacket(iq);
        return createPacketCollector;
    }

    public PacketCollector createPacketCollector(PacketFilter packetFilter) {
        PacketCollector packetCollector = new PacketCollector(this, packetFilter);
        this.collectors.add(packetCollector);
        return packetCollector;
    }

    /* access modifiers changed from: protected */
    public void removePacketCollector(PacketCollector packetCollector) {
        this.collectors.remove(packetCollector);
    }

    /* access modifiers changed from: protected */
    public Collection<PacketCollector> getPacketCollectors() {
        return this.collectors;
    }

    public void addPacketListener(PacketListener packetListener, PacketFilter packetFilter) {
        if (packetListener == null) {
            throw new NullPointerException("Packet listener is null.");
        }
        this.recvListeners.put(packetListener, new ListenerWrapper(packetListener, packetFilter));
    }

    public void removePacketListener(PacketListener packetListener) {
        this.recvListeners.remove(packetListener);
    }

    /* access modifiers changed from: protected */
    public Map<PacketListener, ListenerWrapper> getPacketListeners() {
        return this.recvListeners;
    }

    public void addPacketSendingListener(PacketListener packetListener, PacketFilter packetFilter) {
        if (packetListener == null) {
            throw new NullPointerException("Packet listener is null.");
        }
        this.sendListeners.put(packetListener, new ListenerWrapper(packetListener, packetFilter));
    }

    public void removePacketSendingListener(PacketListener packetListener) {
        this.sendListeners.remove(packetListener);
    }

    /* access modifiers changed from: protected */
    public Map<PacketListener, ListenerWrapper> getPacketSendingListeners() {
        return this.sendListeners;
    }

    private void firePacketSendingListeners(Packet packet) {
        for (ListenerWrapper notifyListener : this.sendListeners.values()) {
            try {
                notifyListener.notifyListener(packet);
            } catch (SmackException.NotConnectedException e) {
                LOGGER.log(Level.WARNING, "Got not connected exception, aborting");
                return;
            }
        }
    }

    public void addPacketInterceptor(PacketInterceptor packetInterceptor, PacketFilter packetFilter) {
        if (packetInterceptor == null) {
            throw new NullPointerException("Packet interceptor is null.");
        }
        this.interceptors.put(packetInterceptor, new InterceptorWrapper(packetInterceptor, packetFilter));
    }

    public void removePacketInterceptor(PacketInterceptor packetInterceptor) {
        this.interceptors.remove(packetInterceptor);
    }

    /* access modifiers changed from: protected */
    public Map<PacketInterceptor, InterceptorWrapper> getPacketInterceptors() {
        return this.interceptors;
    }

    private void firePacketInterceptors(Packet packet) {
        if (packet != null) {
            for (InterceptorWrapper notifyListener : this.interceptors.values()) {
                notifyListener.notifyListener(packet);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initDebugger() {
        String str;
        Class<?> cls = null;
        if (this.reader == null || this.writer == null) {
            throw new NullPointerException("Reader or writer isn't initialized.");
        } else if (!this.config.isDebuggerEnabled()) {
        } else {
            if (this.debugger == null) {
                try {
                    str = System.getProperty("smack.debuggerClass");
                } catch (Throwable th) {
                    str = null;
                }
                if (str != null) {
                    try {
                        cls = Class.forName(str);
                    } catch (Exception e) {
                        LOGGER.warning("Unabled to instantiate debugger class " + str);
                    }
                }
                if (cls == null) {
                    try {
                        cls = Class.forName("de.measite.smack.AndroidDebugger");
                    } catch (Exception e2) {
                        try {
                            cls = Class.forName("org.jivesoftware.smack.debugger.ConsoleDebugger");
                        } catch (Exception e3) {
                            LOGGER.warning("Unabled to instantiate either Smack debugger class");
                        }
                    }
                }
                try {
                    this.debugger = (SmackDebugger) cls.getConstructor(XMPPConnection.class, Writer.class, Reader.class).newInstance(this, this.writer, this.reader);
                    this.reader = this.debugger.getReader();
                    this.writer = this.debugger.getWriter();
                } catch (Exception e4) {
                    throw new IllegalArgumentException("Can't initialize the configured debugger!", e4);
                }
            } else {
                this.reader = this.debugger.newConnectionReader(this.reader);
                this.writer = this.debugger.newConnectionWriter(this.writer);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setServiceCapsNode(String str) {
        this.serviceCapsNode = str;
    }

    public String getServiceCapsNode() {
        return this.serviceCapsNode;
    }

    public boolean isRosterVersioningSupported() {
        return this.rosterVersioningSupported;
    }

    /* access modifiers changed from: protected */
    public void setRosterVersioningSupported() {
        this.rosterVersioningSupported = true;
    }

    public long getPacketReplyTimeout() {
        return this.packetReplyTimeout;
    }

    public void setPacketReplyTimeout(long j) {
        this.packetReplyTimeout = j;
    }

    /* access modifiers changed from: protected */
    public void processPacket(Packet packet) {
        if (packet != null) {
            for (PacketCollector processPacket : getPacketCollectors()) {
                processPacket.processPacket(packet);
            }
            this.executorService.submit(new ListenerNotification(packet));
        }
    }

    private class ListenerNotification implements Runnable {
        private Packet packet;

        public ListenerNotification(Packet packet2) {
            this.packet = packet2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
         arg types: [java.util.logging.Level, java.lang.String, org.jivesoftware.smack.SmackException$NotConnectedException]
         candidates:
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
         arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
         candidates:
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
        public void run() {
            for (ListenerWrapper notifyListener : XMPPConnection.this.recvListeners.values()) {
                try {
                    notifyListener.notifyListener(this.packet);
                } catch (SmackException.NotConnectedException e) {
                    XMPPConnection.LOGGER.log(Level.WARNING, "Got not connected exception, aborting", (Throwable) e);
                    return;
                } catch (Exception e2) {
                    XMPPConnection.LOGGER.log(Level.SEVERE, "Exception in packet listener", (Throwable) e2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setWasAuthenticated(boolean z) {
        if (!this.wasAuthenticated) {
            this.wasAuthenticated = z;
        }
    }

    /* access modifiers changed from: protected */
    public void callConnectionConnectedListener() {
        for (ConnectionListener connected : getConnectionListeners()) {
            connected.connected(this);
        }
    }

    /* access modifiers changed from: protected */
    public void callConnectionAuthenticatedListener() {
        for (ConnectionListener authenticated2 : getConnectionListeners()) {
            authenticated2.authenticated(this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: package-private */
    public void callConnectionClosedListener() {
        for (ConnectionListener connectionClosed : getConnectionListeners()) {
            try {
                connectionClosed.connectionClosed();
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, "Error in listener while closing connection", (Throwable) e);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: protected */
    public void callConnectionClosedOnErrorListener(Exception exc) {
        LOGGER.log(Level.WARNING, "Connection closed with error", (Throwable) exc);
        for (ConnectionListener connectionClosedOnError : getConnectionListeners()) {
            try {
                connectionClosedOnError.connectionClosedOnError(exc);
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, "Error in listener while closing connection", (Throwable) e);
            }
        }
    }

    protected static class ListenerWrapper {
        private PacketFilter packetFilter;
        private PacketListener packetListener;

        public ListenerWrapper(PacketListener packetListener2, PacketFilter packetFilter2) {
            this.packetListener = packetListener2;
            this.packetFilter = packetFilter2;
        }

        public void notifyListener(Packet packet) throws SmackException.NotConnectedException {
            if (this.packetFilter == null || this.packetFilter.accept(packet)) {
                this.packetListener.processPacket(packet);
            }
        }
    }

    protected static class InterceptorWrapper {
        private PacketFilter packetFilter;
        private PacketInterceptor packetInterceptor;

        public InterceptorWrapper(PacketInterceptor packetInterceptor2, PacketFilter packetFilter2) {
            this.packetInterceptor = packetInterceptor2;
            this.packetFilter = packetFilter2;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (obj instanceof InterceptorWrapper) {
                return ((InterceptorWrapper) obj).packetInterceptor.equals(this.packetInterceptor);
            }
            if (obj instanceof PacketInterceptor) {
                return obj.equals(this.packetInterceptor);
            }
            return false;
        }

        public void notifyListener(Packet packet) {
            if (this.packetFilter == null || this.packetFilter.accept(packet)) {
                this.packetInterceptor.interceptPacket(packet);
            }
        }
    }

    /* access modifiers changed from: protected */
    public ScheduledFuture<?> schedule(Runnable runnable, long j, TimeUnit timeUnit) {
        return this.executorService.schedule(runnable, j, timeUnit);
    }

    public int getConnectionCounter() {
        return this.connectionCounterValue;
    }

    public void setFromMode(FromMode fromMode2) {
        this.fromMode = fromMode2;
    }

    public FromMode getFromMode() {
        return this.fromMode;
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        try {
            this.executorService.shutdownNow();
        } finally {
            super.finalize();
        }
    }
}
