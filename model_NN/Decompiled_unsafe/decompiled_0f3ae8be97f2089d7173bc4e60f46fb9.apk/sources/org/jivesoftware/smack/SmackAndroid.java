package org.jivesoftware.smack;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.logging.Logger;
import org.jivesoftware.smack.util.DNSUtil;
import org.jivesoftware.smack.util.dns.dnsjava.DNSJavaResolver;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.ResolverConfig;

public class SmackAndroid {
    /* access modifiers changed from: private */
    public static final Logger LOGGER = Logger.getLogger(SmackAndroid.class.getName());
    private static boolean receiverRegistered = false;
    private static SmackAndroid sSmackAndroid = null;
    private BroadcastReceiver mConnectivityChangedReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            SmackAndroid.LOGGER.fine("ConnectivityChange received, calling ResolverConfig.refresh() and Lookup.refreshDefault() in new Thread");
            AnonymousClass1 r0 = new Thread() {
                public void run() {
                    ResolverConfig.refresh();
                    Lookup.refreshDefault();
                }
            };
            r0.setDaemon(true);
            r0.start();
        }
    };
    private Context mCtx;

    private SmackAndroid(Context context) {
        this.mCtx = context.getApplicationContext();
        DNSUtil.setDNSResolver(DNSJavaResolver.getInstance());
    }

    public static synchronized SmackAndroid init(Context context) {
        SmackAndroid smackAndroid;
        synchronized (SmackAndroid.class) {
            if (sSmackAndroid == null) {
                sSmackAndroid = new SmackAndroid(context);
            }
            sSmackAndroid.maybeRegisterReceiver();
            smackAndroid = sSmackAndroid;
        }
        return smackAndroid;
    }

    public synchronized void onDestroy() {
        LOGGER.fine("onDestroy: receiverRegistered=" + receiverRegistered);
        if (receiverRegistered) {
            this.mCtx.unregisterReceiver(this.mConnectivityChangedReceiver);
            receiverRegistered = false;
        }
    }

    private void maybeRegisterReceiver() {
        LOGGER.fine("maybeRegisterReceiver: receiverRegistered=" + receiverRegistered);
        if (!receiverRegistered) {
            this.mCtx.registerReceiver(this.mConnectivityChangedReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            receiverRegistered = true;
        }
    }
}
