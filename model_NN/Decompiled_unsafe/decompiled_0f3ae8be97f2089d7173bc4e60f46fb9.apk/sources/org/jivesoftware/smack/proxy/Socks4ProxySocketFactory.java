package org.jivesoftware.smack.proxy;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import javax.net.SocketFactory;

public class Socks4ProxySocketFactory extends SocketFactory {
    private ProxyInfo proxy;

    public Socks4ProxySocketFactory(ProxyInfo proxyInfo) {
        this.proxy = proxyInfo;
    }

    public Socket createSocket(String str, int i) throws IOException, UnknownHostException {
        return socks4ProxifiedSocket(str, i);
    }

    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) throws IOException, UnknownHostException {
        return socks4ProxifiedSocket(str, i);
    }

    public Socket createSocket(InetAddress inetAddress, int i) throws IOException {
        return socks4ProxifiedSocket(inetAddress.getHostAddress(), i);
    }

    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) throws IOException {
        return socks4ProxifiedSocket(inetAddress.getHostAddress(), i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0056, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0062, code lost:
        throw new org.jivesoftware.smack.proxy.ProxyException(org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4, r0.toString(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0063, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0064, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0096, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0063 A[ExcHandler: RuntimeException (r0v4 'e' java.lang.RuntimeException A[CUSTOM_DECLARE]), Splitter:B:1:0x0015] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0099 A[SYNTHETIC, Splitter:B:28:0x0099] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.net.Socket socks4ProxifiedSocket(java.lang.String r12, int r13) throws java.io.IOException {
        /*
            r11 = this;
            r3 = 4
            r0 = 0
            r2 = 0
            org.jivesoftware.smack.proxy.ProxyInfo r1 = r11.proxy
            java.lang.String r4 = r1.getProxyAddress()
            org.jivesoftware.smack.proxy.ProxyInfo r1 = r11.proxy
            int r5 = r1.getProxyPort()
            org.jivesoftware.smack.proxy.ProxyInfo r1 = r11.proxy
            java.lang.String r6 = r1.getProxyUsername()
            java.net.Socket r1 = new java.net.Socket     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0102 }
            r1.<init>(r4, r5)     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0102 }
            java.io.InputStream r5 = r1.getInputStream()     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            java.io.OutputStream r7 = r1.getOutputStream()     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r2 = 1
            r1.setTcpNoDelay(r2)     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r8 = new byte[r2]     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r2 = 0
            r4 = 1
            r9 = 4
            r8[r2] = r9     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r2 = 2
            r9 = 1
            r8[r4] = r9     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r4 = 3
            int r9 = r13 >>> 8
            byte r9 = (byte) r9     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r8[r2] = r9     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r2 = r13 & 255(0xff, float:3.57E-43)
            byte r2 = (byte) r2     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r8[r4] = r2     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            java.net.InetAddress r2 = java.net.InetAddress.getByName(r12)     // Catch:{ UnknownHostException -> 0x0056 }
            byte[] r9 = r2.getAddress()     // Catch:{ UnknownHostException -> 0x0056 }
            r2 = r3
            r3 = r0
        L_0x0048:
            int r4 = r9.length     // Catch:{ UnknownHostException -> 0x0056 }
            if (r3 >= r4) goto L_0x0065
            int r4 = r2 + 1
            byte r10 = r9[r3]     // Catch:{ UnknownHostException -> 0x0056 }
            r8[r2] = r10     // Catch:{ UnknownHostException -> 0x0056 }
            int r2 = r3 + 1
            r3 = r2
            r2 = r4
            goto L_0x0048
        L_0x0056:
            r0 = move-exception
            org.jivesoftware.smack.proxy.ProxyException r2 = new org.jivesoftware.smack.proxy.ProxyException     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r3 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            java.lang.String r4 = r0.toString()     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r2.<init>(r3, r4, r0)     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            throw r2     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
        L_0x0063:
            r0 = move-exception
            throw r0
        L_0x0065:
            if (r6 == 0) goto L_0x0078
            byte[] r3 = r6.getBytes()     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r4 = 0
            int r9 = r6.length()     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            java.lang.System.arraycopy(r3, r4, r8, r2, r9)     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            int r3 = r6.length()     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            int r2 = r2 + r3
        L_0x0078:
            int r3 = r2 + 1
            r4 = 0
            r8[r2] = r4     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r2 = 0
            r7.write(r8, r2, r3)     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r2 = 6
        L_0x0082:
            if (r0 >= r2) goto L_0x00aa
            int r3 = r2 - r0
            int r3 = r5.read(r8, r0, r3)     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            if (r3 > 0) goto L_0x00a8
            org.jivesoftware.smack.proxy.ProxyException r0 = new org.jivesoftware.smack.proxy.ProxyException     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r2 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            java.lang.String r3 = "stream is closed"
            r0.<init>(r2, r3)     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            throw r0     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
        L_0x0096:
            r0 = move-exception
        L_0x0097:
            if (r1 == 0) goto L_0x009c
            r1.close()     // Catch:{ Exception -> 0x0100 }
        L_0x009c:
            org.jivesoftware.smack.proxy.ProxyException r1 = new org.jivesoftware.smack.proxy.ProxyException
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r2 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4
            java.lang.String r0 = r0.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x00a8:
            int r0 = r0 + r3
            goto L_0x0082
        L_0x00aa:
            r0 = 0
            byte r0 = r8[r0]     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            if (r0 == 0) goto L_0x00cd
            org.jivesoftware.smack.proxy.ProxyException r0 = new org.jivesoftware.smack.proxy.ProxyException     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r2 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r3.<init>()     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            java.lang.String r4 = "server returns VN "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r4 = 0
            byte r4 = r8[r4]     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            java.lang.String r3 = r3.toString()     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r0.<init>(r2, r3)     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            throw r0     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
        L_0x00cd:
            r0 = 1
            byte r0 = r8[r0]     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r2 = 90
            if (r0 == r2) goto L_0x00f5
            r1.close()     // Catch:{ Exception -> 0x00fe, RuntimeException -> 0x0063 }
        L_0x00d7:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r0.<init>()     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            java.lang.String r2 = "ProxySOCKS4: server returns CD "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r2 = 1
            byte r2 = r8[r2]     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            java.lang.String r0 = r0.toString()     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            org.jivesoftware.smack.proxy.ProxyException r2 = new org.jivesoftware.smack.proxy.ProxyException     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            org.jivesoftware.smack.proxy.ProxyInfo$ProxyType r3 = org.jivesoftware.smack.proxy.ProxyInfo.ProxyType.SOCKS4     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r2.<init>(r3, r0)     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            throw r2     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
        L_0x00f5:
            r0 = 2
            byte[] r0 = new byte[r0]     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            r2 = 0
            r3 = 2
            r5.read(r0, r2, r3)     // Catch:{ RuntimeException -> 0x0063, Exception -> 0x0096 }
            return r1
        L_0x00fe:
            r0 = move-exception
            goto L_0x00d7
        L_0x0100:
            r1 = move-exception
            goto L_0x009c
        L_0x0102:
            r0 = move-exception
            r1 = r2
            goto L_0x0097
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smack.proxy.Socks4ProxySocketFactory.socks4ProxifiedSocket(java.lang.String, int):java.net.Socket");
    }
}
