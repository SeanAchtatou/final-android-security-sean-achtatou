package org.jivesoftware.smack;

import java.util.Random;
import java.util.logging.Logger;
import org.jivesoftware.smack.XMPPException;

public class ReconnectionManager extends AbstractConnectionListener {
    /* access modifiers changed from: private */
    public static final Logger LOGGER = Logger.getLogger(ReconnectionManager.class.getName());
    /* access modifiers changed from: private */
    public XMPPConnection connection;
    boolean done;
    /* access modifiers changed from: private */
    public int randomBase;
    private Thread reconnectionThread;

    static {
        XMPPConnection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(XMPPConnection xMPPConnection) {
                xMPPConnection.addConnectionListener(new ReconnectionManager(xMPPConnection));
            }
        });
    }

    private ReconnectionManager(XMPPConnection xMPPConnection) {
        this.randomBase = new Random().nextInt(11) + 5;
        this.done = false;
        this.connection = xMPPConnection;
    }

    /* access modifiers changed from: private */
    public boolean isReconnectionAllowed() {
        return !this.done && !this.connection.isConnected() && this.connection.getConfiguration().isReconnectionAllowed();
    }

    /* access modifiers changed from: protected */
    public synchronized void reconnect() {
        if (isReconnectionAllowed() && (this.reconnectionThread == null || !this.reconnectionThread.isAlive())) {
            this.reconnectionThread = new Thread() {
                private int attempts = 0;

                private int timeDelay() {
                    this.attempts++;
                    if (this.attempts > 13) {
                        return ReconnectionManager.this.randomBase * 6 * 5;
                    }
                    if (this.attempts > 7) {
                        return ReconnectionManager.this.randomBase * 6;
                    }
                    return ReconnectionManager.this.randomBase;
                }

                public void run() {
                    while (ReconnectionManager.this.isReconnectionAllowed()) {
                        int timeDelay = timeDelay();
                        while (ReconnectionManager.this.isReconnectionAllowed() && timeDelay > 0) {
                            try {
                                Thread.sleep(1000);
                                timeDelay--;
                                ReconnectionManager.this.notifyAttemptToReconnectIn(timeDelay);
                            } catch (InterruptedException e) {
                                ReconnectionManager.LOGGER.warning("Sleeping thread interrupted");
                                ReconnectionManager.this.notifyReconnectionFailed(e);
                            }
                        }
                        try {
                            if (ReconnectionManager.this.isReconnectionAllowed()) {
                                ReconnectionManager.this.connection.connect();
                            }
                        } catch (Exception e2) {
                            ReconnectionManager.this.notifyReconnectionFailed(e2);
                        }
                    }
                }
            };
            this.reconnectionThread.setName("Smack Reconnection Manager");
            this.reconnectionThread.setDaemon(true);
            this.reconnectionThread.start();
        }
    }

    /* access modifiers changed from: protected */
    public void notifyReconnectionFailed(Exception exc) {
        if (isReconnectionAllowed()) {
            for (ConnectionListener reconnectionFailed : this.connection.connectionListeners) {
                reconnectionFailed.reconnectionFailed(exc);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void notifyAttemptToReconnectIn(int i) {
        if (isReconnectionAllowed()) {
            for (ConnectionListener reconnectingIn : this.connection.connectionListeners) {
                reconnectingIn.reconnectingIn(i);
            }
        }
    }

    public void connectionClosed() {
        this.done = true;
    }

    public void connectionClosedOnError(Exception exc) {
        this.done = false;
        if ((!(exc instanceof XMPPException.StreamErrorException) || !"conflict".equals(((XMPPException.StreamErrorException) exc).getStreamError().getCode())) && isReconnectionAllowed()) {
            reconnect();
        }
    }
}
