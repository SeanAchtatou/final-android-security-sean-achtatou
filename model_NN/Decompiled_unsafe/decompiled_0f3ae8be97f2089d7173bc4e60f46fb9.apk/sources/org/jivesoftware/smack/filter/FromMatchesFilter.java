package org.jivesoftware.smack.filter;

import java.util.Locale;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.StringUtils;

public class FromMatchesFilter implements PacketFilter {
    private String address;
    private boolean matchBareJID = false;

    public FromMatchesFilter(String str, boolean z) {
        this.address = str == null ? null : str.toLowerCase(Locale.US);
        this.matchBareJID = z;
    }

    public static FromMatchesFilter create(String str) {
        return new FromMatchesFilter(str, "".equals(StringUtils.parseResource(str)));
    }

    public static FromMatchesFilter createBare(String str) {
        return new FromMatchesFilter(str == null ? null : StringUtils.parseBareAddress(str), true);
    }

    public static FromMatchesFilter createFull(String str) {
        return new FromMatchesFilter(str, false);
    }

    public boolean accept(Packet packet) {
        String from = packet.getFrom();
        if (from == null) {
            return this.address == null;
        }
        String lowerCase = from.toLowerCase(Locale.US);
        if (this.matchBareJID) {
            lowerCase = StringUtils.parseBareAddress(lowerCase);
        }
        return lowerCase.equals(this.address);
    }

    public String toString() {
        return "FromMatchesFilter (" + (this.matchBareJID ? "bare" : "full") + "): " + this.address;
    }
}
