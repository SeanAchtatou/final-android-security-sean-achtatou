package org.jivesoftware.smack.filter;

import org.jivesoftware.smack.packet.Packet;

public class PacketIDFilter implements PacketFilter {
    private String packetID;

    public PacketIDFilter(Packet packet) {
        this(packet.getPacketID());
    }

    public PacketIDFilter(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Packet ID must not be null.");
        }
        this.packetID = str;
    }

    public boolean accept(Packet packet) {
        return this.packetID.equals(packet.getPacketID());
    }

    public String toString() {
        return "PacketIDFilter by id: " + this.packetID;
    }
}
