package org.jivesoftware.smack.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.jivesoftware.smack.packet.Packet;

public class OrFilter implements PacketFilter {
    private final List<PacketFilter> filters;

    public OrFilter() {
        this.filters = new ArrayList();
    }

    public OrFilter(PacketFilter... packetFilterArr) {
        if (packetFilterArr == null) {
            throw new IllegalArgumentException("Parameter must not be null.");
        }
        for (PacketFilter packetFilter : packetFilterArr) {
            if (packetFilter == null) {
                throw new IllegalArgumentException("Parameter must not be null.");
            }
        }
        this.filters = new ArrayList(Arrays.asList(packetFilterArr));
    }

    public void addFilter(PacketFilter packetFilter) {
        if (packetFilter == null) {
            throw new IllegalArgumentException("Parameter must not be null.");
        }
        this.filters.add(packetFilter);
    }

    public boolean accept(Packet packet) {
        for (PacketFilter accept : this.filters) {
            if (accept.accept(packet)) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        return this.filters.toString();
    }
}
