package org.jivesoftware.smack.filter;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.StringUtils;

public class IQReplyFilter implements PacketFilter {
    private static final Logger LOGGER = Logger.getLogger(IQReplyFilter.class.getName());
    private final OrFilter fromFilter;
    private final PacketFilter iqAndIdFilter;
    private final String local;
    private final String packetId;
    private final String server;
    private final String to;

    public IQReplyFilter(IQ iq, XMPPConnection xMPPConnection) {
        this.to = iq.getTo();
        if (xMPPConnection.getUser() == null) {
            this.local = null;
        } else {
            this.local = xMPPConnection.getUser().toLowerCase(Locale.US);
        }
        this.server = xMPPConnection.getServiceName().toLowerCase(Locale.US);
        this.packetId = iq.getPacketID();
        this.iqAndIdFilter = new AndFilter(new OrFilter(new IQTypeFilter(IQ.Type.ERROR), new IQTypeFilter(IQ.Type.RESULT)), new PacketIDFilter(iq));
        this.fromFilter = new OrFilter();
        this.fromFilter.addFilter(FromMatchesFilter.createFull(this.to));
        if (this.to == null) {
            if (this.local != null) {
                this.fromFilter.addFilter(FromMatchesFilter.createBare(this.local));
            }
            this.fromFilter.addFilter(FromMatchesFilter.createFull(this.server));
        } else if (this.local != null && this.to.toLowerCase(Locale.US).equals(StringUtils.parseBareAddress(this.local))) {
            this.fromFilter.addFilter(FromMatchesFilter.createFull(null));
        }
    }

    public boolean accept(Packet packet) {
        if (!this.iqAndIdFilter.accept(packet)) {
            return false;
        }
        if (this.fromFilter.accept(packet)) {
            return true;
        }
        LOGGER.log(Level.WARNING, String.format("Rejected potentially spoofed reply to IQ-packet. Filter settings: packetId=%s, to=%s, local=%s, server=%s. Received packet with from=%s", this.packetId, this.to, this.local, this.server, packet.getFrom()), packet);
        return false;
    }
}
