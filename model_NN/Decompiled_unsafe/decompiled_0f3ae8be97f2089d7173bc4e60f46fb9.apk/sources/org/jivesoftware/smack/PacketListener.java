package org.jivesoftware.smack;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.packet.Packet;

public interface PacketListener {
    void processPacket(Packet packet) throws SmackException.NotConnectedException;
}
