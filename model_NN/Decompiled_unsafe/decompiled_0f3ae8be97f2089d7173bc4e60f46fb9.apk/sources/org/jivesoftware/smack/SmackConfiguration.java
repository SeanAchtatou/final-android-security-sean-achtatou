package org.jivesoftware.smack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HostnameVerifier;
import org.jivesoftware.smack.compression.Java7ZlibInputOutputStream;
import org.jivesoftware.smack.compression.XMPPInputOutputStream;
import org.jivesoftware.smack.initializer.SmackInitializer;
import org.jivesoftware.smack.parsing.ExceptionThrowingCallback;
import org.jivesoftware.smack.parsing.ParsingExceptionCallback;
import org.jivesoftware.smack.util.DNSUtil;
import org.jivesoftware.smack.util.FileUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public final class SmackConfiguration {
    public static boolean DEBUG_ENABLED = false;
    private static final String DEFAULT_CONFIG_FILE = "classpath:org.jivesoftware.smack/smack-config.xml";
    private static final Logger LOGGER = Logger.getLogger(SmackConfiguration.class.getName());
    private static final String SMACK_VERSION;
    private static final List<XMPPInputOutputStream> compressionHandlers = new ArrayList(2);
    private static ParsingExceptionCallback defaultCallback = new ExceptionThrowingCallback();
    private static HostnameVerifier defaultHostnameVerififer;
    private static List<String> defaultMechs = new ArrayList();
    private static int defaultPacketReplyTimeout = 5000;
    private static Set<String> disabledSmackClasses = new HashSet();
    private static int packetCollectorSize = 5000;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    static {
        String str;
        DEBUG_ENABLED = false;
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(FileUtils.getStreamForUrl("classpath:org.jivesoftware.smack/version", null)));
            str = bufferedReader.readLine();
            try {
                bufferedReader.close();
            } catch (IOException e) {
                LOGGER.log(Level.WARNING, "IOException closing stream", (Throwable) e);
            }
        } catch (Exception e2) {
            LOGGER.log(Level.SEVERE, "Could not determine Smack version", (Throwable) e2);
            str = "unkown";
        }
        SMACK_VERSION = str;
        String property = System.getProperty("smack.disabledClasses");
        if (property != null) {
            for (String add : property.split(",")) {
                disabledSmackClasses.add(add);
            }
        }
        try {
            FileUtils.addLines("classpath:org.jivesoftware.smack/disabledClasses", disabledSmackClasses);
            try {
                String[] strArr = (String[]) Class.forName("org.jivesoftware.smack.CustomSmackConfiguration").getField("DISABLED_SMACK_CLASSES").get(null);
                if (strArr != null) {
                    for (String add2 : strArr) {
                        disabledSmackClasses.add(add2);
                    }
                }
            } catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException e3) {
            }
            try {
                try {
                    processConfigFile(FileUtils.getStreamForUrl(DEFAULT_CONFIG_FILE, null), null);
                    compressionHandlers.add(new Java7ZlibInputOutputStream());
                    try {
                        DEBUG_ENABLED = Boolean.getBoolean("smack.debugEnabled");
                    } catch (Exception e4) {
                    }
                    DNSUtil.init();
                } catch (Exception e5) {
                    throw new IllegalStateException(e5);
                }
            } catch (Exception e6) {
                throw new IllegalStateException(e6);
            }
        } catch (Exception e7) {
            throw new IllegalStateException(e7);
        }
    }

    public static String getVersion() {
        return SMACK_VERSION;
    }

    public static int getDefaultPacketReplyTimeout() {
        if (defaultPacketReplyTimeout <= 0) {
            defaultPacketReplyTimeout = 5000;
        }
        return defaultPacketReplyTimeout;
    }

    public static void setDefaultPacketReplyTimeout(int i) {
        if (i <= 0) {
            throw new IllegalArgumentException();
        }
        defaultPacketReplyTimeout = i;
    }

    public static int getPacketCollectorSize() {
        return packetCollectorSize;
    }

    public static void setPacketCollectorSize(int i) {
        packetCollectorSize = i;
    }

    public static void addSaslMech(String str) {
        if (!defaultMechs.contains(str)) {
            defaultMechs.add(str);
        }
    }

    public static void addSaslMechs(Collection<String> collection) {
        for (String addSaslMech : collection) {
            addSaslMech(addSaslMech);
        }
    }

    public static void removeSaslMech(String str) {
        defaultMechs.remove(str);
    }

    public static void removeSaslMechs(Collection<String> collection) {
        defaultMechs.removeAll(collection);
    }

    public static List<String> getSaslMechs() {
        return Collections.unmodifiableList(defaultMechs);
    }

    public static void setDefaultParsingExceptionCallback(ParsingExceptionCallback parsingExceptionCallback) {
        defaultCallback = parsingExceptionCallback;
    }

    public static ParsingExceptionCallback getDefaultParsingExceptionCallback() {
        return defaultCallback;
    }

    public static void addCompressionHandler(XMPPInputOutputStream xMPPInputOutputStream) {
        compressionHandlers.add(xMPPInputOutputStream);
    }

    public static List<XMPPInputOutputStream> getCompresionHandlers() {
        ArrayList arrayList = new ArrayList(compressionHandlers.size());
        for (XMPPInputOutputStream next : compressionHandlers) {
            if (next.isSupported()) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public static void setDefaultHostnameVerifier(HostnameVerifier hostnameVerifier) {
        defaultHostnameVerififer = hostnameVerifier;
    }

    static HostnameVerifier getDefaultHostnameVerifier() {
        return defaultHostnameVerififer;
    }

    public static void processConfigFile(InputStream inputStream, Collection<Exception> collection) throws Exception {
        processConfigFile(inputStream, collection, SmackConfiguration.class.getClassLoader());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public static void processConfigFile(InputStream inputStream, Collection<Exception> collection, ClassLoader classLoader) throws Exception {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
        newPullParser.setInput(inputStream, "UTF-8");
        int eventType = newPullParser.getEventType();
        do {
            if (eventType == 2) {
                if (newPullParser.getName().equals("startupClasses")) {
                    parseClassesToLoad(newPullParser, false, collection, classLoader);
                } else if (newPullParser.getName().equals("optionalStartupClasses")) {
                    parseClassesToLoad(newPullParser, true, collection, classLoader);
                }
            }
            eventType = newPullParser.next();
        } while (eventType != 1);
        try {
            inputStream.close();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error while closing config file input stream", (Throwable) e);
        }
    }

    private static void parseClassesToLoad(XmlPullParser xmlPullParser, boolean z, Collection<Exception> collection, ClassLoader classLoader) throws XmlPullParserException, IOException, Exception {
        String name = xmlPullParser.getName();
        while (true) {
            int next = xmlPullParser.next();
            String name2 = xmlPullParser.getName();
            if (next == 2 && "className".equals(name2)) {
                String nextText = xmlPullParser.nextText();
                if (disabledSmackClasses.contains(nextText)) {
                    LOGGER.info("Not loading disabled Smack class " + nextText);
                } else {
                    try {
                        loadSmackClass(nextText, z, classLoader);
                    } catch (Exception e) {
                        if (collection != null) {
                            collection.add(e);
                        } else {
                            throw e;
                        }
                    }
                }
            }
            if (next == 3 && name.equals(name2)) {
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private static void loadSmackClass(String str, boolean z, ClassLoader classLoader) throws Exception {
        Level level;
        try {
            Class<?> cls = Class.forName(str, true, classLoader);
            if (SmackInitializer.class.isAssignableFrom(cls)) {
                List<Exception> initialize = ((SmackInitializer) cls.newInstance()).initialize();
                if (initialize.size() == 0) {
                    LOGGER.log(Level.FINE, "Loaded SmackInitializer " + str);
                    return;
                }
                for (Exception exc : initialize) {
                    LOGGER.log(Level.SEVERE, "Exception in loadSmackClass", (Throwable) exc);
                }
                return;
            }
            LOGGER.log(Level.FINE, "Loaded " + str);
        } catch (ClassNotFoundException e) {
            if (z) {
                level = Level.FINE;
            } else {
                level = Level.WARNING;
            }
            LOGGER.log(level, "A startup class '" + str + "' could not be loaded.");
            if (!z) {
                throw e;
            }
        }
    }
}
