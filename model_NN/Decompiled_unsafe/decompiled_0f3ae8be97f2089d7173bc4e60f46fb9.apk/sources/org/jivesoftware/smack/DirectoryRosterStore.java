package org.jivesoftware.smack;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.packet.RosterPacket;
import org.jivesoftware.smack.util.Base32Encoder;
import org.jivesoftware.smack.util.FileUtils;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jivesoftware.smackx.xdata.packet.DataForm;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class DirectoryRosterStore implements RosterStore {
    private static final String ENTRY_PREFIX = "entry-";
    private static final Logger LOGGER = Logger.getLogger(DirectoryRosterStore.class.getName());
    private static final String STORE_ID = "DEFAULT_ROSTER_STORE";
    private static final String VERSION_FILE_NAME = "__version__";
    private static final FileFilter rosterDirFilter = new FileFilter() {
        public boolean accept(File file) {
            return file.getName().startsWith(DirectoryRosterStore.ENTRY_PREFIX);
        }
    };
    private final File fileDir;

    private DirectoryRosterStore(File file) {
        this.fileDir = file;
    }

    public static DirectoryRosterStore init(File file) {
        DirectoryRosterStore directoryRosterStore = new DirectoryRosterStore(file);
        if (directoryRosterStore.setRosterVersion("")) {
            return directoryRosterStore;
        }
        return null;
    }

    public static DirectoryRosterStore open(File file) {
        DirectoryRosterStore directoryRosterStore = new DirectoryRosterStore(file);
        String readFile = FileUtils.readFile(directoryRosterStore.getVersionFile());
        if (readFile == null || !readFile.startsWith("DEFAULT_ROSTER_STORE\n")) {
            return null;
        }
        return directoryRosterStore;
    }

    private File getVersionFile() {
        return new File(this.fileDir, VERSION_FILE_NAME);
    }

    public List<RosterPacket.Item> getEntries() {
        ArrayList arrayList = new ArrayList();
        for (File file : this.fileDir.listFiles(rosterDirFilter)) {
            RosterPacket.Item readEntry = readEntry(file);
            if (readEntry == null) {
                log("Roster store file '" + file + "' is invalid.");
            } else {
                arrayList.add(readEntry);
            }
        }
        return arrayList;
    }

    public RosterPacket.Item getEntry(String str) {
        return readEntry(getBareJidFile(str));
    }

    public String getRosterVersion() {
        String readFile = FileUtils.readFile(getVersionFile());
        if (readFile == null) {
            return null;
        }
        String[] split = readFile.split("\n", 2);
        if (split.length >= 2) {
            return split[1];
        }
        return null;
    }

    private boolean setRosterVersion(String str) {
        return FileUtils.writeFile(getVersionFile(), "DEFAULT_ROSTER_STORE\n" + str);
    }

    public boolean addEntry(RosterPacket.Item item, String str) {
        return addEntryRaw(item) && setRosterVersion(str);
    }

    public boolean removeEntry(String str, String str2) {
        return getBareJidFile(str).delete() && setRosterVersion(str2);
    }

    public boolean resetEntries(Collection<RosterPacket.Item> collection, String str) {
        for (File delete : this.fileDir.listFiles(rosterDirFilter)) {
            delete.delete();
        }
        for (RosterPacket.Item addEntryRaw : collection) {
            if (!addEntryRaw(addEntryRaw)) {
                return false;
            }
        }
        return setRosterVersion(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, org.xmlpull.v1.XmlPullParserException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private RosterPacket.Item readEntry(File file) {
        String readFile = FileUtils.readFile(file);
        if (readFile == null) {
            return null;
        }
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(new StringReader(readFile));
            boolean z = false;
            String str = null;
            String str2 = null;
            String str3 = null;
            String str4 = null;
            while (!z) {
                int next = newPullParser.next();
                String name = newPullParser.getName();
                if (next == 2) {
                    if (name.equals(DataForm.Item.ELEMENT)) {
                        str = null;
                        str2 = null;
                        str3 = null;
                        str4 = null;
                    } else if (name.equals("user")) {
                        newPullParser.next();
                        str4 = newPullParser.getText();
                    } else if (name.equals("name")) {
                        newPullParser.next();
                        str3 = newPullParser.getText();
                    } else if (name.equals("type")) {
                        newPullParser.next();
                        str2 = newPullParser.getText();
                    } else if (name.equals("status")) {
                        newPullParser.next();
                        str = newPullParser.getText();
                    } else if (name.equals("group")) {
                        newPullParser.next();
                        newPullParser.next();
                        String text = newPullParser.getText();
                        if (text != null) {
                            arrayList.add(text);
                        } else {
                            log("Invalid group entry in store entry file " + file);
                        }
                    }
                } else if (next == 3) {
                    if (name.equals(DataForm.Item.ELEMENT)) {
                        z = true;
                    }
                }
            }
            if (str4 == null) {
                return null;
            }
            RosterPacket.Item item = new RosterPacket.Item(str4, str3);
            for (String addGroupName : arrayList) {
                item.addGroupName(addGroupName);
            }
            if (str2 != null) {
                try {
                    item.setItemType(RosterPacket.ItemType.valueOf(str2));
                    if (str != null) {
                        RosterPacket.ItemStatus fromString = RosterPacket.ItemStatus.fromString(str);
                        if (fromString == null) {
                            log("Invalid status in store entry file " + file);
                            return null;
                        }
                        item.setItemStatus(fromString);
                    }
                } catch (IllegalArgumentException e) {
                    log("Invalid type in store entry file " + file);
                    return null;
                }
            }
            return item;
        } catch (IOException e2) {
            LOGGER.log(Level.SEVERE, "readEntry()", (Throwable) e2);
            return null;
        } catch (XmlPullParserException e3) {
            log("Invalid group entry in store entry file " + file);
            LOGGER.log(Level.SEVERE, "readEntry()", (Throwable) e3);
            return null;
        }
    }

    private boolean addEntryRaw(RosterPacket.Item item) {
        XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
        xmlStringBuilder.openElement(DataForm.Item.ELEMENT);
        xmlStringBuilder.element("user", item.getUser());
        xmlStringBuilder.optElement("name", item.getName());
        xmlStringBuilder.optElement("type", item.getItemType());
        xmlStringBuilder.optElement("status", item.getItemStatus());
        for (String element : item.getGroupNames()) {
            xmlStringBuilder.openElement("group");
            xmlStringBuilder.element("groupName", element);
            xmlStringBuilder.closeElement("group");
        }
        xmlStringBuilder.closeElement(DataForm.Item.ELEMENT);
        return FileUtils.writeFile(getBareJidFile(item.getUser()), xmlStringBuilder.toString());
    }

    private File getBareJidFile(String str) {
        return new File(this.fileDir, ENTRY_PREFIX + Base32Encoder.getInstance().encode(str));
    }

    private void log(String str) {
        System.err.println(str);
    }
}
