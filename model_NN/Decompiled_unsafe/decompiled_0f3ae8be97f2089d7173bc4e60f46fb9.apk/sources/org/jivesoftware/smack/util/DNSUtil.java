package org.jivesoftware.smack.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.util.dns.DNSResolver;
import org.jivesoftware.smack.util.dns.HostAddress;
import org.jivesoftware.smack.util.dns.SRVRecord;

public class DNSUtil {
    private static final Logger LOGGER = Logger.getLogger(DNSUtil.class.getName());
    private static DNSResolver dnsResolver = null;

    public static void init() {
        String[] strArr = {"javax.JavaxResolver", "minidns.MiniDnsResolver", "dnsjava.DNSJavaResolver"};
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            try {
                DNSResolver dNSResolver = (DNSResolver) Class.forName("org.jivesoftware.smack.util.dns" + strArr[i]).getMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
                if (dNSResolver != null) {
                    setDNSResolver(dNSResolver);
                    return;
                }
                i++;
            } catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
                LOGGER.log(Level.FINE, "Exception on init", e);
            }
        }
    }

    public static void setDNSResolver(DNSResolver dNSResolver) {
        dnsResolver = dNSResolver;
    }

    public static DNSResolver getDNSResolver() {
        return dnsResolver;
    }

    public static List<HostAddress> resolveXMPPDomain(String str) {
        if (dnsResolver != null) {
            return resolveDomain(str, 'c');
        }
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(new HostAddress(str, 5222));
        return arrayList;
    }

    public static List<HostAddress> resolveXMPPServerDomain(String str) {
        if (dnsResolver != null) {
            return resolveDomain(str, 's');
        }
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(new HostAddress(str, 5269));
        return arrayList;
    }

    private static List<HostAddress> resolveDomain(String str, char c) {
        String str2;
        String str3;
        ArrayList arrayList = new ArrayList();
        if (c == 's') {
            str2 = "_xmpp-server._tcp." + str;
        } else {
            str2 = c == 'c' ? "_xmpp-client._tcp." + str : str;
        }
        try {
            List<SRVRecord> lookupSRVRecords = dnsResolver.lookupSRVRecords(str2);
            if (LOGGER.isLoggable(Level.FINE)) {
                String str4 = "Resolved SRV RR for " + str2 + ":";
                Iterator<SRVRecord> it = lookupSRVRecords.iterator();
                while (true) {
                    str3 = str4;
                    if (!it.hasNext()) {
                        break;
                    }
                    str4 = str3 + " " + it.next();
                }
                LOGGER.fine(str3);
            }
            arrayList.addAll(sortSRVRecords(lookupSRVRecords));
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Exception while resovling SRV records for " + str + ". Consider adding '_xmpp-(server|client)._tcp' DNS SRV Records");
        }
        arrayList.add(new HostAddress(str));
        return arrayList;
    }

    private static List<HostAddress> sortSRVRecords(List<SRVRecord> list) {
        int bisect;
        int i;
        if (list.size() == 1 && list.get(0).getFQDN().equals(".")) {
            return Collections.emptyList();
        }
        Collections.sort(list);
        TreeMap treeMap = new TreeMap();
        for (SRVRecord next : list) {
            Integer valueOf = Integer.valueOf(next.getPriority());
            Object obj = (List) treeMap.get(valueOf);
            if (obj == null) {
                obj = new LinkedList();
                treeMap.put(valueOf, obj);
            }
            obj.add(next);
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (Integer num : treeMap.keySet()) {
            List<SRVRecord> list2 = (List) treeMap.get(num);
            while (true) {
                int size = list2.size();
                if (size > 0) {
                    int[] iArr = new int[list2.size()];
                    int i2 = 1;
                    for (SRVRecord weight : list2) {
                        if (weight.getWeight() > 0) {
                            i = 0;
                        } else {
                            i = i2;
                        }
                        i2 = i;
                    }
                    int i3 = 0;
                    int i4 = 0;
                    for (SRVRecord weight2 : list2) {
                        i4 += weight2.getWeight() + i2;
                        iArr[i3] = i4;
                        i3++;
                    }
                    if (i4 == 0) {
                        bisect = (int) (Math.random() * ((double) size));
                    } else {
                        bisect = bisect(iArr, Math.random() * ((double) i4));
                    }
                    arrayList.add((SRVRecord) list2.remove(bisect));
                }
            }
        }
        return arrayList;
    }

    private static int bisect(int[] iArr, double d) {
        int i = 0;
        int length = iArr.length;
        int i2 = 0;
        while (i < length && d >= ((double) iArr[i])) {
            i2++;
            i++;
        }
        return i2;
    }
}
