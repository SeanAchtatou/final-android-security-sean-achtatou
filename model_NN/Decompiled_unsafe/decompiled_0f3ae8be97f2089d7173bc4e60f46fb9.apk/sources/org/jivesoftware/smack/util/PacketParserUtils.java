package org.jivesoftware.smack.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Bind;
import org.jivesoftware.smack.packet.DefaultPacketExtension;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Registration;
import org.jivesoftware.smack.packet.RosterPacket;
import org.jivesoftware.smack.packet.StreamError;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.sasl.SASLMechanism;
import org.jivesoftware.smackx.privacy.packet.PrivacyItem;
import org.jivesoftware.smackx.xdata.FormField;
import org.jivesoftware.smackx.xdata.packet.DataForm;
import org.jivesoftware.smackx.xhtmlim.provider.XHTMLExtensionProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class PacketParserUtils {
    static final /* synthetic */ boolean $assertionsDisabled = (!PacketParserUtils.class.desiredAssertionStatus());
    private static final Logger LOGGER = Logger.getLogger(PacketParserUtils.class.getName());

    public static XmlPullParser newXmppParser() throws XmlPullParserException {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
        return newPullParser;
    }

    public static Message parseMessage(XmlPullParser xmlPullParser) throws Exception {
        Message message = new Message();
        String attributeValue = xmlPullParser.getAttributeValue("", "id");
        if (attributeValue == null) {
            attributeValue = Packet.ID_NOT_AVAILABLE;
        }
        message.setPacketID(attributeValue);
        message.setTo(xmlPullParser.getAttributeValue("", "to"));
        message.setFrom(xmlPullParser.getAttributeValue("", PrivacyItem.SUBSCRIPTION_FROM));
        message.setType(Message.Type.fromString(xmlPullParser.getAttributeValue("", "type")));
        String languageAttribute = getLanguageAttribute(xmlPullParser);
        if (languageAttribute == null || "".equals(languageAttribute.trim())) {
            languageAttribute = Packet.getDefaultLanguage();
        } else {
            message.setLanguage(languageAttribute);
        }
        boolean z = false;
        String str = null;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                String name = xmlPullParser.getName();
                String namespace = xmlPullParser.getNamespace();
                if (name.equals("subject")) {
                    String languageAttribute2 = getLanguageAttribute(xmlPullParser);
                    if (languageAttribute2 == null) {
                        languageAttribute2 = languageAttribute;
                    }
                    String parseElementText = parseElementText(xmlPullParser);
                    if (message.getSubject(languageAttribute2) == null) {
                        message.addSubject(languageAttribute2, parseElementText);
                    }
                } else if (name.equals(XHTMLExtensionProvider.BODY_ELEMENT)) {
                    String languageAttribute3 = getLanguageAttribute(xmlPullParser);
                    if (languageAttribute3 == null) {
                        languageAttribute3 = languageAttribute;
                    }
                    String parseElementText2 = parseElementText(xmlPullParser);
                    if (message.getBody(languageAttribute3) == null) {
                        message.addBody(languageAttribute3, parseElementText2);
                    }
                } else if (name.equals("thread")) {
                    if (str == null) {
                        str = xmlPullParser.nextText();
                    }
                } else if (name.equals("error")) {
                    message.setError(parseError(xmlPullParser));
                } else {
                    message.addExtension(parsePacketExtension(name, namespace, xmlPullParser));
                }
            } else if (next == 3 && xmlPullParser.getName().equals("message")) {
                z = true;
            }
        }
        message.setThread(str);
        return message;
    }

    public static String parseElementText(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        if (!$assertionsDisabled && xmlPullParser.getEventType() != 2) {
            throw new AssertionError();
        } else if (xmlPullParser.isEmptyElementTag()) {
            return "";
        } else {
            int next = xmlPullParser.next();
            if (next == 4) {
                String text = xmlPullParser.getText();
                if (xmlPullParser.next() == 3) {
                    return text;
                }
                throw new XmlPullParserException("Non-empty element tag contains child-elements, while Mixed Content (XML 3.2.2) is disallowed");
            } else if (next == 3) {
                return "";
            } else {
                throw new XmlPullParserException("Non-empty element tag not followed by text, while Mixed Content (XML 3.2.2) is disallowed");
            }
        }
    }

    public static String parseElement(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        return parseElement(xmlPullParser, false);
    }

    public static String parseElement(XmlPullParser xmlPullParser, boolean z) throws XmlPullParserException, IOException {
        if ($assertionsDisabled || xmlPullParser.getEventType() == 2) {
            return parseContentDepth(xmlPullParser, xmlPullParser.getDepth(), z);
        }
        throw new AssertionError();
    }

    public static String parseContent(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        if (!$assertionsDisabled && xmlPullParser.getEventType() != 2) {
            throw new AssertionError();
        } else if (xmlPullParser.isEmptyElementTag()) {
            return "";
        } else {
            xmlPullParser.next();
            return parseContentDepth(xmlPullParser, xmlPullParser.getDepth(), false);
        }
    }

    public static String parseContentDepth(XmlPullParser xmlPullParser, int i) throws XmlPullParserException, IOException {
        return parseContentDepth(xmlPullParser, i, false);
    }

    public static String parseContentDepth(XmlPullParser xmlPullParser, int i, boolean z) throws XmlPullParserException, IOException {
        boolean z2;
        XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
        boolean z3 = false;
        int eventType = xmlPullParser.getEventType();
        String str = null;
        while (true) {
            if (eventType == 2) {
                xmlStringBuilder.halfOpenElement(xmlPullParser.getName());
                if (str == null || z) {
                    String namespace = xmlPullParser.getNamespace();
                    if (StringUtils.isNotEmpty(namespace)) {
                        xmlStringBuilder.attribute("xmlns", namespace);
                        str = xmlPullParser.getName();
                    }
                }
                for (int i2 = 0; i2 < xmlPullParser.getAttributeCount(); i2++) {
                    xmlStringBuilder.attribute(xmlPullParser.getAttributeName(i2), xmlPullParser.getAttributeValue(i2));
                }
                if (xmlPullParser.isEmptyElementTag()) {
                    xmlStringBuilder.closeEmptyElement();
                    z2 = true;
                } else {
                    xmlStringBuilder.rightAngelBracket();
                    z2 = z3;
                }
            } else if (eventType == 3) {
                if (z3) {
                    z2 = false;
                } else {
                    xmlStringBuilder.closeElement(xmlPullParser.getName());
                    z2 = z3;
                }
                if (str != null && str.equals(xmlPullParser.getName())) {
                    str = null;
                }
                if (xmlPullParser.getDepth() <= i) {
                    return xmlStringBuilder.toString();
                }
            } else {
                if (eventType == 4) {
                    xmlStringBuilder.append((CharSequence) xmlPullParser.getText());
                }
                z2 = z3;
            }
            boolean z4 = z2;
            eventType = xmlPullParser.next();
            z3 = z4;
        }
    }

    public static Presence parsePresence(XmlPullParser xmlPullParser) throws Exception {
        String str;
        Presence.Type type = Presence.Type.available;
        String attributeValue = xmlPullParser.getAttributeValue("", "type");
        if (attributeValue != null && !attributeValue.equals("")) {
            try {
                type = Presence.Type.valueOf(attributeValue);
            } catch (IllegalArgumentException e) {
                LOGGER.warning("Found invalid presence type " + attributeValue);
            }
        }
        Presence presence = new Presence(type);
        presence.setTo(xmlPullParser.getAttributeValue("", "to"));
        presence.setFrom(xmlPullParser.getAttributeValue("", PrivacyItem.SUBSCRIPTION_FROM));
        String attributeValue2 = xmlPullParser.getAttributeValue("", "id");
        if (attributeValue2 == null) {
            str = Packet.ID_NOT_AVAILABLE;
        } else {
            str = attributeValue2;
        }
        presence.setPacketID(str);
        String languageAttribute = getLanguageAttribute(xmlPullParser);
        if (languageAttribute != null && !"".equals(languageAttribute.trim())) {
            presence.setLanguage(languageAttribute);
        }
        if (attributeValue2 == null) {
            attributeValue2 = Packet.ID_NOT_AVAILABLE;
        }
        presence.setPacketID(attributeValue2);
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                String name = xmlPullParser.getName();
                String namespace = xmlPullParser.getNamespace();
                if (name.equals("status")) {
                    presence.setStatus(xmlPullParser.nextText());
                } else if (name.equals("priority")) {
                    try {
                        presence.setPriority(Integer.parseInt(xmlPullParser.nextText()));
                    } catch (NumberFormatException e2) {
                    } catch (IllegalArgumentException e3) {
                        presence.setPriority(0);
                    }
                } else if (name.equals("show")) {
                    String nextText = xmlPullParser.nextText();
                    try {
                        presence.setMode(Presence.Mode.valueOf(nextText));
                    } catch (IllegalArgumentException e4) {
                        LOGGER.warning("Found invalid presence mode " + nextText);
                    }
                } else if (name.equals("error")) {
                    presence.setError(parseError(xmlPullParser));
                } else {
                    try {
                        presence.addExtension(parsePacketExtension(name, namespace, xmlPullParser));
                    } catch (Exception e5) {
                        LOGGER.warning("Failed to parse extension packet in Presence packet.");
                    }
                }
            } else if (next == 3 && xmlPullParser.getName().equals("presence")) {
                z = true;
            }
        }
        return presence;
    }

    public static IQ parseIQ(XmlPullParser xmlPullParser, XMPPConnection xMPPConnection) throws Exception {
        boolean z;
        XMPPError xMPPError;
        IQ iq;
        String attributeValue = xmlPullParser.getAttributeValue("", "id");
        String attributeValue2 = xmlPullParser.getAttributeValue("", "to");
        String attributeValue3 = xmlPullParser.getAttributeValue("", PrivacyItem.SUBSCRIPTION_FROM);
        IQ.Type fromString = IQ.Type.fromString(xmlPullParser.getAttributeValue("", "type"));
        boolean z2 = false;
        XMPPError xMPPError2 = null;
        IQ iq2 = null;
        while (!z2) {
            int next = xmlPullParser.next();
            if (next == 2) {
                String name = xmlPullParser.getName();
                String namespace = xmlPullParser.getNamespace();
                if (name.equals("error")) {
                    xMPPError = parseError(xmlPullParser);
                    iq = iq2;
                } else if (name.equals("query") && namespace.equals("jabber:iq:roster")) {
                    XMPPError xMPPError3 = xMPPError2;
                    iq = parseRoster(xmlPullParser);
                    xMPPError = xMPPError3;
                } else if (name.equals("query") && namespace.equals("jabber:iq:register")) {
                    XMPPError xMPPError4 = xMPPError2;
                    iq = parseRegistration(xmlPullParser);
                    xMPPError = xMPPError4;
                } else if (!name.equals("bind") || !namespace.equals("urn:ietf:params:xml:ns:xmpp-bind")) {
                    Object iQProvider = ProviderManager.getIQProvider(name, namespace);
                    if (iQProvider != null) {
                        if (iQProvider instanceof IQProvider) {
                            XMPPError xMPPError5 = xMPPError2;
                            iq = ((IQProvider) iQProvider).parseIQ(xmlPullParser);
                            xMPPError = xMPPError5;
                        } else if (iQProvider instanceof Class) {
                            XMPPError xMPPError6 = xMPPError2;
                            iq = (IQ) parseWithIntrospection(name, (Class) iQProvider, xmlPullParser);
                            xMPPError = xMPPError6;
                        }
                    } else if (IQ.Type.RESULT == fromString) {
                        XMPPError xMPPError7 = xMPPError2;
                        iq = new UnparsedResultIQ(parseContent(xmlPullParser));
                        xMPPError = xMPPError7;
                    }
                    xMPPError = xMPPError2;
                    iq = iq2;
                } else {
                    XMPPError xMPPError8 = xMPPError2;
                    iq = parseResourceBinding(xmlPullParser);
                    xMPPError = xMPPError8;
                }
                iq2 = iq;
                xMPPError2 = xMPPError;
                z = z2;
            } else if (next != 3 || !xmlPullParser.getName().equals("iq")) {
                z = z2;
            } else {
                z = true;
            }
            z2 = z;
        }
        if (iq2 == null) {
            if (IQ.Type.GET == fromString || IQ.Type.SET == fromString) {
                AnonymousClass1 r0 = new IQ() {
                    public String getChildElementXML() {
                        return null;
                    }
                };
                r0.setPacketID(attributeValue);
                r0.setTo(attributeValue3);
                r0.setFrom(attributeValue2);
                r0.setType(IQ.Type.ERROR);
                r0.setError(new XMPPError(XMPPError.Condition.feature_not_implemented));
                xMPPConnection.sendPacket(r0);
                return null;
            }
            iq2 = new IQ() {
                public String getChildElementXML() {
                    return null;
                }
            };
        }
        iq2.setPacketID(attributeValue);
        iq2.setTo(attributeValue2);
        iq2.setFrom(attributeValue3);
        iq2.setType(fromString);
        iq2.setError(xMPPError2);
        return iq2;
    }

    private static RosterPacket parseRoster(XmlPullParser xmlPullParser) throws Exception {
        boolean z;
        RosterPacket rosterPacket = new RosterPacket();
        boolean z2 = false;
        RosterPacket.Item item = null;
        rosterPacket.setVersion(xmlPullParser.getAttributeValue("", "ver"));
        while (!z2) {
            int next = xmlPullParser.next();
            if (next != 2) {
                if (next == 3) {
                    if (xmlPullParser.getName().equals(DataForm.Item.ELEMENT)) {
                        rosterPacket.addRosterItem(item);
                    }
                    if (xmlPullParser.getName().equals("query")) {
                        z = true;
                    }
                }
                z = z2;
            } else if (xmlPullParser.getName().equals(DataForm.Item.ELEMENT)) {
                RosterPacket.Item item2 = new RosterPacket.Item(xmlPullParser.getAttributeValue("", "jid"), xmlPullParser.getAttributeValue("", "name"));
                item2.setItemStatus(RosterPacket.ItemStatus.fromString(xmlPullParser.getAttributeValue("", "ask")));
                String attributeValue = xmlPullParser.getAttributeValue("", "subscription");
                if (attributeValue == null) {
                    attributeValue = PrivacyItem.SUBSCRIPTION_NONE;
                }
                item2.setItemType(RosterPacket.ItemType.valueOf(attributeValue));
                item = item2;
                z = z2;
            } else {
                if (xmlPullParser.getName().equals("group") && item != null) {
                    String nextText = xmlPullParser.nextText();
                    if (nextText != null && nextText.trim().length() > 0) {
                        item.addGroupName(nextText);
                    }
                    z = z2;
                }
                z = z2;
            }
            z2 = z;
        }
        return rosterPacket;
    }

    private static Registration parseRegistration(XmlPullParser xmlPullParser) throws Exception {
        HashMap hashMap;
        boolean z;
        Registration registration = new Registration();
        HashMap hashMap2 = null;
        boolean z2 = false;
        while (!z2) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getNamespace().equals("jabber:iq:register")) {
                    String name = xmlPullParser.getName();
                    String str = "";
                    if (hashMap2 == null) {
                        hashMap2 = new HashMap();
                    }
                    if (xmlPullParser.next() == 4) {
                        str = xmlPullParser.getText();
                    }
                    if (!name.equals("instructions")) {
                        hashMap2.put(name, str);
                    } else {
                        registration.setInstructions(str);
                    }
                    hashMap = hashMap2;
                    z = z2;
                } else {
                    registration.addExtension(parsePacketExtension(xmlPullParser.getName(), xmlPullParser.getNamespace(), xmlPullParser));
                    hashMap = hashMap2;
                    z = z2;
                }
            } else if (next != 3 || !xmlPullParser.getName().equals("query")) {
                hashMap = hashMap2;
                z = z2;
            } else {
                hashMap = hashMap2;
                z = true;
            }
            z2 = z;
            hashMap2 = hashMap;
        }
        registration.setAttributes(hashMap2);
        return registration;
    }

    private static Bind parseResourceBinding(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        Bind bind = new Bind();
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("resource")) {
                    bind.setResource(xmlPullParser.nextText());
                } else if (xmlPullParser.getName().equals("jid")) {
                    bind.setJid(xmlPullParser.nextText());
                }
            } else if (next == 3 && xmlPullParser.getName().equals("bind")) {
                z = true;
            }
        }
        return bind;
    }

    public static Collection<String> parseMechanisms(XmlPullParser xmlPullParser) throws Exception {
        ArrayList arrayList = new ArrayList();
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("mechanism")) {
                    arrayList.add(xmlPullParser.nextText());
                }
            } else if (next == 3 && xmlPullParser.getName().equals("mechanisms")) {
                z = true;
            }
        }
        return arrayList;
    }

    public static Collection<String> parseCompressionMethods(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        ArrayList arrayList = new ArrayList();
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("method")) {
                    arrayList.add(xmlPullParser.nextText());
                }
            } else if (next == 3 && xmlPullParser.getName().equals("compression")) {
                z = true;
            }
        }
        return arrayList;
    }

    public static SASLMechanism.SASLFailure parseSASLFailure(XmlPullParser xmlPullParser) throws Exception {
        String str = null;
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (!xmlPullParser.getName().equals("failure")) {
                    str = xmlPullParser.getName();
                }
            } else if (next == 3 && xmlPullParser.getName().equals("failure")) {
                z = true;
            }
        }
        return new SASLMechanism.SASLFailure(str);
    }

    public static StreamError parseStreamError(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        boolean z;
        String str = null;
        int depth = xmlPullParser.getDepth();
        boolean z2 = false;
        String str2 = null;
        while (!z2) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (StreamError.NAMESPACE.equals(xmlPullParser.getNamespace())) {
                    String name = xmlPullParser.getName();
                    if (!name.equals("text") || xmlPullParser.isEmptyElementTag()) {
                        str2 = name;
                    } else {
                        xmlPullParser.next();
                        str = xmlPullParser.getText();
                    }
                }
                z = z2;
            } else if (next == 3 && depth == xmlPullParser.getDepth()) {
                z = true;
            } else {
                z = z2;
            }
            z2 = z;
        }
        return new StreamError(str2, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.IllegalArgumentException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public static XMPPError parseError(XmlPullParser xmlPullParser) throws Exception {
        XMPPError.Type type;
        String str = null;
        ArrayList arrayList = new ArrayList();
        String str2 = null;
        for (int i = 0; i < xmlPullParser.getAttributeCount(); i++) {
            if (xmlPullParser.getAttributeName(i).equals("type")) {
                str2 = xmlPullParser.getAttributeValue("", "type");
            }
        }
        boolean z = false;
        String str3 = null;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("text")) {
                    str = xmlPullParser.nextText();
                } else {
                    String name = xmlPullParser.getName();
                    String namespace = xmlPullParser.getNamespace();
                    if ("urn:ietf:params:xml:ns:xmpp-stanzas".equals(namespace)) {
                        str3 = name;
                    } else {
                        arrayList.add(parsePacketExtension(name, namespace, xmlPullParser));
                    }
                }
            } else if (next == 3 && xmlPullParser.getName().equals("error")) {
                z = true;
            }
        }
        XMPPError.Type type2 = XMPPError.Type.CANCEL;
        if (str2 != null) {
            try {
                type = XMPPError.Type.valueOf(str2.toUpperCase(Locale.US));
            } catch (IllegalArgumentException e) {
                LOGGER.log(Level.SEVERE, "Could not find error type for " + str2.toUpperCase(Locale.US), (Throwable) e);
            }
        } else {
            type = type2;
        }
        type2 = type;
        return new XMPPError(type2, str3, str, arrayList);
    }

    public static PacketExtension parsePacketExtension(String str, String str2, XmlPullParser xmlPullParser) throws Exception {
        Object extensionProvider = ProviderManager.getExtensionProvider(str, str2);
        if (extensionProvider != null) {
            if (extensionProvider instanceof PacketExtensionProvider) {
                return ((PacketExtensionProvider) extensionProvider).parseExtension(xmlPullParser);
            }
            if (extensionProvider instanceof Class) {
                return (PacketExtension) parseWithIntrospection(str, (Class) extensionProvider, xmlPullParser);
            }
        }
        DefaultPacketExtension defaultPacketExtension = new DefaultPacketExtension(str, str2);
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                String name = xmlPullParser.getName();
                if (xmlPullParser.isEmptyElementTag()) {
                    defaultPacketExtension.setValue(name, "");
                } else if (xmlPullParser.next() == 4) {
                    defaultPacketExtension.setValue(name, xmlPullParser.getText());
                }
            } else if (next == 3 && xmlPullParser.getName().equals(str)) {
                z = true;
            }
        }
        return defaultPacketExtension;
    }

    private static String getLanguageAttribute(XmlPullParser xmlPullParser) {
        for (int i = 0; i < xmlPullParser.getAttributeCount(); i++) {
            String attributeName = xmlPullParser.getAttributeName(i);
            if ("xml:lang".equals(attributeName) || ("lang".equals(attributeName) && "xml".equals(xmlPullParser.getAttributePrefix(i)))) {
                return xmlPullParser.getAttributeValue(i);
            }
        }
        return null;
    }

    public static Object parseWithIntrospection(String str, Class<?> cls, XmlPullParser xmlPullParser) throws Exception {
        Object newInstance = cls.newInstance();
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                String name = xmlPullParser.getName();
                String nextText = xmlPullParser.nextText();
                Class<?> returnType = newInstance.getClass().getMethod("get" + Character.toUpperCase(name.charAt(0)) + name.substring(1), new Class[0]).getReturnType();
                newInstance.getClass().getMethod("set" + Character.toUpperCase(name.charAt(0)) + name.substring(1), returnType).invoke(newInstance, decode(returnType, nextText));
            } else if (next == 3 && xmlPullParser.getName().equals(str)) {
                z = true;
            }
        }
        return newInstance;
    }

    private static Object decode(Class<?> cls, String str) throws Exception {
        if (cls.getName().equals("java.lang.String")) {
            return str;
        }
        if (cls.getName().equals(FormField.TYPE_BOOLEAN)) {
            return Boolean.valueOf(str);
        }
        if (cls.getName().equals("int")) {
            return Integer.valueOf(str);
        }
        if (cls.getName().equals("long")) {
            return Long.valueOf(str);
        }
        if (cls.getName().equals("float")) {
            return Float.valueOf(str);
        }
        if (cls.getName().equals("double")) {
            return Double.valueOf(str);
        }
        if (cls.getName().equals("java.lang.Class")) {
            return Class.forName(str);
        }
        return null;
    }

    public static class UnparsedResultIQ extends IQ {
        private final String str;

        public UnparsedResultIQ(String str2) {
            this.str = str2;
        }

        public String getChildElementXML() {
            return this.str;
        }
    }
}
