package org.jivesoftware.smack.util;

import org.jivesoftware.smack.packet.PacketExtension;

public class XmlStringBuilder implements Appendable, CharSequence {
    static final /* synthetic */ boolean $assertionsDisabled = (!XmlStringBuilder.class.desiredAssertionStatus());
    public static final String RIGHT_ANGEL_BRACKET = Character.toString('>');
    private final LazyStringBuilder sb;

    public XmlStringBuilder() {
        this.sb = new LazyStringBuilder();
    }

    public XmlStringBuilder(PacketExtension packetExtension) {
        this();
        prelude(packetExtension);
    }

    public XmlStringBuilder element(String str, String str2) {
        if ($assertionsDisabled || str2 != null) {
            openElement(str);
            escape(str2);
            closeElement(str);
            return this;
        }
        throw new AssertionError();
    }

    public XmlStringBuilder element(String str, Enum<?> enumR) {
        if ($assertionsDisabled || enumR != null) {
            element(str, enumR.name());
            return this;
        }
        throw new AssertionError();
    }

    public XmlStringBuilder optElement(String str, String str2) {
        if (str2 != null) {
            element(str, str2);
        }
        return this;
    }

    public XmlStringBuilder optElement(String str, Enum<?> enumR) {
        if (enumR != null) {
            element(str, enumR);
        }
        return this;
    }

    public XmlStringBuilder halfOpenElement(String str) {
        this.sb.append('<').append((CharSequence) str);
        return this;
    }

    public XmlStringBuilder openElement(String str) {
        halfOpenElement(str).rightAngelBracket();
        return this;
    }

    public XmlStringBuilder closeElement(String str) {
        this.sb.append((CharSequence) "</").append((CharSequence) str);
        rightAngelBracket();
        return this;
    }

    public XmlStringBuilder closeElement(PacketExtension packetExtension) {
        closeElement(packetExtension.getElementName());
        return this;
    }

    public XmlStringBuilder closeEmptyElement() {
        this.sb.append((CharSequence) "/>");
        return this;
    }

    public XmlStringBuilder rightAngelBracket() {
        this.sb.append((CharSequence) RIGHT_ANGEL_BRACKET);
        return this;
    }

    public XmlStringBuilder attribute(String str, String str2) {
        if ($assertionsDisabled || str2 != null) {
            this.sb.append(' ').append((CharSequence) str).append((CharSequence) "='");
            escape(str2);
            this.sb.append('\'');
            return this;
        }
        throw new AssertionError();
    }

    public XmlStringBuilder attribute(String str, Enum<?> enumR) {
        if ($assertionsDisabled || enumR != null) {
            attribute(str, enumR.name());
            return this;
        }
        throw new AssertionError();
    }

    public XmlStringBuilder optAttribute(String str, String str2) {
        if (str2 != null) {
            attribute(str, str2);
        }
        return this;
    }

    public XmlStringBuilder optAttribute(String str, Enum<?> enumR) {
        if (enumR != null) {
            attribute(str, enumR.name());
        }
        return this;
    }

    public XmlStringBuilder xmlnsAttribute(String str) {
        optAttribute("xmlns", str);
        return this;
    }

    public XmlStringBuilder xmllangAttribute(String str) {
        optAttribute("xml:lang", str);
        return this;
    }

    public XmlStringBuilder escape(String str) {
        if ($assertionsDisabled || str != null) {
            this.sb.append(StringUtils.escapeForXML(str));
            return this;
        }
        throw new AssertionError();
    }

    public XmlStringBuilder prelude(PacketExtension packetExtension) {
        halfOpenElement(packetExtension.getElementName());
        xmlnsAttribute(packetExtension.getNamespace());
        return this;
    }

    public XmlStringBuilder optAppend(CharSequence charSequence) {
        if (charSequence != null) {
            append(charSequence);
        }
        return this;
    }

    public XmlStringBuilder append(XmlStringBuilder xmlStringBuilder) {
        if ($assertionsDisabled || xmlStringBuilder != null) {
            this.sb.append(xmlStringBuilder.sb);
            return this;
        }
        throw new AssertionError();
    }

    public XmlStringBuilder emptyElement(String str) {
        halfOpenElement(str);
        return closeEmptyElement();
    }

    public XmlStringBuilder condEmptyElement(boolean z, String str) {
        if (z) {
            emptyElement(str);
        }
        return this;
    }

    public XmlStringBuilder condAttribute(boolean z, String str, String str2) {
        if (z) {
            attribute(str, str2);
        }
        return this;
    }

    public XmlStringBuilder append(CharSequence charSequence) {
        if ($assertionsDisabled || charSequence != null) {
            this.sb.append(charSequence);
            return this;
        }
        throw new AssertionError();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jivesoftware.smack.util.LazyStringBuilder.append(java.lang.CharSequence, int, int):org.jivesoftware.smack.util.LazyStringBuilder
     arg types: [java.lang.CharSequence, int, int]
     candidates:
      org.jivesoftware.smack.util.LazyStringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      org.jivesoftware.smack.util.LazyStringBuilder.append(java.lang.CharSequence, int, int):org.jivesoftware.smack.util.LazyStringBuilder */
    public XmlStringBuilder append(CharSequence charSequence, int i, int i2) {
        if ($assertionsDisabled || charSequence != null) {
            this.sb.append(charSequence, i, i2);
            return this;
        }
        throw new AssertionError();
    }

    public XmlStringBuilder append(char c) {
        this.sb.append(c);
        return this;
    }

    public int length() {
        return this.sb.length();
    }

    public char charAt(int i) {
        return this.sb.charAt(i);
    }

    public CharSequence subSequence(int i, int i2) {
        return this.sb.subSequence(i, i2);
    }

    public String toString() {
        return this.sb.toString();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof XmlStringBuilder)) {
            return false;
        }
        return toString().equals(((XmlStringBuilder) obj).toString());
    }

    public int hashCode() {
        return toString().hashCode();
    }
}
