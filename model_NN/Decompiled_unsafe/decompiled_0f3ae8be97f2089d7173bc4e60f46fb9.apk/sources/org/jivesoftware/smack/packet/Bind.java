package org.jivesoftware.smack.packet;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.XmlStringBuilder;

public class Bind extends IQ {
    private String jid = null;
    private String resource = null;

    public Bind() {
        setType(IQ.Type.SET);
    }

    public String getResource() {
        return this.resource;
    }

    public void setResource(String str) {
        this.resource = str;
    }

    public String getJid() {
        return this.jid;
    }

    public void setJid(String str) {
        this.jid = str;
    }

    public CharSequence getChildElementXML() {
        XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
        xmlStringBuilder.halfOpenElement("bind");
        xmlStringBuilder.xmlnsAttribute("urn:ietf:params:xml:ns:xmpp-bind");
        xmlStringBuilder.rightAngelBracket();
        xmlStringBuilder.optElement("resource", this.resource);
        xmlStringBuilder.optElement("jid", this.jid);
        xmlStringBuilder.closeElement("bind");
        return xmlStringBuilder;
    }
}
