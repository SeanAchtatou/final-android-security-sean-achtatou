package org.jivesoftware.smack.packet;

import android.support.v4.widget.ExploreByTouchHelper;
import org.jivesoftware.smack.util.XmlStringBuilder;

public class Presence extends Packet {
    private String language;
    private Mode mode = null;
    private int priority = ExploreByTouchHelper.INVALID_ID;
    private String status = null;
    private Type type = Type.available;

    public enum Mode {
        chat,
        available,
        away,
        xa,
        dnd
    }

    public enum Type {
        available,
        unavailable,
        subscribe,
        subscribed,
        unsubscribe,
        unsubscribed,
        error
    }

    public Presence(Type type2) {
        setType(type2);
    }

    public Presence(Type type2, String str, int i, Mode mode2) {
        setType(type2);
        setStatus(str);
        setPriority(i);
        setMode(mode2);
    }

    public boolean isAvailable() {
        return this.type == Type.available;
    }

    public boolean isAway() {
        return this.type == Type.available && (this.mode == Mode.away || this.mode == Mode.xa || this.mode == Mode.dnd);
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type2) {
        if (type2 == null) {
            throw new NullPointerException("Type cannot be null");
        }
        this.type = type2;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String str) {
        this.status = str;
    }

    public int getPriority() {
        return this.priority;
    }

    public void setPriority(int i) {
        if (i < -128 || i > 128) {
            throw new IllegalArgumentException("Priority value " + i + " is not valid. Valid range is -128 through 128.");
        }
        this.priority = i;
    }

    public Mode getMode() {
        return this.mode;
    }

    public void setMode(Mode mode2) {
        this.mode = mode2;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String str) {
        this.language = str;
    }

    public XmlStringBuilder toXML() {
        XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
        xmlStringBuilder.halfOpenElement("presence");
        xmlStringBuilder.xmlnsAttribute(getXmlns());
        xmlStringBuilder.xmllangAttribute(getLanguage());
        addCommonAttributes(xmlStringBuilder);
        if (this.type != Type.available) {
            xmlStringBuilder.attribute("type", this.type);
        }
        xmlStringBuilder.rightAngelBracket();
        xmlStringBuilder.optElement("status", this.status);
        if (this.priority != Integer.MIN_VALUE) {
            xmlStringBuilder.element("priority", Integer.toString(this.priority));
        }
        if (!(this.mode == null || this.mode == Mode.available)) {
            xmlStringBuilder.element("show", this.mode);
        }
        xmlStringBuilder.append(getExtensionsXML());
        XMPPError error = getError();
        if (error != null) {
            xmlStringBuilder.append(error.toXML());
        }
        xmlStringBuilder.closeElement("presence");
        return xmlStringBuilder;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.type);
        if (this.mode != null) {
            sb.append(": ").append(this.mode);
        }
        if (getStatus() != null) {
            sb.append(" (").append(getStatus()).append(")");
        }
        return sb.toString();
    }
}
