package org.jivesoftware.smack.packet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.jivesoftware.smack.util.StringUtils;

public class XMPPError {
    private List<PacketExtension> applicationExtensions;
    private final String condition;
    private String message;
    private final Type type;

    public enum Type {
        WAIT,
        CANCEL,
        MODIFY,
        AUTH,
        CONTINUE
    }

    public XMPPError(Condition condition2) {
        this.applicationExtensions = null;
        ErrorSpecification specFor = ErrorSpecification.specFor(condition2);
        this.condition = condition2.value;
        if (specFor != null) {
            this.type = specFor.getType();
        } else {
            this.type = null;
        }
    }

    public XMPPError(Condition condition2, String str) {
        this(condition2);
        this.message = str;
    }

    public XMPPError(Type type2, String str, String str2, List<PacketExtension> list) {
        this.applicationExtensions = null;
        this.type = type2;
        this.condition = str;
        this.message = str2;
        this.applicationExtensions = list;
    }

    public String getCondition() {
        return this.condition;
    }

    public Type getType() {
        return this.type;
    }

    public String getMessage() {
        return this.message;
    }

    public CharSequence toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<error");
        if (this.type != null) {
            sb.append(" type=\"");
            sb.append(this.type.name().toLowerCase(Locale.US));
            sb.append("\"");
        }
        sb.append(">");
        if (this.condition != null) {
            sb.append("<").append(this.condition);
            sb.append(" xmlns=\"urn:ietf:params:xml:ns:xmpp-stanzas\"/>");
        }
        if (this.message != null) {
            sb.append("<text xml:lang=\"en\" xmlns=\"urn:ietf:params:xml:ns:xmpp-stanzas\">");
            sb.append(this.message);
            sb.append("</text>");
        }
        for (PacketExtension xml : getExtensions()) {
            sb.append(xml.toXML());
        }
        sb.append("</error>");
        return sb.toString();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.condition != null) {
            sb.append(this.condition);
        }
        if (this.message != null) {
            sb.append(" ").append(this.message);
        }
        return sb.toString();
    }

    public synchronized List<PacketExtension> getExtensions() {
        List<PacketExtension> unmodifiableList;
        if (this.applicationExtensions == null) {
            unmodifiableList = Collections.emptyList();
        } else {
            unmodifiableList = Collections.unmodifiableList(this.applicationExtensions);
        }
        return unmodifiableList;
    }

    public synchronized PacketExtension getExtension(String str, String str2) {
        PacketExtension packetExtension;
        if (this.applicationExtensions != null && str != null && str2 != null) {
            Iterator<PacketExtension> it = this.applicationExtensions.iterator();
            while (true) {
                if (!it.hasNext()) {
                    packetExtension = null;
                    break;
                }
                packetExtension = it.next();
                if (str.equals(packetExtension.getElementName()) && str2.equals(packetExtension.getNamespace())) {
                    break;
                }
            }
        } else {
            packetExtension = null;
        }
        return packetExtension;
    }

    public synchronized void addExtension(PacketExtension packetExtension) {
        if (this.applicationExtensions == null) {
            this.applicationExtensions = new ArrayList();
        }
        this.applicationExtensions.add(packetExtension);
    }

    public synchronized void setExtension(List<PacketExtension> list) {
        this.applicationExtensions = list;
    }

    public static class Condition implements CharSequence {
        public static final Condition bad_request = new Condition("bad-request");
        public static final Condition conflict = new Condition("conflict");
        public static final Condition feature_not_implemented = new Condition("feature-not-implemented");
        public static final Condition forbidden = new Condition("forbidden");
        public static final Condition gone = new Condition("gone");
        public static final Condition internal_server_error = new Condition("internal-server-error");
        public static final Condition item_not_found = new Condition("item-not-found");
        public static final Condition jid_malformed = new Condition("jid-malformed");
        public static final Condition not_acceptable = new Condition("not-acceptable");
        public static final Condition not_allowed = new Condition("not-allowed");
        public static final Condition not_authorized = new Condition("not-authorized");
        public static final Condition payment_required = new Condition("payment-required");
        public static final Condition recipient_unavailable = new Condition("recipient-unavailable");
        public static final Condition redirect = new Condition("redirect");
        public static final Condition registration_required = new Condition("registration-required");
        public static final Condition remote_server_error = new Condition("remote-server-error");
        public static final Condition remote_server_not_found = new Condition("remote-server-not-found");
        public static final Condition remote_server_timeout = new Condition("remote-server-timeout");
        public static final Condition request_timeout = new Condition("request-timeout");
        public static final Condition resource_constraint = new Condition("resource-constraint");
        public static final Condition service_unavailable = new Condition("service-unavailable");
        public static final Condition subscription_required = new Condition("subscription-required");
        public static final Condition undefined_condition = new Condition("undefined-condition");
        public static final Condition unexpected_request = new Condition("unexpected-request");
        /* access modifiers changed from: private */
        public final String value;

        public Condition(String str) {
            this.value = str;
        }

        public String toString() {
            return this.value;
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            return toString().equals(obj.toString());
        }

        public boolean equals(CharSequence charSequence) {
            return StringUtils.nullSafeCharSequenceEquals(this, charSequence);
        }

        public int hashCode() {
            return this.value.hashCode();
        }

        public int length() {
            return this.value.length();
        }

        public char charAt(int i) {
            return this.value.charAt(i);
        }

        public CharSequence subSequence(int i, int i2) {
            return this.value.subSequence(i, i2);
        }
    }

    private static class ErrorSpecification {
        private static Map<Condition, ErrorSpecification> instances = new HashMap();
        private final Condition condition;
        private final Type type;

        static {
            instances.put(Condition.internal_server_error, new ErrorSpecification(Condition.internal_server_error, Type.WAIT));
            instances.put(Condition.forbidden, new ErrorSpecification(Condition.forbidden, Type.AUTH));
            instances.put(Condition.bad_request, new ErrorSpecification(Condition.bad_request, Type.MODIFY));
            instances.put(Condition.item_not_found, new ErrorSpecification(Condition.item_not_found, Type.CANCEL));
            instances.put(Condition.conflict, new ErrorSpecification(Condition.conflict, Type.CANCEL));
            instances.put(Condition.feature_not_implemented, new ErrorSpecification(Condition.feature_not_implemented, Type.CANCEL));
            instances.put(Condition.gone, new ErrorSpecification(Condition.gone, Type.MODIFY));
            instances.put(Condition.jid_malformed, new ErrorSpecification(Condition.jid_malformed, Type.MODIFY));
            instances.put(Condition.not_acceptable, new ErrorSpecification(Condition.not_acceptable, Type.MODIFY));
            instances.put(Condition.not_allowed, new ErrorSpecification(Condition.not_allowed, Type.CANCEL));
            instances.put(Condition.not_authorized, new ErrorSpecification(Condition.not_authorized, Type.AUTH));
            instances.put(Condition.payment_required, new ErrorSpecification(Condition.payment_required, Type.AUTH));
            instances.put(Condition.recipient_unavailable, new ErrorSpecification(Condition.recipient_unavailable, Type.WAIT));
            instances.put(Condition.redirect, new ErrorSpecification(Condition.redirect, Type.MODIFY));
            instances.put(Condition.registration_required, new ErrorSpecification(Condition.registration_required, Type.AUTH));
            instances.put(Condition.remote_server_not_found, new ErrorSpecification(Condition.remote_server_not_found, Type.CANCEL));
            instances.put(Condition.remote_server_timeout, new ErrorSpecification(Condition.remote_server_timeout, Type.WAIT));
            instances.put(Condition.remote_server_error, new ErrorSpecification(Condition.remote_server_error, Type.CANCEL));
            instances.put(Condition.resource_constraint, new ErrorSpecification(Condition.resource_constraint, Type.WAIT));
            instances.put(Condition.service_unavailable, new ErrorSpecification(Condition.service_unavailable, Type.CANCEL));
            instances.put(Condition.subscription_required, new ErrorSpecification(Condition.subscription_required, Type.AUTH));
            instances.put(Condition.undefined_condition, new ErrorSpecification(Condition.undefined_condition, Type.WAIT));
            instances.put(Condition.unexpected_request, new ErrorSpecification(Condition.unexpected_request, Type.WAIT));
            instances.put(Condition.request_timeout, new ErrorSpecification(Condition.request_timeout, Type.CANCEL));
        }

        private ErrorSpecification(Condition condition2, Type type2) {
            this.type = type2;
            this.condition = condition2;
        }

        protected static ErrorSpecification specFor(Condition condition2) {
            return instances.get(condition2);
        }

        /* access modifiers changed from: protected */
        public Type getType() {
            return this.type;
        }
    }
}
