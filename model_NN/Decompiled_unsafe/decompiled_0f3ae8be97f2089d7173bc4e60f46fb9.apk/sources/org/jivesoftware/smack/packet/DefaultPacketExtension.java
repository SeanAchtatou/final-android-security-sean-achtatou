package org.jivesoftware.smack.packet;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.jivesoftware.smack.util.XmlStringBuilder;

public class DefaultPacketExtension implements PacketExtension {
    private String elementName;
    private Map<String, String> map;
    private String namespace;

    public DefaultPacketExtension(String str, String str2) {
        this.elementName = str;
        this.namespace = str2;
    }

    public String getElementName() {
        return this.elementName;
    }

    public String getNamespace() {
        return this.namespace;
    }

    public CharSequence toXML() {
        XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
        xmlStringBuilder.halfOpenElement(this.elementName).xmlnsAttribute(this.namespace).rightAngelBracket();
        for (String next : getNames()) {
            xmlStringBuilder.element(next, getValue(next));
        }
        xmlStringBuilder.closeElement(this.elementName);
        return xmlStringBuilder;
    }

    public synchronized Collection<String> getNames() {
        Set unmodifiableSet;
        if (this.map == null) {
            unmodifiableSet = Collections.emptySet();
        } else {
            unmodifiableSet = Collections.unmodifiableSet(new HashMap(this.map).keySet());
        }
        return unmodifiableSet;
    }

    public synchronized String getValue(String str) {
        String str2;
        if (this.map == null) {
            str2 = null;
        } else {
            str2 = this.map.get(str);
        }
        return str2;
    }

    public synchronized void setValue(String str, String str2) {
        if (this.map == null) {
            this.map = new HashMap();
        }
        this.map.put(str, str2);
    }
}
