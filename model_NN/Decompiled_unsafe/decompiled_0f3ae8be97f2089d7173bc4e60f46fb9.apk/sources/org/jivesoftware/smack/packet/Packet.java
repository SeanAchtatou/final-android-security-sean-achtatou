package org.jivesoftware.smack.packet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jivesoftware.smackx.privacy.packet.PrivacyItem;

public abstract class Packet {
    protected static final String DEFAULT_LANGUAGE = Locale.getDefault().getLanguage().toLowerCase(Locale.US);
    private static String DEFAULT_XML_NS = null;
    public static final String ID_NOT_AVAILABLE = "ID_NOT_AVAILABLE";
    private static long id = 0;
    private static String prefix = (StringUtils.randomString(5) + "-");
    private XMPPError error = null;
    private String from = null;
    private final List<PacketExtension> packetExtensions = new CopyOnWriteArrayList();
    private String packetID = null;
    private String to = null;
    private String xmlns = DEFAULT_XML_NS;

    public abstract CharSequence toXML();

    public static synchronized String nextID() {
        String sb;
        synchronized (Packet.class) {
            StringBuilder append = new StringBuilder().append(prefix);
            long j = id;
            id = 1 + j;
            sb = append.append(Long.toString(j)).toString();
        }
        return sb;
    }

    public static void setDefaultXmlns(String str) {
        DEFAULT_XML_NS = str;
    }

    public Packet() {
    }

    public Packet(Packet packet) {
        this.packetID = packet.getPacketID();
        this.to = packet.getTo();
        this.from = packet.getFrom();
        this.xmlns = packet.xmlns;
        this.error = packet.error;
        for (PacketExtension addExtension : packet.getExtensions()) {
            addExtension(addExtension);
        }
    }

    public String getPacketID() {
        if (ID_NOT_AVAILABLE.equals(this.packetID)) {
            return null;
        }
        if (this.packetID == null) {
            this.packetID = nextID();
        }
        return this.packetID;
    }

    public void setPacketID(String str) {
        this.packetID = str;
    }

    public String getTo() {
        return this.to;
    }

    public void setTo(String str) {
        this.to = str;
    }

    public String getFrom() {
        return this.from;
    }

    public void setFrom(String str) {
        this.from = str;
    }

    public XMPPError getError() {
        return this.error;
    }

    public void setError(XMPPError xMPPError) {
        this.error = xMPPError;
    }

    public synchronized Collection<PacketExtension> getExtensions() {
        List unmodifiableList;
        if (this.packetExtensions == null) {
            unmodifiableList = Collections.emptyList();
        } else {
            unmodifiableList = Collections.unmodifiableList(new ArrayList(this.packetExtensions));
        }
        return unmodifiableList;
    }

    public PacketExtension getExtension(String str) {
        return getExtension(null, str);
    }

    public <PE extends PacketExtension> PE getExtension(String str, String str2) {
        if (str2 == null) {
            return null;
        }
        Iterator<PacketExtension> it = this.packetExtensions.iterator();
        while (it.hasNext()) {
            PE pe = (PacketExtension) it.next();
            if ((str == null || str.equals(pe.getElementName())) && str2.equals(pe.getNamespace())) {
                return pe;
            }
        }
        return null;
    }

    public void addExtension(PacketExtension packetExtension) {
        if (packetExtension != null) {
            this.packetExtensions.add(packetExtension);
        }
    }

    public void addExtensions(Collection<PacketExtension> collection) {
        if (collection != null) {
            this.packetExtensions.addAll(collection);
        }
    }

    public void removeExtension(PacketExtension packetExtension) {
        this.packetExtensions.remove(packetExtension);
    }

    /* access modifiers changed from: protected */
    public synchronized CharSequence getExtensionsXML() {
        XmlStringBuilder xmlStringBuilder;
        xmlStringBuilder = new XmlStringBuilder();
        for (PacketExtension xml : getExtensions()) {
            xmlStringBuilder.append(xml.toXML());
        }
        return xmlStringBuilder;
    }

    public String getXmlns() {
        return this.xmlns;
    }

    public static String getDefaultLanguage() {
        return DEFAULT_LANGUAGE;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Packet packet = (Packet) obj;
        if (this.error != null) {
            if (!this.error.equals(packet.error)) {
                return false;
            }
        } else if (packet.error != null) {
            return false;
        }
        if (this.from != null) {
            if (!this.from.equals(packet.from)) {
                return false;
            }
        } else if (packet.from != null) {
            return false;
        }
        if (!this.packetExtensions.equals(packet.packetExtensions)) {
            return false;
        }
        if (this.packetID != null) {
            if (!this.packetID.equals(packet.packetID)) {
                return false;
            }
        } else if (packet.packetID != null) {
            return false;
        }
        if (this.to != null) {
            if (!this.to.equals(packet.to)) {
                return false;
            }
        } else if (packet.to != null) {
            return false;
        }
        if (this.xmlns == null ? packet.xmlns != null : !this.xmlns.equals(packet.xmlns)) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        int hashCode = (this.xmlns != null ? this.xmlns.hashCode() : 0) * 31;
        if (this.packetID != null) {
            i = this.packetID.hashCode();
        } else {
            i = 0;
        }
        int i5 = (i + hashCode) * 31;
        if (this.to != null) {
            i2 = this.to.hashCode();
        } else {
            i2 = 0;
        }
        int i6 = (i2 + i5) * 31;
        if (this.from != null) {
            i3 = this.from.hashCode();
        } else {
            i3 = 0;
        }
        int hashCode2 = (((i3 + i6) * 31) + this.packetExtensions.hashCode()) * 31;
        if (this.error != null) {
            i4 = this.error.hashCode();
        }
        return hashCode2 + i4;
    }

    public String toString() {
        return toXML().toString();
    }

    /* access modifiers changed from: protected */
    public void addCommonAttributes(XmlStringBuilder xmlStringBuilder) {
        xmlStringBuilder.optAttribute("id", getPacketID());
        xmlStringBuilder.optAttribute("to", getTo());
        xmlStringBuilder.optAttribute(PrivacyItem.SUBSCRIPTION_FROM, getFrom());
    }
}
