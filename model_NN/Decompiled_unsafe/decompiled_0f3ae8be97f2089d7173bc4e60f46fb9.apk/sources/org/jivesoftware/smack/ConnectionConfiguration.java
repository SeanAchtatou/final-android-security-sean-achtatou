package org.jivesoftware.smack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.jivesoftware.smack.proxy.ProxyInfo;
import org.jivesoftware.smack.util.DNSUtil;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smack.util.dns.HostAddress;

public class ConnectionConfiguration implements Cloneable {
    private CallbackHandler callbackHandler;
    private boolean compressionEnabled = false;
    private SSLContext customSSLContext;
    private boolean debuggerEnabled = SmackConfiguration.DEBUG_ENABLED;
    protected List<HostAddress> hostAddresses;
    private HostnameVerifier hostnameVerifier;
    private String keystorePath;
    private String keystoreType;
    private boolean legacySessionDisabled = false;
    private String password;
    private String pkcs11Library;
    protected ProxyInfo proxy;
    private boolean reconnectionAllowed = true;
    private String resource;
    private boolean rosterLoadedAtLogin = true;
    private RosterStore rosterStore;
    private SecurityMode securityMode = SecurityMode.enabled;
    private boolean sendPresence = true;
    private String serviceName;
    private SocketFactory socketFactory;
    private boolean useDnsSrvRr = true;
    private String username;

    public enum SecurityMode {
        required,
        enabled,
        disabled
    }

    public ConnectionConfiguration(String str) {
        init(str, ProxyInfo.forDefaultProxy());
    }

    public ConnectionConfiguration(String str, ProxyInfo proxyInfo) {
        init(str, proxyInfo);
    }

    public ConnectionConfiguration(String str, int i, String str2) {
        initHostAddresses(str, i);
        init(str2, ProxyInfo.forDefaultProxy());
    }

    public ConnectionConfiguration(String str, int i, String str2, ProxyInfo proxyInfo) {
        initHostAddresses(str, i);
        init(str2, proxyInfo);
    }

    public ConnectionConfiguration(String str, int i) {
        initHostAddresses(str, i);
        init(str, ProxyInfo.forDefaultProxy());
    }

    public ConnectionConfiguration(String str, int i, ProxyInfo proxyInfo) {
        initHostAddresses(str, i);
        init(str, proxyInfo);
    }

    /* access modifiers changed from: protected */
    public void init(String str, ProxyInfo proxyInfo) {
        if (StringUtils.isEmpty(str)) {
            throw new IllegalArgumentException("serviceName must not be the empty String");
        }
        this.serviceName = str;
        this.proxy = proxyInfo;
        this.keystorePath = System.getProperty("javax.net.ssl.keyStore");
        this.keystoreType = "jks";
        this.pkcs11Library = "pkcs11.config";
        this.socketFactory = proxyInfo.getSocketFactory();
    }

    /* access modifiers changed from: package-private */
    public void setServiceName(String str) {
        this.serviceName = str;
    }

    public String getServiceName() {
        return this.serviceName;
    }

    public SecurityMode getSecurityMode() {
        return this.securityMode;
    }

    public void setSecurityMode(SecurityMode securityMode2) {
        this.securityMode = securityMode2;
    }

    public String getKeystorePath() {
        return this.keystorePath;
    }

    public void setKeystorePath(String str) {
        this.keystorePath = str;
    }

    public String getKeystoreType() {
        return this.keystoreType;
    }

    public void setKeystoreType(String str) {
        this.keystoreType = str;
    }

    public String getPKCS11Library() {
        return this.pkcs11Library;
    }

    public void setPKCS11Library(String str) {
        this.pkcs11Library = str;
    }

    public SSLContext getCustomSSLContext() {
        return this.customSSLContext;
    }

    public void setCustomSSLContext(SSLContext sSLContext) {
        this.customSSLContext = sSLContext;
    }

    public void setHostnameVerifier(HostnameVerifier hostnameVerifier2) {
        this.hostnameVerifier = hostnameVerifier2;
    }

    public HostnameVerifier getHostnameVerifier() {
        if (this.hostnameVerifier != null) {
            return this.hostnameVerifier;
        }
        return SmackConfiguration.getDefaultHostnameVerifier();
    }

    public boolean isCompressionEnabled() {
        return this.compressionEnabled;
    }

    public void setCompressionEnabled(boolean z) {
        this.compressionEnabled = z;
    }

    public boolean isDebuggerEnabled() {
        return this.debuggerEnabled;
    }

    public void setDebuggerEnabled(boolean z) {
        this.debuggerEnabled = z;
    }

    public void setReconnectionAllowed(boolean z) {
        this.reconnectionAllowed = z;
    }

    public boolean isReconnectionAllowed() {
        return this.reconnectionAllowed;
    }

    public void setSocketFactory(SocketFactory socketFactory2) {
        this.socketFactory = socketFactory2;
    }

    public void setSendPresence(boolean z) {
        this.sendPresence = z;
    }

    public boolean isRosterLoadedAtLogin() {
        return this.rosterLoadedAtLogin;
    }

    public void setRosterLoadedAtLogin(boolean z) {
        this.rosterLoadedAtLogin = z;
    }

    public boolean isLegacySessionDisabled() {
        return this.legacySessionDisabled;
    }

    public void setLegacySessionDisabled(boolean z) {
        this.legacySessionDisabled = z;
    }

    public CallbackHandler getCallbackHandler() {
        return this.callbackHandler;
    }

    public void setCallbackHandler(CallbackHandler callbackHandler2) {
        this.callbackHandler = callbackHandler2;
    }

    public SocketFactory getSocketFactory() {
        return this.socketFactory;
    }

    public List<HostAddress> getHostAddresses() {
        return Collections.unmodifiableList(this.hostAddresses);
    }

    public void setRosterStore(RosterStore rosterStore2) {
        this.rosterStore = rosterStore2;
    }

    public RosterStore getRosterStore() {
        return this.rosterStore;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public String getResource() {
        return this.resource;
    }

    public boolean isSendPresence() {
        return this.sendPresence;
    }

    /* access modifiers changed from: package-private */
    public void setLoginInfo(String str, String str2, String str3) {
        this.username = str;
        this.password = str2;
        this.resource = str3;
    }

    /* access modifiers changed from: package-private */
    public void maybeResolveDns() throws Exception {
        if (this.useDnsSrvRr) {
            this.hostAddresses = DNSUtil.resolveXMPPDomain(this.serviceName);
        }
    }

    private void initHostAddresses(String str, int i) {
        if (StringUtils.isEmpty(str)) {
            throw new IllegalArgumentException("host must not be the empty String");
        }
        this.hostAddresses = new ArrayList(1);
        this.hostAddresses.add(new HostAddress(str, i));
        this.useDnsSrvRr = false;
    }
}
