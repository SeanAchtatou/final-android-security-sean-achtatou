package org.jivesoftware.smack.initializer;

import java.util.List;

public interface SmackInitializer {
    List<Exception> initialize();

    List<Exception> initialize(ClassLoader classLoader);
}
