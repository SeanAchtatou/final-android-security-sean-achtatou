package org.jivesoftware.smack.sasl;

import de.measite.smack.Sasl;
import java.io.IOException;
import java.util.HashMap;
import org.apache.harmony.javax.security.auth.callback.Callback;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.apache.harmony.javax.security.auth.callback.NameCallback;
import org.apache.harmony.javax.security.auth.callback.PasswordCallback;
import org.apache.harmony.javax.security.auth.callback.UnsupportedCallbackException;
import org.apache.harmony.javax.security.sasl.RealmCallback;
import org.apache.harmony.javax.security.sasl.RealmChoiceCallback;
import org.apache.harmony.javax.security.sasl.SaslClient;
import org.apache.harmony.javax.security.sasl.SaslException;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.StringUtils;

public abstract class SASLMechanism implements CallbackHandler {
    protected String authenticationId;
    protected String hostname;
    protected String password;
    private SASLAuthentication saslAuthentication;
    protected SaslClient sc;

    /* access modifiers changed from: protected */
    public abstract String getName();

    public SASLMechanism(SASLAuthentication sASLAuthentication) {
        this.saslAuthentication = sASLAuthentication;
    }

    public void authenticate(String str, String str2, String str3, String str4) throws IOException, SaslException, SmackException.NotConnectedException {
        this.authenticationId = str;
        this.password = str4;
        this.hostname = str2;
        this.sc = Sasl.createSaslClient(new String[]{getName()}, null, "xmpp", str3, new HashMap(), this);
        authenticate();
    }

    public void authenticate(String str, CallbackHandler callbackHandler) throws IOException, SaslException, SmackException.NotConnectedException {
        this.sc = Sasl.createSaslClient(new String[]{getName()}, null, "xmpp", str, new HashMap(), callbackHandler);
        authenticate();
    }

    /* access modifiers changed from: protected */
    public void authenticate() throws IOException, SaslException, SmackException.NotConnectedException {
        String str = null;
        if (this.sc.hasInitialResponse()) {
            str = StringUtils.encodeBase64(this.sc.evaluateChallenge(new byte[0]), false);
        }
        getSASLAuthentication().send(new AuthMechanism(getName(), str));
    }

    public void challengeReceived(String str) throws IOException, SmackException.NotConnectedException {
        byte[] evaluateChallenge;
        Response response;
        if (str != null) {
            evaluateChallenge = this.sc.evaluateChallenge(StringUtils.decodeBase64(str));
        } else {
            evaluateChallenge = this.sc.evaluateChallenge(new byte[0]);
        }
        if (evaluateChallenge == null) {
            response = new Response();
        } else {
            response = new Response(StringUtils.encodeBase64(evaluateChallenge, false));
        }
        getSASLAuthentication().send(response);
    }

    /* access modifiers changed from: protected */
    public SASLAuthentication getSASLAuthentication() {
        return this.saslAuthentication;
    }

    public void handle(Callback[] callbackArr) throws IOException, UnsupportedCallbackException {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < callbackArr.length) {
                if (callbackArr[i2] instanceof NameCallback) {
                    ((NameCallback) callbackArr[i2]).setName(this.authenticationId);
                } else if (callbackArr[i2] instanceof PasswordCallback) {
                    ((PasswordCallback) callbackArr[i2]).setPassword(this.password.toCharArray());
                } else if (callbackArr[i2] instanceof RealmCallback) {
                    RealmCallback realmCallback = (RealmCallback) callbackArr[i2];
                    realmCallback.setText(realmCallback.getDefaultText());
                } else if (!(callbackArr[i2] instanceof RealmChoiceCallback)) {
                    throw new UnsupportedCallbackException(callbackArr[i2]);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public static class AuthMechanism extends Packet {
        private final String authenticationText;
        private final String name;

        public AuthMechanism(String str, String str2) {
            if (str == null) {
                throw new NullPointerException("SASL mechanism name shouldn't be null.");
            }
            this.name = str;
            this.authenticationText = str2;
        }

        public String toXML() {
            StringBuilder sb = new StringBuilder();
            sb.append("<auth mechanism=\"").append(this.name);
            sb.append("\" xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\">");
            if (this.authenticationText != null && this.authenticationText.trim().length() > 0) {
                sb.append(this.authenticationText);
            }
            sb.append("</auth>");
            return sb.toString();
        }
    }

    public static class Challenge extends Packet {
        private final String data;

        public Challenge(String str) {
            this.data = str;
        }

        public String toXML() {
            StringBuilder sb = new StringBuilder();
            sb.append("<challenge xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\">");
            if (this.data != null && this.data.trim().length() > 0) {
                sb.append(this.data);
            }
            sb.append("</challenge>");
            return sb.toString();
        }
    }

    public static class Response extends Packet {
        private final String authenticationText;

        public Response() {
            this.authenticationText = null;
        }

        public Response(String str) {
            if (str == null || str.trim().length() == 0) {
                this.authenticationText = null;
            } else {
                this.authenticationText = str;
            }
        }

        public String toXML() {
            StringBuilder sb = new StringBuilder();
            sb.append("<response xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\">");
            if (this.authenticationText != null) {
                sb.append(this.authenticationText);
            }
            sb.append("</response>");
            return sb.toString();
        }
    }

    public static class Success extends Packet {
        private final String data;

        public Success(String str) {
            this.data = str;
        }

        public String toXML() {
            StringBuilder sb = new StringBuilder();
            sb.append("<success xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\">");
            if (this.data != null && this.data.trim().length() > 0) {
                sb.append(this.data);
            }
            sb.append("</success>");
            return sb.toString();
        }
    }

    public static class SASLFailure extends Packet {
        private final SASLError saslError;
        private final String saslErrorString;

        public SASLFailure(String str) {
            SASLError fromString = SASLError.fromString(str);
            if (fromString == null) {
                this.saslError = SASLError.not_authorized;
            } else {
                this.saslError = fromString;
            }
            this.saslErrorString = str;
        }

        public SASLError getSASLError() {
            return this.saslError;
        }

        public String getSASLErrorString() {
            return this.saslErrorString;
        }

        public String toXML() {
            StringBuilder sb = new StringBuilder();
            sb.append("<failure xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\">");
            sb.append("<").append(this.saslErrorString).append("/>");
            sb.append("</failure>");
            return sb.toString();
        }
    }
}
