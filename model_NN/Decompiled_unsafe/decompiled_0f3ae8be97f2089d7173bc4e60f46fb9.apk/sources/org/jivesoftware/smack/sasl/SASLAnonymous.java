package org.jivesoftware.smack.sasl;

import java.io.IOException;
import org.apache.harmony.javax.security.auth.callback.CallbackHandler;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.sasl.SASLMechanism;

public class SASLAnonymous extends SASLMechanism {
    public SASLAnonymous(SASLAuthentication sASLAuthentication) {
        super(sASLAuthentication);
    }

    /* access modifiers changed from: protected */
    public String getName() {
        return "ANONYMOUS";
    }

    public void authenticate(String str, String str2, CallbackHandler callbackHandler) throws IOException, SmackException.NotConnectedException {
        authenticate();
    }

    public void authenticate(String str, String str2, String str3) throws IOException, SmackException.NotConnectedException {
        authenticate();
    }

    /* access modifiers changed from: protected */
    public void authenticate() throws IOException, SmackException.NotConnectedException {
        getSASLAuthentication().send(new SASLMechanism.AuthMechanism(getName(), null));
    }

    public void challengeReceived(String str) throws IOException, SmackException.NotConnectedException {
        getSASLAuthentication().send(new SASLMechanism.Response());
    }
}
