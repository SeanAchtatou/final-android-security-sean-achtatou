package org.jivesoftware.smack;

import java.lang.ref.WeakReference;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public abstract class Manager {
    final WeakReference<XMPPConnection> weakConnection;

    public Manager(XMPPConnection xMPPConnection) {
        this.weakConnection = new WeakReference<>(xMPPConnection);
    }

    /* access modifiers changed from: protected */
    public final XMPPConnection connection() {
        return this.weakConnection.get();
    }

    /* access modifiers changed from: protected */
    public ScheduledFuture<?> schedule(Runnable runnable, long j, TimeUnit timeUnit) {
        return connection().schedule(runnable, j, timeUnit);
    }
}
