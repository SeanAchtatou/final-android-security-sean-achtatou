package org.jivesoftware.smack.provider;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.jivesoftware.smack.packet.IQ;

public final class ProviderManager {
    private static final Map<String, Object> extensionProviders = new ConcurrentHashMap();
    private static final Map<String, Object> iqProviders = new ConcurrentHashMap();

    public static void addLoader(ProviderLoader providerLoader) {
        if (providerLoader.getIQProviderInfo() != null) {
            for (IQProviderInfo next : providerLoader.getIQProviderInfo()) {
                iqProviders.put(getProviderKey(next.getElementName(), next.getNamespace()), next.getProvider());
            }
        }
        if (providerLoader.getExtensionProviderInfo() != null) {
            for (ExtensionProviderInfo next2 : providerLoader.getExtensionProviderInfo()) {
                extensionProviders.put(getProviderKey(next2.getElementName(), next2.getNamespace()), next2.getProvider());
            }
        }
    }

    public static Object getIQProvider(String str, String str2) {
        return iqProviders.get(getProviderKey(str, str2));
    }

    public static Collection<Object> getIQProviders() {
        return Collections.unmodifiableCollection(iqProviders.values());
    }

    public static void addIQProvider(String str, String str2, Object obj) {
        if ((obj instanceof IQProvider) || ((obj instanceof Class) && IQ.class.isAssignableFrom((Class) obj))) {
            iqProviders.put(getProviderKey(str, str2), obj);
            return;
        }
        throw new IllegalArgumentException("Provider must be an IQProvider or a Class instance sublcassing IQ.");
    }

    public static void removeIQProvider(String str, String str2) {
        iqProviders.remove(getProviderKey(str, str2));
    }

    public static Object getExtensionProvider(String str, String str2) {
        return extensionProviders.get(getProviderKey(str, str2));
    }

    public static void addExtensionProvider(String str, String str2, Object obj) {
        if ((obj instanceof PacketExtensionProvider) || (obj instanceof Class)) {
            extensionProviders.put(getProviderKey(str, str2), obj);
            return;
        }
        throw new IllegalArgumentException("Provider must be a PacketExtensionProvider or a Class instance.");
    }

    public static void removeExtensionProvider(String str, String str2) {
        extensionProviders.remove(getProviderKey(str, str2));
    }

    public static Collection<Object> getExtensionProviders() {
        return Collections.unmodifiableCollection(extensionProviders.values());
    }

    private static String getProviderKey(String str, String str2) {
        return str + '#' + str2;
    }
}
