package org.jivesoftware.smackx.caps;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import java.util.TreeSet;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.AbstractConnectionListener;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.Manager;
import org.jivesoftware.smack.PacketInterceptor;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.NotFilter;
import org.jivesoftware.smack.filter.PacketExtensionFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.util.Base64;
import org.jivesoftware.smack.util.Cache;
import org.jivesoftware.smackx.caps.cache.EntityCapsPersistentCache;
import org.jivesoftware.smackx.caps.packet.CapsExtension;
import org.jivesoftware.smackx.disco.NodeInformationProvider;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.disco.packet.DiscoverInfo;
import org.jivesoftware.smackx.disco.packet.DiscoverItems;
import org.jivesoftware.smackx.xdata.FormField;
import org.jivesoftware.smackx.xdata.packet.DataForm;

public class EntityCapsManager extends Manager {
    private static String DEFAULT_ENTITY_NODE = "http://www.igniterealtime.org/projects/smack";
    public static final String ELEMENT = "c";
    private static final Logger LOGGER = Logger.getLogger(EntityCapsManager.class.getName());
    public static final String NAMESPACE = "http://jabber.org/protocol/caps";
    private static final PacketFilter PRESENCES = new PacketTypeFilter(Presence.class);
    private static final PacketFilter PRESENCES_WITHOUT_CAPS = new AndFilter(new PacketTypeFilter(Presence.class), new NotFilter(new PacketExtensionFilter(ELEMENT, NAMESPACE)));
    private static final PacketFilter PRESENCES_WITH_CAPS = new AndFilter(new PacketTypeFilter(Presence.class), new PacketExtensionFilter(ELEMENT, NAMESPACE));
    /* access modifiers changed from: private */
    public static final Map<String, MessageDigest> SUPPORTED_HASHES = new HashMap();
    private static boolean autoEnableEntityCaps = true;
    protected static Map<String, DiscoverInfo> caps = new Cache(1000, -1);
    private static Map<XMPPConnection, EntityCapsManager> instances = Collections.synchronizedMap(new WeakHashMap());
    protected static Map<String, NodeVerHash> jidCaps = new Cache(10000, -1);
    protected static EntityCapsPersistentCache persistentCache;
    private String currentCapsVersion;
    /* access modifiers changed from: private */
    public boolean entityCapsEnabled;
    /* access modifiers changed from: private */
    public String entityNode = DEFAULT_ENTITY_NODE;
    private Queue<String> lastLocalCapsVersions = new ConcurrentLinkedQueue();
    /* access modifiers changed from: private */
    public boolean presenceSend = false;
    /* access modifiers changed from: private */
    public ServiceDiscoveryManager sdm;

    static {
        XMPPConnection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(XMPPConnection xMPPConnection) {
                EntityCapsManager.getInstanceFor(xMPPConnection);
            }
        });
        try {
            SUPPORTED_HASHES.put("sha-1", MessageDigest.getInstance("SHA-1"));
        } catch (NoSuchAlgorithmException e) {
        }
    }

    public static void setDefaultEntityNode(String str) {
        DEFAULT_ENTITY_NODE = str;
    }

    public static void addDiscoverInfoByNode(String str, DiscoverInfo discoverInfo) {
        caps.put(str, discoverInfo);
        if (persistentCache != null) {
            persistentCache.addDiscoverInfoByNodePersistent(str, discoverInfo);
        }
    }

    public static String getNodeVersionByJid(String str) {
        NodeVerHash nodeVerHash = jidCaps.get(str);
        if (nodeVerHash != null) {
            return nodeVerHash.nodeVer;
        }
        return null;
    }

    public static NodeVerHash getNodeVerHashByJid(String str) {
        return jidCaps.get(str);
    }

    public static DiscoverInfo getDiscoverInfoByUser(String str) {
        NodeVerHash nodeVerHash = jidCaps.get(str);
        if (nodeVerHash == null) {
            return null;
        }
        return getDiscoveryInfoByNodeVer(nodeVerHash.nodeVer);
    }

    public static DiscoverInfo getDiscoveryInfoByNodeVer(String str) {
        DiscoverInfo discoverInfo = caps.get(str);
        if (discoverInfo != null) {
            return new DiscoverInfo(discoverInfo);
        }
        return discoverInfo;
    }

    public static void setPersistentCache(EntityCapsPersistentCache entityCapsPersistentCache) throws IOException {
        if (persistentCache != null) {
            throw new IllegalStateException("Entity Caps Persistent Cache was already set");
        }
        persistentCache = entityCapsPersistentCache;
        persistentCache.replay();
    }

    public static void setJidCapsMaxCacheSize(int i) {
        ((Cache) jidCaps).setMaxCacheSize(i);
    }

    public static void setCapsMaxCacheSize(int i) {
        ((Cache) caps).setMaxCacheSize(i);
    }

    private EntityCapsManager(XMPPConnection xMPPConnection) {
        super(xMPPConnection);
        this.sdm = ServiceDiscoveryManager.getInstanceFor(xMPPConnection);
        instances.put(xMPPConnection, this);
        xMPPConnection.addConnectionListener(new AbstractConnectionListener() {
            public void connectionClosed() {
                boolean unused = EntityCapsManager.this.presenceSend = false;
            }

            public void connectionClosedOnError(Exception exc) {
                boolean unused = EntityCapsManager.this.presenceSend = false;
            }
        });
        updateLocalEntityCaps();
        if (autoEnableEntityCaps) {
            enableEntityCaps();
        }
        xMPPConnection.addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) {
                if (EntityCapsManager.this.entityCapsEnabled()) {
                    CapsExtension capsExtension = (CapsExtension) packet.getExtension(EntityCapsManager.ELEMENT, EntityCapsManager.NAMESPACE);
                    String lowerCase = capsExtension.getHash().toLowerCase(Locale.US);
                    if (EntityCapsManager.SUPPORTED_HASHES.containsKey(lowerCase)) {
                        EntityCapsManager.jidCaps.put(packet.getFrom(), new NodeVerHash(capsExtension.getNode(), capsExtension.getVer(), lowerCase));
                    }
                }
            }
        }, PRESENCES_WITH_CAPS);
        xMPPConnection.addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) {
                EntityCapsManager.jidCaps.remove(packet.getFrom());
            }
        }, PRESENCES_WITHOUT_CAPS);
        xMPPConnection.addPacketSendingListener(new PacketListener() {
            public void processPacket(Packet packet) {
                boolean unused = EntityCapsManager.this.presenceSend = true;
            }
        }, PRESENCES);
        xMPPConnection.addPacketInterceptor(new PacketInterceptor() {
            public void interceptPacket(Packet packet) {
                if (EntityCapsManager.this.entityCapsEnabled) {
                    packet.addExtension(new CapsExtension(EntityCapsManager.this.entityNode, EntityCapsManager.this.getCapsVersion(), "sha-1"));
                }
            }
        }, PRESENCES);
        this.sdm.setEntityCapsManager(this);
    }

    public static synchronized EntityCapsManager getInstanceFor(XMPPConnection xMPPConnection) {
        EntityCapsManager entityCapsManager;
        synchronized (EntityCapsManager.class) {
            if (SUPPORTED_HASHES.size() <= 0) {
                throw new IllegalStateException("No supported hashes for EntityCapsManager");
            }
            entityCapsManager = instances.get(xMPPConnection);
            if (entityCapsManager == null) {
                entityCapsManager = new EntityCapsManager(xMPPConnection);
            }
        }
        return entityCapsManager;
    }

    public synchronized void enableEntityCaps() {
        this.sdm.addFeature(NAMESPACE);
        updateLocalEntityCaps();
        this.entityCapsEnabled = true;
    }

    public synchronized void disableEntityCaps() {
        this.entityCapsEnabled = false;
        this.sdm.removeFeature(NAMESPACE);
    }

    public boolean entityCapsEnabled() {
        return this.entityCapsEnabled;
    }

    public void setEntityNode(String str) throws SmackException.NotConnectedException {
        this.entityNode = str;
        updateLocalEntityCaps();
    }

    public void removeUserCapsNode(String str) {
        jidCaps.remove(str);
    }

    public String getCapsVersion() {
        return this.currentCapsVersion;
    }

    public String getLocalNodeVer() {
        return this.entityNode + '#' + getCapsVersion();
    }

    public boolean areEntityCapsSupported(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return this.sdm.supportsFeature(str, NAMESPACE);
    }

    public boolean areEntityCapsSupportedByServer() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return areEntityCapsSupported(connection().getServiceName());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, org.jivesoftware.smack.SmackException$NotConnectedException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public void updateLocalEntityCaps() {
        XMPPConnection connection = connection();
        DiscoverInfo discoverInfo = new DiscoverInfo();
        discoverInfo.setType(IQ.Type.RESULT);
        discoverInfo.setNode(getLocalNodeVer());
        if (connection != null) {
            discoverInfo.setFrom(connection.getUser());
        }
        this.sdm.addDiscoverInfoTo(discoverInfo);
        this.currentCapsVersion = generateVerificationString(discoverInfo, "sha-1");
        addDiscoverInfoByNode(this.entityNode + '#' + this.currentCapsVersion, discoverInfo);
        if (this.lastLocalCapsVersions.size() > 10) {
            this.sdm.removeNodeInformationProvider(this.entityNode + '#' + this.lastLocalCapsVersions.poll());
        }
        this.lastLocalCapsVersions.add(this.currentCapsVersion);
        caps.put(this.currentCapsVersion, discoverInfo);
        if (connection != null) {
            jidCaps.put(connection.getUser(), new NodeVerHash(this.entityNode, this.currentCapsVersion, "sha-1"));
        }
        final LinkedList linkedList = new LinkedList(ServiceDiscoveryManager.getInstanceFor(connection).getIdentities());
        this.sdm.setNodeInformationProvider(this.entityNode + '#' + this.currentCapsVersion, new NodeInformationProvider() {
            List<String> features = EntityCapsManager.this.sdm.getFeaturesList();
            List<PacketExtension> packetExtensions = EntityCapsManager.this.sdm.getExtendedInfoAsList();

            public List<DiscoverItems.Item> getNodeItems() {
                return null;
            }

            public List<String> getNodeFeatures() {
                return this.features;
            }

            public List<DiscoverInfo.Identity> getNodeIdentities() {
                return linkedList;
            }

            public List<PacketExtension> getNodePacketExtensions() {
                return this.packetExtensions;
            }
        });
        if (connection != null && connection.isAuthenticated() && this.presenceSend) {
            try {
                connection.sendPacket(new Presence(Presence.Type.available));
            } catch (SmackException.NotConnectedException e) {
                LOGGER.log(Level.WARNING, "Could could not update presence with caps info", (Throwable) e);
            }
        }
    }

    public static boolean verifyDiscoverInfoVersion(String str, String str2, DiscoverInfo discoverInfo) {
        if (!discoverInfo.containsDuplicateIdentities() && !discoverInfo.containsDuplicateFeatures() && !verifyPacketExtensions(discoverInfo) && str.equals(generateVerificationString(discoverInfo, str2))) {
            return true;
        }
        return false;
    }

    protected static boolean verifyPacketExtensions(DiscoverInfo discoverInfo) {
        LinkedList<FormField> linkedList = new LinkedList<>();
        for (PacketExtension next : discoverInfo.getExtensions()) {
            if (next.getNamespace().equals(DataForm.NAMESPACE)) {
                for (FormField next2 : ((DataForm) next).getFields()) {
                    if (next2.getVariable().equals("FORM_TYPE")) {
                        for (FormField equals : linkedList) {
                            if (next2.equals(equals)) {
                                return true;
                            }
                        }
                        linkedList.add(next2);
                    }
                }
                continue;
            }
        }
        return false;
    }

    protected static String generateVerificationString(DiscoverInfo discoverInfo, String str) {
        byte[] digest;
        MessageDigest messageDigest = SUPPORTED_HASHES.get(str.toLowerCase(Locale.US));
        if (messageDigest == null) {
            return null;
        }
        DataForm dataForm = (DataForm) discoverInfo.getExtension("x", DataForm.NAMESPACE);
        StringBuilder sb = new StringBuilder();
        TreeSet<DiscoverInfo.Identity> treeSet = new TreeSet<>();
        for (DiscoverInfo.Identity add : discoverInfo.getIdentities()) {
            treeSet.add(add);
        }
        for (DiscoverInfo.Identity identity : treeSet) {
            sb.append(identity.getCategory());
            sb.append("/");
            sb.append(identity.getType());
            sb.append("/");
            sb.append(identity.getLanguage() == null ? "" : identity.getLanguage());
            sb.append("/");
            sb.append(identity.getName() == null ? "" : identity.getName());
            sb.append("<");
        }
        TreeSet<String> treeSet2 = new TreeSet<>();
        for (DiscoverInfo.Feature var : discoverInfo.getFeatures()) {
            treeSet2.add(var.getVar());
        }
        for (String append : treeSet2) {
            sb.append(append);
            sb.append("<");
        }
        if (dataForm != null && dataForm.hasHiddenFormTypeField()) {
            synchronized (dataForm) {
                TreeSet<FormField> treeSet3 = new TreeSet<>(new Comparator<FormField>() {
                    public int compare(FormField formField, FormField formField2) {
                        return formField.getVariable().compareTo(formField2.getVariable());
                    }
                });
                FormField formField = null;
                for (FormField next : dataForm.getFields()) {
                    if (!next.getVariable().equals("FORM_TYPE")) {
                        treeSet3.add(next);
                        next = formField;
                    }
                    formField = next;
                }
                if (formField != null) {
                    formFieldValuesToCaps(formField.getValues(), sb);
                }
                for (FormField formField2 : treeSet3) {
                    sb.append(formField2.getVariable());
                    sb.append("<");
                    formFieldValuesToCaps(formField2.getValues(), sb);
                }
            }
        }
        synchronized (messageDigest) {
            digest = messageDigest.digest(sb.toString().getBytes());
        }
        return Base64.encodeBytes(digest);
    }

    private static void formFieldValuesToCaps(List<String> list, StringBuilder sb) {
        TreeSet<String> treeSet = new TreeSet<>();
        for (String add : list) {
            treeSet.add(add);
        }
        for (String append : treeSet) {
            sb.append(append);
            sb.append("<");
        }
    }

    public static class NodeVerHash {
        private String hash;
        private String node;
        /* access modifiers changed from: private */
        public String nodeVer;
        private String ver;

        NodeVerHash(String str, String str2, String str3) {
            this.node = str;
            this.ver = str2;
            this.hash = str3;
            this.nodeVer = str + "#" + str2;
        }

        public String getNodeVer() {
            return this.nodeVer;
        }

        public String getNode() {
            return this.node;
        }

        public String getHash() {
            return this.hash;
        }

        public String getVer() {
            return this.ver;
        }
    }
}
