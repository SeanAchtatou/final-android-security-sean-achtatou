package org.jivesoftware.smackx.caps.cache;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.util.Base32Encoder;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.jivesoftware.smack.util.StringEncoder;
import org.jivesoftware.smackx.caps.EntityCapsManager;
import org.jivesoftware.smackx.disco.packet.DiscoverInfo;
import org.jivesoftware.smackx.disco.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.privacy.packet.PrivacyItem;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class SimpleDirectoryPersistentCache implements EntityCapsPersistentCache {
    private static final Logger LOGGER = Logger.getLogger(SimpleDirectoryPersistentCache.class.getName());
    private File cacheDir;
    private StringEncoder filenameEncoder;

    public SimpleDirectoryPersistentCache(File file) {
        this(file, Base32Encoder.getInstance());
    }

    public SimpleDirectoryPersistentCache(File file, StringEncoder stringEncoder) {
        if (!file.exists()) {
            throw new IllegalStateException("Cache directory \"" + file + "\" does not exist");
        } else if (!file.isDirectory()) {
            throw new IllegalStateException("Cache directory \"" + file + "\" is not a directory");
        } else {
            this.cacheDir = file;
            this.filenameEncoder = stringEncoder;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public void addDiscoverInfoByNodePersistent(String str, DiscoverInfo discoverInfo) {
        File file = new File(this.cacheDir, this.filenameEncoder.encode(str));
        try {
            if (file.createNewFile()) {
                writeInfoToFile(file, discoverInfo);
            }
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Failed to write disco info to file", (Throwable) e);
        }
    }

    public void replay() throws IOException {
        for (File file : this.cacheDir.listFiles()) {
            String decode = this.filenameEncoder.decode(file.getName());
            DiscoverInfo restoreInfoFromFile = restoreInfoFromFile(file);
            if (restoreInfoFromFile != null) {
                EntityCapsManager.addDiscoverInfoByNode(decode, restoreInfoFromFile);
            }
        }
    }

    public void emptyCache() {
        for (File delete : this.cacheDir.listFiles()) {
            delete.delete();
        }
    }

    private static void writeInfoToFile(File file, DiscoverInfo discoverInfo) throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(file));
        try {
            dataOutputStream.writeUTF(discoverInfo.toXML().toString());
        } finally {
            dataOutputStream.close();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, org.xmlpull.v1.XmlPullParserException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private static DiscoverInfo restoreInfoFromFile(File file) throws IOException {
        DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file));
        try {
            String readUTF = dataInputStream.readUTF();
            if (readUTF == null) {
                return null;
            }
            StringReader stringReader = new StringReader(readUTF);
            try {
                XmlPullParser newXmppParser = PacketParserUtils.newXmppParser();
                newXmppParser.setInput(stringReader);
                DiscoverInfoProvider discoverInfoProvider = new DiscoverInfoProvider();
                try {
                    newXmppParser.next();
                    String attributeValue = newXmppParser.getAttributeValue("", "id");
                    String attributeValue2 = newXmppParser.getAttributeValue("", PrivacyItem.SUBSCRIPTION_FROM);
                    String attributeValue3 = newXmppParser.getAttributeValue("", "to");
                    newXmppParser.next();
                    try {
                        DiscoverInfo discoverInfo = (DiscoverInfo) discoverInfoProvider.parseIQ(newXmppParser);
                        discoverInfo.setPacketID(attributeValue);
                        discoverInfo.setFrom(attributeValue2);
                        discoverInfo.setTo(attributeValue3);
                        discoverInfo.setType(IQ.Type.RESULT);
                        return discoverInfo;
                    } catch (Exception e) {
                        return null;
                    }
                } catch (XmlPullParserException e2) {
                    return null;
                }
            } catch (XmlPullParserException e3) {
                LOGGER.log(Level.SEVERE, "Exception initializing parser", (Throwable) e3);
                return null;
            }
        } finally {
            dataInputStream.close();
        }
    }
}
