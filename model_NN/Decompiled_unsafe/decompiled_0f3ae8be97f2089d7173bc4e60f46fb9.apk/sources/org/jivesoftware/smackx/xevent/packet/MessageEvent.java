package org.jivesoftware.smackx.xevent.packet;

import java.util.ArrayList;
import java.util.List;
import org.jivesoftware.smack.packet.PacketExtension;

public class MessageEvent implements PacketExtension {
    public static final String CANCELLED = "cancelled";
    public static final String COMPOSING = "composing";
    public static final String DELIVERED = "delivered";
    public static final String DISPLAYED = "displayed";
    public static final String OFFLINE = "offline";
    private boolean cancelled = true;
    private boolean composing = false;
    private boolean delivered = false;
    private boolean displayed = false;
    private boolean offline = false;
    private String packetID = null;

    public String getElementName() {
        return "x";
    }

    public String getNamespace() {
        return "jabber:x:event";
    }

    public boolean isComposing() {
        return this.composing;
    }

    public boolean isDelivered() {
        return this.delivered;
    }

    public boolean isDisplayed() {
        return this.displayed;
    }

    public boolean isOffline() {
        return this.offline;
    }

    public boolean isCancelled() {
        return this.cancelled;
    }

    public String getPacketID() {
        return this.packetID;
    }

    public List<String> getEventTypes() {
        ArrayList arrayList = new ArrayList();
        if (isDelivered()) {
            arrayList.add(DELIVERED);
        }
        if (!isMessageEventRequest() && isCancelled()) {
            arrayList.add(CANCELLED);
        }
        if (isComposing()) {
            arrayList.add(COMPOSING);
        }
        if (isDisplayed()) {
            arrayList.add(DISPLAYED);
        }
        if (isOffline()) {
            arrayList.add(OFFLINE);
        }
        return arrayList;
    }

    public void setComposing(boolean z) {
        this.composing = z;
        setCancelled(false);
    }

    public void setDelivered(boolean z) {
        this.delivered = z;
        setCancelled(false);
    }

    public void setDisplayed(boolean z) {
        this.displayed = z;
        setCancelled(false);
    }

    public void setOffline(boolean z) {
        this.offline = z;
        setCancelled(false);
    }

    public void setCancelled(boolean z) {
        this.cancelled = z;
    }

    public void setPacketID(String str) {
        this.packetID = str;
    }

    public boolean isMessageEventRequest() {
        return this.packetID == null;
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\">");
        if (isOffline()) {
            sb.append("<").append(OFFLINE).append("/>");
        }
        if (isDelivered()) {
            sb.append("<").append(DELIVERED).append("/>");
        }
        if (isDisplayed()) {
            sb.append("<").append(DISPLAYED).append("/>");
        }
        if (isComposing()) {
            sb.append("<").append(COMPOSING).append("/>");
        }
        if (getPacketID() != null) {
            sb.append("<id>").append(getPacketID()).append("</id>");
        }
        sb.append("</").append(getElementName()).append(">");
        return sb.toString();
    }
}
