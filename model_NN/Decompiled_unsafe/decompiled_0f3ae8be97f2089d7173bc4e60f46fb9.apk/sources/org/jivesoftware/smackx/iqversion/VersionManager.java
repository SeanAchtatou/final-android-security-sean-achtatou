package org.jivesoftware.smackx.iqversion;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import org.jivesoftware.smack.Manager;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.IQTypeFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.iqversion.packet.Version;

public class VersionManager extends Manager {
    private static final Map<XMPPConnection, VersionManager> instances = Collections.synchronizedMap(new WeakHashMap());
    /* access modifiers changed from: private */
    public Version own_version;

    private VersionManager(XMPPConnection xMPPConnection) {
        super(xMPPConnection);
        instances.put(xMPPConnection, this);
        ServiceDiscoveryManager.getInstanceFor(xMPPConnection).addFeature(Version.NAMESPACE);
        xMPPConnection.addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                if (VersionManager.this.own_version != null) {
                    Version version = new Version(VersionManager.this.own_version);
                    version.setPacketID(packet.getPacketID());
                    version.setTo(packet.getFrom());
                    VersionManager.this.connection().sendPacket(version);
                }
            }
        }, new AndFilter(new PacketTypeFilter(Version.class), new IQTypeFilter(IQ.Type.GET)));
    }

    public static synchronized VersionManager getInstanceFor(XMPPConnection xMPPConnection) {
        VersionManager versionManager;
        synchronized (VersionManager.class) {
            versionManager = instances.get(xMPPConnection);
            if (versionManager == null) {
                versionManager = new VersionManager(xMPPConnection);
            }
        }
        return versionManager;
    }

    public void setVersion(Version version) {
        this.own_version = version;
    }
}
