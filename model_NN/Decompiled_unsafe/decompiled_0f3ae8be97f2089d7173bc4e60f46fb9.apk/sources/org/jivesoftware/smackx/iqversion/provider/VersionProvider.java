package org.jivesoftware.smackx.iqversion.provider;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smackx.iqversion.packet.Version;
import org.xmlpull.v1.XmlPullParser;

public class VersionProvider implements IQProvider {
    public IQ parseIQ(XmlPullParser xmlPullParser) throws Exception {
        String str = null;
        boolean z = false;
        String str2 = null;
        String str3 = null;
        while (!z) {
            int next = xmlPullParser.next();
            String name = xmlPullParser.getName();
            if (next == 2) {
                if (name.equals("name")) {
                    str3 = xmlPullParser.nextText();
                } else if (name.equals("version")) {
                    str2 = xmlPullParser.nextText();
                } else if (name.equals("os")) {
                    str = xmlPullParser.nextText();
                }
            } else if (next == 3 && name.equals("query")) {
                z = true;
            }
        }
        return new Version(str3, str2, str);
    }
}
