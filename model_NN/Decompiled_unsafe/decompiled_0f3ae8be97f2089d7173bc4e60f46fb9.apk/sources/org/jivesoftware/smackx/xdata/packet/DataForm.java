package org.jivesoftware.smackx.xdata.packet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jivesoftware.smackx.xdata.FormField;

public class DataForm implements PacketExtension {
    public static final String ELEMENT = "x";
    public static final String NAMESPACE = "jabber:x:data";
    private final List<FormField> fields = new ArrayList();
    private List<String> instructions = new ArrayList();
    private final List<Item> items = new ArrayList();
    private ReportedData reportedData;
    private String title;
    private String type;

    public DataForm(String str) {
        this.type = str;
    }

    public String getType() {
        return this.type;
    }

    public String getTitle() {
        return this.title;
    }

    public List<String> getInstructions() {
        List<String> unmodifiableList;
        synchronized (this.instructions) {
            unmodifiableList = Collections.unmodifiableList(new ArrayList(this.instructions));
        }
        return unmodifiableList;
    }

    public ReportedData getReportedData() {
        return this.reportedData;
    }

    public List<Item> getItems() {
        List<Item> unmodifiableList;
        synchronized (this.items) {
            unmodifiableList = Collections.unmodifiableList(new ArrayList(this.items));
        }
        return unmodifiableList;
    }

    public List<FormField> getFields() {
        List<FormField> unmodifiableList;
        synchronized (this.fields) {
            unmodifiableList = Collections.unmodifiableList(new ArrayList(this.fields));
        }
        return unmodifiableList;
    }

    public String getElementName() {
        return "x";
    }

    public String getNamespace() {
        return NAMESPACE;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setInstructions(List<String> list) {
        this.instructions = list;
    }

    public void setReportedData(ReportedData reportedData2) {
        this.reportedData = reportedData2;
    }

    public void addField(FormField formField) {
        synchronized (this.fields) {
            this.fields.add(formField);
        }
    }

    public void addInstruction(String str) {
        synchronized (this.instructions) {
            this.instructions.add(str);
        }
    }

    public void addItem(Item item) {
        synchronized (this.items) {
            this.items.add(item);
        }
    }

    public boolean hasHiddenFormTypeField() {
        boolean z = false;
        Iterator<FormField> it = this.fields.iterator();
        while (true) {
            boolean z2 = z;
            if (!it.hasNext()) {
                return z2;
            }
            FormField next = it.next();
            if (next.getVariable().equals("FORM_TYPE") && next.getType() != null && next.getType().equals(FormField.TYPE_HIDDEN)) {
                z2 = true;
            }
            z = z2;
        }
    }

    public XmlStringBuilder toXML() {
        XmlStringBuilder xmlStringBuilder = new XmlStringBuilder(this);
        xmlStringBuilder.attribute("type", getType());
        xmlStringBuilder.rightAngelBracket();
        xmlStringBuilder.optElement("title", getTitle());
        for (String element : getInstructions()) {
            xmlStringBuilder.element("instructions", element);
        }
        if (getReportedData() != null) {
            xmlStringBuilder.append(getReportedData().toXML());
        }
        for (Item xml : getItems()) {
            xmlStringBuilder.append(xml.toXML());
        }
        for (FormField xml2 : getFields()) {
            xmlStringBuilder.append(xml2.toXML());
        }
        xmlStringBuilder.closeElement(this);
        return xmlStringBuilder;
    }

    public static class ReportedData {
        public static final String ELEMENT = "reported";
        private List<FormField> fields = new ArrayList();

        public ReportedData(List<FormField> list) {
            this.fields = list;
        }

        public List<FormField> getFields() {
            return Collections.unmodifiableList(new ArrayList(this.fields));
        }

        public CharSequence toXML() {
            XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
            xmlStringBuilder.openElement(ELEMENT);
            for (FormField xml : getFields()) {
                xmlStringBuilder.append(xml.toXML());
            }
            xmlStringBuilder.closeElement(ELEMENT);
            return xmlStringBuilder;
        }
    }

    public static class Item {
        public static final String ELEMENT = "item";
        private List<FormField> fields = new ArrayList();

        public Item(List<FormField> list) {
            this.fields = list;
        }

        public List<FormField> getFields() {
            return Collections.unmodifiableList(new ArrayList(this.fields));
        }

        public CharSequence toXML() {
            XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
            xmlStringBuilder.openElement(ELEMENT);
            for (FormField xml : getFields()) {
                xmlStringBuilder.append(xml.toXML());
            }
            xmlStringBuilder.closeElement(ELEMENT);
            return xmlStringBuilder;
        }
    }
}
