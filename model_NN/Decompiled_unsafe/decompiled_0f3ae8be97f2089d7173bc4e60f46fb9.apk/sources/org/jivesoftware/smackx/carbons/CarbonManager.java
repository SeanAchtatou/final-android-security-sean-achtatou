package org.jivesoftware.smackx.carbons;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.Manager;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.IQReplyFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.carbons.packet.CarbonExtension;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;

public class CarbonManager extends Manager {
    private static Map<XMPPConnection, CarbonManager> instances = Collections.synchronizedMap(new WeakHashMap());
    /* access modifiers changed from: private */
    public volatile boolean enabled_state = false;

    static {
        XMPPConnection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(XMPPConnection xMPPConnection) {
                CarbonManager.getInstanceFor(xMPPConnection);
            }
        });
    }

    private CarbonManager(XMPPConnection xMPPConnection) {
        super(xMPPConnection);
        ServiceDiscoveryManager.getInstanceFor(xMPPConnection).addFeature(CarbonExtension.NAMESPACE);
        instances.put(xMPPConnection, this);
    }

    public static synchronized CarbonManager getInstanceFor(XMPPConnection xMPPConnection) {
        CarbonManager carbonManager;
        synchronized (CarbonManager.class) {
            carbonManager = instances.get(xMPPConnection);
            if (carbonManager == null) {
                carbonManager = new CarbonManager(xMPPConnection);
            }
        }
        return carbonManager;
    }

    private IQ carbonsEnabledIQ(final boolean z) {
        AnonymousClass2 r0 = new IQ() {
            public String getChildElementXML() {
                return "<" + (z ? "enable" : "disable") + " xmlns='" + CarbonExtension.NAMESPACE + "'/>";
            }
        };
        r0.setType(IQ.Type.SET);
        return r0;
    }

    public boolean isSupportedByServer() throws XMPPException, SmackException {
        return ServiceDiscoveryManager.getInstanceFor(connection()).supportsFeature(connection().getServiceName(), CarbonExtension.NAMESPACE);
    }

    public void sendCarbonsEnabled(final boolean z) throws SmackException.NotConnectedException {
        IQ carbonsEnabledIQ = carbonsEnabledIQ(z);
        connection().addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) {
                if (((IQ) packet).getType() == IQ.Type.RESULT) {
                    boolean unused = CarbonManager.this.enabled_state = z;
                }
                CarbonManager.this.connection().removePacketListener(this);
            }
        }, new IQReplyFilter(carbonsEnabledIQ, connection()));
        connection().sendPacket(carbonsEnabledIQ);
    }

    public synchronized void setCarbonsEnabled(boolean z) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        if (this.enabled_state != z) {
            connection().createPacketCollectorAndSend(carbonsEnabledIQ(z)).nextResultOrThrow();
            this.enabled_state = z;
        }
    }

    public void enableCarbons() throws XMPPException, SmackException {
        setCarbonsEnabled(true);
    }

    public void disableCarbons() throws XMPPException, SmackException {
        setCarbonsEnabled(false);
    }

    public boolean getCarbonsEnabled() {
        return this.enabled_state;
    }

    public static CarbonExtension getCarbon(Message message) {
        CarbonExtension carbonExtension = (CarbonExtension) message.getExtension(DeliveryReceipt.ELEMENT, CarbonExtension.NAMESPACE);
        if (carbonExtension == null) {
            return (CarbonExtension) message.getExtension("sent", CarbonExtension.NAMESPACE);
        }
        return carbonExtension;
    }

    public static void disableCarbons(Message message) {
        message.addExtension(new CarbonExtension.Private());
    }
}
