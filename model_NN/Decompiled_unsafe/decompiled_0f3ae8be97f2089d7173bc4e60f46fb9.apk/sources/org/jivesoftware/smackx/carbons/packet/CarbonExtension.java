package org.jivesoftware.smackx.carbons.packet;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smackx.forward.Forwarded;

public class CarbonExtension implements PacketExtension {
    public static final String NAMESPACE = "urn:xmpp:carbons:2";
    private Direction dir;
    private Forwarded fwd;

    public enum Direction {
        received,
        sent
    }

    public CarbonExtension(Direction direction, Forwarded forwarded) {
        this.dir = direction;
        this.fwd = forwarded;
    }

    public Direction getDirection() {
        return this.dir;
    }

    public Forwarded getForwarded() {
        return this.fwd;
    }

    public String getElementName() {
        return this.dir.toString();
    }

    public String getNamespace() {
        return NAMESPACE;
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\">");
        sb.append(this.fwd.toXML());
        sb.append("</").append(getElementName()).append(">");
        return sb.toString();
    }

    public static class Private implements PacketExtension {
        public static final String ELEMENT = "private";

        public String getElementName() {
            return ELEMENT;
        }

        public String getNamespace() {
            return CarbonExtension.NAMESPACE;
        }

        public String toXML() {
            return "<private xmlns=\"urn:xmpp:carbons:2\"/>";
        }
    }
}
