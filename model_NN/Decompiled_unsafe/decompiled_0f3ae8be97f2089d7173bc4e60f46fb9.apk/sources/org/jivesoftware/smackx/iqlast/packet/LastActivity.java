package org.jivesoftware.smackx.iqlast.packet;

import java.io.IOException;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class LastActivity extends IQ {
    public static final String NAMESPACE = "jabber:iq:last";
    public long lastActivity;
    public String message;

    public LastActivity() {
        this.lastActivity = -1;
        setType(IQ.Type.GET);
    }

    public LastActivity(String str) {
        this();
        setTo(str);
    }

    public XmlStringBuilder getChildElementXML() {
        XmlStringBuilder xmlStringBuilder = new XmlStringBuilder();
        xmlStringBuilder.halfOpenElement("query");
        xmlStringBuilder.xmlnsAttribute(NAMESPACE);
        if (this.lastActivity != -1) {
            xmlStringBuilder.attribute("seconds", Long.toString(this.lastActivity));
        }
        xmlStringBuilder.closeEmptyElement();
        return xmlStringBuilder;
    }

    public void setLastActivity(long j) {
        this.lastActivity = j;
    }

    /* access modifiers changed from: private */
    public void setMessage(String str) {
        this.message = str;
    }

    public long getIdleTime() {
        return this.lastActivity;
    }

    public String getStatusMessage() {
        return this.message;
    }

    public static class Provider implements IQProvider {
        public IQ parseIQ(XmlPullParser xmlPullParser) throws SmackException, XmlPullParserException {
            if (xmlPullParser.getEventType() != 2) {
                throw new SmackException("Parser not in proper position, or bad XML.");
            }
            LastActivity lastActivity = new LastActivity();
            String attributeValue = xmlPullParser.getAttributeValue("", "seconds");
            if (attributeValue != null) {
                try {
                    lastActivity.setLastActivity(Long.parseLong(attributeValue));
                } catch (NumberFormatException e) {
                    throw new SmackException("Could not parse last activity number", e);
                }
            }
            try {
                lastActivity.setMessage(xmlPullParser.nextText());
                return lastActivity;
            } catch (IOException e2) {
                throw new SmackException(e2);
            }
        }
    }
}
