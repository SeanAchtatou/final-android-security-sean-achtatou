package org.jivesoftware.smackx.address.packet;

import java.util.ArrayList;
import java.util.List;
import org.jivesoftware.smack.packet.PacketExtension;

public class MultipleAddresses implements PacketExtension {
    public static final String BCC = "bcc";
    public static final String CC = "cc";
    public static final String ELEMENT = "addresses";
    public static final String NAMESPACE = "http://jabber.org/protocol/address";
    public static final String NO_REPLY = "noreply";
    public static final String REPLY_ROOM = "replyroom";
    public static final String REPLY_TO = "replyto";
    public static final String TO = "to";
    private List<Address> addresses = new ArrayList();

    public void addAddress(String str, String str2, String str3, String str4, boolean z, String str5) {
        Address address = new Address(str);
        address.setJid(str2);
        address.setNode(str3);
        address.setDescription(str4);
        address.setDelivered(z);
        address.setUri(str5);
        this.addresses.add(address);
    }

    public void setNoReply() {
        this.addresses.add(new Address(NO_REPLY));
    }

    public List<Address> getAddressesOfType(String str) {
        ArrayList arrayList = new ArrayList(this.addresses.size());
        for (Address next : this.addresses) {
            if (next.getType().equals(str)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public String getElementName() {
        return ELEMENT;
    }

    public String getNamespace() {
        return NAMESPACE;
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(getElementName());
        sb.append(" xmlns=\"").append(NAMESPACE).append("\">");
        for (Address access$600 : this.addresses) {
            sb.append(access$600.toXML());
        }
        sb.append("</").append(getElementName()).append(">");
        return sb.toString();
    }

    public static class Address {
        private boolean delivered;
        private String description;
        private String jid;
        private String node;
        private String type;
        private String uri;

        private Address(String str) {
            this.type = str;
        }

        public String getType() {
            return this.type;
        }

        public String getJid() {
            return this.jid;
        }

        /* access modifiers changed from: private */
        public void setJid(String str) {
            this.jid = str;
        }

        public String getNode() {
            return this.node;
        }

        /* access modifiers changed from: private */
        public void setNode(String str) {
            this.node = str;
        }

        public String getDescription() {
            return this.description;
        }

        /* access modifiers changed from: private */
        public void setDescription(String str) {
            this.description = str;
        }

        public boolean isDelivered() {
            return this.delivered;
        }

        /* access modifiers changed from: private */
        public void setDelivered(boolean z) {
            this.delivered = z;
        }

        public String getUri() {
            return this.uri;
        }

        /* access modifiers changed from: private */
        public void setUri(String str) {
            this.uri = str;
        }

        /* access modifiers changed from: private */
        public String toXML() {
            StringBuilder sb = new StringBuilder();
            sb.append("<address type=\"");
            sb.append(this.type).append("\"");
            if (this.jid != null) {
                sb.append(" jid=\"");
                sb.append(this.jid).append("\"");
            }
            if (this.node != null) {
                sb.append(" node=\"");
                sb.append(this.node).append("\"");
            }
            if (this.description != null && this.description.trim().length() > 0) {
                sb.append(" desc=\"");
                sb.append(this.description).append("\"");
            }
            if (this.delivered) {
                sb.append(" delivered=\"true\"");
            }
            if (this.uri != null) {
                sb.append(" uri=\"");
                sb.append(this.uri).append("\"");
            }
            sb.append("/>");
            return sb.toString();
        }
    }
}
