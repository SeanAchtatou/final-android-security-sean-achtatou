package org.jivesoftware.smackx.forward.provider;

import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.jivesoftware.smackx.delay.packet.DelayInfo;
import org.jivesoftware.smackx.forward.Forwarded;
import org.xmlpull.v1.XmlPullParser;

public class ForwardedProvider implements PacketExtensionProvider {
    public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
        boolean z;
        Message message;
        DelayInfo delayInfo;
        Message message2 = null;
        boolean z2 = false;
        DelayInfo delayInfo2 = null;
        while (!z2) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("delay")) {
                    boolean z3 = z2;
                    message = message2;
                    delayInfo = (DelayInfo) PacketParserUtils.parsePacketExtension(xmlPullParser.getName(), xmlPullParser.getNamespace(), xmlPullParser);
                    z = z3;
                } else if (xmlPullParser.getName().equals("message")) {
                    delayInfo = delayInfo2;
                    z = z2;
                    message = PacketParserUtils.parseMessage(xmlPullParser);
                } else {
                    throw new Exception("Unsupported forwarded packet type: " + xmlPullParser.getName());
                }
            } else if (next != 3 || !xmlPullParser.getName().equals(Forwarded.ELEMENT_NAME)) {
                z = z2;
                message = message2;
                delayInfo = delayInfo2;
            } else {
                z = true;
                message = message2;
                delayInfo = delayInfo2;
            }
            delayInfo2 = delayInfo;
            message2 = message;
            z2 = z;
        }
        if (message2 != null) {
            return new Forwarded(delayInfo2, message2);
        }
        throw new Exception("forwarded extension must contain a packet");
    }
}
