package org.jivesoftware.smackx.disco.provider;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smackx.amp.packet.AMPExtension;
import org.jivesoftware.smackx.disco.packet.DiscoverItems;
import org.jivesoftware.smackx.xdata.packet.DataForm;
import org.xmlpull.v1.XmlPullParser;

public class DiscoverItemsProvider implements IQProvider {
    public IQ parseIQ(XmlPullParser xmlPullParser) throws Exception {
        DiscoverItems discoverItems = new DiscoverItems();
        boolean z = false;
        String str = "";
        String str2 = "";
        String str3 = "";
        String str4 = "";
        discoverItems.setNode(xmlPullParser.getAttributeValue("", "node"));
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2 && DataForm.Item.ELEMENT.equals(xmlPullParser.getName())) {
                str = xmlPullParser.getAttributeValue("", "jid");
                str2 = xmlPullParser.getAttributeValue("", "name");
                str4 = xmlPullParser.getAttributeValue("", "node");
                str3 = xmlPullParser.getAttributeValue("", AMPExtension.Action.ATTRIBUTE_NAME);
            } else if (next == 3 && DataForm.Item.ELEMENT.equals(xmlPullParser.getName())) {
                DiscoverItems.Item item = new DiscoverItems.Item(str);
                item.setName(str2);
                item.setNode(str4);
                item.setAction(str3);
                discoverItems.addItem(item);
            } else if (next == 3 && "query".equals(xmlPullParser.getName())) {
                z = true;
            }
        }
        return discoverItems;
    }
}
