package org.jivesoftware.smackx.disco.provider;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.jivesoftware.smackx.disco.packet.DiscoverInfo;
import org.xmlpull.v1.XmlPullParser;

public class DiscoverInfoProvider implements IQProvider {
    public IQ parseIQ(XmlPullParser xmlPullParser) throws Exception {
        DiscoverInfo discoverInfo = new DiscoverInfo();
        boolean z = false;
        String str = "";
        String str2 = "";
        String str3 = "";
        String str4 = "";
        String str5 = "";
        discoverInfo.setNode(xmlPullParser.getAttributeValue("", "node"));
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("identity")) {
                    str = xmlPullParser.getAttributeValue("", "category");
                    str2 = xmlPullParser.getAttributeValue("", "name");
                    str3 = xmlPullParser.getAttributeValue("", "type");
                    str5 = xmlPullParser.getAttributeValue(xmlPullParser.getNamespace("xml"), "lang");
                } else if (xmlPullParser.getName().equals("feature")) {
                    str4 = xmlPullParser.getAttributeValue("", "var");
                } else {
                    discoverInfo.addExtension(PacketParserUtils.parsePacketExtension(xmlPullParser.getName(), xmlPullParser.getNamespace(), xmlPullParser));
                }
            } else if (next == 3) {
                if (xmlPullParser.getName().equals("identity")) {
                    DiscoverInfo.Identity identity = new DiscoverInfo.Identity(str, str2, str3);
                    if (str5 != null) {
                        identity.setLanguage(str5);
                    }
                    discoverInfo.addIdentity(identity);
                }
                if (xmlPullParser.getName().equals("feature")) {
                    discoverInfo.addFeature(str4);
                }
                if (xmlPullParser.getName().equals("query")) {
                    z = true;
                }
            }
        }
        return discoverInfo;
    }
}
