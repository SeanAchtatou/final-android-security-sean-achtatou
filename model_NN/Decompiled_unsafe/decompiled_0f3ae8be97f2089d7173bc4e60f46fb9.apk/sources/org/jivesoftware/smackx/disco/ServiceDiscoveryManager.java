package org.jivesoftware.smackx.disco;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.Manager;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smackx.caps.EntityCapsManager;
import org.jivesoftware.smackx.disco.packet.DiscoverInfo;
import org.jivesoftware.smackx.disco.packet.DiscoverItems;
import org.jivesoftware.smackx.xdata.packet.DataForm;

public class ServiceDiscoveryManager extends Manager {
    private static final String DEFAULT_IDENTITY_CATEGORY = "client";
    private static final String DEFAULT_IDENTITY_NAME = "Smack";
    private static final String DEFAULT_IDENTITY_TYPE = "pc";
    private static DiscoverInfo.Identity defaultIdentity = new DiscoverInfo.Identity(DEFAULT_IDENTITY_CATEGORY, DEFAULT_IDENTITY_NAME, DEFAULT_IDENTITY_TYPE);
    private static Map<XMPPConnection, ServiceDiscoveryManager> instances = Collections.synchronizedMap(new WeakHashMap());
    private EntityCapsManager capsManager;
    private DataForm extendedInfo = null;
    private final Set<String> features = new HashSet();
    private Set<DiscoverInfo.Identity> identities = new HashSet();
    private DiscoverInfo.Identity identity = defaultIdentity;
    private Map<String, NodeInformationProvider> nodeInformationProviders = new ConcurrentHashMap();

    static {
        XMPPConnection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(XMPPConnection xMPPConnection) {
                ServiceDiscoveryManager.getInstanceFor(xMPPConnection);
            }
        });
    }

    public static void setDefaultIdentity(DiscoverInfo.Identity identity2) {
        defaultIdentity = identity2;
    }

    private ServiceDiscoveryManager(XMPPConnection xMPPConnection) {
        super(xMPPConnection);
        instances.put(xMPPConnection, this);
        addFeature(DiscoverInfo.NAMESPACE);
        addFeature(DiscoverItems.NAMESPACE);
        xMPPConnection.addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                DiscoverItems discoverItems;
                XMPPConnection access$000 = ServiceDiscoveryManager.this.connection();
                if (access$000 != null && (discoverItems = (DiscoverItems) packet) != null && discoverItems.getType() == IQ.Type.GET) {
                    DiscoverItems discoverItems2 = new DiscoverItems();
                    discoverItems2.setType(IQ.Type.RESULT);
                    discoverItems2.setTo(discoverItems.getFrom());
                    discoverItems2.setPacketID(discoverItems.getPacketID());
                    discoverItems2.setNode(discoverItems.getNode());
                    NodeInformationProvider access$100 = ServiceDiscoveryManager.this.getNodeInformationProvider(discoverItems.getNode());
                    if (access$100 != null) {
                        discoverItems2.addItems(access$100.getNodeItems());
                        discoverItems2.addExtensions(access$100.getNodePacketExtensions());
                    } else if (discoverItems.getNode() != null) {
                        discoverItems2.setType(IQ.Type.ERROR);
                        discoverItems2.setError(new XMPPError(XMPPError.Condition.item_not_found));
                    }
                    access$000.sendPacket(discoverItems2);
                }
            }
        }, new PacketTypeFilter(DiscoverItems.class));
        xMPPConnection.addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                DiscoverInfo discoverInfo;
                XMPPConnection access$200 = ServiceDiscoveryManager.this.connection();
                if (access$200 != null && (discoverInfo = (DiscoverInfo) packet) != null && discoverInfo.getType() == IQ.Type.GET) {
                    DiscoverInfo discoverInfo2 = new DiscoverInfo();
                    discoverInfo2.setType(IQ.Type.RESULT);
                    discoverInfo2.setTo(discoverInfo.getFrom());
                    discoverInfo2.setPacketID(discoverInfo.getPacketID());
                    discoverInfo2.setNode(discoverInfo.getNode());
                    if (discoverInfo.getNode() == null) {
                        ServiceDiscoveryManager.this.addDiscoverInfoTo(discoverInfo2);
                    } else {
                        NodeInformationProvider access$100 = ServiceDiscoveryManager.this.getNodeInformationProvider(discoverInfo.getNode());
                        if (access$100 != null) {
                            discoverInfo2.addFeatures(access$100.getNodeFeatures());
                            discoverInfo2.addIdentities(access$100.getNodeIdentities());
                            discoverInfo2.addExtensions(access$100.getNodePacketExtensions());
                        } else {
                            discoverInfo2.setType(IQ.Type.ERROR);
                            discoverInfo2.setError(new XMPPError(XMPPError.Condition.item_not_found));
                        }
                    }
                    access$200.sendPacket(discoverInfo2);
                }
            }
        }, new PacketTypeFilter(DiscoverInfo.class));
    }

    public String getIdentityName() {
        return this.identity.getName();
    }

    public void setIdentityName(String str) {
        this.identity.setName(str);
        renewEntityCapsVersion();
    }

    public void setIdentity(DiscoverInfo.Identity identity2) {
        if (identity2 == null) {
            throw new IllegalArgumentException("Identity can not be null");
        }
        this.identity = identity2;
        renewEntityCapsVersion();
    }

    public DiscoverInfo.Identity getIdentity() {
        return this.identity;
    }

    public String getIdentityType() {
        return this.identity.getType();
    }

    public void addIdentity(DiscoverInfo.Identity identity2) {
        this.identities.add(identity2);
        renewEntityCapsVersion();
    }

    public boolean removeIdentity(DiscoverInfo.Identity identity2) {
        if (identity2.equals(this.identity)) {
            return false;
        }
        this.identities.remove(identity2);
        renewEntityCapsVersion();
        return true;
    }

    public Set<DiscoverInfo.Identity> getIdentities() {
        HashSet hashSet = new HashSet(this.identities);
        hashSet.add(defaultIdentity);
        return Collections.unmodifiableSet(hashSet);
    }

    public static synchronized ServiceDiscoveryManager getInstanceFor(XMPPConnection xMPPConnection) {
        ServiceDiscoveryManager serviceDiscoveryManager;
        synchronized (ServiceDiscoveryManager.class) {
            serviceDiscoveryManager = instances.get(xMPPConnection);
            if (serviceDiscoveryManager == null) {
                serviceDiscoveryManager = new ServiceDiscoveryManager(xMPPConnection);
            }
        }
        return serviceDiscoveryManager;
    }

    public void addDiscoverInfoTo(DiscoverInfo discoverInfo) {
        discoverInfo.addIdentities(getIdentities());
        synchronized (this.features) {
            for (String addFeature : getFeatures()) {
                discoverInfo.addFeature(addFeature);
            }
            discoverInfo.addExtension(this.extendedInfo);
        }
    }

    /* access modifiers changed from: private */
    public NodeInformationProvider getNodeInformationProvider(String str) {
        if (str == null) {
            return null;
        }
        return this.nodeInformationProviders.get(str);
    }

    public void setNodeInformationProvider(String str, NodeInformationProvider nodeInformationProvider) {
        this.nodeInformationProviders.put(str, nodeInformationProvider);
    }

    public void removeNodeInformationProvider(String str) {
        this.nodeInformationProviders.remove(str);
    }

    public List<String> getFeatures() {
        List<String> unmodifiableList;
        synchronized (this.features) {
            unmodifiableList = Collections.unmodifiableList(new ArrayList(this.features));
        }
        return unmodifiableList;
    }

    public List<String> getFeaturesList() {
        LinkedList linkedList;
        synchronized (this.features) {
            linkedList = new LinkedList(this.features);
        }
        return linkedList;
    }

    public void addFeature(String str) {
        synchronized (this.features) {
            this.features.add(str);
            renewEntityCapsVersion();
        }
    }

    public void removeFeature(String str) {
        synchronized (this.features) {
            this.features.remove(str);
            renewEntityCapsVersion();
        }
    }

    public boolean includesFeature(String str) {
        boolean contains;
        synchronized (this.features) {
            contains = this.features.contains(str);
        }
        return contains;
    }

    public void setExtendedInfo(DataForm dataForm) {
        this.extendedInfo = dataForm;
        renewEntityCapsVersion();
    }

    public DataForm getExtendedInfo() {
        return this.extendedInfo;
    }

    public List<PacketExtension> getExtendedInfoAsList() {
        if (this.extendedInfo == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(this.extendedInfo);
        return arrayList;
    }

    public void removeExtendedInfo() {
        this.extendedInfo = null;
        renewEntityCapsVersion();
    }

    public DiscoverInfo discoverInfo(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        String str2 = null;
        if (str == null) {
            return discoverInfo(null, null);
        }
        DiscoverInfo discoverInfoByUser = EntityCapsManager.getDiscoverInfoByUser(str);
        if (discoverInfoByUser != null) {
            return discoverInfoByUser;
        }
        EntityCapsManager.NodeVerHash nodeVerHashByJid = EntityCapsManager.getNodeVerHashByJid(str);
        if (nodeVerHashByJid != null) {
            str2 = nodeVerHashByJid.getNodeVer();
        }
        DiscoverInfo discoverInfo = discoverInfo(str, str2);
        if (nodeVerHashByJid == null || !EntityCapsManager.verifyDiscoverInfoVersion(nodeVerHashByJid.getVer(), nodeVerHashByJid.getHash(), discoverInfo)) {
            return discoverInfo;
        }
        EntityCapsManager.addDiscoverInfoByNode(nodeVerHashByJid.getNodeVer(), discoverInfo);
        return discoverInfo;
    }

    public DiscoverInfo discoverInfo(String str, String str2) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        DiscoverInfo discoverInfo = new DiscoverInfo();
        discoverInfo.setType(IQ.Type.GET);
        discoverInfo.setTo(str);
        discoverInfo.setNode(str2);
        return (DiscoverInfo) connection().createPacketCollectorAndSend(discoverInfo).nextResultOrThrow();
    }

    public DiscoverItems discoverItems(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return discoverItems(str, null);
    }

    public DiscoverItems discoverItems(String str, String str2) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        DiscoverItems discoverItems = new DiscoverItems();
        discoverItems.setType(IQ.Type.GET);
        discoverItems.setTo(str);
        discoverItems.setNode(str2);
        return (DiscoverItems) connection().createPacketCollectorAndSend(discoverItems).nextResultOrThrow();
    }

    public boolean canPublishItems(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return canPublishItems(discoverInfo(str));
    }

    public static boolean canPublishItems(DiscoverInfo discoverInfo) {
        return discoverInfo.containsFeature("http://jabber.org/protocol/disco#publish");
    }

    public void publishItems(String str, DiscoverItems discoverItems) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        publishItems(str, null, discoverItems);
    }

    public void publishItems(String str, String str2, DiscoverItems discoverItems) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        discoverItems.setType(IQ.Type.SET);
        discoverItems.setTo(str);
        discoverItems.setNode(str2);
        connection().createPacketCollectorAndSend(discoverItems).nextResultOrThrow();
    }

    public boolean supportsFeature(String str, String str2) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return discoverInfo(str).containsFeature(str2);
    }

    public void setEntityCapsManager(EntityCapsManager entityCapsManager) {
        this.capsManager = entityCapsManager;
    }

    private void renewEntityCapsVersion() {
        if (this.capsManager != null && this.capsManager.entityCapsEnabled()) {
            this.capsManager.updateLocalEntityCaps();
        }
    }
}
