package org.jivesoftware.smackx.muc.packet;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.xmlpull.v1.XmlPullParser;

public class GroupChatInvitation implements PacketExtension {
    public static final String ELEMENT_NAME = "x";
    public static final String NAMESPACE = "jabber:x:conference";
    private String roomAddress;

    public GroupChatInvitation(String str) {
        this.roomAddress = str;
    }

    public String getRoomAddress() {
        return this.roomAddress;
    }

    public String getElementName() {
        return "x";
    }

    public String getNamespace() {
        return NAMESPACE;
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<x xmlns=\"jabber:x:conference\" jid=\"").append(this.roomAddress).append("\"/>");
        return sb.toString();
    }

    public static class Provider implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
            String attributeValue = xmlPullParser.getAttributeValue("", "jid");
            xmlPullParser.next();
            return new GroupChatInvitation(attributeValue);
        }
    }
}
