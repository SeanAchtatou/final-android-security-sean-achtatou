package org.jivesoftware.smackx.muc;

import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import org.jivesoftware.smack.AbstractConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.StringUtils;

class RoomListenerMultiplexor extends AbstractConnectionListener {
    private static final Map<XMPPConnection, WeakReference<RoomListenerMultiplexor>> monitors = new WeakHashMap();
    private XMPPConnection connection;
    private RoomMultiplexFilter filter;
    private RoomMultiplexListener listener;

    public static RoomListenerMultiplexor getRoomMultiplexor(XMPPConnection xMPPConnection) {
        RoomListenerMultiplexor roomListenerMultiplexor;
        synchronized (monitors) {
            if (!monitors.containsKey(xMPPConnection) || monitors.get(xMPPConnection).get() == null) {
                RoomListenerMultiplexor roomListenerMultiplexor2 = new RoomListenerMultiplexor(xMPPConnection, new RoomMultiplexFilter(), new RoomMultiplexListener());
                roomListenerMultiplexor2.init();
                monitors.put(xMPPConnection, new WeakReference(roomListenerMultiplexor2));
            }
            roomListenerMultiplexor = (RoomListenerMultiplexor) monitors.get(xMPPConnection).get();
        }
        return roomListenerMultiplexor;
    }

    private RoomListenerMultiplexor(XMPPConnection xMPPConnection, RoomMultiplexFilter roomMultiplexFilter, RoomMultiplexListener roomMultiplexListener) {
        if (xMPPConnection == null) {
            throw new IllegalArgumentException("XMPPConnection is null");
        } else if (roomMultiplexFilter == null) {
            throw new IllegalArgumentException("Filter is null");
        } else if (roomMultiplexListener == null) {
            throw new IllegalArgumentException("Listener is null");
        } else {
            this.connection = xMPPConnection;
            this.filter = roomMultiplexFilter;
            this.listener = roomMultiplexListener;
        }
    }

    public void addRoom(String str, PacketMultiplexListener packetMultiplexListener) {
        this.filter.addRoom(str);
        this.listener.addRoom(str, packetMultiplexListener);
    }

    public void connectionClosed() {
        cancel();
    }

    public void connectionClosedOnError(Exception exc) {
        cancel();
    }

    public void init() {
        this.connection.addConnectionListener(this);
        this.connection.addPacketListener(this.listener, this.filter);
    }

    public void removeRoom(String str) {
        this.filter.removeRoom(str);
        this.listener.removeRoom(str);
    }

    private void cancel() {
        this.connection.removeConnectionListener(this);
        this.connection.removePacketListener(this.listener);
    }

    private static class RoomMultiplexFilter implements PacketFilter {
        private Map<String, String> roomAddressTable;

        private RoomMultiplexFilter() {
            this.roomAddressTable = new ConcurrentHashMap();
        }

        public boolean accept(Packet packet) {
            String from = packet.getFrom();
            if (from == null) {
                return false;
            }
            return this.roomAddressTable.containsKey(StringUtils.parseBareAddress(from).toLowerCase(Locale.US));
        }

        public void addRoom(String str) {
            if (str != null) {
                this.roomAddressTable.put(str.toLowerCase(Locale.US), str);
            }
        }

        public void removeRoom(String str) {
            if (str != null) {
                this.roomAddressTable.remove(str.toLowerCase(Locale.US));
            }
        }
    }

    private static class RoomMultiplexListener implements PacketListener {
        private Map<String, PacketMultiplexListener> roomListenersByAddress;

        private RoomMultiplexListener() {
            this.roomListenersByAddress = new ConcurrentHashMap();
        }

        public void processPacket(Packet packet) throws SmackException.NotConnectedException {
            PacketMultiplexListener packetMultiplexListener;
            String from = packet.getFrom();
            if (from != null && (packetMultiplexListener = this.roomListenersByAddress.get(StringUtils.parseBareAddress(from).toLowerCase(Locale.US))) != null) {
                packetMultiplexListener.processPacket(packet);
            }
        }

        public void addRoom(String str, PacketMultiplexListener packetMultiplexListener) {
            if (str != null) {
                this.roomListenersByAddress.put(str.toLowerCase(Locale.US), packetMultiplexListener);
            }
        }

        public void removeRoom(String str) {
            if (str != null) {
                this.roomListenersByAddress.remove(str.toLowerCase(Locale.US));
            }
        }
    }
}
