package org.jivesoftware.smackx.muc.provider;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.jivesoftware.smackx.muc.packet.MUCOwner;
import org.jivesoftware.smackx.nick.packet.Nick;
import org.jivesoftware.smackx.xdata.packet.DataForm;
import org.xmlpull.v1.XmlPullParser;

public class MUCOwnerProvider implements IQProvider {
    public IQ parseIQ(XmlPullParser xmlPullParser) throws Exception {
        MUCOwner mUCOwner = new MUCOwner();
        boolean z = false;
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals(DataForm.Item.ELEMENT)) {
                    mUCOwner.addItem(parseItem(xmlPullParser));
                } else if (xmlPullParser.getName().equals("destroy")) {
                    mUCOwner.setDestroy(parseDestroy(xmlPullParser));
                } else {
                    mUCOwner.addExtension(PacketParserUtils.parsePacketExtension(xmlPullParser.getName(), xmlPullParser.getNamespace(), xmlPullParser));
                }
            } else if (next == 3 && xmlPullParser.getName().equals("query")) {
                z = true;
            }
        }
        return mUCOwner;
    }

    private MUCOwner.Item parseItem(XmlPullParser xmlPullParser) throws Exception {
        boolean z = false;
        MUCOwner.Item item = new MUCOwner.Item(xmlPullParser.getAttributeValue("", "affiliation"));
        item.setNick(xmlPullParser.getAttributeValue("", Nick.ELEMENT_NAME));
        item.setRole(xmlPullParser.getAttributeValue("", "role"));
        item.setJid(xmlPullParser.getAttributeValue("", "jid"));
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("actor")) {
                    item.setActor(xmlPullParser.getAttributeValue("", "jid"));
                }
                if (xmlPullParser.getName().equals("reason")) {
                    item.setReason(xmlPullParser.nextText());
                }
            } else if (next == 3 && xmlPullParser.getName().equals(DataForm.Item.ELEMENT)) {
                z = true;
            }
        }
        return item;
    }

    private MUCOwner.Destroy parseDestroy(XmlPullParser xmlPullParser) throws Exception {
        boolean z = false;
        MUCOwner.Destroy destroy = new MUCOwner.Destroy();
        destroy.setJid(xmlPullParser.getAttributeValue("", "jid"));
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (xmlPullParser.getName().equals("reason")) {
                    destroy.setReason(xmlPullParser.nextText());
                }
            } else if (next == 3 && xmlPullParser.getName().equals("destroy")) {
                z = true;
            }
        }
        return destroy;
    }
}
