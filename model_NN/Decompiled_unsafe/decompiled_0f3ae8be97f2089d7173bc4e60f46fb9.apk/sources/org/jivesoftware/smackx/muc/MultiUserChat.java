package org.jivesoftware.smackx.muc;

import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.AbstractConnectionListener;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.PacketInterceptor;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.FromMatchesFilter;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketExtensionFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Registration;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.disco.NodeInformationProvider;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.disco.packet.DiscoverInfo;
import org.jivesoftware.smackx.disco.packet.DiscoverItems;
import org.jivesoftware.smackx.muc.packet.MUCAdmin;
import org.jivesoftware.smackx.muc.packet.MUCInitialPresence;
import org.jivesoftware.smackx.muc.packet.MUCOwner;
import org.jivesoftware.smackx.muc.packet.MUCUser;
import org.jivesoftware.smackx.privacy.packet.PrivacyItem;
import org.jivesoftware.smackx.xdata.Form;

public class MultiUserChat {
    private static final Logger LOGGER = Logger.getLogger(MultiUserChat.class.getName());
    private static final String discoNamespace = "http://jabber.org/protocol/muc";
    private static final String discoNode = "http://jabber.org/protocol/muc#rooms";
    private static Map<XMPPConnection, List<String>> joinedRooms = new WeakHashMap();
    private XMPPConnection connection;
    private List<PacketListener> connectionListeners = new ArrayList();
    private final List<InvitationRejectionListener> invitationRejectionListeners = new ArrayList();
    private boolean joined = false;
    private ConnectionDetachedPacketCollector messageCollector;
    private PacketFilter messageFilter;
    /* access modifiers changed from: private */
    public String nickname = null;
    /* access modifiers changed from: private */
    public Map<String, Presence> occupantsMap = new ConcurrentHashMap();
    private final List<ParticipantStatusListener> participantStatusListeners = new ArrayList();
    private PacketFilter presenceFilter;
    private List<PacketInterceptor> presenceInterceptors = new ArrayList();
    /* access modifiers changed from: private */
    public String room;
    private RoomListenerMultiplexor roomListenerMultiplexor;
    /* access modifiers changed from: private */
    public String subject;
    private final List<SubjectUpdatedListener> subjectUpdatedListeners = new ArrayList();
    private final List<UserStatusListener> userStatusListeners = new ArrayList();

    static {
        XMPPConnection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(XMPPConnection xMPPConnection) {
                ServiceDiscoveryManager.getInstanceFor(xMPPConnection).addFeature(MultiUserChat.discoNamespace);
                final WeakReference weakReference = new WeakReference(xMPPConnection);
                ServiceDiscoveryManager.getInstanceFor(xMPPConnection).setNodeInformationProvider(MultiUserChat.discoNode, new NodeInformationProvider() {
                    public List<DiscoverItems.Item> getNodeItems() {
                        XMPPConnection xMPPConnection = (XMPPConnection) weakReference.get();
                        if (xMPPConnection == null) {
                            return new LinkedList();
                        }
                        ArrayList arrayList = new ArrayList();
                        for (String item : MultiUserChat.getJoinedRooms(xMPPConnection)) {
                            arrayList.add(new DiscoverItems.Item(item));
                        }
                        return arrayList;
                    }

                    public List<String> getNodeFeatures() {
                        return null;
                    }

                    public List<DiscoverInfo.Identity> getNodeIdentities() {
                        return null;
                    }

                    public List<PacketExtension> getNodePacketExtensions() {
                        return null;
                    }
                });
            }
        });
    }

    public MultiUserChat(XMPPConnection xMPPConnection, String str) {
        this.connection = xMPPConnection;
        this.room = str.toLowerCase(Locale.US);
        init();
    }

    public static boolean isServiceEnabled(XMPPConnection xMPPConnection, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return ServiceDiscoveryManager.getInstanceFor(xMPPConnection).supportsFeature(str, discoNamespace);
    }

    /* access modifiers changed from: private */
    public static List<String> getJoinedRooms(XMPPConnection xMPPConnection) {
        List<String> list = joinedRooms.get(xMPPConnection);
        return list != null ? list : Collections.emptyList();
    }

    public static List<String> getJoinedRooms(XMPPConnection xMPPConnection, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        ArrayList arrayList = new ArrayList();
        for (DiscoverItems.Item entityID : ServiceDiscoveryManager.getInstanceFor(xMPPConnection).discoverItems(str, discoNode).getItems()) {
            arrayList.add(entityID.getEntityID());
        }
        return arrayList;
    }

    public static RoomInfo getRoomInfo(XMPPConnection xMPPConnection, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return new RoomInfo(ServiceDiscoveryManager.getInstanceFor(xMPPConnection).discoverInfo(str));
    }

    public static Collection<String> getServiceNames(XMPPConnection xMPPConnection) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        ArrayList arrayList = new ArrayList();
        ServiceDiscoveryManager instanceFor = ServiceDiscoveryManager.getInstanceFor(xMPPConnection);
        for (DiscoverItems.Item next : instanceFor.discoverItems(xMPPConnection.getServiceName()).getItems()) {
            if (instanceFor.discoverInfo(next.getEntityID()).containsFeature(discoNamespace)) {
                arrayList.add(next.getEntityID());
            }
        }
        return arrayList;
    }

    public static Collection<HostedRoom> getHostedRooms(XMPPConnection xMPPConnection, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        ArrayList arrayList = new ArrayList();
        for (DiscoverItems.Item hostedRoom : ServiceDiscoveryManager.getInstanceFor(xMPPConnection).discoverItems(str).getItems()) {
            arrayList.add(new HostedRoom(hostedRoom));
        }
        return arrayList;
    }

    public String getRoom() {
        return this.room;
    }

    private Presence enter(String str, String str2, DiscussionHistory discussionHistory, long j) throws SmackException.NotConnectedException, SmackException.NoResponseException, XMPPException.XMPPErrorException {
        if (StringUtils.isNullOrEmpty(str)) {
            throw new IllegalArgumentException("Nickname must not be null or blank.");
        }
        Presence presence = new Presence(Presence.Type.available);
        presence.setTo(this.room + "/" + str);
        MUCInitialPresence mUCInitialPresence = new MUCInitialPresence();
        if (str2 != null) {
            mUCInitialPresence.setPassword(str2);
        }
        if (discussionHistory != null) {
            mUCInitialPresence.setHistory(discussionHistory.getMUCHistory());
        }
        presence.addExtension(mUCInitialPresence);
        for (PacketInterceptor interceptPacket : this.presenceInterceptors) {
            interceptPacket.interceptPacket(presence);
        }
        PacketCollector createPacketCollector = this.connection.createPacketCollector(new AndFilter(FromMatchesFilter.createFull(this.room + "/" + str), new PacketTypeFilter(Presence.class)));
        this.connection.sendPacket(presence);
        Presence presence2 = (Presence) createPacketCollector.nextResultOrThrow(j);
        this.nickname = str;
        this.joined = true;
        Object obj = joinedRooms.get(this.connection);
        if (obj == null) {
            obj = new ArrayList();
            joinedRooms.put(this.connection, obj);
        }
        obj.add(this.room);
        return presence2;
    }

    public synchronized void create(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException {
        if (this.joined) {
            throw new IllegalStateException("Creation failed - User already joined the room.");
        } else if (!createOrJoin(str)) {
            leave();
            throw new SmackException("Creation failed - Missing acknowledge of room creation.");
        }
    }

    public synchronized boolean createOrJoin(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException {
        boolean z;
        if (this.joined) {
            throw new IllegalStateException("Creation failed - User already joined the room.");
        }
        MUCUser mUCUserExtension = getMUCUserExtension(enter(str, null, null, this.connection.getPacketReplyTimeout()));
        if (mUCUserExtension == null || mUCUserExtension.getStatus() == null || !"201".equals(mUCUserExtension.getStatus().getCode())) {
            z = false;
        } else {
            z = true;
        }
        return z;
    }

    public void join(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        join(str, null, null, this.connection.getPacketReplyTimeout());
    }

    public void join(String str, String str2) throws XMPPException.XMPPErrorException, SmackException {
        join(str, str2, null, this.connection.getPacketReplyTimeout());
    }

    public synchronized void join(String str, String str2, DiscussionHistory discussionHistory, long j) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        if (this.joined) {
            leave();
        }
        enter(str, str2, discussionHistory, j);
    }

    public boolean isJoined() {
        return this.joined;
    }

    public synchronized void leave() throws SmackException.NotConnectedException {
        if (this.joined) {
            Presence presence = new Presence(Presence.Type.unavailable);
            presence.setTo(this.room + "/" + this.nickname);
            for (PacketInterceptor interceptPacket : this.presenceInterceptors) {
                interceptPacket.interceptPacket(presence);
            }
            this.connection.sendPacket(presence);
            this.occupantsMap.clear();
            this.nickname = null;
            this.joined = false;
            userHasLeft();
        }
    }

    public Form getConfigurationForm() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        MUCOwner mUCOwner = new MUCOwner();
        mUCOwner.setTo(this.room);
        mUCOwner.setType(IQ.Type.GET);
        return Form.getFormFrom((IQ) this.connection.createPacketCollectorAndSend(mUCOwner).nextResultOrThrow());
    }

    public void sendConfigurationForm(Form form) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        MUCOwner mUCOwner = new MUCOwner();
        mUCOwner.setTo(this.room);
        mUCOwner.setType(IQ.Type.SET);
        mUCOwner.addExtension(form.getDataFormToSend());
        this.connection.createPacketCollectorAndSend(mUCOwner).nextResultOrThrow();
    }

    public Form getRegistrationForm() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Registration registration = new Registration();
        registration.setType(IQ.Type.GET);
        registration.setTo(this.room);
        return Form.getFormFrom((IQ) this.connection.createPacketCollectorAndSend(registration).nextResultOrThrow());
    }

    public void sendRegistrationForm(Form form) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Registration registration = new Registration();
        registration.setType(IQ.Type.SET);
        registration.setTo(this.room);
        registration.addExtension(form.getDataFormToSend());
        this.connection.createPacketCollectorAndSend(registration).nextResultOrThrow();
    }

    public void destroy(String str, String str2) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        MUCOwner mUCOwner = new MUCOwner();
        mUCOwner.setTo(this.room);
        mUCOwner.setType(IQ.Type.SET);
        MUCOwner.Destroy destroy = new MUCOwner.Destroy();
        destroy.setReason(str);
        destroy.setJid(str2);
        mUCOwner.setDestroy(destroy);
        this.connection.createPacketCollectorAndSend(mUCOwner).nextResultOrThrow();
        this.occupantsMap.clear();
        this.nickname = null;
        this.joined = false;
        userHasLeft();
    }

    public void invite(String str, String str2) throws SmackException.NotConnectedException {
        invite(new Message(), str, str2);
    }

    public void invite(Message message, String str, String str2) throws SmackException.NotConnectedException {
        message.setTo(this.room);
        MUCUser mUCUser = new MUCUser();
        MUCUser.Invite invite = new MUCUser.Invite();
        invite.setTo(str);
        invite.setReason(str2);
        mUCUser.setInvite(invite);
        message.addExtension(mUCUser);
        this.connection.sendPacket(message);
    }

    public static void decline(XMPPConnection xMPPConnection, String str, String str2, String str3) throws SmackException.NotConnectedException {
        Message message = new Message(str);
        MUCUser mUCUser = new MUCUser();
        MUCUser.Decline decline = new MUCUser.Decline();
        decline.setTo(str2);
        decline.setReason(str3);
        mUCUser.setDecline(decline);
        message.addExtension(mUCUser);
        xMPPConnection.sendPacket(message);
    }

    public static void addInvitationListener(XMPPConnection xMPPConnection, InvitationListener invitationListener) {
        InvitationsMonitor.getInvitationsMonitor(xMPPConnection).addInvitationListener(invitationListener);
    }

    public static void removeInvitationListener(XMPPConnection xMPPConnection, InvitationListener invitationListener) {
        InvitationsMonitor.getInvitationsMonitor(xMPPConnection).removeInvitationListener(invitationListener);
    }

    public void addInvitationRejectionListener(InvitationRejectionListener invitationRejectionListener) {
        synchronized (this.invitationRejectionListeners) {
            if (!this.invitationRejectionListeners.contains(invitationRejectionListener)) {
                this.invitationRejectionListeners.add(invitationRejectionListener);
            }
        }
    }

    public void removeInvitationRejectionListener(InvitationRejectionListener invitationRejectionListener) {
        synchronized (this.invitationRejectionListeners) {
            this.invitationRejectionListeners.remove(invitationRejectionListener);
        }
    }

    /* access modifiers changed from: private */
    public void fireInvitationRejectionListeners(String str, String str2) {
        InvitationRejectionListener[] invitationRejectionListenerArr;
        synchronized (this.invitationRejectionListeners) {
            invitationRejectionListenerArr = new InvitationRejectionListener[this.invitationRejectionListeners.size()];
            this.invitationRejectionListeners.toArray(invitationRejectionListenerArr);
        }
        for (InvitationRejectionListener invitationDeclined : invitationRejectionListenerArr) {
            invitationDeclined.invitationDeclined(str, str2);
        }
    }

    public void addSubjectUpdatedListener(SubjectUpdatedListener subjectUpdatedListener) {
        synchronized (this.subjectUpdatedListeners) {
            if (!this.subjectUpdatedListeners.contains(subjectUpdatedListener)) {
                this.subjectUpdatedListeners.add(subjectUpdatedListener);
            }
        }
    }

    public void removeSubjectUpdatedListener(SubjectUpdatedListener subjectUpdatedListener) {
        synchronized (this.subjectUpdatedListeners) {
            this.subjectUpdatedListeners.remove(subjectUpdatedListener);
        }
    }

    /* access modifiers changed from: private */
    public void fireSubjectUpdatedListeners(String str, String str2) {
        SubjectUpdatedListener[] subjectUpdatedListenerArr;
        synchronized (this.subjectUpdatedListeners) {
            subjectUpdatedListenerArr = new SubjectUpdatedListener[this.subjectUpdatedListeners.size()];
            this.subjectUpdatedListeners.toArray(subjectUpdatedListenerArr);
        }
        for (SubjectUpdatedListener subjectUpdated : subjectUpdatedListenerArr) {
            subjectUpdated.subjectUpdated(str, str2);
        }
    }

    public void addPresenceInterceptor(PacketInterceptor packetInterceptor) {
        this.presenceInterceptors.add(packetInterceptor);
    }

    public void removePresenceInterceptor(PacketInterceptor packetInterceptor) {
        this.presenceInterceptors.remove(packetInterceptor);
    }

    public String getSubject() {
        return this.subject;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, org.jivesoftware.smack.XMPPException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public String getReservedNickname() throws SmackException {
        try {
            Iterator<DiscoverInfo.Identity> it = ServiceDiscoveryManager.getInstanceFor(this.connection).discoverInfo(this.room, "x-roomuser-item").getIdentities().iterator();
            if (it.hasNext()) {
                return it.next().getName();
            }
        } catch (XMPPException e) {
            LOGGER.log(Level.SEVERE, "Error retrieving room nickname", (Throwable) e);
        }
        return null;
    }

    public String getNickname() {
        return this.nickname;
    }

    public void changeNickname(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        if (StringUtils.isNullOrEmpty(str)) {
            throw new IllegalArgumentException("Nickname must not be null or blank.");
        } else if (!this.joined) {
            throw new IllegalStateException("Must be logged into the room to change nickname.");
        } else {
            Presence presence = new Presence(Presence.Type.available);
            presence.setTo(this.room + "/" + str);
            for (PacketInterceptor interceptPacket : this.presenceInterceptors) {
                interceptPacket.interceptPacket(presence);
            }
            PacketCollector createPacketCollector = this.connection.createPacketCollector(new AndFilter(FromMatchesFilter.createFull(this.room + "/" + str), new PacketTypeFilter(Presence.class)));
            this.connection.sendPacket(presence);
            createPacketCollector.nextResultOrThrow();
            this.nickname = str;
        }
    }

    public void changeAvailabilityStatus(String str, Presence.Mode mode) throws SmackException.NotConnectedException {
        if (StringUtils.isNullOrEmpty(this.nickname)) {
            throw new IllegalArgumentException("Nickname must not be null or blank.");
        } else if (!this.joined) {
            throw new IllegalStateException("Must be logged into the room to change the availability status.");
        } else {
            Presence presence = new Presence(Presence.Type.available);
            presence.setStatus(str);
            presence.setMode(mode);
            presence.setTo(this.room + "/" + this.nickname);
            for (PacketInterceptor interceptPacket : this.presenceInterceptors) {
                interceptPacket.interceptPacket(presence);
            }
            this.connection.sendPacket(presence);
        }
    }

    public void kickParticipant(String str, String str2) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeRole(str, PrivacyItem.SUBSCRIPTION_NONE, str2);
    }

    public void grantVoice(Collection<String> collection) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeRole(collection, "participant");
    }

    public void grantVoice(String str) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeRole(str, "participant", null);
    }

    public void revokeVoice(Collection<String> collection) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeRole(collection, "visitor");
    }

    public void revokeVoice(String str) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeRole(str, "visitor", null);
    }

    public void banUsers(Collection<String> collection) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeAffiliationByAdmin(collection, "outcast");
    }

    public void banUser(String str, String str2) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeAffiliationByAdmin(str, "outcast", str2);
    }

    public void grantMembership(Collection<String> collection) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeAffiliationByAdmin(collection, "member");
    }

    public void grantMembership(String str) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeAffiliationByAdmin(str, "member", null);
    }

    public void revokeMembership(Collection<String> collection) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeAffiliationByAdmin(collection, PrivacyItem.SUBSCRIPTION_NONE);
    }

    public void revokeMembership(String str) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeAffiliationByAdmin(str, PrivacyItem.SUBSCRIPTION_NONE, null);
    }

    public void grantModerator(Collection<String> collection) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeRole(collection, "moderator");
    }

    public void grantModerator(String str) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeRole(str, "moderator", null);
    }

    public void revokeModerator(Collection<String> collection) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeRole(collection, "participant");
    }

    public void revokeModerator(String str) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeRole(str, "participant", null);
    }

    public void grantOwnership(Collection<String> collection) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeAffiliationByAdmin(collection, "owner");
    }

    public void grantOwnership(String str) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeAffiliationByAdmin(str, "owner", null);
    }

    public void revokeOwnership(Collection<String> collection) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeAffiliationByAdmin(collection, "admin");
    }

    public void revokeOwnership(String str) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeAffiliationByAdmin(str, "admin", null);
    }

    public void grantAdmin(Collection<String> collection) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeAffiliationByOwner(collection, "admin");
    }

    public void grantAdmin(String str) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeAffiliationByOwner(str, "admin");
    }

    public void revokeAdmin(Collection<String> collection) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeAffiliationByOwner(collection, "member");
    }

    public void revokeAdmin(String str) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        changeAffiliationByOwner(str, "member");
    }

    private void changeAffiliationByOwner(String str, String str2) throws XMPPException.XMPPErrorException, SmackException.NoResponseException, SmackException.NotConnectedException {
        MUCOwner mUCOwner = new MUCOwner();
        mUCOwner.setTo(this.room);
        mUCOwner.setType(IQ.Type.SET);
        MUCOwner.Item item = new MUCOwner.Item(str2);
        item.setJid(str);
        mUCOwner.addItem(item);
        this.connection.createPacketCollectorAndSend(mUCOwner).nextResultOrThrow();
    }

    private void changeAffiliationByOwner(Collection<String> collection, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        MUCOwner mUCOwner = new MUCOwner();
        mUCOwner.setTo(this.room);
        mUCOwner.setType(IQ.Type.SET);
        for (String jid : collection) {
            MUCOwner.Item item = new MUCOwner.Item(str);
            item.setJid(jid);
            mUCOwner.addItem(item);
        }
        this.connection.createPacketCollectorAndSend(mUCOwner).nextResultOrThrow();
    }

    private void changeAffiliationByAdmin(String str, String str2, String str3) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        MUCAdmin mUCAdmin = new MUCAdmin();
        mUCAdmin.setTo(this.room);
        mUCAdmin.setType(IQ.Type.SET);
        MUCAdmin.Item item = new MUCAdmin.Item(str2, null);
        item.setJid(str);
        item.setReason(str3);
        mUCAdmin.addItem(item);
        this.connection.createPacketCollectorAndSend(mUCAdmin).nextResultOrThrow();
    }

    private void changeAffiliationByAdmin(Collection<String> collection, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        MUCAdmin mUCAdmin = new MUCAdmin();
        mUCAdmin.setTo(this.room);
        mUCAdmin.setType(IQ.Type.SET);
        for (String jid : collection) {
            MUCAdmin.Item item = new MUCAdmin.Item(str, null);
            item.setJid(jid);
            mUCAdmin.addItem(item);
        }
        this.connection.createPacketCollectorAndSend(mUCAdmin).nextResultOrThrow();
    }

    private void changeRole(String str, String str2, String str3) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        MUCAdmin mUCAdmin = new MUCAdmin();
        mUCAdmin.setTo(this.room);
        mUCAdmin.setType(IQ.Type.SET);
        MUCAdmin.Item item = new MUCAdmin.Item(null, str2);
        item.setNick(str);
        item.setReason(str3);
        mUCAdmin.addItem(item);
        this.connection.createPacketCollectorAndSend(mUCAdmin).nextResultOrThrow();
    }

    private void changeRole(Collection<String> collection, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        MUCAdmin mUCAdmin = new MUCAdmin();
        mUCAdmin.setTo(this.room);
        mUCAdmin.setType(IQ.Type.SET);
        for (String nick : collection) {
            MUCAdmin.Item item = new MUCAdmin.Item(null, str);
            item.setNick(nick);
            mUCAdmin.addItem(item);
        }
        this.connection.createPacketCollectorAndSend(mUCAdmin).nextResultOrThrow();
    }

    public int getOccupantsCount() {
        return this.occupantsMap.size();
    }

    public List<String> getOccupants() {
        return Collections.unmodifiableList(new ArrayList(this.occupantsMap.keySet()));
    }

    public Presence getOccupantPresence(String str) {
        return this.occupantsMap.get(str);
    }

    public Occupant getOccupant(String str) {
        Presence presence = this.occupantsMap.get(str);
        if (presence != null) {
            return new Occupant(presence);
        }
        return null;
    }

    public void addParticipantListener(PacketListener packetListener) {
        this.connection.addPacketListener(packetListener, this.presenceFilter);
        this.connectionListeners.add(packetListener);
    }

    public void removeParticipantListener(PacketListener packetListener) {
        this.connection.removePacketListener(packetListener);
        this.connectionListeners.remove(packetListener);
    }

    public Collection<Affiliate> getOwners() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return getAffiliatesByAdmin("owner");
    }

    public Collection<Affiliate> getAdmins() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return getAffiliatesByAdmin("admin");
    }

    public Collection<Affiliate> getMembers() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return getAffiliatesByAdmin("member");
    }

    public Collection<Affiliate> getOutcasts() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return getAffiliatesByAdmin("outcast");
    }

    private Collection<Affiliate> getAffiliatesByAdmin(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        MUCAdmin mUCAdmin = new MUCAdmin();
        mUCAdmin.setTo(this.room);
        mUCAdmin.setType(IQ.Type.GET);
        mUCAdmin.addItem(new MUCAdmin.Item(str, null));
        ArrayList arrayList = new ArrayList();
        for (MUCAdmin.Item affiliate : ((MUCAdmin) this.connection.createPacketCollectorAndSend(mUCAdmin).nextResultOrThrow()).getItems()) {
            arrayList.add(new Affiliate(affiliate));
        }
        return arrayList;
    }

    public Collection<Occupant> getModerators() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return getOccupants("moderator");
    }

    public Collection<Occupant> getParticipants() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return getOccupants("participant");
    }

    private Collection<Occupant> getOccupants(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        MUCAdmin mUCAdmin = new MUCAdmin();
        mUCAdmin.setTo(this.room);
        mUCAdmin.setType(IQ.Type.GET);
        mUCAdmin.addItem(new MUCAdmin.Item(null, str));
        ArrayList arrayList = new ArrayList();
        for (MUCAdmin.Item occupant : ((MUCAdmin) this.connection.createPacketCollectorAndSend(mUCAdmin).nextResultOrThrow()).getItems()) {
            arrayList.add(new Occupant(occupant));
        }
        return arrayList;
    }

    public void sendMessage(String str) throws XMPPException, SmackException.NotConnectedException {
        Message message = new Message(this.room, Message.Type.groupchat);
        message.setBody(str);
        this.connection.sendPacket(message);
    }

    public Chat createPrivateChat(String str, MessageListener messageListener) {
        return ChatManager.getInstanceFor(this.connection).createChat(str, messageListener);
    }

    public Message createMessage() {
        return new Message(this.room, Message.Type.groupchat);
    }

    public void sendMessage(Message message) throws XMPPException, SmackException.NotConnectedException {
        this.connection.sendPacket(message);
    }

    public Message pollMessage() {
        return (Message) this.messageCollector.pollResult();
    }

    public Message nextMessage() {
        return (Message) this.messageCollector.nextResult();
    }

    public Message nextMessage(long j) {
        return (Message) this.messageCollector.nextResult(j);
    }

    public void addMessageListener(PacketListener packetListener) {
        this.connection.addPacketListener(packetListener, this.messageFilter);
        this.connectionListeners.add(packetListener);
    }

    public void removeMessageListener(PacketListener packetListener) {
        this.connection.removePacketListener(packetListener);
        this.connectionListeners.remove(packetListener);
    }

    public void changeSubject(final String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Message message = new Message(this.room, Message.Type.groupchat);
        message.setSubject(str);
        PacketCollector createPacketCollector = this.connection.createPacketCollector(new AndFilter(new AndFilter(FromMatchesFilter.create(this.room), new PacketTypeFilter(Message.class)), new PacketFilter() {
            public boolean accept(Packet packet) {
                return str.equals(((Message) packet).getSubject());
            }
        }));
        this.connection.sendPacket(message);
        createPacketCollector.nextResultOrThrow();
    }

    private synchronized void userHasLeft() {
        List list = joinedRooms.get(this.connection);
        if (list != null) {
            list.remove(this.room);
            cleanup();
        }
    }

    /* access modifiers changed from: private */
    public MUCUser getMUCUserExtension(Packet packet) {
        if (packet != null) {
            return (MUCUser) packet.getExtension("x", "http://jabber.org/protocol/muc#user");
        }
        return null;
    }

    public void addUserStatusListener(UserStatusListener userStatusListener) {
        synchronized (this.userStatusListeners) {
            if (!this.userStatusListeners.contains(userStatusListener)) {
                this.userStatusListeners.add(userStatusListener);
            }
        }
    }

    public void removeUserStatusListener(UserStatusListener userStatusListener) {
        synchronized (this.userStatusListeners) {
            this.userStatusListeners.remove(userStatusListener);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.NoSuchMethodException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.reflect.InvocationTargetException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.IllegalAccessException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void fireUserStatusListeners(String str, Object[] objArr) {
        UserStatusListener[] userStatusListenerArr;
        synchronized (this.userStatusListeners) {
            userStatusListenerArr = new UserStatusListener[this.userStatusListeners.size()];
            this.userStatusListeners.toArray(userStatusListenerArr);
        }
        Class[] clsArr = new Class[objArr.length];
        for (int i = 0; i < objArr.length; i++) {
            clsArr[i] = objArr[i].getClass();
        }
        try {
            Method declaredMethod = UserStatusListener.class.getDeclaredMethod(str, clsArr);
            for (UserStatusListener invoke : userStatusListenerArr) {
                declaredMethod.invoke(invoke, objArr);
            }
        } catch (NoSuchMethodException e) {
            LOGGER.log(Level.SEVERE, "Failed to invoke method on UserStatusListener", (Throwable) e);
        } catch (InvocationTargetException e2) {
            LOGGER.log(Level.SEVERE, "Failed to invoke method on UserStatusListener", (Throwable) e2);
        } catch (IllegalAccessException e3) {
            LOGGER.log(Level.SEVERE, "Failed to invoke method on UserStatusListener", (Throwable) e3);
        }
    }

    public void addParticipantStatusListener(ParticipantStatusListener participantStatusListener) {
        synchronized (this.participantStatusListeners) {
            if (!this.participantStatusListeners.contains(participantStatusListener)) {
                this.participantStatusListeners.add(participantStatusListener);
            }
        }
    }

    public void removeParticipantStatusListener(ParticipantStatusListener participantStatusListener) {
        synchronized (this.participantStatusListeners) {
            this.participantStatusListeners.remove(participantStatusListener);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.NoSuchMethodException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.reflect.InvocationTargetException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.IllegalAccessException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: private */
    public void fireParticipantStatusListeners(String str, List<String> list) {
        ParticipantStatusListener[] participantStatusListenerArr;
        synchronized (this.participantStatusListeners) {
            participantStatusListenerArr = new ParticipantStatusListener[this.participantStatusListeners.size()];
            this.participantStatusListeners.toArray(participantStatusListenerArr);
        }
        try {
            Class[] clsArr = new Class[list.size()];
            for (int i = 0; i < list.size(); i++) {
                clsArr[i] = String.class;
            }
            Method declaredMethod = ParticipantStatusListener.class.getDeclaredMethod(str, clsArr);
            for (ParticipantStatusListener invoke : participantStatusListenerArr) {
                declaredMethod.invoke(invoke, list.toArray());
            }
        } catch (NoSuchMethodException e) {
            LOGGER.log(Level.SEVERE, "Failed to invoke method on ParticipantStatusListener", (Throwable) e);
        } catch (InvocationTargetException e2) {
            LOGGER.log(Level.SEVERE, "Failed to invoke method on ParticipantStatusListener", (Throwable) e2);
        } catch (IllegalAccessException e3) {
            LOGGER.log(Level.SEVERE, "Failed to invoke method on ParticipantStatusListener", (Throwable) e3);
        }
    }

    private void init() {
        this.messageFilter = new AndFilter(FromMatchesFilter.create(this.room), new MessageTypeFilter(Message.Type.groupchat));
        this.presenceFilter = new AndFilter(FromMatchesFilter.create(this.room), new PacketTypeFilter(Presence.class));
        this.messageCollector = new ConnectionDetachedPacketCollector();
        AnonymousClass3 r0 = new PacketListener() {
            public void processPacket(Packet packet) {
                Message message = (Message) packet;
                String unused = MultiUserChat.this.subject = message.getSubject();
                MultiUserChat.this.fireSubjectUpdatedListeners(message.getSubject(), message.getFrom());
            }
        };
        PacketMultiplexListener packetMultiplexListener = new PacketMultiplexListener(this.messageCollector, new PacketListener() {
            public void processPacket(Packet packet) {
                Presence presence = (Presence) packet;
                String from = presence.getFrom();
                String str = MultiUserChat.this.room + "/" + MultiUserChat.this.nickname;
                boolean equals = presence.getFrom().equals(str);
                if (presence.getType() == Presence.Type.available) {
                    Presence presence2 = (Presence) MultiUserChat.this.occupantsMap.put(from, presence);
                    if (presence2 != null) {
                        MUCUser access$600 = MultiUserChat.this.getMUCUserExtension(presence2);
                        String affiliation = access$600.getItem().getAffiliation();
                        String role = access$600.getItem().getRole();
                        MUCUser access$6002 = MultiUserChat.this.getMUCUserExtension(presence);
                        String affiliation2 = access$6002.getItem().getAffiliation();
                        MultiUserChat.this.checkRoleModifications(role, access$6002.getItem().getRole(), equals, from);
                        MultiUserChat.this.checkAffiliationModifications(affiliation, affiliation2, equals, from);
                    } else if (!equals) {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(from);
                        MultiUserChat.this.fireParticipantStatusListeners("joined", arrayList);
                    }
                } else if (presence.getType() == Presence.Type.unavailable) {
                    MultiUserChat.this.occupantsMap.remove(from);
                    MUCUser access$6003 = MultiUserChat.this.getMUCUserExtension(presence);
                    if (access$6003 != null && access$6003.getStatus() != null) {
                        MultiUserChat.this.checkPresenceCode(access$6003.getStatus().getCode(), presence.getFrom().equals(str), access$6003, from);
                    } else if (!equals) {
                        ArrayList arrayList2 = new ArrayList();
                        arrayList2.add(from);
                        MultiUserChat.this.fireParticipantStatusListeners("left", arrayList2);
                    }
                }
            }
        }, r0, new PacketListener() {
            public void processPacket(Packet packet) {
                MUCUser access$600 = MultiUserChat.this.getMUCUserExtension(packet);
                if (access$600.getDecline() != null && ((Message) packet).getType() != Message.Type.error) {
                    MultiUserChat.this.fireInvitationRejectionListeners(access$600.getDecline().getFrom(), access$600.getDecline().getReason());
                }
            }
        });
        this.roomListenerMultiplexor = RoomListenerMultiplexor.getRoomMultiplexor(this.connection);
        this.roomListenerMultiplexor.addRoom(this.room, packetMultiplexListener);
    }

    /* access modifiers changed from: private */
    public void checkRoleModifications(String str, String str2, boolean z, String str3) {
        if (("visitor".equals(str) || PrivacyItem.SUBSCRIPTION_NONE.equals(str)) && "participant".equals(str2)) {
            if (z) {
                fireUserStatusListeners("voiceGranted", new Object[0]);
            } else {
                ArrayList arrayList = new ArrayList();
                arrayList.add(str3);
                fireParticipantStatusListeners("voiceGranted", arrayList);
            }
        } else if ("participant".equals(str) && ("visitor".equals(str2) || PrivacyItem.SUBSCRIPTION_NONE.equals(str2))) {
            if (z) {
                fireUserStatusListeners("voiceRevoked", new Object[0]);
            } else {
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add(str3);
                fireParticipantStatusListeners("voiceRevoked", arrayList2);
            }
        }
        if (!"moderator".equals(str) && "moderator".equals(str2)) {
            if ("visitor".equals(str) || PrivacyItem.SUBSCRIPTION_NONE.equals(str)) {
                if (z) {
                    fireUserStatusListeners("voiceGranted", new Object[0]);
                } else {
                    ArrayList arrayList3 = new ArrayList();
                    arrayList3.add(str3);
                    fireParticipantStatusListeners("voiceGranted", arrayList3);
                }
            }
            if (z) {
                fireUserStatusListeners("moderatorGranted", new Object[0]);
                return;
            }
            ArrayList arrayList4 = new ArrayList();
            arrayList4.add(str3);
            fireParticipantStatusListeners("moderatorGranted", arrayList4);
        } else if ("moderator".equals(str) && !"moderator".equals(str2)) {
            if ("visitor".equals(str2) || PrivacyItem.SUBSCRIPTION_NONE.equals(str2)) {
                if (z) {
                    fireUserStatusListeners("voiceRevoked", new Object[0]);
                } else {
                    ArrayList arrayList5 = new ArrayList();
                    arrayList5.add(str3);
                    fireParticipantStatusListeners("voiceRevoked", arrayList5);
                }
            }
            if (z) {
                fireUserStatusListeners("moderatorRevoked", new Object[0]);
                return;
            }
            ArrayList arrayList6 = new ArrayList();
            arrayList6.add(str3);
            fireParticipantStatusListeners("moderatorRevoked", arrayList6);
        }
    }

    /* access modifiers changed from: private */
    public void checkAffiliationModifications(String str, String str2, boolean z, String str3) {
        if (!"owner".equals(str) || "owner".equals(str2)) {
            if (!"admin".equals(str) || "admin".equals(str2)) {
                if ("member".equals(str) && !"member".equals(str2)) {
                    if (z) {
                        fireUserStatusListeners("membershipRevoked", new Object[0]);
                    } else {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(str3);
                        fireParticipantStatusListeners("membershipRevoked", arrayList);
                    }
                }
            } else if (z) {
                fireUserStatusListeners("adminRevoked", new Object[0]);
            } else {
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add(str3);
                fireParticipantStatusListeners("adminRevoked", arrayList2);
            }
        } else if (z) {
            fireUserStatusListeners("ownershipRevoked", new Object[0]);
        } else {
            ArrayList arrayList3 = new ArrayList();
            arrayList3.add(str3);
            fireParticipantStatusListeners("ownershipRevoked", arrayList3);
        }
        if ("owner".equals(str) || !"owner".equals(str2)) {
            if ("admin".equals(str) || !"admin".equals(str2)) {
                if (!"member".equals(str) && "member".equals(str2)) {
                    if (z) {
                        fireUserStatusListeners("membershipGranted", new Object[0]);
                        return;
                    }
                    ArrayList arrayList4 = new ArrayList();
                    arrayList4.add(str3);
                    fireParticipantStatusListeners("membershipGranted", arrayList4);
                }
            } else if (z) {
                fireUserStatusListeners("adminGranted", new Object[0]);
            } else {
                ArrayList arrayList5 = new ArrayList();
                arrayList5.add(str3);
                fireParticipantStatusListeners("adminGranted", arrayList5);
            }
        } else if (z) {
            fireUserStatusListeners("ownershipGranted", new Object[0]);
        } else {
            ArrayList arrayList6 = new ArrayList();
            arrayList6.add(str3);
            fireParticipantStatusListeners("ownershipGranted", arrayList6);
        }
    }

    /* access modifiers changed from: private */
    public void checkPresenceCode(String str, boolean z, MUCUser mUCUser, String str2) {
        if ("307".equals(str)) {
            if (z) {
                this.joined = false;
                fireUserStatusListeners("kicked", new Object[]{mUCUser.getItem().getActor(), mUCUser.getItem().getReason()});
                this.occupantsMap.clear();
                this.nickname = null;
                userHasLeft();
                return;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.add(str2);
            arrayList.add(mUCUser.getItem().getActor());
            arrayList.add(mUCUser.getItem().getReason());
            fireParticipantStatusListeners("kicked", arrayList);
        } else if ("301".equals(str)) {
            if (z) {
                this.joined = false;
                fireUserStatusListeners("banned", new Object[]{mUCUser.getItem().getActor(), mUCUser.getItem().getReason()});
                this.occupantsMap.clear();
                this.nickname = null;
                userHasLeft();
                return;
            }
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(str2);
            arrayList2.add(mUCUser.getItem().getActor());
            arrayList2.add(mUCUser.getItem().getReason());
            fireParticipantStatusListeners("banned", arrayList2);
        } else if ("321".equals(str)) {
            if (z) {
                this.joined = false;
                fireUserStatusListeners("membershipRevoked", new Object[0]);
                this.occupantsMap.clear();
                this.nickname = null;
                userHasLeft();
            }
        } else if ("303".equals(str)) {
            ArrayList arrayList3 = new ArrayList();
            arrayList3.add(str2);
            arrayList3.add(mUCUser.getItem().getNick());
            fireParticipantStatusListeners("nicknameChanged", arrayList3);
        }
    }

    private void cleanup() {
        try {
            if (this.connection != null) {
                this.roomListenerMultiplexor.removeRoom(this.room);
                for (PacketListener removePacketListener : this.connectionListeners) {
                    this.connection.removePacketListener(removePacketListener);
                }
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        cleanup();
        super.finalize();
    }

    private static class InvitationsMonitor extends AbstractConnectionListener {
        private static final Map<XMPPConnection, WeakReference<InvitationsMonitor>> monitors = new WeakHashMap();
        private XMPPConnection connection;
        private PacketFilter invitationFilter;
        private PacketListener invitationPacketListener;
        private final List<InvitationListener> invitationsListeners = new ArrayList();

        public static InvitationsMonitor getInvitationsMonitor(XMPPConnection xMPPConnection) {
            InvitationsMonitor invitationsMonitor;
            synchronized (monitors) {
                if (!monitors.containsKey(xMPPConnection) || monitors.get(xMPPConnection).get() == null) {
                    invitationsMonitor = new InvitationsMonitor(xMPPConnection);
                    monitors.put(xMPPConnection, new WeakReference(invitationsMonitor));
                } else {
                    invitationsMonitor = (InvitationsMonitor) monitors.get(xMPPConnection).get();
                }
            }
            return invitationsMonitor;
        }

        private InvitationsMonitor(XMPPConnection xMPPConnection) {
            this.connection = xMPPConnection;
        }

        public void addInvitationListener(InvitationListener invitationListener) {
            synchronized (this.invitationsListeners) {
                if (this.invitationsListeners.size() == 0) {
                    init();
                }
                if (!this.invitationsListeners.contains(invitationListener)) {
                    this.invitationsListeners.add(invitationListener);
                }
            }
        }

        public void removeInvitationListener(InvitationListener invitationListener) {
            synchronized (this.invitationsListeners) {
                if (this.invitationsListeners.contains(invitationListener)) {
                    this.invitationsListeners.remove(invitationListener);
                }
                if (this.invitationsListeners.size() == 0) {
                    cancel();
                }
            }
        }

        /* access modifiers changed from: private */
        public void fireInvitationListeners(String str, String str2, String str3, String str4, Message message) {
            InvitationListener[] invitationListenerArr;
            synchronized (this.invitationsListeners) {
                invitationListenerArr = new InvitationListener[this.invitationsListeners.size()];
                this.invitationsListeners.toArray(invitationListenerArr);
            }
            for (InvitationListener invitationReceived : invitationListenerArr) {
                invitationReceived.invitationReceived(this.connection, str, str2, str3, str4, message);
            }
        }

        public void connectionClosed() {
            cancel();
        }

        private void init() {
            this.invitationFilter = new PacketExtensionFilter("x", "http://jabber.org/protocol/muc#user");
            this.invitationPacketListener = new PacketListener() {
                public void processPacket(Packet packet) {
                    MUCUser mUCUser = (MUCUser) packet.getExtension("x", "http://jabber.org/protocol/muc#user");
                    if (mUCUser.getInvite() != null && ((Message) packet).getType() != Message.Type.error) {
                        InvitationsMonitor.this.fireInvitationListeners(packet.getFrom(), mUCUser.getInvite().getFrom(), mUCUser.getInvite().getReason(), mUCUser.getPassword(), (Message) packet);
                    }
                }
            };
            this.connection.addPacketListener(this.invitationPacketListener, this.invitationFilter);
            this.connection.addConnectionListener(this);
        }

        private void cancel() {
            this.connection.removePacketListener(this.invitationPacketListener);
            this.connection.removeConnectionListener(this);
        }
    }
}
