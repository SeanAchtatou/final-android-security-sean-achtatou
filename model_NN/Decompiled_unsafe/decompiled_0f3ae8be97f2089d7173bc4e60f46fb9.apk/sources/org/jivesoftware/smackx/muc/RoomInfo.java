package org.jivesoftware.smackx.muc;

import org.jivesoftware.smackx.disco.packet.DiscoverInfo;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xdata.FormField;

public class RoomInfo {
    private String description = "";
    private boolean membersOnly;
    private boolean moderated;
    private boolean nonanonymous;
    private int occupantsCount = -1;
    private boolean passwordProtected;
    private boolean persistent;
    private String room;
    private String subject = "";

    RoomInfo(DiscoverInfo discoverInfo) {
        this.room = discoverInfo.getFrom();
        this.membersOnly = discoverInfo.containsFeature("muc_membersonly");
        this.moderated = discoverInfo.containsFeature("muc_moderated");
        this.nonanonymous = discoverInfo.containsFeature("muc_nonanonymous");
        this.passwordProtected = discoverInfo.containsFeature("muc_passwordprotected");
        this.persistent = discoverInfo.containsFeature("muc_persistent");
        Form formFrom = Form.getFormFrom(discoverInfo);
        if (formFrom != null) {
            FormField field = formFrom.getField("muc#roominfo_description");
            this.description = (field == null || field.getValues().isEmpty()) ? "" : field.getValues().get(0);
            FormField field2 = formFrom.getField("muc#roominfo_subject");
            this.subject = (field2 == null || field2.getValues().isEmpty()) ? "" : field2.getValues().get(0);
            FormField field3 = formFrom.getField("muc#roominfo_occupants");
            this.occupantsCount = field3 == null ? -1 : Integer.parseInt(field3.getValues().get(0));
        }
    }

    public String getRoom() {
        return this.room;
    }

    public String getDescription() {
        return this.description;
    }

    public String getSubject() {
        return this.subject;
    }

    public int getOccupantsCount() {
        return this.occupantsCount;
    }

    public boolean isMembersOnly() {
        return this.membersOnly;
    }

    public boolean isModerated() {
        return this.moderated;
    }

    public boolean isNonanonymous() {
        return this.nonanonymous;
    }

    public boolean isPasswordProtected() {
        return this.passwordProtected;
    }

    public boolean isPersistent() {
        return this.persistent;
    }
}
