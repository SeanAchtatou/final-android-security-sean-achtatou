package org.jivesoftware.smackx.time.packet;

import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.XmppDateTime;

public class Time extends IQ {
    public static final String ELEMENT = "time";
    private static final Logger LOGGER = Logger.getLogger(Time.class.getName());
    public static final String NAMESPACE = "urn:xmpp:time";
    private String tzo;
    private String utc;

    public Time() {
        setType(IQ.Type.GET);
    }

    public Time(Calendar calendar) {
        this.tzo = XmppDateTime.asString(calendar.getTimeZone());
        this.utc = XmppDateTime.formatXEP0082Date(calendar.getTime());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public Date getTime() {
        if (this.utc == null) {
            return null;
        }
        try {
            return XmppDateTime.parseDate(this.utc);
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Error getting local time", (Throwable) e);
            return null;
        }
    }

    public void setTime(Date date) {
    }

    public String getUtc() {
        return this.utc;
    }

    public void setUtc(String str) {
        this.utc = str;
    }

    public String getTzo() {
        return this.tzo;
    }

    public void setTzo(String str) {
        this.tzo = str;
    }

    public static Time createResponse(Packet packet) {
        Time time = new Time(Calendar.getInstance());
        time.setType(IQ.Type.RESULT);
        time.setTo(packet.getFrom());
        return time;
    }

    public String getChildElementXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<time xmlns='urn:xmpp:time'>");
        if (this.utc != null) {
            sb.append("<utc>").append(this.utc).append("</utc>");
            sb.append("<tzo>").append(this.tzo).append("</tzo>");
        }
        sb.append("</time>");
        return sb.toString();
    }
}
