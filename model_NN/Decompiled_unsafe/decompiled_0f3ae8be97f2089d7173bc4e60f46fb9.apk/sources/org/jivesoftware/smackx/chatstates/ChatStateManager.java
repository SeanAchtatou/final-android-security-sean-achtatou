package org.jivesoftware.smackx.chatstates;

import java.util.Map;
import java.util.WeakHashMap;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.Manager;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PacketInterceptor;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.NotFilter;
import org.jivesoftware.smack.filter.PacketExtensionFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smackx.chatstates.packet.ChatStateExtension;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;

public class ChatStateManager extends Manager {
    private static final Map<XMPPConnection, ChatStateManager> INSTANCES = new WeakHashMap();
    public static final String NAMESPACE = "http://jabber.org/protocol/chatstates";
    private static final PacketFilter filter = new NotFilter(new PacketExtensionFilter(NAMESPACE));
    /* access modifiers changed from: private */
    public final ChatManager chatManager;
    private final Map<Chat, ChatState> chatStates = new WeakHashMap();
    private final IncomingMessageInterceptor incomingInterceptor = new IncomingMessageInterceptor();
    private final OutgoingMessageInterceptor outgoingInterceptor = new OutgoingMessageInterceptor();

    public static synchronized ChatStateManager getInstance(XMPPConnection xMPPConnection) {
        ChatStateManager chatStateManager;
        synchronized (ChatStateManager.class) {
            chatStateManager = INSTANCES.get(xMPPConnection);
            if (chatStateManager == null) {
                chatStateManager = new ChatStateManager(xMPPConnection);
            }
        }
        return chatStateManager;
    }

    private ChatStateManager(XMPPConnection xMPPConnection) {
        super(xMPPConnection);
        this.chatManager = ChatManager.getInstanceFor(xMPPConnection);
        this.chatManager.addOutgoingMessageInterceptor(this.outgoingInterceptor, filter);
        this.chatManager.addChatListener(this.incomingInterceptor);
        ServiceDiscoveryManager.getInstanceFor(xMPPConnection).addFeature(NAMESPACE);
        INSTANCES.put(xMPPConnection, this);
    }

    public void setCurrentState(ChatState chatState, Chat chat) throws SmackException.NotConnectedException {
        if (chat == null || chatState == null) {
            throw new IllegalArgumentException("Arguments cannot be null.");
        } else if (updateChatState(chat, chatState)) {
            Message message = new Message();
            message.addExtension(new ChatStateExtension(chatState));
            chat.sendMessage(message);
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return connection().equals(((ChatStateManager) obj).connection());
    }

    public int hashCode() {
        return connection().hashCode();
    }

    /* access modifiers changed from: private */
    public synchronized boolean updateChatState(Chat chat, ChatState chatState) {
        boolean z;
        if (this.chatStates.get(chat) != chatState) {
            this.chatStates.put(chat, chatState);
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: private */
    public void fireNewChatState(Chat chat, ChatState chatState) {
        for (MessageListener next : chat.getListeners()) {
            if (next instanceof ChatStateListener) {
                ((ChatStateListener) next).stateChanged(chat, chatState);
            }
        }
    }

    private class OutgoingMessageInterceptor implements PacketInterceptor {
        private OutgoingMessageInterceptor() {
        }

        public void interceptPacket(Packet packet) {
            Message message = (Message) packet;
            Chat threadChat = ChatStateManager.this.chatManager.getThreadChat(message.getThread());
            if (threadChat != null && ChatStateManager.this.updateChatState(threadChat, ChatState.active)) {
                message.addExtension(new ChatStateExtension(ChatState.active));
            }
        }
    }

    private class IncomingMessageInterceptor implements ChatManagerListener, MessageListener {
        private IncomingMessageInterceptor() {
        }

        public void chatCreated(Chat chat, boolean z) {
            chat.addMessageListener(this);
        }

        public void processMessage(Chat chat, Message message) {
            PacketExtension extension = message.getExtension(ChatStateManager.NAMESPACE);
            if (extension != null) {
                try {
                    ChatStateManager.this.fireNewChatState(chat, ChatState.valueOf(extension.getElementName()));
                } catch (Exception e) {
                }
            }
        }
    }
}
