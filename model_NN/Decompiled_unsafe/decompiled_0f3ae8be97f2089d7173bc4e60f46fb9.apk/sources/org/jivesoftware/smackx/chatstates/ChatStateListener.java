package org.jivesoftware.smackx.chatstates;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;

public interface ChatStateListener extends MessageListener {
    void stateChanged(Chat chat, ChatState chatState);
}
