package org.jivesoftware.smackx.xhtmlim.provider;

import java.io.IOException;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.xhtmlim.packet.XHTMLExtension;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class XHTMLExtensionProvider implements PacketExtensionProvider {
    public static final String BODY_ELEMENT = "body";

    public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws IOException, XmlPullParserException {
        int i;
        StringBuilder sb;
        boolean z;
        XHTMLExtension xHTMLExtension = new XHTMLExtension();
        String elementName = xHTMLExtension.getElementName();
        int depth = xmlPullParser.getDepth();
        int depth2 = xmlPullParser.getDepth();
        StringBuilder sb2 = new StringBuilder();
        int i2 = depth2;
        boolean z2 = false;
        while (true) {
            int next = xmlPullParser.next();
            if (next == 2) {
                if (BODY_ELEMENT.equals(xmlPullParser.getName())) {
                    StringBuilder sb3 = new StringBuilder();
                    i = xmlPullParser.getDepth();
                    sb = sb3;
                    z = true;
                } else {
                    i = i2;
                    sb = sb2;
                    z = false;
                }
                maybeCloseTag(z2, sb);
                appendStartTagPartial(sb, xmlPullParser, z);
                sb2 = sb;
                z2 = true;
                i2 = i;
            } else if (next == 4) {
                z2 = maybeCloseTag(z2, sb2);
                appendText(sb2, xmlPullParser);
            } else if (next == 3) {
                String name = xmlPullParser.getName();
                if (elementName.equals(name) && xmlPullParser.getDepth() <= depth) {
                    return xHTMLExtension;
                }
                appendEndTag(sb2, xmlPullParser, z2);
                if (BODY_ELEMENT.equals(name) && xmlPullParser.getDepth() <= i2) {
                    xHTMLExtension.addBody(sb2.toString());
                }
                z2 = false;
            } else {
                continue;
            }
        }
    }

    private static void appendStartTagPartial(StringBuilder sb, XmlPullParser xmlPullParser, boolean z) {
        sb.append('<');
        String prefix = xmlPullParser.getPrefix();
        if (StringUtils.isNotEmpty(prefix)) {
            sb.append(prefix).append(':');
        }
        sb.append(xmlPullParser.getName());
        int attributeCount = xmlPullParser.getAttributeCount();
        if (z) {
            String namespace = xmlPullParser.getNamespace();
            if (StringUtils.isNotEmpty(namespace)) {
                sb.append(" xmlns='").append(namespace).append('\'');
            }
        }
        for (int i = 0; i < attributeCount; i++) {
            sb.append(' ');
            String attributeNamespace = xmlPullParser.getAttributeNamespace(i);
            if (StringUtils.isNotEmpty(attributeNamespace)) {
                sb.append(attributeNamespace).append(':');
            }
            sb.append(xmlPullParser.getAttributeName(i));
            String attributeValue = xmlPullParser.getAttributeValue(i);
            if (attributeValue != null) {
                sb.append("='").append(StringUtils.escapeForXML(attributeValue)).append('\'');
            }
        }
    }

    private static void appendEndTag(StringBuilder sb, XmlPullParser xmlPullParser, boolean z) {
        if (z) {
            sb.append("/>");
        } else {
            sb.append("</").append(xmlPullParser.getName()).append('>');
        }
    }

    private static boolean appendText(StringBuilder sb, XmlPullParser xmlPullParser) {
        if (xmlPullParser.getText() == null) {
            return false;
        }
        sb.append(StringUtils.escapeForXML(xmlPullParser.getText()));
        return true;
    }

    private static boolean maybeCloseTag(boolean z, StringBuilder sb) {
        if (!z) {
            return false;
        }
        sb.append('>');
        return false;
    }
}
