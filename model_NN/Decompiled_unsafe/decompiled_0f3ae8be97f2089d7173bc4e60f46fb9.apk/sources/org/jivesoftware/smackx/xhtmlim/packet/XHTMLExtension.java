package org.jivesoftware.smackx.xhtmlim.packet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.jivesoftware.smack.packet.PacketExtension;

public class XHTMLExtension implements PacketExtension {
    private List<String> bodies = new ArrayList();

    public String getElementName() {
        return "html";
    }

    public String getNamespace() {
        return "http://jabber.org/protocol/xhtml-im";
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\">");
        for (String append : getBodies()) {
            sb.append(append);
        }
        sb.append("</").append(getElementName()).append(">");
        return sb.toString();
    }

    public List<String> getBodies() {
        List<String> unmodifiableList;
        synchronized (this.bodies) {
            unmodifiableList = Collections.unmodifiableList(new ArrayList(this.bodies));
        }
        return unmodifiableList;
    }

    public void addBody(String str) {
        synchronized (this.bodies) {
            this.bodies.add(str);
        }
    }

    public int getBodiesCount() {
        return this.bodies.size();
    }
}
