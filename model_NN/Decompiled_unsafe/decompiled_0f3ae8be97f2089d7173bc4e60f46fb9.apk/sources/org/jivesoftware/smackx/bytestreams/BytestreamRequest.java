package org.jivesoftware.smackx.bytestreams;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

public interface BytestreamRequest {
    BytestreamSession accept() throws InterruptedException, SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException;

    String getFrom();

    String getSessionID();

    void reject() throws SmackException.NotConnectedException;
}
