package org.jivesoftware.smackx.bytestreams.ibb;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import org.jivesoftware.smack.AbstractConnectionListener;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smackx.bytestreams.BytestreamListener;
import org.jivesoftware.smackx.bytestreams.BytestreamManager;
import org.jivesoftware.smackx.bytestreams.ibb.packet.Open;

public class InBandBytestreamManager implements BytestreamManager {
    public static final int MAXIMUM_BLOCK_SIZE = 65535;
    public static final String NAMESPACE = "http://jabber.org/protocol/ibb";
    private static final String SESSION_ID_PREFIX = "jibb_";
    private static final Map<XMPPConnection, InBandBytestreamManager> managers = new HashMap();
    private static final Random randomGenerator = new Random();
    private final List<BytestreamListener> allRequestListeners = Collections.synchronizedList(new LinkedList());
    private final CloseListener closeListener;
    private final XMPPConnection connection;
    private final DataListener dataListener;
    private int defaultBlockSize = 4096;
    private List<String> ignoredBytestreamRequests = Collections.synchronizedList(new LinkedList());
    private final InitiationListener initiationListener;
    private int maximumBlockSize = 65535;
    private final Map<String, InBandBytestreamSession> sessions = new ConcurrentHashMap();
    private StanzaType stanza = StanzaType.IQ;
    private final Map<String, BytestreamListener> userListeners = new ConcurrentHashMap();

    public enum StanzaType {
        IQ,
        MESSAGE
    }

    static {
        XMPPConnection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(final XMPPConnection xMPPConnection) {
                InBandBytestreamManager.getByteStreamManager(xMPPConnection);
                xMPPConnection.addConnectionListener(new AbstractConnectionListener() {
                    public void connectionClosed() {
                        InBandBytestreamManager.getByteStreamManager(xMPPConnection).disableService();
                    }

                    public void connectionClosedOnError(Exception exc) {
                        InBandBytestreamManager.getByteStreamManager(xMPPConnection).disableService();
                    }

                    public void reconnectionSuccessful() {
                        InBandBytestreamManager.getByteStreamManager(xMPPConnection);
                    }
                });
            }
        });
    }

    public static synchronized InBandBytestreamManager getByteStreamManager(XMPPConnection xMPPConnection) {
        InBandBytestreamManager inBandBytestreamManager;
        synchronized (InBandBytestreamManager.class) {
            if (xMPPConnection == null) {
                inBandBytestreamManager = null;
            } else {
                inBandBytestreamManager = managers.get(xMPPConnection);
                if (inBandBytestreamManager == null) {
                    inBandBytestreamManager = new InBandBytestreamManager(xMPPConnection);
                    managers.put(xMPPConnection, inBandBytestreamManager);
                }
            }
        }
        return inBandBytestreamManager;
    }

    private InBandBytestreamManager(XMPPConnection xMPPConnection) {
        this.connection = xMPPConnection;
        this.initiationListener = new InitiationListener(this);
        this.connection.addPacketListener(this.initiationListener, this.initiationListener.getFilter());
        this.dataListener = new DataListener(this);
        this.connection.addPacketListener(this.dataListener, this.dataListener.getFilter());
        this.closeListener = new CloseListener(this);
        this.connection.addPacketListener(this.closeListener, this.closeListener.getFilter());
    }

    public void addIncomingBytestreamListener(BytestreamListener bytestreamListener) {
        this.allRequestListeners.add(bytestreamListener);
    }

    public void removeIncomingBytestreamListener(BytestreamListener bytestreamListener) {
        this.allRequestListeners.remove(bytestreamListener);
    }

    public void addIncomingBytestreamListener(BytestreamListener bytestreamListener, String str) {
        this.userListeners.put(str, bytestreamListener);
    }

    public void removeIncomingBytestreamListener(String str) {
        this.userListeners.remove(str);
    }

    public void ignoreBytestreamRequestOnce(String str) {
        this.ignoredBytestreamRequests.add(str);
    }

    public int getDefaultBlockSize() {
        return this.defaultBlockSize;
    }

    public void setDefaultBlockSize(int i) {
        if (i <= 0 || i > 65535) {
            throw new IllegalArgumentException("Default block size must be between 1 and 65535");
        }
        this.defaultBlockSize = i;
    }

    public int getMaximumBlockSize() {
        return this.maximumBlockSize;
    }

    public void setMaximumBlockSize(int i) {
        if (i <= 0 || i > 65535) {
            throw new IllegalArgumentException("Maximum block size must be between 1 and 65535");
        }
        this.maximumBlockSize = i;
    }

    public StanzaType getStanza() {
        return this.stanza;
    }

    public void setStanza(StanzaType stanzaType) {
        this.stanza = stanzaType;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jivesoftware.smackx.bytestreams.ibb.InBandBytestreamManager.establishSession(java.lang.String, java.lang.String):org.jivesoftware.smackx.bytestreams.ibb.InBandBytestreamSession
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.jivesoftware.smackx.bytestreams.ibb.InBandBytestreamManager.establishSession(java.lang.String, java.lang.String):org.jivesoftware.smackx.bytestreams.BytestreamSession
      org.jivesoftware.smackx.bytestreams.BytestreamManager.establishSession(java.lang.String, java.lang.String):org.jivesoftware.smackx.bytestreams.BytestreamSession
      org.jivesoftware.smackx.bytestreams.ibb.InBandBytestreamManager.establishSession(java.lang.String, java.lang.String):org.jivesoftware.smackx.bytestreams.ibb.InBandBytestreamSession */
    public InBandBytestreamSession establishSession(String str) throws XMPPException, SmackException {
        return establishSession(str, getNextSessionID());
    }

    public InBandBytestreamSession establishSession(String str, String str2) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Open open = new Open(str2, this.defaultBlockSize, this.stanza);
        open.setTo(str);
        this.connection.createPacketCollectorAndSend(open).nextResultOrThrow();
        InBandBytestreamSession inBandBytestreamSession = new InBandBytestreamSession(this.connection, open, str);
        this.sessions.put(str2, inBandBytestreamSession);
        return inBandBytestreamSession;
    }

    /* access modifiers changed from: protected */
    public void replyRejectPacket(IQ iq) throws SmackException.NotConnectedException {
        this.connection.sendPacket(IQ.createErrorResponse(iq, new XMPPError(XMPPError.Condition.not_acceptable)));
    }

    /* access modifiers changed from: protected */
    public void replyResourceConstraintPacket(IQ iq) throws SmackException.NotConnectedException {
        this.connection.sendPacket(IQ.createErrorResponse(iq, new XMPPError(XMPPError.Condition.resource_constraint)));
    }

    /* access modifiers changed from: protected */
    public void replyItemNotFoundPacket(IQ iq) throws SmackException.NotConnectedException {
        this.connection.sendPacket(IQ.createErrorResponse(iq, new XMPPError(XMPPError.Condition.item_not_found)));
    }

    private String getNextSessionID() {
        return SESSION_ID_PREFIX + Math.abs(randomGenerator.nextLong());
    }

    /* access modifiers changed from: protected */
    public XMPPConnection getConnection() {
        return this.connection;
    }

    /* access modifiers changed from: protected */
    public BytestreamListener getUserListener(String str) {
        return this.userListeners.get(str);
    }

    /* access modifiers changed from: protected */
    public List<BytestreamListener> getAllRequestListeners() {
        return this.allRequestListeners;
    }

    /* access modifiers changed from: protected */
    public Map<String, InBandBytestreamSession> getSessions() {
        return this.sessions;
    }

    /* access modifiers changed from: protected */
    public List<String> getIgnoredBytestreamRequests() {
        return this.ignoredBytestreamRequests;
    }

    /* access modifiers changed from: private */
    public void disableService() {
        managers.remove(this.connection);
        this.connection.removePacketListener(this.initiationListener);
        this.connection.removePacketListener(this.dataListener);
        this.connection.removePacketListener(this.closeListener);
        this.initiationListener.shutdown();
        this.userListeners.clear();
        this.allRequestListeners.clear();
        this.sessions.clear();
        this.ignoredBytestreamRequests.clear();
    }
}
