package org.jivesoftware.smackx.bytestreams.socks5;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.SmackException;

public class Socks5Proxy {
    private static final Logger LOGGER = Logger.getLogger(Socks5Proxy.class.getName());
    private static boolean localSocks5ProxyEnabled = true;
    private static int localSocks5ProxyPort = -7777;
    private static Socks5Proxy socks5Server;
    /* access modifiers changed from: private */
    public final List<String> allowedConnections = Collections.synchronizedList(new LinkedList());
    /* access modifiers changed from: private */
    public final Map<String, Socket> connectionMap = new ConcurrentHashMap();
    private final Set<String> localAddresses = Collections.synchronizedSet(new LinkedHashSet());
    private Socks5ServerProcess serverProcess = new Socks5ServerProcess();
    /* access modifiers changed from: private */
    public ServerSocket serverSocket;
    private Thread serverThread;

    private Socks5Proxy() {
        try {
            this.localAddresses.add(InetAddress.getLocalHost().getHostAddress());
        } catch (UnknownHostException e) {
        }
    }

    public static boolean isLocalSocks5ProxyEnabled() {
        return localSocks5ProxyEnabled;
    }

    public static void setLocalSocks5ProxyEnabled(boolean z) {
        localSocks5ProxyEnabled = z;
    }

    public static int getLocalSocks5ProxyPort() {
        return localSocks5ProxyPort;
    }

    public static void setLocalSocks5ProxyPort(int i) {
        if (Math.abs(i) > 65535) {
            throw new IllegalArgumentException("localSocks5ProxyPort must be within (-65535,65535)");
        }
        localSocks5ProxyPort = i;
    }

    public static synchronized Socks5Proxy getSocks5Proxy() {
        Socks5Proxy socks5Proxy;
        synchronized (Socks5Proxy.class) {
            if (socks5Server == null) {
                socks5Server = new Socks5Proxy();
            }
            if (isLocalSocks5ProxyEnabled()) {
                socks5Server.start();
            }
            socks5Proxy = socks5Server;
        }
        return socks5Proxy;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public synchronized void start() {
        if (!isRunning()) {
            try {
                if (getLocalSocks5ProxyPort() < 0) {
                    int abs = Math.abs(getLocalSocks5ProxyPort());
                    int i = 0;
                    while (i < 65535 - abs) {
                        try {
                            this.serverSocket = new ServerSocket(abs + i);
                            break;
                        } catch (IOException e) {
                            i++;
                        }
                    }
                } else {
                    this.serverSocket = new ServerSocket(getLocalSocks5ProxyPort());
                }
                if (this.serverSocket != null) {
                    this.serverThread = new Thread(this.serverProcess);
                    this.serverThread.start();
                }
            } catch (IOException e2) {
                LOGGER.log(Level.SEVERE, "couldn't setup local SOCKS5 proxy on port " + getLocalSocks5ProxyPort(), (Throwable) e2);
            }
        }
        return;
    }

    public synchronized void stop() {
        if (isRunning()) {
            try {
                this.serverSocket.close();
            } catch (IOException e) {
            }
            if (this.serverThread != null && this.serverThread.isAlive()) {
                try {
                    this.serverThread.interrupt();
                    this.serverThread.join();
                } catch (InterruptedException e2) {
                }
            }
            this.serverThread = null;
            this.serverSocket = null;
        }
    }

    public void addLocalAddress(String str) {
        if (str == null) {
            throw new IllegalArgumentException("address may not be null");
        }
        this.localAddresses.add(str);
    }

    public void removeLocalAddress(String str) {
        this.localAddresses.remove(str);
    }

    public List<String> getLocalAddresses() {
        return Collections.unmodifiableList(new ArrayList(this.localAddresses));
    }

    public void replaceLocalAddresses(List<String> list) {
        if (list == null) {
            throw new IllegalArgumentException("list must not be null");
        }
        this.localAddresses.clear();
        this.localAddresses.addAll(list);
    }

    public int getPort() {
        if (!isRunning()) {
            return -1;
        }
        return this.serverSocket.getLocalPort();
    }

    /* access modifiers changed from: protected */
    public Socket getSocket(String str) {
        return this.connectionMap.get(str);
    }

    /* access modifiers changed from: protected */
    public void addTransfer(String str) {
        this.allowedConnections.add(str);
    }

    /* access modifiers changed from: protected */
    public void removeTransfer(String str) {
        this.allowedConnections.remove(str);
        this.connectionMap.remove(str);
    }

    public boolean isRunning() {
        return this.serverSocket != null;
    }

    private class Socks5ServerProcess implements Runnable {
        private Socks5ServerProcess() {
        }

        public void run() {
            while (true) {
                Socket socket = null;
                try {
                    if (!Socks5Proxy.this.serverSocket.isClosed() && !Thread.currentThread().isInterrupted()) {
                        establishConnection(Socks5Proxy.this.serverSocket.accept());
                    } else {
                        return;
                    }
                } catch (SocketException e) {
                } catch (Exception e2) {
                    if (socket != null) {
                        try {
                            socket.close();
                        } catch (IOException e3) {
                        }
                    }
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.String.<init>(byte[], int, int):void}
         arg types: [byte[], int, byte]
         candidates:
          ClspMth{java.lang.String.<init>(int[], int, int):void}
          ClspMth{java.lang.String.<init>(char[], int, int):void}
          ClspMth{java.lang.String.<init>(byte[], int, int):void} */
        private void establishConnection(Socket socket) throws SmackException, IOException {
            boolean z;
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            DataInputStream dataInputStream = new DataInputStream(socket.getInputStream());
            if (dataInputStream.read() != 5) {
                throw new SmackException("Only SOCKS5 supported");
            }
            byte[] bArr = new byte[dataInputStream.read()];
            dataInputStream.readFully(bArr);
            byte[] bArr2 = new byte[2];
            bArr2[0] = 5;
            int i = 0;
            while (true) {
                if (i >= bArr.length) {
                    z = false;
                    break;
                } else if (bArr[i] == 0) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
            if (!z) {
                bArr2[1] = -1;
                dataOutputStream.write(bArr2);
                dataOutputStream.flush();
                throw new SmackException("Authentication method not supported");
            }
            bArr2[1] = 0;
            dataOutputStream.write(bArr2);
            dataOutputStream.flush();
            byte[] receiveSocks5Message = Socks5Utils.receiveSocks5Message(dataInputStream);
            String str = new String(receiveSocks5Message, 5, (int) receiveSocks5Message[4]);
            if (!Socks5Proxy.this.allowedConnections.contains(str)) {
                receiveSocks5Message[1] = 5;
                dataOutputStream.write(receiveSocks5Message);
                dataOutputStream.flush();
                throw new SmackException("Connection is not allowed");
            }
            receiveSocks5Message[1] = 0;
            dataOutputStream.write(receiveSocks5Message);
            dataOutputStream.flush();
            Socks5Proxy.this.connectionMap.put(str, socket);
        }
    }
}
