package org.jivesoftware.smackx.bytestreams.socks5.provider;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smackx.bytestreams.socks5.packet.Bytestream;
import org.xmlpull.v1.XmlPullParser;

public class BytestreamsProvider implements IQProvider {
    public IQ parseIQ(XmlPullParser xmlPullParser) throws Exception {
        Bytestream bytestream = new Bytestream();
        String attributeValue = xmlPullParser.getAttributeValue("", "sid");
        String attributeValue2 = xmlPullParser.getAttributeValue("", "mode");
        String str = null;
        String str2 = null;
        boolean z = false;
        String str3 = null;
        while (!z) {
            int next = xmlPullParser.next();
            String name = xmlPullParser.getName();
            if (next == 2) {
                if (name.equals(Bytestream.StreamHost.ELEMENTNAME)) {
                    str2 = xmlPullParser.getAttributeValue("", "jid");
                    str = xmlPullParser.getAttributeValue("", "host");
                    str3 = xmlPullParser.getAttributeValue("", "port");
                } else if (name.equals(Bytestream.StreamHostUsed.ELEMENTNAME)) {
                    bytestream.setUsedHost(xmlPullParser.getAttributeValue("", "jid"));
                } else if (name.equals(Bytestream.Activate.ELEMENTNAME)) {
                    bytestream.setToActivate(xmlPullParser.getAttributeValue("", "jid"));
                }
            } else if (next == 3) {
                if (name.equals("streamhost")) {
                    if (str3 == null) {
                        bytestream.addStreamHost(str2, str);
                    } else {
                        bytestream.addStreamHost(str2, str, Integer.parseInt(str3));
                    }
                    str3 = null;
                    str = null;
                    str2 = null;
                } else if (name.equals("query")) {
                    z = true;
                }
            }
        }
        bytestream.setMode(Bytestream.Mode.fromName(attributeValue2));
        bytestream.setSessionID(attributeValue);
        return bytestream;
    }
}
