package org.jivesoftware.smackx.bytestreams.socks5;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.IQTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.bytestreams.BytestreamListener;
import org.jivesoftware.smackx.bytestreams.socks5.packet.Bytestream;

final class InitiationListener implements PacketListener {
    /* access modifiers changed from: private */
    public static final Logger LOGGER = Logger.getLogger(InitiationListener.class.getName());
    private final PacketFilter initFilter = new AndFilter(new PacketTypeFilter(Bytestream.class), new IQTypeFilter(IQ.Type.SET));
    private final ExecutorService initiationListenerExecutor;
    private final Socks5BytestreamManager manager;

    protected InitiationListener(Socks5BytestreamManager socks5BytestreamManager) {
        this.manager = socks5BytestreamManager;
        this.initiationListenerExecutor = Executors.newCachedThreadPool();
    }

    public void processPacket(final Packet packet) {
        this.initiationListenerExecutor.execute(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
             arg types: [java.util.logging.Level, java.lang.String, org.jivesoftware.smack.SmackException$NotConnectedException]
             candidates:
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
              ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
            public void run() {
                try {
                    InitiationListener.this.processRequest(packet);
                } catch (SmackException.NotConnectedException e) {
                    InitiationListener.LOGGER.log(Level.WARNING, "process request", (Throwable) e);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void processRequest(Packet packet) throws SmackException.NotConnectedException {
        Bytestream bytestream = (Bytestream) packet;
        if (!this.manager.getIgnoredBytestreamRequests().remove(bytestream.getSessionID())) {
            Socks5BytestreamRequest socks5BytestreamRequest = new Socks5BytestreamRequest(this.manager, bytestream);
            BytestreamListener userListener = this.manager.getUserListener(bytestream.getFrom());
            if (userListener != null) {
                userListener.incomingBytestreamRequest(socks5BytestreamRequest);
            } else if (!this.manager.getAllRequestListeners().isEmpty()) {
                for (BytestreamListener incomingBytestreamRequest : this.manager.getAllRequestListeners()) {
                    incomingBytestreamRequest.incomingBytestreamRequest(socks5BytestreamRequest);
                }
            } else {
                this.manager.replyRejectPacket(bytestream);
            }
        }
    }

    /* access modifiers changed from: protected */
    public PacketFilter getFilter() {
        return this.initFilter;
    }

    /* access modifiers changed from: protected */
    public void shutdown() {
        this.initiationListenerExecutor.shutdownNow();
    }
}
