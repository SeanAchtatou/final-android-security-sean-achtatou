package org.jivesoftware.smackx.privacy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.Manager;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.IQTypeFilter;
import org.jivesoftware.smack.filter.PacketExtensionFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.privacy.packet.Privacy;
import org.jivesoftware.smackx.privacy.packet.PrivacyItem;

public class PrivacyListManager extends Manager {
    public static final String NAMESPACE = "jabber:iq:privacy";
    private static final PacketFilter PACKET_FILTER = new AndFilter(new IQTypeFilter(IQ.Type.SET), new PacketExtensionFilter("query", NAMESPACE));
    private static final Map<XMPPConnection, PrivacyListManager> instances = Collections.synchronizedMap(new WeakHashMap());
    /* access modifiers changed from: private */
    public final List<PrivacyListListener> listeners = new ArrayList();

    static {
        XMPPConnection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(XMPPConnection xMPPConnection) {
                PrivacyListManager.getInstanceFor(xMPPConnection);
            }
        });
    }

    private PrivacyListManager(final XMPPConnection xMPPConnection) {
        super(xMPPConnection);
        instances.put(xMPPConnection, this);
        xMPPConnection.addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                Privacy privacy = (Privacy) packet;
                synchronized (PrivacyListManager.this.listeners) {
                    for (PrivacyListListener privacyListListener : PrivacyListManager.this.listeners) {
                        for (Map.Entry next : privacy.getItemLists().entrySet()) {
                            String str = (String) next.getKey();
                            List list = (List) next.getValue();
                            if (list.isEmpty()) {
                                privacyListListener.updatedPrivacyList(str);
                            } else {
                                privacyListListener.setPrivacyList(str, list);
                            }
                        }
                    }
                }
                xMPPConnection.sendPacket(IQ.createResultIQ(privacy));
            }
        }, PACKET_FILTER);
    }

    private String getUser() {
        return connection().getUser();
    }

    public static synchronized PrivacyListManager getInstanceFor(XMPPConnection xMPPConnection) {
        PrivacyListManager privacyListManager;
        synchronized (PrivacyListManager.class) {
            privacyListManager = instances.get(xMPPConnection);
            if (privacyListManager == null) {
                privacyListManager = new PrivacyListManager(xMPPConnection);
            }
        }
        return privacyListManager;
    }

    private Privacy getRequest(Privacy privacy) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        privacy.setType(IQ.Type.GET);
        privacy.setFrom(getUser());
        return (Privacy) connection().createPacketCollectorAndSend(privacy).nextResultOrThrow();
    }

    private Packet setRequest(Privacy privacy) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        privacy.setType(IQ.Type.SET);
        privacy.setFrom(getUser());
        return connection().createPacketCollectorAndSend(privacy).nextResultOrThrow();
    }

    private Privacy getPrivacyWithListNames() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return getRequest(new Privacy());
    }

    public PrivacyList getActiveList() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Privacy privacyWithListNames = getPrivacyWithListNames();
        String activeName = privacyWithListNames.getActiveName();
        return new PrivacyList(true, (privacyWithListNames.getActiveName() == null || privacyWithListNames.getDefaultName() == null || !privacyWithListNames.getActiveName().equals(privacyWithListNames.getDefaultName())) ? false : true, activeName, getPrivacyListItems(activeName));
    }

    public PrivacyList getDefaultList() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Privacy privacyWithListNames = getPrivacyWithListNames();
        String defaultName = privacyWithListNames.getDefaultName();
        return new PrivacyList((privacyWithListNames.getActiveName() == null || privacyWithListNames.getDefaultName() == null || !privacyWithListNames.getActiveName().equals(privacyWithListNames.getDefaultName())) ? false : true, true, defaultName, getPrivacyListItems(defaultName));
    }

    private List<PrivacyItem> getPrivacyListItems(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Privacy privacy = new Privacy();
        privacy.setPrivacyList(str, new ArrayList());
        return getRequest(privacy).getPrivacyList(str);
    }

    public PrivacyList getPrivacyList(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return new PrivacyList(false, false, str, getPrivacyListItems(str));
    }

    public PrivacyList[] getPrivacyLists() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Privacy privacyWithListNames = getPrivacyWithListNames();
        Set<String> privacyListNames = privacyWithListNames.getPrivacyListNames();
        PrivacyList[] privacyListArr = new PrivacyList[privacyListNames.size()];
        int i = 0;
        Iterator<String> it = privacyListNames.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return privacyListArr;
            }
            String next = it.next();
            privacyListArr[i2] = new PrivacyList(next.equals(privacyWithListNames.getActiveName()), next.equals(privacyWithListNames.getDefaultName()), next, getPrivacyListItems(next));
            i = i2 + 1;
        }
    }

    public void setActiveListName(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Privacy privacy = new Privacy();
        privacy.setActiveName(str);
        setRequest(privacy);
    }

    public void declineActiveList() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Privacy privacy = new Privacy();
        privacy.setDeclineActiveList(true);
        setRequest(privacy);
    }

    public void setDefaultListName(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Privacy privacy = new Privacy();
        privacy.setDefaultName(str);
        setRequest(privacy);
    }

    public void declineDefaultList() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Privacy privacy = new Privacy();
        privacy.setDeclineDefaultList(true);
        setRequest(privacy);
    }

    public void createPrivacyList(String str, List<PrivacyItem> list) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        updatePrivacyList(str, list);
    }

    public void updatePrivacyList(String str, List<PrivacyItem> list) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Privacy privacy = new Privacy();
        privacy.setPrivacyList(str, list);
        setRequest(privacy);
    }

    public void deletePrivacyList(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        Privacy privacy = new Privacy();
        privacy.setPrivacyList(str, new ArrayList());
        setRequest(privacy);
    }

    public void addListener(PrivacyListListener privacyListListener) {
        synchronized (this.listeners) {
            this.listeners.add(privacyListListener);
        }
    }

    public boolean isSupported() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return ServiceDiscoveryManager.getInstanceFor(connection()).supportsFeature(connection().getServiceName(), NAMESPACE);
    }
}
