package org.jivesoftware.smackx.commands;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smackx.commands.AdHocCommand;
import org.jivesoftware.smackx.commands.packet.AdHocCommandData;
import org.jivesoftware.smackx.xdata.Form;

public class RemoteCommand extends AdHocCommand {
    private XMPPConnection connection;
    private String jid;
    private String sessionID;

    protected RemoteCommand(XMPPConnection xMPPConnection, String str, String str2) {
        this.connection = xMPPConnection;
        this.jid = str2;
        setNode(str);
    }

    public void cancel() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        executeAction(AdHocCommand.Action.cancel);
    }

    public void complete(Form form) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        executeAction(AdHocCommand.Action.complete, form);
    }

    public void execute() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        executeAction(AdHocCommand.Action.execute);
    }

    public void execute(Form form) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        executeAction(AdHocCommand.Action.execute, form);
    }

    public void next(Form form) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        executeAction(AdHocCommand.Action.next, form);
    }

    public void prev() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        executeAction(AdHocCommand.Action.prev);
    }

    private void executeAction(AdHocCommand.Action action) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        executeAction(action, null);
    }

    private void executeAction(AdHocCommand.Action action, Form form) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        AdHocCommandData adHocCommandData = new AdHocCommandData();
        adHocCommandData.setType(IQ.Type.SET);
        adHocCommandData.setTo(getOwnerJID());
        adHocCommandData.setNode(getNode());
        adHocCommandData.setSessionID(this.sessionID);
        adHocCommandData.setAction(action);
        if (form != null) {
            adHocCommandData.setForm(form.getDataFormToSend());
        }
        AdHocCommandData adHocCommandData2 = (AdHocCommandData) this.connection.createPacketCollectorAndSend(adHocCommandData).nextResultOrThrow();
        this.sessionID = adHocCommandData2.getSessionID();
        super.setData(adHocCommandData2);
    }

    public String getOwnerJID() {
        return this.jid;
    }
}
