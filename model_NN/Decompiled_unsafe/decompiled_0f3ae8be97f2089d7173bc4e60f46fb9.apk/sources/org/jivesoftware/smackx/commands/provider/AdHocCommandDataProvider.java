package org.jivesoftware.smackx.commands.provider;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smackx.commands.AdHocCommand;
import org.jivesoftware.smackx.commands.packet.AdHocCommandData;
import org.xmlpull.v1.XmlPullParser;

public class AdHocCommandDataProvider implements IQProvider {
    /* JADX WARNING: Removed duplicated region for block: B:11:0x005a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.jivesoftware.smack.packet.IQ parseIQ(org.xmlpull.v1.XmlPullParser r8) throws java.lang.Exception {
        /*
            r7 = this;
            r0 = 0
            org.jivesoftware.smackx.commands.packet.AdHocCommandData r2 = new org.jivesoftware.smackx.commands.packet.AdHocCommandData
            r2.<init>()
            org.jivesoftware.smackx.xdata.provider.DataFormProvider r3 = new org.jivesoftware.smackx.xdata.provider.DataFormProvider
            r3.<init>()
            java.lang.String r1 = ""
            java.lang.String r4 = "sessionid"
            java.lang.String r1 = r8.getAttributeValue(r1, r4)
            r2.setSessionID(r1)
            java.lang.String r1 = ""
            java.lang.String r4 = "node"
            java.lang.String r1 = r8.getAttributeValue(r1, r4)
            r2.setNode(r1)
            java.lang.String r1 = ""
            java.lang.String r4 = "status"
            java.lang.String r1 = r8.getAttributeValue(r1, r4)
            org.jivesoftware.smackx.commands.AdHocCommand$Status r4 = org.jivesoftware.smackx.commands.AdHocCommand.Status.executing
            java.lang.String r4 = r4.toString()
            boolean r4 = r4.equalsIgnoreCase(r1)
            if (r4 == 0) goto L_0x0087
            org.jivesoftware.smackx.commands.AdHocCommand$Status r1 = org.jivesoftware.smackx.commands.AdHocCommand.Status.executing
            r2.setStatus(r1)
        L_0x003a:
            java.lang.String r1 = ""
            java.lang.String r4 = "action"
            java.lang.String r1 = r8.getAttributeValue(r1, r4)
            if (r1 == 0) goto L_0x00ae
            org.jivesoftware.smackx.commands.AdHocCommand$Action r1 = org.jivesoftware.smackx.commands.AdHocCommand.Action.valueOf(r1)
            if (r1 == 0) goto L_0x0052
            org.jivesoftware.smackx.commands.AdHocCommand$Action r4 = org.jivesoftware.smackx.commands.AdHocCommand.Action.unknown
            boolean r4 = r1.equals(r4)
            if (r4 == 0) goto L_0x00ab
        L_0x0052:
            org.jivesoftware.smackx.commands.AdHocCommand$Action r1 = org.jivesoftware.smackx.commands.AdHocCommand.Action.unknown
            r2.setAction(r1)
            r1 = r0
        L_0x0058:
            if (r1 != 0) goto L_0x0150
            int r0 = r8.next()
            java.lang.String r4 = r8.getName()
            java.lang.String r5 = r8.getNamespace()
            r6 = 2
            if (r0 != r6) goto L_0x013d
            java.lang.String r0 = r8.getName()
            java.lang.String r6 = "actions"
            boolean r0 = r0.equals(r6)
            if (r0 == 0) goto L_0x00b0
            java.lang.String r0 = ""
            java.lang.String r4 = "execute"
            java.lang.String r0 = r8.getAttributeValue(r0, r4)
            if (r0 == 0) goto L_0x0058
            org.jivesoftware.smackx.commands.AdHocCommand$Action r0 = org.jivesoftware.smackx.commands.AdHocCommand.Action.valueOf(r0)
            r2.setExecuteAction(r0)
            goto L_0x0058
        L_0x0087:
            org.jivesoftware.smackx.commands.AdHocCommand$Status r4 = org.jivesoftware.smackx.commands.AdHocCommand.Status.completed
            java.lang.String r4 = r4.toString()
            boolean r4 = r4.equalsIgnoreCase(r1)
            if (r4 == 0) goto L_0x0099
            org.jivesoftware.smackx.commands.AdHocCommand$Status r1 = org.jivesoftware.smackx.commands.AdHocCommand.Status.completed
            r2.setStatus(r1)
            goto L_0x003a
        L_0x0099:
            org.jivesoftware.smackx.commands.AdHocCommand$Status r4 = org.jivesoftware.smackx.commands.AdHocCommand.Status.canceled
            java.lang.String r4 = r4.toString()
            boolean r1 = r4.equalsIgnoreCase(r1)
            if (r1 == 0) goto L_0x003a
            org.jivesoftware.smackx.commands.AdHocCommand$Status r1 = org.jivesoftware.smackx.commands.AdHocCommand.Status.canceled
            r2.setStatus(r1)
            goto L_0x003a
        L_0x00ab:
            r2.setAction(r1)
        L_0x00ae:
            r1 = r0
            goto L_0x0058
        L_0x00b0:
            java.lang.String r0 = r8.getName()
            java.lang.String r6 = "next"
            boolean r0 = r0.equals(r6)
            if (r0 == 0) goto L_0x00c2
            org.jivesoftware.smackx.commands.AdHocCommand$Action r0 = org.jivesoftware.smackx.commands.AdHocCommand.Action.next
            r2.addAction(r0)
            goto L_0x0058
        L_0x00c2:
            java.lang.String r0 = r8.getName()
            java.lang.String r6 = "complete"
            boolean r0 = r0.equals(r6)
            if (r0 == 0) goto L_0x00d4
            org.jivesoftware.smackx.commands.AdHocCommand$Action r0 = org.jivesoftware.smackx.commands.AdHocCommand.Action.complete
            r2.addAction(r0)
            goto L_0x0058
        L_0x00d4:
            java.lang.String r0 = r8.getName()
            java.lang.String r6 = "prev"
            boolean r0 = r0.equals(r6)
            if (r0 == 0) goto L_0x00e7
            org.jivesoftware.smackx.commands.AdHocCommand$Action r0 = org.jivesoftware.smackx.commands.AdHocCommand.Action.prev
            r2.addAction(r0)
            goto L_0x0058
        L_0x00e7:
            java.lang.String r0 = "x"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0102
            java.lang.String r0 = "jabber:x:data"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0102
            org.jivesoftware.smack.packet.PacketExtension r0 = r3.parseExtension(r8)
            org.jivesoftware.smackx.xdata.packet.DataForm r0 = (org.jivesoftware.smackx.xdata.packet.DataForm) r0
            r2.setForm(r0)
            goto L_0x0058
        L_0x0102:
            java.lang.String r0 = r8.getName()
            java.lang.String r4 = "note"
            boolean r0 = r0.equals(r4)
            if (r0 == 0) goto L_0x0128
            java.lang.String r0 = ""
            java.lang.String r4 = "type"
            java.lang.String r0 = r8.getAttributeValue(r0, r4)
            org.jivesoftware.smackx.commands.AdHocCommandNote$Type r0 = org.jivesoftware.smackx.commands.AdHocCommandNote.Type.valueOf(r0)
            java.lang.String r4 = r8.nextText()
            org.jivesoftware.smackx.commands.AdHocCommandNote r5 = new org.jivesoftware.smackx.commands.AdHocCommandNote
            r5.<init>(r0, r4)
            r2.addNote(r5)
            goto L_0x0058
        L_0x0128:
            java.lang.String r0 = r8.getName()
            java.lang.String r4 = "error"
            boolean r0 = r0.equals(r4)
            if (r0 == 0) goto L_0x0058
            org.jivesoftware.smack.packet.XMPPError r0 = org.jivesoftware.smack.util.PacketParserUtils.parseError(r8)
            r2.setError(r0)
            goto L_0x0058
        L_0x013d:
            r4 = 3
            if (r0 != r4) goto L_0x0058
            java.lang.String r0 = r8.getName()
            java.lang.String r4 = "command"
            boolean r0 = r0.equals(r4)
            if (r0 == 0) goto L_0x0058
            r0 = 1
            r1 = r0
            goto L_0x0058
        L_0x0150:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.commands.provider.AdHocCommandDataProvider.parseIQ(org.xmlpull.v1.XmlPullParser):org.jivesoftware.smack.packet.IQ");
    }

    public static class BadActionError implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
            return new AdHocCommandData.SpecificError(AdHocCommand.SpecificErrorCondition.badAction);
        }
    }

    public static class MalformedActionError implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
            return new AdHocCommandData.SpecificError(AdHocCommand.SpecificErrorCondition.malformedAction);
        }
    }

    public static class BadLocaleError implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
            return new AdHocCommandData.SpecificError(AdHocCommand.SpecificErrorCondition.badLocale);
        }
    }

    public static class BadPayloadError implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
            return new AdHocCommandData.SpecificError(AdHocCommand.SpecificErrorCondition.badPayload);
        }
    }

    public static class BadSessionIDError implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
            return new AdHocCommandData.SpecificError(AdHocCommand.SpecificErrorCondition.badSessionid);
        }
    }

    public static class SessionExpiredError implements PacketExtensionProvider {
        public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
            return new AdHocCommandData.SpecificError(AdHocCommand.SpecificErrorCondition.sessionExpired);
        }
    }
}
