package org.jivesoftware.smackx.commands;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.Manager;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smackx.commands.AdHocCommand;
import org.jivesoftware.smackx.commands.packet.AdHocCommandData;
import org.jivesoftware.smackx.disco.NodeInformationProvider;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.disco.packet.DiscoverInfo;
import org.jivesoftware.smackx.disco.packet.DiscoverItems;
import org.jivesoftware.smackx.xdata.packet.DataForm;

public class AdHocCommandManager extends Manager {
    public static final String NAMESPACE = "http://jabber.org/protocol/commands";
    private static final int SESSION_TIMEOUT = 120;
    private static Map<XMPPConnection, AdHocCommandManager> instances = Collections.synchronizedMap(new WeakHashMap());
    private final Map<String, AdHocCommandInfo> commands = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public final Map<String, LocalCommand> executingCommands = new ConcurrentHashMap();
    private final ServiceDiscoveryManager serviceDiscoveryManager;
    private Thread sessionsSweeper;

    static {
        XMPPConnection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(XMPPConnection xMPPConnection) {
                AdHocCommandManager.getAddHocCommandsManager(xMPPConnection);
            }
        });
    }

    public static synchronized AdHocCommandManager getAddHocCommandsManager(XMPPConnection xMPPConnection) {
        AdHocCommandManager adHocCommandManager;
        synchronized (AdHocCommandManager.class) {
            adHocCommandManager = instances.get(xMPPConnection);
            if (adHocCommandManager == null) {
                adHocCommandManager = new AdHocCommandManager(xMPPConnection);
            }
        }
        return adHocCommandManager;
    }

    private AdHocCommandManager(XMPPConnection xMPPConnection) {
        super(xMPPConnection);
        this.serviceDiscoveryManager = ServiceDiscoveryManager.getInstanceFor(xMPPConnection);
        instances.put(xMPPConnection, this);
        ServiceDiscoveryManager.getInstanceFor(xMPPConnection).addFeature("http://jabber.org/protocol/commands");
        ServiceDiscoveryManager.getInstanceFor(xMPPConnection).setNodeInformationProvider("http://jabber.org/protocol/commands", new NodeInformationProvider() {
            public List<DiscoverItems.Item> getNodeItems() {
                ArrayList arrayList = new ArrayList();
                for (AdHocCommandInfo adHocCommandInfo : AdHocCommandManager.this.getRegisteredCommands()) {
                    DiscoverItems.Item item = new DiscoverItems.Item(adHocCommandInfo.getOwnerJID());
                    item.setName(adHocCommandInfo.getName());
                    item.setNode(adHocCommandInfo.getNode());
                    arrayList.add(item);
                }
                return arrayList;
            }

            public List<String> getNodeFeatures() {
                return null;
            }

            public List<DiscoverInfo.Identity> getNodeIdentities() {
                return null;
            }

            public List<PacketExtension> getNodePacketExtensions() {
                return null;
            }
        });
        xMPPConnection.addPacketListener(new PacketListener() {
            public void processPacket(Packet packet) {
                try {
                    AdHocCommandManager.this.processAdHocCommand((AdHocCommandData) packet);
                } catch (SmackException e) {
                }
            }
        }, new PacketTypeFilter(AdHocCommandData.class));
        this.sessionsSweeper = null;
    }

    public void registerCommand(String str, String str2, final Class<? extends LocalCommand> cls) {
        registerCommand(str, str2, new LocalCommandFactory() {
            public LocalCommand getInstance() throws InstantiationException, IllegalAccessException {
                return (LocalCommand) cls.newInstance();
            }
        });
    }

    public void registerCommand(String str, final String str2, LocalCommandFactory localCommandFactory) {
        this.commands.put(str, new AdHocCommandInfo(str, str2, connection().getUser(), localCommandFactory));
        this.serviceDiscoveryManager.setNodeInformationProvider(str, new NodeInformationProvider() {
            public List<DiscoverItems.Item> getNodeItems() {
                return null;
            }

            public List<String> getNodeFeatures() {
                ArrayList arrayList = new ArrayList();
                arrayList.add("http://jabber.org/protocol/commands");
                arrayList.add(DataForm.NAMESPACE);
                return arrayList;
            }

            public List<DiscoverInfo.Identity> getNodeIdentities() {
                ArrayList arrayList = new ArrayList();
                arrayList.add(new DiscoverInfo.Identity("automation", str2, "command-node"));
                return arrayList;
            }

            public List<PacketExtension> getNodePacketExtensions() {
                return null;
            }
        });
    }

    public DiscoverItems discoverCommands(String str) throws XMPPException, SmackException {
        return this.serviceDiscoveryManager.discoverItems(str, "http://jabber.org/protocol/commands");
    }

    public void publishCommands(String str) throws XMPPException, SmackException {
        DiscoverItems discoverItems = new DiscoverItems();
        for (AdHocCommandInfo next : getRegisteredCommands()) {
            DiscoverItems.Item item = new DiscoverItems.Item(next.getOwnerJID());
            item.setName(next.getName());
            item.setNode(next.getNode());
            discoverItems.addItem(item);
        }
        this.serviceDiscoveryManager.publishItems(str, "http://jabber.org/protocol/commands", discoverItems);
    }

    public RemoteCommand getRemoteCommand(String str, String str2) {
        return new RemoteCommand(connection(), str2, str);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0143, code lost:
        if (org.jivesoftware.smackx.commands.AdHocCommand.Action.execute.equals(r1) != false) goto L_0x0145;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void processAdHocCommand(org.jivesoftware.smackx.commands.packet.AdHocCommandData r9) throws org.jivesoftware.smack.SmackException {
        /*
            r8 = this;
            org.jivesoftware.smack.packet.IQ$Type r0 = r9.getType()
            org.jivesoftware.smack.packet.IQ$Type r1 = org.jivesoftware.smack.packet.IQ.Type.SET
            if (r0 == r1) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            org.jivesoftware.smackx.commands.packet.AdHocCommandData r2 = new org.jivesoftware.smackx.commands.packet.AdHocCommandData
            r2.<init>()
            java.lang.String r0 = r9.getFrom()
            r2.setTo(r0)
            java.lang.String r0 = r9.getPacketID()
            r2.setPacketID(r0)
            java.lang.String r0 = r9.getNode()
            r2.setNode(r0)
            java.lang.String r0 = r9.getTo()
            r2.setId(r0)
            java.lang.String r3 = r9.getSessionID()
            java.lang.String r0 = r9.getNode()
            if (r3 != 0) goto L_0x00ed
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.AdHocCommandManager$AdHocCommandInfo> r1 = r8.commands
            boolean r1 = r1.containsKey(r0)
            if (r1 != 0) goto L_0x0042
            org.jivesoftware.smack.packet.XMPPError$Condition r0 = org.jivesoftware.smack.packet.XMPPError.Condition.item_not_found
            r8.respondError(r2, r0)
            goto L_0x0008
        L_0x0042:
            r1 = 15
            java.lang.String r1 = org.jivesoftware.smack.util.StringUtils.randomString(r1)
            org.jivesoftware.smackx.commands.LocalCommand r0 = r8.newInstanceOfCmd(r0, r1)     // Catch:{ XMPPErrorException -> 0x0064 }
            org.jivesoftware.smack.packet.IQ$Type r3 = org.jivesoftware.smack.packet.IQ.Type.RESULT     // Catch:{ XMPPErrorException -> 0x0064 }
            r2.setType(r3)     // Catch:{ XMPPErrorException -> 0x0064 }
            r0.setData(r2)     // Catch:{ XMPPErrorException -> 0x0064 }
            java.lang.String r3 = r9.getFrom()     // Catch:{ XMPPErrorException -> 0x0064 }
            boolean r3 = r0.hasPermission(r3)     // Catch:{ XMPPErrorException -> 0x0064 }
            if (r3 != 0) goto L_0x0083
            org.jivesoftware.smack.packet.XMPPError$Condition r0 = org.jivesoftware.smack.packet.XMPPError.Condition.forbidden     // Catch:{ XMPPErrorException -> 0x0064 }
            r8.respondError(r2, r0)     // Catch:{ XMPPErrorException -> 0x0064 }
            goto L_0x0008
        L_0x0064:
            r0 = move-exception
            org.jivesoftware.smack.packet.XMPPError r0 = r0.getXMPPError()
            org.jivesoftware.smack.packet.XMPPError$Type r3 = org.jivesoftware.smack.packet.XMPPError.Type.CANCEL
            org.jivesoftware.smack.packet.XMPPError$Type r4 = r0.getType()
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x007f
            org.jivesoftware.smackx.commands.AdHocCommand$Status r3 = org.jivesoftware.smackx.commands.AdHocCommand.Status.canceled
            r2.setStatus(r3)
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.LocalCommand> r3 = r8.executingCommands
            r3.remove(r1)
        L_0x007f:
            r8.respondError(r2, r0)
            goto L_0x0008
        L_0x0083:
            org.jivesoftware.smackx.commands.AdHocCommand$Action r3 = r9.getAction()     // Catch:{ XMPPErrorException -> 0x0064 }
            if (r3 == 0) goto L_0x009a
            org.jivesoftware.smackx.commands.AdHocCommand$Action r4 = org.jivesoftware.smackx.commands.AdHocCommand.Action.unknown     // Catch:{ XMPPErrorException -> 0x0064 }
            boolean r4 = r3.equals(r4)     // Catch:{ XMPPErrorException -> 0x0064 }
            if (r4 == 0) goto L_0x009a
            org.jivesoftware.smack.packet.XMPPError$Condition r0 = org.jivesoftware.smack.packet.XMPPError.Condition.bad_request     // Catch:{ XMPPErrorException -> 0x0064 }
            org.jivesoftware.smackx.commands.AdHocCommand$SpecificErrorCondition r3 = org.jivesoftware.smackx.commands.AdHocCommand.SpecificErrorCondition.malformedAction     // Catch:{ XMPPErrorException -> 0x0064 }
            r8.respondError(r2, r0, r3)     // Catch:{ XMPPErrorException -> 0x0064 }
            goto L_0x0008
        L_0x009a:
            if (r3 == 0) goto L_0x00ad
            org.jivesoftware.smackx.commands.AdHocCommand$Action r4 = org.jivesoftware.smackx.commands.AdHocCommand.Action.execute     // Catch:{ XMPPErrorException -> 0x0064 }
            boolean r3 = r3.equals(r4)     // Catch:{ XMPPErrorException -> 0x0064 }
            if (r3 != 0) goto L_0x00ad
            org.jivesoftware.smack.packet.XMPPError$Condition r0 = org.jivesoftware.smack.packet.XMPPError.Condition.bad_request     // Catch:{ XMPPErrorException -> 0x0064 }
            org.jivesoftware.smackx.commands.AdHocCommand$SpecificErrorCondition r3 = org.jivesoftware.smackx.commands.AdHocCommand.SpecificErrorCondition.badAction     // Catch:{ XMPPErrorException -> 0x0064 }
            r8.respondError(r2, r0, r3)     // Catch:{ XMPPErrorException -> 0x0064 }
            goto L_0x0008
        L_0x00ad:
            r0.incrementStage()     // Catch:{ XMPPErrorException -> 0x0064 }
            r0.execute()     // Catch:{ XMPPErrorException -> 0x0064 }
            boolean r3 = r0.isLastStage()     // Catch:{ XMPPErrorException -> 0x0064 }
            if (r3 == 0) goto L_0x00c7
            org.jivesoftware.smackx.commands.AdHocCommand$Status r0 = org.jivesoftware.smackx.commands.AdHocCommand.Status.completed     // Catch:{ XMPPErrorException -> 0x0064 }
            r2.setStatus(r0)     // Catch:{ XMPPErrorException -> 0x0064 }
        L_0x00be:
            org.jivesoftware.smack.XMPPConnection r0 = r8.connection()     // Catch:{ XMPPErrorException -> 0x0064 }
            r0.sendPacket(r2)     // Catch:{ XMPPErrorException -> 0x0064 }
            goto L_0x0008
        L_0x00c7:
            org.jivesoftware.smackx.commands.AdHocCommand$Status r3 = org.jivesoftware.smackx.commands.AdHocCommand.Status.executing     // Catch:{ XMPPErrorException -> 0x0064 }
            r2.setStatus(r3)     // Catch:{ XMPPErrorException -> 0x0064 }
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.LocalCommand> r3 = r8.executingCommands     // Catch:{ XMPPErrorException -> 0x0064 }
            r3.put(r1, r0)     // Catch:{ XMPPErrorException -> 0x0064 }
            java.lang.Thread r0 = r8.sessionsSweeper     // Catch:{ XMPPErrorException -> 0x0064 }
            if (r0 != 0) goto L_0x00be
            java.lang.Thread r0 = new java.lang.Thread     // Catch:{ XMPPErrorException -> 0x0064 }
            org.jivesoftware.smackx.commands.AdHocCommandManager$6 r3 = new org.jivesoftware.smackx.commands.AdHocCommandManager$6     // Catch:{ XMPPErrorException -> 0x0064 }
            r3.<init>()     // Catch:{ XMPPErrorException -> 0x0064 }
            r0.<init>(r3)     // Catch:{ XMPPErrorException -> 0x0064 }
            r8.sessionsSweeper = r0     // Catch:{ XMPPErrorException -> 0x0064 }
            java.lang.Thread r0 = r8.sessionsSweeper     // Catch:{ XMPPErrorException -> 0x0064 }
            r3 = 1
            r0.setDaemon(r3)     // Catch:{ XMPPErrorException -> 0x0064 }
            java.lang.Thread r0 = r8.sessionsSweeper     // Catch:{ XMPPErrorException -> 0x0064 }
            r0.start()     // Catch:{ XMPPErrorException -> 0x0064 }
            goto L_0x00be
        L_0x00ed:
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.LocalCommand> r0 = r8.executingCommands
            java.lang.Object r0 = r0.get(r3)
            org.jivesoftware.smackx.commands.LocalCommand r0 = (org.jivesoftware.smackx.commands.LocalCommand) r0
            if (r0 != 0) goto L_0x0100
            org.jivesoftware.smack.packet.XMPPError$Condition r0 = org.jivesoftware.smack.packet.XMPPError.Condition.bad_request
            org.jivesoftware.smackx.commands.AdHocCommand$SpecificErrorCondition r1 = org.jivesoftware.smackx.commands.AdHocCommand.SpecificErrorCondition.badSessionid
            r8.respondError(r2, r0, r1)
            goto L_0x0008
        L_0x0100:
            long r4 = r0.getCreationDate()
            long r6 = java.lang.System.currentTimeMillis()
            long r4 = r6 - r4
            r6 = 120000(0x1d4c0, double:5.9288E-319)
            int r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r1 <= 0) goto L_0x011f
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.LocalCommand> r0 = r8.executingCommands
            r0.remove(r3)
            org.jivesoftware.smack.packet.XMPPError$Condition r0 = org.jivesoftware.smack.packet.XMPPError.Condition.not_allowed
            org.jivesoftware.smackx.commands.AdHocCommand$SpecificErrorCondition r1 = org.jivesoftware.smackx.commands.AdHocCommand.SpecificErrorCondition.sessionExpired
            r8.respondError(r2, r0, r1)
            goto L_0x0008
        L_0x011f:
            monitor-enter(r0)
            org.jivesoftware.smackx.commands.AdHocCommand$Action r1 = r9.getAction()     // Catch:{ all -> 0x0138 }
            if (r1 == 0) goto L_0x013b
            org.jivesoftware.smackx.commands.AdHocCommand$Action r4 = org.jivesoftware.smackx.commands.AdHocCommand.Action.unknown     // Catch:{ all -> 0x0138 }
            boolean r4 = r1.equals(r4)     // Catch:{ all -> 0x0138 }
            if (r4 == 0) goto L_0x013b
            org.jivesoftware.smack.packet.XMPPError$Condition r1 = org.jivesoftware.smack.packet.XMPPError.Condition.bad_request     // Catch:{ all -> 0x0138 }
            org.jivesoftware.smackx.commands.AdHocCommand$SpecificErrorCondition r3 = org.jivesoftware.smackx.commands.AdHocCommand.SpecificErrorCondition.malformedAction     // Catch:{ all -> 0x0138 }
            r8.respondError(r2, r1, r3)     // Catch:{ all -> 0x0138 }
            monitor-exit(r0)     // Catch:{ all -> 0x0138 }
            goto L_0x0008
        L_0x0138:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0138 }
            throw r1
        L_0x013b:
            if (r1 == 0) goto L_0x0145
            org.jivesoftware.smackx.commands.AdHocCommand$Action r4 = org.jivesoftware.smackx.commands.AdHocCommand.Action.execute     // Catch:{ all -> 0x0138 }
            boolean r4 = r4.equals(r1)     // Catch:{ all -> 0x0138 }
            if (r4 == 0) goto L_0x0149
        L_0x0145:
            org.jivesoftware.smackx.commands.AdHocCommand$Action r1 = r0.getExecuteAction()     // Catch:{ all -> 0x0138 }
        L_0x0149:
            boolean r4 = r0.isValidAction(r1)     // Catch:{ all -> 0x0138 }
            if (r4 != 0) goto L_0x0159
            org.jivesoftware.smack.packet.XMPPError$Condition r1 = org.jivesoftware.smack.packet.XMPPError.Condition.bad_request     // Catch:{ all -> 0x0138 }
            org.jivesoftware.smackx.commands.AdHocCommand$SpecificErrorCondition r3 = org.jivesoftware.smackx.commands.AdHocCommand.SpecificErrorCondition.badAction     // Catch:{ all -> 0x0138 }
            r8.respondError(r2, r1, r3)     // Catch:{ all -> 0x0138 }
            monitor-exit(r0)     // Catch:{ all -> 0x0138 }
            goto L_0x0008
        L_0x0159:
            org.jivesoftware.smack.packet.IQ$Type r4 = org.jivesoftware.smack.packet.IQ.Type.RESULT     // Catch:{ XMPPErrorException -> 0x0193 }
            r2.setType(r4)     // Catch:{ XMPPErrorException -> 0x0193 }
            r0.setData(r2)     // Catch:{ XMPPErrorException -> 0x0193 }
            org.jivesoftware.smackx.commands.AdHocCommand$Action r4 = org.jivesoftware.smackx.commands.AdHocCommand.Action.next     // Catch:{ XMPPErrorException -> 0x0193 }
            boolean r4 = r4.equals(r1)     // Catch:{ XMPPErrorException -> 0x0193 }
            if (r4 == 0) goto L_0x01b2
            r0.incrementStage()     // Catch:{ XMPPErrorException -> 0x0193 }
            org.jivesoftware.smackx.xdata.Form r1 = new org.jivesoftware.smackx.xdata.Form     // Catch:{ XMPPErrorException -> 0x0193 }
            org.jivesoftware.smackx.xdata.packet.DataForm r4 = r9.getForm()     // Catch:{ XMPPErrorException -> 0x0193 }
            r1.<init>(r4)     // Catch:{ XMPPErrorException -> 0x0193 }
            r0.next(r1)     // Catch:{ XMPPErrorException -> 0x0193 }
            boolean r1 = r0.isLastStage()     // Catch:{ XMPPErrorException -> 0x0193 }
            if (r1 == 0) goto L_0x018d
            org.jivesoftware.smackx.commands.AdHocCommand$Status r1 = org.jivesoftware.smackx.commands.AdHocCommand.Status.completed     // Catch:{ XMPPErrorException -> 0x0193 }
            r2.setStatus(r1)     // Catch:{ XMPPErrorException -> 0x0193 }
        L_0x0183:
            org.jivesoftware.smack.XMPPConnection r1 = r8.connection()     // Catch:{ XMPPErrorException -> 0x0193 }
            r1.sendPacket(r2)     // Catch:{ XMPPErrorException -> 0x0193 }
        L_0x018a:
            monitor-exit(r0)     // Catch:{ all -> 0x0138 }
            goto L_0x0008
        L_0x018d:
            org.jivesoftware.smackx.commands.AdHocCommand$Status r1 = org.jivesoftware.smackx.commands.AdHocCommand.Status.executing     // Catch:{ XMPPErrorException -> 0x0193 }
            r2.setStatus(r1)     // Catch:{ XMPPErrorException -> 0x0193 }
            goto L_0x0183
        L_0x0193:
            r1 = move-exception
            org.jivesoftware.smack.packet.XMPPError r1 = r1.getXMPPError()     // Catch:{ all -> 0x0138 }
            org.jivesoftware.smack.packet.XMPPError$Type r4 = org.jivesoftware.smack.packet.XMPPError.Type.CANCEL     // Catch:{ all -> 0x0138 }
            org.jivesoftware.smack.packet.XMPPError$Type r5 = r1.getType()     // Catch:{ all -> 0x0138 }
            boolean r4 = r4.equals(r5)     // Catch:{ all -> 0x0138 }
            if (r4 == 0) goto L_0x01ae
            org.jivesoftware.smackx.commands.AdHocCommand$Status r4 = org.jivesoftware.smackx.commands.AdHocCommand.Status.canceled     // Catch:{ all -> 0x0138 }
            r2.setStatus(r4)     // Catch:{ all -> 0x0138 }
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.LocalCommand> r4 = r8.executingCommands     // Catch:{ all -> 0x0138 }
            r4.remove(r3)     // Catch:{ all -> 0x0138 }
        L_0x01ae:
            r8.respondError(r2, r1)     // Catch:{ all -> 0x0138 }
            goto L_0x018a
        L_0x01b2:
            org.jivesoftware.smackx.commands.AdHocCommand$Action r4 = org.jivesoftware.smackx.commands.AdHocCommand.Action.complete     // Catch:{ XMPPErrorException -> 0x0193 }
            boolean r4 = r4.equals(r1)     // Catch:{ XMPPErrorException -> 0x0193 }
            if (r4 == 0) goto L_0x01d4
            r0.incrementStage()     // Catch:{ XMPPErrorException -> 0x0193 }
            org.jivesoftware.smackx.xdata.Form r1 = new org.jivesoftware.smackx.xdata.Form     // Catch:{ XMPPErrorException -> 0x0193 }
            org.jivesoftware.smackx.xdata.packet.DataForm r4 = r9.getForm()     // Catch:{ XMPPErrorException -> 0x0193 }
            r1.<init>(r4)     // Catch:{ XMPPErrorException -> 0x0193 }
            r0.complete(r1)     // Catch:{ XMPPErrorException -> 0x0193 }
            org.jivesoftware.smackx.commands.AdHocCommand$Status r1 = org.jivesoftware.smackx.commands.AdHocCommand.Status.completed     // Catch:{ XMPPErrorException -> 0x0193 }
            r2.setStatus(r1)     // Catch:{ XMPPErrorException -> 0x0193 }
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.LocalCommand> r1 = r8.executingCommands     // Catch:{ XMPPErrorException -> 0x0193 }
            r1.remove(r3)     // Catch:{ XMPPErrorException -> 0x0193 }
            goto L_0x0183
        L_0x01d4:
            org.jivesoftware.smackx.commands.AdHocCommand$Action r4 = org.jivesoftware.smackx.commands.AdHocCommand.Action.prev     // Catch:{ XMPPErrorException -> 0x0193 }
            boolean r4 = r4.equals(r1)     // Catch:{ XMPPErrorException -> 0x0193 }
            if (r4 == 0) goto L_0x01e3
            r0.decrementStage()     // Catch:{ XMPPErrorException -> 0x0193 }
            r0.prev()     // Catch:{ XMPPErrorException -> 0x0193 }
            goto L_0x0183
        L_0x01e3:
            org.jivesoftware.smackx.commands.AdHocCommand$Action r4 = org.jivesoftware.smackx.commands.AdHocCommand.Action.cancel     // Catch:{ XMPPErrorException -> 0x0193 }
            boolean r1 = r4.equals(r1)     // Catch:{ XMPPErrorException -> 0x0193 }
            if (r1 == 0) goto L_0x0183
            r0.cancel()     // Catch:{ XMPPErrorException -> 0x0193 }
            org.jivesoftware.smackx.commands.AdHocCommand$Status r1 = org.jivesoftware.smackx.commands.AdHocCommand.Status.canceled     // Catch:{ XMPPErrorException -> 0x0193 }
            r2.setStatus(r1)     // Catch:{ XMPPErrorException -> 0x0193 }
            java.util.Map<java.lang.String, org.jivesoftware.smackx.commands.LocalCommand> r1 = r8.executingCommands     // Catch:{ XMPPErrorException -> 0x0193 }
            r1.remove(r3)     // Catch:{ XMPPErrorException -> 0x0193 }
            goto L_0x0183
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.commands.AdHocCommandManager.processAdHocCommand(org.jivesoftware.smackx.commands.packet.AdHocCommandData):void");
    }

    private void respondError(AdHocCommandData adHocCommandData, XMPPError.Condition condition) throws SmackException.NotConnectedException {
        respondError(adHocCommandData, new XMPPError(condition));
    }

    private void respondError(AdHocCommandData adHocCommandData, XMPPError.Condition condition, AdHocCommand.SpecificErrorCondition specificErrorCondition) throws SmackException.NotConnectedException {
        XMPPError xMPPError = new XMPPError(condition);
        xMPPError.addExtension(new AdHocCommandData.SpecificError(specificErrorCondition));
        respondError(adHocCommandData, xMPPError);
    }

    private void respondError(AdHocCommandData adHocCommandData, XMPPError xMPPError) throws SmackException.NotConnectedException {
        adHocCommandData.setType(IQ.Type.ERROR);
        adHocCommandData.setError(xMPPError);
        connection().sendPacket(adHocCommandData);
    }

    private LocalCommand newInstanceOfCmd(String str, String str2) throws XMPPException.XMPPErrorException {
        AdHocCommandInfo adHocCommandInfo = this.commands.get(str);
        try {
            LocalCommand commandInstance = adHocCommandInfo.getCommandInstance();
            commandInstance.setSessionID(str2);
            commandInstance.setName(adHocCommandInfo.getName());
            commandInstance.setNode(adHocCommandInfo.getNode());
            return commandInstance;
        } catch (InstantiationException e) {
            throw new XMPPException.XMPPErrorException(new XMPPError(XMPPError.Condition.internal_server_error));
        } catch (IllegalAccessException e2) {
            throw new XMPPException.XMPPErrorException(new XMPPError(XMPPError.Condition.internal_server_error));
        }
    }

    /* access modifiers changed from: private */
    public Collection<AdHocCommandInfo> getRegisteredCommands() {
        return this.commands.values();
    }

    private static class AdHocCommandInfo {
        private LocalCommandFactory factory;
        private String name;
        private String node;
        private String ownerJID;

        public AdHocCommandInfo(String str, String str2, String str3, LocalCommandFactory localCommandFactory) {
            this.node = str;
            this.name = str2;
            this.ownerJID = str3;
            this.factory = localCommandFactory;
        }

        public LocalCommand getCommandInstance() throws InstantiationException, IllegalAccessException {
            return this.factory.getInstance();
        }

        public String getName() {
            return this.name;
        }

        public String getNode() {
            return this.node;
        }

        public String getOwnerJID() {
            return this.ownerJID;
        }
    }
}
