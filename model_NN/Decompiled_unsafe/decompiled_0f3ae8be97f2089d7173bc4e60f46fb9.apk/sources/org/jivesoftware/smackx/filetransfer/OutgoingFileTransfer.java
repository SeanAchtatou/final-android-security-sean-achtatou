package org.jivesoftware.smackx.filetransfer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smackx.filetransfer.FileTransfer;

public class OutgoingFileTransfer extends FileTransfer {
    private static int RESPONSE_TIMEOUT = 60000;
    private NegotiationProgress callback;
    private String initiator;
    /* access modifiers changed from: private */
    public OutputStream outputStream;
    private Thread transferThread;

    public interface NegotiationProgress {
        void errorEstablishingStream(Exception exc);

        void outputStreamEstablished(OutputStream outputStream);

        void statusUpdated(FileTransfer.Status status, FileTransfer.Status status2);
    }

    public static int getResponseTimeout() {
        return RESPONSE_TIMEOUT;
    }

    public static void setResponseTimeout(int i) {
        RESPONSE_TIMEOUT = i;
    }

    protected OutgoingFileTransfer(String str, String str2, String str3, FileTransferNegotiator fileTransferNegotiator) {
        super(str2, str3, fileTransferNegotiator);
        this.initiator = str;
    }

    /* access modifiers changed from: protected */
    public void setOutputStream(OutputStream outputStream2) {
        if (this.outputStream == null) {
            this.outputStream = outputStream2;
        }
    }

    /* access modifiers changed from: protected */
    public OutputStream getOutputStream() {
        if (getStatus().equals(FileTransfer.Status.negotiated)) {
            return this.outputStream;
        }
        return null;
    }

    public synchronized OutputStream sendFile(String str, long j, String str2) throws XMPPException, SmackException {
        if (isDone() || this.outputStream != null) {
            throw new IllegalStateException("The negotation process has already been attempted on this file transfer");
        }
        try {
            setFileInfo(str, j);
            this.outputStream = negotiateStream(str, j, str2);
        } catch (XMPPException.XMPPErrorException e) {
            handleXMPPException(e);
            throw e;
        }
        return this.outputStream;
    }

    public synchronized void sendFile(String str, long j, String str2, NegotiationProgress negotiationProgress) {
        if (negotiationProgress == null) {
            throw new IllegalArgumentException("Callback progress cannot be null.");
        }
        checkTransferThread();
        if (isDone() || this.outputStream != null) {
            throw new IllegalStateException("The negotation process has already been attempted for this file transfer");
        }
        setFileInfo(str, j);
        this.callback = negotiationProgress;
        final String str3 = str;
        final long j2 = j;
        final String str4 = str2;
        final NegotiationProgress negotiationProgress2 = negotiationProgress;
        this.transferThread = new Thread(new Runnable() {
            public void run() {
                try {
                    OutputStream unused = OutgoingFileTransfer.this.outputStream = OutgoingFileTransfer.this.negotiateStream(str3, j2, str4);
                    negotiationProgress2.outputStreamEstablished(OutgoingFileTransfer.this.outputStream);
                } catch (XMPPException.XMPPErrorException e) {
                    OutgoingFileTransfer.this.handleXMPPException(e);
                } catch (Exception e2) {
                    OutgoingFileTransfer.this.setException(e2);
                }
            }
        }, "File Transfer Negotiation " + this.streamID);
        this.transferThread.start();
    }

    private void checkTransferThread() {
        if ((this.transferThread != null && this.transferThread.isAlive()) || isDone()) {
            throw new IllegalStateException("File transfer in progress or has already completed.");
        }
    }

    public synchronized void sendFile(final File file, final String str) throws SmackException {
        checkTransferThread();
        if (file == null || !file.exists() || !file.canRead()) {
            throw new IllegalArgumentException("Could not read file");
        }
        setFileInfo(file.getAbsolutePath(), file.getName(), file.length());
        this.transferThread = new Thread(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:25:0x0087 A[SYNTHETIC, Splitter:B:25:0x0087] */
            /* JADX WARNING: Removed duplicated region for block: B:34:0x00af A[SYNTHETIC, Splitter:B:34:0x00af] */
            /* JADX WARNING: Removed duplicated region for block: B:41:0x00cb A[SYNTHETIC, Splitter:B:41:0x00cb] */
            /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x00a1=Splitter:B:31:0x00a1, B:22:0x0072=Splitter:B:22:0x0072} */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r6 = this;
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r0 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ XMPPErrorException -> 0x0022, Exception -> 0x0029 }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r1 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ XMPPErrorException -> 0x0022, Exception -> 0x0029 }
                    java.io.File r2 = r5     // Catch:{ XMPPErrorException -> 0x0022, Exception -> 0x0029 }
                    java.lang.String r2 = r2.getName()     // Catch:{ XMPPErrorException -> 0x0022, Exception -> 0x0029 }
                    java.io.File r3 = r5     // Catch:{ XMPPErrorException -> 0x0022, Exception -> 0x0029 }
                    long r3 = r3.length()     // Catch:{ XMPPErrorException -> 0x0022, Exception -> 0x0029 }
                    java.lang.String r5 = r6     // Catch:{ XMPPErrorException -> 0x0022, Exception -> 0x0029 }
                    java.io.OutputStream r1 = r1.negotiateStream(r2, r3, r5)     // Catch:{ XMPPErrorException -> 0x0022, Exception -> 0x0029 }
                    java.io.OutputStream unused = r0.outputStream = r1     // Catch:{ XMPPErrorException -> 0x0022, Exception -> 0x0029 }
                L_0x0019:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r0 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this
                    java.io.OutputStream r0 = r0.outputStream
                    if (r0 != 0) goto L_0x0030
                L_0x0021:
                    return
                L_0x0022:
                    r0 = move-exception
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r1 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this
                    r1.handleXMPPException(r0)
                    goto L_0x0021
                L_0x0029:
                    r0 = move-exception
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r1 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this
                    r1.setException(r0)
                    goto L_0x0019
                L_0x0030:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r0 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this
                    org.jivesoftware.smackx.filetransfer.FileTransfer$Status r1 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.negotiated
                    org.jivesoftware.smackx.filetransfer.FileTransfer$Status r2 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.in_progress
                    boolean r0 = r0.updateStatus(r1, r2)
                    if (r0 == 0) goto L_0x0021
                    r2 = 0
                    java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0070, SmackException -> 0x009f, all -> 0x00c7 }
                    java.io.File r0 = r5     // Catch:{ FileNotFoundException -> 0x0070, SmackException -> 0x009f, all -> 0x00c7 }
                    r1.<init>(r0)     // Catch:{ FileNotFoundException -> 0x0070, SmackException -> 0x009f, all -> 0x00c7 }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r0 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ FileNotFoundException -> 0x00e7, SmackException -> 0x00e5 }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r2 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ FileNotFoundException -> 0x00e7, SmackException -> 0x00e5 }
                    java.io.OutputStream r2 = r2.outputStream     // Catch:{ FileNotFoundException -> 0x00e7, SmackException -> 0x00e5 }
                    r0.writeToStream(r1, r2)     // Catch:{ FileNotFoundException -> 0x00e7, SmackException -> 0x00e5 }
                    if (r1 == 0) goto L_0x0054
                    r1.close()     // Catch:{ IOException -> 0x00e9 }
                L_0x0054:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r0 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x00e9 }
                    java.io.OutputStream r0 = r0.outputStream     // Catch:{ IOException -> 0x00e9 }
                    r0.flush()     // Catch:{ IOException -> 0x00e9 }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r0 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x00e9 }
                    java.io.OutputStream r0 = r0.outputStream     // Catch:{ IOException -> 0x00e9 }
                    r0.close()     // Catch:{ IOException -> 0x00e9 }
                L_0x0066:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r0 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this
                    org.jivesoftware.smackx.filetransfer.FileTransfer$Status r1 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.in_progress
                    org.jivesoftware.smackx.filetransfer.FileTransfer$Status r2 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.complete
                    r0.updateStatus(r1, r2)
                    goto L_0x0021
                L_0x0070:
                    r0 = move-exception
                    r1 = r2
                L_0x0072:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r2 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ all -> 0x00e3 }
                    org.jivesoftware.smackx.filetransfer.FileTransfer$Status r3 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.error     // Catch:{ all -> 0x00e3 }
                    r2.setStatus(r3)     // Catch:{ all -> 0x00e3 }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r2 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ all -> 0x00e3 }
                    org.jivesoftware.smackx.filetransfer.FileTransfer$Error r3 = org.jivesoftware.smackx.filetransfer.FileTransfer.Error.bad_file     // Catch:{ all -> 0x00e3 }
                    r2.setError(r3)     // Catch:{ all -> 0x00e3 }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r2 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ all -> 0x00e3 }
                    r2.setException(r0)     // Catch:{ all -> 0x00e3 }
                    if (r1 == 0) goto L_0x008a
                    r1.close()     // Catch:{ IOException -> 0x009d }
                L_0x008a:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r0 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x009d }
                    java.io.OutputStream r0 = r0.outputStream     // Catch:{ IOException -> 0x009d }
                    r0.flush()     // Catch:{ IOException -> 0x009d }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r0 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x009d }
                    java.io.OutputStream r0 = r0.outputStream     // Catch:{ IOException -> 0x009d }
                    r0.close()     // Catch:{ IOException -> 0x009d }
                    goto L_0x0066
                L_0x009d:
                    r0 = move-exception
                    goto L_0x0066
                L_0x009f:
                    r0 = move-exception
                    r1 = r2
                L_0x00a1:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r2 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ all -> 0x00e3 }
                    org.jivesoftware.smackx.filetransfer.FileTransfer$Status r3 = org.jivesoftware.smackx.filetransfer.FileTransfer.Status.error     // Catch:{ all -> 0x00e3 }
                    r2.setStatus(r3)     // Catch:{ all -> 0x00e3 }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r2 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ all -> 0x00e3 }
                    r2.setException(r0)     // Catch:{ all -> 0x00e3 }
                    if (r1 == 0) goto L_0x00b2
                    r1.close()     // Catch:{ IOException -> 0x00c5 }
                L_0x00b2:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r0 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x00c5 }
                    java.io.OutputStream r0 = r0.outputStream     // Catch:{ IOException -> 0x00c5 }
                    r0.flush()     // Catch:{ IOException -> 0x00c5 }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r0 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x00c5 }
                    java.io.OutputStream r0 = r0.outputStream     // Catch:{ IOException -> 0x00c5 }
                    r0.close()     // Catch:{ IOException -> 0x00c5 }
                    goto L_0x0066
                L_0x00c5:
                    r0 = move-exception
                    goto L_0x0066
                L_0x00c7:
                    r0 = move-exception
                    r1 = r2
                L_0x00c9:
                    if (r1 == 0) goto L_0x00ce
                    r1.close()     // Catch:{ IOException -> 0x00e1 }
                L_0x00ce:
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r1 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x00e1 }
                    java.io.OutputStream r1 = r1.outputStream     // Catch:{ IOException -> 0x00e1 }
                    r1.flush()     // Catch:{ IOException -> 0x00e1 }
                    org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer r1 = org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.this     // Catch:{ IOException -> 0x00e1 }
                    java.io.OutputStream r1 = r1.outputStream     // Catch:{ IOException -> 0x00e1 }
                    r1.close()     // Catch:{ IOException -> 0x00e1 }
                L_0x00e0:
                    throw r0
                L_0x00e1:
                    r1 = move-exception
                    goto L_0x00e0
                L_0x00e3:
                    r0 = move-exception
                    goto L_0x00c9
                L_0x00e5:
                    r0 = move-exception
                    goto L_0x00a1
                L_0x00e7:
                    r0 = move-exception
                    goto L_0x0072
                L_0x00e9:
                    r0 = move-exception
                    goto L_0x0066
                */
                throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer.AnonymousClass2.run():void");
            }
        }, "File Transfer " + this.streamID);
        this.transferThread.start();
    }

    public synchronized void sendStream(InputStream inputStream, String str, long j, String str2) {
        checkTransferThread();
        setFileInfo(str, j);
        final String str3 = str;
        final long j2 = j;
        final String str4 = str2;
        final InputStream inputStream2 = inputStream;
        this.transferThread = new Thread(new Runnable() {
            public void run() {
                try {
                    OutputStream unused = OutgoingFileTransfer.this.outputStream = OutgoingFileTransfer.this.negotiateStream(str3, j2, str4);
                } catch (XMPPException.XMPPErrorException e) {
                    OutgoingFileTransfer.this.handleXMPPException(e);
                    return;
                } catch (Exception e2) {
                    OutgoingFileTransfer.this.setException(e2);
                }
                if (OutgoingFileTransfer.this.outputStream != null && OutgoingFileTransfer.this.updateStatus(FileTransfer.Status.negotiated, FileTransfer.Status.in_progress)) {
                    try {
                        OutgoingFileTransfer.this.writeToStream(inputStream2, OutgoingFileTransfer.this.outputStream);
                        try {
                            if (inputStream2 != null) {
                                inputStream2.close();
                            }
                            OutgoingFileTransfer.this.outputStream.flush();
                            OutgoingFileTransfer.this.outputStream.close();
                        } catch (IOException e3) {
                        }
                    } catch (SmackException e4) {
                        OutgoingFileTransfer.this.setStatus(FileTransfer.Status.error);
                        OutgoingFileTransfer.this.setException(e4);
                        try {
                            if (inputStream2 != null) {
                                inputStream2.close();
                            }
                            OutgoingFileTransfer.this.outputStream.flush();
                            OutgoingFileTransfer.this.outputStream.close();
                        } catch (IOException e5) {
                        }
                    } catch (Throwable th) {
                        try {
                            if (inputStream2 != null) {
                                inputStream2.close();
                            }
                            OutgoingFileTransfer.this.outputStream.flush();
                            OutgoingFileTransfer.this.outputStream.close();
                        } catch (IOException e6) {
                        }
                        throw th;
                    }
                    OutgoingFileTransfer.this.updateStatus(FileTransfer.Status.in_progress, FileTransfer.Status.complete);
                }
            }
        }, "File Transfer " + this.streamID);
        this.transferThread.start();
    }

    /* access modifiers changed from: private */
    public void handleXMPPException(XMPPException.XMPPErrorException xMPPErrorException) {
        XMPPError xMPPError = xMPPErrorException.getXMPPError();
        if (xMPPError != null) {
            String condition = xMPPError.getCondition();
            if (XMPPError.Condition.forbidden.equals((CharSequence) condition)) {
                setStatus(FileTransfer.Status.refused);
                return;
            } else if (XMPPError.Condition.bad_request.equals((CharSequence) condition)) {
                setStatus(FileTransfer.Status.error);
                setError(FileTransfer.Error.not_acceptable);
            } else {
                setStatus(FileTransfer.Status.error);
            }
        }
        setException(xMPPErrorException);
    }

    public long getBytesSent() {
        return this.amountWritten;
    }

    /* access modifiers changed from: private */
    public OutputStream negotiateStream(String str, long j, String str2) throws SmackException, XMPPException {
        if (!updateStatus(FileTransfer.Status.initial, FileTransfer.Status.negotiating_transfer)) {
            throw new SmackException.IllegalStateChangeException();
        }
        StreamNegotiator negotiateOutgoingTransfer = this.negotiator.negotiateOutgoingTransfer(getPeer(), this.streamID, str, j, str2, RESPONSE_TIMEOUT);
        if (negotiateOutgoingTransfer == null) {
            setStatus(FileTransfer.Status.error);
            setError(FileTransfer.Error.no_response);
            return null;
        } else if (!updateStatus(FileTransfer.Status.negotiating_transfer, FileTransfer.Status.negotiating_stream)) {
            throw new SmackException.IllegalStateChangeException();
        } else {
            this.outputStream = negotiateOutgoingTransfer.createOutgoingStream(this.streamID, this.initiator, getPeer());
            if (updateStatus(FileTransfer.Status.negotiating_stream, FileTransfer.Status.negotiated)) {
                return this.outputStream;
            }
            throw new SmackException.IllegalStateChangeException();
        }
    }

    public void cancel() {
        setStatus(FileTransfer.Status.cancelled);
    }

    /* access modifiers changed from: protected */
    public boolean updateStatus(FileTransfer.Status status, FileTransfer.Status status2) {
        boolean updateStatus = super.updateStatus(status, status2);
        if (this.callback != null && updateStatus) {
            this.callback.statusUpdated(status, status2);
        }
        return updateStatus;
    }

    /* access modifiers changed from: protected */
    public void setStatus(FileTransfer.Status status) {
        FileTransfer.Status status2 = getStatus();
        super.setStatus(status);
        if (this.callback != null) {
            this.callback.statusUpdated(status2, status);
        }
    }

    /* access modifiers changed from: protected */
    public void setException(Exception exc) {
        super.setException(exc);
        if (this.callback != null) {
            this.callback.errorEstablishingStream(exc);
        }
    }
}
