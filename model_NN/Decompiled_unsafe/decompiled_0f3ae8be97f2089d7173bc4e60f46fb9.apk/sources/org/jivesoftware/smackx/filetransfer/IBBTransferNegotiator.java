package org.jivesoftware.smackx.filetransfer;

import java.io.InputStream;
import java.io.OutputStream;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.AndFilter;
import org.jivesoftware.smack.filter.FromMatchesFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.bytestreams.ibb.InBandBytestreamManager;
import org.jivesoftware.smackx.bytestreams.ibb.InBandBytestreamRequest;
import org.jivesoftware.smackx.bytestreams.ibb.InBandBytestreamSession;
import org.jivesoftware.smackx.bytestreams.ibb.packet.Open;
import org.jivesoftware.smackx.si.packet.StreamInitiation;

public class IBBTransferNegotiator extends StreamNegotiator {
    private XMPPConnection connection;
    private InBandBytestreamManager manager;

    protected IBBTransferNegotiator(XMPPConnection xMPPConnection) {
        this.connection = xMPPConnection;
        this.manager = InBandBytestreamManager.getByteStreamManager(xMPPConnection);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jivesoftware.smackx.bytestreams.ibb.InBandBytestreamManager.establishSession(java.lang.String, java.lang.String):org.jivesoftware.smackx.bytestreams.ibb.InBandBytestreamSession
     arg types: [java.lang.String, java.lang.String]
     candidates:
      org.jivesoftware.smackx.bytestreams.ibb.InBandBytestreamManager.establishSession(java.lang.String, java.lang.String):org.jivesoftware.smackx.bytestreams.BytestreamSession
      org.jivesoftware.smackx.bytestreams.BytestreamManager.establishSession(java.lang.String, java.lang.String):org.jivesoftware.smackx.bytestreams.BytestreamSession
      org.jivesoftware.smackx.bytestreams.ibb.InBandBytestreamManager.establishSession(java.lang.String, java.lang.String):org.jivesoftware.smackx.bytestreams.ibb.InBandBytestreamSession */
    public OutputStream createOutgoingStream(String str, String str2, String str3) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        InBandBytestreamSession establishSession = this.manager.establishSession(str3, str);
        establishSession.setCloseBothStreamsEnabled(true);
        return establishSession.getOutputStream();
    }

    public InputStream createIncomingStream(StreamInitiation streamInitiation) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        this.manager.ignoreBytestreamRequestOnce(streamInitiation.getSessionID());
        return negotiateIncomingStream(initiateIncomingStream(this.connection, streamInitiation));
    }

    public PacketFilter getInitiationPacketFilter(String str, String str2) {
        this.manager.ignoreBytestreamRequestOnce(str2);
        return new AndFilter(FromMatchesFilter.create(str), new IBBOpenSidFilter(str2));
    }

    public String[] getNamespaces() {
        return new String[]{InBandBytestreamManager.NAMESPACE};
    }

    /* access modifiers changed from: package-private */
    public InputStream negotiateIncomingStream(Packet packet) throws SmackException.NotConnectedException {
        InBandBytestreamSession accept = new ByteStreamRequest(this.manager, (Open) packet).accept();
        accept.setCloseBothStreamsEnabled(true);
        return accept.getInputStream();
    }

    public void cleanup() {
    }

    private static class IBBOpenSidFilter extends PacketTypeFilter {
        private String sessionID;

        public IBBOpenSidFilter(String str) {
            super(Open.class);
            if (str == null) {
                throw new IllegalArgumentException("StreamID cannot be null");
            }
            this.sessionID = str;
        }

        public boolean accept(Packet packet) {
            if (!super.accept(packet)) {
                return false;
            }
            Open open = (Open) packet;
            if (!this.sessionID.equals(open.getSessionID()) || !IQ.Type.SET.equals(open.getType())) {
                return false;
            }
            return true;
        }
    }

    private static class ByteStreamRequest extends InBandBytestreamRequest {
        private ByteStreamRequest(InBandBytestreamManager inBandBytestreamManager, Open open) {
            super(inBandBytestreamManager, open);
        }
    }
}
