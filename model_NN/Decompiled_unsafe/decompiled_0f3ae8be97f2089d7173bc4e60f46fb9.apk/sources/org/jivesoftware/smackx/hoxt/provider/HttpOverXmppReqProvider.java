package org.jivesoftware.smackx.hoxt.provider;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smackx.hoxt.packet.HttpMethod;
import org.jivesoftware.smackx.hoxt.packet.HttpOverXmppReq;
import org.xmlpull.v1.XmlPullParser;

public class HttpOverXmppReqProvider extends AbstractHttpOverXmppProvider {
    private static final String ATTRIBUTE_MAX_CHUNK_SIZE = "maxChunkSize";
    private static final String ATTRIBUTE_METHOD = "method";
    private static final String ATTRIBUTE_RESOURCE = "resource";
    private static final String ELEMENT_REQ = "req";

    public IQ parseIQ(XmlPullParser xmlPullParser) throws Exception {
        String attributeValue = xmlPullParser.getAttributeValue("", ATTRIBUTE_METHOD);
        String attributeValue2 = xmlPullParser.getAttributeValue("", ATTRIBUTE_RESOURCE);
        String attributeValue3 = xmlPullParser.getAttributeValue("", "version");
        String attributeValue4 = xmlPullParser.getAttributeValue("", ATTRIBUTE_MAX_CHUNK_SIZE);
        HttpOverXmppReq.Req req = new HttpOverXmppReq.Req(HttpMethod.valueOf(attributeValue), attributeValue2);
        req.setVersion(attributeValue3);
        Boolean bool = true;
        Boolean bool2 = true;
        Boolean bool3 = true;
        String attributeValue5 = xmlPullParser.getAttributeValue("", "sipub");
        String attributeValue6 = xmlPullParser.getAttributeValue("", "ibb");
        String attributeValue7 = xmlPullParser.getAttributeValue("", "jingle");
        if (attributeValue5 != null) {
            bool = Boolean.valueOf(attributeValue5);
        }
        if (attributeValue6 != null) {
            bool3 = Boolean.valueOf(attributeValue6);
        }
        if (attributeValue7 != null) {
            bool2 = Boolean.valueOf(attributeValue7);
        }
        req.setIbb(bool3.booleanValue());
        req.setSipub(bool.booleanValue());
        req.setJingle(bool2.booleanValue());
        if (attributeValue4 != null) {
            req.setMaxChunkSize(Integer.parseInt(attributeValue4));
        }
        parseHeadersAndData(xmlPullParser, ELEMENT_REQ, req);
        HttpOverXmppReq httpOverXmppReq = new HttpOverXmppReq();
        httpOverXmppReq.setReq(req);
        return httpOverXmppReq;
    }
}
