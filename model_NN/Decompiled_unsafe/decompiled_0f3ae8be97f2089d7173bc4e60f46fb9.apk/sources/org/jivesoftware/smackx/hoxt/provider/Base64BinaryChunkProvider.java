package org.jivesoftware.smackx.hoxt.provider;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smackx.hoxt.packet.Base64BinaryChunk;
import org.xmlpull.v1.XmlPullParser;

public class Base64BinaryChunkProvider implements PacketExtensionProvider {
    public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
        boolean z;
        boolean z2 = false;
        String attributeValue = xmlPullParser.getAttributeValue("", Base64BinaryChunk.ATTRIBUTE_STREAM_ID);
        String attributeValue2 = xmlPullParser.getAttributeValue("", Base64BinaryChunk.ATTRIBUTE_LAST);
        if (attributeValue2 != null) {
            z = Boolean.parseBoolean(attributeValue2);
        } else {
            z = false;
        }
        String str = null;
        while (!z2) {
            int next = xmlPullParser.next();
            if (next == 3) {
                if (xmlPullParser.getName().equals(Base64BinaryChunk.ELEMENT_CHUNK)) {
                    z2 = true;
                } else {
                    throw new IllegalArgumentException("unexpected end tag of: " + xmlPullParser.getName());
                }
            } else if (next == 4) {
                str = xmlPullParser.getText();
            } else {
                throw new IllegalArgumentException("unexpected eventType: " + next);
            }
        }
        return new Base64BinaryChunk(str, attributeValue, z);
    }
}
