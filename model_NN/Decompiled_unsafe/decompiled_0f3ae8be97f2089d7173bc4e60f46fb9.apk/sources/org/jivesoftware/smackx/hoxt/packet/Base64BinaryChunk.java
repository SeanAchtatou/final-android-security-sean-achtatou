package org.jivesoftware.smackx.hoxt.packet;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smackx.hoxt.HOXTManager;

public class Base64BinaryChunk implements PacketExtension {
    public static final String ATTRIBUTE_LAST = "last";
    public static final String ATTRIBUTE_STREAM_ID = "streamId";
    public static final String ELEMENT_CHUNK = "chunk";
    private final boolean last;
    private final String streamId;
    private final String text;

    public Base64BinaryChunk(String str, String str2, boolean z) {
        this.text = str;
        this.streamId = str2;
        this.last = z;
    }

    public Base64BinaryChunk(String str, String str2) {
        this(str, str2, false);
    }

    public String getStreamId() {
        return this.streamId;
    }

    public boolean isLast() {
        return this.last;
    }

    public String getText() {
        return this.text;
    }

    public String getElementName() {
        return ELEMENT_CHUNK;
    }

    public String getNamespace() {
        return HOXTManager.NAMESPACE;
    }

    public String toXML() {
        return "<chunk xmlns='urn:xmpp:http' streamId='" + this.streamId + "' last='" + Boolean.toString(this.last) + "'>" + this.text + "</chunk>";
    }
}
