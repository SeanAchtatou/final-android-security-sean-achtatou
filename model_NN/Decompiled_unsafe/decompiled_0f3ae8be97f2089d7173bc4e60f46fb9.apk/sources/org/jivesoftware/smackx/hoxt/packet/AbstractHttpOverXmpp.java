package org.jivesoftware.smackx.hoxt.packet;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smackx.shim.packet.HeadersExtension;

public abstract class AbstractHttpOverXmpp extends IQ {

    public interface DataChild {
        String toXML();
    }

    public static abstract class AbstractBody {
        private Data data;
        private HeadersExtension headers;
        protected String version;

        /* access modifiers changed from: protected */
        public abstract String getEndTag();

        /* access modifiers changed from: protected */
        public abstract String getStartTag();

        public String toXML() {
            return getStartTag() + this.headers.toXML() + this.data.toXML() + getEndTag();
        }

        public String getVersion() {
            return this.version;
        }

        public void setVersion(String str) {
            this.version = str;
        }

        public HeadersExtension getHeaders() {
            return this.headers;
        }

        public void setHeaders(HeadersExtension headersExtension) {
            this.headers = headersExtension;
        }

        public Data getData() {
            return this.data;
        }

        public void setData(Data data2) {
            this.data = data2;
        }
    }

    public static class Data {
        private final DataChild child;

        public Data(DataChild dataChild) {
            this.child = dataChild;
        }

        public String toXML() {
            return "<data>" + this.child.toXML() + "</data>";
        }

        public DataChild getChild() {
            return this.child;
        }
    }

    public static class Text implements DataChild {
        private final String text;

        public Text(String str) {
            this.text = str;
        }

        public String toXML() {
            StringBuilder sb = new StringBuilder();
            sb.append("<text>");
            if (this.text != null) {
                sb.append(this.text);
            }
            sb.append("</text>");
            return sb.toString();
        }

        public String getText() {
            return this.text;
        }
    }

    public static class Base64 implements DataChild {
        private final String text;

        public Base64(String str) {
            this.text = str;
        }

        public String toXML() {
            StringBuilder sb = new StringBuilder();
            sb.append("<base64>");
            if (this.text != null) {
                sb.append(this.text);
            }
            sb.append("</base64>");
            return sb.toString();
        }

        public String getText() {
            return this.text;
        }
    }

    public static class Xml implements DataChild {
        private final String text;

        public Xml(String str) {
            this.text = str;
        }

        public String toXML() {
            StringBuilder sb = new StringBuilder();
            sb.append("<xml>");
            if (this.text != null) {
                sb.append(this.text);
            }
            sb.append("</xml>");
            return sb.toString();
        }

        public String getText() {
            return this.text;
        }
    }

    public static class ChunkedBase64 implements DataChild {
        private final String streamId;

        public ChunkedBase64(String str) {
            this.streamId = str;
        }

        public String toXML() {
            return "<chunkedBase64 streamId='" + this.streamId + "'/>";
        }

        public String getStreamId() {
            return this.streamId;
        }
    }

    public static class Ibb implements DataChild {
        private final String sid;

        public Ibb(String str) {
            this.sid = str;
        }

        public String toXML() {
            return "<ibb sid='" + this.sid + "'/>";
        }

        public String getSid() {
            return this.sid;
        }
    }
}
