package org.jivesoftware.smackx.hoxt.packet;

import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.hoxt.HOXTManager;
import org.jivesoftware.smackx.hoxt.packet.AbstractHttpOverXmpp;

public class HttpOverXmppResp extends AbstractHttpOverXmpp {
    private Resp resp;

    public String getChildElementXML() {
        return this.resp.toXML();
    }

    public Resp getResp() {
        return this.resp;
    }

    public void setResp(Resp resp2) {
        this.resp = resp2;
    }

    public static class Resp extends AbstractHttpOverXmpp.AbstractBody {
        private int statusCode;
        private String statusMessage = null;

        /* access modifiers changed from: protected */
        public String getStartTag() {
            StringBuilder sb = new StringBuilder();
            sb.append("<resp");
            sb.append(" ");
            sb.append("xmlns='").append(HOXTManager.NAMESPACE).append("'");
            sb.append(" ");
            sb.append("version='").append(StringUtils.escapeForXML(this.version)).append("'");
            sb.append(" ");
            sb.append("statusCode='").append(Integer.toString(this.statusCode)).append("'");
            if (this.statusMessage != null) {
                sb.append(" ");
                sb.append("statusMessage='").append(StringUtils.escapeForXML(this.statusMessage)).append("'");
            }
            sb.append(">");
            return sb.toString();
        }

        /* access modifiers changed from: protected */
        public String getEndTag() {
            return "</resp>";
        }

        public int getStatusCode() {
            return this.statusCode;
        }

        public void setStatusCode(int i) {
            this.statusCode = i;
        }

        public String getStatusMessage() {
            return this.statusMessage;
        }

        public void setStatusMessage(String str) {
            this.statusMessage = str;
        }
    }
}
