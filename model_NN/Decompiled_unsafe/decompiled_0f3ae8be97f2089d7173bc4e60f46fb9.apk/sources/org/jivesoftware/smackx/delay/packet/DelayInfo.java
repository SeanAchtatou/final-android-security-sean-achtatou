package org.jivesoftware.smackx.delay.packet;

import java.util.Date;
import org.jivesoftware.smack.util.XmppDateTime;

public class DelayInfo extends DelayInformation {
    DelayInformation wrappedInfo;

    public DelayInfo(DelayInformation delayInformation) {
        super(delayInformation.getStamp());
        this.wrappedInfo = delayInformation;
    }

    public String getFrom() {
        return this.wrappedInfo.getFrom();
    }

    public String getReason() {
        return this.wrappedInfo.getReason();
    }

    public Date getStamp() {
        return this.wrappedInfo.getStamp();
    }

    public void setFrom(String str) {
        this.wrappedInfo.setFrom(str);
    }

    public void setReason(String str) {
        this.wrappedInfo.setReason(str);
    }

    public String getElementName() {
        return "delay";
    }

    public String getNamespace() {
        return "urn:xmpp:delay";
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\"");
        sb.append(" stamp=\"");
        sb.append(XmppDateTime.formatXEP0082Date(getStamp()));
        sb.append("\"");
        if (getFrom() != null && getFrom().length() > 0) {
            sb.append(" from=\"").append(getFrom()).append("\"");
        }
        sb.append(">");
        if (getReason() != null && getReason().length() > 0) {
            sb.append(getReason());
        }
        sb.append("</").append(getElementName()).append(">");
        return sb.toString();
    }
}
