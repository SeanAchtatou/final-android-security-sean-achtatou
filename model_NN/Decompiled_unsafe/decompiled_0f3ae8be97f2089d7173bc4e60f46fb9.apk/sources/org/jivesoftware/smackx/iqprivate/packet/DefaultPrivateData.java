package org.jivesoftware.smackx.iqprivate.packet;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DefaultPrivateData implements PrivateData {
    private String elementName;
    private Map<String, String> map;
    private String namespace;

    public DefaultPrivateData(String str, String str2) {
        this.elementName = str;
        this.namespace = str2;
    }

    public String getElementName() {
        return this.elementName;
    }

    public String getNamespace() {
        return this.namespace;
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(this.elementName).append(" xmlns=\"").append(this.namespace).append("\">");
        for (String next : getNames()) {
            String value = getValue(next);
            sb.append("<").append(next).append(">");
            sb.append(value);
            sb.append("</").append(next).append(">");
        }
        sb.append("</").append(this.elementName).append(">");
        return sb.toString();
    }

    public synchronized Set<String> getNames() {
        Set<String> unmodifiableSet;
        if (this.map == null) {
            unmodifiableSet = Collections.emptySet();
        } else {
            unmodifiableSet = Collections.unmodifiableSet(this.map.keySet());
        }
        return unmodifiableSet;
    }

    public synchronized String getValue(String str) {
        String str2;
        if (this.map == null) {
            str2 = null;
        } else {
            str2 = this.map.get(str);
        }
        return str2;
    }

    public synchronized void setValue(String str, String str2) {
        if (this.map == null) {
            this.map = new HashMap();
        }
        this.map.put(str, str2);
    }
}
