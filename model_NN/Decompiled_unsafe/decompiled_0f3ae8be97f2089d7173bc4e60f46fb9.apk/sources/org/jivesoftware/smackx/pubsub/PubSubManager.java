package org.jivesoftware.smackx.pubsub;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.disco.packet.DiscoverInfo;
import org.jivesoftware.smackx.disco.packet.DiscoverItems;
import org.jivesoftware.smackx.pubsub.packet.PubSub;
import org.jivesoftware.smackx.pubsub.packet.PubSubNamespace;
import org.jivesoftware.smackx.pubsub.util.NodeUtils;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xdata.FormField;

public final class PubSubManager {
    private XMPPConnection con;
    private Map<String, Node> nodeMap = new ConcurrentHashMap();
    private String to;

    public PubSubManager(XMPPConnection xMPPConnection) {
        this.con = xMPPConnection;
        this.to = "pubsub." + xMPPConnection.getServiceName();
    }

    public PubSubManager(XMPPConnection xMPPConnection, String str) {
        this.con = xMPPConnection;
        this.to = str;
    }

    public LeafNode createNode() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        LeafNode leafNode = new LeafNode(this.con, ((NodeExtension) ((PubSub) sendPubsubPacket(IQ.Type.SET, new NodeExtension(PubSubElementType.CREATE), null)).getExtension("create", PubSubNamespace.BASIC.getXmlns())).getNode());
        leafNode.setTo(this.to);
        this.nodeMap.put(leafNode.getId(), leafNode);
        return leafNode;
    }

    public LeafNode createNode(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return (LeafNode) createNode(str, null);
    }

    public Node createNode(String str, Form form) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        PubSub createPubsubPacket = PubSub.createPubsubPacket(this.to, IQ.Type.SET, new NodeExtension(PubSubElementType.CREATE, str), null);
        boolean z = true;
        if (form != null) {
            createPubsubPacket.addExtension(new FormNode(FormNodeType.CONFIGURE, form));
            FormField field = form.getField(ConfigureNodeFields.node_type.getFieldName());
            if (field != null) {
                z = field.getValues().get(0).equals(NodeType.leaf.toString());
            }
        }
        sendPubsubPacket(this.con, createPubsubPacket);
        Node leafNode = z ? new LeafNode(this.con, str) : new CollectionNode(this.con, str);
        leafNode.setTo(this.to);
        this.nodeMap.put(leafNode.getId(), leafNode);
        return leafNode;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public <T extends org.jivesoftware.smackx.pubsub.Node> T getNode(java.lang.String r3) throws org.jivesoftware.smack.SmackException.NoResponseException, org.jivesoftware.smack.XMPPException.XMPPErrorException, org.jivesoftware.smack.SmackException.NotConnectedException {
        /*
            r2 = this;
            java.util.Map<java.lang.String, org.jivesoftware.smackx.pubsub.Node> r0 = r2.nodeMap
            java.lang.Object r0 = r0.get(r3)
            org.jivesoftware.smackx.pubsub.Node r0 = (org.jivesoftware.smackx.pubsub.Node) r0
            if (r0 != 0) goto L_0x004f
            org.jivesoftware.smackx.disco.packet.DiscoverInfo r0 = new org.jivesoftware.smackx.disco.packet.DiscoverInfo
            r0.<init>()
            java.lang.String r1 = r2.to
            r0.setTo(r1)
            r0.setNode(r3)
            org.jivesoftware.smack.XMPPConnection r1 = r2.con
            org.jivesoftware.smack.PacketCollector r0 = r1.createPacketCollectorAndSend(r0)
            org.jivesoftware.smack.packet.Packet r0 = r0.nextResultOrThrow()
            org.jivesoftware.smackx.disco.packet.DiscoverInfo r0 = (org.jivesoftware.smackx.disco.packet.DiscoverInfo) r0
            java.util.List r0 = r0.getIdentities()
            r1 = 0
            java.lang.Object r0 = r0.get(r1)
            org.jivesoftware.smackx.disco.packet.DiscoverInfo$Identity r0 = (org.jivesoftware.smackx.disco.packet.DiscoverInfo.Identity) r0
            java.lang.String r0 = r0.getType()
            org.jivesoftware.smackx.pubsub.NodeType r1 = org.jivesoftware.smackx.pubsub.NodeType.leaf
            java.lang.String r1 = r1.toString()
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0050
            org.jivesoftware.smackx.pubsub.LeafNode r0 = new org.jivesoftware.smackx.pubsub.LeafNode
            org.jivesoftware.smack.XMPPConnection r1 = r2.con
            r0.<init>(r1, r3)
        L_0x0045:
            java.lang.String r1 = r2.to
            r0.setTo(r1)
            java.util.Map<java.lang.String, org.jivesoftware.smackx.pubsub.Node> r1 = r2.nodeMap
            r1.put(r3, r0)
        L_0x004f:
            return r0
        L_0x0050:
            org.jivesoftware.smackx.pubsub.CollectionNode r0 = new org.jivesoftware.smackx.pubsub.CollectionNode
            org.jivesoftware.smack.XMPPConnection r1 = r2.con
            r0.<init>(r1, r3)
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.pubsub.PubSubManager.getNode(java.lang.String):org.jivesoftware.smackx.pubsub.Node");
    }

    public DiscoverItems discoverNodes(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        DiscoverItems discoverItems = new DiscoverItems();
        if (str != null) {
            discoverItems.setNode(str);
        }
        discoverItems.setTo(this.to);
        return (DiscoverItems) this.con.createPacketCollectorAndSend(discoverItems).nextResultOrThrow();
    }

    public List<Subscription> getSubscriptions() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return ((SubscriptionsExtension) sendPubsubPacket(IQ.Type.GET, new NodeExtension(PubSubElementType.SUBSCRIPTIONS), null).getExtension(PubSubElementType.SUBSCRIPTIONS.getElementName(), PubSubElementType.SUBSCRIPTIONS.getNamespace().getXmlns())).getSubscriptions();
    }

    public List<Affiliation> getAffiliations() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return ((AffiliationsExtension) ((PubSub) sendPubsubPacket(IQ.Type.GET, new NodeExtension(PubSubElementType.AFFILIATIONS), null)).getExtension(PubSubElementType.AFFILIATIONS)).getAffiliations();
    }

    public void deleteNode(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        sendPubsubPacket(IQ.Type.SET, new NodeExtension(PubSubElementType.DELETE, str), PubSubElementType.DELETE.getNamespace());
        this.nodeMap.remove(str);
    }

    public ConfigureForm getDefaultConfiguration() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return NodeUtils.getFormFromPacket((PubSub) sendPubsubPacket(IQ.Type.GET, new NodeExtension(PubSubElementType.DEFAULT), PubSubElementType.DEFAULT.getNamespace()), PubSubElementType.DEFAULT);
    }

    public DiscoverInfo getSupportedFeatures() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return ServiceDiscoveryManager.getInstanceFor(this.con).discoverInfo(this.to);
    }

    private Packet sendPubsubPacket(IQ.Type type, PacketExtension packetExtension, PubSubNamespace pubSubNamespace) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return sendPubsubPacket(this.con, this.to, type, Collections.singletonList(packetExtension), pubSubNamespace);
    }

    static Packet sendPubsubPacket(XMPPConnection xMPPConnection, String str, IQ.Type type, List<PacketExtension> list, PubSubNamespace pubSubNamespace) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        PubSub pubSub = new PubSub(str, type, pubSubNamespace);
        for (PacketExtension addExtension : list) {
            pubSub.addExtension(addExtension);
        }
        return sendPubsubPacket(xMPPConnection, pubSub);
    }

    static Packet sendPubsubPacket(XMPPConnection xMPPConnection, PubSub pubSub) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return xMPPConnection.createPacketCollectorAndSend(pubSub).nextResultOrThrow();
    }
}
