package org.jivesoftware.smackx.pep.packet;

import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smackx.pubsub.packet.PubSub;

public class PEPEvent implements PacketExtension {
    PEPItem item;

    public PEPEvent() {
    }

    public PEPEvent(PEPItem pEPItem) {
        this.item = pEPItem;
    }

    public void addPEPItem(PEPItem pEPItem) {
        this.item = pEPItem;
    }

    public String getElementName() {
        return "event";
    }

    public String getNamespace() {
        return PubSub.NAMESPACE;
    }

    public String toXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\">");
        sb.append(this.item.toXML());
        sb.append("</").append(getElementName()).append(">");
        return sb.toString();
    }
}
