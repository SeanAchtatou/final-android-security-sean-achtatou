package org.jivesoftware.smackx.pep;

import java.util.ArrayList;
import java.util.List;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.PacketExtensionFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.pep.packet.PEPEvent;
import org.jivesoftware.smackx.pep.packet.PEPItem;
import org.jivesoftware.smackx.pep.packet.PEPPubSub;

public class PEPManager {
    private XMPPConnection connection;
    private PacketFilter packetFilter = new PacketExtensionFilter("event", "http://jabber.org/protocol/pubsub#event");
    private PacketListener packetListener;
    private List<PEPListener> pepListeners = new ArrayList();

    public PEPManager(XMPPConnection xMPPConnection) {
        this.connection = xMPPConnection;
        init();
    }

    public void addPEPListener(PEPListener pEPListener) {
        synchronized (this.pepListeners) {
            if (!this.pepListeners.contains(pEPListener)) {
                this.pepListeners.add(pEPListener);
            }
        }
    }

    public void removePEPListener(PEPListener pEPListener) {
        synchronized (this.pepListeners) {
            this.pepListeners.remove(pEPListener);
        }
    }

    public void publish(PEPItem pEPItem) throws SmackException.NotConnectedException {
        PEPPubSub pEPPubSub = new PEPPubSub(pEPItem);
        pEPPubSub.setType(IQ.Type.SET);
        this.connection.sendPacket(pEPPubSub);
    }

    /* access modifiers changed from: private */
    public void firePEPListeners(String str, PEPEvent pEPEvent) {
        PEPListener[] pEPListenerArr;
        synchronized (this.pepListeners) {
            pEPListenerArr = new PEPListener[this.pepListeners.size()];
            this.pepListeners.toArray(pEPListenerArr);
        }
        for (PEPListener eventReceived : pEPListenerArr) {
            eventReceived.eventReceived(str, pEPEvent);
        }
    }

    private void init() {
        this.packetListener = new PacketListener() {
            public void processPacket(Packet packet) {
                Message message = (Message) packet;
                PEPManager.this.firePEPListeners(message.getFrom(), (PEPEvent) message.getExtension("event", "http://jabber.org/protocol/pubsub#event"));
            }
        };
        this.connection.addPacketListener(this.packetListener, this.packetFilter);
    }

    public void destroy() {
        if (this.connection != null) {
            this.connection.removePacketListener(this.packetListener);
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        destroy();
        super.finalize();
    }
}
