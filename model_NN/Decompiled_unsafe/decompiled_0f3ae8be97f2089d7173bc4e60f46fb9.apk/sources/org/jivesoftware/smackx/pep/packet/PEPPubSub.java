package org.jivesoftware.smackx.pep.packet;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smackx.pubsub.packet.PubSub;

public class PEPPubSub extends IQ {
    PEPItem item;

    public PEPPubSub(PEPItem pEPItem) {
        this.item = pEPItem;
    }

    public String getElementName() {
        return PubSub.ELEMENT;
    }

    public String getNamespace() {
        return PubSub.NAMESPACE;
    }

    public String getChildElementXML() {
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(getElementName()).append(" xmlns=\"").append(getNamespace()).append("\">");
        sb.append("<publish node=\"").append(this.item.getNode()).append("\">");
        sb.append(this.item.toXML());
        sb.append("</publish>");
        sb.append("</").append(getElementName()).append(">");
        return sb.toString();
    }
}
