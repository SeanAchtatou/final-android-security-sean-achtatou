package org.jivesoftware.smackx.pep.provider;

import java.util.HashMap;
import java.util.Map;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.xmlpull.v1.XmlPullParser;

public class PEPProvider implements PacketExtensionProvider {
    Map<String, PacketExtensionProvider> nodeParsers = new HashMap();
    PacketExtension pepItem;

    public void registerPEPParserExtension(String str, PacketExtensionProvider packetExtensionProvider) {
        this.nodeParsers.put(str, packetExtensionProvider);
    }

    public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
        boolean z;
        boolean z2 = false;
        while (!z2) {
            int next = xmlPullParser.next();
            if (next != 2) {
                if (next == 3 && xmlPullParser.getName().equals("event")) {
                    z = true;
                }
                z = z2;
            } else if (xmlPullParser.getName().equals("event")) {
                z = z2;
            } else {
                if (xmlPullParser.getName().equals("items")) {
                    PacketExtensionProvider packetExtensionProvider = this.nodeParsers.get(xmlPullParser.getAttributeValue("", "node"));
                    if (packetExtensionProvider != null) {
                        this.pepItem = packetExtensionProvider.parseExtension(xmlPullParser);
                    }
                    z = z2;
                }
                z = z2;
            }
            z2 = z;
        }
        return this.pepItem;
    }
}
