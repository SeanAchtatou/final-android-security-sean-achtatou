package org.jivesoftware.smackx.vcardtemp.provider;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilderFactory;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.vcardtemp.VCardManager;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class VCardProvider implements IQProvider {
    private static final Logger LOGGER = Logger.getLogger(VCardProvider.class.getName());
    private static final String PREFERRED_ENCODING = "UTF-8";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, org.xmlpull.v1.XmlPullParserException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public IQ parseIQ(XmlPullParser xmlPullParser) throws Exception {
        StringBuilder sb = new StringBuilder();
        try {
            int eventType = xmlPullParser.getEventType();
            while (true) {
                switch (eventType) {
                    case 2:
                        sb.append('<').append(xmlPullParser.getName()).append('>');
                        break;
                    case 3:
                        sb.append("</").append(xmlPullParser.getName()).append('>');
                        break;
                    case 4:
                        sb.append(StringUtils.escapeForXML(xmlPullParser.getText()));
                        break;
                }
                if (eventType == 3 && VCardManager.ELEMENT.equals(xmlPullParser.getName())) {
                    return createVCardFromXML(sb.toString());
                }
                eventType = xmlPullParser.next();
            }
        } catch (XmlPullParserException e) {
            LOGGER.log(Level.SEVERE, "Exception parsing VCard", (Throwable) e);
        } catch (IOException e2) {
            LOGGER.log(Level.SEVERE, "Exception parsing VCard", (Throwable) e2);
        }
    }

    public static VCard createVCardFromXML(String str) throws Exception {
        VCard vCard = new VCard();
        new VCardReader(vCard, DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes(PREFERRED_ENCODING)))).initializeFields();
        return vCard;
    }

    private static class VCardReader {
        private final Document document;
        private final VCard vCard;

        VCardReader(VCard vCard2, Document document2) {
            this.vCard = vCard2;
            this.document = document2;
        }

        public void initializeFields() {
            this.vCard.setFirstName(getTagContents("GIVEN"));
            this.vCard.setLastName(getTagContents("FAMILY"));
            this.vCard.setMiddleName(getTagContents("MIDDLE"));
            setupPhoto();
            setupEmails();
            this.vCard.setOrganization(getTagContents("ORGNAME"));
            this.vCard.setOrganizationUnit(getTagContents("ORGUNIT"));
            setupSimpleFields();
            setupPhones();
            setupAddresses();
        }

        private void setupPhoto() {
            String str;
            String str2 = null;
            NodeList elementsByTagName = this.document.getElementsByTagName("PHOTO");
            if (elementsByTagName.getLength() == 1) {
                NodeList childNodes = elementsByTagName.item(0).getChildNodes();
                int length = childNodes.getLength();
                ArrayList<Node> arrayList = new ArrayList<>(length);
                for (int i = 0; i < length; i++) {
                    arrayList.add(childNodes.item(i));
                }
                String str3 = null;
                for (Node node : arrayList) {
                    String nodeName = node.getNodeName();
                    String textContent = node.getTextContent();
                    if (nodeName.equals("BINVAL")) {
                        String str4 = str2;
                        str = textContent;
                        textContent = str4;
                    } else if (nodeName.equals("TYPE")) {
                        str = str3;
                    } else {
                        textContent = str2;
                        str = str3;
                    }
                    str3 = str;
                    str2 = textContent;
                }
                if (str3 != null && str2 != null) {
                    this.vCard.setAvatar(str3, str2);
                }
            }
        }

        private void setupEmails() {
            NodeList elementsByTagName = this.document.getElementsByTagName("USERID");
            if (elementsByTagName != null) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 < elementsByTagName.getLength()) {
                        Element element = (Element) elementsByTagName.item(i2);
                        if ("WORK".equals(element.getParentNode().getFirstChild().getNodeName())) {
                            this.vCard.setEmailWork(getTextContent(element));
                        } else {
                            this.vCard.setEmailHome(getTextContent(element));
                        }
                        i = i2 + 1;
                    } else {
                        return;
                    }
                }
            }
        }

        private void setupPhones() {
            String nodeName;
            NodeList elementsByTagName = this.document.getElementsByTagName("TEL");
            if (elementsByTagName != null) {
                for (int i = 0; i < elementsByTagName.getLength(); i++) {
                    NodeList childNodes = elementsByTagName.item(i).getChildNodes();
                    int i2 = 0;
                    String str = null;
                    String str2 = null;
                    String str3 = null;
                    while (i2 < childNodes.getLength()) {
                        Node item = childNodes.item(i2);
                        if (item.getNodeType() != 1) {
                            nodeName = str3;
                        } else {
                            nodeName = item.getNodeName();
                            if ("NUMBER".equals(nodeName)) {
                                str = getTextContent(item);
                                nodeName = str3;
                            } else if (!isWorkHome(nodeName)) {
                                str2 = nodeName;
                                nodeName = str3;
                            }
                        }
                        i2++;
                        str3 = nodeName;
                    }
                    if (str != null) {
                        if (str2 == null) {
                            str2 = "VOICE";
                        }
                        if ("HOME".equals(str3)) {
                            this.vCard.setPhoneHome(str2, str);
                        } else {
                            this.vCard.setPhoneWork(str2, str);
                        }
                    }
                }
            }
        }

        private boolean isWorkHome(String str) {
            return "HOME".equals(str) || "WORK".equals(str);
        }

        private void setupAddresses() {
            String nodeName;
            NodeList elementsByTagName = this.document.getElementsByTagName("ADR");
            if (elementsByTagName != null) {
                for (int i = 0; i < elementsByTagName.getLength(); i++) {
                    String str = null;
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    NodeList childNodes = ((Element) elementsByTagName.item(i)).getChildNodes();
                    int i2 = 0;
                    while (i2 < childNodes.getLength()) {
                        Node item = childNodes.item(i2);
                        if (item.getNodeType() != 1) {
                            nodeName = str;
                        } else {
                            nodeName = item.getNodeName();
                            if (!isWorkHome(nodeName)) {
                                arrayList.add(nodeName);
                                arrayList2.add(getTextContent(item));
                                nodeName = str;
                            }
                        }
                        i2++;
                        str = nodeName;
                    }
                    for (int i3 = 0; i3 < arrayList2.size(); i3++) {
                        if ("HOME".equals(str)) {
                            this.vCard.setAddressFieldHome((String) arrayList.get(i3), (String) arrayList2.get(i3));
                        } else {
                            this.vCard.setAddressFieldWork((String) arrayList.get(i3), (String) arrayList2.get(i3));
                        }
                    }
                }
            }
        }

        private String getTagContents(String str) {
            NodeList elementsByTagName = this.document.getElementsByTagName(str);
            if (elementsByTagName == null || elementsByTagName.getLength() != 1) {
                return null;
            }
            return getTextContent(elementsByTagName.item(0));
        }

        private void setupSimpleFields() {
            NodeList childNodes = this.document.getDocumentElement().getChildNodes();
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node item = childNodes.item(i);
                if (item instanceof Element) {
                    Element element = (Element) item;
                    String nodeName = element.getNodeName();
                    if (element.getChildNodes().getLength() == 0) {
                        this.vCard.setField(nodeName, "");
                    } else if (element.getChildNodes().getLength() == 1 && (element.getChildNodes().item(0) instanceof Text)) {
                        this.vCard.setField(nodeName, getTextContent(element));
                    }
                }
            }
        }

        private String getTextContent(Node node) {
            StringBuilder sb = new StringBuilder();
            appendText(sb, node);
            return sb.toString();
        }

        private void appendText(StringBuilder sb, Node node) {
            NodeList childNodes = node.getChildNodes();
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node item = childNodes.item(i);
                String nodeValue = item.getNodeValue();
                if (nodeValue != null) {
                    sb.append(nodeValue);
                }
                appendText(sb, item);
            }
        }
    }
}
