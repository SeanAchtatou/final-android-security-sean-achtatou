package org.jivesoftware.smackx.jiveproperties.provider;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.jiveproperties.JivePropertiesManager;
import org.jivesoftware.smackx.jiveproperties.packet.JivePropertiesExtension;
import org.jivesoftware.smackx.xdata.FormField;
import org.xmlpull.v1.XmlPullParser;

public class JivePropertiesExtensionProvider implements PacketExtensionProvider {
    private static final Logger LOGGER = Logger.getLogger(JivePropertiesExtensionProvider.class.getName());

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
        Object obj;
        HashMap hashMap = new HashMap();
        while (true) {
            int next = xmlPullParser.next();
            if (next == 2 && xmlPullParser.getName().equals("property")) {
                Object obj2 = null;
                String str = null;
                String str2 = null;
                boolean z = false;
                String str3 = null;
                while (!z) {
                    int next2 = xmlPullParser.next();
                    if (next2 == 2) {
                        String name = xmlPullParser.getName();
                        if (name.equals("name")) {
                            str2 = xmlPullParser.nextText();
                        } else if (name.equals("value")) {
                            str = xmlPullParser.getAttributeValue("", "type");
                            str3 = xmlPullParser.nextText();
                        }
                    } else if (next2 == 3 && xmlPullParser.getName().equals("property")) {
                        if ("integer".equals(str)) {
                            obj = Integer.valueOf(str3);
                        } else if ("long".equals(str)) {
                            obj = Long.valueOf(str3);
                        } else if ("float".equals(str)) {
                            obj = Float.valueOf(str3);
                        } else if ("double".equals(str)) {
                            obj = Double.valueOf(str3);
                        } else if (FormField.TYPE_BOOLEAN.equals(str)) {
                            obj = Boolean.valueOf(str3);
                        } else if ("string".equals(str)) {
                            obj = str3;
                        } else {
                            if ("java-object".equals(str)) {
                                if (JivePropertiesManager.isJavaObjectEnabled()) {
                                    try {
                                        obj = new ObjectInputStream(new ByteArrayInputStream(StringUtils.decodeBase64(str3))).readObject();
                                    } catch (Exception e) {
                                        LOGGER.log(Level.SEVERE, "Error parsing java object", (Throwable) e);
                                        obj = obj2;
                                    }
                                } else {
                                    LOGGER.severe("JavaObject is not enabled. Enable with JivePropertiesManager.setJavaObjectEnabled(true)");
                                }
                            }
                            obj = obj2;
                        }
                        if (!(str2 == null || obj == null)) {
                            hashMap.put(str2, obj);
                        }
                        Object obj3 = obj;
                        z = true;
                        obj2 = obj3;
                    }
                }
            } else if (next == 3 && xmlPullParser.getName().equals(JivePropertiesExtension.ELEMENT)) {
                return new JivePropertiesExtension(hashMap);
            }
        }
    }
}
