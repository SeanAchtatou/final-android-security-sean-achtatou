package org.jivesoftware.smackx.jiveproperties;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.jiveproperties.packet.JivePropertiesExtension;

public class JivePropertiesManager {
    private static boolean javaObjectEnabled = false;

    public static void setJavaObjectEnabled(boolean z) {
        javaObjectEnabled = z;
    }

    public static boolean isJavaObjectEnabled() {
        return javaObjectEnabled;
    }

    public static void addProperty(Packet packet, String str, Object obj) {
        JivePropertiesExtension jivePropertiesExtension = (JivePropertiesExtension) packet.getExtension(JivePropertiesExtension.NAMESPACE);
        if (jivePropertiesExtension == null) {
            jivePropertiesExtension = new JivePropertiesExtension();
            packet.addExtension(jivePropertiesExtension);
        }
        jivePropertiesExtension.setProperty(str, obj);
    }

    public static Object getProperty(Packet packet, String str) {
        JivePropertiesExtension jivePropertiesExtension = (JivePropertiesExtension) packet.getExtension(JivePropertiesExtension.NAMESPACE);
        if (jivePropertiesExtension != null) {
            return jivePropertiesExtension.getProperty(str);
        }
        return null;
    }

    public static Collection<String> getPropertiesNames(Packet packet) {
        JivePropertiesExtension jivePropertiesExtension = (JivePropertiesExtension) packet.getExtension(JivePropertiesExtension.NAMESPACE);
        if (jivePropertiesExtension == null) {
            return Collections.emptyList();
        }
        return jivePropertiesExtension.getPropertyNames();
    }

    public static Map<String, Object> getProperties(Packet packet) {
        JivePropertiesExtension jivePropertiesExtension = (JivePropertiesExtension) packet.getExtension(JivePropertiesExtension.NAMESPACE);
        if (jivePropertiesExtension == null) {
            return Collections.emptyMap();
        }
        return jivePropertiesExtension.getProperties();
    }
}
