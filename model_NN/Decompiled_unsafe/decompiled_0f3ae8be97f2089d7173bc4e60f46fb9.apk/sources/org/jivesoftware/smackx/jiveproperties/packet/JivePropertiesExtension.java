package org.jivesoftware.smackx.jiveproperties.packet;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.jivesoftware.smack.packet.PacketExtension;

public class JivePropertiesExtension implements PacketExtension {
    public static final String ELEMENT = "properties";
    private static final Logger LOGGER = Logger.getLogger(JivePropertiesExtension.class.getName());
    public static final String NAMESPACE = "http://www.jivesoftware.com/xmlns/xmpp/properties";
    private final Map<String, Object> properties;

    public JivePropertiesExtension() {
        this.properties = new HashMap();
    }

    public JivePropertiesExtension(Map<String, Object> map) {
        this.properties = map;
    }

    public synchronized Object getProperty(String str) {
        Object obj;
        if (this.properties == null) {
            obj = null;
        } else {
            obj = this.properties.get(str);
        }
        return obj;
    }

    public synchronized void setProperty(String str, Object obj) {
        if (!(obj instanceof Serializable)) {
            throw new IllegalArgumentException("Value must be serialiazble");
        }
        this.properties.put(str, obj);
    }

    public synchronized void deleteProperty(String str) {
        if (this.properties != null) {
            this.properties.remove(str);
        }
    }

    public synchronized Collection<String> getPropertyNames() {
        Set unmodifiableSet;
        if (this.properties == null) {
            unmodifiableSet = Collections.emptySet();
        } else {
            unmodifiableSet = Collections.unmodifiableSet(new HashSet(this.properties.keySet()));
        }
        return unmodifiableSet;
    }

    public synchronized Map<String, Object> getProperties() {
        Map<String, Object> unmodifiableMap;
        if (this.properties == null) {
            unmodifiableMap = Collections.emptyMap();
        } else {
            unmodifiableMap = Collections.unmodifiableMap(new HashMap(this.properties));
        }
        return unmodifiableMap;
    }

    public String getElementName() {
        return ELEMENT;
    }

    public String getNamespace() {
        return NAMESPACE;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f3 A[SYNTHETIC, Splitter:B:40:0x00f3] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f8 A[SYNTHETIC, Splitter:B:43:0x00f8] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0105 A[SYNTHETIC, Splitter:B:49:0x0105] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x010a A[SYNTHETIC, Splitter:B:52:0x010a] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0042 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.CharSequence toXML() {
        /*
            r9 = this;
            r3 = 0
            org.jivesoftware.smack.util.XmlStringBuilder r5 = new org.jivesoftware.smack.util.XmlStringBuilder
            r5.<init>(r9)
            r5.rightAngelBracket()
            java.util.Collection r0 = r9.getPropertyNames()
            java.util.Iterator r6 = r0.iterator()
        L_0x0011:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x010e
            java.lang.Object r0 = r6.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r9.getProperty(r0)
            java.lang.String r2 = "property"
            r5.openElement(r2)
            java.lang.String r2 = "name"
            r5.element(r2, r0)
            java.lang.String r0 = "value"
            r5.halfOpenElement(r0)
            boolean r0 = r1 instanceof java.lang.Integer
            if (r0 == 0) goto L_0x0058
            java.lang.String r2 = "integer"
            r0 = r1
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            java.lang.String r1 = java.lang.Integer.toString(r0)
            r0 = r2
        L_0x0042:
            java.lang.String r2 = "type"
            r5.attribute(r2, r0)
            r5.rightAngelBracket()
            r5.escape(r1)
            java.lang.String r0 = "value"
            r5.closeElement(r0)
            java.lang.String r0 = "property"
            r5.closeElement(r0)
            goto L_0x0011
        L_0x0058:
            boolean r0 = r1 instanceof java.lang.Long
            if (r0 == 0) goto L_0x0069
            java.lang.String r0 = "long"
            java.lang.Long r1 = (java.lang.Long) r1
            long r1 = r1.longValue()
            java.lang.String r1 = java.lang.Long.toString(r1)
            goto L_0x0042
        L_0x0069:
            boolean r0 = r1 instanceof java.lang.Float
            if (r0 == 0) goto L_0x007a
            java.lang.String r0 = "float"
            java.lang.Float r1 = (java.lang.Float) r1
            float r1 = r1.floatValue()
            java.lang.String r1 = java.lang.Float.toString(r1)
            goto L_0x0042
        L_0x007a:
            boolean r0 = r1 instanceof java.lang.Double
            if (r0 == 0) goto L_0x008b
            java.lang.String r0 = "double"
            java.lang.Double r1 = (java.lang.Double) r1
            double r1 = r1.doubleValue()
            java.lang.String r1 = java.lang.Double.toString(r1)
            goto L_0x0042
        L_0x008b:
            boolean r0 = r1 instanceof java.lang.Boolean
            if (r0 == 0) goto L_0x009c
            java.lang.String r0 = "boolean"
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            java.lang.String r1 = java.lang.Boolean.toString(r1)
            goto L_0x0042
        L_0x009c:
            boolean r0 = r1 instanceof java.lang.String
            if (r0 == 0) goto L_0x00a5
            java.lang.String r0 = "string"
            java.lang.String r1 = (java.lang.String) r1
            goto L_0x0042
        L_0x00a5:
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x00cb, all -> 0x0100 }
            r4.<init>()     // Catch:{ Exception -> 0x00cb, all -> 0x0100 }
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x011f, all -> 0x011a }
            r2.<init>(r4)     // Catch:{ Exception -> 0x011f, all -> 0x011a }
            r2.writeObject(r1)     // Catch:{ Exception -> 0x0123 }
            java.lang.String r0 = "java-object"
            byte[] r1 = r4.toByteArray()     // Catch:{ Exception -> 0x0123 }
            java.lang.String r1 = org.jivesoftware.smack.util.StringUtils.encodeBase64(r1)     // Catch:{ Exception -> 0x0123 }
            if (r2 == 0) goto L_0x00c1
            r2.close()     // Catch:{ Exception -> 0x0112 }
        L_0x00c1:
            if (r4 == 0) goto L_0x0042
            r4.close()     // Catch:{ Exception -> 0x00c8 }
            goto L_0x0042
        L_0x00c8:
            r2 = move-exception
            goto L_0x0042
        L_0x00cb:
            r0 = move-exception
            r1 = r0
            r2 = r3
            r4 = r3
        L_0x00cf:
            java.util.logging.Logger r0 = org.jivesoftware.smackx.jiveproperties.packet.JivePropertiesExtension.LOGGER     // Catch:{ all -> 0x011d }
            java.util.logging.Level r7 = java.util.logging.Level.SEVERE     // Catch:{ all -> 0x011d }
            java.lang.String r8 = "Error encoding java object"
            r0.log(r7, r8, r1)     // Catch:{ all -> 0x011d }
            java.lang.String r0 = "java-object"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x011d }
            r7.<init>()     // Catch:{ all -> 0x011d }
            java.lang.String r8 = "Serializing error: "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x011d }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x011d }
            java.lang.StringBuilder r1 = r7.append(r1)     // Catch:{ all -> 0x011d }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x011d }
            if (r2 == 0) goto L_0x00f6
            r2.close()     // Catch:{ Exception -> 0x0114 }
        L_0x00f6:
            if (r4 == 0) goto L_0x0042
            r4.close()     // Catch:{ Exception -> 0x00fd }
            goto L_0x0042
        L_0x00fd:
            r2 = move-exception
            goto L_0x0042
        L_0x0100:
            r0 = move-exception
            r2 = r3
            r4 = r3
        L_0x0103:
            if (r2 == 0) goto L_0x0108
            r2.close()     // Catch:{ Exception -> 0x0116 }
        L_0x0108:
            if (r4 == 0) goto L_0x010d
            r4.close()     // Catch:{ Exception -> 0x0118 }
        L_0x010d:
            throw r0
        L_0x010e:
            r5.closeElement(r9)
            return r5
        L_0x0112:
            r2 = move-exception
            goto L_0x00c1
        L_0x0114:
            r2 = move-exception
            goto L_0x00f6
        L_0x0116:
            r1 = move-exception
            goto L_0x0108
        L_0x0118:
            r1 = move-exception
            goto L_0x010d
        L_0x011a:
            r0 = move-exception
            r2 = r3
            goto L_0x0103
        L_0x011d:
            r0 = move-exception
            goto L_0x0103
        L_0x011f:
            r0 = move-exception
            r1 = r0
            r2 = r3
            goto L_0x00cf
        L_0x0123:
            r0 = move-exception
            r1 = r0
            goto L_0x00cf
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.jiveproperties.packet.JivePropertiesExtension.toXML():java.lang.CharSequence");
    }
}
