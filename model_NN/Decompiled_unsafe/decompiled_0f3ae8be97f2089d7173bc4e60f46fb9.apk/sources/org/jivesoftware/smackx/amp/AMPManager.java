package org.jivesoftware.smackx.amp;

import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.amp.packet.AMPExtension;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.disco.packet.DiscoverInfo;

public class AMPManager {
    static {
        XMPPConnection.addConnectionCreationListener(new ConnectionCreationListener() {
            public void connectionCreated(XMPPConnection xMPPConnection) {
                AMPManager.setServiceEnabled(xMPPConnection, true);
            }
        });
    }

    public static synchronized void setServiceEnabled(XMPPConnection xMPPConnection, boolean z) {
        synchronized (AMPManager.class) {
            if (isServiceEnabled(xMPPConnection) != z) {
                if (z) {
                    ServiceDiscoveryManager.getInstanceFor(xMPPConnection).addFeature(AMPExtension.NAMESPACE);
                } else {
                    ServiceDiscoveryManager.getInstanceFor(xMPPConnection).removeFeature(AMPExtension.NAMESPACE);
                }
            }
        }
    }

    public static boolean isServiceEnabled(XMPPConnection xMPPConnection) {
        xMPPConnection.getServiceName();
        return ServiceDiscoveryManager.getInstanceFor(xMPPConnection).includesFeature(AMPExtension.NAMESPACE);
    }

    public static boolean isActionSupported(XMPPConnection xMPPConnection, AMPExtension.Action action) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return isFeatureSupportedByServer(xMPPConnection, "http://jabber.org/protocol/amp?action=" + action.toString(), AMPExtension.NAMESPACE);
    }

    public static boolean isConditionSupported(XMPPConnection xMPPConnection, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return isFeatureSupportedByServer(xMPPConnection, "http://jabber.org/protocol/amp?condition=" + str, AMPExtension.NAMESPACE);
    }

    private static boolean isFeatureSupportedByServer(XMPPConnection xMPPConnection, String str, String str2) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        for (DiscoverInfo.Feature var : ServiceDiscoveryManager.getInstanceFor(xMPPConnection).discoverInfo(xMPPConnection.getServiceName(), str2).getFeatures()) {
            if (str.equals(var.getVar())) {
                return true;
            }
        }
        return false;
    }
}
