package org.jivesoftware.smackx.amp.provider;

import java.util.logging.Logger;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.jivesoftware.smackx.amp.AMPDeliverCondition;
import org.jivesoftware.smackx.amp.AMPExpireAtCondition;
import org.jivesoftware.smackx.amp.AMPMatchResourceCondition;
import org.jivesoftware.smackx.amp.packet.AMPExtension;

public class AMPExtensionProvider implements PacketExtensionProvider {
    private static final Logger LOGGER = Logger.getLogger(AMPExtensionProvider.class.getName());

    /* JADX WARNING: Removed duplicated region for block: B:6:0x0026  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0030  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.jivesoftware.smack.packet.PacketExtension parseExtension(org.xmlpull.v1.XmlPullParser r9) throws java.lang.Exception {
        /*
            r8 = this;
            r1 = 0
            java.lang.String r0 = "from"
            java.lang.String r2 = r9.getAttributeValue(r1, r0)
            java.lang.String r0 = "to"
            java.lang.String r3 = r9.getAttributeValue(r1, r0)
            java.lang.String r0 = "status"
            java.lang.String r0 = r9.getAttributeValue(r1, r0)
            if (r0 == 0) goto L_0x0084
            org.jivesoftware.smackx.amp.packet.AMPExtension$Status r0 = org.jivesoftware.smackx.amp.packet.AMPExtension.Status.valueOf(r0)     // Catch:{ IllegalArgumentException -> 0x006b }
        L_0x0019:
            org.jivesoftware.smackx.amp.packet.AMPExtension r4 = new org.jivesoftware.smackx.amp.packet.AMPExtension
            r4.<init>(r2, r3, r0)
            java.lang.String r0 = "per-hop"
            java.lang.String r0 = r9.getAttributeValue(r1, r0)
            if (r0 == 0) goto L_0x002d
            boolean r0 = java.lang.Boolean.parseBoolean(r0)
            r4.setPerHop(r0)
        L_0x002d:
            r0 = 0
        L_0x002e:
            if (r0 != 0) goto L_0x00bc
            int r2 = r9.next()
            r3 = 2
            if (r2 != r3) goto L_0x00aa
            java.lang.String r2 = r9.getName()
            java.lang.String r3 = "rule"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x002e
            java.lang.String r2 = "action"
            java.lang.String r2 = r9.getAttributeValue(r1, r2)
            java.lang.String r3 = "condition"
            java.lang.String r3 = r9.getAttributeValue(r1, r3)
            java.lang.String r5 = "value"
            java.lang.String r5 = r9.getAttributeValue(r1, r5)
            org.jivesoftware.smackx.amp.packet.AMPExtension$Condition r3 = r8.createCondition(r3, r5)
            if (r2 == 0) goto L_0x009f
            org.jivesoftware.smackx.amp.packet.AMPExtension$Action r2 = org.jivesoftware.smackx.amp.packet.AMPExtension.Action.valueOf(r2)     // Catch:{ IllegalArgumentException -> 0x0086 }
        L_0x005f:
            if (r2 == 0) goto L_0x0063
            if (r3 != 0) goto L_0x00a1
        L_0x0063:
            java.util.logging.Logger r2 = org.jivesoftware.smackx.amp.provider.AMPExtensionProvider.LOGGER
            java.lang.String r3 = "Rule is skipped because either it's action or it's condition is invalid"
            r2.severe(r3)
            goto L_0x002e
        L_0x006b:
            r4 = move-exception
            java.util.logging.Logger r4 = org.jivesoftware.smackx.amp.provider.AMPExtensionProvider.LOGGER
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Found invalid amp status "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r0 = r0.toString()
            r4.severe(r0)
        L_0x0084:
            r0 = r1
            goto L_0x0019
        L_0x0086:
            r5 = move-exception
            java.util.logging.Logger r5 = org.jivesoftware.smackx.amp.provider.AMPExtensionProvider.LOGGER
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Found invalid rule action value "
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r2 = r6.append(r2)
            java.lang.String r2 = r2.toString()
            r5.severe(r2)
        L_0x009f:
            r2 = r1
            goto L_0x005f
        L_0x00a1:
            org.jivesoftware.smackx.amp.packet.AMPExtension$Rule r5 = new org.jivesoftware.smackx.amp.packet.AMPExtension$Rule
            r5.<init>(r2, r3)
            r4.addRule(r5)
            goto L_0x002e
        L_0x00aa:
            r3 = 3
            if (r2 != r3) goto L_0x002e
            java.lang.String r2 = r9.getName()
            java.lang.String r3 = "amp"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x002e
            r0 = 1
            goto L_0x002e
        L_0x00bc:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jivesoftware.smackx.amp.provider.AMPExtensionProvider.parseExtension(org.xmlpull.v1.XmlPullParser):org.jivesoftware.smack.packet.PacketExtension");
    }

    private AMPExtension.Condition createCondition(String str, String str2) {
        if (str == null || str2 == null) {
            LOGGER.severe("Can't create rule condition from null name and/or value");
            return null;
        } else if (AMPDeliverCondition.NAME.equals(str)) {
            try {
                return new AMPDeliverCondition(AMPDeliverCondition.Value.valueOf(str2));
            } catch (IllegalArgumentException e) {
                LOGGER.severe("Found invalid rule delivery condition value " + str2);
                return null;
            }
        } else if (AMPExpireAtCondition.NAME.equals(str)) {
            return new AMPExpireAtCondition(str2);
        } else {
            if (AMPMatchResourceCondition.NAME.equals(str)) {
                try {
                    return new AMPMatchResourceCondition(AMPMatchResourceCondition.Value.valueOf(str2));
                } catch (IllegalArgumentException e2) {
                    LOGGER.severe("Found invalid rule match-resource condition value " + str2);
                    return null;
                }
            } else {
                LOGGER.severe("Found unknown rule condition name " + str);
                return null;
            }
        }
    }
}
