package org.jivesoftware.smackx.search;

import java.util.ArrayList;
import java.util.Collection;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.disco.packet.DiscoverItems;
import org.jivesoftware.smackx.xdata.Form;

public class UserSearchManager {
    private XMPPConnection con;
    private UserSearch userSearch = new UserSearch();

    public UserSearchManager(XMPPConnection xMPPConnection) {
        this.con = xMPPConnection;
    }

    public Form getSearchForm(String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return this.userSearch.getSearchForm(this.con, str);
    }

    public ReportedData getSearchResults(Form form, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        return this.userSearch.sendSearchForm(this.con, form, str);
    }

    public Collection<String> getSearchServices() throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        ArrayList arrayList = new ArrayList();
        ServiceDiscoveryManager instanceFor = ServiceDiscoveryManager.getInstanceFor(this.con);
        for (DiscoverItems.Item next : instanceFor.discoverItems(this.con.getServiceName()).getItems()) {
            try {
                try {
                    if (instanceFor.discoverInfo(next.getEntityID()).containsFeature("jabber:iq:search")) {
                        arrayList.add(next.getEntityID());
                    }
                } catch (Exception e) {
                }
            } catch (XMPPException e2) {
            }
        }
        return arrayList;
    }
}
