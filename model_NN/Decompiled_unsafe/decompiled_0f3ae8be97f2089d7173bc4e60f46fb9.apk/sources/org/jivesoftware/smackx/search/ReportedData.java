package org.jivesoftware.smackx.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smackx.xdata.FormField;
import org.jivesoftware.smackx.xdata.packet.DataForm;

public class ReportedData {
    private List<Column> columns = new ArrayList();
    private List<Row> rows = new ArrayList();
    private String title = "";

    public static ReportedData getReportedDataFrom(Packet packet) {
        PacketExtension extension = packet.getExtension("x", DataForm.NAMESPACE);
        if (extension != null) {
            DataForm dataForm = (DataForm) extension;
            if (dataForm.getReportedData() != null) {
                return new ReportedData(dataForm);
            }
        }
        return null;
    }

    private ReportedData(DataForm dataForm) {
        for (FormField next : dataForm.getReportedData().getFields()) {
            this.columns.add(new Column(next.getLabel(), next.getVariable(), next.getType()));
        }
        for (DataForm.Item fields : dataForm.getItems()) {
            ArrayList arrayList = new ArrayList(this.columns.size());
            for (FormField next2 : fields.getFields()) {
                ArrayList arrayList2 = new ArrayList();
                for (String add : next2.getValues()) {
                    arrayList2.add(add);
                }
                arrayList.add(new Field(next2.getVariable(), arrayList2));
            }
            this.rows.add(new Row(arrayList));
        }
        this.title = dataForm.getTitle();
    }

    public ReportedData() {
    }

    public void addRow(Row row) {
        this.rows.add(row);
    }

    public void addColumn(Column column) {
        this.columns.add(column);
    }

    public List<Row> getRows() {
        return Collections.unmodifiableList(new ArrayList(this.rows));
    }

    public List<Column> getColumns() {
        return Collections.unmodifiableList(new ArrayList(this.columns));
    }

    public String getTitle() {
        return this.title;
    }

    public static class Column {
        private String label;
        private String type;
        private String variable;

        public Column(String str, String str2, String str3) {
            this.label = str;
            this.variable = str2;
            this.type = str3;
        }

        public String getLabel() {
            return this.label;
        }

        public String getType() {
            return this.type;
        }

        public String getVariable() {
            return this.variable;
        }
    }

    public static class Row {
        private List<Field> fields = new ArrayList();

        public Row(List<Field> list) {
            this.fields = list;
        }

        public List<String> getValues(String str) {
            for (Field next : getFields()) {
                if (str.equalsIgnoreCase(next.getVariable())) {
                    return next.getValues();
                }
            }
            return null;
        }

        private List<Field> getFields() {
            return Collections.unmodifiableList(new ArrayList(this.fields));
        }
    }

    public static class Field {
        private List<String> values;
        private String variable;

        public Field(String str, List<String> list) {
            this.variable = str;
            this.values = list;
        }

        public String getVariable() {
            return this.variable;
        }

        public List<String> getValues() {
            return Collections.unmodifiableList(this.values);
        }
    }
}
