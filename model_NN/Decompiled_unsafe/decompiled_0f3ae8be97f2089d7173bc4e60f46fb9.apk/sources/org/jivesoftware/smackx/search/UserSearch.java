package org.jivesoftware.smackx.search;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.jivesoftware.smackx.hoxt.packet.Base64BinaryChunk;
import org.jivesoftware.smackx.nick.packet.Nick;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xdata.FormField;
import org.jivesoftware.smackx.xdata.packet.DataForm;
import org.xmlpull.v1.XmlPullParser;

public class UserSearch extends IQ {
    public String getChildElementXML() {
        return "<query xmlns=\"jabber:iq:search\">" + getExtensionsXML() + "</query>";
    }

    public Form getSearchForm(XMPPConnection xMPPConnection, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        UserSearch userSearch = new UserSearch();
        userSearch.setType(IQ.Type.GET);
        userSearch.setTo(str);
        return Form.getFormFrom((IQ) xMPPConnection.createPacketCollectorAndSend(userSearch).nextResultOrThrow());
    }

    public ReportedData sendSearchForm(XMPPConnection xMPPConnection, Form form, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        UserSearch userSearch = new UserSearch();
        userSearch.setType(IQ.Type.SET);
        userSearch.setTo(str);
        userSearch.addExtension(form.getDataFormToSend());
        return ReportedData.getReportedDataFrom((IQ) xMPPConnection.createPacketCollectorAndSend(userSearch).nextResultOrThrow());
    }

    public ReportedData sendSimpleSearchForm(XMPPConnection xMPPConnection, Form form, String str) throws SmackException.NoResponseException, XMPPException.XMPPErrorException, SmackException.NotConnectedException {
        SimpleUserSearch simpleUserSearch = new SimpleUserSearch();
        simpleUserSearch.setForm(form);
        simpleUserSearch.setType(IQ.Type.SET);
        simpleUserSearch.setTo(str);
        return ((SimpleUserSearch) xMPPConnection.createPacketCollectorAndSend(simpleUserSearch).nextResultOrThrow()).getReportedData();
    }

    public static class Provider implements IQProvider {
        public IQ parseIQ(XmlPullParser xmlPullParser) throws Exception {
            UserSearch userSearch = null;
            SimpleUserSearch simpleUserSearch = new SimpleUserSearch();
            boolean z = false;
            while (!z) {
                int next = xmlPullParser.next();
                if (next == 2 && xmlPullParser.getName().equals("instructions")) {
                    UserSearch.buildDataForm(simpleUserSearch, xmlPullParser.nextText(), xmlPullParser);
                    return simpleUserSearch;
                } else if (next == 2 && xmlPullParser.getName().equals(DataForm.Item.ELEMENT)) {
                    simpleUserSearch.parseItems(xmlPullParser);
                    return simpleUserSearch;
                } else if (next == 2 && xmlPullParser.getNamespace().equals(DataForm.NAMESPACE)) {
                    userSearch = new UserSearch();
                    userSearch.addExtension(PacketParserUtils.parsePacketExtension(xmlPullParser.getName(), xmlPullParser.getNamespace(), xmlPullParser));
                } else if (next == 3 && xmlPullParser.getName().equals("query")) {
                    z = true;
                }
            }
            if (userSearch == null) {
                return simpleUserSearch;
            }
            return userSearch;
        }
    }

    /* access modifiers changed from: private */
    public static void buildDataForm(SimpleUserSearch simpleUserSearch, String str, XmlPullParser xmlPullParser) throws Exception {
        DataForm dataForm = new DataForm(Form.TYPE_FORM);
        boolean z = false;
        dataForm.setTitle("User Search");
        dataForm.addInstruction(str);
        while (!z) {
            int next = xmlPullParser.next();
            if (next == 2 && !xmlPullParser.getNamespace().equals(DataForm.NAMESPACE)) {
                String name = xmlPullParser.getName();
                FormField formField = new FormField(name);
                if (name.equals("first")) {
                    formField.setLabel("First Name");
                } else if (name.equals(Base64BinaryChunk.ATTRIBUTE_LAST)) {
                    formField.setLabel("Last Name");
                } else if (name.equals("email")) {
                    formField.setLabel("Email Address");
                } else if (name.equals(Nick.ELEMENT_NAME)) {
                    formField.setLabel("Nickname");
                }
                formField.setType(FormField.TYPE_TEXT_SINGLE);
                dataForm.addField(formField);
            } else if (next == 3) {
                if (xmlPullParser.getName().equals("query")) {
                    z = true;
                }
            } else if (next == 2 && xmlPullParser.getNamespace().equals(DataForm.NAMESPACE)) {
                simpleUserSearch.addExtension(PacketParserUtils.parsePacketExtension(xmlPullParser.getName(), xmlPullParser.getNamespace(), xmlPullParser));
                z = true;
            }
        }
        if (simpleUserSearch.getExtension("x", DataForm.NAMESPACE) == null) {
            simpleUserSearch.addExtension(dataForm);
        }
    }
}
