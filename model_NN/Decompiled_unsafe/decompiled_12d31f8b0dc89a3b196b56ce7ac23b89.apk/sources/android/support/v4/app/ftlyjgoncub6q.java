package android.support.v4.app;

import android.graphics.Rect;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class ftlyjgoncub6q {
    /* access modifiers changed from: private */
    public static Rect cehyzt7dw(View view) {
        Rect rect = new Rect();
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        rect.set(iArr[0], iArr[1], iArr[0] + view.getWidth(), iArr[1] + view.getHeight());
        return rect;
    }

    public static void ozpoxuz523b2(Object obj, ArrayList arrayList) {
        Transition transition = (Transition) obj;
        if (transition instanceof TransitionSet) {
            TransitionSet transitionSet = (TransitionSet) transition;
            int transitionCount = transitionSet.getTransitionCount();
            for (int i = 0; i < transitionCount; i++) {
                ozpoxuz523b2(transitionSet.getTransitionAt(i), arrayList);
            }
        } else if (!ttmhx7(transition) && ttmhx7((List) transition.getTargets())) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                transition.addTarget((View) arrayList.get(i2));
            }
        }
    }

    /* access modifiers changed from: private */
    public static void ozpoxuz523b2(ArrayList arrayList, View view) {
        if (view.getVisibility() != 0) {
            return;
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            if (viewGroup.isTransitionGroup()) {
                arrayList.add(viewGroup);
                return;
            }
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                ozpoxuz523b2(arrayList, viewGroup.getChildAt(i));
            }
            return;
        }
        arrayList.add(view);
    }

    public static Object ttmhx7(Object obj) {
        return obj != null ? ((Transition) obj).clone() : obj;
    }

    public static Object ttmhx7(Object obj, View view, ArrayList arrayList, Map map, View view2) {
        if (obj == null) {
            return obj;
        }
        ozpoxuz523b2(arrayList, view);
        if (map != null) {
            arrayList.removeAll(map.values());
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        arrayList.add(view2);
        ozpoxuz523b2((Transition) obj, arrayList);
        return obj;
    }

    public static Object ttmhx7(Object obj, Object obj2, Object obj3, boolean z) {
        Transition transition = (Transition) obj;
        Transition transition2 = (Transition) obj2;
        Transition transition3 = (Transition) obj3;
        if (transition == null || transition2 == null) {
            z = true;
        }
        if (z) {
            TransitionSet transitionSet = new TransitionSet();
            if (transition != null) {
                transitionSet.addTransition(transition);
            }
            if (transition2 != null) {
                transitionSet.addTransition(transition2);
            }
            if (transition3 == null) {
                return transitionSet;
            }
            transitionSet.addTransition(transition3);
            return transitionSet;
        }
        Transition transition4 = null;
        if (transition2 != null && transition != null) {
            transition4 = new TransitionSet().addTransition(transition2).addTransition(transition).setOrdering(1);
        } else if (transition2 != null) {
            transition4 = transition2;
        } else if (transition != null) {
            transition4 = transition;
        }
        if (transition3 == null) {
            return transition4;
        }
        TransitionSet transitionSet2 = new TransitionSet();
        if (transition4 != null) {
            transitionSet2.addTransition(transition4);
        }
        transitionSet2.addTransition(transition3);
        return transitionSet2;
    }

    public static String ttmhx7(View view) {
        return view.getTransitionName();
    }

    private static void ttmhx7(Transition transition, bjcn50q4e9 bjcn50q4e9) {
        if (transition != null) {
            transition.setEpicenterCallback(new wg4f90m80dyc0s(bjcn50q4e9));
        }
    }

    public static void ttmhx7(View view, View view2, Object obj, ArrayList arrayList, Object obj2, ArrayList arrayList2, Object obj3, ArrayList arrayList3, Object obj4, ArrayList arrayList4, Map map) {
        Transition transition = (Transition) obj;
        Transition transition2 = (Transition) obj2;
        Transition transition3 = (Transition) obj3;
        Transition transition4 = (Transition) obj4;
        if (transition4 != null) {
            view.getViewTreeObserver().addOnPreDrawListener(new jaztd5t99d(view, transition, view2, arrayList, transition2, arrayList2, transition3, arrayList3, map, arrayList4, transition4));
        }
    }

    public static void ttmhx7(ViewGroup viewGroup, Object obj) {
        TransitionManager.beginDelayedTransition(viewGroup, (Transition) obj);
    }

    public static void ttmhx7(Object obj, View view) {
        ((Transition) obj).setEpicenterCallback(new ijavw7l1x(cehyzt7dw(view)));
    }

    public static void ttmhx7(Object obj, View view, boolean z) {
        ((Transition) obj).excludeTarget(view, z);
    }

    public static void ttmhx7(Object obj, Object obj2, View view, b6p1j7hoons8 b6p1j7hoons8, View view2, bjcn50q4e9 bjcn50q4e9, Map map, ArrayList arrayList, Map map2, ArrayList arrayList2) {
        if (obj != null || obj2 != null) {
            Transition transition = (Transition) obj;
            if (transition != null) {
                transition.addTarget(view2);
            }
            if (obj2 != null) {
                ozpoxuz523b2((Transition) obj2, arrayList2);
            }
            if (b6p1j7hoons8 != null) {
                view.getViewTreeObserver().addOnPreDrawListener(new s6o869vduri(view, b6p1j7hoons8, map, map2, transition, arrayList, view2));
            }
            ttmhx7(transition, bjcn50q4e9);
        }
    }

    public static void ttmhx7(Object obj, ArrayList arrayList) {
        List<View> targets;
        Transition transition = (Transition) obj;
        if (transition instanceof TransitionSet) {
            TransitionSet transitionSet = (TransitionSet) transition;
            int transitionCount = transitionSet.getTransitionCount();
            for (int i = 0; i < transitionCount; i++) {
                ttmhx7(transitionSet.getTransitionAt(i), arrayList);
            }
        } else if (!ttmhx7(transition) && (targets = transition.getTargets()) != null && targets.size() == arrayList.size() && targets.containsAll(arrayList)) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                transition.removeTarget((View) arrayList.get(size));
            }
        }
    }

    public static void ttmhx7(Map map, View view) {
        if (view.getVisibility() == 0) {
            String transitionName = view.getTransitionName();
            if (transitionName != null) {
                map.put(transitionName, view);
            }
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                int childCount = viewGroup.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    ttmhx7(map, viewGroup.getChildAt(i));
                }
            }
        }
    }

    private static boolean ttmhx7(Transition transition) {
        return !ttmhx7(transition.getTargetIds()) || !ttmhx7(transition.getTargetNames()) || !ttmhx7(transition.getTargetTypes());
    }

    private static boolean ttmhx7(List list) {
        return list == null || list.isEmpty();
    }
}
