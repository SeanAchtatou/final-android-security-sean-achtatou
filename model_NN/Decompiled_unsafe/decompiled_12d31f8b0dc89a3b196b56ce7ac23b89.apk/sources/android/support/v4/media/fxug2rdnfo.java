package android.support.v4.media;

import android.os.Parcel;
import android.os.Parcelable;

final class fxug2rdnfo implements Parcelable.Creator {
    fxug2rdnfo() {
    }

    /* renamed from: ttmhx7 */
    public RatingCompat createFromParcel(Parcel parcel) {
        return new RatingCompat(parcel.readInt(), parcel.readFloat(), null);
    }

    /* renamed from: ttmhx7 */
    public RatingCompat[] newArray(int i) {
        return new RatingCompat[i];
    }
}
