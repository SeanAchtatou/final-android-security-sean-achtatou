package android.support.v4.c;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

final class ef5tn1cvshg414 implements Set {
    final /* synthetic */ fxug2rdnfo ttmhx7;

    ef5tn1cvshg414(fxug2rdnfo fxug2rdnfo) {
        this.ttmhx7 = fxug2rdnfo;
    }

    public boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public void clear() {
        this.ttmhx7.cehyzt7dw();
    }

    public boolean contains(Object obj) {
        return this.ttmhx7.ttmhx7(obj) >= 0;
    }

    public boolean containsAll(Collection collection) {
        return fxug2rdnfo.ttmhx7(this.ttmhx7.ozpoxuz523b2(), collection);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.c.fxug2rdnfo.ttmhx7(java.util.Set, java.lang.Object):boolean
     arg types: [android.support.v4.c.ef5tn1cvshg414, java.lang.Object]
     candidates:
      android.support.v4.c.fxug2rdnfo.ttmhx7(java.util.Map, java.util.Collection):boolean
      android.support.v4.c.fxug2rdnfo.ttmhx7(int, int):java.lang.Object
      android.support.v4.c.fxug2rdnfo.ttmhx7(int, java.lang.Object):java.lang.Object
      android.support.v4.c.fxug2rdnfo.ttmhx7(java.lang.Object, java.lang.Object):void
      android.support.v4.c.fxug2rdnfo.ttmhx7(java.lang.Object[], int):java.lang.Object[]
      android.support.v4.c.fxug2rdnfo.ttmhx7(java.util.Set, java.lang.Object):boolean */
    public boolean equals(Object obj) {
        return fxug2rdnfo.ttmhx7((Set) this, obj);
    }

    public int hashCode() {
        int i = 0;
        for (int ttmhx72 = this.ttmhx7.ttmhx7() - 1; ttmhx72 >= 0; ttmhx72--) {
            Object ttmhx73 = this.ttmhx7.ttmhx7(ttmhx72, 0);
            i += ttmhx73 == null ? 0 : ttmhx73.hashCode();
        }
        return i;
    }

    public boolean isEmpty() {
        return this.ttmhx7.ttmhx7() == 0;
    }

    public Iterator iterator() {
        return new e8kxjqktk9t(this.ttmhx7, 0);
    }

    public boolean remove(Object obj) {
        int ttmhx72 = this.ttmhx7.ttmhx7(obj);
        if (ttmhx72 < 0) {
            return false;
        }
        this.ttmhx7.ttmhx7(ttmhx72);
        return true;
    }

    public boolean removeAll(Collection collection) {
        return fxug2rdnfo.ozpoxuz523b2(this.ttmhx7.ozpoxuz523b2(), collection);
    }

    public boolean retainAll(Collection collection) {
        return fxug2rdnfo.cehyzt7dw(this.ttmhx7.ozpoxuz523b2(), collection);
    }

    public int size() {
        return this.ttmhx7.ttmhx7();
    }

    public Object[] toArray() {
        return this.ttmhx7.ozpoxuz523b2(0);
    }

    public Object[] toArray(Object[] objArr) {
        return this.ttmhx7.ttmhx7(objArr, 0);
    }
}
