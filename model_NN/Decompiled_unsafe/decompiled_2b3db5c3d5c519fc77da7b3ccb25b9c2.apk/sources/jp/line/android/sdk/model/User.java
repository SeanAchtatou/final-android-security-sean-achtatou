package jp.line.android.sdk.model;

public class User {
    public final String displayName;
    public final String mid;
    public final String pictureUrl;

    public User(String str, String str2, String str3) {
        this.mid = str;
        this.displayName = str2;
        this.pictureUrl = str3;
    }

    public String toString() {
        return "User [mid=" + this.mid + ", displayName=" + this.displayName + ", pictureUrl=" + this.pictureUrl + "]";
    }
}
