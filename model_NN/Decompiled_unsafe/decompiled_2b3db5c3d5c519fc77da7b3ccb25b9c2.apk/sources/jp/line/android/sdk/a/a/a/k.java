package jp.line.android.sdk.a.a.a;

import java.net.HttpURLConnection;
import javax.net.ssl.SSLSocketFactory;
import jp.line.android.sdk.exception.LineSdkApiError;
import jp.line.android.sdk.exception.LineSdkApiException;

public final class k extends a<String> {
    protected k(SSLSocketFactory sSLSocketFactory) {
        super(true, sSLSocketFactory);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0156  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.net.HttpURLConnection r17, jp.line.android.sdk.a.a.c r18, jp.line.android.sdk.a.a.d<java.lang.String> r19) throws java.lang.Exception {
        /*
            r16 = this;
            java.lang.String r6 = r18.g()
            if (r6 == 0) goto L_0x000c
            int r2 = r6.length()
            if (r2 != 0) goto L_0x0025
        L_0x000c:
            java.io.FileNotFoundException r2 = new java.io.FileNotFoundException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r4 = " was not found."
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            throw r2
        L_0x0025:
            r5 = 0
            r4 = 0
            r2 = 0
            java.lang.String r7 = "assets/"
            boolean r7 = r6.startsWith(r7)
            if (r7 == 0) goto L_0x0082
            r2 = 7
            java.lang.String r5 = r6.substring(r2)
            jp.line.android.sdk.LineSdkContext r2 = jp.line.android.sdk.LineSdkContextManager.getSdkContext()
            android.content.Context r2 = r2.getApplicationContext()
            android.content.res.AssetManager r4 = r2.getAssets()
            android.content.res.AssetFileDescriptor r2 = r4.openFd(r5)
            long r2 = r2.getLength()
            java.io.InputStream r4 = r4.open(r5)
            java.lang.String r7 = "/"
            int r7 = r5.lastIndexOf(r7)
            if (r7 <= 0) goto L_0x007e
            java.lang.String r5 = r5.substring(r7)
            r8 = r2
            r10 = r4
            r2 = r5
        L_0x005d:
            if (r10 == 0) goto L_0x0065
            r4 = 0
            int r3 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r3 >= 0) goto L_0x00a3
        L_0x0065:
            java.io.FileNotFoundException r2 = new java.io.FileNotFoundException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r4 = " was not found."
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            throw r2
        L_0x007e:
            r8 = r2
            r10 = r4
            r2 = r5
            goto L_0x005d
        L_0x0082:
            java.io.File r7 = new java.io.File
            r7.<init>(r6)
            boolean r8 = r7.isFile()
            if (r8 == 0) goto L_0x015f
            long r2 = r7.length()
            java.io.BufferedInputStream r4 = new java.io.BufferedInputStream
            java.io.FileInputStream r5 = new java.io.FileInputStream
            r5.<init>(r7)
            r4.<init>(r5)
            java.lang.String r5 = r7.getName()
            r8 = r2
            r10 = r4
            r2 = r5
            goto L_0x005d
        L_0x00a3:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "--AaB03x\r\nContent-Disposition: form-data; name=\"Filedata\"; filename=\""
            java.lang.StringBuilder r4 = r3.append(r4)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = "\"\r\nContent-Type: application/octet-stream\r\n\r\n"
            r2.append(r4)
            java.lang.String r2 = r3.toString()
            java.lang.String r3 = "UTF-8"
            byte[] r2 = r2.getBytes(r3)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "\r\n--AaB03x--"
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = "UTF-8"
            byte[] r11 = r3.getBytes(r4)
            java.lang.String r3 = "POST"
            r0 = r17
            r0.setRequestMethod(r3)
            r3 = 1
            r0 = r17
            r0.setDoOutput(r3)
            java.lang.String r3 = "Content-Type"
            java.lang.String r4 = "multipart/form-data; boundary=AaB03x"
            r0 = r17
            r0.setRequestProperty(r3, r4)
            java.lang.String r3 = "Content-Length"
            int r4 = r2.length
            long r4 = (long) r4
            long r4 = r4 + r8
            int r6 = r11.length
            long r6 = (long) r6
            long r4 = r4 + r6
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r0 = r17
            r0.setRequestProperty(r3, r4)
            b(r17)
            r3 = 0
            java.io.BufferedOutputStream r5 = new java.io.BufferedOutputStream     // Catch:{ all -> 0x0153 }
            java.io.OutputStream r4 = r17.getOutputStream()     // Catch:{ all -> 0x0153 }
            r5.<init>(r4)     // Catch:{ all -> 0x0153 }
            r5.write(r2)     // Catch:{ all -> 0x015a }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r12 = new byte[r2]     // Catch:{ all -> 0x015a }
            r6 = 0
            r4 = 0
            if (r19 == 0) goto L_0x0131
            r2 = 1
        L_0x0116:
            r3 = 0
            int r13 = r12.length     // Catch:{ all -> 0x015a }
            int r3 = r10.read(r12, r3, r13)     // Catch:{ all -> 0x015a }
            r13 = -1
            if (r3 == r13) goto L_0x0149
            r13 = 0
            r5.write(r12, r13, r3)     // Catch:{ all -> 0x015a }
            long r14 = (long) r3     // Catch:{ all -> 0x015a }
            long r6 = r6 + r14
            if (r2 == 0) goto L_0x0133
            boolean r3 = r19.isDone()     // Catch:{ all -> 0x015a }
            if (r3 == 0) goto L_0x0133
            r5.close()
        L_0x0130:
            return
        L_0x0131:
            r2 = 0
            goto L_0x0116
        L_0x0133:
            r14 = 50
            long r14 = r14 * r6
            long r14 = r14 / r8
            int r3 = (int) r14     // Catch:{ all -> 0x015a }
            if (r4 >= r3) goto L_0x015d
            if (r2 == 0) goto L_0x0141
            r0 = r19
            r0.a(r6, r8)     // Catch:{ all -> 0x015a }
        L_0x0141:
            java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x015a }
            java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x015a }
        L_0x0147:
            r4 = r3
            goto L_0x0116
        L_0x0149:
            r5.write(r11)     // Catch:{ all -> 0x015a }
            r5.flush()     // Catch:{ all -> 0x015a }
            r5.close()
            goto L_0x0130
        L_0x0153:
            r2 = move-exception
        L_0x0154:
            if (r3 == 0) goto L_0x0159
            r3.close()
        L_0x0159:
            throw r2
        L_0x015a:
            r2 = move-exception
            r3 = r5
            goto L_0x0154
        L_0x015d:
            r3 = r4
            goto L_0x0147
        L_0x015f:
            r8 = r2
            r10 = r4
            r2 = r5
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.line.android.sdk.a.a.a.k.a(java.net.HttpURLConnection, jp.line.android.sdk.a.a.c, jp.line.android.sdk.a.a.d):void");
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object c(HttpURLConnection httpURLConnection) throws Exception {
        if (httpURLConnection.getResponseCode() == 200) {
            return l.a(httpURLConnection).optString("result");
        }
        throw new LineSdkApiException(LineSdkApiError.SERVER_ERROR, httpURLConnection.getResponseCode(), a(httpURLConnection));
    }
}
