package jp.line.android.sdk.encryption;

import android.content.Context;

public interface LineSdkEncryption {
    String decrypt(Context context, int i, String str);

    byte[] decrypt(Context context, int i, byte[] bArr);

    String encrypt(Context context, int i, String str);

    byte[] encrypt(Context context, int i, byte[] bArr);
}
