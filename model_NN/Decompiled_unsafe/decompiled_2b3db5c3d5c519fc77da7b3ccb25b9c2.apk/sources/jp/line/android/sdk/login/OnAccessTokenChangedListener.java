package jp.line.android.sdk.login;

import jp.line.android.sdk.model.AccessToken;

public interface OnAccessTokenChangedListener {
    void onAccessTokenChanged(AccessToken accessToken);
}
