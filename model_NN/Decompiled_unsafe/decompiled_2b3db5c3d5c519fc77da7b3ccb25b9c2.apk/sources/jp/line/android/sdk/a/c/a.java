package jp.line.android.sdk.a.c;

import android.net.Uri;

public final class a {

    /* renamed from: jp.line.android.sdk.a.c.a$a  reason: collision with other inner class name */
    public static final class C0022a {
        public final int a;
        public final String b;
        public final String c;

        C0022a(int i, String str, String str2) {
            this.a = i;
            this.b = str;
            this.c = str2;
        }
    }

    public static final C0022a a(Uri uri) {
        int i;
        String str;
        String str2;
        String str3;
        int i2;
        try {
            String[] split = uri.getQuery().split("&");
            int length = split.length;
            int i3 = 0;
            str = null;
            i = -1;
            str2 = null;
            while (i3 < length) {
                try {
                    String str4 = split[i3];
                    int indexOf = str4.indexOf("=");
                    String substring = str4.substring(0, indexOf);
                    if (substring != null) {
                        str3 = str4.length() <= indexOf + 1 ? null : str4.substring(indexOf + 1);
                        if (substring.equals("status")) {
                            String str5 = str;
                            i2 = Integer.parseInt(str3);
                            str3 = str5;
                        } else if (substring.equals("otpId")) {
                            str2 = str3;
                            str3 = str;
                            i2 = i;
                        } else if (substring.equals("requestToken")) {
                            i2 = i;
                        }
                        i3++;
                        i = i2;
                        str = str3;
                    }
                    str3 = str;
                    i2 = i;
                } catch (Throwable th) {
                    if (str == null && i == 0) {
                        i = -1;
                    }
                    return new C0022a(i, str, str2);
                }
                i3++;
                i = i2;
                str = str3;
            }
        } catch (Throwable th2) {
            str2 = null;
            str = null;
            i = -1;
            i = -1;
            return new C0022a(i, str, str2);
        }
        return new C0022a(i, str, str2);
    }
}
