package jp.line.android.sdk.activity;

import jp.line.android.sdk.LineSdkContextManager;
import jp.line.android.sdk.a.c.b;
import jp.line.android.sdk.a.c.c;
import jp.line.android.sdk.login.LineAuthManager;
import jp.line.android.sdk.login.LineLoginFuture;

public final class a {
    protected static final c a() {
        LineAuthManager authManager = LineSdkContextManager.getSdkContext().getAuthManager();
        if (!(authManager instanceof b)) {
            throw new RuntimeException("AuthManager instance was not jp.line.android.sdk.login.impl.LineAuthManagerImpl.");
        }
        LineLoginFuture b = ((b) authManager).b();
        if (b == null || (b instanceof c)) {
            return (c) b;
        }
        throw new RuntimeException("LoginFuture instance was not jp.line.android.sdk.login.impl.LineLoginFutureImpl.");
    }
}
