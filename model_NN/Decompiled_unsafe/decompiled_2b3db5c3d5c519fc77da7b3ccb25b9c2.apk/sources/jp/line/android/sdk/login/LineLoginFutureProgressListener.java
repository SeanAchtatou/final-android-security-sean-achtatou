package jp.line.android.sdk.login;

public interface LineLoginFutureProgressListener {
    void onUpdateProgress(LineLoginFuture lineLoginFuture);
}
