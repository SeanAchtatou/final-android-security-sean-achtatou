package jp.line.android.sdk.api;

import java.util.Map;
import jp.line.android.sdk.model.AccessToken;
import jp.line.android.sdk.model.GroupMembers;
import jp.line.android.sdk.model.Groups;
import jp.line.android.sdk.model.Otp;
import jp.line.android.sdk.model.PostEventResult;
import jp.line.android.sdk.model.Profile;
import jp.line.android.sdk.model.Users;

public interface ApiClient {
    ApiRequestFuture<AccessToken> getAccessToken(String str, String str2, ApiRequestFutureListener<AccessToken> apiRequestFutureListener);

    ApiRequestFuture<Users> getFavoriteFriends(int i, int i2, ApiRequestFutureListener<Users> apiRequestFutureListener);

    ApiRequestFuture<Users> getFriends(int i, int i2, ApiRequestFutureListener<Users> apiRequestFutureListener);

    ApiRequestFuture<Users> getFriends(String[] strArr, ApiRequestFutureListener<Users> apiRequestFutureListener);

    ApiRequestFuture<GroupMembers> getGroupMembers(String str, int i, int i2, ApiRequestFutureListener<GroupMembers> apiRequestFutureListener);

    ApiRequestFuture<Groups> getGroups(int i, int i2, ApiRequestFutureListener<Groups> apiRequestFutureListener);

    ApiRequestFuture<Profile> getMyProfile(ApiRequestFutureListener<Profile> apiRequestFutureListener);

    ApiRequestFuture<Otp> getOtp(ApiRequestFutureListener<Otp> apiRequestFutureListener);

    ApiRequestFuture<Users> getSameChannelFriend(int i, int i2, ApiRequestFutureListener<Users> apiRequestFutureListener);

    ApiRequestFuture<String> logout(String str, ApiRequestFutureListener<String> apiRequestFutureListener);

    ApiRequestFuture<PostEventResult> postEvent(String[] strArr, int i, String str, Map<String, Object> map, Map<String, Object> map2, ApiRequestFutureListener<PostEventResult> apiRequestFutureListener);

    ApiRequestFuture<String> uploadProfileImage(String str, ApiRequestFutureListener<String> apiRequestFutureListener);
}
