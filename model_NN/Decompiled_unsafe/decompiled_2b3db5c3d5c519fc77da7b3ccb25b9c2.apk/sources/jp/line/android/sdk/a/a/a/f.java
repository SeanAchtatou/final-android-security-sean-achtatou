package jp.line.android.sdk.a.a.a;

import java.net.HttpURLConnection;
import javax.net.ssl.SSLSocketFactory;
import jp.line.android.sdk.a.a.c;
import jp.line.android.sdk.a.a.d;
import jp.line.android.sdk.exception.LineSdkApiError;
import jp.line.android.sdk.exception.LineSdkApiException;
import jp.line.android.sdk.model.Profile;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONObject;

public final class f extends a<Profile> {
    protected f(SSLSocketFactory sSLSocketFactory) {
        super(true, sSLSocketFactory);
    }

    /* access modifiers changed from: protected */
    public final void a(HttpURLConnection httpURLConnection, c cVar, d<Profile> dVar) throws Exception {
        httpURLConnection.setRequestMethod(HttpGet.METHOD_NAME);
        httpURLConnection.setDoOutput(false);
        b(httpURLConnection);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object c(HttpURLConnection httpURLConnection) throws Exception {
        int responseCode = httpURLConnection.getResponseCode();
        if (responseCode == 200) {
            JSONObject a = l.a(httpURLConnection);
            return new Profile(a.optString("mid"), a.optString("displayName"), a.optString("pictureUrl"), a.optString("statusMessage"));
        }
        throw new LineSdkApiException(LineSdkApiError.SERVER_ERROR, responseCode, a(httpURLConnection));
    }
}
