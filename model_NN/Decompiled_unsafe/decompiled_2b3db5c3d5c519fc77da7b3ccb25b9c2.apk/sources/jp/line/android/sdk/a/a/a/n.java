package jp.line.android.sdk.a.a.a;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.EnumMap;
import java.util.Map;
import javax.net.ssl.SSLContext;
import jp.line.android.sdk.a.a.b;
import jp.line.android.sdk.a.a.c;
import jp.line.android.sdk.a.a.d;
import jp.line.android.sdk.api.ApiType;

public final class n implements b {
    private final Map<ApiType, a<?>> a = new EnumMap(ApiType.class);

    public n() {
        o oVar;
        try {
            SSLContext instance = SSLContext.getInstance("TLSv1");
            instance.init(null, null, null);
            oVar = new o(instance.getSocketFactory());
        } catch (NoSuchAlgorithmException e) {
            oVar = null;
        } catch (KeyManagementException e2) {
            oVar = null;
        }
        this.a.put(ApiType.GET_OTP, new g(oVar));
        this.a.put(ApiType.GET_ACCESS_TOKEN, new b(oVar));
        this.a.put(ApiType.LOGOUT, new h(oVar));
        this.a.put(ApiType.REFRESH_ACCESS_TOKEN, new j(oVar));
        this.a.put(ApiType.GET_MY_PROFILE, new f(oVar));
        this.a.put(ApiType.POST_EVENT, new i(oVar));
        this.a.put(ApiType.UPLOAD_PROFILE_IMAGE, new k(oVar));
        c cVar = new c(oVar);
        this.a.put(ApiType.GET_FAVORITE_FRIENDS, cVar);
        this.a.put(ApiType.GET_FRIENDS, cVar);
        this.a.put(ApiType.GET_PROFILES, cVar);
        this.a.put(ApiType.GET_SAME_CHANNEL_FRIENDS, cVar);
        this.a.put(ApiType.GET_GROUPS, new e(oVar));
        this.a.put(ApiType.GET_GROUP_MEMBERS, new d(oVar));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final <RO> void a(c cVar, d<RO> dVar) {
        try {
            a aVar = this.a.get(cVar.a);
            if (aVar == null) {
                dVar.a((Throwable) new NullPointerException("Not implemented"));
            } else if (aVar.a(cVar, dVar)) {
                d dVar2 = new d();
                this.a.get(ApiType.REFRESH_ACCESS_TOKEN).a(new c(ApiType.REFRESH_ACCESS_TOKEN), dVar2);
                switch (dVar2.getStatus()) {
                    case SUCCESS:
                        aVar.a(cVar, dVar);
                        return;
                    case CANCELED:
                        dVar.a();
                        return;
                    default:
                        dVar.a(dVar2.getCause());
                        return;
                }
                dVar.a(th);
            }
        } catch (Throwable th) {
            dVar.a(th);
        }
    }
}
