package jp.line.android.sdk.exception;

public enum LineSdkApiError {
    NOT_FOUND_ACCESS_TOKEN,
    SERVER_ERROR,
    ILLEGAL_RESPONSE,
    UNKNOWN
}
