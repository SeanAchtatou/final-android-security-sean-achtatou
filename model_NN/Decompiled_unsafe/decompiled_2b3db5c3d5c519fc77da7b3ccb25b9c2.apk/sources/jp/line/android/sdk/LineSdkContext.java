package jp.line.android.sdk;

import android.app.Activity;
import android.content.Context;
import jp.line.android.sdk.api.ApiClient;
import jp.line.android.sdk.login.LineAuthManager;

public interface LineSdkContext {
    Class<? extends Activity> getActivityClassThatFiresWhenProcessWasKilled();

    ApiClient getApiClient();

    Context getApplicationContext();

    LineAuthManager getAuthManager();

    String getAuthScheme();

    String getChannelApiServerHost();

    int getChannelId();

    String getChannelServerHost();

    Phase getPhase();

    String getSdkVersion();

    Class<? extends Activity> getWebLoginActivityClass();
}
