package jp.line.android.sdk.a.a.a;

import com.mol.seaplus.tool.connection.internet.IHttpHeader;
import com.tencent.android.tpush.common.MessageKey;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.Map;
import javax.net.ssl.SSLSocketFactory;
import jp.line.android.sdk.a.a.c;
import jp.line.android.sdk.a.a.d;
import jp.line.android.sdk.exception.LineSdkApiError;
import jp.line.android.sdk.exception.LineSdkApiException;
import jp.line.android.sdk.model.PostEventResult;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

public final class i extends a<PostEventResult> {
    protected i(SSLSocketFactory sSLSocketFactory) {
        super(true, sSLSocketFactory);
    }

    /* access modifiers changed from: protected */
    public final void a(HttpURLConnection httpURLConnection, c cVar, d<PostEventResult> dVar) throws Exception {
        JSONObject a;
        JSONObject a2;
        String[] e = cVar.e();
        int h = cVar.h();
        String i = cVar.i();
        Map<String, Object> j = cVar.j();
        Map<String, Object> k = cVar.k();
        JSONObject jSONObject = new JSONObject();
        if (e != null && e.length > 0) {
            JSONArray jSONArray = new JSONArray();
            for (String put : cVar.e()) {
                jSONArray.put(put);
            }
            jSONObject.put("to", jSONArray);
        }
        jSONObject.put("toChannel", h);
        jSONObject.put("eventType", i);
        if (!(j == null || j.size() <= 0 || (a2 = m.a((Map) j)) == null)) {
            jSONObject.put(MessageKey.MSG_CONTENT, a2);
        }
        if (!(j == null || j.size() <= 0 || (a = m.a((Map) k)) == null)) {
            jSONObject.put("push", a);
        }
        byte[] bytes = jSONObject.toString().getBytes(HTTP.UTF_8);
        httpURLConnection.setRequestMethod(HttpPost.METHOD_NAME);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestProperty("Content-Type", IHttpHeader.CONTENT_TYPE_APPLICATION_JSON);
        httpURLConnection.setRequestProperty("Content-Length", String.valueOf(bytes.length));
        b(httpURLConnection);
        OutputStream outputStream = httpURLConnection.getOutputStream();
        outputStream.write(bytes);
        outputStream.flush();
        outputStream.close();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object c(HttpURLConnection httpURLConnection) throws Exception {
        if (httpURLConnection.getResponseCode() == 200) {
            JSONObject a = l.a(httpURLConnection);
            int optInt = a.optInt("version", 1);
            long optLong = a.optLong("timestamp");
            String optString = a.optString("messageId");
            String[] strArr = null;
            JSONArray optJSONArray = a.optJSONArray("failed");
            if (optJSONArray != null) {
                int length = optJSONArray.length();
                strArr = new String[length];
                for (int i = 0; i < length; i++) {
                    strArr[i] = optJSONArray.optString(i);
                }
            }
            return new PostEventResult(optInt, optLong, optString, strArr);
        }
        throw new LineSdkApiException(LineSdkApiError.SERVER_ERROR, httpURLConnection.getResponseCode(), a(httpURLConnection));
    }
}
