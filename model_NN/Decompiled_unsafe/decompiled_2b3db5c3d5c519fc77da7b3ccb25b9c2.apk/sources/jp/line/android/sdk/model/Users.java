package jp.line.android.sdk.model;

import java.util.List;

public class Users {
    public final int count;
    public final int display;
    public final int start;
    public final int total;
    public final List<User> userList;

    public Users(int i, int i2, int i3, int i4, List<User> list) {
        this.count = i;
        this.total = i4;
        this.start = i2;
        this.display = i3;
        this.userList = list;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Users [count=").append(this.count).append(", total=").append(this.total).append(", start=").append(this.start).append(", display=").append(this.display);
        sb.append(", userList=");
        if (this.userList == null || this.userList.size() <= 0) {
            sb.append("null or empty");
        } else {
            for (User append : this.userList) {
                sb.append(append).append(",").append(System.getProperty("line.separator"));
            }
            sb.delete(sb.length() - 2, sb.length());
        }
        return sb.toString();
    }
}
