package jp.line.android.sdk.api;

import java.util.concurrent.TimeUnit;

public interface ApiRequestFuture<RO> {
    boolean addListener(ApiRequestFutureListener<RO> apiRequestFutureListener);

    boolean addProgressListener(ApiRequestFutureProgressListener<RO> apiRequestFutureProgressListener);

    void await();

    boolean await(long j, TimeUnit timeUnit);

    boolean cancel();

    Throwable getCause();

    RO getResponseObject();

    FutureStatus getStatus();

    boolean isDone();

    boolean removeProgressListener(ApiRequestFutureProgressListener<RO> apiRequestFutureProgressListener);

    boolean removeistener(ApiRequestFutureListener<RO> apiRequestFutureListener);
}
