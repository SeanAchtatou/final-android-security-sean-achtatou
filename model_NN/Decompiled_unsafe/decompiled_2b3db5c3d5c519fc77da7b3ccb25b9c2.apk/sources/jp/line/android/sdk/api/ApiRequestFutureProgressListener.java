package jp.line.android.sdk.api;

public interface ApiRequestFutureProgressListener<RO> {
    void operationProgressed(ApiRequestFuture<RO> apiRequestFuture, long j, long j2);
}
