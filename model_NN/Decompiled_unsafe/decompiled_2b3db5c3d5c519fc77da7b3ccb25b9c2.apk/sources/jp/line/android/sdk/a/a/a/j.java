package jp.line.android.sdk.a.a.a;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import javax.net.ssl.SSLSocketFactory;
import jp.line.android.sdk.LineSdkContextManager;
import jp.line.android.sdk.a.a;
import jp.line.android.sdk.a.a.c;
import jp.line.android.sdk.a.a.d;
import jp.line.android.sdk.a.c.b;
import jp.line.android.sdk.exception.LineSdkApiError;
import jp.line.android.sdk.exception.LineSdkApiException;
import jp.line.android.sdk.login.LineAuthManager;
import jp.line.android.sdk.model.AccessToken;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

public final class j extends a<AccessToken> {
    protected j(SSLSocketFactory sSLSocketFactory) {
        super(true, sSLSocketFactory);
    }

    /* access modifiers changed from: protected */
    public final void a(HttpURLConnection httpURLConnection, c cVar, d<AccessToken> dVar) throws Exception {
        byte[] bytes = ("refreshToken=" + a.a().b().refreshToken).getBytes(HTTP.UTF_8);
        httpURLConnection.setRequestMethod(HttpPost.METHOD_NAME);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        httpURLConnection.setRequestProperty("Content-Length", String.valueOf(bytes.length));
        b(httpURLConnection);
        OutputStream outputStream = httpURLConnection.getOutputStream();
        outputStream.write(bytes);
        outputStream.flush();
        outputStream.close();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object c(HttpURLConnection httpURLConnection) throws Exception {
        if (httpURLConnection.getResponseCode() == 200) {
            JSONObject a = l.a(httpURLConnection);
            AccessToken accessToken = new AccessToken(a.optString("mid"), a.optString("accessToken"), a.optLong("expire"), a.optString("refreshToken"));
            if (accessToken.mid == null || accessToken.mid.length() == 0) {
                throw new LineSdkApiException(LineSdkApiError.ILLEGAL_RESPONSE, "mid is null");
            } else if (accessToken.accessToken == null || accessToken.accessToken.length() == 0) {
                throw new LineSdkApiException(LineSdkApiError.ILLEGAL_RESPONSE, "accessToken is null");
            } else {
                LineAuthManager authManager = LineSdkContextManager.getSdkContext().getAuthManager();
                if (authManager instanceof b) {
                    ((b) authManager).a(accessToken);
                }
                a.a().a(accessToken);
                return accessToken;
            }
        } else {
            throw new LineSdkApiException(LineSdkApiError.SERVER_ERROR, httpURLConnection.getResponseCode(), a(httpURLConnection));
        }
    }
}
