package jp.line.android.sdk.a.a.a;

import java.io.IOException;
import java.net.HttpURLConnection;
import javax.net.ssl.SSLSocketFactory;
import jp.line.android.sdk.a.a.c;
import jp.line.android.sdk.a.a.d;
import jp.line.android.sdk.exception.LineSdkApiError;
import jp.line.android.sdk.exception.LineSdkApiException;
import jp.line.android.sdk.exception.LineSdkApiServerError;
import jp.line.android.sdk.model.AccessToken;
import org.json.JSONObject;

public abstract class a<RO> {
    private final boolean a;
    private final SSLSocketFactory b;

    protected a(boolean z, SSLSocketFactory sSLSocketFactory) {
        this.a = z;
        this.b = sSLSocketFactory;
    }

    protected static LineSdkApiServerError a(HttpURLConnection httpURLConnection) {
        try {
            JSONObject a2 = l.a(httpURLConnection);
            if (a2 != null) {
                int optInt = a2.optInt("statusCode", -1);
                String optString = a2.optString("statusMessage");
                if (optInt >= 0 && optString != null) {
                    return new LineSdkApiServerError(optInt, optString);
                }
            }
        } catch (Throwable th) {
        }
        return null;
    }

    protected static void b(HttpURLConnection httpURLConnection) throws IOException {
        httpURLConnection.getURL();
        httpURLConnection.connect();
    }

    /* access modifiers changed from: protected */
    public String a(c cVar) throws LineSdkApiException {
        AccessToken b2 = jp.line.android.sdk.a.a.a().b();
        if (b2 != null && b2.accessToken != null) {
            return b2.accessToken;
        }
        throw new LineSdkApiException(LineSdkApiError.NOT_FOUND_ACCESS_TOKEN);
    }

    /* access modifiers changed from: protected */
    public abstract void a(HttpURLConnection httpURLConnection, c cVar, d<RO> dVar) throws Exception;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v2, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v7, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v9, resolved type: java.net.HttpURLConnection} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        r10.a(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x006d, code lost:
        r3.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0086, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0089, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0090, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0091, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0097, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0098, code lost:
        r7 = r3;
        r3 = r2;
        r2 = r7;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0068 A[SYNTHETIC, Splitter:B:33:0x0068] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0086 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:6:0x0013] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0089  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(jp.line.android.sdk.a.a.c r9, jp.line.android.sdk.a.a.d<?> r10) {
        /*
            r8 = this;
            r3 = 0
            r4 = 0
            boolean r1 = r8.a     // Catch:{ Throwable -> 0x0093, all -> 0x008d }
            if (r1 == 0) goto L_0x009e
            java.lang.String r1 = r8.a(r9)     // Catch:{ Throwable -> 0x0093, all -> 0x008d }
            r5 = r1
        L_0x000b:
            java.lang.String r1 = r8.b(r9)     // Catch:{ Throwable -> 0x0093, all -> 0x008d }
            java.net.HttpURLConnection r2 = jp.line.android.sdk.a.a.a.l.a(r1)     // Catch:{ Throwable -> 0x0093, all -> 0x008d }
            boolean r1 = r2 instanceof javax.net.ssl.HttpsURLConnection     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            if (r1 == 0) goto L_0x0038
            r0 = r2
            javax.net.ssl.HttpsURLConnection r0 = (javax.net.ssl.HttpsURLConnection) r0     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            r1 = r0
            javax.net.ssl.SSLSocketFactory r6 = r8.b     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            r1.setSSLSocketFactory(r6)     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            jp.line.android.sdk.LineSdkContext r1 = jp.line.android.sdk.LineSdkContextManager.getSdkContext()     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            jp.line.android.sdk.Phase r1 = r1.getPhase()     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            jp.line.android.sdk.Phase r6 = jp.line.android.sdk.Phase.BETA     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            if (r1 != r6) goto L_0x0038
            r0 = r2
            javax.net.ssl.HttpsURLConnection r0 = (javax.net.ssl.HttpsURLConnection) r0     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            r1 = r0
            jp.line.android.sdk.a.a.a.a$1 r6 = new jp.line.android.sdk.a.a.a.a$1     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            r6.<init>()     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            r1.setHostnameVerifier(r6)     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
        L_0x0038:
            boolean r1 = r8.a     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            if (r1 == 0) goto L_0x0041
            java.lang.String r1 = "X-Line-ChannelToken"
            r2.setRequestProperty(r1, r5)     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
        L_0x0041:
            r8.a(r2, r9, r10)     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            r2.getHeaderFields()     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            java.lang.Object r3 = r8.c(r2)     // Catch:{ JSONException -> 0x0059, LineSdkApiException -> 0x0071 }
            r1 = r4
        L_0x004c:
            if (r1 != 0) goto L_0x0053
            if (r10 == 0) goto L_0x0053
            r10.a(r3)     // Catch:{ Throwable -> 0x0097, all -> 0x0086 }
        L_0x0053:
            if (r2 == 0) goto L_0x0058
            r2.disconnect()
        L_0x0058:
            return r1
        L_0x0059:
            r1 = move-exception
            jp.line.android.sdk.exception.LineSdkApiException r3 = new jp.line.android.sdk.exception.LineSdkApiException     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            jp.line.android.sdk.exception.LineSdkApiError r5 = jp.line.android.sdk.exception.LineSdkApiError.ILLEGAL_RESPONSE     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            r3.<init>(r5, r1)     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            throw r3     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
        L_0x0062:
            r1 = move-exception
            r3 = r2
            r2 = r1
            r1 = r4
        L_0x0066:
            if (r10 == 0) goto L_0x006b
            r10.a(r2)     // Catch:{ all -> 0x0090 }
        L_0x006b:
            if (r3 == 0) goto L_0x0058
            r3.disconnect()
            goto L_0x0058
        L_0x0071:
            r1 = move-exception
            int[] r5 = jp.line.android.sdk.a.a.a.a.AnonymousClass2.a     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            jp.line.android.sdk.api.ApiType r6 = r9.a     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            int r6 = r6.ordinal()     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            r5 = r5[r6]     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
            switch(r5) {
                case 1: goto L_0x0083;
                case 2: goto L_0x0083;
                case 3: goto L_0x0083;
                case 4: goto L_0x0083;
                default: goto L_0x007f;
            }     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
        L_0x007f:
            boolean r4 = r1.isAccessTokenExpired()     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
        L_0x0083:
            if (r4 != 0) goto L_0x009c
            throw r1     // Catch:{ Throwable -> 0x0062, all -> 0x0086 }
        L_0x0086:
            r1 = move-exception
        L_0x0087:
            if (r2 == 0) goto L_0x008c
            r2.disconnect()
        L_0x008c:
            throw r1
        L_0x008d:
            r1 = move-exception
            r2 = r3
            goto L_0x0087
        L_0x0090:
            r1 = move-exception
            r2 = r3
            goto L_0x0087
        L_0x0093:
            r1 = move-exception
            r2 = r1
            r1 = r4
            goto L_0x0066
        L_0x0097:
            r3 = move-exception
            r7 = r3
            r3 = r2
            r2 = r7
            goto L_0x0066
        L_0x009c:
            r1 = r4
            goto L_0x004c
        L_0x009e:
            r5 = r3
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.line.android.sdk.a.a.a.a.a(jp.line.android.sdk.a.a.c, jp.line.android.sdk.a.a.d):boolean");
    }

    /* access modifiers changed from: protected */
    public String b(c cVar) {
        return p.a(cVar);
    }

    /* access modifiers changed from: protected */
    public abstract RO c(HttpURLConnection httpURLConnection) throws Exception;
}
