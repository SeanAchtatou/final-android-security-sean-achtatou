package jp.line.android.sdk.a.c;

import android.content.Context;
import android.content.SharedPreferences;
import com.facebook.appevents.AppEventsConstants;
import java.util.Locale;
import jp.line.android.sdk.LineSdkContextManager;
import jp.line.android.sdk.encryption.LineSdkEncryption;
import jp.line.android.sdk.encryption.LineSdkEncryptionFactory;
import jp.line.android.sdk.login.LineLoginFuture;
import jp.line.android.sdk.model.Otp;
import jp.line.android.sdk.model.RequestToken;

public class d {
    private static d a;
    private boolean b;
    private long c;
    private LineLoginFuture.ProgressOfLogin d;
    private Otp e;
    private RequestToken f;
    private Locale g;

    private d() {
    }

    public static d a() {
        if (a == null) {
            synchronized (d.class) {
                if (a == null) {
                    a = new d();
                }
            }
        }
        return a;
    }

    private final void b(c cVar) {
        if (cVar == null) {
            this.c = -1;
            this.d = LineLoginFuture.ProgressOfLogin.STARTED;
            this.e = null;
            this.f = null;
            this.b = false;
            this.g = null;
            return;
        }
        this.c = cVar.getCreatedTime();
        this.d = cVar.getProgress();
        this.e = cVar.getOtp();
        this.f = cVar.getRequestToken();
        this.b = cVar.isForceLoginByOtherAccount();
        this.g = cVar.getLocale();
    }

    public static void c() {
        SharedPreferences.Editor edit = d().edit();
        edit.clear();
        edit.commit();
    }

    private static SharedPreferences d() {
        return LineSdkContextManager.getSdkContext().getApplicationContext().getSharedPreferences("linesdk-1", 0);
    }

    public final void a(c cVar) {
        boolean z = true;
        synchronized (this) {
            if (cVar != null) {
                if (!(this.c == cVar.getCreatedTime() && this.d == cVar.getProgress())) {
                    b(cVar);
                    z = false;
                }
                if (!z) {
                    try {
                        SharedPreferences.Editor edit = d().edit();
                        edit.clear();
                        edit.putBoolean("9", cVar.isForceLoginByOtherAccount());
                        edit.putLong(AppEventsConstants.EVENT_PARAM_VALUE_YES, cVar.getCreatedTime());
                        edit.putInt(Version.code, cVar.getProgress().code);
                        LineSdkEncryption lineSdkEncryption = LineSdkEncryptionFactory.getLineSdkEncryption();
                        Context applicationContext = LineSdkContextManager.getSdkContext().getApplicationContext();
                        Otp otp = cVar.getOtp();
                        if (otp != null) {
                            edit.putString("3", lineSdkEncryption.encrypt(applicationContext, 32454345, otp.password));
                            edit.putString("4", lineSdkEncryption.encrypt(applicationContext, 32454345, otp.id));
                        }
                        RequestToken requestToken = cVar.getRequestToken();
                        if (requestToken != null) {
                            edit.putString("5", lineSdkEncryption.encrypt(applicationContext, 32454345, requestToken.requestToken));
                            edit.putLong("6", requestToken.expire);
                            edit.putString("7", lineSdkEncryption.encrypt(applicationContext, 32454345, requestToken.refleshToken));
                            edit.putBoolean("8", requestToken.isFromWebLogin);
                        }
                        Locale locale = cVar.getLocale();
                        if (locale != null) {
                            edit.putString("10", locale.getLanguage());
                            edit.putString("11", locale.getCountry());
                        }
                        edit.commit();
                    } catch (Throwable th) {
                        b(null);
                    }
                }
            }
            Boolean.valueOf(this.b);
            Long.valueOf(this.c);
            LineLoginFuture.ProgressOfLogin progressOfLogin = this.d;
            Otp otp2 = this.e;
            RequestToken requestToken2 = this.f;
            Locale locale2 = this.g;
        }
    }

    public final c b() {
        Locale locale;
        c cVar = null;
        synchronized (this) {
            if (this.c <= 0 || this.c + 3600000 >= System.currentTimeMillis()) {
                SharedPreferences d2 = d();
                long j = d2.getLong(AppEventsConstants.EVENT_PARAM_VALUE_YES, -1);
                if (j >= 0 && 3600000 + j >= System.currentTimeMillis()) {
                    boolean z = d2.getBoolean("9", false);
                    LineLoginFuture.ProgressOfLogin findValueByCode = LineLoginFuture.ProgressOfLogin.findValueByCode(d2.getInt(Version.code, -1));
                    LineSdkEncryption lineSdkEncryption = LineSdkEncryptionFactory.getLineSdkEncryption();
                    Context applicationContext = LineSdkContextManager.getSdkContext().getApplicationContext();
                    String decrypt = lineSdkEncryption.decrypt(applicationContext, 32454345, d2.getString("3", null));
                    String decrypt2 = lineSdkEncryption.decrypt(applicationContext, 32454345, d2.getString("4", null));
                    Otp otp = (decrypt == null || decrypt2 == null) ? null : new Otp(decrypt2, decrypt);
                    String decrypt3 = lineSdkEncryption.decrypt(applicationContext, 32454345, d2.getString("5", null));
                    RequestToken createFromWebLogin = decrypt3 != null ? d2.getBoolean("8", false) ? RequestToken.createFromWebLogin(decrypt3, d2.getLong("6", -1), lineSdkEncryption.decrypt(applicationContext, 32454345, d2.getString("7", null))) : RequestToken.createFromA2ALogin(decrypt3) : null;
                    String string = d2.getString("10", null);
                    String string2 = d2.getString("11", null);
                    if (string != null) {
                        if (string2 != null) {
                            new Locale(string, string2);
                        }
                        locale = new Locale(string);
                    } else {
                        locale = null;
                    }
                    cVar = new c(j, findValueByCode, otp, createFromWebLogin, z, locale);
                    b(cVar);
                } else if (j > 0) {
                    c();
                    Long.valueOf(j);
                }
            } else {
                cVar = new c(this.c, this.d, this.e, this.f, this.b, this.g);
            }
        }
        return cVar;
    }
}
