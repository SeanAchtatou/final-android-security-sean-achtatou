package jp.line.android.sdk.model;

public class AccessToken {
    public final String accessToken;
    public final long expire;
    public final String mid;
    public final String refreshToken;

    public AccessToken(String str, String str2, long j, String str3) {
        this.mid = str;
        this.accessToken = str2;
        this.expire = j;
        this.refreshToken = str3;
    }

    public String toString() {
        return "AccessToken [mid=" + this.mid + ", accessToken=" + this.accessToken + ", expire=" + this.expire + ", refleshToken=" + this.refreshToken + "]";
    }
}
