package jp.line.android.sdk.a.a.a;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import javax.net.ssl.SSLSocketFactory;
import jp.line.android.sdk.LineSdkContextManager;
import jp.line.android.sdk.a.a.c;
import jp.line.android.sdk.a.a.d;
import jp.line.android.sdk.exception.LineSdkApiError;
import jp.line.android.sdk.exception.LineSdkApiException;
import jp.line.android.sdk.model.Otp;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

public final class g extends a<Otp> {
    protected g(SSLSocketFactory sSLSocketFactory) {
        super(false, sSLSocketFactory);
    }

    /* access modifiers changed from: protected */
    public final void a(HttpURLConnection httpURLConnection, c cVar, d<Otp> dVar) throws Exception {
        byte[] bytes = ("channelId=" + LineSdkContextManager.getSdkContext().getChannelId()).getBytes(HTTP.UTF_8);
        httpURLConnection.setRequestMethod(HttpPost.METHOD_NAME);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        httpURLConnection.setRequestProperty("Content-Length", String.valueOf(bytes.length));
        b(httpURLConnection);
        OutputStream outputStream = httpURLConnection.getOutputStream();
        outputStream.write(bytes);
        outputStream.flush();
        outputStream.close();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object c(HttpURLConnection httpURLConnection) throws Exception {
        if (httpURLConnection.getResponseCode() == 200) {
            JSONObject a = l.a(httpURLConnection);
            String optString = a.optString("otpId");
            String optString2 = a.optString("otp");
            if (optString != null && optString2 != null) {
                return new Otp(optString, optString2);
            }
            throw new LineSdkApiException(LineSdkApiError.ILLEGAL_RESPONSE, "otp is null");
        }
        throw new LineSdkApiException(LineSdkApiError.SERVER_ERROR, httpURLConnection.getResponseCode(), a(httpURLConnection));
    }
}
