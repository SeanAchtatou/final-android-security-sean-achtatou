package jp.line.android.sdk.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import jp.line.android.sdk.LineSdkContextManager;
import jp.line.android.sdk.a.c.a;
import jp.line.android.sdk.a.c.b;
import jp.line.android.sdk.a.c.c;
import jp.line.android.sdk.a.c.d;
import jp.line.android.sdk.exception.LineSdkLoginError;
import jp.line.android.sdk.exception.LineSdkLoginException;
import jp.line.android.sdk.login.LineAuthManager;
import jp.line.android.sdk.login.LineLoginFuture;
import jp.line.android.sdk.model.Otp;
import jp.line.android.sdk.model.RequestToken;

public class LineAuthCompleteActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Class<? extends Activity> activityClassThatFiresWhenProcessWasKilled;
        Otp otp;
        super.onCreate(bundle);
        a.C0022a a = a.a(getIntent().getData());
        LineAuthManager authManager = LineSdkContextManager.getSdkContext().getAuthManager();
        boolean z = (authManager instanceof b) && !((b) authManager).a();
        Boolean.valueOf(z);
        c a2 = a.a();
        if (a2 == null || (otp = a2.getOtp()) == null || otp.id == null || !otp.id.equals(a.c)) {
            a2 = null;
        }
        if (a2 != null && a2.getProgress() == LineLoginFuture.ProgressOfLogin.STARTED_A2A_LOGIN) {
            switch (a.a) {
                case 0:
                    a2.a(RequestToken.createFromA2ALogin(a.b));
                    d.a().a(a2);
                    break;
                case 1:
                    a2.a();
                    break;
                default:
                    a2.a(new LineSdkLoginException(LineSdkLoginError.FAILED_A2A_LOGIN, a.a));
                    break;
            }
        }
        if (!(!z || a2 == null || (activityClassThatFiresWhenProcessWasKilled = LineSdkContextManager.getSdkContext().getActivityClassThatFiresWhenProcessWasKilled()) == null)) {
            startActivity(new Intent(this, activityClassThatFiresWhenProcessWasKilled));
        }
        finish();
    }
}
