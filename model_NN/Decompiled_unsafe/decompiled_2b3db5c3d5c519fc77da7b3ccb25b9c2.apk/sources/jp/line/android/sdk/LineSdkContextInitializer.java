package jp.line.android.sdk;

import jp.line.android.sdk.api.ApiClient;
import jp.line.android.sdk.login.LineAuthManager;

public interface LineSdkContextInitializer {
    ApiClient createApiClient(LineSdkContext lineSdkContext);

    LineAuthManager createAuthManager(LineSdkContext lineSdkContext);
}
