package jp.line.android.sdk.encryption;

import jp.line.android.sdk.a.b.b;

public class LineSdkEncryptionFactory {
    private static LineSdkEncryption lineSdkEncryption;

    private LineSdkEncryptionFactory() {
    }

    public static final LineSdkEncryption getLineSdkEncryption() {
        if (lineSdkEncryption == null) {
            synchronized (LineSdkEncryptionFactory.class) {
                if (lineSdkEncryption == null) {
                    lineSdkEncryption = new b();
                }
            }
        }
        return lineSdkEncryption;
    }
}
