package jp.line.android.sdk.login;

import java.util.Locale;
import java.util.concurrent.TimeUnit;
import jp.line.android.sdk.model.AccessToken;
import jp.line.android.sdk.model.Otp;
import jp.line.android.sdk.model.RequestToken;

public interface LineLoginFuture {

    public enum ProgressOfLogin {
        STARTED(0, 0),
        REQUESTED_OTP(10, 1),
        GOT_OTP(11, 2),
        STARTED_A2A_LOGIN(20, 3),
        STARTED_WEB_LOGIN(21, 3),
        GOT_REQUEST_TOKEN(22, 4),
        REQUESTED_ACCESS_TOKEN(30, 5),
        SUCCESS(100, 100),
        FAILED(101, 100),
        CANCELED(102, 100);
        
        public final int code;
        public final int flowNumber;

        private ProgressOfLogin(int i, int i2) {
            this.code = i;
            this.flowNumber = i2;
        }

        public static ProgressOfLogin findValueByCode(int i) {
            for (ProgressOfLogin progressOfLogin : values()) {
                if (progressOfLogin.code == i) {
                    return progressOfLogin;
                }
            }
            return STARTED;
        }
    }

    boolean addFutureListener(LineLoginFutureListener lineLoginFutureListener);

    boolean addProgressListener(LineLoginFutureProgressListener lineLoginFutureProgressListener);

    void await();

    boolean await(long j, TimeUnit timeUnit);

    AccessToken getAccessToken();

    Throwable getCause();

    long getCreatedTime();

    Locale getLocale();

    Otp getOtp();

    ProgressOfLogin getProgress();

    RequestToken getRequestToken();

    boolean isForceLoginByOtherAccount();

    boolean isProcessing();

    boolean isSameRequest(boolean z, Locale locale);

    boolean removeFutureListener(LineLoginFutureListener lineLoginFutureListener);

    boolean removeProgressListener(LineLoginFutureProgressListener lineLoginFutureProgressListener);
}
