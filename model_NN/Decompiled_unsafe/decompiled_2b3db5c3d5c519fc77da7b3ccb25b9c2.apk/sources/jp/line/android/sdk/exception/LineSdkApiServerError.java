package jp.line.android.sdk.exception;

public class LineSdkApiServerError {
    public final int statusCode;
    public final String statusMessage;

    public LineSdkApiServerError(int i, String str) {
        this.statusCode = i;
        this.statusMessage = str;
    }

    public String toString() {
        return "LineSdkServerError [statusCode=" + this.statusCode + ", statusMessage=" + this.statusMessage + "]";
    }
}
