package jp.line.android.sdk.model;

public class Otp {
    public final String id;
    public final String password;

    public Otp(String str, String str2) {
        this.id = str;
        this.password = str2;
    }

    public String toString() {
        return "Otp [id=" + this.id + ", password=" + this.password + "]";
    }
}
