package jp.line.android.sdk.exception;

public enum LineSdkLoginError {
    FAILED_START_LOGIN_ACTIVITY,
    FAILED_A2A_LOGIN,
    FAILED_WEB_LOGIN,
    UNKNOWN
}
