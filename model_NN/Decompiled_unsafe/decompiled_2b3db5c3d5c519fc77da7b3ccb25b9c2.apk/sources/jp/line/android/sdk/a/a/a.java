package jp.line.android.sdk.a.a;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import jp.line.android.sdk.a.a.a.n;
import jp.line.android.sdk.api.ApiClient;
import jp.line.android.sdk.api.ApiRequestFuture;
import jp.line.android.sdk.api.ApiRequestFutureListener;
import jp.line.android.sdk.api.ApiType;
import jp.line.android.sdk.api.FutureStatus;
import jp.line.android.sdk.model.AccessToken;
import jp.line.android.sdk.model.GroupMembers;
import jp.line.android.sdk.model.Groups;
import jp.line.android.sdk.model.Otp;
import jp.line.android.sdk.model.PostEventResult;
import jp.line.android.sdk.model.Profile;
import jp.line.android.sdk.model.Users;

public final class a implements ApiClient {
    final b a = new n();
    private e b = null;
    private final Map<String, d<?>> c = new HashMap();
    private final Executor d = Executors.newCachedThreadPool();

    /* renamed from: jp.line.android.sdk.a.a.a$a  reason: collision with other inner class name */
    public static final class C0021a {
        private static final AtomicInteger a = new AtomicInteger(0);

        public static final String a(c cVar) {
            if (cVar == null) {
                return null;
            }
            ApiType apiType = cVar.a;
            StringBuilder sb = new StringBuilder();
            sb.append(apiType.ordinal());
            switch (apiType) {
                case GET_ACCESS_TOKEN:
                case GET_MY_PROFILE:
                case GET_OTP:
                case LOGOUT:
                case REFRESH_ACCESS_TOKEN:
                    break;
                case GET_FAVORITE_FRIENDS:
                case GET_FRIENDS:
                case GET_SAME_CHANNEL_FRIENDS:
                case GET_GROUPS:
                case GET_GROUP_MEMBERS:
                    sb.append("_").append(cVar.c()).append("_").append(cVar.d());
                    break;
                case UPLOAD_PROFILE_IMAGE:
                    sb.append("_").append(cVar.g());
                    break;
                default:
                    sb.append("_").append(a.getAndIncrement());
                    break;
            }
            return sb.toString();
        }
    }

    private final class b<RO> implements Runnable {
        private final String b;
        private final c c;
        private final d<RO> d;

        b(String str, c cVar, d<RO> dVar) {
            this.b = str;
            this.c = cVar;
            this.d = dVar;
        }

        public final void run() {
            f fVar;
            try {
                e a2 = a.this.a();
                if (a2 != null) {
                    c cVar = this.c;
                    if (a2.a()) {
                        c cVar2 = this.c;
                        fVar = a2.b();
                        if (fVar != null || fVar.a == null) {
                            String str = this.b;
                            c cVar3 = this.c;
                            c cVar4 = this.c;
                            a.this.a.a(this.c, this.d);
                            a.this.a(this.b);
                        }
                        String str2 = this.b;
                        c cVar5 = this.c;
                        this.d.a((Object) fVar.a);
                        a.this.a(this.b);
                        return;
                    }
                }
                fVar = null;
                if (fVar != null) {
                }
                String str3 = this.b;
                c cVar32 = this.c;
            } catch (Throwable th) {
            }
            try {
                c cVar42 = this.c;
                a.this.a.a(this.c, this.d);
            } catch (Throwable th2) {
                a.this.a(this.b);
                throw th2;
            }
            a.this.a(this.b);
        }
    }

    private static final class c<RO> implements ApiRequestFutureListener<RO> {
        private final c a;
        private final e b;

        c(c cVar, e eVar) {
            this.a = cVar;
            this.b = eVar;
        }

        public final void requestComplete(ApiRequestFuture<RO> apiRequestFuture) {
            if (apiRequestFuture.getStatus() == FutureStatus.SUCCESS && apiRequestFuture.getResponseObject() != null) {
                e eVar = this.b;
                c cVar = this.a;
            }
        }
    }

    private <RO> ApiRequestFuture<RO> a(c cVar, ApiRequestFutureListener<RO> apiRequestFutureListener) {
        d dVar;
        boolean z;
        d dVar2;
        if (cVar == null) {
            throw new IllegalArgumentException("request is null.");
        }
        String a2 = C0021a.a(cVar);
        synchronized (this) {
            try {
                dVar = this.c.get(a2);
            } catch (Exception e) {
                dVar = null;
            }
            if (dVar == null) {
                d dVar3 = new d();
                if (this.b != null && this.b.a()) {
                    dVar3.addListener(new c(cVar, this.b));
                }
                this.c.put(a2, dVar3);
                dVar2 = dVar3;
                z = true;
            } else {
                z = false;
                dVar2 = dVar;
            }
        }
        if (z) {
            this.d.execute(new b(a2, cVar, dVar2));
        }
        if (apiRequestFutureListener != null) {
            dVar2.addListener(apiRequestFutureListener);
        }
        return dVar2;
    }

    public final e a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        synchronized (this) {
            this.c.remove(str);
        }
    }

    public final ApiRequestFuture<AccessToken> getAccessToken(String str, String str2, ApiRequestFutureListener<AccessToken> apiRequestFutureListener) {
        c cVar = new c(ApiType.GET_ACCESS_TOKEN);
        cVar.a(str);
        cVar.b(str2);
        return a(cVar, apiRequestFutureListener);
    }

    public final ApiRequestFuture<Users> getFavoriteFriends(int i, int i2, ApiRequestFutureListener<Users> apiRequestFutureListener) {
        c cVar = new c(ApiType.GET_FAVORITE_FRIENDS);
        cVar.a(i);
        cVar.b(i2);
        return a(cVar, apiRequestFutureListener);
    }

    public final ApiRequestFuture<Users> getFriends(int i, int i2, ApiRequestFutureListener<Users> apiRequestFutureListener) {
        c cVar = new c(ApiType.GET_FRIENDS);
        cVar.a(i);
        cVar.b(i2);
        return a(cVar, apiRequestFutureListener);
    }

    public final ApiRequestFuture<Users> getFriends(String[] strArr, ApiRequestFutureListener<Users> apiRequestFutureListener) {
        c cVar = new c(ApiType.GET_PROFILES);
        cVar.a(strArr);
        return a(cVar, apiRequestFutureListener);
    }

    public final ApiRequestFuture<GroupMembers> getGroupMembers(String str, int i, int i2, ApiRequestFutureListener<GroupMembers> apiRequestFutureListener) {
        int i3 = 1;
        c cVar = new c(ApiType.GET_GROUP_MEMBERS);
        cVar.c(str);
        if (i <= 0) {
            i = 1;
        }
        cVar.a(i);
        if (i2 > 0) {
            i3 = i2;
        }
        if (i3 > 500) {
            i3 = 500;
        }
        cVar.b(i3);
        return a(cVar, apiRequestFutureListener);
    }

    public final ApiRequestFuture<Groups> getGroups(int i, int i2, ApiRequestFutureListener<Groups> apiRequestFutureListener) {
        int i3 = 1;
        c cVar = new c(ApiType.GET_GROUPS);
        if (i <= 0) {
            i = 1;
        }
        cVar.a(i);
        if (i2 > 0) {
            i3 = i2;
        }
        if (i3 > 500) {
            i3 = 500;
        }
        cVar.b(i3);
        return a(cVar, apiRequestFutureListener);
    }

    public final ApiRequestFuture<Profile> getMyProfile(ApiRequestFutureListener<Profile> apiRequestFutureListener) {
        return a(new c(ApiType.GET_MY_PROFILE), apiRequestFutureListener);
    }

    public final ApiRequestFuture<Otp> getOtp(ApiRequestFutureListener<Otp> apiRequestFutureListener) {
        return a(new c(ApiType.GET_OTP), apiRequestFutureListener);
    }

    public final ApiRequestFuture<Users> getSameChannelFriend(int i, int i2, ApiRequestFutureListener<Users> apiRequestFutureListener) {
        c cVar = new c(ApiType.GET_SAME_CHANNEL_FRIENDS);
        cVar.a(i);
        cVar.b(i2);
        return a(cVar, apiRequestFutureListener);
    }

    public final ApiRequestFuture<String> logout(String str, ApiRequestFutureListener<String> apiRequestFutureListener) {
        c cVar = new c(ApiType.LOGOUT);
        cVar.f(str);
        return a(cVar, apiRequestFutureListener);
    }

    public final ApiRequestFuture<PostEventResult> postEvent(String[] strArr, int i, String str, Map<String, Object> map, Map<String, Object> map2, ApiRequestFutureListener<PostEventResult> apiRequestFutureListener) {
        c cVar = new c(ApiType.POST_EVENT);
        cVar.a(strArr);
        cVar.c(i);
        cVar.e(str);
        cVar.a(map);
        cVar.b(map2);
        return a(cVar, apiRequestFutureListener);
    }

    public final ApiRequestFuture<String> uploadProfileImage(String str, ApiRequestFutureListener<String> apiRequestFutureListener) {
        c cVar = new c(ApiType.UPLOAD_PROFILE_IMAGE);
        cVar.d(str);
        return a(cVar, apiRequestFutureListener);
    }
}
