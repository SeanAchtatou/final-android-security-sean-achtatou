package jp.line.android.sdk.a.a;

import java.util.Arrays;
import java.util.Map;
import jp.line.android.sdk.api.ApiType;

public final class c {
    public final ApiType a;
    private String b;
    private String c;
    private int d;
    private int e;
    private String[] f;
    private String g;
    private String h;
    private int i;
    private String j;
    private Map<String, Object> k;
    private Map<String, Object> l;
    private String m;

    public c(ApiType apiType) {
        this.a = apiType;
    }

    public final String a() {
        return this.b;
    }

    public final void a(int i2) {
        this.d = i2;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void a(Map<String, Object> map) {
        this.k = map;
    }

    public final void a(String... strArr) {
        this.f = strArr;
    }

    public final String b() {
        return this.c;
    }

    public final void b(int i2) {
        this.e = i2;
    }

    public final void b(String str) {
        this.c = str;
    }

    public final void b(Map<String, Object> map) {
        this.l = map;
    }

    public final int c() {
        return this.d;
    }

    public final void c(int i2) {
        this.i = i2;
    }

    public final void c(String str) {
        this.g = str;
    }

    public final int d() {
        return this.e;
    }

    public final void d(String str) {
        this.h = str;
    }

    public final void e(String str) {
        this.j = str;
    }

    public final String[] e() {
        return this.f;
    }

    public final String f() {
        return this.g;
    }

    public final void f(String str) {
        this.m = str;
    }

    public final String g() {
        return this.h;
    }

    public final int h() {
        return this.i;
    }

    public final String i() {
        return this.j;
    }

    public final Map<String, Object> j() {
        return this.k;
    }

    public final Map<String, Object> k() {
        return this.l;
    }

    public final String l() {
        return this.m;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ApiRequest[apiType=").append(this.a);
        if (this.c != null) {
            sb.append(", otp=").append(this.c);
        }
        if (this.b != null) {
            sb.append(", requestToken=").append(this.b);
        }
        if (this.d > 0) {
            sb.append(", start=").append(this.d);
        }
        if (this.e > 0) {
            sb.append(", display=").append(this.e);
        }
        if (this.f != null) {
            sb.append(", mids=").append(Arrays.toString(this.f));
        }
        if (this.h != null) {
            sb.append(", filePath=").append(this.h);
        }
        if (this.i > 0) {
            sb.append(", toChannel=").append(this.i);
        }
        if (this.j != null) {
            sb.append(", postEventType=").append(this.j);
        }
        if (this.k != null) {
            sb.append(", content=").append(this.k);
        }
        if (this.l != null) {
            sb.append(", push=").append(this.l);
        }
        if (this.m != null) {
            sb.append(", logoutTarget=").append(this.m);
        }
        sb.append("]");
        return sb.toString();
    }
}
