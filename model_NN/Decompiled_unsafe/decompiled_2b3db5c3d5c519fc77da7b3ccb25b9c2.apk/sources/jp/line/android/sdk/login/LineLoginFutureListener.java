package jp.line.android.sdk.login;

public interface LineLoginFutureListener {
    void loginComplete(LineLoginFuture lineLoginFuture);
}
