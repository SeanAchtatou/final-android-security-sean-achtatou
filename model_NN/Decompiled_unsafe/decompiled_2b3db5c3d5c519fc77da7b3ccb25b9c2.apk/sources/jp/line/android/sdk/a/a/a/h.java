package jp.line.android.sdk.a.a.a;

import java.net.HttpURLConnection;
import javax.net.ssl.SSLSocketFactory;
import jp.line.android.sdk.a.a.c;
import jp.line.android.sdk.a.a.d;
import jp.line.android.sdk.exception.LineSdkApiException;
import org.apache.http.client.methods.HttpDelete;

public final class h extends a<String> {
    protected h(SSLSocketFactory sSLSocketFactory) {
        super(true, sSLSocketFactory);
    }

    private static String d(HttpURLConnection httpURLConnection) throws Exception {
        if (httpURLConnection.getResponseCode() == 200) {
            try {
                return l.a(httpURLConnection).optString("result");
            } catch (Throwable th) {
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final String a(c cVar) throws LineSdkApiException {
        return cVar.l();
    }

    /* access modifiers changed from: protected */
    public final void a(HttpURLConnection httpURLConnection, c cVar, d<String> dVar) throws Exception {
        httpURLConnection.setRequestMethod(HttpDelete.METHOD_NAME);
        httpURLConnection.setDoOutput(false);
        b(httpURLConnection);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object c(HttpURLConnection httpURLConnection) throws Exception {
        return d(httpURLConnection);
    }
}
