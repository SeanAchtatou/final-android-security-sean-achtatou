package jp.line.android.sdk.a.a.a;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.GZIPInputStream;
import jp.line.android.sdk.LineSdkContextManager;
import org.apache.http.HttpHeaders;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

public final class l {
    private static String a;
    private static final AtomicBoolean b = new AtomicBoolean(false);

    private static String a() {
        if (a == null) {
            StringBuilder sb = new StringBuilder();
            Context applicationContext = LineSdkContextManager.getSdkContext().getApplicationContext();
            try {
                PackageInfo packageInfo = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 128);
                if (packageInfo != null) {
                    sb.append(packageInfo.packageName).append("/").append(packageInfo.versionName);
                } else {
                    sb.append("UNK/UNK");
                }
                sb.append(" ChannelSDK/").append(LineSdkContextManager.getSdkContext().getSdkVersion());
                sb.append(" (Linux; U; Android ").append(Build.VERSION.RELEASE).append("; ");
                Locale locale = Locale.getDefault();
                sb.append(locale.getLanguage()).append("-").append(locale.getCountry()).append("; ").append(Build.MODEL).append(" Build/").append(Build.ID).append(")");
                a = sb.toString();
            } catch (PackageManager.NameNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
        return a;
    }

    public static final HttpURLConnection a(String str) throws MalformedURLException, IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setRequestProperty("User-Agent", a());
        httpURLConnection.setRequestProperty(HttpHeaders.ACCEPT_ENCODING, "gzip");
        httpURLConnection.setConnectTimeout(90000);
        httpURLConnection.setReadTimeout(90000);
        System.setProperty("http.keepAlive", "false");
        return httpURLConnection;
    }

    public static final JSONObject a(HttpURLConnection httpURLConnection) throws UnsupportedEncodingException, IOException, JSONException {
        return new JSONObject(b(httpURLConnection));
    }

    private static String b(HttpURLConnection httpURLConnection) throws UnsupportedEncodingException, IOException {
        boolean z = false;
        List list = httpURLConnection.getHeaderFields().get("Content-Encoding");
        if (list != null) {
            int i = 0;
            while (true) {
                if (i >= list.size()) {
                    break;
                } else if (((String) list.get(i)).equals("gzip")) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        BufferedReader bufferedReader = null;
        try {
            InputStream inputStream = httpURLConnection.getResponseCode() < 400 ? httpURLConnection.getInputStream() : httpURLConnection.getErrorStream();
            BufferedReader bufferedReader2 = z ? new BufferedReader(new InputStreamReader(new GZIPInputStream(inputStream), HTTP.UTF_8)) : new BufferedReader(new InputStreamReader(inputStream, HTTP.UTF_8));
            while (true) {
                String readLine = bufferedReader2.readLine();
                if (readLine != null) {
                    sb.append(readLine);
                } else {
                    bufferedReader2.close();
                    return sb.toString();
                }
            }
        } catch (Throwable th) {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            throw th;
        }
    }
}
