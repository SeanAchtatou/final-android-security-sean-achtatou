package jp.line.android.sdk.a.a;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import jp.line.android.sdk.api.ApiRequestFuture;
import jp.line.android.sdk.api.ApiRequestFutureListener;
import jp.line.android.sdk.api.ApiRequestFutureProgressListener;
import jp.line.android.sdk.api.FutureStatus;

public final class d<RO> implements ApiRequestFuture<RO> {
    private boolean a;
    private FutureStatus b = FutureStatus.PROCESSING;
    private CountDownLatch c;
    private List<ApiRequestFutureListener<RO>> d;
    private List<ApiRequestFutureProgressListener<RO>> e;
    private RO f;
    private Throwable g;
    private long h;
    private long i;

    private final void a(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            a((ApiRequestFutureListener) it.next());
        }
    }

    private final void a(ApiRequestFutureListener apiRequestFutureListener) {
        try {
            apiRequestFutureListener.requestComplete(this);
        } catch (Throwable th) {
        }
    }

    private final void a(ApiRequestFutureProgressListener<RO> apiRequestFutureProgressListener, long j, long j2) {
        try {
            apiRequestFutureProgressListener.operationProgressed(this, j, j2);
        } catch (Throwable th) {
        }
    }

    private boolean a(FutureStatus futureStatus) {
        if (this.a) {
            return false;
        }
        if (this.c != null) {
            this.c.countDown();
        }
        this.a = true;
        this.b = futureStatus;
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001f, code lost:
        if (r0 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0021, code lost:
        r6 = r0.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0029, code lost:
        if (r6.hasNext() == false) goto L_0x0006;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002b, code lost:
        a((jp.line.android.sdk.api.ApiRequestFutureProgressListener) r6.next(), r8, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(long r8, long r10) {
        /*
            r7 = this;
            monitor-enter(r7)
            boolean r0 = r7.a     // Catch:{ all -> 0x003a }
            if (r0 == 0) goto L_0x0007
            monitor-exit(r7)     // Catch:{ all -> 0x003a }
        L_0x0006:
            return
        L_0x0007:
            r7.h = r8     // Catch:{ all -> 0x003a }
            r7.i = r10     // Catch:{ all -> 0x003a }
            java.util.List<jp.line.android.sdk.api.ApiRequestFutureProgressListener<RO>> r0 = r7.e     // Catch:{ all -> 0x003a }
            if (r0 == 0) goto L_0x0038
            java.util.List<jp.line.android.sdk.api.ApiRequestFutureProgressListener<RO>> r0 = r7.e     // Catch:{ all -> 0x003a }
            int r0 = r0.size()     // Catch:{ all -> 0x003a }
            if (r0 <= 0) goto L_0x0038
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x003a }
            java.util.List<jp.line.android.sdk.api.ApiRequestFutureProgressListener<RO>> r1 = r7.e     // Catch:{ all -> 0x003a }
            r0.<init>(r1)     // Catch:{ all -> 0x003a }
        L_0x001e:
            monitor-exit(r7)     // Catch:{ all -> 0x003a }
            if (r0 == 0) goto L_0x0006
            java.util.Iterator r6 = r0.iterator()
        L_0x0025:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x0006
            java.lang.Object r1 = r6.next()
            jp.line.android.sdk.api.ApiRequestFutureProgressListener r1 = (jp.line.android.sdk.api.ApiRequestFutureProgressListener) r1
            r0 = r7
            r2 = r8
            r4 = r10
            r0.a(r1, r2, r4)
            goto L_0x0025
        L_0x0038:
            r0 = 0
            goto L_0x001e
        L_0x003a:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.line.android.sdk.a.a.d.a(long, long):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0016, code lost:
        if (r0 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0018, code lost:
        a((java.util.List) r0);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.Object r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            r2.f = r3     // Catch:{ all -> 0x001c }
            jp.line.android.sdk.api.FutureStatus r0 = jp.line.android.sdk.api.FutureStatus.SUCCESS     // Catch:{ all -> 0x001c }
            boolean r0 = r2.a(r0)     // Catch:{ all -> 0x001c }
            if (r0 != 0) goto L_0x000d
            monitor-exit(r2)     // Catch:{ all -> 0x001c }
        L_0x000c:
            return
        L_0x000d:
            java.util.List<jp.line.android.sdk.api.ApiRequestFutureListener<RO>> r0 = r2.d     // Catch:{ all -> 0x001c }
            r1 = 0
            r2.d = r1     // Catch:{ all -> 0x001c }
            r1 = 0
            r2.e = r1     // Catch:{ all -> 0x001c }
            monitor-exit(r2)     // Catch:{ all -> 0x001c }
            if (r0 == 0) goto L_0x000c
            r2.a(r0)
            goto L_0x000c
        L_0x001c:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.line.android.sdk.a.a.d.a(java.lang.Object):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0016, code lost:
        if (r0 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0018, code lost:
        a((java.util.List) r0);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.Throwable r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            r2.g = r3     // Catch:{ all -> 0x001c }
            jp.line.android.sdk.api.FutureStatus r0 = jp.line.android.sdk.api.FutureStatus.FAILED     // Catch:{ all -> 0x001c }
            boolean r0 = r2.a(r0)     // Catch:{ all -> 0x001c }
            if (r0 != 0) goto L_0x000d
            monitor-exit(r2)     // Catch:{ all -> 0x001c }
        L_0x000c:
            return
        L_0x000d:
            java.util.List<jp.line.android.sdk.api.ApiRequestFutureListener<RO>> r0 = r2.d     // Catch:{ all -> 0x001c }
            r1 = 0
            r2.d = r1     // Catch:{ all -> 0x001c }
            r1 = 0
            r2.e = r1     // Catch:{ all -> 0x001c }
            monitor-exit(r2)     // Catch:{ all -> 0x001c }
            if (r0 == 0) goto L_0x000c
            r2.a(r0)
            goto L_0x000c
        L_0x001c:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.line.android.sdk.a.a.d.a(java.lang.Throwable):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001a, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0015, code lost:
        if (r0 == null) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0017, code lost:
        a((java.util.List) r0);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a() {
        /*
            r2 = this;
            monitor-enter(r2)
            jp.line.android.sdk.api.FutureStatus r0 = jp.line.android.sdk.api.FutureStatus.CANCELED     // Catch:{ all -> 0x001c }
            boolean r0 = r2.a(r0)     // Catch:{ all -> 0x001c }
            if (r0 != 0) goto L_0x000c
            r0 = 0
            monitor-exit(r2)     // Catch:{ all -> 0x001c }
        L_0x000b:
            return r0
        L_0x000c:
            java.util.List<jp.line.android.sdk.api.ApiRequestFutureListener<RO>> r0 = r2.d     // Catch:{ all -> 0x001c }
            r1 = 0
            r2.d = r1     // Catch:{ all -> 0x001c }
            r1 = 0
            r2.e = r1     // Catch:{ all -> 0x001c }
            monitor-exit(r2)     // Catch:{ all -> 0x001c }
            if (r0 == 0) goto L_0x001a
            r2.a(r0)
        L_0x001a:
            r0 = 1
            goto L_0x000b
        L_0x001c:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.line.android.sdk.a.a.d.a():boolean");
    }

    public final boolean addListener(ApiRequestFutureListener<RO> apiRequestFutureListener) {
        boolean z = false;
        boolean z2 = true;
        if (!this.a) {
            synchronized (this) {
                if (!this.a) {
                    if (this.d == null) {
                        this.d = new ArrayList();
                    }
                    if (!this.d.contains(apiRequestFutureListener)) {
                        this.d.add(apiRequestFutureListener);
                        z = true;
                        z2 = false;
                    } else {
                        z2 = false;
                    }
                }
            }
        }
        if (z2) {
            a((ApiRequestFutureListener) apiRequestFutureListener);
        }
        return z;
    }

    public final boolean addProgressListener(ApiRequestFutureProgressListener<RO> apiRequestFutureProgressListener) {
        long j;
        long j2;
        boolean z;
        boolean z2 = false;
        if (this.a) {
            j = this.h;
            j2 = this.i;
            z = false;
        } else {
            synchronized (this) {
                j = this.h;
                j2 = this.i;
                if (!this.a) {
                    if (this.e == null) {
                        this.e = new ArrayList();
                    }
                    if (!this.e.contains(apiRequestFutureProgressListener)) {
                        this.e.add(apiRequestFutureProgressListener);
                        z2 = true;
                    }
                }
            }
            z = z2;
        }
        if (j > 0 || j2 > 0) {
            a(apiRequestFutureProgressListener, j, j2);
        }
        return z;
    }

    public final void await() {
        if (!await(600000, TimeUnit.MILLISECONDS)) {
            a((Throwable) new TimeoutException("ApiRequestFuture.await() has timed out.(timeout=600000msec)"));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return r3.c.await(r4, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean await(long r4, java.util.concurrent.TimeUnit r6) {
        /*
            r3 = this;
            r0 = 1
            boolean r1 = r3.a
            if (r1 == 0) goto L_0x0006
        L_0x0005:
            return r0
        L_0x0006:
            monitor-enter(r3)
            boolean r1 = r3.a     // Catch:{ all -> 0x000d }
            if (r1 == 0) goto L_0x0010
            monitor-exit(r3)     // Catch:{ all -> 0x000d }
            goto L_0x0005
        L_0x000d:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x0010:
            java.util.concurrent.CountDownLatch r1 = r3.c     // Catch:{ all -> 0x000d }
            if (r1 != 0) goto L_0x001c
            java.util.concurrent.CountDownLatch r1 = new java.util.concurrent.CountDownLatch     // Catch:{ all -> 0x000d }
            r2 = 1
            r1.<init>(r2)     // Catch:{ all -> 0x000d }
            r3.c = r1     // Catch:{ all -> 0x000d }
        L_0x001c:
            monitor-exit(r3)     // Catch:{ all -> 0x000d }
            java.util.concurrent.CountDownLatch r1 = r3.c     // Catch:{ InterruptedException -> 0x0024 }
            boolean r0 = r1.await(r4, r6)     // Catch:{ InterruptedException -> 0x0024 }
            goto L_0x0005
        L_0x0024:
            r1 = move-exception
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.line.android.sdk.a.a.d.await(long, java.util.concurrent.TimeUnit):boolean");
    }

    public final boolean cancel() {
        return a();
    }

    public final Throwable getCause() {
        return this.g;
    }

    public final RO getResponseObject() {
        return this.f;
    }

    public final FutureStatus getStatus() {
        return this.b;
    }

    public final boolean isDone() {
        return this.a;
    }

    public final boolean removeProgressListener(ApiRequestFutureProgressListener<RO> apiRequestFutureProgressListener) {
        boolean remove;
        synchronized (this) {
            remove = this.e.remove(apiRequestFutureProgressListener);
        }
        return remove;
    }

    public final boolean removeistener(ApiRequestFutureListener<RO> apiRequestFutureListener) {
        boolean remove;
        synchronized (this) {
            remove = this.d.remove(apiRequestFutureListener);
        }
        return remove;
    }

    public final String toString() {
        String property = System.getProperty("line.separator");
        StringBuilder sb = new StringBuilder();
        sb.append("ApiRequestFuture [").append(property).append("    isDone=").append(this.a).append(property).append("    status=").append(this.b).append(property).append("    responseObject=").append((Object) this.f).append(property);
        if (this.g != null) {
            StringWriter stringWriter = new StringWriter();
            this.g.printStackTrace(new PrintWriter(stringWriter));
            sb.append("    cause=").append(stringWriter.toString());
        }
        if (this.i > 0) {
            sb.append("    progress=").append(this.h).append("/").append(this.i);
        }
        sb.append("]");
        return sb.toString();
    }
}
