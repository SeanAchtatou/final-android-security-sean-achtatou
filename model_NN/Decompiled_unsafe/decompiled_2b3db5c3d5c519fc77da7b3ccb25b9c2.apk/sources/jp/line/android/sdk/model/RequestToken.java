package jp.line.android.sdk.model;

public class RequestToken {
    public final long expire;
    public final boolean isFromWebLogin;
    public final String refleshToken;
    public final String requestToken;

    RequestToken(String str, long j, String str2, boolean z) {
        this.requestToken = str;
        this.expire = j;
        this.refleshToken = str2;
        this.isFromWebLogin = z;
    }

    public static final RequestToken createFromA2ALogin(String str) {
        return new RequestToken(str, -1, null, false);
    }

    public static final RequestToken createFromWebLogin(String str, long j, String str2) {
        return new RequestToken(str, j, str2, true);
    }

    public String toString() {
        return "RequestToken [requestToken=" + this.requestToken + ", expire=" + this.expire + ", refleshToken=" + this.refleshToken + ", isFromWebLogin=" + this.isFromWebLogin + "]";
    }
}
