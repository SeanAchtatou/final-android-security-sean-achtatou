package jp.line.android.sdk.model;

public class Profile extends User {
    public final String statusMessage;

    public Profile(String str, String str2, String str3, String str4) {
        super(str, str2, str3);
        this.statusMessage = str4;
    }

    public String toString() {
        return "Profile [mid=" + this.mid + ", displayName=" + this.displayName + ", pictureUrl=" + this.pictureUrl + ", statusMessage=" + this.statusMessage + "]";
    }
}
