package jp.line.android.sdk.a.a.a;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public final class m {
    private static final Collection a(Object obj) {
        int i = 0;
        if (obj instanceof int[]) {
            int[] iArr = (int[]) obj;
            ArrayList arrayList = new ArrayList();
            int length = iArr.length;
            while (i < length) {
                arrayList.add(Integer.valueOf(iArr[i]));
                i++;
            }
            return arrayList;
        } else if (obj instanceof long[]) {
            long[] jArr = (long[]) obj;
            ArrayList arrayList2 = new ArrayList();
            int length2 = jArr.length;
            while (i < length2) {
                arrayList2.add(Long.valueOf(jArr[i]));
                i++;
            }
            return arrayList2;
        } else if (obj instanceof double[]) {
            double[] dArr = (double[]) obj;
            ArrayList arrayList3 = new ArrayList();
            int length3 = dArr.length;
            while (i < length3) {
                arrayList3.add(Double.valueOf(dArr[i]));
                i++;
            }
            return arrayList3;
        } else if (obj instanceof float[]) {
            float[] fArr = (float[]) obj;
            ArrayList arrayList4 = new ArrayList();
            int length4 = fArr.length;
            while (i < length4) {
                arrayList4.add(Float.valueOf(fArr[i]));
                i++;
            }
            return arrayList4;
        } else if (obj instanceof Object[]) {
            return Arrays.asList((Object[]) obj);
        } else {
            return null;
        }
    }

    private static JSONArray a(Collection collection) {
        if (collection == null || collection.size() <= 0) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        for (Object next : collection) {
            if (next != null) {
                try {
                    if (next.getClass().isArray()) {
                        Collection a = a(next);
                        if ((a != null ? a(a) : null) != null) {
                            jSONArray.put(jSONArray);
                        }
                    }
                } catch (Throwable th) {
                }
            }
            if (next instanceof Collection) {
                if (a((Collection) next) != null) {
                    jSONArray.put(jSONArray);
                }
            } else if (next instanceof Map) {
                JSONObject a2 = a((Map) next);
                if (a2 != null) {
                    jSONArray.put(a2);
                }
            } else {
                jSONArray.put(next);
            }
        }
        return jSONArray;
    }

    public static final JSONObject a(Map map) {
        if (map == null || map.size() <= 0) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        for (Object next : map.keySet()) {
            Object obj = map.get(next);
            if (obj != null) {
                try {
                    if (obj.getClass().isArray()) {
                        Collection a = a(obj);
                        JSONArray a2 = a != null ? a(a) : null;
                        if (a2 != null) {
                            jSONObject.putOpt(next.toString(), a2);
                        }
                    }
                } catch (Throwable th) {
                }
            }
            if (obj instanceof Collection) {
                JSONArray a3 = a((Collection) obj);
                if (a3 != null) {
                    jSONObject.putOpt(next.toString(), a3);
                }
            } else if (obj instanceof Map) {
                JSONObject a4 = a((Map) obj);
                if (a4 != null) {
                    jSONObject.putOpt(next.toString(), a4);
                }
            } else {
                jSONObject.putOpt(next.toString(), obj);
            }
        }
        return jSONObject;
    }
}
