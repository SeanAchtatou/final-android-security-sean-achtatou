package jp.line.android.sdk.a.b;

import android.content.Context;
import android.util.SparseArray;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import jp.line.android.sdk.encryption.LineSdkEncryption;
import jp.line.android.sdk.util.Lspg;
import org.apache.http.protocol.HTTP;

public final class b implements LineSdkEncryption {
    private final SparseArray<byte[]> a = new SparseArray<>();

    private final byte[] a(Context context, int i) {
        byte[] bArr;
        synchronized (this) {
            bArr = this.a.get(i);
        }
        if (bArr == null) {
            bArr = Lspg.gk(context, i);
            synchronized (this) {
                this.a.put(i, bArr);
            }
        }
        return bArr;
    }

    public final String decrypt(Context context, int i, String str) {
        if (str == null) {
            return null;
        }
        try {
            return new String(decrypt(context, i, a.a(str)), HTTP.UTF_8);
        } catch (Throwable th) {
            return null;
        }
    }

    public final byte[] decrypt(Context context, int i, byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        try {
            Cipher instance = Cipher.getInstance("AES");
            instance.init(2, new SecretKeySpec(a(context, i), "AES"));
            return instance.doFinal(bArr);
        } catch (Throwable th) {
            return null;
        }
    }

    public final String encrypt(Context context, int i, String str) {
        if (str == null) {
            return null;
        }
        try {
            return a.a(encrypt(context, i, str.getBytes(HTTP.UTF_8)));
        } catch (Throwable th) {
            return null;
        }
    }

    public final byte[] encrypt(Context context, int i, byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        try {
            Cipher instance = Cipher.getInstance("AES");
            instance.init(1, new SecretKeySpec(a(context, i), "AES"));
            return instance.doFinal(bArr);
        } catch (Throwable th) {
            return null;
        }
    }
}
