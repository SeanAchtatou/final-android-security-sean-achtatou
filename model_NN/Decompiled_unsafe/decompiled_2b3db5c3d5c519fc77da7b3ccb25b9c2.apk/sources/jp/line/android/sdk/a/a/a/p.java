package jp.line.android.sdk.a.a.a;

import jp.line.android.sdk.LineSdkContextManager;
import jp.line.android.sdk.a.a.c;

public final class p {
    public static final String a(c cVar) {
        StringBuilder sb = new StringBuilder();
        switch (cVar.a) {
            case GET_OTP:
                sb.append(LineSdkContextManager.getSdkContext().getChannelApiServerHost()).append("/v1/oauth/otp");
                break;
            case GET_ACCESS_TOKEN:
            case REFRESH_ACCESS_TOKEN:
                sb.append(LineSdkContextManager.getSdkContext().getChannelApiServerHost()).append("/v1/oauth/accessToken");
                break;
            case LOGOUT:
                sb.append(LineSdkContextManager.getSdkContext().getChannelApiServerHost()).append("/v1/oauth/logout");
                break;
            case GET_MY_PROFILE:
                sb.append(LineSdkContextManager.getSdkContext().getChannelApiServerHost()).append("/v1/profile");
                break;
            case GET_FRIENDS:
                sb.append(LineSdkContextManager.getSdkContext().getChannelApiServerHost()).append("/v1/friends");
                break;
            case GET_FAVORITE_FRIENDS:
                sb.append(LineSdkContextManager.getSdkContext().getChannelApiServerHost()).append("/v1/friends/favorite");
                break;
            case GET_PROFILES:
                sb.append(LineSdkContextManager.getSdkContext().getChannelApiServerHost()).append("/v1/profiles");
                break;
            case GET_SAME_CHANNEL_FRIENDS:
                sb.append(LineSdkContextManager.getSdkContext().getChannelApiServerHost()).append("/v1/friends/channel");
                break;
            case UPLOAD_PROFILE_IMAGE:
                sb.append(LineSdkContextManager.getSdkContext().getChannelApiServerHost()).append("/v1/profile/image");
                break;
            case POST_EVENT:
                sb.append(LineSdkContextManager.getSdkContext().getChannelApiServerHost()).append("/v1/events");
                break;
            case GET_GROUPS:
                sb.append(LineSdkContextManager.getSdkContext().getChannelApiServerHost()).append("/v1/groups");
                break;
            case GET_GROUP_MEMBERS:
                sb.append(LineSdkContextManager.getSdkContext().getChannelApiServerHost()).append("/v1/group/").append(cVar.f()).append("/members");
                break;
        }
        return sb.toString();
    }
}
