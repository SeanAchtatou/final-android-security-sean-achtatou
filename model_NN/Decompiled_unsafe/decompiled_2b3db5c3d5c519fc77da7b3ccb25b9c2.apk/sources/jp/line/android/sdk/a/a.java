package jp.line.android.sdk.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.facebook.appevents.AppEventsConstants;
import jp.line.android.sdk.LineSdkContextManager;
import jp.line.android.sdk.encryption.LineSdkEncryption;
import jp.line.android.sdk.encryption.LineSdkEncryptionFactory;
import jp.line.android.sdk.model.AccessToken;

public final class a {
    private static a a;
    private AccessToken b;

    private a() {
    }

    public static final a a() {
        if (a == null) {
            synchronized (a.class) {
                if (a == null) {
                    a = new a();
                }
            }
        }
        return a;
    }

    private static SharedPreferences d() {
        return LineSdkContextManager.getSdkContext().getApplicationContext().getSharedPreferences("linesdk-2", 0);
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(jp.line.android.sdk.model.AccessToken r9) {
        /*
            r8 = this;
            r0 = 0
            if (r9 != 0) goto L_0x0007
            r8.c()     // Catch:{ Throwable -> 0x0058 }
        L_0x0006:
            return r0
        L_0x0007:
            monitor-enter(r8)     // Catch:{ Throwable -> 0x0058 }
            android.content.SharedPreferences r1 = d()     // Catch:{ all -> 0x0055 }
            jp.line.android.sdk.encryption.LineSdkEncryption r2 = jp.line.android.sdk.encryption.LineSdkEncryptionFactory.getLineSdkEncryption()     // Catch:{ all -> 0x0055 }
            jp.line.android.sdk.LineSdkContext r3 = jp.line.android.sdk.LineSdkContextManager.getSdkContext()     // Catch:{ all -> 0x0055 }
            android.content.Context r3 = r3.getApplicationContext()     // Catch:{ all -> 0x0055 }
            android.content.SharedPreferences$Editor r1 = r1.edit()     // Catch:{ all -> 0x0055 }
            java.lang.String r4 = "1"
            r5 = 3455546(0x34ba3a, float:4.842251E-39)
            java.lang.String r6 = r9.mid     // Catch:{ all -> 0x0055 }
            java.lang.String r5 = r2.encrypt(r3, r5, r6)     // Catch:{ all -> 0x0055 }
            r1.putString(r4, r5)     // Catch:{ all -> 0x0055 }
            java.lang.String r4 = "2"
            r5 = 3455546(0x34ba3a, float:4.842251E-39)
            java.lang.String r6 = r9.accessToken     // Catch:{ all -> 0x0055 }
            java.lang.String r5 = r2.encrypt(r3, r5, r6)     // Catch:{ all -> 0x0055 }
            r1.putString(r4, r5)     // Catch:{ all -> 0x0055 }
            java.lang.String r4 = "3"
            long r6 = r9.expire     // Catch:{ all -> 0x0055 }
            r1.putLong(r4, r6)     // Catch:{ all -> 0x0055 }
            java.lang.String r4 = "4"
            r5 = 3455546(0x34ba3a, float:4.842251E-39)
            java.lang.String r6 = r9.refreshToken     // Catch:{ all -> 0x0055 }
            java.lang.String r2 = r2.encrypt(r3, r5, r6)     // Catch:{ all -> 0x0055 }
            r1.putString(r4, r2)     // Catch:{ all -> 0x0055 }
            r1.commit()     // Catch:{ all -> 0x0055 }
            r8.b = r9     // Catch:{ all -> 0x0055 }
            monitor-exit(r8)     // Catch:{ all -> 0x0055 }
            r0 = 1
            goto L_0x0006
        L_0x0055:
            r1 = move-exception
            monitor-exit(r8)     // Catch:{ Throwable -> 0x0058 }
            throw r1     // Catch:{ Throwable -> 0x0058 }
        L_0x0058:
            r1 = move-exception
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.line.android.sdk.a.a.a(jp.line.android.sdk.model.AccessToken):boolean");
    }

    public final AccessToken b() {
        AccessToken accessToken = null;
        if (this.b == null) {
            synchronized (this) {
                if (this.b == null) {
                    SharedPreferences d = d();
                    LineSdkEncryption lineSdkEncryption = LineSdkEncryptionFactory.getLineSdkEncryption();
                    Context applicationContext = LineSdkContextManager.getSdkContext().getApplicationContext();
                    String decrypt = lineSdkEncryption.decrypt(applicationContext, 3455546, d.getString(AppEventsConstants.EVENT_PARAM_VALUE_YES, null));
                    if (decrypt != null) {
                        String decrypt2 = lineSdkEncryption.decrypt(applicationContext, 3455546, d.getString(Version.code, null));
                        long j = d.getLong("3", -1);
                        String decrypt3 = lineSdkEncryption.decrypt(applicationContext, 3455546, d.getString("4", null));
                        if (!(decrypt == null || decrypt2 == null)) {
                            accessToken = new AccessToken(decrypt, decrypt2, j, decrypt3);
                        }
                    }
                    this.b = accessToken;
                }
            }
        }
        return this.b;
    }

    public final boolean c() {
        boolean z;
        synchronized (this) {
            try {
                d().edit().clear().commit();
                this.b = null;
                z = true;
            } catch (Throwable th) {
                z = false;
            }
        }
        return z;
    }
}
