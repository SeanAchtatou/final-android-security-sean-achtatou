package jp.line.android.sdk.exception;

public class LineSdkApiException extends Exception {
    private static final long serialVersionUID = 1;
    public final LineSdkApiError apiError;
    public final int httpStatusCode;
    public final LineSdkApiServerError serverError;

    public LineSdkApiException(LineSdkApiError lineSdkApiError) {
        super(createMessage(null, lineSdkApiError, -1, null));
        this.apiError = lineSdkApiError;
        this.httpStatusCode = -1;
        this.serverError = null;
    }

    public LineSdkApiException(LineSdkApiError lineSdkApiError, int i, String str) {
        super(createMessage(str, lineSdkApiError, i, null));
        this.apiError = lineSdkApiError;
        this.httpStatusCode = i;
        this.serverError = null;
    }

    public LineSdkApiException(LineSdkApiError lineSdkApiError, int i, LineSdkApiServerError lineSdkApiServerError) {
        super(createMessage(null, lineSdkApiError, i, lineSdkApiServerError));
        this.apiError = lineSdkApiError;
        this.httpStatusCode = i;
        this.serverError = lineSdkApiServerError;
    }

    public LineSdkApiException(LineSdkApiError lineSdkApiError, String str) {
        super(createMessage(str, lineSdkApiError, -1, null));
        this.apiError = lineSdkApiError;
        this.httpStatusCode = -1;
        this.serverError = null;
    }

    public LineSdkApiException(LineSdkApiError lineSdkApiError, Throwable th) {
        super(createMessage(null, lineSdkApiError, -1, null), th);
        this.apiError = lineSdkApiError;
        this.httpStatusCode = -1;
        this.serverError = null;
    }

    private static final String createMessage(String str, LineSdkApiError lineSdkApiError, int i, LineSdkApiServerError lineSdkApiServerError) {
        StringBuilder sb = new StringBuilder();
        if (str != null) {
            sb.append(str);
        }
        if (lineSdkApiServerError != null) {
            sb.append(" serverError=").append(lineSdkApiServerError);
        }
        sb.append(" LineSdkApiError=").append(lineSdkApiError);
        if (i > 0) {
            sb.append(" httpStatusCode=").append(i);
        }
        return sb.toString();
    }

    public final boolean isAccessTokenExpired() {
        return this.apiError == LineSdkApiError.SERVER_ERROR && this.httpStatusCode == 401 && (this.serverError == null || this.serverError.statusCode == 412 || this.serverError.statusCode == 401);
    }
}
