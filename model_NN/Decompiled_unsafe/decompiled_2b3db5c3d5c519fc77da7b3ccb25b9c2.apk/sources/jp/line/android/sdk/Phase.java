package jp.line.android.sdk;

public enum Phase {
    BETA,
    RC,
    REAL
}
