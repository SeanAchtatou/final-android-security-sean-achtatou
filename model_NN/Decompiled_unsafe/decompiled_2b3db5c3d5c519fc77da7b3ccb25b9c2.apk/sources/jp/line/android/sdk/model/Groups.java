package jp.line.android.sdk.model;

import java.util.List;

public class Groups {
    public final int count;
    public final int display;
    public final List<Group> groupList;
    public final int start;
    public final int total;

    public Groups(int i, int i2, int i3, int i4, List<Group> list) {
        this.count = i;
        this.total = i4;
        this.start = i2;
        this.display = i3;
        this.groupList = list;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Groups [count=").append(this.count).append(", total=").append(this.total).append(", start=").append(this.start).append(", display=").append(this.display);
        sb.append(", groupList=");
        if (this.groupList == null || this.groupList.size() <= 0) {
            sb.append("null or empty");
        } else {
            for (Group append : this.groupList) {
                sb.append(append).append(",").append(System.getProperty("line.separator"));
            }
            sb.delete(sb.length() - 2, sb.length());
        }
        return sb.toString();
    }
}
