package jp.line.android.sdk;

import android.content.Context;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import jp.line.android.sdk.a.c.b;
import jp.line.android.sdk.api.ApiClient;
import jp.line.android.sdk.login.LineAuthManager;

public final class LineSdkContextManager {
    static CountDownLatch initializeLatch;
    static LineSdkContext lineSdkConfig;

    public static class a implements LineSdkContextInitializer {
        public final ApiClient createApiClient(LineSdkContext lineSdkContext) {
            return new jp.line.android.sdk.a.a.a();
        }

        public final LineAuthManager createAuthManager(LineSdkContext lineSdkContext) {
            return new b();
        }
    }

    private LineSdkContextManager() {
    }

    public static LineSdkContext getSdkContext() {
        CountDownLatch countDownLatch;
        if (lineSdkConfig == null) {
            if (initializeLatch != null) {
                countDownLatch = initializeLatch;
            } else {
                synchronized (LineSdkContextManager.class) {
                    if (initializeLatch == null) {
                        throw new RuntimeException("LineSdkConfig was not initialized.");
                    }
                    countDownLatch = initializeLatch;
                }
            }
            try {
                countDownLatch.await(2000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
            }
            if (lineSdkConfig == null) {
                throw new RuntimeException("LineSdkConfig was not initialized.");
            }
        }
        return lineSdkConfig;
    }

    public static final void initialize(Context context) {
        initialize(context, -1, null, null);
    }

    public static final void initialize(Context context, int i, Phase phase) {
        initialize(context, i, phase, null);
    }

    public static final void initialize(final Context context, final int i, final Phase phase, final LineSdkContextInitializer lineSdkContextInitializer) {
        boolean z = false;
        if (lineSdkConfig == null && initializeLatch == null) {
            synchronized (LineSdkContextManager.class) {
                if (lineSdkConfig == null && initializeLatch == null) {
                    initializeLatch = new CountDownLatch(1);
                    z = true;
                }
            }
        }
        if (z) {
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                public final void run() {
                    try {
                        jp.line.android.sdk.a.b bVar = new jp.line.android.sdk.a.b();
                        if (lineSdkContextInitializer == null) {
                            bVar.a(context, i, phase, new a());
                        } else {
                            bVar.a(context, i, phase, lineSdkContextInitializer);
                        }
                        synchronized (LineSdkContextManager.class) {
                            LineSdkContextManager.lineSdkConfig = bVar;
                        }
                        LineSdkContextManager.initializeLatch.countDown();
                    } catch (Throwable th) {
                        LineSdkContextManager.initializeLatch.countDown();
                        throw th;
                    }
                }
            });
        }
    }
}
