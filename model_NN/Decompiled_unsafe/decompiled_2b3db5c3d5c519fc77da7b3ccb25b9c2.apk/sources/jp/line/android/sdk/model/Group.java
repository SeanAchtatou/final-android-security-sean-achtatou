package jp.line.android.sdk.model;

public class Group {
    public final String mid;
    public final String name;
    public final String pictureUrl;

    public Group(String str, String str2, String str3) {
        this.mid = str;
        this.name = str2;
        this.pictureUrl = str3;
    }

    public String toString() {
        return "Group [mid=" + this.mid + ", name=" + this.name + ", pictureUrl=" + this.pictureUrl + "]";
    }
}
