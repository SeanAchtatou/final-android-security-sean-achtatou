package jp.line.android.sdk.a.c;

import android.app.Activity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import jp.line.android.sdk.LineSdkContextManager;
import jp.line.android.sdk.api.ApiRequestFuture;
import jp.line.android.sdk.api.ApiRequestFutureListener;
import jp.line.android.sdk.exception.LineSdkLoginError;
import jp.line.android.sdk.exception.LineSdkLoginException;
import jp.line.android.sdk.login.LineAuthManager;
import jp.line.android.sdk.login.LineLoginFuture;
import jp.line.android.sdk.login.LineLoginFutureProgressListener;
import jp.line.android.sdk.login.OnAccessTokenChangedListener;
import jp.line.android.sdk.model.AccessToken;
import jp.line.android.sdk.model.Otp;

public final class b implements LineAuthManager, LineLoginFutureProgressListener {
    c a;
    private final ExecutorService b = Executors.newCachedThreadPool();
    private List<OnAccessTokenChangedListener> c;

    private static final class a implements Runnable {
        private List<OnAccessTokenChangedListener> a;
        private AccessToken b;

        public a(List<OnAccessTokenChangedListener> list, AccessToken accessToken) {
            this.a = list;
            this.b = accessToken;
        }

        public final void run() {
            for (OnAccessTokenChangedListener onAccessTokenChanged : this.a) {
                try {
                    onAccessTokenChanged.onAccessTokenChanged(this.b);
                } catch (Throwable th) {
                }
            }
        }
    }

    /* renamed from: jp.line.android.sdk.a.c.b$b  reason: collision with other inner class name */
    static class C0023b implements Runnable, Future<Object> {
        private final CountDownLatch a = new CountDownLatch(1);
        private boolean b;
        private ExecutionException c;

        static AccessToken b() {
            AccessToken b2 = jp.line.android.sdk.a.a.a().b();
            if (b2 != null) {
                jp.line.android.sdk.a.a.a().c();
            }
            d.a();
            d.c();
            return b2;
        }

        /* access modifiers changed from: package-private */
        public void a() throws Exception {
            b();
        }

        public boolean cancel(boolean z) {
            return false;
        }

        public Object get() throws InterruptedException, ExecutionException {
            this.a.await();
            if (this.c == null) {
                return null;
            }
            throw this.c;
        }

        public Object get(long j, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
            this.a.await(j, timeUnit);
            if (this.c == null) {
                return null;
            }
            throw this.c;
        }

        public boolean isCancelled() {
            return false;
        }

        public boolean isDone() {
            return this.b;
        }

        public void run() {
            try {
                a();
            } catch (Throwable th) {
                this.c = new ExecutionException(th);
            } finally {
                this.b = true;
                this.a.countDown();
            }
        }
    }

    private static final class c implements ApiRequestFutureListener<AccessToken> {
        private final c a;

        c(c cVar) {
            this.a = cVar;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public final void requestComplete(ApiRequestFuture<AccessToken> apiRequestFuture) {
            try {
                switch (apiRequestFuture.getStatus()) {
                    case SUCCESS:
                        this.a.a(apiRequestFuture.getResponseObject());
                        return;
                    case CANCELED:
                        this.a.a();
                        return;
                    case FAILED:
                        this.a.a(apiRequestFuture.getCause());
                        return;
                    default:
                        this.a.a(new LineSdkLoginException(LineSdkLoginError.UNKNOWN, "Unknown ApiReqeustFuture.status. status = " + apiRequestFuture.getStatus()));
                        return;
                }
            } catch (Throwable th) {
                this.a.a(th);
            }
            this.a.a(th);
        }
    }

    private static final class d implements ApiRequestFutureListener<Otp> {
        private final WeakReference<Activity> a;
        private final c b;

        d(Activity activity, c cVar) {
            this.a = new WeakReference<>(activity);
            this.b = cVar;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public final void requestComplete(ApiRequestFuture<Otp> apiRequestFuture) {
            try {
                switch (apiRequestFuture.getStatus()) {
                    case SUCCESS:
                        this.b.a(apiRequestFuture.getResponseObject());
                        b.a(this.b, this.a.get());
                        return;
                    case CANCELED:
                        this.b.a();
                        return;
                    case FAILED:
                        this.b.a(apiRequestFuture.getCause());
                        return;
                    default:
                        this.b.a(new LineSdkLoginException(LineSdkLoginError.UNKNOWN, "Unknown ApiReqeustFuture.status. status = " + apiRequestFuture.getStatus()));
                        return;
                }
            } catch (Throwable th) {
                this.b.a(th);
            }
            this.b.a(th);
        }
    }

    static class e extends C0023b {
        e() {
        }

        public final void a() {
            AccessToken b = C0023b.b();
            if (b != null) {
                try {
                    LineSdkContextManager.getSdkContext().getApiClient().logout(b.accessToken, null);
                } catch (Throwable th) {
                }
            }
        }
    }

    private static final class f implements Runnable {
        private final Activity a;
        private final c b;

        f(Activity activity, c cVar) {
            this.a = activity;
            this.b = cVar;
        }

        public final void run() {
            AccessToken b2;
            if (!this.b.isForceLoginByOtherAccount() && (b2 = jp.line.android.sdk.a.a.a().b()) != null) {
                this.b.a(b2);
            } else if (this.a != null || this.b.getProgress().flowNumber >= LineLoginFuture.ProgressOfLogin.GOT_REQUEST_TOKEN.flowNumber) {
                b.a(this.b, this.a);
            } else {
                this.b.a(new LineSdkLoginException(LineSdkLoginError.FAILED_START_LOGIN_ACTIVITY, "activity is null"));
            }
        }
    }

    private final LineLoginFuture a(Activity activity, Locale locale, boolean z) {
        c cVar;
        boolean z2 = true;
        synchronized (this) {
            if (this.a != null) {
                if (this.a.isSameRequest(z, locale)) {
                    switch (this.a.getProgress()) {
                        case STARTED_WEB_LOGIN:
                        case STARTED_A2A_LOGIN:
                            break;
                        case GOT_REQUEST_TOKEN:
                        case REQUESTED_ACCESS_TOKEN:
                        default:
                            z2 = false;
                            break;
                        case SUCCESS:
                        case CANCELED:
                        case FAILED:
                            this.a = null;
                            break;
                    }
                } else {
                    this.a = null;
                }
                if (this.a == null) {
                    this.a = new c(z, locale);
                    this.a.addProgressListener(this);
                    Boolean.valueOf(z);
                    c cVar2 = this.a;
                } else {
                    Boolean.valueOf(z);
                    c cVar3 = this.a;
                }
            } else {
                c b2 = d.a().b();
                if (b2 != null && b2.isSameRequest(z, locale)) {
                    switch (b2.getProgress()) {
                        case STARTED:
                        case REQUESTED_OTP:
                        case GOT_OTP:
                        case STARTED_WEB_LOGIN:
                        case GOT_REQUEST_TOKEN:
                        case REQUESTED_ACCESS_TOKEN:
                        case STARTED_A2A_LOGIN:
                            this.a = b2;
                            this.a.addProgressListener(this);
                            break;
                    }
                }
                if (this.a == null) {
                    this.a = new c(z, locale);
                    this.a.addProgressListener(this);
                    Boolean.valueOf(z);
                    c cVar4 = this.a;
                } else {
                    Boolean.valueOf(z);
                    c cVar5 = this.a;
                }
            }
            cVar = this.a;
        }
        if (z2) {
            this.b.execute(new f(activity, cVar));
        }
        return cVar;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0097 A[Catch:{ Throwable -> 0x00df, Throwable -> 0x002f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void a(jp.line.android.sdk.a.c.c r6, android.app.Activity r7) {
        /*
            r1 = 1
            r2 = 0
            int[] r0 = jp.line.android.sdk.a.c.b.AnonymousClass2.a     // Catch:{ Throwable -> 0x002f }
            jp.line.android.sdk.login.LineLoginFuture$ProgressOfLogin r3 = r6.getProgress()     // Catch:{ Throwable -> 0x002f }
            int r3 = r3.ordinal()     // Catch:{ Throwable -> 0x002f }
            r0 = r0[r3]     // Catch:{ Throwable -> 0x002f }
            switch(r0) {
                case 1: goto L_0x0012;
                case 2: goto L_0x0012;
                case 3: goto L_0x0034;
                case 4: goto L_0x0034;
                case 5: goto L_0x00f3;
                case 6: goto L_0x00f3;
                case 7: goto L_0x0034;
                default: goto L_0x0011;
            }     // Catch:{ Throwable -> 0x002f }
        L_0x0011:
            return
        L_0x0012:
            jp.line.android.sdk.login.LineLoginFuture$ProgressOfLogin r0 = jp.line.android.sdk.login.LineLoginFuture.ProgressOfLogin.REQUESTED_OTP     // Catch:{ Throwable -> 0x002f }
            r6.a(r0)     // Catch:{ Throwable -> 0x002f }
            jp.line.android.sdk.a.c.d r0 = jp.line.android.sdk.a.c.d.a()     // Catch:{ Throwable -> 0x002f }
            r0.a(r6)     // Catch:{ Throwable -> 0x002f }
            jp.line.android.sdk.LineSdkContext r0 = jp.line.android.sdk.LineSdkContextManager.getSdkContext()     // Catch:{ Throwable -> 0x002f }
            jp.line.android.sdk.api.ApiClient r0 = r0.getApiClient()     // Catch:{ Throwable -> 0x002f }
            jp.line.android.sdk.a.c.b$d r1 = new jp.line.android.sdk.a.c.b$d     // Catch:{ Throwable -> 0x002f }
            r1.<init>(r7, r6)     // Catch:{ Throwable -> 0x002f }
            r0.getOtp(r1)     // Catch:{ Throwable -> 0x002f }
            goto L_0x0011
        L_0x002f:
            r0 = move-exception
            r6.a(r0)
            goto L_0x0011
        L_0x0034:
            jp.line.android.sdk.a.c.d r0 = jp.line.android.sdk.a.c.d.a()     // Catch:{ Throwable -> 0x002f }
            r0.a(r6)     // Catch:{ Throwable -> 0x002f }
            if (r7 != 0) goto L_0x0048
            jp.line.android.sdk.exception.LineSdkLoginException r0 = new jp.line.android.sdk.exception.LineSdkLoginException     // Catch:{ Throwable -> 0x002f }
            jp.line.android.sdk.exception.LineSdkLoginError r1 = jp.line.android.sdk.exception.LineSdkLoginError.FAILED_START_LOGIN_ACTIVITY     // Catch:{ Throwable -> 0x002f }
            r0.<init>(r1)     // Catch:{ Throwable -> 0x002f }
            r6.a(r0)     // Catch:{ Throwable -> 0x002f }
            goto L_0x0011
        L_0x0048:
            boolean r0 = r6.isForceLoginByOtherAccount()     // Catch:{ Throwable -> 0x002f }
            if (r0 != 0) goto L_0x00e9
            jp.line.android.sdk.LineSdkContext r0 = jp.line.android.sdk.LineSdkContextManager.getSdkContext()     // Catch:{ Throwable -> 0x002f }
            android.content.Context r3 = r0.getApplicationContext()     // Catch:{ Throwable -> 0x002f }
            android.content.pm.PackageManager r0 = r3.getPackageManager()     // Catch:{ Throwable -> 0x002f }
            r4 = 128(0x80, float:1.794E-43)
            java.util.List r0 = r0.getInstalledApplications(r4)     // Catch:{ Throwable -> 0x002f }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ Throwable -> 0x002f }
        L_0x0064:
            boolean r0 = r4.hasNext()     // Catch:{ Throwable -> 0x002f }
            if (r0 == 0) goto L_0x00e5
            java.lang.Object r0 = r4.next()     // Catch:{ Throwable -> 0x002f }
            android.content.pm.ApplicationInfo r0 = (android.content.pm.ApplicationInfo) r0     // Catch:{ Throwable -> 0x002f }
            java.lang.String r5 = "jp.naver.line.android"
            java.lang.String r0 = r0.packageName     // Catch:{ Throwable -> 0x002f }
            boolean r0 = r5.equals(r0)     // Catch:{ Throwable -> 0x002f }
            if (r0 == 0) goto L_0x0064
            r0 = r1
        L_0x007b:
            if (r0 == 0) goto L_0x00e7
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Throwable -> 0x002f }
            java.lang.String r4 = "jp.naver.line.android.intent.action.APPAUTH"
            r5 = 0
            r0.<init>(r4, r5)     // Catch:{ Throwable -> 0x002f }
            android.content.pm.PackageManager r3 = r3.getPackageManager()     // Catch:{ Throwable -> 0x002f }
            r4 = 0
            java.util.List r0 = r3.queryIntentActivities(r0, r4)     // Catch:{ Throwable -> 0x002f }
            boolean r0 = r0.isEmpty()     // Catch:{ Throwable -> 0x002f }
            if (r0 != 0) goto L_0x00e7
            r0 = r1
        L_0x0095:
            if (r0 == 0) goto L_0x00e9
            jp.line.android.sdk.login.LineLoginFuture$ProgressOfLogin r0 = jp.line.android.sdk.login.LineLoginFuture.ProgressOfLogin.STARTED_A2A_LOGIN     // Catch:{ Throwable -> 0x002f }
            r6.a(r0)     // Catch:{ Throwable -> 0x002f }
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Throwable -> 0x002f }
            java.lang.String r1 = "jp.naver.line.android.intent.action.APPAUTH"
            r0.<init>(r1)     // Catch:{ Throwable -> 0x002f }
            java.lang.String r1 = "channelId"
            jp.line.android.sdk.LineSdkContext r2 = jp.line.android.sdk.LineSdkContextManager.getSdkContext()     // Catch:{ Throwable -> 0x002f }
            int r2 = r2.getChannelId()     // Catch:{ Throwable -> 0x002f }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Throwable -> 0x002f }
            r0.putExtra(r1, r2)     // Catch:{ Throwable -> 0x002f }
            java.lang.String r1 = "otpId"
            jp.line.android.sdk.model.Otp r2 = r6.getOtp()     // Catch:{ Throwable -> 0x002f }
            java.lang.String r2 = r2.id     // Catch:{ Throwable -> 0x002f }
            r0.putExtra(r1, r2)     // Catch:{ Throwable -> 0x002f }
            java.lang.String r1 = "appPackage"
            java.lang.String r2 = r7.getPackageName()     // Catch:{ Throwable -> 0x002f }
            r0.putExtra(r1, r2)     // Catch:{ Throwable -> 0x002f }
            java.lang.String r1 = "authScheme"
            jp.line.android.sdk.LineSdkContext r2 = jp.line.android.sdk.LineSdkContextManager.getSdkContext()     // Catch:{ Throwable -> 0x002f }
            java.lang.String r2 = r2.getAuthScheme()     // Catch:{ Throwable -> 0x002f }
            r0.putExtra(r1, r2)     // Catch:{ Throwable -> 0x002f }
            r1 = 65536(0x10000, float:9.18355E-41)
            r0.addFlags(r1)     // Catch:{ Throwable -> 0x002f }
            r7.startActivity(r0)     // Catch:{ Throwable -> 0x00df }
            goto L_0x0011
        L_0x00df:
            r0 = move-exception
            r6.a(r0)     // Catch:{ Throwable -> 0x002f }
            goto L_0x0011
        L_0x00e5:
            r0 = r2
            goto L_0x007b
        L_0x00e7:
            r0 = r2
            goto L_0x0095
        L_0x00e9:
            jp.line.android.sdk.login.LineLoginFuture$ProgressOfLogin r0 = jp.line.android.sdk.login.LineLoginFuture.ProgressOfLogin.STARTED_WEB_LOGIN     // Catch:{ Throwable -> 0x002f }
            r6.a(r0)     // Catch:{ Throwable -> 0x002f }
            jp.line.android.sdk.activity.WebLoginActivity.a(r7, r6)     // Catch:{ Throwable -> 0x002f }
            goto L_0x0011
        L_0x00f3:
            jp.line.android.sdk.login.LineLoginFuture$ProgressOfLogin r0 = jp.line.android.sdk.login.LineLoginFuture.ProgressOfLogin.REQUESTED_ACCESS_TOKEN     // Catch:{ Throwable -> 0x002f }
            r6.a(r0)     // Catch:{ Throwable -> 0x002f }
            jp.line.android.sdk.a.c.d r0 = jp.line.android.sdk.a.c.d.a()     // Catch:{ Throwable -> 0x002f }
            r0.a(r6)     // Catch:{ Throwable -> 0x002f }
            jp.line.android.sdk.LineSdkContext r0 = jp.line.android.sdk.LineSdkContextManager.getSdkContext()     // Catch:{ Throwable -> 0x002f }
            jp.line.android.sdk.api.ApiClient r0 = r0.getApiClient()     // Catch:{ Throwable -> 0x002f }
            jp.line.android.sdk.model.RequestToken r1 = r6.getRequestToken()     // Catch:{ Throwable -> 0x002f }
            java.lang.String r1 = r1.requestToken     // Catch:{ Throwable -> 0x002f }
            jp.line.android.sdk.model.Otp r2 = r6.getOtp()     // Catch:{ Throwable -> 0x002f }
            java.lang.String r2 = r2.password     // Catch:{ Throwable -> 0x002f }
            jp.line.android.sdk.a.c.b$c r3 = new jp.line.android.sdk.a.c.b$c     // Catch:{ Throwable -> 0x002f }
            r3.<init>(r6)     // Catch:{ Throwable -> 0x002f }
            r0.getAccessToken(r1, r2, r3)     // Catch:{ Throwable -> 0x002f }
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.line.android.sdk.a.c.b.a(jp.line.android.sdk.a.c.c, android.app.Activity):void");
    }

    public final void a(AccessToken accessToken) {
        ArrayList arrayList = null;
        synchronized (this) {
            if (this.c != null) {
                arrayList = new ArrayList(this.c);
            }
        }
        if (arrayList != null && arrayList.size() > 0) {
            this.b.execute(new a(arrayList, accessToken));
        }
    }

    public final boolean a() {
        return this.a != null;
    }

    public final boolean addOnAccessTokenChangedListener(OnAccessTokenChangedListener onAccessTokenChangedListener) {
        boolean add;
        synchronized (this) {
            if (this.c == null) {
                this.c = new ArrayList();
            }
            add = !this.c.contains(onAccessTokenChangedListener) ? this.c.add(onAccessTokenChangedListener) : false;
        }
        return add;
    }

    public final LineLoginFuture b() {
        boolean z = false;
        final c cVar = this.a;
        if (cVar == null) {
            synchronized (this) {
                if (this.a == null) {
                    this.a = d.a().b();
                    if (this.a != null) {
                        this.a.addProgressListener(this);
                        switch (this.a.getProgress()) {
                            case STARTED:
                            case REQUESTED_OTP:
                            case GOT_OTP:
                            case STARTED_WEB_LOGIN:
                            case GOT_REQUEST_TOKEN:
                            case REQUESTED_ACCESS_TOKEN:
                                z = true;
                                break;
                        }
                    }
                }
                cVar = this.a;
            }
        }
        if (z) {
            this.b.execute(new Runnable() {
                public final void run() {
                    b.a(cVar, null);
                }
            });
        }
        return cVar;
    }

    public final Future<?> clearLocalLoginInfo() {
        C0023b bVar = new C0023b();
        this.b.execute(bVar);
        return bVar;
    }

    public final AccessToken getAccessToken() {
        return jp.line.android.sdk.a.a.a().b();
    }

    public final LineLoginFuture login(Activity activity) {
        return a(activity, Locale.getDefault(), false);
    }

    public final LineLoginFuture login(Activity activity, Locale locale) {
        return a(activity, locale, false);
    }

    public final LineLoginFuture loginByAccount(Activity activity) {
        return a(activity, Locale.getDefault(), true);
    }

    public final LineLoginFuture loginByAccount(Activity activity, Locale locale) {
        return a(activity, locale, true);
    }

    public final Future<?> logout() {
        e eVar = new e();
        this.b.execute(eVar);
        return eVar;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void onUpdateProgress(LineLoginFuture lineLoginFuture) {
        boolean z = false;
        boolean z2 = true;
        c cVar = (c) lineLoginFuture;
        try {
            switch (cVar.getProgress()) {
                case GOT_OTP:
                    d.a().a(cVar);
                    z2 = false;
                    break;
                case STARTED_WEB_LOGIN:
                case REQUESTED_ACCESS_TOKEN:
                case STARTED_A2A_LOGIN:
                default:
                    z2 = false;
                    boolean z3 = z2;
                    z2 = false;
                    z = z3;
                    break;
                case GOT_REQUEST_TOKEN:
                    try {
                        d.a().a(cVar);
                        break;
                    } catch (Throwable th) {
                        break;
                    }
                case SUCCESS:
                    z2 = false;
                    z = true;
                    break;
                case CANCELED:
                case FAILED:
                    boolean z32 = z2;
                    z2 = false;
                    z = z32;
                    break;
            }
        } catch (Throwable th2) {
            z2 = false;
        }
        if (z2) {
            a(cVar, null);
        }
        if (z) {
            d.a();
            d.c();
        }
    }

    public final boolean removeOnAccessTokenChangedListener(OnAccessTokenChangedListener onAccessTokenChangedListener) {
        boolean remove;
        synchronized (this) {
            remove = (this.c == null || !this.c.contains(onAccessTokenChangedListener)) ? false : this.c.remove(onAccessTokenChangedListener);
        }
        return remove;
    }
}
