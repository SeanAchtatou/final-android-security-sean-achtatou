package jp.line.android.sdk.model;

public class GroupMember extends User {
    public final String statusMessage;

    public GroupMember(String str, String str2, String str3, String str4) {
        super(str, str2, str3);
        this.statusMessage = str4;
    }

    public String toString() {
        return "GroupMember [mid=" + this.mid + ", displayName=" + this.displayName + ", pictureUrl=" + this.pictureUrl + ", statusMessage=" + this.statusMessage + "]";
    }
}
