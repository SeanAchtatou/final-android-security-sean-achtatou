package jp.line.android.sdk.api;

public interface ApiRequestFutureListener<RO> {
    void requestComplete(ApiRequestFuture<RO> apiRequestFuture);
}
