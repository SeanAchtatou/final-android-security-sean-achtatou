package jp.line.android.sdk.a.c;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import jp.line.android.sdk.login.LineLoginFuture;
import jp.line.android.sdk.login.LineLoginFutureListener;
import jp.line.android.sdk.login.LineLoginFutureProgressListener;
import jp.line.android.sdk.model.AccessToken;
import jp.line.android.sdk.model.Otp;
import jp.line.android.sdk.model.RequestToken;

public final class c implements LineLoginFuture {
    private final long a;
    private LineLoginFuture.ProgressOfLogin b;
    private boolean c;
    private final boolean d;
    private final Locale e;
    private Otp f;
    private RequestToken g;
    private AccessToken h;
    private Throwable i;
    private final List<LineLoginFutureListener> j;
    private final List<LineLoginFutureProgressListener> k;
    private CountDownLatch l;
    private Executor m;

    protected c(long j2, LineLoginFuture.ProgressOfLogin progressOfLogin, Otp otp, RequestToken requestToken, boolean z, Locale locale) {
        this.b = LineLoginFuture.ProgressOfLogin.STARTED;
        this.j = new ArrayList();
        this.k = new ArrayList();
        this.a = j2;
        this.f = otp;
        this.g = requestToken;
        this.d = z;
        this.e = locale;
        if (progressOfLogin.flowNumber >= LineLoginFuture.ProgressOfLogin.GOT_OTP.flowNumber && (this.f == null || this.f.id == null || this.f.password == null)) {
            this.b = LineLoginFuture.ProgressOfLogin.STARTED;
        } else if (progressOfLogin.flowNumber >= LineLoginFuture.ProgressOfLogin.GOT_REQUEST_TOKEN.flowNumber && (this.g == null || this.g.requestToken == null)) {
            this.b = LineLoginFuture.ProgressOfLogin.STARTED;
        } else if (progressOfLogin.flowNumber < LineLoginFuture.ProgressOfLogin.SUCCESS.flowNumber || !(this.h == null || this.h.accessToken == null)) {
            this.b = progressOfLogin;
            this.c = true;
        } else {
            this.b = LineLoginFuture.ProgressOfLogin.STARTED;
        }
    }

    protected c(boolean z, Locale locale) {
        this.b = LineLoginFuture.ProgressOfLogin.STARTED;
        this.j = new ArrayList();
        this.k = new ArrayList();
        this.a = System.currentTimeMillis();
        this.d = z;
        this.e = locale;
    }

    private final void a(final List<LineLoginFutureListener> list) {
        if (list != null && list.size() != 0) {
            b().execute(new Runnable() {
                public final void run() {
                    for (LineLoginFutureListener a2 : list) {
                        c.this.a(a2);
                    }
                }
            });
        }
    }

    private final Executor b() {
        if (this.m == null) {
            synchronized (this) {
                if (this.m == null) {
                    this.m = Executors.newCachedThreadPool();
                }
            }
        }
        return this.m;
    }

    private final void b(final List<LineLoginFutureProgressListener> list) {
        if (list != null && list.size() != 0) {
            b().execute(new Runnable() {
                public final void run() {
                    for (LineLoginFutureProgressListener a2 : list) {
                        c.this.a(a2);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(LineLoginFutureListener lineLoginFutureListener) {
        try {
            lineLoginFutureListener.loginComplete(this);
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(LineLoginFutureProgressListener lineLoginFutureProgressListener) {
        try {
            lineLoginFutureProgressListener.onUpdateProgress(this);
        } catch (Throwable th) {
        }
    }

    public final boolean a() {
        boolean z;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (this.b.flowNumber < LineLoginFuture.ProgressOfLogin.CANCELED.flowNumber) {
            synchronized (this) {
                if (this.b.flowNumber < LineLoginFuture.ProgressOfLogin.CANCELED.flowNumber) {
                    this.b = LineLoginFuture.ProgressOfLogin.CANCELED;
                    z = true;
                    if (!this.j.isEmpty()) {
                        arrayList = new ArrayList(this.j);
                        this.j.clear();
                    } else {
                        arrayList = null;
                    }
                    if (!this.k.isEmpty()) {
                        arrayList2 = new ArrayList(this.k);
                        this.k.clear();
                    }
                } else {
                    z = false;
                    arrayList = null;
                }
            }
        } else {
            z = false;
            arrayList = null;
        }
        a(arrayList);
        b(arrayList2);
        return z;
    }

    public final boolean a(Throwable th) {
        boolean z;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (this.b.flowNumber < LineLoginFuture.ProgressOfLogin.FAILED.flowNumber) {
            synchronized (this) {
                if (this.b.flowNumber < LineLoginFuture.ProgressOfLogin.FAILED.flowNumber) {
                    this.b = LineLoginFuture.ProgressOfLogin.FAILED;
                    this.i = th;
                    z = true;
                    if (!this.j.isEmpty()) {
                        arrayList = new ArrayList(this.j);
                        this.j.clear();
                    } else {
                        arrayList = null;
                    }
                    if (!this.k.isEmpty()) {
                        arrayList2 = new ArrayList(this.k);
                        this.k.clear();
                    }
                } else {
                    z = false;
                    arrayList = null;
                }
            }
        } else {
            z = false;
            arrayList = null;
        }
        a(arrayList);
        b(arrayList2);
        return z;
    }

    public final boolean a(LineLoginFuture.ProgressOfLogin progressOfLogin) {
        boolean z = false;
        ArrayList arrayList = null;
        if (this.b.flowNumber < progressOfLogin.flowNumber) {
            synchronized (this) {
                if (this.b.flowNumber < progressOfLogin.flowNumber) {
                    this.b = progressOfLogin;
                    z = true;
                    if (!this.k.isEmpty()) {
                        arrayList = new ArrayList(this.k);
                    }
                }
            }
        }
        b(arrayList);
        return z;
    }

    public final boolean a(AccessToken accessToken) {
        boolean z;
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        if (this.b.flowNumber < LineLoginFuture.ProgressOfLogin.SUCCESS.flowNumber) {
            synchronized (this) {
                if (this.b.flowNumber < LineLoginFuture.ProgressOfLogin.SUCCESS.flowNumber) {
                    this.b = LineLoginFuture.ProgressOfLogin.SUCCESS;
                    this.h = accessToken;
                    z = true;
                    if (!this.j.isEmpty()) {
                        arrayList = new ArrayList(this.j);
                        this.j.clear();
                    } else {
                        arrayList = null;
                    }
                    if (!this.k.isEmpty()) {
                        arrayList2 = new ArrayList(this.k);
                        this.k.clear();
                    }
                } else {
                    z = false;
                    arrayList = null;
                }
            }
        } else {
            z = false;
            arrayList = null;
        }
        a(arrayList);
        b(arrayList2);
        return z;
    }

    public final boolean a(Otp otp) {
        boolean z = false;
        ArrayList arrayList = null;
        if (this.b.flowNumber < LineLoginFuture.ProgressOfLogin.GOT_OTP.flowNumber) {
            synchronized (this) {
                if (this.b.flowNumber < LineLoginFuture.ProgressOfLogin.GOT_OTP.flowNumber) {
                    this.b = LineLoginFuture.ProgressOfLogin.GOT_OTP;
                    this.f = otp;
                    z = true;
                    if (!this.k.isEmpty()) {
                        arrayList = new ArrayList(this.k);
                    }
                }
            }
        }
        b(arrayList);
        return z;
    }

    public final boolean a(RequestToken requestToken) {
        boolean z = false;
        ArrayList arrayList = null;
        if (this.b.flowNumber < LineLoginFuture.ProgressOfLogin.GOT_REQUEST_TOKEN.flowNumber) {
            synchronized (this) {
                if (this.b.flowNumber < LineLoginFuture.ProgressOfLogin.GOT_REQUEST_TOKEN.flowNumber) {
                    this.b = LineLoginFuture.ProgressOfLogin.GOT_REQUEST_TOKEN;
                    this.g = requestToken;
                    z = true;
                    if (!this.k.isEmpty()) {
                        arrayList = new ArrayList(this.k);
                    }
                }
            }
        }
        b(arrayList);
        return z;
    }

    public final boolean addFutureListener(LineLoginFutureListener lineLoginFutureListener) {
        boolean z = true;
        boolean z2 = false;
        synchronized (this) {
            if (!isProcessing()) {
                z = false;
                z2 = true;
            } else if (!this.j.contains(lineLoginFutureListener)) {
                this.j.add(lineLoginFutureListener);
            } else {
                z = false;
            }
        }
        if (z2) {
            a(lineLoginFutureListener);
        }
        return z;
    }

    public final boolean addProgressListener(LineLoginFutureProgressListener lineLoginFutureProgressListener) {
        boolean z;
        boolean z2 = true;
        synchronized (this) {
            if (!isProcessing() || this.k.contains(lineLoginFutureProgressListener)) {
                z = false;
            } else {
                this.k.add(lineLoginFutureProgressListener);
                z = true;
            }
            if (this.b.flowNumber <= LineLoginFuture.ProgressOfLogin.STARTED.flowNumber) {
                z2 = false;
            }
        }
        if (z2) {
            a(lineLoginFutureProgressListener);
        }
        return z;
    }

    public final void await() {
        if (!await(600000, TimeUnit.MILLISECONDS)) {
            a(new TimeoutException("LineLoginFuture.await() has timed out.(timeout=600000msec)"));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return r3.l.await(r4, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean await(long r4, java.util.concurrent.TimeUnit r6) {
        /*
            r3 = this;
            r0 = 1
            boolean r1 = r3.isProcessing()
            if (r1 != 0) goto L_0x0008
        L_0x0007:
            return r0
        L_0x0008:
            monitor-enter(r3)
            jp.line.android.sdk.login.LineLoginFuture$ProgressOfLogin r1 = r3.b     // Catch:{ all -> 0x0015 }
            int r1 = r1.flowNumber     // Catch:{ all -> 0x0015 }
            jp.line.android.sdk.login.LineLoginFuture$ProgressOfLogin r2 = jp.line.android.sdk.login.LineLoginFuture.ProgressOfLogin.SUCCESS     // Catch:{ all -> 0x0015 }
            int r2 = r2.flowNumber     // Catch:{ all -> 0x0015 }
            if (r1 < r2) goto L_0x0018
            monitor-exit(r3)     // Catch:{ all -> 0x0015 }
            goto L_0x0007
        L_0x0015:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x0018:
            java.util.concurrent.CountDownLatch r1 = r3.l     // Catch:{ all -> 0x0015 }
            if (r1 != 0) goto L_0x0024
            java.util.concurrent.CountDownLatch r1 = new java.util.concurrent.CountDownLatch     // Catch:{ all -> 0x0015 }
            r2 = 1
            r1.<init>(r2)     // Catch:{ all -> 0x0015 }
            r3.l = r1     // Catch:{ all -> 0x0015 }
        L_0x0024:
            monitor-exit(r3)     // Catch:{ all -> 0x0015 }
            java.util.concurrent.CountDownLatch r1 = r3.l     // Catch:{ InterruptedException -> 0x002c }
            boolean r0 = r1.await(r4, r6)     // Catch:{ InterruptedException -> 0x002c }
            goto L_0x0007
        L_0x002c:
            r1 = move-exception
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.line.android.sdk.a.c.c.await(long, java.util.concurrent.TimeUnit):boolean");
    }

    public final AccessToken getAccessToken() {
        return this.h;
    }

    public final Throwable getCause() {
        return this.i;
    }

    public final long getCreatedTime() {
        return this.a;
    }

    public final Locale getLocale() {
        return this.e;
    }

    public final Otp getOtp() {
        return this.f;
    }

    public final LineLoginFuture.ProgressOfLogin getProgress() {
        return this.b;
    }

    public final RequestToken getRequestToken() {
        return this.g;
    }

    public final boolean isForceLoginByOtherAccount() {
        return this.d;
    }

    public final boolean isProcessing() {
        switch (this.b) {
            case SUCCESS:
            case FAILED:
            case CANCELED:
                return false;
            default:
                return true;
        }
    }

    public final boolean isSameRequest(boolean z, Locale locale) {
        return this.d == z && this.e.equals(locale);
    }

    public final boolean removeFutureListener(LineLoginFutureListener lineLoginFutureListener) {
        boolean remove;
        synchronized (this) {
            remove = this.j.remove(lineLoginFutureListener);
        }
        return remove;
    }

    public final boolean removeProgressListener(LineLoginFutureProgressListener lineLoginFutureProgressListener) {
        boolean remove;
        synchronized (this) {
            remove = this.k.remove(lineLoginFutureProgressListener);
        }
        return remove;
    }

    public final String toString() {
        String property = System.getProperty("line.separator");
        StringBuilder sb = new StringBuilder();
        sb.append("LineLoginFuture : ").append(property).append("    progress=").append(this.b).append(property).append("    createdTime=").append(this.a).append(property).append("    otp=").append(this.f).append(property).append("    requestToken=").append(this.g).append(property).append("    accessToken=").append(this.h).append(property).append("    locale=").append(this.e).append(property);
        if (this.i != null) {
            StringWriter stringWriter = new StringWriter();
            this.i.printStackTrace(new PrintWriter(stringWriter));
            sb.append("    cause=").append(stringWriter.toString());
        }
        return sb.toString();
    }
}
