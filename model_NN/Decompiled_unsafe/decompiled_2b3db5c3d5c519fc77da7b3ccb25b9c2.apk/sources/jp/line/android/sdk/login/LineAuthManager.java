package jp.line.android.sdk.login;

import android.app.Activity;
import java.util.Locale;
import java.util.concurrent.Future;
import jp.line.android.sdk.model.AccessToken;

public interface LineAuthManager {
    boolean addOnAccessTokenChangedListener(OnAccessTokenChangedListener onAccessTokenChangedListener);

    Future<?> clearLocalLoginInfo();

    AccessToken getAccessToken();

    LineLoginFuture login(Activity activity);

    LineLoginFuture login(Activity activity, Locale locale);

    LineLoginFuture loginByAccount(Activity activity);

    LineLoginFuture loginByAccount(Activity activity, Locale locale);

    Future<?> logout();

    boolean removeOnAccessTokenChangedListener(OnAccessTokenChangedListener onAccessTokenChangedListener);
}
