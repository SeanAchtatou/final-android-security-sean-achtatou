package jp.line.android.sdk.a;

import android.app.Activity;
import android.content.Context;
import jp.line.android.sdk.LineSdkContext;
import jp.line.android.sdk.Phase;
import jp.line.android.sdk.api.ApiClient;
import jp.line.android.sdk.login.LineAuthManager;

public final class b implements LineSdkContext {
    private boolean a;
    private Context b;
    private int c;
    private String d;
    private Phase e;
    private int f;
    private String g;
    private String h;
    private Class<? extends Activity> i;
    private Class<? extends Activity> j;
    private String k;
    private ApiClient l;
    private LineAuthManager m;

    /* renamed from: jp.line.android.sdk.a.b$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[Phase.values().length];

        static {
            try {
                a[Phase.REAL.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
        }
    }

    private final void a() {
        if (!this.a) {
            synchronized (this) {
                if (!this.a) {
                    throw new RuntimeException("LineSdkConfig was not initialized.");
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:73:0x012c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x012d, code lost:
        new java.lang.RuntimeException(r1 + " is not found.", r0);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.content.Context r6, int r7, jp.line.android.sdk.Phase r8, jp.line.android.sdk.LineSdkContextInitializer r9) {
        /*
            r5 = this;
            boolean r0 = r5.a
            if (r0 != 0) goto L_0x0100
            monitor-enter(r5)
            boolean r0 = r5.a     // Catch:{ all -> 0x0036 }
            if (r0 != 0) goto L_0x00ff
            android.content.Context r0 = r6.getApplicationContext()     // Catch:{ NameNotFoundException -> 0x002f }
            r5.b = r0     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r0 = r6.getPackageName()     // Catch:{ NameNotFoundException -> 0x002f }
            android.content.pm.PackageManager r1 = r6.getPackageManager()     // Catch:{ NameNotFoundException -> 0x002f }
            r2 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r0 = r1.getApplicationInfo(r0, r2)     // Catch:{ NameNotFoundException -> 0x002f }
            android.os.Bundle r2 = r0.metaData     // Catch:{ NameNotFoundException -> 0x002f }
            if (r7 <= 0) goto L_0x0039
            r5.c = r7     // Catch:{ NameNotFoundException -> 0x002f }
        L_0x0023:
            int r0 = r5.c     // Catch:{ NameNotFoundException -> 0x002f }
            if (r0 >= 0) goto L_0x0043
            java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r1 = "Metadata of jp.line.sdk.ChannelId was not found from AndroidManifest.xml"
            r0.<init>(r1)     // Catch:{ NameNotFoundException -> 0x002f }
            throw r0     // Catch:{ NameNotFoundException -> 0x002f }
        L_0x002f:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x0036 }
            r1.<init>(r0)     // Catch:{ all -> 0x0036 }
            throw r1     // Catch:{ all -> 0x0036 }
        L_0x0036:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x0039:
            java.lang.String r0 = "jp.line.sdk.ChannelId"
            r1 = -1
            int r0 = r2.getInt(r0, r1)     // Catch:{ NameNotFoundException -> 0x002f }
            r5.c = r0     // Catch:{ NameNotFoundException -> 0x002f }
            goto L_0x0023
        L_0x0043:
            java.lang.String r0 = "jp.line.sdk.AuthScheme"
            java.lang.String r0 = r2.getString(r0)     // Catch:{ NameNotFoundException -> 0x002f }
            r5.d = r0     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r0 = r5.d     // Catch:{ NameNotFoundException -> 0x002f }
            if (r0 == 0) goto L_0x0057
            java.lang.String r0 = r5.d     // Catch:{ NameNotFoundException -> 0x002f }
            int r0 = r0.length()     // Catch:{ NameNotFoundException -> 0x002f }
            if (r0 > 0) goto L_0x005f
        L_0x0057:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r1 = "Metadata of jp.line.sdk.AuthScheme was not found from AndroidManifest.xml"
            r0.<init>(r1)     // Catch:{ NameNotFoundException -> 0x002f }
            throw r0     // Catch:{ NameNotFoundException -> 0x002f }
        L_0x005f:
            if (r8 == 0) goto L_0x0101
            r5.e = r8     // Catch:{ NameNotFoundException -> 0x002f }
        L_0x0063:
            int[] r0 = jp.line.android.sdk.a.b.AnonymousClass1.a     // Catch:{ NameNotFoundException -> 0x002f }
            jp.line.android.sdk.Phase r0 = r5.e     // Catch:{ NameNotFoundException -> 0x002f }
            r0.ordinal()     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r0 = "https://access.line.me"
            r5.g = r0     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r0 = "https://api.line.me"
            r5.h = r0     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r0 = "jp.line.sdk.ActivityClassThatFiresWhenProcessWasKilled"
            java.lang.String r0 = r2.getString(r0)     // Catch:{ NameNotFoundException -> 0x002f }
            if (r0 == 0) goto L_0x00a4
            int r1 = r0.length()     // Catch:{ NameNotFoundException -> 0x002f }
            if (r1 <= 0) goto L_0x00a4
            java.lang.String r1 = "."
            boolean r1 = r0.startsWith(r1)     // Catch:{ NameNotFoundException -> 0x002f }
            if (r1 == 0) goto L_0x016b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x002f }
            r1.<init>()     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r3 = r6.getPackageName()     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r0 = r0.toString()     // Catch:{ NameNotFoundException -> 0x002f }
            r1 = r0
        L_0x009e:
            java.lang.Class r0 = java.lang.Class.forName(r1)     // Catch:{ ClassNotFoundException -> 0x012c }
            r5.i = r0     // Catch:{ ClassNotFoundException -> 0x012c }
        L_0x00a4:
            java.lang.String r0 = "jp.line.sdk.WebLoginActivityClass"
            java.lang.String r0 = r2.getString(r0)     // Catch:{ NameNotFoundException -> 0x002f }
            if (r0 == 0) goto L_0x0162
            int r1 = r0.length()     // Catch:{ NameNotFoundException -> 0x002f }
            if (r1 <= 0) goto L_0x0162
            java.lang.String r1 = "."
            boolean r1 = r0.startsWith(r1)     // Catch:{ NameNotFoundException -> 0x002f }
            if (r1 == 0) goto L_0x0168
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x002f }
            r1.<init>()     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r2 = r6.getPackageName()     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r0 = r0.toString()     // Catch:{ NameNotFoundException -> 0x002f }
            r1 = r0
        L_0x00d0:
            java.lang.Class r0 = java.lang.Class.forName(r1)     // Catch:{ ClassNotFoundException -> 0x0147 }
            r5.j = r0     // Catch:{ ClassNotFoundException -> 0x0147 }
        L_0x00d6:
            jp.line.android.sdk.api.ApiClient r0 = r9.createApiClient(r5)     // Catch:{ NameNotFoundException -> 0x002f }
            r5.l = r0     // Catch:{ NameNotFoundException -> 0x002f }
            jp.line.android.sdk.login.LineAuthManager r0 = r9.createAuthManager(r5)     // Catch:{ NameNotFoundException -> 0x002f }
            r5.m = r0     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r0 = "3.1.21"
            r5.k = r0     // Catch:{ NameNotFoundException -> 0x002f }
            int r0 = r5.c     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.Integer.valueOf(r0)     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r0 = r5.d     // Catch:{ NameNotFoundException -> 0x002f }
            jp.line.android.sdk.Phase r0 = r5.e     // Catch:{ NameNotFoundException -> 0x002f }
            int r0 = r5.f     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.Integer.valueOf(r0)     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r0 = r5.g     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r0 = r5.h     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.Class<? extends android.app.Activity> r0 = r5.i     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r0 = r5.k     // Catch:{ NameNotFoundException -> 0x002f }
            r0 = 1
            r5.a = r0     // Catch:{ all -> 0x0036 }
        L_0x00ff:
            monitor-exit(r5)     // Catch:{ all -> 0x0036 }
        L_0x0100:
            return
        L_0x0101:
            java.lang.String r0 = "jp.line.sdk.Phase"
            java.lang.String r0 = r2.getString(r0)     // Catch:{ NameNotFoundException -> 0x002f }
            if (r0 == 0) goto L_0x0115
            java.lang.String r1 = "beta"
            boolean r1 = r1.equalsIgnoreCase(r0)     // Catch:{ NameNotFoundException -> 0x002f }
            if (r1 == 0) goto L_0x011f
            jp.line.android.sdk.Phase r0 = jp.line.android.sdk.Phase.BETA     // Catch:{ NameNotFoundException -> 0x002f }
            r5.e = r0     // Catch:{ NameNotFoundException -> 0x002f }
        L_0x0115:
            jp.line.android.sdk.Phase r0 = r5.e     // Catch:{ NameNotFoundException -> 0x002f }
            if (r0 != 0) goto L_0x0063
            jp.line.android.sdk.Phase r0 = jp.line.android.sdk.Phase.REAL     // Catch:{ NameNotFoundException -> 0x002f }
            r5.e = r0     // Catch:{ NameNotFoundException -> 0x002f }
            goto L_0x0063
        L_0x011f:
            java.lang.String r1 = "rc"
            boolean r0 = r1.equalsIgnoreCase(r0)     // Catch:{ NameNotFoundException -> 0x002f }
            if (r0 == 0) goto L_0x0115
            jp.line.android.sdk.Phase r0 = jp.line.android.sdk.Phase.RC     // Catch:{ NameNotFoundException -> 0x002f }
            r5.e = r0     // Catch:{ NameNotFoundException -> 0x002f }
            goto L_0x0115
        L_0x012c:
            r0 = move-exception
            java.lang.RuntimeException r3 = new java.lang.RuntimeException     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x002f }
            r4.<init>()     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r4 = " is not found."
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r1 = r1.toString()     // Catch:{ NameNotFoundException -> 0x002f }
            r3.<init>(r1, r0)     // Catch:{ NameNotFoundException -> 0x002f }
            goto L_0x00a4
        L_0x0147:
            r0 = move-exception
            java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x002f }
            r3.<init>()     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r3 = " is not found."
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r1 = r1.toString()     // Catch:{ NameNotFoundException -> 0x002f }
            r2.<init>(r1, r0)     // Catch:{ NameNotFoundException -> 0x002f }
            goto L_0x00d6
        L_0x0162:
            java.lang.Class<jp.line.android.sdk.activity.WebLoginActivity> r0 = jp.line.android.sdk.activity.WebLoginActivity.class
            r5.j = r0     // Catch:{ NameNotFoundException -> 0x002f }
            goto L_0x00d6
        L_0x0168:
            r1 = r0
            goto L_0x00d0
        L_0x016b:
            r1 = r0
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: jp.line.android.sdk.a.b.a(android.content.Context, int, jp.line.android.sdk.Phase, jp.line.android.sdk.LineSdkContextInitializer):void");
    }

    public final Class<? extends Activity> getActivityClassThatFiresWhenProcessWasKilled() {
        a();
        return this.i;
    }

    public final ApiClient getApiClient() {
        a();
        return this.l;
    }

    public final Context getApplicationContext() {
        a();
        return this.b;
    }

    public final LineAuthManager getAuthManager() {
        a();
        return this.m;
    }

    public final String getAuthScheme() {
        a();
        return this.d;
    }

    public final String getChannelApiServerHost() {
        a();
        return this.h;
    }

    public final int getChannelId() {
        a();
        return this.c;
    }

    public final String getChannelServerHost() {
        a();
        return this.g;
    }

    public final Phase getPhase() {
        a();
        return this.e;
    }

    public final String getSdkVersion() {
        a();
        return this.k;
    }

    public final Class<? extends Activity> getWebLoginActivityClass() {
        a();
        return this.j;
    }
}
