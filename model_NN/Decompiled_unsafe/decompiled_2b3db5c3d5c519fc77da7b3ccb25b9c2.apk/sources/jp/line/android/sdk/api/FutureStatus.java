package jp.line.android.sdk.api;

public enum FutureStatus {
    PROCESSING,
    SUCCESS,
    FAILED,
    CANCELED
}
