package jp.line.android.sdk.a.a.a;

import com.facebook.internal.ServerProtocol;
import com.tencent.android.tpush.common.MessageKey;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import javax.net.ssl.SSLSocketFactory;
import jp.line.android.sdk.a.a.c;
import jp.line.android.sdk.a.a.d;
import jp.line.android.sdk.exception.LineSdkApiError;
import jp.line.android.sdk.exception.LineSdkApiException;
import jp.line.android.sdk.model.Group;
import jp.line.android.sdk.model.Groups;
import org.apache.http.client.methods.HttpGet;
import org.json.JSONArray;
import org.json.JSONObject;

public final class e extends a<Groups> {
    protected e(SSLSocketFactory sSLSocketFactory) {
        super(true, sSLSocketFactory);
    }

    /* access modifiers changed from: protected */
    public final void a(HttpURLConnection httpURLConnection, c cVar, d<Groups> dVar) throws Exception {
        httpURLConnection.setRequestMethod(HttpGet.METHOD_NAME);
        httpURLConnection.setDoOutput(false);
        b(httpURLConnection);
    }

    /* access modifiers changed from: protected */
    public final String b(c cVar) {
        StringBuilder sb = new StringBuilder();
        sb.append(p.a(cVar));
        sb.append("?start=").append(cVar.c()).append("&display=").append(cVar.d());
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object c(HttpURLConnection httpURLConnection) throws Exception {
        if (httpURLConnection.getResponseCode() == 200) {
            JSONObject a = l.a(httpURLConnection);
            int optInt = a.optInt("count");
            int optInt2 = a.optInt(ServerProtocol.DIALOG_PARAM_DISPLAY);
            int optInt3 = a.optInt(MessageKey.MSG_ACCEPT_TIME_START);
            int optInt4 = a.optInt("total");
            ArrayList arrayList = new ArrayList();
            JSONArray optJSONArray = a.optJSONArray("groups");
            if (optJSONArray != null && optJSONArray.length() > 0) {
                int length = optJSONArray.length();
                for (int i = 0; i < length; i++) {
                    JSONObject jSONObject = optJSONArray.getJSONObject(i);
                    arrayList.add(new Group(jSONObject.optString("mid"), jSONObject.optString("name"), jSONObject.optString("pictureUrl")));
                }
            }
            return new Groups(optInt, optInt3, optInt2, optInt4, arrayList);
        }
        throw new LineSdkApiException(LineSdkApiError.SERVER_ERROR, httpURLConnection.getResponseCode(), a(httpURLConnection));
    }
}
