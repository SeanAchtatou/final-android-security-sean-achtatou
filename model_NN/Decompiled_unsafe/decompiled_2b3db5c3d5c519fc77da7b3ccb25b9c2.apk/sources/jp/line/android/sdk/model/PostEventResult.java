package jp.line.android.sdk.model;

import java.util.Arrays;

public class PostEventResult {
    public final String[] failedMids;
    public final String messageId;
    public final long timestamp;
    public final int version;

    public PostEventResult(int i, long j, String str, String... strArr) {
        this.version = i;
        this.timestamp = j;
        this.messageId = str;
        this.failedMids = strArr;
    }

    public String toString() {
        return "PostEventResult [version=" + this.version + ", timestamp=" + this.timestamp + ", messageId=" + this.messageId + ", failedMids=" + Arrays.toString(this.failedMids) + "]";
    }
}
