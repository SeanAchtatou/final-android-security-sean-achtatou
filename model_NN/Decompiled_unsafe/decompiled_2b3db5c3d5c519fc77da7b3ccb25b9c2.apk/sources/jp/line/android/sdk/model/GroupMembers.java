package jp.line.android.sdk.model;

import java.util.List;

public class GroupMembers {
    public final int count;
    public final int display;
    public final List<GroupMember> groupMemberList;
    public final int start;
    public final int total;

    public GroupMembers(int i, int i2, int i3, int i4, List<GroupMember> list) {
        this.count = i;
        this.total = i4;
        this.start = i2;
        this.display = i3;
        this.groupMemberList = list;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("GroupMembers [count=").append(this.count).append(", total=").append(this.total).append(", start=").append(this.start).append(", display=").append(this.display);
        sb.append(", groupMemberList=");
        if (this.groupMemberList == null || this.groupMemberList.size() <= 0) {
            sb.append("null or empty");
        } else {
            for (GroupMember append : this.groupMemberList) {
                sb.append(append).append(",").append(System.getProperty("line.separator"));
            }
            sb.delete(sb.length() - 2, sb.length());
        }
        return sb.toString();
    }
}
