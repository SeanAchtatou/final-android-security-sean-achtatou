package jp.line.android.sdk.exception;

public class LineSdkLoginException extends Exception {
    private static final long serialVersionUID = 1;
    public final LineSdkLoginError error;
    public final int errorCode;

    public LineSdkLoginException(LineSdkLoginError lineSdkLoginError) {
        this(lineSdkLoginError, -1, null, null);
    }

    public LineSdkLoginException(LineSdkLoginError lineSdkLoginError, int i) {
        this(lineSdkLoginError, i, null, null);
    }

    public LineSdkLoginException(LineSdkLoginError lineSdkLoginError, int i, String str, Throwable th) {
        super(createMessage(str, lineSdkLoginError, i), th);
        this.error = lineSdkLoginError;
        this.errorCode = i;
    }

    public LineSdkLoginException(LineSdkLoginError lineSdkLoginError, String str) {
        this(lineSdkLoginError, -1, str, null);
    }

    public LineSdkLoginException(LineSdkLoginError lineSdkLoginError, Throwable th) {
        this(lineSdkLoginError, -1, null, th);
    }

    private static final String createMessage(String str, LineSdkLoginError lineSdkLoginError, int i) {
        StringBuilder sb = new StringBuilder();
        if (str != null) {
            sb.append(str);
        }
        sb.append(" error=").append(lineSdkLoginError);
        if (i >= 0) {
            sb.append(", errorCode=").append(i);
        }
        return sb.toString();
    }
}
