package jp.line.android.sdk.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import com.bluepay.data.Config;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;
import jp.line.android.sdk.LineSdkContextManager;
import jp.line.android.sdk.a.c.c;
import jp.line.android.sdk.login.LineLoginFuture;
import jp.line.android.sdk.model.RequestToken;
import org.apache.http.protocol.HTTP;

public class WebLoginActivity extends Activity {
    private long a;
    private Locale b;
    private WebView c;

    final class a extends WebViewClient {
        a() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:55:0x00f9, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x00ff, code lost:
            throw r0;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean shouldOverrideUrlLoading(android.webkit.WebView r13, java.lang.String r14) {
            /*
                r12 = this;
                r2 = 0
                r0 = 1
                java.lang.String r1 = "market://"
                boolean r1 = r14.startsWith(r1)
                if (r1 == 0) goto L_0x0035
                android.content.Intent r1 = new android.content.Intent
                java.lang.String r2 = "android.intent.action.VIEW"
                r1.<init>(r2)
                android.net.Uri r2 = android.net.Uri.parse(r14)
                r1.setData(r2)
                r2 = 268435456(0x10000000, float:2.5243549E-29)
                r1.addFlags(r2)
                jp.line.android.sdk.activity.WebLoginActivity r2 = jp.line.android.sdk.activity.WebLoginActivity.this
                android.content.pm.PackageManager r2 = r2.getPackageManager()
                r3 = 65536(0x10000, float:9.18355E-41)
                java.util.List r2 = r2.queryIntentActivities(r1, r3)
                boolean r2 = r2.isEmpty()
                if (r2 != 0) goto L_0x0034
                jp.line.android.sdk.activity.WebLoginActivity r2 = jp.line.android.sdk.activity.WebLoginActivity.this
                r2.startActivity(r1)
            L_0x0034:
                return r0
            L_0x0035:
                java.lang.String r1 = "lineconnect://"
                boolean r1 = r14.startsWith(r1)
                if (r1 == 0) goto L_0x012a
                r6 = 0
                r7 = 0
                r1 = -1
                android.net.Uri r3 = android.net.Uri.parse(r14)     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                java.lang.String r4 = r3.getHost()     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                java.lang.String r5 = "success"
                boolean r5 = r4.equalsIgnoreCase(r5)     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                if (r5 == 0) goto L_0x008c
                java.lang.String r4 = "requestToken"
                java.lang.String r8 = r3.getQueryParameter(r4)     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                java.lang.String r4 = "refreshToken"
                java.lang.String r9 = r3.getQueryParameter(r4)     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                r4 = -1
                java.lang.String r10 = "expire"
                java.lang.String r3 = r3.getQueryParameter(r10)     // Catch:{ Exception -> 0x012d }
                long r4 = java.lang.Long.parseLong(r3)     // Catch:{ Exception -> 0x012d }
            L_0x0068:
                if (r8 == 0) goto L_0x0070
                int r3 = r8.length()     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                if (r3 != 0) goto L_0x0087
            L_0x0070:
                java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                java.lang.String r4 = "Illegal login result. Request Token is null."
                r3.<init>(r4)     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                r11 = r3
                r3 = r2
                r2 = r11
            L_0x007a:
                if (r3 == 0) goto L_0x00be
                jp.line.android.sdk.activity.WebLoginActivity r1 = jp.line.android.sdk.activity.WebLoginActivity.this     // Catch:{ all -> 0x00c6 }
                jp.line.android.sdk.activity.WebLoginActivity.a(r3)     // Catch:{ all -> 0x00c6 }
            L_0x0081:
                jp.line.android.sdk.activity.WebLoginActivity r1 = jp.line.android.sdk.activity.WebLoginActivity.this
                r1.finish()
                goto L_0x0034
            L_0x0087:
                jp.line.android.sdk.model.RequestToken r3 = jp.line.android.sdk.model.RequestToken.createFromWebLogin(r8, r4, r9)     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                goto L_0x007a
            L_0x008c:
                java.lang.String r5 = "fail"
                boolean r4 = r4.equalsIgnoreCase(r5)     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                if (r4 == 0) goto L_0x00b3
                java.lang.String r4 = "errorCode"
                java.lang.String r3 = r3.getQueryParameter(r4)     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                if (r3 == 0) goto L_0x00a8
                int r4 = r3.length()     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                if (r4 <= 0) goto L_0x00a8
                int r1 = java.lang.Integer.parseInt(r3)     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                r3 = r2
                goto L_0x007a
            L_0x00a8:
                java.lang.NullPointerException r3 = new java.lang.NullPointerException     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                java.lang.String r4 = "error code is null."
                r3.<init>(r4)     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                r11 = r3
                r3 = r2
                r2 = r11
                goto L_0x007a
            L_0x00b3:
                java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                java.lang.String r4 = "Illegal login result."
                r3.<init>(r4)     // Catch:{ Throwable -> 0x00e4, all -> 0x0100 }
                r11 = r3
                r3 = r2
                r2 = r11
                goto L_0x007a
            L_0x00be:
                if (r2 == 0) goto L_0x00cd
                jp.line.android.sdk.activity.WebLoginActivity r1 = jp.line.android.sdk.activity.WebLoginActivity.this     // Catch:{ all -> 0x00c6 }
                r1.a(r2)     // Catch:{ all -> 0x00c6 }
                goto L_0x0081
            L_0x00c6:
                r0 = move-exception
                jp.line.android.sdk.activity.WebLoginActivity r1 = jp.line.android.sdk.activity.WebLoginActivity.this
                r1.finish()
                throw r0
            L_0x00cd:
                r2 = 417(0x1a1, float:5.84E-43)
                if (r1 != r2) goto L_0x00d7
                jp.line.android.sdk.activity.WebLoginActivity r1 = jp.line.android.sdk.activity.WebLoginActivity.this     // Catch:{ all -> 0x00c6 }
                r1.a()     // Catch:{ all -> 0x00c6 }
                goto L_0x0081
            L_0x00d7:
                jp.line.android.sdk.activity.WebLoginActivity r2 = jp.line.android.sdk.activity.WebLoginActivity.this     // Catch:{ all -> 0x00c6 }
                jp.line.android.sdk.exception.LineSdkLoginException r3 = new jp.line.android.sdk.exception.LineSdkLoginException     // Catch:{ all -> 0x00c6 }
                jp.line.android.sdk.exception.LineSdkLoginError r4 = jp.line.android.sdk.exception.LineSdkLoginError.FAILED_WEB_LOGIN     // Catch:{ all -> 0x00c6 }
                r3.<init>(r4, r1)     // Catch:{ all -> 0x00c6 }
                r2.a(r3)     // Catch:{ all -> 0x00c6 }
                goto L_0x0081
            L_0x00e4:
                r1 = move-exception
                if (r2 == 0) goto L_0x00f3
                jp.line.android.sdk.activity.WebLoginActivity r1 = jp.line.android.sdk.activity.WebLoginActivity.this     // Catch:{ all -> 0x00f9 }
                jp.line.android.sdk.activity.WebLoginActivity.a(r6)     // Catch:{ all -> 0x00f9 }
            L_0x00ec:
                jp.line.android.sdk.activity.WebLoginActivity r1 = jp.line.android.sdk.activity.WebLoginActivity.this
                r1.finish()
                goto L_0x0034
            L_0x00f3:
                jp.line.android.sdk.activity.WebLoginActivity r2 = jp.line.android.sdk.activity.WebLoginActivity.this     // Catch:{ all -> 0x00f9 }
                r2.a(r1)     // Catch:{ all -> 0x00f9 }
                goto L_0x00ec
            L_0x00f9:
                r0 = move-exception
                jp.line.android.sdk.activity.WebLoginActivity r1 = jp.line.android.sdk.activity.WebLoginActivity.this
                r1.finish()
                throw r0
            L_0x0100:
                r0 = move-exception
                if (r2 == 0) goto L_0x010e
                jp.line.android.sdk.activity.WebLoginActivity r1 = jp.line.android.sdk.activity.WebLoginActivity.this     // Catch:{ all -> 0x0116 }
                jp.line.android.sdk.activity.WebLoginActivity.a(r6)     // Catch:{ all -> 0x0116 }
            L_0x0108:
                jp.line.android.sdk.activity.WebLoginActivity r1 = jp.line.android.sdk.activity.WebLoginActivity.this
                r1.finish()
                throw r0
            L_0x010e:
                if (r2 == 0) goto L_0x011d
                jp.line.android.sdk.activity.WebLoginActivity r1 = jp.line.android.sdk.activity.WebLoginActivity.this     // Catch:{ all -> 0x0116 }
                r1.a(r7)     // Catch:{ all -> 0x0116 }
                goto L_0x0108
            L_0x0116:
                r0 = move-exception
                jp.line.android.sdk.activity.WebLoginActivity r1 = jp.line.android.sdk.activity.WebLoginActivity.this
                r1.finish()
                throw r0
            L_0x011d:
                jp.line.android.sdk.activity.WebLoginActivity r2 = jp.line.android.sdk.activity.WebLoginActivity.this     // Catch:{ all -> 0x0116 }
                jp.line.android.sdk.exception.LineSdkLoginException r3 = new jp.line.android.sdk.exception.LineSdkLoginException     // Catch:{ all -> 0x0116 }
                jp.line.android.sdk.exception.LineSdkLoginError r4 = jp.line.android.sdk.exception.LineSdkLoginError.FAILED_WEB_LOGIN     // Catch:{ all -> 0x0116 }
                r3.<init>(r4, r1)     // Catch:{ all -> 0x0116 }
                r2.a(r3)     // Catch:{ all -> 0x0116 }
                goto L_0x0108
            L_0x012a:
                r0 = 0
                goto L_0x0034
            L_0x012d:
                r3 = move-exception
                goto L_0x0068
            */
            throw new UnsupportedOperationException("Method not decompiled: jp.line.android.sdk.activity.WebLoginActivity.a.shouldOverrideUrlLoading(android.webkit.WebView, java.lang.String):boolean");
        }
    }

    private static String a(String str) {
        if (str == null) {
            return "";
        }
        try {
            return URLEncoder.encode(str, HTTP.UTF_8);
        } catch (UnsupportedEncodingException e) {
            return str;
        }
    }

    public static final void a(Activity activity, LineLoginFuture lineLoginFuture) {
        String str = null;
        if (activity != null && lineLoginFuture != null) {
            Intent intent = new Intent(activity, LineSdkContextManager.getSdkContext().getWebLoginActivityClass());
            intent.putExtra("futureKey", lineLoginFuture.getCreatedTime());
            Locale locale = lineLoginFuture.getLocale();
            intent.putExtra("langKey", locale != null ? locale.getLanguage() : null);
            if (locale != null) {
                str = locale.getCountry();
            }
            intent.putExtra("countryKey", str);
            activity.startActivity(intent);
        }
    }

    static void a(RequestToken requestToken) {
        c a2 = a.a();
        if (a2 != null) {
            a2.a(requestToken);
        }
    }

    private final c b() {
        c a2 = a.a();
        if (a2 != null) {
            if (a2.getCreatedTime() == this.a) {
                return a2;
            }
            Long.valueOf(this.a);
            Long.valueOf(a2.getCreatedTime());
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        c b2 = b();
        if (b2 != null) {
            b2.a();
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Throwable th) {
        c b2 = b();
        if (b2 != null) {
            b2.a(th);
        }
    }

    public void onBackPressed() {
        a();
        finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.c = new WebView(this);
        this.c.setScrollBarStyle(0);
        this.c.setWebViewClient(new a());
        this.c.getSettings().setJavaScriptEnabled(true);
        this.c.getSettings().setUseWideViewPort(true);
        this.c.getSettings().setSupportZoom(true);
        LinearLayout linearLayout = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        if (Build.VERSION.SDK_INT >= 14) {
            linearLayout.setFitsSystemWindows(true);
        }
        linearLayout.addView(this.c, layoutParams);
        setContentView(linearLayout, layoutParams);
        Intent intent = getIntent();
        if (intent != null) {
            this.a = intent.getLongExtra("futureKey", -1);
            String stringExtra = intent.getStringExtra("langKey");
            String stringExtra2 = intent.getStringExtra("countryKey");
            if (stringExtra == null) {
                this.b = getResources().getConfiguration().locale;
            } else if (stringExtra2 != null) {
                this.b = new Locale(stringExtra, stringExtra2);
            } else {
                this.b = new Locale(stringExtra);
            }
        } else {
            this.b = getResources().getConfiguration().locale;
            this.a = -1;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        String sb;
        super.onResume();
        c b2 = b();
        if (this.a <= 0 || b2 == null) {
            finish();
            return;
        }
        String country = this.b.getCountry();
        int i = getResources().getConfiguration().mcc;
        String str = b2.getOtp().id;
        int channelId = LineSdkContextManager.getSdkContext().getChannelId();
        StringBuilder sb2 = new StringBuilder();
        StringBuilder append = sb2.append(LineSdkContextManager.getSdkContext().getChannelServerHost()).append("/dialog/oauth/login?channelId=").append(channelId).append("&otpId=").append(a(str)).append("&lang=");
        Locale locale = this.b;
        if (locale == null) {
            sb = "";
        } else {
            String language = locale.getLanguage();
            if (language == null) {
                sb = "";
            } else {
                String str2 = null;
                if (Config.LAN_ID3.equals(language)) {
                    language = "id";
                }
                if ("zh".equals(language)) {
                    String country2 = locale.getCountry();
                    str2 = (country2 == null || Locale.SIMPLIFIED_CHINESE.getCountry().equals(country2)) ? "Hans" : "Hant";
                }
                StringBuilder sb3 = new StringBuilder(10);
                sb3.append(language);
                if (str2 != null) {
                    sb3.append('-').append(str2);
                }
                sb = sb3.toString();
            }
        }
        append.append(sb).append("&country=").append(country).append("&mcc=").append(i);
        this.c.loadUrl(sb2.toString());
    }
}
