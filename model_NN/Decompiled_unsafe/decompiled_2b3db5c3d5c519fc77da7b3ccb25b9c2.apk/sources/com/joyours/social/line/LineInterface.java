package com.joyours.social.line;

import android.util.Log;
import com.joyours.base.framework.Cocos2dxApp;
import org.cocos2dx.lib.Cocos2dxLuaJavaBridge;

public class LineInterface {
    public static String SIG = LineInterface.class.getSimpleName();
    protected static int accessTokenRefreshCallback = -1;
    protected static int loginCallback = -1;

    public interface AccessTokenRefreshCallback {
        void call(String str);
    }

    public interface LoginCallback {
        void call(String str);
    }

    public static void setLoginCallback(int loginCallback2) {
        if (loginCallback != -1) {
            Log.d(SIG, "Release lua function loginCallback :" + loginCallback);
            Cocos2dxLuaJavaBridge.releaseLuaFunction(loginCallback);
            loginCallback = -1;
        }
        loginCallback = loginCallback2;
    }

    public static void setAccessTokenRefreshCallback(int accessTokenRefreshCallback2) {
        if (accessTokenRefreshCallback != -1) {
            Log.d(SIG, "Release lua function accessTokenRefreshCallback :" + accessTokenRefreshCallback);
            Cocos2dxLuaJavaBridge.releaseLuaFunction(accessTokenRefreshCallback);
            accessTokenRefreshCallback = -1;
        }
        accessTokenRefreshCallback = accessTokenRefreshCallback2;
    }

    public static void login() {
        final LineBean lineBean = Cocos2dxApp.getContext().getLineBean();
        if (lineBean != null) {
            Cocos2dxApp.getContext().getMainHandler().postDelayed(new Runnable() {
                public void run() {
                    lineBean.login(new LoginCallback() {
                        public void call(final String accessToken) {
                            Log.d(LineInterface.SIG, "LoginCallback," + Thread.currentThread().getId());
                            if (Cocos2dxApp.getContext() != null) {
                                Cocos2dxApp.getContext().runOnResume(new Runnable() {
                                    public void run() {
                                        Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                            public void run() {
                                                Log.d(LineInterface.SIG, "Call lua function loginCallback " + LineInterface.loginCallback + ", accessToken=" + accessToken);
                                                Cocos2dxLuaJavaBridge.callLuaFunctionWithString(LineInterface.loginCallback, accessToken);
                                            }
                                        }, 48);
                                    }
                                });
                            }
                        }
                    }, new AccessTokenRefreshCallback() {
                        public void call(final String accessToken) {
                            Log.d(LineInterface.SIG, "AccessTokenRefreshCallback," + Thread.currentThread().getId());
                            if (Cocos2dxApp.getContext() != null) {
                                Cocos2dxApp.getContext().runOnResume(new Runnable() {
                                    public void run() {
                                        Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                            public void run() {
                                                Log.d(LineInterface.SIG, "Call lua function accessTokenRefreshCallback " + LineInterface.accessTokenRefreshCallback + ", accessToken=" + accessToken);
                                                Cocos2dxLuaJavaBridge.callLuaFunctionWithString(LineInterface.accessTokenRefreshCallback, accessToken);
                                            }
                                        }, 48);
                                    }
                                });
                            }
                        }
                    });
                }
            }, 48);
        }
    }
}
