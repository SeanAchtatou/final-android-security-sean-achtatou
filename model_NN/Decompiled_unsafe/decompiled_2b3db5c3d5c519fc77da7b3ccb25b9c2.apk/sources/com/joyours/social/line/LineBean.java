package com.joyours.social.line;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.joyours.base.framework.Cocos2dxApp;
import com.joyours.base.framework.IBean;
import com.joyours.social.line.LineInterface;
import jp.line.android.sdk.LineSdkContextManager;
import jp.line.android.sdk.exception.LineSdkLoginError;
import jp.line.android.sdk.exception.LineSdkLoginException;
import jp.line.android.sdk.login.LineLoginFuture;
import jp.line.android.sdk.login.LineLoginFutureListener;
import jp.line.android.sdk.login.OnAccessTokenChangedListener;
import jp.line.android.sdk.model.AccessToken;

public class LineBean implements IBean {
    public void onCreate(Activity activity, Bundle bundle) {
        LineSdkContextManager.initialize(activity);
    }

    public void onRestoreInstanceState(Activity activity, Bundle bundle) {
    }

    public void onStart(Activity activity) {
    }

    public void onRestart(Activity activity) {
    }

    public void onSaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onPause(Activity activity) {
    }

    public void onResume(Activity activity) {
    }

    public void onStop(Activity activity) {
    }

    public void onDestroy(Activity activity) {
    }

    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    }

    public void login(final LineInterface.LoginCallback loginCallback, final LineInterface.AccessTokenRefreshCallback accessTokenRefreshCallback) {
        LineSdkContextManager.getSdkContext().getAuthManager().login(Cocos2dxApp.getContext()).addFutureListener(new LineLoginFutureListener() {
            public void loginComplete(LineLoginFuture future) {
                switch (AnonymousClass2.$SwitchMap$jp$line$android$sdk$login$LineLoginFuture$ProgressOfLogin[future.getProgress().ordinal()]) {
                    case 1:
                        LineSdkContextManager.getSdkContext().getAuthManager().addOnAccessTokenChangedListener(new OnAccessTokenChangedListener() {
                            public void onAccessTokenChanged(AccessToken newAccessToken) {
                                accessTokenRefreshCallback.call(newAccessToken.accessToken);
                            }
                        });
                        loginCallback.call(future.getAccessToken().accessToken);
                        return;
                    case 2:
                        loginCallback.call("cancel");
                        return;
                    default:
                        Throwable cause = future.getCause();
                        if (cause instanceof LineSdkLoginException) {
                            LineSdkLoginException loginException = (LineSdkLoginException) cause;
                            LineSdkLoginError error = loginException.error;
                            loginCallback.call(loginException.getMessage());
                            switch (AnonymousClass2.$SwitchMap$jp$line$android$sdk$exception$LineSdkLoginError[error.ordinal()]) {
                                case 1:
                                case 2:
                                case 3:
                                default:
                                    return;
                            }
                        } else {
                            loginCallback.call("error");
                            return;
                        }
                }
            }
        });
    }

    /* renamed from: com.joyours.social.line.LineBean$2  reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$jp$line$android$sdk$exception$LineSdkLoginError = new int[LineSdkLoginError.values().length];
        static final /* synthetic */ int[] $SwitchMap$jp$line$android$sdk$login$LineLoginFuture$ProgressOfLogin = new int[LineLoginFuture.ProgressOfLogin.values().length];

        static {
            try {
                $SwitchMap$jp$line$android$sdk$login$LineLoginFuture$ProgressOfLogin[LineLoginFuture.ProgressOfLogin.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$jp$line$android$sdk$login$LineLoginFuture$ProgressOfLogin[LineLoginFuture.ProgressOfLogin.CANCELED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$jp$line$android$sdk$exception$LineSdkLoginError[LineSdkLoginError.FAILED_START_LOGIN_ACTIVITY.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$jp$line$android$sdk$exception$LineSdkLoginError[LineSdkLoginError.FAILED_A2A_LOGIN.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$jp$line$android$sdk$exception$LineSdkLoginError[LineSdkLoginError.FAILED_WEB_LOGIN.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$jp$line$android$sdk$exception$LineSdkLoginError[LineSdkLoginError.UNKNOWN.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
        }
    }
}
