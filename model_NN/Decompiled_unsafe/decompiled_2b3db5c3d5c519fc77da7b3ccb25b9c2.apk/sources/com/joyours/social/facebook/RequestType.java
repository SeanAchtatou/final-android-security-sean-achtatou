package com.joyours.social.facebook;

public class RequestType {
    public static String GET_REQUEST_ID = "GET_REQUEST_ID";
    public static String GET_UNAUTHORIZED_FRIENDS = "GET_UNAUTHORIZED_FRIENDS";
    public static String LOGIN = "LOGIN";
    public static String NONE = "NONE";
    public static String SEND_INVITE = "SEND_INVITE";
    public static String SHARE_FEED = "SHARE_FEED";

    private RequestType() {
    }
}
