package com.joyours.social.facebook;

import android.util.Log;
import com.joyours.base.framework.Cocos2dxApp;
import org.cocos2dx.lib.Cocos2dxLuaJavaBridge;

public class FacebookInterface {
    public static String SIG = FacebookInterface.class.getSimpleName();
    protected static int getRequestIdCallback = -1;
    protected static int getUnauthorizedFriendsCallback = -1;
    protected static int loginCallback = -1;
    protected static int sendInvitesCallback = -1;
    protected static int shareFeedCallback = -1;

    public interface GetRequestIdCallback {
        void call(String str);
    }

    public interface GetUnauthorizedFriendsCallback {
        void call(String str);
    }

    public interface LoginCallback {
        void call(String str);
    }

    public interface SendInvitesCallback {
        void call(String str);
    }

    public interface ShareFeedCallback {
        void call(String str);
    }

    public static void setLoginCallback(int loginCallback2) {
        if (loginCallback != -1) {
            Log.d(SIG, "Release lua function loginCallback :" + loginCallback);
            Cocos2dxLuaJavaBridge.releaseLuaFunction(loginCallback);
            loginCallback = -1;
        }
        loginCallback = loginCallback2;
    }

    public static void setSendInvitesCallback(int sendInvitesCallback2) {
        if (sendInvitesCallback != -1) {
            Log.d(SIG, "Release lua function sendInvitesCallback :" + sendInvitesCallback);
            Cocos2dxLuaJavaBridge.releaseLuaFunction(sendInvitesCallback);
            sendInvitesCallback = -1;
        }
        sendInvitesCallback = sendInvitesCallback2;
    }

    public static void setGetUnauthorizedFriendsCallback(int getUnauthorizedFriendsCallback2) {
        if (getUnauthorizedFriendsCallback != -1) {
            Log.d(SIG, "Release lua function getUnauthorizedFriendsCallback :" + getUnauthorizedFriendsCallback);
            Cocos2dxLuaJavaBridge.releaseLuaFunction(getUnauthorizedFriendsCallback);
            getUnauthorizedFriendsCallback = -1;
        }
        getUnauthorizedFriendsCallback = getUnauthorizedFriendsCallback2;
    }

    public static void setShareFeedCallback(int shareFeedCallback2) {
        if (shareFeedCallback != -1) {
            Log.d(SIG, "Release lua function shareFeedCallback :" + shareFeedCallback);
            Cocos2dxLuaJavaBridge.releaseLuaFunction(shareFeedCallback);
            shareFeedCallback = -1;
        }
        shareFeedCallback = shareFeedCallback2;
    }

    public static void setGetRequestIdCallback(int getRequestIdCallback2) {
        if (getRequestIdCallback != -1) {
            Log.d(SIG, "Release lua function getRequestIdCallback :" + getRequestIdCallback);
            Cocos2dxLuaJavaBridge.releaseLuaFunction(getRequestIdCallback);
            getRequestIdCallback = -1;
        }
        getRequestIdCallback = getRequestIdCallback2;
    }

    public static void login() {
        final FacebookBean facebookBean = Cocos2dxApp.getContext().getFacebookBean();
        if (facebookBean != null) {
            Cocos2dxApp.getContext().getMainHandler().postDelayed(new Runnable() {
                public void run() {
                    facebookBean.login(new LoginCallback() {
                        public void call(final String accessToken) {
                            Log.d(FacebookInterface.SIG, "LoginCallback," + Thread.currentThread().getId());
                            if (Cocos2dxApp.getContext() != null) {
                                Cocos2dxApp.getContext().runOnResume(new Runnable() {
                                    public void run() {
                                        Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                            public void run() {
                                                Log.d(FacebookInterface.SIG, "Call lua function loginCallback " + FacebookInterface.loginCallback + ", accessToken=" + accessToken);
                                                Cocos2dxLuaJavaBridge.callLuaFunctionWithString(FacebookInterface.loginCallback, accessToken);
                                            }
                                        }, 48);
                                    }
                                });
                            }
                        }
                    });
                }
            }, 48);
        }
    }

    public static void logout() {
        final FacebookBean facebookBean = Cocos2dxApp.getContext().getFacebookBean();
        if (facebookBean != null) {
            Cocos2dxApp.getContext().getMainHandler().postDelayed(new Runnable() {
                public void run() {
                    Log.d(FacebookInterface.SIG, "Facebook logout begin.");
                    facebookBean.logout();
                    Log.d(FacebookInterface.SIG, "Facebook logout end.");
                }
            }, 48);
        }
    }

    public static void getUnauthorizedFriends(final String limit) {
        final FacebookBean facebookBean = Cocos2dxApp.getContext().getFacebookBean();
        if (facebookBean != null) {
            Cocos2dxApp.getContext().getMainHandler().postDelayed(new Runnable() {
                public void run() {
                    facebookBean.getUnauthorizedFriends(limit, new GetUnauthorizedFriendsCallback() {
                        public void call(final String result) {
                            if (Cocos2dxApp.getContext() != null) {
                                Cocos2dxApp.getContext().runOnResume(new Runnable() {
                                    public void run() {
                                        Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                            public void run() {
                                                Log.d(FacebookInterface.SIG, "Call lua function getUnauthorizedFriendsCallback " + FacebookInterface.getUnauthorizedFriendsCallback + ",result=" + result);
                                                Cocos2dxLuaJavaBridge.callLuaFunctionWithString(FacebookInterface.getUnauthorizedFriendsCallback, result);
                                            }
                                        }, 48);
                                    }
                                });
                            }
                        }
                    });
                }
            }, 48);
        }
    }

    public static void sendInvites(String data, String toIds, String title, String message) {
        final FacebookBean facebookBean = Cocos2dxApp.getContext().getFacebookBean();
        if (facebookBean != null) {
            final String str = data;
            final String str2 = toIds;
            final String str3 = title;
            final String str4 = message;
            Cocos2dxApp.getContext().getMainHandler().postDelayed(new Runnable() {
                public void run() {
                    facebookBean.sendInvites(str, str2, str3, str4, new SendInvitesCallback() {
                        public void call(final String result) {
                            Log.d(FacebookInterface.SIG, "SendInvitesCallback " + Thread.currentThread().getId());
                            if (Cocos2dxApp.getContext() != null) {
                                Cocos2dxApp.getContext().runOnResume(new Runnable() {
                                    public void run() {
                                        Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                            public void run() {
                                                Log.d(FacebookInterface.SIG, "Call lua function sendInvitesCallback " + FacebookInterface.sendInvitesCallback + ",result=" + result);
                                                Cocos2dxLuaJavaBridge.callLuaFunctionWithString(FacebookInterface.sendInvitesCallback, result);
                                            }
                                        }, 48);
                                    }
                                });
                            }
                        }
                    });
                }
            }, 48);
        }
    }

    public static void shareFeed(final String feedParams) {
        final FacebookBean facebookBean = Cocos2dxApp.getContext().getFacebookBean();
        if (facebookBean != null) {
            Cocos2dxApp.getContext().getMainHandler().postDelayed(new Runnable() {
                public void run() {
                    facebookBean.shareFeed(feedParams, new ShareFeedCallback() {
                        public void call(final String result) {
                            Log.d(FacebookInterface.SIG, "ShareFeedCallback " + Thread.currentThread().getId());
                            if (Cocos2dxApp.getContext() != null) {
                                Cocos2dxApp.getContext().runOnResume(new Runnable() {
                                    public void run() {
                                        Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                            public void run() {
                                                Log.d(FacebookInterface.SIG, "Call lua function shareFeedResultCallback " + FacebookInterface.shareFeedCallback + ",result=" + result);
                                                Cocos2dxLuaJavaBridge.callLuaFunctionWithString(FacebookInterface.shareFeedCallback, result);
                                            }
                                        }, 48);
                                    }
                                });
                            }
                        }
                    });
                }
            }, 48);
        }
    }

    public static void getRequestId() {
        final FacebookBean facebookBean = Cocos2dxApp.getContext().getFacebookBean();
        if (facebookBean != null) {
            Cocos2dxApp.getContext().getMainHandler().postDelayed(new Runnable() {
                public void run() {
                    facebookBean.getRequestId(new GetRequestIdCallback() {
                        public void call(final String result) {
                            Log.d(FacebookInterface.SIG, "GetRequestIdCallback " + Thread.currentThread().getId());
                            if (Cocos2dxApp.getContext() != null) {
                                Cocos2dxApp.getContext().runOnResume(new Runnable() {
                                    public void run() {
                                        Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                            public void run() {
                                                Cocos2dxLuaJavaBridge.callLuaFunctionWithString(FacebookInterface.getRequestIdCallback, result);
                                            }
                                        }, 50);
                                    }
                                });
                            }
                        }
                    });
                }
            }, 48);
        }
    }

    public static void deleteRequestId(final String requestId) {
        final FacebookBean facebookBean = Cocos2dxApp.getContext().getFacebookBean();
        if (facebookBean != null) {
            Cocos2dxApp.getContext().getMainHandler().postDelayed(new Runnable() {
                public void run() {
                    Log.d(FacebookInterface.SIG, "facebookBean deleteRequestId begin");
                    facebookBean.deleteRequestId(requestId);
                }
            }, 50);
        }
    }
}
