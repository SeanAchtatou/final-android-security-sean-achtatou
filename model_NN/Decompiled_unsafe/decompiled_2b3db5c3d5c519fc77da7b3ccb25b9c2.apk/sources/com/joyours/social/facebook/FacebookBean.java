package com.joyours.social.facebook;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookRequestError;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.internal.ShareConstants;
import com.facebook.share.model.GameRequestContent;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.GameRequestDialog;
import com.facebook.share.widget.ShareDialog;
import com.joyours.base.framework.Cocos2dxApp;
import com.joyours.base.framework.IBean;
import com.joyours.social.facebook.FacebookInterface;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;

public class FacebookBean implements IBean {
    protected static final String REQUEST_TO_APPLY_KEY = "com.joyours.social.facebook.FacebookBean.requestType";
    public static String SIG = FacebookBean.class.getSimpleName();
    private CallbackManager callbackManager;
    protected String feedParams;
    protected FacebookInterface.GetRequestIdCallback getRequestIdCallback;
    protected FacebookInterface.GetUnauthorizedFriendsCallback getUnauthorizedFriendsCallback;
    protected String inviteData;
    protected String inviteMessage;
    protected String inviteTitle;
    protected String inviteToIds;
    protected FacebookInterface.LoginCallback loginCallback;
    private LoginManager loginManager;
    protected String requestType = RequestType.NONE;
    protected FacebookInterface.SendInvitesCallback sendInvitesCallback;
    protected FacebookInterface.ShareFeedCallback shareFeedCallback;

    public void onCreate(Activity activity, Bundle bundle) {
        FacebookSdk.sdkInitialize(activity);
        AppEventsLogger.activateApp(activity.getApplication());
        this.callbackManager = CallbackManager.Factory.create();
        this.loginManager = LoginManager.getInstance();
        if (bundle != null) {
            try {
                this.requestType = bundle.getString(REQUEST_TO_APPLY_KEY);
            } catch (Exception e) {
                Log.e(SIG, e.getMessage());
            }
        }
    }

    public void onResume(Activity activity) {
        AppEventsLogger.activateApp(activity.getApplication());
    }

    public void onSaveInstanceState(Activity activity, Bundle bundle) {
        if (bundle != null && this.requestType != null) {
            bundle.putString(REQUEST_TO_APPLY_KEY, this.requestType);
        }
    }

    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        if (resultCode != 0) {
            this.callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    }

    public void onPause(Activity activity) {
        AppEventsLogger.deactivateApp(activity);
    }

    public void onDestroy(Activity activity) {
    }

    public void onRestoreInstanceState(Activity activity, Bundle bundle) {
    }

    public void onStart(Activity activity) {
    }

    public void onRestart(Activity activity) {
    }

    public void onStop(Activity activity) {
    }

    /* access modifiers changed from: protected */
    public void request(boolean isPermissionGranted) {
        String currentRequestType = this.requestType;
        this.requestType = RequestType.NONE;
        if (isPermissionGranted) {
            if (currentRequestType == RequestType.LOGIN) {
                processLogin();
            } else if (currentRequestType == RequestType.GET_UNAUTHORIZED_FRIENDS) {
                processGetUnauthorizedFriends("200");
            } else if (currentRequestType == RequestType.GET_REQUEST_ID) {
                processGetRequestId();
            } else if (currentRequestType == RequestType.SEND_INVITE) {
                processSendInvite();
            } else if (currentRequestType == RequestType.SHARE_FEED) {
                processShareFeed();
            }
        } else if (currentRequestType == RequestType.LOGIN) {
            processLoginFail();
        } else if (currentRequestType == RequestType.GET_UNAUTHORIZED_FRIENDS) {
            processGetUnauthorizedFriendsFail();
        } else if (currentRequestType == RequestType.GET_REQUEST_ID) {
            processGetRequestIdFail();
        } else if (currentRequestType == RequestType.SEND_INVITE) {
            processSendInviteFail();
        } else if (currentRequestType == RequestType.SHARE_FEED) {
            processShareFeedFail();
        }
    }

    /* access modifiers changed from: protected */
    public void processLogin() {
        Log.d(SIG, "processLogin");
        if (this.loginCallback != null) {
        }
        this.loginCallback = null;
    }

    /* access modifiers changed from: protected */
    public void processLoginFail() {
        if (this.loginCallback != null) {
            this.loginCallback.call("fail");
        }
        this.loginCallback = null;
    }

    /* access modifiers changed from: protected */
    public void processGetUnauthorizedFriends(String limit) {
        if (limit == "" || limit == null) {
            limit = "200";
        }
        Bundle bundle = new Bundle();
        bundle.putString("limit", limit);
        new GraphRequest(AccessToken.getCurrentAccessToken(), "me/invitable_friends", bundle, HttpMethod.GET, new GraphRequest.Callback() {
            public void onCompleted(GraphResponse response) {
                JSONObject data;
                FacebookRequestError error = response.getError();
                if (error != null) {
                    Log.d(FacebookBean.SIG, error.toString());
                    FacebookBean.this.processGetUnauthorizedFriendsFail();
                    return;
                }
                try {
                    JSONArray dataArray = new JSONObject(response.getRawResponse()).getJSONArray(ShareConstants.WEB_DIALOG_PARAM_DATA);
                    int len = dataArray.length();
                    JSONArray resulteArray = new JSONArray();
                    for (int i = 0; i < len; i++) {
                        JSONObject item = dataArray.getJSONObject(i);
                        JSONObject picture = item.getJSONObject("picture");
                        JSONObject json = new JSONObject();
                        json.put("id", item.optString("id"));
                        json.put("name", item.optString("name"));
                        if (!(picture == null || (data = picture.optJSONObject(ShareConstants.WEB_DIALOG_PARAM_DATA)) == null)) {
                            json.put("url", data.optString("url"));
                        }
                        resulteArray.put(json);
                    }
                    if (FacebookBean.this.getUnauthorizedFriendsCallback != null) {
                        FacebookBean.this.getUnauthorizedFriendsCallback.call(resulteArray.toString());
                    }
                } catch (Exception e) {
                    Log.e(FacebookBean.SIG, e.getMessage(), e);
                    FacebookBean.this.processGetUnauthorizedFriendsFail();
                }
            }
        }).executeAsync();
    }

    /* access modifiers changed from: protected */
    public void processGetUnauthorizedFriendsFail() {
        if (this.getUnauthorizedFriendsCallback != null) {
            this.getUnauthorizedFriendsCallback.call("fail");
        }
    }

    /* access modifiers changed from: protected */
    public void processSendInvite() {
        Bundle params = new Bundle();
        params.putString(ShareConstants.WEB_DIALOG_PARAM_MESSAGE, this.inviteMessage);
        params.putString("title", this.inviteTitle);
        params.putString("to", this.inviteToIds);
        params.putString(ShareConstants.WEB_DIALOG_PARAM_DATA, this.inviteData);
        Log.d(SIG, ">>>>>inviteToIds = " + this.inviteToIds);
        GameRequestDialog requestDialog = new GameRequestDialog(Cocos2dxApp.getContext());
        requestDialog.registerCallback(this.callbackManager, new FacebookCallback<GameRequestDialog.Result>() {
            public void onSuccess(GameRequestDialog.Result result) {
                Log.d(FacebookBean.SIG, ">>>>>>>>>>>>>>>>>>>>>>invite Success");
                String requestId = result.getRequestId();
                if (requestId != null) {
                    StringBuilder stringBuilder = new StringBuilder();
                    for (String key : result.getRequestRecipients()) {
                        if (stringBuilder.length() > 0) {
                            stringBuilder.append(",");
                        }
                        stringBuilder.append(key);
                    }
                    JSONObject json = new JSONObject();
                    try {
                        json.put("requestId", requestId);
                        json.put("toIds", stringBuilder.toString());
                    } catch (Exception e) {
                        Log.e(FacebookBean.SIG, e.getMessage(), e);
                    }
                    if (FacebookBean.this.sendInvitesCallback != null) {
                        Log.d(FacebookBean.SIG, "result = " + json.toString());
                        FacebookBean.this.sendInvitesCallback.call(json.toString());
                    }
                }
            }

            public void onCancel() {
                Log.d(FacebookBean.SIG, ">>>>>>>>>>>>>>>>>>>>>>invite onCancel");
                FacebookBean.this.processSendInviteFail();
            }

            public void onError(FacebookException error) {
                Log.d(FacebookBean.SIG, ">>>>>>>>>>>>>>>>>>>>>>invite onError = " + error.getMessage());
                FacebookBean.this.processSendInviteFail();
            }
        });
        requestDialog.show(new GameRequestContent.Builder().setMessage(this.inviteMessage).setTitle(this.inviteTitle).setData(this.inviteData).setTo(this.inviteToIds).build());
    }

    /* access modifiers changed from: protected */
    public void processSendInviteFail() {
        if (this.sendInvitesCallback != null) {
            this.sendInvitesCallback.call("fail");
        }
    }

    /* access modifiers changed from: protected */
    public void processShareFeed() {
        try {
            JSONObject json = new JSONObject(this.feedParams);
            Bundle params = new Bundle();
            params.putString("name", json.optString("name"));
            params.putString(ShareConstants.FEED_CAPTION_PARAM, json.optString(ShareConstants.FEED_CAPTION_PARAM));
            params.putString(ShareConstants.WEB_DIALOG_PARAM_MESSAGE, json.optString(ShareConstants.WEB_DIALOG_PARAM_MESSAGE));
            params.putString("link", json.optString("link"));
            params.putString("picture", json.optString("picture"));
            ShareLinkContent content = ((ShareLinkContent.Builder) new ShareLinkContent.Builder().setContentUrl(Uri.parse(json.optString("link")))).setContentTitle(json.optString("name")).setImageUrl(Uri.parse(json.optString("picture"))).setContentDescription(json.optString(ShareConstants.WEB_DIALOG_PARAM_MESSAGE)).build();
            if (ShareDialog.canShow((Class<? extends ShareContent>) ShareLinkContent.class)) {
                ShareDialog shareDialog = new ShareDialog(Cocos2dxApp.getContext());
                shareDialog.registerCallback(this.callbackManager, new FacebookCallback<Sharer.Result>() {
                    public void onSuccess(Sharer.Result result) {
                        if (FacebookBean.this.shareFeedCallback != null) {
                            FacebookBean.this.shareFeedCallback.call(GraphResponse.SUCCESS_KEY);
                        }
                    }

                    public void onError(FacebookException error) {
                        FacebookBean.this.processShareFeedFail();
                    }

                    public void onCancel() {
                        FacebookBean.this.processShareFeedFail();
                    }
                });
                shareDialog.show(content);
            }
        } catch (Exception e) {
            Log.e(SIG, e.getMessage(), e);
        }
    }

    /* access modifiers changed from: protected */
    public void processShareFeedFail() {
        if (this.shareFeedCallback != null) {
            this.shareFeedCallback.call("failed");
        }
    }

    /* access modifiers changed from: protected */
    public void processGetRequestId() {
        GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(), "me/apprequests", new GraphRequest.Callback() {
            public void onCompleted(GraphResponse response) {
                FacebookRequestError error = response.getError();
                if (error != null) {
                    Log.e(FacebookBean.SIG, "Get apprequests error:" + error.toString());
                    FacebookBean.this.processGetRequestIdFail();
                    return;
                }
                String rawStr = response.getRawResponse();
                Log.d(FacebookBean.SIG, "Get apprequests ret=" + rawStr);
                try {
                    JSONArray dataArr = new JSONObject(rawStr).getJSONArray(ShareConstants.WEB_DIALOG_PARAM_DATA);
                    JSONObject ret = new JSONObject();
                    if (dataArr != null && dataArr.length() > 0) {
                        JSONObject json = dataArr.getJSONObject(0);
                        String requestId = json.optString("id");
                        String requestData = json.optString(ShareConstants.WEB_DIALOG_PARAM_DATA);
                        ret.putOpt("requestId", requestId);
                        ret.putOpt("requestData", requestData);
                    }
                    if (FacebookBean.this.getRequestIdCallback != null) {
                        FacebookBean.this.getRequestIdCallback.call(ret.toString());
                    }
                } catch (Exception e) {
                    Log.e(FacebookBean.SIG, e.getMessage(), e);
                    FacebookBean.this.processGetRequestIdFail();
                }
            }
        }).executeAsync();
    }

    /* access modifiers changed from: protected */
    public void processGetRequestIdFail() {
        if (this.getRequestIdCallback != null) {
            this.getRequestIdCallback.call("fail");
        }
    }

    public void deleteRequestId(String requestId) {
        GraphRequest.newDeleteObjectRequest(AccessToken.getCurrentAccessToken(), requestId, new GraphRequest.Callback() {
            public void onCompleted(GraphResponse response) {
                FacebookRequestError error = response.getError();
                if (error != null) {
                    Log.e(FacebookBean.SIG, "deleteRequestId error=" + error.toString());
                    return;
                }
                Log.d(FacebookBean.SIG, "deleteRequestId ret=" + response.getRawResponse());
            }
        }).executeAsync();
    }

    public void login(final FacebookInterface.LoginCallback loginCallback2) {
        FacebookSdk.sdkInitialize(Cocos2dxApp.getContext());
        this.requestType = RequestType.LOGIN;
        try {
            if (AccessToken.getCurrentAccessToken() != null) {
                loginCallback2.call(AccessToken.getCurrentAccessToken().getToken());
                return;
            }
            this.loginManager.registerCallback(this.callbackManager, new FacebookCallback<LoginResult>() {
                public void onSuccess(LoginResult loginResult) {
                    AccessToken.setCurrentAccessToken(loginResult.getAccessToken());
                    Log.d(FacebookBean.SIG, ">>>>>>>>>>>>>>>>>>>>>>>>>>>login success");
                    loginCallback2.call(AccessToken.getCurrentAccessToken().getToken());
                }

                public void onCancel() {
                    Log.d(FacebookBean.SIG, ">>>>>>>>>>>>>>>>>Facebook Login cancel");
                    loginCallback2.call("cancel");
                }

                public void onError(FacebookException e) {
                    Log.d(FacebookBean.SIG, ">>>>>>>>>>>>>>>>>Facebook Login error");
                    Log.e(FacebookBean.SIG, e.getMessage(), e);
                    loginCallback2.call("fail");
                }
            });
            this.loginManager.logInWithReadPermissions(Cocos2dxApp.getContext(), Arrays.asList("public_profile", "user_friends", "email"));
        } catch (FacebookException e) {
            Log.e(SIG, e.getMessage(), e);
        }
    }

    public void logout() {
        if (AccessToken.getCurrentAccessToken() != null) {
            this.loginManager.logOut();
        } else {
            this.requestType = RequestType.NONE;
        }
    }

    public void getUnauthorizedFriends(String limit, FacebookInterface.GetUnauthorizedFriendsCallback getUnauthorizedFriendsCallback2) {
        this.requestType = RequestType.GET_UNAUTHORIZED_FRIENDS;
        this.getUnauthorizedFriendsCallback = getUnauthorizedFriendsCallback2;
        processGetUnauthorizedFriends(limit);
    }

    public void sendInvites(String data, String toIds, String title, String message, FacebookInterface.SendInvitesCallback sendInvitesCallback2) {
        this.sendInvitesCallback = sendInvitesCallback2;
        this.requestType = RequestType.SEND_INVITE;
        this.inviteMessage = message;
        this.inviteToIds = toIds;
        this.inviteTitle = title;
        this.inviteData = data;
        processSendInvite();
    }

    public void shareFeed(String feedParams2, FacebookInterface.ShareFeedCallback shareFeedCallback2) {
        this.shareFeedCallback = shareFeedCallback2;
        this.requestType = RequestType.SHARE_FEED;
        this.feedParams = feedParams2;
        processShareFeed();
    }

    public void getRequestId(FacebookInterface.GetRequestIdCallback getRequestIdCallback2) {
        this.getRequestIdCallback = getRequestIdCallback2;
        this.requestType = RequestType.GET_REQUEST_ID;
        processGetRequestId();
    }
}
