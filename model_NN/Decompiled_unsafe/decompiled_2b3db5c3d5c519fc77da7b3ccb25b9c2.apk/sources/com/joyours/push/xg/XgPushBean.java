package com.joyours.push.xg;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.joyours.base.framework.Cocos2dxApp;
import com.joyours.base.framework.IBean;
import com.joyours.push.xg.XgPushInterface;
import com.tencent.android.tpush.XGIOperateCallback;
import com.tencent.android.tpush.XGPushClickedResult;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.XGPushManager;

public class XgPushBean implements IBean {
    public static String SIG = XgPushBean.class.getSimpleName();
    protected boolean isRegisterd = false;
    protected XgPushInterface.RegisterCallback registerCallback;

    public void onCreate(Activity activity, Bundle bundle) {
    }

    public void onRestoreInstanceState(Activity activity, Bundle bundle) {
    }

    public void onStart(Activity activity) {
        XGPushClickedResult xgPushClickedResult = XGPushManager.onActivityStarted(activity);
        if (xgPushClickedResult != null) {
            Log.d(SIG, "Push clicked," + xgPushClickedResult.toString());
        }
    }

    public void onRestart(Activity activity) {
    }

    public void onSaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onPause(Activity activity) {
    }

    public void onResume(Activity activity) {
    }

    public void onStop(Activity activity) {
        XGPushManager.onActivityStoped(activity);
    }

    public void onDestroy(Activity activity) {
    }

    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    }

    public void register(String uid, XgPushInterface.RegisterCallback callback) {
        this.registerCallback = callback;
        if (!this.isRegisterd) {
            setXgPushTag(Cocos2dxApp.getContext());
            if (uid == null || uid.equals("")) {
                XGPushManager.registerPush(Cocos2dxApp.getContext(), new XGIOperateCallback() {
                    public void onSuccess(Object data, int flag) {
                        Log.d(XgPushBean.SIG, "Register push success. token=" + data);
                        XgPushBean.this.registerCallback.call("PUSH_REGISTERED", true, data);
                    }

                    public void onFail(Object data, int errCode, String msg) {
                        Log.d(XgPushBean.SIG, "Register no uid push fail. token=" + data + ",code=" + errCode + ",msg=" + msg);
                        XgPushBean.this.registerCallback.call("PUSH_REGISTER_FAIL", true, data);
                    }
                });
            } else {
                XGPushManager.registerPush(Cocos2dxApp.getContext(), uid, new XGIOperateCallback() {
                    public void onSuccess(Object data, int flag) {
                        Log.d(XgPushBean.SIG, ">>>>>>>>>>>>>>>>>>>>>>Register push success. token=" + data);
                        XgPushBean.this.isRegisterd = true;
                        XgPushBean.this.registerCallback.call("PUSH_REGISTERED", true, data);
                    }

                    public void onFail(Object data, int errCode, String msg) {
                        Log.d(XgPushBean.SIG, "Register push with uid fail. token=" + data + ",code=" + errCode + ",msg=" + msg);
                        XgPushBean.this.registerCallback.call("PUSH_REGISTER_FAIL", true, data);
                    }
                });
            }
        } else {
            String token = XGPushConfig.getToken(Cocos2dxApp.getContext());
            Log.d(SIG, "Push already registered. token=" + token);
            this.registerCallback.call("PUSH_REGISTERED", true, token);
        }
    }

    /* access modifiers changed from: protected */
    public void setXgPushTag(Context context) {
        Object distributionId = null;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (!(applicationInfo == null || applicationInfo.metaData == null)) {
                distributionId = applicationInfo.metaData.get("DISTRIBUTION_ID");
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(SIG, e.getMessage());
        }
        if (distributionId != null) {
            String tag = convertXgPushTag(String.valueOf(distributionId));
            if (!TextUtils.isEmpty(tag)) {
                XGPushManager.setTag(context, tag);
            }
        }
    }

    private String convertXgPushTag(Object string) {
        String tag = null;
        if (string != null) {
            if (string instanceof String) {
                tag = (String) string;
            } else {
                tag = String.valueOf(string);
            }
        }
        if (TextUtils.isEmpty(tag)) {
            return "";
        }
        return tag.trim();
    }
}
