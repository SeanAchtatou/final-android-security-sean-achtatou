package com.joyours.push.xg;

import android.content.Context;
import android.util.Log;
import com.tencent.android.tpush.XGPushBaseReceiver;
import com.tencent.android.tpush.XGPushClickedResult;
import com.tencent.android.tpush.XGPushRegisterResult;
import com.tencent.android.tpush.XGPushShowedResult;
import com.tencent.android.tpush.XGPushTextMessage;

public class XgPushReceiver extends XGPushBaseReceiver {
    public static String SIG = XgPushReceiver.class.getSimpleName();

    public void onDeleteTagResult(Context context, int errorCode, String tag) {
        if (errorCode == 0) {
            Log.d(SIG, "Delete tag success,tag=" + tag);
        } else {
            Log.d(SIG, "Delete tag failed,tag=" + tag + ",code=" + errorCode);
        }
    }

    public void onNotifactionClickedResult(Context context, XGPushClickedResult xgPushClickedResult) {
    }

    public void onNotifactionShowedResult(Context context, XGPushShowedResult xgPushShowedResult) {
    }

    public void onRegisterResult(Context context, int i, XGPushRegisterResult xgPushRegisterResult) {
    }

    public void onSetTagResult(Context context, int i, String string) {
    }

    public void onTextMessage(Context context, XGPushTextMessage xgPushTextMessage) {
    }

    public void onUnregisterResult(Context arg0, int arg1) {
    }
}
