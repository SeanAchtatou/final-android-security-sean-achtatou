package com.joyours.push.xg;

import android.util.Log;
import com.facebook.share.internal.ShareConstants;
import com.joyours.base.framework.Cocos2dxApp;
import org.cocos2dx.lib.Cocos2dxLuaJavaBridge;
import org.json.JSONException;
import org.json.JSONObject;

public class XgPushInterface {
    public static String SIG = XgPushInterface.class.getSimpleName();
    public static int registerCallback = -1;

    public interface RegisterCallback {
        void call(String str, boolean z, Object obj);
    }

    public static void setRegisterCallback(int callback) {
        if (registerCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(registerCallback);
            registerCallback = -1;
        }
        registerCallback = callback;
    }

    public static void register(final String uid) {
        Cocos2dxApp.getContext().getBackgroundHandler().post(new Runnable() {
            public void run() {
                XgPushBean xgPushBean = Cocos2dxApp.getContext().getXgPushBean();
                if (xgPushBean != null) {
                    Log.d(XgPushInterface.SIG, "XgPush register begin.");
                    xgPushBean.register(uid, new RegisterCallback() {
                        public void call(final String eventName, final boolean isSuccess, final Object data) {
                            Cocos2dxApp.getContext().runOnResume(new Runnable() {
                                public void run() {
                                    Cocos2dxApp.getContext().runOnGLThread(new Runnable() {
                                        public void run() {
                                            JSONObject json = new JSONObject();
                                            try {
                                                json.put("eventName", eventName);
                                                json.put("isSuccess", isSuccess);
                                                json.put(ShareConstants.WEB_DIALOG_PARAM_DATA, data);
                                                Log.d(XgPushInterface.SIG, "XgPush registerCallback.registerCallback=" + XgPushInterface.registerCallback);
                                                Cocos2dxLuaJavaBridge.callLuaFunctionWithString(XgPushInterface.registerCallback, json.toString());
                                            } catch (JSONException e) {
                                                Log.e(XgPushInterface.SIG, e.getMessage(), e);
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                    Log.d(XgPushInterface.SIG, "XgPush register end.");
                }
            }
        });
    }
}
