package com.joyours.push.fcm;

import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FcmPushIDService extends FirebaseInstanceIdService {
    public static String SIG = FcmPushBean.class.getSimpleName();

    public void onCreate() {
        super.onCreate();
        Log.d(SIG, "Token register begin");
    }

    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(SIG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        if (token != null) {
            FcmPushInterface.sendToken(token);
        }
    }
}
