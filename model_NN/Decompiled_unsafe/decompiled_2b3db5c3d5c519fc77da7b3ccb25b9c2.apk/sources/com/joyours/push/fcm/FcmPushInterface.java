package com.joyours.push.fcm;

import android.util.Log;
import com.facebook.share.internal.ShareConstants;
import com.google.firebase.iid.FirebaseInstanceId;
import com.joyours.base.framework.Cocos2dxApp;
import org.cocos2dx.lib.Cocos2dxLuaJavaBridge;
import org.json.JSONException;
import org.json.JSONObject;

public class FcmPushInterface {
    public static String SIG = FcmPushInterface.class.getSimpleName();
    public static int registerCallback = -1;

    public interface RegisterCallback {
        void call(boolean z, Object obj);
    }

    public static void setRegisterCallback(int callback) {
        if (registerCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(registerCallback);
            registerCallback = -1;
        }
        registerCallback = callback;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public static String getFcmData() {
        JSONObject json = new JSONObject();
        try {
            if (Cocos2dxApp.getMessageCode() != null) {
                json.put("isSuccess", true);
                json.put(ShareConstants.WEB_DIALOG_PARAM_DATA, Cocos2dxApp.getMessageCode());
            } else {
                json.put("isSuccess", false);
                json.put(ShareConstants.WEB_DIALOG_PARAM_DATA, (Object) null);
            }
        } catch (JSONException e) {
            Log.e(SIG, e.getMessage(), e);
        }
        return json.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public static String getToken() {
        JSONObject json = new JSONObject();
        try {
            String token = FirebaseInstanceId.getInstance().getToken();
            if (token == null) {
                json.put("isSuccess", false);
            } else {
                json.put("isSuccess", true);
                json.put(ShareConstants.WEB_DIALOG_PARAM_DATA, token);
            }
        } catch (JSONException e) {
            Log.e(SIG, e.getMessage(), e);
        }
        return json.toString();
    }

    public static void sendToken(final String token) {
        Cocos2dxApp.getContext().getBackgroundHandler().post(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
              ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
              ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
              ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
              ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
            public void run() {
                JSONObject json = new JSONObject();
                try {
                    json.put("isSuccess", true);
                    json.put(ShareConstants.WEB_DIALOG_PARAM_DATA, token);
                    Cocos2dxLuaJavaBridge.callLuaFunctionWithString(FcmPushInterface.registerCallback, json.toString());
                } catch (JSONException e) {
                    Log.e(FcmPushInterface.SIG, e.getMessage(), e);
                }
            }
        });
    }
}
