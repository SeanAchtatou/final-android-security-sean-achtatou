package com.joyours.push.fcm;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.firebase.iid.FirebaseInstanceId;
import com.joyours.base.framework.IBean;
import com.joyours.push.fcm.FcmPushInterface;

public class FcmPushBean implements IBean {
    public static String SIG = FcmPushBean.class.getSimpleName();
    private static String authorizedEntity = "";
    public static Context context = null;
    protected FcmPushInterface.RegisterCallback registerCallback;

    public void onCreate(Activity activity, Bundle bundle) {
    }

    public void onRestoreInstanceState(Activity activity, Bundle bundle) {
    }

    public void onStart(Activity activity) {
    }

    public void onRestart(Activity activity) {
    }

    public void onSaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onPause(Activity activity) {
    }

    public void onResume(Activity activity) {
    }

    public void onStop(Activity activity) {
    }

    public void onDestroy(Activity activity) {
    }

    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    }

    public static String getToken() {
        return FirebaseInstanceId.getInstance().getToken();
    }
}
