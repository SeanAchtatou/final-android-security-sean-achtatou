package com.joyours.push.fcm;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.joyours.base.framework.Cocos2dxApp;
import com.seantech.nineke.R;

public class FcmPushReceiver extends FirebaseMessagingService {
    public static String SIG = FcmPushBean.class.getSimpleName();
    private static Context context = Cocos2dxApp.getContext();
    private static int status_code = 0;

    public void onCreate() {
        super.onCreate();
        Log.d(SIG, "onMessageReceived");
    }

    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(SIG, "From: " + remoteMessage.getFrom());
        Log.d(SIG, "Title: " + remoteMessage.getData().get("Title"));
        Log.d(SIG, "data: " + remoteMessage.getData().get("Content"));
        Log.d(SIG, "Title: " + remoteMessage.getNotification().getTitle());
        Log.d(SIG, "data: " + remoteMessage.getNotification().getBody());
        Log.d(SIG, "messageCode" + remoteMessage.getData().get("messageCode"));
        notifyMessage(remoteMessage);
    }

    @SuppressLint({"NewApi"})
    private void notifyMessage(RemoteMessage message) {
        NotificationManager manager = (NotificationManager) context.getSystemService("notification");
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        Intent it = new Intent(context, context.getClass());
        it.putExtra("messageCode", message.getData().get("messageCode"));
        PendingIntent contentIndent = PendingIntent.getActivity(context, 0, it, 134217728);
        if (message.getData().get("Title") != null) {
            builder.setTicker(message.getData().get("Title"));
            builder.setContentTitle(message.getData().get("Title"));
        } else {
            builder.setTicker(message.getNotification().getTitle());
            builder.setContentTitle(message.getNotification().getTitle());
        }
        if (message.getData().get("Content") != null) {
            builder.setContentText(message.getData().get("Content"));
        } else {
            builder.setContentText(message.getNotification().getBody());
        }
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
        builder.setWhen(System.currentTimeMillis());
        builder.setAutoCancel(true);
        builder.setContentIntent(contentIndent);
        builder.setDefaults(-1);
        manager.notify(status_code, builder.build());
        status_code++;
    }
}
