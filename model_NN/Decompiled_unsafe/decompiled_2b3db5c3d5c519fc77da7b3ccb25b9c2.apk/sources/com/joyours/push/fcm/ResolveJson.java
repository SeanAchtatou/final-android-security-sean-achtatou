package com.joyours.push.fcm;

import android.content.Context;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import org.json.JSONObject;

public class ResolveJson {
    private static HashMap<String, String> hashMap = new HashMap<>();

    protected static HashMap<String, String> resolve(Context context) {
        try {
            InputStream is = context.getResources().getAssets().open("google-services.json");
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            while (true) {
                int len = is.read(buffer);
                if (len == -1) {
                    break;
                }
                outStream.write(buffer, 0, len);
            }
            outStream.flush();
            createHashMap(new String(outStream.toByteArray(), "ISO-8859-1"));
            outStream.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashMap;
    }

    private static void createHashMap(String str) throws Exception {
        JSONObject json = new JSONObject(str);
        JSONObject info = json.getJSONObject("project_info");
        JSONObject client = json.getJSONArray("client").getJSONObject(0);
        JSONObject apiKey = client.getJSONArray("api_key").getJSONObject(0);
        hashMap.put("ProjectId", info.getString("project_id"));
        hashMap.put("ApiKey", apiKey.getString("current_key"));
        hashMap.put("ApplicationId", client.getJSONObject("client_info").getString("mobilesdk_app_id"));
        hashMap.put("DatabaseUrl", info.getString("firebase_url"));
        hashMap.put("GcmSenderId", info.getString("project_number"));
    }
}
