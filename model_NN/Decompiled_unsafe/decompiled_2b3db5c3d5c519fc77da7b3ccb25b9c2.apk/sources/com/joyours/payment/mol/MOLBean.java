package com.joyours.payment.mol;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;
import com.facebook.GraphResponse;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.joyours.base.framework.Cocos2dxApp;
import com.joyours.base.framework.IBean;
import com.joyours.payment.mol.MOLInterface;
import com.mol.seaplus.Language;
import com.mol.seaplus.sdk.MOLSEAPlus;
import com.mol.seaplus.sdk.MOLSEAPlusListener;
import com.mol.seaplus.sdk.ResultHolder;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint({"SetJavaScriptEnabled"})
public class MOLBean implements IBean, MOLSEAPlusListener {
    protected static int SEND_SMS_REQUEST = 23361111;
    private static String SIG = MOLBean.class.getSimpleName();
    private static MOLInterface.PayCallback payCallback;
    /* access modifiers changed from: private */
    public MOLSEAPlus MOLThailandDirect;
    /* access modifiers changed from: private */
    public String _priceId = null;
    /* access modifiers changed from: private */
    public String _propName = null;
    /* access modifiers changed from: private */
    public String _ptxId = null;
    private String _secretKey = null;
    private String _serviceId = null;
    /* access modifiers changed from: private */
    public String _userId = null;
    private Context context = Cocos2dxApp.getContext();
    private Handler handler = null;

    public void onCreate(Activity activity, Bundle bundle) {
    }

    public void onRestoreInstanceState(Activity activity, Bundle bundle) {
    }

    public void onStart(Activity activity) {
    }

    public void onRestart(Activity activity) {
    }

    public void onSaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onPause(Activity activity) {
    }

    public void onResume(Activity activity) {
    }

    public void onStop(Activity activity) {
    }

    public void onDestroy(Activity activity) {
    }

    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == SEND_SMS_REQUEST && permissions[0].equals("android.permission.SEND_SMS") && grantResults[0] == 0) {
            purchase();
        }
    }

    public void initialize() {
    }

    private boolean checkSendSMSPermission() {
        if (Build.VERSION.SDK_INT < 23) {
            return true;
        }
        if (ContextCompat.checkSelfPermission(Cocos2dxApp.getContext(), "android.permission.SEND_SMS") == 0) {
            return true;
        }
        ActivityCompat.requestPermissions(Cocos2dxApp.getContext(), new String[]{"android.permission.SEND_SMS"}, SEND_SMS_REQUEST);
        return false;
    }

    public void purchaseByPSMS(String ptxId, String userId, String priceId, String propName, String serviceId, String secretKey, MOLInterface.PayCallback callback) {
        payCallback = callback;
        this._ptxId = ptxId;
        this._userId = userId;
        this._priceId = priceId;
        this._propName = propName;
        this._serviceId = serviceId;
        this._secretKey = secretKey;
        if (checkSendSMSPermission()) {
            purchase();
        }
    }

    public void purchase() {
        this.MOLThailandDirect = new MOLSEAPlus(this.context, this._serviceId, this._secretKey, Language.TH);
        Log.d(SIG, "purchaseByPSMS (" + this._ptxId + ", " + this._userId + ", " + this._priceId + "," + this._propName + ") ...");
        MOLSEAPlus mOLSEAPlus = this.MOLThailandDirect;
        MOLSEAPlus.enableTest(false);
        Cocos2dxApp.getContext().getMainHandler().post(new Runnable() {
            public void run() {
                MOLBean.this.MOLThailandDirect.purchaseByPSMS(MOLBean.this._ptxId, MOLBean.this._userId, MOLBean.this._priceId, MOLBean.this._propName, "", this);
            }
        });
    }

    public void onRequestError(int errorCode, String error) {
        Log.d(SIG, "onRequestError Request fail : " + error + "errorCode =" + errorCode);
        JSONObject json = new JSONObject();
        try {
            json.put("status", "fail");
            json.put("error", error);
            json.put("errorCode", errorCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        payCallback.call(json.toString(), true);
    }

    public void onRequestEvent(int pEventCode) {
        String message = null;
        switch (pEventCode) {
            case 202:
                message = "Charging";
                break;
            case 203:
                message = "Charging in background";
                break;
        }
        if (message != null) {
            Toast.makeText(this.context, message, 1).show();
            Log.d(SIG, "onRequestEvent : " + message + "pEventCode =" + pEventCode);
        }
    }

    public void onUserCancel() {
        JSONObject json = new JSONObject();
        try {
            json.put("status", "cancel");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        payCallback.call(json.toString(), true);
    }

    public void onRequestSuccess(MOLSEAPlus.PaymentChannel pPaymentChannel, ResultHolder pResultHolder) {
        JSONObject json = new JSONObject();
        try {
            json.put("status", GraphResponse.SUCCESS_KEY);
            json.put("pPaymentChannel", pPaymentChannel);
            json.put("ptxId", pResultHolder.getPartnerTransactionId());
            json.put("txid", pResultHolder.getTransactionId());
            json.put(FirebaseAnalytics.Param.PRICE, pResultHolder.getPriceId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        payCallback.call(json.toString(), true);
    }
}
