package com.joyours.payment.mol;

import android.util.Log;
import com.joyours.base.framework.Cocos2dxApp;
import org.cocos2dx.lib.Cocos2dxLuaJavaBridge;

public class MOLInterface {
    public static String SIG = MOLInterface.class.getSimpleName();
    /* access modifiers changed from: private */
    public static int payCallback = -1;

    public interface PayCallback {
        void call(String str, boolean z);
    }

    public static void setPayCallback(int payCallback2) {
        if (payCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(payCallback);
            payCallback = -1;
        }
        payCallback = payCallback2;
    }

    public static void purchaseByPSMS(int paymentId, String uid, String propsName, String price, String orderId, String serviceId, String secretKey, int payCallback2) {
        if (payCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(payCallback);
            payCallback = -1;
        }
        payCallback = payCallback2;
        MOLBean molBean = Cocos2dxApp.getContext().getMOLBean();
        if (molBean != null) {
            Log.d(SIG, "purchaseByPSMS=" + paymentId + "." + uid + "." + propsName + "." + price + "." + orderId);
            molBean.purchaseByPSMS(orderId, uid, price, propsName, serviceId, secretKey, new PayCallback() {
                public void call(final String result, boolean delay) {
                    Cocos2dxApp.getContext().runOnResume(new Runnable() {
                        public void run() {
                            Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                public void run() {
                                    Cocos2dxLuaJavaBridge.callLuaFunctionWithString(MOLInterface.payCallback, result);
                                }
                            }, 48);
                        }
                    });
                }
            });
        }
    }
}
