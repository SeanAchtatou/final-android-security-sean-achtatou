package com.joyours.payment.bluepay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import com.bluepay.data.Config;
import com.bluepay.interfaceClass.BlueInitCallback;
import com.bluepay.pay.BlueMessage;
import com.bluepay.pay.BluePay;
import com.bluepay.pay.Client;
import com.bluepay.pay.IPayCallback;
import com.bluepay.pay.PublisherCode;
import com.facebook.GraphResponse;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.joyours.base.framework.Cocos2dxApp;
import com.joyours.base.framework.IBean;
import com.joyours.payment.bluepay.BluePayInterface;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint({"HandlerLeak"})
public class BluePayBean implements IBean {
    protected static int READ_PHONE_STATE_REQUEST = 23363333;
    protected static int SEND_SMS_REQUEST = 23362222;
    public static String SIG = BluePayBean.class.getSimpleName();
    /* access modifiers changed from: private */
    public BluePayInterface.InitializeCallback initializeCallback = null;
    /* access modifiers changed from: private */
    public boolean isInitialized = false;

    public void onCreate(Activity activity, Bundle bundle) {
        BluePay.setLandscape(true);
        BluePay.setShowCardLoading(false);
    }

    public void onRestoreInstanceState(Activity activity, Bundle bundle) {
    }

    public void onStart(Activity activity) {
    }

    public void onRestart(Activity activity) {
    }

    public void onSaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onPause(Activity activity) {
    }

    public void onResume(Activity activity) {
    }

    public void onStop(Activity activity) {
    }

    public void onDestroy(Activity activity) {
    }

    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.d(SIG, ">>>>>>>>>>>>>>>>>>>>>>>>>blue pay requestCode = " + requestCode);
        if (requestCode == READ_PHONE_STATE_REQUEST) {
            if (!permissions[0].equals("android.permission.READ_PHONE_STATE") || grantResults[0] != 0) {
                this.initializeCallback.call("initFail", false);
            } else if (ContextCompat.checkSelfPermission(Cocos2dxApp.getContext(), "android.permission.SEND_SMS") != 0) {
                ActivityCompat.requestPermissions(Cocos2dxApp.getContext(), new String[]{"android.permission.SEND_SMS"}, SEND_SMS_REQUEST);
            } else {
                initBlueSDK();
            }
        } else if (requestCode != SEND_SMS_REQUEST) {
        } else {
            if (!permissions[0].equals("android.permission.SEND_SMS") || grantResults[0] != 0) {
                this.initializeCallback.call("initFail", false);
            } else if (ContextCompat.checkSelfPermission(Cocos2dxApp.getContext(), "android.permission.READ_PHONE_STATE") != 0) {
                ActivityCompat.requestPermissions(Cocos2dxApp.getContext(), new String[]{"android.permission.READ_PHONE_STATE"}, READ_PHONE_STATE_REQUEST);
            } else {
                initBlueSDK();
            }
        }
    }

    public void initialize(BluePayInterface.InitializeCallback callback) {
        this.initializeCallback = callback;
        boolean permissonGranted = true;
        boolean isRequesting = false;
        if (Build.VERSION.SDK_INT >= 23) {
            int readPhoneState = ContextCompat.checkSelfPermission(Cocos2dxApp.getContext(), "android.permission.READ_PHONE_STATE");
            int sendSms = ContextCompat.checkSelfPermission(Cocos2dxApp.getContext(), "android.permission.SEND_SMS");
            if (readPhoneState != 0) {
                ActivityCompat.requestPermissions(Cocos2dxApp.getContext(), new String[]{"android.permission.READ_PHONE_STATE"}, READ_PHONE_STATE_REQUEST);
                permissonGranted = false;
                isRequesting = true;
            }
            if (!isRequesting && sendSms != 0) {
                ActivityCompat.requestPermissions(Cocos2dxApp.getContext(), new String[]{"android.permission.SEND_SMS"}, SEND_SMS_REQUEST);
                permissonGranted = false;
            }
            if (permissonGranted) {
                initBlueSDK();
                return;
            }
            return;
        }
        initBlueSDK();
    }

    public void initBlueSDK() {
        Log.d(SIG, "initialize");
        Client.init(Cocos2dxApp.getContext(), new BlueInitCallback() {
            public void initComplete(String loginResult, String resultDesc) {
                if (loginResult == "200") {
                    boolean unused = BluePayBean.this.isInitialized = true;
                    BluePayBean.this.initializeCallback.call(GraphResponse.SUCCESS_KEY, true);
                } else if (loginResult == "404") {
                    BluePayBean.this.initializeCallback.call("fail", true);
                } else {
                    BluePayBean.this.initializeCallback.call("fail", true);
                }
                Log.d(BluePayBean.SIG, "loginResult = " + loginResult + ", resultDesc = " + resultDesc);
            }
        });
    }

    /* access modifiers changed from: private */
    public String buildPaySuccMsg(BlueMessage blueMessage) {
        JSONObject json = new JSONObject();
        try {
            json.put("ret", 0);
            json.put("propsName", blueMessage.getPropsName());
            json.put(FirebaseAnalytics.Param.PRICE, blueMessage.getPrice());
            json.put("transationId", blueMessage.getTransactionId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    /* access modifiers changed from: private */
    public String buildPayFailMsg(BlueMessage message) {
        JSONObject json = new JSONObject();
        try {
            json.put("ret", message.getCode());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    public void payByTrueMoney(int paymentId, String uid, String propsName, String cardNo, String transationId, final BluePayInterface.PayCallback callback) {
        IPayCallback payCallback = new IPayCallback() {
            private static final long serialVersionUID = 1;

            public String onPrepared() {
                return "";
            }

            public void onFinished(BlueMessage message) {
                switch (message.getCode()) {
                    case 200:
                    case 201:
                        callback.call(BluePayBean.this.buildPaySuccMsg(message), true);
                        break;
                    default:
                        callback.call(BluePayBean.this.buildPayFailMsg(message), true);
                        break;
                }
                Log.d(BluePayBean.SIG, "code=" + message.getCode() + ",msg=" + message.toString());
            }
        };
        Log.d(SIG, "BluePay.mBluePay.payByCashcard, Publisher = truemoney");
        BluePay.getInstance().payByCashcard(Cocos2dxApp.getContext(), uid, transationId, propsName, PublisherCode.PUBLISHER_TRUEMONEY, cardNo, "", payCallback);
    }

    public void payBy12Call(int paymentId, String uid, String propsName, String cardNo, String transationId, final BluePayInterface.PayCallback callback) {
        BluePay.getInstance().payByCashcard(Cocos2dxApp.getContext(), uid, transationId, propsName, PublisherCode.PUBLISHER_12CALL, cardNo, "", new IPayCallback() {
            private static final long serialVersionUID = 2;

            public String onPrepared() {
                return "";
            }

            public void onFinished(BlueMessage message) {
                switch (message.getCode()) {
                    case 200:
                    case 201:
                        callback.call(BluePayBean.this.buildPaySuccMsg(message), true);
                        break;
                    default:
                        callback.call(BluePayBean.this.buildPayFailMsg(message), true);
                        break;
                }
                Log.d(BluePayBean.SIG, "code=" + message.getCode() + ",msg=" + message.toString());
            }
        });
    }

    public void payByHappy(int paymentId, String uid, String propsName, String cardNo, String transationId, final BluePayInterface.PayCallback callback) {
        BluePay.getInstance().payByCashcard(Cocos2dxApp.getContext(), uid, transationId, propsName, PublisherCode.PUBLISHER_HAPPY, cardNo, "", new IPayCallback() {
            private static final long serialVersionUID = 4;

            public String onPrepared() {
                return "";
            }

            public void onFinished(BlueMessage message) {
                switch (message.getCode()) {
                    case 200:
                    case 201:
                        callback.call(BluePayBean.this.buildPaySuccMsg(message), true);
                        break;
                    default:
                        callback.call(BluePayBean.this.buildPayFailMsg(message), true);
                        break;
                }
                Log.d(BluePayBean.SIG, "code=" + message.getCode() + ",msg=" + message.toString());
            }
        });
    }

    public void payBySMS(int paymentId, String uid, String propsName, int price, String orderId, final BluePayInterface.PayCallback callback) {
        String transationId = orderId;
        IPayCallback payCallback = new IPayCallback() {
            private static final long serialVersionUID = 3;

            public String onPrepared() {
                return "";
            }

            public void onFinished(BlueMessage message) {
                switch (message.getCode()) {
                    case 200:
                    case 201:
                        callback.call(BluePayBean.this.buildPaySuccMsg(message), true);
                        break;
                    default:
                        callback.call(BluePayBean.this.buildPayFailMsg(message), true);
                        break;
                }
                Log.d(BluePayBean.SIG, "code=" + message.getCode() + ",msg=" + message.toString());
            }
        };
        Log.d(SIG, ">>>>>>>>>>>>>transationId = " + transationId.toString());
        BluePay.getInstance().payBySMS(Cocos2dxApp.getContext(), transationId, Config.K_CURRENCY_THB, String.valueOf(price), 0, propsName, false, payCallback);
    }

    public void payByBank(int paymentId, String uid, String propsName, int price, String orderId, final BluePayInterface.PayCallback callback) {
        IPayCallback payCallback = new IPayCallback() {
            private static final long serialVersionUID = 3;

            public String onPrepared() {
                return "";
            }

            public void onFinished(BlueMessage message) {
                switch (message.getCode()) {
                    case 200:
                    case 201:
                        callback.call(BluePayBean.this.buildPaySuccMsg(message), true);
                        break;
                    default:
                        callback.call(BluePayBean.this.buildPayFailMsg(message), true);
                        break;
                }
                Log.d(BluePayBean.SIG, "code=" + message.getCode() + ",msg=" + message.toString());
            }
        };
        BluePay.getInstance().payByBank(Cocos2dxApp.getContext(), orderId, Config.K_CURRENCY_THB, String.valueOf(price), propsName, PublisherCode.PUBLISHER_BANK, false, payCallback);
    }

    public boolean isInitialized() {
        return this.isInitialized;
    }
}
