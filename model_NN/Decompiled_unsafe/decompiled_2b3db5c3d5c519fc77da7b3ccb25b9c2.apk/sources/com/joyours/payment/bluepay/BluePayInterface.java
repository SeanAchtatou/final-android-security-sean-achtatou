package com.joyours.payment.bluepay;

import android.util.Log;
import com.bluepay.pay.Client;
import com.joyours.base.framework.Cocos2dxApp;
import org.cocos2dx.lib.Cocos2dxLuaJavaBridge;

public class BluePayInterface {
    public static String SIG = BluePayInterface.class.getSimpleName();
    /* access modifiers changed from: private */
    public static int initCallback = -1;
    /* access modifiers changed from: private */
    public static int payCallback = -1;

    public interface InitializeCallback {
        void call(String str, boolean z);
    }

    public interface PayCallback {
        void call(String str, boolean z);
    }

    public static void setInitCallback(int initCallback2) {
        if (initCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(initCallback);
            initCallback = -1;
        }
        initCallback = initCallback2;
    }

    public static boolean isInitialized() {
        Log.d(SIG, "isInitialized()");
        BluePayBean bluePayBean = Cocos2dxApp.getContext().getBluePayBean();
        if (bluePayBean != null) {
            return bluePayBean.isInitialized();
        }
        return false;
    }

    public static void initialize() {
        Log.d(SIG, "BluePayInterface.initialize");
        Cocos2dxApp.getContext().getMainHandler().postDelayed(new Runnable() {
            public void run() {
                BluePayBean bluePayBean = Cocos2dxApp.getContext().getBluePayBean();
                if (bluePayBean != null) {
                    bluePayBean.initialize(new InitializeCallback() {
                        public void call(final String result, boolean delay) {
                            Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                public void run() {
                                    Log.d(BluePayInterface.SIG, "BluePayInterface.InitializeCallback,result=" + result);
                                    Cocos2dxLuaJavaBridge.callLuaFunctionWithString(BluePayInterface.initCallback, result);
                                }
                            }, 48);
                        }
                    });
                } else {
                    Cocos2dxLuaJavaBridge.callLuaFunctionWithString(BluePayInterface.initCallback, "fail");
                }
            }
        }, 48);
    }

    public static void setPayCallback(int payCallback2) {
        if (payCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(payCallback);
            payCallback = -1;
        }
        payCallback = payCallback2;
    }

    public static void payBySMS(int paymentId, String uid, String propsName, int price, String orderId) {
        BluePayBean bluePayBean = Cocos2dxApp.getContext().getBluePayBean();
        if (bluePayBean != null) {
            bluePayBean.payBySMS(paymentId, uid, propsName, price, orderId, new PayCallback() {
                public void call(final String result, boolean delay) {
                    Cocos2dxApp.getContext().runOnResume(new Runnable() {
                        public void run() {
                            Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                public void run() {
                                    Cocos2dxLuaJavaBridge.callLuaFunctionWithString(BluePayInterface.payCallback, result);
                                }
                            }, 48);
                        }
                    });
                }
            });
        }
    }

    public static void payByTrueMoney(int paymentId, String uid, String propsName, String cardNo, String transactionId) {
        Log.d(SIG, "payByTrueMoney=" + paymentId + "." + uid + "." + propsName + "." + cardNo + "." + transactionId);
        BluePayBean bluePayBean = Cocos2dxApp.getContext().getBluePayBean();
        if (bluePayBean != null) {
            bluePayBean.payByTrueMoney(paymentId, uid, propsName, cardNo, transactionId, new PayCallback() {
                public void call(final String result, boolean delay) {
                    Cocos2dxApp.getContext().runOnResume(new Runnable() {
                        public void run() {
                            Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                public void run() {
                                    Cocos2dxLuaJavaBridge.callLuaFunctionWithString(BluePayInterface.payCallback, result);
                                }
                            }, 48);
                        }
                    });
                }
            });
        }
    }

    public static void payBy12Call(int paymentId, String uid, String propsName, String cardNo, String transationId) {
        BluePayBean bluePayBean = Cocos2dxApp.getContext().getBluePayBean();
        if (bluePayBean != null) {
            bluePayBean.payBy12Call(paymentId, uid, propsName, cardNo, transationId, new PayCallback() {
                public void call(final String result, boolean delay) {
                    Cocos2dxApp.getContext().runOnResume(new Runnable() {
                        public void run() {
                            Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                public void run() {
                                    Cocos2dxLuaJavaBridge.callLuaFunctionWithString(BluePayInterface.payCallback, result);
                                }
                            }, 48);
                        }
                    });
                }
            });
        }
    }

    public static void payByHappy(int paymentId, String uid, String propsName, String cardNo, String transationId) {
        BluePayBean bluePayBean = Cocos2dxApp.getContext().getBluePayBean();
        if (bluePayBean != null) {
            bluePayBean.payByHappy(paymentId, uid, propsName, cardNo, transationId, new PayCallback() {
                public void call(final String result, boolean delay) {
                    Cocos2dxApp.getContext().runOnResume(new Runnable() {
                        public void run() {
                            Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                public void run() {
                                    Cocos2dxLuaJavaBridge.callLuaFunctionWithString(BluePayInterface.payCallback, result);
                                }
                            }, 48);
                        }
                    });
                }
            });
        }
    }

    public static void payByBank(int paymentId, String uid, String propsName, int price, String orderId) {
        BluePayBean bluePayBean = Cocos2dxApp.getContext().getBluePayBean();
        if (bluePayBean != null) {
            bluePayBean.payByBank(paymentId, uid, propsName, price, orderId, new PayCallback() {
                public void call(final String result, boolean delay) {
                    Cocos2dxApp.getContext().runOnResume(new Runnable() {
                        public void run() {
                            Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                public void run() {
                                    Cocos2dxLuaJavaBridge.callLuaFunctionWithString(BluePayInterface.payCallback, result);
                                }
                            }, 48);
                        }
                    });
                }
            });
        }
    }

    public static void delayDispose(int delay) {
        Cocos2dxApp.getContext().getBackgroundHandler().postDelayed(new Runnable() {
            public void run() {
                Client.exit();
            }
        }, (long) (delay * 1000));
    }
}
