package com.joyours.payment.iab;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.facebook.GraphResponse;
import com.joyours.base.framework.Cocos2dxApp;
import com.joyours.base.framework.IBean;
import com.joyours.payment.iab.IabHelper;
import com.tencent.android.tpush.common.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IabBean implements IBean {
    private static final int REQUEST_CODE = 485451345;
    /* access modifiers changed from: private */
    public static final String SIG = IabBean.class.getSimpleName();
    private String base64EncodePublicKey;
    private IabHelper.OnConsumeFinishedListener consumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            if (IabBean.this.iabHelper != null && result.isSuccess()) {
                IabBean.this.inventory.erasePurchase(purchase.getSku());
                IabInterface.callLuaConsumeCallback(GraphResponse.SUCCESS_KEY);
            }
        }
    };
    /* access modifiers changed from: private */
    public Runnable destroyRunnable = new Runnable() {
        public void run() {
            boolean unused = IabBean.this.isInitialized = false;
            boolean unused2 = IabBean.this.isQueryingIventory = false;
            boolean unused3 = IabBean.this.isPurchasing = false;
            boolean unused4 = IabBean.this.isSetuping = false;
            boolean unused5 = IabBean.this.isSupport = false;
            if (IabBean.this.iabHelper != null) {
                IabBean.this.iabHelper.dispose();
            }
            IabHelper unused6 = IabBean.this.iabHelper = null;
            Cocos2dxApp.getContext().getMainHandler().removeCallbacks(IabBean.this.destroyRunnable);
        }
    };
    /* access modifiers changed from: private */
    public List<String> goodsList = null;
    /* access modifiers changed from: private */
    public IabHelper iabHelper;
    private IabHelper.OnIabPurchaseFinishedListener iabPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            boolean unused = IabBean.this.isPurchasing = false;
            Log.d(IabBean.SIG, "iab finished");
            if (IabBean.this.iabHelper != null) {
                if (!(IabBean.this.inventory == null || purchase == null || IabBean.this.inventory.hasPurchase(purchase.getSku()))) {
                    IabBean.this.inventory.addPurchase(purchase);
                }
                if (result.isFailure()) {
                    Log.d(IabBean.SIG, result.mMessage);
                    if (result.mResponse == 7) {
                        if (!(IabBean.this.purchasingGoodsId == null || IabBean.this.inventory == null)) {
                            purchase = IabBean.this.inventory.getPurchase(IabBean.this.purchasingGoodsId);
                            String unused2 = IabBean.this.purchasingGoodsId = null;
                        }
                        if (purchase != null) {
                            JSONObject json = new JSONObject();
                            try {
                                json.put("purchaseTime", purchase.getPurchaseTime());
                                json.put("developerPayload", purchase.getDeveloperPayload());
                                json.put("itemType", purchase.getItemType());
                                json.put("orderId", purchase.getOrderId());
                                json.put("originalJson", purchase.getOriginalJson());
                                json.put(Constants.FLAG_PACKAGE_NAME, purchase.getPackageName());
                                json.put("signature", purchase.getSignature());
                                json.put("sku", purchase.getSku());
                                json.put(Constants.FLAG_TOKEN, purchase.getToken());
                                json.put("purchaseState", purchase.getPurchaseState());
                            } catch (JSONException e) {
                                Log.e(IabBean.SIG, e.getMessage(), e);
                            }
                            IabInterface.callLuaPurchaseCallback(json.toString());
                        }
                    }
                    IabInterface.callLuaPurchaseCallback("fail");
                    return;
                }
                JSONObject json2 = new JSONObject();
                try {
                    json2.put("purchaseTime", purchase.getPurchaseTime());
                    json2.put("developerPayload", purchase.getDeveloperPayload());
                    json2.put("itemType", purchase.getItemType());
                    json2.put("orderId", purchase.getOrderId());
                    json2.put("originalJson", purchase.getOriginalJson());
                    json2.put(Constants.FLAG_PACKAGE_NAME, purchase.getPackageName());
                    json2.put("signature", purchase.getSignature());
                    json2.put("sku", purchase.getSku());
                    json2.put(Constants.FLAG_TOKEN, purchase.getToken());
                    json2.put("purchaseState", purchase.getPurchaseState());
                } catch (JSONException e2) {
                    Log.e(IabBean.SIG, e2.getMessage(), e2);
                }
                IabInterface.callLuaPurchaseCallback(json2.toString());
            }
        }
    };
    /* access modifiers changed from: private */
    public IabHelper.OnIabSetupFinishedListener iabSetupFinishedListener = new IabHelper.OnIabSetupFinishedListener() {
        public void onIabSetupFinished(IabResult result) {
            if (result.isSuccess()) {
                boolean unused = IabBean.this.isInitialized = true;
                boolean unused2 = IabBean.this.isSetuping = false;
                boolean unused3 = IabBean.this.isSupport = true;
                IabInterface.callLuaSetupCallback(true);
            } else if (IabBean.this.retry > 0) {
                IabBean.access$710(IabBean.this);
                IabBean.this.iabHelper.startSetup(IabBean.this.iabSetupFinishedListener);
            } else {
                boolean unused4 = IabBean.this.isInitialized = true;
                boolean unused5 = IabBean.this.isSetuping = false;
                boolean unused6 = IabBean.this.isSupport = false;
                IabInterface.callLuaSetupCallback(false);
            }
        }
    };
    /* access modifiers changed from: private */
    public Inventory inventory;
    private boolean isDebug;
    /* access modifiers changed from: private */
    public boolean isInitialized = false;
    /* access modifiers changed from: private */
    public boolean isPurchasing = false;
    /* access modifiers changed from: private */
    public boolean isQueryingIventory = false;
    /* access modifiers changed from: private */
    public boolean isSetuping = false;
    /* access modifiers changed from: private */
    public boolean isSupport = false;
    /* access modifiers changed from: private */
    public String purchasingGoodsId = null;
    /* access modifiers changed from: private */
    public IabHelper.QueryInventoryFinishedListener queryInventoryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            if (IabBean.this.iabHelper != null) {
                if (result.isFailure()) {
                    if (IabBean.this.retry > 0) {
                        IabBean.this.iabHelper.queryInventoryAsync(true, IabBean.this.goodsList, IabBean.this.queryInventoryFinishedListener);
                        IabBean.access$710(IabBean.this);
                    } else {
                        boolean unused = IabBean.this.isQueryingIventory = false;
                        IabInterface.callLuaQueryIventoryCallback("fail");
                    }
                    Log.d(IabBean.SIG, "result.isFailure()" + result.getMessage());
                    return;
                }
                boolean unused2 = IabBean.this.isQueryingIventory = false;
                Inventory unused3 = IabBean.this.inventory = inventory;
                JSONArray array = new JSONArray();
                if (IabBean.this.goodsList != null) {
                    for (String goods : IabBean.this.goodsList) {
                        Purchase purchase = inventory.getPurchase(goods);
                        if (purchase != null) {
                            JSONObject json = new JSONObject();
                            try {
                                json.put("purchaseTime", purchase.getPurchaseTime());
                                json.put("developerPayload", purchase.getDeveloperPayload());
                                json.put("itemType", purchase.getItemType());
                                json.put("orderId", purchase.getOrderId());
                                json.put("originalJson", purchase.getOriginalJson());
                                json.put(Constants.FLAG_PACKAGE_NAME, purchase.getPackageName());
                                json.put("signature", purchase.getSignature());
                                json.put("sku", purchase.getSku());
                                json.put(Constants.FLAG_TOKEN, purchase.getToken());
                                json.put("purchaseState", purchase.getPurchaseState());
                            } catch (JSONException e) {
                                Log.e(IabBean.SIG, e.getMessage(), e);
                            }
                            IabInterface.callLuaDeliverCallback(json.toString());
                        }
                    }
                }
                IabInterface.callLuaQueryIventoryCallback(array.toString());
            }
        }
    };
    /* access modifiers changed from: private */
    public int retry = 4;

    static /* synthetic */ int access$710(IabBean x0) {
        int i = x0.retry;
        x0.retry = i - 1;
        return i;
    }

    public IabBean(String base64EncodePublicKey2, boolean isDebug2) {
        this.base64EncodePublicKey = base64EncodePublicKey2;
        this.isDebug = isDebug2;
    }

    public void delayDispose(int delaySeconds) {
        Cocos2dxApp.getContext().getMainHandler().postDelayed(this.destroyRunnable, (long) (delaySeconds * 1000));
    }

    public void onCreate(Activity activity, Bundle bundle) {
        if (this.iabHelper != null) {
            this.destroyRunnable.run();
        }
        this.iabHelper = new IabHelper(activity, this.base64EncodePublicKey);
        this.iabHelper.enableDebugLogging(this.isDebug, "IabBean");
    }

    public void onRestoreInstanceState(Activity activity, Bundle bundle) {
    }

    public void onStart(Activity activity) {
    }

    public void onRestart(Activity activity) {
    }

    public void onSaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onPause(Activity activity) {
    }

    public void onResume(Activity activity) {
    }

    public void onStop(Activity activity) {
    }

    public void onDestroy(Activity activity) {
        this.destroyRunnable.run();
    }

    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
        if (this.iabHelper != null) {
            this.iabHelper.handleActivityResult(requestCode, resultCode, intent);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    }

    public boolean isInitialized() {
        return this.isInitialized;
    }

    public boolean isSupport() {
        return this.isSupport;
    }

    public void initialize() {
        Cocos2dxApp.getContext().getMainHandler().removeCallbacks(this.destroyRunnable);
        if (this.iabHelper == null) {
            this.destroyRunnable.run();
            this.iabHelper = new IabHelper(Cocos2dxApp.getContext(), this.base64EncodePublicKey);
            this.iabHelper.enableDebugLogging(this.isDebug, "GooglePlayPlugin");
            this.retry = 4;
            this.iabHelper.startSetup(this.iabSetupFinishedListener);
        } else if (this.isInitialized) {
            IabInterface.callLuaSetupCallback(this.isSupport);
        } else if (!this.isSetuping) {
            this.isSetuping = true;
            this.retry = 4;
            this.iabHelper.startSetup(this.iabSetupFinishedListener);
        }
    }

    public void queryIventory(String[] skus) {
        if (this.iabHelper != null && this.isInitialized && this.isSupport && !this.isQueryingIventory) {
            this.isQueryingIventory = true;
            this.retry = 4;
            this.goodsList = Arrays.asList(skus);
            this.iabHelper.queryInventoryAsync(true, this.goodsList, this.queryInventoryFinishedListener);
        }
    }

    public void checkPurchase(String sku) {
        if (this.inventory != null || this.inventory.hasPurchase(sku)) {
            Purchase purchase = this.inventory.getPurchase(sku);
            JSONObject json = new JSONObject();
            try {
                json.put("purchaseTime", purchase.getPurchaseTime());
                json.put("developerPayload", purchase.getDeveloperPayload());
                json.put("itemType", purchase.getItemType());
                json.put("orderId", purchase.getOrderId());
                json.put("originalJson", purchase.getOriginalJson());
                json.put(Constants.FLAG_PACKAGE_NAME, purchase.getPackageName());
                json.put("signature", purchase.getSignature());
                json.put("sku", purchase.getSku());
                json.put(Constants.FLAG_TOKEN, purchase.getToken());
                json.put("purchaseState", purchase.getPurchaseState());
            } catch (JSONException e) {
                Log.e(SIG, e.getMessage(), e);
            }
            IabInterface.callLuaPurchaseCallback(json.toString());
        }
    }

    public void purchase(String orderId, String paymentId, String goodsId, String uid, String channel) {
        if (this.iabHelper != null && this.isInitialized && this.isSupport && !this.isPurchasing) {
            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("uid", uid));
            params.add(new BasicNameValuePair("paymentId", paymentId));
            params.add(new BasicNameValuePair("sku", goodsId));
            params.add(new BasicNameValuePair("channel", channel));
            try {
                this.isPurchasing = true;
                this.purchasingGoodsId = goodsId;
                this.iabHelper.launchPurchaseFlow(Cocos2dxApp.getContext(), goodsId, REQUEST_CODE, this.iabPurchaseFinishedListener, URLEncodedUtils.format(params, HTTP.UTF_8));
            } catch (Exception e) {
                this.isPurchasing = false;
                Log.d(SIG, e.getMessage());
                this.purchasingGoodsId = null;
                IabInterface.callLuaPurchaseCallback("fail");
            }
        }
    }

    public void consume(final String goodsId) {
        if (this.iabHelper != null && this.isInitialized && this.isSupport) {
            if (this.inventory == null || !this.inventory.hasPurchase(goodsId)) {
                IabInterface.callLuaConsumeCallback(GraphResponse.SUCCESS_KEY);
                return;
            }
            Purchase purchase = this.inventory.getPurchase(goodsId);
            if (purchase != null) {
                try {
                    this.iabHelper.consumeAsync(purchase, this.consumeFinishedListener);
                } catch (IllegalStateException e) {
                    if (!this.iabHelper.isDisposed() && this.iabHelper.isSetupDone()) {
                        Cocos2dxApp.getContext().getBackgroundHandler().postDelayed(new Runnable() {
                            public void run() {
                                IabBean.this.consume(goodsId);
                            }
                        }, 64);
                    }
                }
            } else {
                IabInterface.callLuaConsumeCallback("fail");
            }
        }
    }
}
