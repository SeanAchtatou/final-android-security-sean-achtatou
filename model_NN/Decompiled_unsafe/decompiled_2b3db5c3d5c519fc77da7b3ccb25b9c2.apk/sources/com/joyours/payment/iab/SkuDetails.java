package com.joyours.payment.iab;

import android.util.Log;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.text.NumberFormat;
import java.util.Currency;
import org.json.JSONException;
import org.json.JSONObject;

public class SkuDetails {
    String mDescription;
    String mItemType;
    String mJson;
    String mPrice;
    double mPriceAmountMicros;
    String mPriceCurrencyCode;
    String mSku;
    String mTitle;
    String mType;

    public SkuDetails(String jsonSkuDetails) throws JSONException {
        this(IabHelper.ITEM_TYPE_INAPP, jsonSkuDetails);
    }

    public SkuDetails(String itemType, String jsonSkuDetails) throws JSONException {
        this.mItemType = itemType;
        this.mJson = jsonSkuDetails;
        JSONObject o = new JSONObject(this.mJson);
        this.mSku = o.optString("productId");
        this.mType = o.optString("type");
        this.mPrice = o.optString(FirebaseAnalytics.Param.PRICE);
        this.mTitle = o.optString("title");
        this.mDescription = o.optString("description");
        this.mPriceCurrencyCode = o.optString("price_currency_code");
        this.mPriceAmountMicros = o.optDouble("price_amount_micros");
    }

    public String getSku() {
        return this.mSku;
    }

    public String getType() {
        return this.mType;
    }

    public String getPrice() {
        return this.mPrice;
    }

    public String getTitle() {
        return this.mTitle;
    }

    public String getDescription() {
        return this.mDescription;
    }

    public Double getPriceNum() {
        if (this.mPriceCurrencyCode != null) {
            NumberFormat nf = NumberFormat.getCurrencyInstance();
            try {
                nf.setCurrency(Currency.getInstance(this.mPriceCurrencyCode));
                return Double.valueOf(nf.parse(this.mPrice).doubleValue());
            } catch (Exception e) {
                Log.e(SkuDetails.class.getSimpleName(), e.getMessage(), e);
            }
        }
        return null;
    }

    public String getPriceDollar() {
        if (this.mPriceCurrencyCode != null) {
            try {
                return Currency.getInstance(this.mPriceCurrencyCode).getSymbol();
            } catch (Exception e) {
                Log.e(SkuDetails.class.getSimpleName(), e.getMessage(), e);
            }
        }
        return null;
    }

    public String toString() {
        return "SkuDetails:" + this.mJson;
    }
}
