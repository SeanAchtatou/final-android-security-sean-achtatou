package com.joyours.payment.iab;

import android.util.Log;
import com.joyours.base.framework.Cocos2dxApp;
import org.cocos2dx.lib.Cocos2dxLuaJavaBridge;

public class IabInterface {
    public static String SIG = IabInterface.class.getSimpleName();
    /* access modifiers changed from: private */
    public static int consumeCallback;
    /* access modifiers changed from: private */
    public static int deliverCallback;
    /* access modifiers changed from: private */
    public static int initializeCallback;
    /* access modifiers changed from: private */
    public static int purchaseCallback;
    /* access modifiers changed from: private */
    public static int queryIventoryCallback;

    public static void initIabBean(String base64EncodePublicKey) {
        Cocos2dxApp.getContext().initIabBean(base64EncodePublicKey);
    }

    public static boolean isSupport() {
        IabBean iabBean = Cocos2dxApp.getContext().getIabBean();
        if (iabBean != null) {
            return iabBean.isSupport();
        }
        return false;
    }

    public static boolean isInitialized() {
        IabBean iabBean = Cocos2dxApp.getContext().getIabBean();
        if (iabBean != null) {
            return iabBean.isInitialized();
        }
        return false;
    }

    public static void initialize() {
        Log.d(SIG, "initialize");
        final IabBean iabBean = Cocos2dxApp.getContext().getIabBean();
        if (iabBean != null) {
            Cocos2dxApp.getContext().getBackgroundHandler().postDelayed(new Runnable() {
                public void run() {
                    iabBean.initialize();
                }
            }, 50);
        }
    }

    public static void queryIventory(String skusJSONString) {
        final IabBean iabBean = Cocos2dxApp.getContext().getIabBean();
        if (iabBean != null) {
            Log.d(SIG, skusJSONString);
            final String[] skus = skusJSONString.split(",");
            Cocos2dxApp.getContext().getBackgroundHandler().post(new Runnable() {
                public void run() {
                    iabBean.queryIventory(skus);
                }
            });
        }
    }

    public static void checkPurchase(final String sku) {
        final IabBean iabBean = Cocos2dxApp.getContext().getIabBean();
        if (iabBean != null) {
            Cocos2dxApp.getContext().getMainHandler().postDelayed(new Runnable() {
                public void run() {
                    iabBean.checkPurchase(sku);
                }
            }, 64);
        }
    }

    public static void purchase(String orderId, String paymentId, String sku, String uid, String channel) {
        final IabBean iabBean = Cocos2dxApp.getContext().getIabBean();
        if (iabBean != null) {
            final String str = orderId;
            final String str2 = paymentId;
            final String str3 = sku;
            final String str4 = uid;
            final String str5 = channel;
            Cocos2dxApp.getContext().getMainHandler().postDelayed(new Runnable() {
                public void run() {
                    iabBean.purchase(str, str2, str3, str4, str5);
                }
            }, 64);
        }
    }

    public static void consume(final String sku) {
        final IabBean iabBean = Cocos2dxApp.getContext().getIabBean();
        if (iabBean != null) {
            Cocos2dxApp.getContext().getBackgroundHandler().post(new Runnable() {
                public void run() {
                    iabBean.consume(sku);
                }
            });
        }
    }

    public static void delayDispose(final int delaySeconds) {
        final IabBean iabBean = Cocos2dxApp.getContext().getIabBean();
        if (iabBean != null) {
            Cocos2dxApp.getContext().getMainHandler().post(new Runnable() {
                public void run() {
                    iabBean.delayDispose(delaySeconds);
                }
            });
        }
    }

    public static void setInitializeCallback(int callback) {
        if (initializeCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(initializeCallback);
            initializeCallback = -1;
        }
        if (callback != -1) {
            initializeCallback = callback;
        }
    }

    public static void setQueryIventoryCallback(int callback) {
        if (queryIventoryCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(queryIventoryCallback);
            queryIventoryCallback = -1;
        }
        if (callback != -1) {
            queryIventoryCallback = callback;
        }
    }

    public static void setPurchaseCallback(int callback) {
        if (purchaseCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(purchaseCallback);
            purchaseCallback = -1;
        }
        if (callback != -1) {
            purchaseCallback = callback;
        }
    }

    public static void setDeliverCallback(int callback) {
        if (deliverCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(deliverCallback);
            deliverCallback = -1;
        }
        if (callback != -1) {
            deliverCallback = callback;
        }
    }

    public static void setConsumeCallback(int callback) {
        if (consumeCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(consumeCallback);
            consumeCallback = -1;
        }
        if (callback != -1) {
            consumeCallback = callback;
        }
    }

    public static void callLuaDeliverCallback(final String jsonString) {
        if (deliverCallback != -1) {
            Cocos2dxApp.getContext().runOnResume(new Runnable() {
                public void run() {
                    Cocos2dxApp.getContext().runOnGLThread(new Runnable() {
                        public void run() {
                            Cocos2dxLuaJavaBridge.callLuaFunctionWithString(IabInterface.deliverCallback, jsonString);
                        }
                    });
                }
            });
        }
    }

    public static void callLuaSetupCallback(final boolean isSupport) {
        Log.d(SIG, "callLuaSetupCallback " + isSupport);
        if (initializeCallback != -1) {
            Cocos2dxApp.getContext().runOnResume(new Runnable() {
                public void run() {
                    Cocos2dxApp.getContext().runOnGLThread(new Runnable() {
                        public void run() {
                            Cocos2dxLuaJavaBridge.callLuaFunctionWithString(IabInterface.initializeCallback, String.valueOf(isSupport));
                        }
                    });
                }
            });
        }
    }

    public static void callLuaQueryIventoryCallback(final String products) {
        Log.d(SIG, "callLuaqueryIventoryCallback " + products);
        if (queryIventoryCallback != -1) {
            Cocos2dxApp.getContext().runOnResume(new Runnable() {
                public void run() {
                    Cocos2dxApp.getContext().runOnGLThread(new Runnable() {
                        public void run() {
                            Cocos2dxLuaJavaBridge.callLuaFunctionWithString(IabInterface.queryIventoryCallback, products);
                        }
                    });
                }
            });
        }
    }

    public static void callLuaPurchaseCallback(final String result) {
        Log.d(SIG, "callLuaPurchaseCallback " + result);
        if (purchaseCallback != -1) {
            Cocos2dxApp.getContext().runOnResume(new Runnable() {
                public void run() {
                    Cocos2dxApp.getContext().runOnGLThread(new Runnable() {
                        public void run() {
                            Cocos2dxLuaJavaBridge.callLuaFunctionWithString(IabInterface.purchaseCallback, result);
                        }
                    });
                }
            });
        }
    }

    public static void callLuaConsumeCallback(final String result) {
        Log.d(SIG, "callLuaConsumeCallback " + result);
        if (consumeCallback != -1) {
            Cocos2dxApp.getContext().runOnResume(new Runnable() {
                public void run() {
                    Cocos2dxApp.getContext().runOnGLThread(new Runnable() {
                        public void run() {
                            Cocos2dxLuaJavaBridge.callLuaFunctionWithString(IabInterface.consumeCallback, result);
                        }
                    });
                }
            });
        }
    }
}
