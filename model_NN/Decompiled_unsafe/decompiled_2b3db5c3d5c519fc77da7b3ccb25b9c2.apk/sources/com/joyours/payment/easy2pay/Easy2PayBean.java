package com.joyours.payment.easy2pay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.android.easy2pay.Easy2Pay;
import com.android.easy2pay.Easy2PayListener;
import com.joyours.base.framework.Cocos2dxApp;
import com.joyours.base.framework.IBean;
import com.joyours.payment.easy2pay.Easy2PayInterface;
import com.mol.seaplus.sdk.psms.PSMSResponse;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint({"HandlerLeak"})
public class Easy2PayBean implements IBean, Easy2PayListener {
    public static String SIG = Easy2PayBean.class.getSimpleName();
    private static Easy2PayInterface.PayCallback payCallback;
    /* access modifiers changed from: private */
    public Easy2Pay easy2Pay;
    private boolean isInitialized = false;
    private String merchantId;
    private String secret;

    public Easy2PayBean(final String merchantId2, final String secret2) {
        this.merchantId = merchantId2;
        this.secret = secret2;
        Cocos2dxApp.getContext().getMainHandler().post(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.android.easy2pay.Easy2Pay.<init>(android.content.Context, java.lang.String, java.lang.String, int, boolean, com.android.easy2pay.Easy2Pay$Language):void
             arg types: [com.joyours.base.framework.Cocos2dxApp, java.lang.String, java.lang.String, int, int, com.android.easy2pay.Easy2Pay$Language]
             candidates:
              com.android.easy2pay.Easy2Pay.<init>(android.content.Context, java.lang.String, java.lang.String, int, java.lang.String, java.lang.String):void
              com.android.easy2pay.Easy2Pay.<init>(android.content.Context, java.lang.String, java.lang.String, int, boolean, com.android.easy2pay.Easy2Pay$Language):void */
            public void run() {
                Easy2Pay unused = Easy2PayBean.this.easy2Pay = new Easy2Pay((Context) Cocos2dxApp.getContext(), merchantId2, secret2, 60, true, Easy2Pay.Language.TH);
                Easy2PayBean.this.easy2Pay.setEasy2PayListener(this);
            }
        });
    }

    public void onCreate(Activity activity, Bundle bundle) {
    }

    public void onRestoreInstanceState(Activity activity, Bundle bundle) {
    }

    public void onStart(Activity activity) {
    }

    public void onRestart(Activity activity) {
    }

    public void onSaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onPause(Activity activity) {
    }

    public void onResume(Activity activity) {
    }

    public void onStop(Activity activity) {
    }

    public void onDestroy(Activity activity) {
    }

    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    }

    public void payBySMS(String orderId, String uid, String priceId, Easy2PayInterface.PayCallback callback) {
        payCallback = callback;
        this.easy2Pay.purchase(orderId, uid, priceId);
    }

    public boolean isInitialized() {
        return this.isInitialized;
    }

    public void onError(String orderId, String userId, String txId, int errCode, String description) {
        switch (errCode) {
            case 301:
                payCallback.call("fail=301", true);
                return;
            case 302:
                payCallback.call("fail=302", true);
                return;
            case 303:
                payCallback.call("fail=303", true);
                return;
            case 304:
                payCallback.call("fail=304", true);
                return;
            case 305:
                payCallback.call("fail=305", true);
                return;
            case 306:
                payCallback.call("fail=306", true);
                return;
            default:
                return;
        }
    }

    public void onEvent(String orderId, String userId, String txId, int eventCode, String description) {
    }

    public void onPurchaseResult(String orderId, String userId, String txId, String priceId, int purchaseCode, String description) {
        if (payCallback != null) {
            JSONObject json = new JSONObject();
            try {
                json.put("orderId", orderId);
                json.put("uid", userId);
                json.put(PSMSResponse.TRANSACTION_ID, txId);
                json.put(PSMSResponse.PRICE_ID, priceId);
                json.put("purchaseCode", purchaseCode);
            } catch (JSONException e) {
                Log.e(SIG, e.getMessage(), e);
            }
            payCallback.call(json.toString(), true);
        }
    }
}
