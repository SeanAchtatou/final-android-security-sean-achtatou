package com.joyours.payment.easy2pay;

import com.joyours.base.framework.Cocos2dxApp;
import org.cocos2dx.lib.Cocos2dxLuaJavaBridge;

public class Easy2PayInterface {
    public static String SIG = Easy2PayInterface.class.getSimpleName();
    /* access modifiers changed from: private */
    public static int payCallback = -1;

    public interface PayCallback {
        void call(String str, boolean z);
    }

    public static void initEasy2PayBean(String merchantId, String secret) {
        Cocos2dxApp.getContext().initEasy2PayBean(merchantId, secret);
    }

    public static void setPayCallback(int payCallback2) {
        if (payCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(payCallback);
            payCallback = -1;
        }
        payCallback = payCallback2;
    }

    public static void payBySMS(String orderId, String uid, String priceId) {
        Easy2PayBean Easy2PayBean = Cocos2dxApp.getContext().getEasy2PayBean();
        if (Easy2PayBean != null) {
            Easy2PayBean.payBySMS(orderId, uid, priceId, new PayCallback() {
                public void call(final String result, boolean delay) {
                    Cocos2dxApp.getContext().runOnResume(new Runnable() {
                        public void run() {
                            Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                public void run() {
                                    Cocos2dxLuaJavaBridge.callLuaFunctionWithString(Easy2PayInterface.payCallback, result);
                                }
                            }, 48);
                        }
                    });
                }
            });
        }
    }
}
