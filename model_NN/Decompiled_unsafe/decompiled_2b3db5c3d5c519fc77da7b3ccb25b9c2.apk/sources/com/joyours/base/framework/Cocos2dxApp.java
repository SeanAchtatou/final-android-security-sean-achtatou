package com.joyours.base.framework;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.PowerManager;
import android.util.Log;
import com.joyours.payment.bluepay.BluePayBean;
import com.joyours.payment.easy2pay.Easy2PayBean;
import com.joyours.payment.iab.IabBean;
import com.joyours.payment.mol.MOLBean;
import com.joyours.push.fcm.FcmPushBean;
import com.joyours.push.xg.XgPushBean;
import com.joyours.social.facebook.FacebookBean;
import com.joyours.social.line.LineBean;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.List;
import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;
import org.cocos2dx.utils.PSNative;
import org.cocos2dx.utils.PSNetwork;

public class Cocos2dxApp extends Cocos2dxActivity {
    public static String SIG = Cocos2dxApp.class.getSimpleName();
    protected static BeanManager beanManager;
    protected static String messageCode = null;
    protected Handler backgroundHandler;
    protected HandlerThread backgroundThread;
    protected BluePayBean bluePayBean;
    protected Easy2PayBean easy2PayBean;
    protected FacebookBean facebookBean;
    protected FcmPushBean fcmPushBean;
    protected IabBean iabBean;
    protected boolean isResume = false;
    protected LineBean lineBean;
    protected Handler mainHandler;
    protected MOLBean molBean;
    protected PowerManager powerManager;
    protected List<Runnable> resumeCallbacks = new ArrayList();
    protected PowerManager.WakeLock wakeLock;
    protected XgPushBean xgPushBean;

    public static Cocos2dxApp getContext() {
        return (Cocos2dxApp) Cocos2dxActivity.getContext();
    }

    public static String getMessageCode() {
        return messageCode;
    }

    public void onCreate(Bundle bundle) {
        Log.d(SIG, "onCreate");
        super.onCreate(bundle);
        messageCode = getIntent().getStringExtra("messageCode");
        PSNative.init(this);
        PSNetwork.init(this);
        this.backgroundThread = new HandlerThread(getClass().getName() + ".backgroundThread") {
            public void run() {
                final Thread.UncaughtExceptionHandler handler = Thread.getDefaultUncaughtExceptionHandler();
                Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                    public void uncaughtException(Thread thread, Throwable throwable) {
                        Log.e(Cocos2dxApp.SIG, "Thread(" + thread.getId() + ")[" + thread.getName() + "] uncaught exception:" + throwable.getMessage(), throwable);
                        if (handler != null) {
                            handler.uncaughtException(thread, throwable);
                        }
                    }
                });
                super.run();
            }
        };
        this.backgroundThread.start();
        this.backgroundHandler = new Handler(this.backgroundThread.getLooper());
        this.backgroundHandler.post(new Runnable() {
            public void run() {
                Log.d(Cocos2dxApp.SIG, "backgroundHandler " + Thread.currentThread().getId());
            }
        });
        PSNative.init(this);
        this.mainHandler = new Handler(getMainLooper());
        Log.d(SIG, "mainHandler " + Thread.currentThread().getId());
        beanManager = new BeanManager();
        this.facebookBean = new FacebookBean();
        beanManager.add(this.facebookBean);
        this.xgPushBean = new XgPushBean();
        beanManager.add(this.xgPushBean);
        this.fcmPushBean = new FcmPushBean();
        beanManager.add(this.fcmPushBean);
        this.bluePayBean = new BluePayBean();
        beanManager.add(this.bluePayBean);
        this.molBean = new MOLBean();
        beanManager.add(this.molBean);
        beanManager.onCreate(this, bundle);
    }

    public Cocos2dxGLSurfaceView onCreateView() {
        Cocos2dxGLSurfaceView glSurfaceView = new Cocos2dxGLSurfaceView(this);
        glSurfaceView.setEGLConfigChooser(5, 6, 5, 0, 16, 8);
        return glSurfaceView;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.d(SIG, "onPause");
        super.onPause();
        try {
            if (this.wakeLock != null) {
                this.wakeLock.release();
            }
        } catch (Exception e) {
            Log.e(SIG, e.getMessage(), e);
        }
        beanManager.onPause(this);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0048, code lost:
        if (r3 == null) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004c, code lost:
        if (r8.backgroundHandler == null) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004e, code lost:
        r8.backgroundHandler.postDelayed(r3, 48);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0059, code lost:
        r3.run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005e, code lost:
        android.util.Log.e(com.joyours.base.framework.Cocos2dxApp.SIG, r2.getMessage(), r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r8.powerManager = (android.os.PowerManager) getSystemService("power");
        r8.wakeLock = r8.powerManager.newWakeLock(536870922, com.joyours.base.framework.Cocos2dxApp.SIG);
        r8.wakeLock.acquire();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onResume() {
        /*
            r8 = this;
            java.lang.String r4 = com.joyours.base.framework.Cocos2dxApp.SIG
            java.lang.String r5 = "onResume"
            android.util.Log.d(r4, r5)
            r4 = 1
            r8.isResume = r4
            super.onResume()
        L_0x000d:
            r3 = 0
            java.util.List<java.lang.Runnable> r5 = r8.resumeCallbacks
            monitor-enter(r5)
            java.util.List<java.lang.Runnable> r4 = r8.resumeCallbacks     // Catch:{ all -> 0x0056 }
            boolean r4 = r4.isEmpty()     // Catch:{ all -> 0x0056 }
            if (r4 == 0) goto L_0x003c
            monitor-exit(r5)     // Catch:{ all -> 0x0056 }
            java.lang.String r4 = "power"
            java.lang.Object r4 = r8.getSystemService(r4)     // Catch:{ Exception -> 0x005d }
            android.os.PowerManager r4 = (android.os.PowerManager) r4     // Catch:{ Exception -> 0x005d }
            r8.powerManager = r4     // Catch:{ Exception -> 0x005d }
            android.os.PowerManager r4 = r8.powerManager     // Catch:{ Exception -> 0x005d }
            r5 = 536870922(0x2000000a, float:1.0842035E-19)
            java.lang.String r6 = com.joyours.base.framework.Cocos2dxApp.SIG     // Catch:{ Exception -> 0x005d }
            android.os.PowerManager$WakeLock r4 = r4.newWakeLock(r5, r6)     // Catch:{ Exception -> 0x005d }
            r8.wakeLock = r4     // Catch:{ Exception -> 0x005d }
            android.os.PowerManager$WakeLock r4 = r8.wakeLock     // Catch:{ Exception -> 0x005d }
            r4.acquire()     // Catch:{ Exception -> 0x005d }
        L_0x0036:
            com.joyours.base.framework.BeanManager r4 = com.joyours.base.framework.Cocos2dxApp.beanManager
            r4.onResume(r8)
            return
        L_0x003c:
            java.util.List<java.lang.Runnable> r4 = r8.resumeCallbacks     // Catch:{ all -> 0x0056 }
            r6 = 0
            java.lang.Object r4 = r4.remove(r6)     // Catch:{ all -> 0x0056 }
            r0 = r4
            java.lang.Runnable r0 = (java.lang.Runnable) r0     // Catch:{ all -> 0x0056 }
            r3 = r0
            monitor-exit(r5)     // Catch:{ all -> 0x0056 }
            if (r3 == 0) goto L_0x000d
            android.os.Handler r4 = r8.backgroundHandler
            if (r4 == 0) goto L_0x0059
            android.os.Handler r4 = r8.backgroundHandler
            r6 = 48
            r4.postDelayed(r3, r6)
            goto L_0x000d
        L_0x0056:
            r4 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0056 }
            throw r4
        L_0x0059:
            r3.run()
            goto L_0x000d
        L_0x005d:
            r2 = move-exception
            java.lang.String r4 = com.joyours.base.framework.Cocos2dxApp.SIG
            java.lang.String r5 = r2.getMessage()
            android.util.Log.e(r4, r5, r2)
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.joyours.base.framework.Cocos2dxApp.onResume():void");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Log.d(SIG, "onStop");
        this.isResume = false;
        super.onStop();
        beanManager.onStop(this);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.d(SIG, "onDestroy");
        this.isResume = false;
        this.backgroundThread.quit();
        this.backgroundThread = null;
        super.onDestroy();
        beanManager.onDestroy(this);
    }

    public Handler getBackgroundHandler() {
        return this.backgroundHandler;
    }

    public Handler getMainHandler() {
        return this.mainHandler;
    }

    public FacebookBean getFacebookBean() {
        if (this.facebookBean == null) {
            Log.d(SIG, "FacebookBean not initialize.");
        }
        return this.facebookBean;
    }

    public void runOnResume(Runnable runnable) {
        if (this.isResume) {
            runnable.run();
            return;
        }
        synchronized (this.resumeCallbacks) {
            this.resumeCallbacks.add(runnable);
        }
    }

    public void runOnGLThreadDelay(final Runnable runnable, long delayMillis) {
        final Cocos2dxApp context = getContext();
        if (context != null) {
            context.getMainHandler().postDelayed(new Runnable() {
                public void run() {
                    context.runOnGLThread(runnable);
                }
            }, delayMillis);
        }
    }

    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
    }

    public void onStart() {
        super.onStart();
        beanManager.onStart(this);
    }

    public void onRestart() {
        super.onRestart();
        beanManager.onRestart(this);
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        beanManager.onSaveInstanceState(this, bundle);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        beanManager.onActivityResult(this, requestCode, resultCode, intent);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        beanManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public BeanManager getBeanManager() {
        return beanManager;
    }

    public XgPushBean getXgPushBean() {
        if (this.xgPushBean == null) {
            Log.d(SIG, "XgPushBean not initialize.");
        }
        return this.xgPushBean;
    }

    public FcmPushBean getFcmPushBean() {
        if (this.fcmPushBean == null) {
            Log.d(SIG, "FcmPushBean not initialize.");
        }
        return this.fcmPushBean;
    }

    public void initIabBean(String base64EncodePublicKey) {
        this.iabBean = new IabBean(base64EncodePublicKey, true);
        beanManager.add(this.iabBean);
    }

    public void initDataEyeBean() {
    }

    public void initEasy2PayBean(String merchantId, String secret) {
        this.easy2PayBean = new Easy2PayBean(merchantId, secret);
        beanManager.add(this.easy2PayBean);
    }

    public IabBean getIabBean() {
        if (this.iabBean == null) {
            Log.d(SIG, "IabBean not initialize.");
        }
        return this.iabBean;
    }

    public BluePayBean getBluePayBean() {
        if (this.bluePayBean == null) {
            Log.d(SIG, "BluePayBean not initialize.");
        }
        return this.bluePayBean;
    }

    public Easy2PayBean getEasy2PayBean() {
        if (this.easy2PayBean == null) {
            Log.d(SIG, "Easy2PayBean not initialize.");
        }
        return this.easy2PayBean;
    }

    public MOLBean getMOLBean() {
        if (this.molBean == null) {
            Log.d(SIG, "molBean not initialize.");
        }
        return this.molBean;
    }

    public LineBean getLineBean() {
        if (this.lineBean == null) {
            Log.d(SIG, "LineBean not initialize.");
        }
        return this.lineBean;
    }
}
