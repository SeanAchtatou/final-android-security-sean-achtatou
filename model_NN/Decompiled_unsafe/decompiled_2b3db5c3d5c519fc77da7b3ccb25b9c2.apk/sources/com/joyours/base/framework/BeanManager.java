package com.joyours.base.framework;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import java.util.ArrayList;

public class BeanManager implements IBean {
    protected ArrayList<IBean> beanList = new ArrayList<>();

    public void add(IBean bean) {
        this.beanList.add(bean);
    }

    public void remove(IBean bean) {
        this.beanList.remove(bean);
    }

    public void onCreate(Activity activity, Bundle bundle) {
        for (int i = 0; i < this.beanList.size(); i++) {
            this.beanList.get(i).onCreate(activity, bundle);
        }
    }

    public void onRestoreInstanceState(Activity activity, Bundle bundle) {
        for (int i = 0; i < this.beanList.size(); i++) {
            this.beanList.get(i).onRestoreInstanceState(activity, bundle);
        }
    }

    public void onStart(Activity activity) {
        for (int i = 0; i < this.beanList.size(); i++) {
            this.beanList.get(i).onStart(activity);
        }
    }

    public void onRestart(Activity activity) {
        for (int i = 0; i < this.beanList.size(); i++) {
            this.beanList.get(i).onRestart(activity);
        }
    }

    public void onSaveInstanceState(Activity activity, Bundle bundle) {
        for (int i = 0; i < this.beanList.size(); i++) {
            this.beanList.get(i).onRestoreInstanceState(activity, bundle);
        }
    }

    public void onPause(Activity activity) {
        for (int i = 0; i < this.beanList.size(); i++) {
            this.beanList.get(i).onPause(activity);
        }
    }

    public void onResume(Activity activity) {
        for (int i = 0; i < this.beanList.size(); i++) {
            this.beanList.get(i).onResume(activity);
        }
    }

    public void onStop(Activity activity) {
        for (int i = 0; i < this.beanList.size(); i++) {
            this.beanList.get(i).onStop(activity);
        }
    }

    public void onDestroy(Activity activity) {
        for (int i = 0; i < this.beanList.size(); i++) {
            this.beanList.get(i).onDestroy(activity);
        }
    }

    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
        for (int i = 0; i < this.beanList.size(); i++) {
            this.beanList.get(i).onActivityResult(activity, requestCode, resultCode, intent);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        for (int i = 0; i < this.beanList.size(); i++) {
            this.beanList.get(i).onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
