package com.joyours.base.framework;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public interface IBean {
    void onActivityResult(Activity activity, int i, int i2, Intent intent);

    void onCreate(Activity activity, Bundle bundle);

    void onDestroy(Activity activity);

    void onPause(Activity activity);

    void onRequestPermissionsResult(int i, String[] strArr, int[] iArr);

    void onRestart(Activity activity);

    void onRestoreInstanceState(Activity activity, Bundle bundle);

    void onResume(Activity activity);

    void onSaveInstanceState(Activity activity, Bundle bundle);

    void onStart(Activity activity);

    void onStop(Activity activity);
}
