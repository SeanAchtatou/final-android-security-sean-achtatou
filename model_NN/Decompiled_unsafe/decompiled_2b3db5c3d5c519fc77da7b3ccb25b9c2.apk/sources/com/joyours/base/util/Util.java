package com.joyours.base.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import com.bluepay.data.Config;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.internal.ServerProtocol;
import com.joyours.base.framework.Cocos2dxApp;
import com.joyours.base.framework.IBean;
import com.tencent.android.tpush.common.MessageKey;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Stack;
import org.cocos2dx.lib.Cocos2dxLuaJavaBridge;
import org.json.JSONException;
import org.json.JSONObject;

public class Util {
    private static final String DEFAULT_MAC_ADDRESS = "02:00:00:00:00:00";
    protected static int FEEDBACK_PIC_CROP_REQUEST = 23363350;
    protected static int FEEDBACK_PIC_REQUEST = 23363349;
    protected static int HEAD_PIC_CROP_REQUEST = 23363348;
    protected static int HEAD_PIC_REQUEST = 23363347;
    private static final int NETWORK_AVAILABLE = 0;
    private static final int NETWORK_DISABLED = 1;
    protected static int READ_EXTERNAL_STORAGE_REQUEST = 23353350;
    public static String SIG = Util.class.getSimpleName();
    private static final String ZERO_MAC_ADDRESS = "00:00:00:00:00:00";
    private static char[] charSet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
    protected static final IBean getFeedbackPicBean = new IBean() {
        public void onCreate(Activity activity, Bundle bundle) {
        }

        public void onRestoreInstanceState(Activity activity, Bundle bundle) {
        }

        public void onStart(Activity activity) {
        }

        public void onRestart(Activity activity) {
        }

        public void onSaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onPause(Activity activity) {
        }

        public void onResume(Activity activity) {
        }

        public void onStop(Activity activity) {
        }

        public void onDestroy(Activity activity) {
        }

        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        }

        @SuppressLint({"NewApi"})
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
            if (resultCode != 0 && requestCode == Util.FEEDBACK_PIC_REQUEST) {
                Uri uri = intent.getData();
                Cocos2dxApp context = Cocos2dxApp.getContext();
                Log.d(Util.SIG, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Uri: " + uri);
                final String path = Util.getFilePathFromUri(uri);
                Log.d(Util.SIG, "uri:" + uri + ",path:" + path);
                if (Util.getFeedbackPicCallback != -1) {
                    Cocos2dxApp.getContext().getBackgroundHandler().post(new Runnable() {
                        /* JADX WARNING: Removed duplicated region for block: B:31:0x008d A[SYNTHETIC, Splitter:B:31:0x008d] */
                        /* JADX WARNING: Removed duplicated region for block: B:38:0x009f A[SYNTHETIC, Splitter:B:38:0x009f] */
                        /* JADX WARNING: Removed duplicated region for block: B:43:0x00a8 A[SYNTHETIC, Splitter:B:43:0x00a8] */
                        /* JADX WARNING: Unknown top exception splitter block from list: {B:28:0x0082=Splitter:B:28:0x0082, B:35:0x0094=Splitter:B:35:0x0094} */
                        /* Code decompiled incorrectly, please refer to instructions dump. */
                        public void run() {
                            /*
                                r15 = this;
                                android.graphics.BitmapFactory$Options r8 = new android.graphics.BitmapFactory$Options
                                r8.<init>()
                                r13 = 1
                                r8.inJustDecodeBounds = r13
                                r15.BitmapFactory()
                                java.lang.String r13 = r2
                                android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeFile(r13, r8)
                                r13 = 0
                                r8.inJustDecodeBounds = r13
                                int r10 = r8.outWidth
                                int r9 = r8.outHeight
                                r6 = 1142947840(0x44200000, float:640.0)
                                r12 = 1142947840(0x44200000, float:640.0)
                                r11 = 1
                                if (r10 <= r9) goto L_0x0073
                                float r13 = (float) r10
                                int r13 = (r13 > r12 ? 1 : (r13 == r12 ? 0 : -1))
                                if (r13 <= 0) goto L_0x0073
                                float r13 = (float) r10
                                float r13 = r13 / r12
                                int r11 = (int) r13
                            L_0x0027:
                                if (r11 > 0) goto L_0x002a
                                r11 = 1
                            L_0x002a:
                                r8.inSampleSize = r11
                                java.lang.String r13 = r2
                                android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeFile(r13, r8)
                                java.io.File r2 = new java.io.File
                                java.io.File r13 = android.os.Environment.getExternalStorageDirectory()
                                java.lang.String r14 = "/tmp_upload_img.jpg"
                                r2.<init>(r13, r14)
                                boolean r13 = r2.exists()
                                if (r13 == 0) goto L_0x0046
                                r2.delete()
                            L_0x0046:
                                r3 = 0
                                java.lang.String r7 = r2
                                java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0081, IOException -> 0x0093 }
                                r4.<init>(r2)     // Catch:{ FileNotFoundException -> 0x0081, IOException -> 0x0093 }
                                android.graphics.Bitmap$CompressFormat r13 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ FileNotFoundException -> 0x00b4, IOException -> 0x00b1, all -> 0x00ae }
                                r14 = 40
                                r0.compress(r13, r14, r4)     // Catch:{ FileNotFoundException -> 0x00b4, IOException -> 0x00b1, all -> 0x00ae }
                                r4.flush()     // Catch:{ FileNotFoundException -> 0x00b4, IOException -> 0x00b1, all -> 0x00ae }
                                java.lang.String r7 = r2.getAbsolutePath()     // Catch:{ FileNotFoundException -> 0x00b4, IOException -> 0x00b1, all -> 0x00ae }
                                r2.deleteOnExit()     // Catch:{ FileNotFoundException -> 0x00b4, IOException -> 0x00b1, all -> 0x00ae }
                                if (r4 == 0) goto L_0x00b7
                                r4.close()     // Catch:{ Exception -> 0x007e }
                                r3 = r4
                            L_0x0065:
                                r5 = r7
                                com.joyours.base.framework.Cocos2dxApp r13 = com.joyours.base.framework.Cocos2dxApp.getContext()
                                com.joyours.base.util.Util$2$1$1 r14 = new com.joyours.base.util.Util$2$1$1
                                r14.<init>(r5)
                                r13.runOnResume(r14)
                                return
                            L_0x0073:
                                if (r10 >= r9) goto L_0x0027
                                float r13 = (float) r9
                                int r13 = (r13 > r6 ? 1 : (r13 == r6 ? 0 : -1))
                                if (r13 <= 0) goto L_0x0027
                                float r13 = (float) r9
                                float r13 = r13 / r6
                                int r11 = (int) r13
                                goto L_0x0027
                            L_0x007e:
                                r13 = move-exception
                                r3 = r4
                                goto L_0x0065
                            L_0x0081:
                                r1 = move-exception
                            L_0x0082:
                                java.lang.String r13 = com.joyours.base.util.Util.SIG     // Catch:{ all -> 0x00a5 }
                                java.lang.String r14 = r1.getMessage()     // Catch:{ all -> 0x00a5 }
                                android.util.Log.e(r13, r14, r1)     // Catch:{ all -> 0x00a5 }
                                if (r3 == 0) goto L_0x0065
                                r3.close()     // Catch:{ Exception -> 0x0091 }
                                goto L_0x0065
                            L_0x0091:
                                r13 = move-exception
                                goto L_0x0065
                            L_0x0093:
                                r1 = move-exception
                            L_0x0094:
                                java.lang.String r13 = com.joyours.base.util.Util.SIG     // Catch:{ all -> 0x00a5 }
                                java.lang.String r14 = r1.getMessage()     // Catch:{ all -> 0x00a5 }
                                android.util.Log.e(r13, r14, r1)     // Catch:{ all -> 0x00a5 }
                                if (r3 == 0) goto L_0x0065
                                r3.close()     // Catch:{ Exception -> 0x00a3 }
                                goto L_0x0065
                            L_0x00a3:
                                r13 = move-exception
                                goto L_0x0065
                            L_0x00a5:
                                r13 = move-exception
                            L_0x00a6:
                                if (r3 == 0) goto L_0x00ab
                                r3.close()     // Catch:{ Exception -> 0x00ac }
                            L_0x00ab:
                                throw r13
                            L_0x00ac:
                                r14 = move-exception
                                goto L_0x00ab
                            L_0x00ae:
                                r13 = move-exception
                                r3 = r4
                                goto L_0x00a6
                            L_0x00b1:
                                r1 = move-exception
                                r3 = r4
                                goto L_0x0094
                            L_0x00b4:
                                r1 = move-exception
                                r3 = r4
                                goto L_0x0082
                            L_0x00b7:
                                r3 = r4
                                goto L_0x0065
                            */
                            throw new UnsupportedOperationException("Method not decompiled: com.joyours.base.util.Util.AnonymousClass2.AnonymousClass1.run():void");
                        }

                        private BitmapFactory BitmapFactory() {
                            return null;
                        }
                    });
                }
            }
        }
    };
    protected static int getFeedbackPicCallback = -1;
    protected static final IBean getHeadPicBean = new IBean() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
            if (Build.VERSION.SDK_INT >= 19) {
            }
            Log.d(Util.SIG, "onActivityResultk, requestCode=" + requestCode);
            if (resultCode != 0 && requestCode == Util.HEAD_PIC_REQUEST) {
                Uri selectedImage = intent.getData();
                if (selectedImage != null) {
                    File pic = new File(Util.getFilePathFromUri(selectedImage));
                    Intent cropIntent = new Intent("com.android.camera.action.CROP");
                    cropIntent.setDataAndType(Uri.fromFile(pic), "image/jpeg");
                    cropIntent.putExtra("crop", ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
                    cropIntent.putExtra("aspectX", 1);
                    cropIntent.putExtra("aspectY", 1);
                    cropIntent.putExtra("outputX", 150);
                    cropIntent.putExtra("outputY", 150);
                    cropIntent.putExtra("scale", true);
                    cropIntent.putExtra("return-data", false);
                    cropIntent.putExtra("output", Util.getHeadPicTempFileUri());
                    cropIntent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
                    cropIntent.putExtra("noFaceDetection", true);
                    activity.startActivityForResult(cropIntent, Util.HEAD_PIC_CROP_REQUEST);
                }
            } else if (resultCode == 0 || requestCode != Util.HEAD_PIC_CROP_REQUEST) {
                if (resultCode != 0) {
                    return;
                }
                if (requestCode == Util.HEAD_PIC_CROP_REQUEST || requestCode == Util.HEAD_PIC_REQUEST || requestCode == Util.FEEDBACK_PIC_REQUEST) {
                    Cocos2dxApp.getContext().getBeanManager().remove(Util.getHeadPicBean);
                }
            } else if (Util.headPicTempFile == null || !Util.headPicTempFile.exists() || !Util.headPicTempFile.isFile()) {
                Log.d(Util.SIG, "headPicTempFile not found!");
                if (Util.getHeadPicCallback != -1) {
                    Cocos2dxApp.getContext().runOnResume(new Runnable() {
                        public void run() {
                            Cocos2dxApp.getContext().runOnGLThread(new Runnable() {
                                public void run() {
                                    Log.d(Util.SIG, "Call getPicCallback " + Util.getHeadPicCallback + " onError");
                                    Cocos2dxLuaJavaBridge.callLuaFunctionWithString(Util.getHeadPicCallback, "error");
                                }
                            });
                        }
                    });
                }
                Cocos2dxApp.getContext().getBeanManager().remove(Util.getHeadPicBean);
            } else {
                Log.d(Util.SIG, "headPicTempFile,path=" + Util.headPicTempFile.getAbsolutePath());
                if (Util.getHeadPicCallback != -1) {
                    Cocos2dxApp.getContext().runOnResume(new Runnable() {
                        public void run() {
                            Cocos2dxApp.getContext().runOnGLThreadDelay(new Runnable() {
                                public void run() {
                                    Log.d(Util.SIG, "Call getPicCallback " + Util.getHeadPicCallback + ",path=" + Util.headPicTempFile.getAbsolutePath() + ",exists=" + Util.headPicTempFile.exists());
                                    Cocos2dxLuaJavaBridge.callLuaFunctionWithString(Util.getHeadPicCallback, Util.headPicTempFile.getAbsolutePath());
                                }
                            }, 256);
                        }
                    });
                }
                Cocos2dxApp.getContext().getBeanManager().remove(Util.getHeadPicBean);
            }
        }

        public void onCreate(Activity activity, Bundle bundle) {
        }

        public void onRestoreInstanceState(Activity activity, Bundle bundle) {
        }

        public void onStart(Activity activity) {
        }

        public void onRestart(Activity activity) {
        }

        public void onSaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onPause(Activity activity) {
        }

        public void onResume(Activity activity) {
        }

        public void onStop(Activity activity) {
        }

        public void onDestroy(Activity activity) {
        }

        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        }
    };
    protected static int getHeadPicCallback = -1;
    protected static File headPicTempFile;
    private static String macAddress = null;
    protected static int networkStateCallback = -1;
    protected static int wifiStateCallback = -1;

    public static String getVersion() {
        try {
            Context context = Cocos2dxApp.getContext();
            if (context != null) {
                return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static int getAppVersionCode() {
        try {
            Context context = Cocos2dxApp.getContext();
            if (context != null) {
                return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return 1;
    }

    public static String getAppPackageName() {
        Context context = Cocos2dxApp.getContext();
        if (context != null) {
            return context.getPackageName();
        }
        return "";
    }

    public static void vibrate(int time) {
        Context context = Cocos2dxApp.getContext();
        if (context != null) {
            Vibrator vibrator = (Vibrator) context.getSystemService("vibrator");
            if (vibrator != null) {
                vibrator.vibrate((long) time);
            } else {
                Log.e(SIG, "VIBRATOR_SERVICE not availiable.");
            }
        }
    }

    public static void gotoMarket() {
        Context context = Cocos2dxApp.getContext();
        if (context != null) {
            String appPackageName = context.getPackageName();
            try {
                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + appPackageName)));
            } catch (ActivityNotFoundException e) {
                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        }
    }

    public static String getFixedWidthText(String font, int size, String text, int width) {
        if (text == null || text.length() <= 3) {
            return text;
        }
        try {
            Paint paint = new Paint();
            paint.setTextSize((float) size);
            if (!TextUtils.isEmpty(font)) {
                paint.setTypeface(Typeface.create(font, 0));
            } else {
                paint.setTypeface(Typeface.DEFAULT);
            }
            if (paint.measureText(text) <= ((float) width)) {
                return text;
            }
            return text.substring(0, Math.min(Math.max(new Float((((float) width) / paint.measureText(text)) * ((float) text.length())).intValue(), 0), text.length() - 1)) + "..";
        } catch (Exception e) {
            return text;
        }
    }

    public static void getHeadPic(int callback) {
        if (getHeadPicCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(getHeadPicCallback);
            getHeadPicCallback = -1;
        }
        getHeadPicCallback = callback;
        getPic(callback, getHeadPicBean, true);
    }

    protected static Uri getHeadPicTempFileUri() {
        if (!isSdCardMounted()) {
            return null;
        }
        try {
            headPicTempFile = new File(Environment.getExternalStorageDirectory(), "tmp_avatar_" + System.currentTimeMillis() + ".jpg");
            if (headPicTempFile.exists()) {
                headPicTempFile.delete();
            }
            Uri uri = Uri.fromFile(headPicTempFile);
            Log.d(SIG, "Temp file is " + uri.getPath());
            return uri;
        } catch (Exception e) {
            Log.d(SIG, e.getMessage());
            return null;
        }
    }

    public static void getFeedbackPic(int callback) {
        if (getFeedbackPicCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(getFeedbackPicCallback);
            getFeedbackPicCallback = -1;
        }
        getFeedbackPicCallback = callback;
        getPic(callback, getFeedbackPicBean, true);
    }

    /* access modifiers changed from: private */
    public static String getFilePathFromUri(Uri uri) {
        boolean isKitKat;
        if (Build.VERSION.SDK_INT >= 19) {
            isKitKat = true;
        } else {
            isKitKat = false;
        }
        Context context = Cocos2dxApp.getContext();
        if (!isKitKat || !DocumentsContract.isDocumentUri(context, uri)) {
            if (MessageKey.MSG_CONTENT.equalsIgnoreCase(uri.getScheme())) {
                if (isGooglePhotosUri(uri)) {
                    String filePath = uri.getLastPathSegment();
                }
                return getDataColumn(context, uri, null, null);
            } else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            } else {
                return null;
            }
        } else if (isExternalStorageDocument(uri)) {
            String[] split = DocumentsContract.getDocumentId(uri).split(":");
            if ("primary".equalsIgnoreCase(split[0])) {
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            }
            return null;
        } else if (isDownloadsDocument(uri)) {
            return getDataColumn(context, ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(DocumentsContract.getDocumentId(uri)).longValue()), null, null);
        } else if (!isMediaDocument(uri)) {
            return null;
        } else {
            String[] split2 = DocumentsContract.getDocumentId(uri).split(":");
            String type = split2[0];
            Uri contentUri = null;
            if ("image".equals(type)) {
                contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            } else if (AnalyticsEvents.PARAMETER_SHARE_DIALOG_CONTENT_VIDEO.equals(type)) {
                contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            } else if ("audio".equals(type)) {
                contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            }
            return getDataColumn(context, contentUri, "_id=?", new String[]{split2[1]});
        }
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri, new String[]{"_data"}, selection, selectionArgs, null);
            if (cursor == null || !cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return null;
            }
            String string = cursor.getString(cursor.getColumnIndexOrThrow("_data"));
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static boolean getPic(int callback, final IBean bean, boolean isCrop) {
        if (!canReadExternalStorage()) {
            return false;
        }
        if (isSdCardMounted()) {
            final Intent intent = new Intent("android.intent.action.GET_CONTENT", (Uri) null);
            intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
            Cocos2dxApp.getContext().getBeanManager().remove(bean);
            Cocos2dxApp.getContext().getBeanManager().add(bean);
            Cocos2dxApp.getContext().getMainHandler().postDelayed(new Runnable() {
                public void run() {
                    Cocos2dxApp context = Cocos2dxApp.getContext();
                    if (context == null) {
                        return;
                    }
                    if (bean == Util.getHeadPicBean) {
                        Log.d(Util.SIG, "static getPic getHeadPicBean");
                        context.startActivityForResult(intent, Util.HEAD_PIC_REQUEST);
                        return;
                    }
                    context.startActivityForResult(intent, Util.FEEDBACK_PIC_REQUEST);
                }
            }, 48);
            return true;
        }
        Log.d(SIG, "SD Card not found.");
        if (callback == -1) {
            return false;
        }
        Cocos2dxLuaJavaBridge.callLuaFunctionWithString(callback, "nosdcard");
        Cocos2dxLuaJavaBridge.releaseLuaFunction(callback);
        return false;
    }

    protected static boolean canReadExternalStorage() {
        if (Build.VERSION.SDK_INT < 23) {
            return true;
        }
        if (ContextCompat.checkSelfPermission(Cocos2dxApp.getContext(), "android.permission.READ_EXTERNAL_STORAGE") == 0) {
            return true;
        }
        ActivityCompat.requestPermissions(Cocos2dxApp.getContext(), new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, READ_EXTERNAL_STORAGE_REQUEST);
        return false;
    }

    protected static boolean isSdCardMounted() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return true;
        }
        return false;
    }

    public static void setWifiStateChangeCallback(int callback) {
        if (wifiStateCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(wifiStateCallback);
            wifiStateCallback = -1;
        }
        wifiStateCallback = callback;
    }

    public static void setNetworkStateChangeCallback(int callback) {
        if (networkStateCallback != -1) {
            Cocos2dxLuaJavaBridge.releaseLuaFunction(networkStateCallback);
            networkStateCallback = -1;
        }
        networkStateCallback = callback;
    }

    public static void onWifiStateChangedToLua(int wifiState) {
        if (wifiStateCallback != -1) {
            String ret = "closed";
            if (wifiState == 3) {
                ret = "open";
            }
            Cocos2dxLuaJavaBridge.callLuaFunctionWithString(wifiStateCallback, ret);
        }
    }

    public static void onNetworkStateChangedToLua() {
        if (networkStateCallback != -1) {
            Cocos2dxLuaJavaBridge.callLuaFunctionWithString(networkStateCallback, String.valueOf(getNetworkState(Cocos2dxApp.getContext())));
        }
    }

    public static int getNetworkState(Context context) {
        NetworkInfo networkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null || (networkInfo = connectivityManager.getActiveNetworkInfo()) == null || !networkInfo.isConnected()) {
            return 1;
        }
        return 0;
    }

    public static String getMacAddress() {
        Context context;
        if ((TextUtils.isEmpty(macAddress) || ZERO_MAC_ADDRESS.equals(macAddress) || DEFAULT_MAC_ADDRESS.equals(macAddress)) && (context = Cocos2dxApp.getContext()) != null) {
            if (TextUtils.isEmpty(macAddress) || ZERO_MAC_ADDRESS.equals(macAddress) || DEFAULT_MAC_ADDRESS.equals(macAddress)) {
                macAddress = getMacAddressFromWifiManager(context);
            }
            if (TextUtils.isEmpty(macAddress) || ZERO_MAC_ADDRESS.equals(macAddress) || DEFAULT_MAC_ADDRESS.equals(macAddress)) {
                macAddress = getMacAddressFromShell();
                Log.d(SIG, "getMacAddressFromShell:" + macAddress);
            }
            if (TextUtils.isEmpty(macAddress) || ZERO_MAC_ADDRESS.equals(macAddress) || DEFAULT_MAC_ADDRESS.equals(macAddress)) {
                macAddress = getMacAddressFromNetworkInterface("wlan0");
                Log.d(SIG, "getMacAddressFromNetworkInterface:" + macAddress);
            }
            if (ZERO_MAC_ADDRESS.equals(macAddress) || DEFAULT_MAC_ADDRESS.equals(macAddress)) {
                macAddress = null;
            }
        }
        if (macAddress == null) {
            macAddress = "";
        }
        return macAddress;
    }

    private static String getMacAddressFromWifiManager(Context context) {
        WifiInfo connectionInfo;
        try {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Config.NETWORKTYPE_WIFI);
            if (!(wifiManager == null || (connectionInfo = wifiManager.getConnectionInfo()) == null)) {
                String macAddress2 = connectionInfo.getMacAddress();
                if (!TextUtils.isEmpty(macAddress2) && !ZERO_MAC_ADDRESS.equals(macAddress2) && !DEFAULT_MAC_ADDRESS.equals(macAddress2)) {
                    return macAddress2;
                }
                boolean isTurnOff = false;
                try {
                    int wifiState = wifiManager.getWifiState();
                    if (!(wifiState == 3 || wifiState == 2)) {
                        isTurnOff = true;
                        wifiManager.setWifiEnabled(true);
                    }
                    WifiInfo connectionInfo2 = wifiManager.getConnectionInfo();
                    if (connectionInfo2 != null) {
                        String macAddress3 = connectionInfo2.getMacAddress();
                        if (!TextUtils.isEmpty(macAddress3) && !ZERO_MAC_ADDRESS.equals(macAddress3) && !DEFAULT_MAC_ADDRESS.equals(macAddress3)) {
                            return isTurnOff ? macAddress3 : macAddress3;
                        }
                        String macAddress4 = getMacAddressFromShell();
                        if (isTurnOff) {
                        }
                        return macAddress4;
                    } else if (isTurnOff) {
                    }
                } catch (Exception e) {
                    Log.e(SIG, e.getMessage(), e);
                    if (0 != 0) {
                    }
                } catch (Throwable th) {
                    if (0 != 0) {
                    }
                    throw th;
                }
            }
        } catch (Exception e2) {
            Log.e(SIG, e2.getMessage(), e2);
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x005f A[SYNTHETIC, Splitter:B:37:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0064 A[SYNTHETIC, Splitter:B:40:0x0064] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0069 A[SYNTHETIC, Splitter:B:43:0x0069] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0072 A[SYNTHETIC, Splitter:B:48:0x0072] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0077 A[SYNTHETIC, Splitter:B:51:0x0077] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x007c A[SYNTHETIC, Splitter:B:54:0x007c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String getMacAddressFromShell() {
        /*
            r1 = 0
            r3 = 0
            r6 = 0
            java.lang.Runtime r7 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x0053 }
            java.lang.String r8 = "cat /sys/class/net/wlan0/address "
            java.lang.Process r6 = r7.exec(r8)     // Catch:{ Exception -> 0x0053 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0053 }
            java.io.InputStream r7 = r6.getInputStream()     // Catch:{ Exception -> 0x0053 }
            r2.<init>(r7)     // Catch:{ Exception -> 0x0053 }
            java.io.LineNumberReader r4 = new java.io.LineNumberReader     // Catch:{ Exception -> 0x009b, all -> 0x0094 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x009b, all -> 0x0094 }
        L_0x001b:
            java.lang.String r5 = r4.readLine()     // Catch:{ Exception -> 0x009e, all -> 0x0097 }
            if (r5 == 0) goto L_0x003c
            java.lang.String r5 = r5.trim()     // Catch:{ Exception -> 0x009e, all -> 0x0097 }
            boolean r7 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Exception -> 0x009e, all -> 0x0097 }
            if (r7 != 0) goto L_0x001b
            if (r4 == 0) goto L_0x0030
            r4.close()     // Catch:{ Exception -> 0x0080 }
        L_0x0030:
            if (r2 == 0) goto L_0x0035
            r2.close()     // Catch:{ Exception -> 0x0082 }
        L_0x0035:
            if (r6 == 0) goto L_0x003a
            r6.destroy()     // Catch:{ Exception -> 0x0084 }
        L_0x003a:
            r3 = r4
        L_0x003b:
            return r5
        L_0x003c:
            if (r4 == 0) goto L_0x0041
            r4.close()     // Catch:{ Exception -> 0x0086 }
        L_0x0041:
            if (r2 == 0) goto L_0x0046
            r2.close()     // Catch:{ Exception -> 0x0088 }
        L_0x0046:
            if (r6 == 0) goto L_0x00a2
            r6.destroy()     // Catch:{ Exception -> 0x004f }
            r3 = r4
            r1 = r2
        L_0x004d:
            r5 = 0
            goto L_0x003b
        L_0x004f:
            r7 = move-exception
            r3 = r4
            r1 = r2
            goto L_0x004d
        L_0x0053:
            r0 = move-exception
        L_0x0054:
            java.lang.String r7 = com.joyours.base.util.Util.SIG     // Catch:{ all -> 0x006f }
            java.lang.String r8 = r0.getMessage()     // Catch:{ all -> 0x006f }
            android.util.Log.e(r7, r8, r0)     // Catch:{ all -> 0x006f }
            if (r3 == 0) goto L_0x0062
            r3.close()     // Catch:{ Exception -> 0x008a }
        L_0x0062:
            if (r1 == 0) goto L_0x0067
            r1.close()     // Catch:{ Exception -> 0x008c }
        L_0x0067:
            if (r6 == 0) goto L_0x004d
            r6.destroy()     // Catch:{ Exception -> 0x006d }
            goto L_0x004d
        L_0x006d:
            r7 = move-exception
            goto L_0x004d
        L_0x006f:
            r7 = move-exception
        L_0x0070:
            if (r3 == 0) goto L_0x0075
            r3.close()     // Catch:{ Exception -> 0x008e }
        L_0x0075:
            if (r1 == 0) goto L_0x007a
            r1.close()     // Catch:{ Exception -> 0x0090 }
        L_0x007a:
            if (r6 == 0) goto L_0x007f
            r6.destroy()     // Catch:{ Exception -> 0x0092 }
        L_0x007f:
            throw r7
        L_0x0080:
            r7 = move-exception
            goto L_0x0030
        L_0x0082:
            r7 = move-exception
            goto L_0x0035
        L_0x0084:
            r7 = move-exception
            goto L_0x003a
        L_0x0086:
            r7 = move-exception
            goto L_0x0041
        L_0x0088:
            r7 = move-exception
            goto L_0x0046
        L_0x008a:
            r7 = move-exception
            goto L_0x0062
        L_0x008c:
            r7 = move-exception
            goto L_0x0067
        L_0x008e:
            r8 = move-exception
            goto L_0x0075
        L_0x0090:
            r8 = move-exception
            goto L_0x007a
        L_0x0092:
            r8 = move-exception
            goto L_0x007f
        L_0x0094:
            r7 = move-exception
            r1 = r2
            goto L_0x0070
        L_0x0097:
            r7 = move-exception
            r3 = r4
            r1 = r2
            goto L_0x0070
        L_0x009b:
            r0 = move-exception
            r1 = r2
            goto L_0x0054
        L_0x009e:
            r0 = move-exception
            r3 = r4
            r1 = r2
            goto L_0x0054
        L_0x00a2:
            r3 = r4
            r1 = r2
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.joyours.base.util.Util.getMacAddressFromShell():java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0050 A[SYNTHETIC, Splitter:B:21:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0055 A[SYNTHETIC, Splitter:B:24:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005a A[SYNTHETIC, Splitter:B:27:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0062 A[SYNTHETIC, Splitter:B:32:0x0062] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0067 A[SYNTHETIC, Splitter:B:35:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x006c A[SYNTHETIC, Splitter:B:38:0x006c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getDeviceNetworkInfo() {
        /*
            r1 = 0
            r3 = 0
            r4 = 0
            java.lang.Runtime r7 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x0044 }
            java.lang.String r8 = "ifconfig"
            java.lang.Process r4 = r7.exec(r8)     // Catch:{ Exception -> 0x0044 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0044 }
            java.io.InputStream r7 = r4.getInputStream()     // Catch:{ Exception -> 0x0044 }
            java.lang.String r8 = "UTF-8"
            r2.<init>(r7, r8)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r5 = ""
        L_0x001a:
            int r6 = r2.read()     // Catch:{ Exception -> 0x0085, all -> 0x0082 }
            r7 = -1
            if (r6 == r7) goto L_0x0034
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0085, all -> 0x0082 }
            r7.<init>()     // Catch:{ Exception -> 0x0085, all -> 0x0082 }
            java.lang.StringBuilder r7 = r7.append(r5)     // Catch:{ Exception -> 0x0085, all -> 0x0082 }
            char r8 = (char) r6     // Catch:{ Exception -> 0x0085, all -> 0x0082 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x0085, all -> 0x0082 }
            java.lang.String r5 = r7.toString()     // Catch:{ Exception -> 0x0085, all -> 0x0082 }
            goto L_0x001a
        L_0x0034:
            if (r3 == 0) goto L_0x0039
            r3.close()     // Catch:{ Exception -> 0x0070 }
        L_0x0039:
            if (r2 == 0) goto L_0x003e
            r2.close()     // Catch:{ Exception -> 0x0072 }
        L_0x003e:
            if (r4 == 0) goto L_0x0043
            r4.destroy()     // Catch:{ Exception -> 0x0074 }
        L_0x0043:
            return r5
        L_0x0044:
            r0 = move-exception
        L_0x0045:
            java.lang.String r7 = com.joyours.base.util.Util.SIG     // Catch:{ all -> 0x005f }
            java.lang.String r8 = r0.getMessage()     // Catch:{ all -> 0x005f }
            android.util.Log.e(r7, r8, r0)     // Catch:{ all -> 0x005f }
            if (r3 == 0) goto L_0x0053
            r3.close()     // Catch:{ Exception -> 0x0076 }
        L_0x0053:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ Exception -> 0x0078 }
        L_0x0058:
            if (r4 == 0) goto L_0x005d
            r4.destroy()     // Catch:{ Exception -> 0x007a }
        L_0x005d:
            r5 = 0
            goto L_0x0043
        L_0x005f:
            r7 = move-exception
        L_0x0060:
            if (r3 == 0) goto L_0x0065
            r3.close()     // Catch:{ Exception -> 0x007c }
        L_0x0065:
            if (r1 == 0) goto L_0x006a
            r1.close()     // Catch:{ Exception -> 0x007e }
        L_0x006a:
            if (r4 == 0) goto L_0x006f
            r4.destroy()     // Catch:{ Exception -> 0x0080 }
        L_0x006f:
            throw r7
        L_0x0070:
            r7 = move-exception
            goto L_0x0039
        L_0x0072:
            r7 = move-exception
            goto L_0x003e
        L_0x0074:
            r7 = move-exception
            goto L_0x0043
        L_0x0076:
            r7 = move-exception
            goto L_0x0053
        L_0x0078:
            r7 = move-exception
            goto L_0x0058
        L_0x007a:
            r7 = move-exception
            goto L_0x005d
        L_0x007c:
            r8 = move-exception
            goto L_0x0065
        L_0x007e:
            r8 = move-exception
            goto L_0x006a
        L_0x0080:
            r8 = move-exception
            goto L_0x006f
        L_0x0082:
            r7 = move-exception
            r1 = r2
            goto L_0x0060
        L_0x0085:
            r0 = move-exception
            r1 = r2
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: com.joyours.base.util.Util.getDeviceNetworkInfo():java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x0019 A[Catch:{ Exception -> 0x0067 }] */
    @android.annotation.SuppressLint({"NewApi"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String getMacAddressFromNetworkInterface(java.lang.String r11) {
        /*
            r6 = 0
            int r7 = android.os.Build.VERSION.SDK_INT
            r8 = 9
            if (r7 < r8) goto L_0x0031
            java.util.Enumeration r7 = java.net.NetworkInterface.getNetworkInterfaces()     // Catch:{ Exception -> 0x0067 }
            java.util.ArrayList r5 = java.util.Collections.list(r7)     // Catch:{ Exception -> 0x0067 }
            java.util.Iterator r7 = r5.iterator()     // Catch:{ Exception -> 0x0067 }
        L_0x0013:
            boolean r8 = r7.hasNext()     // Catch:{ Exception -> 0x0067 }
            if (r8 == 0) goto L_0x0031
            java.lang.Object r4 = r7.next()     // Catch:{ Exception -> 0x0067 }
            java.net.NetworkInterface r4 = (java.net.NetworkInterface) r4     // Catch:{ Exception -> 0x0067 }
            if (r11 == 0) goto L_0x002b
            java.lang.String r8 = r4.getName()     // Catch:{ Exception -> 0x0067 }
            boolean r8 = r8.equalsIgnoreCase(r11)     // Catch:{ Exception -> 0x0067 }
            if (r8 == 0) goto L_0x0013
        L_0x002b:
            byte[] r3 = r4.getHardwareAddress()     // Catch:{ Exception -> 0x0067 }
            if (r3 != 0) goto L_0x0032
        L_0x0031:
            return r6
        L_0x0032:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0067 }
            r0.<init>()     // Catch:{ Exception -> 0x0067 }
            r2 = 0
        L_0x0038:
            int r7 = r3.length     // Catch:{ Exception -> 0x0067 }
            if (r2 >= r7) goto L_0x0053
            java.lang.String r7 = "%02X:"
            r8 = 1
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x0067 }
            r9 = 0
            byte r10 = r3[r2]     // Catch:{ Exception -> 0x0067 }
            java.lang.Byte r10 = java.lang.Byte.valueOf(r10)     // Catch:{ Exception -> 0x0067 }
            r8[r9] = r10     // Catch:{ Exception -> 0x0067 }
            java.lang.String r7 = java.lang.String.format(r7, r8)     // Catch:{ Exception -> 0x0067 }
            r0.append(r7)     // Catch:{ Exception -> 0x0067 }
            int r2 = r2 + 1
            goto L_0x0038
        L_0x0053:
            int r7 = r0.length()     // Catch:{ Exception -> 0x0067 }
            if (r7 <= 0) goto L_0x0062
            int r7 = r0.length()     // Catch:{ Exception -> 0x0067 }
            int r7 = r7 + -1
            r0.deleteCharAt(r7)     // Catch:{ Exception -> 0x0067 }
        L_0x0062:
            java.lang.String r6 = r0.toString()     // Catch:{ Exception -> 0x0067 }
            goto L_0x0031
        L_0x0067:
            r1 = move-exception
            java.lang.String r7 = com.joyours.base.util.Util.SIG
            java.lang.String r8 = r1.getMessage()
            android.util.Log.e(r7, r8, r1)
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.joyours.base.util.Util.getMacAddressFromNetworkInterface(java.lang.String):java.lang.String");
    }

    public static String decimalTo62(int number) {
        Stack<Character> stack = new Stack<>();
        StringBuilder result = new StringBuilder(0);
        while (number != 0) {
            stack.add(Character.valueOf(charSet[number - ((number / 62) * 62)]));
            number /= 62;
        }
        while (!stack.isEmpty()) {
            result.append(stack.pop());
        }
        return result.toString();
    }

    public static String getAndroidVersion() {
        String model = Build.MODEL;
        String release = Build.VERSION.RELEASE;
        int sdkInt = Build.VERSION.SDK_INT;
        JSONObject json = new JSONObject();
        try {
            json.put("model", model);
            json.put("android_release", release);
            json.put("SDK_INT", sdkInt);
            return json.toString();
        } catch (JSONException e) {
            Log.e(SIG, e.getMessage(), e);
            return null;
        }
    }

    private static boolean checkLineInstalled() {
        for (ApplicationInfo ai : Cocos2dxApp.getContext().getPackageManager().getInstalledApplications(0)) {
            if (ai.packageName.equals("jp.naver.line.android")) {
                return true;
            }
        }
        return false;
    }

    public static void sendMsgToLine(String message) {
        Cocos2dxApp context = Cocos2dxApp.getContext();
        if (checkLineInstalled()) {
            try {
                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("line://msg/text/" + URLEncoder.encode(message, "utf-8"))));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=jp.naver.line.android")));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException} */
    public static void saveKeyToSDCard(String content) {
        try {
            Log.d(SIG, "saveKeyToSDCard key =" + content);
            FileWriter writer = new FileWriter(Environment.getExternalStorageDirectory().toString() + "/joyoursNineke.txt", true);
            writer.write(content);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0065 A[SYNTHETIC, Splitter:B:17:0x0065] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0072 A[SYNTHETIC, Splitter:B:23:0x0072] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getKeyFromSDCard() {
        /*
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.io.File r8 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r8 = r8.toString()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "/joyoursNineke.txt"
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r3 = r7.toString()
            java.io.File r2 = new java.io.File
            r2.<init>(r3)
            boolean r7 = r2.exists()
            if (r7 == 0) goto L_0x007d
            r4 = 0
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ IOException -> 0x005d }
            java.io.FileReader r7 = new java.io.FileReader     // Catch:{ IOException -> 0x005d }
            r7.<init>(r2)     // Catch:{ IOException -> 0x005d }
            r5.<init>(r7)     // Catch:{ IOException -> 0x005d }
            r6 = 0
            java.lang.String r6 = r5.readLine()     // Catch:{ IOException -> 0x0083, all -> 0x0080 }
            java.lang.String r7 = com.joyours.base.util.Util.SIG     // Catch:{ IOException -> 0x0083, all -> 0x0080 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0083, all -> 0x0080 }
            r8.<init>()     // Catch:{ IOException -> 0x0083, all -> 0x0080 }
            java.lang.String r9 = "tempString ="
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ IOException -> 0x0083, all -> 0x0080 }
            java.lang.StringBuilder r8 = r8.append(r6)     // Catch:{ IOException -> 0x0083, all -> 0x0080 }
            java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x0083, all -> 0x0080 }
            android.util.Log.d(r7, r8)     // Catch:{ IOException -> 0x0083, all -> 0x0080 }
            r5.close()     // Catch:{ IOException -> 0x0083, all -> 0x0080 }
            if (r5 == 0) goto L_0x0058
            r5.close()     // Catch:{ IOException -> 0x0059 }
            java.lang.String r6 = ""
        L_0x0058:
            return r6
        L_0x0059:
            r1 = move-exception
            java.lang.String r6 = ""
            goto L_0x0058
        L_0x005d:
            r0 = move-exception
        L_0x005e:
            r0.printStackTrace()     // Catch:{ all -> 0x006f }
            java.lang.String r6 = ""
            if (r4 == 0) goto L_0x0058
            r4.close()     // Catch:{ IOException -> 0x006b }
            java.lang.String r6 = ""
            goto L_0x0058
        L_0x006b:
            r1 = move-exception
            java.lang.String r6 = ""
            goto L_0x0058
        L_0x006f:
            r7 = move-exception
        L_0x0070:
            if (r4 == 0) goto L_0x007c
            r4.close()     // Catch:{ IOException -> 0x0078 }
            java.lang.String r6 = ""
            goto L_0x0058
        L_0x0078:
            r1 = move-exception
            java.lang.String r6 = ""
            goto L_0x0058
        L_0x007c:
            throw r7
        L_0x007d:
            java.lang.String r6 = ""
            goto L_0x0058
        L_0x0080:
            r7 = move-exception
            r4 = r5
            goto L_0x0070
        L_0x0083:
            r0 = move-exception
            r4 = r5
            goto L_0x005e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.joyours.base.util.Util.getKeyFromSDCard():java.lang.String");
    }
}
