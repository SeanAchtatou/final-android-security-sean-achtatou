package com.joyours.nineke;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.joyours.base.framework.Cocos2dxApp;
import com.joyours.base.util.Util;

public class NineKe extends Cocos2dxApp {
    public static String SIG = NineKe.class.getSimpleName();
    public static Context STATIC_REF = null;
    private final BroadcastReceiver mWifiReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("android.net.wifi.WIFI_STATE_CHANGED".equals(action)) {
                Util.onWifiStateChangedToLua(intent.getIntExtra("wifi_state", 0));
            } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
                Util.onNetworkStateChangedToLua();
            }
        }
    };

    public static Cocos2dxApp getContext() {
        return (Cocos2dxApp) STATIC_REF;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        STATIC_REF = this;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        registerReceiver(this.mWifiReceiver, filter);
    }
}
