package com.mol.seaplus.base.sdk.impl;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mol.seaplus.base.sdk.IMolWaitingDialog;
import com.mol.seaplus.tool.utils.UiUtils;

public class MolWaitingDialog extends MolDialog implements IMolWaitingDialog {
    private ProgressBar mProgressBar;

    public MolWaitingDialog(Context context) {
        super(context);
        setContentView(initView(context));
    }

    private View initView(Context pContext) {
        int padding5 = (int) UiUtils.applyDimension(pContext, 1, 5);
        LinearLayout mainLayout = new LinearLayout(pContext);
        TextView checkingTextView = new TextView(pContext);
        this.mProgressBar = new ProgressBar(pContext, null, 16842872);
        mainLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        mainLayout.setOrientation(1);
        mainLayout.setPadding(padding5, padding5, padding5, padding5);
        checkingTextView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        checkingTextView.setTextSize(2, 16.0f);
        checkingTextView.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
        checkingTextView.setText(Resources.getString(7));
        this.mProgressBar.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.mProgressBar.setIndeterminate(true);
        mainLayout.addView(checkingTextView);
        mainLayout.addView(this.mProgressBar);
        return mainLayout;
    }

    public void setMaxWaitingTime(int pMaxWaitingTime) {
        this.mProgressBar.setMax(pMaxWaitingTime);
    }

    public void updateWaitingTime(int pWaitingTime) {
        if (this.mProgressBar.isIndeterminate()) {
            this.mProgressBar.setIndeterminate(false);
        }
        this.mProgressBar.setProgress(pWaitingTime);
    }
}
