package com.mol.seaplus.base.sdk;

public interface IMolDialog {
    void askForConfirmClose();

    void dismiss(boolean z);

    boolean isShowing();

    void show();

    void showCloseButton(boolean z);
}
