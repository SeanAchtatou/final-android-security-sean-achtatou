package com.mol.seaplus.base.sdk.impl;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mol.seaplus.Language;
import com.mol.seaplus.base.sdk.IMolDialog;
import com.mol.seaplus.base.sdk.IMolDialogPresenter;
import com.mol.seaplus.tool.dialog.ToolAlertDialog;
import com.mol.seaplus.tool.dialog.ToolDialogInterface;
import com.mol.seaplus.tool.utils.UiUtils;

public class MolDialog extends ToolDialogInterface implements IMolDialog {
    private RelativeLayout mBodyLayout;
    private TextView mCloseBtn;
    private Dialog mDialog;
    /* access modifiers changed from: private */
    public IMolDialogPresenter mMolDialogPresenter;
    private DialogInterface.OnCancelListener mOnCancelListener;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            MolDialog.this.mMolDialogPresenter.onCloseClicked();
        }
    };

    public MolDialog(Context context) {
        super(context);
        this.mDialog = new Dialog(context, 16973841);
        this.mDialog.setContentView(initView(context));
        this.mCloseBtn.setOnClickListener(this.mOnClickListener);
        super.setCancelable(false);
        super.setCanceledOnTouchOutside(false);
        setMolDialogPresenter(new MolDialogPresenter(this));
    }

    public final void setLanguage(Language pLanguage) {
        Resources.setLanguage(pLanguage);
    }

    private View initView(Context pContext) {
        int padding5 = (int) UiUtils.applyDimension(pContext, 1, 5);
        int padding10 = (int) UiUtils.applyDimension(pContext, 1, 10);
        int size36 = (int) UiUtils.applyDimension(pContext, 1, 36);
        int size18 = size36 / 2;
        RelativeLayout mainRelativeLayout = new RelativeLayout(pContext);
        RelativeLayout bodyRelativeLayout = new RelativeLayout(pContext);
        this.mBodyLayout = new RelativeLayout(pContext);
        this.mCloseBtn = new TextView(pContext);
        mainRelativeLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        mainRelativeLayout.setBackgroundColor(-1728053248);
        mainRelativeLayout.setPadding(padding10, padding10, padding10, padding10);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-1, -2);
        params.addRule(13);
        bodyRelativeLayout.setLayoutParams(params);
        RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(-1, -2);
        int i = size18 / 2;
        params2.rightMargin = i;
        params2.topMargin = i;
        int i2 = size18 / 2;
        params2.bottomMargin = i2;
        params2.leftMargin = i2;
        this.mBodyLayout.setLayoutParams(params2);
        this.mBodyLayout.setPadding(padding5, padding5, padding5, padding5);
        this.mBodyLayout.setBackgroundColor(-1);
        RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(size36, size36);
        params3.addRule(11);
        this.mCloseBtn.setLayoutParams(params3);
        this.mCloseBtn.setTextSize(2, 16.0f);
        this.mCloseBtn.setTypeface(this.mCloseBtn.getTypeface(), 1);
        this.mCloseBtn.setText("X");
        this.mCloseBtn.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
        this.mCloseBtn.setGravity(17);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColor(-1);
        gradientDrawable.setShape(1);
        gradientDrawable.setStroke(1, (int) ViewCompat.MEASURED_STATE_MASK);
        if (Build.VERSION.SDK_INT >= 16) {
            this.mCloseBtn.setBackground(gradientDrawable);
        } else {
            this.mCloseBtn.setBackgroundDrawable(gradientDrawable);
        }
        bodyRelativeLayout.addView(this.mBodyLayout);
        bodyRelativeLayout.addView(this.mCloseBtn);
        mainRelativeLayout.addView(bodyRelativeLayout);
        return mainRelativeLayout;
    }

    /* access modifiers changed from: protected */
    public Context getContext() {
        return this.context;
    }

    public void showCloseButton(boolean isShow) {
        this.mCloseBtn.setVisibility(isShow ? 0 : 8);
    }

    public boolean isShowing() {
        return this.mDialog.isShowing();
    }

    public void dismiss(boolean byUser) {
        super.dismiss();
        if (byUser && this.mOnCancelListener != null) {
            this.mOnCancelListener.onCancel(this.mDialog);
        }
    }

    public void setCancelable(boolean flag) {
    }

    public void setCanceledOnTouchOutside(boolean flag) {
    }

    public void setOnCancelListener(DialogInterface.OnCancelListener onCancelListener) {
        this.mOnCancelListener = onCancelListener;
    }

    public void setMolDialogPresenter(IMolDialogPresenter pMolDialogPresenter) {
        this.mMolDialogPresenter = pMolDialogPresenter;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.RelativeLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void setContentView(int resId) {
        setContentView(LayoutInflater.from(this.context).inflate(resId, (ViewGroup) this.mBodyLayout, false));
    }

    /* access modifiers changed from: protected */
    public void setContentView(View view) {
        this.mBodyLayout.removeAllViews();
        this.mBodyLayout.addView(view);
    }

    /* access modifiers changed from: protected */
    public View findViewById(int id) {
        return this.mBodyLayout.findViewById(id);
    }

    public void askForConfirmClose() {
        ToolAlertDialog.show(this.context, Resources.getString(0), new String[]{Resources.getString(1), Resources.getString(2)}, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int which) {
                if (which == -1) {
                    MolDialog.this.mMolDialogPresenter.onConfirmClose(true);
                } else {
                    MolDialog.this.mMolDialogPresenter.onConfirmClose(false);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onCreateDialog() {
    }

    /* access modifiers changed from: protected */
    public final Dialog createDialog() {
        onCreateDialog();
        return this.mDialog;
    }
}
