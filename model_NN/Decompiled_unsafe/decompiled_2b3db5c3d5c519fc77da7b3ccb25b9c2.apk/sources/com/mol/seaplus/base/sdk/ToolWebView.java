package com.mol.seaplus.base.sdk;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ToolWebView extends WebView {
    private ToolWebViewAdapter adapter;

    public ToolWebView(Context context) {
        super(context);
    }

    public ToolWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public final void setAdapter(ToolWebViewAdapter adapter2) {
        this.adapter = adapter2;
        adapter2.setToolWebView(this);
        WebChromeClient webChromeClient = adapter2.getWebChromeClient();
        WebViewClient webViewClient = adapter2.getWebViewClient();
        if (webChromeClient != null) {
            super.setWebChromeClient(webChromeClient);
        }
        if (webViewClient != null) {
            super.setWebViewClient(webViewClient);
        }
    }

    public final void setWebChromeClient(WebChromeClient client) {
    }

    public final void setWebViewClient(WebViewClient client) {
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (this.adapter != null) {
            this.adapter.dispatchDraw(canvas);
        }
    }
}
