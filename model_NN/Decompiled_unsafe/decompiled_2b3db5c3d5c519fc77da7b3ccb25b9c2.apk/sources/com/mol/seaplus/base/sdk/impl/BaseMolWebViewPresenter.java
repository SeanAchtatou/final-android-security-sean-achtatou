package com.mol.seaplus.base.sdk.impl;

import com.mol.seaplus.base.sdk.IMolWebView;
import com.mol.seaplus.base.sdk.IMolWebViewPresenter;
import com.mol.seaplus.tool.datareader.data.IDataHolder;
import com.mol.seaplus.tool.datareader.data.IDataReaderOption;
import com.mol.seaplus.tool.datareader.data.impl.DataHolder;
import com.mol.seaplus.tool.datareader.data.impl.DataReaderOption;
import com.mol.seaplus.tool.datareader.data.impl.JSONDataReader;
import org.json.JSONException;
import org.json.JSONObject;

public class BaseMolWebViewPresenter implements IMolWebViewPresenter {
    private static final String CLOSE_COMMAND = "close";
    private static final String CODE = "code";
    private static final String COMMAND = "command";
    private static final String DESCRIPTION = "desc";
    private static final String ERROR_EVENT = "onError";
    /* access modifiers changed from: private */
    public static final String[] KEYS = {COMMAND, RETURN_EVENT, "txId", "priceId", CODE, DESCRIPTION};
    private static final String ON_EVENT = "onEvent";
    private static final String PRICE_ID = "priceId";
    private static final String PURCHASE_RESULT_EVENT = "onPurchaseResult";
    private static final String PURCHASE_VIA_EASY_2_PAY_COMMAND = "purchaseViaEasy2pay";
    private static final String RETURN_EVENT = "returnEvent";
    private static final String TXID = "txId";
    private IMolWebView mMolWebView;

    public enum Command {
        PurchaseViaEasy2Pay,
        Close
    }

    public enum ReturnEvent {
        OnPurchaseResult,
        OnEvent,
        OnError
    }

    public BaseMolWebViewPresenter(String pUrl, IMolWebView pMolWebView) {
        this.mMolWebView = pMolWebView;
        this.mMolWebView.loadUrl(pUrl);
    }

    /* access modifiers changed from: protected */
    public boolean onHandlerCommand(CommandHolder pCommandHolder) {
        return false;
    }

    public boolean onCommand(String pCommand) {
        try {
            return onHandlerCommand((CommandHolder) wrapCommand(new JSONObject(pCommand), new CommandHolder()));
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    private <T extends DataHolder> T wrapCommand(JSONObject pResult, T t) {
        JSONObject data = new JSONObject();
        try {
            data.put(t.getTagName(), pResult);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        IDataReaderOption<T> dataReaderOption = new DataReaderOption();
        dataReaderOption.setDataHolder(t);
        JSONDataReader<T> jsonDataReader = new JSONDataReader<>(dataReaderOption);
        jsonDataReader.extractJSONObject(data);
        return (DataHolder) jsonDataReader.getAllData().get(0);
    }

    public class CommandHolder extends DataHolder {
        private CommandHolder() {
            super(BaseMolWebViewPresenter.COMMAND, BaseMolWebViewPresenter.KEYS);
        }

        public Command getCommand() {
            String command = getString(BaseMolWebViewPresenter.COMMAND);
            if (BaseMolWebViewPresenter.PURCHASE_VIA_EASY_2_PAY_COMMAND.equals(command)) {
                return Command.PurchaseViaEasy2Pay;
            }
            if (BaseMolWebViewPresenter.CLOSE_COMMAND.equals(command)) {
                return Command.Close;
            }
            return null;
        }

        public ReturnEvent getReturnEvent() {
            String returnEvent = getString(BaseMolWebViewPresenter.RETURN_EVENT);
            if (BaseMolWebViewPresenter.PURCHASE_RESULT_EVENT.equals(returnEvent)) {
                return ReturnEvent.OnPurchaseResult;
            }
            if (BaseMolWebViewPresenter.ON_EVENT.equals(returnEvent)) {
                return ReturnEvent.OnEvent;
            }
            if (BaseMolWebViewPresenter.ERROR_EVENT.equals(returnEvent)) {
                return ReturnEvent.OnError;
            }
            return null;
        }

        public String getTxId() {
            return getString("txId");
        }

        public String getPriceId() {
            return getString("priceId");
        }

        public String getCode() {
            return getString(BaseMolWebViewPresenter.CODE);
        }

        public String getDescription() {
            return getString(BaseMolWebViewPresenter.DESCRIPTION);
        }

        public DataHolder initDataHolder() {
            return new CommandHolder();
        }

        public IDataHolder getChildHolderByTag(String s) {
            return null;
        }
    }
}
