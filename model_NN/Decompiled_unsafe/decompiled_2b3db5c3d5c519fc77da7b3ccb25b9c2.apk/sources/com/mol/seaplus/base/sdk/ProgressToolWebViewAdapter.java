package com.mol.seaplus.base.sdk;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class ProgressToolWebViewAdapter extends ToolWebViewAdapter {
    private int color;
    private boolean enableProgressBar;
    private WebChromeClient pWebChromeClient;
    private WebViewClient pWebViewClient;
    /* access modifiers changed from: private */
    public int progress;
    private Paint progressPaint;

    public ProgressToolWebViewAdapter() {
        this(null, null);
    }

    public ProgressToolWebViewAdapter(WebChromeClient pWebChromeClient2, WebViewClient pWebViewClient2) {
        this.progress = 0;
        this.color = -16711936;
        this.enableProgressBar = false;
        this.pWebChromeClient = pWebChromeClient2;
        this.pWebViewClient = pWebViewClient2;
        enableProgressBar(true);
    }

    public final void enableProgressBar(boolean enable) {
        this.enableProgressBar = enable;
    }

    public void setProgressBarColor(int color2) {
        this.color = color2;
        if (this.progressPaint != null) {
            this.progressPaint.setColor(color2);
        }
    }

    public void setProgress(int pProgress) {
        this.progress = pProgress;
    }

    public int getProgrss() {
        return this.progress;
    }

    public final boolean isEnableProgressBar() {
        return this.enableProgressBar;
    }

    /* access modifiers changed from: protected */
    public WebChromeClient getWebChromeClient() {
        if (this.pWebChromeClient == null) {
            this.pWebChromeClient = new ProgressWebChromeClient();
        }
        return this.pWebChromeClient;
    }

    /* access modifiers changed from: protected */
    public WebViewClient getWebViewClient() {
        if (this.pWebViewClient == null) {
            this.pWebViewClient = new WebViewClient();
        }
        return this.pWebViewClient;
    }

    public void onDispatchDraw(ToolWebView view, Canvas canvas) {
        if (this.enableProgressBar) {
            if (this.progressPaint == null) {
                this.progressPaint = new Paint();
                this.progressPaint.setColor(this.color);
                this.progressPaint.setStyle(Paint.Style.FILL);
                this.progressPaint.setAntiAlias(true);
            }
            float right = ((float) (this.progress * view.getWidth())) / 100.0f;
            canvas.drawRect(0.0f, 0.0f, right, 5.0f, this.progressPaint);
            if (this.progress > 0) {
                canvas.drawOval(new RectF(right - ((float) 6), (float) (-6), ((float) 6) + right, (float) 6), this.progressPaint);
            }
        }
    }

    public class ProgressWebChromeClient extends WebChromeClient {
        public ProgressWebChromeClient() {
        }

        public void onProgressChanged(WebView view, int newProgress) {
            int unused = ProgressToolWebViewAdapter.this.progress = newProgress;
            if (ProgressToolWebViewAdapter.this.progress == 100) {
                int unused2 = ProgressToolWebViewAdapter.this.progress = 0;
            }
            view.invalidate();
        }
    }
}
