package com.mol.seaplus.base.sdk.impl;

import org.json.JSONObject;

public interface MolApiCallback {
    void onApiError(int i, String str);

    void onApiSuccess(JSONObject jSONObject);
}
