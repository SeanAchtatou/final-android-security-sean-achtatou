package com.mol.seaplus.base.sdk;

public interface IMolWaitingDialog extends IMolDialog {
    void setMaxWaitingTime(int i);

    void updateWaitingTime(int i);
}
