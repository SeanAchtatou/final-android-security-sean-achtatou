package com.mol.seaplus.base.sdk;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

public class Sdk {
    private static final int INDONESIA_MCC = 510;
    private static final int INDOSAT_MNC = 1;
    private static final int TELKOMSEL_MNC = 10;
    private static final int XL_MNC = 11;
    private static boolean mTesting = false;

    public static final void enableTest(boolean pEnable) {
        mTesting = pEnable;
    }

    public static final boolean isTest() {
        return mTesting;
    }

    public static final int[] getSimOperatorAsInt(Context pContext) {
        String[] simOperator = getSimOperator(pContext);
        if (simOperator == null) {
            return null;
        }
        return new int[]{Integer.parseInt(simOperator[0]), Integer.parseInt(simOperator[1])};
    }

    public static final String[] getSimOperator(Context pContext) {
        String simOperator = ((TelephonyManager) pContext.getSystemService("phone")).getSimOperator();
        if (TextUtils.isEmpty(simOperator)) {
            return null;
        }
        return new String[]{simOperator.substring(0, 3), simOperator.substring(3)};
    }

    public static final boolean isSupportSmsBilling(Context pContext) {
        int[] simOperator = getSimOperatorAsInt(pContext);
        if (simOperator != null) {
            return isSupportSmsBilling(simOperator[0], simOperator[1]);
        }
        return false;
    }

    public static final boolean isSupportSmsBilling(int mcc, int mnc) {
        if (mcc != INDONESIA_MCC) {
            return true;
        }
        switch (mnc) {
            case 1:
            case 11:
                return true;
            default:
                return false;
        }
    }

    public static final boolean isSupportUPoint(Context pContext) {
        int[] simOperator = getSimOperatorAsInt(pContext);
        if (simOperator != null) {
            return isSupportUPoint(simOperator[0], simOperator[1]);
        }
        return false;
    }

    public static final boolean isSupportUPoint(int mcc, int mnc) {
        if (mcc != INDONESIA_MCC) {
            return false;
        }
        switch (mnc) {
            case 10:
                return true;
            default:
                return false;
        }
    }

    public static final boolean isTelkomsel(int mcc, int mnc) {
        if (mcc != INDONESIA_MCC) {
            return false;
        }
        switch (mnc) {
            case 10:
                return true;
            default:
                return false;
        }
    }
}
