package com.mol.seaplus.base.sdk.impl;

import android.content.Context;
import com.mol.seaplus.Language;
import com.mol.seaplus.Log;
import com.mol.seaplus.tool.connection.handler.ErrorHandler;
import com.mol.seaplus.tool.datareader.api.IApiRequest;
import com.mol.seaplus.tool.datareader.api.impl.ApiRequest;
import com.mol.seaplus.tool.datareader.data.IDataReader;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class MolApiRequest extends ApiRequest {
    /* access modifiers changed from: private */
    public Language mLanguage;
    private MolApiCallback mMolApiCallback;

    public MolApiRequest(Context context, String source) {
        super(context, source);
    }

    public void request() {
        Language language = this.mLanguage;
        if (language == null) {
            language = Language.EN;
        }
        Resources.setLanguage(language);
        super.request();
    }

    public final void stopRequest() {
        setMolApiCallback(null);
    }

    /* access modifiers changed from: protected */
    public final void setLanguage(Language pLanguage) {
        this.mLanguage = pLanguage;
    }

    public void setMolApiCallback(MolApiCallback pMolApiCallback) {
        this.mMolApiCallback = pMolApiCallback;
    }

    /* access modifiers changed from: protected */
    public void updateSuccess(JSONObject pResult) {
        if (this.mMolApiCallback != null) {
            this.mMolApiCallback.onApiSuccess(pResult);
        }
    }

    /* access modifiers changed from: protected */
    public void updateError(int pErrorCode, String pError) {
        if (this.mMolApiCallback != null) {
            this.mMolApiCallback.onApiError(pErrorCode, getError(pErrorCode, pError));
        }
    }

    /* access modifiers changed from: protected */
    public void onApiResponse(IDataReader iDataReader) {
        if (iDataReader.getReadCode() == 200) {
            boolean isSuccess = isSuccess(iDataReader);
            int apiStatusCode = getApiStatusCode(iDataReader);
            String description = getApiDescription(iDataReader);
            if (!isSuccess) {
                updateError(apiStatusCode, description);
                return;
            }
            String response = iDataReader.getResponse();
            Log.d("response = " + response);
            try {
                updateSuccess(new JSONObject(response));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            updateError(iDataReader.getErrorCode(), iDataReader.getError());
        }
    }

    protected static abstract class Builder<G extends Builder> extends ApiRequest.Builder {
        private Language mLanguage = null;
        private MolApiCallback mMolApiCallback;

        /* access modifiers changed from: protected */
        public abstract MolApiRequest onBuild(Context context);

        public Builder(Context context) {
            super(context);
        }

        public G callback(MolApiCallback pMolApiCallback) {
            this.mMolApiCallback = pMolApiCallback;
            return this;
        }

        public G language(Language pLanguage) {
            this.mLanguage = pLanguage;
            return this;
        }

        public G setErrorHandler(ErrorHandler errorHandler) {
            return (Builder) super.setErrorHandler(errorHandler);
        }

        /* access modifiers changed from: protected */
        public final IApiRequest onBuild() {
            MolApiRequest molApiRequest = onBuild(getContext());
            Language unused = molApiRequest.mLanguage = this.mLanguage;
            molApiRequest.setMolApiCallback(this.mMolApiCallback);
            return molApiRequest;
        }
    }
}
