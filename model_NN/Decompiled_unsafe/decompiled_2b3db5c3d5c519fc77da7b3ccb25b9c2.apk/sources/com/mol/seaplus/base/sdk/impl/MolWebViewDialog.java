package com.mol.seaplus.base.sdk.impl;

import android.view.View;
import com.mol.seaplus.base.sdk.IMolWebView;

public class MolWebViewDialog<T extends View & IMolWebView> extends MolDialog implements IMolWebView {
    private T mMolWebView;

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public MolWebViewDialog(android.content.Context r5, T r6) {
        /*
            r4 = this;
            r4.<init>(r5)
            r4.mMolWebView = r6
            T r0 = r4.mMolWebView
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r2 = -1
            r3 = -2
            r1.<init>(r2, r3)
            r0.setLayoutParams(r1)
            T r0 = r4.mMolWebView
            r4.setContentView(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mol.seaplus.base.sdk.impl.MolWebViewDialog.<init>(android.content.Context, android.view.View):void");
    }

    public void loadUrl(String url) {
        ((IMolWebView) this.mMolWebView).loadUrl(url);
    }
}
