package com.mol.seaplus.base.sdk;

import android.graphics.Canvas;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import org.apache.http.protocol.HTTP;

public class ToolWebViewAdapter {
    private ToolWebView webView;

    /* access modifiers changed from: protected */
    public final void setToolWebView(ToolWebView webView2) {
        this.webView = webView2;
        settingWebView();
    }

    /* access modifiers changed from: protected */
    public WebChromeClient getWebChromeClient() {
        return null;
    }

    /* access modifiers changed from: protected */
    public WebViewClient getWebViewClient() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void onSettingWebView(WebSettings webSetting) {
    }

    /* access modifiers changed from: protected */
    public void onDispatchDraw(ToolWebView view, Canvas canvas) {
    }

    private void settingWebView() {
        WebSettings webSetting = this.webView.getSettings();
        webSetting.setJavaScriptEnabled(true);
        webSetting.setDefaultTextEncodingName(HTTP.UTF_8);
        webSetting.setDomStorageEnabled(true);
        onSettingWebView(webSetting);
    }

    public final void dispatchDraw(Canvas canvas) {
        if (this.webView != null) {
            onDispatchDraw(this.webView, canvas);
        }
    }
}
