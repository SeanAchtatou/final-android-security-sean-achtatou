package com.mol.seaplus.base.sdk.impl;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.mol.seaplus.tool.imageloader.CashCardlogo;
import com.mol.seaplus.tool.utils.UiUtils;
import java.util.List;

public class MolSelectorDialog extends MolDialog {
    public MolSelectorDialog(Context context, List<String> pSelectList, final AdapterView.OnItemClickListener pOnItemClickListener) {
        super(context);
        ListView listView = new ListView(context);
        listView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        MySelectorAdapter adapter = new MySelectorAdapter();
        List unused = adapter.mSelectorList = pSelectList;
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColor((int) ViewCompat.MEASURED_STATE_MASK);
        setContentView(listView);
        listView.setAdapter((ListAdapter) adapter);
        listView.setDivider(gradientDrawable);
        listView.setDividerHeight((int) UiUtils.convertDpiToPixel(getContext(), 1));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                MolSelectorDialog.this.dismiss();
                if (pOnItemClickListener != null) {
                    pOnItemClickListener.onItemClick(adapterView, view, i, l);
                }
            }
        });
    }

    private class MySelectorAdapter extends BaseAdapter {
        /* access modifiers changed from: private */
        public List<String> mSelectorList;

        private MySelectorAdapter() {
        }

        public int getCount() {
            if (this.mSelectorList != null) {
                return this.mSelectorList.size();
            }
            return 0;
        }

        public String getItem(int i) {
            return this.mSelectorList.get(i);
        }

        public long getItemId(int i) {
            return 0;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ImageView imageView = (ImageView) view;
            if (imageView != null) {
                return imageView;
            }
            int convertDpiToPixel = (int) UiUtils.convertDpiToPixel(MolSelectorDialog.this.getContext(), 10);
            int padding8 = (int) UiUtils.convertDpiToPixel(MolSelectorDialog.this.getContext(), 8);
            ImageView imageView2 = new ImageView(MolSelectorDialog.this.getContext());
            imageView2.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
            new CashCardlogo();
            imageView2.setImageBitmap(CashCardlogo.getLogo("https://sea-sdk.molthailand.com/sdk_server/images/logo/" + getItem(i) + ".png"));
            imageView2.setPadding(padding8, padding8, padding8, padding8);
            return imageView2;
        }
    }
}
