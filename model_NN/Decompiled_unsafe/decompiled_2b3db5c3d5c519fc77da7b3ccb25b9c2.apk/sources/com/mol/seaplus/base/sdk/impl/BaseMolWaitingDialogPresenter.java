package com.mol.seaplus.base.sdk.impl;

import android.os.Message;
import com.mol.seaplus.base.sdk.IMolDialog;
import com.mol.seaplus.base.sdk.IMolWaitingDialog;
import com.mol.seaplus.tool.utils.os.UIHandler;

public class BaseMolWaitingDialogPresenter extends MolDialogPresenter {
    private static final int INTERVAL = 9046;
    private static final int UPDATE_PROGRESS = 9045;
    private static final int WAITING_TIMEOUT = 9047;
    /* access modifiers changed from: private */
    public long mDialogEndTime;
    /* access modifiers changed from: private */
    public long mDialogMaxTime = 30000;
    private long mIntervalTime = 1000;
    /* access modifiers changed from: private */
    public boolean mIsStarted;
    private long mMaxWaitingTime = 60000;
    /* access modifiers changed from: private */
    public IMolWaitingDialog mMolWaitingDialog;
    /* access modifiers changed from: private */
    public UIHandler mUiHandler = new UIHandler() {
        /* access modifiers changed from: protected */
        public boolean onHandlerMessage(Message message, int source) {
            switch (message.what) {
                case BaseMolWaitingDialogPresenter.UPDATE_PROGRESS /*9045*/:
                    if (!BaseMolWaitingDialogPresenter.this.mIsStarted) {
                        return true;
                    }
                    long currentTime = System.currentTimeMillis();
                    BaseMolWaitingDialogPresenter.this.mMolWaitingDialog.updateWaitingTime(100 - ((int) ((((float) (BaseMolWaitingDialogPresenter.this.mDialogEndTime - currentTime)) / ((float) BaseMolWaitingDialogPresenter.this.mDialogMaxTime)) * 100.0f)));
                    if (currentTime > BaseMolWaitingDialogPresenter.this.mDialogEndTime) {
                        BaseMolWaitingDialogPresenter.this.mMolWaitingDialog.dismiss(false);
                        BaseMolWaitingDialogPresenter.this.onDialogTimeout();
                        return true;
                    }
                    BaseMolWaitingDialogPresenter.this.mUiHandler.sendEmptyMessageDelayed(BaseMolWaitingDialogPresenter.UPDATE_PROGRESS, 150);
                    return true;
                case BaseMolWaitingDialogPresenter.INTERVAL /*9046*/:
                    BaseMolWaitingDialogPresenter.this.onInterval();
                    return true;
                case BaseMolWaitingDialogPresenter.WAITING_TIMEOUT /*9047*/:
                    BaseMolWaitingDialogPresenter.this.onWaitingTimeout();
                    return true;
                default:
                    return false;
            }
        }
    };

    public BaseMolWaitingDialogPresenter(IMolWaitingDialog pMolWaitingDialog) {
        super(pMolWaitingDialog);
        this.mMolWaitingDialog = pMolWaitingDialog;
        this.mMolWaitingDialog.showCloseButton(false);
        this.mMolWaitingDialog.setMaxWaitingTime(100);
    }

    /* access modifiers changed from: protected */
    public void setMaxWaitingTime(long pMaxWaitingTime) {
        this.mMaxWaitingTime = pMaxWaitingTime;
        if (this.mMaxWaitingTime < 60000) {
            this.mMaxWaitingTime = 60000;
        }
    }

    /* access modifiers changed from: protected */
    public void setDialogMaxTime(long pDialogMaxTime) {
        this.mDialogMaxTime = pDialogMaxTime;
    }

    /* access modifiers changed from: protected */
    public void setmIntervalTime(long pIntervalTime) {
        this.mIntervalTime = pIntervalTime;
    }

    /* access modifiers changed from: protected */
    public boolean startWaiting() {
        if (this.mIsStarted) {
            return false;
        }
        this.mIsStarted = true;
        this.mDialogEndTime = System.currentTimeMillis() + this.mDialogMaxTime;
        this.mUiHandler.sendEmptyMessage(UPDATE_PROGRESS);
        this.mUiHandler.removeMessages(WAITING_TIMEOUT);
        this.mUiHandler.sendEmptyMessageDelayed(WAITING_TIMEOUT, this.mMaxWaitingTime);
        return true;
    }

    public boolean stopWaiting() {
        IMolDialog iMolDialog = getMolDialog();
        if (iMolDialog.isShowing()) {
            iMolDialog.dismiss(false);
        }
        if (!this.mIsStarted) {
            return false;
        }
        this.mIsStarted = false;
        this.mUiHandler.removeMessages(UPDATE_PROGRESS);
        this.mUiHandler.removeMessages(WAITING_TIMEOUT);
        this.mUiHandler.removeMessages(INTERVAL);
        return true;
    }

    public boolean isStartWaiting() {
        return this.mIsStarted;
    }

    /* access modifiers changed from: protected */
    public void startInterval() {
        this.mUiHandler.sendEmptyMessageDelayed(INTERVAL, this.mIntervalTime);
    }

    /* access modifiers changed from: protected */
    public void stopInterval() {
        this.mUiHandler.removeMessages(INTERVAL);
    }

    /* access modifiers changed from: protected */
    public void onDialogTimeout() {
    }

    /* access modifiers changed from: protected */
    public void onInterval() {
    }

    /* access modifiers changed from: protected */
    public void onWaitingTimeout() {
    }
}
