package com.mol.seaplus.base.sdk;

import org.json.JSONObject;

public interface IMolRequest {

    public interface BaseRequestListener {
        void onRequestError(int i, String str);

        void onRequestEvent(int i);

        void onUserCancel();
    }

    public interface OnRequestListener extends BaseRequestListener {
        void onRequestSuccess(JSONObject jSONObject);
    }

    IMolRequest request();
}
