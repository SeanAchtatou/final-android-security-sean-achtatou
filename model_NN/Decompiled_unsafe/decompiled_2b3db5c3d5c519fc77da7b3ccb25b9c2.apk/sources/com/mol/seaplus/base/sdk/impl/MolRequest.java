package com.mol.seaplus.base.sdk.impl;

import android.content.Context;
import com.mol.seaplus.CodeList;
import com.mol.seaplus.Language;
import com.mol.seaplus.base.sdk.IMolRequest;
import com.mol.seaplus.tool.connection.handler.ErrorHandler;
import com.mol.seaplus.tool.dialog.ToolProgressDialog;
import org.json.JSONObject;

public abstract class MolRequest implements IMolRequest {
    private Context mContext;
    /* access modifiers changed from: private */
    public ErrorHandler mErrorHandler;
    private boolean mIsFinishRequest;
    /* access modifiers changed from: private */
    public Language mLanguage;
    /* access modifiers changed from: private */
    public IMolRequest.OnRequestListener mOnRequestListener;
    private ToolProgressDialog mProgressDialog;

    /* access modifiers changed from: protected */
    public abstract IMolRequest onRequest(Context context);

    public MolRequest(Context pContext) {
        this.mContext = pContext;
    }

    public final IMolRequest request() {
        this.mIsFinishRequest = false;
        Language language = this.mLanguage;
        if (language == null) {
            language = Language.EN;
        }
        Resources.setLanguage(language);
        return onRequest(getContext());
    }

    /* access modifiers changed from: protected */
    public Context getContext() {
        return this.mContext;
    }

    /* access modifiers changed from: protected */
    public final void setLanguage(Language pLanguage) {
        this.mLanguage = pLanguage;
    }

    /* access modifiers changed from: protected */
    public final Language getLanguage() {
        return this.mLanguage;
    }

    /* access modifiers changed from: protected */
    public ErrorHandler getErrorHandler() {
        return this.mErrorHandler;
    }

    public void setErrorHandler(ErrorHandler pErrorHandler) {
        this.mErrorHandler = pErrorHandler;
    }

    /* access modifiers changed from: protected */
    public IMolRequest.OnRequestListener getOnRequestListener() {
        return this.mOnRequestListener;
    }

    public void setOnRequestListener(IMolRequest.OnRequestListener mOnRequestListener2) {
        this.mOnRequestListener = mOnRequestListener2;
    }

    /* access modifiers changed from: protected */
    public void showProgress() {
        showProgress(Resources.getString(5), Resources.getString(6));
    }

    /* access modifiers changed from: protected */
    public void showProgress(String pTitle, String pMessage) {
        hideProgress();
        this.mProgressDialog = ToolProgressDialog.show(getContext(), pTitle, pMessage);
    }

    /* access modifiers changed from: protected */
    public void hideProgress() {
        if (this.mProgressDialog != null) {
            this.mProgressDialog.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void onUpdateSuccess(JSONObject pJSONObject) {
    }

    /* access modifiers changed from: protected */
    public void onUpdateError(int pErrorCode, String pError) {
    }

    /* access modifiers changed from: protected */
    public void updateSuccess(JSONObject pJSONObject) {
        if (!this.mIsFinishRequest) {
            this.mIsFinishRequest = true;
            onUpdateSuccess(pJSONObject);
            if (this.mOnRequestListener != null) {
                this.mOnRequestListener.onRequestSuccess(pJSONObject);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void updateEvent(int pEventCode) {
        if (this.mOnRequestListener != null) {
            this.mOnRequestListener.onRequestEvent(pEventCode);
        }
    }

    /* access modifiers changed from: protected */
    public String getError(int pErrorCode, String pError) {
        if (this.mErrorHandler != null) {
            return this.mErrorHandler.handlerError(pErrorCode, pError);
        }
        return pError;
    }

    /* access modifiers changed from: protected */
    public void updateError(int pErrorCode, String pError) {
        if (!this.mIsFinishRequest) {
            this.mIsFinishRequest = true;
            onUpdateError(pErrorCode, pError);
            String pError2 = getError(pErrorCode, pError);
            if (this.mOnRequestListener != null) {
                this.mOnRequestListener.onRequestError(pErrorCode, pError2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void updateTimeout() {
        updateError(CodeList.REQUEST_TIMEOUT, Resources.getString(8));
    }

    /* access modifiers changed from: protected */
    public void updateUserCancel() {
        if (this.mOnRequestListener != null) {
            this.mOnRequestListener.onUserCancel();
        }
    }

    public static abstract class Builder<T extends IMolRequest, G extends Builder> {
        private Context mContext;
        private ErrorHandler mErrorHandler;
        private Language mLanguage;
        private IMolRequest.OnRequestListener mOnRequestListener;

        /* access modifiers changed from: protected */
        public abstract T onBuild(Context context);

        public Builder(Context pContext) {
            this.mContext = pContext;
        }

        /* access modifiers changed from: protected */
        public Context getContext() {
            return this.mContext;
        }

        public G setOnRequestListener(IMolRequest.OnRequestListener mOnRequestListener2) {
            this.mOnRequestListener = mOnRequestListener2;
            return this;
        }

        public G language(Language pLanguage) {
            this.mLanguage = pLanguage;
            return this;
        }

        /* access modifiers changed from: protected */
        public Language getLanguage() {
            return this.mLanguage;
        }

        public G setErrorHandler(ErrorHandler pErrorHandler) {
            this.mErrorHandler = pErrorHandler;
            return this;
        }

        public T build() {
            T t = onBuild(this.mContext);
            MolRequest molRequest = (MolRequest) t;
            Language unused = molRequest.mLanguage = this.mLanguage;
            IMolRequest.OnRequestListener unused2 = molRequest.mOnRequestListener = this.mOnRequestListener;
            ErrorHandler unused3 = molRequest.mErrorHandler = this.mErrorHandler;
            return t;
        }
    }
}
