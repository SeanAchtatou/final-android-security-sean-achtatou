package com.mol.seaplus.base.sdk.impl;

import com.mol.seaplus.base.sdk.IMolDialog;
import com.mol.seaplus.base.sdk.IMolDialogPresenter;

public class MolDialogPresenter implements IMolDialogPresenter {
    private IMolDialog mMolDialog;

    public MolDialogPresenter(IMolDialog pMolDialog) {
        this.mMolDialog = pMolDialog;
    }

    /* access modifiers changed from: protected */
    public IMolDialog getMolDialog() {
        return this.mMolDialog;
    }

    public void onCloseClicked() {
        this.mMolDialog.askForConfirmClose();
    }

    public void onConfirmClose(boolean pConfirm) {
        if (pConfirm) {
            this.mMolDialog.dismiss(true);
        }
    }
}
