package com.mol.seaplus.base.sdk;

public interface IMolDialogPresenter {
    void onCloseClicked();

    void onConfirmClose(boolean z);
}
