package com.mol.seaplus.base.sdk.impl;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.util.AttributeSet;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mol.seaplus.Log;
import com.mol.seaplus.base.sdk.IMolWebView;
import com.mol.seaplus.base.sdk.IMolWebViewPresenter;
import com.mol.seaplus.base.sdk.ProgressToolWebViewAdapter;
import com.mol.seaplus.base.sdk.ToolWebView;

public class MolWebView extends ToolWebView implements IMolWebView {
    /* access modifiers changed from: private */
    public IMolWebViewPresenter mMolWebViewPresenter;
    /* access modifiers changed from: private */
    public ProgressToolWebViewAdapter mProgressToolWebViewAdapter;
    /* access modifiers changed from: private */
    public String notification_error_ssl_cert_invalid = "Server SSL Certificate seems invalid. Do you want to continue ?";

    public MolWebView(Context context) {
        super(context);
        init();
    }

    public MolWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        this.mProgressToolWebViewAdapter = new ProgressToolWebViewAdapter(new WebChromeClient() {
            public boolean onJsAlert(WebView view, String url, String jsonMessage, JsResult result) {
                Log.d("jsonMessage = " + jsonMessage);
                if (!MolWebView.this.mMolWebViewPresenter.onCommand(jsonMessage)) {
                    return false;
                }
                result.confirm();
                return true;
            }

            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    newProgress = 0;
                }
                MolWebView.this.mProgressToolWebViewAdapter.setProgress(newProgress);
                view.invalidate();
            }
        }, new WebViewClient() {
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setMessage(MolWebView.this.notification_error_ssl_cert_invalid);
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                    }
                });
                builder.create().show();
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith("sms")) {
                    Log.d("SMS " + url);
                    Intent sendIntent = new Intent("android.intent.action.VIEW");
                    sendIntent.setType("vnd.android-dir/mms-sms");
                    sendIntent.setData(Uri.parse(url));
                    view.getContext().startActivity(sendIntent);
                    return true;
                } else if (!url.startsWith("https://web-pay.line.me")) {
                    return false;
                } else {
                    Log.d("LINEPAY " + url);
                    Intent sendIntent2 = new Intent("android.intent.action.VIEW");
                    sendIntent2.setData(Uri.parse(url));
                    view.getContext().startActivity(sendIntent2);
                    return true;
                }
            }
        });
        this.mProgressToolWebViewAdapter.enableProgressBar(true);
        getSettings().setJavaScriptEnabled(true);
        clearHistory();
        clearFormData();
        clearCache(true);
        setAdapter(this.mProgressToolWebViewAdapter);
    }

    public void setMolWebViewPresenter(IMolWebViewPresenter pMolWebViewPresenter) {
        this.mMolWebViewPresenter = pMolWebViewPresenter;
    }
}
