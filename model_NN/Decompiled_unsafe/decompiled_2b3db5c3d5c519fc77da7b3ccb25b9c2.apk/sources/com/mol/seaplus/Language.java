package com.mol.seaplus;

import com.bluepay.data.Config;

public class Language {
    public static final Language EN = new Language(0, "EN");
    public static final int EN_ID = 0;
    public static final Language ID = new Language(3, Config.COUNTRY_ID);
    public static final int ID_ID = 3;
    public static final Language MS = new Language(2, "MS");
    public static final int MS_ID = 2;
    public static final Language MY = new Language(5, "MY");
    public static final int MY_ID = 5;
    public static final Language TH = new Language(1, Config.COUNTRY_TH);
    public static final int TH_ID = 1;
    public static final Language VI = new Language(4, "VI");
    public static final int VI_ID = 4;
    private String mLanguageCode;
    private int mLanguageId;

    private Language(int pLanguageId, String pLanguageCode) {
        this.mLanguageId = pLanguageId;
        this.mLanguageCode = pLanguageCode;
    }

    public int getLanguageId() {
        return this.mLanguageId;
    }

    public String getLanguageCode() {
        return this.mLanguageCode;
    }
}
