package com.mol.seaplus.linepay.sdk;

import com.mol.seaplus.webview.sdk.Channel;

public final class LINEPay {
    public static final Channel channel = Channel.createChannel("LINEPAY", "linepay");
}
