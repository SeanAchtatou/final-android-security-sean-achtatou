package com.mol.seaplus.linepay.sdk;

import android.content.Context;
import com.mol.seaplus.webview.sdk.WebViewRequest;

public final class LINEPayRequest extends WebViewRequest {
    private LINEPayRequest(Context pContext) {
        super(pContext);
    }

    public LINEPayRequest onRequest(Context pContext) {
        return (LINEPayRequest) super.onRequest(pContext);
    }

    public static final class Builder extends WebViewRequest.Builder<LINEPayRequest, Builder> {
        public Builder(Context pContext) {
            super(pContext, LINEPay.channel);
        }

        /* access modifiers changed from: protected */
        public LINEPayRequest doBuild(Context pContext) {
            return new LINEPayRequest(pContext);
        }
    }
}
