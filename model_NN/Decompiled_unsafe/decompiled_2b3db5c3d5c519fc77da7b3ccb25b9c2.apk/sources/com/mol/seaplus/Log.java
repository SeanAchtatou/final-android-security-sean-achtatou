package com.mol.seaplus;

public final class Log {
    public static String TAG = "Mol-SDK";
    public static boolean enabled = true;

    private Log() {
    }

    public static void d(String text) {
        if (enabled) {
            android.util.Log.d(TAG, text);
        }
    }

    public static void e(String text) {
        e(text, null);
    }

    public static void e(String text, Throwable e) {
        if (enabled) {
            android.util.Log.e(TAG, text, e);
        }
    }
}
