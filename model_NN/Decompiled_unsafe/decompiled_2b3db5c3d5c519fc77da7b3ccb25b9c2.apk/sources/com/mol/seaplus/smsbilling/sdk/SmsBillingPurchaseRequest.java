package com.mol.seaplus.smsbilling.sdk;

import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import com.android.easy2pay.Easy2Pay;
import com.android.easy2pay.Easy2PayListener;
import com.mol.seaplus.Language;
import com.mol.seaplus.base.sdk.Sdk;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.sdk.psms.PSMSResponse;
import com.mol.seaplus.smsbilling.sdk.BaseSmsBillingRequest;
import com.mol.seaplus.tool.dialog.ToolAlertDialog;
import com.mol.seaplus.tool.utils.HttpUtils;
import com.mol.seaplus.tool.utils.os.UIHandler;
import org.json.JSONException;
import org.json.JSONObject;

public final class SmsBillingPurchaseRequest extends BaseSmsBillingRequest {
    private static final String CODE = "CODE";
    private static final String DATA = "DATA";
    private static final int UPDATE_ERROR = 2;
    private static final int UPDATE_EVENT = 1;
    private static final int UPDATE_SUCCESS = 0;
    private Easy2Pay mEasy2Pay;
    private Easy2PayListener mEasy2PayListener = new Easy2PayListener() {
        public void onEvent(String pTxId, String userId, String txId, int pEventCode, String pEventDescription) {
            switch (pEventCode) {
                case 201:
                    SmsBillingPurchaseRequest.this.updateUserCancel();
                    return;
                case 202:
                case 203:
                    SmsBillingPurchaseRequest.this.updateEvent(pEventCode);
                    return;
                default:
                    return;
            }
        }

        public void onError(String pTxId, String userId, String txId, int pErrorCode, String pErrorDescription) {
            SmsBillingPurchaseRequest.this.updateError(pErrorCode, pErrorDescription);
        }

        public void onPurchaseResult(String pPartnerTransactionId, String pUserId, String pPriceId, String pTxId, int pStatusCode, String pStatusDetail) {
            if (pStatusCode == 200) {
                JSONObject result = new JSONObject();
                try {
                    result.put(PSMSResponse.PARTNER_TRANSACTION_ID, pPartnerTransactionId);
                    result.put(PSMSResponse.USER_ID, pUserId);
                    result.put(PSMSResponse.PRICE_ID, pPriceId);
                    result.put(PSMSResponse.TRANSACTION_ID, pTxId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                SmsBillingPurchaseRequest.this.updateSuccess(result);
                return;
            }
            SmsBillingPurchaseRequest.this.sendError(pStatusCode, pStatusDetail);
        }
    };
    /* access modifiers changed from: private */
    public boolean mIsFullScreen;
    /* access modifiers changed from: private */
    public String mMcc;
    /* access modifiers changed from: private */
    public String mMnc;
    /* access modifiers changed from: private */
    public String mPriceId;
    /* access modifiers changed from: private */
    public int mSmsWaitingTime;
    private UIHandler mUiHandler = new UIHandler() {
        /* access modifiers changed from: protected */
        public boolean onHandlerMessage(Message message, int source) {
            if (message != null) {
                Bundle bundle = message.getData();
                int code = bundle.getInt(SmsBillingPurchaseRequest.CODE);
                String data = bundle.getString(SmsBillingPurchaseRequest.DATA);
                switch (message.what) {
                    case 0:
                        try {
                            SmsBillingPurchaseRequest.super.updateSuccess(new JSONObject(data));
                            return true;
                        } catch (JSONException e) {
                            e.printStackTrace();
                            return true;
                        }
                    case 1:
                        SmsBillingPurchaseRequest.super.updateEvent(code);
                        return true;
                    case 2:
                        SmsBillingPurchaseRequest.super.updateError(code, data);
                        return true;
                }
            }
            return false;
        }
    };

    public SmsBillingPurchaseRequest(Context pContext) {
        super(pContext);
    }

    public void onResume() {
        if (this.mEasy2Pay != null) {
            this.mEasy2Pay.onActivityResume();
        }
    }

    public void onPause() {
        if (this.mEasy2Pay != null) {
            this.mEasy2Pay.onActivityPause();
        }
    }

    public SmsBillingPurchaseRequest onRequest(Context pContext) {
        if (!HttpUtils.isHasNetworkConnection(pContext)) {
            updateError(16776960, Resources.getString(10));
        } else {
            if (TextUtils.isEmpty(this.mMcc) || TextUtils.isEmpty(this.mMnc)) {
                String[] simOperator = Sdk.getSimOperator(pContext);
                if (simOperator == null) {
                    updateError(303, Resources.getString(58));
                } else {
                    this.mMcc = simOperator[0];
                    this.mMnc = simOperator[1];
                }
            }
            if (!Sdk.isSupportSmsBilling(Integer.parseInt(this.mMcc), Integer.parseInt(this.mMnc))) {
                updateError(306, Resources.getString(59));
            } else {
                Language language = getLanguage();
                if (language == null) {
                    language = Language.EN;
                }
                Easy2Pay.Language e2pLanguage = null;
                switch (language.getLanguageId()) {
                    case 0:
                        e2pLanguage = Easy2Pay.Language.EN;
                        break;
                    case 1:
                        e2pLanguage = Easy2Pay.Language.TH;
                        break;
                    case 2:
                        e2pLanguage = Easy2Pay.Language.MS;
                        break;
                    case 3:
                        e2pLanguage = Easy2Pay.Language.ID;
                        break;
                    case 4:
                        e2pLanguage = Easy2Pay.Language.VI;
                        break;
                }
                if (e2pLanguage == null) {
                    e2pLanguage = Easy2Pay.Language.TH;
                }
                this.mEasy2Pay = new Easy2Pay(pContext, this.mPartnerId, this.mSecretKey, this.mSmsWaitingTime / 1000, this.mIsFullScreen, this.mMcc, this.mMnc, e2pLanguage);
                this.mEasy2Pay.setEasy2PayListener(this.mEasy2PayListener);
                this.mEasy2Pay.purchase(this.mPartnerTransactionId, this.mUserId, this.mPriceId);
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void updateSuccess(JSONObject pJSONObject) {
        Bundle bundle = new Bundle();
        bundle.putString(DATA, pJSONObject.toString());
        Message message = Message.obtain(this.mUiHandler, 0);
        message.setData(bundle);
        this.mUiHandler.sendMessage(message);
    }

    /* access modifiers changed from: protected */
    public void updateEvent(int pEventCode) {
        Bundle bundle = new Bundle();
        bundle.putInt(CODE, pEventCode);
        Message message = Message.obtain(this.mUiHandler, 1);
        message.setData(bundle);
        this.mUiHandler.sendMessage(message);
    }

    /* access modifiers changed from: protected */
    public void updateError(int pErrorCode, String pError) {
        doUpdateError(pErrorCode, getError(pErrorCode, pError));
    }

    private void doUpdateError(int pErrorCode, String pError) {
        ToolAlertDialog.alert(getContext(), pError);
        sendError(pErrorCode, pError);
    }

    /* access modifiers changed from: private */
    public void sendError(int pErrorCode, String pError) {
        Bundle bundle = new Bundle();
        bundle.putInt(CODE, pErrorCode);
        bundle.putString(DATA, pError);
        Message message = Message.obtain(this.mUiHandler, 2);
        message.setData(bundle);
        this.mUiHandler.sendMessage(message);
    }

    public static final class Builder extends BaseSmsBillingRequest.Builder<SmsBillingPurchaseRequest, Builder> {
        private boolean mIsFullScreen = true;
        private String mMcc;
        private String mMnc;
        private String mPriceId;
        private int mSmsWaitingTime = 180000;

        public Builder(Context pContext) {
            super(pContext);
        }

        public Builder mcc(String pMcc) {
            this.mMcc = pMcc;
            return this;
        }

        public Builder mnc(String pMnc) {
            this.mMnc = pMnc;
            return this;
        }

        public Builder priceId(String pPriceId) {
            this.mPriceId = pPriceId;
            return this;
        }

        public Builder smsWaitingTime(int pSmsWaitingTime) {
            this.mSmsWaitingTime = pSmsWaitingTime;
            return this;
        }

        public Builder fullScreen(boolean pIsFullScreen) {
            this.mIsFullScreen = pIsFullScreen;
            return this;
        }

        /* access modifiers changed from: protected */
        public boolean onCheck() {
            if (!TextUtils.isEmpty(this.mPriceId)) {
                return true;
            }
            throw new IllegalArgumentException(Resources.getString(57));
        }

        /* access modifiers changed from: protected */
        public SmsBillingPurchaseRequest doBuild(Context pContext) {
            SmsBillingPurchaseRequest smsBillingPurchaseRequest = new SmsBillingPurchaseRequest(pContext);
            String unused = smsBillingPurchaseRequest.mMcc = this.mMcc;
            String unused2 = smsBillingPurchaseRequest.mMnc = this.mMnc;
            String unused3 = smsBillingPurchaseRequest.mPriceId = this.mPriceId;
            int unused4 = smsBillingPurchaseRequest.mSmsWaitingTime = this.mSmsWaitingTime;
            boolean unused5 = smsBillingPurchaseRequest.mIsFullScreen = this.mIsFullScreen;
            return smsBillingPurchaseRequest;
        }
    }
}
