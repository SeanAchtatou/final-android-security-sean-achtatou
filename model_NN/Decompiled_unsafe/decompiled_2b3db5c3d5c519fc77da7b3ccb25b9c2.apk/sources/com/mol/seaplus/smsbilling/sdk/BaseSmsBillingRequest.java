package com.mol.seaplus.smsbilling.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.mol.seaplus.base.sdk.impl.MolRequest;
import com.mol.seaplus.base.sdk.impl.Resources;

abstract class BaseSmsBillingRequest extends MolRequest {
    protected String mPartnerId;
    protected String mPartnerTransactionId;
    protected String mSecretKey;
    protected String mUserId;

    public BaseSmsBillingRequest(Context pContext) {
        super(pContext);
    }

    protected static abstract class Builder<T extends BaseSmsBillingRequest, G extends Builder> extends MolRequest.Builder<T, G> {
        protected String mPartnerId;
        protected String mPartnerTransactionId;
        protected String mSecretKey;
        protected String mUserId;

        /* access modifiers changed from: protected */
        public abstract T doBuild(Context context);

        public Builder(Context pContext) {
            super(pContext);
        }

        public G partnerId(String pPartnerId) {
            this.mPartnerId = pPartnerId;
            return this;
        }

        public G secretKey(String pSecretKey) {
            this.mSecretKey = pSecretKey;
            return this;
        }

        public G userId(String pUserId) {
            this.mUserId = pUserId;
            return this;
        }

        public G partnerTransactionId(String pPartnerTransactionId) {
            this.mPartnerTransactionId = pPartnerTransactionId;
            return this;
        }

        /* access modifiers changed from: protected */
        public boolean onCheck() {
            return true;
        }

        /* access modifiers changed from: protected */
        public boolean check() {
            Context context = getContext();
            if (TextUtils.isEmpty(this.mPartnerId)) {
                throw new IllegalArgumentException(Resources.getString(53));
            } else if (TextUtils.isEmpty(this.mSecretKey)) {
                throw new IllegalArgumentException(Resources.getString(54));
            } else if (TextUtils.isEmpty(this.mUserId)) {
                throw new IllegalArgumentException(Resources.getString(56));
            } else if (TextUtils.isEmpty(this.mPartnerTransactionId)) {
                throw new IllegalArgumentException(Resources.getString(55));
            } else if (onCheck()) {
                return true;
            } else {
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public T onBuild(Context pContext) {
            if (!check()) {
                return null;
            }
            BaseSmsBillingRequest t = doBuild(pContext);
            BaseSmsBillingRequest baseSmsBillingRequest = t;
            baseSmsBillingRequest.mPartnerId = this.mPartnerId;
            baseSmsBillingRequest.mSecretKey = this.mSecretKey;
            baseSmsBillingRequest.mUserId = this.mUserId;
            baseSmsBillingRequest.mPartnerTransactionId = this.mPartnerTransactionId;
            return t;
        }
    }
}
