package com.mol.seaplus.tool.connection;

import com.mol.seaplus.tool.connection.handler.ErrorHandler;

public interface IConnection2 {
    void connect();

    IConnection2 deepCopy();

    IConnectionDescriptor getConnectionDescription();

    ConnectionListener getConnectionListener();

    ErrorHandler getErrorHandler();

    String getUrl();

    void immediateConnect();

    void setConnectionListener(ConnectionListener connectionListener);

    void setErrorHandler(ErrorHandler errorHandler);

    void stopConnect();
}
