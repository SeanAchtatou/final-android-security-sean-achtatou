package com.mol.seaplus.tool.connection.file;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import com.mol.seaplus.tool.connection.IConnection2;
import com.mol.seaplus.tool.connection.IConnectionDescriptor;
import com.mol.seaplus.tool.connection.SetableConnectionDescriptor;
import java.io.IOException;

public class AssetConnection extends FileConnection {
    /* access modifiers changed from: private */
    public Context context;

    public AssetConnection(Context context2, String url) {
        super(url);
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public IConnectionDescriptor doConnect(String url) {
        return new AssetConnectionDescriptor(this.context, url);
    }

    /* access modifiers changed from: protected */
    public IConnection2 onDeepCopy() {
        return new AssetConnection(this.context, getUrl());
    }

    public class AssetConnectionDescriptor extends SetableConnectionDescriptor {
        public AssetConnectionDescriptor(Context context, String fileName) {
            try {
                AssetFileDescriptor assetFileDescriptor = context.getAssets().openFd(fileName);
                setLength(assetFileDescriptor.getLength());
                setInputStream(assetFileDescriptor.createInputStream());
            } catch (IOException e) {
                try {
                    setInputStream(context.getAssets().open(fileName));
                } catch (IOException e1) {
                    e1.printStackTrace();
                    setError(FileConnection.FILE_NOT_FOUND_ERROR, e1.toString());
                }
            }
        }

        public IConnectionDescriptor deepCopy() {
            return new AssetConnectionDescriptor(AssetConnection.this.context, AssetConnection.this.getUrl());
        }
    }
}
