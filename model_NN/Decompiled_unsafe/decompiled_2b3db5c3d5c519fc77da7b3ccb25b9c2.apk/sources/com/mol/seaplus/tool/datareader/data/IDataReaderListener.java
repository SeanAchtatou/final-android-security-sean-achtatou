package com.mol.seaplus.tool.datareader.data;

import com.mol.seaplus.tool.datareader.data.IDataHolder;

public interface IDataReaderListener<T extends IDataHolder> {
    public static final int EXTRACT_DATA_ERROR = 16777184;

    void onFinish(IDataReader<T> iDataReader);
}
