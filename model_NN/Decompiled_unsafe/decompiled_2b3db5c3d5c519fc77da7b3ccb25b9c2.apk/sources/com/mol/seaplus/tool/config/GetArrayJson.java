package com.mol.seaplus.tool.config;

import android.os.AsyncTask;

/* compiled from: OrderCashcard */
class GetArrayJson extends AsyncTask<String, Integer, String> {
    GetArrayJson() {
    }

    /* JADX WARN: Type inference failed for: r10v5, types: [java.net.URLConnection] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String doInBackground(java.lang.String... r14) {
        /*
            r13 = this;
            r10 = 0
            r2 = 0
            r8 = 0
            r9 = r14[r10]
            java.net.URL r10 = new java.net.URL     // Catch:{ Exception -> 0x0041 }
            r10.<init>(r9)     // Catch:{ Exception -> 0x0041 }
            java.net.URLConnection r10 = r10.openConnection()     // Catch:{ Exception -> 0x0041 }
            r0 = r10
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0041 }
            r2 = r0
            java.lang.String r10 = "GET"
            r2.setRequestMethod(r10)     // Catch:{ Exception -> 0x0041 }
            r10 = 0
            r2.setAllowUserInteraction(r10)     // Catch:{ Exception -> 0x0041 }
            r10 = 1
            r2.setInstanceFollowRedirects(r10)     // Catch:{ Exception -> 0x0041 }
            r10 = 0
            r2.setUseCaches(r10)     // Catch:{ Exception -> 0x0041 }
            int r6 = r2.getResponseCode()     // Catch:{ Exception -> 0x0041 }
            r10 = 200(0xc8, float:2.8E-43)
            if (r6 != r10) goto L_0x006b
            r7 = 0
            java.io.InputStream r4 = r2.getInputStream()     // Catch:{ Exception -> 0x0041 }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0041 }
            r1.<init>()     // Catch:{ Exception -> 0x0041 }
            r5 = 0
        L_0x0036:
            int r5 = r4.read()     // Catch:{ Exception -> 0x0041 }
            r10 = -1
            if (r5 == r10) goto L_0x0060
            r1.write(r5)     // Catch:{ Exception -> 0x0041 }
            goto L_0x0036
        L_0x0041:
            r3 = move-exception
            java.lang.String r10 = "Load Array"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x0076 }
            r11.<init>()     // Catch:{ all -> 0x0076 }
            java.lang.String r12 = "Exception AsyncTask:"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x0076 }
            java.lang.StringBuilder r11 = r11.append(r3)     // Catch:{ all -> 0x0076 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x0076 }
            android.util.Log.e(r10, r11)     // Catch:{ all -> 0x0076 }
            if (r2 == 0) goto L_0x005f
            r2.disconnect()     // Catch:{ Exception -> 0x007d }
        L_0x005f:
            return r8
        L_0x0060:
            byte[] r7 = r1.toByteArray()     // Catch:{ Exception -> 0x0041 }
            java.lang.String r8 = r1.toString()     // Catch:{ Exception -> 0x0041 }
            r1.close()     // Catch:{ Exception -> 0x0041 }
        L_0x006b:
            r2.disconnect()     // Catch:{ Exception -> 0x0041 }
            if (r2 == 0) goto L_0x005f
            r2.disconnect()     // Catch:{ Exception -> 0x0074 }
            goto L_0x005f
        L_0x0074:
            r10 = move-exception
            goto L_0x005f
        L_0x0076:
            r10 = move-exception
            if (r2 == 0) goto L_0x007c
            r2.disconnect()     // Catch:{ Exception -> 0x007f }
        L_0x007c:
            throw r10
        L_0x007d:
            r10 = move-exception
            goto L_0x005f
        L_0x007f:
            r11 = move-exception
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mol.seaplus.tool.config.GetArrayJson.doInBackground(java.lang.String[]):java.lang.String");
    }
}
