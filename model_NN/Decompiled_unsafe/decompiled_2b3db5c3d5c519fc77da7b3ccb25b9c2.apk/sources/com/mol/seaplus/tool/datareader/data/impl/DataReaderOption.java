package com.mol.seaplus.tool.datareader.data.impl;

import com.mol.seaplus.tool.connection.IConnection2;
import com.mol.seaplus.tool.connection.internet.InternetConnection;
import com.mol.seaplus.tool.datareader.data.IDataHolder;
import com.mol.seaplus.tool.datareader.data.IDataReaderOption;

public class DataReaderOption implements IDataReaderOption {
    private IConnection2 conn;
    private IDataHolder dataHolder;

    public DataReaderOption() {
    }

    public DataReaderOption(IConnection2 conn2) {
        this.conn = conn2;
    }

    public IDataReaderOption setDataHolder(IDataHolder dataHolder2) {
        this.dataHolder = dataHolder2;
        return this;
    }

    public DataReaderOption setTimeout(int timeout) {
        if (this.conn != null && (this.conn instanceof InternetConnection)) {
            ((InternetConnection) this.conn).setTimeout(timeout);
        }
        return this;
    }

    public IDataHolder getDataHolder() {
        return this.dataHolder;
    }

    public IConnection2 getConnection() {
        return this.conn;
    }
}
