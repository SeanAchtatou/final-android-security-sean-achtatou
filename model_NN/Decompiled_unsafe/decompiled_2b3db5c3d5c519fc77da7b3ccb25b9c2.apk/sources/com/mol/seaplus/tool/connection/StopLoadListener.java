package com.mol.seaplus.tool.connection;

public interface StopLoadListener {
    void stopLoadAll();
}
