package com.mol.seaplus.tool.dialog.selector;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.mol.seaplus.tool.utils.UiUtils;

public abstract class ListViewSelectorAdapter<T> extends ArraySelectorAdapter<T> {
    private int currentIndex;
    private ListView listView;
    private BaseAdapter listViewAdapter;
    private AdapterView.OnItemClickListener listViewOnItemClickListener;
    private int styleResId;
    private String title;
    private TextView titleView;

    public interface OnListViewSelectorAdapterItemSelectListener {
        void onListViewSelectorItemSelected(ListViewSelectorAdapter<?> listViewSelectorAdapter, int i);
    }

    public abstract Object getItem(int i);

    public abstract long getItemId(int i);

    public abstract View getView(int i, View view, ViewGroup viewGroup);

    public ListViewSelectorAdapter(Context context, T[] datas) {
        this(context, datas, 0);
    }

    public ListViewSelectorAdapter(Context context, T[] datas, int currentIndex2) {
        super(context, datas);
        this.listViewOnItemClickListener = new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                ListViewSelectorAdapter.this.itemClicked(view, position, id);
                ListViewSelectorAdapter.this.dismissDialog();
            }
        };
        this.listViewAdapter = new BaseAdapter() {
            public View getView(int position, View convertView, ViewGroup parent) {
                return ListViewSelectorAdapter.this.getView(position, convertView, parent);
            }

            public long getItemId(int position) {
                return ListViewSelectorAdapter.this.getItemId(position);
            }

            public Object getItem(int position) {
                return ListViewSelectorAdapter.this.getItem(position);
            }

            public int getCount() {
                return ListViewSelectorAdapter.this.getDataCount();
            }
        };
        this.currentIndex = currentIndex2;
    }

    public void setTitle(String title2) {
        this.title = title2;
        if (this.titleView != null) {
            this.titleView.setText(title2);
            if (title2 == null || title2.length() <= 0) {
                this.titleView.setVisibility(8);
            } else {
                this.titleView.setVisibility(0);
            }
        }
    }

    public String getTitle() {
        return this.title;
    }

    public View getTitleView(ViewGroup parent) {
        if (this.titleView == null) {
            Context context = getContext();
            this.titleView = new TextView(context);
            this.titleView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            this.titleView.setGravity(17);
            this.titleView.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
            this.titleView.setTextColor(-1);
            this.titleView.setTextSize(1, 20.0f);
            this.titleView.setTypeface(null, 1);
            this.titleView.setPadding(0, (int) UiUtils.convertDpiToPixel(context, 10), 0, (int) UiUtils.convertDpiToPixel(context, 10));
            setTitle(this.title);
        }
        return this.titleView;
    }

    public View getBodyView(ViewGroup parent) {
        if (this.listView == null) {
            this.listView = new ListView(getContext());
            this.listView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            this.listView.setAdapter((ListAdapter) this.listViewAdapter);
            this.listView.setSelection(this.currentIndex);
            this.listView.setOnItemClickListener(this.listViewOnItemClickListener);
        }
        return this.listView;
    }

    public void notifyDataSetChanged() {
        this.listViewAdapter.notifyDataSetChanged();
    }
}
