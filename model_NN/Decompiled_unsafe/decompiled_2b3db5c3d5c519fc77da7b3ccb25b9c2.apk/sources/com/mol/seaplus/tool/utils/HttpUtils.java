package com.mol.seaplus.tool.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.Hashtable;
import org.apache.http.HttpHost;

public class HttpUtils {
    static final String[] hex = {"%00", "%01", "%02", "%03", "%04", "%05", "%06", "%07", "%08", "%09", "%0a", "%0b", "%0c", "%0d", "%0e", "%0f", "%10", "%11", "%12", "%13", "%14", "%15", "%16", "%17", "%18", "%19", "%1a", "%1b", "%1c", "%1d", "%1e", "%1f", "%20", "%21", "%22", "%23", "%24", "%25", "%26", "%27", "%28", "%29", "%2a", "%2b", "%2c", "%2d", "%2e", "%2f", "%30", "%31", "%32", "%33", "%34", "%35", "%36", "%37", "%38", "%39", "%3a", "%3b", "%3c", "%3d", "%3e", "%3f", "%40", "%41", "%42", "%43", "%44", "%45", "%46", "%47", "%48", "%49", "%4a", "%4b", "%4c", "%4d", "%4e", "%4f", "%50", "%51", "%52", "%53", "%54", "%55", "%56", "%57", "%58", "%59", "%5a", "%5b", "%5c", "%5d", "%5e", "%5f", "%60", "%61", "%62", "%63", "%64", "%65", "%66", "%67", "%68", "%69", "%6a", "%6b", "%6c", "%6d", "%6e", "%6f", "%70", "%71", "%72", "%73", "%74", "%75", "%76", "%77", "%78", "%79", "%7a", "%7b", "%7c", "%7d", "%7e", "%7f", "%80", "%81", "%82", "%83", "%84", "%85", "%86", "%87", "%88", "%89", "%8a", "%8b", "%8c", "%8d", "%8e", "%8f", "%90", "%91", "%92", "%93", "%94", "%95", "%96", "%97", "%98", "%99", "%9a", "%9b", "%9c", "%9d", "%9e", "%9f", "%a0", "%a1", "%a2", "%a3", "%a4", "%a5", "%a6", "%a7", "%a8", "%a9", "%aa", "%ab", "%ac", "%ad", "%ae", "%af", "%b0", "%b1", "%b2", "%b3", "%b4", "%b5", "%b6", "%b7", "%b8", "%b9", "%ba", "%bb", "%bc", "%bd", "%be", "%bf", "%c0", "%c1", "%c2", "%c3", "%c4", "%c5", "%c6", "%c7", "%c8", "%c9", "%ca", "%cb", "%cc", "%cd", "%ce", "%cf", "%d0", "%d1", "%d2", "%d3", "%d4", "%d5", "%d6", "%d7", "%d8", "%d9", "%da", "%db", "%dc", "%dd", "%de", "%df", "%e0", "%e1", "%e2", "%e3", "%e4", "%e5", "%e6", "%e7", "%e8", "%e9", "%ea", "%eb", "%ec", "%ed", "%ee", "%ef", "%f0", "%f1", "%f2", "%f3", "%f4", "%f5", "%f6", "%f7", "%f8", "%f9", "%fa", "%fb", "%fc", "%fd", "%fe", "%ff"};

    public static String getParam(Hashtable<String, String> hParam, boolean isForPost) {
        return getParam(hParam, isForPost, false);
    }

    public static String getParam(Hashtable<String, String> hParam, boolean isForPost, boolean isEncode) {
        String param = "";
        Enumeration<String> keys = hParam.keys();
        int index = 0;
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            String value = hParam.get(key);
            if (isEncode) {
                value = URLEncoder.encode(value);
            }
            param = param + key + "=" + value;
            if (index < hParam.size() - 1) {
                param = param + "&";
            }
            index++;
        }
        if (!isForPost) {
            return "?" + param;
        }
        return param;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:15:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.String URLEncode2(java.lang.String r12) {
        /*
            r10 = 0
            java.net.URL r11 = new java.net.URL     // Catch:{ MalformedURLException -> 0x002e, URISyntaxException -> 0x0033 }
            r11.<init>(r12)     // Catch:{ MalformedURLException -> 0x002e, URISyntaxException -> 0x0033 }
            java.net.URI r0 = new java.net.URI     // Catch:{ MalformedURLException -> 0x0040, URISyntaxException -> 0x003d }
            java.lang.String r1 = r11.getProtocol()     // Catch:{ MalformedURLException -> 0x0040, URISyntaxException -> 0x003d }
            java.lang.String r2 = r11.getUserInfo()     // Catch:{ MalformedURLException -> 0x0040, URISyntaxException -> 0x003d }
            java.lang.String r3 = r11.getHost()     // Catch:{ MalformedURLException -> 0x0040, URISyntaxException -> 0x003d }
            int r4 = r11.getPort()     // Catch:{ MalformedURLException -> 0x0040, URISyntaxException -> 0x003d }
            java.lang.String r5 = r11.getPath()     // Catch:{ MalformedURLException -> 0x0040, URISyntaxException -> 0x003d }
            java.lang.String r6 = r11.getQuery()     // Catch:{ MalformedURLException -> 0x0040, URISyntaxException -> 0x003d }
            java.lang.String r7 = r11.getRef()     // Catch:{ MalformedURLException -> 0x0040, URISyntaxException -> 0x003d }
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ MalformedURLException -> 0x0040, URISyntaxException -> 0x003d }
            java.net.URL r10 = r0.toURL()     // Catch:{ MalformedURLException -> 0x0040, URISyntaxException -> 0x003d }
        L_0x002b:
            if (r10 != 0) goto L_0x0038
        L_0x002d:
            return r12
        L_0x002e:
            r8 = move-exception
        L_0x002f:
            r8.printStackTrace()
            goto L_0x002b
        L_0x0033:
            r9 = move-exception
        L_0x0034:
            r9.printStackTrace()
            goto L_0x002b
        L_0x0038:
            java.lang.String r12 = r10.toString()
            goto L_0x002d
        L_0x003d:
            r9 = move-exception
            r10 = r11
            goto L_0x0034
        L_0x0040:
            r8 = move-exception
            r10 = r11
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mol.seaplus.tool.utils.HttpUtils.URLEncode2(java.lang.String):java.lang.String");
    }

    public static boolean isHttpConnection(String path) {
        if (path == null || !path.startsWith(HttpHost.DEFAULT_SCHEME_NAME)) {
            return false;
        }
        return true;
    }

    public static String URLdecode(String str) {
        StringBuffer result = new StringBuffer();
        int l = str.length();
        int i = 0;
        while (i < l) {
            char c = str.charAt(i);
            if (c != '%' || i + 2 >= l) {
                result.append(c);
            } else {
                char c1 = str.charAt(i + 1);
                char c2 = str.charAt(i + 2);
                if (!isHexit(c1) || !isHexit(c2)) {
                    result.append(c);
                } else {
                    result.append((char) ((hexit(c1) * 16) + hexit(c2)));
                    i += 2;
                }
            }
            i++;
        }
        return result.toString();
    }

    private static boolean isHexit(char c) {
        return "0123456789abcdefABCDEF".indexOf(c) != -1;
    }

    private static int hexit(char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        if (c >= 'a' && c <= 'f') {
            return (c - 'a') + 10;
        }
        if (c < 'A' || c > 'F') {
            return 0;
        }
        return (c - 'A') + 10;
    }

    public static String URLencode(String s) {
        if (s == null) {
            return null;
        }
        StringBuffer sbuf = new StringBuffer();
        int len = s.length();
        for (int i = 0; i < len; i++) {
            int ch = s.charAt(i);
            if (65 <= ch && ch <= 90) {
                sbuf.append((char) ch);
            } else if (97 <= ch && ch <= 122) {
                sbuf.append((char) ch);
            } else if (48 <= ch && ch <= 57) {
                sbuf.append((char) ch);
            } else if (ch == 32) {
                sbuf.append('+');
            } else if (ch <= 127) {
                sbuf.append(hex[ch]);
            } else if (ch <= 2047) {
                sbuf.append(hex[(ch >> 6) | 192]);
                sbuf.append(hex[(ch & 63) | 128]);
            } else {
                sbuf.append(hex[(ch >> 12) | 224]);
                sbuf.append(hex[((ch >> 6) & 63) | 128]);
                sbuf.append(hex[(ch & 63) | 128]);
            }
        }
        return sbuf.toString();
    }

    public static boolean isHasNetworkConnection(Context ctx) {
        NetworkInfo[] info;
        ConnectivityManager connectivity = (ConnectivityManager) ctx.getSystemService("connectivity");
        if (!(connectivity == null || (info = connectivity.getAllNetworkInfo()) == null)) {
            for (int i = 0; i < info.length; i++) {
                if ((String.valueOf(info[i].getTypeName()).equalsIgnoreCase("MOBILE") || String.valueOf(info[i].getTypeName()).equalsIgnoreCase("WIFI")) && info[i].getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }
}
