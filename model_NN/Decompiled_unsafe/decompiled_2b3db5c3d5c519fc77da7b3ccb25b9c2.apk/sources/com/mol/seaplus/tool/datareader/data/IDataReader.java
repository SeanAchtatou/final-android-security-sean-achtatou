package com.mol.seaplus.tool.datareader.data;

import com.mol.seaplus.tool.connection.handler.ErrorHandler;
import com.mol.seaplus.tool.datareader.data.IDataHolder;
import java.io.InputStream;
import java.util.Vector;

public interface IDataReader<T extends IDataHolder> {
    public static final int READ_ERROR = -1;
    public static final int READ_OK = 200;

    Vector<T> getAllData();

    T getData(int i);

    IDataReaderOption getDataReaderOption();

    String getError();

    int getErrorCode();

    int getReadCode();

    String getResponse();

    String getTableData(String str);

    void read();

    String readFromInputStream(InputStream inputStream);

    void setErrorHandler(ErrorHandler errorHandler);

    void setListener(IDataReaderListener<T> iDataReaderListener);

    void setResponse(String str);

    void stopRead();
}
