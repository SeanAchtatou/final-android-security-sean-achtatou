package com.mol.seaplus.tool.datareader.data.impl;

import android.content.Intent;
import com.mol.seaplus.tool.datareader.data.IDataHolder;
import com.mol.seaplus.tool.utils.StringUtils;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class DataHolder implements IDataHolder {
    protected Object[] datas;
    private String[] keys;
    private String tagName;

    public abstract DataHolder initDataHolder();

    public DataHolder() {
    }

    public DataHolder(String tagName2, String... keys2) {
        setKeys(keys2);
        this.tagName = tagName2;
    }

    /* access modifiers changed from: protected */
    public void injectKeys(String... superKeys) {
        if (superKeys != null) {
            ArrayList<String> keyList = new ArrayList<>(Arrays.asList(getKeys()));
            boolean keyChange = false;
            for (String key : superKeys) {
                if (!keyList.contains(key)) {
                    keyList.add(key);
                    keyChange = true;
                }
            }
            if (keyChange) {
                String[] keys2 = new String[keyList.size()];
                for (int i = 0; i < keyList.size(); i++) {
                    keys2[i] = (String) keyList.get(i);
                }
                setKeys(keys2);
            }
        }
    }

    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        for (String key : getKeys()) {
            Object obj = get(key);
            if (obj != null) {
                jsonObject.put(key, obj);
            }
        }
        return jsonObject;
    }

    public String toJsonString() throws JSONException {
        JSONObject jObj = toJson();
        if (jObj != null) {
            return jObj.toString();
        }
        return null;
    }

    public void setKeys(String[] keys2) {
        this.keys = keys2;
        this.datas = new Object[keys2.length];
    }

    public final String[] getKeys() {
        return this.keys;
    }

    public final String getTagName() {
        return this.tagName;
    }

    public void remove(String key) {
        int index = getKeyIndex(key);
        if (index != -1) {
            this.datas[index] = null;
        }
    }

    public final boolean isContain(String key) {
        return getKeyIndex(key) >= 0;
    }

    public final void put(String key, Object data) {
        int index;
        if (data != null && (index = getKeyIndex(key)) != -1) {
            this.datas[index] = data;
        }
    }

    public final void put(String key, boolean value) {
        put(key, new Boolean(value));
    }

    public final void put(String key, int data) {
        put(key, new Integer(data));
    }

    public final void put(String key, long data) {
        put(key, new Long(data));
    }

    public String getString(String key) {
        Object result = get(key);
        if (result instanceof String) {
            return (String) result;
        }
        return null;
    }

    public final String getStringNotNull(String key) {
        String result = getString(key);
        if (result == null) {
            return "";
        }
        return result;
    }

    public final int getInt(String key, int defaultValue) {
        return get(key) == null ? defaultValue : ((Integer) get(key)).intValue();
    }

    public final long getLong(String key, long defValue) {
        return get(key) == null ? defValue : ((Long) get(key)).longValue();
    }

    public final double getDouble(String key, double defValue) {
        return get(key) == null ? defValue : ((Double) get(key)).doubleValue();
    }

    public final float getFloat(String key, float defValue) {
        return get(key) == null ? defValue : ((Float) get(key)).floatValue();
    }

    public final Float getStringAsFloat(String key) {
        return getStringAsFloat(key, -1.0f);
    }

    public final Float getStringAsFloat(String key, float defaultValue) {
        String value = getString(key);
        if (value == null || value.length() == 0) {
            return Float.valueOf(defaultValue);
        }
        return Float.valueOf(Float.parseFloat(value));
    }

    public final Double getStringAsDouble(String key) {
        return getStringAsDouble(key, -1.0d);
    }

    public final Double getStringAsDouble(String key, double defaultValue) {
        String value = getString(key);
        if (value == null) {
            return Double.valueOf(defaultValue);
        }
        return Double.valueOf(Double.parseDouble(value));
    }

    public final long getStringAsLong(String key) {
        return Long.parseLong(getString(key));
    }

    public final int getStringAsInt(String key) {
        return getStringAsInt(key, -1);
    }

    public final int getStringAsInt(String key, int defaultValue) {
        String value = getString(key);
        return (value == null || value.length() == 0) ? defaultValue : Integer.parseInt(value);
    }

    public final boolean getBoolean(String key) {
        if (get(key) == null) {
            return false;
        }
        return ((Boolean) get(key)).booleanValue();
    }

    public final Object get(String key) {
        int index = getKeyIndex(key);
        if (index == -1) {
            return null;
        }
        return this.datas[index];
    }

    /* access modifiers changed from: protected */
    public int getKeyIndex(String key) {
        return StringUtils.getStingIndexInArray(key, this.keys);
    }

    public void writeDataToIntent(Intent intent) {
        for (String key : this.keys) {
            Object obj = get(key);
            if (obj instanceof String) {
                intent.putExtra(key, (String) obj);
            } else if (obj instanceof Integer) {
                intent.putExtra(key, (Integer) obj);
            } else if (obj instanceof Long) {
                intent.putExtra(key, (Long) obj);
            } else if (obj instanceof Double) {
                intent.putExtra(key, (Double) obj);
            } else if (obj instanceof Float) {
                intent.putExtra(key, (Float) obj);
            } else if (obj instanceof Boolean) {
                intent.putExtra(key, (Boolean) obj);
            }
        }
    }

    public void readDataFromIntent(Intent intent) {
        for (String key : this.keys) {
            if (intent.getExtras().containsKey(key)) {
                put(key, intent.getExtras().get(key));
            }
        }
    }
}
