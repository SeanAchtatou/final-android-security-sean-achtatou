package com.mol.seaplus.tool.connection.internet.httpurlconnection;

import android.content.Context;
import com.mol.seaplus.tool.utils.HttpUtils;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.util.Hashtable;
import org.apache.http.client.methods.HttpPost;

public class HttpUrlPostConnection extends HttpURLConnection {
    private Hashtable<String, String> param;
    private String postData;

    public HttpUrlPostConnection(Context context, String url) {
        super(context, url);
    }

    public HttpUrlPostConnection(Context context, String url, Hashtable<String, String> param2) {
        super(context, url);
        this.param = param2;
        if (param2 != null) {
            setPostEntity(HttpUtils.getParam(param2, true, false));
        }
    }

    public Hashtable<String, String> getParams() {
        return this.param;
    }

    public void setPostEntity(String postData2) {
        this.postData = postData2;
    }

    public String getPostEntity() {
        return this.postData;
    }

    /* access modifiers changed from: protected */
    public String getRequestMethod() {
        return HttpPost.METHOD_NAME;
    }

    /* access modifiers changed from: protected */
    public void doHttpConnect(HttpURLConnection connection) throws IOException {
        connection.setDoOutput(true);
        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
        writer.write(this.postData);
        writer.flush();
    }

    /* access modifiers changed from: protected */
    public HttpURLConnection doDeepCopy() {
        return new HttpUrlPostConnection(getContext(), getUrl(), this.param);
    }
}
