package com.mol.seaplus.tool.utils;

import android.util.Patterns;
import java.util.Formatter;
import java.util.Hashtable;
import java.util.Vector;

public class StringUtils {
    private static Hashtable<String, String> htmlEntities = new Hashtable<>();

    static {
        htmlEntities.put("&lt;", "<");
        htmlEntities.put("&gt;", ">");
        htmlEntities.put("&amp;", "&");
        htmlEntities.put("&quot;", "\"");
        htmlEntities.put("&agrave;", "ÃƒÂ ");
        htmlEntities.put("&Agrave;", "Ãƒâ‚¬");
        htmlEntities.put("&acirc;", "ÃƒÂ¢");
        htmlEntities.put("&auml;", "ÃƒÂ¤");
        htmlEntities.put("&Auml;", "Ãƒâ€ž");
        htmlEntities.put("&Acirc;", "Ãƒâ€š");
        htmlEntities.put("&aring;", "ÃƒÂ¥");
        htmlEntities.put("&Aring;", "Ãƒâ€¦");
        htmlEntities.put("&aelig;", "ÃƒÂ¦");
        htmlEntities.put("&AElig;", "Ãƒâ€ ");
        htmlEntities.put("&ccedil;", "ÃƒÂ§");
        htmlEntities.put("&Ccedil;", "Ãƒâ€¡");
        htmlEntities.put("&eacute;", "ÃƒÂ©");
        htmlEntities.put("&Eacute;", "Ãƒâ€°");
        htmlEntities.put("&egrave;", "ÃƒÂ¨");
        htmlEntities.put("&Egrave;", "ÃƒË†");
        htmlEntities.put("&ecirc;", "ÃƒÂª");
        htmlEntities.put("&Ecirc;", "ÃƒÅ ");
        htmlEntities.put("&euml;", "ÃƒÂ«");
        htmlEntities.put("&Euml;", "Ãƒâ€¹");
        htmlEntities.put("&iuml;", "ÃƒÂ¯");
        htmlEntities.put("&Iuml;", "Ãƒï¿½");
        htmlEntities.put("&ocirc;", "ÃƒÂ´");
        htmlEntities.put("&Ocirc;", "Ãƒâ€�");
        htmlEntities.put("&ouml;", "ÃƒÂ¶");
        htmlEntities.put("&Ouml;", "Ãƒâ€“");
        htmlEntities.put("&oslash;", "ÃƒÂ¸");
        htmlEntities.put("&Oslash;", "ÃƒËœ");
        htmlEntities.put("&szlig;", "ÃƒÅ¸");
        htmlEntities.put("&ugrave;", "ÃƒÂ¹");
        htmlEntities.put("&Ugrave;", "Ãƒâ„¢");
        htmlEntities.put("&ucirc;", "ÃƒÂ»");
        htmlEntities.put("&Ucirc;", "Ãƒâ€º");
        htmlEntities.put("&uuml;", "ÃƒÂ¼");
        htmlEntities.put("&Uuml;", "ÃƒÅ“");
        htmlEntities.put("&nbsp;", " ");
        htmlEntities.put("&copy;", "©");
        htmlEntities.put("&reg;", "®");
        htmlEntities.put("&euro;", "₠");
    }

    public static int getStringAsInt(String source, int defaultValue) {
        if (source == null || source.length() <= 0) {
            return defaultValue;
        }
        try {
            return Integer.parseInt(source);
        } catch (Exception e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public static long getStringAsLong(String source, long defaultValue) {
        if (source == null || source.length() <= 0) {
            return defaultValue;
        }
        try {
            return Long.parseLong(source);
        } catch (Exception e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public static float getStringAsLong(String source, float defaultValue) {
        if (source == null || source.length() <= 0) {
            return defaultValue;
        }
        try {
            return Float.parseFloat(source);
        } catch (Exception e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public static double getStringAsLong(String source, double defaultValue) {
        if (source == null || source.length() <= 0) {
            return defaultValue;
        }
        try {
            return Double.parseDouble(source);
        } catch (Exception e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public static boolean getStringAsInt(String source, boolean defaultValue) {
        if (source == null || source.length() <= 0) {
            return defaultValue;
        }
        try {
            return Boolean.parseBoolean(source);
        } catch (Exception e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public static boolean checkPhoneNumberFormat(String phoneNumber) {
        if (phoneNumber != null && phoneNumber.length() >= 9 && phoneNumber.length() <= 10) {
            return Patterns.PHONE.matcher(phoneNumber).matches();
        }
        return false;
    }

    public static boolean checkEmailFormat(String checkValue) {
        if (checkValue == null) {
            return false;
        }
        return Patterns.EMAIL_ADDRESS.matcher(checkValue).matches();
    }

    public static String toHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        Formatter formatter = new Formatter(sb);
        int length = bytes.length;
        for (int i = 0; i < length; i++) {
            formatter.format("%02x", Byte.valueOf(bytes[i]));
        }
        return sb.toString();
    }

    public static String unescapeHTML(String source, int start) {
        int j;
        String value;
        int i = source.indexOf("&", start);
        if (i <= -1 || (j = source.indexOf(";", i)) <= i || (value = htmlEntities.get(source.substring(i, j + 1))) == null) {
            return source;
        }
        return unescapeHTML(new StringBuffer().append(source.substring(0, i)).append(value).append(source.substring(j + 1)).toString(), i + 1);
    }

    public static String[] split(String inString, String delimeter) {
        String[] retAr = new String[0];
        try {
            Vector vec = new Vector();
            int indexA = 0;
            int indexB = inString.indexOf(delimeter);
            while (indexB != -1) {
                if (indexB > indexA) {
                    vec.addElement(new String(inString.substring(indexA, indexB)));
                }
                indexA = indexB + delimeter.length();
                indexB = inString.indexOf(delimeter, indexA);
            }
            vec.addElement(new String(inString.substring(indexA, inString.length())));
            retAr = new String[vec.size()];
            for (int i = 0; i < vec.size(); i++) {
                retAr[i] = vec.elementAt(i).toString();
            }
        } catch (Exception e) {
        }
        return retAr;
    }

    public static String replaceAll(String source, String pattern, String replacement) {
        if (source == null) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        int patIdx = 0;
        while (true) {
            int idx = source.indexOf(pattern, patIdx);
            if (idx != -1) {
                sb.append(source.substring(patIdx, idx));
                sb.append(replacement);
                patIdx = idx + pattern.length();
            } else {
                sb.append(source.substring(patIdx));
                return sb.toString();
            }
        }
    }

    public static int getStingIndexInArray(String findString, String[] stringArray) {
        for (int i = 0; i < stringArray.length; i++) {
            if (findString.equals(stringArray[i])) {
                return i;
            }
        }
        return -1;
    }
}
