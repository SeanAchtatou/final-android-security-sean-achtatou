package com.mol.seaplus.tool.utils;

import android.content.Context;
import android.util.TypedValue;

public class UiUtils {
    public static float convertDpiToPixel(Context context, int dp) {
        return applyDimension(context, 1, dp);
    }

    public static float getSPToPixel(Context context, int sp) {
        return applyDimension(context, 2, sp);
    }

    public static final float applyDimension(Context context, int unit, int value) {
        return TypedValue.applyDimension(unit, (float) value, context.getResources().getDisplayMetrics());
    }
}
