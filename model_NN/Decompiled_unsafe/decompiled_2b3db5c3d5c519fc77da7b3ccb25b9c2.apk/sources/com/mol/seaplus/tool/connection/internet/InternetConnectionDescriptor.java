package com.mol.seaplus.tool.connection.internet;

import com.mol.seaplus.tool.connection.SetableConnectionDescriptor;
import java.util.List;
import java.util.Map;

public abstract class InternetConnectionDescriptor extends SetableConnectionDescriptor {
    public abstract Map<String, List<String>> getAllRequestHeader();

    public abstract Map<String, List<String>> getAllResponseHeader();

    public abstract List<String> getRequestHeaders(String str);

    public abstract List<String> getResponseHeaders(String str);

    public InternetConnectionDescriptor() {
    }

    public InternetConnectionDescriptor(int errorCode, String error) {
        setError(errorCode, error);
    }
}
