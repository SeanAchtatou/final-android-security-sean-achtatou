package com.mol.seaplus.tool.connection;

import android.os.AsyncTask;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ProgressDownloader extends AsyncTask<Void, Long, String> {
    private static final int BUFFER_SIZE = 1024;
    private int bufferSize = 1024;
    private InputStream is;
    private long length;
    private OnProgressListener onProgressListener;
    private OutputStream os;

    public interface OnProgressListener {
        public static final int PROGRESS_DOWNLOAD_ERROR = 13634;

        void onFinishLoad();

        void onLoadFailed(int i, String str);

        void onProgressUpdate(long j, long j2);
    }

    public ProgressDownloader(InputStream is2, OutputStream os2, long length2) {
        this.is = is2;
        this.os = os2;
        this.length = length2;
    }

    public void setBufferSize(int size) {
        this.bufferSize = size;
        if (this.bufferSize <= 0) {
            this.bufferSize = 1024;
        }
    }

    public void setOnProgressListener(OnProgressListener onProgressListener2) {
        this.onProgressListener = onProgressListener2;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(Void... params) {
        byte[] buffers = new byte[this.bufferSize];
        String error = null;
        long currentLoad = 0;
        while (true) {
            try {
                int chunk = this.is.read(buffers);
                if (chunk != -1) {
                    this.os.write(buffers, 0, chunk);
                    currentLoad += (long) chunk;
                    publishProgress(Long.valueOf(currentLoad));
                } else {
                    try {
                        break;
                    } catch (IOException e) {
                    }
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                error = e2.toString();
                try {
                    if (this.is != null) {
                        this.is.close();
                    }
                    if (this.os != null) {
                        this.os.close();
                    }
                } catch (IOException e3) {
                }
            } catch (Throwable th) {
                try {
                    if (this.is != null) {
                        this.is.close();
                    }
                    if (this.os != null) {
                        this.os.close();
                    }
                } catch (IOException e4) {
                }
                throw th;
            }
        }
        if (this.is != null) {
            this.is.close();
        }
        if (this.os != null) {
            this.os.close();
        }
        return error;
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Long... values) {
        if (this.onProgressListener != null) {
            this.onProgressListener.onProgressUpdate(values[0].longValue(), this.length);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String error) {
        if (this.onProgressListener == null) {
            return;
        }
        if (error == null) {
            this.onProgressListener.onFinishLoad();
        } else {
            this.onProgressListener.onLoadFailed(OnProgressListener.PROGRESS_DOWNLOAD_ERROR, error);
        }
    }
}
