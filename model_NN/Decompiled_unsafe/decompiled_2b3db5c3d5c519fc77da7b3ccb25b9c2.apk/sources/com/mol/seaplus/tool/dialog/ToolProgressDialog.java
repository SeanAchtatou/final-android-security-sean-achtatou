package com.mol.seaplus.tool.dialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.mol.seaplus.tool.utils.os.UIHandler;

public class ToolProgressDialog extends ToolDialogInterface {
    public static final int DIALOG_DOWNLOAD_PROGRESS = 83014;
    public static final int DIALOG_PROGRESS = 83013;
    private int dialogType = DIALOG_PROGRESS;
    private boolean indeterminate;
    private int max;
    private String message;
    /* access modifiers changed from: private */
    public Dialog pDialog;
    private int progress;
    private String title;
    private UIHandler uiHandler = new UIHandler();

    public static ToolProgressDialog show(Context context) {
        return show(context, "Please wait", "Loading", true, false, null);
    }

    public static ToolProgressDialog show(Context context, DialogInterface.OnCancelListener listener) {
        return show(context, "Please wait", "Loading", true, true, listener);
    }

    public static ToolProgressDialog show(Context context, String title2, String message2) {
        return show(context, title2, message2, false);
    }

    public static ToolProgressDialog show(Context context, String title2, String message2, boolean indeterminate2) {
        return show(context, title2, message2, indeterminate2, false);
    }

    public static ToolProgressDialog show(Context context, String title2, String message2, boolean indeterminate2, boolean cancelable) {
        return show(context, title2, message2, indeterminate2, cancelable, null);
    }

    public static ToolProgressDialog show(Context context, String title2, String message2, boolean indeterminate2, boolean cancelable, DialogInterface.OnCancelListener cancelListener) {
        ToolProgressDialog tDialog = new ToolProgressDialog(context);
        tDialog.setTitle(title2);
        tDialog.setMessage(message2);
        tDialog.setIndeTerminate(indeterminate2);
        tDialog.setCancelable(cancelable);
        tDialog.setOnCancelListener(cancelListener);
        tDialog.show();
        return tDialog;
    }

    private ToolProgressDialog(Context context) {
        super(context);
    }

    public void setDialogType(int type) {
        this.dialogType = type;
    }

    public void setTitle(final String title2) {
        this.title = title2;
        if (this.pDialog != null) {
            this.uiHandler.post(new Runnable() {
                public void run() {
                    ToolProgressDialog.this.pDialog.setTitle(title2);
                }
            });
        }
    }

    public void setMessage(final String message2) {
        this.message = message2;
        if (this.pDialog != null && (this.pDialog instanceof ProgressDialog)) {
            this.uiHandler.post(new Runnable() {
                public void run() {
                    ((ProgressDialog) ToolProgressDialog.this.pDialog).setMessage(message2);
                }
            });
        }
    }

    public void setIndeTerminate(final boolean terminate) {
        this.indeterminate = terminate;
        if (this.pDialog != null && (this.pDialog instanceof ProgressDialog)) {
            this.uiHandler.post(new Runnable() {
                public void run() {
                    ((ProgressDialog) ToolProgressDialog.this.pDialog).setIndeterminate(terminate);
                }
            });
        }
    }

    public void setMax(final int max2) {
        this.max = max2;
        if (this.pDialog != null && (this.pDialog instanceof ProgressDialog)) {
            this.uiHandler.post(new Runnable() {
                public void run() {
                    ((ProgressDialog) ToolProgressDialog.this.pDialog).setMax(max2);
                }
            });
        }
    }

    public void setProgress(final int value) {
        this.progress = value;
        if (this.pDialog != null && (this.pDialog instanceof ProgressDialog)) {
            this.uiHandler.post(new Runnable() {
                public void run() {
                    ((ProgressDialog) ToolProgressDialog.this.pDialog).setProgress(value);
                }
            });
        }
    }

    public void setOnCancelListener(DialogInterface.OnCancelListener onCancelListener) {
        super.setOnCancelListener(onCancelListener);
        if (this.pDialog != null) {
            this.pDialog.setOnCancelListener(onCancelListener);
        }
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        super.setOnDismissListener(onDismissListener);
        if (this.pDialog != null) {
            this.pDialog.setOnDismissListener(onDismissListener);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog createDialog() {
        if (this.pDialog == null) {
            switch (this.dialogType) {
                case DIALOG_PROGRESS /*83013*/:
                    initBasicProgressDialog();
                    break;
                case DIALOG_DOWNLOAD_PROGRESS /*83014*/:
                    ProgressDialog pDialog2 = new ProgressDialog(this.context);
                    pDialog2.setTitle(this.title);
                    pDialog2.setMessage(this.message);
                    pDialog2.setProgressStyle(1);
                    pDialog2.setIndeterminate(this.indeterminate);
                    pDialog2.setMax(this.max);
                    pDialog2.setProgress(this.progress);
                    pDialog2.setCancelable(isCancelable());
                    pDialog2.setCanceledOnTouchOutside(isCanceledOnTouchOutside());
                    pDialog2.setOnCancelListener(getOnCancelListener());
                    pDialog2.setOnDismissListener(getOnDismissListener());
                    this.pDialog = pDialog2;
                    break;
            }
        }
        return this.pDialog;
    }

    private void initBasicProgressDialog() {
        if (getOnCancelListener() != null) {
            this.pDialog = ProgressDialog.show(this.context, this.title, this.message, this.indeterminate, isCancelable(), getOnCancelListener());
        } else {
            this.pDialog = ProgressDialog.show(this.context, this.title, this.message, this.indeterminate, isCancelable());
        }
    }
}
