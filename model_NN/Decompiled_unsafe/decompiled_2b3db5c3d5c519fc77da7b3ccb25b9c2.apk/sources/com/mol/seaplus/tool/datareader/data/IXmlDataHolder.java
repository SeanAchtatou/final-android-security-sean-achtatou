package com.mol.seaplus.tool.datareader.data;

import org.w3c.dom.Node;

public interface IXmlDataHolder extends IDataHolder {
    Node getXmlNode();

    void setXmlNode(Node node);
}
