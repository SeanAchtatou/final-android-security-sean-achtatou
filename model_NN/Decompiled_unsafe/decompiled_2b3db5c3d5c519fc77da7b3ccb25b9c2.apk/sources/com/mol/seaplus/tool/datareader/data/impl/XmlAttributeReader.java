package com.mol.seaplus.tool.datareader.data.impl;

import com.mol.seaplus.tool.datareader.data.IDataReaderOption;
import com.mol.seaplus.tool.datareader.data.IXmlDataHolder;
import java.util.Vector;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlAttributeReader<T extends IXmlDataHolder> extends XmlDataReader<T> {
    public XmlAttributeReader() {
    }

    public XmlAttributeReader(IDataReaderOption<T> iDataReaderOption) {
        super(iDataReaderOption);
    }

    public void extractXmlNode(NodeList list) {
        extractXmlNodeList(list, null);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void extractXmlNodeList(org.w3c.dom.NodeList r8, com.mol.seaplus.tool.datareader.data.IXmlDataHolder r9) {
        /*
            r7 = this;
            if (r8 == 0) goto L_0x004e
            int r6 = r8.getLength()
            if (r6 <= 0) goto L_0x004e
            r1 = r9
            if (r1 != 0) goto L_0x0015
            com.mol.seaplus.tool.datareader.data.IDataReaderOption r6 = r7.getDataReaderOption()
            com.mol.seaplus.tool.datareader.data.IDataHolder r1 = r6.getDataHolder()
            com.mol.seaplus.tool.datareader.data.IXmlDataHolder r1 = (com.mol.seaplus.tool.datareader.data.IXmlDataHolder) r1
        L_0x0015:
            r0 = 0
        L_0x0016:
            int r6 = r8.getLength()
            if (r0 >= r6) goto L_0x004e
            org.w3c.dom.Node r3 = r8.item(r0)
            java.lang.String r5 = r3.getNodeName()
            org.w3c.dom.NamedNodeMap r2 = r3.getAttributes()
            if (r1 == 0) goto L_0x004a
            java.lang.String r6 = r1.getTagName()
            boolean r6 = r5.equals(r6)
            if (r6 == 0) goto L_0x0046
            com.mol.seaplus.tool.datareader.data.IDataHolder r4 = r1.initDataHolder()
            com.mol.seaplus.tool.datareader.data.IXmlDataHolder r4 = (com.mol.seaplus.tool.datareader.data.IXmlDataHolder) r4
            r4.setXmlNode(r3)
            r7.extractDataNode(r3, r4)
            r7.addData(r4)
        L_0x0043:
            int r0 = r0 + 1
            goto L_0x0016
        L_0x0046:
            r7.extractDataNode(r3, r9)
            goto L_0x0043
        L_0x004a:
            r7.extractDataNode(r3, r9)
            goto L_0x0043
        L_0x004e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mol.seaplus.tool.datareader.data.impl.XmlAttributeReader.extractXmlNodeList(org.w3c.dom.NodeList, com.mol.seaplus.tool.datareader.data.IXmlDataHolder):void");
    }

    private void extractDataNode(Node node, IXmlDataHolder iXmlDataHolder) {
        extractAttr(node.getAttributes(), iXmlDataHolder);
        NodeList childList = node.getChildNodes();
        if (childList != null && childList.getLength() > 0) {
            if (iXmlDataHolder == null) {
                extractXmlNodeList(childList, iXmlDataHolder);
                return;
            }
            for (int i = 0; i < childList.getLength(); i++) {
                Node childNode = childList.item(i);
                String tag = childNode.getNodeName();
                if (iXmlDataHolder == null) {
                    extractDataNode(childNode, iXmlDataHolder);
                } else if (iXmlDataHolder.isContain(tag)) {
                    IXmlDataHolder childHolder = (IXmlDataHolder) iXmlDataHolder.getChildHolderByTag(tag);
                    if (childHolder == null) {
                        iXmlDataHolder.put(tag, childNode);
                    } else {
                        extractDataNode(childNode, childHolder);
                        Vector result = (Vector) iXmlDataHolder.get(tag);
                        if (result == null) {
                            result = new Vector();
                            iXmlDataHolder.put(tag, result);
                        }
                        result.add(childHolder);
                    }
                } else {
                    extractDataNode(childNode, iXmlDataHolder);
                }
            }
        }
    }

    private void extractAttr(NamedNodeMap nodeMapList, IXmlDataHolder iXmlDataHolder) {
        if (nodeMapList != null && nodeMapList.getLength() > 0) {
            for (int i = 0; i < nodeMapList.getLength(); i++) {
                Node node = nodeMapList.item(i);
                String key = node.getNodeName();
                String value = node.getNodeValue();
                if (iXmlDataHolder == null || !iXmlDataHolder.isContain(key)) {
                    putTableData(key, value);
                } else {
                    iXmlDataHolder.put(key, value);
                }
            }
        }
    }
}
