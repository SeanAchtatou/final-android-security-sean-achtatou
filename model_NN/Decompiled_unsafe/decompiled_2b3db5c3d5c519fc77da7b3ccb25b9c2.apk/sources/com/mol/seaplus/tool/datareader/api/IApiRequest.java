package com.mol.seaplus.tool.datareader.api;

import com.mol.seaplus.tool.connection.handler.ErrorHandler;

public interface IApiRequest {
    void request();

    void setErrorHandler(ErrorHandler errorHandler);
}
