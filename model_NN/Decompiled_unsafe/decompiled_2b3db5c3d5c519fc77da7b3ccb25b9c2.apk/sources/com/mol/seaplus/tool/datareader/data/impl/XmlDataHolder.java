package com.mol.seaplus.tool.datareader.data.impl;

import com.mol.seaplus.tool.datareader.data.IXmlDataHolder;
import org.w3c.dom.Node;

public abstract class XmlDataHolder extends DataHolder implements IXmlDataHolder {
    private Node node;

    public void setXmlNode(Node node2) {
        this.node = node2;
    }

    public Node getXmlNode() {
        return this.node;
    }
}
