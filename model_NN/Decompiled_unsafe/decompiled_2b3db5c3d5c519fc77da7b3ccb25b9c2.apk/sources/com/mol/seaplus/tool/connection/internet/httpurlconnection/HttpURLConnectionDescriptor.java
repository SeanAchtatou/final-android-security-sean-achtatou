package com.mol.seaplus.tool.connection.internet.httpurlconnection;

import com.mol.seaplus.Log;
import com.mol.seaplus.tool.connection.internet.InternetConnectionDescriptor;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class HttpURLConnectionDescriptor extends InternetConnectionDescriptor {
    private HttpURLConnection httpURLConnection;
    private HttpURLConnection mHttpURLConnection;
    private int resCode = -1;

    /* access modifiers changed from: protected */
    public void setMyHttpUrlConnection(HttpURLConnection httpURLConnection2) {
        this.mHttpURLConnection = httpURLConnection2;
    }

    public HttpURLConnection getMyHttpUrlConnection() {
        return this.mHttpURLConnection;
    }

    /* access modifiers changed from: protected */
    public final void setHttpURLConnection(HttpURLConnection httpURLConnection2) {
        this.httpURLConnection = httpURLConnection2;
    }

    /* access modifiers changed from: protected */
    public void onSetHttpURLConnection(HttpURLConnection httpURLConnection2) {
        try {
            this.resCode = httpURLConnection2.getResponseCode();
            Log.d("HttpUrlConnection responseFrom = " + this.mHttpURLConnection.getUrl());
            if (this.resCode == 200 || this.resCode == 201) {
                setLength((long) httpURLConnection2.getContentLength());
                setInputStream(httpURLConnection2.getInputStream());
                return;
            }
            setError(16776961, "Listening - Other HTTP_RESPONSE_CODE " + this.resCode);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.e("HttpURLConnection error MalformedURLException = " + e.toString());
            setError(16776964, e.toString());
        } catch (SocketTimeoutException e2) {
            e2.printStackTrace();
            Log.e("HttpURLConnection error SocketTimeoutException = " + e2.toString());
            setError(16776963, e2.toString());
        } catch (Exception e3) {
            e3.printStackTrace();
            Log.e("HttpURLConnection error Exception = " + e3.toString());
            setError(16776962, e3.toString());
        }
    }

    /* access modifiers changed from: protected */
    public void setResponseCode(int responseCode) {
        this.resCode = responseCode;
    }

    public int getResponseCode() {
        return this.resCode;
    }

    public HttpURLConnection getHttpUrlConnection() {
        return this.httpURLConnection;
    }

    public Map<String, List<String>> getAllRequestHeader() {
        return this.httpURLConnection.getRequestProperties();
    }

    public List<String> getRequestHeaders(String field) {
        return Arrays.asList(this.httpURLConnection.getRequestProperty(field));
    }

    public Map<String, List<String>> getAllResponseHeader() {
        return this.httpURLConnection.getHeaderFields();
    }

    public List<String> getResponseHeaders(String field) {
        return Arrays.asList(this.httpURLConnection.getHeaderField(field));
    }

    public HttpURLConnectionDescriptor deepCopy() {
        return new HttpURLConnectionDescriptor();
    }
}
