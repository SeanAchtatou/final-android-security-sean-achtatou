package com.mol.seaplus.tool.connection;

import java.io.InputStream;

public class ConnectionDescriptor extends SetableConnectionDescriptor {
    private InputStream in;
    private long length = -1;

    public ConnectionDescriptor() {
    }

    public ConnectionDescriptor(int errorCode, String error) {
        setError(errorCode, error);
    }

    public long getLength() {
        return this.length;
    }

    public boolean isSuccess() {
        return this.in != null;
    }

    public InputStream getInputStream() {
        return this.in;
    }

    public IConnectionDescriptor deepCopy() {
        return new ConnectionDescriptor();
    }
}
