package com.mol.seaplus.tool.datareader.api.impl;

import android.content.Context;
import com.mol.seaplus.tool.connection.handler.ErrorHandler;
import com.mol.seaplus.tool.datareader.api.IApiRequest;
import com.mol.seaplus.tool.datareader.data.IDataReader;
import com.mol.seaplus.tool.datareader.data.IDataReaderListener;

public abstract class ApiRequest implements IApiRequest {
    private Context context;
    private ErrorHandler errorHandler;
    private IDataReaderListener mDataReaderListener = new IDataReaderListener() {
        public void onFinish(IDataReader iDataReader) {
            ApiRequest.this.onApiResponse(iDataReader);
        }
    };
    private String source;

    /* access modifiers changed from: protected */
    public abstract String getApiDescription(IDataReader iDataReader);

    /* access modifiers changed from: protected */
    public abstract int getApiStatusCode(IDataReader iDataReader);

    /* access modifiers changed from: protected */
    public abstract boolean isSuccess(IDataReader iDataReader);

    /* access modifiers changed from: protected */
    public abstract void onApiResponse(IDataReader iDataReader);

    /* access modifiers changed from: protected */
    public abstract IDataReader onInitialRequest(Context context2);

    public ApiRequest(Context context2, String source2) {
        this.context = context2;
        this.source = source2;
    }

    /* access modifiers changed from: protected */
    public Context getContext() {
        return this.context;
    }

    /* access modifiers changed from: protected */
    public String getSource() {
        return this.source;
    }

    public void request() {
        IDataReader reader = onInitialRequest(this.context);
        reader.setErrorHandler(this.errorHandler);
        reader.setListener(this.mDataReaderListener);
        reader.read();
    }

    public void setErrorHandler(ErrorHandler errorHandler2) {
        this.errorHandler = errorHandler2;
    }

    /* access modifiers changed from: protected */
    public String getError(int errorCode, String error) {
        if (this.errorHandler != null) {
            return this.errorHandler.handlerError(errorCode, error);
        }
        return error;
    }

    public static abstract class Builder {
        private Context context;
        private ErrorHandler errorHandler;

        /* access modifiers changed from: protected */
        public abstract IApiRequest onBuild();

        public Builder(Context context2) {
            this.context = context2;
        }

        /* access modifiers changed from: protected */
        public Context getContext() {
            return this.context;
        }

        public Builder setErrorHandler(ErrorHandler errorHandler2) {
            this.errorHandler = errorHandler2;
            return this;
        }

        public IApiRequest build() {
            IApiRequest iApiRequest = onBuild();
            iApiRequest.setErrorHandler(this.errorHandler);
            return iApiRequest;
        }
    }
}
