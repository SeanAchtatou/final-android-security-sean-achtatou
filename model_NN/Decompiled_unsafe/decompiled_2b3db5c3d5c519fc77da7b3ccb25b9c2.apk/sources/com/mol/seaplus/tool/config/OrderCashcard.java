package com.mol.seaplus.tool.config;

import android.util.Log;
import com.mol.seaplus.prepaidcard.sdk2.PrepaidCard;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class OrderCashcard {
    private static String order_url = "https://api.easy2pay.co/add-on/sdk_server/webAPI_cashcard_return_sort_order.php";
    private ArrayList<PrepaidCard> cardorder = new ArrayList<>();

    public List<PrepaidCard> getCashcardOrder() {
        try {
            String resultStr = (String) new GetArrayJson().execute(order_url).get();
            if (resultStr != null) {
                JSONArray jArray = new JSONObject(resultStr).getJSONArray("sorfOrder");
                for (int i = 0; i < jArray.length(); i++) {
                    String json_string = jArray.getString(i);
                    if (json_string.equals("TRUEMONEY_CARD")) {
                        this.cardorder.add(PrepaidCard.TRUEMONEY_CARD);
                    } else if (json_string.equals("ONETWOCALL_CARD")) {
                        this.cardorder.add(PrepaidCard.ONETWOCALL_CARD);
                    } else if (json_string.equals("DTACHAPPY_CARD")) {
                        this.cardorder.add(PrepaidCard.DTACHAPPY_CARD);
                    } else if (json_string.equals("MOLPOINTS_CARD")) {
                        this.cardorder.add(PrepaidCard.MOLPOINTS_CARD);
                    }
                }
            }
        } catch (Exception e) {
            Log.e("load order execute", "Exception AsyncTask:" + e);
        }
        return this.cardorder;
    }
}
