package com.mol.seaplus.tool.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;

public class ToolAlertDialog extends ToolDialogInterface {
    private static final String[][] CHOICES = {new String[]{"OK"}, new String[]{"Yes", "No"}, new String[]{"OK", "Cancel"}};
    public static final int D_OK = 0;
    public static final int D_OK_CANCEL = 2;
    public static final int D_YES_NO = 1;
    /* access modifiers changed from: private */
    public Dialog aDialog;
    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            int which;
            if (v.equals(ToolAlertDialog.this.positiveBtn)) {
                which = -1;
            } else if (v.equals(ToolAlertDialog.this.neutralBtn)) {
                which = -3;
            } else {
                which = -2;
            }
            if (ToolAlertDialog.this.listener != null) {
                ToolAlertDialog.this.listener.onClick(ToolAlertDialog.this.aDialog, which);
            }
            ToolAlertDialog.this.dismiss();
        }
    };
    private String[] dialogChoices;
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener listener;
    private String message;
    private Button negativeBtn;
    /* access modifiers changed from: private */
    public Button neutralBtn;
    /* access modifiers changed from: private */
    public Button positiveBtn;

    public static ToolAlertDialog alert(Context context, int resId) {
        return alert(context, context.getString(resId));
    }

    public static ToolAlertDialog alert(Context context, String message2) {
        return show(context, message2, 0, (DialogInterface.OnClickListener) null);
    }

    public static ToolAlertDialog ask(Context context, int resId, DialogInterface.OnClickListener listener2) {
        return ask(context, context.getString(resId), listener2);
    }

    public static ToolAlertDialog ask(Context context, String message2, DialogInterface.OnClickListener listener2) {
        return show(context, message2, 1, listener2);
    }

    public static ToolAlertDialog askOk(Context context, int resId, DialogInterface.OnClickListener listener2) {
        return askOk(context, context.getString(resId), listener2);
    }

    public static ToolAlertDialog askOk(Context context, String message2, DialogInterface.OnClickListener listener2) {
        return show(context, message2, 2, listener2);
    }

    public static ToolAlertDialog show(Context context, int resId, int dialogType, DialogInterface.OnClickListener listener2) {
        return show(context, context.getString(resId), dialogType, listener2);
    }

    public static ToolAlertDialog show(Context context, String message2, int dialogType, DialogInterface.OnClickListener listener2) {
        return show(context, message2, CHOICES[dialogType], listener2);
    }

    public static ToolAlertDialog show(Context context, int resId, String[] dialogChoices2, DialogInterface.OnClickListener listener2) {
        return show(context, context.getString(resId), dialogChoices2, listener2);
    }

    public static ToolAlertDialog show(Context context, String message2, String[] dialogChoices2, DialogInterface.OnClickListener listener2) {
        ToolAlertDialog dialog = new ToolAlertDialog(context, message2, dialogChoices2, listener2);
        dialog.show();
        return dialog;
    }

    protected ToolAlertDialog(Context context, String message2, String[] dialogChoices2, DialogInterface.OnClickListener listener2) {
        super(context);
        this.message = message2;
        this.dialogChoices = dialogChoices2;
        this.listener = listener2;
    }

    /* access modifiers changed from: protected */
    public Dialog createDialog() {
        if (this.aDialog == null) {
            initBasicDialog();
        }
        return this.aDialog;
    }

    private void initBasicDialog() {
        AlertDialog aDialog2 = new AlertDialog.Builder(this.context).create();
        aDialog2.setMessage(this.message);
        int i = 0;
        while (i < this.dialogChoices.length && i < 3) {
            if (i == 0) {
                aDialog2.setButton(this.dialogChoices[0], this.listener);
            }
            if (i == 1) {
                aDialog2.setButton2(this.dialogChoices[1], this.listener);
            }
            if (i == 2) {
                aDialog2.setButton3(this.dialogChoices[2], this.listener);
            }
            i++;
        }
        this.aDialog = aDialog2;
    }
}
