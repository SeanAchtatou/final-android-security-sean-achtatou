package com.mol.seaplus.tool.connection;

import java.util.Enumeration;
import java.util.Vector;

public class StopLoadManager {
    private Vector<StopLoadListener> stopLoadList = new Vector<>();

    public void addStopLoadListener(StopLoadListener listener) {
        if (listener != null && !this.stopLoadList.contains(listener)) {
            this.stopLoadList.add(listener);
        }
    }

    public void removeStopLoadListener(StopLoadListener listener) {
        this.stopLoadList.remove(listener);
    }

    public void removeAllStopLoadListener() {
        this.stopLoadList.removeAllElements();
    }

    public void stopLoadAll() {
        stopLoadAll(false);
    }

    public void stopLoadAll(boolean removeAfterStop) {
        Enumeration<StopLoadListener> e = this.stopLoadList.elements();
        while (e.hasMoreElements()) {
            e.nextElement().stopLoadAll();
        }
        if (removeAfterStop) {
            removeAllStopLoadListener();
        }
    }
}
