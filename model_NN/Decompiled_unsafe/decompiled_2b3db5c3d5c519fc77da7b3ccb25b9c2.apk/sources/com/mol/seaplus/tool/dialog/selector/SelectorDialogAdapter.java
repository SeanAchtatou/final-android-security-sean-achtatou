package com.mol.seaplus.tool.dialog.selector;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

public abstract class SelectorDialogAdapter {
    private Context context;
    private SelectorDialog dialog;
    private OnSelectorItemClickListener onSelectorItemClickListener;

    public interface OnSelectorItemClickListener {
        void onSelectorItemClicked(SelectorDialogAdapter selectorDialogAdapter, View view, int i, long j);
    }

    public abstract View getBodyView(ViewGroup viewGroup);

    public abstract View getTitleView(ViewGroup viewGroup);

    public SelectorDialogAdapter(Context context2) {
        this.context = context2;
    }

    public Context getContext() {
        return this.context;
    }

    /* access modifiers changed from: protected */
    public void setSelectorDialog(SelectorDialog dialog2) {
        this.dialog = dialog2;
    }

    /* access modifiers changed from: protected */
    public void dismissDialog() {
        this.dialog.dismiss();
    }

    public void setOnSelectorItemClickListener(OnSelectorItemClickListener onSelectorItemClickListener2) {
        this.onSelectorItemClickListener = onSelectorItemClickListener2;
    }

    /* access modifiers changed from: protected */
    public void itemClicked(View view, int position, long id) {
        if (this.onSelectorItemClickListener != null) {
            this.onSelectorItemClickListener.onSelectorItemClicked(this, view, position, id);
        }
    }
}
