package com.mol.seaplus.tool.utils;

public class TimeUtils {
    public static final int DAY = 86400000;
    public static final int HOUR = 3600000;
    public static final int MIN = 60000;
    public static final int SEC = 1000;
}
