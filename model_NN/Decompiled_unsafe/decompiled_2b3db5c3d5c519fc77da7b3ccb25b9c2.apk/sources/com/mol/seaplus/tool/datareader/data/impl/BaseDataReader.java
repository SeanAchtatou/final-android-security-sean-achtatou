package com.mol.seaplus.tool.datareader.data.impl;

import com.mol.seaplus.tool.connection.ConnectionListener;
import com.mol.seaplus.tool.connection.IConnection2;
import com.mol.seaplus.tool.connection.handler.ErrorHandler;
import com.mol.seaplus.tool.datareader.data.IDataHolder;
import com.mol.seaplus.tool.datareader.data.IDataReader;
import com.mol.seaplus.tool.datareader.data.IDataReaderListener;
import com.mol.seaplus.tool.datareader.data.IDataReaderOption;
import com.mol.seaplus.tool.utils.os.UIHandler;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Vector;

public abstract class BaseDataReader<T extends IDataHolder> implements IDataReader<T> {
    private ConnectionListener connectionListener;
    private Vector<T> dataList;
    /* access modifiers changed from: private */
    public IDataReaderListener dataReaderListener;
    private String error;
    private int errorCode;
    private ErrorHandler errorHandler;
    private IDataReaderOption<T> iDataReaderOption;
    private int readCode;
    /* access modifiers changed from: private */
    public String response;
    private Hashtable<String, String> tableList;

    public BaseDataReader() {
        this(new DataReaderOption());
    }

    public BaseDataReader(IDataReaderOption<T> iDataReaderOption2) {
        this.readCode = 200;
        this.connectionListener = new ConnectionListener() {
            public void onConnected(IConnection2 iConnection2, InputStream inputStream) {
                String unused = BaseDataReader.this.response = BaseDataReader.this.readFromInputStream(inputStream);
                BaseDataReader.this.setResponse(BaseDataReader.this.response);
                BaseDataReader.this.updateListener();
            }

            public void onConnectFailed(IConnection2 iConnection2, int errorCode, String error) {
                BaseDataReader.this.setError(errorCode, error);
                BaseDataReader.this.updateListener();
            }
        };
        this.iDataReaderOption = iDataReaderOption2;
    }

    public void read() {
        IConnection2 iConnection2 = this.iDataReaderOption.getConnection();
        if (iConnection2 != null) {
            iConnection2.setConnectionListener(this.connectionListener);
            iConnection2.connect();
        }
    }

    public void stopRead() {
        IConnection2 iConnection2 = this.iDataReaderOption.getConnection();
        if (iConnection2 != null) {
            iConnection2.stopConnect();
        }
    }

    public int getReadCode() {
        return this.readCode;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getError() {
        return this.error;
    }

    public void setResponse(String response2) {
        this.response = response2;
    }

    public String getResponse() {
        return this.response;
    }

    /* access modifiers changed from: protected */
    public void addData(T data) {
        if (this.dataList == null) {
            this.dataList = new Vector<>();
        }
        this.dataList.add(data);
    }

    public void putTableData(String key, String value) {
        if (key != null && value != null) {
            if (this.tableList == null) {
                this.tableList = new Hashtable<>();
            }
            this.tableList.put(key, value);
        }
    }

    public String getTableData(String key) {
        if (key == null || this.tableList == null || !this.tableList.containsKey(key)) {
            return null;
        }
        return this.tableList.get(key);
    }

    public T getData(int index) {
        if (this.dataList == null || index <= this.dataList.size()) {
            return null;
        }
        return (IDataHolder) this.dataList.get(index);
    }

    public Vector<T> getAllData() {
        return this.dataList;
    }

    public void setErrorHandler(ErrorHandler errorHandler2) {
        this.errorHandler = errorHandler2;
    }

    public void setListener(IDataReaderListener<T> dataReaderListener2) {
        this.dataReaderListener = dataReaderListener2;
    }

    public IDataReaderOption<T> getDataReaderOption() {
        return this.iDataReaderOption;
    }

    /* access modifiers changed from: protected */
    public void setError(int errorCode2, String error2) {
        this.errorCode = errorCode2;
        this.readCode = -1;
        if (this.errorHandler != null) {
            error2 = this.errorHandler.handlerError(errorCode2, error2);
        }
        this.error = error2;
    }

    /* access modifiers changed from: protected */
    public void updateListener() {
        if (this.dataReaderListener != null) {
            new UIHandler().post(new Runnable() {
                public void run() {
                    BaseDataReader.this.dataReaderListener.onFinish(BaseDataReader.this);
                }
            });
        }
    }
}
