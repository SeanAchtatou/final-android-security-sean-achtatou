package com.mol.seaplus.tool.datareader.data;

public interface IDataHolder {
    Object get(String str);

    IDataHolder getChildHolderByTag(String str);

    String[] getKeys();

    String getTagName();

    IDataHolder initDataHolder();

    boolean isContain(String str);

    void put(String str, Object obj);
}
