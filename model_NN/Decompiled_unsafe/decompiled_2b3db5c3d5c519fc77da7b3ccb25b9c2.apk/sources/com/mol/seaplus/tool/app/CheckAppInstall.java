package com.mol.seaplus.tool.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.tool.dialog.ToolAlertDialog;

public class CheckAppInstall {
    /* access modifiers changed from: private */
    public Context dialogcontext;
    /* access modifiers changed from: private */
    public String uriString;

    public boolean isPackageInstalled(String packagename, Context context) {
        try {
            context.getPackageManager().getPackageInfo(packagename, 1);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public void launchUri(String packagename, Context context) {
        Log.w("Mol-SDK", "Package name : " + packagename);
        this.uriString = packagename;
        this.dialogcontext = context;
        String[] buttons = new String[2];
        buttons[0] = Resources.getString(3);
        Log.w("Mol-SDK", "button[0] : " + buttons[0]);
        buttons[1] = Resources.getString(4);
        Log.w("Mol-SDK", "button[1] : " + buttons[1]);
        String ask = Resources.getString(84);
        Log.w("Mol-SDK", "ask : " + ask);
        Log.w("Mol-SDK", "set batton done");
        ToolAlertDialog.show(context, ask, buttons, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int which) {
                if (which == -1) {
                    CheckAppInstall.this.dialogcontext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + CheckAppInstall.this.uriString)));
                }
            }
        });
    }
}
