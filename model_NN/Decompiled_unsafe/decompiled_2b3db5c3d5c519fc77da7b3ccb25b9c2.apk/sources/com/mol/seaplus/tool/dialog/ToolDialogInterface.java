package com.mol.seaplus.tool.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Looper;
import com.mol.seaplus.tool.utils.os.UIHandler;

public abstract class ToolDialogInterface implements UIHandler.UIHandlerListener {
    private static final int DISMISS_DIALOG = 1;
    private static final int SHOW_DIALOG = 0;
    protected Context context;
    private Dialog dialog;
    private boolean isCancelable;
    private boolean isCanceledOnTouchOutside;
    /* access modifiers changed from: private */
    public DialogInterface.OnCancelListener onCancelListener;
    private DialogInterface.OnCancelListener onCancelListener2 = new DialogInterface.OnCancelListener() {
        public void onCancel(DialogInterface dialog) {
            if (ToolDialogInterface.this.onCancelListener != null) {
                ToolDialogInterface.this.onCancelListener.onCancel(dialog);
            }
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnDismissListener onDismissListener;
    private DialogInterface.OnDismissListener onDismissListener2 = new DialogInterface.OnDismissListener() {
        public void onDismiss(DialogInterface dialog) {
            if (ToolDialogInterface.this.onDismissListener != null) {
                ToolDialogInterface.this.onDismissListener.onDismiss(dialog);
            }
        }
    };
    private UIHandler uiHandler;

    /* access modifiers changed from: protected */
    public abstract Dialog createDialog();

    public ToolDialogInterface(Context context2) {
        this.context = context2;
        this.uiHandler = new UIHandler(Looper.getMainLooper());
        this.uiHandler.setUIHandlerListener(this);
    }

    /* access modifiers changed from: protected */
    public boolean isCanceledOnTouchOutside() {
        return this.isCanceledOnTouchOutside;
    }

    public void setCanceledOnTouchOutside(boolean flag) {
        this.isCanceledOnTouchOutside = flag;
        if (this.dialog != null) {
            this.dialog.setCanceledOnTouchOutside(flag);
        }
    }

    public void setCancelable(boolean flag) {
        this.isCancelable = flag;
        if (this.dialog != null) {
            this.dialog.setCancelable(flag);
        }
    }

    public boolean isCancelable() {
        return this.isCancelable;
    }

    public DialogInterface.OnDismissListener getOnDismissListener() {
        return this.onDismissListener;
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener3) {
        this.onDismissListener = onDismissListener3;
    }

    public DialogInterface.OnCancelListener getOnCancelListener() {
        return this.onCancelListener;
    }

    public void setOnCancelListener(DialogInterface.OnCancelListener onCancelListener3) {
        this.onCancelListener = onCancelListener3;
    }

    public void show() {
        this.uiHandler.sendMessage(0);
    }

    public void dismiss() {
        this.uiHandler.sendMessage(1);
    }

    /* access modifiers changed from: protected */
    public void onShowDialog() {
    }

    /* access modifiers changed from: protected */
    public void onDismissDialog() {
    }

    public final void onUpdateUI(int source) {
        switch (source) {
            case 0:
                if (this.dialog == null) {
                    this.dialog = createDialog();
                    this.dialog.setOnDismissListener(this.onDismissListener2);
                    this.dialog.setOnCancelListener(this.onCancelListener2);
                }
                this.dialog.setCancelable(this.isCancelable);
                this.dialog.setCanceledOnTouchOutside(this.isCanceledOnTouchOutside);
                if (!this.dialog.isShowing()) {
                    this.dialog.show();
                    onShowDialog();
                    return;
                }
                return;
            case 1:
                if (this.dialog != null && this.dialog.isShowing()) {
                    this.dialog.dismiss();
                    onDismissDialog();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
