package com.mol.seaplus.tool.connection.internet.httpurlconnection;

import android.content.Context;
import com.mol.seaplus.tool.utils.HttpUtils;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Hashtable;
import org.apache.http.client.methods.HttpGet;

public class HttpURLGetConnection extends HttpURLConnection {
    public HttpURLGetConnection(Context context, String url, Hashtable<String, String> params) {
        super(context, url + HttpUtils.getParam(params, false, true));
    }

    public HttpURLGetConnection(Context context, String url) {
        super(context, url);
    }

    /* access modifiers changed from: protected */
    public String getRequestMethod() {
        return HttpGet.METHOD_NAME;
    }

    /* access modifiers changed from: protected */
    public void doHttpConnect(HttpURLConnection connection) throws IOException {
        connection.connect();
    }

    /* access modifiers changed from: protected */
    public HttpURLConnection doDeepCopy() {
        return new HttpURLGetConnection(getContext(), getUrl());
    }
}
