package com.mol.seaplus.tool.connection.internet.httpurlconnection;

import android.content.Context;
import com.mol.seaplus.Log;
import com.mol.seaplus.tool.connection.IConnectionDescriptor;
import com.mol.seaplus.tool.connection.internet.InternetConnection;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.HttpHeaders;
import org.apache.http.conn.ssl.SSLSocketFactory;

public abstract class HttpURLConnection extends InternetConnection {
    private HttpURLConnectionDescriptor conDesc;
    private Hashtable<String, String> requestProperties;

    /* access modifiers changed from: protected */
    public abstract HttpURLConnection doDeepCopy();

    /* access modifiers changed from: protected */
    public abstract void doHttpConnect(java.net.HttpURLConnection httpURLConnection) throws IOException;

    /* access modifiers changed from: protected */
    public abstract String getRequestMethod();

    public HttpURLConnection(Context context, String url) {
        super(context, url);
        setHttpURLConnectionDescriptor(new HttpURLConnectionDescriptor());
    }

    public void setHttpURLConnectionDescriptor(HttpURLConnectionDescriptor conDesc2) {
        this.conDesc = conDesc2;
        conDesc2.setMyHttpUrlConnection(this);
    }

    /* access modifiers changed from: protected */
    public IConnectionDescriptor doInternetConnect(String urlString) {
        HttpURLConnectionDescriptor reDirectCondesc;
        if (this.conDesc == null) {
            setHttpURLConnectionDescriptor(new HttpURLConnectionDescriptor());
        }
        SSLContext context = null;
        HostnameVerifier hostnameVerifier = null;
        try {
            if (urlString.startsWith(InternetConnection.HTTPS_PROTOCOL)) {
                KeyStore instance = KeyStore.getInstance(KeyStore.getDefaultType());
                TrustManager tm = new X509TrustManager() {
                    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                };
                context = SSLContext.getInstance("TLS");
                context.init(null, new TrustManager[]{tm}, null);
                hostnameVerifier = SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
            }
            Log.d("HttpUrlConnection connect to = " + urlString);
            java.net.HttpURLConnection conn = (java.net.HttpURLConnection) new URL(urlString).openConnection();
            conn.setRequestMethod(getRequestMethod());
            conn.setConnectTimeout(getTimeout());
            conn.setReadTimeout(getTimeout());
            if (this.requestProperties != null) {
                Enumeration<String> keys = this.requestProperties.keys();
                while (keys.hasMoreElements()) {
                    String key = keys.nextElement();
                    conn.addRequestProperty(key, this.requestProperties.get(key));
                }
            }
            if (context != null) {
                ((HttpsURLConnection) conn).setSSLSocketFactory(context.getSocketFactory());
                ((HttpsURLConnection) conn).setHostnameVerifier(hostnameVerifier);
            }
            this.conDesc.setHttpURLConnection(conn);
            doHttpConnect(conn);
            this.conDesc.onSetHttpURLConnection(conn);
            int resCode = this.conDesc.getResponseCode();
            if ((resCode == 302 || resCode == 301) && (reDirectCondesc = handlerRedirect(conn)) != null) {
                this.conDesc = reDirectCondesc;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.e("HttpURLConnection error MalformedURLException = " + e.toString());
            this.conDesc.setError(16776964, e.toString());
        } catch (SocketTimeoutException e2) {
            e2.printStackTrace();
            Log.e("HttpURLConnection error SocketTimeoutException = " + e2.toString());
            this.conDesc.setError(16776963, e2.toString());
        } catch (Exception e3) {
            e3.printStackTrace();
            Log.e("HttpURLConnection error Exception = " + e3.toString());
            this.conDesc.setError(16776962, e3.toString());
        }
        return this.conDesc;
    }

    /* access modifiers changed from: protected */
    public HttpURLConnectionDescriptor handlerRedirect(java.net.HttpURLConnection conn) {
        String redirectUrl = conn.getHeaderField(HttpHeaders.LOCATION);
        Log.d("HttpURLConnection redirectUrl = " + redirectUrl);
        if (redirectUrl != null) {
            return (HttpURLConnectionDescriptor) new HttpURLGetConnection(getContext(), redirectUrl).doInternetConnect(redirectUrl);
        }
        return null;
    }

    public InternetConnection addHeader(String key, String value) {
        putRequestProperty(key, value);
        return this;
    }

    public void putRequestProperty(String field, String value) {
        if (this.requestProperties == null) {
            this.requestProperties = new Hashtable<>();
        }
        this.requestProperties.put(field, value);
    }

    public Hashtable<String, String> getRequestProperties() {
        return this.requestProperties;
    }

    /* access modifiers changed from: protected */
    public final HttpURLConnection onDeepCopy() {
        HttpURLConnection httpURLConnection = doDeepCopy();
        if (httpURLConnection != null) {
            HttpURLConnectionDescriptor httpURLConnectionDescriptor = (HttpURLConnectionDescriptor) getConnectionDescription();
            if (httpURLConnectionDescriptor != null) {
                httpURLConnection.setHttpURLConnectionDescriptor(httpURLConnectionDescriptor.deepCopy());
            }
            if (this.requestProperties != null) {
                Enumeration<String> keys = this.requestProperties.keys();
                while (keys.hasMoreElements()) {
                    String key = keys.nextElement();
                    httpURLConnection.putRequestProperty(key, this.requestProperties.get(key));
                }
            }
        }
        return httpURLConnection;
    }
}
