package com.mol.seaplus.tool.dialog.selector;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.mol.seaplus.tool.dialog.ToolDialogInterface;

public class SelectorDialog extends ToolDialogInterface {
    private SelectorDialogAdapter adapter;
    private Dialog alertDialog;

    public SelectorDialog(Context context) {
        super(context);
        this.alertDialog = new Dialog(context, 16973835);
        this.alertDialog.requestWindowFeature(1);
        this.alertDialog.setContentView(initView(context));
    }

    private View initView(Context pContext) {
        LinearLayout mainLayout = new LinearLayout(pContext);
        LinearLayout titleLayout = new LinearLayout(pContext);
        LinearLayout bodyLayout = new LinearLayout(pContext);
        mainLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        mainLayout.setOrientation(1);
        titleLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        titleLayout.setOrientation(0);
        titleLayout.setId(1);
        bodyLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        bodyLayout.setOrientation(0);
        bodyLayout.setId(2);
        mainLayout.addView(titleLayout);
        mainLayout.addView(bodyLayout);
        return mainLayout;
    }

    public SelectorDialogAdapter getSelectorDialogAdapter() {
        return this.adapter;
    }

    public void setSelectorDialogAdapter(SelectorDialogAdapter adapter2) {
        this.adapter = adapter2;
        adapter2.setSelectorDialog(this);
        LinearLayout titleLayout = (LinearLayout) this.alertDialog.findViewById(1);
        LinearLayout bodyLayout = (LinearLayout) this.alertDialog.findViewById(2);
        titleLayout.removeAllViews();
        bodyLayout.removeAllViews();
        View getTitleView = adapter2.getTitleView(titleLayout);
        View getBodyView = adapter2.getBodyView(bodyLayout);
        if (getTitleView.getParent() != null) {
            ((ViewGroup) getTitleView.getParent()).removeView(getTitleView);
        }
        if (getBodyView.getParent() != null) {
            ((ViewGroup) getBodyView.getParent()).removeView(getBodyView);
        }
        titleLayout.addView(getTitleView);
        bodyLayout.addView(getBodyView);
    }

    /* access modifiers changed from: protected */
    public Dialog createDialog() {
        return this.alertDialog;
    }
}
