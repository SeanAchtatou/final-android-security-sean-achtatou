package com.mol.seaplus.tool.connection.internet;

import android.content.Context;
import com.mol.seaplus.tool.connection.Connection2;
import com.mol.seaplus.tool.connection.IConnectionDescriptor;
import com.mol.seaplus.tool.utils.HttpUtils;
import com.mol.seaplus.tool.utils.StringUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public abstract class InternetConnection extends Connection2 {
    public static final String HTTPS_PROTOCOL = "https://";
    public static final String HTTP_PROTOCOL = "http://";
    public static final int INTERNET_ERROR = 16776962;
    public static final int INTERNET_RESPONSE_ERROR = 16776961;
    public static final int INTERNET_TIMEOUT_ERROR = 16776963;
    public static final int MULFORMED_URL_ERROR = 16776964;
    public static final int NO_INTERNET_CONNECTION = 16776960;
    public static final String NO_INTERNET_CONNECTION_STRING = "No network connection";
    private Context context;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
    private int timeout = 30000;

    public abstract InternetConnection addHeader(String str, String str2);

    /* access modifiers changed from: protected */
    public abstract IConnectionDescriptor doInternetConnect(String str);

    public InternetConnection(Context context2, String url) {
        super(url);
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public Context getContext() {
        return this.context;
    }

    public void setTimeout(int timeout2) {
        this.timeout = timeout2;
    }

    public int getTimeout() {
        return this.timeout;
    }

    public Map<String, List<String>> getAllRequestHeader() {
        InternetConnectionDescriptor internetConnectionDescriptor = getConnectionDescription();
        if (internetConnectionDescriptor != null) {
            return internetConnectionDescriptor.getAllRequestHeader();
        }
        return null;
    }

    public List<String> getRequestHeaders(String field) {
        InternetConnectionDescriptor internetConnectionDescriptor = getConnectionDescription();
        if (internetConnectionDescriptor != null) {
            return internetConnectionDescriptor.getRequestHeaders(field);
        }
        return null;
    }

    public Map<String, List<String>> getAllResponseHeader() {
        InternetConnectionDescriptor internetConnectionDescriptor = getConnectionDescription();
        if (internetConnectionDescriptor != null) {
            return internetConnectionDescriptor.getAllResponseHeader();
        }
        return null;
    }

    public List<String> getResponseHeaders(String field) {
        InternetConnectionDescriptor internetConnectionDescriptor = getConnectionDescription();
        if (internetConnectionDescriptor != null) {
            return internetConnectionDescriptor.getResponseHeaders(field);
        }
        return null;
    }

    public String getRequestHeader(String field) {
        List<String> results = getRequestHeaders(field);
        if (results == null || results.size() <= 0) {
            return null;
        }
        return results.get(0);
    }

    public String getResponseHeader(String field) {
        List<String> results = getResponseHeaders(field);
        if (results == null || results.size() <= 0) {
            return null;
        }
        return results.get(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mol.seaplus.tool.utils.StringUtils.getStringAsLong(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.mol.seaplus.tool.utils.StringUtils.getStringAsLong(java.lang.String, double):double
      com.mol.seaplus.tool.utils.StringUtils.getStringAsLong(java.lang.String, float):float
      com.mol.seaplus.tool.utils.StringUtils.getStringAsLong(java.lang.String, long):long */
    public long getContentLength() {
        return StringUtils.getStringAsLong(getResponseHeader("Content-Length"), -1L);
    }

    public long getIfModifiedSince() {
        String ifModifiedSince = getResponseHeader("If-Modified-Since");
        if (ifModifiedSince == null || ifModifiedSince.length() <= 0) {
            return 0;
        }
        try {
            Date date = this.simpleDateFormat.parse(ifModifiedSince);
            if (date != null) {
                return date.getTime();
            }
            return 0;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public long getLastModified() {
        String lastModified = getResponseHeader("Last-Modified");
        if (lastModified == null || lastModified.length() <= 0) {
            return 0;
        }
        try {
            Date date = this.simpleDateFormat.parse(lastModified);
            if (date != null) {
                return date.getTime();
            }
            return 0;
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public InternetConnectionDescriptor getConnectionDescription() {
        IConnectionDescriptor iConnectionDescriptor = super.getConnectionDescription();
        if (iConnectionDescriptor != null) {
            return (InternetConnectionDescriptor) iConnectionDescriptor;
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public IConnectionDescriptor doConnect(String url) {
        if (!HttpUtils.isHasNetworkConnection(this.context)) {
            return new MyInternetConnectionDescriptor(16776960, NO_INTERNET_CONNECTION_STRING);
        }
        return doInternetConnect(onEncodeUrl(url));
    }

    /* access modifiers changed from: protected */
    public String onEncodeUrl(String url) {
        return url;
    }

    public class MyInternetConnectionDescriptor extends InternetConnectionDescriptor {
        public MyInternetConnectionDescriptor() {
        }

        public MyInternetConnectionDescriptor(int errorCode, String error) {
            super(errorCode, error);
        }

        public Map<String, List<String>> getAllRequestHeader() {
            return null;
        }

        public List<String> getRequestHeaders(String field) {
            return null;
        }

        public Map<String, List<String>> getAllResponseHeader() {
            return null;
        }

        public List<String> getResponseHeaders(String field) {
            return null;
        }

        public IConnectionDescriptor deepCopy() {
            return new MyInternetConnectionDescriptor(getErrorCode(), getError());
        }
    }
}
