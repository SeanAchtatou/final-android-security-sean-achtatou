package com.mol.seaplus.tool.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileUtil {
    public static void galleryAddPic(Context context, String mCurrentPhotoPath) {
        galleryAddPic(context, new File(mCurrentPhotoPath));
    }

    public static void galleryAddPic(Context context, File pictureFile) {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        mediaScanIntent.setData(Uri.fromFile(pictureFile));
        context.sendBroadcast(mediaScanIntent);
    }

    public static final String getAppFolder(Context context) {
        return context.getFilesDir().getAbsolutePath();
    }

    public static final String getExternalAppFolder(Context context) {
        return getExternalAbsolutePath() + "/Android/data/" + context.getPackageName();
    }

    public static final String getExternalAbsolutePath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath();
    }

    public static boolean isExternalPathExist() {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath();
        if (path != null && new File(path).canRead()) {
            return true;
        }
        return false;
    }

    public static boolean deleteFile(String filePath) {
        return deleteFile(new File(filePath));
    }

    public static boolean deleteFile(File file) {
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mol.seaplus.tool.utils.FileUtil.saveFile(java.lang.String, byte[], boolean):void
     arg types: [java.lang.String, byte[], int]
     candidates:
      com.mol.seaplus.tool.utils.FileUtil.saveFile(java.lang.String, java.io.InputStream, boolean):void
      com.mol.seaplus.tool.utils.FileUtil.saveFile(java.lang.String, byte[], int):void
      com.mol.seaplus.tool.utils.FileUtil.saveFile(java.lang.String, byte[], boolean):void */
    public static void saveFile(String filePath, byte[] data) throws IOException {
        saveFile(filePath, data, true);
    }

    public static void saveFile(String filePath, byte[] data, int bufferSize) throws IOException {
        saveFile(filePath, data, true, bufferSize);
    }

    public static void saveFile(String filePath, byte[] data, boolean deleteIfExist) throws IOException {
        saveFile(filePath, data, deleteIfExist, -1);
    }

    public static void saveFile(String filePath, byte[] data, boolean deleteIfExist, int bufferSize) throws IOException {
        File file = new File(filePath);
        if (file.exists() && deleteIfExist) {
            file.delete();
        }
        DataInputStream dInputStream = new DataInputStream(new ByteArrayInputStream(data));
        FileOutputStream fOut = new FileOutputStream(file);
        DataOutputStream dOut = new DataOutputStream(fOut);
        if (bufferSize > 0) {
            byte[] buffer = new byte[bufferSize];
            while (true) {
                int ch = dInputStream.read(buffer);
                if (ch == -1) {
                    break;
                }
                dOut.write(buffer, 0, ch);
            }
        } else {
            dOut.write(data);
            dOut.flush();
        }
        dOut.close();
        fOut.close();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mol.seaplus.tool.utils.FileUtil.saveFile(java.lang.String, java.io.InputStream, boolean):void
     arg types: [java.lang.String, java.io.InputStream, int]
     candidates:
      com.mol.seaplus.tool.utils.FileUtil.saveFile(java.lang.String, byte[], int):void
      com.mol.seaplus.tool.utils.FileUtil.saveFile(java.lang.String, byte[], boolean):void
      com.mol.seaplus.tool.utils.FileUtil.saveFile(java.lang.String, java.io.InputStream, boolean):void */
    public static void saveFile(String filePath, InputStream in) throws IOException {
        saveFile(filePath, in, true);
    }

    public static void saveFile(String filePath, InputStream in, boolean deleteIfExist) throws IOException {
        File file = new File(filePath);
        if (!file.exists()) {
            file.createNewFile();
        } else if (deleteIfExist) {
            file.delete();
            file.createNewFile();
        }
        FileOutputStream fOut = new FileOutputStream(file);
        DataOutputStream dOut = new DataOutputStream(fOut);
        byte[] buf = new byte[1024];
        while (true) {
            int chuck = in.read(buf);
            if (chuck != -1) {
                dOut.write(buf, 0, chuck);
            } else {
                dOut.close();
                fOut.close();
                return;
            }
        }
    }

    public static byte[] readFile(String filePath) throws FileNotFoundException {
        return IOUtils.readByteArrayFromInputStream(openFile(filePath));
    }

    public static FileInputStream openFile(String filePath) throws FileNotFoundException {
        return new FileInputStream(new File(filePath));
    }

    public static final void clearFolder(String folderPath) {
        File[] fileList;
        File folder = new File(folderPath);
        if (folder.exists() && folder.isDirectory() && (fileList = folder.listFiles()) != null && fileList.length > 0) {
            for (File file : fileList) {
                if (file.canWrite()) {
                    file.delete();
                }
            }
        }
    }
}
