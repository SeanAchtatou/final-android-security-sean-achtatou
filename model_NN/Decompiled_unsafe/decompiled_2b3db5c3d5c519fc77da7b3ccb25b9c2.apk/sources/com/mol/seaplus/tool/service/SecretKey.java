package com.mol.seaplus.tool.service;

public class SecretKey {
    /* JADX WARN: Type inference failed for: r11v5, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getSecretKey(java.lang.String r14) {
        /*
            r13 = this;
            java.lang.String r11 = "GetSecretkey Start"
            com.mol.seaplus.Log.d(r11)
            r2 = 0
            r9 = 0
            java.lang.String r3 = ""
            java.lang.String r10 = "http://202.142.215.96/android-lab/champ/usdk/getsig.php"
            java.net.URL r11 = new java.net.URL     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            r11.<init>(r10)     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            java.net.URLConnection r11 = r11.openConnection()     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            r0 = r11
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            r2 = r0
            java.lang.String r11 = "GET"
            r2.setRequestMethod(r11)     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            r11 = 0
            r2.setAllowUserInteraction(r11)     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            r11 = 1
            r2.setInstanceFollowRedirects(r11)     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            r11 = 0
            r2.setUseCaches(r11)     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            int r7 = r2.getResponseCode()     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            r11 = 200(0xc8, float:2.8E-43)
            if (r7 != r11) goto L_0x0090
            r8 = 0
            java.io.InputStream r4 = r2.getInputStream()     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            r1.<init>()     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            r6 = 0
        L_0x003c:
            int r6 = r4.read()     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            r11 = -1
            if (r6 == r11) goto L_0x004e
            r1.write(r6)     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            goto L_0x003c
        L_0x0047:
            r11 = move-exception
            if (r2 == 0) goto L_0x004d
            r2.disconnect()     // Catch:{ Exception -> 0x00a2 }
        L_0x004d:
            return r3
        L_0x004e:
            byte[] r8 = r1.toByteArray()     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            java.lang.String r9 = r1.toString()     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            r1.close()     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            r5.<init>(r9)     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            java.lang.String r11 = "secret"
            java.lang.String r3 = r5.getString(r11)     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            r11.<init>()     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            java.lang.String r12 = "jsonMessage = "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            java.lang.StringBuilder r11 = r11.append(r9)     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            com.mol.seaplus.Log.d(r11)     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            r11.<init>()     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            java.lang.String r12 = "secret_key = "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            java.lang.StringBuilder r11 = r11.append(r3)     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            com.mol.seaplus.Log.d(r11)     // Catch:{ Exception -> 0x0047, all -> 0x009b }
        L_0x0090:
            r2.disconnect()     // Catch:{ Exception -> 0x0047, all -> 0x009b }
            if (r2 == 0) goto L_0x004d
            r2.disconnect()     // Catch:{ Exception -> 0x0099 }
            goto L_0x004d
        L_0x0099:
            r11 = move-exception
            goto L_0x004d
        L_0x009b:
            r11 = move-exception
            if (r2 == 0) goto L_0x00a1
            r2.disconnect()     // Catch:{ Exception -> 0x00a4 }
        L_0x00a1:
            throw r11
        L_0x00a2:
            r11 = move-exception
            goto L_0x004d
        L_0x00a4:
            r12 = move-exception
            goto L_0x00a1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mol.seaplus.tool.service.SecretKey.getSecretKey(java.lang.String):java.lang.String");
    }
}
