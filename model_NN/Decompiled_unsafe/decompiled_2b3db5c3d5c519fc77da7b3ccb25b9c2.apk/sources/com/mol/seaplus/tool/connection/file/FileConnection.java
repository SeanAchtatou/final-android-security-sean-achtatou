package com.mol.seaplus.tool.connection.file;

import com.mol.seaplus.tool.connection.Connection2;
import com.mol.seaplus.tool.connection.IConnection2;
import com.mol.seaplus.tool.connection.IConnectionDescriptor;
import com.mol.seaplus.tool.connection.SetableConnectionDescriptor;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class FileConnection extends Connection2 {
    public static final int FILE_NOT_FOUND_ERROR = 15728624;

    public FileConnection(String url) {
        super(url);
    }

    /* access modifiers changed from: protected */
    public IConnectionDescriptor doConnect(String url) {
        return new FileConnectionDescriptor(url);
    }

    /* access modifiers changed from: protected */
    public IConnection2 onDeepCopy() {
        return new FileConnection(getUrl());
    }

    public class FileConnectionDescriptor extends SetableConnectionDescriptor {
        private File file;

        public FileConnectionDescriptor(String path) {
            this.file = new File(path);
            try {
                setLength(this.file.length());
                setInputStream(new FileInputStream(this.file));
            } catch (FileNotFoundException e) {
                setError(FileConnection.FILE_NOT_FOUND_ERROR, e.toString());
            }
        }

        public File getFile() {
            return this.file;
        }

        public IConnectionDescriptor deepCopy() {
            return new FileConnectionDescriptor(FileConnection.this.getUrl());
        }
    }
}
