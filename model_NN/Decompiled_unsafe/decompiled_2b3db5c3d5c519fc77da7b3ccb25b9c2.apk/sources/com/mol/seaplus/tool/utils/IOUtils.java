package com.mol.seaplus.tool.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import org.apache.http.protocol.HTTP;

public class IOUtils {
    public static String readStringFromInputStream(InputStream in) {
        byte[] data;
        String result = null;
        if (in == null || (data = readByteArrayFromInputStream(in)) == null) {
            return null;
        }
        try {
            result = new String(data, HTTP.UTF_8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static byte[] readByteArrayFromInputStream(InputStream in) {
        byte[] result;
        if (in == null) {
            return null;
        }
        try {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            byte[] data = new byte[1024];
            while (true) {
                int chunk = in.read(data);
                if (-1 == chunk) {
                    break;
                }
                buffer.write(data, 0, chunk);
            }
            result = buffer.toByteArray();
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e2) {
            System.out.println("read byte array from inputstream error :" + e2.toString());
            result = null;
            try {
                in.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        } catch (Throwable th) {
            try {
                in.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
            throw th;
        }
        return result;
    }
}
