package com.mol.seaplus.tool.connection;

import com.mol.seaplus.tool.connection.handler.ErrorHandler;
import com.mol.seaplus.tool.connection.handler.SimpleErrorHandler;
import java.io.InputStream;

public abstract class Connection2 implements IConnection2 {
    private IConnectionDescriptor conDesc;
    private ConnectThread connectThread;
    private ErrorHandler errorHandler;
    private ConnectionListener listener;
    private String url;

    /* access modifiers changed from: protected */
    public abstract IConnectionDescriptor doConnect(String str);

    /* access modifiers changed from: protected */
    public abstract IConnection2 onDeepCopy();

    public Connection2(String url2) {
        this.url = url2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setConnectionListener(ConnectionListener listener2) {
        this.listener = listener2;
    }

    public ConnectionListener getConnectionListener() {
        return this.listener;
    }

    public void setErrorHandler(ErrorHandler errorHandler2) {
        this.errorHandler = errorHandler2;
    }

    public ErrorHandler getErrorHandler() {
        return this.errorHandler;
    }

    public IConnectionDescriptor getConnectionDescription() {
        return this.conDesc;
    }

    public void connect() {
        if (this.connectThread == null) {
            this.connectThread = new ConnectThread();
            this.connectThread.start();
        }
    }

    public void stopConnect() {
        this.listener = null;
    }

    public void immediateConnect() {
        doConnect();
    }

    /* access modifiers changed from: private */
    public void doConnect() {
        this.conDesc = doConnect(this.url);
        if (this.conDesc != null) {
            if (this.conDesc.isSuccess()) {
                updateSuccess(this.conDesc.getInputStream());
            } else {
                updateError(this.conDesc.getErrorCode(), this.conDesc.getError());
            }
        }
    }

    private void updateSuccess(InputStream in) {
        if (this.listener != null) {
            this.listener.onConnected(this, in);
        }
    }

    private void updateError(int errorCode, String error) {
        if (this.listener != null) {
            if (this.errorHandler == null) {
                setErrorHandler(new SimpleErrorHandler());
            }
            this.listener.onConnectFailed(this, errorCode, this.errorHandler.handlerError(errorCode, error));
        }
    }

    public final IConnection2 deepCopy() {
        IConnection2 iConnection2 = onDeepCopy();
        if (iConnection2 != null) {
            iConnection2.setErrorHandler(getErrorHandler());
        }
        return iConnection2;
    }

    class ConnectThread extends Thread {
        ConnectThread() {
        }

        public void run() {
            Connection2.this.doConnect();
        }
    }
}
