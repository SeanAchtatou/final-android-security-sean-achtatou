package com.mol.seaplus.tool.connection.internet.httpurlconnection;

import com.mol.seaplus.Log;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;

public class HttpURLRestfulConnectionDescriptor extends HttpURLConnectionDescriptor {
    /* access modifiers changed from: protected */
    public void onSetHttpURLConnection(HttpURLConnection httpURLConnection) {
        try {
            int resCode = httpURLConnection.getResponseCode();
            Log.d("HttpUrlConnection responseFrom = " + getMyHttpUrlConnection().getUrl());
            setResponseCode(resCode);
            setLength((long) httpURLConnection.getContentLength());
            setInputStream(httpURLConnection.getInputStream());
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.e("HttpURLConnection error MalformedURLException = " + e.toString());
            setError(16776964, e.toString());
        } catch (SocketTimeoutException e2) {
            e2.printStackTrace();
            Log.e("HttpURLConnection error SocketTimeoutException = " + e2.toString());
            setError(16776963, e2.toString());
        } catch (Exception e3) {
            e3.printStackTrace();
            Log.e("HttpURLConnection error Exception = " + e3.toString());
            setError(16776962, e3.toString());
        }
    }
}
