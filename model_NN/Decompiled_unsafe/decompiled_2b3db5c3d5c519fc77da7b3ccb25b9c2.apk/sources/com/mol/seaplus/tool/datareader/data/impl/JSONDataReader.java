package com.mol.seaplus.tool.datareader.data.impl;

import com.mol.seaplus.tool.datareader.data.IDataHolder;
import com.mol.seaplus.tool.datareader.data.IDataReaderOption;
import com.mol.seaplus.tool.utils.IOUtils;
import java.io.InputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONDataReader<T extends IDataHolder> extends BaseDataReader<T> {
    public JSONDataReader() {
    }

    public JSONDataReader(IDataReaderOption<T> iDataReaderOption) {
        super(iDataReaderOption);
    }

    public String readFromInputStream(InputStream inputStream) {
        String response = IOUtils.readStringFromInputStream(inputStream);
        try {
            try {
                extractJSONObject(new JSONObject(response));
            } catch (JSONException e) {
                e = e;
                setError(16777184, "Read error : " + e.toString());
                e.printStackTrace();
                return response;
            }
        } catch (JSONException e2) {
            e = e2;
            setError(16777184, "Read error : " + e.toString());
            e.printStackTrace();
            return response;
        }
        return response;
    }

    public void extractJSONObject(JSONObject jObj) {
        extractJSONObject(jObj, null);
    }

    private void extractJSONObject(JSONObject jObj, IDataHolder holder) {
        IDataReaderOption dOption = getDataReaderOption();
        Iterator keys = jObj.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            try {
                if (dOption.getDataHolder() == null || holder != null || !key.equals(dOption.getDataHolder().getTagName())) {
                    extractJObject(key, jObj.get(key), holder);
                } else {
                    Object obj = jObj.get(key);
                    if (obj instanceof JSONArray) {
                        extractJSONDataNode(jObj.getJSONArray(key));
                    } else if (obj instanceof JSONObject) {
                        Vector<JSONObject> v = new Vector<>();
                        v.add((JSONObject) obj);
                        extractJSONDataNode(new JSONArray((Collection) v));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void extractJSONDataNode(JSONArray jArray) {
        IDataReaderOption dOption = getDataReaderOption();
        for (int i = 0; i < jArray.length(); i++) {
            T holder = dOption.getDataHolder().initDataHolder();
            try {
                extractJSONObject(jArray.getJSONObject(i), holder);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            addData(holder);
        }
    }

    private void extractJObject(String key, Object obj, IDataHolder holder) {
        if (holder != null && holder.isContain(key)) {
            putDataNode(key, obj, holder);
        } else if (obj instanceof JSONObject) {
            extractJSONObject((JSONObject) obj, holder);
        } else if (obj instanceof JSONArray) {
            extractJSONArray((JSONArray) obj, holder);
        } else {
            putDataNode(key, obj, holder);
        }
    }

    private void putDataNode(String key, Object o, IDataHolder holder) {
        if (holder == null || holder.isContain(key)) {
            String put = null;
            if (o instanceof Byte) {
                put = "" + ((int) ((Byte) o).byteValue());
            } else if (o instanceof Short) {
                put = "" + ((int) ((Short) o).shortValue());
            } else if (o instanceof Integer) {
                put = "" + ((Integer) o).intValue();
            } else if (o instanceof Long) {
                put = "" + ((Long) o).longValue();
            } else if (o instanceof Float) {
                put = "" + ((Float) o).floatValue();
            } else if (o instanceof Double) {
                put = "" + ((Double) o).doubleValue();
            } else if (o instanceof String) {
                put = (String) o;
            } else if (o instanceof Boolean) {
                put = "" + ((Boolean) o).booleanValue();
            }
            if (holder == null) {
                if (put != null) {
                    putTableData(key, put);
                }
            } else if (put != null) {
                holder.put(key, put);
            } else {
                IDataHolder childHolder = holder.getChildHolderByTag(key);
                if (childHolder == null) {
                    holder.put(key, o);
                    return;
                }
                JSONArray jsonArray = null;
                if (o instanceof JSONObject) {
                    jsonArray = new JSONArray();
                    jsonArray.put(o);
                } else if (o instanceof JSONArray) {
                    jsonArray = (JSONArray) o;
                }
                if (jsonArray != null) {
                    Vector result = (Vector) holder.get(key);
                    if (result == null) {
                        result = new Vector();
                        holder.put(key, result);
                    }
                    for (int i = 0; i < jsonArray.length(); i++) {
                        IDataHolder iDataHolder = childHolder.initDataHolder();
                        try {
                            extractJSONObject(jsonArray.getJSONObject(i), iDataHolder);
                            result.add(iDataHolder);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    private void extractJSONArray(JSONArray jArray, IDataHolder holder) {
        for (int i = 0; i < jArray.length(); i++) {
            try {
                extractJSONObject(jArray.getJSONObject(i), holder);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
