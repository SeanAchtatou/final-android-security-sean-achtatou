package com.mol.seaplus.tool.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import com.bluepay.data.Config;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class DeviceUtils {
    public static final String getMacAddress(Context context) {
        return ((WifiManager) context.getSystemService(Config.NETWORKTYPE_WIFI)).getConnectionInfo().getMacAddress();
    }

    public static final String getIMEI(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId().replaceAll(" ", "");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static final String getSignatureKey(Context context) {
        try {
            Signature[] signatureArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures;
            if (0 < signatureArr.length) {
                Signature signature = signatureArr[0];
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.e("AndroidUtils", "hash key = " + something);
                return something;
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("AndroidUtils", "name not found = " + e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("AndroidUtils", "no such an algorithm = " + e.toString());
        } catch (Exception e2) {
            Log.e("AndroidUtils", "exception = " + e2.toString());
        }
        return null;
    }

    public static final String getReleaseOSVersion() {
        return Build.VERSION.RELEASE;
    }
}
