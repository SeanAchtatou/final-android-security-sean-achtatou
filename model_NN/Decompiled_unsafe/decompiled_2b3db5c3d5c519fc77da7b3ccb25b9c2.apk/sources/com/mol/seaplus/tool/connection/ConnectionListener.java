package com.mol.seaplus.tool.connection;

import java.io.InputStream;

public interface ConnectionListener {
    void onConnectFailed(IConnection2 iConnection2, int i, String str);

    void onConnected(IConnection2 iConnection2, InputStream inputStream);
}
