package com.mol.seaplus.tool.dialog.selector;

import android.content.Context;

public abstract class ArraySelectorAdapter<T> extends SelectorDialogAdapter {
    private T[] datas;

    public ArraySelectorAdapter(Context context, T[] datas2) {
        super(context);
        this.datas = datas2;
    }

    public T getData(int position) {
        return this.datas[position];
    }

    public T[] getDatas() {
        return this.datas;
    }

    public int getDataCount() {
        return this.datas.length;
    }
}
