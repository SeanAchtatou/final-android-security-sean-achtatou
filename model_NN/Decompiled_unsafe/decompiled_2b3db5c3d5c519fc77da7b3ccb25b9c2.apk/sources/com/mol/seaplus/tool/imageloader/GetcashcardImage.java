package com.mol.seaplus.tool.imageloader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import com.mol.seaplus.Log;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.http.client.methods.HttpGet;

/* compiled from: CashCardlogo */
class GetcashcardImage extends AsyncTask<String, Integer, Bitmap> {
    GetcashcardImage() {
    }

    /* access modifiers changed from: protected */
    public Bitmap doInBackground(String... urls) {
        HttpURLConnection conn = null;
        Bitmap bitmap = null;
        try {
            HttpURLConnection conn2 = (HttpURLConnection) new URL(urls[0]).openConnection();
            conn2.setRequestMethod(HttpGet.METHOD_NAME);
            conn2.setAllowUserInteraction(false);
            if (conn2.getResponseCode() == 200) {
                bitmap = BitmapFactory.decodeStream(conn2.getInputStream());
            }
            conn2.disconnect();
            if (conn2 != null) {
                try {
                    conn2.disconnect();
                } catch (Exception e) {
                }
            }
        } catch (Exception e2) {
            Log.d("Exception AsyncTask:" + e2);
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e3) {
                }
            }
        } catch (Throwable th) {
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e4) {
                }
            }
            throw th;
        }
        return bitmap;
    }
}
