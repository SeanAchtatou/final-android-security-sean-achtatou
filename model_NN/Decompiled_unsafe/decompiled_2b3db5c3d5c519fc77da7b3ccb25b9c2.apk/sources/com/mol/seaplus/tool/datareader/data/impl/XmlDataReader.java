package com.mol.seaplus.tool.datareader.data.impl;

import com.mol.seaplus.tool.datareader.data.IDataReaderOption;
import com.mol.seaplus.tool.datareader.data.IXmlDataHolder;
import com.mol.seaplus.tool.utils.IOUtils;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlDataReader<T extends IXmlDataHolder> extends BaseDataReader<T> {
    public XmlDataReader() {
    }

    public XmlDataReader(IDataReaderOption<T> iDataReaderOption) {
        super(iDataReaderOption);
    }

    public String readFromInputStream(InputStream inputStream) {
        String response = IOUtils.readStringFromInputStream(inputStream);
        try {
            DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            docBuilder.isValidating();
            Document xmlDoc = docBuilder.parse(new ByteArrayInputStream(response.getBytes()));
            xmlDoc.getDocumentElement().normalize();
            extractXmlNode(xmlDoc.getChildNodes());
        } catch (Exception e) {
            e.printStackTrace();
            setError(16777184, "Read error : " + e.toString());
        }
        return response;
    }

    public void extractXml(Document xmlDoc) {
        if (xmlDoc != null) {
            extractXmlNode(xmlDoc.getChildNodes());
        }
    }

    public void extractXmlNode(NodeList list) {
        extractXmlNodeList(list, null);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void extractXmlNodeList(org.w3c.dom.NodeList r8, com.mol.seaplus.tool.datareader.data.IXmlDataHolder r9) {
        /*
            r7 = this;
            if (r8 == 0) goto L_0x004e
            int r6 = r8.getLength()
            if (r6 <= 0) goto L_0x004e
            r2 = r9
            if (r2 != 0) goto L_0x0015
            com.mol.seaplus.tool.datareader.data.IDataReaderOption r6 = r7.getDataReaderOption()
            com.mol.seaplus.tool.datareader.data.IDataHolder r2 = r6.getDataHolder()
            com.mol.seaplus.tool.datareader.data.IXmlDataHolder r2 = (com.mol.seaplus.tool.datareader.data.IXmlDataHolder) r2
        L_0x0015:
            r1 = 0
        L_0x0016:
            int r6 = r8.getLength()
            if (r1 >= r6) goto L_0x004e
            org.w3c.dom.Node r3 = r8.item(r1)
            java.lang.String r5 = r3.getNodeName()
            org.w3c.dom.NodeList r0 = r3.getChildNodes()
            if (r2 == 0) goto L_0x004a
            java.lang.String r6 = r2.getTagName()
            boolean r6 = r5.equals(r6)
            if (r6 == 0) goto L_0x0046
            com.mol.seaplus.tool.datareader.data.IDataHolder r4 = r2.initDataHolder()
            com.mol.seaplus.tool.datareader.data.IXmlDataHolder r4 = (com.mol.seaplus.tool.datareader.data.IXmlDataHolder) r4
            r4.setXmlNode(r3)
            r7.extractDataNode(r3, r4)
            r7.addData(r4)
        L_0x0043:
            int r1 = r1 + 1
            goto L_0x0016
        L_0x0046:
            r7.extractDataNode(r3, r9)
            goto L_0x0043
        L_0x004a:
            r7.extractDataNode(r3, r9)
            goto L_0x0043
        L_0x004e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mol.seaplus.tool.datareader.data.impl.XmlDataReader.extractXmlNodeList(org.w3c.dom.NodeList, com.mol.seaplus.tool.datareader.data.IXmlDataHolder):void");
    }

    private void printXmlList(NodeList nodeList) {
        if (nodeList != null && nodeList.getLength() > 0) {
            for (int i = 0; i < nodeList.getLength(); i++) {
                printXmlList(nodeList.item(i).getChildNodes());
            }
        }
    }

    private void extractDataList(NodeList nodeList, IXmlDataHolder iXmlDataHolder) {
        if (nodeList != null && nodeList.getLength() > 0) {
            for (int i = 0; i < nodeList.getLength(); i++) {
                extractDataNode(nodeList.item(i), iXmlDataHolder);
            }
        }
    }

    private void extractDataNode(Node node, IXmlDataHolder iXmlDataHolder) {
        Node dataNode;
        int dataType;
        NodeList dataList = node.getChildNodes();
        if (dataList != null && dataList.getLength() > 0) {
            String tag = node.getNodeName();
            if (dataList.getLength() == 1 && ((dataType = (dataNode = dataList.item(0)).getNodeType()) == 3 || dataType == 4)) {
                String value = dataNode.getNodeValue();
                if (iXmlDataHolder == null) {
                    putTableData(tag, value);
                } else if (iXmlDataHolder.isContain(tag)) {
                    iXmlDataHolder.put(tag, value);
                }
            } else if (iXmlDataHolder == null) {
                extractXmlNodeList(dataList, iXmlDataHolder);
            } else if (iXmlDataHolder.isContain(tag)) {
                IXmlDataHolder childHolder = (IXmlDataHolder) iXmlDataHolder.getChildHolderByTag(tag);
                if (childHolder == null) {
                    iXmlDataHolder.put(tag, node);
                    return;
                }
                childHolder.setXmlNode(node);
                extractDataNode(node, childHolder);
                Vector result = (Vector) iXmlDataHolder.get(tag);
                if (result == null) {
                    result = new Vector();
                    iXmlDataHolder.put(tag, result);
                }
                result.add(childHolder);
            } else {
                extractDataList(node.getChildNodes(), iXmlDataHolder);
            }
        }
    }
}
