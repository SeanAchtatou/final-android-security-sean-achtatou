package com.mol.seaplus.tool.datareader.data.impl;

import java.util.Vector;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlNodeList implements NodeList {
    private Vector<Node> nodeList = new Vector<>();

    public void add(Node node) {
        this.nodeList.add(node);
    }

    public Node item(int index) {
        return this.nodeList.get(index);
    }

    public int getLength() {
        return this.nodeList.size();
    }
}
