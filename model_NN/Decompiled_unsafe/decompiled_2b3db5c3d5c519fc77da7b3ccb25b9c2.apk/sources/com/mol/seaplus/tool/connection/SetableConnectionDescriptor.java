package com.mol.seaplus.tool.connection;

import java.io.InputStream;

public abstract class SetableConnectionDescriptor implements ISetableConnectionDescriptor {
    private String error;
    private int errorCode;
    private InputStream in;
    private long length = -1;

    public void setInputStream(InputStream in2) {
        this.in = in2;
    }

    public InputStream getInputStream() {
        return this.in;
    }

    public boolean isSuccess() {
        return this.in != null;
    }

    public void setError(int errorCode2, String error2) {
        this.errorCode = errorCode2;
        this.error = error2;
    }

    public String getError() {
        return this.error;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setLength(long length2) {
        this.length = length2;
    }

    public long getLength() {
        return this.length;
    }
}
