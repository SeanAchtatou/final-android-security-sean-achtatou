package com.mol.seaplus.tool.connection;

import java.io.InputStream;

public interface IConnectionDescriptor {
    public static final long UNKNOWN_LENGTH = -1;

    IConnectionDescriptor deepCopy();

    String getError();

    int getErrorCode();

    InputStream getInputStream();

    long getLength();

    boolean isSuccess();
}
