package com.mol.seaplus.tool.imageloader;

import android.graphics.Bitmap;
import com.mol.seaplus.Log;
import java.util.concurrent.ExecutionException;
import org.apache.http.HttpStatus;

public class CashCardlogo {
    public static Bitmap getLogo(String url) {
        try {
            return Bitmap.createScaledBitmap((Bitmap) new GetcashcardImage().execute(url).get(), HttpStatus.SC_BAD_REQUEST, 200, true);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Log.e("load image InterruptedException", e);
            return null;
        } catch (ExecutionException e2) {
            e2.printStackTrace();
            Log.e("load image ExecutionException", e2);
            return null;
        } catch (Throwable th) {
            return null;
        }
    }
}
