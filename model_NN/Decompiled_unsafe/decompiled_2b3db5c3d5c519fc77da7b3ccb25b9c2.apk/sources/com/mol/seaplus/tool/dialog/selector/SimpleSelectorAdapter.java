package com.mol.seaplus.tool.dialog.selector;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;
import com.mol.seaplus.tool.utils.UiUtils;

public class SimpleSelectorAdapter extends ListViewSelectorAdapter<String> {
    public SimpleSelectorAdapter(Context context, String[] datas) {
        super(context, datas);
    }

    public SimpleSelectorAdapter(Context context, String[] datas, int index) {
        super(context, datas, index);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = (TextView) convertView;
        if (textView == null) {
            Context context = getContext();
            textView = new TextView(context);
            textView.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
            textView.setGravity(17);
            textView.setBackgroundColor(-1);
            textView.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
            textView.setTextSize(1, 20.0f);
            textView.setPadding(0, (int) UiUtils.convertDpiToPixel(context, 10), 0, (int) UiUtils.convertDpiToPixel(context, 10));
        }
        textView.setText((CharSequence) getData(position));
        return textView;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public Object getItem(int position) {
        return null;
    }
}
