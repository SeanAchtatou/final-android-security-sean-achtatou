package com.mol.seaplus.tool.utils.os;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class UIHandler extends Handler {
    public static final int UPDATE_UI = 15790321;
    private static final String UPDATE_UI_KEY = "UPDATE_UI_KEY";
    private UIHandlerListener uiListener;

    public interface UIHandlerListener {
        void onUpdateUI(int i);
    }

    public UIHandler() {
        super(Looper.getMainLooper());
    }

    public UIHandler(Looper looper) {
        super(looper);
    }

    public void setUIHandlerListener(UIHandlerListener uiListener2) {
        this.uiListener = uiListener2;
    }

    public Message sendUpdateUi() {
        return sendMessage(UPDATE_UI);
    }

    public Message sendMessage(int source) {
        Bundle b = new Bundle();
        b.putInt(UPDATE_UI_KEY, source);
        Message msg = Message.obtain();
        msg.setData(b);
        sendMessage(msg);
        return msg;
    }

    /* access modifiers changed from: protected */
    public boolean onHandlerMessage(Message message, int source) {
        return false;
    }

    public final void handleMessage(Message msg) {
        int source = msg.getData().getInt(UPDATE_UI_KEY);
        if (!onHandlerMessage(msg, source) && this.uiListener != null) {
            this.uiListener.onUpdateUI(source);
        }
    }
}
