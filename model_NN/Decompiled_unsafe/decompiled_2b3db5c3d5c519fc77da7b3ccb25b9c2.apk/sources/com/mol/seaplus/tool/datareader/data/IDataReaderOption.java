package com.mol.seaplus.tool.datareader.data;

import com.mol.seaplus.tool.connection.IConnection2;
import com.mol.seaplus.tool.datareader.data.IDataHolder;

public interface IDataReaderOption<T extends IDataHolder> {
    IConnection2 getConnection();

    T getDataHolder();

    IDataReaderOption setDataHolder(T t);
}
