package com.mol.seaplus.tool.connection;

import java.io.InputStream;

public interface ISetableConnectionDescriptor extends IConnectionDescriptor {
    void setError(int i, String str);

    void setInputStream(InputStream inputStream);
}
