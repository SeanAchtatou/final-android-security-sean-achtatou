package com.mol.seaplus.dcb.sdk;

import com.bluepay.pay.PublisherCode;
import com.mol.seaplus.webview.sdk.Channel;

public final class DCB {
    public static final Channel channel = Channel.createChannel("DCB", PublisherCode.PUBLISHER_DCB);
}
