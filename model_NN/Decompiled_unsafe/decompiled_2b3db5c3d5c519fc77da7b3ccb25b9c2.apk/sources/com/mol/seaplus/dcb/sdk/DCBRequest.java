package com.mol.seaplus.dcb.sdk;

import android.content.Context;
import com.mol.seaplus.webview.sdk.WebViewRequest;

public final class DCBRequest extends WebViewRequest {
    private DCBRequest(Context pContext) {
        super(pContext);
    }

    public DCBRequest onRequest(Context pContext) {
        return (DCBRequest) super.onRequest(pContext);
    }

    public static final class Builder extends WebViewRequest.Builder<DCBRequest, Builder> {
        public Builder(Context pContext) {
            super(pContext, DCB.channel);
        }

        /* access modifiers changed from: protected */
        public DCBRequest doBuild(Context pContext) {
            return new DCBRequest(pContext);
        }
    }
}
