package com.mol.seaplus.mpay.sdk;

import com.mol.seaplus.webview.sdk.Channel;

public final class MPay {
    public static final Channel channel = Channel.createChannel("MPAY", "mpay");
}
