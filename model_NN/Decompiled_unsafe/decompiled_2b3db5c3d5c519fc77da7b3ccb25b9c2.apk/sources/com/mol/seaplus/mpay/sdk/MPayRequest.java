package com.mol.seaplus.mpay.sdk;

import android.content.Context;
import com.mol.seaplus.webview.sdk.WebViewRequest;

public final class MPayRequest extends WebViewRequest {
    private MPayRequest(Context pContext) {
        super(pContext);
    }

    public MPayRequest onRequest(Context pContext) {
        return (MPayRequest) super.onRequest(pContext);
    }

    public static final class Builder extends WebViewRequest.Builder<MPayRequest, Builder> {
        public Builder(Context pContext) {
            super(pContext, MPay.channel);
        }

        /* access modifiers changed from: protected */
        public MPayRequest doBuild(Context pContext) {
            return new MPayRequest(pContext);
        }
    }
}
