package com.mol.seaplus;

import com.bluepay.data.Config;

public final class Currency {
    public static final Currency IDR = create("Indonesian Ringgit", "IDR");
    public static final Currency MMK = create("Myanmar Kyat", "MMK");
    public static final Currency MYR = create("Malaysian Ringgit", "MYR");
    public static final Currency PHP = create("Philippine Peso", "PHP");
    public static final Currency SGD = create("Singapore Dollar", "SGD");
    public static final Currency THB = create("Thai Baht", Config.K_CURRENCY_THB);
    public static final Currency VND = create("Vietnamese Dollar", Config.K_CURRENCY_VND);
    private String mCurrency;
    private String mName;

    public static final Currency create(String pName, String pCurrency) {
        return new Currency(pName, pCurrency);
    }

    private Currency(String pName, String pCurrency) {
        this.mName = pName;
        this.mCurrency = pCurrency;
    }

    public String getName() {
        return this.mName;
    }

    public String getCurrency() {
        return this.mCurrency;
    }

    public String toString() {
        return getName();
    }
}
