package com.mol.seaplus.webapisms.sdk;

import android.content.Context;
import com.mol.seaplus.webview.sdk.WebViewRequest;

public final class WebAPISMSRequest extends WebViewRequest {
    private WebAPISMSRequest(Context pContext) {
        super(pContext);
    }

    public WebAPISMSRequest onRequest(Context pContext) {
        return (WebAPISMSRequest) super.onRequest(pContext);
    }

    public static final class Builder extends WebViewRequest.Builder<WebAPISMSRequest, Builder> {
        public Builder(Context pContext) {
            super(pContext, WebAPISMS.channel);
        }

        /* access modifiers changed from: protected */
        public WebAPISMSRequest doBuild(Context pContext) {
            return new WebAPISMSRequest(pContext);
        }
    }
}
