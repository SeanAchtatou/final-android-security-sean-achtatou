package com.mol.seaplus.webapisms.sdk;

import com.mol.seaplus.webview.sdk.Channel;

public final class WebAPISMS {
    public static final Channel channel = Channel.createChannel("WebAPISMS", "psms");
}
