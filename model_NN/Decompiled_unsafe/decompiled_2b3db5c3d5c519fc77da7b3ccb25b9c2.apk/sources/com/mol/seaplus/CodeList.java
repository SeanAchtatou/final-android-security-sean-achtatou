package com.mol.seaplus;

public interface CodeList {
    public static final int ERROR_CANNOT_CHARGING = 306;
    public static final int ERROR_CANNOT_GET_PINCODE = 302;
    public static final int ERROR_CANNOT_GET_PRICE_LIST = 301;
    public static final int ERROR_CANNOT_SEND_SMS = 305;
    public static final int ERROR_GSM_NOT_ALLOWED = 613;
    public static final int ERROR_INVALID_KEYWORD = 615;
    public static final int ERROR_MT_SUBMIT_UNKNOWN_ERROR = 609;
    public static final int ERROR_MT_SUMBIT_INSUFFICIAL_BALLANCE = 600;
    public static final int ERROR_PRICE_IS_INVALID = 304;
    public static final int ERROR_REQUEST_SERIAL_FAIL = 404;
    public static final int ERROR_SIMCARD_NOTFOUND = 303;
    public static final int ERROR_USER_HAS_BEEN_BLOCKED = 614;
    public static final int ERROR_USER_QUOTA_EXCEED = 612;
    public static final int ERROR_USER_REQ_HAS_BEEN_DELAYED = 610;
    public static final int EVENT_CHARGING = 202;
    public static final int EVENT_CHARGING_IN_BACKGROUND = 203;
    public static final int EXTRACT_DATA_ERROR = 16777184;
    public static final int INTERNET_ERROR = 16776962;
    public static final int INTERNET_RESPONSE_ERROR = 16776961;
    public static final int INTERNET_TIMEOUT_ERROR = 16776963;
    public static final int MULFORMED_URL_ERROR = 16776964;
    public static final int NO_INTERNET_CONNECTION = 16776960;
    public static final int REQUEST_TIMEOUT = 2084;
    public static final int UPOINT_ERROR_MOBILE_NUMBER_IS_INVALID = 999;
    public static final int UPOINT_ERROR_PRICE_IS_INVALID = 401;
}
