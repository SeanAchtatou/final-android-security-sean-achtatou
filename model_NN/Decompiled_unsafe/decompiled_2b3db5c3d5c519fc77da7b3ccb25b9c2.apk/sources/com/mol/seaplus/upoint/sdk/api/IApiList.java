package com.mol.seaplus.upoint.sdk.api;

public interface IApiList {
    public static final String UPOINT_API = "https://sea-sdk.molthailand.com";
    public static final String UPOINT_DOMAIN = "https://sea-sdk.molthailand.com";
    public static final String UPOINT_GET_SMS_CODE_API = "https://sea-sdk.molthailand.com/sdk_server/init.php";
    public static final String UPOINT_RESULT_INQUIRY_API = "https://sea-sdk.molthailand.com/upoint/inquiry.php";
    public static final String UPOINT_RESULT_INQUIRY_TEST_API = "http://202.142.215.96/android-lab/champ/usdk/returnDR.php";
}
