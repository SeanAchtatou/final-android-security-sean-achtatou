package com.mol.seaplus.upoint.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.mol.seaplus.Language;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.tool.connection.handler.ErrorHandler;
import com.mol.seaplus.upoint.sdk.BaseBuilder;

abstract class BaseBuilder<T, G extends BaseBuilder> {
    private Context mContext;
    protected ErrorHandler mErrorHandler;
    protected Language mLanguage;
    protected String mPartnerTransactionId;
    protected String mSecretKey;
    protected String mServiceId;

    /* access modifiers changed from: protected */
    public abstract T onBuild(Context context);

    public BaseBuilder(Context pContext) {
        this.mContext = pContext;
    }

    /* access modifiers changed from: protected */
    public Context getContext() {
        return this.mContext;
    }

    public G serviceId(String pServiceId) {
        this.mServiceId = pServiceId;
        return this;
    }

    public G secretKey(String pSecretKey) {
        this.mSecretKey = pSecretKey;
        return this;
    }

    public G partnerTransactionId(String pPartnerTransactionId) {
        this.mPartnerTransactionId = pPartnerTransactionId;
        return this;
    }

    public G language(Language pLanguage) {
        this.mLanguage = pLanguage;
        return this;
    }

    public G setErrorHandler(ErrorHandler pErrorHandler) {
        this.mErrorHandler = pErrorHandler;
        return this;
    }

    /* access modifiers changed from: protected */
    public boolean onCheck() {
        return true;
    }

    private boolean check() {
        if (TextUtils.isEmpty(this.mServiceId)) {
            throw new IllegalArgumentException(Resources.getString(60));
        } else if (TextUtils.isEmpty(this.mSecretKey)) {
            throw new IllegalArgumentException(Resources.getString(61));
        } else if (!TextUtils.isEmpty(this.mPartnerTransactionId)) {
            return onCheck();
        } else {
            throw new IllegalArgumentException(Resources.getString(62));
        }
    }

    public T build() {
        if (check()) {
            return onBuild(this.mContext);
        }
        return null;
    }
}
