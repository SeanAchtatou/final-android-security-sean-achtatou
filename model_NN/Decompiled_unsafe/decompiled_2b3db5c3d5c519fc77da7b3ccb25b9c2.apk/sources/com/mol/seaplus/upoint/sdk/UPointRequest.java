package com.mol.seaplus.upoint.sdk;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.text.TextUtils;
import com.mol.seaplus.Log;
import com.mol.seaplus.base.sdk.IMolRequest;
import com.mol.seaplus.base.sdk.IMolWaitingDialog;
import com.mol.seaplus.base.sdk.impl.BaseMolWaitingDialogPresenter;
import com.mol.seaplus.base.sdk.impl.MolApiCallback;
import com.mol.seaplus.base.sdk.impl.MolRequest;
import com.mol.seaplus.base.sdk.impl.MolWaitingDialog;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.tool.dialog.ToolAlertDialog;
import com.mol.seaplus.upoint.sdk.UPointGetMsisdnDialog;
import com.mol.seaplus.upoint.sdk.UPointGetSmsCodeApiRequest;
import com.mol.seaplus.upoint.sdk.UPointResultInquiryApiRequest;
import org.json.JSONObject;

public final class UPointRequest extends MolRequest {
    private static final String FILTER = "com.mol.easy2pay.upoint.sdk.SMS_SENT";
    private MolApiCallback mMolApiCallback = new MolApiCallback() {
        public void onApiSuccess(JSONObject pResult) {
            UPointRequest.this.hideProgress();
            final String smsCode = pResult.optString("sms_code");
            final String shortcode = pResult.optString("shortcode");
            SMSSendResultReceiver unused = UPointRequest.this.mSmsSendResultReceiver = new SMSSendResultReceiver();
            UPointRequest.this.getContext().registerReceiver(UPointRequest.this.mSmsSendResultReceiver, new IntentFilter(UPointRequest.FILTER));
            String[] buttons = {Resources.getString(68), Resources.getString(69)};
            ToolAlertDialog.show(UPointRequest.this.getContext(), Resources.getString(67).replaceAll("XXX", smsCode).replaceAll("YYY", shortcode), buttons, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int which) {
                    if (which == -1) {
                        UPointRequest.this.sendSMS(shortcode, smsCode);
                    } else {
                        UPointRequest.this.updateUserCancel();
                    }
                }
            });
        }

        public void onApiError(int pErrorCode, String pError) {
            UPointRequest.this.hideProgress();
            UPointRequest.this.updateError(pErrorCode, pError);
        }
    };
    /* access modifiers changed from: private */
    public String mMsisdn;
    /* access modifiers changed from: private */
    public SMSSendResultReceiver mSmsSendResultReceiver;
    /* access modifiers changed from: private */
    public UPointGetSmsCodeApiRequest.Builder mUPointGetSmsCodeApiBuilder;
    private UPointGetSmsCodeApiRequest mUPointGetSmsCodeApiRequest;
    /* access modifiers changed from: private */
    public UPointResultInquiryApiRequest.Builder mUPointResultInquiryApiBuilder;
    /* access modifiers changed from: private */
    public UPointResultInquiryApiRequest mUPointResultInquiryApiRequest;
    private UPointWaitingDialogPresenter mUPointWaitingDialogPresenter;
    /* access modifiers changed from: private */
    public long mWaitingTime;

    public UPointRequest(Context pContext) {
        super(pContext);
    }

    /* access modifiers changed from: private */
    public void unRegisterSmsSendResultReceiver() {
        if (this.mSmsSendResultReceiver != null) {
            getContext().unregisterReceiver(this.mSmsSendResultReceiver);
            this.mSmsSendResultReceiver = null;
        }
    }

    public UPointRequest onRequest(Context pContext) {
        if (TextUtils.isEmpty(this.mMsisdn)) {
            UPointGetMsisdnDialog uPointGetMsisdnDialog = new UPointGetMsisdnDialog(getContext());
            uPointGetMsisdnDialog.setLanguage(getLanguage());
            uPointGetMsisdnDialog.setOnSetMsisdnListener(new UPointGetMsisdnDialog.OnSetMsisdnListener() {
                public void onSetMsisdn(String pMsisdn) {
                    String unused = UPointRequest.this.mMsisdn = pMsisdn;
                    UPointRequest.this.request();
                }
            });
            uPointGetMsisdnDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialogInterface) {
                    UPointRequest.this.updateUserCancel();
                }
            });
            uPointGetMsisdnDialog.show();
        } else {
            this.mUPointGetSmsCodeApiBuilder.msisdn(this.mMsisdn).callback(this.mMolApiCallback);
            showProgress();
            this.mUPointGetSmsCodeApiRequest = (UPointGetSmsCodeApiRequest) this.mUPointGetSmsCodeApiBuilder.build();
            this.mUPointGetSmsCodeApiRequest.request();
        }
        return this;
    }

    public void stopRequest() {
        unRegisterSmsSendResultReceiver();
        if (this.mUPointWaitingDialogPresenter != null) {
            this.mUPointWaitingDialogPresenter.stopWaiting();
        }
        if (this.mUPointGetSmsCodeApiRequest != null) {
            this.mUPointGetSmsCodeApiRequest.stopRequest();
        }
        if (this.mUPointResultInquiryApiRequest != null) {
            this.mUPointResultInquiryApiRequest.stopRequest();
        }
    }

    private void startWaitingDialog() {
        MolWaitingDialog molWaitingDialog = new MolWaitingDialog(getContext());
        UPointWaitingDialogPresenter uPointWaitingDialogPresenter = new UPointWaitingDialogPresenter(molWaitingDialog);
        this.mUPointWaitingDialogPresenter = uPointWaitingDialogPresenter;
        molWaitingDialog.setMolDialogPresenter(uPointWaitingDialogPresenter);
    }

    /* access modifiers changed from: private */
    public void sendSMS(String destination, String message) {
        Log.d("send SMS");
        startWaitingDialog();
        SmsManager.getDefault().sendTextMessage(destination, null, message, PendingIntent.getBroadcast(getContext(), 0, new Intent(FILTER), 134217728), null);
    }

    /* access modifiers changed from: protected */
    public void onUpdateSuccess(JSONObject pJSONObject) {
        ToolAlertDialog.alert(getContext(), Resources.getString(9));
    }

    /* access modifiers changed from: protected */
    public void onUpdateError(int pErrorCode, String pError) {
        unRegisterSmsSendResultReceiver();
        ToolAlertDialog.alert(getContext(), getError(pErrorCode, pError));
    }

    private class UPointWaitingDialogPresenter extends BaseMolWaitingDialogPresenter implements MolApiCallback {
        public UPointWaitingDialogPresenter(IMolWaitingDialog pMolWaitingDialog) {
            super(pMolWaitingDialog);
            setMaxWaitingTime(UPointRequest.this.mWaitingTime);
            UPointRequest.this.mUPointResultInquiryApiBuilder.callback(this);
            pMolWaitingDialog.show();
        }

        public boolean startWaiting() {
            return super.startWaiting();
        }

        /* access modifiers changed from: protected */
        public void onInterval() {
            UPointResultInquiryApiRequest unused = UPointRequest.this.mUPointResultInquiryApiRequest = (UPointResultInquiryApiRequest) UPointRequest.this.mUPointResultInquiryApiBuilder.build();
            UPointRequest.this.mUPointResultInquiryApiRequest.request();
        }

        /* access modifiers changed from: protected */
        public void onDialogTimeout() {
            UPointRequest.this.updateEvent(203);
        }

        /* access modifiers changed from: protected */
        public void onWaitingTimeout() {
            if (UPointRequest.this.mUPointResultInquiryApiRequest != null) {
                UPointRequest.this.mUPointResultInquiryApiRequest.stopRequest();
            }
            UPointRequest.this.updateTimeout();
        }

        public void onApiSuccess(JSONObject pResult) {
            String status = pResult.optString("status");
            if ("200".equals(status)) {
                stopWaiting();
                UPointRequest.this.updateSuccess(pResult);
            } else if ("100".equals(status)) {
                startInterval();
            }
        }

        public void onApiError(int pErrorCode, String pError) {
            switch (pErrorCode) {
                case 16776960:
                case 16776963:
                    startInterval();
                    return;
                case 16776961:
                case 16776962:
                default:
                    stopWaiting();
                    UPointRequest.this.updateError(pErrorCode, pError);
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void onSendSMSSuccess() {
        this.mUPointWaitingDialogPresenter.startWaiting();
        this.mUPointWaitingDialogPresenter.onInterval();
    }

    /* access modifiers changed from: private */
    public void onSendSMSFailed(int pErrorCode, String pError) {
        if (this.mUPointWaitingDialogPresenter != null) {
            this.mUPointWaitingDialogPresenter.stopWaiting();
        }
        updateError(pErrorCode, pError);
    }

    private class SMSSendResultReceiver extends BroadcastReceiver {
        private SMSSendResultReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            UPointRequest.this.unRegisterSmsSendResultReceiver();
            switch (getResultCode()) {
                case -1:
                    UPointRequest.this.onSendSMSSuccess();
                    return;
                default:
                    UPointRequest.this.onSendSMSFailed(305, Resources.getString(70));
                    return;
            }
        }
    }

    public static final class Builder extends BaseBuilder<UPointRequest, Builder> {
        private String mAmount;
        private String mItem;
        private String mMcc;
        private String mMnc;
        private String mMsisdn;
        private IMolRequest.OnRequestListener mOnRequestListener;
        private String mOperator;
        private long mWaitingTime = 30000;

        public Builder(Context context) {
            super(context);
            setErrorHandler(new UPointErrorHandler());
        }

        public Builder mcc(String pMcc) {
            this.mMcc = pMcc;
            return this;
        }

        public Builder mnc(String pMnc) {
            this.mMnc = pMnc;
            return this;
        }

        public Builder msisdn(String pMsisdn) {
            this.mMsisdn = pMsisdn;
            return this;
        }

        public Builder operator(String pOperator) {
            this.mOperator = pOperator;
            return this;
        }

        public Builder amount(String pAmount) {
            this.mAmount = pAmount;
            return this;
        }

        public Builder item(String pItem) {
            this.mItem = pItem;
            return this;
        }

        public Builder waitingTime(long pWaitingTime) {
            this.mWaitingTime = pWaitingTime;
            return this;
        }

        public Builder setOnRequestListener(IMolRequest.OnRequestListener pOnRequestListener) {
            this.mOnRequestListener = pOnRequestListener;
            return this;
        }

        /* access modifiers changed from: protected */
        public boolean onCheck() {
            if (TextUtils.isEmpty(this.mOperator)) {
                throw new IllegalArgumentException(Resources.getString(64));
            } else if (TextUtils.isEmpty(this.mAmount)) {
                throw new IllegalArgumentException(Resources.getString(65));
            } else if (!TextUtils.isEmpty(this.mItem)) {
                return true;
            } else {
                throw new IllegalArgumentException(Resources.getString(66));
            }
        }

        /* access modifiers changed from: protected */
        public UPointRequest onBuild(Context pContext) {
            UPointRequest uPointRequest = new UPointRequest(pContext);
            UPointGetSmsCodeApiRequest.Builder unused = uPointRequest.mUPointGetSmsCodeApiBuilder = (UPointGetSmsCodeApiRequest.Builder) ((UPointGetSmsCodeApiRequest.Builder) ((UPointGetSmsCodeApiRequest.Builder) ((UPointGetSmsCodeApiRequest.Builder) ((UPointGetSmsCodeApiRequest.Builder) new UPointGetSmsCodeApiRequest.Builder(pContext).serviceId(this.mServiceId)).secretKey(this.mSecretKey)).partnerTransactionId(this.mPartnerTransactionId)).msisdn(this.mMsisdn).operator(this.mOperator).mcc(this.mMcc).mnc(this.mMnc).amount(this.mAmount).item(this.mItem).language(this.mLanguage)).setErrorHandler(this.mErrorHandler);
            UPointResultInquiryApiRequest.Builder unused2 = uPointRequest.mUPointResultInquiryApiBuilder = (UPointResultInquiryApiRequest.Builder) ((UPointResultInquiryApiRequest.Builder) ((UPointResultInquiryApiRequest.Builder) ((UPointResultInquiryApiRequest.Builder) ((UPointResultInquiryApiRequest.Builder) new UPointResultInquiryApiRequest.Builder(pContext).serviceId(this.mServiceId)).secretKey(this.mSecretKey)).partnerTransactionId(this.mPartnerTransactionId)).language(this.mLanguage)).setErrorHandler(this.mErrorHandler);
            String unused3 = uPointRequest.mMsisdn = this.mMsisdn;
            long unused4 = uPointRequest.mWaitingTime = this.mWaitingTime;
            uPointRequest.setLanguage(this.mLanguage);
            uPointRequest.setOnRequestListener(this.mOnRequestListener);
            return uPointRequest;
        }
    }
}
