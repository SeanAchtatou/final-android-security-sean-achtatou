package com.mol.seaplus.upoint.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.mol.seaplus.CodeList;
import com.mol.seaplus.Log;
import com.mol.seaplus.base.sdk.MD5Factory;
import com.mol.seaplus.base.sdk.Sdk;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.sdk.psms.PSMSResponse;
import com.mol.seaplus.tool.connection.internet.httpurlconnection.HttpURLGetConnection;
import com.mol.seaplus.tool.datareader.data.IDataReader;
import com.mol.seaplus.tool.datareader.data.impl.DataReaderOption;
import com.mol.seaplus.tool.datareader.data.impl.JSONDataReader;
import com.mol.seaplus.tool.utils.HttpUtils;
import com.mol.seaplus.upoint.sdk.BaseUPointApiRequest;
import java.util.Hashtable;

final class UPointGetSmsCodeApiRequest extends BaseUPointApiRequest {
    private static final String TAG = UPointGetSmsCodeApiRequest.class.getName();
    /* access modifiers changed from: private */
    public String mAmount;
    /* access modifiers changed from: private */
    public String mItem;
    /* access modifiers changed from: private */
    public String mMcc;
    /* access modifiers changed from: private */
    public String mMnc;
    /* access modifiers changed from: private */
    public String mMsisdn;
    /* access modifiers changed from: private */
    public String mOperator;

    public UPointGetSmsCodeApiRequest(Context context) {
        super(context, TAG);
    }

    public void request() {
        if (!UPointUtils.checkMobileNumberFormat(this.mMsisdn)) {
            updateError(CodeList.UPOINT_ERROR_MOBILE_NUMBER_IS_INVALID, Resources.getString(74));
            return;
        }
        if (TextUtils.isEmpty(this.mMcc) || TextUtils.isEmpty(this.mMnc)) {
            String[] simOperator = Sdk.getSimOperator(getContext());
            if (simOperator == null) {
                updateError(303, Resources.getString(71));
                return;
            } else {
                this.mMcc = simOperator[0];
                this.mMnc = simOperator[1];
            }
        }
        int mcc = Integer.parseInt(this.mMcc);
        int mnc = Integer.parseInt(this.mMnc);
        if (!Sdk.isSupportUPoint(mcc, mnc)) {
            updateError(306, Resources.getString(72));
            return;
        }
        if (Sdk.isTelkomsel(mcc, mnc)) {
            this.mOperator = "TELKOMSEL";
        }
        super.request();
    }

    /* access modifiers changed from: protected */
    public IDataReader onInitialRequest(Context context) {
        String msisdn = "62" + this.mMsisdn.substring(1);
        String sig = MD5Factory.md5("getcode" + this.mAmount + this.mItem + msisdn + this.mOperator + this.mPartnerTransactionId + this.mServiceId + this.mSecretKey);
        Hashtable<String, String> params = new Hashtable<>();
        params.put("act", "getcode");
        params.put("p_txid", this.mPartnerTransactionId);
        params.put("msisdn", msisdn);
        params.put("operator", this.mOperator);
        params.put("amount", this.mAmount);
        params.put(PSMSResponse.ITEM, this.mItem);
        params.put("service_id", this.mServiceId);
        params.put("sig", sig);
        String api = "https://sea-sdk.molthailand.com/sdk_server/init.php" + HttpUtils.getParam(params, false, true);
        Log.d("api = " + api);
        return new JSONDataReader(new DataReaderOption(new HttpURLGetConnection(context, api)));
    }

    public static final class Builder extends BaseUPointApiRequest.Builder<UPointGetSmsCodeApiRequest, Builder> {
        private String mAmount;
        private String mItem;
        private String mMcc;
        private String mMnc;
        private String mMsisdn;
        private String mOperator;

        public Builder(Context context) {
            super(context);
        }

        public Builder mcc(String pMcc) {
            this.mMcc = pMcc;
            return this;
        }

        public Builder mnc(String pMnc) {
            this.mMnc = pMnc;
            return this;
        }

        public Builder msisdn(String pMsisdn) {
            this.mMsisdn = pMsisdn;
            return this;
        }

        public Builder operator(String pOperator) {
            this.mOperator = pOperator;
            return this;
        }

        public Builder amount(String pAmount) {
            this.mAmount = pAmount;
            return this;
        }

        public Builder item(String pItem) {
            this.mItem = pItem;
            return this;
        }

        /* access modifiers changed from: protected */
        public boolean onCheck() {
            if (TextUtils.isEmpty(this.mMsisdn)) {
                throw new IllegalArgumentException(Resources.getString(63));
            } else if (TextUtils.isEmpty(this.mOperator)) {
                throw new IllegalArgumentException(Resources.getString(64));
            } else if (TextUtils.isEmpty(this.mAmount)) {
                throw new IllegalArgumentException(Resources.getString(65));
            } else if (!TextUtils.isEmpty(this.mItem)) {
                return true;
            } else {
                throw new IllegalArgumentException(Resources.getString(66));
            }
        }

        /* access modifiers changed from: protected */
        public UPointGetSmsCodeApiRequest doBuild(Context pContext) {
            UPointGetSmsCodeApiRequest uPointGetSmsCodeApiRequest = new UPointGetSmsCodeApiRequest(pContext);
            String unused = uPointGetSmsCodeApiRequest.mMsisdn = this.mMsisdn;
            String unused2 = uPointGetSmsCodeApiRequest.mOperator = this.mOperator;
            String unused3 = uPointGetSmsCodeApiRequest.mMcc = this.mMcc;
            String unused4 = uPointGetSmsCodeApiRequest.mMnc = this.mMnc;
            String unused5 = uPointGetSmsCodeApiRequest.mAmount = this.mAmount;
            String unused6 = uPointGetSmsCodeApiRequest.mItem = this.mItem;
            return uPointGetSmsCodeApiRequest;
        }
    }
}
