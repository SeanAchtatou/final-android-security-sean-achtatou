package com.mol.seaplus.upoint.sdk;

import android.text.TextUtils;

final class UPointUtils {
    UPointUtils() {
    }

    public static final boolean checkMobileNumberFormat(String pMobileNumber) {
        if (TextUtils.isEmpty(pMobileNumber) || !pMobileNumber.startsWith("08") || pMobileNumber.length() < 10 || pMobileNumber.length() > 12) {
            return false;
        }
        return true;
    }
}
