package com.mol.seaplus.upoint.sdk;

import android.content.Context;
import com.mol.seaplus.Log;
import com.mol.seaplus.base.sdk.MD5Factory;
import com.mol.seaplus.base.sdk.Sdk;
import com.mol.seaplus.tool.connection.internet.httpurlconnection.HttpURLGetConnection;
import com.mol.seaplus.tool.datareader.data.IDataReader;
import com.mol.seaplus.tool.datareader.data.impl.DataReaderOption;
import com.mol.seaplus.tool.datareader.data.impl.JSONDataReader;
import com.mol.seaplus.tool.utils.HttpUtils;
import com.mol.seaplus.upoint.sdk.BaseUPointApiRequest;
import com.mol.seaplus.upoint.sdk.api.IApiList;
import java.util.Hashtable;

final class UPointResultInquiryApiRequest extends BaseUPointApiRequest {
    private static final String TAG = UPointResultInquiryApiRequest.class.getName();

    public UPointResultInquiryApiRequest(Context context) {
        super(context, TAG);
    }

    /* access modifiers changed from: protected */
    public IDataReader onInitialRequest(Context context) {
        String sig = MD5Factory.md5(this.mPartnerTransactionId + this.mServiceId + this.mSecretKey);
        Hashtable<String, String> params = new Hashtable<>();
        params.put("p_txid", this.mPartnerTransactionId);
        params.put("service_id", this.mServiceId);
        params.put("sig", sig);
        String api = IApiList.UPOINT_RESULT_INQUIRY_API;
        if (Sdk.isTest()) {
            api = IApiList.UPOINT_RESULT_INQUIRY_TEST_API;
        }
        String api2 = api + HttpUtils.getParam(params, false, true);
        Log.d("api = " + api2);
        return new JSONDataReader(new DataReaderOption(new HttpURLGetConnection(context, api2)));
    }

    /* access modifiers changed from: protected */
    public boolean isSuccess(IDataReader iDataReader) {
        boolean result = super.isSuccess(iDataReader);
        if (!result) {
            return "100".equals(iDataReader.getTableData("status"));
        }
        return result;
    }

    public static final class Builder extends BaseUPointApiRequest.Builder<UPointResultInquiryApiRequest, Builder> {
        public Builder(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public UPointResultInquiryApiRequest doBuild(Context pContext) {
            return new UPointResultInquiryApiRequest(pContext);
        }
    }
}
