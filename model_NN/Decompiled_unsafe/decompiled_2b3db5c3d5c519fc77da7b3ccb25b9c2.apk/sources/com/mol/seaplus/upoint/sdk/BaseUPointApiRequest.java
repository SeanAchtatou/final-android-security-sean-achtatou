package com.mol.seaplus.upoint.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.mol.seaplus.base.sdk.impl.MolApiCallback;
import com.mol.seaplus.base.sdk.impl.MolApiRequest;
import com.mol.seaplus.tool.datareader.api.IApiRequest;
import com.mol.seaplus.tool.datareader.data.IDataReader;

abstract class BaseUPointApiRequest extends MolApiRequest {
    protected String mPartnerTransactionId;
    protected String mSecretKey;
    protected String mServiceId;

    public BaseUPointApiRequest(Context context, String source) {
        super(context, source);
    }

    /* access modifiers changed from: protected */
    public boolean isSuccess(IDataReader iDataReader) {
        return getApiStatusCode(iDataReader) == 200;
    }

    /* access modifiers changed from: protected */
    public int getApiStatusCode(IDataReader pDataReader) {
        String status = pDataReader.getTableData("status");
        if (TextUtils.isEmpty(status)) {
            return 0;
        }
        try {
            return Integer.parseInt(status);
        } catch (Exception e) {
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    public String getApiDescription(IDataReader iDataReader) {
        return iDataReader.getTableData("status_detail");
    }

    protected static abstract class Builder<T extends IApiRequest, G extends Builder> extends BaseBuilder<T, G> {
        private MolApiCallback mMolApiCallback;

        /* access modifiers changed from: protected */
        public abstract T doBuild(Context context);

        public Builder(Context context) {
            super(context);
        }

        public G callback(MolApiCallback pMolApiCallback) {
            this.mMolApiCallback = pMolApiCallback;
            return this;
        }

        /* access modifiers changed from: protected */
        public T onBuild(Context pContext) {
            T t = doBuild(pContext);
            BaseUPointApiRequest baseUPointApiRequest = (BaseUPointApiRequest) t;
            baseUPointApiRequest.mServiceId = this.mServiceId;
            baseUPointApiRequest.mSecretKey = this.mSecretKey;
            baseUPointApiRequest.mPartnerTransactionId = this.mPartnerTransactionId;
            baseUPointApiRequest.setLanguage(this.mLanguage);
            baseUPointApiRequest.setMolApiCallback(this.mMolApiCallback);
            baseUPointApiRequest.setErrorHandler(this.mErrorHandler);
            return t;
        }
    }
}
