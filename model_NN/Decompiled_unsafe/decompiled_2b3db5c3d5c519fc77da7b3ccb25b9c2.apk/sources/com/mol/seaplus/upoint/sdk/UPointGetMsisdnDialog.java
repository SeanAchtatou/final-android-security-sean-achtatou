package com.mol.seaplus.upoint.sdk;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mol.seaplus.base.sdk.impl.MolDialog;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.tool.dialog.ToolAlertDialog;
import com.mol.seaplus.tool.utils.UiUtils;

class UPointGetMsisdnDialog extends MolDialog implements View.OnClickListener {
    private EditText mMsisdnEditText;
    private OnSetMsisdnListener mOnSetMsisdnListener;

    public interface OnSetMsisdnListener {
        void onSetMsisdn(String str);
    }

    public UPointGetMsisdnDialog(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onCreateDialog() {
        setContentView(initView(this.context));
    }

    private View initView(Context pContext) {
        int padding5 = (int) UiUtils.convertDpiToPixel(pContext, 5);
        LinearLayout linearLayout = new LinearLayout(pContext);
        this.mMsisdnEditText = new EditText(pContext);
        Button mNextBtn = new Button(pContext);
        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(1);
        linearLayout.setGravity(1);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1, -2);
        params.bottomMargin = padding5;
        TextView msisdnTextView = new TextView(this.context);
        msisdnTextView.setLayoutParams(params);
        msisdnTextView.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
        msisdnTextView.setTextSize(2, 18.0f);
        msisdnTextView.setText(Resources.getString(75));
        this.mMsisdnEditText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.mMsisdnEditText.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
        this.mMsisdnEditText.setHintTextColor(-7829368);
        this.mMsisdnEditText.setTextSize(2, 18.0f);
        this.mMsisdnEditText.setHint("08XXXXXXX");
        this.mMsisdnEditText.setSingleLine(true);
        this.mMsisdnEditText.setInputType(8194);
        this.mMsisdnEditText.setPadding(padding5, padding5, padding5, padding5);
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(-2, -2);
        params2.topMargin = padding5;
        mNextBtn.setLayoutParams(params2);
        mNextBtn.setTextSize(2, 16.0f);
        mNextBtn.setText(Resources.getString(76));
        mNextBtn.setOnClickListener(this);
        linearLayout.addView(msisdnTextView);
        linearLayout.addView(this.mMsisdnEditText);
        linearLayout.addView(mNextBtn);
        return linearLayout;
    }

    public void setOnSetMsisdnListener(OnSetMsisdnListener pOnSetMsisdnListener) {
        this.mOnSetMsisdnListener = pOnSetMsisdnListener;
    }

    public void onClick(View view) {
        if (!UPointUtils.checkMobileNumberFormat(this.mMsisdnEditText.getText().toString())) {
            ToolAlertDialog.alert(getContext(), Resources.getString(74));
            return;
        }
        if (this.mOnSetMsisdnListener != null) {
            this.mOnSetMsisdnListener.onSetMsisdn(this.mMsisdnEditText.getText().toString());
        }
        dismiss();
    }
}
