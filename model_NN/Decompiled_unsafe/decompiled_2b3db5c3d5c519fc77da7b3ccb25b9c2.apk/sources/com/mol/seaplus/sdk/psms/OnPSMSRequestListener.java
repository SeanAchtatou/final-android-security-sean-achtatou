package com.mol.seaplus.sdk.psms;

import com.mol.seaplus.base.sdk.IMolRequest;

interface OnPSMSRequestListener extends IMolRequest.BaseRequestListener {
    void onPSMSRequestSuccess(PSMSResponse pSMSResponse);
}
