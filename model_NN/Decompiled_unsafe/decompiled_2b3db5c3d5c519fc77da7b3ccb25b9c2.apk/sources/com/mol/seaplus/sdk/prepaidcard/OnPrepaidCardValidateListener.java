package com.mol.seaplus.sdk.prepaidcard;

public interface OnPrepaidCardValidateListener {
    void onPrepaidCardApiError(int i, String str);

    void onPrepaidCardApiSuccess(PrepaidCardApiResponse prepaidCardApiResponse);
}
