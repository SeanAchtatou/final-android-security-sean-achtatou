package com.mol.seaplus.sdk;

import com.mol.seaplus.base.sdk.IMolRequest;
import com.mol.seaplus.sdk.MOLSEAPlus;

public interface MOLSEAPlusListener extends IMolRequest.BaseRequestListener {
    void onRequestSuccess(MOLSEAPlus.PaymentChannel paymentChannel, ResultHolder resultHolder);
}
