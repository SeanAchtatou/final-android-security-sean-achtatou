package com.mol.seaplus.sdk.webapisms;

import com.mol.seaplus.base.sdk.IMolRequest;

public interface OnWebAPISMSRequestListener extends IMolRequest.BaseRequestListener {
    void onWebAPISMSRequestSuccess(WebAPISMSResponse webAPISMSResponse);
}
