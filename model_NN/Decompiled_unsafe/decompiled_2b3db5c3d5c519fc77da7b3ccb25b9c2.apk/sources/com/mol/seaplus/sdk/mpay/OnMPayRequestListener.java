package com.mol.seaplus.sdk.mpay;

import com.mol.seaplus.base.sdk.IMolRequest;

public interface OnMPayRequestListener extends IMolRequest.BaseRequestListener {
    void onMPayRequestSuccess(MPayResponse mPayResponse);
}
