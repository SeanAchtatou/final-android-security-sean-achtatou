package com.mol.seaplus.sdk.prepaidcard;

import com.mol.seaplus.base.sdk.IMolRequest;

public interface OnPrepaidCardVerifyListener extends IMolRequest.BaseRequestListener {
    void onPrepaidCardVerifyRequestSuccess(PrepaidCardApiResponse prepaidCardApiResponse);
}
