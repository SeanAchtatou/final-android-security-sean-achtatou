package com.mol.seaplus.sdk.dcb;

interface OnDCBResultInquiryApiCallbackListener {
    void onDCBResultInquiryApiError(int i, String str);

    void onDCBResultInquiryApiSuccess(DCBResponse dCBResponse);
}
