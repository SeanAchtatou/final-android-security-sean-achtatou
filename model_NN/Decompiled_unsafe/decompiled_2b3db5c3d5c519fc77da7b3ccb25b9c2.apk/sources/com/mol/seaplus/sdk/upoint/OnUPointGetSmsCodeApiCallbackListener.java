package com.mol.seaplus.sdk.upoint;

interface OnUPointGetSmsCodeApiCallbackListener {
    void onGetSmsCodeApiFail(int i, String str);

    void onGetSmsCodeApiSuccess(UPointResponse uPointResponse);
}
