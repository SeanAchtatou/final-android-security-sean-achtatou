package com.mol.seaplus.sdk.dcb;

import com.mol.seaplus.base.sdk.IMolRequest;

public interface OnDCBRequestListener extends IMolRequest.BaseRequestListener {
    void onDCBRequestSuccess(DCBResponse dCBResponse);
}
