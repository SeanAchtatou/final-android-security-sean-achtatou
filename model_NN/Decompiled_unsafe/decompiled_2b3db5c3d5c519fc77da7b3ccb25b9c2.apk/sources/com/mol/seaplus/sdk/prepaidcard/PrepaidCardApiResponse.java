package com.mol.seaplus.sdk.prepaidcard;

import com.mol.seaplus.tool.datareader.data.IDataHolder;
import com.mol.seaplus.tool.datareader.data.impl.DataHolder;

public final class PrepaidCardApiResponse extends DataHolder {
    private static final String AMOUNT = "amount";
    private static final String CARD_NO = "cardno";
    private static final String CHANNEL = "channel";
    private static final String CUSTOMER_ID = "cus_id";
    private static final String GAME_ID = "gameid";
    private static final String[] KEYS = {ORDER_ID, MERCHANT_ID, MREF_ID, GAME_ID, CUSTOMER_ID, CHANNEL, AMOUNT, CARD_NO, SERIAL_NO, PIN_NO, TRANSDATETIME, SIGNATURE};
    private static final String MERCHANT_ID = "merchantid";
    private static final String MREF_ID = "mref_id";
    private static final String ORDER_ID = "orderid";
    private static final String PIN_NO = "pin_no";
    private static final String SERIAL_NO = "serial_no";
    private static final String SIGNATURE = "signature";
    private static final String TRANSDATETIME = "transdatetime";

    public PrepaidCardApiResponse() {
        super("response", KEYS);
    }

    public String getOrderId() {
        return getString(ORDER_ID);
    }

    public String getMerchantId() {
        return getString(MERCHANT_ID);
    }

    public String getGameId() {
        return getString(GAME_ID);
    }

    public String getCustomerId() {
        return getString(CUSTOMER_ID);
    }

    public String getRefId() {
        return getString(MREF_ID);
    }

    public String getChannel() {
        return getString(CHANNEL);
    }

    public String getAmount() {
        return getString(AMOUNT);
    }

    public String getCardNo() {
        return getString(CARD_NO);
    }

    public String getSerialCode() {
        return getString(SERIAL_NO);
    }

    public String getPinCode() {
        return getString(PIN_NO);
    }

    public String getTransactionDate() {
        return getString(TRANSDATETIME);
    }

    public String getSignature() {
        return getString(SIGNATURE);
    }

    public DataHolder initDataHolder() {
        return new PrepaidCardApiResponse();
    }

    public IDataHolder getChildHolderByTag(String tag) {
        return null;
    }
}
