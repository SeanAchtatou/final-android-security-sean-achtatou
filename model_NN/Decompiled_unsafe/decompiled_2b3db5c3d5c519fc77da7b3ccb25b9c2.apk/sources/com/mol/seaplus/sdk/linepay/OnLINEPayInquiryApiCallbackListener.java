package com.mol.seaplus.sdk.linepay;

interface OnLINEPayInquiryApiCallbackListener {
    void onLINEPayResultInquiryApiError(int i, String str);

    void onLINEPayResultInquiryApiSuccess(LINEPayResponse lINEPayResponse);
}
