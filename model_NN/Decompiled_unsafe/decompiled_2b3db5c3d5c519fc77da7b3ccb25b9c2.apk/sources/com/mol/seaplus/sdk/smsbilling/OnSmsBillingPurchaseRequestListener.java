package com.mol.seaplus.sdk.smsbilling;

import com.mol.seaplus.base.sdk.IMolRequest;

public interface OnSmsBillingPurchaseRequestListener extends IMolRequest.BaseRequestListener {
    void onSmsBillingPurchaseRequestSuccess(SmsBillingApiResponse smsBillingApiResponse);
}
