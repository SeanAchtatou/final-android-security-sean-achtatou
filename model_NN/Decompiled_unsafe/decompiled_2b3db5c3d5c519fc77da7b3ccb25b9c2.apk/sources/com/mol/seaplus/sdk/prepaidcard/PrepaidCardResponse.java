package com.mol.seaplus.sdk.prepaidcard;

import com.mol.seaplus.tool.datareader.data.IDataHolder;
import com.mol.seaplus.tool.datareader.data.impl.DataHolder;

public class PrepaidCardResponse extends DataHolder {
    private static final String[] KEYS = {ORDER_ID, TRANSACTION_ID};
    private static final String ORDER_ID = "orderid";
    private static final String TRANSACTION_ID = "txid";

    public PrepaidCardResponse() {
        super("response", KEYS);
    }

    public String getOrderId() {
        return getString(ORDER_ID);
    }

    public String getTransactionId() {
        return getString(TRANSACTION_ID);
    }

    public DataHolder initDataHolder() {
        return new PrepaidCardResponse();
    }

    public IDataHolder getChildHolderByTag(String tag) {
        return null;
    }
}
