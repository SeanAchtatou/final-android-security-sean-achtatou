package com.mol.seaplus.sdk.smsbilling;

import com.mol.seaplus.tool.datareader.data.IDataHolder;
import com.mol.seaplus.tool.datareader.data.impl.DataHolder;

public class SmsBillingApiResponse extends DataHolder {
    private static final String[] KEYS = {"pTxId", "userId", "priceId", "txId"};
    private static final String PRICE_ID = "priceId";
    private static final String REF_ID = "pTxId";
    private static final String TRANSACTION_ID = "txId";
    private static final String USER_ID = "userId";

    public SmsBillingApiResponse() {
        super("response", KEYS);
    }

    public String getRefId() {
        return getString("pTxId");
    }

    public String getUserId() {
        return getString("userId");
    }

    public String getPriceId() {
        return getString("priceId");
    }

    public String getTransactionId() {
        return getString("txId");
    }

    public DataHolder initDataHolder() {
        return new SmsBillingApiResponse();
    }

    public IDataHolder getChildHolderByTag(String s) {
        return null;
    }
}
