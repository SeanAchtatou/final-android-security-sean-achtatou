package com.mol.seaplus.sdk.dcb;

import com.mol.seaplus.tool.datareader.data.IDataHolder;
import com.mol.seaplus.tool.datareader.data.impl.DataHolder;

class DCBApiResponse extends DataHolder {
    private static final String AMOUNT = "amount";
    private static final String CHANNEL = "channel";
    private static final String CONTENT_NAME = "for";
    private static final String CURRENCY = "currency";
    private static final String[] KEYS = {ORDER_ID, TRANSACTION_ID, CHANNEL, AMOUNT, "currency", CONTENT_NAME, USER_ID};
    private static final String ORDER_ID = "orderid";
    private static final String TRANSACTION_ID = "txid";
    private static final String USER_ID = "uid";

    public DCBApiResponse() {
        super("response", KEYS);
    }

    public String getOrderId() {
        return getString(ORDER_ID);
    }

    public String getTransactionId() {
        return getString(TRANSACTION_ID);
    }

    public String getChannel() {
        return getString(CHANNEL);
    }

    public String getAmount() {
        return getString(AMOUNT);
    }

    public String getCurrency() {
        return getString("currency");
    }

    public String getContentName() {
        return getString(CONTENT_NAME);
    }

    public String getUserId() {
        return getString(USER_ID);
    }

    public DataHolder initDataHolder() {
        return new DCBApiResponse();
    }

    public IDataHolder getChildHolderByTag(String s) {
        return null;
    }
}
