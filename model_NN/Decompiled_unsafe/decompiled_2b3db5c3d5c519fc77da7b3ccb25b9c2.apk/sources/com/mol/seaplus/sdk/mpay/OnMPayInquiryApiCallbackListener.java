package com.mol.seaplus.sdk.mpay;

interface OnMPayInquiryApiCallbackListener {
    void onMPayResultInquiryApiError(int i, String str);

    void onMPayResultInquiryApiSuccess(MPayResponse mPayResponse);
}
