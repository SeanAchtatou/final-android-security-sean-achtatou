package com.mol.seaplus.sdk;

public final class ResultHolder {
    /* access modifiers changed from: private */
    public String mPartnerTransactionId;
    /* access modifiers changed from: private */
    public String mPriceId;
    /* access modifiers changed from: private */
    public String mTransactionId;
    /* access modifiers changed from: private */
    public String mUserId;

    public String getTransactionId() {
        return this.mTransactionId;
    }

    public String getPartnerTransactionId() {
        return this.mPartnerTransactionId;
    }

    public String getUserId() {
        return this.mUserId;
    }

    public String getPriceId() {
        return this.mPriceId;
    }

    static class Builder {
        private String mPartnerTransactionId;
        private String mPriceId;
        private String mTransactionId;
        private String mUserId;

        Builder() {
        }

        public Builder transactionId(String pTransactionId) {
            this.mTransactionId = pTransactionId;
            return this;
        }

        public Builder partnerTransactionId(String pPartnerTransactionId) {
            this.mPartnerTransactionId = pPartnerTransactionId;
            return this;
        }

        public Builder userId(String pUserId) {
            this.mUserId = pUserId;
            return this;
        }

        public Builder priceId(String pPriceId) {
            this.mPriceId = pPriceId;
            return this;
        }

        public ResultHolder build() {
            ResultHolder resultHolder = new ResultHolder();
            String unused = resultHolder.mTransactionId = this.mTransactionId;
            String unused2 = resultHolder.mPartnerTransactionId = this.mPartnerTransactionId;
            String unused3 = resultHolder.mUserId = this.mUserId;
            String unused4 = resultHolder.mPriceId = this.mPriceId;
            return resultHolder;
        }
    }
}
