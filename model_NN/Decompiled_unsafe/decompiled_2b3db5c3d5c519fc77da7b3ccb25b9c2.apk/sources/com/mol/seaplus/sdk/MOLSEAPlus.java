package com.mol.seaplus.sdk;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.mol.seaplus.Currency;
import com.mol.seaplus.Language;
import com.mol.seaplus.base.sdk.IMolRequest;
import com.mol.seaplus.dcb.sdk.DCBRequest;
import com.mol.seaplus.linepay.sdk.LINEPayRequest;
import com.mol.seaplus.mpay.sdk.MPayRequest;
import com.mol.seaplus.prepaidcard.sdk.PrepaidCardVerifyRequest;
import com.mol.seaplus.prepaidcard.sdk2.PrepaidCard;
import com.mol.seaplus.prepaidcard.sdk2.PrepaidCardRequest;
import com.mol.seaplus.sdk.ResultHolder;
import com.mol.seaplus.sdk.dcb.DCBResponse;
import com.mol.seaplus.sdk.dcb.OnDCBRequestListener;
import com.mol.seaplus.sdk.linepay.LINEPayResponse;
import com.mol.seaplus.sdk.linepay.OnLINEPayRequestListener;
import com.mol.seaplus.sdk.mpay.MPayResponse;
import com.mol.seaplus.sdk.mpay.OnMPayRequestListener;
import com.mol.seaplus.sdk.prepaidcard.OnPrepaidCardRequestListener;
import com.mol.seaplus.sdk.prepaidcard.OnPrepaidCardValidateListener;
import com.mol.seaplus.sdk.prepaidcard.OnPrepaidCardVerifyListener;
import com.mol.seaplus.sdk.prepaidcard.PrepaidCardApiResponse;
import com.mol.seaplus.sdk.prepaidcard.PrepaidCardResponse;
import com.mol.seaplus.sdk.psms.PSMSResponse;
import com.mol.seaplus.sdk.smsbilling.OnSmsBillingPurchaseRequestListener;
import com.mol.seaplus.sdk.smsbilling.SmsBillingApiResponse;
import com.mol.seaplus.sdk.upoint.OnUPointRequestListener;
import com.mol.seaplus.sdk.webapisms.OnWebAPISMSRequestListener;
import com.mol.seaplus.sdk.webapisms.WebAPISMSResponse;
import com.mol.seaplus.smsbilling.sdk.SmsBillingPurchaseRequest;
import com.mol.seaplus.tool.app.CheckAppInstall;
import com.mol.seaplus.tool.utils.TimeUtils;
import com.mol.seaplus.upoint.sdk.UPointRequest;
import com.mol.seaplus.webapisms.sdk.WebAPISMSRequest;

public final class MOLSEAPlus {
    private Context mContext;
    private Language mLanguage;
    private String mSecretKey;
    private String mServiceId;
    /* access modifiers changed from: private */
    public SmsBillingPurchaseRequest mSmsBillingPurchaseRequest;

    public enum PaymentChannel {
        PrepaidCard,
        PSMS,
        DCB,
        WebAPISMS,
        LINEPAY,
        MPAY
    }

    public static final void enableTest(boolean pEnableTest) {
        MolCoreSdk.enableTest(pEnableTest);
    }

    public MOLSEAPlus(Context pContext, String pServiceId, String pSecretKey) {
        this(pContext, pServiceId, pSecretKey, Language.EN);
    }

    public MOLSEAPlus(Context pContext, String pServiceId, String pSecretKey, Language pLanguage) {
        this.mLanguage = Language.EN;
        this.mContext = pContext;
        this.mServiceId = pServiceId;
        this.mSecretKey = pSecretKey;
        setLanguage(pLanguage);
    }

    public void setLanguage(Language pLanguage) {
        this.mLanguage = pLanguage;
    }

    public void onResume() {
        if (this.mSmsBillingPurchaseRequest != null) {
            this.mSmsBillingPurchaseRequest.onResume();
        }
    }

    public void onPause() {
        if (this.mSmsBillingPurchaseRequest != null) {
            this.mSmsBillingPurchaseRequest.onPause();
        }
    }

    public void purchaseByPrepaidCard(String pUserId, String pOrderId, MOLSEAPlusListener pMOLSEAPlusListener) {
        purchaseByPrepaidCard(null, pUserId, pOrderId, pMOLSEAPlusListener);
    }

    public void purchaseByPrepaidCard(PrepaidCard pPrepaidCard, String pUserId, String pOrderId, MOLSEAPlusListener pMOLSEAPlusListener) {
        purchaseByPrepaidCard(pPrepaidCard, pUserId, pOrderId, (int) TimeUtils.MIN, pMOLSEAPlusListener);
    }

    public void purchaseByPrepaidCard(PrepaidCard pPrepaidCard, String pUserId, String pOrderId, int pWaitTime, MOLSEAPlusListener pMOLSEAPlusListener) {
        ((PrepaidCardRequest) ((PrepaidCardRequest.Builder) ((PrepaidCardRequest.Builder) ((PrepaidCardRequest.Builder) ((PrepaidCardRequest.Builder) ((PrepaidCardRequest.Builder) ((PrepaidCardRequest.Builder) MolCoreSdk.buildPrepaidCardRequest(this.mContext, new PrepaidCardWebViewRequestWrapper(pMOLSEAPlusListener)).serviceId(this.mServiceId)).secretKey(this.mSecretKey)).forCard(pPrepaidCard).userId(pUserId)).orderId(pOrderId)).waitingTime((long) pWaitTime)).language(this.mLanguage)).build()).request();
    }

    public void purchaseByPrepaidCard(String pCustomerId, String pGameId, String pRefId, String pCallbackUrl, MOLSEAPlusListener pMOLSEAPlusListener) {
        purchaseByPrepaidCard(null, pCustomerId, pGameId, pRefId, pCallbackUrl, pMOLSEAPlusListener);
    }

    public void purchaseByPrepaidCard(com.mol.seaplus.prepaidcard.sdk.PrepaidCard pPrepaidCard, String pCustomerId, String pGameId, String pRefId, String pCallbackUrl, MOLSEAPlusListener pMOLSEAPlusListener) {
        ((PrepaidCardVerifyRequest) ((PrepaidCardVerifyRequest.Builder) MolCoreSdk.buildPrepaidCardVerifyRequest(this.mContext, new PrepaidCardVerifyApiRequestWrapper(pMOLSEAPlusListener)).merchantId(this.mServiceId).secretKey(this.mSecretKey).forCard(pPrepaidCard).customerId(pCustomerId).gameId(pGameId).refId(pRefId).backUrl(pCallbackUrl).language(this.mLanguage)).build()).request();
    }

    public void validatePurchaseByPrepaidCard(String pRefId, OnPrepaidCardValidateListener pOnPrepaidCardValidateListener) {
        MolCoreSdk.buildPrepaidCardValidateApiRequest(this.mContext, pOnPrepaidCardValidateListener).merchantId(this.mServiceId).secretKey(this.mSecretKey).refId(pRefId).build().request();
    }

    private void purchaseBySmsBilling(String pPartnerTransactionId, String pUserId, String pPriceId, String pMcc, String pMnc, int pWaitingTime, boolean pIsFullScreen, OnSmsBillingPurchaseRequestListener pOnSmsBillingPurchaseRequestListener) {
        this.mSmsBillingPurchaseRequest = (SmsBillingPurchaseRequest) ((SmsBillingPurchaseRequest.Builder) ((SmsBillingPurchaseRequest.Builder) ((SmsBillingPurchaseRequest.Builder) ((SmsBillingPurchaseRequest.Builder) ((SmsBillingPurchaseRequest.Builder) MolCoreSdk.buildSmsBillingPurchaseRequest(this.mContext, new MyOnSmsBillingPurchaseRequestListener(pOnSmsBillingPurchaseRequestListener)).partnerId(this.mServiceId)).secretKey(this.mSecretKey)).partnerTransactionId(pPartnerTransactionId)).userId(pUserId)).priceId(pPriceId).smsWaitingTime(pWaitingTime).fullScreen(pIsFullScreen).mcc(pMcc).mnc(pMnc).language(this.mLanguage)).build();
        this.mSmsBillingPurchaseRequest.request();
    }

    private void purchaseByUPoint(String pPartnerTransactionId, String pItem, String pAmount, String pMsisdn, String pOperator, String pMcc, String pMnc, int pWaitingTime, OnUPointRequestListener pOnUPointRequestListener) {
        ((UPointRequest) ((UPointRequest.Builder) ((UPointRequest.Builder) ((UPointRequest.Builder) ((UPointRequest.Builder) MolCoreSdk.buildUPointRequest(this.mContext, pOnUPointRequestListener).serviceId(this.mServiceId)).secretKey(this.mSecretKey)).partnerTransactionId(pPartnerTransactionId)).item(pItem).amount(pAmount).msisdn(pMsisdn).operator(pOperator).mcc(pMcc).mnc(pMnc).waitingTime((long) pWaitingTime).language(this.mLanguage)).build()).request();
    }

    public void purchaseByPSMS(String pPartnerTransactionId, String pUserId, String pPriceId, String pItem, String pOperator, MOLSEAPlusListener pMOLSEAPlusListener) {
        purchaseByPSMS(pPartnerTransactionId, pUserId, pPriceId, pItem, null, pOperator, pMOLSEAPlusListener);
    }

    public void purchaseByPSMS(String pPartnerTransactionId, String pUserId, String pPriceId, String pItem, String pMsisdn, String pOperator, MOLSEAPlusListener pMOLSEAPlusListener) {
        purchaseByPSMS(pPartnerTransactionId, pUserId, pPriceId, pItem, pMsisdn, pOperator, null, null, TimeUtils.MIN, pMOLSEAPlusListener);
    }

    public void purchaseByPSMS(String pPartnerTransactionId, String pUserId, String pPriceId, String pItem, String pMsisdn, String pOperator, String pMcc, String pMnc, int pWaitingTime, MOLSEAPlusListener pMOLSEAPlusListener) {
        int mcc = -1;
        int mnc = -1;
        if (TextUtils.isEmpty(pMcc) || TextUtils.isEmpty(pMnc)) {
            String[] simOpStrings = MolCoreSdk.getSimOperator(this.mContext);
            int[] simOp = MolCoreSdk.getSimOperatorAsInt(this.mContext);
            if (simOp != null) {
                mcc = simOp[0];
                mnc = simOp[1];
                pMcc = simOpStrings[0];
                pMnc = simOpStrings[1];
            }
        } else {
            try {
                mcc = Integer.parseInt(pMcc);
                mnc = Integer.parseInt(pMnc);
            } catch (Exception e) {
                mcc = -1;
                mnc = -1;
            }
        }
        PSMSResponse psmsResponse = new PSMSResponse();
        psmsResponse.put(PSMSResponse.PARTNER_TRANSACTION_ID, pPartnerTransactionId);
        psmsResponse.put(PSMSResponse.USER_ID, pUserId);
        psmsResponse.put(PSMSResponse.PRICE_ID, pPriceId);
        psmsResponse.put(PSMSResponse.ITEM, pItem);
        PSMSRequestWrapper psmsRequestWrapper = new PSMSRequestWrapper(psmsResponse, pMOLSEAPlusListener);
        if (MolCoreSdk.isSupportUPoint(mcc, mnc)) {
            purchaseByUPoint(pPartnerTransactionId, pItem, pPriceId, pMsisdn, pOperator, pMcc, pMnc, pWaitingTime, psmsRequestWrapper);
        } else {
            purchaseBySmsBilling(pPartnerTransactionId, pUserId, pPriceId, pMcc, pMnc, pWaitingTime, true, psmsRequestWrapper);
        }
    }

    public void purchaseByDCB(String pUserId, String pOrderId, String pPrice, Currency pCurrency, String pContent, MOLSEAPlusListener pMOLSEAPlusListener) {
        purchaseByDCB(pUserId, pOrderId, pPrice, pCurrency, pContent, TimeUtils.MIN, pMOLSEAPlusListener);
    }

    public void purchaseByDCB(String pUserId, String pOrderId, String pPrice, Currency pCurrency, String pContent, int pWaitTime, MOLSEAPlusListener pMOLSEAPlusListener) {
        ((DCBRequest) ((DCBRequest.Builder) ((DCBRequest.Builder) ((DCBRequest.Builder) ((DCBRequest.Builder) ((DCBRequest.Builder) ((DCBRequest.Builder) ((DCBRequest.Builder) ((DCBRequest.Builder) MolCoreSdk.buildDCBRequest(this.mContext, new DCBRequestWrapper(pUserId, pPrice, pMOLSEAPlusListener)).serviceId(this.mServiceId)).secretKey(this.mSecretKey)).userId(pUserId)).orderId(pOrderId)).price(pPrice, pCurrency)).content(pContent)).waitingTime((long) pWaitTime)).language(this.mLanguage)).build()).request();
    }

    public void purchaseByWebAPISMS(String pUserId, String pOrderId, String pPrice, Currency pCurrency, String pContent, MOLSEAPlusListener pMOLSEAPlusListener) {
        purchaseByWebAPISMS(pUserId, pOrderId, pPrice, pCurrency, pContent, TimeUtils.MIN, pMOLSEAPlusListener);
    }

    public void purchaseByWebAPISMS(String pUserId, String pOrderId, String pPrice, Currency pCurrency, String pContent, int pWaitTime, MOLSEAPlusListener pMOLSEAPlusListener) {
        ((WebAPISMSRequest) ((WebAPISMSRequest.Builder) ((WebAPISMSRequest.Builder) ((WebAPISMSRequest.Builder) ((WebAPISMSRequest.Builder) ((WebAPISMSRequest.Builder) ((WebAPISMSRequest.Builder) ((WebAPISMSRequest.Builder) ((WebAPISMSRequest.Builder) MolCoreSdk.buildWebAPISMSRequest(this.mContext, new WebAPISMSRequestWrapper(pUserId, pPrice, pMOLSEAPlusListener)).serviceId(this.mServiceId)).secretKey(this.mSecretKey)).userId(pUserId)).orderId(pOrderId)).price(pPrice, pCurrency)).content(pContent)).waitingTime((long) pWaitTime)).language(this.mLanguage)).build()).request();
    }

    public void purchaseByMPay(String pUserId, String pOrderId, String pPrice, Currency pCurrency, String pContent, MOLSEAPlusListener pMOLSEAPlusListener) {
        purchaseByMPay(pUserId, pOrderId, pPrice, pCurrency, pContent, TimeUtils.MIN, pMOLSEAPlusListener);
    }

    public void purchaseByMPay(String pUserId, String pOrderId, String pPrice, Currency pCurrency, String pContent, int pWaitTime, MOLSEAPlusListener pMOLSEAPlusListener) {
        CheckAppInstall checkAPPMPay = new CheckAppInstall();
        if (checkAPPMPay.isPackageInstalled("com.palomar.mpay", this.mContext)) {
            ((MPayRequest) ((MPayRequest.Builder) ((MPayRequest.Builder) ((MPayRequest.Builder) ((MPayRequest.Builder) ((MPayRequest.Builder) ((MPayRequest.Builder) ((MPayRequest.Builder) ((MPayRequest.Builder) MolCoreSdk.buildMPayRequest(this.mContext, new MPayRequestWrapper(pUserId, pPrice, pMOLSEAPlusListener)).serviceId(this.mServiceId)).secretKey(this.mSecretKey)).userId(pUserId)).orderId(pOrderId)).price(pPrice, pCurrency)).content(pContent)).waitingTime((long) pWaitTime)).language(this.mLanguage)).build()).request();
            return;
        }
        Log.w("Mol-SDK", "APP MPay not install send to Play Store APP MPay");
        checkAPPMPay.launchUri("com.palomar.mpay", this.mContext);
    }

    public void purchaseByLINEPay(String pUserId, String pOrderId, String pPrice, Currency pCurrency, String pContent, MOLSEAPlusListener pMOLSEAPlusListener) {
        purchaseByLINEPay(pUserId, pOrderId, pPrice, pCurrency, pContent, TimeUtils.MIN, pMOLSEAPlusListener);
    }

    public void purchaseByLINEPay(String pUserId, String pOrderId, String pPrice, Currency pCurrency, String pContent, int pWaitTime, MOLSEAPlusListener pMOLSEAPlusListener) {
        CheckAppInstall checkAPPLINE = new CheckAppInstall();
        if (checkAPPLINE.isPackageInstalled("jp.naver.line.android", this.mContext)) {
            ((LINEPayRequest) ((LINEPayRequest.Builder) ((LINEPayRequest.Builder) ((LINEPayRequest.Builder) ((LINEPayRequest.Builder) ((LINEPayRequest.Builder) ((LINEPayRequest.Builder) ((LINEPayRequest.Builder) ((LINEPayRequest.Builder) MolCoreSdk.buildLINEPayRequest(this.mContext, new LINEPayRequestWrapper(pUserId, pPrice, pMOLSEAPlusListener)).serviceId(this.mServiceId)).secretKey(this.mSecretKey)).userId(pUserId)).orderId(pOrderId)).price(pPrice, pCurrency)).content(pContent)).waitingTime((long) pWaitTime)).language(this.mLanguage)).build()).request();
            return;
        }
        Log.w("Mol-SDK", "APP LINE not install send to Play Store APP LINE");
        checkAPPLINE.launchUri("jp.naver.line.android", this.mContext);
    }

    class PrepaidCardVerifyApiRequestWrapper extends MolRequestWrapper implements OnPrepaidCardVerifyListener {
        public PrepaidCardVerifyApiRequestWrapper(MOLSEAPlusListener pMOLSEAPlusListener) {
            super(PaymentChannel.PrepaidCard, pMOLSEAPlusListener);
        }

        public void onPrepaidCardVerifyRequestSuccess(PrepaidCardApiResponse pPrepaidCardApiResponse) {
            updateSuccess(new ResultHolder.Builder().transactionId(pPrepaidCardApiResponse.getOrderId()).partnerTransactionId(pPrepaidCardApiResponse.getRefId()).build());
        }
    }

    class PrepaidCardWebViewRequestWrapper extends MolRequestWrapper implements OnPrepaidCardRequestListener {
        public PrepaidCardWebViewRequestWrapper(MOLSEAPlusListener pMOLSEAPlusListener) {
            super(PaymentChannel.PrepaidCard, pMOLSEAPlusListener);
        }

        public void onPrepaidCardRequestSuccess(PrepaidCardResponse pPrepaidCardResponse) {
            updateSuccess(new ResultHolder.Builder().transactionId(pPrepaidCardResponse.getOrderId()).partnerTransactionId(pPrepaidCardResponse.getTransactionId()).build());
        }
    }

    class PSMSRequestWrapper extends MolRequestWrapper implements OnSmsBillingPurchaseRequestListener, OnUPointRequestListener {
        private PSMSResponse mPsmsResponse;

        public PSMSRequestWrapper(PSMSResponse pPsmsResponse, MOLSEAPlusListener pMOLSEAPlusListener) {
            super(PaymentChannel.PSMS, pMOLSEAPlusListener);
            this.mPsmsResponse = pPsmsResponse;
        }

        public void onSmsBillingPurchaseRequestSuccess(SmsBillingApiResponse pSmsBillingApiResponse) {
            onRequetSuccess(pSmsBillingApiResponse.getTransactionId());
        }

        public void onRequetSuccess(String pTransactionId) {
            this.mPsmsResponse.put(PSMSResponse.TRANSACTION_ID, pTransactionId);
            updateSuccess(new ResultHolder.Builder().transactionId(pTransactionId).partnerTransactionId(this.mPsmsResponse.getPartnerTransactionId()).userId(this.mPsmsResponse.getUserId()).priceId(this.mPsmsResponse.getPriceId()).build());
        }
    }

    class MyOnSmsBillingPurchaseRequestListener implements OnSmsBillingPurchaseRequestListener {
        private OnSmsBillingPurchaseRequestListener mOnSmsBillingPurchaseRequestListener;

        public MyOnSmsBillingPurchaseRequestListener(OnSmsBillingPurchaseRequestListener pOnSmsBillingPurchaseRequestListener) {
            this.mOnSmsBillingPurchaseRequestListener = pOnSmsBillingPurchaseRequestListener;
        }

        private void clear() {
            SmsBillingPurchaseRequest unused = MOLSEAPlus.this.mSmsBillingPurchaseRequest = null;
        }

        public void onSmsBillingPurchaseRequestSuccess(SmsBillingApiResponse pSmsBillingApiResponse) {
            clear();
            if (this.mOnSmsBillingPurchaseRequestListener != null) {
                this.mOnSmsBillingPurchaseRequestListener.onSmsBillingPurchaseRequestSuccess(pSmsBillingApiResponse);
            }
        }

        public void onUserCancel() {
            clear();
            if (this.mOnSmsBillingPurchaseRequestListener != null) {
                this.mOnSmsBillingPurchaseRequestListener.onUserCancel();
            }
        }

        public void onRequestEvent(int pEventCode) {
            clear();
            if (this.mOnSmsBillingPurchaseRequestListener != null) {
                this.mOnSmsBillingPurchaseRequestListener.onRequestEvent(pEventCode);
            }
        }

        public void onRequestError(int errorCode, String error) {
            clear();
            if (this.mOnSmsBillingPurchaseRequestListener != null) {
                this.mOnSmsBillingPurchaseRequestListener.onRequestError(errorCode, error);
            }
        }
    }

    class DCBRequestWrapper extends MolRequestWrapper implements OnDCBRequestListener {
        private ResultHolder.Builder mResultBuilder;

        public DCBRequestWrapper(String pUserId, String pPriceId, MOLSEAPlusListener pMOLSEAPlusListener) {
            super(PaymentChannel.DCB, pMOLSEAPlusListener);
            this.mResultBuilder = new ResultHolder.Builder().userId(pUserId).priceId(pPriceId);
        }

        public void onDCBRequestSuccess(DCBResponse pDcbResponse) {
            this.mResultBuilder.transactionId(pDcbResponse.getTransactionId()).partnerTransactionId(pDcbResponse.getOrderId());
            updateSuccess(this.mResultBuilder.build());
        }
    }

    class WebAPISMSRequestWrapper extends MolRequestWrapper implements OnWebAPISMSRequestListener {
        private ResultHolder.Builder mResultBuilder;

        public WebAPISMSRequestWrapper(String pUserId, String pPriceId, MOLSEAPlusListener pMOLSEAPlusListener) {
            super(PaymentChannel.WebAPISMS, pMOLSEAPlusListener);
            this.mResultBuilder = new ResultHolder.Builder().userId(pUserId).priceId(pPriceId);
        }

        public void onWebAPISMSRequestSuccess(WebAPISMSResponse pWSMSResponse) {
            this.mResultBuilder.transactionId(pWSMSResponse.getTransactionId()).partnerTransactionId(pWSMSResponse.getOrderId());
            updateSuccess(this.mResultBuilder.build());
        }
    }

    class MPayRequestWrapper extends MolRequestWrapper implements OnMPayRequestListener {
        private ResultHolder.Builder mResultBuilder;

        public MPayRequestWrapper(String pUserId, String pPriceId, MOLSEAPlusListener pMOLSEAPlusListener) {
            super(PaymentChannel.MPAY, pMOLSEAPlusListener);
            this.mResultBuilder = new ResultHolder.Builder().userId(pUserId).priceId(pPriceId);
        }

        public void onMPayRequestSuccess(MPayResponse pMpayResponse) {
            this.mResultBuilder.transactionId(pMpayResponse.getTransactionId()).partnerTransactionId(pMpayResponse.getOrderId());
            updateSuccess(this.mResultBuilder.build());
        }
    }

    class LINEPayRequestWrapper extends MolRequestWrapper implements OnLINEPayRequestListener {
        private ResultHolder.Builder mResultBuilder;

        public LINEPayRequestWrapper(String pUserId, String pPriceId, MOLSEAPlusListener pMOLSEAPlusListener) {
            super(PaymentChannel.LINEPAY, pMOLSEAPlusListener);
            this.mResultBuilder = new ResultHolder.Builder().userId(pUserId).priceId(pPriceId);
        }

        public void onLINEPayRequestSuccess(LINEPayResponse pLINEpayResponse) {
            this.mResultBuilder.transactionId(pLINEpayResponse.getTransactionId()).partnerTransactionId(pLINEpayResponse.getOrderId());
            updateSuccess(this.mResultBuilder.build());
        }
    }

    class MolRequestWrapper implements IMolRequest.BaseRequestListener {
        private MOLSEAPlusListener mMOLSEAPlusListener;
        private PaymentChannel mPaymentChannel;

        public MolRequestWrapper(PaymentChannel pPaymentChannel, MOLSEAPlusListener pMOLSEAPlusListener) {
            this.mPaymentChannel = pPaymentChannel;
            this.mMOLSEAPlusListener = pMOLSEAPlusListener;
        }

        /* access modifiers changed from: protected */
        public void updateSuccess(ResultHolder pResultHolder) {
            if (this.mMOLSEAPlusListener != null) {
                this.mMOLSEAPlusListener.onRequestSuccess(this.mPaymentChannel, pResultHolder);
            }
        }

        public void onUserCancel() {
            if (this.mMOLSEAPlusListener != null) {
                this.mMOLSEAPlusListener.onUserCancel();
            }
        }

        public void onRequestEvent(int pEventCode) {
            if (this.mMOLSEAPlusListener != null) {
                this.mMOLSEAPlusListener.onRequestEvent(pEventCode);
            }
        }

        public void onRequestError(int errorCode, String error) {
            if (this.mMOLSEAPlusListener != null) {
                this.mMOLSEAPlusListener.onRequestError(errorCode, error);
            }
        }
    }
}
