package com.mol.seaplus.sdk.upoint;

import com.mol.seaplus.base.sdk.IMolRequest;

public interface OnUPointRequestListener extends IMolRequest.BaseRequestListener {
    void onRequetSuccess(String str);
}
