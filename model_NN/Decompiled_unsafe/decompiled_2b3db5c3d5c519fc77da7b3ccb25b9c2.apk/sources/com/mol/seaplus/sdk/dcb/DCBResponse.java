package com.mol.seaplus.sdk.dcb;

import com.mol.seaplus.tool.datareader.data.IDataHolder;
import com.mol.seaplus.tool.datareader.data.impl.DataHolder;

public class DCBResponse extends DCBApiResponse {
    private static final String MSISDN = "msisdn";
    private static final String OPERATOR = "operator";

    public /* bridge */ /* synthetic */ String getAmount() {
        return super.getAmount();
    }

    public /* bridge */ /* synthetic */ String getChannel() {
        return super.getChannel();
    }

    public /* bridge */ /* synthetic */ IDataHolder getChildHolderByTag(String str) {
        return super.getChildHolderByTag(str);
    }

    public /* bridge */ /* synthetic */ String getContentName() {
        return super.getContentName();
    }

    public /* bridge */ /* synthetic */ String getCurrency() {
        return super.getCurrency();
    }

    public /* bridge */ /* synthetic */ String getOrderId() {
        return super.getOrderId();
    }

    public /* bridge */ /* synthetic */ String getTransactionId() {
        return super.getTransactionId();
    }

    public /* bridge */ /* synthetic */ String getUserId() {
        return super.getUserId();
    }

    public DCBResponse() {
        injectKeys(OPERATOR, MSISDN);
    }

    public String getOperator() {
        return getString(OPERATOR);
    }

    public String getMsisdn() {
        return getString(MSISDN);
    }

    public DataHolder initDataHolder() {
        return new DCBResponse();
    }
}
