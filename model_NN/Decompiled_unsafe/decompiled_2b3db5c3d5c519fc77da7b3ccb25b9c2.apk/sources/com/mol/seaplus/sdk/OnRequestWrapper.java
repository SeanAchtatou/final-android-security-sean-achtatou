package com.mol.seaplus.sdk;

import com.mol.seaplus.base.sdk.IMolRequest;
import com.mol.seaplus.base.sdk.IMolRequest.BaseRequestListener;
import org.json.JSONObject;

abstract class OnRequestWrapper<T extends IMolRequest.BaseRequestListener> implements IMolRequest.OnRequestListener {
    private T mOnRequestListener;

    /* access modifiers changed from: protected */
    public abstract void onWrapResult(IMolRequest.BaseRequestListener baseRequestListener, JSONObject jSONObject);

    public OnRequestWrapper(T pOnRequestListener) {
        this.mOnRequestListener = pOnRequestListener;
    }

    public void onRequestSuccess(JSONObject result) {
        if (this.mOnRequestListener != null) {
            onWrapResult(this.mOnRequestListener, result);
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onRequestEvent(int r2) {
        /*
            r1 = this;
            T r0 = r1.mOnRequestListener
            if (r0 == 0) goto L_0x0009
            T r0 = r1.mOnRequestListener
            r0.onRequestEvent(r2)
        L_0x0009:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mol.seaplus.sdk.OnRequestWrapper.onRequestEvent(int):void");
    }

    public void onUserCancel() {
        if (this.mOnRequestListener != null) {
            this.mOnRequestListener.onUserCancel();
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onRequestError(int r2, java.lang.String r3) {
        /*
            r1 = this;
            T r0 = r1.mOnRequestListener
            if (r0 == 0) goto L_0x0009
            T r0 = r1.mOnRequestListener
            r0.onRequestError(r2, r3)
        L_0x0009:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mol.seaplus.sdk.OnRequestWrapper.onRequestError(int, java.lang.String):void");
    }
}
