package com.mol.seaplus.sdk.webapisms;

import com.mol.seaplus.sdk.dcb.DCBResponse;

interface OnWebAPISMSInquiryApiCallbackListener {
    void onWebAPISMSResultInquiryApiError(int i, String str);

    void onWebAPISMSResultInquiryApiSuccess(DCBResponse dCBResponse);
}
