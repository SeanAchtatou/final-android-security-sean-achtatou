package com.mol.seaplus.sdk.upoint;

import com.mol.seaplus.tool.datareader.data.IDataHolder;
import com.mol.seaplus.tool.datareader.data.impl.DataHolder;

public class UPointResponse extends DataHolder {
    private static final String[] KEYS = {PARTNER_TRANSACTION_ID, TRANSACTION_ID, SMS_CODE, SHORT_CODE};
    private static final String PARTNER_TRANSACTION_ID = "p_txid";
    private static final String SHORT_CODE = "shortcode";
    private static final String SMS_CODE = "sms_code";
    private static final String TRANSACTION_ID = "trans_id";

    public UPointResponse() {
        super("response", KEYS);
    }

    public String getPartnerTransactionId() {
        return getString(PARTNER_TRANSACTION_ID);
    }

    public String getTransactionId() {
        return getString(TRANSACTION_ID);
    }

    public String getSmsCode() {
        return getString(SMS_CODE);
    }

    public String getShortCode() {
        return getString(SHORT_CODE);
    }

    public DataHolder initDataHolder() {
        return new UPointResponse();
    }

    public IDataHolder getChildHolderByTag(String s) {
        return null;
    }
}
