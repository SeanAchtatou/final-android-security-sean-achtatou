package com.mol.seaplus.sdk;

import android.content.Context;
import com.mol.seaplus.base.sdk.IMolRequest;
import com.mol.seaplus.base.sdk.Sdk;
import com.mol.seaplus.base.sdk.impl.MolApiCallback;
import com.mol.seaplus.dcb.sdk.DCBRequest;
import com.mol.seaplus.linepay.sdk.LINEPayRequest;
import com.mol.seaplus.mpay.sdk.MPayRequest;
import com.mol.seaplus.prepaidcard.sdk.PrepaidCard;
import com.mol.seaplus.prepaidcard.sdk.PrepaidCardValidateApiRequest;
import com.mol.seaplus.prepaidcard.sdk.PrepaidCardVerifyRequest;
import com.mol.seaplus.prepaidcard.sdk2.PrepaidCardRequest;
import com.mol.seaplus.sdk.dcb.DCBResponse;
import com.mol.seaplus.sdk.dcb.OnDCBRequestListener;
import com.mol.seaplus.sdk.linepay.LINEPayResponse;
import com.mol.seaplus.sdk.linepay.OnLINEPayRequestListener;
import com.mol.seaplus.sdk.mpay.MPayResponse;
import com.mol.seaplus.sdk.mpay.OnMPayRequestListener;
import com.mol.seaplus.sdk.prepaidcard.OnPrepaidCardRequestListener;
import com.mol.seaplus.sdk.prepaidcard.OnPrepaidCardValidateListener;
import com.mol.seaplus.sdk.prepaidcard.OnPrepaidCardVerifyListener;
import com.mol.seaplus.sdk.prepaidcard.PrepaidCardApiResponse;
import com.mol.seaplus.sdk.prepaidcard.PrepaidCardResponse;
import com.mol.seaplus.sdk.smsbilling.OnSmsBillingPurchaseRequestListener;
import com.mol.seaplus.sdk.smsbilling.SmsBillingApiResponse;
import com.mol.seaplus.sdk.upoint.OnUPointRequestListener;
import com.mol.seaplus.sdk.upoint.UPointResponse;
import com.mol.seaplus.sdk.webapisms.OnWebAPISMSRequestListener;
import com.mol.seaplus.sdk.webapisms.WebAPISMSResponse;
import com.mol.seaplus.smsbilling.sdk.SmsBillingPurchaseRequest;
import com.mol.seaplus.tool.datareader.data.IDataReaderOption;
import com.mol.seaplus.tool.datareader.data.impl.DataHolder;
import com.mol.seaplus.tool.datareader.data.impl.DataReaderOption;
import com.mol.seaplus.tool.datareader.data.impl.JSONDataReader;
import com.mol.seaplus.upoint.sdk.UPointRequest;
import com.mol.seaplus.webapisms.sdk.WebAPISMSRequest;
import org.json.JSONException;
import org.json.JSONObject;

final class MolCoreSdk extends Sdk {
    MolCoreSdk() {
    }

    public static final DCBRequest.Builder buildDCBRequest(Context pContext) {
        return buildDCBRequest(pContext, null);
    }

    public static final DCBRequest.Builder buildDCBRequest(Context pContext, OnDCBRequestListener pOnDCBRequestListener) {
        return (DCBRequest.Builder) new DCBRequest.Builder(pContext).setOnRequestListener((IMolRequest.OnRequestListener) new DCBRequestCallbackWrapper(new DCBResponse(), pOnDCBRequestListener));
    }

    public static final WebAPISMSRequest.Builder buildWebAPISMSRequest(Context pContext) {
        return buildWebAPISMSRequest(pContext, null);
    }

    public static final WebAPISMSRequest.Builder buildWebAPISMSRequest(Context pContext, OnWebAPISMSRequestListener pOnWebAPISMSRequestListener) {
        return (WebAPISMSRequest.Builder) new WebAPISMSRequest.Builder(pContext).setOnRequestListener((IMolRequest.OnRequestListener) new WebAPISMSRequestCallbackWrapper(new WebAPISMSResponse(), pOnWebAPISMSRequestListener));
    }

    public static final MPayRequest.Builder buildMPayRequest(Context pContext) {
        return buildMPayRequest(pContext, null);
    }

    public static final MPayRequest.Builder buildMPayRequest(Context pContext, OnMPayRequestListener pOnMPayRequestListener) {
        return (MPayRequest.Builder) new MPayRequest.Builder(pContext).setOnRequestListener((IMolRequest.OnRequestListener) new MPayRequestCallbackWrapper(new MPayResponse(), pOnMPayRequestListener));
    }

    public static final LINEPayRequest.Builder buildLINEPayRequest(Context pContext) {
        return buildLINEPayRequest(pContext, null);
    }

    public static final LINEPayRequest.Builder buildLINEPayRequest(Context pContext, OnLINEPayRequestListener pOnLINEPayRequestListener) {
        return (LINEPayRequest.Builder) new LINEPayRequest.Builder(pContext).setOnRequestListener((IMolRequest.OnRequestListener) new LINEPayRequestCallbackWrapper(new LINEPayResponse(), pOnLINEPayRequestListener));
    }

    public static final UPointRequest.Builder buildUPointRequest(Context pContext) {
        return buildUPointRequest(pContext, null);
    }

    public static final UPointRequest.Builder buildUPointRequest(Context pContext, OnUPointRequestListener pOnUPointRequestListener) {
        return new UPointRequest.Builder(pContext).setOnRequestListener(new UPointRequestCallbackWrapper(pOnUPointRequestListener));
    }

    public static final SmsBillingPurchaseRequest.Builder buildSmsBillingPurchaseRequest(Context pContext) {
        return buildSmsBillingPurchaseRequest(pContext, null, null, null, null);
    }

    public static final SmsBillingPurchaseRequest.Builder buildSmsBillingPurchaseRequest(Context pContext, OnSmsBillingPurchaseRequestListener pOnSmsBillingPurchaseRequestListener) {
        return buildSmsBillingPurchaseRequest(pContext, null, null, null, pOnSmsBillingPurchaseRequestListener);
    }

    public static final SmsBillingPurchaseRequest.Builder buildSmsBillingPurchaseRequest(Context pContext, String pPartnerTransactionId, String pUserId, String pPriceId, OnSmsBillingPurchaseRequestListener pOnSmsBillingPurchaseRequestListener) {
        return (SmsBillingPurchaseRequest.Builder) ((SmsBillingPurchaseRequest.Builder) ((SmsBillingPurchaseRequest.Builder) new SmsBillingPurchaseRequest.Builder(pContext).partnerTransactionId(pPartnerTransactionId)).userId(pUserId)).priceId(pPriceId).setOnRequestListener(new OnRequestWrapper<OnSmsBillingPurchaseRequestListener>(pOnSmsBillingPurchaseRequestListener) {
            /* access modifiers changed from: protected */
            public void onWrapResult(OnSmsBillingPurchaseRequestListener onSmsBillingPurchaseRequestListener, JSONObject result) {
                onSmsBillingPurchaseRequestListener.onSmsBillingPurchaseRequestSuccess((SmsBillingApiResponse) MolCoreSdk.wrapResult(result, new SmsBillingApiResponse()));
            }
        });
    }

    public static final PrepaidCardRequest.Builder buildPrepaidCardRequest(Context pContext) {
        return buildPrepaidCardRequest(pContext, null);
    }

    public static final PrepaidCardRequest.Builder buildPrepaidCardRequest(Context pContext, OnPrepaidCardRequestListener pOnPrepaidCardRequestListener) {
        return (PrepaidCardRequest.Builder) new PrepaidCardRequest.Builder(pContext).setOnRequestListener((IMolRequest.OnRequestListener) new PrepaidCardRequestCallbackWrapper(pOnPrepaidCardRequestListener));
    }

    public static final PrepaidCardValidateApiRequest.Builder buildPrepaidCardValidateApiRequest(Context pContext, OnPrepaidCardValidateListener pOnPrepaidCardApiCallbackListener) {
        return (PrepaidCardValidateApiRequest.Builder) new PrepaidCardValidateApiRequest.Builder(pContext).callback(new PrepaidCardApiCallbackWrapper(pOnPrepaidCardApiCallbackListener));
    }

    public static final PrepaidCardVerifyRequest.Builder buildPrepaidCardVerifyRequest(Context pContext) {
        return buildPrepaidCardVerifyRequest(pContext, null, null, null, null);
    }

    public static final PrepaidCardVerifyRequest.Builder buildPrepaidCardVerifyRequest(Context pContext, OnPrepaidCardVerifyListener pOnPrepaidCardVerifyListener) {
        return buildPrepaidCardVerifyRequest(pContext, null, null, null, pOnPrepaidCardVerifyListener);
    }

    public static final PrepaidCardVerifyRequest.Builder buildPrepaidCardVerifyRequest(Context pContext, PrepaidCard pPrepaidCard, String pRefId, String pCallbackUrl, OnPrepaidCardVerifyListener pOnPrepaidCardVerifyListener) {
        return new PrepaidCardVerifyRequest.Builder(pContext).forCard(pPrepaidCard).refId(pRefId).backUrl(pCallbackUrl).setOnRequestListener((IMolRequest.OnRequestListener) new OnRequestWrapper<OnPrepaidCardVerifyListener>(pOnPrepaidCardVerifyListener) {
            /* access modifiers changed from: protected */
            public void onWrapResult(OnPrepaidCardVerifyListener onPrepaidCardVerifyListener, JSONObject result) {
                onPrepaidCardVerifyListener.onPrepaidCardVerifyRequestSuccess((PrepaidCardApiResponse) MolCoreSdk.wrapResult(result, new PrepaidCardApiResponse()));
            }
        });
    }

    protected static final <T extends DataHolder> T wrapResult(JSONObject pResult, T t) {
        JSONObject data = new JSONObject();
        try {
            data.put(t.getTagName(), pResult);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        IDataReaderOption<T> dataReaderOption = new DataReaderOption();
        dataReaderOption.setDataHolder(t);
        JSONDataReader<T> jsonDataReader = new JSONDataReader<>(dataReaderOption);
        jsonDataReader.extractJSONObject(data);
        return (DataHolder) jsonDataReader.getAllData().get(0);
    }

    private static final class PrepaidCardRequestCallbackWrapper extends OnRequestWrapper<OnPrepaidCardRequestListener> {
        public PrepaidCardRequestCallbackWrapper(OnPrepaidCardRequestListener pOnRequestListener) {
            super(pOnRequestListener);
        }

        /* access modifiers changed from: protected */
        public void onWrapResult(OnPrepaidCardRequestListener pOnPrepaidCardRequestListener, JSONObject result) {
            pOnPrepaidCardRequestListener.onPrepaidCardRequestSuccess((PrepaidCardResponse) MolCoreSdk.wrapResult(result, new PrepaidCardResponse()));
        }
    }

    private static final class PrepaidCardApiCallbackWrapper implements MolApiCallback {
        private OnPrepaidCardValidateListener mOnPrepaidCardApiListener;

        public PrepaidCardApiCallbackWrapper(OnPrepaidCardValidateListener pOnPrepaidCardApiListener) {
            this.mOnPrepaidCardApiListener = pOnPrepaidCardApiListener;
        }

        public void onApiSuccess(JSONObject pResult) {
            if (this.mOnPrepaidCardApiListener != null) {
                this.mOnPrepaidCardApiListener.onPrepaidCardApiSuccess((PrepaidCardApiResponse) MolCoreSdk.wrapResult(pResult, new PrepaidCardApiResponse()));
            }
        }

        public void onApiError(int pErrorCode, String pError) {
            if (this.mOnPrepaidCardApiListener != null) {
                this.mOnPrepaidCardApiListener.onPrepaidCardApiError(pErrorCode, pError);
            }
        }
    }

    private static final class UPointRequestCallbackWrapper extends OnRequestWrapper<OnUPointRequestListener> {
        public UPointRequestCallbackWrapper(OnUPointRequestListener pOnUPointRequestListener) {
            super(pOnUPointRequestListener);
        }

        /* access modifiers changed from: protected */
        public void onWrapResult(OnUPointRequestListener onUPointRequestListener, JSONObject result) {
            onUPointRequestListener.onRequetSuccess(((UPointResponse) MolCoreSdk.wrapResult(result, new UPointResponse())).getPartnerTransactionId());
        }
    }

    private static final class DCBRequestCallbackWrapper extends OnRequestWrapper<OnDCBRequestListener> {
        private DCBResponse mDcbResponse;

        public DCBRequestCallbackWrapper(DCBResponse pDcbResponse, OnDCBRequestListener pOnDCBRequestListener) {
            super(pOnDCBRequestListener);
            this.mDcbResponse = pDcbResponse;
        }

        /* access modifiers changed from: protected */
        public void onWrapResult(OnDCBRequestListener pOnDCBRequestListener, JSONObject result) {
            pOnDCBRequestListener.onDCBRequestSuccess((DCBResponse) MolCoreSdk.wrapResult(result, this.mDcbResponse));
        }
    }

    private static final class WebAPISMSRequestCallbackWrapper extends OnRequestWrapper<OnWebAPISMSRequestListener> {
        private WebAPISMSResponse mwsmsResponse;

        public WebAPISMSRequestCallbackWrapper(WebAPISMSResponse pwsmsResponse, OnWebAPISMSRequestListener pOnwsmsRequestListener) {
            super(pOnwsmsRequestListener);
            this.mwsmsResponse = pwsmsResponse;
        }

        /* access modifiers changed from: protected */
        public void onWrapResult(OnWebAPISMSRequestListener pOnwsmsRequestListener, JSONObject result) {
            pOnwsmsRequestListener.onWebAPISMSRequestSuccess((WebAPISMSResponse) MolCoreSdk.wrapResult(result, this.mwsmsResponse));
        }
    }

    private static final class MPayRequestCallbackWrapper extends OnRequestWrapper<OnMPayRequestListener> {
        private MPayResponse mmpayResponse;

        public MPayRequestCallbackWrapper(MPayResponse pmpayResponse, OnMPayRequestListener pOnmpayRequestListener) {
            super(pOnmpayRequestListener);
            this.mmpayResponse = pmpayResponse;
        }

        /* access modifiers changed from: protected */
        public void onWrapResult(OnMPayRequestListener pOnmpayRequestListener, JSONObject result) {
            pOnmpayRequestListener.onMPayRequestSuccess((MPayResponse) MolCoreSdk.wrapResult(result, this.mmpayResponse));
        }
    }

    private static final class LINEPayRequestCallbackWrapper extends OnRequestWrapper<OnLINEPayRequestListener> {
        private LINEPayResponse mlinepayResponse;

        public LINEPayRequestCallbackWrapper(LINEPayResponse plinepayResponse, OnLINEPayRequestListener pOnlinepayRequestListener) {
            super(pOnlinepayRequestListener);
            this.mlinepayResponse = plinepayResponse;
        }

        /* access modifiers changed from: protected */
        public void onWrapResult(OnLINEPayRequestListener pOnLINEpayRequestListener, JSONObject result) {
            pOnLINEpayRequestListener.onLINEPayRequestSuccess((LINEPayResponse) MolCoreSdk.wrapResult(result, this.mlinepayResponse));
        }
    }
}
