package com.mol.seaplus.sdk.psms;

import com.mol.seaplus.tool.datareader.data.IDataHolder;
import com.mol.seaplus.tool.datareader.data.impl.DataHolder;

public class PSMSResponse extends DataHolder {
    public static final String ITEM = "item";
    private static final String[] KEYS = {PARTNER_TRANSACTION_ID, TRANSACTION_ID, USER_ID, PRICE_ID, ITEM};
    public static final String PARTNER_TRANSACTION_ID = "pTxId";
    public static final String PRICE_ID = "priceId";
    public static final String TRANSACTION_ID = "txId";
    public static final String USER_ID = "userId";

    public PSMSResponse() {
        super("result", KEYS);
    }

    public String getPartnerTransactionId() {
        return getString(PARTNER_TRANSACTION_ID);
    }

    public String getTransactionId() {
        return getString(TRANSACTION_ID);
    }

    public String getUserId() {
        return getString(USER_ID);
    }

    public String getPriceId() {
        return getString(PRICE_ID);
    }

    public String getItem() {
        return getString(ITEM);
    }

    public DataHolder initDataHolder() {
        return new PSMSResponse();
    }

    public IDataHolder getChildHolderByTag(String tag) {
        return null;
    }
}
