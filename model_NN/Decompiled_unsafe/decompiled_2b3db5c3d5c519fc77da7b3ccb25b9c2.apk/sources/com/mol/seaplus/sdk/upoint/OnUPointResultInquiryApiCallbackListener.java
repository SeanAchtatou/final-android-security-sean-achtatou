package com.mol.seaplus.sdk.upoint;

interface OnUPointResultInquiryApiCallbackListener {
    void onResultInquiryApiFail(int i, String str);

    void onResultInquiryApiSuccess(String str);

    void onResultInquiryApiWaitingFromUPoint(String str);
}
