package com.mol.seaplus.sdk.prepaidcard;

import com.mol.seaplus.base.sdk.IMolRequest;

public interface OnPrepaidCardRequestListener extends IMolRequest.BaseRequestListener {
    void onPrepaidCardRequestSuccess(PrepaidCardResponse prepaidCardResponse);
}
