package com.mol.seaplus.sdk.linepay;

import com.mol.seaplus.sdk.mpay.MPayApiResponse;
import com.mol.seaplus.tool.datareader.data.impl.DataHolder;

public class LINEPayResponse extends MPayApiResponse {
    private static final String MSISDN = "msisdn";
    private static final String OPERATOR = "operator";

    public LINEPayResponse() {
        injectKeys(OPERATOR, MSISDN);
    }

    public String getOperator() {
        return getString(OPERATOR);
    }

    public String getMsisdn() {
        return getString(MSISDN);
    }

    public DataHolder initDataHolder() {
        return new LINEPayApiResponse();
    }
}
