package com.mol.seaplus.sdk.linepay;

import com.mol.seaplus.base.sdk.IMolRequest;

public interface OnLINEPayRequestListener extends IMolRequest.BaseRequestListener {
    void onLINEPayRequestSuccess(LINEPayResponse lINEPayResponse);
}
