package com.mol.seaplus.sdk.mpay;

import com.mol.seaplus.tool.datareader.data.impl.DataHolder;

public class MPayResponse extends MPayApiResponse {
    private static final String MSISDN = "msisdn";
    private static final String OPERATOR = "operator";

    public MPayResponse() {
        injectKeys(OPERATOR, MSISDN);
    }

    public String getOperator() {
        return getString(OPERATOR);
    }

    public String getMsisdn() {
        return getString(MSISDN);
    }

    public DataHolder initDataHolder() {
        return new MPayApiResponse();
    }
}
