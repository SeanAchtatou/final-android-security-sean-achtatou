package com.mol.seaplus.webview.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.mol.seaplus.Currency;
import com.mol.seaplus.base.sdk.IMolRequest;
import com.mol.seaplus.base.sdk.IMolWaitingDialog;
import com.mol.seaplus.base.sdk.impl.BaseMolWaitingDialogPresenter;
import com.mol.seaplus.base.sdk.impl.MolApiCallback;
import com.mol.seaplus.base.sdk.impl.MolRequest;
import com.mol.seaplus.base.sdk.impl.MolWaitingDialog;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.tool.connection.handler.ErrorHandler;
import com.mol.seaplus.tool.dialog.ToolAlertDialog;
import com.mol.seaplus.webview.sdk.WebViewPaymentRequest;
import com.mol.seaplus.webview.sdk.WebViewResultInquiryApiRequest;
import org.json.JSONObject;

public class WebViewRequest extends MolRequest {
    /* access modifiers changed from: private */
    public IMolRequest.OnRequestListener mOnPaymentRequestListener = new IMolRequest.OnRequestListener() {
        public void onRequestSuccess(JSONObject result) {
            MolWaitingDialog molWaitingDialog = new MolWaitingDialog(WebViewRequest.this.getContext());
            molWaitingDialog.setMolDialogPresenter(WebViewRequest.this.mWebViewWaitingDialogPresenter = new WebViewWaitingDialogPresenter(molWaitingDialog));
        }

        public void onUserCancel() {
            WebViewRequest.this.updateUserCancel();
        }

        public void onRequestEvent(int pEventCode) {
            WebViewRequest.this.updateEvent(pEventCode);
        }

        public void onRequestError(int errorCode, String error) {
            WebViewRequest.this.updateError(errorCode, error);
        }
    };
    /* access modifiers changed from: private */
    public long mWaitingTime;
    /* access modifiers changed from: private */
    public WebViewPaymentRequest.Builder mWebViewPaymentBuilder;
    private WebViewPaymentRequest mWebViewPaymentRequest;
    /* access modifiers changed from: private */
    public WebViewResultInquiryApiRequest mWebViewResultInquiryApiRequest;
    /* access modifiers changed from: private */
    public WebViewResultInquiryApiRequest.Builder mWebViewResultInquiryBuilder;
    /* access modifiers changed from: private */
    public WebViewWaitingDialogPresenter mWebViewWaitingDialogPresenter;

    protected WebViewRequest(Context pContext) {
        super(pContext);
    }

    public WebViewRequest onRequest(Context pContext) {
        this.mWebViewPaymentRequest = (WebViewPaymentRequest) this.mWebViewPaymentBuilder.build();
        this.mWebViewPaymentRequest.request();
        return this;
    }

    public void stopRequest() {
        if (this.mWebViewWaitingDialogPresenter != null) {
            this.mWebViewWaitingDialogPresenter.stopWaiting();
        }
        if (this.mWebViewPaymentRequest != null) {
            this.mWebViewPaymentRequest.stopRequest();
        }
        if (this.mWebViewResultInquiryApiRequest != null) {
            this.mWebViewResultInquiryApiRequest.stopRequest();
        }
    }

    /* access modifiers changed from: protected */
    public void onUpdateSuccess(JSONObject pJSONObject) {
        ToolAlertDialog.alert(getContext(), Resources.getString(9));
    }

    /* access modifiers changed from: protected */
    public void onUpdateError(int pErrorCode, String pError) {
        ToolAlertDialog.alert(getContext(), getError(pErrorCode, pError));
    }

    /* access modifiers changed from: protected */
    public void setChannel(Channel pChannel) {
        this.mWebViewPaymentBuilder.channel(pChannel);
    }

    private class WebViewWaitingDialogPresenter extends BaseMolWaitingDialogPresenter implements MolApiCallback {
        public WebViewWaitingDialogPresenter(IMolWaitingDialog pMolWaitingDialog) {
            super(pMolWaitingDialog);
            setMaxWaitingTime(WebViewRequest.this.mWaitingTime);
            WebViewRequest.this.mWebViewResultInquiryBuilder.callback(this);
            pMolWaitingDialog.show();
            startWaiting();
            onInterval();
        }

        /* access modifiers changed from: protected */
        public void onInterval() {
            WebViewResultInquiryApiRequest unused = WebViewRequest.this.mWebViewResultInquiryApiRequest = (WebViewResultInquiryApiRequest) WebViewRequest.this.mWebViewResultInquiryBuilder.build();
            WebViewRequest.this.mWebViewResultInquiryApiRequest.request();
        }

        /* access modifiers changed from: protected */
        public void onDialogTimeout() {
            WebViewRequest.this.updateEvent(203);
        }

        /* access modifiers changed from: protected */
        public void onWaitingTimeout() {
            if (WebViewRequest.this.mWebViewResultInquiryApiRequest != null) {
                WebViewRequest.this.mWebViewResultInquiryApiRequest.stopRequest();
            }
            WebViewRequest.this.updateTimeout();
        }

        public void onApiSuccess(JSONObject pResult) {
            if ("200".equals(pResult.optString("status"))) {
                stopWaiting();
                WebViewRequest.this.updateSuccess(pResult);
                return;
            }
            startInterval();
        }

        public void onApiError(int pErrorCode, String pError) {
            switch (pErrorCode) {
                case 16776960:
                case 16776963:
                    startInterval();
                    return;
                case 16776961:
                case 16776962:
                default:
                    stopWaiting();
                    WebViewRequest.this.updateError(pErrorCode, pError);
                    return;
            }
        }
    }

    protected static abstract class Builder<T extends WebViewRequest, G extends Builder> extends MolRequest.Builder<T, G> {
        protected Channel mChannel;
        private String mContent;
        private Currency mCurrency;
        private String mOrderId;
        private String mPrice;
        private String mSecretKey;
        private String mServiceId;
        private String mUserId;
        private long mWaitingTime = 30000;

        /* access modifiers changed from: protected */
        public abstract T doBuild(Context context);

        public Builder(Context pContext, Channel pChannel) {
            super(pContext);
            this.mChannel = pChannel;
            setErrorHandler((ErrorHandler) new WebViewErrorHandler());
        }

        public G serviceId(String pServiceId) {
            this.mServiceId = pServiceId;
            return this;
        }

        public G secretKey(String pSecretKey) {
            this.mSecretKey = pSecretKey;
            return this;
        }

        public G content(String pContent) {
            this.mContent = pContent;
            return this;
        }

        public G price(String pPrice, Currency pCurrency) {
            this.mPrice = pPrice;
            this.mCurrency = pCurrency;
            return this;
        }

        public G userId(String pUserId) {
            this.mUserId = pUserId;
            return this;
        }

        public G orderId(String pOrderId) {
            this.mOrderId = pOrderId;
            return this;
        }

        public G waitingTime(long pWaitingTime) {
            this.mWaitingTime = pWaitingTime;
            return this;
        }

        public G setErrorHandler(ErrorHandler pErrorHandler) {
            return (Builder) super.setErrorHandler(pErrorHandler);
        }

        public G setOnRequestListener(IMolRequest.OnRequestListener mOnRequestListener) {
            return (Builder) super.setOnRequestListener(mOnRequestListener);
        }

        /* access modifiers changed from: protected */
        public boolean onCheck() {
            return true;
        }

        private boolean check() {
            if (TextUtils.isEmpty(this.mServiceId)) {
                throw new IllegalArgumentException(Resources.getString(77));
            } else if (TextUtils.isEmpty(this.mSecretKey)) {
                throw new IllegalArgumentException(Resources.getString(78));
            } else if (this.mChannel == null) {
                throw new IllegalArgumentException(Resources.getString(83));
            } else if (TextUtils.isEmpty(this.mOrderId)) {
                throw new IllegalArgumentException(Resources.getString(79));
            } else {
                if (!this.mChannel.isCashCard()) {
                    if (TextUtils.isEmpty(this.mContent)) {
                        throw new IllegalArgumentException(Resources.getString(81));
                    } else if (TextUtils.isEmpty(this.mPrice) || this.mCurrency == null) {
                        throw new IllegalArgumentException(Resources.getString(80));
                    }
                }
                if (!TextUtils.isEmpty(this.mUserId)) {
                    return onCheck();
                }
                throw new IllegalArgumentException(Resources.getString(82));
            }
        }

        /* access modifiers changed from: protected */
        public WebViewPaymentRequest.Builder onBuildPayment(Context pContext, Channel pChannel) {
            return new WebViewPaymentRequest.Builder(pContext, pChannel);
        }

        /* access modifiers changed from: protected */
        public WebViewResultInquiryApiRequest.Builder onBuildResultInquiry(Context pContext) {
            return new WebViewResultInquiryApiRequest.Builder(pContext);
        }

        /* access modifiers changed from: protected */
        public T onBuild(Context pContext) {
            if (!check()) {
                return null;
            }
            WebViewRequest t = doBuild(pContext);
            WebViewRequest webViewRequest = t;
            long unused = webViewRequest.mWaitingTime = this.mWaitingTime;
            WebViewPaymentRequest.Builder unused2 = webViewRequest.mWebViewPaymentBuilder = onBuildPayment(pContext, this.mChannel);
            WebViewResultInquiryApiRequest.Builder unused3 = webViewRequest.mWebViewResultInquiryBuilder = onBuildResultInquiry(pContext);
            webViewRequest.mWebViewPaymentBuilder.serviceId(this.mServiceId).secretKey(this.mSecretKey).orderId(this.mOrderId).content(this.mContent).price(this.mPrice, this.mCurrency).userId(this.mUserId).language(getLanguage()).setOnRequestListener(webViewRequest.mOnPaymentRequestListener);
            webViewRequest.mWebViewResultInquiryBuilder.serviceId(this.mServiceId).secretKey(this.mSecretKey).orderId(this.mOrderId).language(getLanguage());
            return t;
        }
    }
}
