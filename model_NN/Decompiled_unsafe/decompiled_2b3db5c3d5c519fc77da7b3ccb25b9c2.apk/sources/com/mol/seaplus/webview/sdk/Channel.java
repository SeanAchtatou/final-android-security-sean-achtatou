package com.mol.seaplus.webview.sdk;

public final class Channel {
    private String mChannel;
    private boolean mIsCashCard;
    private String mName;

    public static final Channel createCashCardChannel(String pName, String pChannel) {
        return new Channel(pName, pChannel, true);
    }

    public static final Channel createChannel(String pName, String pChannel) {
        return new Channel(pName, pChannel, false);
    }

    private Channel(String pName, String pChannel, boolean pIsCashCard) {
        this.mName = pName;
        this.mChannel = pChannel;
        this.mIsCashCard = pIsCashCard;
    }

    public String getName() {
        return this.mName;
    }

    public String getChannel() {
        return this.mChannel;
    }

    public boolean isCashCard() {
        return this.mIsCashCard;
    }

    public String toString() {
        return getName();
    }
}
