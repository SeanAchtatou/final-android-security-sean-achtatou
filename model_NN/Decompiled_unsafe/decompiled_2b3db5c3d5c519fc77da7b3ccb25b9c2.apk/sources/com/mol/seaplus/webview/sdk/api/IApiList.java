package com.mol.seaplus.webview.sdk.api;

public interface IApiList {
    public static final String UNIFIED_INQUIRY_API = "http://wpay.easy2pay.co/inquiry";
    public static final String UNIFIED_PAY_API = "https://sea-sdk.molthailand.com/sdk_server/init.php";
    public static final String UNIFIED_URL = "https://sea-sdk.molthailand.com/sdk_server";
}
