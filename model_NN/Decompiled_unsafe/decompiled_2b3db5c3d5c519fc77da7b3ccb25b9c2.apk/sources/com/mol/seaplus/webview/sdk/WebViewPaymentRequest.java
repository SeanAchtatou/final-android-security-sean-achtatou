package com.mol.seaplus.webview.sdk;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mol.seaplus.Currency;
import com.mol.seaplus.Log;
import com.mol.seaplus.base.sdk.IMolRequest;
import com.mol.seaplus.base.sdk.IMolWebView;
import com.mol.seaplus.base.sdk.MD5Factory;
import com.mol.seaplus.base.sdk.impl.BaseMolWebViewPresenter;
import com.mol.seaplus.base.sdk.impl.MolRequest;
import com.mol.seaplus.base.sdk.impl.MolWebView;
import com.mol.seaplus.base.sdk.impl.MolWebViewDialog;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.tool.connection.handler.ErrorHandler;
import com.mol.seaplus.tool.utils.HttpUtils;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

class WebViewPaymentRequest extends MolRequest {
    private static final String CHANNEL = "channel";
    private static final String FOR = "for";
    private static final String[] KEYS = {FOR, "price", CHANNEL, SERVICE_ID, ORDER_ID, USER_ID, VERSION};
    private static final String ORDER_ID = "orderid";
    private static final String PRICE = "price";
    private static final String SERVICE_ID = "sid";
    private static final String SIGNATURE = "sig";
    private static final String USER_ID = "uid";
    private static final String VERSION = "v";
    /* access modifiers changed from: private */
    public Channel mChannel;
    private Comparator<String> mComparator = new Comparator<String>() {
        public int compare(String l, String r) {
            return l.compareTo(r);
        }
    };
    protected String mContent;
    /* access modifiers changed from: private */
    public Currency mCurrency;
    private MolWebViewDialog mMolWebViewDialog;
    protected String mOrderId;
    protected String mPrice;
    protected String mSecretKey;
    protected String mServiceId;
    protected String mUserId;
    protected String mVersion;

    protected WebViewPaymentRequest(Context pContext) {
        super(pContext);
    }

    public WebViewPaymentRequest onRequest(Context pContext) {
        if (!HttpUtils.isHasNetworkConnection(pContext)) {
            updateError(16776960, Resources.getString(10));
        } else {
            Hashtable<String, String> params = new Hashtable<>();
            if (!TextUtils.isEmpty(this.mContent)) {
                params.put(FOR, this.mContent);
            }
            if (!TextUtils.isEmpty(this.mPrice)) {
                params.put("price", this.mPrice + this.mCurrency.getCurrency());
            }
            params.put(CHANNEL, this.mChannel.getChannel());
            params.put(SERVICE_ID, this.mServiceId);
            params.put(ORDER_ID, this.mOrderId);
            params.put(USER_ID, this.mUserId);
            params.put(VERSION, this.mVersion);
            List<String> keys = Arrays.asList(KEYS);
            Collections.sort(keys, this.mComparator);
            StringBuilder stringBuilder = new StringBuilder();
            for (String key : keys) {
                String value = (String) params.get(key);
                if (!TextUtils.isEmpty(value)) {
                    stringBuilder.append(value);
                }
            }
            params.put(SIGNATURE, MD5Factory.md5(stringBuilder.toString() + this.mSecretKey));
            String api = "https://sea-sdk.molthailand.com/sdk_server/init.php" + HttpUtils.getParam(params, false, true);
            Log.d("api = " + api);
            MolWebView molWebView = new MolWebView(pContext);
            this.mMolWebViewDialog = new MolWebViewDialog(pContext, molWebView);
            molWebView.setMolWebViewPresenter(new MyWebViewPresenter(api, molWebView));
            this.mMolWebViewDialog.setLanguage(getLanguage());
            this.mMolWebViewDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialogInterface) {
                    WebViewPaymentRequest.this.updateUserCancel();
                }
            });
            this.mMolWebViewDialog.show();
        }
        return this;
    }

    public void stopRequest() {
        if (this.mMolWebViewDialog != null) {
            this.mMolWebViewDialog.dismiss(false);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onHandlerCommand(BaseMolWebViewPresenter.CommandHolder pCommandHolder, MolWebViewDialog pMolWebViewDialog) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void updateSuccess(String pTxId, String pPriceId) {
        JSONObject result = new JSONObject();
        try {
            result.put(ORDER_ID, this.mOrderId);
            result.put("txid", pTxId);
            result.put(CHANNEL, this.mChannel.getChannel());
            result.put(USER_ID, this.mUserId);
            if (!TextUtils.isEmpty(this.mContent)) {
                result.put(FOR, this.mContent);
            }
            if (!TextUtils.isEmpty(pPriceId)) {
                result.put("amount", pPriceId);
                result.put(FirebaseAnalytics.Param.CURRENCY, this.mCurrency.getCurrency());
            }
            updateSuccess(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public boolean handlerCommand(BaseMolWebViewPresenter.CommandHolder pCommandHolder) {
        if (onHandlerCommand(pCommandHolder, this.mMolWebViewDialog)) {
            return true;
        }
        switch (pCommandHolder.getCommand()) {
            case Close:
                switch (pCommandHolder.getReturnEvent()) {
                    case OnEvent:
                        this.mMolWebViewDialog.dismiss(true);
                        return true;
                    case OnPurchaseResult:
                        this.mMolWebViewDialog.dismiss(false);
                        updateSuccess(pCommandHolder.getTxId(), this.mPrice);
                        return true;
                    default:
                        this.mMolWebViewDialog.dismiss(false);
                        return true;
                }
            default:
                return false;
        }
    }

    private class MyWebViewPresenter extends BaseMolWebViewPresenter {
        public MyWebViewPresenter(String pUrl, IMolWebView pMolWebView) {
            super(pUrl, pMolWebView);
        }

        /* access modifiers changed from: protected */
        public boolean onHandlerCommand(BaseMolWebViewPresenter.CommandHolder pCommandHolder) {
            return WebViewPaymentRequest.this.handlerCommand(pCommandHolder);
        }
    }

    protected static class Builder<T extends WebViewPaymentRequest, G extends Builder> extends MolRequest.Builder {
        private Channel mChannel;
        private String mContent;
        private Currency mCurrency;
        private String mOrderId;
        private String mPrice;
        private String mSecretKey;
        private String mServiceId;
        private String mUserId;

        public Builder(Context pContext, Channel pChannel) {
            super(pContext);
            channel(pChannel);
        }

        /* access modifiers changed from: protected */
        public G channel(Channel pChannel) {
            this.mChannel = pChannel;
            return this;
        }

        public G serviceId(String pServiceId) {
            this.mServiceId = pServiceId;
            return this;
        }

        public G secretKey(String pSecretKey) {
            this.mSecretKey = pSecretKey;
            return this;
        }

        public G content(String pContent) {
            this.mContent = pContent;
            return this;
        }

        public G price(String pPrice, Currency pCurrency) {
            this.mPrice = pPrice;
            this.mCurrency = pCurrency;
            return this;
        }

        public G userId(String pUserId) {
            this.mUserId = pUserId;
            return this;
        }

        public G orderId(String pOrderId) {
            this.mOrderId = pOrderId;
            return this;
        }

        public G setErrorHandler(ErrorHandler pErrorHandler) {
            return (Builder) super.setErrorHandler(pErrorHandler);
        }

        public G setOnRequestListener(IMolRequest.OnRequestListener mOnRequestListener) {
            return (Builder) super.setOnRequestListener(mOnRequestListener);
        }

        /* access modifiers changed from: protected */
        public boolean onCheck() {
            return true;
        }

        private boolean check() {
            Context context = getContext();
            if (TextUtils.isEmpty(this.mServiceId)) {
                throw new IllegalArgumentException(Resources.getString(77));
            } else if (TextUtils.isEmpty(this.mSecretKey)) {
                throw new IllegalArgumentException(Resources.getString(78));
            } else if (this.mChannel == null) {
                throw new IllegalArgumentException(Resources.getString(83));
            } else if (TextUtils.isEmpty(this.mOrderId)) {
                throw new IllegalArgumentException(Resources.getString(79));
            } else {
                if (!this.mChannel.isCashCard()) {
                    if (TextUtils.isEmpty(this.mContent)) {
                        throw new IllegalArgumentException(Resources.getString(81));
                    } else if (TextUtils.isEmpty(this.mPrice) || this.mCurrency == null) {
                        throw new IllegalArgumentException(Resources.getString(80));
                    }
                }
                if (!TextUtils.isEmpty(this.mUserId)) {
                    return onCheck();
                }
                throw new IllegalArgumentException(Resources.getString(82));
            }
        }

        /* access modifiers changed from: protected */
        public WebViewPaymentRequest doBuild(Context pContext) {
            return new WebViewPaymentRequest(pContext);
        }

        /* access modifiers changed from: protected */
        public MolRequest onBuild(Context pContext) {
            if (!check()) {
                return null;
            }
            WebViewPaymentRequest webViewPaymentRequest = doBuild(pContext);
            webViewPaymentRequest.mServiceId = this.mServiceId;
            webViewPaymentRequest.mSecretKey = this.mSecretKey;
            webViewPaymentRequest.mContent = this.mContent;
            webViewPaymentRequest.mPrice = this.mPrice;
            webViewPaymentRequest.mUserId = this.mUserId;
            webViewPaymentRequest.mOrderId = this.mOrderId;
            Currency unused = webViewPaymentRequest.mCurrency = this.mCurrency;
            Channel unused2 = webViewPaymentRequest.mChannel = this.mChannel;
            webViewPaymentRequest.mVersion = Version.code;
            return webViewPaymentRequest;
        }
    }
}
