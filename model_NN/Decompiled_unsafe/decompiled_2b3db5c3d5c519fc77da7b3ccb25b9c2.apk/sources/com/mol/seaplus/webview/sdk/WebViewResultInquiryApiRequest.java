package com.mol.seaplus.webview.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.mol.seaplus.Log;
import com.mol.seaplus.base.sdk.MD5Factory;
import com.mol.seaplus.base.sdk.impl.MolApiRequest;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.tool.connection.internet.httpurlconnection.HttpURLGetConnection;
import com.mol.seaplus.tool.datareader.data.IDataReader;
import com.mol.seaplus.tool.datareader.data.impl.DataReaderOption;
import com.mol.seaplus.tool.datareader.data.impl.JSONDataReader;
import com.mol.seaplus.tool.utils.HttpUtils;
import com.mol.seaplus.webview.sdk.api.IApiList;
import java.util.Hashtable;

class WebViewResultInquiryApiRequest extends MolApiRequest {
    private static final String TAG = WebViewResultInquiryApiRequest.class.getName();
    /* access modifiers changed from: private */
    public String mOrderId;
    /* access modifiers changed from: private */
    public String mSecretKey;
    /* access modifiers changed from: private */
    public String mServiceId;

    protected WebViewResultInquiryApiRequest(Context context) {
        super(context, TAG);
    }

    /* access modifiers changed from: protected */
    public IDataReader onInitialRequest(Context context) {
        String signature = MD5Factory.md5(this.mOrderId + this.mServiceId + this.mSecretKey);
        Hashtable<String, String> params = new Hashtable<>();
        params.put("orderid", this.mOrderId);
        params.put("sid", this.mServiceId);
        params.put("sig", signature);
        String api = IApiList.UNIFIED_INQUIRY_API + HttpUtils.getParam(params, false, true);
        Log.d("api = " + api);
        return new JSONDataReader(new DataReaderOption(new HttpURLGetConnection(getContext(), api)));
    }

    /* access modifiers changed from: protected */
    public boolean isSuccess(IDataReader pDataReader) {
        switch (getApiStatusCode(pDataReader)) {
            case 200:
            case 601:
            case 602:
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public int getApiStatusCode(IDataReader pDataReader) {
        String status = pDataReader.getTableData("status");
        if (TextUtils.isEmpty(status)) {
            return 0;
        }
        try {
            return Integer.parseInt(status);
        } catch (Exception e) {
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    public String getApiDescription(IDataReader pDataReader) {
        return pDataReader.getTableData("detail");
    }

    protected static class Builder<T extends WebViewResultInquiryApiRequest, G extends Builder> extends MolApiRequest.Builder<G> {
        private String mOrderId;
        private String mSecretKey;
        private String mServiceId;

        public Builder(Context context) {
            super(context);
        }

        public G serviceId(String pServiceId) {
            this.mServiceId = pServiceId;
            return this;
        }

        public G secretKey(String pSecretKey) {
            this.mSecretKey = pSecretKey;
            return this;
        }

        public G orderId(String pOrderId) {
            this.mOrderId = pOrderId;
            return this;
        }

        /* access modifiers changed from: protected */
        public boolean onCheck() {
            return true;
        }

        private boolean check() {
            if (TextUtils.isEmpty(this.mServiceId)) {
                throw new IllegalArgumentException(Resources.getString(77));
            } else if (TextUtils.isEmpty(this.mSecretKey)) {
                throw new IllegalArgumentException(Resources.getString(78));
            } else if (!TextUtils.isEmpty(this.mOrderId)) {
                return onCheck();
            } else {
                throw new IllegalArgumentException(Resources.getString(79));
            }
        }

        /* access modifiers changed from: protected */
        public WebViewResultInquiryApiRequest doBuild(Context pContext) {
            return new WebViewResultInquiryApiRequest(pContext);
        }

        /* access modifiers changed from: protected */
        public T onBuild(Context pContext) {
            if (!check()) {
                return null;
            }
            WebViewResultInquiryApiRequest webViewResultInquiryApiRequest = doBuild(getContext());
            String unused = webViewResultInquiryApiRequest.mServiceId = this.mServiceId;
            String unused2 = webViewResultInquiryApiRequest.mSecretKey = this.mSecretKey;
            String unused3 = webViewResultInquiryApiRequest.mOrderId = this.mOrderId;
            return webViewResultInquiryApiRequest;
        }
    }
}
