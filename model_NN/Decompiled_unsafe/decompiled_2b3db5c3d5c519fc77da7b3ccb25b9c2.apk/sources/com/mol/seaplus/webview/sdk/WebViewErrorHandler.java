package com.mol.seaplus.webview.sdk;

import com.mol.seaplus.CodeList;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.tool.connection.handler.ErrorHandler;

class WebViewErrorHandler implements ErrorHandler {
    WebViewErrorHandler() {
    }

    public String handlerError(int errorCode, String errorMessage) {
        switch (errorCode) {
            case 404:
                return Resources.getString(17);
            case CodeList.ERROR_MT_SUMBIT_INSUFFICIAL_BALLANCE:
                return Resources.getString(18);
            case CodeList.ERROR_USER_REQ_HAS_BEEN_DELAYED:
                return Resources.getString(14);
            case CodeList.ERROR_USER_QUOTA_EXCEED:
                return Resources.getString(13);
            case CodeList.ERROR_GSM_NOT_ALLOWED:
                return Resources.getString(16);
            case CodeList.ERROR_USER_HAS_BEEN_BLOCKED:
                return Resources.getString(12);
            case CodeList.ERROR_INVALID_KEYWORD:
                return Resources.getString(15);
            case CodeList.REQUEST_TIMEOUT:
                return Resources.getString(14);
            case 16776960:
                return Resources.getString(10);
            case 16776961:
            case 16776962:
            case 16776963:
            case 16776964:
            case 16777184:
                return Resources.getString(11);
            default:
                return Resources.getString(19);
        }
    }
}
