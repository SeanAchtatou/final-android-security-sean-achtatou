package com.mol.seaplus.prepaidcard.sdk;

import com.bluepay.pay.PublisherCode;

public final class PrepaidCard {
    public static final PrepaidCard DTACHAPPY_CARD = create("Happy Dtac", "dtachappy");
    public static final PrepaidCard MOLPOINT_CARD = create("MOL Point", "molpoints");
    public static final PrepaidCard ONETWOCALL_CARD = create("AIS 12Call", PublisherCode.PUBLISHER_12CALL);
    public static final PrepaidCard TRUEMONEY_CARD = create("True Money", PublisherCode.PUBLISHER_TRUEMONEY);
    private String mCardName;
    private String mCardParam;

    private static final PrepaidCard create(String pCardName, String pCardParam) {
        return new PrepaidCard(pCardName, pCardParam);
    }

    private PrepaidCard(String pCardName, String pCardParam) {
        this.mCardParam = pCardParam;
        this.mCardName = pCardName;
    }

    public String getCardName() {
        return this.mCardName;
    }

    public String getCardParam() {
        return this.mCardParam;
    }
}
