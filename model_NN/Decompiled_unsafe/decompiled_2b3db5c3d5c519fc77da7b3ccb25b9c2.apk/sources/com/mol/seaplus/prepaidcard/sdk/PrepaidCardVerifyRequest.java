package com.mol.seaplus.prepaidcard.sdk;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import com.mol.seaplus.Language;
import com.mol.seaplus.base.sdk.IMolRequest;
import com.mol.seaplus.base.sdk.impl.MolApiCallback;
import com.mol.seaplus.base.sdk.impl.MolRequest;
import com.mol.seaplus.base.sdk.impl.MolSelectorDialog;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.prepaidcard.sdk.PrepaidCardVerifyDialog;
import com.mol.seaplus.tool.connection.handler.ErrorHandler;
import com.mol.seaplus.tool.dialog.ToolAlertDialog;
import java.util.ArrayList;
import org.json.JSONObject;

public final class PrepaidCardVerifyRequest extends MolRequest {
    private Language mLanguage;
    private MolApiCallback mMolApiCallback = new MolApiCallback() {
        public void onApiSuccess(JSONObject pResult) {
            PrepaidCardVerifyRequest.this.updateSuccess(pResult);
        }

        public void onApiError(int pErrorCode, String pError) {
            PrepaidCardVerifyRequest.this.updateError(pErrorCode, pError);
        }
    };
    private DialogInterface.OnCancelListener mOnCancelListener = new DialogInterface.OnCancelListener() {
        public void onCancel(DialogInterface dialogInterface) {
            PrepaidCardVerifyRequest.this.updateUserCancel();
        }
    };
    /* access modifiers changed from: private */
    public PrepaidCard mPrepaidCard;
    /* access modifiers changed from: private */
    public PrepaidCardVerifyDialog.Builder mPrepaidCardDalogBuilder;

    public PrepaidCardVerifyRequest(Context pContext) {
        super(pContext);
    }

    public PrepaidCardVerifyRequest onRequest(Context pContext) {
        if (this.mPrepaidCard == null) {
            ArrayList<String> channelList = new ArrayList<>();
            channelList.add(PrepaidCard.ONETWOCALL_CARD.getCardParam());
            channelList.add(PrepaidCard.DTACHAPPY_CARD.getCardParam());
            channelList.add(PrepaidCard.TRUEMONEY_CARD.getCardParam());
            channelList.add(PrepaidCard.MOLPOINT_CARD.getCardParam());
            MolSelectorDialog molSelectorDialog = new MolSelectorDialog(getContext(), channelList, new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    switch (position) {
                        case 0:
                            PrepaidCard unused = PrepaidCardVerifyRequest.this.mPrepaidCard = PrepaidCard.ONETWOCALL_CARD;
                            break;
                        case 1:
                            PrepaidCard unused2 = PrepaidCardVerifyRequest.this.mPrepaidCard = PrepaidCard.MOLPOINT_CARD;
                            break;
                        case 2:
                            PrepaidCard unused3 = PrepaidCardVerifyRequest.this.mPrepaidCard = PrepaidCard.DTACHAPPY_CARD;
                            break;
                        case 3:
                            PrepaidCard unused4 = PrepaidCardVerifyRequest.this.mPrepaidCard = PrepaidCard.TRUEMONEY_CARD;
                            break;
                    }
                    PrepaidCardVerifyRequest.this.request();
                }
            });
            molSelectorDialog.setLanguage(getLanguage());
            molSelectorDialog.setOnCancelListener(this.mOnCancelListener);
            molSelectorDialog.show();
        } else {
            PrepaidCardVerifyDialog prepaidCardVerifyDialog = (PrepaidCardVerifyDialog) ((PrepaidCardVerifyDialog.Builder) ((PrepaidCardVerifyDialog.Builder) ((PrepaidCardVerifyDialog.Builder) this.mPrepaidCardDalogBuilder.forCard(this.mPrepaidCard)).callback(this.mMolApiCallback)).language(getLanguage())).build();
            prepaidCardVerifyDialog.setOnCancelListener(this.mOnCancelListener);
            prepaidCardVerifyDialog.show();
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void onUpdateSuccess(JSONObject pJSONObject) {
        ToolAlertDialog.alert(getContext(), Resources.getString(9));
    }

    /* access modifiers changed from: protected */
    public void onUpdateError(int pErrorCode, String pError) {
        ToolAlertDialog.alert(getContext(), getError(pErrorCode, pError));
    }

    public static final class Builder extends MolRequest.Builder<PrepaidCardVerifyRequest, Builder> {
        private PrepaidCard mPrepaidCard;
        private PrepaidCardVerifyDialog.Builder mPrepaidCardDalogBuilder;

        public Builder(Context pContext) {
            super(pContext);
            this.mPrepaidCardDalogBuilder = new PrepaidCardVerifyDialog.Builder(pContext);
        }

        public Builder merchantId(String pMerchantId) {
            this.mPrepaidCardDalogBuilder.merchantId(pMerchantId);
            return this;
        }

        public Builder gameId(String pGameId) {
            this.mPrepaidCardDalogBuilder.gameId(pGameId);
            return this;
        }

        public Builder customerId(String pCustomerId) {
            this.mPrepaidCardDalogBuilder.customerId(pCustomerId);
            return this;
        }

        public Builder secretKey(String pSecretKey) {
            this.mPrepaidCardDalogBuilder.secretKey(pSecretKey);
            return this;
        }

        public Builder forCard(PrepaidCard pPrepaidCard) {
            this.mPrepaidCard = pPrepaidCard;
            this.mPrepaidCardDalogBuilder.forCard(pPrepaidCard);
            return this;
        }

        public Builder refId(String pRefId) {
            this.mPrepaidCardDalogBuilder.refId(pRefId);
            return this;
        }

        public Builder backUrl(String pBackUrl) {
            this.mPrepaidCardDalogBuilder.backUrl(pBackUrl);
            return this;
        }

        public Builder setOnRequestListener(IMolRequest.OnRequestListener mOnRequestListener) {
            return (Builder) super.setOnRequestListener(mOnRequestListener);
        }

        public Builder setErrorHandler(ErrorHandler pErrorHandler) {
            return (Builder) super.setErrorHandler(pErrorHandler);
        }

        /* access modifiers changed from: protected */
        public boolean check() {
            Context context = getContext();
            if (TextUtils.isEmpty(this.mPrepaidCardDalogBuilder.mMerchantId)) {
                throw new IllegalArgumentException(Resources.getString(43));
            } else if (TextUtils.isEmpty(this.mPrepaidCardDalogBuilder.mSecretKey)) {
                throw new IllegalArgumentException(Resources.getString(44));
            } else if (TextUtils.isEmpty(this.mPrepaidCardDalogBuilder.mGameId)) {
                throw new IllegalArgumentException(Resources.getString(45));
            } else if (TextUtils.isEmpty(this.mPrepaidCardDalogBuilder.mCustomerId)) {
                throw new IllegalArgumentException(Resources.getString(46));
            } else if (TextUtils.isEmpty(this.mPrepaidCardDalogBuilder.mRefId)) {
                throw new IllegalArgumentException(Resources.getString(48));
            } else if (!TextUtils.isEmpty(this.mPrepaidCardDalogBuilder.mBackUrl)) {
                return true;
            } else {
                throw new IllegalArgumentException(Resources.getString(49));
            }
        }

        /* access modifiers changed from: protected */
        public PrepaidCardVerifyRequest onBuild(Context pContext) {
            if (!check()) {
                return null;
            }
            PrepaidCardVerifyRequest prepaidCardVerifyRequest = new PrepaidCardVerifyRequest(pContext);
            PrepaidCard unused = prepaidCardVerifyRequest.mPrepaidCard = this.mPrepaidCard;
            PrepaidCardVerifyDialog.Builder unused2 = prepaidCardVerifyRequest.mPrepaidCardDalogBuilder = this.mPrepaidCardDalogBuilder;
            return prepaidCardVerifyRequest;
        }
    }
}
