package com.mol.seaplus.prepaidcard.sdk2;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;
import com.mol.seaplus.base.sdk.IMolRequest;
import com.mol.seaplus.base.sdk.impl.MolSelectorDialog;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.tool.config.OrderCashcard;
import com.mol.seaplus.tool.utils.HttpUtils;
import com.mol.seaplus.webview.sdk.Channel;
import com.mol.seaplus.webview.sdk.WebViewRequest;
import java.util.ArrayList;
import java.util.List;

public class PrepaidCardRequest extends WebViewRequest {
    /* access modifiers changed from: private */
    public static final Channel FAKE_CHANNEL = Channel.createCashCardChannel("fake", "fake");
    /* access modifiers changed from: private */
    public List<PrepaidCard> CARD_LIST;
    private DialogInterface.OnCancelListener mOnCancelListener;
    /* access modifiers changed from: private */
    public PrepaidCard mPrepaidCard;

    private PrepaidCardRequest(Context pContext) {
        super(pContext);
        this.mOnCancelListener = new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                PrepaidCardRequest.this.updateUserCancel();
            }
        };
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    public PrepaidCardRequest onRequest(Context pContext) {
        this.CARD_LIST = new OrderCashcard().getCashcardOrder();
        if (this.mPrepaidCard == null) {
            ArrayList<String> channelList = new ArrayList<>();
            for (PrepaidCard prepaidCard : this.CARD_LIST) {
                channelList.add(prepaidCard.getChannel().getChannel());
            }
            MolSelectorDialog molSelectorDialog = new MolSelectorDialog(getContext(), channelList, new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    PrepaidCard unused = PrepaidCardRequest.this.mPrepaidCard = (PrepaidCard) PrepaidCardRequest.this.CARD_LIST.get(position);
                    IMolRequest unused2 = PrepaidCardRequest.super.request();
                }
            });
            molSelectorDialog.setLanguage(getLanguage());
            molSelectorDialog.setOnCancelListener(this.mOnCancelListener);
            if (!HttpUtils.isHasNetworkConnection(pContext)) {
                updateError(16776960, Resources.getString(10));
                return this;
            }
            molSelectorDialog.show();
            return this;
        }
        setChannel(this.mPrepaidCard.getChannel());
        return (PrepaidCardRequest) super.onRequest(pContext);
    }

    public static final class Builder extends WebViewRequest.Builder<PrepaidCardRequest, Builder> {
        private PrepaidCard mPrepaidCard;

        public Builder(Context pContext) {
            super(pContext, PrepaidCardRequest.FAKE_CHANNEL);
        }

        public Builder forCard(PrepaidCard pPrepaidCard) {
            this.mPrepaidCard = pPrepaidCard;
            return this;
        }

        /* access modifiers changed from: protected */
        public PrepaidCardRequest doBuild(Context pContext) {
            PrepaidCardRequest prepaidCardRequest = new PrepaidCardRequest(pContext);
            PrepaidCard unused = prepaidCardRequest.mPrepaidCard = this.mPrepaidCard;
            return prepaidCardRequest;
        }
    }
}
