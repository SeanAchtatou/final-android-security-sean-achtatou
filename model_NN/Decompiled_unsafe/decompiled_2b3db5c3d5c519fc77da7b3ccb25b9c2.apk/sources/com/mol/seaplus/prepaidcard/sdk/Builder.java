package com.mol.seaplus.prepaidcard.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.mol.seaplus.Language;
import com.mol.seaplus.base.sdk.impl.MolApiCallback;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.prepaidcard.sdk.Builder;
import com.mol.seaplus.tool.connection.handler.ErrorHandler;

abstract class Builder<T, G extends Builder> {
    protected String mBackUrl;
    protected String mCardNo;
    protected Context mContext;
    protected String mCustomerId;
    protected ErrorHandler mErrorHandler;
    protected String mGameId;
    protected Language mLanguage;
    protected String mMerchantId;
    protected MolApiCallback mMolApiCallback;
    protected String mPinNo;
    protected PrepaidCard mPrepaidCard;
    protected String mRefId;
    protected String mSecretKey;
    protected String mSerialNo;

    /* access modifiers changed from: protected */
    public abstract T onBuild();

    public Builder(Context pContext) {
        this.mContext = pContext;
    }

    public G merchantId(String pMerchantId) {
        this.mMerchantId = pMerchantId;
        return this;
    }

    public G gameId(String pGameId) {
        this.mGameId = pGameId;
        return this;
    }

    public G customerId(String pCustomerId) {
        this.mCustomerId = pCustomerId;
        return this;
    }

    public G secretKey(String pSecretKey) {
        this.mSecretKey = pSecretKey;
        return this;
    }

    public G forCard(PrepaidCard pPrepaidCard) {
        this.mPrepaidCard = pPrepaidCard;
        return this;
    }

    public G refId(String pRefId) {
        this.mRefId = pRefId;
        return this;
    }

    public G backUrl(String pBackUrl) {
        this.mBackUrl = pBackUrl;
        return this;
    }

    public G cardNo(String pCardNo) {
        this.mCardNo = pCardNo;
        return this;
    }

    public G serialNo(String pSerialNo) {
        this.mSerialNo = pSerialNo;
        return this;
    }

    public G pinNo(String pPinNo) {
        this.mPinNo = pPinNo;
        return this;
    }

    public G language(Language pLanguage) {
        this.mLanguage = pLanguage;
        return this;
    }

    public G callback(MolApiCallback pMolApiCallback) {
        this.mMolApiCallback = pMolApiCallback;
        return this;
    }

    public G setErrorHandler(ErrorHandler pErrorHandler) {
        this.mErrorHandler = pErrorHandler;
        return this;
    }

    /* access modifiers changed from: protected */
    public boolean onCheck() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean check() {
        if (TextUtils.isEmpty(this.mMerchantId)) {
            throw new IllegalArgumentException(Resources.getString(43));
        } else if (TextUtils.isEmpty(this.mSecretKey)) {
            throw new IllegalArgumentException(Resources.getString(44));
        } else if (TextUtils.isEmpty(this.mGameId)) {
            throw new IllegalArgumentException(Resources.getString(45));
        } else if (TextUtils.isEmpty(this.mCustomerId)) {
            throw new IllegalArgumentException(Resources.getString(46));
        } else if (this.mPrepaidCard == null) {
            throw new IllegalArgumentException(Resources.getString(47));
        } else if (TextUtils.isEmpty(this.mRefId)) {
            throw new IllegalArgumentException(Resources.getString(48));
        } else if (!TextUtils.isEmpty(this.mBackUrl)) {
            return onCheck();
        } else {
            throw new IllegalArgumentException(Resources.getString(49));
        }
    }

    public T build() {
        if (!check()) {
            return null;
        }
        return onBuild();
    }
}
