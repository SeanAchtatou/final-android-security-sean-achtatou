package com.mol.seaplus.prepaidcard.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.mol.seaplus.base.sdk.impl.MolApiRequest;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.tool.datareader.data.IDataReader;
import java.util.Hashtable;

abstract class BasePrepaidCardApiRequest extends MolApiRequest {
    private static final String CARD_WAS_USED_CODE = "101";
    private static final String INVALID_GAME_ID_CODE = "106";
    private static final String INVALID_INPUT_CODE = "105";
    private static final String INVALID_MERCHANT_ID_CODE = "104";
    private static final String INVALID_MERCHANT_REF_ID_CODE = "102";
    private static final String INVALID_PIN_CODE = "100";
    private static final String INVALID_SIGNATURE_CODE = "103";
    private static final String PAYMENT_CHANNEL_NOT_FOUND_CODE = "108";
    private static final String REASON_CODE = "reason_code";
    private static final String SERIAL_NOT_FOUND_CODE = "107";
    private static final String SUCCESS_CODE = "000";
    private static final String TRANSACTION_NOT_FOUND_CODE = "109";
    private static Hashtable<String, Integer> mApiDescription;

    public BasePrepaidCardApiRequest(Context context, String source) {
        super(context, source);
        if (mApiDescription == null) {
            mApiDescription = new Hashtable<>();
            mApiDescription.put(SUCCESS_CODE, 32);
            mApiDescription.put(INVALID_PIN_CODE, 33);
            mApiDescription.put(CARD_WAS_USED_CODE, 34);
            mApiDescription.put(INVALID_MERCHANT_REF_ID_CODE, 35);
            mApiDescription.put(INVALID_SIGNATURE_CODE, 36);
            mApiDescription.put(INVALID_MERCHANT_ID_CODE, 37);
            mApiDescription.put(INVALID_INPUT_CODE, 38);
            mApiDescription.put(INVALID_GAME_ID_CODE, 39);
            mApiDescription.put(SERIAL_NOT_FOUND_CODE, 40);
            mApiDescription.put(PAYMENT_CHANNEL_NOT_FOUND_CODE, 41);
            mApiDescription.put(TRANSACTION_NOT_FOUND_CODE, 42);
        }
    }

    /* access modifiers changed from: protected */
    public boolean isSuccess(IDataReader pDataReader) {
        return SUCCESS_CODE.equals(pDataReader.getTableData(REASON_CODE));
    }

    /* access modifiers changed from: protected */
    public int getApiStatusCode(IDataReader pDataReader) {
        String status = pDataReader.getTableData(REASON_CODE);
        if (TextUtils.isEmpty(status)) {
            return 0;
        }
        try {
            return Integer.parseInt(status);
        } catch (Exception e) {
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    public String getApiDescription(IDataReader pDataReader) {
        String reasonCode = pDataReader.getTableData(REASON_CODE);
        String reason = null;
        if (reasonCode != null) {
            reason = Resources.getString(mApiDescription.get(reasonCode).intValue());
        }
        if (reason == null) {
            return "Unknown error";
        }
        return reason;
    }
}
