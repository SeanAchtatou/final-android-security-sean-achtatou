package com.mol.seaplus.prepaidcard.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.mol.seaplus.Log;
import com.mol.seaplus.base.sdk.MD5Factory;
import com.mol.seaplus.base.sdk.Sdk;
import com.mol.seaplus.base.sdk.impl.MolApiRequest;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.prepaidcard.sdk.api.IApiList;
import com.mol.seaplus.tool.connection.internet.httpurlconnection.HttpURLGetConnection;
import com.mol.seaplus.tool.datareader.data.IDataReader;
import com.mol.seaplus.tool.datareader.data.impl.DataReaderOption;
import com.mol.seaplus.tool.datareader.data.impl.JSONDataReader;
import java.util.Hashtable;

public final class PrepaidCardValidateApiRequest extends BasePrepaidCardApiRequest {
    private static final String TAG = PrepaidCardValidateApiRequest.class.getName();
    /* access modifiers changed from: private */
    public String mMerchantId;
    /* access modifiers changed from: private */
    public String mRefId;
    /* access modifiers changed from: private */
    public String mSecretKey;

    public PrepaidCardValidateApiRequest(Context context) {
        super(context, TAG);
    }

    /* access modifiers changed from: protected */
    public IDataReader onInitialRequest(Context context) {
        String api;
        String signature = MD5Factory.md5(this.mMerchantId + this.mRefId + this.mSecretKey);
        Hashtable<String, String> params = new Hashtable<>();
        params.put("r", "command/check");
        params.put("merchantid", this.mMerchantId);
        params.put("mref_id", this.mRefId);
        params.put("signature", signature);
        if (Sdk.isTest()) {
            api = IApiList.PREPAID_TOPUP_TEST_URL;
        } else {
            api = IApiList.PREPAID_TOPUP_PRODUCTION_URL;
        }
        Log.d("api = " + api);
        Log.d("params = " + params);
        return new JSONDataReader(new DataReaderOption(new HttpURLGetConnection(context, api, params)));
    }

    public static class Builder extends MolApiRequest.Builder<Builder> {
        private String mMerchantId;
        private String mRefId;
        private String mSecretKey;

        public Builder(Context pContext) {
            super(pContext);
        }

        public Builder merchantId(String pMerchantId) {
            this.mMerchantId = pMerchantId;
            return this;
        }

        public Builder secretKey(String pSecretKey) {
            this.mSecretKey = pSecretKey;
            return this;
        }

        public Builder refId(String pRefId) {
            this.mRefId = pRefId;
            return this;
        }

        /* access modifiers changed from: protected */
        public MolApiRequest onBuild(Context pContext) {
            if (!check()) {
                return null;
            }
            PrepaidCardValidateApiRequest prepaidCardValidateApiRequest = new PrepaidCardValidateApiRequest(pContext);
            String unused = prepaidCardValidateApiRequest.mMerchantId = this.mMerchantId;
            String unused2 = prepaidCardValidateApiRequest.mSecretKey = this.mSecretKey;
            String unused3 = prepaidCardValidateApiRequest.mRefId = this.mRefId;
            return prepaidCardValidateApiRequest;
        }

        private boolean check() {
            if (TextUtils.isEmpty(this.mMerchantId)) {
                throw new IllegalArgumentException(Resources.getString(43));
            } else if (TextUtils.isEmpty(this.mSecretKey)) {
                throw new IllegalArgumentException(Resources.getString(44));
            } else if (!TextUtils.isEmpty(this.mRefId)) {
                return true;
            } else {
                throw new IllegalArgumentException(Resources.getString(48));
            }
        }
    }
}
