package com.mol.seaplus.prepaidcard.sdk2;

import com.bluepay.pay.PublisherCode;
import com.mol.seaplus.webview.sdk.Channel;

public final class PrepaidCard {
    public static final PrepaidCard DTACHAPPY_CARD = new PrepaidCard(Channel.createCashCardChannel("Dtac Happy", "dtachappy"));
    public static final PrepaidCard MOLPOINTS_CARD = new PrepaidCard(Channel.createCashCardChannel("MOL Points", "molpoints"));
    public static final PrepaidCard ONETWOCALL_CARD = new PrepaidCard(Channel.createCashCardChannel("AIS 12Call", PublisherCode.PUBLISHER_12CALL));
    public static final PrepaidCard TRUEMONEY_CARD = new PrepaidCard(Channel.createCashCardChannel("True Money", PublisherCode.PUBLISHER_TRUEMONEY));
    private Channel mChannel;

    private PrepaidCard(Channel pChannel) {
        this.mChannel = pChannel;
    }

    public Channel getChannel() {
        return this.mChannel;
    }
}
