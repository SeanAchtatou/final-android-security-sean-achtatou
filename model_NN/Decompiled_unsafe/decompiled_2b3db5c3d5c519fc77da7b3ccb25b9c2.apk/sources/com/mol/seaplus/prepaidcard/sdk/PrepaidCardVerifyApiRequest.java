package com.mol.seaplus.prepaidcard.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.mol.seaplus.Log;
import com.mol.seaplus.base.sdk.MD5Factory;
import com.mol.seaplus.base.sdk.Sdk;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.prepaidcard.sdk.api.IApiList;
import com.mol.seaplus.tool.connection.internet.InternetConnection;
import com.mol.seaplus.tool.connection.internet.httpurlconnection.HttpURLGetConnection;
import com.mol.seaplus.tool.datareader.data.IDataReader;
import com.mol.seaplus.tool.datareader.data.impl.DataReaderOption;
import com.mol.seaplus.tool.datareader.data.impl.JSONDataReader;
import java.util.Hashtable;

final class PrepaidCardVerifyApiRequest extends BasePrepaidCardApiRequest {
    private static final String TAG = PrepaidCardVerifyApiRequest.class.getName();
    /* access modifiers changed from: private */
    public String mBackUrl;
    /* access modifiers changed from: private */
    public String mCardNo;
    /* access modifiers changed from: private */
    public String mCustomerId;
    /* access modifiers changed from: private */
    public String mGameId;
    /* access modifiers changed from: private */
    public String mMerchantId;
    /* access modifiers changed from: private */
    public String mPinNo;
    /* access modifiers changed from: private */
    public PrepaidCard mPrepaidCard;
    /* access modifiers changed from: private */
    public String mRefId;
    /* access modifiers changed from: private */
    public String mSecretKey;
    /* access modifiers changed from: private */
    public String mSerialNo;

    protected PrepaidCardVerifyApiRequest(Context context) {
        super(context, TAG);
    }

    /* access modifiers changed from: protected */
    public IDataReader onInitialRequest(Context context) {
        String api;
        String signature = MD5Factory.md5(this.mMerchantId + this.mRefId + this.mSecretKey);
        Hashtable<String, String> params = new Hashtable<>();
        params.put("r", "command/verify");
        params.put("merchantid", this.mMerchantId);
        params.put("mref_id", this.mRefId);
        params.put("gameid", this.mGameId);
        params.put("cus_id", this.mCustomerId);
        params.put("channel", this.mPrepaidCard.getCardParam());
        params.put("back_url", this.mBackUrl);
        params.put("signature", signature);
        if (this.mPrepaidCard.equals(PrepaidCard.ONETWOCALL_CARD)) {
            params.put("cardno", this.mCardNo);
        } else if (this.mPrepaidCard.equals(PrepaidCard.MOLPOINT_CARD)) {
            params.put("serial_no", this.mSerialNo);
            params.put("pin_no", this.mPinNo);
        } else {
            params.put("pin_no", this.mPinNo);
        }
        if (Sdk.isTest()) {
            api = IApiList.PREPAID_TOPUP_TEST_URL;
        } else {
            api = IApiList.PREPAID_TOPUP_PRODUCTION_URL;
        }
        Log.d("api = " + api);
        Log.d("params = " + params);
        InternetConnection internetConnection = new HttpURLGetConnection(context, api, params);
        internetConnection.setTimeout(90000);
        return new JSONDataReader(new DataReaderOption(internetConnection));
    }

    public static final class Builder extends Builder<PrepaidCardVerifyApiRequest, Builder> {
        public Builder(Context pContext) {
            super(pContext);
        }

        /* access modifiers changed from: protected */
        public boolean onCheck() {
            if (this.mPrepaidCard.equals(PrepaidCard.ONETWOCALL_CARD)) {
                if (!TextUtils.isEmpty(this.mCardNo)) {
                    return true;
                }
                throw new IllegalArgumentException(Resources.getString(50));
            } else if (this.mPrepaidCard.equals(PrepaidCard.MOLPOINT_CARD)) {
                if (!TextUtils.isEmpty(this.mSerialNo)) {
                    return true;
                }
                throw new IllegalArgumentException(Resources.getString(51));
            } else if (!TextUtils.isEmpty(this.mPinNo)) {
                return true;
            } else {
                throw new IllegalArgumentException(Resources.getString(52));
            }
        }

        /* access modifiers changed from: protected */
        public PrepaidCardVerifyApiRequest onBuild() {
            PrepaidCardVerifyApiRequest prepaidCardVerifyApiRequest = new PrepaidCardVerifyApiRequest(this.mContext);
            String unused = prepaidCardVerifyApiRequest.mMerchantId = this.mMerchantId;
            String unused2 = prepaidCardVerifyApiRequest.mGameId = this.mGameId;
            String unused3 = prepaidCardVerifyApiRequest.mCustomerId = this.mCustomerId;
            String unused4 = prepaidCardVerifyApiRequest.mSecretKey = this.mSecretKey;
            PrepaidCard unused5 = prepaidCardVerifyApiRequest.mPrepaidCard = this.mPrepaidCard;
            String unused6 = prepaidCardVerifyApiRequest.mRefId = this.mRefId;
            String unused7 = prepaidCardVerifyApiRequest.mBackUrl = this.mBackUrl;
            String unused8 = prepaidCardVerifyApiRequest.mCardNo = this.mCardNo;
            String unused9 = prepaidCardVerifyApiRequest.mSerialNo = this.mSerialNo;
            String unused10 = prepaidCardVerifyApiRequest.mPinNo = this.mPinNo;
            prepaidCardVerifyApiRequest.setLanguage(this.mLanguage);
            prepaidCardVerifyApiRequest.setErrorHandler(this.mErrorHandler);
            prepaidCardVerifyApiRequest.setMolApiCallback(this.mMolApiCallback);
            return prepaidCardVerifyApiRequest;
        }
    }
}
