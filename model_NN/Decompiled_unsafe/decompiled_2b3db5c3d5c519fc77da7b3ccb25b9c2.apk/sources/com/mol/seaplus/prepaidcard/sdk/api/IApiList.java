package com.mol.seaplus.prepaidcard.sdk.api;

public class IApiList {
    public static final String PREPAID_TOPUP_PRODUCTION_URL = "https://payment.molthailand.com/web/index.php";
    public static final String PREPAID_TOPUP_TEST_URL = "https://payment.molthailand.com/web-test/index.php";
}
