package com.mol.seaplus.prepaidcard.sdk;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.mol.seaplus.Language;
import com.mol.seaplus.base.sdk.impl.MolApiCallback;
import com.mol.seaplus.base.sdk.impl.MolDialog;
import com.mol.seaplus.base.sdk.impl.Resources;
import com.mol.seaplus.prepaidcard.sdk.PrepaidCardVerifyApiRequest;
import com.mol.seaplus.tool.datareader.api.IApiRequest;
import com.mol.seaplus.tool.dialog.ToolAlertDialog;
import com.mol.seaplus.tool.dialog.ToolProgressDialog;
import com.mol.seaplus.tool.utils.UiUtils;
import org.json.JSONObject;

final class PrepaidCardVerifyDialog extends MolDialog {
    /* access modifiers changed from: private */
    public PrepaidCardVerifyApiRequest.Builder mBuilder;
    /* access modifiers changed from: private */
    public EditText mCardNoEdittext;
    /* access modifiers changed from: private */
    public Language mLanguage;
    /* access modifiers changed from: private */
    public MolApiCallback mMolApiCallback;
    /* access modifiers changed from: private */
    public EditText mPinNoEditText;
    /* access modifiers changed from: private */
    public PrepaidCard mPrepaidCard;
    /* access modifiers changed from: private */
    public ToolProgressDialog mProgressDialog;
    /* access modifiers changed from: private */
    public EditText mSerialNoEditText;
    private Button mTopUpBtn;
    /* access modifiers changed from: private */
    public MolApiCallback molApiCallback;

    private PrepaidCardVerifyDialog(Context context, PrepaidCard pPrepaidCard) {
        super(context);
        this.molApiCallback = new MolApiCallback() {
            public void onApiSuccess(JSONObject pResult) {
                PrepaidCardVerifyDialog.this.mProgressDialog.dismiss();
                PrepaidCardVerifyDialog.this.dismiss();
                if (PrepaidCardVerifyDialog.this.mMolApiCallback != null) {
                    PrepaidCardVerifyDialog.this.mMolApiCallback.onApiSuccess(pResult);
                }
            }

            public void onApiError(int pErrorCode, String pError) {
                PrepaidCardVerifyDialog.this.mProgressDialog.dismiss();
                PrepaidCardVerifyDialog.this.dismiss();
                if (PrepaidCardVerifyDialog.this.mMolApiCallback != null) {
                    PrepaidCardVerifyDialog.this.mMolApiCallback.onApiError(pErrorCode, pError);
                }
            }
        };
        this.mPrepaidCard = pPrepaidCard;
    }

    /* access modifiers changed from: protected */
    public void onCreateDialog() {
        Language language = this.mLanguage;
        if (language == null) {
            language = Language.EN;
        }
        setLanguage(language);
        if (this.mPrepaidCard.equals(PrepaidCard.MOLPOINT_CARD)) {
            setContentView(initSerialPinLayout(this.context));
        } else {
            setContentView(initCardNoLayout(this.context));
        }
        this.mTopUpBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Context context = view.getContext();
                if (PrepaidCardVerifyDialog.this.mPrepaidCard.equals(PrepaidCard.ONETWOCALL_CARD) || PrepaidCardVerifyDialog.this.mPrepaidCard.equals(PrepaidCard.DTACHAPPY_CARD) || PrepaidCardVerifyDialog.this.mPrepaidCard.equals(PrepaidCard.TRUEMONEY_CARD)) {
                    String cardNo = PrepaidCardVerifyDialog.this.mCardNoEdittext.getText().toString();
                    if (TextUtils.isEmpty(cardNo)) {
                        ToolAlertDialog.alert(context, Resources.getString(29));
                        return;
                    }
                    int length = cardNo.length();
                    if (PrepaidCardVerifyDialog.this.mPrepaidCard.equals(PrepaidCard.ONETWOCALL_CARD)) {
                        if (!(length == 13 || length == 16)) {
                            ToolAlertDialog.alert(context, Resources.getString(30));
                            return;
                        }
                    } else if (PrepaidCardVerifyDialog.this.mPrepaidCard.equals(PrepaidCard.TRUEMONEY_CARD)) {
                        if (length != 14) {
                            ToolAlertDialog.alert(context, Resources.getString(30));
                            return;
                        }
                    } else if (PrepaidCardVerifyDialog.this.mPrepaidCard.equals(PrepaidCard.DTACHAPPY_CARD) && length != 16) {
                        ToolAlertDialog.alert(context, Resources.getString(30));
                        return;
                    }
                    if (PrepaidCardVerifyDialog.this.mPrepaidCard.equals(PrepaidCard.ONETWOCALL_CARD)) {
                        PrepaidCardVerifyDialog.this.mBuilder.cardNo(cardNo);
                    } else {
                        PrepaidCardVerifyDialog.this.mBuilder.pinNo(cardNo);
                    }
                } else {
                    String serialNo = PrepaidCardVerifyDialog.this.mSerialNoEditText.getText().toString();
                    String pinNo = PrepaidCardVerifyDialog.this.mPinNoEditText.getText().toString();
                    if (TextUtils.isEmpty(serialNo)) {
                        ToolAlertDialog.alert(context, Resources.getString(28));
                        return;
                    } else if (TextUtils.isEmpty(pinNo)) {
                        ToolAlertDialog.alert(context, Resources.getString(29));
                        return;
                    } else {
                        int serialLength = serialNo.length();
                        int pinLength = pinNo.length();
                        if (PrepaidCardVerifyDialog.this.mPrepaidCard.equals(PrepaidCard.MOLPOINT_CARD)) {
                            if (serialLength != 10) {
                                ToolAlertDialog.alert(context, Resources.getString(31));
                                return;
                            } else if (pinLength != 14) {
                                ToolAlertDialog.alert(context, Resources.getString(30));
                                return;
                            }
                        }
                        ((PrepaidCardVerifyApiRequest.Builder) PrepaidCardVerifyDialog.this.mBuilder.serialNo(serialNo)).pinNo(pinNo);
                    }
                }
                ((PrepaidCardVerifyApiRequest.Builder) PrepaidCardVerifyDialog.this.mBuilder.forCard(PrepaidCardVerifyDialog.this.mPrepaidCard)).callback(PrepaidCardVerifyDialog.this.molApiCallback);
                ToolProgressDialog unused = PrepaidCardVerifyDialog.this.mProgressDialog = ToolProgressDialog.show(context, Resources.getString(5), Resources.getString(6));
                ((IApiRequest) PrepaidCardVerifyDialog.this.mBuilder.build()).request();
            }
        });
    }

    private View initCardNoLayout(Context pContext) {
        int padding5 = (int) UiUtils.applyDimension(pContext, 1, 5);
        LinearLayout mainLayout = new LinearLayout(pContext);
        this.mCardNoEdittext = new EditText(pContext);
        this.mTopUpBtn = new Button(pContext);
        mainLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        mainLayout.setOrientation(1);
        mainLayout.setGravity(1);
        mainLayout.setPadding(padding5, padding5, padding5, padding5);
        this.mCardNoEdittext.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.mCardNoEdittext.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
        this.mCardNoEdittext.setHintTextColor(-7829368);
        this.mCardNoEdittext.setTextSize(2, 18.0f);
        this.mCardNoEdittext.setHint(Resources.getString(21));
        this.mCardNoEdittext.setSingleLine(true);
        this.mCardNoEdittext.setInputType(8194);
        this.mCardNoEdittext.setPadding(padding5, padding5, padding5, padding5);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(-2, -2);
        params1.topMargin = padding5;
        this.mTopUpBtn.setLayoutParams(params1);
        this.mTopUpBtn.setTextSize(2, 16.0f);
        this.mTopUpBtn.setText(Resources.getString(22));
        mainLayout.addView(this.mCardNoEdittext);
        mainLayout.addView(this.mTopUpBtn);
        return mainLayout;
    }

    private View initSerialPinLayout(Context pContext) {
        int padding5 = (int) UiUtils.applyDimension(pContext, 1, 5);
        LinearLayout mainLayout = new LinearLayout(pContext);
        this.mSerialNoEditText = new EditText(pContext);
        this.mPinNoEditText = new EditText(pContext);
        this.mTopUpBtn = new Button(pContext);
        mainLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        mainLayout.setOrientation(1);
        mainLayout.setGravity(1);
        mainLayout.setPadding(padding5, padding5, padding5, padding5);
        this.mSerialNoEditText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.mSerialNoEditText.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
        this.mSerialNoEditText.setHintTextColor(-7829368);
        this.mSerialNoEditText.setTextSize(2, 18.0f);
        this.mSerialNoEditText.setHint(Resources.getString(20));
        this.mSerialNoEditText.setSingleLine(true);
        this.mSerialNoEditText.setInputType(8194);
        this.mSerialNoEditText.setPadding(padding5, padding5, padding5, padding5);
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(-1, -2);
        params1.topMargin = padding5;
        this.mPinNoEditText.setLayoutParams(params1);
        this.mPinNoEditText.setTextSize(2, 18.0f);
        this.mPinNoEditText.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
        this.mPinNoEditText.setHintTextColor(-7829368);
        this.mPinNoEditText.setHint(Resources.getString(21));
        this.mPinNoEditText.setSingleLine(true);
        this.mPinNoEditText.setInputType(8194);
        this.mPinNoEditText.setPadding(padding5, padding5, padding5, padding5);
        LinearLayout.LayoutParams params12 = new LinearLayout.LayoutParams(-2, -2);
        params12.topMargin = padding5;
        this.mTopUpBtn.setLayoutParams(params12);
        this.mTopUpBtn.setTextSize(2, 16.0f);
        this.mTopUpBtn.setText(Resources.getString(22));
        mainLayout.addView(this.mSerialNoEditText);
        mainLayout.addView(this.mPinNoEditText);
        mainLayout.addView(this.mTopUpBtn);
        return mainLayout;
    }

    public static final class Builder extends Builder<PrepaidCardVerifyDialog, Builder> {
        public Builder(Context pContext) {
            this(pContext, null);
        }

        public Builder(Context pContext, PrepaidCard pPrepaidCard) {
            super(pContext);
            forCard(pPrepaidCard);
        }

        /* access modifiers changed from: protected */
        public PrepaidCardVerifyDialog onBuild() {
            PrepaidCardVerifyDialog prepaidCardVerifyDialog = new PrepaidCardVerifyDialog(this.mContext, this.mPrepaidCard);
            Language unused = prepaidCardVerifyDialog.mLanguage = this.mLanguage;
            PrepaidCardVerifyApiRequest.Builder unused2 = prepaidCardVerifyDialog.mBuilder = (PrepaidCardVerifyApiRequest.Builder) ((PrepaidCardVerifyApiRequest.Builder) ((PrepaidCardVerifyApiRequest.Builder) ((PrepaidCardVerifyApiRequest.Builder) ((PrepaidCardVerifyApiRequest.Builder) ((PrepaidCardVerifyApiRequest.Builder) ((PrepaidCardVerifyApiRequest.Builder) ((PrepaidCardVerifyApiRequest.Builder) ((PrepaidCardVerifyApiRequest.Builder) ((PrepaidCardVerifyApiRequest.Builder) new PrepaidCardVerifyApiRequest.Builder(this.mContext).merchantId(this.mMerchantId)).gameId(this.mGameId)).customerId(this.mCustomerId)).forCard(this.mPrepaidCard)).secretKey(this.mSecretKey)).forCard(this.mPrepaidCard)).refId(this.mRefId)).backUrl(this.mBackUrl)).language(this.mLanguage)).setErrorHandler(this.mErrorHandler);
            MolApiCallback unused3 = prepaidCardVerifyDialog.mMolApiCallback = this.mMolApiCallback;
            return prepaidCardVerifyDialog;
        }
    }
}
