package com.seantech.nineke;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.seantech.nineke";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "seantechnineke";
    public static final int VERSION_CODE = 82;
    public static final String VERSION_NAME = "1.3.0";
}
