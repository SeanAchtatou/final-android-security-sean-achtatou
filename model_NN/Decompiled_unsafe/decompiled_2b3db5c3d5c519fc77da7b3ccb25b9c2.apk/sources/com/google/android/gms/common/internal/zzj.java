package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;

public class zzj implements Parcelable.Creator<GetServiceRequest> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
     arg types: [android.os.Parcel, int, android.os.IBinder, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.IBinder, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.api.Scope[], int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
     arg types: [android.os.Parcel, int, android.os.Bundle, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.accounts.Account, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    static void zza(GetServiceRequest getServiceRequest, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, getServiceRequest.version);
        zzb.zzc(parcel, 2, getServiceRequest.yu);
        zzb.zzc(parcel, 3, getServiceRequest.yv);
        zzb.zza(parcel, 4, getServiceRequest.yw, false);
        zzb.zza(parcel, 5, getServiceRequest.yx, false);
        zzb.zza(parcel, 6, (Parcelable[]) getServiceRequest.yy, i, false);
        zzb.zza(parcel, 7, getServiceRequest.yz, false);
        zzb.zza(parcel, 8, (Parcelable) getServiceRequest.yA, i, false);
        zzb.zza(parcel, 9, getServiceRequest.yB);
        zzb.zzaj(parcel, zzcn);
    }

    /* renamed from: zzcg */
    public GetServiceRequest createFromParcel(Parcel parcel) {
        int i = 0;
        Account account = null;
        int zzcm = zza.zzcm(parcel);
        long j = 0;
        Bundle bundle = null;
        Scope[] scopeArr = null;
        IBinder iBinder = null;
        String str = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case 1:
                    i3 = zza.zzg(parcel, zzcl);
                    break;
                case 2:
                    i2 = zza.zzg(parcel, zzcl);
                    break;
                case 3:
                    i = zza.zzg(parcel, zzcl);
                    break;
                case 4:
                    str = zza.zzq(parcel, zzcl);
                    break;
                case 5:
                    iBinder = zza.zzr(parcel, zzcl);
                    break;
                case 6:
                    scopeArr = (Scope[]) zza.zzb(parcel, zzcl, Scope.CREATOR);
                    break;
                case 7:
                    bundle = zza.zzs(parcel, zzcl);
                    break;
                case 8:
                    account = (Account) zza.zza(parcel, zzcl, Account.CREATOR);
                    break;
                case 9:
                    j = zza.zzi(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new GetServiceRequest(i3, i2, i, str, iBinder, scopeArr, bundle, account, j);
        }
        throw new zza.C0000zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcm).toString(), parcel);
    }

    /* renamed from: zzge */
    public GetServiceRequest[] newArray(int i) {
        return new GetServiceRequest[i];
    }
}
