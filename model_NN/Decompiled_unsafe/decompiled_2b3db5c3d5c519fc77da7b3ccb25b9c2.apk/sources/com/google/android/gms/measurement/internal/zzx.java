package com.google.android.gms.measurement.internal;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Size;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.internal.zzapo;
import com.google.android.gms.internal.zzug;
import com.google.android.gms.internal.zzuh;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.android.gms.measurement.internal.zze;
import com.google.android.gms.measurement.internal.zzq;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class zzx {
    private static volatile zzx akP;
    private final zzd akQ;
    private final zzt akR;
    private final zzp akS;
    private final zzw akT;
    private final zzaf akU;
    private final zzv akV;
    private final AppMeasurement akW;
    private final zzal akX;
    private final zze akY;
    private final zzq akZ;
    private final zzad ala;
    private final zzg alb;
    private final zzac alc;
    private final zzn ald;
    private final zzr ale;
    private final zzai alf;
    private final zzc alg;
    public final FirebaseAnalytics alh = new FirebaseAnalytics(this);
    private boolean ali;
    private Boolean alj;
    private FileLock alk;
    private FileChannel all;
    private List<Long> alm;
    private int aln;
    private int alo;
    private final Context mContext;
    private final zze zzaoc;
    private final boolean zzcwq;

    private class zza implements zze.zzb {
        zzuh.zze alq;
        List<Long> alr;
        long als;
        List<zzuh.zzb> zzalc;

        private zza() {
        }

        private long zza(zzuh.zzb zzb) {
            return ((zzb.ano.longValue() / 1000) / 60) / 60;
        }

        /* access modifiers changed from: package-private */
        public boolean isEmpty() {
            return this.zzalc == null || this.zzalc.isEmpty();
        }

        public boolean zza(long j, zzuh.zzb zzb) {
            zzab.zzy(zzb);
            if (this.zzalc == null) {
                this.zzalc = new ArrayList();
            }
            if (this.alr == null) {
                this.alr = new ArrayList();
            }
            if (this.zzalc.size() > 0 && zza(this.zzalc.get(0)) != zza(zzb)) {
                return false;
            }
            long aM = this.als + ((long) zzb.aM());
            if (aM >= ((long) zzx.this.zzbsf().zzbri())) {
                return false;
            }
            this.als = aM;
            this.zzalc.add(zzb);
            this.alr.add(Long.valueOf(j));
            return this.zzalc.size() < zzx.this.zzbsf().zzbrj();
        }

        public void zzc(zzuh.zze zze) {
            zzab.zzy(zze);
            this.alq = zze;
        }
    }

    zzx(zzab zzab) {
        zzab.zzy(zzab);
        this.mContext = zzab.mContext;
        this.zzaoc = zzab.zzl(this);
        this.akQ = zzab.zza(this);
        zzt zzb = zzab.zzb(this);
        zzb.initialize();
        this.akR = zzb;
        zzp zzc = zzab.zzc(this);
        zzc.initialize();
        this.akS = zzc;
        zzbsd().zzbta().zzj("App measurement is starting up, version", Long.valueOf(zzbsf().zzbpz()));
        zzbsd().zzbta().log("To enable debug logging run: adb shell setprop log.tag.FA VERBOSE");
        zzbsd().zzbtb().log("Debug logging enabled");
        zzbsd().zzbtb().zzj("AppMeasurement singleton hash", Integer.valueOf(System.identityHashCode(this)));
        this.akX = zzab.zzi(this);
        zzg zzn = zzab.zzn(this);
        zzn.initialize();
        this.alb = zzn;
        zzn zzo = zzab.zzo(this);
        zzo.initialize();
        this.ald = zzo;
        zze zzj = zzab.zzj(this);
        zzj.initialize();
        this.akY = zzj;
        zzc zzr = zzab.zzr(this);
        zzr.initialize();
        this.alg = zzr;
        zzq zzk = zzab.zzk(this);
        zzk.initialize();
        this.akZ = zzk;
        zzad zzm = zzab.zzm(this);
        zzm.initialize();
        this.ala = zzm;
        zzac zzh = zzab.zzh(this);
        zzh.initialize();
        this.alc = zzh;
        zzai zzq = zzab.zzq(this);
        zzq.initialize();
        this.alf = zzq;
        this.ale = zzab.zzp(this);
        this.akW = zzab.zzg(this);
        zzaf zze = zzab.zze(this);
        zze.initialize();
        this.akU = zze;
        zzv zzf = zzab.zzf(this);
        zzf.initialize();
        this.akV = zzf;
        zzw zzd = zzab.zzd(this);
        zzd.initialize();
        this.akT = zzd;
        if (this.aln != this.alo) {
            zzbsd().zzbsv().zze("Not all components initialized", Integer.valueOf(this.aln), Integer.valueOf(this.alo));
        }
        this.zzcwq = true;
        if (!this.akQ.zzabc() && !zzbty()) {
            if (!(this.mContext.getApplicationContext() instanceof Application)) {
                zzbsd().zzbsx().log("Application context is not an Application");
            } else if (Build.VERSION.SDK_INT >= 14) {
                zzbru().zzbun();
            } else {
                zzbsd().zzbtb().log("Not tracking deep linking pre-ICS");
            }
        }
        this.akT.zzm(new Runnable() {
            public void run() {
                zzx.this.start();
            }
        });
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    @WorkerThread
    public void zza(int i, Throwable th, byte[] bArr) {
        boolean z = false;
        zzwu();
        zzzg();
        if (bArr == null) {
            bArr = new byte[0];
        }
        List<Long> list = this.alm;
        this.alm = null;
        if ((i == 200 || i == 204) && th == null) {
            zzbse().ajY.set(zzyw().currentTimeMillis());
            zzbse().ajZ.set(0);
            zzbue();
            zzbsd().zzbtc().zze("Successful upload. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
            zzbry().beginTransaction();
            try {
                for (Long longValue : list) {
                    zzbry().zzbh(longValue.longValue());
                }
                zzbry().setTransactionSuccessful();
                zzbry().endTransaction();
                if (!zzbts().zzadj() || !zzbud()) {
                    zzbue();
                } else {
                    zzbuc();
                }
            } catch (Throwable th2) {
                zzbry().endTransaction();
                throw th2;
            }
        } else {
            zzbsd().zzbtc().zze("Network upload failed. Will retry later. code, error", Integer.valueOf(i), th);
            zzbse().ajZ.set(zzyw().currentTimeMillis());
            if (i == 503 || i == 429) {
                z = true;
            }
            if (z) {
                zzbse().aka.set(zzyw().currentTimeMillis());
            }
            zzbue();
        }
    }

    private void zza(zzaa zzaa) {
        if (zzaa == null) {
            throw new IllegalStateException("Component not created");
        } else if (!zzaa.isInitialized()) {
            throw new IllegalStateException("Component not initialized");
        }
    }

    private void zza(zzz zzz) {
        if (zzz == null) {
            throw new IllegalStateException("Component not created");
        }
    }

    private zzuh.zza[] zza(String str, zzuh.zzg[] zzgArr, zzuh.zzb[] zzbArr) {
        zzab.zzhr(str);
        return zzbrt().zza(str, zzbArr, zzgArr);
    }

    private void zzad(List<Long> list) {
        zzab.zzbo(!list.isEmpty());
        if (this.alm != null) {
            zzbsd().zzbsv().log("Set uploading progress before finishing the previous upload");
        } else {
            this.alm = new ArrayList(list);
        }
    }

    @WorkerThread
    private boolean zzbub() {
        zzwu();
        return this.alm != null;
    }

    private boolean zzbud() {
        zzwu();
        zzzg();
        return zzbry().zzbsl() || !TextUtils.isEmpty(zzbry().zzbsg());
    }

    @WorkerThread
    private void zzbue() {
        zzwu();
        zzzg();
        if (zzbui()) {
            if (!zzbto() || !zzbud()) {
                zzbtt().unregister();
                zzbtu().cancel();
                return;
            }
            long zzbuf = zzbuf();
            if (zzbuf == 0) {
                zzbtt().unregister();
                zzbtu().cancel();
            } else if (!zzbts().zzadj()) {
                zzbtt().zzadg();
                zzbtu().cancel();
            } else {
                long j = zzbse().aka.get();
                long zzbrm = zzbsf().zzbrm();
                if (!zzbrz().zzg(j, zzbrm)) {
                    zzbuf = Math.max(zzbuf, j + zzbrm);
                }
                zzbtt().unregister();
                long currentTimeMillis = zzbuf - zzyw().currentTimeMillis();
                if (currentTimeMillis <= 0) {
                    zzbtu().zzv(1);
                    return;
                }
                zzbsd().zzbtc().zzj("Upload scheduled in approximately ms", Long.valueOf(currentTimeMillis));
                zzbtu().zzv(currentTimeMillis);
            }
        }
    }

    private long zzbuf() {
        long currentTimeMillis = zzyw().currentTimeMillis();
        long zzbrp = zzbsf().zzbrp();
        long zzbrn = zzbsf().zzbrn();
        long j = zzbse().ajY.get();
        long j2 = zzbse().ajZ.get();
        long max = Math.max(zzbry().zzbsj(), zzbry().zzbsk());
        if (max == 0) {
            return 0;
        }
        long abs = currentTimeMillis - Math.abs(max - currentTimeMillis);
        long abs2 = currentTimeMillis - Math.abs(j2 - currentTimeMillis);
        long max2 = Math.max(currentTimeMillis - Math.abs(j - currentTimeMillis), abs2);
        long j3 = zzbrp + abs;
        if (!zzbrz().zzg(max2, zzbrn)) {
            j3 = max2 + zzbrn;
        }
        if (abs2 == 0 || abs2 < abs) {
            return j3;
        }
        for (int i = 0; i < zzbsf().zzbrr(); i++) {
            j3 += ((long) (1 << i)) * zzbsf().zzbrq();
            if (j3 > abs2) {
                return j3;
            }
        }
        return 0;
    }

    public static zzx zzdo(Context context) {
        zzab.zzy(context);
        zzab.zzy(context.getApplicationContext());
        if (akP == null) {
            synchronized (zzx.class) {
                if (akP == null) {
                    akP = new zzab(context).zzbum();
                }
            }
        }
        return akP;
    }

    @WorkerThread
    private void zze(AppMetadata appMetadata) {
        boolean z = true;
        zzwu();
        zzzg();
        zzab.zzy(appMetadata);
        zzab.zzhr(appMetadata.packageName);
        zza zzln = zzbry().zzln(appMetadata.packageName);
        String zzly = zzbse().zzly(appMetadata.packageName);
        boolean z2 = false;
        if (zzln == null) {
            zza zza2 = new zza(this, appMetadata.packageName);
            zza2.zzky(zzbse().zzbtf());
            zza2.zzla(zzly);
            zzln = zza2;
            z2 = true;
        } else if (!zzly.equals(zzln.zzbpt())) {
            zzln.zzla(zzly);
            zzln.zzky(zzbse().zzbtf());
            z2 = true;
        }
        if (!TextUtils.isEmpty(appMetadata.aic) && !appMetadata.aic.equals(zzln.zzbps())) {
            zzln.zzkz(appMetadata.aic);
            z2 = true;
        }
        if (!TextUtils.isEmpty(appMetadata.aik) && !appMetadata.aik.equals(zzln.zzbpu())) {
            zzln.zzlb(appMetadata.aik);
            z2 = true;
        }
        if (!(appMetadata.aie == 0 || appMetadata.aie == zzln.zzbpz())) {
            zzln.zzax(appMetadata.aie);
            z2 = true;
        }
        if (!TextUtils.isEmpty(appMetadata.aav) && !appMetadata.aav.equals(zzln.zzxc())) {
            zzln.setAppVersion(appMetadata.aav);
            z2 = true;
        }
        if (appMetadata.aij != zzln.zzbpx()) {
            zzln.zzaw(appMetadata.aij);
            z2 = true;
        }
        if (!TextUtils.isEmpty(appMetadata.aid) && !appMetadata.aid.equals(zzln.zzbpy())) {
            zzln.zzlc(appMetadata.aid);
            z2 = true;
        }
        if (appMetadata.aif != zzln.zzbqa()) {
            zzln.zzay(appMetadata.aif);
            z2 = true;
        }
        if (appMetadata.aih != zzln.zzbqb()) {
            zzln.setMeasurementEnabled(appMetadata.aih);
        } else {
            z = z2;
        }
        if (z) {
            zzbry().zza(zzln);
        }
    }

    /* JADX INFO: finally extract failed */
    private boolean zzi(String str, long j) {
        int i;
        boolean z;
        int i2;
        boolean z2;
        zzbry().beginTransaction();
        try {
            zza zza2 = new zza();
            zzbry().zza(str, j, zza2);
            if (!zza2.isEmpty()) {
                zzuh.zze zze = zza2.alq;
                zze.anv = new zzuh.zzb[zza2.zzalc.size()];
                int i3 = 0;
                int i4 = 0;
                while (i4 < zza2.zzalc.size()) {
                    if (zzbsa().zzax(zza2.alq.zzck, zza2.zzalc.get(i4).name)) {
                        zzbsd().zzbsx().zzj("Dropping blacklisted raw event", zza2.zzalc.get(i4).name);
                        zzbrz().zze(11, "_ev", zza2.zzalc.get(i4).name);
                        i = i3;
                    } else {
                        if (zzbsa().zzay(zza2.alq.zzck, zza2.zzalc.get(i4).name)) {
                            if (zza2.zzalc.get(i4).ann == null) {
                                zza2.zzalc.get(i4).ann = new zzuh.zzc[0];
                            }
                            zzuh.zzc[] zzcArr = zza2.zzalc.get(i4).ann;
                            int length = zzcArr.length;
                            int i5 = 0;
                            while (true) {
                                if (i5 >= length) {
                                    z = false;
                                    break;
                                }
                                zzuh.zzc zzc = zzcArr[i5];
                                if ("_c".equals(zzc.name)) {
                                    zzc.anr = 1L;
                                    z = true;
                                    break;
                                }
                                i5++;
                            }
                            if (!z) {
                                zzbsd().zzbtc().zzj("Marking event as conversion", zza2.zzalc.get(i4).name);
                                zzuh.zzc[] zzcArr2 = (zzuh.zzc[]) Arrays.copyOf(zza2.zzalc.get(i4).ann, zza2.zzalc.get(i4).ann.length + 1);
                                zzuh.zzc zzc2 = new zzuh.zzc();
                                zzc2.name = "_c";
                                zzc2.anr = 1L;
                                zzcArr2[zzcArr2.length - 1] = zzc2;
                                zza2.zzalc.get(i4).ann = zzcArr2;
                            }
                            boolean zzmj = zzal.zzmj(zza2.zzalc.get(i4).name);
                            if (zzmj && zzbry().zza(zzbtz(), zza2.alq.zzck, false, zzmj, false).air - ((long) zzbsf().zzlf(zza2.alq.zzck)) > 0) {
                                zzbsd().zzbsx().log("Too many conversions. Not logging as conversion.");
                                zzuh.zzb zzb = zza2.zzalc.get(i4);
                                boolean z3 = false;
                                zzuh.zzc zzc3 = null;
                                zzuh.zzc[] zzcArr3 = zza2.zzalc.get(i4).ann;
                                int length2 = zzcArr3.length;
                                int i6 = 0;
                                while (i6 < length2) {
                                    zzuh.zzc zzc4 = zzcArr3[i6];
                                    if ("_c".equals(zzc4.name)) {
                                        z2 = z3;
                                    } else if ("_err".equals(zzc4.name)) {
                                        zzuh.zzc zzc5 = zzc3;
                                        z2 = true;
                                        zzc4 = zzc5;
                                    } else {
                                        zzc4 = zzc3;
                                        z2 = z3;
                                    }
                                    i6++;
                                    z3 = z2;
                                    zzc3 = zzc4;
                                }
                                if (z3 && zzc3 != null) {
                                    zzuh.zzc[] zzcArr4 = new zzuh.zzc[(zzb.ann.length - 1)];
                                    int i7 = 0;
                                    zzuh.zzc[] zzcArr5 = zzb.ann;
                                    int length3 = zzcArr5.length;
                                    int i8 = 0;
                                    while (i8 < length3) {
                                        zzuh.zzc zzc6 = zzcArr5[i8];
                                        if (zzc6 != zzc3) {
                                            i2 = i7 + 1;
                                            zzcArr4[i7] = zzc6;
                                        } else {
                                            i2 = i7;
                                        }
                                        i8++;
                                        i7 = i2;
                                    }
                                    zza2.zzalc.get(i4).ann = zzcArr4;
                                } else if (zzc3 != null) {
                                    zzc3.name = "_err";
                                    zzc3.anr = 10L;
                                } else {
                                    zzbsd().zzbsv().log("Did not find conversion parameter. Error not tracked");
                                }
                            }
                        }
                        zze.anv[i3] = zza2.zzalc.get(i4);
                        i = i3 + 1;
                    }
                    i4++;
                    i3 = i;
                }
                if (i3 < zza2.zzalc.size()) {
                    zze.anv = (zzuh.zzb[]) Arrays.copyOf(zze.anv, i3);
                }
                zze.anO = zza(zza2.alq.zzck, zza2.alq.anw, zze.anv);
                zze.any = zze.anv[0].ano;
                zze.anz = zze.anv[0].ano;
                for (int i9 = 1; i9 < zze.anv.length; i9++) {
                    zzuh.zzb zzb2 = zze.anv[i9];
                    if (zzb2.ano.longValue() < zze.any.longValue()) {
                        zze.any = zzb2.ano;
                    }
                    if (zzb2.ano.longValue() > zze.anz.longValue()) {
                        zze.anz = zzb2.ano;
                    }
                }
                String str2 = zza2.alq.zzck;
                zza zzln = zzbry().zzln(str2);
                if (zzln == null) {
                    zzbsd().zzbsv().log("Bundling raw events w/o app info");
                } else {
                    long zzbpw = zzln.zzbpw();
                    zze.anB = zzbpw != 0 ? Long.valueOf(zzbpw) : null;
                    long zzbpv = zzln.zzbpv();
                    if (zzbpv != 0) {
                        zzbpw = zzbpv;
                    }
                    zze.anA = zzbpw != 0 ? Long.valueOf(zzbpw) : null;
                    zzln.zzbqf();
                    zze.anM = Integer.valueOf((int) zzln.zzbqc());
                    zzln.zzau(zze.any.longValue());
                    zzln.zzav(zze.anz.longValue());
                    zzbry().zza(zzln);
                }
                zze.aig = zzbsd().zzbtd();
                zzbry().zza(zze);
                zzbry().zzac(zza2.alr);
                zzbry().zzlt(str2);
                zzbry().setTransactionSuccessful();
                zzbry().endTransaction();
                return true;
            }
            zzbry().setTransactionSuccessful();
            zzbry().endTransaction();
            return false;
        } catch (Throwable th) {
            zzbry().endTransaction();
            throw th;
        }
    }

    public Context getContext() {
        return this.mContext;
    }

    @WorkerThread
    public boolean isEnabled() {
        boolean z = false;
        zzwu();
        zzzg();
        if (zzbsf().zzbrd()) {
            return false;
        }
        Boolean zzbre = zzbsf().zzbre();
        if (zzbre != null) {
            z = zzbre.booleanValue();
        } else if (!zzbsf().zzaqp()) {
            z = true;
        }
        return zzbse().zzcc(z);
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public void start() {
        zzwu();
        if (!zzbty() || (this.akT.isInitialized() && !this.akT.zzbul())) {
            zzbry().zzbsh();
            if (zzbto()) {
                if (!zzbsf().zzabc() && !TextUtils.isEmpty(zzbrv().zzbps())) {
                    String zzbti = zzbse().zzbti();
                    if (zzbti == null) {
                        zzbse().zzlz(zzbrv().zzbps());
                    } else if (!zzbti.equals(zzbrv().zzbps())) {
                        zzbsd().zzbta().log("Rechecking which service to use due to a GMP App Id change");
                        zzbse().zzbtk();
                        this.ala.disconnect();
                        this.ala.zzaai();
                        zzbse().zzlz(zzbrv().zzbps());
                    }
                }
                if (!zzbsf().zzabc() && !zzbty() && !TextUtils.isEmpty(zzbrv().zzbps())) {
                    zzbru().zzbuo();
                }
            } else if (isEnabled()) {
                if (!zzbrz().zzeo("android.permission.INTERNET")) {
                    zzbsd().zzbsv().log("App is missing INTERNET permission");
                }
                if (!zzbrz().zzeo("android.permission.ACCESS_NETWORK_STATE")) {
                    zzbsd().zzbsv().log("App is missing ACCESS_NETWORK_STATE permission");
                }
                if (!zzu.zzav(getContext())) {
                    zzbsd().zzbsv().log("AppMeasurementReceiver not registered/enabled");
                }
                if (!zzae.zzaw(getContext())) {
                    zzbsd().zzbsv().log("AppMeasurementService not registered/enabled");
                }
                zzbsd().zzbsv().log("Uploading is not possible. App measurement disabled");
            }
            zzbue();
            return;
        }
        zzbsd().zzbsv().log("Scheduler shutting down before Scion.start() called");
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public int zza(FileChannel fileChannel) {
        zzwu();
        if (fileChannel == null || !fileChannel.isOpen()) {
            zzbsd().zzbsv().log("Bad chanel to read from");
            return 0;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0L);
            int read = fileChannel.read(allocate);
            if (read != 4) {
                zzbsd().zzbsx().zzj("Unexpected data length or empty data in channel. Bytes read", Integer.valueOf(read));
                return 0;
            }
            allocate.flip();
            return allocate.getInt();
        } catch (IOException e) {
            zzbsd().zzbsv().zzj("Failed to read from channel", e);
            return 0;
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public void zza(AppMetadata appMetadata, long j) {
        zza zzln = zzbry().zzln(appMetadata.packageName);
        if (!(zzln == null || zzln.zzbps() == null || zzln.zzbps().equals(appMetadata.aic))) {
            zzbsd().zzbsx().log("New GMP App Id passed in. Removing cached database data.");
            zzbry().zzls(zzln.zzsh());
            zzln = null;
        }
        if (zzln != null && zzln.zzxc() != null && !zzln.zzxc().equals(appMetadata.aav)) {
            Bundle bundle = new Bundle();
            bundle.putString("_pv", zzln.zzxc());
            zzb(new EventParcel("_au", new EventParams(bundle), "auto", j), appMetadata);
        }
    }

    /* access modifiers changed from: package-private */
    public void zza(zzh zzh, AppMetadata appMetadata) {
        zzwu();
        zzzg();
        zzab.zzy(zzh);
        zzab.zzy(appMetadata);
        zzab.zzhr(zzh.zzcjf);
        zzab.zzbo(zzh.zzcjf.equals(appMetadata.packageName));
        zzuh.zze zze = new zzuh.zze();
        zze.anu = 1;
        zze.anC = "android";
        zze.zzck = appMetadata.packageName;
        zze.aid = appMetadata.aid;
        zze.aav = appMetadata.aav;
        zze.anP = Integer.valueOf((int) appMetadata.aij);
        zze.anG = Long.valueOf(appMetadata.aie);
        zze.aic = appMetadata.aic;
        zze.anL = appMetadata.aif == 0 ? null : Long.valueOf(appMetadata.aif);
        Pair<String, Boolean> zzlx = zzbse().zzlx(appMetadata.packageName);
        if (zzlx != null && !TextUtils.isEmpty((CharSequence) zzlx.first)) {
            zze.anI = (String) zzlx.first;
            zze.anJ = (Boolean) zzlx.second;
        } else if (!zzbrw().zzdn(this.mContext)) {
            String string = Settings.Secure.getString(this.mContext.getContentResolver(), "android_id");
            if (string == null) {
                zzbsd().zzbsx().log("null secure ID");
                string = "null";
            } else if (string.isEmpty()) {
                zzbsd().zzbsx().log("empty secure ID");
            }
            zze.anS = string;
        }
        zze.anD = zzbrw().zztg();
        zze.zzct = zzbrw().zzbso();
        zze.anF = Integer.valueOf((int) zzbrw().zzbsp());
        zze.anE = zzbrw().zzbsq();
        zze.anH = null;
        zze.anx = null;
        zze.any = null;
        zze.anz = null;
        zza zzln = zzbry().zzln(appMetadata.packageName);
        if (zzln == null) {
            zzln = new zza(this, appMetadata.packageName);
            zzln.zzky(zzbse().zzbtf());
            zzln.zzlb(appMetadata.aik);
            zzln.zzkz(appMetadata.aic);
            zzln.zzla(zzbse().zzly(appMetadata.packageName));
            zzln.zzaz(0);
            zzln.zzau(0);
            zzln.zzav(0);
            zzln.setAppVersion(appMetadata.aav);
            zzln.zzaw(appMetadata.aij);
            zzln.zzlc(appMetadata.aid);
            zzln.zzax(appMetadata.aie);
            zzln.zzay(appMetadata.aif);
            zzln.setMeasurementEnabled(appMetadata.aih);
            zzbry().zza(zzln);
        }
        zze.anK = zzln.zzawo();
        zze.aik = zzln.zzbpu();
        List<zzak> zzlm = zzbry().zzlm(appMetadata.packageName);
        zze.anw = new zzuh.zzg[zzlm.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < zzlm.size()) {
                zzuh.zzg zzg = new zzuh.zzg();
                zze.anw[i2] = zzg;
                zzg.name = zzlm.get(i2).mName;
                zzg.anW = Long.valueOf(zzlm.get(i2).amx);
                zzbrz().zza(zzg, zzlm.get(i2).zzcnn);
                i = i2 + 1;
            } else {
                try {
                    zzbry().zza(zzh, zzbry().zzb(zze));
                    return;
                } catch (IOException e) {
                    zzbsd().zzbsv().zzj("Data loss. Failed to insert raw event metadata", e);
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public boolean zza(int i, FileChannel fileChannel) {
        zzwu();
        if (fileChannel == null || !fileChannel.isOpen()) {
            zzbsd().zzbsv().log("Bad chanel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i);
        allocate.flip();
        try {
            fileChannel.truncate(0L);
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() == 4) {
                return true;
            }
            zzbsd().zzbsv().zzj("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            return true;
        } catch (IOException e) {
            zzbsd().zzbsv().zzj("Failed to write to channel", e);
            return false;
        }
    }

    @WorkerThread
    public byte[] zza(@NonNull EventParcel eventParcel, @Size(min = 1) String str) {
        long j;
        zzzg();
        zzwu();
        zzbua();
        zzab.zzy(eventParcel);
        zzab.zzhr(str);
        zzuh.zzd zzd = new zzuh.zzd();
        zzbry().beginTransaction();
        try {
            zza zzln = zzbry().zzln(str);
            if (zzln == null) {
                zzbsd().zzbtb().zzj("Log and bundle not available. package_name", str);
                return new byte[0];
            } else if (!zzln.zzbqb()) {
                zzbsd().zzbtb().zzj("Log and bundle disabled. package_name", str);
                byte[] bArr = new byte[0];
                zzbry().endTransaction();
                return bArr;
            } else {
                zzuh.zze zze = new zzuh.zze();
                zzd.ans = new zzuh.zze[]{zze};
                zze.anu = 1;
                zze.anC = "android";
                zze.zzck = zzln.zzsh();
                zze.aid = zzln.zzbpy();
                zze.aav = zzln.zzxc();
                zze.anP = Integer.valueOf((int) zzln.zzbpx());
                zze.anG = Long.valueOf(zzln.zzbpz());
                zze.aic = zzln.zzbps();
                zze.anL = Long.valueOf(zzln.zzbqa());
                Pair<String, Boolean> zzlx = zzbse().zzlx(zzln.zzsh());
                if (zzlx != null && !TextUtils.isEmpty((CharSequence) zzlx.first)) {
                    zze.anI = (String) zzlx.first;
                    zze.anJ = (Boolean) zzlx.second;
                }
                zze.anD = zzbrw().zztg();
                zze.zzct = zzbrw().zzbso();
                zze.anF = Integer.valueOf((int) zzbrw().zzbsp());
                zze.anE = zzbrw().zzbsq();
                zze.anK = zzln.zzawo();
                zze.aik = zzln.zzbpu();
                List<zzak> zzlm = zzbry().zzlm(zzln.zzsh());
                zze.anw = new zzuh.zzg[zzlm.size()];
                for (int i = 0; i < zzlm.size(); i++) {
                    zzuh.zzg zzg = new zzuh.zzg();
                    zze.anw[i] = zzg;
                    zzg.name = zzlm.get(i).mName;
                    zzg.anW = Long.valueOf(zzlm.get(i).amx);
                    zzbrz().zza(zzg, zzlm.get(i).zzcnn);
                }
                Bundle zzbss = eventParcel.aiI.zzbss();
                if ("_iap".equals(eventParcel.name)) {
                    zzbss.putLong("_c", 1);
                }
                zzbss.putString("_o", eventParcel.aiJ);
                zzi zzaq = zzbry().zzaq(str, eventParcel.name);
                if (zzaq == null) {
                    zzbry().zza(new zzi(str, eventParcel.name, 1, 0, eventParcel.aiK));
                    j = 0;
                } else {
                    j = zzaq.aiE;
                    zzbry().zza(zzaq.zzbj(eventParcel.aiK).zzbsr());
                }
                zzh zzh = new zzh(this, eventParcel.aiJ, str, eventParcel.name, eventParcel.aiK, j, zzbss);
                zzuh.zzb zzb = new zzuh.zzb();
                zze.anv = new zzuh.zzb[]{zzb};
                zzb.ano = Long.valueOf(zzh.pJ);
                zzb.name = zzh.mName;
                zzb.anp = Long.valueOf(zzh.aiA);
                zzb.ann = new zzuh.zzc[zzh.aiB.size()];
                Iterator<String> it = zzh.aiB.iterator();
                int i2 = 0;
                while (it.hasNext()) {
                    String next = it.next();
                    zzuh.zzc zzc = new zzuh.zzc();
                    zzb.ann[i2] = zzc;
                    zzc.name = next;
                    zzbrz().zza(zzc, zzh.aiB.get(next));
                    i2++;
                }
                zze.anO = zza(zzln.zzsh(), zze.anw, zze.anv);
                zze.any = zzb.ano;
                zze.anz = zzb.ano;
                long zzbpw = zzln.zzbpw();
                zze.anB = zzbpw != 0 ? Long.valueOf(zzbpw) : null;
                long zzbpv = zzln.zzbpv();
                if (zzbpv != 0) {
                    zzbpw = zzbpv;
                }
                zze.anA = zzbpw != 0 ? Long.valueOf(zzbpw) : null;
                zzln.zzbqf();
                zze.anM = Integer.valueOf((int) zzln.zzbqc());
                zze.anH = Long.valueOf(zzbsf().zzbpz());
                zze.anx = Long.valueOf(zzyw().currentTimeMillis());
                zze.anN = Boolean.TRUE;
                zzln.zzau(zze.any.longValue());
                zzln.zzav(zze.anz.longValue());
                zzbry().zza(zzln);
                zzbry().setTransactionSuccessful();
                zzbry().endTransaction();
                try {
                    byte[] bArr2 = new byte[zzd.aM()];
                    zzapo zzbe = zzapo.zzbe(bArr2);
                    zzd.zza(zzbe);
                    zzbe.az();
                    return zzbrz().zzj(bArr2);
                } catch (IOException e) {
                    zzbsd().zzbsv().zzj("Data loss. Failed to bundle and serialize", e);
                    return null;
                }
            }
        } finally {
            zzbry().endTransaction();
        }
    }

    public void zzas(boolean z) {
        zzbue();
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public void zzb(AppMetadata appMetadata, long j) {
        Bundle bundle = new Bundle();
        bundle.putLong("_c", 1);
        zzb(new EventParcel("_f", new EventParams(bundle), "auto", j), appMetadata);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:49:0x01d3=Splitter:B:49:0x01d3, B:78:0x02b4=Splitter:B:78:0x02b4} */
    @android.support.annotation.WorkerThread
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void zzb(com.google.android.gms.measurement.internal.EventParcel r19, com.google.android.gms.measurement.internal.AppMetadata r20) {
        /*
            r18 = this;
            long r16 = java.lang.System.nanoTime()
            r18.zzwu()
            r18.zzzg()
            r0 = r20
            java.lang.String r4 = r0.packageName
            com.google.android.gms.common.internal.zzab.zzhr(r4)
            r0 = r20
            java.lang.String r2 = r0.aic
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 == 0) goto L_0x001c
        L_0x001b:
            return
        L_0x001c:
            r0 = r20
            boolean r2 = r0.aih
            if (r2 != 0) goto L_0x002a
            r0 = r18
            r1 = r20
            r0.zze(r1)
            goto L_0x001b
        L_0x002a:
            com.google.android.gms.measurement.internal.zzv r2 = r18.zzbsa()
            r0 = r19
            java.lang.String r3 = r0.name
            boolean r2 = r2.zzax(r4, r3)
            if (r2 == 0) goto L_0x0059
            com.google.android.gms.measurement.internal.zzp r2 = r18.zzbsd()
            com.google.android.gms.measurement.internal.zzp$zza r2 = r2.zzbsx()
            java.lang.String r3 = "Dropping blacklisted event"
            r0 = r19
            java.lang.String r4 = r0.name
            r2.zzj(r3, r4)
            com.google.android.gms.measurement.internal.zzal r2 = r18.zzbrz()
            r3 = 11
            java.lang.String r4 = "_ev"
            r0 = r19
            java.lang.String r5 = r0.name
            r2.zze(r3, r4, r5)
            goto L_0x001b
        L_0x0059:
            com.google.android.gms.measurement.internal.zzp r2 = r18.zzbsd()
            r3 = 2
            boolean r2 = r2.zzaz(r3)
            if (r2 == 0) goto L_0x0073
            com.google.android.gms.measurement.internal.zzp r2 = r18.zzbsd()
            com.google.android.gms.measurement.internal.zzp$zza r2 = r2.zzbtc()
            java.lang.String r3 = "Logging event"
            r0 = r19
            r2.zzj(r3, r0)
        L_0x0073:
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()
            r2.beginTransaction()
            r0 = r19
            com.google.android.gms.measurement.internal.EventParams r2 = r0.aiI     // Catch:{ all -> 0x0204 }
            android.os.Bundle r14 = r2.zzbss()     // Catch:{ all -> 0x0204 }
            r0 = r18
            r1 = r20
            r0.zze(r1)     // Catch:{ all -> 0x0204 }
            java.lang.String r2 = "_iap"
            r0 = r19
            java.lang.String r3 = r0.name     // Catch:{ all -> 0x0204 }
            boolean r2 = r2.equals(r3)     // Catch:{ all -> 0x0204 }
            if (r2 != 0) goto L_0x00a1
            java.lang.String r2 = "ecommerce_purchase"
            r0 = r19
            java.lang.String r3 = r0.name     // Catch:{ all -> 0x0204 }
            boolean r2 = r2.equals(r3)     // Catch:{ all -> 0x0204 }
            if (r2 == 0) goto L_0x0163
        L_0x00a1:
            java.lang.String r2 = "currency"
            java.lang.String r5 = r14.getString(r2)     // Catch:{ all -> 0x0204 }
            java.lang.String r2 = "ecommerce_purchase"
            r0 = r19
            java.lang.String r3 = r0.name     // Catch:{ all -> 0x0204 }
            boolean r2 = r2.equals(r3)     // Catch:{ all -> 0x0204 }
            if (r2 == 0) goto L_0x01f4
            java.lang.String r2 = "value"
            double r2 = r14.getDouble(r2)     // Catch:{ all -> 0x0204 }
            r6 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r2 = r2 * r6
            r6 = 0
            int r6 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r6 != 0) goto L_0x00d2
            java.lang.String r2 = "value"
            long r2 = r14.getLong(r2)     // Catch:{ all -> 0x0204 }
            double r2 = (double) r2     // Catch:{ all -> 0x0204 }
            r6 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r2 = r2 * r6
        L_0x00d2:
            r6 = 4890909195324358656(0x43e0000000000000, double:9.223372036854776E18)
            int r6 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r6 > 0) goto L_0x01d3
            r6 = -4332462841530417152(0xc3e0000000000000, double:-9.223372036854776E18)
            int r6 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r6 < 0) goto L_0x01d3
            long r2 = java.lang.Math.round(r2)     // Catch:{ all -> 0x0204 }
            r8 = r2
        L_0x00e3:
            boolean r2 = android.text.TextUtils.isEmpty(r5)     // Catch:{ all -> 0x0204 }
            if (r2 != 0) goto L_0x0163
            java.util.Locale r2 = java.util.Locale.US     // Catch:{ all -> 0x0204 }
            java.lang.String r2 = r5.toUpperCase(r2)     // Catch:{ all -> 0x0204 }
            java.lang.String r3 = "[A-Z]{3}"
            boolean r3 = r2.matches(r3)     // Catch:{ all -> 0x0204 }
            if (r3 == 0) goto L_0x0163
            java.lang.String r3 = "_ltv_"
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ all -> 0x0204 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ all -> 0x0204 }
            int r5 = r2.length()     // Catch:{ all -> 0x0204 }
            if (r5 == 0) goto L_0x01fd
            java.lang.String r5 = r3.concat(r2)     // Catch:{ all -> 0x0204 }
        L_0x010b:
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzak r2 = r2.zzas(r4, r5)     // Catch:{ all -> 0x0204 }
            if (r2 == 0) goto L_0x011b
            java.lang.Object r3 = r2.zzcnn     // Catch:{ all -> 0x0204 }
            boolean r3 = r3 instanceof java.lang.Long     // Catch:{ all -> 0x0204 }
            if (r3 != 0) goto L_0x020d
        L_0x011b:
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzd r3 = r18.zzbsf()     // Catch:{ all -> 0x0204 }
            int r3 = r3.zzlh(r4)     // Catch:{ all -> 0x0204 }
            int r3 = r3 + -1
            r2.zzy(r4, r3)     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzak r3 = new com.google.android.gms.measurement.internal.zzak     // Catch:{ all -> 0x0204 }
            com.google.android.gms.common.util.zze r2 = r18.zzyw()     // Catch:{ all -> 0x0204 }
            long r6 = r2.currentTimeMillis()     // Catch:{ all -> 0x0204 }
            java.lang.Long r8 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x0204 }
            r3.<init>(r4, r5, r6, r8)     // Catch:{ all -> 0x0204 }
        L_0x013d:
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()     // Catch:{ all -> 0x0204 }
            boolean r2 = r2.zza(r3)     // Catch:{ all -> 0x0204 }
            if (r2 != 0) goto L_0x0163
            com.google.android.gms.measurement.internal.zzp r2 = r18.zzbsd()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzp$zza r2 = r2.zzbsv()     // Catch:{ all -> 0x0204 }
            java.lang.String r5 = "Too many unique user properties are set. Ignoring user property."
            java.lang.String r6 = r3.mName     // Catch:{ all -> 0x0204 }
            java.lang.Object r3 = r3.zzcnn     // Catch:{ all -> 0x0204 }
            r2.zze(r5, r6, r3)     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzal r2 = r18.zzbrz()     // Catch:{ all -> 0x0204 }
            r3 = 9
            r5 = 0
            r6 = 0
            r2.zze(r3, r5, r6)     // Catch:{ all -> 0x0204 }
        L_0x0163:
            r0 = r19
            java.lang.String r2 = r0.name     // Catch:{ all -> 0x0204 }
            boolean r9 = com.google.android.gms.measurement.internal.zzal.zzmj(r2)     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzal.zzam(r14)     // Catch:{ all -> 0x0204 }
            java.lang.String r2 = "_err"
            r0 = r19
            java.lang.String r3 = r0.name     // Catch:{ all -> 0x0204 }
            boolean r11 = r2.equals(r3)     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zze r5 = r18.zzbry()     // Catch:{ all -> 0x0204 }
            long r6 = r18.zzbtz()     // Catch:{ all -> 0x0204 }
            r10 = 0
            r8 = r4
            com.google.android.gms.measurement.internal.zze$zza r2 = r5.zza(r6, r8, r9, r10, r11)     // Catch:{ all -> 0x0204 }
            long r6 = r2.aiq     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzd r3 = r18.zzbsf()     // Catch:{ all -> 0x0204 }
            long r12 = r3.zzbqv()     // Catch:{ all -> 0x0204 }
            long r6 = r6 - r12
            r12 = 0
            int r3 = (r6 > r12 ? 1 : (r6 == r12 ? 0 : -1))
            if (r3 <= 0) goto L_0x0229
            r4 = 1000(0x3e8, double:4.94E-321)
            long r4 = r6 % r4
            r6 = 1
            int r3 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r3 != 0) goto L_0x01b4
            com.google.android.gms.measurement.internal.zzp r3 = r18.zzbsd()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzp$zza r3 = r3.zzbsv()     // Catch:{ all -> 0x0204 }
            java.lang.String r4 = "Data loss. Too many events logged. count"
            long r6 = r2.aiq     // Catch:{ all -> 0x0204 }
            java.lang.Long r2 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0204 }
            r3.zzj(r4, r2)     // Catch:{ all -> 0x0204 }
        L_0x01b4:
            com.google.android.gms.measurement.internal.zzal r2 = r18.zzbrz()     // Catch:{ all -> 0x0204 }
            r3 = 16
            java.lang.String r4 = "_ev"
            r0 = r19
            java.lang.String r5 = r0.name     // Catch:{ all -> 0x0204 }
            r2.zze(r3, r4, r5)     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()     // Catch:{ all -> 0x0204 }
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()
            r2.endTransaction()
            goto L_0x001b
        L_0x01d3:
            com.google.android.gms.measurement.internal.zzp r4 = r18.zzbsd()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzp$zza r4 = r4.zzbsx()     // Catch:{ all -> 0x0204 }
            java.lang.String r5 = "Data lost. Currency value is too big"
            java.lang.Double r2 = java.lang.Double.valueOf(r2)     // Catch:{ all -> 0x0204 }
            r4.zzj(r5, r2)     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()     // Catch:{ all -> 0x0204 }
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()
            r2.endTransaction()
            goto L_0x001b
        L_0x01f4:
            java.lang.String r2 = "value"
            long r2 = r14.getLong(r2)     // Catch:{ all -> 0x0204 }
            r8 = r2
            goto L_0x00e3
        L_0x01fd:
            java.lang.String r5 = new java.lang.String     // Catch:{ all -> 0x0204 }
            r5.<init>(r3)     // Catch:{ all -> 0x0204 }
            goto L_0x010b
        L_0x0204:
            r2 = move-exception
            com.google.android.gms.measurement.internal.zze r3 = r18.zzbry()
            r3.endTransaction()
            throw r2
        L_0x020d:
            java.lang.Object r2 = r2.zzcnn     // Catch:{ all -> 0x0204 }
            java.lang.Long r2 = (java.lang.Long) r2     // Catch:{ all -> 0x0204 }
            long r10 = r2.longValue()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzak r3 = new com.google.android.gms.measurement.internal.zzak     // Catch:{ all -> 0x0204 }
            com.google.android.gms.common.util.zze r2 = r18.zzyw()     // Catch:{ all -> 0x0204 }
            long r6 = r2.currentTimeMillis()     // Catch:{ all -> 0x0204 }
            long r8 = r8 + r10
            java.lang.Long r8 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x0204 }
            r3.<init>(r4, r5, r6, r8)     // Catch:{ all -> 0x0204 }
            goto L_0x013d
        L_0x0229:
            if (r9 == 0) goto L_0x0278
            long r6 = r2.aip     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzd r3 = r18.zzbsf()     // Catch:{ all -> 0x0204 }
            long r8 = r3.zzbqw()     // Catch:{ all -> 0x0204 }
            long r6 = r6 - r8
            r8 = 0
            int r3 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r3 <= 0) goto L_0x0278
            r4 = 1000(0x3e8, double:4.94E-321)
            long r4 = r6 % r4
            r6 = 1
            int r3 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r3 != 0) goto L_0x0259
            com.google.android.gms.measurement.internal.zzp r3 = r18.zzbsd()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzp$zza r3 = r3.zzbsv()     // Catch:{ all -> 0x0204 }
            java.lang.String r4 = "Data loss. Too many public events logged. count"
            long r6 = r2.aip     // Catch:{ all -> 0x0204 }
            java.lang.Long r2 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0204 }
            r3.zzj(r4, r2)     // Catch:{ all -> 0x0204 }
        L_0x0259:
            com.google.android.gms.measurement.internal.zzal r2 = r18.zzbrz()     // Catch:{ all -> 0x0204 }
            r3 = 16
            java.lang.String r4 = "_ev"
            r0 = r19
            java.lang.String r5 = r0.name     // Catch:{ all -> 0x0204 }
            r2.zze(r3, r4, r5)     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()     // Catch:{ all -> 0x0204 }
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()
            r2.endTransaction()
            goto L_0x001b
        L_0x0278:
            if (r11 == 0) goto L_0x02b4
            long r6 = r2.ais     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzd r3 = r18.zzbsf()     // Catch:{ all -> 0x0204 }
            long r8 = r3.zzbqx()     // Catch:{ all -> 0x0204 }
            long r6 = r6 - r8
            r8 = 0
            int r3 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r3 <= 0) goto L_0x02b4
            r4 = 1
            int r3 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r3 != 0) goto L_0x02a4
            com.google.android.gms.measurement.internal.zzp r3 = r18.zzbsd()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzp$zza r3 = r3.zzbsv()     // Catch:{ all -> 0x0204 }
            java.lang.String r4 = "Too many error events logged. count"
            long r6 = r2.ais     // Catch:{ all -> 0x0204 }
            java.lang.Long r2 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0204 }
            r3.zzj(r4, r2)     // Catch:{ all -> 0x0204 }
        L_0x02a4:
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()     // Catch:{ all -> 0x0204 }
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()
            r2.endTransaction()
            goto L_0x001b
        L_0x02b4:
            com.google.android.gms.measurement.internal.zzal r2 = r18.zzbrz()     // Catch:{ all -> 0x0204 }
            java.lang.String r3 = "_o"
            r0 = r19
            java.lang.String r5 = r0.aiJ     // Catch:{ all -> 0x0204 }
            r2.zza(r14, r3, r5)     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()     // Catch:{ all -> 0x0204 }
            long r2 = r2.zzlo(r4)     // Catch:{ all -> 0x0204 }
            r6 = 0
            int r5 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r5 <= 0) goto L_0x02e0
            com.google.android.gms.measurement.internal.zzp r5 = r18.zzbsd()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzp$zza r5 = r5.zzbsx()     // Catch:{ all -> 0x0204 }
            java.lang.String r6 = "Data lost. Too many events stored on disk, deleted"
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x0204 }
            r5.zzj(r6, r2)     // Catch:{ all -> 0x0204 }
        L_0x02e0:
            com.google.android.gms.measurement.internal.zzh r5 = new com.google.android.gms.measurement.internal.zzh     // Catch:{ all -> 0x0204 }
            r0 = r19
            java.lang.String r7 = r0.aiJ     // Catch:{ all -> 0x0204 }
            r0 = r19
            java.lang.String r9 = r0.name     // Catch:{ all -> 0x0204 }
            r0 = r19
            long r10 = r0.aiK     // Catch:{ all -> 0x0204 }
            r12 = 0
            r6 = r18
            r8 = r4
            r5.<init>(r6, r7, r8, r9, r10, r12, r14)     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()     // Catch:{ all -> 0x0204 }
            java.lang.String r3 = r5.mName     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzi r2 = r2.zzaq(r4, r3)     // Catch:{ all -> 0x0204 }
            if (r2 != 0) goto L_0x03ac
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()     // Catch:{ all -> 0x0204 }
            long r2 = r2.zzlu(r4)     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzd r6 = r18.zzbsf()     // Catch:{ all -> 0x0204 }
            int r6 = r6.zzbqu()     // Catch:{ all -> 0x0204 }
            long r6 = (long) r6     // Catch:{ all -> 0x0204 }
            int r2 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r2 < 0) goto L_0x0346
            com.google.android.gms.measurement.internal.zzp r2 = r18.zzbsd()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzp$zza r2 = r2.zzbsv()     // Catch:{ all -> 0x0204 }
            java.lang.String r3 = "Too many event names used, ignoring event. name, supported count"
            java.lang.String r4 = r5.mName     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzd r5 = r18.zzbsf()     // Catch:{ all -> 0x0204 }
            int r5 = r5.zzbqu()     // Catch:{ all -> 0x0204 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0204 }
            r2.zze(r3, r4, r5)     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzal r2 = r18.zzbrz()     // Catch:{ all -> 0x0204 }
            r3 = 8
            r4 = 0
            r5 = 0
            r2.zze(r3, r4, r5)     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()
            r2.endTransaction()
            goto L_0x001b
        L_0x0346:
            com.google.android.gms.measurement.internal.zzi r7 = new com.google.android.gms.measurement.internal.zzi     // Catch:{ all -> 0x0204 }
            java.lang.String r9 = r5.mName     // Catch:{ all -> 0x0204 }
            r10 = 0
            r12 = 0
            long r14 = r5.pJ     // Catch:{ all -> 0x0204 }
            r8 = r4
            r7.<init>(r8, r9, r10, r12, r14)     // Catch:{ all -> 0x0204 }
        L_0x0354:
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()     // Catch:{ all -> 0x0204 }
            r2.zza(r7)     // Catch:{ all -> 0x0204 }
            r0 = r18
            r1 = r20
            r0.zza(r5, r1)     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()     // Catch:{ all -> 0x0204 }
            r2.setTransactionSuccessful()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzp r2 = r18.zzbsd()     // Catch:{ all -> 0x0204 }
            r3 = 2
            boolean r2 = r2.zzaz(r3)     // Catch:{ all -> 0x0204 }
            if (r2 == 0) goto L_0x0381
            com.google.android.gms.measurement.internal.zzp r2 = r18.zzbsd()     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzp$zza r2 = r2.zzbtc()     // Catch:{ all -> 0x0204 }
            java.lang.String r3 = "Event recorded"
            r2.zzj(r3, r5)     // Catch:{ all -> 0x0204 }
        L_0x0381:
            com.google.android.gms.measurement.internal.zze r2 = r18.zzbry()
            r2.endTransaction()
            r18.zzbue()
            com.google.android.gms.measurement.internal.zzp r2 = r18.zzbsd()
            com.google.android.gms.measurement.internal.zzp$zza r2 = r2.zzbtc()
            java.lang.String r3 = "Background event processing time, ms"
            long r4 = java.lang.System.nanoTime()
            long r4 = r4 - r16
            r6 = 500000(0x7a120, double:2.47033E-318)
            long r4 = r4 + r6
            r6 = 1000000(0xf4240, double:4.940656E-318)
            long r4 = r4 / r6
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            r2.zzj(r3, r4)
            goto L_0x001b
        L_0x03ac:
            long r6 = r2.aiE     // Catch:{ all -> 0x0204 }
            r0 = r18
            com.google.android.gms.measurement.internal.zzh r5 = r5.zza(r0, r6)     // Catch:{ all -> 0x0204 }
            long r6 = r5.pJ     // Catch:{ all -> 0x0204 }
            com.google.android.gms.measurement.internal.zzi r7 = r2.zzbj(r6)     // Catch:{ all -> 0x0204 }
            goto L_0x0354
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzx.zzb(com.google.android.gms.measurement.internal.EventParcel, com.google.android.gms.measurement.internal.AppMetadata):void");
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public void zzb(EventParcel eventParcel, String str) {
        zza zzln = zzbry().zzln(str);
        if (zzln == null || TextUtils.isEmpty(zzln.zzxc())) {
            zzbsd().zzbtb().zzj("No app data available; dropping event", str);
            return;
        }
        try {
            String str2 = getContext().getPackageManager().getPackageInfo(str, 0).versionName;
            if (zzln.zzxc() != null && !zzln.zzxc().equals(str2)) {
                zzbsd().zzbsx().zzj("App version does not match; dropping event", str);
                return;
            }
        } catch (PackageManager.NameNotFoundException e) {
            if (!"_ui".equals(eventParcel.name)) {
                zzbsd().zzbsx().zzj("Could not find package", str);
            }
        }
        EventParcel eventParcel2 = eventParcel;
        zzb(eventParcel2, new AppMetadata(str, zzln.zzbps(), zzln.zzxc(), zzln.zzbpx(), zzln.zzbpy(), zzln.zzbpz(), zzln.zzbqa(), null, zzln.zzbqb(), false, zzln.zzbpu()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzal.zza(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.measurement.internal.zzal.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuf$zzc):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuh$zze):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuh$zza[]):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuh$zzb[]):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuh$zzc[]):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuh$zzg[]):void
      com.google.android.gms.measurement.internal.zzal.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.String, int, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    @WorkerThread
    public void zzb(UserAttributeParcel userAttributeParcel, AppMetadata appMetadata) {
        zzwu();
        zzzg();
        if (!TextUtils.isEmpty(appMetadata.aic)) {
            if (!appMetadata.aih) {
                zze(appMetadata);
                return;
            }
            int zzmn = zzbrz().zzmn(userAttributeParcel.name);
            if (zzmn != 0) {
                zzbrz().zze(zzmn, "_ev", zzbrz().zza(userAttributeParcel.name, zzbsf().zzbqo(), true));
                return;
            }
            int zzm = zzbrz().zzm(userAttributeParcel.name, userAttributeParcel.getValue());
            if (zzm != 0) {
                zzbrz().zze(zzm, "_ev", zzbrz().zza(userAttributeParcel.name, zzbsf().zzbqo(), true));
                return;
            }
            Object zzn = zzbrz().zzn(userAttributeParcel.name, userAttributeParcel.getValue());
            if (zzn != null) {
                zzak zzak = new zzak(appMetadata.packageName, userAttributeParcel.name, userAttributeParcel.amt, zzn);
                zzbsd().zzbtb().zze("Setting user property", zzak.mName, zzn);
                zzbry().beginTransaction();
                try {
                    zze(appMetadata);
                    boolean zza2 = zzbry().zza(zzak);
                    zzbry().setTransactionSuccessful();
                    if (zza2) {
                        zzbsd().zzbtb().zze("User property set", zzak.mName, zzak.zzcnn);
                    } else {
                        zzbsd().zzbsv().zze("Too many unique user properties are set. Ignoring user property.", zzak.mName, zzak.zzcnn);
                        zzbrz().zze(9, null, null);
                    }
                } finally {
                    zzbry().endTransaction();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void zzb(zzaa zzaa) {
        this.aln++;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public void zzb(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        boolean z = false;
        zzwu();
        zzzg();
        zzab.zzhr(str);
        if (bArr == null) {
            bArr = new byte[0];
        }
        zzbry().beginTransaction();
        try {
            zza zzln = zzbry().zzln(str);
            boolean z2 = (i == 200 || i == 204 || i == 304) && th == null;
            if (zzln == null) {
                zzbsd().zzbsx().zzj("App does not exist in onConfigFetched", str);
            } else if (z2 || i == 404) {
                List list = map != null ? map.get("Last-Modified") : null;
                String str2 = (list == null || list.size() <= 0) ? null : (String) list.get(0);
                if (i == 404 || i == 304) {
                    if (zzbsa().zzmb(str) == null && !zzbsa().zzb(str, null, null)) {
                        zzbry().endTransaction();
                        return;
                    }
                } else if (!zzbsa().zzb(str, bArr, str2)) {
                    zzbry().endTransaction();
                    return;
                }
                zzln.zzba(zzyw().currentTimeMillis());
                zzbry().zza(zzln);
                if (i == 404) {
                    zzbsd().zzbsx().log("Config not found. Using empty config");
                } else {
                    zzbsd().zzbtc().zze("Successfully fetched config. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
                }
                if (!zzbts().zzadj() || !zzbud()) {
                    zzbue();
                } else {
                    zzbuc();
                }
            } else {
                zzln.zzbb(zzyw().currentTimeMillis());
                zzbry().zza(zzln);
                zzbsd().zzbtc().zze("Fetching config failed. code, error", Integer.valueOf(i), th);
                zzbsa().zzmd(str);
                zzbse().ajZ.set(zzyw().currentTimeMillis());
                if (i == 503 || i == 429) {
                    z = true;
                }
                if (z) {
                    zzbse().aka.set(zzyw().currentTimeMillis());
                }
                zzbue();
            }
            zzbry().setTransactionSuccessful();
        } finally {
            zzbry().endTransaction();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean zzbl(long j) {
        return zzi(null, j);
    }

    public zzc zzbrt() {
        zza((zzaa) this.alg);
        return this.alg;
    }

    public zzac zzbru() {
        zza((zzaa) this.alc);
        return this.alc;
    }

    public zzn zzbrv() {
        zza((zzaa) this.ald);
        return this.ald;
    }

    public zzg zzbrw() {
        zza((zzaa) this.alb);
        return this.alb;
    }

    public zzad zzbrx() {
        zza((zzaa) this.ala);
        return this.ala;
    }

    public zze zzbry() {
        zza((zzaa) this.akY);
        return this.akY;
    }

    public zzal zzbrz() {
        zza(this.akX);
        return this.akX;
    }

    public zzv zzbsa() {
        zza((zzaa) this.akV);
        return this.akV;
    }

    public zzaf zzbsb() {
        zza((zzaa) this.akU);
        return this.akU;
    }

    public zzw zzbsc() {
        zza((zzaa) this.akT);
        return this.akT;
    }

    public zzp zzbsd() {
        zza((zzaa) this.akS);
        return this.akS;
    }

    public zzt zzbse() {
        zza((zzz) this.akR);
        return this.akR;
    }

    public zzd zzbsf() {
        return this.akQ;
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public boolean zzbto() {
        zzzg();
        zzwu();
        if (this.alj == null) {
            this.alj = Boolean.valueOf(zzbrz().zzeo("android.permission.INTERNET") && zzbrz().zzeo("android.permission.ACCESS_NETWORK_STATE") && zzu.zzav(getContext()) && zzae.zzaw(getContext()));
            if (this.alj.booleanValue() && !zzbsf().zzabc()) {
                this.alj = Boolean.valueOf(zzbrz().zzmq(zzbrv().zzbps()));
            }
        }
        return this.alj.booleanValue();
    }

    public zzp zzbtp() {
        if (this.akS == null || !this.akS.isInitialized()) {
            return null;
        }
        return this.akS;
    }

    /* access modifiers changed from: package-private */
    public zzw zzbtq() {
        return this.akT;
    }

    public AppMeasurement zzbtr() {
        return this.akW;
    }

    public zzq zzbts() {
        zza((zzaa) this.akZ);
        return this.akZ;
    }

    public zzr zzbtt() {
        if (this.ale != null) {
            return this.ale;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    public zzai zzbtu() {
        zza((zzaa) this.alf);
        return this.alf;
    }

    /* access modifiers changed from: package-private */
    public FileChannel zzbtv() {
        return this.all;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public void zzbtw() {
        zzwu();
        zzzg();
        if (zzbui() && zzbtx()) {
            zzu(zza(zzbtv()), zzbrv().zzbst());
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public boolean zzbtx() {
        zzwu();
        try {
            this.all = new RandomAccessFile(new File(getContext().getFilesDir(), this.akY.zzaab()), "rw").getChannel();
            this.alk = this.all.tryLock();
            if (this.alk != null) {
                zzbsd().zzbtc().log("Storage concurrent access okay");
                return true;
            }
            zzbsd().zzbsv().log("Storage concurrent data access panic");
            return false;
        } catch (FileNotFoundException e) {
            zzbsd().zzbsv().zzj("Failed to acquire storage lock", e);
        } catch (IOException e2) {
            zzbsd().zzbsv().zzj("Failed to access storage lock file", e2);
        }
    }

    /* access modifiers changed from: protected */
    public boolean zzbty() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public long zzbtz() {
        return ((((zzyw().currentTimeMillis() + zzbse().zzbtg()) / 1000) / 60) / 60) / 24;
    }

    /* access modifiers changed from: package-private */
    public void zzbua() {
        if (!zzbsf().zzabc()) {
            throw new IllegalStateException("Unexpected call on client side");
        }
    }

    @WorkerThread
    public void zzbuc() {
        zza zzln;
        String str;
        List<Pair<zzuh.zze, Long>> list;
        ArrayMap arrayMap = null;
        zzwu();
        zzzg();
        if (!zzbsf().zzabc()) {
            Boolean zzbtj = zzbse().zzbtj();
            if (zzbtj == null) {
                zzbsd().zzbsx().log("Upload data called on the client side before use of service was decided");
                return;
            } else if (zzbtj.booleanValue()) {
                zzbsd().zzbsv().log("Upload called in the client side when service should be used");
                return;
            }
        }
        if (zzbub()) {
            zzbsd().zzbsx().log("Uploading requested multiple times");
        } else if (!zzbts().zzadj()) {
            zzbsd().zzbsx().log("Network not connected, ignoring upload request");
            zzbue();
        } else {
            long currentTimeMillis = zzyw().currentTimeMillis();
            zzbl(currentTimeMillis - zzbsf().zzbrl());
            long j = zzbse().ajY.get();
            if (j != 0) {
                zzbsd().zzbtb().zzj("Uploading events. Elapsed time since last upload attempt (ms)", Long.valueOf(Math.abs(currentTimeMillis - j)));
            }
            String zzbsg = zzbry().zzbsg();
            if (!TextUtils.isEmpty(zzbsg)) {
                List<Pair<zzuh.zze, Long>> zzn = zzbry().zzn(zzbsg, zzbsf().zzlj(zzbsg), zzbsf().zzlk(zzbsg));
                if (!zzn.isEmpty()) {
                    Iterator<Pair<zzuh.zze, Long>> it = zzn.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            str = null;
                            break;
                        }
                        zzuh.zze zze = (zzuh.zze) it.next().first;
                        if (!TextUtils.isEmpty(zze.anI)) {
                            str = zze.anI;
                            break;
                        }
                    }
                    if (str != null) {
                        int i = 0;
                        while (true) {
                            if (i >= zzn.size()) {
                                break;
                            }
                            zzuh.zze zze2 = (zzuh.zze) zzn.get(i).first;
                            if (!TextUtils.isEmpty(zze2.anI) && !zze2.anI.equals(str)) {
                                list = zzn.subList(0, i);
                                break;
                            }
                            i++;
                        }
                    }
                    list = zzn;
                    zzuh.zzd zzd = new zzuh.zzd();
                    zzd.ans = new zzuh.zze[list.size()];
                    ArrayList arrayList = new ArrayList(list.size());
                    for (int i2 = 0; i2 < zzd.ans.length; i2++) {
                        zzd.ans[i2] = (zzuh.zze) list.get(i2).first;
                        arrayList.add((Long) list.get(i2).second);
                        zzd.ans[i2].anH = Long.valueOf(zzbsf().zzbpz());
                        zzd.ans[i2].anx = Long.valueOf(currentTimeMillis);
                        zzd.ans[i2].anN = Boolean.valueOf(zzbsf().zzabc());
                    }
                    String zzb = zzbsd().zzaz(2) ? zzal.zzb(zzd) : null;
                    byte[] zza2 = zzbrz().zza(zzd);
                    String zzbrk = zzbsf().zzbrk();
                    try {
                        URL url = new URL(zzbrk);
                        zzad(arrayList);
                        zzbse().ajZ.set(currentTimeMillis);
                        String str2 = "?";
                        if (zzd.ans.length > 0) {
                            str2 = zzd.ans[0].zzck;
                        }
                        zzbsd().zzbtc().zzd("Uploading data. app, uncompressed size, data", str2, Integer.valueOf(zza2.length), zzb);
                        zzbts().zza(zzbsg, url, zza2, null, new zzq.zza() {
                            public void zza(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
                                zzx.this.zza(i, th, bArr);
                            }
                        });
                    } catch (MalformedURLException e) {
                        zzbsd().zzbsv().zzj("Failed to parse upload URL. Not uploading", zzbrk);
                    }
                }
            } else {
                String zzbi = zzbry().zzbi(currentTimeMillis - zzbsf().zzbrl());
                if (!TextUtils.isEmpty(zzbi) && (zzln = zzbry().zzln(zzbi)) != null) {
                    String zzap = zzbsf().zzap(zzln.zzbps(), zzln.zzawo());
                    try {
                        URL url2 = new URL(zzap);
                        zzbsd().zzbtc().zzj("Fetching remote configuration", zzln.zzsh());
                        zzug.zzb zzmb = zzbsa().zzmb(zzln.zzsh());
                        String zzmc = zzbsa().zzmc(zzln.zzsh());
                        if (zzmb != null && !TextUtils.isEmpty(zzmc)) {
                            arrayMap = new ArrayMap();
                            arrayMap.put("If-Modified-Since", zzmc);
                        }
                        zzbts().zza(zzbi, url2, arrayMap, new zzq.zza() {
                            public void zza(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
                                zzx.this.zzb(str, i, th, bArr, map);
                            }
                        });
                    } catch (MalformedURLException e2) {
                        zzbsd().zzbsv().zzj("Failed to parse config URL. Not fetching", zzap);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void zzbug() {
        this.alo++;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public void zzbuh() {
        zzwu();
        zzzg();
        if (!this.ali) {
            zzbsd().zzbta().log("This instance being marked as an uploader");
            zzbtw();
        }
        this.ali = true;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public boolean zzbui() {
        zzwu();
        zzzg();
        return this.ali || zzbty();
    }

    /* access modifiers changed from: package-private */
    public void zzc(AppMetadata appMetadata) {
        zzwu();
        zzzg();
        zzab.zzhr(appMetadata.packageName);
        zze(appMetadata);
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public void zzc(AppMetadata appMetadata, long j) {
        Bundle bundle = new Bundle();
        bundle.putLong("_et", 1);
        zzb(new EventParcel("_e", new EventParams(bundle), "auto", j), appMetadata);
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public void zzc(UserAttributeParcel userAttributeParcel, AppMetadata appMetadata) {
        zzwu();
        zzzg();
        if (!TextUtils.isEmpty(appMetadata.aic)) {
            if (!appMetadata.aih) {
                zze(appMetadata);
                return;
            }
            zzbsd().zzbtb().zzj("Removing user property", userAttributeParcel.name);
            zzbry().beginTransaction();
            try {
                zze(appMetadata);
                zzbry().zzar(appMetadata.packageName, userAttributeParcel.name);
                zzbry().setTransactionSuccessful();
                zzbsd().zzbtb().zzj("User property removed", userAttributeParcel.name);
            } finally {
                zzbry().endTransaction();
            }
        }
    }

    @WorkerThread
    public void zzd(AppMetadata appMetadata) {
        zzwu();
        zzzg();
        zzab.zzy(appMetadata);
        zzab.zzhr(appMetadata.packageName);
        if (!TextUtils.isEmpty(appMetadata.aic)) {
            if (!appMetadata.aih) {
                zze(appMetadata);
                return;
            }
            long currentTimeMillis = zzyw().currentTimeMillis();
            zzbry().beginTransaction();
            try {
                zza(appMetadata, currentTimeMillis);
                zze(appMetadata);
                if (zzbry().zzaq(appMetadata.packageName, "_f") == null) {
                    zzb(new UserAttributeParcel("_fot", currentTimeMillis, Long.valueOf((1 + (currentTimeMillis / 3600000)) * 3600000), "auto"), appMetadata);
                    zzb(appMetadata, currentTimeMillis);
                    zzc(appMetadata, currentTimeMillis);
                } else if (appMetadata.aii) {
                    zzd(appMetadata, currentTimeMillis);
                }
                zzbry().setTransactionSuccessful();
            } finally {
                zzbry().endTransaction();
            }
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public void zzd(AppMetadata appMetadata, long j) {
        zzb(new EventParcel("_cd", new EventParams(new Bundle()), "auto", j), appMetadata);
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public boolean zzu(int i, int i2) {
        zzwu();
        if (i > i2) {
            zzbsd().zzbsv().zze("Panic: can't downgrade version. Previous, current version", Integer.valueOf(i), Integer.valueOf(i2));
            return false;
        }
        if (i < i2) {
            if (zza(i2, zzbtv())) {
                zzbsd().zzbtc().zze("Storage version upgraded. Previous, current version", Integer.valueOf(i), Integer.valueOf(i2));
            } else {
                zzbsd().zzbsv().zze("Storage version upgrade failed. Previous, current version", Integer.valueOf(i), Integer.valueOf(i2));
                return false;
            }
        }
        return true;
    }

    @WorkerThread
    public void zzwu() {
        zzbsc().zzwu();
    }

    /* access modifiers changed from: package-private */
    public void zzyv() {
        if (zzbsf().zzabc()) {
            throw new IllegalStateException("Unexpected call on package side");
        }
    }

    public com.google.android.gms.common.util.zze zzyw() {
        return this.zzaoc;
    }

    /* access modifiers changed from: package-private */
    public void zzzg() {
        if (!this.zzcwq) {
            throw new IllegalStateException("AppMeasurement is not initialized");
        }
    }
}
