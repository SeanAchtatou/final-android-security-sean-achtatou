package com.google.android.gms.internal;

import java.lang.reflect.Type;

public interface zzand<T> {
    zzamv zza(Object obj, Type type, zzanc zzanc);
}
