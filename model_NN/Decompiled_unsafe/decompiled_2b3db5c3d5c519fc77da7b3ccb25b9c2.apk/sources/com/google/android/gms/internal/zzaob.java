package com.google.android.gms.internal;

public final class zzaob implements zzani {
    private final zzanp bdR;

    public zzaob(zzanp zzanp) {
        this.bdR = zzanp;
    }

    static zzanh<?> zza(zzanp zzanp, zzamp zzamp, zzaol<?> zzaol, zzanj zzanj) {
        Class<?> value = zzanj.value();
        if (zzanh.class.isAssignableFrom(value)) {
            return (zzanh) zzanp.zzb(zzaol.zzr(value)).zzczu();
        }
        if (zzani.class.isAssignableFrom(value)) {
            return ((zzani) zzanp.zzb(zzaol.zzr(value)).zzczu()).zza(zzamp, zzaol);
        }
        throw new IllegalArgumentException("@JsonAdapter value must be TypeAdapter or TypeAdapterFactory reference.");
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [com.google.android.gms.internal.zzaol<T>, com.google.android.gms.internal.zzaol] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> com.google.android.gms.internal.zzanh<T> zza(com.google.android.gms.internal.zzamp r3, com.google.android.gms.internal.zzaol<T> r4) {
        /*
            r2 = this;
            java.lang.Class r0 = r4.m()
            java.lang.Class<com.google.android.gms.internal.zzanj> r1 = com.google.android.gms.internal.zzanj.class
            java.lang.annotation.Annotation r0 = r0.getAnnotation(r1)
            com.google.android.gms.internal.zzanj r0 = (com.google.android.gms.internal.zzanj) r0
            if (r0 != 0) goto L_0x0010
            r0 = 0
        L_0x000f:
            return r0
        L_0x0010:
            com.google.android.gms.internal.zzanp r1 = r2.bdR
            com.google.android.gms.internal.zzanh r0 = zza(r1, r3, r4, r0)
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzaob.zza(com.google.android.gms.internal.zzamp, com.google.android.gms.internal.zzaol):com.google.android.gms.internal.zzanh");
    }
}
