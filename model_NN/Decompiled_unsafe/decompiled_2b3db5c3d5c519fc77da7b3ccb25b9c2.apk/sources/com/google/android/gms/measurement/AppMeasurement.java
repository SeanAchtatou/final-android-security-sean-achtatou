package com.google.android.gms.measurement;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Keep;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.Size;
import android.support.annotation.WorkerThread;
import com.facebook.AccessToken;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zzf;
import com.google.android.gms.measurement.internal.UserAttributeParcel;
import com.google.android.gms.measurement.internal.zzx;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Deprecated
public class AppMeasurement {
    private final zzx ahD;

    public static final class zza {
        public static final Map<String, String> ahE = zzf.zzb(new String[]{"app_clear_data", "app_exception", "app_uninstall", "app_update", "firebase_campaign", "error", "first_open", "in_app_purchase", "notification_dismiss", "notification_foreground", "notification_open", "notification_receive", "os_update", "session_start", "user_engagement"}, new String[]{"_cd", "_ae", "_ui", "_au", "_cmp", "_err", "_f", "_iap", "_nd", "_nf", "_no", "_nr", "_ou", "_s", "_e"});
    }

    public interface zzb {
        @WorkerThread
        void zzb(String str, String str2, Bundle bundle, long j);
    }

    public interface zzc {
        @WorkerThread
        void zzc(String str, String str2, Bundle bundle, long j);
    }

    public static final class zzd {
        public static final Map<String, String> ahF = zzf.zzb(new String[]{"firebase_conversion", "engagement_time_msec", "firebase_error", "error_value", "firebase_event_origin", "message_device_time", "message_id", "message_name", "message_time", "previous_app_version", "previous_os_version", "topic"}, new String[]{"_c", "_et", "_err", "_ev", "_o", "_ndt", "_nmid", "_nmn", "_nmt", "_pv", "_po", "_nt"});
    }

    public static final class zze {
        public static final Map<String, String> ahG = zzf.zzb(new String[]{"firebase_last_notification", "first_open_time", "last_deep_link_referrer", AccessToken.USER_ID_KEY}, new String[]{"_ln", "_fot", "_ldl", "_id"});
    }

    public AppMeasurement(zzx zzx) {
        zzab.zzy(zzx);
        this.ahD = zzx;
    }

    @Keep
    @Deprecated
    public static AppMeasurement getInstance(Context context) {
        return zzx.zzdo(context).zzbtr();
    }

    private void zzc(String str, String str2, Object obj) {
        this.ahD.zzbru().zzd(str, str2, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzal.zza(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.measurement.internal.zzal.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuf$zzc):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuh$zze):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuh$zza[]):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuh$zzb[]):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuh$zzc[]):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuh$zzg[]):void
      com.google.android.gms.measurement.internal.zzal.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.String, int, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzac.zza(java.lang.String, java.lang.String, android.os.Bundle, boolean):void
     arg types: [java.lang.String, java.lang.String, android.os.Bundle, int]
     candidates:
      com.google.android.gms.measurement.internal.zzac.zza(java.lang.String, java.lang.String, java.lang.Object, long):void
      com.google.android.gms.measurement.internal.zzac.zza(java.lang.String, java.lang.String, long, java.lang.Object):void
      com.google.android.gms.measurement.internal.zzac.zza(java.lang.String, java.lang.String, android.os.Bundle, boolean):void */
    @Deprecated
    public void logEvent(@Size(max = 32, min = 1) @NonNull String str, Bundle bundle) {
        int zzmk;
        if (bundle == null) {
            bundle = new Bundle();
        }
        if ((this.ahD.zzbsf().zzabc() || !"_iap".equals(str)) && (zzmk = this.ahD.zzbrz().zzmk(str)) != 0) {
            this.ahD.zzbrz().zze(zzmk, "_ev", this.ahD.zzbrz().zza(str, this.ahD.zzbsf().zzbqn(), true));
        } else {
            this.ahD.zzbru().zza("app", str, bundle, true);
        }
    }

    @Deprecated
    public void setMeasurementEnabled(boolean z) {
        this.ahD.zzbru().setMeasurementEnabled(z);
    }

    @Deprecated
    public void setMinimumSessionDuration(long j) {
        this.ahD.zzbru().setMinimumSessionDuration(j);
    }

    @Deprecated
    public void setSessionTimeoutDuration(long j) {
        this.ahD.zzbru().setSessionTimeoutDuration(j);
    }

    @Deprecated
    public void setUserId(String str) {
        zzb("app", "_id", str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzal.zza(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.measurement.internal.zzal.zza(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuf$zzc):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuh$zze):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuh$zza[]):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuh$zzb[]):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuh$zzc[]):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.StringBuilder, int, com.google.android.gms.internal.zzuh$zzg[]):void
      com.google.android.gms.measurement.internal.zzal.zza(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.zzal.zza(java.lang.String, int, boolean):java.lang.String */
    @Deprecated
    public void setUserProperty(@Size(max = 24, min = 1) @NonNull String str, @Nullable @Size(max = 36) String str2) {
        int zzmm = this.ahD.zzbrz().zzmm(str);
        if (zzmm != 0) {
            this.ahD.zzbrz().zze(zzmm, "_ev", this.ahD.zzbrz().zza(str, this.ahD.zzbsf().zzbqo(), true));
            return;
        }
        zzb("app", str, str2);
    }

    @WorkerThread
    public void zza(zzb zzb2) {
        this.ahD.zzbru().zza(zzb2);
    }

    @WorkerThread
    public void zza(zzc zzc2) {
        this.ahD.zzbru().zza(zzc2);
    }

    public void zza(String str, String str2, Bundle bundle, long j) {
        this.ahD.zzbru().zzd(str, str2, bundle == null ? new Bundle() : bundle, j);
    }

    public void zzb(String str, String str2, Object obj) {
        zzc(str, str2, obj);
    }

    @WorkerThread
    public Map<String, Object> zzca(boolean z) {
        List<UserAttributeParcel> zzce = this.ahD.zzbru().zzce(z);
        HashMap hashMap = new HashMap(zzce.size());
        for (UserAttributeParcel next : zzce) {
            hashMap.put(next.name, next.getValue());
        }
        return hashMap;
    }

    public void zzd(String str, String str2, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        this.ahD.zzbru().zze(str, str2, bundle);
    }
}
