package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zze;
import java.lang.Thread;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;

public class zzw extends zzaa {
    /* access modifiers changed from: private */
    public static final AtomicLong akI = new AtomicLong(Long.MIN_VALUE);
    /* access modifiers changed from: private */
    public zzd akA;
    private final PriorityBlockingQueue<FutureTask<?>> akB = new PriorityBlockingQueue<>();
    private final BlockingQueue<FutureTask<?>> akC = new LinkedBlockingQueue();
    private final Thread.UncaughtExceptionHandler akD = new zzb("Thread death: Uncaught exception on worker thread");
    private final Thread.UncaughtExceptionHandler akE = new zzb("Thread death: Uncaught exception on network thread");
    /* access modifiers changed from: private */
    public final Object akF = new Object();
    /* access modifiers changed from: private */
    public final Semaphore akG = new Semaphore(2);
    /* access modifiers changed from: private */
    public volatile boolean akH;
    /* access modifiers changed from: private */
    public zzd akz;

    static class zza extends RuntimeException {
    }

    private final class zzb implements Thread.UncaughtExceptionHandler {
        private final String akJ;

        public zzb(String str) {
            zzab.zzy(str);
            this.akJ = str;
        }

        public synchronized void uncaughtException(Thread thread, Throwable th) {
            zzw.this.zzbsd().zzbsv().zzj(this.akJ, th);
        }
    }

    private final class zzc<V> extends FutureTask<V> implements Comparable<zzc> {
        private final String akJ;
        private final long akL = zzw.akI.getAndIncrement();
        private final boolean akM;

        zzc(Runnable runnable, boolean z, String str) {
            super(runnable, null);
            zzab.zzy(str);
            this.akJ = str;
            this.akM = z;
            if (this.akL == Long.MAX_VALUE) {
                zzw.this.zzbsd().zzbsv().log("Tasks index overflow");
            }
        }

        zzc(Callable<V> callable, boolean z, String str) {
            super(callable);
            zzab.zzy(str);
            this.akJ = str;
            this.akM = z;
            if (this.akL == Long.MAX_VALUE) {
                zzw.this.zzbsd().zzbsv().log("Tasks index overflow");
            }
        }

        /* access modifiers changed from: protected */
        public void setException(Throwable th) {
            zzw.this.zzbsd().zzbsv().zzj(this.akJ, th);
            if (th instanceof zza) {
                Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), th);
            }
            super.setException(th);
        }

        /* renamed from: zzb */
        public int compareTo(zzc zzc) {
            if (this.akM != zzc.akM) {
                return this.akM ? -1 : 1;
            }
            if (this.akL < zzc.akL) {
                return -1;
            }
            if (this.akL > zzc.akL) {
                return 1;
            }
            zzw.this.zzbsd().zzbsw().zzj("Two tasks share the same index. index", Long.valueOf(this.akL));
            return 0;
        }
    }

    private final class zzd extends Thread {
        private final Object akN = new Object();
        private final BlockingQueue<FutureTask<?>> akO;

        public zzd(String str, BlockingQueue<FutureTask<?>> blockingQueue) {
            zzab.zzy(str);
            zzab.zzy(blockingQueue);
            this.akO = blockingQueue;
            setName(str);
        }

        private void zza(InterruptedException interruptedException) {
            zzw.this.zzbsd().zzbsx().zzj(String.valueOf(getName()).concat(" was interrupted"), interruptedException);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0078, code lost:
            r1 = com.google.android.gms.measurement.internal.zzw.zzc(r4.akK);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x007e, code lost:
            monitor-enter(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
            com.google.android.gms.measurement.internal.zzw.zza(r4.akK).release();
            com.google.android.gms.measurement.internal.zzw.zzc(r4.akK).notifyAll();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x0097, code lost:
            if (r4 != com.google.android.gms.measurement.internal.zzw.zzd(r4.akK)) goto L_0x00a9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x0099, code lost:
            com.google.android.gms.measurement.internal.zzw.zza(r4.akK, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x009f, code lost:
            monitor-exit(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x00a0, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x00af, code lost:
            if (r4 != com.google.android.gms.measurement.internal.zzw.zze(r4.akK)) goto L_0x00bb;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x00b1, code lost:
            com.google.android.gms.measurement.internal.zzw.zzb(r4.akK, null);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
            r4.akK.zzbsd().zzbsv().log("Current scheduler thread is neither worker nor network");
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r4 = this;
                r0 = 0
                r1 = r0
            L_0x0002:
                if (r1 != 0) goto L_0x0015
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ InterruptedException -> 0x0010 }
                java.util.concurrent.Semaphore r0 = r0.akG     // Catch:{ InterruptedException -> 0x0010 }
                r0.acquire()     // Catch:{ InterruptedException -> 0x0010 }
                r0 = 1
                r1 = r0
                goto L_0x0002
            L_0x0010:
                r0 = move-exception
                r4.zza(r0)
                goto L_0x0002
            L_0x0015:
                java.util.concurrent.BlockingQueue<java.util.concurrent.FutureTask<?>> r0 = r4.akO     // Catch:{ all -> 0x0023 }
                java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x0023 }
                java.util.concurrent.FutureTask r0 = (java.util.concurrent.FutureTask) r0     // Catch:{ all -> 0x0023 }
                if (r0 == 0) goto L_0x004d
                r0.run()     // Catch:{ all -> 0x0023 }
                goto L_0x0015
            L_0x0023:
                r0 = move-exception
                com.google.android.gms.measurement.internal.zzw r1 = com.google.android.gms.measurement.internal.zzw.this
                java.lang.Object r1 = r1.akF
                monitor-enter(r1)
                com.google.android.gms.measurement.internal.zzw r2 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00e1 }
                java.util.concurrent.Semaphore r2 = r2.akG     // Catch:{ all -> 0x00e1 }
                r2.release()     // Catch:{ all -> 0x00e1 }
                com.google.android.gms.measurement.internal.zzw r2 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00e1 }
                java.lang.Object r2 = r2.akF     // Catch:{ all -> 0x00e1 }
                r2.notifyAll()     // Catch:{ all -> 0x00e1 }
                com.google.android.gms.measurement.internal.zzw r2 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00e1 }
                com.google.android.gms.measurement.internal.zzw$zzd r2 = r2.akz     // Catch:{ all -> 0x00e1 }
                if (r4 != r2) goto L_0x00d1
                com.google.android.gms.measurement.internal.zzw r2 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00e1 }
                r3 = 0
                com.google.android.gms.measurement.internal.zzw.zzd unused = r2.akz = r3     // Catch:{ all -> 0x00e1 }
            L_0x004b:
                monitor-exit(r1)     // Catch:{ all -> 0x00e1 }
                throw r0
            L_0x004d:
                java.lang.Object r1 = r4.akN     // Catch:{ all -> 0x0023 }
                monitor-enter(r1)     // Catch:{ all -> 0x0023 }
                java.util.concurrent.BlockingQueue<java.util.concurrent.FutureTask<?>> r0 = r4.akO     // Catch:{ all -> 0x00a6 }
                java.lang.Object r0 = r0.peek()     // Catch:{ all -> 0x00a6 }
                if (r0 != 0) goto L_0x0067
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00a6 }
                boolean r0 = r0.akH     // Catch:{ all -> 0x00a6 }
                if (r0 != 0) goto L_0x0067
                java.lang.Object r0 = r4.akN     // Catch:{ InterruptedException -> 0x00a1 }
                r2 = 30000(0x7530, double:1.4822E-319)
                r0.wait(r2)     // Catch:{ InterruptedException -> 0x00a1 }
            L_0x0067:
                monitor-exit(r1)     // Catch:{ all -> 0x00a6 }
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x0023 }
                java.lang.Object r1 = r0.akF     // Catch:{ all -> 0x0023 }
                monitor-enter(r1)     // Catch:{ all -> 0x0023 }
                java.util.concurrent.BlockingQueue<java.util.concurrent.FutureTask<?>> r0 = r4.akO     // Catch:{ all -> 0x00ce }
                java.lang.Object r0 = r0.peek()     // Catch:{ all -> 0x00ce }
                if (r0 != 0) goto L_0x00cb
                monitor-exit(r1)     // Catch:{ all -> 0x00ce }
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this
                java.lang.Object r1 = r0.akF
                monitor-enter(r1)
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00b8 }
                java.util.concurrent.Semaphore r0 = r0.akG     // Catch:{ all -> 0x00b8 }
                r0.release()     // Catch:{ all -> 0x00b8 }
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00b8 }
                java.lang.Object r0 = r0.akF     // Catch:{ all -> 0x00b8 }
                r0.notifyAll()     // Catch:{ all -> 0x00b8 }
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00b8 }
                com.google.android.gms.measurement.internal.zzw$zzd r0 = r0.akz     // Catch:{ all -> 0x00b8 }
                if (r4 != r0) goto L_0x00a9
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00b8 }
                r2 = 0
                com.google.android.gms.measurement.internal.zzw.zzd unused = r0.akz = r2     // Catch:{ all -> 0x00b8 }
            L_0x009f:
                monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
                return
            L_0x00a1:
                r0 = move-exception
                r4.zza(r0)     // Catch:{ all -> 0x00a6 }
                goto L_0x0067
            L_0x00a6:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x00a6 }
                throw r0     // Catch:{ all -> 0x0023 }
            L_0x00a9:
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00b8 }
                com.google.android.gms.measurement.internal.zzw$zzd r0 = r0.akA     // Catch:{ all -> 0x00b8 }
                if (r4 != r0) goto L_0x00bb
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00b8 }
                r2 = 0
                com.google.android.gms.measurement.internal.zzw.zzd unused = r0.akA = r2     // Catch:{ all -> 0x00b8 }
                goto L_0x009f
            L_0x00b8:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
                throw r0
            L_0x00bb:
                com.google.android.gms.measurement.internal.zzw r0 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00b8 }
                com.google.android.gms.measurement.internal.zzp r0 = r0.zzbsd()     // Catch:{ all -> 0x00b8 }
                com.google.android.gms.measurement.internal.zzp$zza r0 = r0.zzbsv()     // Catch:{ all -> 0x00b8 }
                java.lang.String r2 = "Current scheduler thread is neither worker nor network"
                r0.log(r2)     // Catch:{ all -> 0x00b8 }
                goto L_0x009f
            L_0x00cb:
                monitor-exit(r1)     // Catch:{ all -> 0x00ce }
                goto L_0x0015
            L_0x00ce:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x00ce }
                throw r0     // Catch:{ all -> 0x0023 }
            L_0x00d1:
                com.google.android.gms.measurement.internal.zzw r2 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00e1 }
                com.google.android.gms.measurement.internal.zzw$zzd r2 = r2.akA     // Catch:{ all -> 0x00e1 }
                if (r4 != r2) goto L_0x00e4
                com.google.android.gms.measurement.internal.zzw r2 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00e1 }
                r3 = 0
                com.google.android.gms.measurement.internal.zzw.zzd unused = r2.akA = r3     // Catch:{ all -> 0x00e1 }
                goto L_0x004b
            L_0x00e1:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x00e1 }
                throw r0
            L_0x00e4:
                com.google.android.gms.measurement.internal.zzw r2 = com.google.android.gms.measurement.internal.zzw.this     // Catch:{ all -> 0x00e1 }
                com.google.android.gms.measurement.internal.zzp r2 = r2.zzbsd()     // Catch:{ all -> 0x00e1 }
                com.google.android.gms.measurement.internal.zzp$zza r2 = r2.zzbsv()     // Catch:{ all -> 0x00e1 }
                java.lang.String r3 = "Current scheduler thread is neither worker nor network"
                r2.log(r3)     // Catch:{ all -> 0x00e1 }
                goto L_0x004b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzw.zzd.run():void");
        }

        public void zznk() {
            synchronized (this.akN) {
                this.akN.notifyAll();
            }
        }
    }

    zzw(zzx zzx) {
        super(zzx);
    }

    private void zza(zzc<?> zzc2) {
        synchronized (this.akF) {
            this.akB.add(zzc2);
            if (this.akz == null) {
                this.akz = new zzd("Measurement Worker", this.akB);
                this.akz.setUncaughtExceptionHandler(this.akD);
                this.akz.start();
            } else {
                this.akz.zznk();
            }
        }
    }

    private void zza(FutureTask<?> futureTask) {
        synchronized (this.akF) {
            this.akC.add(futureTask);
            if (this.akA == null) {
                this.akA = new zzd("Measurement Network", this.akC);
                this.akA.setUncaughtExceptionHandler(this.akE);
                this.akA.start();
            } else {
                this.akA.zznk();
            }
        }
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    public void zzbrs() {
        if (Thread.currentThread() != this.akA) {
            throw new IllegalStateException("Call expected from network thread");
        }
    }

    public /* bridge */ /* synthetic */ zzc zzbrt() {
        return super.zzbrt();
    }

    public /* bridge */ /* synthetic */ zzac zzbru() {
        return super.zzbru();
    }

    public /* bridge */ /* synthetic */ zzn zzbrv() {
        return super.zzbrv();
    }

    public /* bridge */ /* synthetic */ zzg zzbrw() {
        return super.zzbrw();
    }

    public /* bridge */ /* synthetic */ zzad zzbrx() {
        return super.zzbrx();
    }

    public /* bridge */ /* synthetic */ zze zzbry() {
        return super.zzbry();
    }

    public /* bridge */ /* synthetic */ zzal zzbrz() {
        return super.zzbrz();
    }

    public /* bridge */ /* synthetic */ zzv zzbsa() {
        return super.zzbsa();
    }

    public /* bridge */ /* synthetic */ zzaf zzbsb() {
        return super.zzbsb();
    }

    public /* bridge */ /* synthetic */ zzw zzbsc() {
        return super.zzbsc();
    }

    public /* bridge */ /* synthetic */ zzp zzbsd() {
        return super.zzbsd();
    }

    public /* bridge */ /* synthetic */ zzt zzbse() {
        return super.zzbse();
    }

    public /* bridge */ /* synthetic */ zzd zzbsf() {
        return super.zzbsf();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzw.zzc.<init>(com.google.android.gms.measurement.internal.zzw, java.util.concurrent.Callable, boolean, java.lang.String):void
     arg types: [com.google.android.gms.measurement.internal.zzw, java.util.concurrent.Callable<V>, int, java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.zzw.zzc.<init>(com.google.android.gms.measurement.internal.zzw, java.lang.Runnable, boolean, java.lang.String):void
      com.google.android.gms.measurement.internal.zzw.zzc.<init>(com.google.android.gms.measurement.internal.zzw, java.util.concurrent.Callable, boolean, java.lang.String):void */
    public <V> Future<V> zzd(Callable<V> callable) throws IllegalStateException {
        zzzg();
        zzab.zzy(callable);
        zzc zzc2 = new zzc((Callable) callable, false, "Task exception on worker thread");
        if (Thread.currentThread() == this.akz) {
            zzc2.run();
        } else {
            zza((zzc<?>) zzc2);
        }
        return zzc2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzw.zzc.<init>(com.google.android.gms.measurement.internal.zzw, java.util.concurrent.Callable, boolean, java.lang.String):void
     arg types: [com.google.android.gms.measurement.internal.zzw, java.util.concurrent.Callable<V>, int, java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.zzw.zzc.<init>(com.google.android.gms.measurement.internal.zzw, java.lang.Runnable, boolean, java.lang.String):void
      com.google.android.gms.measurement.internal.zzw.zzc.<init>(com.google.android.gms.measurement.internal.zzw, java.util.concurrent.Callable, boolean, java.lang.String):void */
    public <V> Future<V> zze(Callable<V> callable) throws IllegalStateException {
        zzzg();
        zzab.zzy(callable);
        zzc zzc2 = new zzc((Callable) callable, true, "Task exception on worker thread");
        if (Thread.currentThread() == this.akz) {
            zzc2.run();
        } else {
            zza((zzc<?>) zzc2);
        }
        return zzc2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzw.zzc.<init>(com.google.android.gms.measurement.internal.zzw, java.lang.Runnable, boolean, java.lang.String):void
     arg types: [com.google.android.gms.measurement.internal.zzw, java.lang.Runnable, int, java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.zzw.zzc.<init>(com.google.android.gms.measurement.internal.zzw, java.util.concurrent.Callable, boolean, java.lang.String):void
      com.google.android.gms.measurement.internal.zzw.zzc.<init>(com.google.android.gms.measurement.internal.zzw, java.lang.Runnable, boolean, java.lang.String):void */
    public void zzm(Runnable runnable) throws IllegalStateException {
        zzzg();
        zzab.zzy(runnable);
        zza((zzc<?>) new zzc(runnable, false, "Task exception on worker thread"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzw.zzc.<init>(com.google.android.gms.measurement.internal.zzw, java.lang.Runnable, boolean, java.lang.String):void
     arg types: [com.google.android.gms.measurement.internal.zzw, java.lang.Runnable, int, java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.zzw.zzc.<init>(com.google.android.gms.measurement.internal.zzw, java.util.concurrent.Callable, boolean, java.lang.String):void
      com.google.android.gms.measurement.internal.zzw.zzc.<init>(com.google.android.gms.measurement.internal.zzw, java.lang.Runnable, boolean, java.lang.String):void */
    public void zzn(Runnable runnable) throws IllegalStateException {
        zzzg();
        zzab.zzy(runnable);
        zza((FutureTask<?>) new zzc(runnable, false, "Task exception on network thread"));
    }

    public void zzwu() {
        if (Thread.currentThread() != this.akz) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    /* access modifiers changed from: protected */
    public void zzwv() {
    }

    public /* bridge */ /* synthetic */ void zzyv() {
        super.zzyv();
    }

    public /* bridge */ /* synthetic */ zze zzyw() {
        return super.zzyw();
    }
}
