package com.google.android.gms;

public final class R {

    public static final class array {
    }

    public static final class attr {
        public static final int circleCrop = 2130772152;
        public static final int imageAspectRatio = 2130772151;
        public static final int imageAspectRatioAdjust = 2130772150;
    }

    public static final class color {
    }

    public static final class dimen {
    }

    public static final class drawable {
    }

    public static final class id {
        public static final int adjust_height = 2131427357;
        public static final int adjust_width = 2131427358;
        public static final int button = 2131427370;
        public static final int center = 2131427373;
        public static final int none = 2131427342;
        public static final int normal = 2131427338;
        public static final int radio = 2131427407;
        public static final int standard = 2131427371;
        public static final int text = 2131427566;
        public static final int text2 = 2131427564;
        public static final int wrap_content = 2131427353;
    }

    public static final class integer {
        public static final int google_play_services_version = 2131492867;
    }

    public static final class layout {
    }

    public static final class raw {
    }

    public static final class string {
        public static final int common_google_play_services_unknown_issue = 2131099685;
    }

    public static final class style {
    }

    public static final class styleable {
        public static final int[] LoadingImageView = {com.seantech.nineke.R.attr.imageAspectRatioAdjust, com.seantech.nineke.R.attr.imageAspectRatio, com.seantech.nineke.R.attr.circleCrop};
        public static final int LoadingImageView_circleCrop = 2;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 0;
    }
}
