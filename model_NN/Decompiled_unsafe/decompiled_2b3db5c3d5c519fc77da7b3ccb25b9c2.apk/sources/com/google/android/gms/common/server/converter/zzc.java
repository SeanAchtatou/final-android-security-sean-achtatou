package com.google.android.gms.common.server.converter;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.common.server.converter.StringToIntConverter;

public class zzc implements Parcelable.Creator<StringToIntConverter.Entry> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, long[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    static void zza(StringToIntConverter.Entry entry, Parcel parcel, int i) {
        int zzcn = zzb.zzcn(parcel);
        zzb.zzc(parcel, 1, entry.versionCode);
        zzb.zza(parcel, 2, entry.zD, false);
        zzb.zzc(parcel, 3, entry.zE);
        zzb.zzaj(parcel, zzcn);
    }

    /* renamed from: zzcr */
    public StringToIntConverter.Entry createFromParcel(Parcel parcel) {
        int i = 0;
        int zzcm = zza.zzcm(parcel);
        String str = null;
        int i2 = 0;
        while (parcel.dataPosition() < zzcm) {
            int zzcl = zza.zzcl(parcel);
            switch (zza.zzgm(zzcl)) {
                case 1:
                    i2 = zza.zzg(parcel, zzcl);
                    break;
                case 2:
                    str = zza.zzq(parcel, zzcl);
                    break;
                case 3:
                    i = zza.zzg(parcel, zzcl);
                    break;
                default:
                    zza.zzb(parcel, zzcl);
                    break;
            }
        }
        if (parcel.dataPosition() == zzcm) {
            return new StringToIntConverter.Entry(i2, str, i);
        }
        throw new zza.C0000zza(new StringBuilder(37).append("Overread allowed size end=").append(zzcm).toString(), parcel);
    }

    /* renamed from: zzgr */
    public StringToIntConverter.Entry[] newArray(int i) {
        return new StringToIntConverter.Entry[i];
    }
}
