package com.google.android.gms.internal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class zzaps implements Cloneable {
    private zzapq<?, ?> bjD;
    private List<zzapx> bjE = new ArrayList();
    private Object value;

    zzaps() {
    }

    private byte[] toByteArray() throws IOException {
        byte[] bArr = new byte[zzx()];
        zza(zzapo.zzbe(bArr));
        return bArr;
    }

    /* renamed from: aD */
    public final zzaps clone() {
        int i = 0;
        zzaps zzaps = new zzaps();
        try {
            zzaps.bjD = this.bjD;
            if (this.bjE == null) {
                zzaps.bjE = null;
            } else {
                zzaps.bjE.addAll(this.bjE);
            }
            if (this.value != null) {
                if (this.value instanceof zzapv) {
                    zzaps.value = (zzapv) ((zzapv) this.value).clone();
                } else if (this.value instanceof byte[]) {
                    zzaps.value = ((byte[]) this.value).clone();
                } else if (this.value instanceof byte[][]) {
                    byte[][] bArr = (byte[][]) this.value;
                    byte[][] bArr2 = new byte[bArr.length][];
                    zzaps.value = bArr2;
                    for (int i2 = 0; i2 < bArr.length; i2++) {
                        bArr2[i2] = (byte[]) bArr[i2].clone();
                    }
                } else if (this.value instanceof boolean[]) {
                    zzaps.value = ((boolean[]) this.value).clone();
                } else if (this.value instanceof int[]) {
                    zzaps.value = ((int[]) this.value).clone();
                } else if (this.value instanceof long[]) {
                    zzaps.value = ((long[]) this.value).clone();
                } else if (this.value instanceof float[]) {
                    zzaps.value = ((float[]) this.value).clone();
                } else if (this.value instanceof double[]) {
                    zzaps.value = ((double[]) this.value).clone();
                } else if (this.value instanceof zzapv[]) {
                    zzapv[] zzapvArr = (zzapv[]) this.value;
                    zzapv[] zzapvArr2 = new zzapv[zzapvArr.length];
                    zzaps.value = zzapvArr2;
                    while (true) {
                        int i3 = i;
                        if (i3 >= zzapvArr.length) {
                            break;
                        }
                        zzapvArr2[i3] = (zzapv) zzapvArr[i3].clone();
                        i = i3 + 1;
                    }
                }
            }
            return zzaps;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof zzaps)) {
            return false;
        }
        zzaps zzaps = (zzaps) obj;
        if (this.value == null || zzaps.value == null) {
            if (this.bjE != null && zzaps.bjE != null) {
                return this.bjE.equals(zzaps.bjE);
            }
            try {
                return Arrays.equals(toByteArray(), zzaps.toByteArray());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else if (this.bjD == zzaps.bjD) {
            return !this.bjD.baj.isArray() ? this.value.equals(zzaps.value) : this.value instanceof byte[] ? Arrays.equals((byte[]) this.value, (byte[]) zzaps.value) : this.value instanceof int[] ? Arrays.equals((int[]) this.value, (int[]) zzaps.value) : this.value instanceof long[] ? Arrays.equals((long[]) this.value, (long[]) zzaps.value) : this.value instanceof float[] ? Arrays.equals((float[]) this.value, (float[]) zzaps.value) : this.value instanceof double[] ? Arrays.equals((double[]) this.value, (double[]) zzaps.value) : this.value instanceof boolean[] ? Arrays.equals((boolean[]) this.value, (boolean[]) zzaps.value) : Arrays.deepEquals((Object[]) this.value, (Object[]) zzaps.value);
        } else {
            return false;
        }
    }

    public int hashCode() {
        try {
            return Arrays.hashCode(toByteArray()) + 527;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public void zza(zzapo zzapo) throws IOException {
        if (this.value != null) {
            this.bjD.zza(this.value, zzapo);
            return;
        }
        for (zzapx zza : this.bjE) {
            zza.zza(zzapo);
        }
    }

    /* access modifiers changed from: package-private */
    public void zza(zzapx zzapx) {
        this.bjE.add(zzapx);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [com.google.android.gms.internal.zzapq, com.google.android.gms.internal.zzapq<?, T>, java.lang.Object, com.google.android.gms.internal.zzapq<?, ?>] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    <T> T zzb(com.google.android.gms.internal.zzapq<?, T> r3) {
        /*
            r2 = this;
            java.lang.Object r0 = r2.value
            if (r0 == 0) goto L_0x0014
            com.google.android.gms.internal.zzapq<?, ?> r0 = r2.bjD
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x0021
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Tried to getExtension with a different Extension."
            r0.<init>(r1)
            throw r0
        L_0x0014:
            r2.bjD = r3
            java.util.List<com.google.android.gms.internal.zzapx> r0 = r2.bjE
            java.lang.Object r0 = r3.zzav(r0)
            r2.value = r0
            r0 = 0
            r2.bjE = r0
        L_0x0021:
            java.lang.Object r0 = r2.value
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzaps.zzb(com.google.android.gms.internal.zzapq):java.lang.Object");
    }

    /* access modifiers changed from: package-private */
    public int zzx() {
        int i = 0;
        if (this.value != null) {
            return this.bjD.zzcp(this.value);
        }
        Iterator<zzapx> it = this.bjE.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = it.next().zzx() + i2;
        }
    }
}
