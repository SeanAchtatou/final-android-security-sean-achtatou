package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zze;
import com.google.android.gms.internal.zzapn;
import com.google.android.gms.internal.zzapo;
import com.google.android.gms.internal.zzug;
import com.google.android.gms.measurement.AppMeasurement;
import java.io.IOException;
import java.util.Map;

public class zzv extends zzaa {
    private final Map<String, Map<String, String>> aku = new ArrayMap();
    private final Map<String, Map<String, Boolean>> akv = new ArrayMap();
    private final Map<String, Map<String, Boolean>> akw = new ArrayMap();
    private final Map<String, zzug.zzb> akx = new ArrayMap();
    private final Map<String, String> aky = new ArrayMap();

    zzv(zzx zzx) {
        super(zzx);
    }

    private Map<String, String> zza(zzug.zzb zzb) {
        ArrayMap arrayMap = new ArrayMap();
        if (!(zzb == null || zzb.ane == null)) {
            for (zzug.zzc zzc : zzb.ane) {
                if (zzc != null) {
                    arrayMap.put(zzc.zzcb, zzc.value);
                }
            }
        }
        return arrayMap;
    }

    private void zza(String str, zzug.zzb zzb) {
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        if (!(zzb == null || zzb.anf == null)) {
            for (zzug.zza zza : zzb.anf) {
                if (zza != null) {
                    String str2 = AppMeasurement.zza.ahE.get(zza.name);
                    if (str2 != null) {
                        zza.name = str2;
                    }
                    arrayMap.put(zza.name, zza.ana);
                    arrayMap2.put(zza.name, zza.anb);
                }
            }
        }
        this.akv.put(str, arrayMap);
        this.akw.put(str, arrayMap2);
    }

    @WorkerThread
    private zzug.zzb zze(String str, byte[] bArr) {
        if (bArr == null) {
            return new zzug.zzb();
        }
        zzapn zzbd = zzapn.zzbd(bArr);
        zzug.zzb zzb = new zzug.zzb();
        try {
            zzug.zzb zzb2 = (zzug.zzb) zzb.zzb(zzbd);
            zzbsd().zzbtc().zze("Parsed config. version, gmp_app_id", zzb.anc, zzb.aic);
            return zzb;
        } catch (IOException e) {
            zzbsd().zzbsx().zze("Unable to merge remote config", str, e);
            return null;
        }
    }

    @WorkerThread
    private void zzma(String str) {
        zzzg();
        zzwu();
        zzab.zzhr(str);
        if (!this.akx.containsKey(str)) {
            byte[] zzlp = zzbry().zzlp(str);
            if (zzlp == null) {
                this.aku.put(str, null);
                this.akv.put(str, null);
                this.akw.put(str, null);
                this.akx.put(str, null);
                this.aky.put(str, null);
                return;
            }
            zzug.zzb zze = zze(str, zzlp);
            this.aku.put(str, zza(zze));
            zza(str, zze);
            this.akx.put(str, zze);
            this.aky.put(str, null);
        }
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public String zzaw(String str, String str2) {
        zzwu();
        zzma(str);
        Map map = this.aku.get(str);
        if (map != null) {
            return (String) map.get(str2);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public boolean zzax(String str, String str2) {
        zzwu();
        zzma(str);
        Map map = this.akv.get(str);
        if (map == null) {
            return false;
        }
        Boolean bool = (Boolean) map.get(str2);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    public boolean zzay(String str, String str2) {
        zzwu();
        zzma(str);
        Map map = this.akw.get(str);
        if (map == null) {
            return false;
        }
        Boolean bool = (Boolean) map.get(str2);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public boolean zzb(String str, byte[] bArr, String str2) {
        zzzg();
        zzwu();
        zzab.zzhr(str);
        zzug.zzb zze = zze(str, bArr);
        if (zze == null) {
            return false;
        }
        zza(str, zze);
        this.akx.put(str, zze);
        this.aky.put(str, str2);
        this.aku.put(str, zza(zze));
        zzbrt().zza(str, zze.ang);
        try {
            zze.ang = null;
            byte[] bArr2 = new byte[zze.aM()];
            zze.zza(zzapo.zzbe(bArr2));
            bArr = bArr2;
        } catch (IOException e) {
            zzbsd().zzbsx().zzj("Unable to serialize reduced-size config.  Storing full config instead.", e);
        }
        zzbry().zzd(str, bArr);
        return true;
    }

    public /* bridge */ /* synthetic */ void zzbrs() {
        super.zzbrs();
    }

    public /* bridge */ /* synthetic */ zzc zzbrt() {
        return super.zzbrt();
    }

    public /* bridge */ /* synthetic */ zzac zzbru() {
        return super.zzbru();
    }

    public /* bridge */ /* synthetic */ zzn zzbrv() {
        return super.zzbrv();
    }

    public /* bridge */ /* synthetic */ zzg zzbrw() {
        return super.zzbrw();
    }

    public /* bridge */ /* synthetic */ zzad zzbrx() {
        return super.zzbrx();
    }

    public /* bridge */ /* synthetic */ zze zzbry() {
        return super.zzbry();
    }

    public /* bridge */ /* synthetic */ zzal zzbrz() {
        return super.zzbrz();
    }

    public /* bridge */ /* synthetic */ zzv zzbsa() {
        return super.zzbsa();
    }

    public /* bridge */ /* synthetic */ zzaf zzbsb() {
        return super.zzbsb();
    }

    public /* bridge */ /* synthetic */ zzw zzbsc() {
        return super.zzbsc();
    }

    public /* bridge */ /* synthetic */ zzp zzbsd() {
        return super.zzbsd();
    }

    public /* bridge */ /* synthetic */ zzt zzbse() {
        return super.zzbse();
    }

    public /* bridge */ /* synthetic */ zzd zzbsf() {
        return super.zzbsf();
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public zzug.zzb zzmb(String str) {
        zzzg();
        zzwu();
        zzab.zzhr(str);
        zzma(str);
        return this.akx.get(str);
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public String zzmc(String str) {
        zzwu();
        return this.aky.get(str);
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public void zzmd(String str) {
        zzwu();
        this.aky.put(str, null);
    }

    public /* bridge */ /* synthetic */ void zzwu() {
        super.zzwu();
    }

    /* access modifiers changed from: protected */
    public void zzwv() {
    }

    public /* bridge */ /* synthetic */ void zzyv() {
        super.zzyv();
    }

    public /* bridge */ /* synthetic */ zze zzyw() {
        return super.zzyw();
    }
}
