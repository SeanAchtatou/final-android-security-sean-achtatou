package com.google.android.gms.flags.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import com.google.android.gms.common.util.DynamiteApi;
import com.google.android.gms.dynamic.zzd;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.flags.impl.zza;
import com.google.android.gms.internal.zzty;

@DynamiteApi
public class FlagProviderImpl extends zzty.zza {
    private boolean zzamt = false;
    private SharedPreferences zzaxu;

    public boolean getBooleanFlagValue(String str, boolean z, int i) {
        return !this.zzamt ? z : zza.C0011zza.zza(this.zzaxu, str, Boolean.valueOf(z)).booleanValue();
    }

    public int getIntFlagValue(String str, int i, int i2) {
        return !this.zzamt ? i : zza.zzb.zza(this.zzaxu, str, Integer.valueOf(i)).intValue();
    }

    public long getLongFlagValue(String str, long j, int i) {
        return !this.zzamt ? j : zza.zzc.zza(this.zzaxu, str, Long.valueOf(j)).longValue();
    }

    public String getStringFlagValue(String str, String str2, int i) {
        return !this.zzamt ? str2 : zza.zzd.zza(this.zzaxu, str, str2);
    }

    public void init(zzd zzd) {
        Context context = (Context) zze.zzad(zzd);
        if (!this.zzamt) {
            try {
                this.zzaxu = zzb.zzn(context.createPackageContext("com.google.android.gms", 0));
                this.zzamt = true;
            } catch (PackageManager.NameNotFoundException e) {
            }
        }
    }
}
