package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.WorkerThread;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.util.zze;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

public class zzq extends zzaa {

    @WorkerThread
    interface zza {
        void zza(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map);
    }

    @WorkerThread
    private static class zzb implements Runnable {
        private final String aM;
        private final zza ajD;
        private final Throwable ajE;
        private final byte[] ajF;
        private final Map<String, List<String>> ajG;
        private final int zzblv;

        private zzb(String str, zza zza, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
            zzab.zzy(zza);
            this.ajD = zza;
            this.zzblv = i;
            this.ajE = th;
            this.ajF = bArr;
            this.aM = str;
            this.ajG = map;
        }

        public void run() {
            this.ajD.zza(this.aM, this.zzblv, this.ajE, this.ajF, this.ajG);
        }
    }

    @WorkerThread
    private class zzc implements Runnable {
        private final String aM;
        private final byte[] ajH;
        private final zza ajI;
        private final Map<String, String> ajJ;
        private final URL zzbij;

        public zzc(String str, URL url, byte[] bArr, Map<String, String> map, zza zza) {
            zzab.zzhr(str);
            zzab.zzy(url);
            zzab.zzy(zza);
            this.zzbij = url;
            this.ajH = bArr;
            this.ajI = zza;
            this.aM = str;
            this.ajJ = map;
        }

        /* JADX WARNING: Removed duplicated region for block: B:36:0x00ee A[SYNTHETIC, Splitter:B:36:0x00ee] */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x00f3  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r13 = this;
                r4 = 0
                com.google.android.gms.measurement.internal.zzq r0 = com.google.android.gms.measurement.internal.zzq.this
                r0.zzbrs()
                r3 = 0
                com.google.android.gms.measurement.internal.zzq r0 = com.google.android.gms.measurement.internal.zzq.this     // Catch:{ IOException -> 0x012e, all -> 0x00e7 }
                java.lang.String r1 = r13.aM     // Catch:{ IOException -> 0x012e, all -> 0x00e7 }
                r0.zzet(r1)     // Catch:{ IOException -> 0x012e, all -> 0x00e7 }
                com.google.android.gms.measurement.internal.zzq r0 = com.google.android.gms.measurement.internal.zzq.this     // Catch:{ IOException -> 0x012e, all -> 0x00e7 }
                java.net.URL r1 = r13.zzbij     // Catch:{ IOException -> 0x012e, all -> 0x00e7 }
                java.net.HttpURLConnection r2 = r0.zzc(r1)     // Catch:{ IOException -> 0x012e, all -> 0x00e7 }
                java.util.Map<java.lang.String, java.lang.String> r0 = r13.ajJ     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                if (r0 == 0) goto L_0x0069
                java.util.Map<java.lang.String, java.lang.String> r0 = r13.ajJ     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                java.util.Set r0 = r0.entrySet()     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                java.util.Iterator r5 = r0.iterator()     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
            L_0x0024:
                boolean r0 = r5.hasNext()     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                if (r0 == 0) goto L_0x0069
                java.lang.Object r0 = r5.next()     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                java.lang.Object r1 = r0.getKey()     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                java.lang.String r1 = (java.lang.String) r1     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                java.lang.Object r0 = r0.getValue()     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                r2.addRequestProperty(r1, r0)     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                goto L_0x0024
            L_0x0040:
                r9 = move-exception
                r11 = r4
                r8 = r3
                r0 = r4
                r1 = r2
            L_0x0045:
                if (r0 == 0) goto L_0x004a
                r0.close()     // Catch:{ IOException -> 0x00d5 }
            L_0x004a:
                if (r1 == 0) goto L_0x004f
                r1.disconnect()
            L_0x004f:
                com.google.android.gms.measurement.internal.zzq r0 = com.google.android.gms.measurement.internal.zzq.this
                r0.zzrp()
                com.google.android.gms.measurement.internal.zzq r0 = com.google.android.gms.measurement.internal.zzq.this
                com.google.android.gms.measurement.internal.zzw r0 = r0.zzbsc()
                com.google.android.gms.measurement.internal.zzq$zzb r5 = new com.google.android.gms.measurement.internal.zzq$zzb
                java.lang.String r6 = r13.aM
                com.google.android.gms.measurement.internal.zzq$zza r7 = r13.ajI
                r10 = r4
                r12 = r4
                r5.<init>(r6, r7, r8, r9, r10, r11)
                r0.zzm(r5)
            L_0x0068:
                return
            L_0x0069:
                byte[] r0 = r13.ajH     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                if (r0 == 0) goto L_0x00a9
                com.google.android.gms.measurement.internal.zzq r0 = com.google.android.gms.measurement.internal.zzq.this     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                com.google.android.gms.measurement.internal.zzal r0 = r0.zzbrz()     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                byte[] r1 = r13.ajH     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                byte[] r1 = r0.zzj(r1)     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                com.google.android.gms.measurement.internal.zzq r0 = com.google.android.gms.measurement.internal.zzq.this     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                com.google.android.gms.measurement.internal.zzp r0 = r0.zzbsd()     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                com.google.android.gms.measurement.internal.zzp$zza r0 = r0.zzbtc()     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                java.lang.String r5 = "Uploading data. size"
                int r6 = r1.length     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                r0.zzj(r5, r6)     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                r0 = 1
                r2.setDoOutput(r0)     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                java.lang.String r0 = "Content-Encoding"
                java.lang.String r5 = "gzip"
                r2.addRequestProperty(r0, r5)     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                int r0 = r1.length     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                r2.setFixedLengthStreamingMode(r0)     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                r2.connect()     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                java.io.OutputStream r0 = r2.getOutputStream()     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                r0.write(r1)     // Catch:{ IOException -> 0x0135, all -> 0x0126 }
                r0.close()     // Catch:{ IOException -> 0x0135, all -> 0x0126 }
            L_0x00a9:
                int r3 = r2.getResponseCode()     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                java.util.Map r6 = r2.getHeaderFields()     // Catch:{ IOException -> 0x0040, all -> 0x0121 }
                com.google.android.gms.measurement.internal.zzq r0 = com.google.android.gms.measurement.internal.zzq.this     // Catch:{ IOException -> 0x013b, all -> 0x012a }
                byte[] r5 = r0.zzc(r2)     // Catch:{ IOException -> 0x013b, all -> 0x012a }
                if (r2 == 0) goto L_0x00bc
                r2.disconnect()
            L_0x00bc:
                com.google.android.gms.measurement.internal.zzq r0 = com.google.android.gms.measurement.internal.zzq.this
                r0.zzrp()
                com.google.android.gms.measurement.internal.zzq r0 = com.google.android.gms.measurement.internal.zzq.this
                com.google.android.gms.measurement.internal.zzw r8 = r0.zzbsc()
                com.google.android.gms.measurement.internal.zzq$zzb r0 = new com.google.android.gms.measurement.internal.zzq$zzb
                java.lang.String r1 = r13.aM
                com.google.android.gms.measurement.internal.zzq$zza r2 = r13.ajI
                r7 = r4
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r8.zzm(r0)
                goto L_0x0068
            L_0x00d5:
                r0 = move-exception
                com.google.android.gms.measurement.internal.zzq r2 = com.google.android.gms.measurement.internal.zzq.this
                com.google.android.gms.measurement.internal.zzp r2 = r2.zzbsd()
                com.google.android.gms.measurement.internal.zzp$zza r2 = r2.zzbsv()
                java.lang.String r3 = "Error closing HTTP compressed POST connection output stream"
                r2.zzj(r3, r0)
                goto L_0x004a
            L_0x00e7:
                r0 = move-exception
                r8 = r0
                r6 = r4
                r2 = r4
                r0 = r4
            L_0x00ec:
                if (r0 == 0) goto L_0x00f1
                r0.close()     // Catch:{ IOException -> 0x0110 }
            L_0x00f1:
                if (r2 == 0) goto L_0x00f6
                r2.disconnect()
            L_0x00f6:
                com.google.android.gms.measurement.internal.zzq r0 = com.google.android.gms.measurement.internal.zzq.this
                r0.zzrp()
                com.google.android.gms.measurement.internal.zzq r0 = com.google.android.gms.measurement.internal.zzq.this
                com.google.android.gms.measurement.internal.zzw r9 = r0.zzbsc()
                com.google.android.gms.measurement.internal.zzq$zzb r0 = new com.google.android.gms.measurement.internal.zzq$zzb
                java.lang.String r1 = r13.aM
                com.google.android.gms.measurement.internal.zzq$zza r2 = r13.ajI
                r5 = r4
                r7 = r4
                r0.<init>(r1, r2, r3, r4, r5, r6)
                r9.zzm(r0)
                throw r8
            L_0x0110:
                r0 = move-exception
                com.google.android.gms.measurement.internal.zzq r1 = com.google.android.gms.measurement.internal.zzq.this
                com.google.android.gms.measurement.internal.zzp r1 = r1.zzbsd()
                com.google.android.gms.measurement.internal.zzp$zza r1 = r1.zzbsv()
                java.lang.String r5 = "Error closing HTTP compressed POST connection output stream"
                r1.zzj(r5, r0)
                goto L_0x00f1
            L_0x0121:
                r0 = move-exception
                r8 = r0
                r6 = r4
                r0 = r4
                goto L_0x00ec
            L_0x0126:
                r1 = move-exception
                r8 = r1
                r6 = r4
                goto L_0x00ec
            L_0x012a:
                r0 = move-exception
                r8 = r0
                r0 = r4
                goto L_0x00ec
            L_0x012e:
                r9 = move-exception
                r11 = r4
                r8 = r3
                r0 = r4
                r1 = r4
                goto L_0x0045
            L_0x0135:
                r9 = move-exception
                r11 = r4
                r8 = r3
                r1 = r2
                goto L_0x0045
            L_0x013b:
                r9 = move-exception
                r11 = r6
                r8 = r3
                r0 = r4
                r1 = r2
                goto L_0x0045
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.zzq.zzc.run():void");
        }
    }

    public zzq(zzx zzx) {
        super(zzx);
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public byte[] zzc(HttpURLConnection httpURLConnection) throws IOException {
        InputStream inputStream = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            inputStream = httpURLConnection.getInputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read <= 0) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            return byteArrayOutputStream.toByteArray();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    public /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    @WorkerThread
    public void zza(String str, URL url, Map<String, String> map, zza zza2) {
        zzwu();
        zzzg();
        zzab.zzy(url);
        zzab.zzy(zza2);
        zzbsc().zzn(new zzc(str, url, null, map, zza2));
    }

    @WorkerThread
    public void zza(String str, URL url, byte[] bArr, Map<String, String> map, zza zza2) {
        zzwu();
        zzzg();
        zzab.zzy(url);
        zzab.zzy(bArr);
        zzab.zzy(zza2);
        zzbsc().zzn(new zzc(str, url, bArr, map, zza2));
    }

    public boolean zzadj() {
        NetworkInfo networkInfo;
        zzzg();
        try {
            networkInfo = ((ConnectivityManager) getContext().getSystemService("connectivity")).getActiveNetworkInfo();
        } catch (SecurityException e) {
            networkInfo = null;
        }
        return networkInfo != null && networkInfo.isConnected();
    }

    public /* bridge */ /* synthetic */ void zzbrs() {
        super.zzbrs();
    }

    public /* bridge */ /* synthetic */ zzc zzbrt() {
        return super.zzbrt();
    }

    public /* bridge */ /* synthetic */ zzac zzbru() {
        return super.zzbru();
    }

    public /* bridge */ /* synthetic */ zzn zzbrv() {
        return super.zzbrv();
    }

    public /* bridge */ /* synthetic */ zzg zzbrw() {
        return super.zzbrw();
    }

    public /* bridge */ /* synthetic */ zzad zzbrx() {
        return super.zzbrx();
    }

    public /* bridge */ /* synthetic */ zze zzbry() {
        return super.zzbry();
    }

    public /* bridge */ /* synthetic */ zzal zzbrz() {
        return super.zzbrz();
    }

    public /* bridge */ /* synthetic */ zzv zzbsa() {
        return super.zzbsa();
    }

    public /* bridge */ /* synthetic */ zzaf zzbsb() {
        return super.zzbsb();
    }

    public /* bridge */ /* synthetic */ zzw zzbsc() {
        return super.zzbsc();
    }

    public /* bridge */ /* synthetic */ zzp zzbsd() {
        return super.zzbsd();
    }

    public /* bridge */ /* synthetic */ zzt zzbse() {
        return super.zzbse();
    }

    public /* bridge */ /* synthetic */ zzd zzbsf() {
        return super.zzbsf();
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public HttpURLConnection zzc(URL url) throws IOException {
        URLConnection openConnection = url.openConnection();
        if (!(openConnection instanceof HttpURLConnection)) {
            throw new IOException("Failed to obtain HTTP connection");
        }
        HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
        httpURLConnection.setDefaultUseCaches(false);
        httpURLConnection.setConnectTimeout((int) zzbsf().zzbrb());
        httpURLConnection.setReadTimeout((int) zzbsf().zzbrc());
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setDoInput(true);
        return httpURLConnection;
    }

    /* access modifiers changed from: protected */
    public void zzet(String str) {
    }

    /* access modifiers changed from: protected */
    public void zzrp() {
    }

    public /* bridge */ /* synthetic */ void zzwu() {
        super.zzwu();
    }

    /* access modifiers changed from: protected */
    public void zzwv() {
    }

    public /* bridge */ /* synthetic */ void zzyv() {
        super.zzyv();
    }

    public /* bridge */ /* synthetic */ zze zzyw() {
        return super.zzyw();
    }
}
