package com.google.android.gms.internal;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

public final class zzant<K, V> extends AbstractMap<K, V> implements Serializable {
    static final /* synthetic */ boolean $assertionsDisabled = (!zzant.class.desiredAssertionStatus());
    private static final Comparator<Comparable> beW = new Comparator<Comparable>() {
        /* renamed from: zza */
        public int compare(Comparable comparable, Comparable comparable2) {
            return comparable.compareTo(comparable2);
        }
    };
    Comparator<? super K> aPZ;
    zzd<K, V> beX;
    final zzd<K, V> beY;
    private zza beZ;
    private zzb bfa;
    int modCount;
    int size;

    class zza extends AbstractSet<Map.Entry<K, V>> {
        zza() {
        }

        public void clear() {
            zzant.this.clear();
        }

        public boolean contains(Object obj) {
            return (obj instanceof Map.Entry) && zzant.this.zzc((Map.Entry) obj) != null;
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return new zzc<Map.Entry<K, V>>() {
                {
                    zzant zzant = zzant.this;
                }

                public Map.Entry<K, V> next() {
                    return zzczw();
                }
            };
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, boolean):void
         arg types: [com.google.android.gms.internal.zzant$zzd, int]
         candidates:
          com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, com.google.android.gms.internal.zzant$zzd):void
          com.google.android.gms.internal.zzant.zza(java.lang.Object, boolean):com.google.android.gms.internal.zzant$zzd<K, V>
          com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, boolean):void */
        public boolean remove(Object obj) {
            zzd zzc;
            if (!(obj instanceof Map.Entry) || (zzc = zzant.this.zzc((Map.Entry) obj)) == null) {
                return false;
            }
            zzant.this.zza(zzc, true);
            return true;
        }

        public int size() {
            return zzant.this.size;
        }
    }

    final class zzb extends AbstractSet<K> {
        zzb() {
        }

        public void clear() {
            zzant.this.clear();
        }

        public boolean contains(Object obj) {
            return zzant.this.containsKey(obj);
        }

        public Iterator<K> iterator() {
            return new zzc<K>() {
                {
                    zzant zzant = zzant.this;
                }

                public K next() {
                    return zzczw().aQn;
                }
            };
        }

        public boolean remove(Object obj) {
            return zzant.this.zzcn(obj) != null;
        }

        public int size() {
            return zzant.this.size;
        }
    }

    private abstract class zzc<T> implements Iterator<T> {
        zzd<K, V> bfe;
        zzd<K, V> bff;
        int bfg;

        private zzc() {
            this.bfe = zzant.this.beY.bfe;
            this.bff = null;
            this.bfg = zzant.this.modCount;
        }

        public final boolean hasNext() {
            return this.bfe != zzant.this.beY;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, boolean):void
         arg types: [com.google.android.gms.internal.zzant$zzd<K, V>, int]
         candidates:
          com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, com.google.android.gms.internal.zzant$zzd):void
          com.google.android.gms.internal.zzant.zza(java.lang.Object, boolean):com.google.android.gms.internal.zzant$zzd<K, V>
          com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, boolean):void */
        public final void remove() {
            if (this.bff == null) {
                throw new IllegalStateException();
            }
            zzant.this.zza((zzd) this.bff, true);
            this.bff = null;
            this.bfg = zzant.this.modCount;
        }

        /* access modifiers changed from: package-private */
        public final zzd<K, V> zzczw() {
            zzd<K, V> zzd = this.bfe;
            if (zzd == zzant.this.beY) {
                throw new NoSuchElementException();
            } else if (zzant.this.modCount != this.bfg) {
                throw new ConcurrentModificationException();
            } else {
                this.bfe = zzd.bfe;
                this.bff = zzd;
                return zzd;
            }
        }
    }

    static final class zzd<K, V> implements Map.Entry<K, V> {
        final K aQn;
        zzd<K, V> bfe;
        zzd<K, V> bfh;
        zzd<K, V> bfi;
        zzd<K, V> bfj;
        zzd<K, V> bfk;
        int height;
        V value;

        zzd() {
            this.aQn = null;
            this.bfk = this;
            this.bfe = this;
        }

        zzd(zzd<K, V> zzd, K k, zzd<K, V> zzd2, zzd<K, V> zzd3) {
            this.bfh = zzd;
            this.aQn = k;
            this.height = 1;
            this.bfe = zzd2;
            this.bfk = zzd3;
            zzd3.bfe = this;
            zzd2.bfk = this;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        /* JADX WARNING: Removed duplicated region for block: B:10:0x001b A[ORIG_RETURN, RETURN, SYNTHETIC] */
        public boolean equals(java.lang.Object r4) {
            /*
                r3 = this;
                r0 = 0
                boolean r1 = r4 instanceof java.util.Map.Entry
                if (r1 == 0) goto L_0x001c
                java.util.Map$Entry r4 = (java.util.Map.Entry) r4
                K r1 = r3.aQn
                if (r1 != 0) goto L_0x001d
                java.lang.Object r1 = r4.getKey()
                if (r1 != 0) goto L_0x001c
            L_0x0011:
                V r1 = r3.value
                if (r1 != 0) goto L_0x002a
                java.lang.Object r1 = r4.getValue()
                if (r1 != 0) goto L_0x001c
            L_0x001b:
                r0 = 1
            L_0x001c:
                return r0
            L_0x001d:
                K r1 = r3.aQn
                java.lang.Object r2 = r4.getKey()
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x001c
                goto L_0x0011
            L_0x002a:
                V r1 = r3.value
                java.lang.Object r2 = r4.getValue()
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x001c
                goto L_0x001b
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzant.zzd.equals(java.lang.Object):boolean");
        }

        public K getKey() {
            return this.aQn;
        }

        public V getValue() {
            return this.value;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = this.aQn == null ? 0 : this.aQn.hashCode();
            if (this.value != null) {
                i = this.value.hashCode();
            }
            return hashCode ^ i;
        }

        public V setValue(V v) {
            V v2 = this.value;
            this.value = v;
            return v2;
        }

        public String toString() {
            String valueOf = String.valueOf(this.aQn);
            String valueOf2 = String.valueOf(this.value);
            return new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(valueOf2).length()).append(valueOf).append("=").append(valueOf2).toString();
        }

        public zzd<K, V> zzczx() {
            for (zzd<K, V> zzd = this.bfi; zzd != null; zzd = zzd.bfi) {
                this = zzd;
            }
            return this;
        }

        public zzd<K, V> zzczy() {
            for (zzd<K, V> zzd = this.bfj; zzd != null; zzd = zzd.bfj) {
                this = zzd;
            }
            return this;
        }
    }

    public zzant() {
        this(beW);
    }

    public zzant(Comparator<? super K> comparator) {
        this.size = 0;
        this.modCount = 0;
        this.beY = new zzd<>();
        this.aPZ = comparator == null ? beW : comparator;
    }

    private boolean equal(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    private void zza(zzd<K, V> zzd2) {
        int i = 0;
        zzd<K, V> zzd3 = zzd2.bfi;
        zzd<K, V> zzd4 = zzd2.bfj;
        zzd<K, V> zzd5 = zzd4.bfi;
        zzd<K, V> zzd6 = zzd4.bfj;
        zzd2.bfj = zzd5;
        if (zzd5 != null) {
            zzd5.bfh = zzd2;
        }
        zza(zzd2, zzd4);
        zzd4.bfi = zzd2;
        zzd2.bfh = zzd4;
        zzd2.height = Math.max(zzd3 != null ? zzd3.height : 0, zzd5 != null ? zzd5.height : 0) + 1;
        int i2 = zzd2.height;
        if (zzd6 != null) {
            i = zzd6.height;
        }
        zzd4.height = Math.max(i2, i) + 1;
    }

    private void zza(zzd<K, V> zzd2, zzd<K, V> zzd3) {
        zzd<K, V> zzd4 = zzd2.bfh;
        zzd2.bfh = null;
        if (zzd3 != null) {
            zzd3.bfh = zzd4;
        }
        if (zzd4 == null) {
            this.beX = zzd3;
        } else if (zzd4.bfi == zzd2) {
            zzd4.bfi = zzd3;
        } else if ($assertionsDisabled || zzd4.bfj == zzd2) {
            zzd4.bfj = zzd3;
        } else {
            throw new AssertionError();
        }
    }

    private void zzb(zzd<K, V> zzd2) {
        int i = 0;
        zzd<K, V> zzd3 = zzd2.bfi;
        zzd<K, V> zzd4 = zzd2.bfj;
        zzd<K, V> zzd5 = zzd3.bfi;
        zzd<K, V> zzd6 = zzd3.bfj;
        zzd2.bfi = zzd6;
        if (zzd6 != null) {
            zzd6.bfh = zzd2;
        }
        zza(zzd2, zzd3);
        zzd3.bfj = zzd2;
        zzd2.bfh = zzd3;
        zzd2.height = Math.max(zzd4 != null ? zzd4.height : 0, zzd6 != null ? zzd6.height : 0) + 1;
        int i2 = zzd2.height;
        if (zzd5 != null) {
            i = zzd5.height;
        }
        zzd3.height = Math.max(i2, i) + 1;
    }

    private void zzb(zzd<K, V> zzd2, boolean z) {
        while (zzd2 != null) {
            zzd<K, V> zzd3 = zzd2.bfi;
            zzd<K, V> zzd4 = zzd2.bfj;
            int i = zzd3 != null ? zzd3.height : 0;
            int i2 = zzd4 != null ? zzd4.height : 0;
            int i3 = i - i2;
            if (i3 == -2) {
                zzd<K, V> zzd5 = zzd4.bfi;
                zzd<K, V> zzd6 = zzd4.bfj;
                int i4 = (zzd5 != null ? zzd5.height : 0) - (zzd6 != null ? zzd6.height : 0);
                if (i4 == -1 || (i4 == 0 && !z)) {
                    zza(zzd2);
                } else if ($assertionsDisabled || i4 == 1) {
                    zzb(zzd4);
                    zza(zzd2);
                } else {
                    throw new AssertionError();
                }
                if (z) {
                    return;
                }
            } else if (i3 == 2) {
                zzd<K, V> zzd7 = zzd3.bfi;
                zzd<K, V> zzd8 = zzd3.bfj;
                int i5 = (zzd7 != null ? zzd7.height : 0) - (zzd8 != null ? zzd8.height : 0);
                if (i5 == 1 || (i5 == 0 && !z)) {
                    zzb(zzd2);
                } else if ($assertionsDisabled || i5 == -1) {
                    zza(zzd3);
                    zzb(zzd2);
                } else {
                    throw new AssertionError();
                }
                if (z) {
                    return;
                }
            } else if (i3 == 0) {
                zzd2.height = i + 1;
                if (z) {
                    return;
                }
            } else if ($assertionsDisabled || i3 == -1 || i3 == 1) {
                zzd2.height = Math.max(i, i2) + 1;
                if (!z) {
                    return;
                }
            } else {
                throw new AssertionError();
            }
            zzd2 = zzd2.bfh;
        }
    }

    public void clear() {
        this.beX = null;
        this.size = 0;
        this.modCount++;
        zzd<K, V> zzd2 = this.beY;
        zzd2.bfk = zzd2;
        zzd2.bfe = zzd2;
    }

    public boolean containsKey(Object obj) {
        return zzcm(obj) != null;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        zza zza2 = this.beZ;
        if (zza2 != null) {
            return zza2;
        }
        zza zza3 = new zza();
        this.beZ = zza3;
        return zza3;
    }

    public V get(Object obj) {
        zzd zzcm = zzcm(obj);
        if (zzcm != null) {
            return zzcm.value;
        }
        return null;
    }

    public Set<K> keySet() {
        zzb zzb2 = this.bfa;
        if (zzb2 != null) {
            return zzb2;
        }
        zzb zzb3 = new zzb();
        this.bfa = zzb3;
        return zzb3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzant.zza(java.lang.Object, boolean):com.google.android.gms.internal.zzant$zzd<K, V>
     arg types: [K, int]
     candidates:
      com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, com.google.android.gms.internal.zzant$zzd):void
      com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, boolean):void
      com.google.android.gms.internal.zzant.zza(java.lang.Object, boolean):com.google.android.gms.internal.zzant$zzd<K, V> */
    public V put(K k, V v) {
        if (k == null) {
            throw new NullPointerException("key == null");
        }
        zzd zza2 = zza((Object) k, true);
        V v2 = zza2.value;
        zza2.value = v;
        return v2;
    }

    public V remove(Object obj) {
        zzd zzcn = zzcn(obj);
        if (zzcn != null) {
            return zzcn.value;
        }
        return null;
    }

    public int size() {
        return this.size;
    }

    /* access modifiers changed from: package-private */
    public zzd<K, V> zza(K k, boolean z) {
        zzd<K, V> zzd2;
        int i;
        zzd<K, V> zzd3;
        Comparator<? super K> comparator = this.aPZ;
        zzd<K, V> zzd4 = this.beX;
        if (zzd4 != null) {
            Comparable comparable = comparator == beW ? (Comparable) k : null;
            while (true) {
                int compareTo = comparable != null ? comparable.compareTo(zzd4.aQn) : comparator.compare(k, zzd4.aQn);
                if (compareTo == 0) {
                    return zzd4;
                }
                zzd<K, V> zzd5 = compareTo < 0 ? zzd4.bfi : zzd4.bfj;
                if (zzd5 == null) {
                    int i2 = compareTo;
                    zzd2 = zzd4;
                    i = i2;
                    break;
                }
                zzd4 = zzd5;
            }
        } else {
            zzd2 = zzd4;
            i = 0;
        }
        if (!z) {
            return null;
        }
        zzd<K, V> zzd6 = this.beY;
        if (zzd2 != null) {
            zzd3 = new zzd<>(zzd2, k, zzd6, zzd6.bfk);
            if (i < 0) {
                zzd2.bfi = zzd3;
            } else {
                zzd2.bfj = zzd3;
            }
            zzb(zzd2, true);
        } else if (comparator != beW || (k instanceof Comparable)) {
            zzd3 = new zzd<>(zzd2, k, zzd6, zzd6.bfk);
            this.beX = zzd3;
        } else {
            throw new ClassCastException(String.valueOf(k.getClass().getName()).concat(" is not Comparable"));
        }
        this.size++;
        this.modCount++;
        return zzd3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, boolean):void
     arg types: [com.google.android.gms.internal.zzant$zzd<K, V>, int]
     candidates:
      com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, com.google.android.gms.internal.zzant$zzd):void
      com.google.android.gms.internal.zzant.zza(java.lang.Object, boolean):com.google.android.gms.internal.zzant$zzd<K, V>
      com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, boolean):void */
    /* access modifiers changed from: package-private */
    public void zza(zzd<K, V> zzd2, boolean z) {
        int i;
        int i2 = 0;
        if (z) {
            zzd2.bfk.bfe = zzd2.bfe;
            zzd2.bfe.bfk = zzd2.bfk;
        }
        zzd<K, V> zzd3 = zzd2.bfi;
        zzd<K, V> zzd4 = zzd2.bfj;
        zzd<K, V> zzd5 = zzd2.bfh;
        if (zzd3 == null || zzd4 == null) {
            if (zzd3 != null) {
                zza(zzd2, zzd3);
                zzd2.bfi = null;
            } else if (zzd4 != null) {
                zza(zzd2, zzd4);
                zzd2.bfj = null;
            } else {
                zza(zzd2, (zzd) null);
            }
            zzb(zzd5, false);
            this.size--;
            this.modCount++;
            return;
        }
        zzd<K, V> zzczy = zzd3.height > zzd4.height ? zzd3.zzczy() : zzd4.zzczx();
        zza((zzd) zzczy, false);
        zzd<K, V> zzd6 = zzd2.bfi;
        if (zzd6 != null) {
            i = zzd6.height;
            zzczy.bfi = zzd6;
            zzd6.bfh = zzczy;
            zzd2.bfi = null;
        } else {
            i = 0;
        }
        zzd<K, V> zzd7 = zzd2.bfj;
        if (zzd7 != null) {
            i2 = zzd7.height;
            zzczy.bfj = zzd7;
            zzd7.bfh = zzczy;
            zzd2.bfj = null;
        }
        zzczy.height = Math.max(i, i2) + 1;
        zza(zzd2, zzczy);
    }

    /* access modifiers changed from: package-private */
    public zzd<K, V> zzc(Map.Entry<?, ?> entry) {
        zzd<K, V> zzcm = zzcm(entry.getKey());
        if (zzcm != null && equal(zzcm.value, entry.getValue())) {
            return zzcm;
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzant.zza(java.lang.Object, boolean):com.google.android.gms.internal.zzant$zzd<K, V>
     arg types: [java.lang.Object, int]
     candidates:
      com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, com.google.android.gms.internal.zzant$zzd):void
      com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, boolean):void
      com.google.android.gms.internal.zzant.zza(java.lang.Object, boolean):com.google.android.gms.internal.zzant$zzd<K, V> */
    /* access modifiers changed from: package-private */
    public zzd<K, V> zzcm(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            return zza(obj, false);
        } catch (ClassCastException e) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, boolean):void
     arg types: [com.google.android.gms.internal.zzant$zzd<K, V>, int]
     candidates:
      com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, com.google.android.gms.internal.zzant$zzd):void
      com.google.android.gms.internal.zzant.zza(java.lang.Object, boolean):com.google.android.gms.internal.zzant$zzd<K, V>
      com.google.android.gms.internal.zzant.zza(com.google.android.gms.internal.zzant$zzd, boolean):void */
    /* access modifiers changed from: package-private */
    public zzd<K, V> zzcn(Object obj) {
        zzd<K, V> zzcm = zzcm(obj);
        if (zzcm != null) {
            zza((zzd) zzcm, true);
        }
        return zzcm;
    }
}
