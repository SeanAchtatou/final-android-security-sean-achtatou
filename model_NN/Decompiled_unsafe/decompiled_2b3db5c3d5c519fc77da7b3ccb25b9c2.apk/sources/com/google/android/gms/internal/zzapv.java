package com.google.android.gms.internal;

import java.io.IOException;

public abstract class zzapv {
    protected volatile int bjG = -1;

    public static final <T extends zzapv> T zza(zzapv zzapv, byte[] bArr) throws zzapu {
        return zzb(zzapv, bArr, 0, bArr.length);
    }

    public static final void zza(zzapv zzapv, byte[] bArr, int i, int i2) {
        try {
            zzapo zzc = zzapo.zzc(bArr, i, i2);
            zzapv.zza(zzc);
            zzc.az();
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static final <T extends com.google.android.gms.internal.zzapv> T zzb(T r2, byte[] r3, int r4, int r5) throws com.google.android.gms.internal.zzapu {
        /*
            com.google.android.gms.internal.zzapn r0 = com.google.android.gms.internal.zzapn.zzb(r3, r4, r5)     // Catch:{ zzapu -> 0x000c, IOException -> 0x000e }
            r2.zzb(r0)     // Catch:{ zzapu -> 0x000c, IOException -> 0x000e }
            r1 = 0
            r0.zzafo(r1)     // Catch:{ zzapu -> 0x000c, IOException -> 0x000e }
            return r2
        L_0x000c:
            r0 = move-exception
            throw r0
        L_0x000e:
            r0 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Reading from a byte array threw an IOException (should never happen)."
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzapv.zzb(com.google.android.gms.internal.zzapv, byte[], int, int):com.google.android.gms.internal.zzapv");
    }

    public static final byte[] zzf(zzapv zzapv) {
        byte[] bArr = new byte[zzapv.aM()];
        zza(zzapv, bArr, 0, bArr.length);
        return bArr;
    }

    /* renamed from: aB */
    public zzapv clone() throws CloneNotSupportedException {
        return (zzapv) super.clone();
    }

    public int aL() {
        if (this.bjG < 0) {
            aM();
        }
        return this.bjG;
    }

    public int aM() {
        int zzx = zzx();
        this.bjG = zzx;
        return zzx;
    }

    public String toString() {
        return zzapw.zzg(this);
    }

    public void zza(zzapo zzapo) throws IOException {
    }

    public abstract zzapv zzb(zzapn zzapn) throws IOException;

    /* access modifiers changed from: protected */
    public int zzx() {
        return 0;
    }
}
