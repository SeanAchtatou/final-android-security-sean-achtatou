package com.google.android.gms.common;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.internal.zzab;
import com.google.android.gms.common.internal.zzs;
import com.google.android.gms.common.internal.zzv;
import com.google.android.gms.common.util.zzm;
import com.google.android.gms.dynamic.zze;
import com.google.android.gms.internal.zzsb;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class zzd {
    private static zzv rg;
    private static Context rh;
    private static Set<zzs> ri;
    private static Set<zzs> rj;

    static abstract class zza extends zzs.zza {
        private int rk;

        protected zza(byte[] bArr) {
            boolean z = false;
            if (bArr.length != 25) {
                int length = bArr.length;
                String valueOf = String.valueOf(zzm.zza(bArr, 0, bArr.length, false));
                Log.wtf("GoogleCertificates", new StringBuilder(String.valueOf(valueOf).length() + 51).append("Cert hash data has incorrect length (").append(length).append("):\n").append(valueOf).toString(), new Exception());
                bArr = Arrays.copyOfRange(bArr, 0, 25);
                zzab.zzb(bArr.length == 25 ? true : z, new StringBuilder(55).append("cert hash data has incorrect length. length=").append(bArr.length).toString());
            }
            this.rk = Arrays.hashCode(bArr);
        }

        protected static byte[] zzgv(String str) {
            try {
                return str.getBytes("ISO-8859-1");
            } catch (UnsupportedEncodingException e) {
                throw new AssertionError(e);
            }
        }

        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof zzs)) {
                return false;
            }
            try {
                zzs zzs = (zzs) obj;
                if (zzs.zzanl() != hashCode()) {
                    return false;
                }
                com.google.android.gms.dynamic.zzd zzank = zzs.zzank();
                if (zzank == null) {
                    return false;
                }
                return Arrays.equals(getBytes(), (byte[]) zze.zzad(zzank));
            } catch (RemoteException e) {
                Log.e("GoogleCertificates", "iCertData failed to retrive data from remote");
                return false;
            }
        }

        /* access modifiers changed from: package-private */
        public abstract byte[] getBytes();

        public int hashCode() {
            return this.rk;
        }

        public com.google.android.gms.dynamic.zzd zzank() {
            return zze.zzac(getBytes());
        }

        public int zzanl() {
            return hashCode();
        }
    }

    static class zzb extends zza {
        private final byte[] rl;

        zzb(byte[] bArr) {
            super(Arrays.copyOfRange(bArr, 0, 25));
            this.rl = bArr;
        }

        /* access modifiers changed from: package-private */
        public byte[] getBytes() {
            return this.rl;
        }
    }

    static abstract class zzc extends zza {
        private static final WeakReference<byte[]> rn = new WeakReference<>(null);
        private WeakReference<byte[]> rm = rn;

        zzc(byte[] bArr) {
            super(bArr);
        }

        /* access modifiers changed from: package-private */
        public byte[] getBytes() {
            byte[] bArr;
            synchronized (this) {
                bArr = this.rm.get();
                if (bArr == null) {
                    bArr = zzanm();
                    this.rm = new WeakReference<>(bArr);
                }
            }
            return bArr;
        }

        /* access modifiers changed from: protected */
        public abstract byte[] zzanm();
    }

    /* renamed from: com.google.android.gms.common.zzd$zzd  reason: collision with other inner class name */
    static final class C0008zzd {
        static final zza[] ro = {new zzc(zza.zzgv("0\u0004C0\u0003+ \u0003\u0002\u0001\u0002\u0002\t\u0000ÂàFdJ00")) {
            /* access modifiers changed from: protected */
            public byte[] zzanm() {
                return zza.zzgv("0\u0004C0\u0003+ \u0003\u0002\u0001\u0002\u0002\t\u0000ÂàFdJ00\r\u0006\t*H÷\r\u0001\u0001\u0004\u0005\u00000t1\u000b0\t\u0006\u0003U\u0004\u0006\u0013\u0002US1\u00130\u0011\u0006\u0003U\u0004\b\u0013\nCalifornia1\u00160\u0014\u0006\u0003U\u0004\u0007\u0013\rMountain View1\u00140\u0012\u0006\u0003U\u0004\n\u0013\u000bGoogle Inc.1\u00100\u000e\u0006\u0003U\u0004\u000b\u0013\u0007Android1\u00100\u000e\u0006\u0003U\u0004\u0003\u0013\u0007Android0\u001e\u0017\r080821231334Z\u0017\r360107231334Z0t1\u000b0\t\u0006\u0003U\u0004\u0006\u0013\u0002US1\u00130\u0011\u0006\u0003U\u0004\b\u0013\nCalifornia1\u00160\u0014\u0006\u0003U\u0004\u0007\u0013\rMountain View1\u00140\u0012\u0006\u0003U\u0004\n\u0013\u000bGoogle Inc.1\u00100\u000e\u0006\u0003U\u0004\u000b\u0013\u0007Android1\u00100\u000e\u0006\u0003U\u0004\u0003\u0013\u0007Android0\u0001 0\r\u0006\t*H÷\r\u0001\u0001\u0001\u0005\u0000\u0003\u0001\r\u00000\u0001\b\u0002\u0001\u0001\u0000«V.\u0000Ø;¢\b®\no\u0012N)Ú\u0011ò«VÐXâÌ©\u0013\u0003é·TÓrö@§\u001b\u001dË\u0013\tgbNFV§wj\u0019=²å¿·$©\u001ew\u0018\u000ejG¤;3Ù`w\u00181EÌß{.XftÉáV[\u001fLjYU¿òQ¦=«ùÅ\\'\"\"Rèuäø\u0015Jd_qhÀ±¿Æ\u0012ê¿xWi»4ªyÜ~.¢vL®\u0007ØÁqT×î_d¥\u001aD¦\u0002ÂI\u0005AWÜ\u0002Í_\\\u000eUûï\u0019ûã'ð±Q\u0016Å o\u0019ÑõÄÛÂÖ¹?hÌ)yÇ\u000e\u0018«k;ÕÛU*\u000e;LßXûíÁº5à\u0003Á´±\rÒD¨î$ÿý38r«R!^Ú°ü\r\u000b\u0014[j¡y\u0002\u0001\u0003£Ù0Ö0\u001d\u0006\u0003U\u001d\u000e\u0004\u0016\u0004\u0014Ç}Â!\u0017V%Óßkãä×¥0¦\u0006\u0003U\u001d#\u00040\u0014Ç}Â!\u0017V%Óßkãä×¥¡x¤v0t1\u000b0\t\u0006\u0003U\u0004\u0006\u0013\u0002US1\u00130\u0011\u0006\u0003U\u0004\b\u0013\nCalifornia1\u00160\u0014\u0006\u0003U\u0004\u0007\u0013\rMountain View1\u00140\u0012\u0006\u0003U\u0004\n\u0013\u000bGoogle Inc.1\u00100\u000e\u0006\u0003U\u0004\u000b\u0013\u0007Android1\u00100\u000e\u0006\u0003U\u0004\u0003\u0013\u0007Android\t\u0000ÂàFdJ00\f\u0006\u0003U\u001d\u0013\u0004\u00050\u0003\u0001\u0001ÿ0\r\u0006\t*H÷\r\u0001\u0001\u0004\u0005\u0000\u0003\u0001\u0001\u0000mÒRÎï0,6\nªÎÏòÌ©\u0004»]z\u0016aø®F²B\u0004ÐÿJhÇí\u001aS\u001eÄYZb<æ\u0007c±g)zzãW\u0012Ä\u0007ò\bðË\u0010)\u0012M{\u0010b\u0019ÀÊ>³ù­_¸qï&âñmDÈÙ l²ð\u0005»?âËD~s\u0010v­E³?`\tê\u0019Áaæ&Aª'\u001dýR(ÅÅ]ÛE'XÖaöÌ\fÌ·5.BLÄ6\\R52÷2Q7Y<JãAôÛAíÚ\r\u000b\u0010q§Ä@ðþ \u001c¶'ÊgCiÐ½/Ù\u0011ÿ\u0006Í¿,ú\u0010Ü\u000f:ãWbHÇïÆLqD\u0017B÷\u0005ÉÞW:õ[9\r×ý¹A1]_u0\u0011&ÿb\u0014\u0010Ài0");
            }
        }, new zzc(zza.zzgv("0\u0004¨0\u0003 \u0003\u0002\u0001\u0002\u0002\t\u0000Õ¸l}ÓNõ0")) {
            /* access modifiers changed from: protected */
            public byte[] zzanm() {
                return zza.zzgv("0\u0004¨0\u0003 \u0003\u0002\u0001\u0002\u0002\t\u0000Õ¸l}ÓNõ0\r\u0006\t*H÷\r\u0001\u0001\u0004\u0005\u000001\u000b0\t\u0006\u0003U\u0004\u0006\u0013\u0002US1\u00130\u0011\u0006\u0003U\u0004\b\u0013\nCalifornia1\u00160\u0014\u0006\u0003U\u0004\u0007\u0013\rMountain View1\u00100\u000e\u0006\u0003U\u0004\n\u0013\u0007Android1\u00100\u000e\u0006\u0003U\u0004\u000b\u0013\u0007Android1\u00100\u000e\u0006\u0003U\u0004\u0003\u0013\u0007Android1\"0 \u0006\t*H÷\r\u0001\t\u0001\u0016\u0013android@android.com0\u001e\u0017\r080415233656Z\u0017\r350901233656Z01\u000b0\t\u0006\u0003U\u0004\u0006\u0013\u0002US1\u00130\u0011\u0006\u0003U\u0004\b\u0013\nCalifornia1\u00160\u0014\u0006\u0003U\u0004\u0007\u0013\rMountain View1\u00100\u000e\u0006\u0003U\u0004\n\u0013\u0007Android1\u00100\u000e\u0006\u0003U\u0004\u000b\u0013\u0007Android1\u00100\u000e\u0006\u0003U\u0004\u0003\u0013\u0007Android1\"0 \u0006\t*H÷\r\u0001\t\u0001\u0016\u0013android@android.com0\u0001 0\r\u0006\t*H÷\r\u0001\u0001\u0001\u0005\u0000\u0003\u0001\r\u00000\u0001\b\u0002\u0001\u0001\u0000ÖÎ.\b\n¿â1MÑ³ÏÓ\u0018\\´=3ú\ftá½¶ÑÛ\u0013ö,\\9ßVøF=e¾ÀóÊBk\u0007Å¨íZ9ÁgçkÉ¹'K\u000b\"\u0000\u0019©)\u0015årÅm*0\u001b£oÅü\u0011:ÖËt5¡m#«}úîáeäß\u001f\n½§\nQlN\u0005\u0011Ê|\fU\u0017[ÃuùHÅj®\b¤O¦¤Ý}¿,\n5\"­\u0006¸Ì\u0018^±Uyîøm\b\u000b\u001daÀù¯±ÂëÑ\u0007êE«Ûh£Ç^TÇlSÔ\u000b\u0012\u001dç»Ó\u000eb\f\u0018áªaÛ¼Ý<d_/UóÔÃuì@p©?qQØ6pÁj\u001a¾^òÑ\u0018á¸®ó)ðf¿láD¬èm\u001c\u001b\u000f\u0002\u0001\u0003£ü0ù0\u001d\u0006\u0003U\u001d\u000e\u0004\u0016\u0004\u0014\u001cÅ¾LC<a:\u0015°L¼\u0003òOà²0É\u0006\u0003U\u001d#\u0004Á0¾\u0014\u001cÅ¾LC<a:\u0015°L¼\u0003òOà²¡¤01\u000b0\t\u0006\u0003U\u0004\u0006\u0013\u0002US1\u00130\u0011\u0006\u0003U\u0004\b\u0013\nCalifornia1\u00160\u0014\u0006\u0003U\u0004\u0007\u0013\rMountain View1\u00100\u000e\u0006\u0003U\u0004\n\u0013\u0007Android1\u00100\u000e\u0006\u0003U\u0004\u000b\u0013\u0007Android1\u00100\u000e\u0006\u0003U\u0004\u0003\u0013\u0007Android1\"0 \u0006\t*H÷\r\u0001\t\u0001\u0016\u0013android@android.com\t\u0000Õ¸l}ÓNõ0\f\u0006\u0003U\u001d\u0013\u0004\u00050\u0003\u0001\u0001ÿ0\r\u0006\t*H÷\r\u0001\u0001\u0004\u0005\u0000\u0003\u0001\u0001\u0000\u0019Ó\fñ\u0005ûx?L\r}Ò##=@zÏÎ\u0000\b\u001d[×ÆéÖí k\u000e\u0011 \u0006Al¢D\u0013ÒkJ àõ$ÊÒ»\\nL¡\u0001j\u0015n¡ì]ÉZ^:\u0001\u00006ôHÕ\u0010¿.\u001eag:;åm¯\u000bw±Â)ãÂUãèL]#ïº\tËñ; +NZ\"É2cHJ#Òü)ú\u00199u3¯Øª\u0016\u000fBÂÐ\u0016>fCéÁ/ Á33[Àÿk\"ÞÑ­DB)¥9©Nï­«ÐeÎÒK>QåÝ{fx{ï\u0012þû¤Ä#ûOøÌIL\u0002ðõ\u0005\u0016\u0012ÿe)9>FêÅ»!òwÁQª_*¦'Ñè§\n¶\u00035iÞ;¿ÿ|©Ú>\u0012Cö\u000b");
            }
        }};
    }

    private static Set<zzs> zza(IBinder[] iBinderArr) throws RemoteException {
        HashSet hashSet = new HashSet(r1);
        for (IBinder zzdr : iBinderArr) {
            zzs zzdr2 = zzs.zza.zzdr(zzdr);
            if (zzdr2 == null) {
                Log.e("GoogleCertificates", "iCertData is null, skipping");
            } else {
                hashSet.add(zzdr2);
            }
        }
        return hashSet;
    }

    private static boolean zzanh() {
        zzab.zzy(rh);
        if (rg == null) {
            try {
                zzsb zza2 = zzsb.zza(rh, zzsb.KM, "com.google.android.gms.googlecertificates");
                Log.d("GoogleCertificates", "com.google.android.gms.googlecertificates module is loaded");
                rg = zzv.zza.zzdu(zza2.zziu("com.google.android.gms.common.GoogleCertificatesImpl"));
            } catch (zzsb.zza e) {
                String valueOf = String.valueOf("Failed to load com.google.android.gms.googlecertificates: ");
                String valueOf2 = String.valueOf(e.getMessage());
                Log.e("GoogleCertificates", valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf));
                return false;
            }
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0054, code lost:
        android.util.Log.e("GoogleCertificates", "Failed to retrieve google certificates");
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static synchronized java.util.Set<com.google.android.gms.common.internal.zzs> zzani() {
        /*
            java.lang.Class<com.google.android.gms.common.zzd> r1 = com.google.android.gms.common.zzd.class
            monitor-enter(r1)
            java.util.Set<com.google.android.gms.common.internal.zzs> r0 = com.google.android.gms.common.zzd.ri     // Catch:{ all -> 0x005c }
            if (r0 == 0) goto L_0x000b
            java.util.Set<com.google.android.gms.common.internal.zzs> r0 = com.google.android.gms.common.zzd.ri     // Catch:{ all -> 0x005c }
        L_0x0009:
            monitor-exit(r1)
            return r0
        L_0x000b:
            com.google.android.gms.common.internal.zzv r0 = com.google.android.gms.common.zzd.rg     // Catch:{ all -> 0x005c }
            if (r0 != 0) goto L_0x0018
            boolean r0 = zzanh()     // Catch:{ all -> 0x005c }
            if (r0 != 0) goto L_0x0018
            java.util.Set r0 = java.util.Collections.EMPTY_SET     // Catch:{ all -> 0x005c }
            goto L_0x0009
        L_0x0018:
            com.google.android.gms.common.internal.zzv r0 = com.google.android.gms.common.zzd.rg     // Catch:{ RemoteException -> 0x0053 }
            com.google.android.gms.dynamic.zzd r0 = r0.zzatc()     // Catch:{ RemoteException -> 0x0053 }
            if (r0 != 0) goto L_0x002a
            java.lang.String r0 = "GoogleCertificates"
            java.lang.String r2 = "Failed to get google certificates from remote"
            android.util.Log.e(r0, r2)     // Catch:{ RemoteException -> 0x0053 }
            java.util.Set r0 = java.util.Collections.EMPTY_SET     // Catch:{ RemoteException -> 0x0053 }
            goto L_0x0009
        L_0x002a:
            java.lang.Object r0 = com.google.android.gms.dynamic.zze.zzad(r0)     // Catch:{ RemoteException -> 0x0053 }
            android.os.IBinder[] r0 = (android.os.IBinder[]) r0     // Catch:{ RemoteException -> 0x0053 }
            java.util.Set r0 = zza(r0)     // Catch:{ RemoteException -> 0x0053 }
            com.google.android.gms.common.zzd.ri = r0     // Catch:{ RemoteException -> 0x0053 }
            r0 = 0
        L_0x0037:
            com.google.android.gms.common.zzd$zza[] r2 = com.google.android.gms.common.zzd.C0008zzd.ro     // Catch:{ RemoteException -> 0x0053 }
            int r2 = r2.length     // Catch:{ RemoteException -> 0x0053 }
            if (r0 >= r2) goto L_0x0048
            java.util.Set<com.google.android.gms.common.internal.zzs> r2 = com.google.android.gms.common.zzd.ri     // Catch:{ RemoteException -> 0x0053 }
            com.google.android.gms.common.zzd$zza[] r3 = com.google.android.gms.common.zzd.C0008zzd.ro     // Catch:{ RemoteException -> 0x0053 }
            r3 = r3[r0]     // Catch:{ RemoteException -> 0x0053 }
            r2.add(r3)     // Catch:{ RemoteException -> 0x0053 }
            int r0 = r0 + 1
            goto L_0x0037
        L_0x0048:
            java.util.Set<com.google.android.gms.common.internal.zzs> r0 = com.google.android.gms.common.zzd.ri     // Catch:{ RemoteException -> 0x0053 }
            java.util.Set r0 = java.util.Collections.unmodifiableSet(r0)     // Catch:{ RemoteException -> 0x0053 }
            com.google.android.gms.common.zzd.ri = r0     // Catch:{ RemoteException -> 0x0053 }
        L_0x0050:
            java.util.Set<com.google.android.gms.common.internal.zzs> r0 = com.google.android.gms.common.zzd.ri     // Catch:{ all -> 0x005c }
            goto L_0x0009
        L_0x0053:
            r0 = move-exception
            java.lang.String r0 = "GoogleCertificates"
            java.lang.String r2 = "Failed to retrieve google certificates"
            android.util.Log.e(r0, r2)     // Catch:{ all -> 0x005c }
            goto L_0x0050
        L_0x005c:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.zzd.zzani():java.util.Set");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004c, code lost:
        android.util.Log.e("GoogleCertificates", "Failed to retrieve google release certificates");
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static synchronized java.util.Set<com.google.android.gms.common.internal.zzs> zzanj() {
        /*
            java.lang.Class<com.google.android.gms.common.zzd> r1 = com.google.android.gms.common.zzd.class
            monitor-enter(r1)
            java.util.Set<com.google.android.gms.common.internal.zzs> r0 = com.google.android.gms.common.zzd.rj     // Catch:{ all -> 0x0054 }
            if (r0 == 0) goto L_0x000b
            java.util.Set<com.google.android.gms.common.internal.zzs> r0 = com.google.android.gms.common.zzd.rj     // Catch:{ all -> 0x0054 }
        L_0x0009:
            monitor-exit(r1)
            return r0
        L_0x000b:
            com.google.android.gms.common.internal.zzv r0 = com.google.android.gms.common.zzd.rg     // Catch:{ all -> 0x0054 }
            if (r0 != 0) goto L_0x0018
            boolean r0 = zzanh()     // Catch:{ all -> 0x0054 }
            if (r0 != 0) goto L_0x0018
            java.util.Set r0 = java.util.Collections.EMPTY_SET     // Catch:{ all -> 0x0054 }
            goto L_0x0009
        L_0x0018:
            com.google.android.gms.common.internal.zzv r0 = com.google.android.gms.common.zzd.rg     // Catch:{ RemoteException -> 0x004b }
            com.google.android.gms.dynamic.zzd r0 = r0.zzatd()     // Catch:{ RemoteException -> 0x004b }
            if (r0 != 0) goto L_0x002a
            java.lang.String r0 = "GoogleCertificates"
            java.lang.String r2 = "Failed to get google release certificates from remote"
            android.util.Log.d(r0, r2)     // Catch:{ RemoteException -> 0x004b }
            java.util.Set r0 = java.util.Collections.EMPTY_SET     // Catch:{ RemoteException -> 0x004b }
            goto L_0x0009
        L_0x002a:
            java.lang.Object r0 = com.google.android.gms.dynamic.zze.zzad(r0)     // Catch:{ RemoteException -> 0x004b }
            android.os.IBinder[] r0 = (android.os.IBinder[]) r0     // Catch:{ RemoteException -> 0x004b }
            java.util.Set r0 = zza(r0)     // Catch:{ RemoteException -> 0x004b }
            com.google.android.gms.common.zzd.rj = r0     // Catch:{ RemoteException -> 0x004b }
            java.util.Set<com.google.android.gms.common.internal.zzs> r0 = com.google.android.gms.common.zzd.rj     // Catch:{ RemoteException -> 0x004b }
            com.google.android.gms.common.zzd$zza[] r2 = com.google.android.gms.common.zzd.C0008zzd.ro     // Catch:{ RemoteException -> 0x004b }
            r3 = 0
            r2 = r2[r3]     // Catch:{ RemoteException -> 0x004b }
            r0.add(r2)     // Catch:{ RemoteException -> 0x004b }
            java.util.Set<com.google.android.gms.common.internal.zzs> r0 = com.google.android.gms.common.zzd.rj     // Catch:{ RemoteException -> 0x004b }
            java.util.Set r0 = java.util.Collections.unmodifiableSet(r0)     // Catch:{ RemoteException -> 0x004b }
            com.google.android.gms.common.zzd.rj = r0     // Catch:{ RemoteException -> 0x004b }
        L_0x0048:
            java.util.Set<com.google.android.gms.common.internal.zzs> r0 = com.google.android.gms.common.zzd.rj     // Catch:{ all -> 0x0054 }
            goto L_0x0009
        L_0x004b:
            r0 = move-exception
            java.lang.String r0 = "GoogleCertificates"
            java.lang.String r2 = "Failed to retrieve google release certificates"
            android.util.Log.e(r0, r2)     // Catch:{ all -> 0x0054 }
            goto L_0x0048
        L_0x0054:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.zzd.zzanj():java.util.Set");
    }

    static synchronized void zzbq(Context context) {
        synchronized (zzd.class) {
            if (rh != null) {
                Log.w("GoogleCertificates", "GoogleCertificates has been initialized already");
            } else if (context != null) {
                rh = context.getApplicationContext();
            }
        }
    }
}
