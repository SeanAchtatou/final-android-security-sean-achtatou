package com.google.firebase.messaging;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.facebook.appevents.AppEventsConstants;
import com.google.firebase.iid.FirebaseInstanceIdInternalReceiver;
import com.google.firebase.iid.FirebaseInstanceIdReceiver;
import com.google.firebase.iid.zzb;
import java.util.Iterator;

public class FirebaseMessagingService extends zzb {
    static void zzab(Bundle bundle) {
        Iterator<String> it = bundle.keySet().iterator();
        while (it.hasNext()) {
            String next = it.next();
            if (next != null && next.startsWith("google.c.")) {
                it.remove();
            }
        }
    }

    private void zzae(Intent intent) {
        PendingIntent pendingIntent = (PendingIntent) intent.getParcelableExtra("pending_intent");
        if (pendingIntent != null) {
            try {
                pendingIntent.send();
            } catch (PendingIntent.CanceledException e) {
                Log.e("FirebaseMessaging", "Notification pending intent canceled");
            }
        }
        if (zzav(intent.getExtras())) {
            zzb.zzm(this, intent);
        }
    }

    static boolean zzav(Bundle bundle) {
        if (bundle == null) {
            return false;
        }
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(bundle.getString("google.c.a.e"));
    }

    private void zzn(Intent intent) {
        String stringExtra = intent.getStringExtra("message_type");
        if (stringExtra == null) {
            stringExtra = "gcm";
        }
        char c = 65535;
        switch (stringExtra.hashCode()) {
            case -2062414158:
                if (stringExtra.equals("deleted_messages")) {
                    c = 1;
                    break;
                }
                break;
            case 102161:
                if (stringExtra.equals("gcm")) {
                    c = 0;
                    break;
                }
                break;
            case 814694033:
                if (stringExtra.equals("send_error")) {
                    c = 3;
                    break;
                }
                break;
            case 814800675:
                if (stringExtra.equals("send_event")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                if (zzav(intent.getExtras())) {
                    zzb.zzl(this, intent);
                }
                zzo(intent);
                return;
            case 1:
                onDeletedMessages();
                return;
            case 2:
                onMessageSent(intent.getStringExtra("google.message_id"));
                return;
            case 3:
                onSendError(zzp(intent), new SendException(intent.getStringExtra("error")));
                return;
            default:
                String valueOf = String.valueOf(stringExtra);
                Log.w("FirebaseMessaging", valueOf.length() != 0 ? "Received message with unknown type: ".concat(valueOf) : new String("Received message with unknown type: "));
                return;
        }
    }

    private void zzo(Intent intent) {
        Bundle extras = intent.getExtras();
        extras.remove("android.support.content.wakelockid");
        if (zza.zzac(extras)) {
            if (!zza.zzdc(this)) {
                zza.zzer(this).zzas(extras);
                return;
            } else if (zzav(extras)) {
                zzb.zzo(this, intent);
            }
        }
        onMessageReceived(new RemoteMessage(extras));
    }

    private String zzp(Intent intent) {
        String stringExtra = intent.getStringExtra("google.message_id");
        return stringExtra == null ? intent.getStringExtra("message_id") : stringExtra;
    }

    @WorkerThread
    public void onDeletedMessages() {
    }

    @WorkerThread
    public void onMessageReceived(RemoteMessage remoteMessage) {
    }

    @WorkerThread
    public void onMessageSent(String str) {
    }

    @WorkerThread
    public void onSendError(String str, Exception exc) {
    }

    /* access modifiers changed from: protected */
    public Intent zzaa(Intent intent) {
        return FirebaseInstanceIdInternalReceiver.zzcwx();
    }

    /* access modifiers changed from: protected */
    public int zzab(Intent intent) {
        if (!"com.google.firebase.messaging.NOTIFICATION_OPEN".equals(intent.getAction())) {
            return super.zzab(intent);
        }
        zzae(intent);
        zzble();
        FirebaseInstanceIdReceiver.completeWakefulIntent(intent);
        return 3;
    }

    public void zzm(Intent intent) {
        try {
            String action = intent.getAction();
            char c = 65535;
            switch (action.hashCode()) {
                case 75300319:
                    if (action.equals("com.google.firebase.messaging.NOTIFICATION_DISMISS")) {
                        c = 1;
                        break;
                    }
                    break;
                case 366519424:
                    if (action.equals("com.google.android.c2dm.intent.RECEIVE")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    zzn(intent);
                    break;
                case 1:
                    if (zzav(intent.getExtras())) {
                        zzb.zzn(this, intent);
                        break;
                    }
                    break;
                default:
                    String valueOf = String.valueOf(intent.getAction());
                    Log.d("FirebaseMessaging", valueOf.length() != 0 ? "Unknown intent action: ".concat(valueOf) : new String("Unknown intent action: "));
                    break;
            }
            zzble();
        } finally {
            FirebaseInstanceIdReceiver.completeWakefulIntent(intent);
        }
    }
}
