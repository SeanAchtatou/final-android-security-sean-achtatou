package com.google.firebase.iid;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import com.facebook.appevents.AppEventsConstants;
import com.tencent.android.tpush.common.MessageKey;
import java.io.IOException;
import java.security.KeyPair;
import java.util.HashMap;
import java.util.Map;

public class zzd {
    static Map<String, zzd> aap = new HashMap();
    static String aav;
    private static zzg baF;
    private static zzf baG;
    KeyPair aas;
    String aat = "";
    long aau;
    Context mContext;

    protected zzd(Context context, String str, Bundle bundle) {
        this.mContext = context.getApplicationContext();
        this.aat = str;
    }

    public static synchronized zzd zzb(Context context, Bundle bundle) {
        zzd zzd;
        synchronized (zzd.class) {
            String string = bundle == null ? "" : bundle.getString("subtype");
            String str = string == null ? "" : string;
            Context applicationContext = context.getApplicationContext();
            if (baF == null) {
                baF = new zzg(applicationContext);
                baG = new zzf(applicationContext);
            }
            aav = Integer.toString(FirebaseInstanceId.zzdf(applicationContext));
            zzd = aap.get(str);
            if (zzd == null) {
                zzd = new zzd(applicationContext, str, bundle);
                aap.put(str, zzd);
            }
        }
        return zzd;
    }

    public long getCreationTime() {
        String str;
        if (this.aau == 0 && (str = baF.get(this.aat, "cre")) != null) {
            this.aau = Long.parseLong(str);
        }
        return this.aau;
    }

    public String getToken(String str, String str2, Bundle bundle) throws IOException {
        boolean z = false;
        if (Looper.getMainLooper() == Looper.myLooper()) {
            throw new IOException("MAIN_THREAD");
        }
        boolean z2 = true;
        String zzi = zzbma() ? null : baF.zzi(this.aat, str, str2);
        if (zzi == null) {
            if (bundle == null) {
                bundle = new Bundle();
            }
            if (bundle.getString(MessageKey.MSG_TTL) != null) {
                z2 = false;
            }
            if (!"jwt".equals(bundle.getString("type"))) {
                z = z2;
            }
            zzi = zzc(str, str2, bundle);
            if (zzi != null && z) {
                baF.zza(this.aat, str, str2, zzi, aav);
            }
        }
        return zzi;
    }

    public void zzb(String str, String str2, Bundle bundle) throws IOException {
        if (Looper.getMainLooper() == Looper.myLooper()) {
            throw new IOException("MAIN_THREAD");
        }
        baF.zzj(this.aat, str, str2);
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putString("sender", str);
        if (str2 != null) {
            bundle.putString("scope", str2);
        }
        bundle.putString("subscription", str);
        bundle.putString("delete", AppEventsConstants.EVENT_PARAM_VALUE_YES);
        bundle.putString("X-delete", AppEventsConstants.EVENT_PARAM_VALUE_YES);
        bundle.putString("subtype", "".equals(this.aat) ? str : this.aat);
        if (!"".equals(this.aat)) {
            str = this.aat;
        }
        bundle.putString("X-subtype", str);
        baG.zzt(baG.zza(bundle, zzblw()));
    }

    /* access modifiers changed from: package-private */
    public KeyPair zzblw() {
        if (this.aas == null) {
            this.aas = baF.zzkh(this.aat);
        }
        if (this.aas == null) {
            this.aau = System.currentTimeMillis();
            this.aas = baF.zze(this.aat, this.aau);
        }
        return this.aas;
    }

    public void zzblx() {
        this.aau = 0;
        baF.zzki(this.aat);
        this.aas = null;
    }

    /* access modifiers changed from: package-private */
    public boolean zzbma() {
        String str;
        String str2 = baF.get("appVersion");
        if (str2 == null || !str2.equals(aav) || (str = baF.get("lastToken")) == null) {
            return true;
        }
        return (System.currentTimeMillis() / 1000) - Long.valueOf(Long.parseLong(str)).longValue() > 604800;
    }

    public String zzc(String str, String str2, Bundle bundle) throws IOException {
        if (str2 != null) {
            bundle.putString("scope", str2);
        }
        bundle.putString("sender", str);
        String str3 = "".equals(this.aat) ? str : this.aat;
        if (!bundle.containsKey("legacy.register")) {
            bundle.putString("subscription", str);
            bundle.putString("subtype", str3);
            bundle.putString("X-subscription", str);
            bundle.putString("X-subtype", str3);
        }
        return baG.zzt(baG.zza(bundle, zzblw()));
    }

    public zzg zzcwy() {
        return baF;
    }

    public zzf zzcwz() {
        return baG;
    }
}
