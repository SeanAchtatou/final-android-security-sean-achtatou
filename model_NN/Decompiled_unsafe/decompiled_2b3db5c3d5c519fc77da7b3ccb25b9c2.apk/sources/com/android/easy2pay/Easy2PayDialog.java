package com.android.easy2pay;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import java.util.Vector;

class Easy2PayDialog extends Dialog {
    private static final float BUTTION_OK_WEIGHT = 0.7f;
    private static final float DIMENSIONS_DIFF_PORTRAIT = 40.0f;
    private final String description;
    private final int langId;
    /* access modifiers changed from: private */
    public final Easy2PayDialogListener listener;
    /* access modifiers changed from: private */
    public final Vector<String[]> priceList;
    /* access modifiers changed from: private */
    public final String ptxId;
    private final String title;
    /* access modifiers changed from: private */
    public final String userId;

    protected Easy2PayDialog(Context context, String title2, String description2, Vector<String[]> priceList2, String ptxId2, String userId2, Easy2PayDialogListener listener2, int langId2) {
        super(context);
        this.listener = listener2;
        this.title = title2;
        this.description = description2;
        this.priceList = priceList2;
        this.ptxId = ptxId2;
        this.userId = userId2;
        this.langId = langId2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        String str;
        String desc;
        Spinner sp_price;
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(false);
        requestWindowFeature(1);
        float scale = getContext().getResources().getDisplayMetrics().density;
        LinearLayout layout_main = new LinearLayout(getContext());
        layout_main.setOrientation(1);
        addContentView(layout_main, new LinearLayout.LayoutParams(((int) (320.0f * scale)) - ((int) ((DIMENSIONS_DIFF_PORTRAIT * scale) + 0.5f)), -2));
        TextView tv_title = new TextView(getContext());
        tv_title.setText(this.title);
        tv_title.setTextColor(-1);
        tv_title.setTypeface(Typeface.DEFAULT_BOLD);
        tv_title.setBackgroundColor(2144128204);
        tv_title.setPadding((int) (10.0f * scale), (int) (2.0f * scale), (int) (10.0f * scale), (int) (2.0f * scale));
        layout_main.addView(tv_title);
        TextView textView = new TextView(getContext());
        String desc2 = this.description;
        if (desc2.contains("%s")) {
            desc = desc2.replace("%s", this.priceList.elementAt(0)[1]);
        } else {
            StringBuilder append = new StringBuilder().append(desc2);
            if (this.priceList.size() == 1) {
                str = " \"" + this.priceList.elementAt(0)[1] + "\" ?";
            } else {
                str = " ?";
            }
            desc = append.append(str).toString();
        }
        textView.setText(desc);
        textView.setTextColor(-3355444);
        textView.setPadding((int) (10.0f * scale), (int) (5.0f * scale), (int) (10.0f * scale), (int) (5.0f * scale));
        layout_main.addView(textView);
        if (this.priceList.size() > 1) {
            sp_price = new Spinner(getContext());
            String[] prices = new String[this.priceList.size()];
            for (int i = 0; i < this.priceList.size(); i++) {
                prices[i] = this.priceList.elementAt(i)[1];
            }
            sp_price.setAdapter((SpinnerAdapter) new ArrayAdapter<>(getContext(), 17367048, prices));
            LinearLayout.LayoutParams params_price = new LinearLayout.LayoutParams(-2, -2);
            params_price.setMargins((int) (10.0f * scale), 0, (int) (10.0f * scale), (int) (5.0f * scale));
            params_price.gravity = 1;
            layout_main.addView(sp_price, params_price);
        } else {
            sp_price = null;
        }
        LinearLayout layout_button = new LinearLayout(getContext());
        layout_button.setOrientation(0);
        LinearLayout.LayoutParams params_button = new LinearLayout.LayoutParams(-1, -2);
        params_button.setMargins((int) (10.0f * scale), (int) (5.0f * scale), (int) (10.0f * scale), (int) (5.0f * scale));
        layout_main.addView(layout_button, params_button);
        Button bn_ok = new Button(getContext());
        LinearLayout.LayoutParams params_ok = new LinearLayout.LayoutParams(-2, -2);
        params_ok.weight = BUTTION_OK_WEIGHT;
        bn_ok.setLayoutParams(params_ok);
        bn_ok.setText(Resource.TXT_BUTTON_OK[this.langId]);
        final Spinner spinner = sp_price;
        bn_ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int i;
                if (Easy2PayDialog.this.listener != null) {
                    Vector access$100 = Easy2PayDialog.this.priceList;
                    if (spinner != null) {
                        i = spinner.getSelectedItemPosition();
                    } else {
                        i = 0;
                    }
                    String[] priceData = (String[]) access$100.elementAt(i);
                    Easy2PayDialog.this.listener.onOK(Easy2PayDialog.this.ptxId, Easy2PayDialog.this.userId, priceData[0], priceData[2]);
                }
                Easy2PayDialog.this.dismiss();
            }
        });
        layout_button.addView(bn_ok);
        Button bn_cancel = new Button(getContext());
        LinearLayout.LayoutParams params_cancel = new LinearLayout.LayoutParams(-2, -2);
        params_cancel.weight = 0.3f;
        bn_cancel.setLayoutParams(params_cancel);
        bn_cancel.setText(Resource.TXT_BUTTON_CANCEL[this.langId]);
        bn_cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Easy2PayDialog.this.listener != null) {
                    Easy2PayDialog.this.listener.onCancel(Easy2PayDialog.this.ptxId, Easy2PayDialog.this.userId);
                }
                Easy2PayDialog.this.dismiss();
            }
        });
        layout_button.addView(bn_cancel);
    }
}
