package com.android.easy2pay;

class Easy2PayConnection {
    protected static final int ERROR_HTTP_CONNECTION_FAIL = 302;
    protected static final int ERROR_HTTP_RESPONSE_CODE_NOT_HTTP_OK = 301;
    private Easy2PayConnectionListener listener = null;

    protected Easy2PayConnection(Easy2PayConnectionListener listener2) {
        this.listener = listener2;
    }

    /* access modifiers changed from: protected */
    public void get(final String sessionId, final String[] refValue, final String url) {
        new Thread(new Runnable() {
            public void run() {
                Easy2PayConnection.this.doGet(sessionId, refValue, url);
            }
        }).start();
    }

    /* JADX WARN: Type inference failed for: r9v5, types: [java.net.URLConnection] */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void doGet(java.lang.String r14, java.lang.String[] r15, java.lang.String r16) {
        /*
            r13 = this;
            r2 = 0
            r8 = 0
            java.net.URL r9 = new java.net.URL     // Catch:{ Exception -> 0x004c }
            r0 = r16
            r9.<init>(r0)     // Catch:{ Exception -> 0x004c }
            java.net.URLConnection r9 = r9.openConnection()     // Catch:{ Exception -> 0x004c }
            r0 = r9
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x004c }
            r2 = r0
            java.lang.String r9 = "GET"
            r2.setRequestMethod(r9)     // Catch:{ Exception -> 0x004c }
            r9 = 60000(0xea60, float:8.4078E-41)
            r2.setConnectTimeout(r9)     // Catch:{ Exception -> 0x004c }
            r9 = 60000(0xea60, float:8.4078E-41)
            r2.setReadTimeout(r9)     // Catch:{ Exception -> 0x004c }
            r9 = 0
            r2.setAllowUserInteraction(r9)     // Catch:{ Exception -> 0x004c }
            r9 = 1
            r2.setInstanceFollowRedirects(r9)     // Catch:{ Exception -> 0x004c }
            r9 = 0
            r2.setUseCaches(r9)     // Catch:{ Exception -> 0x004c }
            int r6 = r2.getResponseCode()     // Catch:{ Exception -> 0x004c }
            r9 = 200(0xc8, float:2.8E-43)
            if (r6 != r9) goto L_0x00b9
            r7 = 0
            java.io.InputStream r4 = r2.getInputStream()     // Catch:{ Exception -> 0x004c }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x004c }
            r1.<init>()     // Catch:{ Exception -> 0x004c }
            r5 = 0
        L_0x0041:
            int r5 = r4.read()     // Catch:{ Exception -> 0x004c }
            r9 = -1
            if (r5 == r9) goto L_0x0062
            r1.write(r5)     // Catch:{ Exception -> 0x004c }
            goto L_0x0041
        L_0x004c:
            r3 = move-exception
            com.android.easy2pay.Easy2PayConnectionListener r9 = r13.listener     // Catch:{ all -> 0x00f4 }
            if (r9 == 0) goto L_0x005c
            com.android.easy2pay.Easy2PayConnectionListener r9 = r13.listener     // Catch:{ all -> 0x00f4 }
            r10 = 302(0x12e, float:4.23E-43)
            java.lang.String r11 = r3.getMessage()     // Catch:{ all -> 0x00f4 }
            r9.onError(r14, r15, r10, r11)     // Catch:{ all -> 0x00f4 }
        L_0x005c:
            if (r2 == 0) goto L_0x0061
            r2.disconnect()     // Catch:{ Exception -> 0x00fb }
        L_0x0061:
            return
        L_0x0062:
            byte[] r7 = r1.toByteArray()     // Catch:{ Exception -> 0x004c }
            java.lang.String r8 = r1.toString()     // Catch:{ Exception -> 0x004c }
            r1.close()     // Catch:{ Exception -> 0x004c }
            com.android.easy2pay.Easy2PayConnectionListener r9 = r13.listener     // Catch:{ Exception -> 0x004c }
            if (r9 == 0) goto L_0x0076
            com.android.easy2pay.Easy2PayConnectionListener r9 = r13.listener     // Catch:{ Exception -> 0x004c }
            r9.onReceive(r14, r15, r7)     // Catch:{ Exception -> 0x004c }
        L_0x0076:
            java.lang.String r9 = "E2P Connection"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004c }
            r10.<init>()     // Catch:{ Exception -> 0x004c }
            java.lang.String r11 = "GET "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x004c }
            r0 = r16
            java.lang.StringBuilder r10 = r10.append(r0)     // Catch:{ Exception -> 0x004c }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x004c }
            android.util.Log.w(r9, r10)     // Catch:{ Exception -> 0x004c }
            java.lang.String r9 = "E2P Connection"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004c }
            r10.<init>()     // Catch:{ Exception -> 0x004c }
            java.lang.String r11 = "RESPONSE "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x004c }
            java.lang.StringBuilder r10 = r10.append(r8)     // Catch:{ Exception -> 0x004c }
            java.lang.String r11 = "\n\n"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x004c }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x004c }
            android.util.Log.w(r9, r10)     // Catch:{ Exception -> 0x004c }
            r2.disconnect()     // Catch:{ Exception -> 0x004c }
            if (r2 == 0) goto L_0x0061
            r2.disconnect()     // Catch:{ Exception -> 0x00b7 }
            goto L_0x0061
        L_0x00b7:
            r9 = move-exception
            goto L_0x0061
        L_0x00b9:
            com.android.easy2pay.Easy2PayConnectionListener r9 = r13.listener     // Catch:{ Exception -> 0x004c }
            if (r9 == 0) goto L_0x0076
            com.android.easy2pay.Easy2PayConnectionListener r9 = r13.listener     // Catch:{ Exception -> 0x004c }
            r10 = 301(0x12d, float:4.22E-43)
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004c }
            r11.<init>()     // Catch:{ Exception -> 0x004c }
            java.lang.String r12 = "HTTP RESPONSE CODE : "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x004c }
            int r12 = r2.getResponseCode()     // Catch:{ Exception -> 0x004c }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x004c }
            java.lang.String r12 = "\n"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x004c }
            java.lang.String r12 = "description : The requested resource ("
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x004c }
            r0 = r16
            java.lang.StringBuilder r11 = r11.append(r0)     // Catch:{ Exception -> 0x004c }
            java.lang.String r12 = ") is not available."
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x004c }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x004c }
            r9.onError(r14, r15, r10, r11)     // Catch:{ Exception -> 0x004c }
            goto L_0x0076
        L_0x00f4:
            r9 = move-exception
            if (r2 == 0) goto L_0x00fa
            r2.disconnect()     // Catch:{ Exception -> 0x00fe }
        L_0x00fa:
            throw r9
        L_0x00fb:
            r9 = move-exception
            goto L_0x0061
        L_0x00fe:
            r10 = move-exception
            goto L_0x00fa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.easy2pay.Easy2PayConnection.doGet(java.lang.String, java.lang.String[], java.lang.String):void");
    }
}
