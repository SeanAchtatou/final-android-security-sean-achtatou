package com.android.easy2pay;

class XMLParseException extends RuntimeException {
    public static final int NO_LINE = -1;
    private int lineNr;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public XMLParseException(String name, String message) {
        super("XML Parse Exception during parsing of " + (name == null ? "the XML definition" : "a " + name + " element") + ": " + message);
        this.lineNr = -1;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public XMLParseException(String name, int lineNr2, String message) {
        super("XML Parse Exception during parsing of " + (name == null ? "the XML definition" : "a " + name + " element") + " at line " + lineNr2 + ": " + message);
        this.lineNr = lineNr2;
    }

    public int getLineNr() {
        return this.lineNr;
    }
}
