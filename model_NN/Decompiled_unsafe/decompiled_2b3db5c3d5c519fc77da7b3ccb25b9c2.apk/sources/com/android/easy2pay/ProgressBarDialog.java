package com.android.easy2pay;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Display;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

class ProgressBarDialog extends Dialog {
    private static final float DIMENSIONS_DIFF_LANDSCAPE = 140.0f;
    private static final float DIMENSIONS_DIFF_PORTRAIT = 80.0f;
    private Context context;
    private Easy2Pay e2p;
    private int langId;
    private final ProgressBar pg_wait;
    private int progressMax;
    private String ptxId = "";
    private String txId = "";
    private String userId = "";

    protected ProgressBarDialog(Context context2, int max, int langId2) {
        super(context2);
        this.context = context2;
        this.progressMax = max;
        this.pg_wait = new ProgressBar(context2, null, 16842872);
        this.pg_wait.setMax(max);
        this.langId = langId2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(false);
        requestWindowFeature(1);
        Display display = getWindow().getWindowManager().getDefaultDisplay();
        float scale = getContext().getResources().getDisplayMetrics().density;
        float dimensions = getContext().getResources().getConfiguration().orientation == 2 ? DIMENSIONS_DIFF_LANDSCAPE : DIMENSIONS_DIFF_PORTRAIT;
        LinearLayout layout_main = new LinearLayout(getContext());
        layout_main.setOrientation(1);
        addContentView(layout_main, new LinearLayout.LayoutParams(display.getWidth() - ((int) ((dimensions * scale) + 0.5f)), -2));
        LinearLayout.LayoutParams params_title = new LinearLayout.LayoutParams(-1, -2);
        params_title.setMargins((int) (10.0f * scale), (int) (10.0f * scale), (int) (10.0f * scale), (int) (10.0f * scale));
        params_title.gravity = 16;
        LinearLayout layout_title = new LinearLayout(getContext());
        layout_title.setOrientation(0);
        layout_main.addView(layout_title, params_title);
        ProgressBar pg_indicate = new ProgressBar(getContext(), null, 16842873);
        pg_indicate.setPadding((int) (10.0f * scale), 0, (int) (10.0f * scale), 0);
        layout_title.addView(pg_indicate);
        TextView tv_title = new TextView(getContext());
        tv_title.setText(Resource.STRING_PROGRESS_TITLE[this.langId]);
        tv_title.setTextColor(-3355444);
        tv_title.setPadding((int) (5.0f * scale), 0, (int) (5.0f * scale), 0);
        layout_title.addView(tv_title);
        LinearLayout.LayoutParams params_progress = new LinearLayout.LayoutParams(-1, -2);
        params_progress.setMargins((int) (10.0f * scale), 0, (int) (10.0f * scale), (int) (10.0f * scale));
        layout_main.addView(this.pg_wait, params_progress);
    }

    /* access modifiers changed from: protected */
    public void setPtxId(String ptxId2) {
        this.ptxId = ptxId2;
    }

    /* access modifiers changed from: protected */
    public String getPtxId() {
        return this.ptxId;
    }

    /* access modifiers changed from: protected */
    public void setUserId(String userId2) {
        this.userId = userId2;
    }

    /* access modifiers changed from: protected */
    public void setTxId(String txId2) {
        this.txId = txId2;
    }

    /* access modifiers changed from: protected */
    public String getTxId() {
        return this.txId;
    }

    /* access modifiers changed from: protected */
    public String getUserId() {
        return this.userId;
    }

    /* access modifiers changed from: protected */
    public void setWaitProgress(int progress) {
        this.pg_wait.setProgress(progress);
    }

    /* access modifiers changed from: protected */
    public void setEasy2Pay(Easy2Pay e2p2) {
        this.e2p = e2p2;
    }

    public void onBackPressed() {
        if (isShowing()) {
            setWaitProgress(this.progressMax);
            dismiss();
            if (this.e2p != null) {
                AlertDialog.Builder alert = new AlertDialog.Builder(this.context);
                alert.setCancelable(false);
                alert.setMessage(Resource.STRING_PG_DIALOG_BACK_PRESSED[this.langId]).setPositiveButton(Resource.TXT_BUTTON_OK[this.langId], (DialogInterface.OnClickListener) null).show();
                this.e2p.checkCharging(this.e2p.getPtxId(), this.e2p.getUserId(), this.e2p.getTxId());
            }
        }
    }
}
