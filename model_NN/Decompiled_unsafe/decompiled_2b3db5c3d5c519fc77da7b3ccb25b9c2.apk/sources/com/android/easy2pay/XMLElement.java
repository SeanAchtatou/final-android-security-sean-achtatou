package com.android.easy2pay;

import java.io.ByteArrayOutputStream;
import java.io.CharArrayReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

class XMLElement {
    public static final int NANOXML_MAJOR_VERSION = 2;
    public static final int NANOXML_MINOR_VERSION = 2;
    static final long serialVersionUID = 6685035139346394777L;
    private Hashtable attributes;
    private char charReadTooMuch;
    private Vector children;
    private String contents;
    private Hashtable entities;
    private boolean ignoreCase;
    private boolean ignoreWhitespace;
    private int lineNr;
    private String name;
    private int parserLineNr;
    private Reader reader;

    public XMLElement() {
        this(new Hashtable(), false, true, true);
    }

    public XMLElement(Hashtable entities2) {
        this(entities2, false, true, true);
    }

    public XMLElement(boolean skipLeadingWhitespace) {
        this(new Hashtable(), skipLeadingWhitespace, true, true);
    }

    public XMLElement(Hashtable entities2, boolean skipLeadingWhitespace) {
        this(entities2, skipLeadingWhitespace, true, true);
    }

    public XMLElement(Hashtable entities2, boolean skipLeadingWhitespace, boolean ignoreCase2) {
        this(entities2, skipLeadingWhitespace, true, ignoreCase2);
    }

    /* JADX INFO: Multiple debug info for r2v2 char[]: [D('value' java.lang.Object), D('value' char[])] */
    protected XMLElement(Hashtable entities2, boolean skipLeadingWhitespace, boolean fillBasicConversionTable, boolean ignoreCase2) {
        this.ignoreWhitespace = skipLeadingWhitespace;
        this.ignoreCase = ignoreCase2;
        this.name = null;
        this.contents = "";
        this.attributes = new Hashtable();
        this.children = new Vector();
        this.entities = entities2;
        this.lineNr = 0;
        Enumeration enums = this.entities.keys();
        while (enums.hasMoreElements()) {
            Object key = enums.nextElement();
            Object value = this.entities.get(key);
            if (value instanceof String) {
                this.entities.put(key, ((String) value).toCharArray());
            }
        }
        if (fillBasicConversionTable) {
            this.entities.put("amp", new char[]{'&'});
            this.entities.put("quot", new char[]{'\"'});
            this.entities.put("apos", new char[]{'\''});
            this.entities.put("lt", new char[]{'<'});
            this.entities.put("gt", new char[]{'>'});
        }
    }

    public void addChild(XMLElement child) {
        this.children.addElement(child);
    }

    public void setAttribute(String name2, Object value) {
        if (this.ignoreCase) {
            name2 = name2.toUpperCase();
        }
        this.attributes.put(name2, value.toString());
    }

    public void addProperty(String name2, Object value) {
        setAttribute(name2, value);
    }

    public void setIntAttribute(String name2, int value) {
        if (this.ignoreCase) {
            name2 = name2.toUpperCase();
        }
        this.attributes.put(name2, Integer.toString(value));
    }

    public void addProperty(String key, int value) {
        setIntAttribute(key, value);
    }

    public void setDoubleAttribute(String name2, double value) {
        if (this.ignoreCase) {
            name2 = name2.toUpperCase();
        }
        this.attributes.put(name2, Double.toString(value));
    }

    public void addProperty(String name2, double value) {
        setDoubleAttribute(name2, value);
    }

    public int countChildren() {
        return this.children.size();
    }

    public Enumeration enumerateAttributeNames() {
        return this.attributes.keys();
    }

    public Enumeration enumeratePropertyNames() {
        return enumerateAttributeNames();
    }

    public Enumeration enumerateChildren() {
        return this.children.elements();
    }

    public Vector getChildren() {
        try {
            return (Vector) this.children.clone();
        } catch (Exception e) {
            return null;
        }
    }

    public String getContents() {
        return getContent();
    }

    public String getContent() {
        return this.contents;
    }

    public int getLineNr() {
        return this.lineNr;
    }

    public Object getAttribute(String name2) {
        return getAttribute(name2, null);
    }

    public Object getAttribute(String name2, Object defaultValue) {
        if (this.ignoreCase) {
            name2 = name2.toUpperCase();
        }
        Object value = this.attributes.get(name2);
        if (value == null) {
            return defaultValue;
        }
        return value;
    }

    /* JADX INFO: Multiple debug info for r0v1 'key'  java.lang.Object: [D('result' java.lang.Object), D('key' java.lang.Object)] */
    public Object getAttribute(String name2, Hashtable valueSet, String defaultKey, boolean allowLiterals) {
        if (this.ignoreCase) {
            name2 = name2.toUpperCase();
        }
        Object key = this.attributes.get(name2);
        if (key == null) {
            key = defaultKey;
        }
        Object result = valueSet.get(key);
        if (result != null) {
            return result;
        }
        if (allowLiterals) {
            return key;
        }
        throw invalidValue(name2, (String) key);
    }

    public String getStringAttribute(String name2) {
        return getStringAttribute(name2, null);
    }

    public String getStringAttribute(String name2, String defaultValue) {
        return (String) getAttribute(name2, defaultValue);
    }

    public String getStringAttribute(String name2, Hashtable valueSet, String defaultKey, boolean allowLiterals) {
        return (String) getAttribute(name2, valueSet, defaultKey, allowLiterals);
    }

    public int getIntAttribute(String name2) {
        return getIntAttribute(name2, 0);
    }

    public int getIntAttribute(String name2, int defaultValue) {
        if (this.ignoreCase) {
            name2 = name2.toUpperCase();
        }
        String value = (String) this.attributes.get(name2);
        if (value == null) {
            return defaultValue;
        }
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw invalidValue(name2, value);
        }
    }

    public int getIntAttribute(String name2, Hashtable valueSet, String defaultKey, boolean allowLiteralNumbers) {
        if (this.ignoreCase) {
            name2 = name2.toUpperCase();
        }
        Object key = this.attributes.get(name2);
        if (key == null) {
            key = defaultKey;
        }
        try {
            Integer result = (Integer) valueSet.get(key);
            if (result == null) {
                if (!allowLiteralNumbers) {
                    throw invalidValue(name2, (String) key);
                }
                try {
                    result = Integer.valueOf((String) key);
                } catch (NumberFormatException e) {
                    throw invalidValue(name2, (String) key);
                }
            }
            return result.intValue();
        } catch (ClassCastException e2) {
            throw invalidValueSet(name2);
        }
    }

    public double getDoubleAttribute(String name2) {
        return getDoubleAttribute(name2, 0.0d);
    }

    public double getDoubleAttribute(String name2, double defaultValue) {
        if (this.ignoreCase) {
            name2 = name2.toUpperCase();
        }
        String value = (String) this.attributes.get(name2);
        if (value == null) {
            return defaultValue;
        }
        try {
            return Double.valueOf(value).doubleValue();
        } catch (NumberFormatException e) {
            throw invalidValue(name2, value);
        }
    }

    public double getDoubleAttribute(String name2, Hashtable valueSet, String defaultKey, boolean allowLiteralNumbers) {
        if (this.ignoreCase) {
            name2 = name2.toUpperCase();
        }
        Object key = this.attributes.get(name2);
        if (key == null) {
            key = defaultKey;
        }
        try {
            Double result = (Double) valueSet.get(key);
            if (result == null) {
                if (!allowLiteralNumbers) {
                    throw invalidValue(name2, (String) key);
                }
                try {
                    result = Double.valueOf((String) key);
                } catch (NumberFormatException e) {
                    throw invalidValue(name2, (String) key);
                }
            }
            return result.doubleValue();
        } catch (ClassCastException e2) {
            throw invalidValueSet(name2);
        }
    }

    public boolean getBooleanAttribute(String name2, String trueValue, String falseValue, boolean defaultValue) {
        if (this.ignoreCase) {
            name2 = name2.toUpperCase();
        }
        Object value = this.attributes.get(name2);
        if (value == null) {
            return defaultValue;
        }
        if (value.equals(trueValue)) {
            return true;
        }
        if (value.equals(falseValue)) {
            return false;
        }
        throw invalidValue(name2, (String) value);
    }

    public int getIntProperty(String name2, Hashtable valueSet, String defaultKey) {
        return getIntAttribute(name2, valueSet, defaultKey, false);
    }

    public String getProperty(String name2) {
        return getStringAttribute(name2);
    }

    public String getProperty(String name2, String defaultValue) {
        return getStringAttribute(name2, defaultValue);
    }

    public int getProperty(String name2, int defaultValue) {
        return getIntAttribute(name2, defaultValue);
    }

    public double getProperty(String name2, double defaultValue) {
        return getDoubleAttribute(name2, defaultValue);
    }

    public boolean getProperty(String key, String trueValue, String falseValue, boolean defaultValue) {
        return getBooleanAttribute(key, trueValue, falseValue, defaultValue);
    }

    public Object getProperty(String name2, Hashtable valueSet, String defaultKey) {
        return getAttribute(name2, valueSet, defaultKey, false);
    }

    public String getStringProperty(String name2, Hashtable valueSet, String defaultKey) {
        return getStringAttribute(name2, valueSet, defaultKey, false);
    }

    public int getSpecialIntProperty(String name2, Hashtable valueSet, String defaultKey) {
        return getIntAttribute(name2, valueSet, defaultKey, true);
    }

    public double getSpecialDoubleProperty(String name2, Hashtable valueSet, String defaultKey) {
        return getDoubleAttribute(name2, valueSet, defaultKey, true);
    }

    public String getName() {
        return this.name;
    }

    public String getTagName() {
        return getName();
    }

    public void parseFromReader(Reader reader2) throws IOException, XMLParseException {
        parseFromReader(reader2, 1);
    }

    public void parseFromReader(Reader reader2, int startingLineNr) throws IOException, XMLParseException {
        this.charReadTooMuch = 0;
        this.reader = reader2;
        this.parserLineNr = startingLineNr;
        while (scanWhitespace() == '<') {
            char ch = readChar();
            if (ch == '!' || ch == '?') {
                skipSpecialTag(0);
            } else {
                unreadChar(ch);
                scanElement(this);
                return;
            }
        }
        throw expectedInput("<");
    }

    public void parseString(String string) throws XMLParseException {
        try {
            parseFromReader(new StringReader(string), 1);
        } catch (IOException e) {
        }
    }

    public void parseString(String string, int offset) throws XMLParseException {
        parseString(string.substring(offset));
    }

    public void parseString(String string, int offset, int end) throws XMLParseException {
        parseString(string.substring(offset, end));
    }

    public void parseString(String string, int offset, int end, int startingLineNr) throws XMLParseException {
        try {
            parseFromReader(new StringReader(string.substring(offset, end)), startingLineNr);
        } catch (IOException e) {
        }
    }

    public void parseCharArray(char[] input, int offset, int end) throws XMLParseException {
        parseCharArray(input, offset, end, 1);
    }

    public void parseCharArray(char[] input, int offset, int end, int startingLineNr) throws XMLParseException {
        try {
            parseFromReader(new CharArrayReader(input, offset, end), startingLineNr);
        } catch (IOException e) {
        }
    }

    public void removeChild(XMLElement child) {
        this.children.removeElement(child);
    }

    public void removeAttribute(String name2) {
        if (this.ignoreCase) {
            name2 = name2.toUpperCase();
        }
        this.attributes.remove(name2);
    }

    public void removeProperty(String name2) {
        removeAttribute(name2);
    }

    public void removeChild(String name2) {
        removeAttribute(name2);
    }

    /* access modifiers changed from: protected */
    public XMLElement createAnotherElement() {
        return new XMLElement(this.entities, this.ignoreWhitespace, false, this.ignoreCase);
    }

    public void setContent(String content) {
        this.contents = content;
    }

    public void setTagName(String name2) {
        setName(name2);
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String toString() {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            OutputStreamWriter writer = new OutputStreamWriter(out);
            write(writer);
            writer.flush();
            return new String(out.toByteArray());
        } catch (IOException e) {
            return super.toString();
        }
    }

    public void write(Writer writer) throws IOException {
        if (this.name == null) {
            writeEncoded(writer, this.contents);
            return;
        }
        writer.write(60);
        writer.write(this.name);
        if (!this.attributes.isEmpty()) {
            Enumeration enums = this.attributes.keys();
            while (enums.hasMoreElements()) {
                writer.write(32);
                String key = (String) enums.nextElement();
                writer.write(key);
                writer.write(61);
                writer.write(34);
                writeEncoded(writer, (String) this.attributes.get(key));
                writer.write(34);
            }
        }
        if (this.contents != null && this.contents.length() > 0) {
            writer.write(62);
            writeEncoded(writer, this.contents);
            writer.write(60);
            writer.write(47);
            writer.write(this.name);
            writer.write(62);
        } else if (this.children.isEmpty()) {
            writer.write(47);
            writer.write(62);
        } else {
            writer.write(62);
            Enumeration enums2 = enumerateChildren();
            while (enums2.hasMoreElements()) {
                ((XMLElement) enums2.nextElement()).write(writer);
            }
            writer.write(60);
            writer.write(47);
            writer.write(this.name);
            writer.write(62);
        }
    }

    /* access modifiers changed from: protected */
    public void writeEncoded(Writer writer, String str) throws IOException {
        for (int i = 0; i < str.length(); i++) {
            int ch = str.charAt(i);
            switch (ch) {
                case 34:
                    writer.write(38);
                    writer.write(113);
                    writer.write(117);
                    writer.write(111);
                    writer.write(116);
                    writer.write(59);
                    break;
                case 38:
                    writer.write(38);
                    writer.write(97);
                    writer.write(109);
                    writer.write(112);
                    writer.write(59);
                    break;
                case 39:
                    writer.write(38);
                    writer.write(97);
                    writer.write(112);
                    writer.write(111);
                    writer.write(115);
                    writer.write(59);
                    break;
                case 60:
                    writer.write(38);
                    writer.write(108);
                    writer.write(116);
                    writer.write(59);
                    break;
                case 62:
                    writer.write(38);
                    writer.write(103);
                    writer.write(116);
                    writer.write(59);
                    break;
                default:
                    int unicode = ch;
                    if (unicode >= 32 && unicode <= 126) {
                        writer.write(ch);
                        break;
                    } else {
                        writer.write(38);
                        writer.write(35);
                        writer.write(120);
                        writer.write(Integer.toString(unicode, 16));
                        writer.write(59);
                        break;
                    }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void scanIdentifier(StringBuffer result) throws IOException {
        while (true) {
            char ch = readChar();
            if ((ch < 'A' || ch > 'Z') && ((ch < 'a' || ch > 'z') && !((ch >= '0' && ch <= '9') || ch == '_' || ch == '.' || ch == ':' || ch == '-' || ch > '~'))) {
                unreadChar(ch);
                return;
            }
            result.append(ch);
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0007 A[SYNTHETIC] */
    protected char scanWhitespace() throws java.io.IOException {
        /*
            r1 = this;
        L_0x0000:
            char r0 = r1.readChar()
            switch(r0) {
                case 9: goto L_0x0000;
                case 10: goto L_0x0000;
                case 13: goto L_0x0000;
                case 32: goto L_0x0000;
                default: goto L_0x0007;
            }
        L_0x0007:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.easy2pay.XMLElement.scanWhitespace():char");
    }

    /* access modifiers changed from: protected */
    public char scanWhitespace(StringBuffer result) throws IOException {
        while (true) {
            char ch = readChar();
            switch (ch) {
                case 9:
                case 10:
                case ' ':
                    result.append(ch);
                    break;
                case 13:
                    break;
                default:
                    return ch;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: CFG modification limit reached, blocks count: 116 */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0021, code lost:
        r4.append(r0);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void scanString(java.lang.StringBuffer r4) throws java.io.IOException {
        /*
            r3 = this;
            char r1 = r3.readChar()
            r2 = 39
            if (r1 == r2) goto L_0x001a
            r2 = 34
            if (r1 == r2) goto L_0x001a
            java.lang.String r2 = "' or \""
            com.android.easy2pay.XMLParseException r2 = r3.expectedInput(r2)
            throw r2
        L_0x0013:
            r2 = 38
            if (r0 != r2) goto L_0x0021
            r3.resolveEntity(r4)
        L_0x001a:
            char r0 = r3.readChar()
            if (r0 != r1) goto L_0x0013
            return
        L_0x0021:
            r4.append(r0)
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.easy2pay.XMLElement.scanString(java.lang.StringBuffer):void");
    }

    /* access modifiers changed from: protected */
    public void scanPCData(StringBuffer data) throws IOException {
        while (true) {
            char ch = readChar();
            if (ch == '<') {
                char ch2 = readChar();
                if (ch2 == '!') {
                    checkCDATA(data);
                } else {
                    unreadChar(ch2);
                    return;
                }
            } else if (ch == '&') {
                resolveEntity(data);
            } else {
                data.append(ch);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkCDATA(StringBuffer buf) throws IOException {
        char ch = readChar();
        if (ch != '[') {
            unreadChar(ch);
            skipSpecialTag(0);
            return false;
        } else if (!checkLiteral("CDATA[")) {
            skipSpecialTag(1);
            return false;
        } else {
            int delimiterCharsSkipped = 0;
            while (delimiterCharsSkipped < 3) {
                char ch2 = readChar();
                switch (ch2) {
                    case '>':
                        if (delimiterCharsSkipped >= 2) {
                            delimiterCharsSkipped = 3;
                            break;
                        } else {
                            for (int i = 0; i < delimiterCharsSkipped; i++) {
                                buf.append(']');
                            }
                            delimiterCharsSkipped = 0;
                            buf.append('>');
                            break;
                        }
                    case ']':
                        if (delimiterCharsSkipped >= 2) {
                            buf.append(']');
                            buf.append(']');
                            delimiterCharsSkipped = 0;
                            break;
                        } else {
                            delimiterCharsSkipped++;
                            break;
                        }
                    default:
                        for (int i2 = 0; i2 < delimiterCharsSkipped; i2++) {
                            buf.append(']');
                        }
                        buf.append(ch2);
                        delimiterCharsSkipped = 0;
                        break;
                }
            }
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void skipComment() throws IOException {
        int dashesToRead = 2;
        while (dashesToRead > 0) {
            if (readChar() == '-') {
                dashesToRead--;
            } else {
                dashesToRead = 2;
            }
        }
        if (readChar() != '>') {
            throw expectedInput(">");
        }
    }

    /* access modifiers changed from: protected */
    public void skipSpecialTag(int bracketLevel) throws IOException {
        int tagLevel = 1;
        char stringDelimiter = 0;
        if (bracketLevel == 0) {
            char ch = readChar();
            if (ch == '[') {
                bracketLevel++;
            } else if (ch == '-') {
                char ch2 = readChar();
                if (ch2 == '[') {
                    bracketLevel++;
                } else if (ch2 == ']') {
                    bracketLevel--;
                } else if (ch2 == '-') {
                    skipComment();
                    return;
                }
            }
        }
        while (tagLevel > 0) {
            char ch3 = readChar();
            if (stringDelimiter == 0) {
                if (ch3 == '\"' || ch3 == '\'') {
                    stringDelimiter = ch3;
                } else if (bracketLevel <= 0) {
                    if (ch3 == '<') {
                        tagLevel++;
                    } else if (ch3 == '>') {
                        tagLevel--;
                    }
                }
                if (ch3 == '[') {
                    bracketLevel++;
                } else if (ch3 == ']') {
                    bracketLevel--;
                }
            } else if (ch3 == stringDelimiter) {
                stringDelimiter = 0;
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLiteral(String literal) throws IOException {
        int length = literal.length();
        for (int i = 0; i < length; i++) {
            if (readChar() != literal.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public char readChar() throws IOException {
        if (this.charReadTooMuch != 0) {
            char ch = this.charReadTooMuch;
            this.charReadTooMuch = 0;
            return ch;
        }
        int i = this.reader.read();
        if (i < 0) {
            throw unexpectedEndOfData();
        } else if (i != 10) {
            return (char) i;
        } else {
            this.parserLineNr++;
            return 10;
        }
    }

    /* access modifiers changed from: protected */
    public void scanElement(XMLElement elt) throws IOException {
        StringBuffer buf = new StringBuffer();
        scanIdentifier(buf);
        String name2 = buf.toString();
        elt.setName(name2);
        char ch = scanWhitespace();
        while (ch != '>' && ch != '/') {
            buf.setLength(0);
            unreadChar(ch);
            scanIdentifier(buf);
            String key = buf.toString();
            if (scanWhitespace() != '=') {
                throw expectedInput("=");
            }
            unreadChar(scanWhitespace());
            buf.setLength(0);
            scanString(buf);
            elt.setAttribute(key, buf);
            ch = scanWhitespace();
        }
        if (ch != '/') {
            buf.setLength(0);
            char ch2 = scanWhitespace(buf);
            if (ch2 == '<') {
                while (true) {
                    ch2 = readChar();
                    if (ch2 == '!') {
                        if (!checkCDATA(buf)) {
                            ch2 = scanWhitespace(buf);
                            if (ch2 != '<') {
                                unreadChar(ch2);
                                scanPCData(buf);
                                break;
                            }
                        } else {
                            scanPCData(buf);
                            break;
                        }
                    } else {
                        buf.setLength(0);
                        break;
                    }
                }
            } else {
                unreadChar(ch2);
                scanPCData(buf);
            }
            if (buf.length() == 0) {
                while (ch2 != '/') {
                    if (ch2 != '!') {
                        unreadChar(ch2);
                        XMLElement child = createAnotherElement();
                        scanElement(child);
                        elt.addChild(child);
                    } else if (readChar() != '-') {
                        throw expectedInput("Comment or Element");
                    } else if (readChar() != '-') {
                        throw expectedInput("Comment or Element");
                    } else {
                        skipComment();
                    }
                    if (scanWhitespace() != '<') {
                        throw expectedInput("<");
                    }
                    ch2 = readChar();
                }
                unreadChar(ch2);
            } else if (this.ignoreWhitespace) {
                elt.setContent(buf.toString().trim());
            } else {
                elt.setContent(buf.toString());
            }
            if (readChar() != '/') {
                throw expectedInput("/");
            }
            unreadChar(scanWhitespace());
            if (!checkLiteral(name2)) {
                throw expectedInput(name2);
            } else if (scanWhitespace() != '>') {
                throw expectedInput(">");
            }
        } else if (readChar() != '>') {
            throw expectedInput(">");
        }
    }

    /* access modifiers changed from: protected */
    public void resolveEntity(StringBuffer buf) throws IOException {
        char ch;
        StringBuffer keyBuf = new StringBuffer();
        while (true) {
            char ch2 = readChar();
            if (ch2 == ';') {
                break;
            }
            keyBuf.append(ch2);
        }
        String key = keyBuf.toString();
        if (key.charAt(0) == '#') {
            try {
                if (key.charAt(1) == 'x') {
                    ch = (char) Integer.parseInt(key.substring(2), 16);
                } else {
                    ch = (char) Integer.parseInt(key.substring(1), 10);
                }
                buf.append(ch);
            } catch (NumberFormatException e) {
                throw unknownEntity(key);
            }
        } else {
            char[] value = (char[]) this.entities.get(key);
            if (value == null) {
                throw unknownEntity(key);
            }
            buf.append(value);
        }
    }

    /* access modifiers changed from: protected */
    public void unreadChar(char ch) {
        this.charReadTooMuch = ch;
    }

    /* access modifiers changed from: protected */
    public XMLParseException invalidValueSet(String name2) {
        return new XMLParseException(getName(), this.parserLineNr, "Invalid value set (entity name = \"" + name2 + "\")");
    }

    /* access modifiers changed from: protected */
    public XMLParseException invalidValue(String name2, String value) {
        return new XMLParseException(getName(), this.parserLineNr, "Attribute \"" + name2 + "\" does not contain a valid " + "value (\"" + value + "\")");
    }

    /* access modifiers changed from: protected */
    public XMLParseException unexpectedEndOfData() {
        return new XMLParseException(getName(), this.parserLineNr, "Unexpected end of data reached");
    }

    /* access modifiers changed from: protected */
    public XMLParseException syntaxError(String context) {
        return new XMLParseException(getName(), this.parserLineNr, "Syntax error while parsing " + context);
    }

    /* access modifiers changed from: protected */
    public XMLParseException expectedInput(String charSet) {
        return new XMLParseException(getName(), this.parserLineNr, "Expected: " + charSet);
    }

    /* access modifiers changed from: protected */
    public XMLParseException unknownEntity(String name2) {
        return new XMLParseException(getName(), this.parserLineNr, "Unknown or invalid entity: &" + name2 + ";");
    }
}
