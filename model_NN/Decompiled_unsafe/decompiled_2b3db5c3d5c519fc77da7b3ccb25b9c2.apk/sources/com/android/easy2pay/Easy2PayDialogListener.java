package com.android.easy2pay;

interface Easy2PayDialogListener {
    void onActivityPause();

    void onActivityResume();

    void onCancel(String str, String str2);

    void onOK(String str, String str2, String str3, String str4);
}
