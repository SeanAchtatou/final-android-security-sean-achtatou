package com.android.easy2pay;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

class LoadingDialog extends Dialog {
    int langId;

    protected LoadingDialog(Context context, int langId2) {
        super(context);
        this.langId = langId2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(false);
        requestWindowFeature(1);
        float scale = getContext().getResources().getDisplayMetrics().density;
        LinearLayout.LayoutParams params_main = new LinearLayout.LayoutParams(-2, -2);
        params_main.gravity = 1;
        LinearLayout layout_main = new LinearLayout(getContext());
        layout_main.setOrientation(1);
        addContentView(layout_main, params_main);
        LinearLayout.LayoutParams params_progress = new LinearLayout.LayoutParams(-2, -2);
        params_progress.gravity = 1;
        LinearLayout layout_progress = new LinearLayout(getContext());
        layout_progress.setOrientation(1);
        layout_main.addView(layout_progress, params_progress);
        ProgressBar pg_indicate = new ProgressBar(getContext(), null, 16842871);
        pg_indicate.setPadding(0, (int) (10.0f * scale), 0, (int) (10.0f * scale));
        layout_progress.addView(pg_indicate);
        LinearLayout.LayoutParams params_title = new LinearLayout.LayoutParams(-2, -2);
        params_title.gravity = 1;
        LinearLayout layout_title = new LinearLayout(getContext());
        layout_title.setOrientation(1);
        layout_main.addView(layout_title, params_title);
        TextView tv_title = new TextView(getContext());
        tv_title.setText(Resource.STRING_WAITING_TITLE[this.langId]);
        tv_title.setTextColor(-3355444);
        tv_title.setPadding((int) (10.0f * scale), (int) (10.0f * scale), (int) (10.0f * scale), (int) (10.0f * scale));
        layout_title.addView(tv_title);
    }

    public void onBackPressed() {
    }
}
