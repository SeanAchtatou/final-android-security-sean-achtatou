package com.android.easy2pay;

import java.security.MessageDigest;

abstract class MD5Factory {
    MD5Factory() {
    }

    private static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 15;
            int two_halfs = 0;
            while (true) {
                if (halfbyte < 0 || halfbyte > 9) {
                    buf.append((char) ((halfbyte - 10) + 97));
                } else {
                    buf.append((char) (halfbyte + 48));
                }
                halfbyte = data[i] & 15;
                int two_halfs2 = two_halfs + 1;
                if (two_halfs >= 1) {
                    break;
                }
                two_halfs = two_halfs2;
            }
        }
        return buf.toString();
    }

    protected static String generate(String plain) {
        try {
            MessageDigest mDigest = MessageDigest.getInstance("MD5");
            mDigest.update(plain.getBytes());
            return convertToHex(mDigest.digest());
        } catch (Exception e) {
            return null;
        }
    }
}
