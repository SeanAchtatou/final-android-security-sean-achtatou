package com.android.easy2pay;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.bluepay.data.Config;
import com.facebook.AccessToken;
import com.mol.seaplus.sdk.psms.PSMSResponse;
import java.lang.reflect.Array;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;
import org.apache.http.protocol.HTTP;

public class Easy2Pay implements Easy2PayConnectionListener, Easy2PayDialogListener {
    private static final String ACTION_SMS_SENT = "algorism.ad.ohm.SMS_SENT_ACTION";
    private static final String CHECK_CHAREGD_SESSION = "CHK";
    private static final String CHECK_CHARGED_URL = "https://sea-sdk.molthailand.com/inquiry.php";
    public static final int ERROR_CANNOT_CHARGING = 306;
    public static final int ERROR_CANNOT_GET_PINCODE = 302;
    public static final int ERROR_CANNOT_GET_PRICE_LIST = 301;
    public static final int ERROR_CANNOT_SEND_SMS = 305;
    public static final int ERROR_PRICE_IS_INVALID = 304;
    public static final int ERROR_SIMCARD_NOTFOUND = 303;
    public static final int EVENT_EASY2PAY_IS_CHARGING = 202;
    public static final int EVENT_EASY2PAY_IS_CHARGING_IN_BACKGROUND = 203;
    public static final int EVENT_USER_CANCEL_CHARGE = 201;
    private static final String GET_PINCODE_SESSION = "PIN";
    private static final String GET_PINCODE_URL = "https://sea-sdk.molthailand.com/init.php";
    private static final String GET_PRICES_SESSION = "PRC";
    private static final String GET_PRICES_URL = "https://sea-sdk.molthailand.com/get-price-list2.php";
    private static final int PROGRESS_STEP = 10;
    /* access modifiers changed from: private */
    public final Context context;
    /* access modifiers changed from: private */
    public final Hashtable<String, String> descList;
    private Handler handler;
    private boolean hasIMSI;
    private boolean isBackgroudChecking;
    /* access modifiers changed from: private */
    public boolean isFullScreen;
    private String langCode;
    /* access modifiers changed from: private */
    public int langId;
    private final LoadingDialog ldDialog;
    /* access modifiers changed from: private */
    public Easy2PayListener listener;
    private String mcc;
    private String mnc;
    /* access modifiers changed from: private */
    public final ProgressBarDialog pgDialog;
    private final Vector<String[]> priceList;
    /* access modifiers changed from: private */
    public int progressIndex;
    private final int progressMax;
    private String ptxId;
    private final String secretKey;
    /* access modifiers changed from: private */
    public final String serviceId;
    /* access modifiers changed from: private */
    public final SMSBroadcastReceiver smsListener;
    private String txId;
    private String userId;
    private String[] userPriceIds;
    private final Hashtable<String, String> validPriceIdList;

    public enum Language {
        EN,
        TH,
        MS,
        ID,
        VI,
        MY
    }

    @Deprecated
    public Easy2Pay(Context context2, String serviceId2, String secretKey2) {
        this(context2, serviceId2, secretKey2, 120);
    }

    @Deprecated
    public Easy2Pay(Context context2, String serviceId2, String secretKey2, boolean isFullScreen2) {
        this(context2, serviceId2, secretKey2, 120, isFullScreen2, null, null, Language.TH);
    }

    @Deprecated
    public Easy2Pay(Context context2, String serviceId2, String secretKey2, int progressMax2) {
        this(context2, serviceId2, secretKey2, progressMax2, false, null, null, Language.TH);
    }

    public Easy2Pay(Context context2, String serviceId2, String secretKey2, int progressMax2, boolean isFullScreen2) {
        this(context2, serviceId2, secretKey2, progressMax2, isFullScreen2, null, null, Language.TH);
    }

    public Easy2Pay(Context context2, String serviceId2, String secretKey2, int progressMax2, String mcc2, String mnc2) {
        this(context2, serviceId2, secretKey2, progressMax2, false, mcc2, mnc2, Language.EN);
    }

    public Easy2Pay(Context context2, String serviceId2, String secretKey2, int progressMax2, boolean isFullScreen2, String mcc2, String mnc2) {
        this(context2, serviceId2, secretKey2, progressMax2, isFullScreen2, mcc2, mnc2, Language.EN);
    }

    public Easy2Pay(Context context2, String serviceId2, String secretKey2, int progressMax2, boolean isFullScreen2, Language lang) {
        this(context2, serviceId2, secretKey2, progressMax2, isFullScreen2, null, null, lang);
    }

    public Easy2Pay(Context context2, String serviceId2, String secretKey2, int progressMax2, boolean isFullScreen2, String mcc2, String mnc2, Language lang) {
        this.hasIMSI = false;
        this.userPriceIds = null;
        this.progressIndex = 0;
        this.isFullScreen = false;
        this.ptxId = null;
        this.userId = null;
        this.txId = null;
        this.isBackgroudChecking = false;
        this.context = context2;
        this.serviceId = serviceId2;
        this.secretKey = secretKey2;
        this.progressMax = progressMax2 + 10;
        this.isFullScreen = isFullScreen2;
        if (mcc2 == null || mnc2 == null) {
            try {
                String networkOperator = ((TelephonyManager) context2.getSystemService("phone")).getNetworkOperator();
                if (networkOperator != null) {
                    mcc2 = mcc2 == null ? networkOperator.substring(0, 3) : mcc2;
                    if (mnc2 == null) {
                        mnc2 = networkOperator.substring(3);
                    }
                } else {
                    mcc2 = "000";
                    mnc2 = "00";
                }
            } catch (Exception e) {
                mcc2 = "000";
                mnc2 = "00";
            }
        }
        this.mcc = mcc2;
        this.mnc = mnc2;
        setLangauge(lang);
        this.ldDialog = new LoadingDialog(context2, this.langId);
        this.pgDialog = new ProgressBarDialog(context2, this.progressMax, this.langId);
        this.handler = new Handler();
        this.priceList = new Vector<>();
        this.validPriceIdList = new Hashtable<>();
        this.descList = new Hashtable<>();
        this.descList.put("title", Resource.STRING_TITLE_VALUE[this.langId]);
        this.descList.put("description", Resource.STRING_DESC_VALUE[this.langId]);
        this.smsListener = new SMSBroadcastReceiver();
    }

    public String getVersion() {
        return "1.1.0.6";
    }

    public void setLangauge(Language lang) {
        switch (lang) {
            case EN:
                this.langId = 0;
                this.langCode = "EN";
                return;
            case TH:
                this.langId = 1;
                this.langCode = Config.COUNTRY_TH;
                return;
            case MS:
                this.langId = 2;
                this.langCode = "MS";
                return;
            case ID:
                this.langId = 3;
                this.langCode = Config.COUNTRY_ID;
                return;
            case VI:
                this.langId = 4;
                this.langCode = "VI";
                return;
            default:
                this.langId = 0;
                this.langCode = "EN";
                return;
        }
    }

    public void setEasy2PayListener(Easy2PayListener listener2) {
        this.listener = listener2;
        this.pgDialog.setEasy2Pay(this);
    }

    public void purchase(String ptxId2, String userId2, String priceId) {
        String api_txId;
        String sig;
        Log.w("Easy2Pay", "Initializing (ptxId=" + ptxId2 + ", userId=" + userId2 + ", priceId=" + priceId + ") ...");
        Log.w("Easy2Pay", "mcc = " + this.mcc + ", mnc=" + this.mnc);
        try {
            if (this.isBackgroudChecking) {
                showAlert(Resource.STRING_WAITING_FOR_BACKGROUND_CHARGING[this.langId]);
                return;
            }
            this.ptxId = ptxId2;
            this.userId = userId2;
            try {
                if (!this.hasIMSI) {
                    try {
                        if (((TelephonyManager) this.context.getSystemService("phone")).getSubscriberId() != null) {
                            this.hasIMSI = true;
                            this.userPriceIds = new String[]{priceId};
                            this.ldDialog.show();
                            api_txId = genP_TXID();
                            sig = MD5Factory.generate(stringSortAndCat(new String[]{"lang", "mcc", "mnc", "p_txid", "partner_id"}, new String[]{this.langCode, this.mcc, this.mnc, api_txId, this.serviceId}) + this.secretKey);
                            try {
                                new Easy2PayConnection(this).get(GET_PRICES_SESSION, new String[]{ptxId2, userId2}, "https://sea-sdk.molthailand.com/get-price-list2.php?p_txid=" + URLEncoder.encode(api_txId) + "&partner_id=" + URLEncoder.encode(this.serviceId) + "&mcc=" + URLEncoder.encode(this.mcc) + "&mnc=" + URLEncoder.encode(this.mnc) + "&lang=" + URLEncoder.encode(this.langCode) + "&sig=" + URLEncoder.encode(sig));
                            } catch (Exception e) {
                                Log.w("Easy2Pay", "Exception at method purchase() and get price list process (ptxId=" + ptxId2 + ", userId=" + userId2 + ", GET_PRICES_URL=" + GET_PRICES_URL + ", p_txid=" + api_txId + ", partner_id=" + this.serviceId + ", mcc=" + this.mcc + ", mnc=" + this.mnc + ", lang=" + this.langCode + ", sig=" + sig + ")");
                                Log.w("Easy2Pay", "class: " + e.getClass() + ", message: " + e.getMessage() + ", cause:" + e.getCause() + "\n" + "stack trace: " + e.getStackTrace().toString());
                            }
                        } else if (this.listener != null) {
                            this.listener.onError(null, null, null, 303, Resource.STRING_ERROR_SIMCARD_INACTIVE[this.langId]);
                        }
                    } catch (Exception e2) {
                        Log.w("Easy2Pay", "Exception at method purchase().");
                        Log.w("Easy2Pay", "class: " + e2.getClass() + ", message: " + e2.getMessage() + ", cause:" + e2.getCause() + "\n" + "stack trace: " + e2.getStackTrace().toString());
                    }
                } else {
                    if (this.mcc.equals("000") && this.mnc.equals("00")) {
                        if (this.listener != null) {
                            this.listener.onError(null, null, null, 303, Resource.STRING_ERROR_SIMCARD_INACTIVE[this.langId]);
                            return;
                        }
                        return;
                    }
                    this.userPriceIds = new String[]{priceId};
                    this.ldDialog.show();
                    api_txId = genP_TXID();
                    sig = MD5Factory.generate(stringSortAndCat(new String[]{"lang", "mcc", "mnc", "p_txid", "partner_id"}, new String[]{this.langCode, this.mcc, this.mnc, api_txId, this.serviceId}) + this.secretKey);
                    new Easy2PayConnection(this).get(GET_PRICES_SESSION, new String[]{ptxId2, userId2}, "https://sea-sdk.molthailand.com/get-price-list2.php?p_txid=" + URLEncoder.encode(api_txId) + "&partner_id=" + URLEncoder.encode(this.serviceId) + "&mcc=" + URLEncoder.encode(this.mcc) + "&mnc=" + URLEncoder.encode(this.mnc) + "&lang=" + URLEncoder.encode(this.langCode) + "&sig=" + URLEncoder.encode(sig));
                }
            } catch (Exception e3) {
                Log.w("Easy2Pay", "Exception at method purchase().");
                Log.w("Easy2Pay", "class: " + e3.getClass() + ", message: " + e3.getMessage() + ", cause:" + e3.getCause() + "\n" + "stack trace: " + e3.getStackTrace().toString());
            }
        } catch (Exception e4) {
            Log.w("Easy2Pay", "Exception at method purchase().");
            Log.w("Easy2Pay", "class: " + e4.getClass() + ", message: " + e4.getMessage() + ", cause:" + e4.getCause() + "\n" + "stack trace: " + e4.getStackTrace().toString());
        }
    }

    @Deprecated
    public void purchase(String ptxId2, String userId2) {
        purchase(ptxId2, userId2, (String[]) null);
    }

    @Deprecated
    public void purchase(String ptxId2, String userId2, String[] priceIds) {
        if (this.isBackgroudChecking) {
            showAlert(Resource.STRING_WAITING_FOR_BACKGROUND_CHARGING[this.langId]);
            return;
        }
        this.ptxId = ptxId2;
        this.userId = userId2;
        if (!this.hasIMSI) {
            try {
                if (((TelephonyManager) this.context.getSystemService("phone")).getSubscriberId() != null) {
                    this.hasIMSI = true;
                } else if (this.listener != null) {
                    this.listener.onError(null, null, null, 303, Resource.STRING_ERROR_SIMCARD_INACTIVE[this.langId]);
                    return;
                } else {
                    return;
                }
            } catch (Exception e) {
            }
        } else if (this.mcc.equals("000") && this.mnc.equals("00")) {
            if (this.listener != null) {
                this.listener.onError(null, null, null, 303, Resource.STRING_ERROR_SIMCARD_INACTIVE[this.langId]);
                return;
            }
            return;
        }
        this.userPriceIds = priceIds;
        this.ldDialog.show();
        String api_txId = genP_TXID();
        String sig = MD5Factory.generate(stringSortAndCat(new String[]{"lang", "mcc", "mnc", "p_txid", "partner_id"}, new String[]{this.langCode, this.mcc, this.mnc, api_txId, this.serviceId}) + this.secretKey);
        new Easy2PayConnection(this).get(GET_PRICES_SESSION, new String[]{ptxId2, userId2}, "https://sea-sdk.molthailand.com/get-price-list2.php?p_txid=" + URLEncoder.encode(api_txId) + "&partner_id=" + URLEncoder.encode(this.serviceId) + "&mcc=" + URLEncoder.encode(this.mcc) + "&mnc=" + URLEncoder.encode(this.mnc) + "&lang=" + URLEncoder.encode(this.langCode) + "&sig=" + URLEncoder.encode(sig));
    }

    public void checkCharging(String ptxId2, String userId2, String txId2) {
        checkCharged(ptxId2, userId2, txId2, false);
    }

    private String genP_TXID() {
        String date = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(new Date());
        return date + new SimpleDateFormat("HHmmssSSSSSS", Locale.getDefault()).format(new Date());
    }

    public String getPtxId() {
        return this.ptxId;
    }

    public String getUserId() {
        return this.userId;
    }

    public String getTxId() {
        return this.txId;
    }

    private String stringSortAndCat(String[] sortname, String[] value) {
        String strcat = "";
        String[][] datas = (String[][]) Array.newInstance(String.class, sortname.length, 2);
        for (int i = 0; i < sortname.length; i++) {
            datas[i][0] = sortname[i];
            datas[i][1] = value[i];
        }
        Arrays.sort(datas, new Comparator<String[]>() {
            public int compare(String[] entry1, String[] entry2) {
                return entry1[0].compareTo(entry2[0]);
            }
        });
        for (int i2 = 0; i2 < datas.length; i2++) {
            strcat = strcat + datas[i2][1];
        }
        return strcat;
    }

    public void close() {
        if (this.pgDialog.isShowing()) {
            this.pgDialog.dismiss();
        }
        if (this.ldDialog.isShowing()) {
            this.ldDialog.dismiss();
        }
    }

    public final void onReceive(String sessionId, String[] refValue, byte[] data) {
        String xmlstr;
        String str;
        String str2;
        String xmlstr2;
        String shortcodeTmp;
        String prefixTmp;
        String xmlstr3;
        if (sessionId.equals(GET_PRICES_SESSION)) {
            this.ldDialog.dismiss();
            try {
                xmlstr3 = new String(data, HTTP.UTF_8);
            } catch (Exception e) {
                xmlstr3 = new String(data);
            }
            try {
                XMLElement xml = new XMLElement();
                xml.parseString(xmlstr3);
                Iterator it = xml.getChildren().iterator();
                while (it.hasNext()) {
                    XMLElement elem = (XMLElement) it.next();
                    if (elem.getName().equals("title")) {
                        this.descList.put("title", elem.getContent());
                    } else if (elem.getName().equals("description")) {
                        this.descList.put("description", elem.getContent());
                    } else if (elem.getName().equals("prices")) {
                        this.priceList.clear();
                        this.validPriceIdList.clear();
                        Vector<XMLElement> prices = elem.getChildren();
                        for (int i = 0; i < prices.size(); i++) {
                            String[] priceData = new String[3];
                            Iterator it2 = ((XMLElement) prices.elementAt(i)).getChildren().iterator();
                            while (it2.hasNext()) {
                                XMLElement pdata = (XMLElement) it2.next();
                                if (pdata.getName().equals(PSMSResponse.PRICE_ID)) {
                                    priceData[0] = pdata.getContent();
                                } else if (pdata.getName().equals("priceDescription")) {
                                    priceData[1] = pdata.getContent();
                                } else if (pdata.getName().equals("shortcode")) {
                                    priceData[2] = pdata.getContent();
                                }
                            }
                            this.priceList.add(priceData);
                            this.validPriceIdList.put(priceData[0], "" + i);
                        }
                    }
                }
                if (this.userPriceIds == null && this.priceList.size() > 0) {
                    this.userPriceIds = new String[this.priceList.size()];
                    for (int i2 = 0; i2 < this.priceList.size(); i2++) {
                        this.userPriceIds[i2] = this.priceList.elementAt(i2)[0];
                    }
                }
                if (this.userPriceIds == null || this.userPriceIds.length <= 0) {
                    if (this.userPriceIds != null) {
                        Log.w("Easy2Pay", "userPriceIds.length is " + this.userPriceIds.length);
                    } else {
                        Log.w("Easy2Pay", "userPriceIds.length is null!");
                    }
                    if (this.listener != null) {
                        this.listener.onError(refValue[0], refValue[1], null, 304, Resource.STRING_ERROR_PRICE_IS_INVALID[this.langId]);
                        return;
                    }
                    return;
                }
                Vector<String[]> validPrices = new Vector<>();
                for (int i3 = 0; i3 < this.userPriceIds.length; i3++) {
                    String priceIndex = this.validPriceIdList.get(this.userPriceIds[i3]);
                    if (priceIndex != null) {
                        validPrices.addElement(this.priceList.elementAt(Integer.parseInt(priceIndex)));
                    }
                }
                if (validPrices.size() > 0) {
                    final Vector<String[]> vector = validPrices;
                    final String[] strArr = refValue;
                    this.handler.post(new Runnable() {
                        public void run() {
                            if (!Easy2Pay.this.isFullScreen) {
                                new Easy2PayDialog(Easy2Pay.this.context, (String) Easy2Pay.this.descList.get("title"), (String) Easy2Pay.this.descList.get("description"), vector, strArr[0], strArr[1], Easy2Pay.this, Easy2Pay.this.langId).show();
                                return;
                            }
                            Easy2PayScreen e2pScreen = new Easy2PayScreen();
                            e2pScreen.setEasy2PayDialogListener(Easy2Pay.this);
                            Intent intent = new Intent(Easy2Pay.this.context, e2pScreen.getClass());
                            intent.putExtra("title", (String) Easy2Pay.this.descList.get("title"));
                            intent.putExtra("desc", (String) Easy2Pay.this.descList.get("description"));
                            String[] pIds = new String[vector.size()];
                            String[] pDescs = new String[vector.size()];
                            String[] sCodes = new String[vector.size()];
                            for (int i = 0; i < vector.size(); i++) {
                                String[] price = (String[]) vector.elementAt(i);
                                pIds[i] = price[0];
                                pDescs[i] = price[1];
                                sCodes[i] = price[2];
                            }
                            intent.putExtra("pids", pIds);
                            intent.putExtra("pdescs", pDescs);
                            intent.putExtra("scodes", sCodes);
                            intent.putExtra("ptxid", strArr[0]);
                            intent.putExtra("userid", strArr[1]);
                            intent.putExtra("langId", "" + Easy2Pay.this.langId);
                            ((Activity) Easy2Pay.this.context).startActivity(intent);
                        }
                    });
                    return;
                }
                Log.w("Easy2Pay", "validPrices.size() is " + validPrices.size());
                if (this.listener != null) {
                    this.listener.onError(refValue[0], refValue[1], null, 304, Resource.STRING_ERROR_PRICE_IS_INVALID[this.langId]);
                }
            } catch (Exception e2) {
                if (this.listener != null) {
                    this.listener.onError(refValue[0], refValue[1], null, 301, Resource.STRING_ERROR_XML_IS_INVALID[this.langId]);
                }
            }
        } else if (sessionId.equals(GET_PINCODE_SESSION)) {
            this.ldDialog.dismiss();
            try {
                xmlstr2 = new String(data, HTTP.UTF_8);
            } catch (Exception e3) {
                xmlstr2 = new String(data);
            }
            try {
                XMLElement xml2 = new XMLElement();
                xml2.parseString(xmlstr2);
                String status = "";
                String statusDetail = "";
                String ptxId2 = "";
                String txId2 = "";
                String pin = "";
                Iterator it3 = xml2.getChildren().iterator();
                while (it3.hasNext()) {
                    XMLElement elem2 = (XMLElement) it3.next();
                    if (elem2.getName().equals("status")) {
                        status = elem2.getContent();
                    } else if (elem2.getName().equals("statusDetail")) {
                        statusDetail = elem2.getContent();
                    } else if (elem2.getName().equals(PSMSResponse.PARTNER_TRANSACTION_ID)) {
                        ptxId2 = elem2.getContent();
                    } else if (elem2.getName().equals(PSMSResponse.TRANSACTION_ID)) {
                        txId2 = elem2.getContent();
                    } else if (elem2.getName().equals("pin")) {
                        pin = elem2.getContent();
                    }
                }
                if (status.equals("200")) {
                    final String userId2 = refValue[1];
                    String smsNo = refValue[2];
                    final String fptxId = ptxId2;
                    final String ftxId = txId2;
                    final String fpin = pin;
                    if (smsNo.contains("|")) {
                        String[] pairs = smsNo.split("\\|");
                        shortcodeTmp = pairs[0];
                        prefixTmp = pairs[1] + " ";
                    } else {
                        shortcodeTmp = smsNo;
                        prefixTmp = "";
                    }
                    final String shortcode = shortcodeTmp;
                    final String prefix = prefixTmp;
                    this.handler.post(new Runnable() {
                        public void run() {
                            int unused = Easy2Pay.this.progressIndex = 0;
                            Easy2Pay.this.pgDialog.setPtxId(fptxId);
                            Easy2Pay.this.pgDialog.setUserId(userId2);
                            Easy2Pay.this.pgDialog.setTxId(ftxId);
                            Easy2Pay.this.pgDialog.show();
                            if (Easy2Pay.this.listener != null) {
                                Easy2Pay.this.listener.onEvent(fptxId, userId2, ftxId, 202, Resource.STRING_EVENT_EASY2PAY_IS_CHARGING[Easy2Pay.this.langId]);
                            }
                            Easy2Pay.this.context.registerReceiver(Easy2Pay.this.smsListener, new IntentFilter(Easy2Pay.ACTION_SMS_SENT));
                            SmsManager smsManager = SmsManager.getDefault();
                            Intent intent = new Intent(Easy2Pay.ACTION_SMS_SENT);
                            intent.putExtra("ptxId", fptxId);
                            intent.putExtra(PSMSResponse.TRANSACTION_ID, ftxId);
                            intent.putExtra(PSMSResponse.USER_ID, userId2);
                            PendingIntent pIntent = PendingIntent.getBroadcast(Easy2Pay.this.context, 0, intent, 134217728);
                            String message = prefix + Easy2Pay.this.serviceId + " " + fpin;
                            Log.w("Easy2Pay", "Sending SMS: shortcode=" + shortcode + ", message=" + message);
                            smsManager.sendTextMessage(shortcode, null, message, pIntent, null);
                        }
                    });
                } else if (this.listener != null) {
                    this.listener.onError(refValue[0], refValue[1], null, 302, statusDetail);
                }
            } catch (Exception e4) {
                if (this.listener != null) {
                    this.listener.onError(refValue[0], refValue[1], null, 302, Resource.STRING_ERROR_XML_IS_INVALID[this.langId]);
                }
            }
        } else if (sessionId.equals(CHECK_CHAREGD_SESSION)) {
            try {
                xmlstr = new String(data, HTTP.UTF_8);
            } catch (Exception e5) {
                xmlstr = new String(data);
            }
            try {
                XMLElement xml3 = new XMLElement();
                xml3.parseString(xmlstr);
                String status2 = "";
                String statusDetail2 = "";
                String ptxId3 = "";
                String userId3 = "";
                String priceId = "";
                String txId3 = "";
                Iterator it4 = xml3.getChildren().iterator();
                while (it4.hasNext()) {
                    XMLElement elem3 = (XMLElement) it4.next();
                    if (elem3.getName().equals("status")) {
                        status2 = elem3.getContent();
                    } else if (elem3.getName().equals("statusDetail")) {
                        statusDetail2 = elem3.getContent();
                    } else if (elem3.getName().equals(PSMSResponse.PARTNER_TRANSACTION_ID)) {
                        ptxId3 = elem3.getContent();
                    } else if (elem3.getName().equals(PSMSResponse.USER_ID)) {
                        userId3 = elem3.getContent();
                    } else if (elem3.getName().equals(PSMSResponse.PRICE_ID)) {
                        priceId = elem3.getContent();
                    } else if (elem3.getName().equals(PSMSResponse.TRANSACTION_ID)) {
                        txId3 = elem3.getContent();
                    }
                }
                this.ptxId = ptxId3;
                this.userId = userId3;
                this.txId = txId3;
                if (status2.equals("200")) {
                    if (this.pgDialog.isShowing()) {
                        this.pgDialog.setWaitProgress(this.progressMax);
                        this.pgDialog.dismiss();
                    }
                    this.isBackgroudChecking = false;
                    if (this.listener != null) {
                        this.listener.onPurchaseResult(ptxId3, userId3, txId3, priceId, Integer.parseInt(status2), statusDetail2);
                    }
                    if (refValue[3] == null) {
                        showAlert(Resource.STRING_ALERT_CHARGED[this.langId]);
                    }
                } else if (status2.equals(Config.SMS_STATE_SENDING) || status2.equals("100")) {
                    if (refValue[3] == null) {
                        if (!ptxId3.equals(this.pgDialog.getPtxId())) {
                            backgroundCheckCharged(ptxId3, userId3, txId3);
                        } else if (this.pgDialog.isShowing()) {
                            this.progressIndex = this.progressIndex + 10;
                            this.pgDialog.setWaitProgress(this.progressIndex);
                            if (this.progressIndex < this.progressMax) {
                                try {
                                    Thread.sleep(10000);
                                } catch (Exception e6) {
                                }
                                checkCharged(ptxId3, userId3, txId3);
                                return;
                            }
                            if (this.listener != null) {
                                this.listener.onEvent(ptxId3, userId3, txId3, 203, Resource.STRING_EVENT_BACKGROUND_CHARGING[this.langId]);
                            }
                            this.pgDialog.dismiss();
                            showAlert(Resource.STRING_ALERT_BACKGROUND_CHARGING[this.langId]);
                            backgroundCheckCharged(ptxId3, userId3, txId3);
                        } else {
                            backgroundCheckCharged(ptxId3, userId3, txId3);
                        }
                    } else if (this.listener != null) {
                        this.listener.onEvent(ptxId3, userId3, txId3, 202, Resource.STRING_EVENT_EASY2PAY_IS_CHARGING[this.langId]);
                    }
                } else if (!status2.equals("404")) {
                    if (this.pgDialog.isShowing()) {
                        this.pgDialog.setWaitProgress(this.progressMax);
                        this.pgDialog.dismiss();
                    }
                    this.isBackgroudChecking = false;
                    if (this.listener != null) {
                        this.listener.onPurchaseResult(ptxId3, userId3, txId3, priceId, Integer.parseInt(status2), statusDetail2);
                    }
                    if (refValue[3] == null) {
                        showAlert(Resource.STRING_ALERT_CANNOT_CHARGING[this.langId]);
                    }
                }
            } catch (Exception e7) {
                if (this.listener != null) {
                    Easy2PayListener easy2PayListener = this.listener;
                    if (this.pgDialog.isShowing()) {
                        str = this.pgDialog.getPtxId();
                    } else {
                        str = null;
                    }
                    if (this.pgDialog.isShowing()) {
                        str2 = this.pgDialog.getUserId();
                    } else {
                        str2 = null;
                    }
                    easy2PayListener.onError(str, str2, null, 306, Resource.STRING_ERROR_XML_IS_INVALID[this.langId]);
                }
            }
        }
    }

    public final void onError(String sessionId, String[] refValue, int errCode, String description) {
        if (sessionId.equals(GET_PRICES_SESSION)) {
            this.ldDialog.dismiss();
            if (this.listener != null) {
                this.listener.onError(refValue[0], refValue[1], null, 301, description);
            }
        } else if (sessionId.equals(GET_PINCODE_SESSION)) {
            this.ldDialog.dismiss();
            if (this.listener != null) {
                this.listener.onError(refValue[0], refValue[1], null, 302, description);
            }
        } else if (sessionId.equals(CHECK_CHAREGD_SESSION)) {
            String ptxId2 = refValue[0];
            String userId2 = refValue[1];
            String txId2 = refValue[2];
            if (refValue[3] == null) {
                if (!ptxId2.equals(this.pgDialog.getPtxId())) {
                    backgroundCheckCharged(ptxId2, userId2, txId2);
                } else if (this.pgDialog.isShowing()) {
                    this.progressIndex += 10;
                    this.pgDialog.setWaitProgress(this.progressIndex);
                    if (this.progressIndex < this.progressMax) {
                        try {
                            Thread.sleep(10000);
                        } catch (Exception e) {
                        }
                        checkCharged(ptxId2, userId2, txId2);
                        return;
                    }
                    if (this.listener != null) {
                        this.listener.onEvent(ptxId2, userId2, txId2, 203, Resource.STRING_EVENT_BACKGROUND_CHARGING[this.langId]);
                    }
                    this.pgDialog.dismiss();
                    showAlert(Resource.STRING_ALERT_BACKGROUND_CHARGING[this.langId]);
                    backgroundCheckCharged(ptxId2, userId2, txId2);
                } else {
                    backgroundCheckCharged(ptxId2, userId2, txId2);
                }
            } else if (this.listener != null) {
                this.listener.onEvent(ptxId2, userId2, txId2, 202, Resource.STRING_EVENT_EASY2PAY_IS_CHARGING[this.langId]);
            }
        }
    }

    public final void onOK(String ptxId2, String userId2, String priceId, String smsNo) {
        this.ldDialog.show();
        if (userId2 == null) {
            userId2 = "";
        }
        String sig = MD5Factory.generate(stringSortAndCat(new String[]{"p_txid", AccessToken.USER_ID_KEY, "price_id", "partner_id", "msisdn"}, new String[]{ptxId2, userId2, priceId, this.serviceId, ""}) + this.secretKey);
        new Easy2PayConnection(this).get(GET_PINCODE_SESSION, new String[]{ptxId2, userId2, smsNo}, "https://sea-sdk.molthailand.com/init.php?p_txid=" + ptxId2 + "&user_id=" + userId2 + "&price_id=" + priceId + "&partner_id=" + this.serviceId + "&msisdn=" + "&sig=" + sig);
    }

    public final void onCancel(String ptxId2, String userId2) {
        if (this.listener != null) {
            this.listener.onEvent(ptxId2, userId2, null, 201, Resource.STRING_EVENT_USER_CANCEL_CHARGE[this.langId]);
        }
    }

    /* access modifiers changed from: private */
    public void showAlert(final String message) {
        this.handler.post(new Runnable() {
            public void run() {
                AlertDialog.Builder alert = new AlertDialog.Builder(Easy2Pay.this.context);
                alert.setCancelable(false);
                alert.setMessage(message).setPositiveButton(Resource.TXT_BUTTON_OK[Easy2Pay.this.langId], (DialogInterface.OnClickListener) null).show();
            }
        });
    }

    private void backgroundCheckCharged(String ptxId2, String userId2, String txId2) {
        try {
            Thread.sleep(10000);
        } catch (Exception e) {
        }
        checkCharged(ptxId2, userId2, txId2);
    }

    /* access modifiers changed from: private */
    public void checkCharged(String ptxId2, String userId2, String txId2) {
        checkCharged(ptxId2, userId2, txId2, true);
    }

    private void checkCharged(String ptxId2, String userId2, String txId2, boolean isAuto) {
        this.isBackgroudChecking = true;
        String sig = MD5Factory.generate(txId2 + this.secretKey);
        Easy2PayConnection conn = new Easy2PayConnection(this);
        String[] strArr = new String[4];
        strArr[0] = ptxId2;
        strArr[1] = userId2;
        strArr[2] = txId2;
        strArr[3] = isAuto ? null : "" + isAuto;
        conn.get(CHECK_CHAREGD_SESSION, strArr, "https://sea-sdk.molthailand.com/inquiry.php?txid=" + txId2 + "&sig=" + sig);
    }

    public void onActivityPause() {
        if (this.pgDialog.isShowing()) {
            this.pgDialog.setWaitProgress(this.progressMax);
            this.pgDialog.dismiss();
            showAlert(Resource.STRING_ALERT_BACKGROUND_CHARGING[this.langId]);
            checkCharging(this.ptxId, this.userId, this.ptxId);
        }
    }

    public void onActivityResume() {
    }

    private class SMSBroadcastReceiver extends BroadcastReceiver {
        private SMSBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String ptxId = intent.getStringExtra("ptxId");
            String txId = intent.getStringExtra(PSMSResponse.TRANSACTION_ID);
            String userId = intent.getStringExtra(PSMSResponse.USER_ID);
            switch (getResultCode()) {
                case -1:
                    int unused = Easy2Pay.this.progressIndex = Easy2Pay.this.progressIndex + 10;
                    Easy2Pay.this.pgDialog.setWaitProgress(Easy2Pay.this.progressIndex);
                    Easy2Pay.this.checkCharged(ptxId, userId, txId);
                    break;
                default:
                    if (Easy2Pay.this.listener != null) {
                        Easy2Pay.this.listener.onError(ptxId, userId, txId, 305, "Can not send message because no sevice, null PDU, radio off or etc.");
                    }
                    Easy2Pay.this.pgDialog.dismiss();
                    Easy2Pay.this.showAlert(Resource.STRING_ALERT_CANNOT_SEND_SMS[Easy2Pay.this.langId]);
                    break;
            }
            context.unregisterReceiver(Easy2Pay.this.smsListener);
        }
    }
}
