package com.android.easy2pay;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import java.util.Vector;

public class Easy2PayScreen extends Activity {
    private static final float BUTTION_OK_WEIGHT = 0.7f;
    private static final float DIMENSIONS_DIFF_PORTRAIT = 40.0f;
    /* access modifiers changed from: private */
    public static Easy2PayDialogListener listener = null;
    private String description;
    private int langId;
    /* access modifiers changed from: private */
    public Vector<String[]> priceList;
    /* access modifiers changed from: private */
    public String ptxId;
    private String title;
    /* access modifiers changed from: private */
    public String userId;

    /* access modifiers changed from: protected */
    public void setEasy2PayDialogListener(Easy2PayDialogListener listener2) {
        listener = listener2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        String str;
        String desc;
        Spinner sp_price;
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        Intent intent = getIntent();
        this.title = intent.getStringExtra("title");
        this.description = intent.getStringExtra("desc");
        this.priceList = new Vector<>();
        String[] pIds = intent.getStringArrayExtra("pids");
        String[] pDescs = intent.getStringArrayExtra("pdescs");
        String[] sCodes = intent.getStringArrayExtra("scodes");
        for (int i = 0; i < pIds.length; i++) {
            this.priceList.addElement(new String[]{pIds[i], pDescs[i], sCodes[i]});
        }
        this.ptxId = intent.getStringExtra("ptxid");
        this.userId = intent.getStringExtra("userid");
        this.langId = Integer.parseInt(intent.getStringExtra("langId"));
        float scale = getResources().getDisplayMetrics().density;
        LinearLayout layout_hscreen = new LinearLayout(this);
        layout_hscreen.setOrientation(0);
        layout_hscreen.setBackgroundColor(-1358954496);
        layout_hscreen.setGravity(16);
        setContentView(layout_hscreen, new LinearLayout.LayoutParams(-1, -1));
        LinearLayout layout_vscreen = new LinearLayout(this);
        layout_vscreen.setOrientation(1);
        layout_vscreen.setGravity(1);
        layout_hscreen.addView(layout_vscreen, new LinearLayout.LayoutParams(-1, -2));
        LinearLayout layout_border = new LinearLayout(this);
        layout_border.setPadding(3, 3, 3, 3);
        layout_border.setBackgroundColor(-1342177281);
        layout_vscreen.addView(layout_border, new LinearLayout.LayoutParams(((int) (320.0f * scale)) - ((int) ((DIMENSIONS_DIFF_PORTRAIT * scale) + 0.5f)), -2));
        LinearLayout layout_main = new LinearLayout(this);
        layout_main.setOrientation(1);
        layout_main.setBackgroundColor(-1358954496);
        layout_border.addView(layout_main, new LinearLayout.LayoutParams(((int) (320.0f * scale)) - ((int) ((DIMENSIONS_DIFF_PORTRAIT * scale) + 0.5f)), -2));
        TextView textView = new TextView(this);
        textView.setText(this.title);
        textView.setTextColor(-1);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setBackgroundColor(2144128204);
        textView.setPadding((int) (10.0f * scale), (int) (2.0f * scale), (int) (10.0f * scale), (int) (2.0f * scale));
        layout_main.addView(textView);
        TextView textView2 = new TextView(this);
        String desc2 = this.description;
        if (desc2.contains("%s")) {
            desc = desc2.replace("%s", this.priceList.elementAt(0)[1]);
        } else {
            StringBuilder append = new StringBuilder().append(desc2);
            if (this.priceList.size() == 1) {
                str = " \"" + this.priceList.elementAt(0)[1] + "\" ?";
            } else {
                str = " ?";
            }
            desc = append.append(str).toString();
        }
        textView2.setText(desc);
        textView2.setTextColor(-3355444);
        textView2.setPadding((int) (10.0f * scale), (int) (5.0f * scale), (int) (10.0f * scale), (int) (5.0f * scale));
        layout_main.addView(textView2);
        if (this.priceList.size() > 1) {
            sp_price = new Spinner(this);
            String[] prices = new String[this.priceList.size()];
            for (int i2 = 0; i2 < this.priceList.size(); i2++) {
                prices[i2] = this.priceList.elementAt(i2)[1];
            }
            sp_price.setAdapter((SpinnerAdapter) new ArrayAdapter<>(this, 17367048, prices));
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.setMargins((int) (10.0f * scale), 0, (int) (10.0f * scale), (int) (5.0f * scale));
            layoutParams.gravity = 1;
            layout_main.addView(sp_price, layoutParams);
        } else {
            sp_price = null;
        }
        LinearLayout layout_button = new LinearLayout(this);
        layout_button.setOrientation(0);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.setMargins((int) (10.0f * scale), (int) (5.0f * scale), (int) (10.0f * scale), (int) (5.0f * scale));
        layout_main.addView(layout_button, layoutParams2);
        Button bn_ok = new Button(this);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.weight = BUTTION_OK_WEIGHT;
        bn_ok.setLayoutParams(layoutParams3);
        bn_ok.setText(Resource.TXT_BUTTON_OK[this.langId]);
        final Spinner spinner = sp_price;
        bn_ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int i;
                if (Easy2PayScreen.listener != null) {
                    Vector access$100 = Easy2PayScreen.this.priceList;
                    if (spinner != null) {
                        i = spinner.getSelectedItemPosition();
                    } else {
                        i = 0;
                    }
                    String[] priceData = (String[]) access$100.elementAt(i);
                    Easy2PayScreen.listener.onOK(Easy2PayScreen.this.ptxId, Easy2PayScreen.this.userId, priceData[0], priceData[2]);
                }
                Easy2PayScreen.this.finish();
            }
        });
        layout_button.addView(bn_ok);
        Button bn_cancel = new Button(this);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams4.weight = 0.3f;
        bn_cancel.setLayoutParams(layoutParams4);
        bn_cancel.setText(Resource.TXT_BUTTON_CANCEL[this.langId]);
        bn_cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Easy2PayScreen.listener != null) {
                    Easy2PayScreen.listener.onCancel(Easy2PayScreen.this.ptxId, Easy2PayScreen.this.userId);
                }
                Easy2PayScreen.this.finish();
            }
        });
        layout_button.addView(bn_cancel);
    }

    public void onBackPressed() {
        if (listener != null) {
            listener.onCancel(this.ptxId, this.userId);
        }
        super.onBackPressed();
    }
}
