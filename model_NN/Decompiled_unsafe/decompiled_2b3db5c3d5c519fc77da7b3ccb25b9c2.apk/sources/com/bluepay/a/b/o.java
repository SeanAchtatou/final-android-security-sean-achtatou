package com.bluepay.a.b;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import com.bluepay.pay.Client;
import com.bluepay.sdk.c.aa;

/* compiled from: Proguard */
class o extends BroadcastReceiver {
    private Context a;
    private AlertDialog b;

    public o(Context context, AlertDialog alertDialog) {
        this.a = context;
        this.b = alertDialog;
    }

    public void a() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.CLOSE_SYSTEM_DIALOGS");
        this.a.registerReceiver(this, intentFilter);
    }

    public void b() {
        this.a.unregisterReceiver(this);
    }

    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.CLOSE_SYSTEM_DIALOGS".equals(intent.getAction())) {
            String stringExtra = intent.getStringExtra("reason");
            if ("homekey".equals(stringExtra)) {
                Log.i(Client.TAG, "home key");
                aa.d();
                this.b.dismiss();
            } else if ("recentapps".equals(stringExtra)) {
                aa.d();
                this.b.dismiss();
                Log.i(Client.TAG, "home key long press");
            }
        }
    }
}
