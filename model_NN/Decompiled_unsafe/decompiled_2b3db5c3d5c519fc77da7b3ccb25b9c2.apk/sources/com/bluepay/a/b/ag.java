package com.bluepay.a.b;

import android.app.Activity;
import android.text.TextUtils;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.interfaceClass.a;
import com.bluepay.interfaceClass.c;
import com.bluepay.pay.Client;
import com.bluepay.pay.PublisherCode;
import com.bluepay.sdk.c.aa;

/* compiled from: Proguard */
public class ag extends a {
    c a;

    public ag(c cVar) {
        this.a = cVar;
        a();
    }

    private void a() {
    }

    public void a(b bVar) {
    }

    public void b(b bVar) {
        d(bVar);
    }

    public void c(b bVar) {
        d(bVar);
    }

    /* access modifiers changed from: private */
    public void d(b bVar) {
        if (!TextUtils.isEmpty(bVar.getDesMsisdn())) {
            f(bVar);
        } else if (Client.m_DcbInfo.p == 0) {
            aa.a(bVar.getActivity(), new ah(this, bVar));
        } else {
            e(bVar);
        }
        com.bluepay.sdk.b.c.c("doTask()");
    }

    /* access modifiers changed from: private */
    public void e(b bVar) {
        aa.a(bVar.getActivity(), "", new ai(this, bVar));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void */
    /* access modifiers changed from: private */
    public void f(b bVar) {
        if (bVar.getShowUI()) {
            aa.a(bVar.getActivity(), (CharSequence) "", (CharSequence) "");
        }
        new Thread(new aj(this, bVar)).start();
    }

    public int a(com.bluepay.interfaceClass.b bVar, b bVar2) {
        int i = 0;
        if (bVar == null || bVar2 == null) {
            return -1;
        }
        int a2 = bVar.a();
        com.bluepay.sdk.b.c.c(bVar.b());
        if (a2 == h.a) {
            try {
                aw b = au.b(bVar.b());
                int c = b.c("status");
                if (c == h.b) {
                    String b2 = b.b("payment_code");
                    bVar2.b(b2);
                    this.a.a(14, c, 0, bVar2);
                    if (bVar2.getCPPayType().equals(PublisherCode.PUBLISHER_OFFLINE_ATM)) {
                        i = Client.m_BankChargeInfo.f;
                    } else if (bVar2.getCPPayType().equals(PublisherCode.PUBLISHER_OFFLINE_OTC)) {
                        i = Client.m_BankChargeInfo.e;
                    }
                    if (bVar2.getShowUI()) {
                        a(bVar2.getActivity(), i, bVar2.getPrice(), b2, bVar2.getCPPayType());
                    }
                } else {
                    this.a.a(14, c, 0, bVar2);
                }
                return c;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 1;
    }

    private void a(Activity activity, int i, int i2, String str, String str2) {
        activity.runOnUiThread(new ak(this, activity, str2, str, i, i2));
    }
}
