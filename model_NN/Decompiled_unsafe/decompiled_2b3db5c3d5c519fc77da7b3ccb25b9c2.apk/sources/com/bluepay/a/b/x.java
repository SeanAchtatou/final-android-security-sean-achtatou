package com.bluepay.a.b;

import android.view.View;
import android.widget.EditText;

/* compiled from: Proguard */
class x implements View.OnClickListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ w b;

    x(w wVar, EditText editText) {
        this.b = wVar;
        this.a = editText;
    }

    public void onClick(View v) {
        this.b.c.f.setClickable(false);
        s.e.start();
        this.b.c.a(this.b.b, this.a.getText().toString().trim());
    }
}
