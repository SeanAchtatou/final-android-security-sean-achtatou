package com.bluepay.a.b;

import android.content.BroadcastReceiver;
import android.os.Build;
import android.text.TextUtils;
import com.bluepay.data.Config;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.data.i;
import com.bluepay.data.m;
import com.bluepay.interfaceClass.a;
import com.bluepay.interfaceClass.c;
import com.bluepay.pay.Client;
import com.bluepay.receiver.SmsReceiver;
import com.bluepay.sdk.c.aa;
import com.bluepay.sdk.c.d;
import com.bluepay.sdk.c.e;
import com.bluepay.sdk.c.g;
import com.bluepay.sdk.exception.BlueException;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.tencent.android.tpush.common.MessageKey;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: Proguard */
public class ab extends a {
    public static final String c = "android.provider.Telephony.SMS_RECEIVED";
    c a;
    SmsReceiver b;
    /* access modifiers changed from: private */
    public boolean e = true;

    public ab(c cVar) {
        this.a = cVar;
    }

    public void a(b bVar) {
    }

    public void b(b bVar) {
        d(bVar);
    }

    public void c(b bVar) {
        d(bVar);
    }

    private void d(b bVar) {
        com.bluepay.sdk.b.c.c("request the createTransaction");
        this.e = true;
        if (Client.m_DcbInfo.p == 0) {
            aa.a(bVar.getActivity(), new ac(this, bVar));
            return;
        }
        this.e = false;
        e(bVar);
    }

    /* access modifiers changed from: private */
    public void e(b bVar) {
        aa.b(bVar.getActivity(), "", new ad(this, bVar));
    }

    /* access modifiers changed from: private */
    public void a(b bVar, String str) {
        g.a(bVar.getActivity(), bVar, this.a, str, new ae(this, bVar));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void */
    /* access modifiers changed from: private */
    public void b(b bVar, String str) {
        if (bVar.getShowUI()) {
            aa.a(bVar.getActivity(), (CharSequence) "", (CharSequence) i.a((byte) 6));
        }
        String p = m.p();
        HashMap hashMap = new HashMap();
        hashMap.put("transactionId", bVar.getTransactionId());
        hashMap.put("productId", Integer.valueOf(bVar.getProductId()));
        hashMap.put("promotionId", bVar.getPromotionId());
        hashMap.put("propsName", bVar.getPropsName());
        hashMap.put(FirebaseAnalytics.Param.PRICE, Integer.valueOf(bVar.getPrice()));
        hashMap.put("telco_name", Client.telcoName);
        hashMap.put("msisdn", aa.d(str));
        hashMap.put("version", Integer.valueOf((int) Config.VERSION));
        try {
            String str2 = d.a(hashMap).toString();
            hashMap.put("encrypt", e.a(str2 + Client.getEncrypt()));
            if (!Config.TELCO_NAME_TELKOMSEL.equals(Client.telcoName) && aa.c(bVar.getActivity(), "android.permission.READ_SMS") && !Build.BRAND.equals("Xiaomi") && this.e) {
                this.b = new SmsReceiver(bVar);
                this.b.a();
            }
            a(com.bluepay.sdk.a.a.a(bVar.getActivity(), p, str2, hashMap), bVar);
        } catch (BlueException e2) {
            if (this.b != null) {
                this.b.a((BroadcastReceiver) this.b);
            }
            e2.printStackTrace();
            bVar.desc = e2.getMessage();
            this.a.a(14, e2.getCode(), 0, bVar);
        } catch (Exception e3) {
            if (this.b != null) {
                this.b.a((BroadcastReceiver) this.b);
            }
            e3.printStackTrace();
            this.a.a(14, h.n, 0, bVar);
            com.bluepay.sdk.b.c.b("the get is ERROR >>>> " + hashMap.toString());
        }
    }

    public int a(com.bluepay.interfaceClass.b bVar, b bVar2) {
        if (Client.telcoName.equals(Config.TELCO_NAME_SMARTFREN)) {
            super.a(bVar, bVar2);
            return 0;
        } else if (bVar.a() != h.a) {
            return 0;
        } else {
            String b2 = bVar.b();
            com.bluepay.sdk.b.c.c("doLogic" + b2);
            aw b3 = au.b(b2);
            int c2 = b3.c("status");
            if (c2 == h.b) {
                if (Config.TELCO_NAME_TELKOMSEL.equals(Client.telcoName)) {
                    a(b3, bVar2);
                } else if (!this.e) {
                    com.bluepay.sdk.b.c.c("not one key to pay need manully reply");
                    bVar2.desc = i.a(i.ad) + Client.m_DcbInfo.a(bVar2.getDesMsisdn());
                    bVar2.getActivity().runOnUiThread(new af(this, bVar2));
                    a.o.a(14, c2, 0, bVar2);
                } else if (Build.BRAND.equals("Xiaomi")) {
                    com.bluepay.sdk.b.c.c("Xiaomi mobile phone need manually reply");
                    bVar2.desc = i.a(i.ad) + Client.m_DcbInfo.a(bVar2.getDesMsisdn());
                    a.o.a(14, c2, 0, bVar2);
                    return 1;
                } else if (!aa.c(bVar2.getActivity(), "android.permission.READ_SMS")) {
                    com.bluepay.sdk.b.c.c("no READ_SMS permission");
                    if (!(this.b == null || this.b == null)) {
                        this.b.a((BroadcastReceiver) this.b);
                    }
                    bVar2.desc = i.a(i.ad) + Client.m_DcbInfo.a(bVar2.getDesMsisdn());
                    a.o.a(14, c2, 0, bVar2);
                }
                return 1;
            }
            if (this.b != null) {
                this.b.a((BroadcastReceiver) this.b);
            }
            a.o.a(14, c2, 0, bVar2);
            return 1;
        }
    }

    private void a(aw awVar, b bVar) {
        String str;
        String str2 = null;
        try {
            str = awVar.b("shortcode");
            try {
                str2 = awVar.b(MessageKey.MSG_CONTENT);
            } catch (Exception e2) {
            }
        } catch (Exception e3) {
            str = null;
            if (TextUtils.isEmpty(str) || TextUtils.isEmpty(null)) {
                throw new BlueException(h.i, i.a(i.ae), new Object[0]);
            }
            bVar.c(str);
            bVar.d(str2);
            bVar.a(0);
            a.j.add(bVar);
            a.o.a(5, bVar.a(), 0, bVar);
        }
        bVar.c(str);
        bVar.d(str2);
        bVar.a(0);
        a.j.add(bVar);
        a.o.a(5, bVar.a(), 0, bVar);
    }

    /* access modifiers changed from: private */
    public void f(b bVar) {
        if (Client.m_DcbInfo.b == 4) {
            h(bVar);
        } else if (Client.m_DcbInfo.b == 7) {
            g(bVar);
        } else {
            g(bVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void */
    private void g(b bVar) {
        com.bluepay.sdk.b.c.c("request the dcb");
        if (bVar.getShowUI()) {
            aa.a(bVar.getActivity(), (CharSequence) "", (CharSequence) "");
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        com.bluepay.sdk.b.c.c("paytype:" + bVar.getCPPayType());
        int price = bVar.getPrice();
        linkedHashMap.put("productId", bVar.getProductId() + "");
        linkedHashMap.put(FirebaseAnalytics.Param.PRICE, Integer.valueOf(price));
        linkedHashMap.put("promotionId", bVar.getPromotionId());
        linkedHashMap.put("transactionId", bVar.getTransactionId());
        try {
            String a2 = m.a(7);
            linkedHashMap.put("imsi", Client.m_iIMSI);
            linkedHashMap.put("msisdn", bVar.getDesMsisdn());
            linkedHashMap.put("telco_name", Client.telcoName);
            linkedHashMap.put("encrypt", a(linkedHashMap, Client.getEncrypt()).toString());
            a(com.bluepay.sdk.a.a.a(bVar.getActivity(), a2, "productId=" + linkedHashMap.get("productId").toString() + "&price=" + linkedHashMap.get(FirebaseAnalytics.Param.PRICE).toString() + "&promotionId=" + linkedHashMap.get("promotionId").toString() + "&transactionId=" + linkedHashMap.get("transactionId").toString() + "&imsi=" + linkedHashMap.get("imsi").toString() + "&msisdn=" + linkedHashMap.get("msisdn") + "&telco_name=" + linkedHashMap.get("telco_name"), linkedHashMap), bVar);
        } catch (BlueException e2) {
            bVar.desc = e2.getMessage();
            this.a.a(14, e2.getCode(), 0, bVar);
            e2.printStackTrace();
            com.bluepay.sdk.b.c.b("the dcb is ERROR >>>> " + linkedHashMap.toString());
        } catch (Exception e3) {
            bVar.desc = e3.getMessage();
            this.a.a(14, h.i, 0, bVar);
            e3.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void */
    private void h(b bVar) {
        com.bluepay.sdk.b.c.c("request the dcb");
        if (bVar.getShowUI()) {
            aa.a(bVar.getActivity(), (CharSequence) "", (CharSequence) "");
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        com.bluepay.sdk.b.c.c("paytype:" + bVar.getCPPayType());
        linkedHashMap.put("productid", bVar.getProductId() + "");
        linkedHashMap.put(FirebaseAnalytics.Param.PRICE, Integer.valueOf(bVar.getPrice()));
        linkedHashMap.put("promotionid", bVar.getPromotionId());
        linkedHashMap.put("transactionid", bVar.getTransactionId());
        try {
            String a2 = m.a(4);
            String str = a(linkedHashMap, Client.getEncrypt()).toString();
            linkedHashMap.put("imsi", Client.m_iIMSI);
            linkedHashMap.put("msisdn", bVar.getDesMsisdn());
            linkedHashMap.put("encrypt", str);
            a(com.bluepay.sdk.a.a.a(bVar.getActivity(), a2, "imsi=" + linkedHashMap.get("imsi").toString() + "&msisdn=" + linkedHashMap.get("msisdn").toString() + "&productid=" + linkedHashMap.get("productid").toString() + "&price=" + linkedHashMap.get(FirebaseAnalytics.Param.PRICE).toString() + "&promotionid=" + linkedHashMap.get("promotionid").toString() + "&transactionid=" + linkedHashMap.get("transactionid"), linkedHashMap), bVar);
        } catch (Exception e2) {
            e2.printStackTrace();
            com.bluepay.sdk.b.c.b("the dcb is ERROR >>>> " + linkedHashMap.toString());
            bVar.desc = e2.getMessage();
            this.a.a(14, h.i, 0, bVar);
        }
    }

    public String a(Map map, String str) {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            for (String str2 : map.keySet()) {
                String valueOf = String.valueOf(map.get(str2));
                if (valueOf == null) {
                    valueOf = "";
                }
                stringBuffer.append(str2 + "=" + valueOf + "&");
            }
            String str3 = stringBuffer.toString().substring(0, stringBuffer.toString().length() - 1) + str;
            com.bluepay.sdk.b.c.c("encry:" + str3);
            return e.a(str3);
        } catch (Exception e2) {
            e2.printStackTrace();
            throw new Exception(e2);
        }
    }
}
