package com.bluepay.a.b;

import android.view.View;
import android.widget.TextView;
import com.bluepay.data.Order;
import com.bluepay.data.i;
import com.bluepay.sdk.c.aa;

/* compiled from: Proguard */
final class h implements View.OnClickListener {
    final /* synthetic */ TextView a;
    final /* synthetic */ Order b;

    h(TextView textView, Order order) {
        this.a = textView;
        this.b = order;
    }

    public void onClick(View v) {
        if (this.a.getText().toString().equals(i.a(i.aP))) {
            aa.c(this.b.getActivity());
        }
    }
}
