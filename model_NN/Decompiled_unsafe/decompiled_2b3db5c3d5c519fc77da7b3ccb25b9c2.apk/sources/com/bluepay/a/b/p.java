package com.bluepay.a.b;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import com.bluepay.sdk.c.aa;

/* compiled from: Proguard */
class p implements TextWatcher {
    EditText a;
    TextView b;
    Context c;
    boolean d = false;

    public p(EditText editText, TextView textView, Context context) {
        this.a = editText;
        this.b = textView;
        this.c = context;
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (this.a != null) {
            if (this.a.getId() == aa.a(this.c, "id", "et_Seral_first") && s.length() == 4 && this.a != null) {
                this.d = true;
            } else if (this.a.getId() == aa.a(this.c, "id", "et_Seral_second") && s.length() == 4 && this.a != null) {
                this.d = true;
            } else if (this.a.getId() == aa.a(this.c, "id", "et_Seral_third") && s.length() == 4 && this.a != null) {
                this.d = true;
            } else if (this.a.getId() == aa.a(this.c, "id", "et_Seral_fourth") && s.length() == 8 && this.a != null) {
                this.d = true;
            } else if (this.a.getId() == aa.a(this.c, "id", "et_unipin_first_input") && s.length() == 1 && this.a != null) {
                this.d = true;
            } else if (this.a.getId() == aa.a(this.c, "id", "et_unipin_second_input") && s.length() == 15 && this.a != null) {
                this.d = true;
            }
            if (this.d) {
                this.a.clearFocus();
                this.b.requestFocus();
            }
            this.d = false;
        }
    }

    public void afterTextChanged(Editable s) {
    }
}
