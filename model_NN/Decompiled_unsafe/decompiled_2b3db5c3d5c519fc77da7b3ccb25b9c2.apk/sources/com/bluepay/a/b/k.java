package com.bluepay.a.b;

import android.app.Activity;
import com.bluepay.data.Config;
import com.bluepay.pay.Client;
import com.bluepay.pay.ClientHelper;
import com.bluepay.sdk.b.b;

/* compiled from: Proguard */
final class k implements Runnable {
    final /* synthetic */ Activity a;
    final /* synthetic */ String b;
    final /* synthetic */ String c;

    k(Activity activity, String str, String str2) {
        this.a = activity;
        this.b = str;
        this.c = str2;
    }

    public void run() {
        b.a(this.a, Client.TAG);
        b.a("version", (int) Config.VERSION);
        b.a("BluePay_statUrl", this.b);
        b.a("BluePay_apiUrl", this.c);
        a.g = ClientHelper.generateSystemTime();
        b.a("BluePay_verionTime", a.g);
        b.a();
    }
}
