package com.bluepay.a.b;

import android.text.TextUtils;
import com.bluepay.data.Config;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.interfaceClass.d;
import com.bluepay.pay.Client;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.c.aa;

/* compiled from: Proguard */
class ac implements d {
    final /* synthetic */ b a;
    final /* synthetic */ ab b;

    ac(ab abVar, b bVar) {
        this.b = abVar;
        this.a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.a.b.ab.a(com.bluepay.a.b.ab, boolean):boolean
     arg types: [com.bluepay.a.b.ab, int]
     candidates:
      com.bluepay.a.b.ab.a(com.bluepay.a.b.ab, com.bluepay.data.b):void
      com.bluepay.a.b.ab.a(com.bluepay.a.b.aw, com.bluepay.data.b):void
      com.bluepay.a.b.ab.a(com.bluepay.data.b, java.lang.String):void
      com.bluepay.a.b.ab.a(com.bluepay.interfaceClass.b, com.bluepay.data.b):int
      com.bluepay.a.b.ab.a(java.util.Map, java.lang.String):java.lang.String
      com.bluepay.interfaceClass.a.a(com.bluepay.interfaceClass.b, com.bluepay.data.b):int
      com.bluepay.a.b.ab.a(com.bluepay.a.b.ab, boolean):boolean */
    public void a(int i, String str) {
        if (i == 1) {
            boolean unused = this.b.e = true;
            this.a.setDesMsisdn(str);
            String g = aa.g(str);
            c.c("------telcoName:" + g);
            if (!TextUtils.isEmpty(g)) {
                Client.telcoName = g;
            }
            if (Client.telcoName.equals(Config.TELCO_NAME_SMARTFREN)) {
                this.b.f(this.a);
            } else if (Client.m_DcbInfo.q != 0 || !Client.telcoName.equals(Config.TELCO_NAME_TELKOMSEL)) {
                this.b.b(this.a, str);
            } else {
                boolean unused2 = this.b.e = false;
                this.b.e(this.a);
            }
        } else if (i == -1) {
            boolean unused3 = this.b.e = false;
            this.b.e(this.a);
        } else {
            this.b.a.a(14, h.C, 0, this.a);
        }
    }
}
