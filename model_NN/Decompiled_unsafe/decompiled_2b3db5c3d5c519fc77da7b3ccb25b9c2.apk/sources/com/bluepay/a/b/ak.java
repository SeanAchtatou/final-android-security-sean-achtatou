package com.bluepay.a.b;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.bluepay.data.i;
import com.bluepay.pay.PublisherCode;
import com.bluepay.sdk.c.aa;

/* compiled from: Proguard */
class ak implements Runnable {
    final /* synthetic */ Activity a;
    final /* synthetic */ String b;
    final /* synthetic */ String c;
    final /* synthetic */ int d;
    final /* synthetic */ int e;
    final /* synthetic */ ag f;

    ak(ag agVar, Activity activity, String str, String str2, int i, int i2) {
        this.f = agVar;
        this.a = activity;
        this.b = str;
        this.c = str2;
        this.d = i;
        this.e = i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    public void run() {
        AlertDialog create = new AlertDialog.Builder(this.a).create();
        create.show();
        View inflate = LayoutInflater.from(this.a).inflate(aa.a((Context) this.a, "layout", "bluep_tips_offline"), (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(aa.a((Context) this.a, "id", "tv_offline_channel"));
        TextView textView2 = (TextView) inflate.findViewById(aa.a((Context) this.a, "id", "tv_offline_paymentcode"));
        TextView textView3 = (TextView) inflate.findViewById(aa.a((Context) this.a, "id", "tv_offline_fee"));
        TextView textView4 = (TextView) inflate.findViewById(aa.a((Context) this.a, "id", "tv_offline_price"));
        TextView textView5 = (TextView) inflate.findViewById(aa.a((Context) this.a, "id", "tv_tips"));
        Button button = (Button) inflate.findViewById(aa.a((Context) this.a, "id", "btn_offline_goto_tips"));
        Button button2 = (Button) inflate.findViewById(aa.a((Context) this.a, "id", "btn_offline_concel"));
        ((TextView) inflate.findViewById(aa.a((Context) this.a, "id", "textView1"))).setText(i.a(i.ap));
        ((TextView) inflate.findViewById(aa.a((Context) this.a, "id", "textView2"))).setText(i.a(i.ar));
        ((TextView) inflate.findViewById(aa.a((Context) this.a, "id", "textView3"))).setText(i.a(i.aq));
        ((TextView) inflate.findViewById(aa.a((Context) this.a, "id", "textView4"))).setText(i.a(i.ao));
        textView.setText(this.b.equals(PublisherCode.PUBLISHER_OFFLINE_OTC) ? "bayar di mini market" : this.b);
        if (this.b.equals(PublisherCode.PUBLISHER_OFFLINE_ATM)) {
            textView5.setVisibility(8);
        } else if (this.b.equals(PublisherCode.PUBLISHER_OFFLINE_OTC)) {
            textView5.setVisibility(0);
            textView5.setText("Silahkan pergi ke Alfamart, Alfamidi, Lawson, DAN+DAN untuk melakukan pembayaran");
        }
        textView2.setText(this.c);
        textView3.setText(this.d + "");
        textView4.setText(this.e + "");
        create.setContentView(inflate);
        button.setOnClickListener(new al(this));
        button2.setOnClickListener(new am(this, create));
    }
}
