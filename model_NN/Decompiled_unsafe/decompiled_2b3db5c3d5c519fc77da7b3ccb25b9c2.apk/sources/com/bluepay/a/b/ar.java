package com.bluepay.a.b;

import android.content.Intent;
import android.net.Uri;
import com.bluepay.data.b;
import com.bluepay.data.h;

/* compiled from: Proguard */
class ar implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ b b;
    final /* synthetic */ ap c;

    ar(ap apVar, String str, b bVar) {
        this.c = apVar;
        this.a = str;
        this.b = bVar;
    }

    public void run() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(this.a));
        this.b.getActivity().startActivityForResult(intent, 1);
        this.c.a.a(14, h.b, 0, this.b);
    }
}
