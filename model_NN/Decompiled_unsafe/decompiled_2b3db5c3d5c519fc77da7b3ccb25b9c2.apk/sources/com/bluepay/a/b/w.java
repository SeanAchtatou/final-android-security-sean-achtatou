package com.bluepay.a.b;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.bluepay.data.b;
import com.bluepay.data.i;
import com.bluepay.sdk.c.a;
import com.bluepay.sdk.c.aa;
import com.tencent.android.tpush.common.MessageKey;

/* compiled from: Proguard */
class w implements Runnable {
    final /* synthetic */ Activity a;
    final /* synthetic */ b b;
    final /* synthetic */ s c;

    w(s sVar, Activity activity, b bVar) {
        this.c = sVar;
        this.a = activity;
        this.b = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    public void run() {
        AlertDialog create = new AlertDialog.Builder(this.a, 3).create();
        create.setCanceledOnTouchOutside(false);
        create.setCancelable(false);
        LinearLayout linearLayout = new LinearLayout(this.a);
        linearLayout.setOrientation(1);
        ImageView imageView = new ImageView(this.a);
        imageView.setImageResource(aa.a((Context) this.a, "drawable", MessageKey.MSG_ICON));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.leftMargin = 0;
        imageView.setLayoutParams(layoutParams);
        ImageView imageView2 = new ImageView(this.a);
        imageView2.setImageResource(aa.a((Context) this.a, "drawable", "bluep_logo_" + this.b.getCPPayType()));
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.gravity = 1;
        imageView2.setLayoutParams(layoutParams);
        EditText editText = new EditText(this.a);
        editText.setText(this.b.getDesMsisdn());
        new LinearLayout.LayoutParams(-1, -2).gravity = 1;
        editText.setLayoutParams(layoutParams2);
        Button unused = this.c.f = new Button(this.a);
        this.c.f.setText(i.a(i.T));
        if (s.e == null) {
            a unused2 = s.e = new a(60000, 1000, this.b.getActivity(), this.c.f);
            s.e.start();
        }
        LinearLayout linearLayout2 = new LinearLayout(this.a);
        linearLayout2.setOrientation(0);
        EditText unused3 = s.c = new EditText(this.a);
        s.c.setHint("please input verification code");
        linearLayout2.addView(editText);
        linearLayout2.addView(this.c.f);
        linearLayout.addView(imageView);
        linearLayout.addView(imageView2);
        linearLayout.addView(linearLayout2);
        linearLayout.addView(s.c);
        this.c.f.setOnClickListener(new x(this, editText));
        create.setView(linearLayout);
        create.setButton(-2, "Cancel", new y(this));
        create.setButton(-1, "OK", new z(this));
        create.show();
    }
}
