package com.bluepay.a.b;

import com.bluepay.data.Config;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.data.m;
import com.bluepay.pay.Client;
import com.bluepay.sdk.a.a;
import com.bluepay.sdk.c.d;
import com.bluepay.sdk.c.e;
import com.bluepay.sdk.exception.BlueException;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.HashMap;

/* compiled from: Proguard */
class aj implements Runnable {
    final /* synthetic */ b a;
    final /* synthetic */ ag b;

    aj(ag agVar, b bVar) {
        this.b = agVar;
        this.a = bVar;
    }

    public void run() {
        try {
            HashMap hashMap = new HashMap();
            hashMap.put("transactionid", this.a.getTransactionId());
            hashMap.put("productId", Integer.valueOf(this.a.getProductId()));
            hashMap.put("promotionId", this.a.getPromotionId());
            hashMap.put("propsName", this.a.getPropsName());
            hashMap.put("msisdn", this.a.getDesMsisdn());
            hashMap.put("provider", this.a.getCPPayType());
            hashMap.put(FirebaseAnalytics.Param.PRICE, Integer.valueOf(this.a.getPrice()));
            hashMap.put("customerId", this.a.getCustomId());
            hashMap.put("version", Integer.valueOf((int) Config.VERSION));
            String str = d.a(hashMap).toString();
            hashMap.put("encrypt", e.a(str + Client.getEncrypt()));
            if (this.b.a(a.a(this.a.getActivity(), m.w(), str, hashMap), this.a) == -1) {
                this.a.desc = "parse response error!";
                this.b.a.a(14, h.i, 0, this.a);
            }
        } catch (BlueException e) {
            e.printStackTrace();
            this.a.desc = e.getMessage();
            this.b.a.a(14, e.getCode(), 0, this.a);
        }
    }
}
