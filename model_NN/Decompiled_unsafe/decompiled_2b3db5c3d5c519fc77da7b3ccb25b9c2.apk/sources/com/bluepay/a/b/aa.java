package com.bluepay.a.b;

import com.bluepay.data.b;
import com.bluepay.data.m;
import com.bluepay.pay.Client;
import com.bluepay.sdk.a.a;
import com.bluepay.sdk.c.e;
import com.bluepay.sdk.exception.BlueException;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.HashMap;

/* compiled from: Proguard */
class aa implements Runnable {
    final /* synthetic */ b a;
    final /* synthetic */ String b;
    final /* synthetic */ s c;

    aa(s sVar, b bVar, String str) {
        this.c = sVar;
        this.a = bVar;
        this.b = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void */
    public void run() {
        com.bluepay.interfaceClass.b bVar;
        try {
            com.bluepay.sdk.c.aa.a(this.a.getActivity(), (CharSequence) "loading", (CharSequence) "");
            String a2 = m.a(Client.m_DcbInfo.b);
            HashMap hashMap = new HashMap();
            hashMap.put("msisdn", this.a.getDesMsisdn());
            hashMap.put("productid", this.a.getProductId() + "");
            hashMap.put(FirebaseAnalytics.Param.PRICE, Integer.valueOf(this.a.getPrice()));
            hashMap.put("promotionid", Client.getPromotionId());
            hashMap.put("transactionid", this.a.getTransactionId());
            hashMap.put("pingcode", this.b);
            String str = "msisdn=" + hashMap.get("msisdn").toString() + "&productid=" + hashMap.get("productid").toString() + "&price=" + this.a.getPrice() + "&promotionid=" + hashMap.get("promotionid").toString() + "&transactionid=" + hashMap.get("transactionid").toString() + "&pingcode=" + hashMap.get("pingcode");
            try {
                hashMap.put("encrypt", e.a(str + Client.getEncrypt()));
                bVar = a.a(this.a.getActivity(), a2, str, hashMap);
            } catch (Exception e) {
                e.printStackTrace();
                bVar = null;
            }
            this.c.a(bVar, this.a);
        } catch (BlueException e2) {
            this.a.desc = e2.getMessage();
            a.o.a(14, e2.getCode(), 0, this.a);
        }
    }
}
