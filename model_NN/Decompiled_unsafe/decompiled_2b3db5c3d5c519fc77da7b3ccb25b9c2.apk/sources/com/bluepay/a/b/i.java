package com.bluepay.a.b;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v4.internal.view.SupportMenu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.bluepay.data.Order;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.pay.BluePay;
import com.bluepay.pay.PublisherCode;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.c.aa;

/* compiled from: Proguard */
final class i implements View.OnClickListener {
    final /* synthetic */ Order a;
    final /* synthetic */ EditText b;
    final /* synthetic */ EditText c;
    final /* synthetic */ EditText d;
    final /* synthetic */ EditText e;
    final /* synthetic */ EditText f;
    final /* synthetic */ EditText g;
    final /* synthetic */ EditText h;
    final /* synthetic */ EditText i;
    final /* synthetic */ TextView j;
    final /* synthetic */ ImageView k;
    final /* synthetic */ Context l;
    final /* synthetic */ AlertDialog m;

    i(Order order, EditText editText, EditText editText2, EditText editText3, EditText editText4, EditText editText5, EditText editText6, EditText editText7, EditText editText8, TextView textView, ImageView imageView, Context context, AlertDialog alertDialog) {
        this.a = order;
        this.b = editText;
        this.c = editText2;
        this.d = editText3;
        this.e = editText4;
        this.f = editText5;
        this.g = editText6;
        this.h = editText7;
        this.i = editText8;
        this.j = textView;
        this.k = imageView;
        this.l = context;
        this.m = alertDialog;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void */
    public void onClick(View v) {
        String str = "";
        String str2 = "";
        try {
            if (this.a.getCPPayType().equals(PublisherCode.PUBLISHER_UNIPIN)) {
                if (!(this.b == null || this.c == null || this.d == null || this.e == null)) {
                    str2 = this.b.getText().toString().trim() + this.c.getText().toString().trim() + this.d.getText().toString().trim() + this.e.getText().toString().trim();
                }
            } else if (this.f != null) {
                str2 = this.a.getCPPayType().equals(PublisherCode.PUBLISHER_HAPPY) ? this.f.getText().toString().trim() + this.g.getText().toString().trim() : this.f.getText().toString().trim();
            }
            if (this.a.getCPPayType().equals(PublisherCode.PUBLISHER_UNIPIN)) {
                if (!(this.h == null || this.i == null)) {
                    str = "IDMB" + this.h.getText().toString().trim() + "S" + this.i.getText().toString().trim();
                }
            } else if (this.g != null) {
                str = this.g.getText().toString().trim();
            }
            if (this.a.getCPPayType().equals(PublisherCode.PUBLISHER_UNIPIN) && (this.h.getText().toString().trim() == null || this.h.getText().toString().trim().length() < 1 || this.i.getText().toString().trim() == null || this.i.getText().toString().trim().length() < 1)) {
                this.j.setText(com.bluepay.data.i.a((byte) 8));
                this.j.setTextColor((int) SupportMenu.CATEGORY_MASK);
                this.j.setVisibility(0);
            } else if (!a.n && (str == null || str.trim().length() < 1)) {
                this.j.setGravity(3);
                this.j.setText(com.bluepay.data.i.a((byte) 8));
                this.j.setVisibility(0);
                if (this.a.getCPPayType().equals(PublisherCode.PUBLISHER_12CALL)) {
                    this.k.setImageResource(aa.a(this.l, "drawable", "bluep_icon_one_calltip"));
                } else {
                    this.k.setImageResource(aa.a(this.l, "drawable", "bluep_icon_true_tip"));
                }
            } else if (!a.n || (str2 != null && str2.trim().length() >= 1)) {
                c.c(str);
                this.m.cancel();
                if (BluePay.getShowCardLoading()) {
                    aa.a(this.a.getActivity(), (CharSequence) "", (CharSequence) com.bluepay.data.i.a((byte) 6));
                }
                b bVar = new b(this.a);
                bVar.setCard(str2);
                if (str2 == null || a.n) {
                    bVar.setSerialNo("");
                } else if (this.a.getCPPayType().equals(PublisherCode.PUBLISHER_HAPPY)) {
                    bVar.setSerialNo("");
                } else {
                    bVar.setSerialNo(str);
                }
                bVar.a(5);
                c.c("confirm to pay");
                a.j.add(bVar);
                boolean unused = a.p = true;
                a.o.a(5, 0, 0, bVar);
            } else {
                this.j.setGravity(3);
                this.j.setText(com.bluepay.data.i.a((byte) 8));
                this.j.setTextColor((int) SupportMenu.CATEGORY_MASK);
                this.j.setVisibility(0);
                if (this.a.getCPPayType().equals(PublisherCode.PUBLISHER_12CALL)) {
                    this.k.setImageResource(aa.a(this.l, "drawable", "bluep_icon_one_calltip"));
                } else if (this.a.getCPPayType().equals(PublisherCode.PUBLISHER_HAPPY)) {
                    this.k.setImageResource(aa.a(this.l, "drawable", "bluep_icon_happy_tip"));
                } else if (this.a.getCPPayType().equals(PublisherCode.PUBLISHER_TRUEMONEY)) {
                    this.k.setImageResource(aa.a(this.l, "drawable", "bluep_icon_true_tip"));
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            b bVar2 = new b(this.a);
            bVar2.desc = "showCardDialog error";
            a.o.a(14, h.i, 0, bVar2);
        }
    }
}
