package com.bluepay.a.b;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bluepay.data.Config;
import com.bluepay.data.i;
import com.bluepay.sdk.c.aa;

/* compiled from: Proguard */
final class c implements Runnable {
    final /* synthetic */ Activity a;

    c(Activity activity) {
        this.a = activity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    public void run() {
        AlertDialog create = new AlertDialog.Builder(this.a).create();
        create.show();
        View inflate = LayoutInflater.from(this.a).inflate(aa.a((Context) this.a, "layout", "bluep_tips_offline"), (ViewGroup) null);
        ((LinearLayout) inflate.findViewById(aa.a((Context) this.a, "id", "linear3"))).setVisibility(8);
        ((LinearLayout) inflate.findViewById(aa.a((Context) this.a, "id", "linear4"))).setVisibility(8);
        Button button = (Button) inflate.findViewById(aa.a((Context) this.a, "id", "btn_offline_goto_tips"));
        Button button2 = (Button) inflate.findViewById(aa.a((Context) this.a, "id", "btn_offline_concel"));
        ((TextView) inflate.findViewById(aa.a((Context) this.a, "id", "textView2"))).setText("Error:");
        ((TextView) inflate.findViewById(aa.a((Context) this.a, "id", "textView1"))).setText("Url:");
        ((TextView) inflate.findViewById(aa.a((Context) this.a, "id", "tv_offline_paymentcode"))).setText(i.a(i.aj));
        ((TextView) inflate.findViewById(aa.a((Context) this.a, "id", "tv_offline_fee"))).setText(Config.URL_BIND_BANK_CARD);
        create.setContentView(inflate);
        button.setText(i.a(i.ak));
        button.setOnClickListener(new d(this, create));
        button2.setText(i.a((byte) 1));
        button2.setOnClickListener(new e(this, create));
    }
}
