package com.bluepay.a.b;

import android.content.Intent;
import android.text.TextUtils;
import com.bluepay.data.Config;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.data.i;
import com.bluepay.data.j;
import com.bluepay.data.m;
import com.bluepay.interfaceClass.a;
import com.bluepay.interfaceClass.c;
import com.bluepay.pay.Client;
import com.bluepay.pay.PublisherCode;
import com.bluepay.sdk.c.aa;
import com.bluepay.sdk.c.d;
import com.bluepay.sdk.c.e;
import com.bluepay.sdk.exception.BlueException;
import com.bluepay.ui.BankActivity;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.HashMap;

/* compiled from: Proguard */
public class ap extends a {
    c a;

    public ap(c cVar) {
        this.a = cVar;
    }

    public void a(b bVar) {
    }

    public void b(b bVar) {
        d(bVar);
    }

    public void c(b bVar) {
        d(bVar);
    }

    private void d(b bVar) {
        if (PublisherCode.PUBLISHER_VN_BANK.equals(bVar.getCPPayType()) || PublisherCode.PUBLISHER_ID_BANK.equals(bVar.getCPPayType())) {
            e(bVar);
            return;
        }
        HashMap hashMap = new HashMap();
        if (!TextUtils.isEmpty(Client.phoneNum())) {
        }
        hashMap.put("msisdn", bVar.getDesMsisdn());
        hashMap.put("productid", bVar.getProductId() + "");
        if (PublisherCode.PUBLISHER_LINE.equals(bVar.getCPPayType())) {
            hashMap.put(FirebaseAnalytics.Param.PRICE, Integer.valueOf(Integer.parseInt(bVar.getPrice() + "") / 100));
        } else {
            hashMap.put(FirebaseAnalytics.Param.PRICE, Float.valueOf(Float.parseFloat(bVar.getPrice() + "") / 100.0f));
        }
        String currency = bVar.getCurrency();
        if (currency.equals(Config.K_CURRENCY_TRF) && Client.CONTRY_CODE == 66) {
            currency = Config.K_CURRENCY_THB;
        } else if (currency.equals(Config.K_CURRENCY_TRF) && Client.CONTRY_CODE == 62) {
            currency = "IDR";
        } else if (currency.equals(Config.K_CURRENCY_TRF) && Client.CONTRY_CODE == 84) {
            currency = Config.K_CURRENCY_VND;
        } else if (currency.equals(Config.K_CURRENCY_TRF) && Client.CONTRY_CODE == 86) {
            currency = Config.K_CURRENCY_THB;
        }
        hashMap.put("promotionid", Client.getPromotionId());
        hashMap.put("transactionid", bVar.getTransactionId());
        hashMap.put(FirebaseAnalytics.Param.CURRENCY, currency);
        hashMap.put("propsName", bVar.getPropsName());
        hashMap.put("schema", bVar.getScheme());
        hashMap.put("version", Integer.valueOf((int) Config.VERSION));
        Client.scheme = bVar.getScheme();
        try {
            String a2 = d.a(hashMap);
            hashMap.put("encrypt", e.a(a2 + Client.getEncrypt()));
            a(com.bluepay.sdk.a.a.a(bVar.getActivity(), m.b(bVar.getCPPayType()), a2, hashMap), bVar);
        } catch (BlueException e) {
            e.printStackTrace();
            bVar.desc = "http error";
            a.o.a(14, h.i, 0, bVar);
        }
    }

    public int a(com.bluepay.interfaceClass.b bVar, b bVar2) {
        int a2 = bVar.a();
        com.bluepay.sdk.b.c.c("request code:" + a2);
        if (a2 == h.a) {
            try {
                aw b = au.b(bVar.b());
                int c = b.c("status");
                com.bluepay.sdk.b.c.c("status:" + c);
                if (c == h.a) {
                    bVar2.getActivity().runOnUiThread(new aq(this));
                    if (PublisherCode.PUBLISHER_LINE.equals(bVar2.getCPPayType())) {
                        a(bVar2, b);
                    }
                } else {
                    this.a.a(14, c, 0, bVar2);
                }
            } catch (BlueException e) {
                bVar2.desc = "wallet error:response result parse error";
                this.a.a(14, h.i, 0, bVar2);
            }
        } else {
            this.a.a(14, h.n, 0, bVar2);
        }
        return 0;
    }

    private void e(b bVar) {
        j jVar = new j(Client.getProductId(), Client.getPromotionId(), bVar.getTransactionId(), bVar.getCustomId(), String.valueOf(bVar.getPrice()), bVar.getSmsId(), bVar.getPropsName(), bVar.getCurrency(), Client.getOperator());
        jVar.j = bVar.getScheme();
        jVar.i(bVar.getCPPayType());
        Intent intent = new Intent();
        intent.setClass(bVar.getActivity(), BankActivity.class);
        intent.putExtra("entry", jVar);
        bVar.getActivity().startActivity(intent);
    }

    private void a(b bVar, aw awVar) {
        String b = awVar.b("info.paymentUrl.app");
        if (aa.a(bVar.getActivity(), "jp.naver.line.android", 230)) {
            bVar.getActivity().runOnUiThread(new ar(this, b, bVar));
            return;
        }
        com.bluepay.sdk.b.c.c("please install linepay");
        bVar.desc = i.a(i.Y);
        aa.a(bVar.getActivity(), bVar.desc);
        this.a.a(14, h.i, 0, bVar);
    }
}
