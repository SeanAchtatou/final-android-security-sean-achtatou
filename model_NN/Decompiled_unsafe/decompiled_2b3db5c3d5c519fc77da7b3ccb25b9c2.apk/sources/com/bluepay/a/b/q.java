package com.bluepay.a.b;

import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.data.i;
import com.bluepay.data.k;
import com.bluepay.data.m;
import com.bluepay.interfaceClass.a;
import com.bluepay.interfaceClass.c;
import com.bluepay.pay.Client;
import com.bluepay.pay.ClientHelper;
import com.bluepay.sdk.c.aa;
import com.bluepay.sdk.c.d;
import com.bluepay.sdk.c.v;
import com.tencent.android.tpush.common.Constants;
import java.util.HashMap;

/* compiled from: Proguard */
class q extends a {
    private c a;

    public q(c cVar) {
        this.a = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void */
    private void d(b bVar) {
        if (!Client.m_hashChargeList.containsKey(bVar.getPrice() + "")) {
            bVar.desc = "PRICE IS ERROR!";
            this.a.a(14, h.i, 0, bVar);
            return;
        }
        if (a.b() && bVar.isShowLoading2Dialog()) {
            aa.a(bVar.getActivity(), (CharSequence) "", (CharSequence) i.a((byte) 10, "5"));
        }
        String g = m.g();
        String transactionId = bVar.getTransactionId();
        String sMS_2_Content = bVar.getReference().getSMS_2_Content(transactionId);
        String desMsisdn = bVar.getDesMsisdn();
        String uid = Client.getUid();
        int smsId = bVar.getSmsId();
        String e = ((k) Client.m_hashChargeList.get(bVar.getPrice() + "")).e();
        String a2 = d.a(sMS_2_Content, transactionId, desMsisdn, uid, String.valueOf(smsId));
        HashMap hashMap = new HashMap();
        hashMap.put("referenceId", sMS_2_Content);
        hashMap.put("transactionId", transactionId);
        hashMap.put("msisdn", desMsisdn);
        hashMap.put(Constants.FLAG_DEVICE_ID, uid);
        hashMap.put("smsId", Integer.valueOf(smsId));
        hashMap.put("dest", e);
        hashMap.put("encrypt", a2);
        try {
            com.bluepay.sdk.a.a.a(g, hashMap);
            if (!a.b()) {
                return;
            }
            if (bVar.getCheckNum() < 1) {
                bVar.desc = i.a(i.r);
                this.a.a(14, h.a, 0, bVar);
                return;
            }
            this.a.a(14, h.k, 0, bVar);
        } catch (Exception e2) {
            this.a.a(14, h.i, 0, bVar);
        }
    }

    private void e(b bVar) {
        if (!v.a(bVar, ClientHelper.getPreContent(bVar.getPrice()))) {
            this.a.a(14, h.i, 0, bVar);
        }
    }

    public void a(b bVar) {
    }

    public void b(b bVar) {
        if (as.b) {
            d(bVar);
        } else {
            e(bVar);
        }
    }

    public void c(b bVar) {
        e(bVar);
    }
}
