package com.bluepay.a.b;

import android.app.Activity;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.data.m;
import com.bluepay.interfaceClass.a;
import com.bluepay.interfaceClass.c;
import com.bluepay.pay.Client;
import com.bluepay.sdk.c.aa;
import com.bluepay.sdk.c.e;
import com.bluepay.sdk.c.g;
import com.bluepay.sdk.exception.BlueException;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.LinkedHashMap;
import java.util.Map;

/* compiled from: Proguard */
public class s extends a {
    public static final String b = "android.provider.Telephony.SMS_RECEIVED";
    /* access modifiers changed from: private */
    public static EditText c;
    /* access modifiers changed from: private */
    public static com.bluepay.sdk.c.a e;
    c a;
    /* access modifiers changed from: private */
    public Button f;

    public s(c cVar) {
        this.a = cVar;
    }

    public void a(b bVar) {
    }

    public void b(b bVar) {
        d(bVar);
    }

    public void c(b bVar) {
        d(bVar);
    }

    private void d(b bVar) {
        if (!TextUtils.isEmpty(Client.phoneNum())) {
            bVar.setDesMsisdn(Client.phoneNum());
            f(bVar);
        } else if (Client.m_DcbInfo.p == 0) {
            aa.a(bVar.getActivity(), new t(this, bVar));
        } else {
            e(bVar);
        }
    }

    /* access modifiers changed from: private */
    public void e(b bVar) {
        g.a(bVar.getActivity(), bVar, this.a, "", new u(this, bVar));
    }

    /* access modifiers changed from: private */
    public void f(b bVar) {
        if (Client.m_DcbInfo.b == 4) {
            h(bVar);
        }
        if (Client.m_DcbInfo.b == 7) {
            g(bVar);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void */
    private void g(b bVar) {
        com.bluepay.sdk.b.c.c("request the dcb");
        if (bVar.getShowUI()) {
            aa.a(bVar.getActivity(), (CharSequence) "", (CharSequence) "");
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        com.bluepay.sdk.b.c.c("paytype:" + bVar.getCPPayType());
        int price = bVar.getPrice();
        linkedHashMap.put("productId", bVar.getProductId() + "");
        linkedHashMap.put(FirebaseAnalytics.Param.PRICE, Integer.valueOf(price));
        linkedHashMap.put("promotionId", bVar.getPromotionId());
        linkedHashMap.put("transactionId", bVar.getTransactionId());
        try {
            String a2 = m.a(7);
            linkedHashMap.put("imsi", Client.m_iIMSI);
            linkedHashMap.put("msisdn", bVar.getDesMsisdn());
            linkedHashMap.put("telco_name", Client.telcoName);
            linkedHashMap.put("encrypt", a(linkedHashMap, Client.getEncrypt()).toString());
            a(com.bluepay.sdk.a.a.a(bVar.getActivity(), a2, "productId=" + linkedHashMap.get("productId").toString() + "&price=" + linkedHashMap.get(FirebaseAnalytics.Param.PRICE).toString() + "&promotionId=" + linkedHashMap.get("promotionId").toString() + "&transactionId=" + linkedHashMap.get("transactionId").toString() + "&imsi=" + linkedHashMap.get("imsi").toString() + "&msisdn=" + linkedHashMap.get("msisdn") + "&telco_name=" + linkedHashMap.get("telco_name"), linkedHashMap), bVar);
        } catch (BlueException e2) {
            bVar.desc = e2.getMessage();
            this.a.a(14, e2.getCode(), 0, bVar);
            e2.printStackTrace();
            com.bluepay.sdk.b.c.b("the dcb is ERROR >>>> " + linkedHashMap.toString());
        } catch (Exception e3) {
            bVar.desc = e3.getMessage();
            this.a.a(14, h.i, 0, bVar);
            e3.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void */
    private void h(b bVar) {
        com.bluepay.sdk.b.c.c("request the dcb");
        if (bVar.getShowUI()) {
            aa.a(bVar.getActivity(), (CharSequence) "", (CharSequence) "");
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        com.bluepay.sdk.b.c.c("paytype:" + bVar.getCPPayType());
        linkedHashMap.put("productid", bVar.getProductId() + "");
        linkedHashMap.put(FirebaseAnalytics.Param.PRICE, Integer.valueOf(bVar.getPrice()));
        linkedHashMap.put("promotionid", bVar.getPromotionId());
        linkedHashMap.put("transactionid", bVar.getTransactionId());
        try {
            String a2 = m.a(4);
            String str = a(linkedHashMap, Client.getEncrypt()).toString();
            linkedHashMap.put("imsi", Client.m_iIMSI);
            linkedHashMap.put("msisdn", bVar.getDesMsisdn());
            linkedHashMap.put("encrypt", str);
            a(com.bluepay.sdk.a.a.a(bVar.getActivity(), a2, "imsi=" + linkedHashMap.get("imsi").toString() + "&msisdn=" + linkedHashMap.get("msisdn").toString() + "&productid=" + linkedHashMap.get("productid").toString() + "&price=" + linkedHashMap.get(FirebaseAnalytics.Param.PRICE).toString() + "&promotionid=" + linkedHashMap.get("promotionid").toString() + "&transactionid=" + linkedHashMap.get("transactionid"), linkedHashMap), bVar);
        } catch (Exception e2) {
            e2.printStackTrace();
            com.bluepay.sdk.b.c.b("the dcb is ERROR >>>> " + linkedHashMap.toString());
            bVar.desc = e2.getMessage();
            this.a.a(14, h.i, 0, bVar);
        }
    }

    public String a(Map map, String str) {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            for (String str2 : map.keySet()) {
                String valueOf = String.valueOf(map.get(str2));
                if (valueOf == null) {
                    valueOf = "";
                }
                stringBuffer.append(str2 + "=" + valueOf + "&");
            }
            String str3 = stringBuffer.toString().substring(0, stringBuffer.toString().length() - 1) + str;
            com.bluepay.sdk.b.c.c("encry:" + str3);
            return e.a(str3);
        } catch (Exception e2) {
            e2.printStackTrace();
            throw new Exception(e2);
        }
    }

    public int a(com.bluepay.interfaceClass.b bVar, b bVar2) {
        super.a(bVar, bVar2);
        return 0;
    }

    /* access modifiers changed from: private */
    public void a(b bVar, String str) {
        new Thread(new v(this, str, bVar)).start();
    }

    /* access modifiers changed from: private */
    public void a(Activity activity, b bVar) {
        if (activity == null || bVar == null) {
            try {
                throw new BlueException(h.i, "context is null or order is null", new Object());
            } catch (BlueException e2) {
                e2.printStackTrace();
                bVar.desc = e2.getMessage();
                this.a.a(14, h.i, 0, bVar);
            }
        }
        activity.runOnUiThread(new w(this, activity, bVar));
    }

    /* access modifiers changed from: private */
    public void b(b bVar, String str) {
        new Thread(new aa(this, bVar, str)).start();
    }
}
