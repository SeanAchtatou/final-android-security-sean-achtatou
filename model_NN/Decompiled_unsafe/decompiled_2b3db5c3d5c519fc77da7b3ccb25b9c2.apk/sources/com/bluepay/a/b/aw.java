package com.bluepay.a.b;

import com.bluepay.data.h;
import com.bluepay.sdk.exception.BlueException;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/* compiled from: Proguard */
public class aw {
    private final Map a;

    private aw(Map map) {
        this.a = map;
    }

    public HashMap a(String str, String str2, String str3) {
        Object obj = this.a.get(str);
        if (obj == null) {
            return new HashMap();
        }
        HashMap hashMap = new HashMap();
        hashMap.put("0", "0");
        try {
            JSONArray jSONArray = (JSONArray) obj;
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= jSONArray.length()) {
                    return hashMap;
                }
                JSONObject jSONObject = (JSONObject) new JSONTokener(jSONArray.get(i2).toString()).nextValue();
                hashMap.put(jSONObject.getString(str2), jSONObject.getString(str3));
                i = i2 + 1;
            }
        } catch (Exception e) {
            System.out.println("key: " + str + " keyStr:" + str2 + " valueStr:" + str3);
            return hashMap;
        }
    }

    public String a(String str) {
        Object obj = this.a.get(str);
        if (obj != null) {
            return (String) obj;
        }
        throw new BlueException(h.i, "key: " + str + " not exsits", new Object[0]);
    }

    public String b(String str) {
        Object obj = this.a.get(str);
        if (obj == null) {
            return null;
        }
        return (String) obj;
    }

    public int c(String str) {
        Object obj = this.a.get(str);
        if (obj != null) {
            return ((Integer) obj).intValue();
        }
        throw new BlueException(h.i, "key: " + str + " not exsits", new Object[0]);
    }

    public boolean d(String str) {
        Object obj = this.a.get(str);
        if (obj != null) {
            return ((Boolean) obj).booleanValue();
        }
        throw new BlueException(h.i, "key: " + str + " not exsits", new Object[0]);
    }

    public Object e(String str) {
        Object obj = this.a.get(str);
        if (obj != null) {
            return obj;
        }
        throw new BlueException(h.i, "key: " + str + " not exsits", new Object[0]);
    }

    public String toString() {
        return "MapResult [map=" + this.a + "]";
    }
}
