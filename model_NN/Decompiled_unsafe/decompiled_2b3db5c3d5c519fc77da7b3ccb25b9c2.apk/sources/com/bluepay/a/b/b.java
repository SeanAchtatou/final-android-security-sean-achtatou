package com.bluepay.a.b;

import android.app.Activity;
import com.bluepay.pay.BlueMessage;
import com.bluepay.pay.BluePay;

/* compiled from: Proguard */
final class b implements Runnable {
    final /* synthetic */ BlueMessage a;
    final /* synthetic */ Activity b;

    b(BlueMessage blueMessage, Activity activity) {
        this.a = blueMessage;
        this.b = activity;
    }

    public void run() {
        if (BluePay.getShowResult() && a.b(this.a)) {
            a.b(this.b, this.a);
        }
        if (a.q != null) {
            a.q.onFinished(this.a);
        }
    }
}
