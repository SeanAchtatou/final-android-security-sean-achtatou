package com.bluepay.a.b;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

/* compiled from: Proguard */
class al implements View.OnClickListener {
    final /* synthetic */ ak a;

    al(ak akVar) {
        this.a = akVar;
    }

    public void onClick(View v) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(a.d + "sdk_ui/offline/index_" + this.a.b + ".html?paymentcode=" + this.a.c + "&price=" + this.a.e));
        this.a.a.startActivity(intent);
    }
}
