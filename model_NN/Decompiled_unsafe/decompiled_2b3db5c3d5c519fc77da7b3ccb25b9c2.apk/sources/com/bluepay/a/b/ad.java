package com.bluepay.a.b;

import android.text.TextUtils;
import com.bluepay.data.Config;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.interfaceClass.d;
import com.bluepay.pay.Client;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.c.aa;

/* compiled from: Proguard */
class ad implements d {
    final /* synthetic */ b a;
    final /* synthetic */ ab b;

    ad(ab abVar, b bVar) {
        this.b = abVar;
        this.a = bVar;
    }

    public void a(int i, String str) {
        if (i == 1) {
            this.a.setDesMsisdn(str);
            String g = aa.g(str);
            c.c("------telcoName:" + g);
            if (!TextUtils.isEmpty(g)) {
                Client.telcoName = g;
            }
            if (Client.telcoName.equals(Config.TELCO_NAME_SMARTFREN)) {
                this.b.a(this.a, str);
            } else {
                this.b.b(this.a, str);
            }
        } else {
            this.b.a.a(14, h.C, 0, this.a);
        }
    }
}
