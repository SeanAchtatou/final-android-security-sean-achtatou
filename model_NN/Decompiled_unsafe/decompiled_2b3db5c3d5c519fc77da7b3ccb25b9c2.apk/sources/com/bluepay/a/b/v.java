package com.bluepay.a.b;

import com.bluepay.data.Config;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.data.m;
import com.bluepay.pay.Client;
import com.bluepay.sdk.a.a;
import com.bluepay.sdk.c.aa;
import com.bluepay.sdk.c.e;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.HashMap;

/* compiled from: Proguard */
class v implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ b b;
    final /* synthetic */ s c;

    v(s sVar, String str, b bVar) {
        this.c = sVar;
        this.a = str;
        this.b = bVar;
    }

    public void run() {
        String b2 = m.b(5);
        HashMap hashMap = new HashMap();
        hashMap.put("msisdn", this.a);
        hashMap.put("productid", this.b.getProductId() + "");
        hashMap.put(FirebaseAnalytics.Param.PRICE, Integer.valueOf(this.b.getPrice()));
        hashMap.put("promotionid", Client.getPromotionId());
        hashMap.put("transactionid", this.b.getTransactionId());
        hashMap.put("version", Integer.valueOf((int) Config.VERSION));
        String str = "msisdn=" + hashMap.get("msisdn").toString() + "&productid=" + hashMap.get("productid").toString() + "&price=" + this.b.getPrice() + "&promotionid=" + hashMap.get("promotionid").toString() + "&transactionId=" + hashMap.get("transactionid") + "&version=" + hashMap.get("version");
        try {
            hashMap.put("encrypt", e.a(str));
            com.bluepay.interfaceClass.b a2 = a.a(this.b.getActivity(), b2, str, hashMap);
            if (a2.a() == 200) {
                int c2 = au.b(a2.b()).c("status");
                if (c2 == h.a) {
                    aa.d();
                    this.c.a(this.b.getActivity(), this.b);
                    return;
                }
                this.c.a.a(14, c2, 0, this.b);
                return;
            }
            this.b.desc = "net work is error.Error code:" + a2.a();
            this.c.a.a(14, h.i, 0, this.b);
        } catch (Exception e) {
            e.printStackTrace();
            this.b.desc = e.getMessage();
            this.c.a.a(14, h.i, 0, this.b);
        }
    }
}
