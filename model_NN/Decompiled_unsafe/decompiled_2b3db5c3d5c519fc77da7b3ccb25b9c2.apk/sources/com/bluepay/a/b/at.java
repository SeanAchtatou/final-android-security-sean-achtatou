package com.bluepay.a.b;

import android.app.Activity;
import android.os.AsyncTask;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.data.i;
import com.bluepay.interfaceClass.a;
import com.bluepay.interfaceClass.c;
import com.bluepay.sdk.c.f;
import com.bluepay.sdk.exception.BlueException;

/* compiled from: Proguard */
class at extends AsyncTask {
    private at() {
    }

    protected static AsyncTask a(b bVar, String str, c cVar) {
        Object abVar;
        at atVar = new at();
        switch (bVar.a()) {
            case 0:
            case 8:
                abVar = new an(cVar);
                break;
            case 1:
                abVar = new q(cVar);
                break;
            case 2:
            case 3:
            case 4:
            case 10:
            default:
                abVar = new ao(cVar);
                break;
            case 5:
                abVar = new r(cVar);
                break;
            case 6:
                abVar = new s(cVar);
                break;
            case 7:
                abVar = new ap(cVar);
                break;
            case 9:
                abVar = new ag(cVar);
                break;
            case 11:
                abVar = new ab(cVar);
                break;
        }
        if (abVar != null) {
            atVar.execute(abVar, bVar, cVar);
        }
        return atVar;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Object... objArr) {
        int i;
        a aVar = (a) objArr[0];
        b bVar = (b) objArr[1];
        com.bluepay.sdk.b.c.c("doPay:" + bVar.getPrice());
        c cVar = (c) objArr[2];
        if (bVar.a() == 5) {
            i = 14;
        } else {
            i = 14;
        }
        try {
            Activity activity = bVar.getActivity();
            if (f.c(activity)) {
                aVar.b(bVar);
                return null;
            } else if (f.b(activity)) {
                aVar.c(bVar);
                return null;
            } else {
                bVar.desc = i.a(i.t);
                cVar.a(i, h.n, 0, bVar);
                com.bluepay.sdk.b.c.b("网络错误");
                return null;
            }
        } catch (BlueException e) {
            bVar.desc = e.getLocalizedMessage();
            cVar.a(14, h.i, 0, bVar);
            com.bluepay.sdk.b.c.b(e.getMessage());
            return null;
        } catch (Exception e2) {
            bVar.desc = e2.getLocalizedMessage();
            cVar.a(14, h.i, 0, bVar);
            e2.printStackTrace();
            return null;
        }
    }
}
