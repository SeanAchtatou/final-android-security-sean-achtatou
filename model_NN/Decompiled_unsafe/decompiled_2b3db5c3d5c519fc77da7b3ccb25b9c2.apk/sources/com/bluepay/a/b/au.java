package com.bluepay.a.b;

import com.bluepay.data.h;
import com.bluepay.sdk.exception.BlueException;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/* compiled from: Proguard */
public class au {
    public static boolean a(String str) {
        if (str == null) {
            return false;
        }
        c(str);
        return ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equalsIgnoreCase(str.trim());
    }

    public static aw b(String str) {
        HashMap hashMap = new HashMap();
        c(str);
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(str).nextValue();
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap.put(next, jSONObject.get(next));
            }
            return new aw(hashMap);
        } catch (JSONException e) {
            StackTraceElement[] stackTrace = e.getStackTrace();
            StringBuilder sb = new StringBuilder();
            for (StackTraceElement stackTraceElement : stackTrace) {
                sb.append(stackTraceElement.toString()).append("\n");
            }
            throw new BlueException(h.i, sb.toString(), new Object[0]);
        } catch (Exception e2) {
            StackTraceElement[] stackTrace2 = e2.getStackTrace();
            StringBuilder sb2 = new StringBuilder();
            for (StackTraceElement stackTraceElement2 : stackTrace2) {
                sb2.append(stackTraceElement2.toString()).append("\n");
            }
            throw new BlueException(h.i, sb2.toString(), new Object[0]);
        }
    }

    private static void c(String str) {
        JSONObject jSONObject;
        if (str != null && str.contains("error") && str.contains("code") && str.contains(ShareConstants.WEB_DIALOG_PARAM_MESSAGE)) {
            try {
                JSONObject jSONObject2 = (JSONObject) new JSONTokener(str).nextValue();
                if (jSONObject2.has("error") && (jSONObject = jSONObject2.getJSONObject("error")) != null) {
                    throw new BlueException(jSONObject.getInt("code"), jSONObject.getString(ShareConstants.WEB_DIALOG_PARAM_MESSAGE), new Object[0]);
                }
            } catch (JSONException e) {
                StackTraceElement[] stackTrace = e.getStackTrace();
                StringBuilder sb = new StringBuilder();
                for (StackTraceElement stackTraceElement : stackTrace) {
                    sb.append(stackTraceElement.toString()).append("\n");
                }
                throw new BlueException(h.i, " e: " + sb.toString(), new Object[0]);
            } catch (BlueException e2) {
                StackTraceElement[] stackTrace2 = e2.getStackTrace();
                StringBuilder sb2 = new StringBuilder();
                for (StackTraceElement stackTraceElement2 : stackTrace2) {
                    sb2.append(stackTraceElement2.toString()).append("\n");
                }
                throw new BlueException(e2.getCode(), sb2.toString(), new Object[0]);
            } catch (Exception e3) {
                StackTraceElement[] stackTrace3 = e3.getStackTrace();
                StringBuilder sb3 = new StringBuilder();
                for (StackTraceElement stackTraceElement3 : stackTrace3) {
                    sb3.append(stackTraceElement3.toString()).append("\n");
                }
                throw new BlueException(h.i, sb3.toString(), new Object[0]);
            }
        }
    }
}
