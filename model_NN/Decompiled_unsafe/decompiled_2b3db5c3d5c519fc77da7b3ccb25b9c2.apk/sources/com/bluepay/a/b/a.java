package com.bluepay.a.b;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bluepay.data.Config;
import com.bluepay.data.Order;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.data.i;
import com.bluepay.data.m;
import com.bluepay.pay.BlueMessage;
import com.bluepay.pay.BluePay;
import com.bluepay.pay.Client;
import com.bluepay.pay.ClientHelper;
import com.bluepay.pay.IPayCallback;
import com.bluepay.pay.PublisherCode;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.c.aa;
import com.bluepay.sdk.exception.BlueException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/* compiled from: Proguard */
public class a {
    public static boolean a = false;
    public static boolean b = false;
    public static String c = null;
    public static String d = null;
    public static String e = null;
    public static String f = null;
    public static int g = 0;
    public static String h = null;
    public static Map i = null;
    public static Vector j = new Vector();
    static HashMap k = new HashMap();
    public static int[] l = null;
    public static int m = 0;
    static boolean n = false;
    public static n o = new n();
    /* access modifiers changed from: private */
    public static boolean p = false;
    /* access modifiers changed from: private */
    public static IPayCallback q = null;
    private static final String r = "BluePay_statUrl";
    private static final String s = "BluePay_apiUrl";
    private static final String t = "BluePay_verionTime";

    public static void a(IPayCallback iPayCallback) {
        q = iPayCallback;
    }

    public static IPayCallback a() {
        return q;
    }

    public static boolean b() {
        return p;
    }

    private static void a(Activity activity, BlueMessage blueMessage, boolean z) {
        if (activity == null) {
            c.c("activity null");
            throw new RuntimeException("run on ui error,activity == null");
        } else {
            activity.runOnUiThread(new b(blueMessage, activity));
        }
    }

    /* access modifiers changed from: private */
    public static boolean b(BlueMessage blueMessage) {
        if (blueMessage.getCode() == 603 || blueMessage.getPublisher().equalsIgnoreCase(PublisherCode.PUBLISHER_OFFLINE_ATM) || blueMessage.getPublisher().equalsIgnoreCase(PublisherCode.PUBLISHER_OFFLINE_OTC)) {
            return false;
        }
        if (!blueMessage.getPublisher().equalsIgnoreCase(PublisherCode.PUBLISHER_LINE) || blueMessage.getCode() != 201) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void */
    public static void a(int i2, int i3, Order order) {
        c.c("checkPreState");
        try {
            b bVar = new b(order);
            bVar.a(order.getTransactionId());
            p = true;
            if (order.getShowUI()) {
                aa.a(order.getActivity(), (CharSequence) "", (CharSequence) i.a((byte) 6));
            }
            switch (i2) {
                case 0:
                    if (order.getOperator() == 3) {
                        bVar.a(1);
                    } else {
                        bVar.a(0);
                    }
                    if (order.getCPPayType().equals(PublisherCode.PUBLISHER_BANK)) {
                        bVar.a(8);
                    }
                    j.add(bVar);
                    break;
                case 3:
                    bVar.a(5);
                    String card = order.getCard();
                    if (card != null && card.trim().length() >= 1) {
                        if (BluePay.getShowCardLoading()) {
                            aa.a(order.getActivity(), (CharSequence) "", (CharSequence) i.a((byte) 6));
                        }
                        j.add(bVar);
                        break;
                    } else {
                        order.getActivity().runOnUiThread(new f(order));
                        return;
                    }
                    break;
                case 5:
                    bVar.a(6);
                    j.add(bVar);
                    break;
                case 6:
                    bVar.a(7);
                    j.add(bVar);
                    break;
                case 7:
                    bVar.a(9);
                    j.add(bVar);
                    break;
                case 8:
                    bVar.a(11);
                    j.add(bVar);
                    break;
            }
            o.a(5, i2, 0, bVar);
        } catch (Exception e2) {
            e2.printStackTrace();
            c.b("error:checkPreState():");
            throw new BlueException(h.h, "error:checkPreState()", e2);
        } catch (BlueException e3) {
            e3.printStackTrace();
        }
    }

    public static boolean a(String str, Order order) {
        if (k.containsKey(str)) {
            return false;
        }
        k.put(str, order);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    /* access modifiers changed from: private */
    @SuppressLint({"DefaultLocale"})
    public static void b(Order order) {
        try {
            order.setShowUI(true);
            Activity activity = order.getActivity();
            AlertDialog create = new AlertDialog.Builder(activity).create();
            create.show();
            o oVar = new o(order.getActivity(), create);
            oVar.a();
            create.setOnDismissListener(new g(oVar));
            if (BluePay.getLandscape()) {
                if (order.getCPPayType().equals(PublisherCode.PUBLISHER_HAPPY)) {
                    create.getWindow().setContentView(aa.a((Context) activity, "layout", "bluep_by_otc_landscape_happy"));
                } else {
                    create.getWindow().setContentView(aa.a((Context) activity, "layout", "bluep_by_otc_landscape"));
                }
            } else if (order.getCPPayType().equals(PublisherCode.PUBLISHER_HAPPY)) {
                create.getWindow().setContentView(aa.a((Context) activity, "layout", "bluep_pay_otc_portrait_happy"));
            } else {
                create.getWindow().setContentView(aa.a((Context) activity, "layout", "bluep_by_otc_portrait"));
            }
            EditText editText = (EditText) create.getWindow().findViewById(aa.a((Context) activity, "id", "payEdit"));
            TextView textView = (TextView) create.getWindow().findViewById(aa.a((Context) activity, "id", "payTip"));
            Button button = (Button) create.getWindow().findViewById(aa.a((Context) activity, "id", "paybyYes"));
            ImageView imageView = (ImageView) create.getWindow().findViewById(aa.a((Context) activity, "id", "paybyNo"));
            ImageView imageView2 = (ImageView) create.getWindow().findViewById(aa.a((Context) activity, "id", "tipIv"));
            TextView textView2 = (TextView) create.getWindow().findViewById(aa.a((Context) activity, "id", "Bluep_paySerialNoTip"));
            EditText editText2 = (EditText) create.getWindow().findViewById(aa.a((Context) activity, "id", "Bluep_paySerialNoEdit"));
            LinearLayout linearLayout = (LinearLayout) create.getWindow().findViewById(aa.a((Context) activity, "id", "rl_CardNo_for_unipin"));
            LinearLayout linearLayout2 = (LinearLayout) create.getWindow().findViewById(aa.a((Context) activity, "id", "rl_serialNo_for_unipin"));
            EditText editText3 = (EditText) create.getWindow().findViewById(aa.a((Context) activity, "id", "et_unipin_first_input"));
            EditText editText4 = (EditText) create.getWindow().findViewById(aa.a((Context) activity, "id", "et_unipin_last_input"));
            EditText editText5 = (EditText) create.getWindow().findViewById(aa.a((Context) activity, "id", "et_Seral_first"));
            EditText editText6 = (EditText) create.getWindow().findViewById(aa.a((Context) activity, "id", "et_Seral_second"));
            EditText editText7 = (EditText) create.getWindow().findViewById(aa.a((Context) activity, "id", "et_Seral_third"));
            EditText editText8 = (EditText) create.getWindow().findViewById(aa.a((Context) activity, "id", "et_Seral_fourth"));
            editText3.addTextChangedListener(new p(editText3, editText4, order.getActivity()));
            editText4.addTextChangedListener(new p(editText4, button, order.getActivity()));
            editText5.addTextChangedListener(new p(editText5, editText6, order.getActivity()));
            editText6.addTextChangedListener(new p(editText6, editText7, order.getActivity()));
            editText7.addTextChangedListener(new p(editText7, editText8, order.getActivity()));
            editText8.addTextChangedListener(new p(editText8, editText3, order.getActivity()));
            button.setText(i.a((byte) 0));
            imageView2.setImageResource(aa.a((Context) activity, "drawable", "bluep_logo_" + order.getCPPayType().toLowerCase()));
            if (order.getCPPayType().equals(PublisherCode.PUBLISHER_12CALL) || order.getCPPayType().equals(PublisherCode.PUBLISHER_TRUEMONEY) || order.getCPPayType().equals(PublisherCode.PUBLISHER_MOGPLAY) || order.getCPPayType().equals(PublisherCode.PUBLISHER_LYTOCARD)) {
                textView2.setVisibility(8);
                n = true;
                editText.setHint(i.a(i.H));
                editText2.setVisibility(8);
                textView.setTextSize(13.0f);
                textView.setSingleLine(false);
                textView.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
                if (order.getCPPayType().equals(PublisherCode.PUBLISHER_MOGPLAY)) {
                    textView.setVisibility(0);
                    textView.setText("Note : Kamu dapat membeli voucher MOGPLAY di Alfamart dan 7-11 (Seven Eleven)");
                } else if (order.getCPPayType().equals(PublisherCode.PUBLISHER_LYTOCARD)) {
                    textView.setVisibility(0);
                    textView.setText("Note : Kamu dapat membeli voucher game on melalui warnet dan indomaret terdekat");
                }
            } else if (order.getCPPayType().equals(PublisherCode.PUBLISHER_BLUECOIN)) {
                textView2.setVisibility(8);
                n = true;
                editText.setHint(i.a(i.H));
                editText2.setVisibility(8);
                if (Client.m_BlueWallet.d == 1) {
                    textView.setVisibility(0);
                    textView.setTextColor(-16776961);
                    textView.setGravity(5);
                    textView.setText(i.a(i.aP));
                    textView.setOnClickListener(new h(textView, order));
                } else {
                    textView.setVisibility(8);
                }
            } else if (order.getCPPayType().equals(PublisherCode.PUBLISHER_UNIPIN)) {
                editText.setVisibility(8);
                editText2.setVisibility(8);
                n = false;
                linearLayout.setVisibility(0);
                linearLayout2.setVisibility(0);
            } else if (order.getCPPayType().equals(PublisherCode.PUBLISHER_HAPPY)) {
                n = false;
                editText2.setVisibility(0);
            } else {
                n = false;
                editText.setHint(i.a(i.H));
                editText2.setHint(i.a(i.al));
                textView2.setVisibility(0);
                editText2.setVisibility(0);
            }
            create.getWindow().clearFlags(131080);
            create.setCancelable(false);
            button.setOnClickListener(new i(order, editText5, editText6, editText7, editText8, editText, editText2, editText3, editText4, textView, imageView2, activity, create));
            imageView.setOnClickListener(new j(order, create));
        } catch (Exception e2) {
            e2.printStackTrace();
            b bVar = new b(order);
            bVar.desc = "showCardDialog error";
            o.a(14, h.i, 0, bVar);
        }
    }

    public static void a(Activity activity, String str, String str2) {
        if (d != null && d.length() >= 3) {
            activity.runOnUiThread(new k(activity, str, str2));
        }
    }

    public static void a(Activity activity) {
        try {
            com.bluepay.sdk.b.b.a(activity, Client.TAG);
            d = com.bluepay.sdk.b.b.b(r, m.r());
            c = com.bluepay.sdk.b.b.b(s, Config.getCacheIp());
            g = com.bluepay.sdk.b.b.b(t, 0);
            if (Math.abs(g - ClientHelper.generateSystemTime()) > 500) {
                g = 0;
                d = m.r();
                c = Config.getCacheIp();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            d = null;
            c = null;
        }
    }

    public static int a(String str, int i2) {
        if (k.containsKey(str)) {
            return ((Order) k.get(str)).getPrice();
        }
        return i2;
    }

    public static void a(int i2, b bVar) {
        int price;
        c.c("handlerCallback");
        bVar.getActivity().runOnUiThread(new l());
        BlueMessage blueMessage = new BlueMessage(i2, bVar.getTransactionId(), bVar.getPropsName(), bVar.getPrice() + "", bVar.getCPPayType());
        blueMessage.setOfflinePaymentCode(bVar.j());
        if (bVar.getCPPayType().equals(b.a)) {
            blueMessage.setPublisher(PublisherCode.PUBLISHER_SMS);
        }
        Order order = (Order) k.remove(bVar.g());
        if (order != null) {
            StringBuilder sb = new StringBuilder();
            if (order.getPrice() == 0) {
                price = bVar.getPrice();
            } else {
                price = order.getPrice();
            }
            blueMessage.setPrice(sb.append(price).append("").toString());
        } else {
            blueMessage.setPrice(bVar.getPrice() + "");
        }
        if (i2 == h.i) {
            blueMessage.setDesc(bVar.desc);
        } else if (i2 == h.b) {
            blueMessage.setDesc(bVar.desc);
        } else if (i2 == h.d) {
            blueMessage.setDesc(bVar.desc);
        } else if (i2 == h.y) {
            bVar.desc = i.a(i.aj) + " " + Config.URL_BIND_BANK_CARD;
            blueMessage.setDesc(bVar.desc);
            if (bVar.getShowUI()) {
                b(bVar.getActivity());
            }
        } else {
            blueMessage.setDesc(h.a(i2));
        }
        if (TextUtils.isEmpty(blueMessage.getDesc())) {
            blueMessage.setDesc(h.a(i2));
        }
        if (Client.m_uploadErrorCode.contains(Integer.valueOf(i2))) {
            String str = "ER" + blueMessage.getCode() + "|" + (TextUtils.isEmpty(bVar.desc) ? blueMessage.getDesc() : bVar.desc);
            String str2 = Client.m_iIMSI;
            if (!aa.a()) {
                str2 = "double sim|imsi1:" + Client.m_iIMSI1 + "|imsi2:" + Client.m_iIMSI2 + "|send_imsi:" + Client.m_iIMSI;
            }
            aa.b(bVar.getActivity(), bVar.getTransactionId(), str, str2, 13);
        }
        Log.i(Client.TAG, bVar.getOperator() + " result:code" + i2 + ", msg:" + bVar.desc + " price :" + bVar.getPrice());
        a(bVar.getActivity(), blueMessage, bVar.getShowUI());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    /* access modifiers changed from: private */
    public static void b(Activity activity, BlueMessage blueMessage) {
        String str;
        String str2;
        AlertDialog create = new AlertDialog.Builder(activity).create();
        create.show();
        Display defaultDisplay = activity.getWindowManager().getDefaultDisplay();
        WindowManager.LayoutParams attributes = create.getWindow().getAttributes();
        if (BluePay.getLandscape()) {
            attributes.width = (int) (((double) defaultDisplay.getWidth()) * 0.6d);
        } else {
            attributes.width = (int) (((double) defaultDisplay.getWidth()) * 0.8d);
        }
        attributes.height = -2;
        create.getWindow().setAttributes(attributes);
        View inflate = LayoutInflater.from(activity).inflate(aa.a((Context) activity, "layout", "bluep_pay_result"), (ViewGroup) null);
        ImageView imageView = (ImageView) inflate.findViewById(aa.a((Context) activity, "id", "iv_pay_result"));
        TextView textView = (TextView) inflate.findViewById(aa.a((Context) activity, "id", "tv_title"));
        TextView textView2 = (TextView) inflate.findViewById(aa.a((Context) activity, "id", "textView1"));
        TextView textView3 = (TextView) inflate.findViewById(aa.a((Context) activity, "id", "textView2"));
        TextView textView4 = (TextView) inflate.findViewById(aa.a((Context) activity, "id", "tv_payment_price"));
        TextView textView5 = (TextView) inflate.findViewById(aa.a((Context) activity, "id", "tv_payment_channel"));
        TextView textView6 = (TextView) inflate.findViewById(aa.a((Context) activity, "id", "tv_payment_desc"));
        Button button = (Button) inflate.findViewById(aa.a((Context) activity, "id", "btn_ok"));
        if (blueMessage.getCode() == 200) {
            String a2 = i.a(i.at);
            imageView.setImageResource(aa.a((Context) activity, "drawable", "bluep_icon_success"));
            textView.setTextColor(Color.parseColor("#57DE40"));
            str = a2;
        } else if (blueMessage.getCode() == 201) {
            String a3 = i.a(i.au);
            imageView.setImageResource(aa.a((Context) activity, "drawable", "bluep_icon_success"));
            textView.setTextColor(Color.parseColor("#57DE40"));
            str = a3;
        } else if (blueMessage.getCode() == 603) {
            String a4 = i.a(i.y);
            imageView.setImageResource(aa.a((Context) activity, "drawable", "bluep_icon_fail"));
            textView.setTextColor(Color.parseColor("#FF7E7E"));
            str = a4;
        } else {
            String a5 = i.a(i.av);
            imageView.setImageResource(aa.a((Context) activity, "drawable", "bluep_icon_fail"));
            textView.setTextColor(Color.parseColor("#FF7E7E"));
            str = a5;
        }
        textView.setText(str);
        textView2.setText(i.a(i.aR));
        textView3.setText(i.a(i.aS));
        String publisher = blueMessage.getPublisher();
        char c2 = 'V';
        if (PublisherCode.PUBLISHER_12CALL.equals(publisher) || PublisherCode.PUBLISHER_TRUEMONEY.equals(publisher) || PublisherCode.PUBLISHER_HAPPY.equals(publisher) || PublisherCode.PUBLISHER_BLUECOIN.equals(publisher) || PublisherCode.PUBLISHER_LINE.equals(publisher) || PublisherCode.PUBLISHER_BANK.equals(publisher)) {
            c2 = 'B';
        } else if (PublisherCode.PUBLISHER_MOGPLAY.equals(publisher) || PublisherCode.PUBLISHER_LYTOCARD.equals(publisher) || PublisherCode.PUBLISHER_OFFLINE_ATM.equals(publisher) || PublisherCode.PUBLISHER_OFFLINE_OTC.equals(publisher) || PublisherCode.PUBLISHER_ID_BANK.equals(publisher)) {
            c2 = '>';
        } else if (PublisherCode.PUBLISHER_MOBIFONE.equals(publisher) || PublisherCode.PUBLISHER_VINAPHONE.equals(publisher) || PublisherCode.PUBLISHER_VIETTEL.equals(publisher) || PublisherCode.PUBLISHER_VTC.equals(publisher) || PublisherCode.PUBLISHER_VN_BANK.equals(publisher)) {
            c2 = 'T';
        }
        blueMessage.getPrice();
        if (Client.CONTRY_CODE == 66 || c2 == 'B' || Client.CONTRY_CODE == 86) {
            str2 = (Integer.parseInt(TextUtils.isEmpty(blueMessage.getPrice()) ? "0" : blueMessage.getPrice()) / 100) + Config.K_CURRENCY_THB;
        } else if (Client.CONTRY_CODE == 62 || c2 == '>') {
            str2 = blueMessage.getPrice() + "IDR";
        } else if (Client.CONTRY_CODE == 84 || c2 == 'T') {
            str2 = blueMessage.getPrice() + Config.K_CURRENCY_VND;
        } else {
            str2 = (Integer.parseInt(TextUtils.isEmpty(blueMessage.getPrice()) ? "0" : blueMessage.getPrice()) / 100) + Config.K_CURRENCY_THB;
        }
        textView4.setText(str2);
        textView5.setText(blueMessage.getPublisher());
        textView6.setText(blueMessage.getDesc());
        create.setContentView(inflate);
        create.setCanceledOnTouchOutside(false);
        button.setText(i.a((byte) 0));
        button.setOnClickListener(new m(create));
    }

    private static void b(Activity activity) {
        activity.runOnUiThread(new c(activity));
    }
}
