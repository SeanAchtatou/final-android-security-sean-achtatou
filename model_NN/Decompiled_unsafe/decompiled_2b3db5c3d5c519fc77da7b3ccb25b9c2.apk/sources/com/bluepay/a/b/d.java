package com.bluepay.a.b;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.bluepay.data.Config;

/* compiled from: Proguard */
class d implements View.OnClickListener {
    final /* synthetic */ AlertDialog a;
    final /* synthetic */ c b;

    d(c cVar, AlertDialog alertDialog) {
        this.b = cVar;
        this.a = alertDialog;
    }

    public void onClick(View v) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setData(Uri.parse(Config.URL_BIND_BANK_CARD));
        this.b.a.startActivity(intent);
        this.a.dismiss();
    }
}
