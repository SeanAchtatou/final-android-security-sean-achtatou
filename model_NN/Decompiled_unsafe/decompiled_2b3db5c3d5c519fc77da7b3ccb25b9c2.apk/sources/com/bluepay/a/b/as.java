package com.bluepay.a.b;

import android.os.AsyncTask;
import com.bluepay.data.b;
import com.bluepay.interfaceClass.c;

/* compiled from: Proguard */
public class as {
    public static int a = 500;
    public static boolean b = true;
    private static AsyncTask c;

    protected static boolean a(AsyncTask asyncTask) {
        if (!b(asyncTask)) {
            return false;
        }
        c = null;
        return true;
    }

    protected static boolean b(AsyncTask asyncTask) {
        return asyncTask == a();
    }

    protected static AsyncTask a() {
        if (c == null || c.isCancelled()) {
            return null;
        }
        return c;
    }

    protected static void c(AsyncTask asyncTask) {
        b();
        c = asyncTask;
    }

    protected static void b() {
        if (c != null) {
            if (!c.isCancelled()) {
                c.cancel(true);
            }
            c = null;
        }
    }

    public static void a(c cVar) {
        if (a.j.size() != 0) {
            if (at.a((b) a.j.firstElement(), "", cVar) == null) {
                com.bluepay.sdk.b.c.b("Sdk error");
            }
            a.j.remove(0);
        }
    }
}
