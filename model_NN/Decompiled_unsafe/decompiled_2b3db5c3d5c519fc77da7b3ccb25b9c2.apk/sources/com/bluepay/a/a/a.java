package com.bluepay.a.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* compiled from: Proguard */
public class a extends SQLiteOpenHelper {
    public a(Context context) {
        super(context, "bluepay.db", (SQLiteDatabase.CursorFactory) null, 5);
    }

    private void a(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.execSQL("create table if not exists transRecord (_id integer primary key autoincrement, transactionId varchar(100), desc varchar(500), productId varchar(8), promotinId varchar(4), imsi varchar(15), imei varchar(15), timestamp varchar(14), sdkVersion varchar(9), romVersion integer, payType integer, uploadflag varchar(1));");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onCreate(SQLiteDatabase database) {
        a(database);
    }

    public void onUpgrade(SQLiteDatabase database, int paramInt1, int paramInt2) {
        b(database);
        a(database);
    }

    private void b(SQLiteDatabase sQLiteDatabase) {
        try {
            sQLiteDatabase.execSQL("drop table if exists  transRecord");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
