package com.bluepay.a.a;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.bluepay.data.Config;
import com.bluepay.data.n;
import com.bluepay.pay.Client;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.c.aa;
import com.facebook.appevents.AppEventsConstants;
import java.util.ArrayList;
import java.util.List;

/* compiled from: Proguard */
public class b {
    private static String d = "transRecord";
    private a a;
    private SQLiteDatabase b = this.a.getWritableDatabase();
    private Context c;

    public b(Context context) {
        this.a = new a(context);
        this.c = context;
    }

    public boolean a(String str, String str2, String str3, String str4, int i) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("transactionId", str);
        contentValues.put("desc", str2);
        contentValues.put("productId", Integer.valueOf(Client.getProductId()));
        contentValues.put("promotinId", Client.getPromotionId());
        contentValues.put("imsi", str3);
        contentValues.put("imei", aa.d(this.c));
        contentValues.put("timestamp", str4);
        contentValues.put("sdkVersion", Integer.valueOf((int) Config.VERSION));
        contentValues.put("romVersion", Integer.valueOf(aa.c()));
        contentValues.put("payType", Integer.valueOf(i));
        contentValues.put("uploadflag", AppEventsConstants.EVENT_PARAM_VALUE_YES);
        if (this.b.insert(d, null, contentValues) == -1) {
            return false;
        }
        c.c("create a trans");
        this.b.close();
        return true;
    }

    public List a() {
        ArrayList arrayList = new ArrayList();
        Cursor query = this.b.query(d, null, "uploadflag=?", new String[]{AppEventsConstants.EVENT_PARAM_VALUE_YES}, null, null, null);
        while (query.moveToNext()) {
            n nVar = new n();
            nVar.a = query.getString(query.getColumnIndex("transactionId"));
            nVar.b = query.getString(query.getColumnIndex("desc"));
            nVar.c = query.getString(query.getColumnIndex("productId"));
            nVar.d = query.getString(query.getColumnIndex("promotinId"));
            nVar.e = query.getString(query.getColumnIndex("imsi"));
            nVar.f = query.getString(query.getColumnIndex("imei"));
            nVar.g = query.getString(query.getColumnIndex("timestamp"));
            nVar.h = query.getString(query.getColumnIndex("sdkVersion"));
            nVar.i = query.getString(query.getColumnIndex("payType"));
            nVar.j = query.getString(query.getColumnIndex("uploadflag"));
            nVar.k = query.getInt(query.getColumnIndex("romVersion"));
            arrayList.add(nVar);
        }
        query.close();
        this.b.close();
        if (arrayList == null || arrayList.size() <= 0) {
            return null;
        }
        return arrayList;
    }

    public boolean a(String str) {
        int delete = this.b.delete(d, "transactionId=?", new String[]{str});
        this.b.close();
        if (delete == 0) {
            return false;
        }
        return true;
    }
}
