package com.bluepay.interfaceClass;

import com.bluepay.a.b.au;
import com.bluepay.a.b.aw;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.pay.IPayCallback;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.exception.BlueException;
import com.google.firebase.analytics.FirebaseAnalytics;

/* compiled from: Proguard */
public abstract class a {
    IPayCallback d;

    public abstract void a(b bVar);

    public abstract void b(b bVar);

    public abstract void c(b bVar);

    public int a(b bVar, b bVar2) {
        if (bVar == null || bVar2 == null) {
            return -1;
        }
        if (bVar.a() == h.a) {
            c.c(bVar.b());
            String b = bVar.b();
            try {
                aw b2 = au.b(b);
                try {
                    int c = b2.c("status");
                    try {
                        bVar2.setPrice(b2.c(FirebaseAnalytics.Param.PRICE));
                    } catch (BlueException e) {
                    }
                    if (c == h.c || c == h.b) {
                        return c;
                    }
                    com.bluepay.a.b.a.o.a(14, c, 0, bVar2);
                    return c;
                } catch (BlueException e2) {
                    bVar2.desc = b;
                    com.bluepay.a.b.a.o.a(14, h.i, 0, bVar2);
                    return -1;
                }
            } catch (BlueException e3) {
                bVar2.desc = e3.getMessage();
                com.bluepay.a.b.a.o.a(14, h.i, 0, bVar2);
                return -1;
            }
        } else {
            com.bluepay.a.b.a.o.a(14, h.n, 0, bVar2);
            return -1;
        }
    }
}
