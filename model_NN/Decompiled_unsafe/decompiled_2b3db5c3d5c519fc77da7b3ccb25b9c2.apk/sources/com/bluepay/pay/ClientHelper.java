package com.bluepay.pay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import com.bluepay.a.b.as;
import com.bluepay.a.b.au;
import com.bluepay.a.b.aw;
import com.bluepay.data.Config;
import com.bluepay.data.a;
import com.bluepay.data.c;
import com.bluepay.data.d;
import com.bluepay.data.f;
import com.bluepay.data.h;
import com.bluepay.data.i;
import com.bluepay.data.k;
import com.bluepay.sdk.b.b;
import com.bluepay.sdk.c.aa;
import com.bluepay.sdk.exception.BlueException;
import com.facebook.internal.ServerProtocol;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.apache.http.HttpStatus;

@SuppressLint({"SimpleDateFormat"})
/* compiled from: Proguard */
public abstract class ClientHelper {
    private static final DateFormat InstallDF = new SimpleDateFormat("yyyyMMddHH");
    static HashMap mncs = new HashMap();

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0134, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0138, code lost:
        if (r14 == 3) goto L_0x013a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x013a, code lost:
        com.bluepay.sdk.c.aa.b(r8, com.bluepay.sdk.c.aa.e(), "BluePay init error|Phone Model:" + com.bluepay.pay.Client.m_Model + "|Reason:connect error " + r0.getCode() + " " + getGetUrl(r1, r3, r2), com.bluepay.pay.Client.m_iIMSI, 14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        return initError(r8, r14, r9);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String initPay(android.content.Context r8, java.lang.String r9, java.lang.String r10, java.lang.String r11, java.lang.String r12, java.lang.String r13, int r14) {
        /*
            r6 = 3
            r7 = 14
            java.lang.String r1 = com.bluepay.data.m.a(r14, r11)
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            if (r11 == 0) goto L_0x0019
            int r0 = r11.length()
            if (r0 <= 0) goto L_0x0019
            java.lang.String r0 = "lan"
            r2.put(r0, r11)
        L_0x0019:
            java.lang.String r0 = "productid"
            int r3 = com.bluepay.pay.Client.getProductId()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2.put(r0, r3)
            java.lang.String r0 = "imsi"
            r2.put(r0, r9)
            java.lang.String r0 = com.bluepay.pay.Client.m_iIMSI2
            if (r0 == 0) goto L_0x0049
            java.lang.String r0 = com.bluepay.pay.Client.m_iIMSI2
            boolean r0 = r9.equals(r0)
            if (r0 != 0) goto L_0x0049
            java.lang.String r0 = "imsi2"
            java.lang.String r3 = com.bluepay.pay.Client.m_iIMSI2
            r2.put(r0, r3)
            java.lang.String r0 = "defaultsim"
            int r3 = com.bluepay.sdk.c.aa.a
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2.put(r0, r3)
        L_0x0049:
            java.lang.String r0 = "imei"
            r2.put(r0, r10)
            java.lang.String r0 = "promotionId"
            r2.put(r0, r12)
            java.lang.String r0 = "v"
            r3 = 100038002(0x5f67572, float:2.3176871E-35)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2.put(r0, r3)
            java.lang.String r0 = "rv"
            int r3 = com.bluepay.sdk.c.aa.c()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2.put(r0, r3)
            java.lang.String r0 = "model"
            java.lang.String r3 = com.bluepay.pay.Client.m_Model
            r2.put(r0, r3)
            java.lang.String r0 = com.bluepay.pay.Client.m_iWifiType
            java.lang.String r3 = "unknow"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x0084
            java.lang.String r0 = "networktype"
            java.lang.String r3 = com.bluepay.pay.Client.m_iWifiType
            r2.put(r0, r3)
        L_0x0084:
            java.lang.String r0 = ""
            java.lang.String r0 = com.bluepay.sdk.c.d.a(r2)     // Catch:{ BlueException -> 0x0109 }
            java.lang.String r3 = r0.toString()     // Catch:{ BlueException -> 0x0109 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ BlueException -> 0x0109 }
            r0.<init>()     // Catch:{ BlueException -> 0x0109 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ BlueException -> 0x0109 }
            java.lang.String r4 = com.bluepay.pay.Client.getEncrypt()     // Catch:{ BlueException -> 0x0109 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ BlueException -> 0x0109 }
            java.lang.String r0 = r0.toString()     // Catch:{ BlueException -> 0x0109 }
            java.lang.String r0 = com.bluepay.sdk.c.e.a(r0)     // Catch:{ BlueException -> 0x0109 }
            java.lang.String r4 = "encrypt"
            r2.put(r4, r0)     // Catch:{ BlueException -> 0x0109 }
            com.bluepay.interfaceClass.b r0 = com.bluepay.sdk.a.a.a(r8, r1, r3, r2)     // Catch:{ BlueException -> 0x0134, Exception -> 0x025f }
            java.lang.String r1 = r0.b()     // Catch:{ BlueException -> 0x0134, Exception -> 0x025f }
            com.bluepay.a.b.aw r2 = com.bluepay.a.b.au.b(r1)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r0 = "400"
            java.lang.String r0 = r2.b(r0)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            boolean r3 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            if (r3 != 0) goto L_0x017b
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            r3.<init>()     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r4 = "init---"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            com.bluepay.sdk.b.c.c(r0)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            r0.<init>()     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r3 = "BluePay init error|Phone Model:"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r3 = com.bluepay.pay.Client.m_Model     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r3 = "|Reason:"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            r3 = 400(0x190, float:5.6E-43)
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r3 = com.bluepay.sdk.c.aa.e()     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r4 = com.bluepay.pay.Client.m_iIMSI     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            r5 = 14
            com.bluepay.sdk.c.aa.b(r8, r3, r0, r4, r5)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r0 = "404"
        L_0x0108:
            return r0
        L_0x0109:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "BluePay init error|Phone Model:"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.bluepay.pay.Client.m_Model
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "|Reason:encrypt error"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = com.bluepay.sdk.c.aa.e()
            java.lang.String r2 = com.bluepay.pay.Client.m_iIMSI
            com.bluepay.sdk.c.aa.b(r8, r1, r0, r2, r7)
            java.lang.String r0 = "404"
            goto L_0x0108
        L_0x0134:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            if (r14 != r6) goto L_0x0176
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            r4.<init>()     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r5 = "BluePay init error|Phone Model:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r5 = com.bluepay.pay.Client.m_Model     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r5 = "|Reason:connect error "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            int r0 = r0.getCode()     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r4 = " "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r1 = getGetUrl(r1, r3, r2)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r0 = r0.toString()     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r1 = com.bluepay.sdk.c.aa.e()     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r2 = com.bluepay.pay.Client.m_iIMSI     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            r3 = 14
            com.bluepay.sdk.c.aa.b(r8, r1, r0, r2, r3)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
        L_0x0176:
            java.lang.String r0 = initError(r8, r14, r9)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            goto L_0x0108
        L_0x017b:
            java.lang.String r0 = "401"
            java.lang.String r0 = r2.b(r0)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            boolean r3 = android.text.TextUtils.isEmpty(r0)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            if (r3 != 0) goto L_0x01d1
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            r3.<init>()     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r4 = "init---"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            com.bluepay.sdk.b.c.c(r0)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            r0.<init>()     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r3 = "BluePay init error|Phone Model:"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r3 = com.bluepay.pay.Client.m_Model     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r3 = "|Reason:"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            r3 = 401(0x191, float:5.62E-43)
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r3 = com.bluepay.sdk.c.aa.e()     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r4 = com.bluepay.pay.Client.m_iIMSI     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            r5 = 14
            com.bluepay.sdk.c.aa.b(r8, r3, r0, r4, r5)     // Catch:{ Exception -> 0x01cd, BlueException -> 0x0203 }
            java.lang.String r0 = "404"
            goto L_0x0108
        L_0x01cd:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
        L_0x01d1:
            initData(r2)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r0 = "BluePay"
            com.bluepay.sdk.b.b.a(r8, r0)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r0 = "init_json"
            com.bluepay.sdk.b.b.a(r0, r1)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            com.bluepay.sdk.b.b.a()     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            r0.<init>()     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r1 = "BluePay script init success|num:"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.StringBuilder r0 = r0.append(r14)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r0 = r0.toString()     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r1 = com.bluepay.sdk.c.aa.e()     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r2 = com.bluepay.pay.Client.m_iIMSI     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            r3 = 12
            com.bluepay.sdk.c.aa.b(r8, r1, r0, r2, r3)     // Catch:{ BlueException -> 0x0203, Exception -> 0x025f }
            java.lang.String r0 = "200"
            goto L_0x0108
        L_0x0203:
            r0 = move-exception
            r0.printStackTrace()
            if (r14 != r6) goto L_0x023f
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "BluePay init error|Phone Model:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = com.bluepay.pay.Client.m_Model
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "|num:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r14)
            java.lang.String r2 = "|Reason:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r0.getMessage()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = com.bluepay.sdk.c.aa.e()
            java.lang.String r3 = com.bluepay.pay.Client.m_iIMSI
            com.bluepay.sdk.c.aa.b(r8, r2, r1, r3, r7)
        L_0x023f:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "init---"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.bluepay.sdk.b.c.c(r0)
            java.lang.String r0 = initError(r8, r14, r9)
            goto L_0x0108
        L_0x025f:
            r0 = move-exception
            r0.printStackTrace()
            if (r14 != r6) goto L_0x02ba
            java.lang.StackTraceElement[] r2 = r0.getStackTrace()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            int r4 = r2.length
            r1 = 0
        L_0x0270:
            if (r1 >= r4) goto L_0x0284
            r5 = r2[r1]
            java.lang.String r5 = r5.toString()
            java.lang.StringBuilder r5 = r3.append(r5)
            java.lang.String r6 = "\n"
            r5.append(r6)
            int r1 = r1 + 1
            goto L_0x0270
        L_0x0284:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "BluePay init error|Phone Model:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = com.bluepay.pay.Client.m_Model
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "|num:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r14)
            java.lang.String r2 = "|Reason:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r3.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = com.bluepay.sdk.c.aa.e()
            java.lang.String r3 = com.bluepay.pay.Client.m_iIMSI
            com.bluepay.sdk.c.aa.b(r8, r2, r1, r3, r7)
        L_0x02ba:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "init---"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.bluepay.sdk.b.c.c(r0)
            java.lang.String r0 = initError(r8, r14, r9)
            goto L_0x0108
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bluepay.pay.ClientHelper.initPay(android.content.Context, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int):java.lang.String");
    }

    private static String initError(Context context, int num, String imsi) {
        if (num != 3) {
            return "404";
        }
        b.a(context, Client.TAG);
        String b = b.b("init_json", "");
        if (TextUtils.isEmpty(b)) {
            initNormal(imsi);
            aa.b(context, aa.e(), "BluePay sdk init success|num:" + num, Client.m_iIMSI, 12);
            return "200";
        }
        try {
            initData(au.b(b));
            aa.b(context, aa.e(), "BluePay shareprefence init success|num:" + num, Client.m_iIMSI, 12);
            return "200";
        } catch (BlueException e) {
            e.printStackTrace();
            initNormal(imsi);
            aa.b(context, aa.e(), "BluePay sdk init success|num:" + num, Client.m_iIMSI, 12);
            return "200";
        }
    }

    private static void initData(aw result) {
        int i;
        boolean z = true;
        Client.m_hashChargeList = k.a(result, "chargeList");
        Client.m_hashTRFPriceList = result.a("tariffList", "id", FirebaseAnalytics.Param.PRICE);
        aa.a(result);
        Client.m_DcbInfo = f.a(result);
        Client.m_BankChargeInfo = a.a(result);
        Client.m_uploadErrorCode = aa.b(result);
        Client.setMsNum(result.a("msisdn"));
        com.bluepay.a.b.a.a = aa.a(result.a("cfmFlag"), 1) > 0;
        com.bluepay.a.b.a.c = result.b("apiUrl");
        com.bluepay.a.b.a.d = result.b("statUrl");
        com.bluepay.a.b.a.e = result.b("queryUrl");
        com.bluepay.a.b.a.h = result.a("delimiter");
        Client.CONTRY_CODE = result.c("CountryCode");
        if (aa.a(result.b("dtac3GMode"), 0) <= 0) {
            z = false;
        }
        as.b = z;
        Client.telcoName = result.b("telco");
        Client.m_BlueWallet = c.a(result);
        com.bluepay.a.b.a.i = result.a("apiUrls", "country", "url");
        com.bluepay.a.b.a.f = result.b("userCentreUrl");
        try {
            i = result.c("coca");
        } catch (Exception e) {
            i = 0;
        }
        if (i == 999) {
            com.bluepay.sdk.b.c.a();
        } else {
            com.bluepay.sdk.b.c.b();
        }
        if (Client.m_iOperatorId == 0) {
            Client.m_iOperatorId = aa.a(result.a("operator"), 0);
        }
    }

    private static void initMNC() {
        mncs.clear();
        mncs.put("52005", new String[]{Config.COUNTRY_TH, "dtac"});
        mncs.put("52018", new String[]{Config.COUNTRY_TH, "dtac"});
        mncs.put("52001", new String[]{Config.COUNTRY_TH, "ais"});
        mncs.put("52003", new String[]{Config.COUNTRY_TH, "ais"});
        mncs.put("52023", new String[]{Config.COUNTRY_TH, "ais"});
        mncs.put("52004", new String[]{Config.COUNTRY_TH, ServerProtocol.DIALOG_RETURN_SCOPES_TRUE});
        mncs.put("52025", new String[]{Config.COUNTRY_TH, ServerProtocol.DIALOG_RETURN_SCOPES_TRUE});
        mncs.put("52099", new String[]{Config.COUNTRY_TH, ServerProtocol.DIALOG_RETURN_SCOPES_TRUE});
        mncs.put("52000", new String[]{Config.COUNTRY_TH, ServerProtocol.DIALOG_RETURN_SCOPES_TRUE});
        mncs.put("52002", new String[]{Config.COUNTRY_TH, ServerProtocol.DIALOG_RETURN_SCOPES_TRUE});
        mncs.put("46000", new String[]{Config.COUNTRY_CN, "chinamobile"});
        mncs.put("46001", new String[]{Config.COUNTRY_CN, "chinamobile"});
        mncs.put("46002", new String[]{Config.COUNTRY_CN, "chinamobile"});
        mncs.put("46003", new String[]{Config.COUNTRY_CN, "chinatelecom"});
        mncs.put("46004", new String[]{Config.COUNTRY_CN, "chinatelecom"});
        mncs.put("46005", new String[]{Config.COUNTRY_CN, "chinatelecom"});
        mncs.put("46006", new String[]{Config.COUNTRY_CN, "chinaunicom"});
        mncs.put("46007", new String[]{Config.COUNTRY_CN, "chinamobile"});
        mncs.put("46009", new String[]{Config.COUNTRY_CN, "chinaunicom"});
        mncs.put("46011", new String[]{Config.COUNTRY_CN, "chinatelecom"});
        mncs.put("46020", new String[]{Config.COUNTRY_CN, "chinamobile"});
        mncs.put("45412", new String[]{Config.COUNTRY_CN, "chinamobile"});
        mncs.put("51001", new String[]{Config.COUNTRY_ID, Config.TELCO_NAME_INDOSAT});
        mncs.put("51003", new String[]{Config.COUNTRY_ID, Config.TELCO_NAME_INDOSAT});
        mncs.put("51021", new String[]{Config.COUNTRY_ID, Config.TELCO_NAME_INDOSAT});
        mncs.put("51007", new String[]{Config.COUNTRY_ID, Config.TELCO_NAME_TELKOMSEL});
        mncs.put("51010", new String[]{Config.COUNTRY_ID, Config.TELCO_NAME_TELKOMSEL});
        mncs.put("51020", new String[]{Config.COUNTRY_ID, Config.TELCO_NAME_TELKOMSEL});
        mncs.put("51020", new String[]{Config.COUNTRY_ID, Config.TELCO_NAME_TELKOMSEL});
        mncs.put("51008", new String[]{Config.COUNTRY_ID, Config.TELCO_NAME_XL});
        mncs.put("51011", new String[]{Config.COUNTRY_ID, Config.TELCO_NAME_XL});
        mncs.put("51009", new String[]{Config.COUNTRY_ID, "smartfren"});
        mncs.put("51028", new String[]{Config.COUNTRY_ID, "smartfren"});
        mncs.put("51089", new String[]{Config.COUNTRY_ID, Config.TELCO_NAME_HUTCHISON});
        mncs.put("45404", new String[]{Config.COUNTRY_ID, Config.TELCO_NAME_HUTCHISON});
        mncs.put("45201", new String[]{Config.COUNTRY_VN, PublisherCode.PUBLISHER_MOBIFONE});
        mncs.put("45202", new String[]{Config.COUNTRY_VN, PublisherCode.PUBLISHER_VINAPHONE});
        mncs.put("45204", new String[]{Config.COUNTRY_VN, PublisherCode.PUBLISHER_VIETTEL});
        mncs.put("45206", new String[]{Config.COUNTRY_VN, PublisherCode.PUBLISHER_VIETTEL});
        mncs.put("45208", new String[]{Config.COUNTRY_VN, PublisherCode.PUBLISHER_VIETTEL});
        mncs.put("45205", new String[]{Config.COUNTRY_VN, "vietnammobile"});
        mncs.put("45207", new String[]{Config.COUNTRY_VN, "gmobile"});
        mncs.put("41006", new String[]{"PAK", PublisherCode.PUBLISHER_DCB_TELENOR});
    }

    private static void setApis() {
        com.bluepay.a.b.a.i = new HashMap();
        com.bluepay.a.b.a.i.put(Config.COUNTRY_TH, "http://203.151.93.97");
        com.bluepay.a.b.a.i.put(Config.COUNTRY_CN, "http://120.76.101.146:8160");
        com.bluepay.a.b.a.i.put(Config.COUNTRY_VN, "http://125.212.202.118:8160");
        com.bluepay.a.b.a.i.put(Config.COUNTRY_ID, "http://54.169.238.20:8160");
        com.bluepay.a.b.a.i.put("PAK", "http://203.150.54.214");
    }

    private static void initBoyaa(String imsi) {
        initMNC();
        String str = Config.COUNTRY_CN;
        if (!mncs.containsKey(imsi.substring(0, 5))) {
            Client.m_iOperatorId = 0;
            Client.CONTRY_CODE = 0;
            Client.telcoName = "";
            Client.m_hashChargeList = new HashMap();
            Client.m_BankChargeInfo = new a();
            Client.m_DcbInfo = new f();
        } else {
            String[] strArr = (String[]) mncs.get(imsi.substring(0, 5));
            String str2 = strArr[0];
            Client.telcoName = strArr[1];
            Client.m_DcbInfo = new f(1);
            if (Client.telcoName.equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE)) {
                Client.m_iOperatorId = 1;
                Client.CONTRY_CODE = 66;
                Client.m_hashChargeList = d.a();
                Client.m_BankChargeInfo = new a(true, "4192007");
                str = str2;
            } else if (Client.telcoName.equals("ais")) {
                Client.m_iOperatorId = 2;
                Client.CONTRY_CODE = 66;
                Client.m_hashChargeList = d.c();
                Client.m_BankChargeInfo = new a(true, "4192034");
                str = str2;
            } else if (Client.telcoName.equals("dtac")) {
                Client.m_iOperatorId = 3;
                Client.CONTRY_CODE = 66;
                Client.m_hashChargeList = d.e();
                Client.m_BankChargeInfo = new a(true, "4078005");
                str = str2;
            } else if (Client.telcoName.equals("chinamobile")) {
                Client.m_iOperatorId = 9;
                Client.CONTRY_CODE = 86;
                Client.m_hashChargeList = d.g();
                Client.m_BankChargeInfo = new a(true, "18682106090");
                str = str2;
            } else if (Client.telcoName.equals("chinaunicom")) {
                Client.m_iOperatorId = 9;
                Client.CONTRY_CODE = 86;
                Client.m_hashChargeList = d.g();
                Client.m_BankChargeInfo = new a(true, "18682106090");
                str = str2;
            } else if (Client.telcoName.equals("chinatelecom")) {
                Client.m_iOperatorId = 9;
                Client.CONTRY_CODE = 86;
                Client.m_hashChargeList = d.g();
                Client.m_BankChargeInfo = new a(true, "18682106090");
                str = str2;
            } else {
                Client.m_iOperatorId = 0;
                Client.CONTRY_CODE = 0;
                Client.m_hashChargeList = new HashMap();
                Client.m_DcbInfo = new f();
                Client.m_BankChargeInfo = new a();
                str = str2;
            }
        }
        setApis();
        com.bluepay.a.b.a.c = (String) com.bluepay.a.b.a.i.get(str);
        com.bluepay.a.b.a.h = "@";
        Client.m_BlueWallet = new c("bluepayapp://open.businessAppView.api?appid=sdk&type=bluecoins&do=buy", "https://itunes.apple.com/cn/app/bluepay-topup-bluecoins-game/id1129720826?mt=8", "https://play.google.com/store/apps/details?id=asia.bluepay.client", 1);
        Client.m_uploadErrorCode = new ArrayList() {
            private static final long serialVersionUID = 1;

            {
                add(Integer.valueOf((int) HttpStatus.SC_BAD_REQUEST));
                add(401);
                add(Integer.valueOf((int) HttpStatus.SC_FORBIDDEN));
                add(Integer.valueOf((int) HttpStatus.SC_METHOD_NOT_ALLOWED));
                add(Integer.valueOf((int) HttpStatus.SC_NOT_IMPLEMENTED));
                add(Integer.valueOf((int) Client.C_USER_CANCEL));
            }
        };
    }

    private static void initNormal(String imsi) {
        initMNC();
        String str = Config.COUNTRY_CN;
        if (!mncs.containsKey(imsi.substring(0, 5))) {
            Client.m_iOperatorId = 0;
            Client.CONTRY_CODE = 0;
            Client.telcoName = "";
            com.bluepay.a.b.a.h = "@";
            Client.m_hashChargeList = new HashMap();
            Client.m_BankChargeInfo = new a();
            Client.m_DcbInfo = new f();
        } else {
            String[] strArr = (String[]) mncs.get(imsi.substring(0, 5));
            String str2 = strArr[0];
            Client.telcoName = strArr[1];
            com.bluepay.a.b.a.h = "@";
            aa.h();
            HashMap hashMap = new HashMap();
            hashMap.put(Config.TELCO_NAME_XL, "xxxx");
            hashMap.put(Config.TELCO_NAME_INDOSAT, "BELI xxx");
            hashMap.put(Config.TELCO_NAME_HUTCHISON, "BUY xxxx");
            if (Client.telcoName.equals(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE)) {
                Client.m_iOperatorId = 1;
                Client.CONTRY_CODE = 66;
                Client.m_hashChargeList = d.b();
                Client.m_DcbInfo = new f(1);
                Client.m_BankChargeInfo = new a(true, "4192007");
                str = str2;
            } else if (Client.telcoName.equals("ais")) {
                Client.m_iOperatorId = 2;
                Client.CONTRY_CODE = 66;
                Client.m_hashChargeList = d.d();
                Client.m_DcbInfo = new f(1);
                Client.m_BankChargeInfo = new a(true, "4192034");
                str = str2;
            } else if (Client.telcoName.equals("dtac")) {
                Client.m_iOperatorId = 3;
                Client.CONTRY_CODE = 66;
                Client.m_hashChargeList = d.f();
                Client.m_DcbInfo = new f(1);
                Client.m_BankChargeInfo = new a(true, "4078005");
                str = str2;
            } else if (Client.telcoName.equals("chinamobile")) {
                Client.m_iOperatorId = 9;
                Client.CONTRY_CODE = 86;
                Client.m_hashChargeList = d.g();
                Client.m_DcbInfo = new f(1);
                Client.m_BankChargeInfo = new a(true, "0000000", true, 25, 5500, 4500, "http://120.76.101.146:100/vtc/index.php", "http://103.4.175.3:80/bank/index.php");
                str = str2;
            } else if (Client.telcoName.equals("chinaunicom")) {
                Client.m_iOperatorId = 9;
                Client.CONTRY_CODE = 86;
                Client.m_hashChargeList = d.g();
                Client.m_DcbInfo = new f(1);
                Client.m_BankChargeInfo = new a(true, "0000000", true, 25, 5500, 4500, "http://120.76.101.146:100/vtc/index.php", "http://103.4.175.3:80/bank/index.php");
                str = str2;
            } else if (Client.telcoName.equals("chinatelecom")) {
                Client.m_iOperatorId = 9;
                Client.CONTRY_CODE = 86;
                Client.m_hashChargeList = d.g();
                Client.m_DcbInfo = new f(1);
                Client.m_BankChargeInfo = new a(true, "0000000", true, 25, 5500, 4500, "http://120.76.101.146:100/vtc/index.php", "http://103.4.175.3:80/bank/index.php");
                str = str2;
            } else if (Client.telcoName.equals(Config.TELCO_NAME_TELKOMSEL)) {
                Client.m_iOperatorId = 6;
                Client.CONTRY_CODE = 62;
                Client.m_hashChargeList = d.h();
                Client.m_DcbInfo = new f(1, "085742303868", new String[0], "", "", 1, 0, hashMap);
                Client.m_BankChargeInfo = new a(false, "0000000", true, 25, 5500, 4500, "", "http://103.4.175.3:80/bank/index.php");
                str = str2;
            } else if (Client.telcoName.equals(Config.TELCO_NAME_INDOSAT)) {
                Client.m_iOperatorId = 27;
                Client.CONTRY_CODE = 62;
                Client.m_hashChargeList = d.i();
                Client.m_DcbInfo = new f(6, "085742303868", new String[]{"Ketik_BELI_anda"}, "99231", "8", 1, 0, hashMap);
                Client.m_BankChargeInfo = new a(false, "0000000", true, 25, 5500, 4500, "", "http://103.4.175.3:80/bank/index.php");
                str = str2;
            } else if (Client.telcoName.equals(Config.TELCO_NAME_XL)) {
                Client.m_iOperatorId = 27;
                Client.CONTRY_CODE = 62;
                Client.m_hashChargeList = d.i();
                Client.m_DcbInfo = new f(6, "085742303868", new String[]{"Kamu_menyelesaikan_ CS:"}, "99888", "4", 1, 0, hashMap);
                Client.m_BankChargeInfo = new a(false, "0000000", true, 25, 5500, 4500, "", "http://103.4.175.3:80/bank/index.php");
                str = str2;
            } else if (Client.telcoName.equals(Config.TELCO_NAME_HUTCHISON)) {
                Client.m_iOperatorId = 27;
                Client.CONTRY_CODE = 62;
                Client.m_hashChargeList = d.i();
                Client.m_DcbInfo = new f(6, "085742303868", new String[]{"Kamu_BUY_. CS:"}, "96333", "8", 1, 0, hashMap);
                Client.m_BankChargeInfo = new a(false, "0000000", true, 25, 5500, 4500, "", "http://103.4.175.3:80/bank/index.php");
                str = str2;
            } else if (Client.telcoName.equals("smartfren")) {
                Client.m_iOperatorId = 16;
                Client.CONTRY_CODE = 62;
                Client.m_hashChargeList = d.j();
                Client.m_DcbInfo = new f(4, "085742303868", new String[0], "", "", 1, 0, hashMap);
                Client.m_BankChargeInfo = new a(false, "0000000", true, 25, 5500, 4500, "", "http://103.4.175.3:80/bank/index.php");
                str = str2;
            } else if (Client.telcoName.equals(PublisherCode.PUBLISHER_MOBIFONE)) {
                Client.m_iOperatorId = 13;
                Client.CONTRY_CODE = 84;
                Client.m_hashChargeList = d.k();
                Client.m_DcbInfo = new f(1);
                Client.m_BankChargeInfo = new a(true, "0000000", false, 0, 0, 0, "http://125.212.233.65:80/bank/index.php", "");
                com.bluepay.a.b.a.h = "/";
                str = str2;
            } else if (Client.telcoName.equals(PublisherCode.PUBLISHER_VINAPHONE)) {
                Client.m_iOperatorId = 14;
                Client.CONTRY_CODE = 84;
                Client.m_hashChargeList = d.l();
                Client.m_DcbInfo = new f(1);
                Client.m_BankChargeInfo = new a(true, "0000000", false, 0, 0, 0, "http://125.212.233.65:80/bank/index.php", "");
                com.bluepay.a.b.a.h = "/";
                str = str2;
            } else if (Client.telcoName.equals(PublisherCode.PUBLISHER_VIETTEL)) {
                Client.m_iOperatorId = 12;
                Client.CONTRY_CODE = 84;
                Client.m_hashChargeList = d.m();
                Client.m_DcbInfo = new f(1);
                Client.m_BankChargeInfo = new a(true, "0000000", false, 0, 0, 0, "http://125.212.233.65:80/bank/index.php", "");
                com.bluepay.a.b.a.h = "/";
                str = str2;
            } else {
                Client.m_iOperatorId = 0;
                Client.CONTRY_CODE = 0;
                Client.m_hashChargeList = new HashMap();
                Client.m_DcbInfo = new f();
                Client.m_BankChargeInfo = new a();
                str = str2;
            }
        }
        setApis();
        com.bluepay.a.b.a.c = (String) com.bluepay.a.b.a.i.get(str);
        com.bluepay.a.b.a.f = "http://203.151.93.112:8085";
        Client.m_BlueWallet = new c("bluepayapp://open.businessAppView.api?appid=sdk&type=bluecoins&do=buy", "https://itunes.apple.com/cn/app/bluepay-topup-bluecoins-game/id1129720826?mt=8", "https://play.google.com/store/apps/details?id=asia.bluepay.client", 1);
        Client.m_uploadErrorCode = new ArrayList() {
            private static final long serialVersionUID = 1;

            {
                add(Integer.valueOf((int) HttpStatus.SC_BAD_REQUEST));
                add(401);
                add(Integer.valueOf((int) HttpStatus.SC_FORBIDDEN));
                add(Integer.valueOf((int) HttpStatus.SC_METHOD_NOT_ALLOWED));
                add(Integer.valueOf((int) HttpStatus.SC_NOT_IMPLEMENTED));
                add(Integer.valueOf((int) Client.C_USER_CANCEL));
            }
        };
    }

    private static String getGetUrl(String url, String url_Content, Map map) {
        StringBuilder append = new StringBuilder(url).append("?").append(url_Content);
        if (map != null) {
            try {
                append.append("&").append("encrypt=").append(map.get("encrypt"));
            } catch (Exception e) {
                com.bluepay.sdk.b.c.b("Http get ERROR! getUrl 1 " + i.a((byte) i.D, e.getMessage()));
                throw new BlueException(e, h.i, i.a(i.D), e.getMessage());
            }
        }
        return append.toString();
    }

    public static synchronized String generateTid() {
        String str;
        synchronized (ClientHelper.class) {
            StringBuffer stringBuffer = new StringBuffer();
            try {
                String format = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                com.bluepay.sdk.b.c.c("generate tid:" + format);
                if (Client.getEncrypt().length() > 4) {
                    str = stringBuffer.append(Client.getEncrypt().subSequence(0, 4)).append(format).append(ramdomGenNo("")).toString();
                } else {
                    str = stringBuffer.append(Client.getEncrypt()).append(format).append(ramdomGenNo("")).toString();
                }
            } catch (Exception e) {
                str = "000000000000";
            }
        }
        return str;
    }

    public static String getPreContent(int price) {
        return "";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0019, code lost:
        r0 = 0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized int generateSystemTime() {
        /*
            java.lang.Class<com.bluepay.pay.ClientHelper> r1 = com.bluepay.pay.ClientHelper.class
            monitor-enter(r1)
            java.text.DateFormat r0 = com.bluepay.pay.ClientHelper.InstallDF     // Catch:{ Exception -> 0x0018, all -> 0x001b }
            java.util.Date r2 = new java.util.Date     // Catch:{ Exception -> 0x0018, all -> 0x001b }
            r2.<init>()     // Catch:{ Exception -> 0x0018, all -> 0x001b }
            java.lang.String r0 = r0.format(r2)     // Catch:{ Exception -> 0x0018, all -> 0x001b }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x0018, all -> 0x001b }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0018, all -> 0x001b }
        L_0x0016:
            monitor-exit(r1)
            return r0
        L_0x0018:
            r0 = move-exception
            r0 = 0
            goto L_0x0016
        L_0x001b:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bluepay.pay.ClientHelper.generateSystemTime():int");
    }

    private static String ramdomGenNo(String sNo) {
        Random random = new Random();
        for (int i = 0; i < 2; i++) {
            sNo = sNo + ramdomChar(random);
        }
        return sNo;
    }

    private static char ramdomChar(Random random) {
        if (random.nextInt(2) == 0) {
            return (char) (random.nextInt(26) + 65);
        }
        return (char) (random.nextInt(26) + 97);
    }

    public static int timeDelayed() {
        return 5000;
    }

    public static String getExploreContent() {
        return "50" + fillWithZero(String.valueOf(Client.m_iOperatorId), 3) + fillWithZero(Client.m_iIMSI, 15) + fillWithZero(Client.m_iIMEI, 15);
    }

    private static String fillWithZero(String data, int length) {
        if (length == 0 || TextUtils.isEmpty(data)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int length2 = length - data.length();
        for (int i = 0; i < length2; i++) {
            sb.append(0);
        }
        sb.append(data);
        return sb.toString().trim();
    }
}
