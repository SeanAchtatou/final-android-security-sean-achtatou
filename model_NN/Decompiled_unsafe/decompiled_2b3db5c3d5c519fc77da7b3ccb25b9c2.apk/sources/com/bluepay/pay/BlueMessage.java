package com.bluepay.pay;

/* compiled from: Proguard */
public class BlueMessage {
    int code;
    String errorDesc;
    String offlinePaymentCode;
    String price;
    String propsName;
    String publisher;
    String transactionId;

    public BlueMessage() {
    }

    public BlueMessage(int code2, String transactionId2, String propsName2, String price2, String publisher2) {
        this.code = code2;
        this.transactionId = transactionId2;
        this.propsName = propsName2;
        this.price = price2;
        this.publisher = publisher2;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code2) {
        this.code = code2;
    }

    public String getTransactionId() {
        return this.transactionId;
    }

    public void setTransactionId(String transactionId2) {
        this.transactionId = transactionId2;
    }

    public String getPropsName() {
        return this.propsName;
    }

    public void setPropsName(String propsName2) {
        this.propsName = propsName2;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price2) {
        this.price = price2;
    }

    @Deprecated
    public String getDesc() {
        return this.errorDesc;
    }

    public String getOfflinePaymentCode() {
        return this.offlinePaymentCode;
    }

    public void setOfflinePaymentCode(String offlinePaymentCode2) {
        this.offlinePaymentCode = offlinePaymentCode2;
    }

    public String getPublisher() {
        return this.publisher;
    }

    public void setPublisher(String publisher2) {
        this.publisher = publisher2;
    }

    @Deprecated
    public void setDesc(String message) {
        this.errorDesc = message;
    }

    public String toString() {
        return "BlueMessage [code=" + this.code + ", transactionId=" + this.transactionId + ", propsName=" + this.propsName + ", price=" + this.price + ", publisher=" + this.publisher + ", offlinePaymentCode=" + this.offlinePaymentCode + "]";
    }
}
