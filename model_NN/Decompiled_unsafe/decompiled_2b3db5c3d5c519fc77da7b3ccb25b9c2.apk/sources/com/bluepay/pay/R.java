package com.bluepay.pay;

public final class R {

    public static final class anim {
        public static final int bluepay_loading = 2131034122;
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2131165260;
        public static final int activity_vertical_margin = 2131165261;
    }

    public static final class drawable {
        public static final int bluep_bg_error = 2130837587;
        public static final int bluep_channel1_normal = 2130837588;
        public static final int bluep_channel1_pressed = 2130837589;
        public static final int bluep_channel1_selector = 2130837590;
        public static final int bluep_icon_12call = 2130837591;
        public static final int bluep_icon_arrow_down = 2130837592;
        public static final int bluep_icon_arrow_right = 2130837593;
        public static final int bluep_icon_arrow_white = 2130837594;
        public static final int bluep_icon_atm = 2130837595;
        public static final int bluep_icon_bluecoins = 2130837596;
        public static final int bluep_icon_close = 2130837597;
        public static final int bluep_icon_error = 2130837598;
        public static final int bluep_icon_fail = 2130837599;
        public static final int bluep_icon_happy = 2130837600;
        public static final int bluep_icon_happy_tip = 2130837601;
        public static final int bluep_icon_idbank = 2130837602;
        public static final int bluep_icon_indomog = 2130837603;
        public static final int bluep_icon_line = 2130837604;
        public static final int bluep_icon_lytocard = 2130837605;
        public static final int bluep_icon_mobifone = 2130837606;
        public static final int bluep_icon_one_calltip = 2130837607;
        public static final int bluep_icon_otc = 2130837608;
        public static final int bluep_icon_selected = 2130837609;
        public static final int bluep_icon_sms = 2130837610;
        public static final int bluep_icon_success = 2130837611;
        public static final int bluep_icon_transfer = 2130837612;
        public static final int bluep_icon_true_tip = 2130837613;
        public static final int bluep_icon_truemoney = 2130837614;
        public static final int bluep_icon_viettel = 2130837615;
        public static final int bluep_icon_vinaphone = 2130837616;
        public static final int bluep_icon_vnbank = 2130837617;
        public static final int bluep_icon_vtc = 2130837618;
        public static final int bluep_logo_12call = 2130837619;
        public static final int bluep_logo_bluecoins = 2130837620;
        public static final int bluep_logo_happy = 2130837621;
        public static final int bluep_logo_hope = 2130837622;
        public static final int bluep_logo_indomog = 2130837623;
        public static final int bluep_logo_line = 2130837624;
        public static final int bluep_logo_lytocard = 2130837625;
        public static final int bluep_logo_mobifone = 2130837626;
        public static final int bluep_logo_telenor = 2130837627;
        public static final int bluep_logo_telkomsel = 2130837628;
        public static final int bluep_logo_truemoney = 2130837629;
        public static final int bluep_logo_unipin = 2130837630;
        public static final int bluep_logo_viettel = 2130837631;
        public static final int bluep_logo_vinaphone = 2130837632;
        public static final int bluep_logo_vtc = 2130837633;
        public static final int bluep_result_confirm_bg = 2130837634;
        public static final int bluep_ui_background = 2130837635;
        public static final int bluep_ui_category_bg = 2130837636;
        public static final int bluep_ui_confirm_bg_enable = 2130837637;
        public static final int bluep_ui_confirm_bg_normal = 2130837638;
        public static final int bluep_ui_confirm_bg_pressed = 2130837639;
        public static final int bluep_ui_confirm_bg_selector = 2130837640;
        public static final int bluep_ui_input_bg = 2130837641;
        public static final int bluep_ui_input_error_bg = 2130837642;
        public static final int bluepay_loading = 2130837643;
        public static final int bluepay_logo = 2130837644;
        public static final int bluepay_refresh = 2130837645;
        public static final int bluepay_refresh_down = 2130837646;
    }

    public static final class id {
        public static final int Bluep_payCardNoTip = 2131427469;
        public static final int Bluep_payList = 2131427515;
        public static final int Bluep_paySerialNoEdit = 2131427477;
        public static final int Bluep_paySerialNoTip = 2131427476;
        public static final int Bluep_payTip = 2131427513;
        public static final int Bluep_paybyNo = 2131427512;
        public static final int Bluep_paybyYes = 2131427516;
        public static final int btn_back = 2131427429;
        public static final int btn_bluepay_get_opt = 2131427499;
        public static final int btn_bluepay_opt_cancel = 2131427494;
        public static final int btn_bluepay_opt_confirm = 2131427501;
        public static final int btn_close = 2131427492;
        public static final int btn_complete = 2131427431;
        public static final int btn_offline_concel = 2131427524;
        public static final int btn_offline_goto_tips = 2131427525;
        public static final int btn_ok = 2131427511;
        public static final int btn_submit = 2131427455;
        public static final int divider = 2131427488;
        public static final int et_Seral_first = 2131427472;
        public static final int et_Seral_fourth = 2131427475;
        public static final int et_Seral_second = 2131427473;
        public static final int et_Seral_third = 2131427474;
        public static final int et_bluep_phone = 2131427496;
        public static final int et_bluepay_opt = 2131427498;
        public static final int et_card_no = 2131427451;
        public static final int et_card_no1 = 2131427447;
        public static final int et_serial_no = 2131427453;
        public static final int et_unipin_first_input = 2131427479;
        public static final int et_unipin_last_input = 2131427480;
        public static final int headerLogo = 2131427465;
        public static final int imageView1 = 2131427502;
        public static final int iv_arrow_right = 2131427462;
        public static final int iv_bluepay_logo = 2131427493;
        public static final int iv_cancel = 2131427435;
        public static final int iv_channel_icon = 2131427461;
        public static final int iv_pay_result = 2131427503;
        public static final int iv_selected = 2131427489;
        public static final int label_card_no = 2131427450;
        public static final int label_card_no1 = 2131427446;
        public static final int label_serial_no = 2131427452;
        public static final int linear1 = 2131427504;
        public static final int linear2 = 2131427506;
        public static final int linear3 = 2131427509;
        public static final int linear4 = 2131427519;
        public static final int linearLayout1 = 2131427468;
        public static final int list_exchange = 2131427458;
        public static final int listview = 2131427437;
        public static final int ll_cancel = 2131427434;
        public static final int ll_card_no = 2131427449;
        public static final int ll_card_no1 = 2131427445;
        public static final int ll_price = 2131427439;
        public static final int payByPortrait = 2131427464;
        public static final int payEdit = 2131427470;
        public static final int payTip = 2131427481;
        public static final int paybyNo = 2131427466;
        public static final int paybyYes = 2131427482;
        public static final int pb_loading = 2131427526;
        public static final int pb_send_code = 2131427500;
        public static final int rl_CardNo_for_unipin = 2131427478;
        public static final int rl_pay_list = 2131427514;
        public static final int rl_select_channel = 2131427459;
        public static final int rl_serialNo_for_unipin = 2131427471;
        public static final int scrollView1 = 2131427463;
        public static final int scrollview = 2131427438;
        public static final int textView1 = 2131427485;
        public static final int textView2 = 2131427507;
        public static final int textView3 = 2131427517;
        public static final int textView4 = 2131427520;
        public static final int tipIv = 2131427467;
        public static final int tv_cancel = 2131427436;
        public static final int tv_card_tips = 2131427454;
        public static final int tv_channel_name = 2131427487;
        public static final int tv_error = 2131427432;
        public static final int tv_error_msg = 2131427486;
        public static final int tv_label_exchange = 2131427457;
        public static final int tv_label_payment = 2131427460;
        public static final int tv_label_phoneno = 2131427495;
        public static final int tv_label_verification = 2131427497;
        public static final int tv_loading = 2131427527;
        public static final int tv_name = 2131427490;
        public static final int tv_offline_channel = 2131427518;
        public static final int tv_offline_fee = 2131427522;
        public static final int tv_offline_paymentcode = 2131427523;
        public static final int tv_offline_price = 2131427521;
        public static final int tv_payment = 2131427430;
        public static final int tv_payment_channel = 2131427508;
        public static final int tv_payment_desc = 2131427510;
        public static final int tv_payment_price = 2131427505;
        public static final int tv_price = 2131427441;
        public static final int tv_propsname = 2131427440;
        public static final int tv_tips = 2131427443;
        public static final int tv_title = 2131427491;
        public static final int txtView_desc = 2131427484;
        public static final int txtView_line = 2131427483;
        public static final int view1 = 2131427442;
        public static final int view2 = 2131427444;
        public static final int view3 = 2131427448;
        public static final int view_exchange = 2131427456;
        public static final int wb_content = 2131427433;
    }

    public static final class layout {
        public static final int bluep_activity_pay_bank = 2130968602;
        public static final int bluep_activity_pay_ui_landscape = 2130968603;
        public static final int bluep_activity_pay_ui_portrait = 2130968604;
        public static final int bluep_by_otc_landscape = 2130968605;
        public static final int bluep_by_otc_landscape_happy = 2130968606;
        public static final int bluep_by_otc_portrait = 2130968607;
        public static final int bluep_error_tips = 2130968608;
        public static final int bluep_item_channel1 = 2130968609;
        public static final int bluep_item_channel2 = 2130968610;
        public static final int bluep_item_price = 2130968611;
        public static final int bluep_pay_list = 2130968612;
        public static final int bluep_pay_opt = 2130968613;
        public static final int bluep_pay_otc_portrait_happy = 2130968614;
        public static final int bluep_pay_result = 2130968615;
        public static final int bluep_smslist = 2130968616;
        public static final int bluep_tips_offline = 2130968617;
        public static final int bluepay_view_loading = 2130968618;
    }

    public static final class string {
        public static final int app_name = 2131099701;
        public static final int blueP_cashcard_12call = 2131099702;
        public static final int blueP_cashcard_trueMoney = 2131099703;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131230877;
        public static final int AppTheme = 2131230878;
    }
}
