package com.bluepay.pay;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bluepay.a.b.a;
import com.bluepay.data.Config;
import com.bluepay.data.Order;
import com.bluepay.data.Reference;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.data.i;
import com.bluepay.data.j;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.c.aa;
import com.bluepay.sdk.exception.BlueException;
import com.bluepay.ui.PayUIActivity;
import com.facebook.appevents.AppEventsConstants;

/* compiled from: Proguard */
public class BluePay {
    private static boolean isLandscape = false;
    private static boolean isShowCardloading = false;
    private static boolean isShowPayResult = false;
    public static BluePay mBluePay;
    public final int SHOWDIALOG_LANDSCAPE = 2;
    public final int SHOWDIALOG_NO = 0;
    public final int SHOWDIALOG_PORTRAIT = 1;
    public final String TAG = Client.TAG;
    private byte checkNum = 6;
    int price_position;

    public static BluePay getInstance() {
        if (mBluePay != null) {
            return mBluePay;
        }
        BluePay bluePay = new BluePay();
        mBluePay = bluePay;
        return bluePay;
    }

    private BluePay() {
    }

    public static void setLandscape(boolean _isLandscape) {
        isLandscape = _isLandscape;
    }

    public static boolean getLandscape() {
        return isLandscape;
    }

    public static void setShowCardLoading(boolean _isShowCardLoading) {
        isShowCardloading = _isShowCardLoading;
    }

    public static boolean getShowCardLoading() {
        return isShowCardloading;
    }

    public static void setShowResult(boolean _isShowPayResult) {
        isShowPayResult = _isShowPayResult;
    }

    public static boolean getShowResult() {
        return isShowPayResult;
    }

    public void setCheckNum(byte _checkNum) {
        this.checkNum = _checkNum;
    }

    private void uploadTrans(Activity activity, String transactionId, String publisher) {
        String str;
        String str2 = Client.m_iIMSI;
        if (!aa.a()) {
            str = "double sim|imsi1:" + Client.m_iIMSI1 + "|imsi2:" + Client.m_iIMSI2 + "|send_imsi:" + Client.m_iIMSI;
        } else {
            str = "single sim|imsi:" + Client.m_iIMSI;
        }
        aa.b(activity, transactionId, "|Phone Model:" + Client.m_Model + "|" + publisher, str, 15);
    }

    private void payBySMS2(Activity activity, String transactionId, String currency, String price, int smsId, String propsName, boolean isShowDialog, IPayCallback callback) {
        int safePrice = Order.getSafePrice(currency, price);
        if (safePrice <= 0) {
            throw new BlueException(h.i, "price error:" + price, new Object[0]);
        }
        setCheckNum((byte) 6);
        pay(activity, "000000SMS", transactionId, currency, propsName, "", "", safePrice, smsId, PublisherCode.PUBLISHER_SMS, "", Client.phoneNum(), isShowDialog, callback);
    }

    public boolean payByCashcard(Activity activity, String customId, String transactionId, String propsName, String publisher, String cardNo, String serialNo, IPayCallback callback) {
        try {
            uploadTrans(activity, transactionId, "payByCashcard|" + publisher);
            checkParameters(activity, callback, customId, transactionId, propsName, cardNo, serialNo, 0, "", Client.phoneNum());
            pay(activity, customId == null ? "" : customId, transactionId, Config.K_CURRENCY_TRF, propsName == null ? "" : propsName, cardNo == null ? "" : cardNo, serialNo == null ? "" : serialNo, 0, 0, publisher, "", Client.phoneNum(), false, callback);
            return true;
        } catch (BlueException e) {
            aa.a(activity, transactionId, "0", e.getCode(), publisher, e.getMessage());
            return true;
        }
    }

    private void pay(Activity activity, String customId, String transactionId, String currency, String propsName, String cardNo, String SerialNo, int price, int smsID, String payType, String scheme, String destMsisdn, boolean isShowDialog, IPayCallback callback) {
        boolean z;
        int i;
        try {
            c.c("pay");
            a.a(callback);
            Order order = new Order(activity, customId, Client.getProductId(), Client.getPromotionId(), transactionId, destMsisdn, propsName, new Reference(Client.m_iOperatorId, Client.getProductId(), 2, price, Client.getPromotionId()), isShowDialog, Client.m_iOperatorId);
            order.setSmsId(smsID);
            if (payType.equals(PublisherCode.PUBLISHER_SMS) || payType.equals(PublisherCode.PUBLISHER_BANK)) {
                i = 0;
                z = false;
            } else if (isDcb(payType) || payType.equals(PublisherCode.PUBLISHER_DCB_TELENOR)) {
                i = 5;
                z = false;
            } else if (payType.equals(PublisherCode.PUBLISHER_LINE) || payType.equals(PublisherCode.PUBLISHER_VN_BANK) || payType.equals(PublisherCode.PUBLISHER_ID_BANK)) {
                i = 6;
                order.setScheme(scheme);
                z = false;
            } else if (payType.equals(PublisherCode.PUBLISHER_OFFLINE_ATM) || payType.equals(PublisherCode.PUBLISHER_OFFLINE_OTC)) {
                i = 7;
                z = false;
            } else if (payType.equals(b.a)) {
                i = 8;
                z = false;
            } else {
                order.setCard(cardNo);
                order.setSerialNo(SerialNo);
                i = 3;
                z = true;
            }
            order.setCPPayType(payType);
            order.setCheckNum(this.checkNum);
            order.setCurrency(currency);
            order.setPrice(price);
            if (!a.a(transactionId, order)) {
                aa.a(activity, transactionId, price + "", h.i, payType, "order insert into order list error");
                return;
            }
            c.c("begin to deal billing");
            final b bVar = new b(order);
            if (!a.a || z) {
                a.o.a(0, i, 0, bVar);
                return;
            }
            aa.a(bVar.getActivity(), "TIPS", i.a(i.V), new DialogClickListener(bVar, i, 0), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    a.o.a(14, h.C, 0, bVar);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            throw new BlueException(h.i, "ERROR:do pay get an error.", new Object[0]);
        }
    }

    private static boolean isDcb(String payType) {
        if (payType.equals(PublisherCode.PUBLISHER_DCB) || payType.equals(PublisherCode.PUBLISHER_DCB_INDOSAT) || payType.equals(PublisherCode.PUBLISHER_DCB_SMARTFREN) || payType.equals(PublisherCode.PUBLISHER_DCB_XL)) {
            return true;
        }
        return false;
    }

    /* compiled from: Proguard */
    class DialogClickListener implements DialogInterface.OnClickListener {
        int mode;
        Order order;
        int type;

        public DialogClickListener(Order order2, int type2, int mode2) {
            this.order = order2;
            this.type = type2;
            this.mode = mode2;
        }

        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case -2:
                    dialog.dismiss();
                    return;
                case -1:
                    a.o.a(0, this.type, 0, this.order);
                    return;
                default:
                    return;
            }
        }
    }

    /* compiled from: Proguard */
    class PriceAdapter extends BaseAdapter {
        private Context mContext;
        private String[] mPrices;
        private int selectItem = -1;

        public PriceAdapter(String[] prices, Context context) {
            this.mPrices = prices;
            this.mContext = context;
        }

        public int getCount() {
            return this.mPrices.length;
        }

        public Object getItem(int position) {
            return this.mPrices[position];
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                ViewHolder viewHolder2 = new ViewHolder();
                convertView = LayoutInflater.from(this.mContext).inflate(aa.a(this.mContext, "layout", "bluep_item_price"), (ViewGroup) null);
                viewHolder2.tv_name = (TextView) convertView.findViewById(aa.a(this.mContext, "id", "tv_name"));
                viewHolder2.iv_selected = (ImageView) convertView.findViewById(aa.a(this.mContext, "id", "iv_selected"));
                viewHolder2.divider = convertView.findViewById(aa.a(this.mContext, "id", "divider"));
                convertView.setTag(viewHolder2);
                viewHolder = viewHolder2;
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.tv_name.setText(this.mPrices[position]);
            if (position == this.selectItem) {
                viewHolder.iv_selected.setVisibility(0);
            } else {
                viewHolder.iv_selected.setVisibility(8);
            }
            if (position == this.mPrices.length - 1) {
                viewHolder.divider.setVisibility(8);
            } else {
                viewHolder.divider.setVisibility(0);
            }
            return convertView;
        }

        public void setSelectItem(int selectItem2) {
            this.selectItem = selectItem2;
        }

        /* compiled from: Proguard */
        class ViewHolder {
            public View divider;
            public ImageView iv_selected;
            public TextView tv_name;

            ViewHolder() {
            }
        }
    }

    public void payBySMS(Activity activity, String transactionId, String currency, String price, int smsId, String propsName, boolean isShowDialog, IPayCallback callback) {
        String str;
        String str2;
        try {
            uploadTrans(activity, transactionId, "payBySMS|SMS");
            checkParameters(activity, callback, "", transactionId, propsName, "", "", smsId, "", Client.phoneNum());
            hasSim();
            if (currency.equals(Config.K_CURRENCY_THB) && Client.CONTRY_CODE != 66 && Client.CONTRY_CODE != 86) {
                throw new BlueException(h.i, "SIM error:currency is 'THB',SIM must be China or Thailand.", new Object[0]);
            } else if (currency.equals("IDR") && Client.CONTRY_CODE != 62 && Client.CONTRY_CODE != 86) {
                throw new BlueException(h.i, "SIM error:currency is 'IDR',SIM must be China or Indonesia.", new Object[0]);
            } else if (currency.equals(Config.K_CURRENCY_VND) && Client.CONTRY_CODE != 84 && Client.CONTRY_CODE != 86) {
                throw new BlueException(h.i, "SIM error:currency is 'VND',SIM must be China or Vietnam.", new Object[0]);
            } else if (Client.CONTRY_CODE != 86 || currency.equals(Config.K_CURRENCY_THB) || currency.equals("IDR") || currency.equals(Config.K_CURRENCY_VND) || currency.equals(Config.K_CURRENCY_TRF)) {
                c.c("begin to deal order:" + transactionId);
                String str3 = null;
                if (Client.m_DcbInfo != null) {
                    switch (Client.m_DcbInfo.b) {
                        case 1:
                            c.c("begin trade use original channel");
                            payBySMS2(activity, transactionId, currency, price, smsId, propsName, isShowDialog, callback);
                            return;
                        case 2:
                            str3 = PublisherCode.PUBLISHER_DCB;
                            break;
                        case 3:
                            str3 = PublisherCode.PUBLISHER_DCB_INDOSAT;
                            break;
                        case 4:
                        case 6:
                        case 7:
                            str3 = b.a;
                            break;
                    }
                }
                c.c("publisher:" + str3);
                int safePrice = Order.getSafePrice(currency, price);
                if (safePrice <= 0) {
                    c.b(i.a(i.a(i.E), "price=" + price + " error!"));
                    throw new BlueException(h.i, i.a(i.a(i.E), "price=" + price + " error!"), new Object[0]);
                }
                if (currency == null) {
                    str = "";
                } else {
                    str = currency;
                }
                if (propsName == null) {
                    str2 = "";
                } else {
                    str2 = propsName;
                }
                pay(activity, "", transactionId, str, str2, "", "", safePrice, smsId, str3, "", Client.phoneNum(), isShowDialog, callback);
            } else {
                throw new BlueException(h.i, "Currency error:currency " + currency + " is not support in China.", new Object[0]);
            }
        } catch (BlueException e) {
            e.printStackTrace();
            aa.a(activity, transactionId, price, e.getCode(), PublisherCode.PUBLISHER_SMS, e.getMessage());
        }
    }

    private void checkParameters(Activity activity, IPayCallback callback, String customId, String transactionId, String propsName, String cardNo, String SerialNo, int smsID, String scheme, String destMsisdn) {
        if (activity == null) {
            throw new RuntimeException("activity can not be null");
        } else if (callback == null) {
            throw new RuntimeException("callback can not be null");
        } else {
            a.a(callback);
            if (smsID < 0 || smsID > 99) {
                throw new BlueException(h.d, "smsID should just between 100 and 0", new Object[0]);
            } else if (TextUtils.isEmpty(propsName)) {
                throw new BlueException(h.d, "propsName is null", new Object[0]);
            } else if (TextUtils.isEmpty(transactionId)) {
                throw new BlueException(h.d, "transactionId is null", new Object[0]);
            } else if (transactionId.length() > 32) {
                throw new BlueException(h.d, "transactionId length more than 32", new Object[0]);
            } else if (!TextUtils.isEmpty(customId) && customId.length() > 50) {
                throw new BlueException(h.d, "customId length more than 50", new Object[0]);
            } else if (scheme.length() > 200) {
                throw new BlueException(h.d, "scheme length more than 200", new Object[0]);
            } else if (propsName.length() > 50) {
                throw new BlueException(h.d, "propsName length more than 50", new Object[0]);
            } else if (!TextUtils.isEmpty(destMsisdn) && destMsisdn.length() > 15) {
                throw new BlueException(h.d, "msisdn length more than 15", new Object[0]);
            } else if (!TextUtils.isEmpty(cardNo) && cardNo.length() > 25) {
                throw new BlueException(h.d, "cardNo length must be less  than 25", new Object[0]);
            } else if (!TextUtils.isEmpty(SerialNo) && SerialNo.length() > 20) {
                throw new BlueException(h.d, "SerialNo length must be less than 20", new Object[0]);
            }
        }
    }

    private boolean hasSim() {
        if (Client.m_hashChargeList != null && Client.m_hashChargeList.size() >= 1) {
            return true;
        }
        throw new BlueException(h.g, h.a(h.g), new Object[0]);
    }

    public void payByUI(Activity activity, String currency, String c_id, String price, String propsName, int smsId, String scheme, IPayCallback callback) {
        try {
            checkParameters(activity, callback, c_id, AppEventsConstants.EVENT_PARAM_VALUE_YES, propsName, "", "", smsId, scheme, Client.phoneNum());
            if ("0".equals(price)) {
                throw new BlueException(h.i, "price can not be 0", new Object[0]);
            }
            Integer.parseInt(price);
            Intent intent = new Intent();
            a.b = true;
            intent.setClass(activity, PayUIActivity.class);
            String str = c_id;
            int i = smsId;
            String str2 = propsName;
            String str3 = currency;
            j jVar = new j(Client.getProductId(), Client.getPromotionId(), AppEventsConstants.EVENT_PARAM_VALUE_YES, str, String.valueOf(getSafePrice(currency, price)), i, str2, str3, Client.getOperator());
            if (TextUtils.isEmpty(scheme)) {
                scheme = "bluepay://best.bluepay.asia";
            }
            jVar.j = scheme;
            intent.putExtra("entry", jVar);
            a.a(callback);
            activity.startActivity(intent);
        } catch (Exception e) {
            throw new BlueException(h.i, "parse to int error.price error:" + price, new Object[0]);
        } catch (Exception e2) {
            e2.printStackTrace();
            aa.a(activity, AppEventsConstants.EVENT_PARAM_VALUE_YES, price, h.i, "UI2", e2.getMessage());
        }
    }

    public void payByBank(Activity context, String transactionId, String currency, String price, String propsName, String publisher, boolean isShowDialog, IPayCallback callback) {
        try {
            uploadTrans(context, transactionId, "payByBank|" + publisher);
            checkParameters(context, callback, "", transactionId, propsName, "", "", 0, "", Client.phoneNum());
            if (PublisherCode.PUBLISHER_VN_BANK.equals(publisher)) {
                if (TextUtils.isEmpty(Client.m_BankChargeInfo.g)) {
                    throw new BlueException(h.i, "Bank charge not support", new Object[0]);
                } else if (Integer.parseInt(price) < 10000) {
                    throw new BlueException(h.i, "price must more than 10000", new Object[0]);
                }
            }
            if (PublisherCode.PUBLISHER_ID_BANK.equals(publisher) && TextUtils.isEmpty(Client.m_BankChargeInfo.h)) {
                throw new BlueException(h.i, "Bank charge not support", new Object[0]);
            } else if (PublisherCode.PUBLISHER_BANK.equals(publisher) && (TextUtils.isEmpty(Client.m_iIMSI) || Client.m_iIMSI.equals(Config.ERROR_C_BluePay_IMSI))) {
                throw new BlueException(h.g, h.a(h.g), new Object[0]);
            } else if (!PublisherCode.PUBLISHER_BANK.equals(publisher) || Client.m_BankChargeInfo.a) {
                int safePrice = getSafePrice(currency, price);
                if (safePrice <= 0) {
                    throw new BlueException(h.i, "price error:" + price, new Object[0]);
                }
                pay(context, "", transactionId, currency == null ? "" : currency, propsName == null ? "" : propsName, "", "", safePrice, 0, publisher, "", Client.phoneNum(), isShowDialog, callback);
            } else {
                throw new BlueException(h.i, "Bank charge not support", new Object[0]);
            }
        } catch (Exception e) {
            throw new BlueException(h.i, "parse to int error or price less than 10000.price error:" + price, new Object[0]);
        } catch (BlueException e2) {
            e2.printStackTrace();
            aa.a(context, transactionId, price, e2.getCode(), publisher, e2.getMessage());
        }
    }

    public void payByOffline(Activity context, String transactionId, String customId, String currency, String price, String propsName, String publisher, String msisdn, boolean isShowUI, IPayCallback callback) {
        try {
            uploadTrans(context, transactionId, "payByOffline|" + publisher);
            checkParameters(context, callback, customId, transactionId, propsName, "", "", 0, "", Client.phoneNum());
            int parseInt = Integer.parseInt(price);
            if (parseInt <= 0) {
                c.b(i.a(i.a(i.E), "price=" + price + " error!"));
                throw new BlueException(h.i, i.a(i.a(i.E), "price=" + price + " error!"), new Object[0]);
            } else if (parseInt < 10000) {
                throw new BlueException(h.i, "price must more than 10000", new Object[0]);
            } else {
                pay(context, customId, transactionId, currency, propsName, null, null, parseInt, 0, publisher, "", msisdn, isShowUI, callback);
            }
        } catch (Exception e) {
            throw new BlueException(h.i, "parse to int error.price error:" + price, new Object[0]);
        } catch (BlueException e2) {
            e2.printStackTrace();
            aa.a(context, transactionId, price, e2.getCode(), publisher, e2.getMessage());
        }
    }

    public void queryTrans(Activity context, IPayCallback callback, String tId, String publisher, int num) {
        aa.a(context, callback, tId, publisher, num);
    }

    public void payByWallet(Activity context, String customId, String transactionId, String currency, String price, String propsName, String publisher, String scheme, boolean isShowDialog, IPayCallback callback) {
        try {
            uploadTrans(context, transactionId, "payByWallet|" + publisher);
            checkParameters(context, callback, customId, transactionId, propsName, "", "", 0, scheme, Client.phoneNum());
            int safePrice = getSafePrice(currency, price);
            if (safePrice <= 0) {
                throw new BlueException(h.i, "price error;" + price, new Object[0]);
            }
            pay(context, customId, transactionId, currency, propsName, null, null, safePrice, 0, publisher, scheme, Client.phoneNum(), isShowDialog, callback);
        } catch (BlueException e) {
            e.printStackTrace();
            aa.a(context, transactionId, price, h.i, publisher, e.getMessage());
        }
    }

    private int getSafePrice(String currency, String price) {
        try {
            return Integer.parseInt(price);
        } catch (Exception e) {
            return Order.getSafePrice(currency, price);
        }
    }
}
