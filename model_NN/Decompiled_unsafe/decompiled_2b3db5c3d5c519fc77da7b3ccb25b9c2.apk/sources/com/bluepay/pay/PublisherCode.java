package com.bluepay.pay;

/* compiled from: Proguard */
public class PublisherCode {
    public static final String[][] PUBLISHERS_CONTRY = {new String[]{PUBLISHER_BANK, PUBLISHER_LINE, PUBLISHER_12CALL, PUBLISHER_TRUEMONEY, PUBLISHER_HAPPY}, new String[]{PUBLISHER_VIETTEL, PUBLISHER_MOBIFONE, PUBLISHER_VINAPHONE, PUBLISHER_VTC}, new String[]{PUBLISHER_UNIPIN, PUBLISHER_MOGPLAY, PUBLISHER_OFFLINE_ATM, PUBLISHER_OFFLINE_OTC}};
    public static final String PUBLISHER_12CALL = "12call";
    public static final String PUBLISHER_BANK = "bankcharge";
    public static final String PUBLISHER_BLUECOIN = "bluecoins";
    public static final String PUBLISHER_DCB = "dcb";
    public static final String PUBLISHER_DCB_INDOSAT = "dcb_indosat";
    public static final String PUBLISHER_DCB_SMARTFREN = "dcb_smartfren";
    public static final String PUBLISHER_DCB_TELENOR = "telenor";
    public static final String PUBLISHER_DCB_XL = "dcb_xl";
    public static final String PUBLISHER_HAPPY = "happy";
    @Deprecated
    public static final String PUBLISHER_HOPE = "hope";
    public static final String PUBLISHER_IDR = "IDR";
    public static final String PUBLISHER_ID_BANK = "idbank";
    public static final String PUBLISHER_LINE = "line";
    public static final String PUBLISHER_LYTOCARD = "lytocard";
    public static final String PUBLISHER_MOBIFONE = "mobifone";
    public static final String PUBLISHER_MOGPLAY = "indomog";
    public static final String PUBLISHER_OFFLINE_ATM = "atm";
    public static final String PUBLISHER_OFFLINE_OTC = "otc";
    public static final String PUBLISHER_SMS = "SMS";
    public static final String PUBLISHER_TRUEMONEY = "truemoney";
    @Deprecated
    public static final String PUBLISHER_UNIPIN = "unipin";
    public static final String PUBLISHER_VIETTEL = "viettel";
    public static final String PUBLISHER_VINAPHONE = "vinaphone";
    public static final String PUBLISHER_VN_BANK = "vnbank";
    public static final String PUBLISHER_VTC = "vtc";
}
