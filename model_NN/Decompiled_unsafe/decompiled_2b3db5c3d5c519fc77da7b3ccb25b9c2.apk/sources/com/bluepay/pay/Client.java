package com.bluepay.pay;

import android.app.Activity;
import android.content.Context;
import android.os.Binder;
import android.os.Build;
import android.os.Process;
import android.util.Log;
import com.bluepay.data.Config;
import com.bluepay.data.a;
import com.bluepay.data.c;
import com.bluepay.data.f;
import com.bluepay.data.k;
import com.bluepay.data.l;
import com.bluepay.interfaceClass.BlueInitCallback;
import com.bluepay.sdk.c.aa;
import com.bluepay.sdk.c.d;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: Proguard */
public class Client {
    public static String ApplicationName = null;
    public static int CONTRY_CODE = 0;
    public static final int C_USER_CANCEL = 603;
    public static boolean IS_DEBUG = false;
    public static final String TAG = "BluePay";
    public static String dayLimit = "";
    public static String lan;
    public static a m_BankChargeInfo;
    public static c m_BlueWallet;
    public static f m_DcbInfo;
    public static String m_Model = null;
    /* access modifiers changed from: private */
    public static boolean m_bHasInit = false;
    private static boolean m_bIsSetRef = false;
    public static List m_exchangeList = null;
    public static HashMap m_hashChargeList = null;
    public static HashMap m_hashChargeList1 = null;
    public static HashMap m_hashChargeList2 = null;
    public static List m_hashPriceList = null;
    public static LinkedHashMap m_hashProductList = null;
    public static HashMap m_hashTRFPriceList = null;
    public static String m_iIMEI = null;
    public static String m_iIMSI = null;
    public static String m_iIMSI1 = null;
    public static String m_iIMSI2 = null;
    public static int m_iOperatorId = 0;
    public static int m_iOperatorId1 = 0;
    public static int m_iOperatorId2 = 0;
    public static String m_iWifiType = Config.NETWORKTYPE_INVALID;
    /* access modifiers changed from: private */
    public static l m_pRefNode = null;
    /* access modifiers changed from: private */
    public static String m_sEncryptKey = "";
    private static String m_sPhoneNumber = "";
    private static String m_sUID;
    public static List m_uploadErrorCode = null;
    public static String[] productNameList = null;
    public static String scheme;
    public static String telcoName;
    public static String telcoName1;
    public static String telcoName2;

    public static boolean init(Activity activity, BlueInitCallback callback) {
        if (activity == null || callback == null) {
            aa.b(activity, aa.e(), "BluePay init error|Reason:Activit or callback is null", m_iIMSI, 14);
            throw new IllegalArgumentException("Activit or callback can not be null");
        }
        com.bluepay.sdk.b.c.c("||---- BluePay init() ---- Start ----||");
        lan = aa.l(activity);
        com.bluepay.sdk.b.c.c(lan);
        try {
            m_sUID = com.bluepay.sdk.b.a.a(activity);
            m_Model = Build.MODEL;
            ApplicationName = aa.i(activity);
            IS_DEBUG = aa.g();
            if (!aa.c(activity, "android.permission.READ_PHONE_STATE")) {
                Log.e(TAG, "READ_PHONE_STATE have not granted,Please authorize read phone state permission and try again.");
                aa.b(activity, aa.e(), "BluePay init error|Phone Model:" + m_Model + "|Reason:READ_PHONE_STATE have not granted", "", 14);
            }
            com.bluepay.a.b.a.a(activity);
            if (!setRefInfo(activity, "BluePay.ref")) {
                aa.a(activity, "Have no BluePay.ref or ref file configure error");
                com.bluepay.sdk.b.c.c("||---- BluePay init() --Error-- End ----||");
                if (callback == null) {
                    return false;
                }
                callback.initComplete("404", "miss file BluePay.ref");
                return false;
            }
            m_iIMSI1 = aa.b((Context) activity);
            m_iIMSI = m_iIMSI1;
            m_iIMEI = aa.d(activity);
            com.bluepay.sdk.b.c.c("imsi:" + m_iIMSI + "  imei:" + m_iIMEI);
            if (m_iIMSI == null || m_iIMSI.trim().length() < 5) {
                m_iIMSI = Config.ERROR_C_BluePay_IMSI;
            }
            m_iOperatorId = 0;
            m_iWifiType = aa.a((Context) activity);
            com.bluepay.sdk.b.c.c("productId=" + m_pRefNode.a() + ", promotionId=" + m_pRefNode.c() + "operatorId=" + m_iOperatorId + "IMSI= " + m_iIMSI);
            com.bluepay.sdk.b.c.c(" start ------ get the operator and other paramter! ----||");
            initClientData(activity, callback);
            com.bluepay.sdk.b.c.c(" End ------ get the operator and other paramter! ----||");
            com.bluepay.sdk.b.c.c("||---- BluePay init() ---- End ----||");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            m_bHasInit = false;
            StackTraceElement[] stackTrace = e.getStackTrace();
            StringBuilder sb = new StringBuilder();
            for (StackTraceElement stackTraceElement : stackTrace) {
                sb.append(stackTraceElement.toString()).append("\n");
            }
            aa.b(activity, aa.e(), "BluePay init error|Phone Model:" + m_Model + "|Reason:" + sb.toString(), m_iIMSI, 14);
            if (callback != null) {
                callback.initComplete("404", "init()---" + e.getMessage());
            }
            com.bluepay.sdk.b.c.c("||---- BluePay init() ---- End ----||");
            return false;
        }
    }

    private static boolean setRefInfo(Activity activity, String refFileName) {
        int i = 0;
        if (m_bIsSetRef) {
            return true;
        }
        if (m_hashProductList == null) {
            m_hashProductList = new LinkedHashMap();
        }
        if (m_exchangeList == null) {
            m_exchangeList = new ArrayList();
        }
        try {
            if (m_pRefNode == null) {
                m_pRefNode = new l(activity, refFileName);
            }
            m_sEncryptKey = aa.a(m_pRefNode.d(), d.a);
            if (m_sEncryptKey == null) {
                return false;
            }
            if (m_hashProductList.size() > 1) {
                if (productNameList == null) {
                    productNameList = new String[(m_hashProductList.size() - 1)];
                }
                for (Object next : m_hashProductList.keySet()) {
                    if (!next.equals(TAG)) {
                        productNameList[i] = next.toString();
                        i++;
                    }
                }
            }
            return true;
        } catch (XmlPullParserException e) {
            com.bluepay.sdk.b.c.b("Ill-formatted " + refFileName + " file");
            return false;
        } catch (IOException e2) {
            com.bluepay.sdk.b.c.b("Unable to read " + refFileName + " file");
            return false;
        } catch (Exception e3) {
            com.bluepay.sdk.b.c.b(e3.toString());
            return false;
        }
    }

    public static void setRefInfo(String _language, String _ekKey, int _productId, String _promotionId, ArrayList arrayList) {
        int i = 0;
        if (m_pRefNode == null) {
            m_pRefNode = new l(_language.trim(), _productId, _promotionId);
        }
        m_sEncryptKey = aa.a(_ekKey, d.a);
        if (m_hashProductList == null) {
            m_hashProductList = new LinkedHashMap();
        }
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            k kVar = (k) arrayList.get(i2);
            m_hashProductList.put(kVar.a(), kVar.c());
            if (kVar.f() && m_hashProductList.get(TAG) == null) {
                m_hashProductList.put(TAG, kVar.a());
            }
        }
        if (m_hashProductList.size() > 1) {
            if (productNameList == null) {
                productNameList = new String[(m_hashProductList.size() - 1)];
            }
            for (Object next : m_hashProductList.keySet()) {
                if (!next.equals(TAG)) {
                    productNameList[i] = next.toString();
                    i++;
                }
            }
        }
        m_bIsSetRef = true;
    }

    public static void exit() {
        com.bluepay.a.b.a.a((IPayCallback) null);
        m_BankChargeInfo = null;
        m_hashChargeList = null;
        m_hashPriceList = null;
        m_hashProductList = null;
        m_hashTRFPriceList = null;
        m_pRefNode = null;
        m_bHasInit = false;
    }

    /* access modifiers changed from: package-private */
    public boolean checkCallingPermission(Context mContext, String permission, String func) {
        if (Binder.getCallingPid() == Process.myPid() || mContext.checkCallingPermission(permission) == 0) {
            return true;
        }
        return false;
    }

    public static void initClientData(final Activity activity, final BlueInitCallback callback) {
        new Thread(new Runnable() {
            public void run() {
                final String str = null;
                try {
                    com.bluepay.sdk.b.c.c("step1:  check the phone local in which country! ");
                    int i = 0;
                    final String str2 = null;
                    while (true) {
                        if (i >= 4) {
                            i = 0;
                            break;
                        }
                        str2 = ClientHelper.initPay(activity, Client.m_iIMSI, Client.m_iIMEI, Client.lan, Client.m_pRefNode.c(), Client.m_sEncryptKey, i);
                        if (str2.equals("200")) {
                            com.bluepay.a.b.a.a(activity, com.bluepay.a.b.a.d, com.bluepay.a.b.a.c);
                            boolean unused = Client.m_bHasInit = true;
                            str = "initPay succuess";
                            break;
                        }
                        str = "initPay failed";
                        i++;
                    }
                    com.bluepay.sdk.b.c.c("step2:  check the network and use which state === " + i);
                    if (Client.m_bHasInit) {
                        com.bluepay.sdk.b.c.c("step2:  check ok !");
                    } else {
                        com.bluepay.sdk.b.c.c("step2:  check failed !");
                    }
                    if (activity != null) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                if (callback != null) {
                                    callback.initComplete(str2, str);
                                }
                            }
                        });
                        try {
                            aa.m(activity);
                        } catch (Exception e) {
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    StackTraceElement[] stackTrace = e2.getStackTrace();
                    StringBuilder sb = new StringBuilder();
                    for (StackTraceElement stackTraceElement : stackTrace) {
                        sb.append(stackTraceElement.toString()).append("\n");
                    }
                    aa.b(activity, aa.e(), "BluePay init error|Phone Model:" + Client.m_Model + "|Reason:" + sb.toString(), Client.m_iIMSI, 14);
                    com.bluepay.sdk.b.c.b("final !!!! :  ERROR!! " + e2.getMessage());
                    if (callback != null) {
                        callback.initComplete("404", "final !!!! :  ERROR!! " + e2.getMessage());
                    }
                }
            }
        }).start();
    }

    public static int getOperator() {
        return m_iOperatorId;
    }

    public static String getUid() {
        return m_sUID;
    }

    public static String getIMEI() {
        return m_iIMEI;
    }

    public static String getIMSI() {
        return m_iIMSI;
    }

    public static String getEncrypt() {
        if (m_sEncryptKey == null) {
            return "";
        }
        return m_sEncryptKey;
    }

    public static int getProductId() {
        if (m_pRefNode == null) {
            return 0;
        }
        return m_pRefNode.a();
    }

    public static String getPromotionId() {
        if (m_pRefNode == null) {
            return Config.ERROR_C_BluePay_PromotionID;
        }
        return m_pRefNode.c();
    }

    public static String getLocalLan() {
        if (m_pRefNode == null) {
            return Config.ERROR_C_BluePay_LAN;
        }
        return m_pRefNode.b();
    }

    public static String phoneNum() {
        return m_sPhoneNumber;
    }

    public static void setMsNum(String num) {
        m_sPhoneNumber = num;
    }

    static boolean hasInit() {
        return m_bHasInit;
    }
}
