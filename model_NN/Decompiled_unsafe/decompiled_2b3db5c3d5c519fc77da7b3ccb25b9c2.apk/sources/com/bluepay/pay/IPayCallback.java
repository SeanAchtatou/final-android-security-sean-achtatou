package com.bluepay.pay;

import java.io.Serializable;

/* compiled from: Proguard */
public abstract class IPayCallback implements Serializable {
    public static final long serialVersionUID = -5761748248140226646L;

    public abstract void onFinished(BlueMessage blueMessage);

    public abstract String onPrepared();
}
