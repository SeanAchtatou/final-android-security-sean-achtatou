package com.bluepay.sdk.a;

import java.net.InetAddress;
import java.net.Socket;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/* compiled from: Proguard */
public class d extends SSLSocketFactory {
    private static final String a = "TlsOnlySocketFactory";
    private final SSLSocketFactory b;
    private final boolean c;

    public d() {
        this.b = HttpsURLConnection.getDefaultSSLSocketFactory();
        this.c = false;
    }

    public d(SSLSocketFactory sSLSocketFactory) {
        this.b = sSLSocketFactory;
        this.c = false;
    }

    public d(SSLSocketFactory sSLSocketFactory, boolean z) {
        this.b = sSLSocketFactory;
        this.c = z;
    }

    public String[] getDefaultCipherSuites() {
        return this.b.getDefaultCipherSuites();
    }

    public String[] getSupportedCipherSuites() {
        return this.b.getSupportedCipherSuites();
    }

    private Socket a(Socket socket) {
        if (socket instanceof SSLSocket) {
            return new g(this, (SSLSocket) socket, this.c);
        }
        return socket;
    }

    public Socket createSocket(Socket s, String host, int port, boolean autoClose) {
        return a(this.b.createSocket(s, host, port, autoClose));
    }

    public Socket createSocket(String host, int port) {
        return a(this.b.createSocket(host, port));
    }

    public Socket createSocket(String host, int port, InetAddress localHost, int localPort) {
        return a(this.b.createSocket(host, port, localHost, localPort));
    }

    public Socket createSocket(InetAddress host, int port) {
        return a(this.b.createSocket(host, port));
    }

    public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) {
        return a(this.b.createSocket(address, port, localAddress, localPort));
    }
}
