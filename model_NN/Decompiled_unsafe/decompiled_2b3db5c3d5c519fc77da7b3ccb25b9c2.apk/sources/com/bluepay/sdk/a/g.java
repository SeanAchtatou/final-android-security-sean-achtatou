package com.bluepay.sdk.a;

import android.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import javax.net.ssl.SSLSocket;

/* compiled from: Proguard */
class g extends f {
    final boolean c;
    final /* synthetic */ d d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private g(d dVar, SSLSocket sSLSocket, boolean z) {
        super(dVar, sSLSocket);
        this.d = dVar;
        this.c = z;
        if (z) {
            ArrayList arrayList = new ArrayList(Arrays.asList(sSLSocket.getEnabledProtocols()));
            arrayList.remove("SSLv2");
            arrayList.remove("SSLv3");
            super.setEnabledProtocols((String[]) arrayList.toArray(new String[arrayList.size()]));
            ArrayList arrayList2 = new ArrayList(10);
            Pattern compile = Pattern.compile(".*(EXPORT|NULL).*");
            for (String str : sSLSocket.getEnabledCipherSuites()) {
                if (!compile.matcher(str).matches()) {
                    arrayList2.add(str);
                }
            }
            super.setEnabledCipherSuites((String[]) arrayList2.toArray(new String[arrayList2.size()]));
            return;
        }
        ArrayList arrayList3 = new ArrayList(Arrays.asList(sSLSocket.getSupportedProtocols()));
        arrayList3.remove("SSLv2");
        arrayList3.remove("SSLv3");
        super.setEnabledProtocols((String[]) arrayList3.toArray(new String[arrayList3.size()]));
        ArrayList arrayList4 = new ArrayList(10);
        Pattern compile2 = Pattern.compile(".*(_DES|DH_|DSS|EXPORT|MD5|NULL|RC4).*");
        for (String str2 : sSLSocket.getSupportedCipherSuites()) {
            if (!compile2.matcher(str2).matches()) {
                arrayList4.add(str2);
            }
        }
        super.setEnabledCipherSuites((String[]) arrayList4.toArray(new String[arrayList4.size()]));
    }

    public void setEnabledProtocols(String[] protocols) {
        List asList;
        if (protocols != null && protocols.length == 1 && "SSLv3".equals(protocols[0])) {
            if (this.c) {
                asList = Arrays.asList(this.a.getEnabledProtocols());
            } else {
                asList = Arrays.asList(this.a.getSupportedProtocols());
            }
            ArrayList arrayList = new ArrayList(asList);
            if (arrayList.size() > 1) {
                arrayList.remove("SSLv2");
                arrayList.remove("SSLv3");
            } else {
                Log.w("TlsOnlySocketFactory", "SSL stuck with protocol available for " + String.valueOf(arrayList));
            }
            protocols = (String[]) arrayList.toArray(new String[arrayList.size()]);
        }
        super.setEnabledProtocols(protocols);
    }
}
