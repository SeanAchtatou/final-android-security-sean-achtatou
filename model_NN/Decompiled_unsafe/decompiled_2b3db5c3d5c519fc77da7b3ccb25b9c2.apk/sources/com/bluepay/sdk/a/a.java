package com.bluepay.sdk.a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Process;
import com.bluepay.data.h;
import com.bluepay.data.i;
import com.bluepay.interfaceClass.b;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.exception.BlueException;
import com.tencent.android.tpush.common.Constants;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.protocol.HTTP;

/* compiled from: Proguard */
public class a {
    private static String a(String str, String str2, Map map) {
        StringBuilder append = new StringBuilder(str).append("?").append(str2);
        if (map != null) {
            try {
                append.append("&").append("encrypt=").append(map.get("encrypt"));
            } catch (Exception e) {
                c.b("Http get ERROR! getUrl 1 " + i.a((byte) i.D, e.getMessage()));
                throw new BlueException(e, h.i, i.a(i.D), e.getMessage());
            }
        }
        return append.toString();
    }

    private static String a(String str, String str2, Map map, boolean z) {
        StringBuilder sb = new StringBuilder("");
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                String str3 = (String) entry.getKey();
                Object value = entry.getValue();
                if (value != null) {
                    try {
                        if (!"encrypt".equals(str3)) {
                            c.c("url = " + str + " url_content = " + str2);
                        }
                        sb.append("&").append(str3).append("=").append(URLEncoder.encode(String.valueOf(value), HTTP.UTF_8));
                    } catch (UnsupportedEncodingException e) {
                        c.b("Http get ERROR! getUrl " + i.a((byte) i.D, e.getMessage()));
                        throw new BlueException(e, h.i, i.a(i.D), e.getMessage());
                    }
                }
            }
        }
        StringBuilder append = new StringBuilder(str).append(str2);
        if (!z) {
            append.append("?1=1").append((CharSequence) sb);
        } else {
            append.append("?").append(sb.substring(1));
        }
        return append.toString();
    }

    public static b a(Context context, String str, String str2, Map map) {
        if (context.checkPermission("android.permission.ACCESS_NETWORK_STATE", Process.myPid(), Process.myUid()) == -1) {
            throw new BlueException(h.d, "network permission denied", new Object[0]);
        }
        String a = a(str, str2, map);
        c.c(" urls " + a);
        return a(a);
    }

    public static b b(Context context, String str, String str2, Map map) {
        if (context.checkPermission("android.permission.ACCESS_NETWORK_STATE", Process.myPid(), Process.myUid()) == -1) {
            throw new BlueException(h.d, "network permission denied", new Object[0]);
        }
        StringBuilder append = new StringBuilder(str).append("?").append(str2);
        if (map != null) {
        }
        return a(append.toString());
    }

    private static b a(String str) {
        HttpURLConnection c;
        try {
            if (b(str)) {
                c = b(str, HttpGet.METHOD_NAME, null);
            } else {
                c = c(str, HttpGet.METHOD_NAME, null);
            }
            int responseCode = c.getResponseCode();
            if (responseCode == 200) {
                String a = a(c.getInputStream());
                c.disconnect();
                return new b(responseCode, a);
            }
            c.c("http Get ERROR! error code:" + c.getResponseCode());
            throw new BlueException(h.i, "connection error " + c.getResponseCode(), new Object[0]);
        } catch (Exception e) {
            e.printStackTrace();
            c.c("http Get ERROR! :" + e.getMessage());
            throw new BlueException(h.n, "connection error", new Object[0]);
        }
    }

    private static b c(String str, Map map) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setRequestMethod(HttpPost.METHOD_NAME);
            httpURLConnection.setConnectTimeout(Constants.ERRORCODE_UNKNOWN);
            httpURLConnection.setUseCaches(false);
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            StringBuilder sb = new StringBuilder();
            if (map != null) {
                for (Map.Entry entry : map.entrySet()) {
                    Object value = entry.getValue();
                    if (value != null) {
                        sb.append("&" + ((String) entry.getKey()) + "=" + URLEncoder.encode(value.toString(), HTTP.UTF_8));
                    }
                }
            }
            String substring = sb.toString().substring(1);
            c.c(substring);
            dataOutputStream.writeBytes(substring);
            dataOutputStream.flush();
            dataOutputStream.close();
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200) {
                String a = a(httpURLConnection.getInputStream());
                httpURLConnection.disconnect();
                return new b(responseCode, a);
            }
            c.c("http POST ERROR! error code:" + httpURLConnection.getResponseCode());
            throw new BlueException(h.i, "connection error", new Object[0]);
        } catch (Exception e) {
            c.c("http POST ERROR! :" + e.getMessage());
            throw new BlueException(h.i, "connection error", new Object[0]);
        }
    }

    private static String a(InputStream inputStream) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                stringBuffer.append(new String(bArr, 0, read));
            }
            inputStream.close();
        } catch (IOException e) {
            c.b("io error:" + e.getMessage());
        }
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.a.a.a(java.lang.String, java.lang.String, java.util.Map, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, java.util.Map, int]
     candidates:
      com.bluepay.sdk.a.a.a(android.content.Context, java.lang.String, java.lang.String, java.util.Map):com.bluepay.interfaceClass.b
      com.bluepay.sdk.a.a.a(java.lang.String, java.lang.String, java.util.Map, boolean):java.lang.String */
    public static synchronized b a(String str, Map map) {
        b a;
        synchronized (a.class) {
            String a2 = a(str, "", map, false);
            c.c(a2);
            a = a(a2);
        }
        return a;
    }

    public static b b(String str, Map map) {
        return c(str, map);
    }

    @SuppressLint({"TrulyRandom"})
    private static HttpURLConnection b(String str, String str2, Map map) {
        try {
            TrustManager[] trustManagerArr = {new c(null)};
            SSLContext instance = SSLContext.getInstance("SSL");
            instance.init(null, trustManagerArr, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(new d(instance.getSocketFactory()));
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) new URL(str).openConnection();
            httpsURLConnection.setHostnameVerifier(new b());
            httpsURLConnection.setConnectTimeout(15000);
            httpsURLConnection.setReadTimeout(15000);
            httpsURLConnection.setRequestMethod(str2);
            httpsURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpsURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; Android 5.0; Grand Build/LRX21M) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile MQQBrowser/6.8 TBS/036824 Safari/537.36");
            if (map != null && !map.isEmpty()) {
                for (Map.Entry entry : map.entrySet()) {
                    httpsURLConnection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
                }
            }
            httpsURLConnection.setDoOutput(true);
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.connect();
            return httpsURLConnection;
        } catch (Exception e) {
            c.c("init https error");
            e.printStackTrace();
            throw new BlueException(h.i, h.a(h.i), new Object[0]);
        }
    }

    private static HttpURLConnection c(String str, String str2, Map map) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setConnectTimeout(15000);
            httpURLConnection.setReadTimeout(15000);
            httpURLConnection.setRequestMethod(str2);
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            httpURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Linux; Android 5.0; Grand Build/LRX21M) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/37.0.0.0 Mobile MQQBrowser/6.8 TBS/036824 Safari/537.36");
            if (map != null && !map.isEmpty()) {
                for (Map.Entry entry : map.entrySet()) {
                    httpURLConnection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
                }
            }
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            return httpURLConnection;
        } catch (Exception e) {
            c.c("init http error");
            e.printStackTrace();
            throw new BlueException(h.i, h.a(h.i), new Object[0]);
        }
    }

    private static boolean b(String str) {
        return str.startsWith("https");
    }
}
