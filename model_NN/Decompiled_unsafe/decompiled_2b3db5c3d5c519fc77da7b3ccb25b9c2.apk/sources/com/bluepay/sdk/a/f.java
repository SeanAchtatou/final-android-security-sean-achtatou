package com.bluepay.sdk.a;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.nio.channels.SocketChannel;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

/* compiled from: Proguard */
public class f extends SSLSocket {
    protected final SSLSocket a;
    final /* synthetic */ d b;

    f(d dVar, SSLSocket sSLSocket) {
        this.b = dVar;
        this.a = sSLSocket;
    }

    public String[] getSupportedCipherSuites() {
        return this.a.getSupportedCipherSuites();
    }

    public String[] getEnabledCipherSuites() {
        return this.a.getEnabledCipherSuites();
    }

    public void setEnabledCipherSuites(String[] suites) {
        this.a.setEnabledCipherSuites(suites);
    }

    public String[] getSupportedProtocols() {
        return this.a.getSupportedProtocols();
    }

    public String[] getEnabledProtocols() {
        return this.a.getEnabledProtocols();
    }

    public void setEnabledProtocols(String[] protocols) {
        this.a.setEnabledProtocols(protocols);
    }

    public SSLSession getSession() {
        return this.a.getSession();
    }

    public void addHandshakeCompletedListener(HandshakeCompletedListener listener) {
        this.a.addHandshakeCompletedListener(listener);
    }

    public void removeHandshakeCompletedListener(HandshakeCompletedListener listener) {
        this.a.removeHandshakeCompletedListener(listener);
    }

    public void startHandshake() {
        this.a.startHandshake();
    }

    public void setUseClientMode(boolean mode) {
        this.a.setUseClientMode(mode);
    }

    public boolean getUseClientMode() {
        return this.a.getUseClientMode();
    }

    public void setNeedClientAuth(boolean need) {
        this.a.setNeedClientAuth(need);
    }

    public void setWantClientAuth(boolean want) {
        this.a.setWantClientAuth(want);
    }

    public boolean getNeedClientAuth() {
        return this.a.getNeedClientAuth();
    }

    public boolean getWantClientAuth() {
        return this.a.getWantClientAuth();
    }

    public void setEnableSessionCreation(boolean flag) {
        this.a.setEnableSessionCreation(flag);
    }

    public boolean getEnableSessionCreation() {
        return this.a.getEnableSessionCreation();
    }

    public void bind(SocketAddress localAddr) {
        this.a.bind(localAddr);
    }

    public synchronized void close() {
        this.a.close();
    }

    public void connect(SocketAddress remoteAddr) {
        this.a.connect(remoteAddr);
    }

    public void connect(SocketAddress remoteAddr, int timeout) {
        this.a.connect(remoteAddr, timeout);
    }

    public SocketChannel getChannel() {
        return this.a.getChannel();
    }

    public InetAddress getInetAddress() {
        return this.a.getInetAddress();
    }

    public InputStream getInputStream() {
        return this.a.getInputStream();
    }

    public boolean getKeepAlive() {
        return this.a.getKeepAlive();
    }

    public InetAddress getLocalAddress() {
        return this.a.getLocalAddress();
    }

    public int getLocalPort() {
        return this.a.getLocalPort();
    }

    public SocketAddress getLocalSocketAddress() {
        return this.a.getLocalSocketAddress();
    }

    public boolean getOOBInline() {
        return this.a.getOOBInline();
    }

    public OutputStream getOutputStream() {
        return this.a.getOutputStream();
    }

    public int getPort() {
        return this.a.getPort();
    }

    public synchronized int getReceiveBufferSize() {
        return this.a.getReceiveBufferSize();
    }

    public SocketAddress getRemoteSocketAddress() {
        return this.a.getRemoteSocketAddress();
    }

    public boolean getReuseAddress() {
        return this.a.getReuseAddress();
    }

    public synchronized int getSendBufferSize() {
        return this.a.getSendBufferSize();
    }

    public int getSoLinger() {
        return this.a.getSoLinger();
    }

    public synchronized int getSoTimeout() {
        return this.a.getSoTimeout();
    }

    public boolean getTcpNoDelay() {
        return this.a.getTcpNoDelay();
    }

    public int getTrafficClass() {
        return this.a.getTrafficClass();
    }

    public boolean isBound() {
        return this.a.isBound();
    }

    public boolean isClosed() {
        return this.a.isClosed();
    }

    public boolean isConnected() {
        return this.a.isConnected();
    }

    public boolean isInputShutdown() {
        return this.a.isInputShutdown();
    }

    public boolean isOutputShutdown() {
        return this.a.isOutputShutdown();
    }

    public void sendUrgentData(int value) {
        this.a.sendUrgentData(value);
    }

    public void setKeepAlive(boolean keepAlive) {
        this.a.setKeepAlive(keepAlive);
    }

    public void setOOBInline(boolean oobinline) {
        this.a.setOOBInline(oobinline);
    }

    public void setPerformancePreferences(int connectionTime, int latency, int bandwidth) {
        this.a.setPerformancePreferences(connectionTime, latency, bandwidth);
    }

    public synchronized void setReceiveBufferSize(int size) {
        this.a.setReceiveBufferSize(size);
    }

    public void setReuseAddress(boolean reuse) {
        this.a.setReuseAddress(reuse);
    }

    public synchronized void setSendBufferSize(int size) {
        this.a.setSendBufferSize(size);
    }

    public void setSoLinger(boolean on, int timeout) {
        this.a.setSoLinger(on, timeout);
    }

    public synchronized void setSoTimeout(int timeout) {
        this.a.setSoTimeout(timeout);
    }

    public void setTcpNoDelay(boolean on) {
        this.a.setTcpNoDelay(on);
    }

    public void setTrafficClass(int value) {
        this.a.setTrafficClass(value);
    }

    public void shutdownInput() {
        this.a.shutdownInput();
    }

    public void shutdownOutput() {
        this.a.shutdownOutput();
    }

    public String toString() {
        return this.a.toString();
    }

    public boolean equals(Object o) {
        return this.a.equals(o);
    }
}
