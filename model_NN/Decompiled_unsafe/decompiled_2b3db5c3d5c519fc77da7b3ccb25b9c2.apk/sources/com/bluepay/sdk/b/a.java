package com.bluepay.sdk.b;

import android.content.Context;
import com.bluepay.pay.ClientHelper;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;

/* compiled from: Proguard */
public class a {
    private static String a = null;
    private static final String b = "jmtInstallation";

    public static synchronized String a(Context context) {
        String str;
        synchronized (a.class) {
            if (a == null) {
                File file = new File(context.getFilesDir(), b);
                try {
                    if (!file.exists()) {
                        b(file);
                    }
                    a = a(file);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            str = a;
        }
        return str;
    }

    private static String a(File file) {
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
        byte[] bArr = new byte[((int) randomAccessFile.length())];
        randomAccessFile.readFully(bArr);
        randomAccessFile.close();
        return new String(bArr);
    }

    private static void b(File file) {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(ClientHelper.generateTid().getBytes());
        fileOutputStream.close();
    }
}
