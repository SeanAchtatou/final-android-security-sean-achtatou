package com.bluepay.sdk.b;

import android.content.Context;
import android.content.SharedPreferences;

/* compiled from: Proguard */
public class b {
    private static final String a = "share";
    private static SharedPreferences b;
    private static SharedPreferences.Editor c;

    public static void a(Context context) {
        b = context.getSharedPreferences("share", 0);
    }

    public static void a(Context context, String str) {
        b = context.getSharedPreferences(str, 0);
        c = b.edit();
    }

    public static void a(String str, String str2) {
        c.putString(str, str2);
    }

    public static void a(String str, boolean z) {
        c.putBoolean(str, z);
    }

    public static void a(String str, int i) {
        c.putInt(str, i);
    }

    public static String b(String str, String str2) {
        return b.getString(str, str2);
    }

    public static boolean b(String str, boolean z) {
        return b.getBoolean(str, z);
    }

    public static int b(String str, int i) {
        return b.getInt(str, i);
    }

    public static void a() {
        c.commit();
    }
}
