package com.bluepay.sdk.b;

import android.text.TextUtils;
import android.util.Log;
import com.bluepay.sdk.c.aa;

/* compiled from: Proguard */
public class c {
    public static String a = "";
    public static boolean b = false;
    public static boolean c = false;
    public static boolean d = false;
    public static boolean e = false;
    public static boolean f = false;
    public static boolean g = false;
    public static d h = null;
    private static boolean i = false;
    private static final String j = "BluePay";

    public static void a() {
        i = aa.g();
        b = i;
        c = i;
        d = i;
        e = i;
        f = i;
        g = i;
    }

    public static void b() {
        i = false;
        b = i;
        c = i;
        d = i;
        e = i;
        f = i;
        g = i;
    }

    private static String a(StackTraceElement stackTraceElement) {
        String className = stackTraceElement.getClassName();
        String format = String.format("%s.%s(L:%d)", className.substring(className.lastIndexOf(".") + 1), stackTraceElement.getMethodName(), Integer.valueOf(stackTraceElement.getLineNumber()));
        if (TextUtils.isEmpty(a)) {
            return format;
        }
        return a + ":" + format;
    }

    public static StackTraceElement c() {
        return Thread.currentThread().getStackTrace()[3];
    }

    public static StackTraceElement d() {
        return Thread.currentThread().getStackTrace()[4];
    }

    public static void a(String str) {
        if (b) {
            if (h != null) {
                h.a("BluePay", str);
            } else {
                Log.d("BluePay", str);
            }
        }
    }

    public static void a(String str, Throwable th) {
        if (b) {
            if (h != null) {
                h.a("BluePay", str, th);
            } else {
                Log.d("BluePay", str, th);
            }
        }
    }

    public static void b(String str) {
        if (c) {
            if (h != null) {
                h.b("BluePay", str);
            } else {
                Log.e("BluePay", str);
            }
        }
    }

    public static void b(String str, Throwable th) {
        if (c) {
            if (h != null) {
                h.b("BluePay", str, th);
            } else {
                Log.e("BluePay", str, th);
            }
        }
    }

    public static void c(String str) {
        if (d) {
            if (h != null) {
                h.c("BluePay", str);
            } else {
                Log.i("BluePay", str);
            }
        }
    }

    public static void c(String str, Throwable th) {
        if (d) {
            if (h != null) {
                h.c("BluePay", str, th);
            } else {
                Log.i("BluePay", str, th);
            }
        }
    }

    public static void d(String str) {
        if (e) {
            if (h != null) {
                h.d("BluePay", str);
            } else {
                Log.v("BluePay", str);
            }
        }
    }

    public static void d(String str, Throwable th) {
        if (e) {
            if (h != null) {
                h.d("BluePay", str, th);
            } else {
                Log.v("BluePay", str, th);
            }
        }
    }

    public static void e(String str) {
        if (f) {
            if (h != null) {
                h.e("BluePay", str);
            } else {
                Log.w("BluePay", str);
            }
        }
    }

    public static void e(String str, Throwable th) {
        if (f) {
            if (h != null) {
                h.e("BluePay", str, th);
            } else {
                Log.w("BluePay", str, th);
            }
        }
    }

    public static void a(Throwable th) {
        if (f) {
            if (h != null) {
                h.a("BluePay", th);
            } else {
                Log.w("BluePay", th);
            }
        }
    }

    public static void f(String str) {
        if (g) {
            if (h != null) {
                h.f("BluePay", str);
            } else {
                Log.wtf("BluePay", str);
            }
        }
    }

    public static void f(String str, Throwable th) {
        if (g) {
            if (h != null) {
                h.f("BluePay", str, th);
            } else {
                Log.wtf("BluePay", str, th);
            }
        }
    }

    public static void b(Throwable th) {
        if (g) {
            if (h != null) {
                h.b("BluePay", th);
            } else {
                Log.wtf("BluePay", th);
            }
        }
    }
}
