package com.bluepay.sdk.exception;

import com.bluepay.data.i;

/* compiled from: Proguard */
public class BlueException extends Exception {
    private static final long a = -3344348348107197428L;
    private final int b;
    private final String c;

    public BlueException(int code, String description, Object... args) {
        this(null, code, description, args);
    }

    public BlueException(Throwable e, int code, String description, Object... args) {
        super(e);
        this.b = code;
        if (args == null || args.length == 0) {
            this.c = description;
        } else {
            this.c = i.a(description, args);
        }
    }

    public int getCode() {
        return this.b;
    }

    public String getMessage() {
        return this.c;
    }
}
