package com.bluepay.sdk.c;

import com.bluepay.data.h;
import com.bluepay.data.i;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.exception.BlueException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Map;
import org.apache.http.protocol.HTTP;

/* compiled from: Proguard */
public class d {
    public static final String a = "bluePay_JMT_1001";

    public static String a(Object... objArr) {
        StringBuilder sb = new StringBuilder();
        int length = objArr.length;
        for (int i = 0; i < length; i++) {
            sb.append(objArr[i] + "|");
        }
        return e.a(sb.toString());
    }

    public static String a(Map map) {
        StringBuilder sb = new StringBuilder();
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                String str = (String) entry.getKey();
                Object value = entry.getValue();
                if (value != null) {
                    try {
                        sb.append("&").append(str).append("=").append(URLEncoder.encode(String.valueOf(value), HTTP.UTF_8));
                    } catch (UnsupportedEncodingException e) {
                        throw new BlueException(e, h.i, i.a(i.D), e.getMessage());
                    }
                }
            }
        }
        return sb.substring(1).toString();
    }

    public static String a(String str) {
        String str2 = null;
        try {
            str2 = e.a(URLDecoder.decode(str, HTTP.UTF_8));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return str2.substring(str2.length() - 1) + str2.substring(1, str2.length() - 1) + str2.substring(0, 1);
    }

    public static String a(String str, String str2) {
        c.c(str2);
        try {
            return y.a(str2, b(y.a(str.getBytes())));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String b(String str, String str2) {
        c.c(str2);
        c.c(str);
        try {
            return y.b(str2, b(y.a(str.getBytes())));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static String b(String str) {
        if (str.length() < 24) {
            int length = 24 - str.length();
            for (int i = 1; i <= length; i++) {
                str = str + "0";
            }
        }
        return str;
    }
}
