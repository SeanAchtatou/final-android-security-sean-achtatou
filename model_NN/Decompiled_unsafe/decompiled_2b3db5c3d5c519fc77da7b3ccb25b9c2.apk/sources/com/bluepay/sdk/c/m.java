package com.bluepay.sdk.c;

import android.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.bluepay.data.i;
import com.bluepay.pay.Client;

/* compiled from: Proguard */
class m implements View.OnClickListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ EditText b;
    final /* synthetic */ AlertDialog c;
    final /* synthetic */ TextView d;
    final /* synthetic */ h e;

    m(h hVar, EditText editText, EditText editText2, AlertDialog alertDialog, TextView textView) {
        this.e = hVar;
        this.a = editText;
        this.b = editText2;
        this.c = alertDialog;
        this.d = textView;
    }

    public void onClick(View v) {
        if (TextUtils.isEmpty(this.a.getText().toString().trim())) {
            aa.a(this.e.a, i.a((byte) 3));
        } else if (!aa.b(this.a.getText().toString().trim(), Client.telcoName)) {
            aa.a(this.e.a, i.a((byte) i.af, Client.telcoName));
        } else if (TextUtils.isEmpty(this.b.getText().toString().trim())) {
            aa.a(this.e.a, i.a(i.ag));
        } else {
            new Thread(new n(this)).start();
        }
    }
}
