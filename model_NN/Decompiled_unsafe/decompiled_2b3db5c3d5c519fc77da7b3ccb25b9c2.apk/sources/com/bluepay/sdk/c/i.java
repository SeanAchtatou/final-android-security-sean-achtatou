package com.bluepay.sdk.c;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;

/* compiled from: Proguard */
class i implements TextWatcher {
    final /* synthetic */ EditText a;
    final /* synthetic */ Button b;
    final /* synthetic */ h c;

    i(h hVar, EditText editText, Button button) {
        this.c = hVar;
        this.a = editText;
        this.b = button;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (this.a.getText().toString().length() >= 4) {
            this.b.setBackgroundResource(aa.a((Context) this.c.a, "drawable", "bluep_ui_confirm_bg_selector"));
            this.b.setEnabled(true);
            return;
        }
        this.b.setBackgroundResource(aa.a((Context) this.c.a, "drawable", "bluep_ui_confirm_bg_enable"));
        this.b.setEnabled(false);
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void afterTextChanged(Editable s) {
    }
}
