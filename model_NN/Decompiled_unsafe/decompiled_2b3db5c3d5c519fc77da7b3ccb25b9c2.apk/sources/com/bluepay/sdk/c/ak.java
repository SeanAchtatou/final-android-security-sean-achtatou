package com.bluepay.sdk.c;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.view.ViewCompat;
import android.widget.TextView;
import com.bluepay.data.i;

/* compiled from: Proguard */
final class ak implements Runnable {
    final /* synthetic */ Activity a;
    final /* synthetic */ String b;
    final /* synthetic */ String c;
    final /* synthetic */ DialogInterface.OnClickListener d;
    final /* synthetic */ DialogInterface.OnClickListener e;

    ak(Activity activity, String str, String str2, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        this.a = activity;
        this.b = str;
        this.c = str2;
        this.d = onClickListener;
        this.e = onClickListener2;
    }

    public void run() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a, 5);
        builder.setTitle(this.b);
        TextView textView = new TextView(this.a);
        textView.setText(this.c);
        textView.setTextColor((int) ViewCompat.MEASURED_STATE_MASK);
        builder.setView(textView);
        builder.setPositiveButton(i.a((byte) 0), this.d);
        builder.setNegativeButton(i.a((byte) 1), this.e);
        builder.setCancelable(false);
        builder.create().show();
    }
}
