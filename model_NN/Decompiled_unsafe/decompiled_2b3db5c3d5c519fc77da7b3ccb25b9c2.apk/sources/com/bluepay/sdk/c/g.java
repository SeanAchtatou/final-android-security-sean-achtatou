package com.bluepay.sdk.c;

import android.app.Activity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import com.bluepay.a.b.au;
import com.bluepay.data.h;
import com.bluepay.data.i;
import com.bluepay.data.m;
import com.bluepay.interfaceClass.b;
import com.bluepay.interfaceClass.c;
import com.bluepay.pay.Client;
import com.bluepay.sdk.a.a;
import com.bluepay.sdk.exception.BlueException;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import org.apache.http.protocol.HTTP;

/* compiled from: Proguard */
public class g {
    static a a = null;

    public static void a(Activity activity, String str, u uVar) {
        try {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            linkedHashMap.put("imsi", Client.m_iIMSI);
            linkedHashMap.put("msisdn", aa.d(str));
            linkedHashMap.put("type", Version.code);
            linkedHashMap.put("countryCode", Integer.valueOf(Client.CONTRY_CODE));
            linkedHashMap.put("productid", Integer.valueOf(Client.getProductId()));
            linkedHashMap.put("encrypt", d.a(d.a(linkedHashMap) + Client.getEncrypt()));
            String a2 = d.a(Client.getEncrypt(), d.a(linkedHashMap));
            linkedHashMap.clear();
            linkedHashMap.put("enctext", URLEncoder.encode(a2, HTTP.UTF_8));
            linkedHashMap.put("productid", Integer.valueOf(Client.getProductId()));
            a(activity, a.b(activity, m.c(1), d.a(linkedHashMap).toString(), linkedHashMap), uVar);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BlueException(h.i, "Obtain verifycation code error", new Object[0]);
        }
    }

    public static void a(Activity activity, String str, String str2, u uVar) {
        try {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            linkedHashMap.put("msisdn", aa.d(str));
            linkedHashMap.put("type", Version.code);
            linkedHashMap.put("code", str2);
            linkedHashMap.put("countryCode", Integer.valueOf(Client.CONTRY_CODE));
            linkedHashMap.put("productid", Integer.valueOf(Client.getProductId()));
            linkedHashMap.put("encrypt", d.a(d.a(linkedHashMap) + Client.getEncrypt()));
            String a2 = d.a(Client.getEncrypt(), d.a(linkedHashMap));
            linkedHashMap.clear();
            linkedHashMap.put("enctext", URLEncoder.encode(a2, HTTP.UTF_8));
            linkedHashMap.put("productid", Integer.valueOf(Client.getProductId()));
            a(activity, a.b(m.c(2), linkedHashMap), str, uVar);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BlueException(h.i, "Verifi code error", new Object[0]);
        }
    }

    private static void a(Activity activity, b bVar, u uVar) {
        if (bVar.a() != 200) {
            return;
        }
        if (au.b(bVar.b()).c("status") == h.a) {
            aa.a(activity, i.a(i.r));
            uVar.a(1);
            return;
        }
        aa.a(activity, i.a(i.o));
        uVar.a(-1);
    }

    private static void a(Activity activity, b bVar, String str, u uVar) {
        if (bVar.a() != 200) {
            return;
        }
        if (au.b(bVar.b()).c("status") == h.a) {
            aa.a(activity, i.a(i.ai));
            uVar.a(1, str);
            return;
        }
        uVar.a(-1, str);
    }

    public static void a(Activity activity, com.bluepay.data.b bVar, c cVar, String str, u uVar) {
        if (activity != null) {
            activity.runOnUiThread(new h(activity, str, bVar, cVar, uVar));
        }
    }

    /* access modifiers changed from: private */
    public static void b(ProgressBar progressBar, Button button, Activity activity, EditText editText, EditText editText2, com.bluepay.data.b bVar, c cVar) {
        progressBar.setVisibility(0);
        button.setText("");
        new Thread(new q(activity, editText, progressBar, button, editText2, bVar, cVar)).start();
    }
}
