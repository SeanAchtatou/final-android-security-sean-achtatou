package com.bluepay.sdk.c;

import java.io.File;
import java.io.FileFilter;
import java.util.regex.Pattern;

/* compiled from: Proguard */
final class ab implements FileFilter {
    ab() {
    }

    public boolean accept(File paramAnonymousFile) {
        return Pattern.matches("cpu[0-9]", paramAnonymousFile.getName());
    }
}
