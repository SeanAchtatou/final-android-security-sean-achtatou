package com.bluepay.sdk.c;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/* compiled from: Proguard */
final class ap implements Runnable {
    final /* synthetic */ Activity a;
    final /* synthetic */ CharSequence b;
    final /* synthetic */ CharSequence c;
    final /* synthetic */ DialogInterface.OnClickListener d;
    final /* synthetic */ int e;

    ap(Activity activity, CharSequence charSequence, CharSequence charSequence2, DialogInterface.OnClickListener onClickListener, int i) {
        this.a = activity;
        this.b = charSequence;
        this.c = charSequence2;
        this.d = onClickListener;
        this.e = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
     arg types: [android.app.Activity, java.lang.CharSequence, java.lang.CharSequence]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog */
    public void run() {
        AlertDialog a2 = aa.c((Context) this.a, this.b, this.c);
        a2.setCancelable(true);
        a2.setButton(-1, "OK", this.d);
        if (this.e == 2) {
            a2.setButton(-2, "Cancel", this.d);
        }
        a2.show();
    }
}
