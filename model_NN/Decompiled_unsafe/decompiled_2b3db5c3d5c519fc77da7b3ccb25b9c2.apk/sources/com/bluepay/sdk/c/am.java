package com.bluepay.sdk.c;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.bluepay.sdk.b.c;

/* compiled from: Proguard */
final class am implements Runnable {
    final /* synthetic */ CharSequence a;
    final /* synthetic */ Activity b;
    final /* synthetic */ CharSequence c;

    am(CharSequence charSequence, Activity activity, CharSequence charSequence2) {
        this.a = charSequence;
        this.b = activity;
        this.c = charSequence2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    public void run() {
        c.c("show loading:" + ((Object) this.a));
        if (aa.e == null) {
            aa.e = new AlertDialog.Builder(this.b, 5).create();
            View inflate = LayoutInflater.from(this.b).inflate(aa.a((Context) this.b, "layout", "bluepay_view_loading"), (ViewGroup) null);
            TextView unused = aa.n = (TextView) inflate.findViewById(aa.a((Context) this.b, "id", "tv_loading"));
            ((ProgressBar) inflate.findViewById(aa.a((Context) this.b, "id", "pb_loading"))).startAnimation(AnimationUtils.loadAnimation(this.b, aa.a((Context) this.b, "anim", "bluepay_loading")));
            aa.n.setText(this.a);
            aa.e.setView(inflate);
            aa.e.setCancelable(false);
            aa.e.setCanceledOnTouchOutside(false);
            aa.e.setTitle(this.c);
        }
        aa.n.setText(this.a);
        aa.e.show();
    }
}
