package com.bluepay.sdk.c;

import android.app.Activity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.interfaceClass.c;
import com.bluepay.sdk.exception.BlueException;

/* compiled from: Proguard */
final class q implements Runnable {
    final /* synthetic */ Activity a;
    final /* synthetic */ EditText b;
    final /* synthetic */ ProgressBar c;
    final /* synthetic */ Button d;
    final /* synthetic */ EditText e;
    final /* synthetic */ b f;
    final /* synthetic */ c g;

    q(Activity activity, EditText editText, ProgressBar progressBar, Button button, EditText editText2, b bVar, c cVar) {
        this.a = activity;
        this.b = editText;
        this.c = progressBar;
        this.d = button;
        this.e = editText2;
        this.f = bVar;
        this.g = cVar;
    }

    public void run() {
        try {
            g.a(this.a, this.b.getText().toString().trim(), new r(this));
        } catch (BlueException e2) {
            this.a.runOnUiThread(new t(this));
            e2.printStackTrace();
            this.f.desc = e2.getMessage();
            this.g.a(14, h.i, 0, this.f);
        }
    }
}
