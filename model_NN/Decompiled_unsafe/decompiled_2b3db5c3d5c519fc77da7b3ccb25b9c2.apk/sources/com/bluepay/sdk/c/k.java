package com.bluepay.sdk.c;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import com.bluepay.data.i;
import com.bluepay.pay.Client;

/* compiled from: Proguard */
class k implements View.OnClickListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ ProgressBar b;
    final /* synthetic */ Button c;
    final /* synthetic */ EditText d;
    final /* synthetic */ h e;

    k(h hVar, EditText editText, ProgressBar progressBar, Button button, EditText editText2) {
        this.e = hVar;
        this.a = editText;
        this.b = progressBar;
        this.c = button;
        this.d = editText2;
    }

    public void onClick(View v) {
        if (TextUtils.isEmpty(this.a.getText().toString().trim())) {
            aa.a(this.e.a, i.a((byte) 3));
        } else if (!aa.b(this.a.getText().toString().trim(), Client.telcoName)) {
            aa.a(this.e.a, i.a((byte) i.af, Client.telcoName));
        } else {
            g.b(this.b, this.c, this.e.a, this.a, this.d, this.e.c, this.e.d);
        }
    }
}
