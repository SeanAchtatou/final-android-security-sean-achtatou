package com.bluepay.sdk.c;

import com.bluepay.data.i;
import java.io.File;
import java.io.FileInputStream;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: Proguard */
public class e {
    protected static char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    protected static MessageDigest b;

    static {
        b = null;
        try {
            b = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static String a(String str) {
        return a(str.getBytes());
    }

    public static boolean a(String str, String str2) {
        return a(str).equals(str2);
    }

    public static String a(File file) {
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bArr = new byte[1024];
        while (true) {
            int read = fileInputStream.read(bArr);
            if (read > 0) {
                b.update(bArr, 0, read);
            } else {
                fileInputStream.close();
                return b(b.digest());
            }
        }
    }

    public static String b(File file) {
        FileInputStream fileInputStream = new FileInputStream(file);
        b.update(fileInputStream.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, file.length()));
        fileInputStream.close();
        return b(b.digest());
    }

    public static String a(byte[] bArr) {
        b.update(bArr);
        return b(b.digest());
    }

    private static String b(byte[] bArr) {
        return a(bArr, 0, bArr.length);
    }

    private static String a(byte[] bArr, int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer(i2 * 2);
        int i3 = i + i2;
        while (i < i3) {
            a(bArr[i], stringBuffer);
            i++;
        }
        return stringBuffer.toString();
    }

    private static void a(byte b2, StringBuffer stringBuffer) {
        char c = a[(b2 & 240) >> 4];
        char c2 = a[b2 & i.p];
        stringBuffer.append(c);
        stringBuffer.append(c2);
    }
}
