package com.bluepay.sdk.c;

import android.app.AlertDialog;
import android.view.View;
import android.widget.ProgressBar;

/* compiled from: Proguard */
class l implements View.OnClickListener {
    final /* synthetic */ ProgressBar a;
    final /* synthetic */ AlertDialog b;
    final /* synthetic */ h c;

    l(h hVar, ProgressBar progressBar, AlertDialog alertDialog) {
        this.c = hVar;
        this.a = progressBar;
        this.b = alertDialog;
    }

    public void onClick(View v) {
        this.a.setVisibility(8);
        if (g.a != null) {
            g.a.cancel();
        }
        this.b.dismiss();
        this.c.e.a(0, "");
    }
}
