package com.bluepay.sdk.c;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Xml;
import android.widget.TextView;
import com.bluepay.a.a.b;
import com.bluepay.a.b.au;
import com.bluepay.a.b.aw;
import com.bluepay.data.Config;
import com.bluepay.data.Order;
import com.bluepay.data.h;
import com.bluepay.data.i;
import com.bluepay.data.m;
import com.bluepay.data.n;
import com.bluepay.interfaceClass.d;
import com.bluepay.pay.BlueMessage;
import com.bluepay.pay.Client;
import com.bluepay.pay.IPayCallback;
import com.bluepay.pay.PublisherCode;
import com.bluepay.sdk.a.a;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.exception.BlueException;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Pattern;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

@SuppressLint({"InlinedApi"})
/* compiled from: Proguard */
public class aa {
    public static int a = 0;
    public static int b = -1;
    public static int c = 0;
    public static String d = "JMTT";
    static AlertDialog e = null;
    private static List f = null;
    private static List g = null;
    private static List h = null;
    private static List i = null;
    private static List j = null;
    /* access modifiers changed from: private */
    public static final int[] k = {5000, 8000, 8000, 9000, 9000, 8000, 5000};
    private static int l = 0;
    private static final String m = "0123456789ABCDEF";
    /* access modifiers changed from: private */
    public static TextView n;
    private static Map o = null;

    public static int a(Context context, String str, String str2) {
        int identifier = context.getResources().getIdentifier(str2, str, context.getPackageName());
        if (identifier == 0) {
            Log.e("BLUE", "getIdByReflection error" + str2);
        }
        return identifier;
    }

    public static Bitmap a(Bitmap bitmap) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-12434878);
        canvas.drawRoundRect(rectF, 12.0f, 12.0f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return createBitmap;
    }

    public static Bitmap a(Drawable drawable) {
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, drawable.getOpacity() != -1 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        drawable.draw(canvas);
        return createBitmap;
    }

    private static String k() {
        return "zxcvbnmk09876543";
    }

    public static int a(String str, int i2) {
        try {
            return Integer.valueOf(str).intValue();
        } catch (Exception e2) {
            Log.e(Client.TAG, "toInt :" + str);
            return i2;
        }
    }

    public static int a(Object obj, int i2) {
        if (obj == null) {
            return i2;
        }
        try {
            return Integer.valueOf(obj.toString()).intValue();
        } catch (Exception e2) {
            Log.e(Client.TAG, "toInt :" + obj);
            return i2;
        }
    }

    public static String a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return Config.NETWORKTYPE_INVALID;
        }
        String typeName = activeNetworkInfo.getTypeName();
        if (typeName.equalsIgnoreCase("WIFI")) {
            return Config.NETWORKTYPE_WIFI;
        }
        if (!typeName.equalsIgnoreCase("MOBILE")) {
            return Config.NETWORKTYPE_INVALID;
        }
        if (TextUtils.isEmpty(Proxy.getDefaultHost())) {
            return o(context) ? Config.NETWORKTYPE_3G : Config.NETWORKTYPE_2G;
        }
        return Config.NETWORKTYPE_WAP;
    }

    private static boolean o(Context context) {
        switch (((TelephonyManager) context.getSystemService("phone")).getNetworkType()) {
            case 0:
                return false;
            case 1:
                return false;
            case 2:
                return false;
            case 3:
                return true;
            case 4:
                return false;
            case 5:
                return true;
            case 6:
                return true;
            case 7:
                return false;
            case 8:
                return true;
            case 9:
                return true;
            case 10:
                return true;
            case 11:
                return false;
            case 12:
                return true;
            case 13:
                return true;
            case 14:
                return true;
            case 15:
                return true;
            default:
                return false;
        }
    }

    public static boolean a() {
        return Config.MOBILE_PHONE_TYPE == 5;
    }

    public static void a(Class cls) {
        Log.i(Client.TAG, "-------" + cls.getSimpleName() + "-------");
        for (Method method : cls.getMethods()) {
            Log.i(Client.TAG, "方法名称:" + method.getName());
            Class<?>[] parameterTypes = method.getParameterTypes();
            int length = parameterTypes.length;
            for (int i2 = 0; i2 < length; i2++) {
                Log.i(Client.TAG, "参数名称:" + parameterTypes[i2].getName());
            }
            Log.i(Client.TAG, "*****************************");
        }
    }

    public static String b(Context context) {
        String c2 = c(context);
        if (c2 != null && !"".equals(c2)) {
            return c2;
        }
        Config.MOBILE_PHONE_TYPE = 5;
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        } catch (Exception e2) {
            c.b("this Phone Don't have SIM!");
            return Config.ERROR_C_BluePay_IMSI;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(android.content.Context r8, int r9) {
        /*
            r6 = 0
            java.lang.String r0 = "content://telephony/siminfo"
            android.net.Uri r1 = android.net.Uri.parse(r0)
            android.content.ContentResolver r0 = r8.getContentResolver()
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0048, all -> 0x0050 }
            r3 = 0
            java.lang.String r4 = "_id"
            r2[r3] = r4     // Catch:{ Exception -> 0x0048, all -> 0x0050 }
            r3 = 1
            java.lang.String r4 = "sim_id"
            r2[r3] = r4     // Catch:{ Exception -> 0x0048, all -> 0x0050 }
            java.lang.String r3 = "sim_id=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0048, all -> 0x0050 }
            r5 = 0
            java.lang.String r7 = java.lang.String.valueOf(r9)     // Catch:{ Exception -> 0x0048, all -> 0x0050 }
            r4[r5] = r7     // Catch:{ Exception -> 0x0048, all -> 0x0050 }
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0048, all -> 0x0050 }
            if (r1 == 0) goto L_0x0041
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x005a, all -> 0x0057 }
            if (r0 == 0) goto L_0x0041
            java.lang.String r0 = "_id"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x005a, all -> 0x0057 }
            int r0 = r1.getInt(r0)     // Catch:{ Exception -> 0x005a, all -> 0x0057 }
            if (r1 == 0) goto L_0x0040
            r1.close()
        L_0x0040:
            return r0
        L_0x0041:
            if (r1 == 0) goto L_0x0046
            r1.close()
        L_0x0046:
            r0 = -1
            goto L_0x0040
        L_0x0048:
            r0 = move-exception
            r0 = r6
        L_0x004a:
            if (r0 == 0) goto L_0x0046
            r0.close()
            goto L_0x0046
        L_0x0050:
            r0 = move-exception
        L_0x0051:
            if (r6 == 0) goto L_0x0056
            r6.close()
        L_0x0056:
            throw r0
        L_0x0057:
            r0 = move-exception
            r6 = r1
            goto L_0x0051
        L_0x005a:
            r0 = move-exception
            r0 = r1
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bluepay.sdk.c.aa.a(android.content.Context, int):int");
    }

    /* JADX WARNING: Removed duplicated region for block: B:107:0x0477 A[Catch:{ Exception -> 0x04ab }] */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0497 A[Catch:{ Exception -> 0x04ab }] */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x04be  */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x04d2 A[Catch:{ Exception -> 0x0341 }] */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0501  */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0507  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x02c8 A[Catch:{ Exception -> 0x0341 }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0302 A[Catch:{ Exception -> 0x0341 }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0392 A[Catch:{ Exception -> 0x0411 }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x03bd A[Catch:{ NoSuchMethodException -> 0x03d1 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String c(android.content.Context r15) {
        /*
            r9 = 21
            r14 = 2
            r13 = 5
            r4 = 1
            r3 = 0
            java.lang.String r0 = ""
            int r2 = a(r15, r3)
            int r5 = a(r15, r4)
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0129 }
            if (r0 < r9) goto L_0x0131
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r15.getSystemService(r0)     // Catch:{ Exception -> 0x0129 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Exception -> 0x0129 }
            java.lang.Class r8 = r0.getClass()     // Catch:{ Exception -> 0x0129 }
            java.lang.String r1 = "getDefaultSim"
            r6 = 0
            java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Exception -> 0x0129 }
            java.lang.reflect.Method r1 = r8.getMethod(r1, r6)     // Catch:{ Exception -> 0x0129 }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x0129 }
            java.lang.Object r1 = r1.invoke(r0, r6)     // Catch:{ Exception -> 0x0129 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Exception -> 0x0129 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0129 }
            com.bluepay.sdk.c.aa.a = r1     // Catch:{ Exception -> 0x0129 }
            int r1 = com.bluepay.sdk.c.aa.a     // Catch:{ Exception -> 0x0129 }
            int r6 = a(r15, r1)     // Catch:{ Exception -> 0x0129 }
            r1 = 0
            int r7 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0129 }
            if (r7 <= r9) goto L_0x00e3
            java.lang.String r1 = "getSubscriberId"
            r7 = 1
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Exception -> 0x0129 }
            r9 = 0
            java.lang.Class r10 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0129 }
            r7[r9] = r10     // Catch:{ Exception -> 0x0129 }
            java.lang.reflect.Method r1 = r8.getMethod(r1, r7)     // Catch:{ Exception -> 0x0129 }
            r7 = r1
        L_0x0052:
            java.lang.String r1 = "getSimCount"
            r9 = 0
            java.lang.Class[] r9 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x0129 }
            java.lang.reflect.Method r1 = r8.getMethod(r1, r9)     // Catch:{ Exception -> 0x0129 }
            java.lang.String r9 = "getSimState"
            r10 = 1
            java.lang.Class[] r10 = new java.lang.Class[r10]     // Catch:{ Exception -> 0x0129 }
            r11 = 0
            java.lang.Class r12 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0129 }
            r10[r11] = r12     // Catch:{ Exception -> 0x0129 }
            java.lang.reflect.Method r9 = r8.getMethod(r9, r10)     // Catch:{ Exception -> 0x0129 }
            r10 = 0
            java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x0129 }
            java.lang.Object r1 = r1.invoke(r0, r10)     // Catch:{ Exception -> 0x0129 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Exception -> 0x0129 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0129 }
            if (r1 < r14) goto L_0x007b
            r1 = 6
            com.bluepay.data.Config.MOBILE_PHONE_TYPE = r1     // Catch:{ Exception -> 0x0129 }
        L_0x007b:
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0129 }
            r10 = 0
            java.lang.Integer r11 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0129 }
            r1[r10] = r11     // Catch:{ Exception -> 0x0129 }
            java.lang.Object r1 = r9.invoke(r0, r1)     // Catch:{ Exception -> 0x0129 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Exception -> 0x0129 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0129 }
            if (r1 != r13) goto L_0x00f8
            r1 = 0
            com.bluepay.sdk.c.aa.a = r1     // Catch:{ Exception -> 0x0129 }
            r1 = r2
        L_0x0095:
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x0129 }
            r9 = 0
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0129 }
            r6[r9] = r1     // Catch:{ Exception -> 0x0129 }
            java.lang.Object r1 = r7.invoke(r0, r6)     // Catch:{ Exception -> 0x0129 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0129 }
            int r6 = com.bluepay.sdk.c.aa.a     // Catch:{ Exception -> 0x0129 }
            if (r6 != 0) goto L_0x0113
            r2 = -1
            if (r5 == r2) goto L_0x00be
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0129 }
            r6 = 0
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0129 }
            r2[r6] = r5     // Catch:{ Exception -> 0x0129 }
            java.lang.Object r2 = r7.invoke(r0, r2)     // Catch:{ Exception -> 0x0129 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0129 }
            com.bluepay.pay.Client.m_iIMSI2 = r2     // Catch:{ Exception -> 0x0129 }
        L_0x00be:
            java.lang.String r2 = "getMsisdn"
            r5 = 1
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x04b5 }
            r6 = 0
            java.lang.Class r7 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x04b5 }
            r5[r6] = r7     // Catch:{ Exception -> 0x04b5 }
            java.lang.reflect.Method r2 = r8.getMethod(r2, r5)     // Catch:{ Exception -> 0x04b5 }
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x04b5 }
            r6 = 0
            int r7 = com.bluepay.sdk.c.aa.a     // Catch:{ Exception -> 0x04b5 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x04b5 }
            r5[r6] = r7     // Catch:{ Exception -> 0x04b5 }
            java.lang.Object r0 = r2.invoke(r0, r5)     // Catch:{ Exception -> 0x04b5 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x04b5 }
            com.bluepay.pay.Client.setMsNum(r0)     // Catch:{ Exception -> 0x04b5 }
        L_0x00e1:
            r0 = r1
        L_0x00e2:
            return r0
        L_0x00e3:
            int r7 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0129 }
            if (r7 != r9) goto L_0x0512
            java.lang.String r1 = "getSubscriberId"
            r7 = 1
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Exception -> 0x0129 }
            r9 = 0
            java.lang.Class r10 = java.lang.Long.TYPE     // Catch:{ Exception -> 0x0129 }
            r7[r9] = r10     // Catch:{ Exception -> 0x0129 }
            java.lang.reflect.Method r1 = r8.getMethod(r1, r7)     // Catch:{ Exception -> 0x0129 }
            r7 = r1
            goto L_0x0052
        L_0x00f8:
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0129 }
            r10 = 0
            java.lang.Integer r11 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0129 }
            r1[r10] = r11     // Catch:{ Exception -> 0x0129 }
            java.lang.Object r1 = r9.invoke(r0, r1)     // Catch:{ Exception -> 0x0129 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Exception -> 0x0129 }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0129 }
            if (r1 != r13) goto L_0x050f
            r1 = 1
            com.bluepay.sdk.c.aa.a = r1     // Catch:{ Exception -> 0x0129 }
            r1 = r5
            goto L_0x0095
        L_0x0113:
            r5 = -1
            if (r2 == r5) goto L_0x00be
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0129 }
            r6 = 0
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0129 }
            r5[r6] = r2     // Catch:{ Exception -> 0x0129 }
            java.lang.Object r2 = r7.invoke(r0, r5)     // Catch:{ Exception -> 0x0129 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0129 }
            com.bluepay.pay.Client.m_iIMSI2 = r2     // Catch:{ Exception -> 0x0129 }
            goto L_0x00be
        L_0x0129:
            r0 = move-exception
            r0.printStackTrace()
            com.bluepay.sdk.c.aa.a = r3
            java.lang.String r0 = ""
        L_0x0131:
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r15.getSystemService(r0)     // Catch:{ Exception -> 0x01d3 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Exception -> 0x01d3 }
            java.lang.Class r1 = r0.getClass()     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r2 = "getSmsDefaultSim"
            r5 = 0
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x01d3 }
            java.lang.reflect.Method r1 = r1.getMethod(r2, r5)     // Catch:{ Exception -> 0x01d3 }
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x01d3 }
            java.lang.Object r0 = r1.invoke(r0, r2)     // Catch:{ Exception -> 0x01d3 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x01d3 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x01d3 }
            com.bluepay.sdk.c.aa.a = r0     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r0 = "com.mediatek.telephony.TelephonyManagerEx"
            java.lang.Class r1 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r0 = "getDefault"
            r2 = 0
            java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x01d3 }
            java.lang.reflect.Method r0 = r1.getMethod(r0, r2)     // Catch:{ Exception -> 0x01d3 }
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x01d3 }
            java.lang.Object r2 = r0.invoke(r1, r2)     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r0 = "getSimState"
            r5 = 1
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x01d3 }
            r6 = 0
            java.lang.Class r7 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x01d3 }
            r5[r6] = r7     // Catch:{ Exception -> 0x01d3 }
            java.lang.reflect.Method r0 = r1.getDeclaredMethod(r0, r5)     // Catch:{ Exception -> 0x01d3 }
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x01d3 }
            r6 = 0
            int r7 = com.bluepay.sdk.c.aa.a     // Catch:{ Exception -> 0x01d3 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x01d3 }
            r5[r6] = r7     // Catch:{ Exception -> 0x01d3 }
            java.lang.Object r0 = r0.invoke(r2, r5)     // Catch:{ Exception -> 0x01d3 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x01d3 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x01d3 }
            if (r13 == r0) goto L_0x0197
            int r0 = com.bluepay.sdk.c.aa.a     // Catch:{ Exception -> 0x01d3 }
            int r0 = 1 - r0
            com.bluepay.sdk.c.aa.a = r0     // Catch:{ Exception -> 0x01d3 }
        L_0x0197:
            java.lang.String r0 = "getSubscriberId"
            r5 = 1
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x01d3 }
            r6 = 0
            java.lang.Class r7 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x01d3 }
            r5[r6] = r7     // Catch:{ Exception -> 0x01d3 }
            java.lang.reflect.Method r5 = r1.getMethod(r0, r5)     // Catch:{ Exception -> 0x01d3 }
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x01d3 }
            r1 = 0
            int r6 = com.bluepay.sdk.c.aa.a     // Catch:{ Exception -> 0x01d3 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x01d3 }
            r0[r1] = r6     // Catch:{ Exception -> 0x01d3 }
            java.lang.Object r0 = r5.invoke(r2, r0)     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x01d3 }
            r1 = 1
            java.lang.Object[] r6 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x01d3 }
            r7 = 0
            int r1 = com.bluepay.sdk.c.aa.a     // Catch:{ Exception -> 0x01d3 }
            if (r1 != r4) goto L_0x04bb
            r1 = r3
        L_0x01c0:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x01d3 }
            r6[r7] = r1     // Catch:{ Exception -> 0x01d3 }
            java.lang.Object r1 = r5.invoke(r2, r6)     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x01d3 }
            com.bluepay.pay.Client.m_iIMSI2 = r1     // Catch:{ Exception -> 0x01d3 }
            r1 = 0
            com.bluepay.data.Config.MOBILE_PHONE_TYPE = r1     // Catch:{ Exception -> 0x01d3 }
            goto L_0x00e2
        L_0x01d3:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r2 = ""
            com.bluepay.sdk.c.aa.a = r3
            java.lang.String r0 = ""
            if (r2 == r0) goto L_0x01e1
            if (r2 != 0) goto L_0x050c
        L_0x01e1:
            java.lang.String r0 = "com.android.internal.telephony.PhoneFactory"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x024f }
            java.lang.String r1 = "getServiceName"
            r5 = 2
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x024f }
            r6 = 0
            java.lang.Class<java.lang.String> r7 = java.lang.String.class
            r5[r6] = r7     // Catch:{ Exception -> 0x024f }
            r6 = 1
            java.lang.Class r7 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x024f }
            r5[r6] = r7     // Catch:{ Exception -> 0x024f }
            java.lang.reflect.Method r1 = r0.getMethod(r1, r5)     // Catch:{ Exception -> 0x024f }
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x024f }
            r6 = 0
            java.lang.String r7 = "phone"
            r5[r6] = r7     // Catch:{ Exception -> 0x024f }
            r6 = 1
            r7 = 1
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x024f }
            r5[r6] = r7     // Catch:{ Exception -> 0x024f }
            java.lang.Object r0 = r1.invoke(r0, r5)     // Catch:{ Exception -> 0x024f }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x024f }
            java.lang.String r1 = "phone"
            java.lang.Object r1 = r15.getSystemService(r1)     // Catch:{ Exception -> 0x024f }
            android.telephony.TelephonyManager r1 = (android.telephony.TelephonyManager) r1     // Catch:{ Exception -> 0x024f }
            java.lang.String r1 = r1.getSubscriberId()     // Catch:{ Exception -> 0x024f }
            java.lang.Object r0 = r15.getSystemService(r0)     // Catch:{ Exception -> 0x024f }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Exception -> 0x024f }
            java.lang.String r5 = r0.getSubscriberId()     // Catch:{ Exception -> 0x024f }
            boolean r0 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x024f }
            if (r0 == 0) goto L_0x0509
            boolean r0 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Exception -> 0x024f }
            if (r0 != 0) goto L_0x0509
            r0 = 1
            com.bluepay.sdk.c.aa.a = r0     // Catch:{ Exception -> 0x024f }
            com.bluepay.pay.Client.m_iIMSI2 = r1     // Catch:{ Exception -> 0x024f }
            r0 = r5
        L_0x0238:
            boolean r2 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Exception -> 0x024f }
            if (r2 == 0) goto L_0x024a
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x024f }
            if (r2 != 0) goto L_0x024a
            r0 = 0
            com.bluepay.sdk.c.aa.a = r0     // Catch:{ Exception -> 0x024f }
            com.bluepay.pay.Client.m_iIMSI2 = r5     // Catch:{ Exception -> 0x024f }
            r0 = r1
        L_0x024a:
            r1 = 1
            com.bluepay.data.Config.MOBILE_PHONE_TYPE = r1     // Catch:{ Exception -> 0x024f }
            goto L_0x00e2
        L_0x024f:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r0 = ""
            com.bluepay.sdk.c.aa.a = r3
        L_0x0257:
            java.lang.String r1 = ""
            if (r0 == r1) goto L_0x025d
            if (r0 != 0) goto L_0x0349
        L_0x025d:
            java.lang.String r0 = "android.telephony.MultiSimTelephonyManager"
            java.lang.Class r1 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x0341 }
            java.lang.String r0 = "getDefault"
            r2 = 0
            java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x0341 }
            java.lang.reflect.Method r0 = r1.getMethod(r0, r2)     // Catch:{ Exception -> 0x0341 }
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0341 }
            java.lang.Object r0 = r0.invoke(r1, r2)     // Catch:{ Exception -> 0x0341 }
            java.lang.String r2 = "getDefaultSubscription"
            r5 = 0
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x0341 }
            java.lang.reflect.Method r2 = r1.getMethod(r2, r5)     // Catch:{ Exception -> 0x0341 }
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0341 }
            java.lang.Object r0 = r2.invoke(r0, r5)     // Catch:{ Exception -> 0x0341 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0341 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0341 }
            com.bluepay.sdk.c.aa.a = r0     // Catch:{ Exception -> 0x0341 }
            java.lang.String r0 = "getDefault"
            r2 = 1
            java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x0341 }
            r5 = 0
            java.lang.Class r6 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0341 }
            r2[r5] = r6     // Catch:{ Exception -> 0x0341 }
            java.lang.reflect.Method r0 = r1.getMethod(r0, r2)     // Catch:{ Exception -> 0x0341 }
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0341 }
            r5 = 0
            r6 = 0
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0341 }
            r2[r5] = r6     // Catch:{ Exception -> 0x0341 }
            java.lang.Object r2 = r0.invoke(r1, r2)     // Catch:{ Exception -> 0x0341 }
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0341 }
            r6 = 0
            r7 = 1
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0341 }
            r5[r6] = r7     // Catch:{ Exception -> 0x0341 }
            java.lang.Object r5 = r0.invoke(r1, r5)     // Catch:{ Exception -> 0x0341 }
            java.lang.String r0 = "getSimState"
            r6 = 0
            java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Exception -> 0x0341 }
            java.lang.reflect.Method r0 = r1.getDeclaredMethod(r0, r6)     // Catch:{ Exception -> 0x0341 }
            r6 = 1
            r0.setAccessible(r6)     // Catch:{ Exception -> 0x0341 }
            int r6 = com.bluepay.sdk.c.aa.a     // Catch:{ Exception -> 0x0341 }
            if (r6 != 0) goto L_0x04be
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x0341 }
            java.lang.Object r0 = r0.invoke(r2, r6)     // Catch:{ Exception -> 0x0341 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0341 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0341 }
            if (r13 == r0) goto L_0x02da
            r0 = 1
            com.bluepay.sdk.c.aa.a = r0     // Catch:{ Exception -> 0x0341 }
        L_0x02da:
            java.lang.String r0 = "getSubscriberInfo"
            r6 = 0
            java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Exception -> 0x0341 }
            java.lang.reflect.Method r0 = r1.getDeclaredMethod(r0, r6)     // Catch:{ Exception -> 0x0341 }
            r6 = 1
            r0.setAccessible(r6)     // Catch:{ Exception -> 0x0341 }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x0341 }
            java.lang.Object r6 = r0.invoke(r2, r6)     // Catch:{ Exception -> 0x0341 }
            r7 = 0
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0341 }
            java.lang.Object r7 = r0.invoke(r5, r7)     // Catch:{ Exception -> 0x0341 }
            java.lang.String r0 = "getMsisdn"
            r8 = 0
            java.lang.Class[] r8 = new java.lang.Class[r8]     // Catch:{ Exception -> 0x0341 }
            java.lang.reflect.Method r0 = r1.getMethod(r0, r8)     // Catch:{ Exception -> 0x0341 }
            int r1 = com.bluepay.sdk.c.aa.a     // Catch:{ Exception -> 0x0341 }
            if (r1 != 0) goto L_0x04d2
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0341 }
            java.lang.Object r0 = r0.invoke(r2, r1)     // Catch:{ Exception -> 0x0341 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0341 }
            com.bluepay.pay.Client.setMsNum(r0)     // Catch:{ Exception -> 0x0341 }
            java.lang.Class r0 = r6.getClass()     // Catch:{ Exception -> 0x0341 }
            java.lang.String r1 = "getSubscriberId"
            r2 = 0
            java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x0341 }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r1, r2)     // Catch:{ Exception -> 0x0341 }
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0341 }
            java.lang.Object r0 = r0.invoke(r6, r1)     // Catch:{ Exception -> 0x0341 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0341 }
            java.lang.Class r1 = r7.getClass()     // Catch:{ Exception -> 0x0341 }
            java.lang.String r2 = "getSubscriberId"
            r5 = 0
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x0341 }
            java.lang.reflect.Method r1 = r1.getDeclaredMethod(r2, r5)     // Catch:{ Exception -> 0x0341 }
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0341 }
            java.lang.Object r1 = r1.invoke(r7, r2)     // Catch:{ Exception -> 0x0341 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0341 }
            com.bluepay.pay.Client.m_iIMSI2 = r1     // Catch:{ Exception -> 0x0341 }
        L_0x033c:
            r1 = 1
            com.bluepay.data.Config.MOBILE_PHONE_TYPE = r1     // Catch:{ Exception -> 0x0341 }
            goto L_0x00e2
        L_0x0341:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r0 = ""
            com.bluepay.sdk.c.aa.a = r3
        L_0x0349:
            java.lang.String r1 = ""
            if (r0 == r1) goto L_0x034f
            if (r0 != 0) goto L_0x0419
        L_0x034f:
            java.lang.String r0 = "android.telephony.TelephonyManager"
            java.lang.Class r2 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x0411 }
            java.lang.String r0 = "getDefault"
            r1 = 0
            java.lang.Class[] r1 = new java.lang.Class[r1]     // Catch:{ Exception -> 0x0411 }
            java.lang.reflect.Method r0 = r2.getMethod(r0, r1)     // Catch:{ Exception -> 0x0411 }
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0411 }
            java.lang.Object r5 = r0.invoke(r2, r1)     // Catch:{ Exception -> 0x0411 }
            java.lang.String r0 = "getMsisdn"
            r1 = 0
            java.lang.Class[] r1 = new java.lang.Class[r1]     // Catch:{ Exception -> 0x0411 }
            java.lang.reflect.Method r0 = r2.getMethod(r0, r1)     // Catch:{ Exception -> 0x0411 }
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0411 }
            java.lang.Object r0 = r0.invoke(r5, r1)     // Catch:{ Exception -> 0x0411 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0411 }
            com.bluepay.pay.Client.setMsNum(r0)     // Catch:{ Exception -> 0x0411 }
            java.lang.String r0 = "getSimState"
            r1 = 0
            java.lang.Class[] r1 = new java.lang.Class[r1]     // Catch:{ Exception -> 0x0411 }
            java.lang.reflect.Method r0 = r2.getMethod(r0, r1)     // Catch:{ Exception -> 0x0411 }
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0411 }
            java.lang.Object r0 = r0.invoke(r5, r1)     // Catch:{ Exception -> 0x0411 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0411 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0411 }
            if (r0 == r13) goto L_0x0395
            r0 = 1
            com.bluepay.sdk.c.aa.a = r0     // Catch:{ Exception -> 0x0411 }
        L_0x0395:
            java.lang.String r0 = "getSubscriberId"
            r1 = 1
            java.lang.Class[] r1 = new java.lang.Class[r1]     // Catch:{ NoSuchMethodException -> 0x03d1 }
            r6 = 0
            java.lang.Class r7 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x03d1 }
            r1[r6] = r7     // Catch:{ NoSuchMethodException -> 0x03d1 }
            java.lang.reflect.Method r6 = r2.getMethod(r0, r1)     // Catch:{ NoSuchMethodException -> 0x03d1 }
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ NoSuchMethodException -> 0x03d1 }
            r1 = 0
            int r7 = com.bluepay.sdk.c.aa.a     // Catch:{ NoSuchMethodException -> 0x03d1 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ NoSuchMethodException -> 0x03d1 }
            r0[r1] = r7     // Catch:{ NoSuchMethodException -> 0x03d1 }
            java.lang.Object r0 = r6.invoke(r5, r0)     // Catch:{ NoSuchMethodException -> 0x03d1 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ NoSuchMethodException -> 0x03d1 }
            r1 = 1
            java.lang.Object[] r7 = new java.lang.Object[r1]     // Catch:{ NoSuchMethodException -> 0x03d1 }
            r8 = 0
            int r1 = com.bluepay.sdk.c.aa.a     // Catch:{ NoSuchMethodException -> 0x03d1 }
            if (r1 != r4) goto L_0x0501
            r1 = r3
        L_0x03be:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ NoSuchMethodException -> 0x03d1 }
            r7[r8] = r1     // Catch:{ NoSuchMethodException -> 0x03d1 }
            java.lang.Object r1 = r6.invoke(r5, r7)     // Catch:{ NoSuchMethodException -> 0x03d1 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ NoSuchMethodException -> 0x03d1 }
            com.bluepay.pay.Client.m_iIMSI2 = r1     // Catch:{ NoSuchMethodException -> 0x03d1 }
            r1 = 2
            com.bluepay.data.Config.MOBILE_PHONE_TYPE = r1     // Catch:{ NoSuchMethodException -> 0x03d1 }
            goto L_0x00e2
        L_0x03d1:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0411 }
            java.lang.String r0 = "getSubscriberId"
            r1 = 1
            java.lang.Class[] r1 = new java.lang.Class[r1]     // Catch:{ Exception -> 0x0411 }
            r6 = 0
            java.lang.Class r7 = java.lang.Long.TYPE     // Catch:{ Exception -> 0x0411 }
            r1[r6] = r7     // Catch:{ Exception -> 0x0411 }
            java.lang.reflect.Method r2 = r2.getMethod(r0, r1)     // Catch:{ Exception -> 0x0411 }
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0411 }
            r1 = 0
            int r6 = com.bluepay.sdk.c.aa.a     // Catch:{ Exception -> 0x0411 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0411 }
            r0[r1] = r6     // Catch:{ Exception -> 0x0411 }
            java.lang.Object r0 = r2.invoke(r5, r0)     // Catch:{ Exception -> 0x0411 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0411 }
            r1 = 1
            java.lang.Object[] r6 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0411 }
            r7 = 0
            int r1 = com.bluepay.sdk.c.aa.a     // Catch:{ Exception -> 0x0411 }
            if (r1 != r4) goto L_0x0504
            r1 = r3
        L_0x03fe:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0411 }
            r6[r7] = r1     // Catch:{ Exception -> 0x0411 }
            java.lang.Object r1 = r2.invoke(r5, r6)     // Catch:{ Exception -> 0x0411 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0411 }
            com.bluepay.pay.Client.m_iIMSI2 = r1     // Catch:{ Exception -> 0x0411 }
            r1 = 2
            com.bluepay.data.Config.MOBILE_PHONE_TYPE = r1     // Catch:{ Exception -> 0x0411 }
            goto L_0x00e2
        L_0x0411:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r0 = ""
            com.bluepay.sdk.c.aa.a = r3
        L_0x0419:
            java.lang.String r1 = ""
            if (r0 == r1) goto L_0x041f
            if (r0 != 0) goto L_0x00e2
        L_0x041f:
            java.lang.String r0 = "android.telephony.MSimTelephonyManager"
            java.lang.Class r1 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x04ab }
            java.lang.String r0 = "phone_msim"
            java.lang.Object r2 = r15.getSystemService(r0)     // Catch:{ Exception -> 0x04ab }
            java.lang.String r0 = "getDefaultSubscription"
            r5 = 0
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x04ab }
            java.lang.reflect.Method r0 = r1.getMethod(r0, r5)     // Catch:{ Exception -> 0x04ab }
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x04ab }
            java.lang.Object r0 = r0.invoke(r2, r5)     // Catch:{ Exception -> 0x04ab }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x04ab }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x04ab }
            com.bluepay.sdk.c.aa.a = r0     // Catch:{ Exception -> 0x04ab }
            java.lang.String r0 = "getSubscriberId"
            r5 = 1
            java.lang.Class[] r5 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x04ab }
            r6 = 0
            java.lang.Class r7 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x04ab }
            r5[r6] = r7     // Catch:{ Exception -> 0x04ab }
            java.lang.reflect.Method r5 = r1.getMethod(r0, r5)     // Catch:{ Exception -> 0x04ab }
            java.lang.String r0 = "getSimState"
            r6 = 1
            java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Exception -> 0x04ab }
            r7 = 0
            java.lang.Class r8 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x04ab }
            r6[r7] = r8     // Catch:{ Exception -> 0x04ab }
            java.lang.reflect.Method r0 = r1.getMethod(r0, r6)     // Catch:{ Exception -> 0x04ab }
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x04ab }
            r6 = 0
            int r7 = com.bluepay.sdk.c.aa.a     // Catch:{ Exception -> 0x04ab }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x04ab }
            r1[r6] = r7     // Catch:{ Exception -> 0x04ab }
            java.lang.Object r0 = r0.invoke(r2, r1)     // Catch:{ Exception -> 0x04ab }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x04ab }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x04ab }
            if (r0 == r13) goto L_0x047d
            int r0 = com.bluepay.sdk.c.aa.a     // Catch:{ Exception -> 0x04ab }
            int r0 = 1 - r0
            com.bluepay.sdk.c.aa.a = r0     // Catch:{ Exception -> 0x04ab }
        L_0x047d:
            r0 = 1
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x04ab }
            r1 = 0
            int r6 = com.bluepay.sdk.c.aa.a     // Catch:{ Exception -> 0x04ab }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x04ab }
            r0[r1] = r6     // Catch:{ Exception -> 0x04ab }
            java.lang.Object r0 = r5.invoke(r2, r0)     // Catch:{ Exception -> 0x04ab }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x04ab }
            r1 = 1
            java.lang.Object[] r6 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x04ab }
            r7 = 0
            int r1 = com.bluepay.sdk.c.aa.a     // Catch:{ Exception -> 0x04ab }
            if (r1 != r4) goto L_0x0507
            r1 = r3
        L_0x0498:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x04ab }
            r6[r7] = r1     // Catch:{ Exception -> 0x04ab }
            java.lang.Object r1 = r5.invoke(r2, r6)     // Catch:{ Exception -> 0x04ab }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x04ab }
            com.bluepay.pay.Client.m_iIMSI2 = r1     // Catch:{ Exception -> 0x04ab }
            r1 = 3
            com.bluepay.data.Config.MOBILE_PHONE_TYPE = r1     // Catch:{ Exception -> 0x04ab }
            goto L_0x00e2
        L_0x04ab:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r0 = ""
            com.bluepay.sdk.c.aa.a = r3
            goto L_0x00e2
        L_0x04b5:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0129 }
            goto L_0x00e1
        L_0x04bb:
            r1 = r4
            goto L_0x01c0
        L_0x04be:
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x0341 }
            java.lang.Object r0 = r0.invoke(r5, r6)     // Catch:{ Exception -> 0x0341 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0341 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0341 }
            if (r13 == r0) goto L_0x02da
            r0 = 0
            com.bluepay.sdk.c.aa.a = r0     // Catch:{ Exception -> 0x0341 }
            goto L_0x02da
        L_0x04d2:
            r1 = 0
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0341 }
            java.lang.Object r0 = r0.invoke(r5, r1)     // Catch:{ Exception -> 0x0341 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0341 }
            com.bluepay.pay.Client.setMsNum(r0)     // Catch:{ Exception -> 0x0341 }
            java.lang.Class r0 = r7.getClass()     // Catch:{ Exception -> 0x0341 }
            java.lang.String r1 = "getSubscriberId"
            r2 = 0
            java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x0341 }
            java.lang.reflect.Method r1 = r0.getDeclaredMethod(r1, r2)     // Catch:{ Exception -> 0x0341 }
            r0 = 0
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ Exception -> 0x0341 }
            java.lang.Object r0 = r1.invoke(r7, r0)     // Catch:{ Exception -> 0x0341 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0341 }
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0341 }
            java.lang.Object r1 = r1.invoke(r6, r2)     // Catch:{ Exception -> 0x0341 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0341 }
            com.bluepay.pay.Client.m_iIMSI2 = r1     // Catch:{ Exception -> 0x0341 }
            goto L_0x033c
        L_0x0501:
            r1 = r4
            goto L_0x03be
        L_0x0504:
            r1 = r4
            goto L_0x03fe
        L_0x0507:
            r1 = r4
            goto L_0x0498
        L_0x0509:
            r0 = r2
            goto L_0x0238
        L_0x050c:
            r0 = r2
            goto L_0x0257
        L_0x050f:
            r1 = r6
            goto L_0x0095
        L_0x0512:
            r7 = r1
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bluepay.sdk.c.aa.c(android.content.Context):java.lang.String");
    }

    public static final String d(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public static String a(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getWidth();
        activity.getWindowManager().getDefaultDisplay().getHeight();
        new DisplayMetrics();
        DisplayMetrics displayMetrics2 = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics2);
        int i2 = (int) ((((float) displayMetrics2.widthPixels) * displayMetrics2.density) + 0.5f);
        int i3 = (int) ((displayMetrics2.density * ((float) displayMetrics2.heightPixels)) + 0.5f);
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int i4 = displayMetrics.widthPixels;
        return i4 + "x" + displayMetrics.heightPixels;
    }

    public static int b(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getWidth();
        activity.getWindowManager().getDefaultDisplay().getHeight();
        new DisplayMetrics();
        DisplayMetrics displayMetrics2 = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics2);
        int i2 = (int) ((((float) displayMetrics2.widthPixels) * displayMetrics2.density) + 0.5f);
        int i3 = (int) ((displayMetrics2.density * ((float) displayMetrics2.heightPixels)) + 0.5f);
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int i4 = displayMetrics.widthPixels;
        return displayMetrics.heightPixels;
    }

    public static String e(Context context) {
        return Build.VERSION.RELEASE;
    }

    public static String f(Context context) {
        return Build.MODEL != null ? Build.MODEL.replace(" ", "") : "unknown";
    }

    public static float g(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    public static boolean a(String str) {
        if (str == null || "".equals(str)) {
            return true;
        }
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if (charAt != ' ' && charAt != 9 && charAt != 13 && charAt != 10) {
                return false;
            }
        }
        return true;
    }

    public static String h(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 16384).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            return "";
        }
    }

    public static String i(Context context) {
        PackageManager packageManager;
        ApplicationInfo applicationInfo = null;
        try {
            packageManager = context.getApplicationContext().getPackageManager();
            try {
                applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e2) {
            }
        } catch (PackageManager.NameNotFoundException e3) {
            packageManager = null;
        }
        return (String) packageManager.getApplicationLabel(applicationInfo);
    }

    public static int b() {
        try {
            return new File("/sys/devices/system/cpu/").listFiles(new ab()).length;
        } catch (Exception e2) {
            return 1;
        }
    }

    public static void a(Activity activity, String str, String str2, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        activity.runOnUiThread(new ak(activity, str, str2, onClickListener, onClickListener2));
    }

    public static void a(Order order, String str, int... iArr) {
        new Thread(new al(order, str, iArr)).start();
    }

    public static HashMap a(Context context, String str, String str2, ArrayList arrayList) {
        HashMap hashMap = new HashMap();
        try {
            try {
                InputStream open = context.createPackageContext(str, 2).getAssets().open(str2);
                XmlPullParser newPullParser = Xml.newPullParser();
                try {
                    newPullParser.setInput(open, "utf-8");
                    for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                        switch (eventType) {
                            case 2:
                                System.out.println("key : " + newPullParser.getName());
                                break;
                        }
                    }
                    open.close();
                } catch (XmlPullParserException e2) {
                    e2.printStackTrace();
                }
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        } catch (PackageManager.NameNotFoundException e4) {
            e4.printStackTrace();
        }
        return hashMap;
    }

    public static byte[] b(String str) {
        int length = str.length() / 2;
        byte[] bArr = new byte[length];
        for (int i2 = 0; i2 < length; i2++) {
            bArr[i2] = Integer.valueOf(str.substring(i2 * 2, (i2 * 2) + 2), 16).byteValue();
        }
        return bArr;
    }

    public static String a(byte[] bArr) {
        if (bArr == null) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer(bArr.length * 2);
        for (byte a2 : bArr) {
            a(stringBuffer, a2);
        }
        return stringBuffer.toString();
    }

    private static void a(StringBuffer stringBuffer, byte b2) {
        stringBuffer.append(m.charAt((b2 >> 4) & 15)).append(m.charAt(b2 & i.p));
    }

    @SuppressLint({"TrulyRandom"})
    private static byte[] b(byte[] bArr) {
        KeyGenerator instance = KeyGenerator.getInstance("AES");
        SecureRandom instance2 = SecureRandom.getInstance("SHA1PRNG", "Crypto");
        instance2.setSeed(bArr);
        instance.init(128, instance2);
        return instance.generateKey().getEncoded();
    }

    public static String a(String str, String str2) {
        if (str == null) {
            System.out.print("encrypted error!");
            return null;
        }
        int indexOf = str.indexOf("_");
        if (indexOf == -1 || indexOf >= 3) {
            try {
                return new String(a(b(str2.getBytes()), b(str)));
            } catch (Exception e2) {
                System.out.print("encrypted error!");
                return null;
            }
        } else {
            try {
                byte[] b2 = b(str.substring(2).replace("\r", "").replace("\n", ""));
                Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
                instance.init(2, new SecretKeySpec(str2.getBytes(), "AES"), new IvParameterSpec(k().getBytes()));
                return new String(instance.doFinal(b2)).trim();
            } catch (Exception e3) {
                System.out.print("encrypted error!");
                return null;
            }
        }
    }

    private static byte[] a(byte[] bArr, byte[] bArr2) {
        SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
        Cipher instance = Cipher.getInstance("AES/ECB/ZeroBytePadding");
        instance.init(2, secretKeySpec);
        return instance.doFinal(bArr2);
    }

    public static int c() {
        try {
            return Build.VERSION.SDK_INT;
        } catch (Exception e2) {
            return -1;
        }
    }

    public static void a(Activity activity, CharSequence charSequence, CharSequence charSequence2) {
        activity.runOnUiThread(new am(charSequence2, activity, charSequence));
    }

    public static void b(Activity activity, CharSequence charSequence, CharSequence charSequence2) {
        activity.runOnUiThread(new an(activity, charSequence, charSequence2));
    }

    public static void a(Activity activity, CharSequence charSequence, CharSequence charSequence2, int i2, DialogInterface.OnClickListener onClickListener) {
        activity.runOnUiThread(new ap(activity, charSequence, charSequence2, onClickListener, i2));
    }

    public static void d() {
        c.c("hide loading:" + e);
        if (e != null && e.isShowing()) {
            e.cancel();
            e.dismiss();
            e = null;
            c.c("hide loading:" + e);
        }
    }

    public static void a(Context context, String str) {
        ((Activity) context).runOnUiThread(new aq(context, str));
    }

    public static void a(Context context, d dVar) {
        if (!f.e(context)) {
            a(context, "network is invalid,please check your setting");
            return;
        }
        c.c("begin explore msisdn:" + Client.m_iIMSI);
        if (TextUtils.isEmpty(Client.m_iIMSI)) {
            dVar.a(-1, null);
        } else if (!TextUtils.isEmpty(Client.phoneNum())) {
            f(context, Client.phoneNum());
            dVar.a(1, Client.phoneNum());
        } else {
            new Thread(new ar(context, dVar)).start();
        }
    }

    public static String c(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("imsi", Client.m_iIMSI);
        hashMap.put("productid", Integer.valueOf(Client.getProductId()));
        aw b2 = au.b(a.a(m.x(), hashMap).b());
        int c2 = b2.c("status");
        c.c("status:" + c2);
        if (c2 == h.a) {
            return b2.a("msisdn");
        }
        return null;
    }

    /* access modifiers changed from: private */
    public static AlertDialog c(Context context, CharSequence charSequence, CharSequence charSequence2) {
        AlertDialog create = new AlertDialog.Builder(context, 5).create();
        create.setCanceledOnTouchOutside(false);
        create.setCancelable(false);
        create.setTitle(charSequence);
        create.setMessage(charSequence2);
        return create;
    }

    /* access modifiers changed from: private */
    public static AlertDialog d(Context context, CharSequence charSequence, CharSequence charSequence2) {
        AlertDialog create = new AlertDialog.Builder(context, 5).setPositiveButton("OK", (DialogInterface.OnClickListener) null).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).create();
        create.setCanceledOnTouchOutside(false);
        create.setCancelable(false);
        create.setTitle(charSequence);
        return create;
    }

    public static void a(Context context, CharSequence charSequence, d dVar) {
        ((Activity) context).runOnUiThread(new as(context, charSequence, dVar));
    }

    public static void b(Context context, CharSequence charSequence, d dVar) {
        ((Activity) context).runOnUiThread(new ac(context, charSequence, dVar));
    }

    public static int a(Context context, float f2) {
        return (int) ((context.getResources().getDisplayMetrics().density * f2) + 0.5f);
    }

    public static boolean a(Context context, String str, int i2) {
        try {
            int i3 = context.getPackageManager().getPackageInfo(str, 0).versionCode;
            c.c("app version code:" + i3);
            if (i2 <= i3) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static boolean a(Context context, String str, String str2, String str3, int i2) {
        return new b(context).a(str, str2, str3, e(), i2);
    }

    public static boolean b(Context context, String str) {
        return new b(context).a(str);
    }

    public static List j(Context context) {
        return new b(context).a();
    }

    @SuppressLint({"SimpleDateFormat"})
    public static String e() {
        return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
    }

    @SuppressLint({"NewApi"})
    public static boolean c(Context context, String str) {
        int checkPermission;
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                checkPermission = context.checkSelfPermission(str);
            } else {
                checkPermission = context.getPackageManager().checkPermission(str, context.getPackageName());
            }
            if (checkPermission == 0) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static void a(Activity activity, IPayCallback iPayCallback, String str, String str2, int i2) {
        new Thread(new ag(str2, str, iPayCallback, activity, i2)).start();
    }

    /* access modifiers changed from: private */
    public static void b(IPayCallback iPayCallback, Activity activity, BlueMessage blueMessage) {
        activity.runOnUiThread(new ah(iPayCallback, blueMessage));
    }

    public static String d(Context context, String str) {
        String str2;
        String str3;
        String attributeName;
        String str4;
        int safePrice;
        if (str == null) {
            return "";
        }
        InputStream inputStream = null;
        try {
            String str5 = "default".equals(str) ? "prices" : "prices-";
            if (str.equals("default")) {
                str2 = "";
            } else {
                str2 = str;
            }
            if (str2.equals(PublisherCode.PUBLISHER_OFFLINE_ATM) || str2.equals(PublisherCode.PUBLISHER_OFFLINE_OTC)) {
                str3 = "offline";
            } else {
                str3 = str2;
            }
            StringBuffer stringBuffer = new StringBuffer();
            inputStream = context.getResources().getAssets().open("BluePay.ref");
            XmlPullParser newPullParser = Xml.newPullParser();
            newPullParser.setInput(inputStream, "utf-8");
            boolean z = false;
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                switch (eventType) {
                    case 2:
                        if (newPullParser.getName().equals(str5 + str3)) {
                            z = true;
                        }
                        if (newPullParser.getName().equals(FirebaseAnalytics.Param.PRICE) && z && ((attributeName = newPullParser.getAttributeName(0)) == null || attributeName.equals(FirebaseAnalytics.Param.VALUE))) {
                            try {
                                safePrice = Integer.parseInt(newPullParser.getAttributeValue(0));
                            } catch (Exception e2) {
                                safePrice = Order.getSafePrice(Config.K_CURRENCY_TRF, str4);
                            }
                            stringBuffer.append((safePrice + ":" + newPullParser.nextText().trim()) + ",");
                            break;
                        }
                    case 3:
                        if (!newPullParser.getName().equals(str5 + str3)) {
                            break;
                        } else {
                            String stringBuffer2 = stringBuffer.toString();
                            if (!TextUtils.isEmpty(stringBuffer) && stringBuffer2.contains(",")) {
                                String substring = stringBuffer2.substring(0, stringBuffer2.lastIndexOf(","));
                                if (inputStream == null) {
                                    return substring;
                                }
                                try {
                                    inputStream.close();
                                    return substring;
                                } catch (IOException e3) {
                                    e3.printStackTrace();
                                    return substring;
                                }
                            } else if (inputStream == null) {
                                return "";
                            } else {
                                try {
                                    inputStream.close();
                                    return "";
                                } catch (IOException e4) {
                                    e4.printStackTrace();
                                    return "";
                                }
                            }
                        }
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
            }
            return "";
        } catch (Exception e6) {
            e6.printStackTrace();
            c.b("parse ref for  error");
            if (inputStream == null) {
                return "";
            }
            try {
                inputStream.close();
                return "";
            } catch (IOException e7) {
                e7.printStackTrace();
                return "";
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e8) {
                    e8.printStackTrace();
                }
            }
            throw th;
        }
    }

    public static Map f() {
        return o;
    }

    public static String[] e(Context context, String str) {
        try {
            if (str.equals(PublisherCode.PUBLISHER_SMS) || str.equals(com.bluepay.data.b.a) || str.equals(PublisherCode.PUBLISHER_DCB_TELENOR)) {
                str = "default";
            }
            String[] split = d(context, str).split(",");
            String[] strArr = new String[split.length];
            for (int i2 = 0; i2 < split.length; i2++) {
                strArr[i2] = split[i2].split(":")[1];
            }
            if (o == null) {
                o = new HashMap();
            }
            for (String str2 : split) {
                o.put(str2.split(":")[1], str2.split(":")[0]);
            }
            if (strArr.length == 0) {
                return null;
            }
            return strArr;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String k(Context context) {
        String language = context.getResources().getConfiguration().locale.getLanguage();
        c.c("locale:" + language);
        if (language.equalsIgnoreCase("id") || language.equalsIgnoreCase(Config.LAN_ID2) || language.equalsIgnoreCase(Config.LAN_ID3) || language.equalsIgnoreCase(Config.LAN_ID4)) {
            return "id";
        }
        if (language.equalsIgnoreCase(Config.LAN_TH1) || language.equals(Config.LAN_TH2)) {
            return Config.LAN_TH1;
        }
        if (language.equalsIgnoreCase(Config.LAN_VN1) || language.equalsIgnoreCase(Config.LAN_VN2)) {
            return Config.LAN_VN1;
        }
        return Locale.ENGLISH.getDisplayLanguage();
    }

    public static String d(String str) {
        if (str.startsWith("0")) {
            str = str.substring(1, str.length());
        }
        if (str.startsWith(Client.CONTRY_CODE + "0")) {
            return str.replace(Client.CONTRY_CODE + "0", Client.CONTRY_CODE + "");
        }
        return !str.startsWith(String.valueOf(Client.CONTRY_CODE)) ? Client.CONTRY_CODE + str : str;
    }

    public static String e(String str) {
        if (str.startsWith("+")) {
            str = str.substring(1, str.length());
        }
        if (str.startsWith(String.valueOf(Client.CONTRY_CODE))) {
            return str.substring(String.valueOf(Client.CONTRY_CODE).length());
        }
        return str;
    }

    public static void a(Activity activity, String str, String str2, int i2, String str3, String str4) {
        com.bluepay.data.b bVar = new com.bluepay.data.b(activity, "", 0, str);
        bVar.desc = str4;
        bVar.setCPPayType(str3);
        try {
            bVar.setPrice(Integer.parseInt(str2));
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
        }
        com.bluepay.a.b.a.o.a(14, i2, 0, bVar);
    }

    public static boolean g() {
        TimeZone timeZone = TimeZone.getDefault();
        if ((timeZone.getID().equals("Asia/Shanghai") || timeZone.getID().equals("Asia/Chongqing") || timeZone.getID().equals("Asia/Harbin") || timeZone.getID().equals("Asia/Hong_Kong") || timeZone.getID().equals("Asia/Shanghai") || timeZone.getID().equals("Asia/Taipei")) && timeZone.getDisplayName(false, 0).equals("GMT+08:00")) {
            return true;
        }
        return false;
    }

    public static boolean f(String str) {
        return Pattern.compile("^[-\\+]?[\\d]*$").matcher(str).matches();
    }

    public static void h() {
        p();
        l();
        o();
        m();
        n();
    }

    public static void a(aw awVar) {
        if (f == null) {
            f = new ArrayList();
        }
        f.clear();
        if (g == null) {
            g = new ArrayList();
        }
        g.clear();
        if (j == null) {
            j = new ArrayList();
        }
        j.clear();
        if (h == null) {
            h = new ArrayList();
        }
        h.clear();
        if (i == null) {
            i = new ArrayList();
        }
        i.clear();
        try {
            JSONObject jSONObject = ((JSONObject) awVar.e("telcoPrefix")).getJSONObject("id");
            JSONArray jSONArray = jSONObject.getJSONArray(Config.TELCO_NAME_INDOSAT);
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                f.add(jSONArray.getString(i2));
            }
            JSONArray jSONArray2 = jSONObject.getJSONArray(Config.TELCO_NAME_XL);
            for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                h.add(jSONArray2.getString(i3));
            }
            JSONArray jSONArray3 = jSONObject.getJSONArray(Config.TELCO_NAME_HUTCHISON);
            for (int i4 = 0; i4 < jSONArray3.length(); i4++) {
                g.add(jSONArray3.getString(i4));
            }
            JSONArray jSONArray4 = jSONObject.getJSONArray("smartfren");
            for (int i5 = 0; i5 < jSONArray4.length(); i5++) {
                j.add(jSONArray4.getString(i5));
            }
            JSONArray jSONArray5 = jSONObject.getJSONArray(Config.TELCO_NAME_TELKOMSEL);
            for (int i6 = 0; i6 < jSONArray5.length(); i6++) {
                i.add(jSONArray5.getString(i6));
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        } catch (BlueException e3) {
            e3.printStackTrace();
        }
    }

    private static void l() {
        if (f == null) {
            f = new ArrayList();
        }
        f.add("0814");
        f.add("0815");
        f.add("0816");
        f.add("0855");
        f.add("0856");
        f.add("0857");
        f.add("0858");
    }

    private static void m() {
        if (g == null) {
            g = new ArrayList();
        }
        g.add("0895");
        g.add("0896");
        g.add("0897");
        g.add("0898");
        g.add("0899");
    }

    private static void n() {
        if (j == null) {
            j = new ArrayList();
        }
        j.add("0881");
        j.add("0882");
        j.add("0883");
        j.add("0884");
        j.add("0885");
        j.add("0886");
        j.add("0887");
        j.add("0888");
        j.add("0889");
    }

    private static void o() {
        if (h == null) {
            h = new ArrayList();
        }
        h.add("0817");
        h.add("0818");
        h.add("0819");
        h.add("0831");
        h.add("0832");
        h.add("0838");
        h.add("0859");
        h.add("0877");
        h.add("0878");
        h.add("0879");
    }

    private static void p() {
        if (i == null) {
            i = new ArrayList();
        }
        i.add("0811");
        i.add("0812");
        i.add("0813");
        i.add("0821");
        i.add("0822");
        i.add("0823");
        i.add("0852");
        i.add("0853");
    }

    public static boolean b(String str, String str2) {
        boolean z = false;
        if (str.startsWith(Client.CONTRY_CODE + "0") || str.startsWith(Client.CONTRY_CODE + "")) {
            str = str.substring(2, str.length());
        }
        if (!str.startsWith("0")) {
            str = "0" + str;
        }
        if (str.length() < 10 || str.length() > 15) {
            return false;
        }
        if (Client.CONTRY_CODE == 86) {
            return true;
        }
        if (str2.equals(Config.TELCO_NAME_INDOSAT)) {
            z = a(f, str);
        }
        if (str2.equals(Config.TELCO_NAME_SMARTFREN)) {
            z = a(j, str);
        }
        if (str2.equals(Config.TELCO_NAME_HUTCHISON)) {
            z = a(g, str);
        }
        if (str2.equals(Config.TELCO_NAME_XL)) {
            return a(h, str);
        }
        if (str2.equals(Config.TELCO_NAME_TELKOMSEL)) {
            return a(i, str);
        }
        return z;
    }

    public static String g(String str) {
        if (str.startsWith(Client.CONTRY_CODE + "0") || str.startsWith(Client.CONTRY_CODE + "")) {
            str = str.substring(2, str.length());
        }
        if (!str.startsWith("0")) {
            str = "0" + str;
        }
        if (str.length() < 10 || str.length() > 15) {
            return "";
        }
        if (Client.CONTRY_CODE == 86) {
            return "china";
        }
        if (a(f, str)) {
            return Config.TELCO_NAME_INDOSAT;
        }
        if (a(j, str)) {
            return Config.TELCO_NAME_SMARTFREN;
        }
        if (a(g, str)) {
            return Config.TELCO_NAME_HUTCHISON;
        }
        if (a(h, str)) {
            return Config.TELCO_NAME_XL;
        }
        if (a(i, str)) {
            return Config.TELCO_NAME_TELKOMSEL;
        }
        return "";
    }

    public static boolean h(String str) {
        if (str.startsWith(Client.CONTRY_CODE + "0") || str.startsWith(Client.CONTRY_CODE + "")) {
            str = str.substring(2, str.length());
        }
        if (!str.startsWith("0")) {
            str = "0" + str;
        }
        if (str.length() < 10 || str.length() > 15) {
            return false;
        }
        return a(i, str);
    }

    private static boolean a(List list, String str) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            if (str.startsWith((String) it.next())) {
                return true;
            }
        }
        return false;
    }

    public static String l(Context context) {
        return context.getResources().getConfiguration().locale.getLanguage();
    }

    public static void b(Context context, String str, String str2, String str3, int i2) {
        c.c("||---- BluePay uploadTransRecordInTime ---- start ----||");
        if (TextUtils.isEmpty(str)) {
            c.c("||---- BluePay uploadTransRecordInTime ---- end -- getTransactionId -- null ---||");
        } else if (!Config.NETWORKTYPE_INVALID.equals(a(context))) {
            new Thread(new ai(str, str2, i2, str3, context)).start();
        } else {
            c.c("||---- BluePay uploadTransRecordInTime ---- end --no network---||");
            a(context, str, str2, str3, i2);
        }
    }

    public static void m(Context context) {
        c.c("||---- BluePay uploadTransRecord() ---- start ----||");
        List<n> j2 = j(context);
        if (j2 == null || j2.size() < 0) {
            c.c("||---- BluePay uploadTransRecord() --0-- end ----||");
            return;
        }
        try {
            for (n nVar : j2) {
                if (nVar.a == null) {
                    break;
                }
                String y = m.y();
                HashMap hashMap = new HashMap();
                hashMap.put("fwflag", "yes");
                hashMap.put("transactionid", nVar.a);
                hashMap.put("desc", nVar.b);
                hashMap.put("actiontype", nVar.i);
                hashMap.put("productid", nVar.c);
                hashMap.put("promotinId", nVar.d);
                hashMap.put("imsi", nVar.e);
                hashMap.put("imei", nVar.f);
                hashMap.put("timestamp", nVar.g);
                hashMap.put("v", nVar.h);
                hashMap.put("rv", Integer.valueOf(nVar.k));
                String str = d.a(hashMap).toString();
                hashMap.put("encrypt", e.a(str + Client.getEncrypt()));
                if (a.a(context, y, str, hashMap).a() == 200) {
                    Log.i(Client.TAG, "server record ok");
                    b(context, nVar.a);
                }
            }
            c.c("||---- BluePay uploadTransRecord() ---- end ----||");
        } catch (BlueException e2) {
            e2.printStackTrace();
        }
    }

    public static List b(aw awVar) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = (JSONArray) awVar.e("erCode");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                arrayList.add(Integer.valueOf(jSONArray.getInt(i2)));
            }
        } catch (BlueException e2) {
            e2.printStackTrace();
        } catch (JSONException e3) {
            e3.printStackTrace();
        }
        return arrayList;
    }

    public static void f(Context context, String str) {
        com.bluepay.sdk.b.b.a(context, Client.TAG);
        com.bluepay.sdk.b.b.a("msisdn", str);
        com.bluepay.sdk.b.b.a();
    }

    public static String n(Context context) {
        com.bluepay.sdk.b.b.a(context, Client.TAG);
        return com.bluepay.sdk.b.b.b("msisdn", "");
    }

    public static String a(int i2) {
        switch (i2) {
            case 62:
                return "IDR";
            case 66:
                return Config.K_CURRENCY_THB;
            case 84:
                return Config.K_CURRENCY_VND;
            default:
                return Config.K_CURRENCY_THB;
        }
    }

    public static void c(Activity activity) {
        try {
            if (a(activity, "asia.bluepay.client", 3018)) {
                try {
                    activity.runOnUiThread(new aj(Client.m_BlueWallet.a, activity));
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                x.b(activity, Client.m_BlueWallet.c);
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public static String i(String str) {
        if (str.equalsIgnoreCase(PublisherCode.PUBLISHER_LINE)) {
            return "LinePay";
        }
        if (str.equalsIgnoreCase(PublisherCode.PUBLISHER_MOGPLAY)) {
            return "Mogplay";
        }
        if (str.equalsIgnoreCase(PublisherCode.PUBLISHER_LYTOCARD)) {
            return "Game-ON";
        }
        if (str.equalsIgnoreCase(PublisherCode.PUBLISHER_OFFLINE_OTC)) {
            return "Bayar di Mini Market";
        }
        if (str.equalsIgnoreCase(PublisherCode.PUBLISHER_BANK)) {
            return "Thailand Bank";
        }
        if (str.equalsIgnoreCase(PublisherCode.PUBLISHER_VN_BANK)) {
            return "Vietnam Bank";
        }
        if (str.equalsIgnoreCase(PublisherCode.PUBLISHER_ID_BANK)) {
            return "Mandri Bank";
        }
        return str;
    }
}
