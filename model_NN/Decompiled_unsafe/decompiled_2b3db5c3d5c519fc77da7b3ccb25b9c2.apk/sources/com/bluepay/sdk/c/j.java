package com.bluepay.sdk.c;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/* compiled from: Proguard */
class j implements View.OnTouchListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ TextView b;
    final /* synthetic */ h c;

    j(h hVar, EditText editText, TextView textView) {
        this.c = hVar;
        this.a = editText;
        this.b = textView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    public boolean onTouch(View v, MotionEvent event) {
        this.a.setBackgroundResource(aa.a((Context) this.c.a, "drawable", "bluep_ui_input_bg"));
        this.b.setVisibility(8);
        return false;
    }
}
