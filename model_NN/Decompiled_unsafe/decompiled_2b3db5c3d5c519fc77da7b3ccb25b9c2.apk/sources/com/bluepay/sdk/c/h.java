package com.bluepay.sdk.c;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.bluepay.data.b;
import com.bluepay.data.i;
import com.bluepay.interfaceClass.c;

/* compiled from: Proguard */
final class h implements Runnable {
    final /* synthetic */ Activity a;
    final /* synthetic */ String b;
    final /* synthetic */ b c;
    final /* synthetic */ c d;
    final /* synthetic */ u e;

    h(Activity activity, String str, b bVar, c cVar, u uVar) {
        this.a = activity;
        this.b = str;
        this.c = bVar;
        this.d = cVar;
        this.e = uVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    public void run() {
        AlertDialog create = new AlertDialog.Builder(this.a).create();
        create.show();
        create.getWindow().setContentView(aa.a((Context) this.a, "layout", "bluep_pay_opt"));
        ImageView imageView = (ImageView) create.getWindow().findViewById(aa.a((Context) this.a, "id", "iv_bluepay_logo"));
        TextView textView = (TextView) create.getWindow().findViewById(aa.a((Context) this.a, "id", "tv_label_phoneno"));
        TextView textView2 = (TextView) create.getWindow().findViewById(aa.a((Context) this.a, "id", "tv_label_verification"));
        EditText editText = (EditText) create.getWindow().findViewById(aa.a((Context) this.a, "id", "et_bluep_phone"));
        EditText editText2 = (EditText) create.getWindow().findViewById(aa.a((Context) this.a, "id", "et_bluepay_opt"));
        Button button = (Button) create.getWindow().findViewById(aa.a((Context) this.a, "id", "btn_bluepay_get_opt"));
        ImageView imageView2 = (ImageView) create.getWindow().findViewById(aa.a((Context) this.a, "id", "btn_bluepay_opt_cancel"));
        TextView textView3 = (TextView) create.getWindow().findViewById(aa.a((Context) this.a, "id", "tv_tips"));
        Button button2 = (Button) create.getWindow().findViewById(aa.a((Context) this.a, "id", "btn_bluepay_opt_confirm"));
        ProgressBar progressBar = (ProgressBar) create.getWindow().findViewById(aa.a((Context) this.a, "id", "pb_send_code"));
        if (!TextUtils.isEmpty(this.b)) {
            editText.setText(this.b);
            editText.setEnabled(false);
            g.b(progressBar, button, this.a, editText, editText2, this.c, this.d);
        } else if (!TextUtils.isEmpty(aa.n(this.a))) {
            editText.setText(aa.n(this.a));
        }
        textView.setText(i.a(i.aw));
        textView2.setText(i.a(i.ax));
        button2.setText(i.a((byte) 0));
        button2.setBackgroundResource(aa.a((Context) this.a, "drawable", "bluep_ui_confirm_bg_enable"));
        button2.setEnabled(false);
        editText2.addTextChangedListener(new i(this, editText2, button2));
        editText2.setOnTouchListener(new j(this, editText2, textView3));
        button.setOnClickListener(new k(this, editText, progressBar, button, editText2));
        imageView2.setOnClickListener(new l(this, progressBar, create));
        button2.setOnClickListener(new m(this, editText, editText2, create, textView3));
        create.getWindow().clearFlags(131080);
        create.setCancelable(false);
    }
}
