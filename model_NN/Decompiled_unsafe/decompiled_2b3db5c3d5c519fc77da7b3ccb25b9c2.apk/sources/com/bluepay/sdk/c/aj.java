package com.bluepay.sdk.c;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import com.bluepay.sdk.b.c;

/* compiled from: Proguard */
final class aj implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ Activity b;

    aj(String str, Activity activity) {
        this.a = str;
        this.b = activity;
    }

    public void run() {
        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.setData(Uri.parse(this.a));
            this.b.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            c.c("call wallet error");
        }
    }
}
