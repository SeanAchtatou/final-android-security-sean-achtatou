package com.bluepay.sdk.c;

import android.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import com.bluepay.data.i;
import com.bluepay.pay.Client;

/* compiled from: Proguard */
class ad implements View.OnClickListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ AlertDialog b;
    final /* synthetic */ ac c;

    ad(ac acVar, EditText editText, AlertDialog alertDialog) {
        this.c = acVar;
        this.a = editText;
        this.b = alertDialog;
    }

    public void onClick(View v) {
        String obj = this.a.getText().toString();
        if (TextUtils.isEmpty(obj) || TextUtils.isEmpty(aa.g(obj))) {
            aa.a(this.c.a, i.a((byte) i.ae, new Object[0]));
        } else if (Client.m_DcbInfo.q != 0 || !aa.h(obj)) {
            new Thread(new ae(this, obj)).start();
        } else {
            aa.a(this.c.a, i.a((byte) i.af, Client.telcoName));
        }
    }
}
