package com.bluepay.sdk.c;

import android.app.Activity;
import com.bluepay.pay.IPayCallback;

/* compiled from: Proguard */
final class ag implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ String b;
    final /* synthetic */ IPayCallback c;
    final /* synthetic */ Activity d;
    final /* synthetic */ int e;

    ag(String str, String str2, IPayCallback iPayCallback, Activity activity, int i) {
        this.a = str;
        this.b = str2;
        this.c = iPayCallback;
        this.d = activity;
        this.e = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x016b A[Catch:{ Exception -> 0x0134 }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x012b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r12 = this;
            r0 = 3
            r2 = 6
            com.bluepay.pay.BlueMessage r5 = new com.bluepay.pay.BlueMessage
            r5.<init>()
            java.lang.String r1 = r12.a
            r5.setPublisher(r1)
            java.lang.String r1 = ""
            r5.setOfflinePaymentCode(r1)
            java.lang.String r1 = r12.b
            r5.setTransactionId(r1)
            com.bluepay.pay.IPayCallback r1 = r12.c     // Catch:{ Exception -> 0x0134 }
            if (r1 == 0) goto L_0x0026
            java.lang.String r1 = r12.b     // Catch:{ Exception -> 0x0134 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0134 }
            if (r1 != 0) goto L_0x0026
            android.app.Activity r1 = r12.d     // Catch:{ Exception -> 0x0134 }
            if (r1 != 0) goto L_0x0038
        L_0x0026:
            int r0 = com.bluepay.data.h.i     // Catch:{ Exception -> 0x0134 }
            r5.setCode(r0)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r0 = "parameter error"
            r5.setDesc(r0)     // Catch:{ Exception -> 0x0134 }
            com.bluepay.pay.IPayCallback r0 = r12.c     // Catch:{ Exception -> 0x0134 }
            android.app.Activity r1 = r12.d     // Catch:{ Exception -> 0x0134 }
            com.bluepay.sdk.c.aa.b(r0, r1, r5)     // Catch:{ Exception -> 0x0134 }
        L_0x0037:
            return
        L_0x0038:
            int r1 = com.bluepay.pay.Client.m_iOperatorId     // Catch:{ Exception -> 0x0134 }
            android.app.Activity r3 = r12.d     // Catch:{ Exception -> 0x0134 }
            java.lang.String r4 = ""
            java.lang.String r6 = "Loading"
            com.bluepay.sdk.c.aa.a(r3, r4, r6)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r6 = com.bluepay.data.m.l()     // Catch:{ Exception -> 0x0134 }
            java.lang.String r3 = r12.a     // Catch:{ Exception -> 0x0134 }
            java.lang.String r4 = "line"
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x0134 }
            if (r3 == 0) goto L_0x0053
            r1 = 19
        L_0x0053:
            java.lang.String r3 = r12.a     // Catch:{ Exception -> 0x0134 }
            java.lang.String r4 = "atm"
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x0134 }
            if (r3 != 0) goto L_0x0067
            java.lang.String r3 = r12.a     // Catch:{ Exception -> 0x0134 }
            java.lang.String r4 = "otc"
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x0134 }
            if (r3 == 0) goto L_0x006b
        L_0x0067:
            com.bluepay.data.a r1 = com.bluepay.pay.Client.m_BankChargeInfo     // Catch:{ Exception -> 0x0134 }
            int r1 = r1.d     // Catch:{ Exception -> 0x0134 }
        L_0x006b:
            java.util.HashMap r7 = new java.util.HashMap     // Catch:{ Exception -> 0x0134 }
            r7.<init>()     // Catch:{ Exception -> 0x0134 }
            java.lang.String r3 = "operatorId"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0134 }
            r7.put(r3, r1)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r1 = "t_id"
            java.lang.String r3 = r12.b     // Catch:{ Exception -> 0x0134 }
            r7.put(r1, r3)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r1 = "productid"
            int r3 = com.bluepay.pay.Client.getProductId()     // Catch:{ Exception -> 0x0134 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0134 }
            r7.put(r1, r3)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r1 = "v"
            r3 = 100038002(0x5f67572, float:2.3176871E-35)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0134 }
            r7.put(r1, r3)     // Catch:{ Exception -> 0x0134 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0134 }
            r1.<init>()     // Catch:{ Exception -> 0x0134 }
            java.lang.String r3 = "operatorId="
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r3 = "operatorId"
            java.lang.Object r3 = r7.get(r3)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0134 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r3 = "&productid="
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r3 = "productid"
            java.lang.Object r3 = r7.get(r3)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0134 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r3 = "&t_id="
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r3 = "t_id"
            java.lang.Object r3 = r7.get(r3)     // Catch:{ Exception -> 0x0134 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r8 = r1.toString()     // Catch:{ Exception -> 0x0134 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0134 }
            r1.<init>()     // Catch:{ Exception -> 0x0134 }
            java.lang.StringBuilder r1 = r1.append(r8)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r3 = com.bluepay.pay.Client.getEncrypt()     // Catch:{ Exception -> 0x0134 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0134 }
            java.lang.String r1 = com.bluepay.sdk.c.e.a(r1)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r3 = "encrypt"
            r7.put(r3, r1)     // Catch:{ Exception -> 0x0134 }
            int r1 = r12.e     // Catch:{ Exception -> 0x0134 }
            if (r1 <= r0) goto L_0x0158
            int r1 = r12.e     // Catch:{ Exception -> 0x0134 }
            if (r1 >= r2) goto L_0x0158
            int r2 = r12.e     // Catch:{ Exception -> 0x0134 }
        L_0x0102:
            r1 = 2000(0x7d0, float:2.803E-42)
            java.lang.String r0 = ""
        L_0x0106:
            r4 = 0
            int r3 = r2 + -1
            android.app.Activity r2 = r12.d     // Catch:{ Exception -> 0x015e }
            com.bluepay.interfaceClass.b r2 = com.bluepay.sdk.a.a.a(r2, r6, r8, r7)     // Catch:{ Exception -> 0x015e }
            java.lang.String r2 = r2.b()     // Catch:{ Exception -> 0x015e }
            com.bluepay.a.b.aw r2 = com.bluepay.a.b.au.b(r2)     // Catch:{ Exception -> 0x015e }
            java.lang.String r4 = "status"
            java.lang.String r4 = r2.a(r4)     // Catch:{ Exception -> 0x01a7 }
            int r2 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x01a7 }
        L_0x0121:
            r5.setCode(r2)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r4 = "0"
            r5.setPrice(r4)     // Catch:{ Exception -> 0x0134 }
            if (r3 != 0) goto L_0x016b
            com.bluepay.pay.IPayCallback r0 = r12.c     // Catch:{ Exception -> 0x0134 }
            android.app.Activity r1 = r12.d     // Catch:{ Exception -> 0x0134 }
            com.bluepay.sdk.c.aa.b(r0, r1, r5)     // Catch:{ Exception -> 0x0134 }
            goto L_0x0037
        L_0x0134:
            r1 = move-exception
            r1.printStackTrace()
            boolean r0 = r1 instanceof com.bluepay.sdk.exception.BlueException
            if (r0 == 0) goto L_0x0198
            r0 = r1
            com.bluepay.sdk.exception.BlueException r0 = (com.bluepay.sdk.exception.BlueException) r0
            int r0 = r0.getCode()
            r5.setCode(r0)
            com.bluepay.sdk.exception.BlueException r1 = (com.bluepay.sdk.exception.BlueException) r1
            java.lang.String r0 = r1.getMessage()
            r5.setDesc(r0)
        L_0x014f:
            com.bluepay.pay.IPayCallback r0 = r12.c
            android.app.Activity r1 = r12.d
            com.bluepay.sdk.c.aa.b(r0, r1, r5)
            goto L_0x0037
        L_0x0158:
            int r1 = r12.e     // Catch:{ Exception -> 0x0134 }
            if (r1 > r2) goto L_0x0102
            r2 = r0
            goto L_0x0102
        L_0x015e:
            r0 = move-exception
            r2 = r4
        L_0x0160:
            java.lang.String r4 = "status"
            int r2 = r2.c(r4)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ Exception -> 0x0134 }
            goto L_0x0121
        L_0x016b:
            int r4 = com.bluepay.data.h.b     // Catch:{ Exception -> 0x0134 }
            if (r2 == r4) goto L_0x0173
            int r4 = com.bluepay.data.h.h     // Catch:{ Exception -> 0x0134 }
            if (r2 != r4) goto L_0x018c
        L_0x0173:
            int r2 = r1 + 1
            long r10 = (long) r1     // Catch:{ Exception -> 0x0134 }
            java.lang.Thread.sleep(r10)     // Catch:{ Exception -> 0x0134 }
            if (r3 != 0) goto L_0x01a9
            int r1 = com.bluepay.data.h.h     // Catch:{ Exception -> 0x0134 }
            r5.setCode(r1)     // Catch:{ Exception -> 0x0134 }
            r5.setDesc(r0)     // Catch:{ Exception -> 0x0134 }
            com.bluepay.pay.IPayCallback r0 = r12.c     // Catch:{ Exception -> 0x0134 }
            android.app.Activity r1 = r12.d     // Catch:{ Exception -> 0x0134 }
            com.bluepay.sdk.c.aa.b(r0, r1, r5)     // Catch:{ Exception -> 0x0134 }
            goto L_0x0037
        L_0x018c:
            r5.setDesc(r0)     // Catch:{ Exception -> 0x0134 }
            com.bluepay.pay.IPayCallback r0 = r12.c     // Catch:{ Exception -> 0x0134 }
            android.app.Activity r1 = r12.d     // Catch:{ Exception -> 0x0134 }
            com.bluepay.sdk.c.aa.b(r0, r1, r5)     // Catch:{ Exception -> 0x0134 }
            goto L_0x0037
        L_0x0198:
            int r0 = com.bluepay.data.h.i
            r5.setCode(r0)
            r0 = 44
            java.lang.String r0 = com.bluepay.data.i.a(r0)
            r5.setDesc(r0)
            goto L_0x014f
        L_0x01a7:
            r0 = move-exception
            goto L_0x0160
        L_0x01a9:
            r1 = r2
            r2 = r3
            goto L_0x0106
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bluepay.sdk.c.ag.run():void");
    }
}
