package com.bluepay.sdk.c;

import android.content.Context;
import com.bluepay.interfaceClass.d;
import com.bluepay.pay.Client;
import com.bluepay.pay.ClientHelper;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.exception.BlueException;

/* compiled from: Proguard */
final class ar implements Runnable {
    final /* synthetic */ Context a;
    final /* synthetic */ d b;

    ar(Context context, d dVar) {
        this.a = context;
        this.b = dVar;
    }

    public void run() {
        int i = 3;
        boolean z = false;
        while (i > 0) {
            try {
                String c = aa.c(Client.m_iIMSI);
                if (c != null) {
                    Client.setMsNum(c);
                    c.c("query msisdn:" + c);
                    aa.f(this.a, c);
                    this.b.a(1, c);
                    return;
                }
                if (!z) {
                    c.c("now send a message to server to explore msisdn");
                    v.a(this.a, Client.m_DcbInfo.c, ClientHelper.getExploreContent());
                    z = true;
                }
                int i2 = i - 1;
                try {
                    Thread.currentThread();
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                i = i2;
            } catch (BlueException e2) {
                e2.printStackTrace();
                this.b.a(-1, null);
                c.b("there is a error occured when explore the msisdn :" + e2.getMessage());
                return;
            }
        }
        this.b.a(-1, "");
    }
}
