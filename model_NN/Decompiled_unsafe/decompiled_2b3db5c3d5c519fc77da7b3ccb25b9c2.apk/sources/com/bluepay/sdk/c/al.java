package com.bluepay.sdk.c;

import com.bluepay.data.Order;

/* compiled from: Proguard */
final class al implements Runnable {
    final /* synthetic */ Order a;
    final /* synthetic */ String b;
    final /* synthetic */ int[] c;

    al(Order order, String str, int[] iArr) {
        this.a = order;
        this.b = str;
        this.c = iArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void */
    /* JADX WARNING: Failed to process nested try/catch */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r15 = this;
            r1 = 3
            r2 = 6
            r13 = 14
            r12 = 0
            com.bluepay.data.Order r0 = r15.a
            com.bluepay.data.b r0 = (com.bluepay.data.b) r0
            java.lang.String r4 = com.bluepay.data.m.m()
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            if (r0 == 0) goto L_0x0156
            int r3 = r0.a()
            r6 = 8
            if (r3 != r6) goto L_0x0156
            java.lang.String r3 = "operatorId"
            r6 = 21
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            r5.put(r3, r6)
        L_0x0027:
            java.lang.String r3 = "t_id"
            java.lang.String r6 = r15.b
            r5.put(r3, r6)
            java.lang.String r3 = "productid"
            int r6 = com.bluepay.pay.Client.getProductId()
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            r5.put(r3, r6)
            java.lang.String r3 = "v"
            r6 = 100038002(0x5f67572, float:2.3176871E-35)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            r5.put(r3, r6)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r6 = "operatorId="
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r6 = "operatorId"
            java.lang.Object r6 = r5.get(r6)
            java.lang.String r6 = r6.toString()
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r6 = "&productid="
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r6 = "productid"
            java.lang.Object r6 = r5.get(r6)
            java.lang.String r6 = r6.toString()
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r6 = "&t_id="
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r6 = "t_id"
            java.lang.Object r6 = r5.get(r6)
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r6 = r3.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r7 = com.bluepay.pay.Client.getEncrypt()
            java.lang.StringBuilder r3 = r3.append(r7)
            java.lang.String r3 = r3.toString()
            java.lang.String r3 = com.bluepay.sdk.c.e.a(r3)
            java.lang.String r7 = "encrypt"
            r5.put(r7, r3)
            int[] r3 = r15.c
            int r3 = r3.length
            if (r3 <= 0) goto L_0x02fd
            int[] r3 = r15.c
            r3 = r3[r12]
            if (r3 >= r1) goto L_0x0163
        L_0x00b1:
            r2 = 4
            r3 = 5000(0x1388, float:7.006E-42)
            java.lang.String r7 = "601"
            long r8 = (long) r3
            java.lang.Thread.sleep(r8)     // Catch:{ Exception -> 0x01c1 }
            r14 = r2
            r2 = r1
            r1 = r14
        L_0x00bd:
            boolean r3 = r0.getShowUI()     // Catch:{ Exception -> 0x01c1 }
            if (r3 == 0) goto L_0x00e4
            r3 = 6
            int[] r7 = r15.c     // Catch:{ Exception -> 0x01c1 }
            r8 = 0
            r7 = r7[r8]     // Catch:{ Exception -> 0x01c1 }
            int r3 = r3 / r7
            int r1 = r1 + r3
            android.app.Activity r3 = r0.getActivity()     // Catch:{ Exception -> 0x01c1 }
            java.lang.String r7 = ""
            r8 = 10
            r9 = 1
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x01c1 }
            r10 = 0
            java.lang.Integer r11 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x01c1 }
            r9[r10] = r11     // Catch:{ Exception -> 0x01c1 }
            java.lang.String r8 = com.bluepay.data.i.a(r8, r9)     // Catch:{ Exception -> 0x01c1 }
            com.bluepay.sdk.c.aa.a(r3, r7, r8)     // Catch:{ Exception -> 0x01c1 }
        L_0x00e4:
            android.app.Activity r3 = r0.getActivity()     // Catch:{ BlueException -> 0x01b0 }
            com.bluepay.interfaceClass.b r3 = com.bluepay.sdk.a.a.a(r3, r4, r6, r5)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r3 = r3.b()     // Catch:{ BlueException -> 0x01b0 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ BlueException -> 0x01b0 }
            r7.<init>()     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r8 = "result:"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.StringBuilder r7 = r7.append(r3)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r7 = r7.toString()     // Catch:{ BlueException -> 0x01b0 }
            com.bluepay.sdk.b.c.c(r7)     // Catch:{ BlueException -> 0x01b0 }
            com.bluepay.a.b.aw r3 = com.bluepay.a.b.au.b(r3)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r7 = new java.lang.String     // Catch:{ Exception -> 0x0172, BlueException -> 0x01b0 }
            java.lang.String r8 = "status"
            java.lang.String r8 = r3.a(r8)     // Catch:{ Exception -> 0x0172, BlueException -> 0x01b0 }
            r7.<init>(r8)     // Catch:{ Exception -> 0x0172, BlueException -> 0x01b0 }
            java.lang.String r3 = r7.trim()     // Catch:{ Exception -> 0x0172, BlueException -> 0x01b0 }
        L_0x0119:
            java.lang.String r7 = "404"
            boolean r7 = r3.equals(r7)     // Catch:{ BlueException -> 0x01b0 }
            if (r7 != 0) goto L_0x02ef
            java.lang.String r7 = "201"
            boolean r7 = r3.equals(r7)     // Catch:{ BlueException -> 0x01b0 }
            if (r7 != 0) goto L_0x02ef
            java.lang.String r7 = "0"
            boolean r7 = r3.equals(r7)     // Catch:{ BlueException -> 0x01b0 }
            if (r7 == 0) goto L_0x0196
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ BlueException -> 0x01b0 }
            r7.<init>()     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r8 = "default code:"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.StringBuilder r7 = r7.append(r3)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r7 = r7.toString()     // Catch:{ BlueException -> 0x01b0 }
            com.bluepay.sdk.b.c.c(r7)     // Catch:{ BlueException -> 0x01b0 }
        L_0x0147:
            if (r2 != 0) goto L_0x00bd
            com.bluepay.a.b.n r1 = com.bluepay.a.b.a.o     // Catch:{ Exception -> 0x01c1 }
            r2 = 14
            int r3 = java.lang.Integer.parseInt(r3)     // Catch:{ Exception -> 0x01c1 }
            r4 = 0
            r1.a(r2, r3, r4, r0)     // Catch:{ Exception -> 0x01c1 }
        L_0x0155:
            return
        L_0x0156:
            java.lang.String r3 = "operatorId"
            int r6 = com.bluepay.pay.Client.m_iOperatorId
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            r5.put(r3, r6)
            goto L_0x0027
        L_0x0163:
            int[] r1 = r15.c
            r1 = r1[r12]
            if (r1 <= r2) goto L_0x016c
            r1 = r2
            goto L_0x00b1
        L_0x016c:
            int[] r1 = r15.c
            r1 = r1[r12]
            goto L_0x00b1
        L_0x0172:
            r7 = move-exception
            java.lang.String r7 = new java.lang.String     // Catch:{ BlueException -> 0x01b0 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ BlueException -> 0x01b0 }
            r8.<init>()     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r9 = "status"
            java.lang.String r3 = r3.a(r9)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.StringBuilder r3 = r8.append(r3)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r8 = ""
            java.lang.StringBuilder r3 = r3.append(r8)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r3 = r3.toString()     // Catch:{ BlueException -> 0x01b0 }
            r7.<init>(r3)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r3 = r7.trim()     // Catch:{ BlueException -> 0x01b0 }
            goto L_0x0119
        L_0x0196:
            int r7 = r0.a()     // Catch:{ BlueException -> 0x01b0 }
            switch(r7) {
                case 3: goto L_0x01d0;
                case 4: goto L_0x01d0;
                default: goto L_0x019d;
            }     // Catch:{ BlueException -> 0x01b0 }
        L_0x019d:
            java.lang.String r7 = "200"
            boolean r7 = r3.equals(r7)     // Catch:{ BlueException -> 0x01b0 }
            if (r7 == 0) goto L_0x0218
            com.bluepay.a.b.n r1 = com.bluepay.a.b.a.o     // Catch:{ BlueException -> 0x01b0 }
            r2 = 14
            int r3 = com.bluepay.data.h.a     // Catch:{ BlueException -> 0x01b0 }
            r4 = 0
            r1.a(r2, r3, r4, r0)     // Catch:{ BlueException -> 0x01b0 }
            goto L_0x0155
        L_0x01b0:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ Exception -> 0x01c1 }
            com.bluepay.a.b.n r2 = com.bluepay.a.b.a.o     // Catch:{ Exception -> 0x01c1 }
            r3 = 14
            int r1 = r1.getCode()     // Catch:{ Exception -> 0x01c1 }
            r4 = 0
            r2.a(r3, r1, r4, r0)     // Catch:{ Exception -> 0x01c1 }
            goto L_0x0155
        L_0x01c1:
            r1 = move-exception
            java.lang.String r1 = r1.getMessage()
            r0.desc = r1
            com.bluepay.a.b.n r1 = com.bluepay.a.b.a.o
            int r2 = com.bluepay.data.h.i
            r1.a(r13, r2, r12, r0)
            goto L_0x0155
        L_0x01d0:
            boolean r7 = r0.i()     // Catch:{ BlueException -> 0x01b0 }
            if (r7 == 0) goto L_0x019d
            java.lang.String r1 = "200"
            boolean r1 = r3.equals(r1)     // Catch:{ BlueException -> 0x01b0 }
            if (r1 == 0) goto L_0x01f8
            java.lang.String r1 = r0.g()     // Catch:{ BlueException -> 0x01b0 }
            int r2 = r0.getPrice()     // Catch:{ BlueException -> 0x01b0 }
            int r1 = com.bluepay.a.b.a.a(r1, r2)     // Catch:{ BlueException -> 0x01b0 }
            r0.setPrice(r1)     // Catch:{ BlueException -> 0x01b0 }
            com.bluepay.a.b.n r1 = com.bluepay.a.b.a.o     // Catch:{ BlueException -> 0x01b0 }
            r2 = 3
            int r3 = com.bluepay.data.h.a     // Catch:{ BlueException -> 0x01b0 }
            r4 = 0
            r1.a(r2, r3, r4, r0)     // Catch:{ BlueException -> 0x01b0 }
            goto L_0x0155
        L_0x01f8:
            java.lang.String r1 = "605"
            boolean r1 = r3.equals(r1)     // Catch:{ BlueException -> 0x01b0 }
            if (r1 == 0) goto L_0x020c
            com.bluepay.a.b.n r1 = com.bluepay.a.b.a.o     // Catch:{ BlueException -> 0x01b0 }
            r2 = 14
            int r3 = com.bluepay.data.h.h     // Catch:{ BlueException -> 0x01b0 }
            r4 = 0
            r1.a(r2, r3, r4, r0)     // Catch:{ BlueException -> 0x01b0 }
            goto L_0x0155
        L_0x020c:
            com.bluepay.a.b.n r1 = com.bluepay.a.b.a.o     // Catch:{ BlueException -> 0x01b0 }
            r2 = 14
            int r3 = com.bluepay.data.h.h     // Catch:{ BlueException -> 0x01b0 }
            r4 = 0
            r1.a(r2, r3, r4, r0)     // Catch:{ BlueException -> 0x01b0 }
            goto L_0x0155
        L_0x0218:
            java.lang.String r7 = "601"
            boolean r7 = r3.equals(r7)     // Catch:{ BlueException -> 0x01b0 }
            if (r7 == 0) goto L_0x022c
            com.bluepay.a.b.n r1 = com.bluepay.a.b.a.o     // Catch:{ BlueException -> 0x01b0 }
            r2 = 14
            int r3 = com.bluepay.data.h.A     // Catch:{ BlueException -> 0x01b0 }
            r4 = 0
            r1.a(r2, r3, r4, r0)     // Catch:{ BlueException -> 0x01b0 }
            goto L_0x0155
        L_0x022c:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ BlueException -> 0x01b0 }
            r7.<init>()     // Catch:{ BlueException -> 0x01b0 }
            int r8 = com.bluepay.data.h.t     // Catch:{ BlueException -> 0x01b0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r8 = ""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r7 = r7.toString()     // Catch:{ BlueException -> 0x01b0 }
            boolean r7 = r3.equals(r7)     // Catch:{ BlueException -> 0x01b0 }
            if (r7 == 0) goto L_0x0253
            com.bluepay.a.b.n r1 = com.bluepay.a.b.a.o     // Catch:{ BlueException -> 0x01b0 }
            r2 = 14
            int r3 = com.bluepay.data.h.t     // Catch:{ BlueException -> 0x01b0 }
            r4 = 0
            r1.a(r2, r3, r4, r0)     // Catch:{ BlueException -> 0x01b0 }
            goto L_0x0155
        L_0x0253:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ BlueException -> 0x01b0 }
            r7.<init>()     // Catch:{ BlueException -> 0x01b0 }
            int r8 = com.bluepay.data.h.J     // Catch:{ BlueException -> 0x01b0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r8 = ""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r7 = r7.toString()     // Catch:{ BlueException -> 0x01b0 }
            boolean r7 = r3.equals(r7)     // Catch:{ BlueException -> 0x01b0 }
            if (r7 == 0) goto L_0x027a
            com.bluepay.a.b.n r1 = com.bluepay.a.b.a.o     // Catch:{ BlueException -> 0x01b0 }
            r2 = 14
            int r3 = com.bluepay.data.h.J     // Catch:{ BlueException -> 0x01b0 }
            r4 = 0
            r1.a(r2, r3, r4, r0)     // Catch:{ BlueException -> 0x01b0 }
            goto L_0x0155
        L_0x027a:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ BlueException -> 0x01b0 }
            r7.<init>()     // Catch:{ BlueException -> 0x01b0 }
            int r8 = com.bluepay.data.h.q     // Catch:{ BlueException -> 0x01b0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r8 = ""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r7 = r7.toString()     // Catch:{ BlueException -> 0x01b0 }
            boolean r7 = r3.equals(r7)     // Catch:{ BlueException -> 0x01b0 }
            if (r7 == 0) goto L_0x02a1
            com.bluepay.a.b.n r1 = com.bluepay.a.b.a.o     // Catch:{ BlueException -> 0x01b0 }
            r2 = 14
            int r3 = com.bluepay.data.h.q     // Catch:{ BlueException -> 0x01b0 }
            r4 = 0
            r1.a(r2, r3, r4, r0)     // Catch:{ BlueException -> 0x01b0 }
            goto L_0x0155
        L_0x02a1:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ BlueException -> 0x01b0 }
            r7.<init>()     // Catch:{ BlueException -> 0x01b0 }
            int r8 = com.bluepay.data.h.r     // Catch:{ BlueException -> 0x01b0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r8 = ""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r7 = r7.toString()     // Catch:{ BlueException -> 0x01b0 }
            boolean r7 = r3.equals(r7)     // Catch:{ BlueException -> 0x01b0 }
            if (r7 == 0) goto L_0x02c8
            com.bluepay.a.b.n r1 = com.bluepay.a.b.a.o     // Catch:{ BlueException -> 0x01b0 }
            r2 = 14
            int r3 = com.bluepay.data.h.r     // Catch:{ BlueException -> 0x01b0 }
            r4 = 0
            r1.a(r2, r3, r4, r0)     // Catch:{ BlueException -> 0x01b0 }
            goto L_0x0155
        L_0x02c8:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ BlueException -> 0x01b0 }
            r7.<init>()     // Catch:{ BlueException -> 0x01b0 }
            int r8 = com.bluepay.data.h.l     // Catch:{ BlueException -> 0x01b0 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r8 = ""
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ BlueException -> 0x01b0 }
            java.lang.String r7 = r7.toString()     // Catch:{ BlueException -> 0x01b0 }
            boolean r7 = r3.equals(r7)     // Catch:{ BlueException -> 0x01b0 }
            if (r7 == 0) goto L_0x02ef
            com.bluepay.a.b.n r1 = com.bluepay.a.b.a.o     // Catch:{ BlueException -> 0x01b0 }
            r2 = 14
            int r3 = com.bluepay.data.h.l     // Catch:{ BlueException -> 0x01b0 }
            r4 = 0
            r1.a(r2, r3, r4, r0)     // Catch:{ BlueException -> 0x01b0 }
            goto L_0x0155
        L_0x02ef:
            int[] r7 = com.bluepay.sdk.c.aa.k     // Catch:{ Exception -> 0x01c1 }
            r7 = r7[r2]     // Catch:{ Exception -> 0x01c1 }
            long r8 = (long) r7     // Catch:{ Exception -> 0x01c1 }
            java.lang.Thread.sleep(r8)     // Catch:{ Exception -> 0x01c1 }
            int r2 = r2 + -1
            goto L_0x0147
        L_0x02fd:
            r1 = r2
            goto L_0x00b1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bluepay.sdk.c.al.run():void");
    }
}
