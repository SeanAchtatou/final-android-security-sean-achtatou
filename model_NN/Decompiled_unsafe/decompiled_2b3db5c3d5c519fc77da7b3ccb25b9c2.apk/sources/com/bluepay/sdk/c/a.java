package com.bluepay.sdk.c;

import android.app.Activity;
import android.content.Context;
import android.os.CountDownTimer;
import android.widget.Button;
import com.bluepay.sdk.b.c;

/* compiled from: Proguard */
public class a extends CountDownTimer {
    private Context a;
    /* access modifiers changed from: private */
    public Button b;

    public a(long j, long j2) {
        super(j, j2);
    }

    public a(long j, long j2, Context context, Button button) {
        super(j, j2);
        this.a = context;
        this.b = button;
    }

    public void onTick(long millisUntilFinished) {
        ((Activity) this.a).runOnUiThread(new b(this, millisUntilFinished));
    }

    public void onFinish() {
        c.c("timer finish");
        ((Activity) this.a).runOnUiThread(new c(this));
    }
}
