package com.bluepay.sdk.c;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.bluepay.a.b.aw;
import com.google.android.gms.common.zze;
import java.util.Iterator;
import java.util.Random;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Proguard */
public class x {
    public static boolean a(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String string = jSONObject.getString("checksum");
            if (TextUtils.isEmpty(string)) {
                return false;
            }
            Iterator<String> keys = jSONObject.keys();
            if (keys == null || !keys.hasNext()) {
                return false;
            }
            StringBuilder sb = new StringBuilder();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next.equals("mid") || next.equals("encrypt")) {
                    String string2 = jSONObject.getString(next);
                    sb.append(next);
                    sb.append("=");
                    sb.append(string2);
                    sb.append("&");
                }
            }
            if (sb.length() <= 0) {
                return false;
            }
            sb.deleteCharAt(sb.length() - 1);
            String sb2 = sb.toString();
            if (!sb2.contains("mid") || !sb2.contains("encrypt")) {
                return false;
            }
            return e.a(b(sb2).getBytes()).equals(string);
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int length = str.length();
        for (int i = 0; i < length; i += 4) {
            sb.append(str.charAt(i));
        }
        for (int i2 = 0; i2 < length; i2 += 3) {
            sb.append(str.charAt(i2));
        }
        return sb.toString();
    }

    public static String c(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int length = str.length();
        for (int i = 0; i < length; i += 3) {
            sb.append(str.charAt(i));
        }
        for (int i2 = 0; i2 < length; i2 += 4) {
            sb.append(str.charAt(i2));
        }
        return sb.toString();
    }

    public static String d(String str) {
        int length = str.length();
        if (length >= 20) {
            return str;
        }
        int i = 20 - length;
        StringBuilder sb = new StringBuilder(str);
        Random random = new Random();
        int length2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-*.".length();
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-*.".charAt(random.nextInt(length2)));
        }
        return sb.toString();
    }

    public static boolean a(aw awVar) {
        String b = awVar.b("status");
        String b2 = awVar.b("tid");
        String str = "tid=" + b2 + "&status=" + b;
        return e.a(str + c(str)).equals(awVar.b("checksum"));
    }

    public static String e(String str) {
        int length = str.length();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt < '0' || charAt > '9') {
                break;
            }
            sb.append(charAt);
        }
        return sb.toString();
    }

    public static void a(Activity activity, String str) {
        try {
            Intent launchIntentForPackage = activity.getPackageManager().getLaunchIntentForPackage(zze.GOOGLE_PLAY_STORE_PACKAGE);
            launchIntentForPackage.setComponent(new ComponentName(zze.GOOGLE_PLAY_STORE_PACKAGE, "com.google.android.finsky.activities.LaunchUrlHandlerActivity"));
            launchIntentForPackage.setData(Uri.parse("market://details?id=" + str));
            activity.startActivity(launchIntentForPackage);
        } catch (Exception e) {
            try {
                activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + str)));
            } catch (Exception e2) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://play.Google.com/store/apps/details?id=" + str));
                intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                activity.startActivity(intent);
            }
        }
    }

    public static void b(Activity activity, String str) {
        try {
            Intent launchIntentForPackage = activity.getPackageManager().getLaunchIntentForPackage(zze.GOOGLE_PLAY_STORE_PACKAGE);
            launchIntentForPackage.setComponent(new ComponentName(zze.GOOGLE_PLAY_STORE_PACKAGE, "com.google.android.finsky.activities.LaunchUrlHandlerActivity"));
            launchIntentForPackage.setData(Uri.parse(str));
            activity.startActivity(launchIntentForPackage);
        } catch (Exception e) {
            try {
                activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            } catch (Exception e2) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                activity.startActivity(intent);
            }
        }
    }

    public static String f(String str) {
        if (TextUtils.isEmpty(str)) {
            return "0000000000000000";
        }
        if (str.length() > 16) {
            return str.substring(0, 16);
        }
        if (str.length() >= 16) {
            return str;
        }
        int length = 16 - str.length();
        for (int i = 1; i <= length; i++) {
            str = str + "0";
        }
        return str;
    }
}
