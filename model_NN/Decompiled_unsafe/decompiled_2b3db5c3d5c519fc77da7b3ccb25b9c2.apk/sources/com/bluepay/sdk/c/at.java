package com.bluepay.sdk.c;

import android.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import com.bluepay.data.i;

/* compiled from: Proguard */
class at implements View.OnClickListener {
    final /* synthetic */ EditText a;
    final /* synthetic */ AlertDialog b;
    final /* synthetic */ as c;

    at(as asVar, EditText editText, AlertDialog alertDialog) {
        this.c = asVar;
        this.a = editText;
        this.b = alertDialog;
    }

    public void onClick(View v) {
        String obj = this.a.getText().toString();
        if (TextUtils.isEmpty(obj)) {
            aa.a(this.c.a, i.a((byte) i.ae, new Object[0]));
        } else {
            new Thread(new au(this, obj)).start();
        }
    }
}
