package com.bluepay.sdk.c;

import android.app.AlertDialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.bluepay.data.i;
import com.bluepay.interfaceClass.d;

/* compiled from: Proguard */
final class ac implements Runnable {
    final /* synthetic */ Context a;
    final /* synthetic */ CharSequence b;
    final /* synthetic */ d c;

    ac(Context context, CharSequence charSequence, d dVar) {
        this.a = context;
        this.b = charSequence;
        this.c = dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, float):int
     arg types: [android.content.Context, int]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, int):int
      com.bluepay.sdk.c.aa.a(java.lang.Object, int):int
      com.bluepay.sdk.c.aa.a(java.lang.String, int):int
      com.bluepay.sdk.c.aa.a(java.lang.String, java.lang.String):java.lang.String
      com.bluepay.sdk.c.aa.a(android.content.Context, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String):void
      com.bluepay.sdk.c.aa.a(java.lang.StringBuffer, byte):void
      com.bluepay.sdk.c.aa.a(java.util.List, java.lang.String):boolean
      com.bluepay.sdk.c.aa.a(byte[], byte[]):byte[]
      com.bluepay.sdk.c.aa.a(android.content.Context, float):int */
    public void run() {
        aa.d();
        AlertDialog b2 = aa.d(this.a, "Tips", this.b);
        LinearLayout linearLayout = new LinearLayout(this.a);
        linearLayout.setGravity(17);
        linearLayout.setPadding(aa.a(this.a, 10.0f), aa.a(this.a, 10.0f), aa.a(this.a, 10.0f), aa.a(this.a, 5.0f));
        EditText editText = new EditText(this.a);
        editText.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        if (!TextUtils.isEmpty(aa.n(this.a))) {
            editText.setText(aa.n(this.a));
        }
        editText.setHint(i.a((byte) 3));
        editText.setInputType(3);
        linearLayout.addView(editText);
        b2.setView(linearLayout);
        b2.show();
        b2.getButton(-1).setOnClickListener(new ad(this, editText, b2));
        b2.getButton(-2).setOnClickListener(new af(this, b2));
    }
}
