package com.bluepay.sdk.c;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.telephony.SmsManager;
import android.util.Log;
import com.bluepay.data.b;
import com.bluepay.pay.Client;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.exception.BlueException;
import java.lang.reflect.Method;

/* compiled from: Proguard */
public class v {
    private static final String a = "SENT_SMS_ACTION";
    private static String b = "SMS_DELIVERED_ACTION";
    /* access modifiers changed from: private */
    public static w c;

    public static boolean a(b bVar, String str) {
        bVar.desc = new String(str);
        c.c("preStr:" + str);
        if (bVar.a() != 8) {
            return b(bVar, str);
        }
        c.c("PayByBank");
        String l = bVar.l();
        String str2 = "";
        if (Client.CONTRY_CODE == 86) {
            str2 = bVar.getPrice() + "_";
        }
        return a(bVar, Client.m_BankChargeInfo.b, str, str2, l);
    }

    public static boolean a(Context context, String str, String str2) {
        try {
            a(context, (b) null, str, str2, (PendingIntent) null, (PendingIntent) null);
            return true;
        } catch (BlueException e) {
            c.b(e.getMessage());
            return false;
        }
    }

    public static boolean b(b bVar, String str) {
        return a(bVar, bVar.k(), str, bVar.m(), bVar.l());
    }

    @SuppressLint({"NewApi"})
    private static boolean a(b bVar, String str, String str2, String str3, String str4) {
        Activity activity = bVar.getActivity();
        PendingIntent broadcast = PendingIntent.getBroadcast(activity, 0, new Intent(a), 0);
        PendingIntent broadcast2 = PendingIntent.getBroadcast(activity, 0, new Intent(b), 0);
        c.c("preStr:" + str2 + "  smsPre:" + str3 + "   content:" + str4);
        String str5 = str2 + str3 + str4;
        Log.i(Client.TAG, "content:   " + str5);
        a(activity, bVar, str, str5, broadcast, broadcast2);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0071, code lost:
        throw new com.bluepay.sdk.exception.BlueException(com.bluepay.data.h.i, "please granted send sms permission then try again.", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x009f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00ad, code lost:
        throw new com.bluepay.sdk.exception.BlueException(com.bluepay.data.h.i, r0.getMessage(), new java.lang.Object[0]);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0065 A[ExcHandler: SecurityException (e java.lang.SecurityException), Splitter:B:1:0x0003] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r7, java.lang.String r8, java.lang.String r9, android.app.PendingIntent r10, android.app.PendingIntent r11) {
        /*
            r6 = 0
            java.lang.String r0 = "BluePay"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            r1.<init>()     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            java.lang.String r2 = "content:   "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            java.lang.StringBuilder r1 = r1.append(r9)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            java.lang.String r1 = r1.toString()     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            android.util.Log.i(r0, r1)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            r0.<init>()     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            java.lang.String r1 = "content:"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            java.lang.String r1 = " length:"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            int r1 = r9.length()     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            java.lang.String r0 = r0.toString()     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            com.bluepay.sdk.b.c.c(r0)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            boolean r0 = com.bluepay.sdk.c.aa.a()     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            if (r0 != 0) goto L_0x0072
            byte r3 = com.bluepay.data.Config.MOBILE_PHONE_TYPE     // Catch:{ Exception -> 0x0054, SecurityException -> 0x0065 }
            r0 = r7
            r1 = r8
            r2 = r9
            r4 = r10
            r5 = r11
            a(r0, r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0054, SecurityException -> 0x0065 }
        L_0x004d:
            java.lang.String r0 = "send sms finish"
            com.bluepay.sdk.b.c.c(r0)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            r0 = 1
            return r0
        L_0x0054:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            android.telephony.SmsManager r0 = android.telephony.SmsManager.getDefault()     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            r2 = 0
            r1 = r8
            r3 = r9
            r4 = r10
            r5 = r11
            r0.sendTextMessage(r1, r2, r3, r4, r5)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            goto L_0x004d
        L_0x0065:
            r0 = move-exception
            com.bluepay.sdk.exception.BlueException r0 = new com.bluepay.sdk.exception.BlueException
            int r1 = com.bluepay.data.h.i
            java.lang.String r2 = "please granted send sms permission then try again."
            java.lang.Object[] r3 = new java.lang.Object[r6]
            r0.<init>(r1, r2, r3)
            throw r0
        L_0x0072:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            r0.<init>()     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            java.lang.String r1 = "sigle sim send,dest:"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            java.lang.StringBuilder r0 = r0.append(r8)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            java.lang.String r1 = " content:"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            java.lang.String r0 = r0.toString()     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            com.bluepay.sdk.b.c.c(r0)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            android.telephony.SmsManager r0 = android.telephony.SmsManager.getDefault()     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            r2 = 0
            r1 = r8
            r3 = r9
            r4 = r10
            r5 = r11
            r0.sendTextMessage(r1, r2, r3, r4, r5)     // Catch:{ SecurityException -> 0x0065, Exception -> 0x009f }
            goto L_0x004d
        L_0x009f:
            r0 = move-exception
            com.bluepay.sdk.exception.BlueException r1 = new com.bluepay.sdk.exception.BlueException
            int r2 = com.bluepay.data.h.i
            java.lang.String r0 = r0.getMessage()
            java.lang.Object[] r3 = new java.lang.Object[r6]
            r1.<init>(r2, r0, r3)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bluepay.sdk.c.v.a(android.content.Context, java.lang.String, java.lang.String, android.app.PendingIntent, android.app.PendingIntent):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0106, code lost:
        throw new com.bluepay.sdk.exception.BlueException(com.bluepay.data.h.i, "please granted send sms permission then try again.", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0135, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0143, code lost:
        throw new com.bluepay.sdk.exception.BlueException(com.bluepay.data.h.i, r0.getMessage(), new java.lang.Object[0]);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00fa A[ExcHandler: SecurityException (e java.lang.SecurityException), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(android.content.Context r7, com.bluepay.data.b r8, java.lang.String r9, java.lang.String r10, android.app.PendingIntent r11, android.app.PendingIntent r12) {
        /*
            r6 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            r0.<init>()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r1 = "content:"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r0 = r0.append(r10)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r1 = " length:"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            int r1 = r10.length()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r0 = r0.toString()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            com.bluepay.sdk.b.c.c(r0)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            boolean r0 = com.bluepay.sdk.c.aa.a()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            if (r0 != 0) goto L_0x0107
            byte r3 = com.bluepay.data.Config.MOBILE_PHONE_TYPE     // Catch:{ Exception -> 0x00c8, SecurityException -> 0x00fa }
            r0 = r7
            r1 = r9
            r2 = r10
            r4 = r11
            r5 = r12
            a(r0, r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00c8, SecurityException -> 0x00fa }
        L_0x0035:
            java.lang.String r0 = "send sms finish"
            com.bluepay.sdk.b.c.c(r0)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r0 = com.bluepay.pay.Client.m_iIMSI     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            boolean r0 = com.bluepay.sdk.c.aa.a()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            if (r0 != 0) goto L_0x0144
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            r0.<init>()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r1 = "double sim|imsi1:"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r1 = com.bluepay.pay.Client.m_iIMSI1     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r1 = "|imsi2:"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r1 = com.bluepay.pay.Client.m_iIMSI2     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r1 = "|send_imsi:"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r1 = com.bluepay.pay.Client.m_iIMSI     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r0 = r0.toString()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
        L_0x006f:
            android.app.Activity r1 = r8.getActivity()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            r2.<init>()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r3 = r8.getTransactionId()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r3 = com.bluepay.pay.Client.telcoName     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r3 = "|"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r2 = r2.toString()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            r3.<init>()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r4 = "Phone Model:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r4 = com.bluepay.pay.Client.m_Model     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r3 = r3.toString()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            r4 = 10
            com.bluepay.sdk.c.aa.b(r1, r2, r3, r0, r4)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            com.bluepay.a.b.n r0 = com.bluepay.a.b.a.o     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            r1 = 2
            r2 = 0
            r3 = 0
            r0.a(r1, r2, r3, r8)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            r0 = 1
            return r0
        L_0x00c8:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            r0.<init>()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r1 = "sigle sim send,dest:"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r1 = " content:"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r0 = r0.append(r10)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r0 = r0.toString()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            com.bluepay.sdk.b.c.c(r0)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            android.telephony.SmsManager r0 = android.telephony.SmsManager.getDefault()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            r2 = 0
            r1 = r9
            r3 = r10
            r4 = r11
            r5 = r12
            r0.sendTextMessage(r1, r2, r3, r4, r5)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            goto L_0x0035
        L_0x00fa:
            r0 = move-exception
            com.bluepay.sdk.exception.BlueException r0 = new com.bluepay.sdk.exception.BlueException
            int r1 = com.bluepay.data.h.i
            java.lang.String r2 = "please granted send sms permission then try again."
            java.lang.Object[] r3 = new java.lang.Object[r6]
            r0.<init>(r1, r2, r3)
            throw r0
        L_0x0107:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            r0.<init>()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r1 = "sigle sim send,dest:"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r0 = r0.append(r9)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r1 = " content:"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r0 = r0.append(r10)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r0 = r0.toString()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            com.bluepay.sdk.b.c.c(r0)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            android.telephony.SmsManager r0 = android.telephony.SmsManager.getDefault()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            r2 = 0
            r1 = r9
            r3 = r10
            r4 = r11
            r5 = r12
            r0.sendTextMessage(r1, r2, r3, r4, r5)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            goto L_0x0035
        L_0x0135:
            r0 = move-exception
            com.bluepay.sdk.exception.BlueException r1 = new com.bluepay.sdk.exception.BlueException
            int r2 = com.bluepay.data.h.i
            java.lang.String r0 = r0.getMessage()
            java.lang.Object[] r3 = new java.lang.Object[r6]
            r1.<init>(r2, r0, r3)
            throw r1
        L_0x0144:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            r0.<init>()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r1 = "single sim|imsi:"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r1 = com.bluepay.pay.Client.m_iIMSI     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            java.lang.String r0 = r0.toString()     // Catch:{ SecurityException -> 0x00fa, Exception -> 0x0135 }
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bluepay.sdk.c.v.a(android.content.Context, com.bluepay.data.b, java.lang.String, java.lang.String, android.app.PendingIntent, android.app.PendingIntent):boolean");
    }

    public static void a(Context context, String str, String str2, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2) {
        Method method;
        Class[] clsArr = {String.class, String.class, String.class, PendingIntent.class, PendingIntent.class};
        c.c("---sendMsgByDualSim---defaultPhoneSoltSim---" + aa.a + "---imsi---" + Client.m_iIMSI);
        if (Build.VERSION.SDK_INT >= 21) {
            Class<?> cls = Class.forName("android.telephony.SmsManager");
            if (Build.VERSION.SDK_INT == 21) {
                method = cls.getMethod("getSmsManagerForSubscriber", Long.TYPE);
            } else {
                method = cls.getMethod("getSmsManagerForSubscriptionId", Integer.TYPE);
            }
            Object[] objArr = {Integer.valueOf(aa.a(context, aa.a))};
            cls.getDeclaredMethod("sendTextMessage", clsArr).invoke((SmsManager) method.invoke(cls, objArr), str, null, str2, pendingIntent, pendingIntent2);
            return;
        }
        Class[] clsArr2 = {String.class, String.class, String.class, PendingIntent.class, PendingIntent.class, Integer.TYPE};
        switch (i) {
            case 0:
                Class<?> cls2 = Class.forName("com.mediatek.telephony.SmsManagerEx");
                Method method2 = cls2.getMethod("getDefault", new Class[0]);
                method2.setAccessible(true);
                Object invoke = method2.invoke(cls2, new Object[0]);
                cls2.getMethod("sendTextMessage", clsArr2).invoke(invoke, str, null, str2, pendingIntent, pendingIntent2, Integer.valueOf(aa.a));
                return;
            case 1:
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.getClass().getDeclaredMethod("sendTextMessage", clsArr2).invoke(smsManager, str, null, str2, pendingIntent, pendingIntent2, Integer.valueOf(aa.a));
                return;
            case 2:
                Class<?> cls3 = Class.forName("android.telephony.SmsManager");
                Method declaredMethod = cls3.getDeclaredMethod("getSmsManagerForSubscriber", Long.TYPE);
                declaredMethod.setAccessible(true);
                Object invoke2 = declaredMethod.invoke(cls3, Integer.valueOf(aa.a));
                invoke2.getClass().getDeclaredMethod("sendTextMessage", clsArr).invoke(invoke2, str, null, str2, pendingIntent, pendingIntent2);
                return;
            case 3:
                Class<?> cls4 = Class.forName("android.telephony.MSimSmsManager");
                Object invoke3 = cls4.getMethod("getDefault", new Class[0]).invoke(cls4, new Object[0]);
                cls4.getDeclaredMethod("sendTextMessage", clsArr2).invoke(invoke3, str, null, str2, pendingIntent, pendingIntent2, Integer.valueOf(aa.a));
                return;
            default:
                return;
        }
    }

    private static void a(b bVar, Context context) {
        if (c == null) {
            c = new w();
        }
        c.a(bVar);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(a);
        intentFilter.addAction(b);
        context.registerReceiver(c, intentFilter);
    }
}
