package com.bluepay.sdk.c;

import android.content.Context;
import com.bluepay.data.Config;
import com.bluepay.data.m;
import com.bluepay.pay.Client;
import com.bluepay.sdk.a.a;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.exception.BlueException;
import java.util.HashMap;

/* compiled from: Proguard */
final class ai implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ String b;
    final /* synthetic */ int c;
    final /* synthetic */ String d;
    final /* synthetic */ Context e;

    ai(String str, String str2, int i, String str3, Context context) {
        this.a = str;
        this.b = str2;
        this.c = i;
        this.d = str3;
        this.e = context;
    }

    public void run() {
        try {
            String y = m.y();
            HashMap hashMap = new HashMap();
            hashMap.put("fwflag", "yes");
            hashMap.put("transactionid", this.a);
            hashMap.put("desc", this.b);
            hashMap.put("actiontype", Integer.valueOf(this.c));
            hashMap.put("productid", Integer.valueOf(Client.getProductId()));
            hashMap.put("promotinId", Client.getPromotionId());
            hashMap.put("imsi", this.d);
            hashMap.put("imei", aa.d(this.e));
            hashMap.put("timestamp", aa.e());
            hashMap.put("v", Integer.valueOf((int) Config.VERSION));
            hashMap.put("rv", Integer.valueOf(aa.c()));
            String str = d.a(hashMap).toString();
            hashMap.put("encrypt", e.a(str + Client.getEncrypt()));
            if (a.a(this.e, y, str, hashMap).a() == 200) {
                c.c("BluePay uploadErrorRecord() success");
            }
            c.c("||---- BluePay uploadErrorRecord() ---- end ----||");
        } catch (BlueException e2) {
            e2.printStackTrace();
        }
    }
}
