package com.bluepay.sdk.c;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/* compiled from: Proguard */
public class f {
    public static boolean a(Context context) {
        NetworkInfo d = d(context);
        return d != null && d.isConnected();
    }

    public static boolean b(Context context) {
        NetworkInfo[] allNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (!(connectivityManager == null || (allNetworkInfo = connectivityManager.getAllNetworkInfo()) == null)) {
            for (NetworkInfo networkInfo : allNetworkInfo) {
                if (networkInfo.getTypeName().equals("WIFI") && networkInfo.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean c(Context context) {
        NetworkInfo d = d(context);
        return d != null && d.getType() == 0 && d.isConnected();
    }

    public static NetworkInfo d(Context context) {
        return ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
    }

    public static boolean e(Context context) {
        return c(context) || b(context);
    }
}
