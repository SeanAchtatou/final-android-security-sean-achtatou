package com.bluepay.sdk.c;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.bluepay.a.b.a;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.data.i;
import com.bluepay.pay.Client;

/* compiled from: Proguard */
class w extends BroadcastReceiver {
    b a;

    public void a(b bVar) {
        this.a = bVar;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("SENT_SMS_ACTION")) {
            Log.i(Client.TAG, "----Receive send broadcast---");
            switch (getResultCode()) {
                case -1:
                    this.a.desc = "sms send success";
                    a.o.a(14, 200, 0, this.a);
                    break;
                default:
                    if (a.b()) {
                        this.a.desc = i.a(i.o);
                        a.o.a(14, h.k, 0, this.a);
                        break;
                    }
                    break;
            }
            context.unregisterReceiver(v.c);
        }
    }
}
