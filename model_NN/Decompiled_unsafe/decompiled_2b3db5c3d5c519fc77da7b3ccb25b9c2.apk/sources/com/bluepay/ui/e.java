package com.bluepay.ui;

import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.c.aa;

/* compiled from: Proguard */
class e extends WebViewClient {
    final /* synthetic */ BankActivity a;

    e(BankActivity bankActivity) {
        this.a = bankActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.ui.BankActivity.a(com.bluepay.ui.BankActivity, boolean):boolean
     arg types: [com.bluepay.ui.BankActivity, int]
     candidates:
      com.bluepay.ui.BankActivity.a(com.bluepay.ui.BankActivity, int):int
      com.bluepay.ui.BankActivity.a(com.bluepay.ui.BankActivity, boolean):boolean */
    public void onPageFinished(WebView view, String url) {
        c.c("page load finished:" + url);
        aa.d();
        boolean unused = this.a.j = true;
        super.onPageFinished(view, url);
    }

    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.getSettings().setCacheMode(-1);
        WebView.HitTestResult hitTestResult = view.getHitTestResult();
        if (TextUtils.isEmpty(url) || hitTestResult != null) {
            return super.shouldOverrideUrlLoading(view, url);
        }
        this.a.a.addJavascriptInterface(new f(this.a), "entry");
        view.loadUrl(url);
        return true;
    }
}
