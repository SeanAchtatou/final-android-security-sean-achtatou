package com.bluepay.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.bluepay.a.b.a;
import com.bluepay.data.Config;
import com.bluepay.data.i;
import com.bluepay.data.j;
import com.bluepay.pay.BluePay;
import com.bluepay.pay.Client;
import com.bluepay.pay.IPayCallback;
import com.bluepay.pay.PublisherCode;
import com.bluepay.sdk.c.aa;
import java.util.ArrayList;
import java.util.List;

/* compiled from: Proguard */
public class PayUIActivity extends Activity {
    private LinearLayout A;
    /* access modifiers changed from: private */
    public TextView B;
    /* access modifiers changed from: private */
    public af C;
    /* access modifiers changed from: private */
    public al D;
    /* access modifiers changed from: private */
    public h E;
    /* access modifiers changed from: private */
    public String F = PublisherCode.PUBLISHER_SMS;
    /* access modifiers changed from: private */
    public List G;
    /* access modifiers changed from: private */
    public j H;
    /* access modifiers changed from: private */
    public BluePay I;
    /* access modifiers changed from: private */
    public String J;
    /* access modifiers changed from: private */
    public IPayCallback K;
    @SuppressLint({"DefaultLocale"})
    private View.OnClickListener L = new m(this);
    private List M = new s(this);
    private List N = new t(this);
    private List O = new u(this);
    private List P = new v(this);
    ak a = new ak(this);
    private ListView b;
    private ScrollView c;
    private WrapListView d;
    private TextView e;
    private TextView f;
    private TextView g;
    private LinearLayout h;
    private LinearLayout i;
    private LinearLayout j;
    private LinearLayout k;
    private LinearLayout l;
    /* access modifiers changed from: private */
    public LinearLayout m;
    /* access modifiers changed from: private */
    public LinearLayout n;
    private TextView o;
    private TextView p;
    private TextView q;
    private TextView r;
    private TextView s;
    private TextView t;
    /* access modifiers changed from: private */
    public EditText u;
    /* access modifiers changed from: private */
    public EditText v;
    /* access modifiers changed from: private */
    public EditText w;
    private Button x;
    private ImageView y;
    /* access modifiers changed from: private */
    public RelativeLayout z;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [com.bluepay.ui.PayUIActivity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
     arg types: [com.bluepay.ui.PayUIActivity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.H = (j) getIntent().getExtras().getSerializable("entry");
        this.I = BluePay.getInstance();
        this.K = a.a();
        if (this.H == null) {
            finish();
            return;
        }
        if (BluePay.getLandscape()) {
            setRequestedOrientation(0);
            setContentView(aa.a((Context) this, "layout", "bluep_activity_pay_ui_landscape"));
        } else {
            setRequestedOrientation(1);
            setContentView(aa.a((Context) this, "layout", "bluep_activity_pay_ui_portrait"));
        }
        a();
        if (BluePay.getLandscape()) {
            b();
        }
        aa.a((Activity) this, (CharSequence) "", (CharSequence) "");
        a(new i(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [com.bluepay.ui.PayUIActivity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    @SuppressLint({"ClickableViewAccessibility"})
    private void a() {
        String str;
        this.A = (LinearLayout) findViewById(aa.a((Context) this, "id", "ll_cancel"));
        this.B = (TextView) findViewById(aa.a((Context) this, "id", "tv_cancel"));
        this.h = (LinearLayout) findViewById(aa.a((Context) this, "id", "ll_price"));
        this.e = (TextView) findViewById(aa.a((Context) this, "id", "tv_propsname"));
        this.f = (TextView) findViewById(aa.a((Context) this, "id", "tv_price"));
        this.i = (LinearLayout) findViewById(aa.a((Context) this, "id", "view1"));
        this.j = (LinearLayout) findViewById(aa.a((Context) this, "id", "view2"));
        this.k = (LinearLayout) findViewById(aa.a((Context) this, "id", "view3"));
        this.m = (LinearLayout) findViewById(aa.a((Context) this, "id", "ll_card_no1"));
        this.n = (LinearLayout) findViewById(aa.a((Context) this, "id", "ll_card_no"));
        this.o = (TextView) findViewById(aa.a((Context) this, "id", "tv_tips"));
        this.q = (TextView) findViewById(aa.a((Context) this, "id", "label_card_no1"));
        this.r = (TextView) findViewById(aa.a((Context) this, "id", "label_card_no"));
        this.s = (TextView) findViewById(aa.a((Context) this, "id", "label_serial_no"));
        this.u = (EditText) findViewById(aa.a((Context) this, "id", "et_card_no1"));
        this.v = (EditText) findViewById(aa.a((Context) this, "id", "et_card_no"));
        this.w = (EditText) findViewById(aa.a((Context) this, "id", "et_serial_no"));
        this.p = (TextView) findViewById(aa.a((Context) this, "id", "tv_card_tips"));
        this.x = (Button) findViewById(aa.a((Context) this, "id", "btn_submit"));
        this.l = (LinearLayout) findViewById(aa.a((Context) this, "id", "view_exchange"));
        this.t = (TextView) findViewById(aa.a((Context) this, "id", "tv_label_exchange"));
        this.x.setText(i.a(i.aA));
        this.x.setOnClickListener(this.L);
        this.e.setText(this.H.g());
        TextView textView = this.f;
        if (this.H.h().equalsIgnoreCase(Config.K_CURRENCY_THB)) {
            str = (Integer.parseInt(this.H.e()) / 100) + this.H.h();
        } else {
            str = this.H.e() + this.H.h();
        }
        textView.setText(str);
        this.q.setText(i.a(i.aB));
        this.r.setText(i.a(i.aB));
        this.s.setText(i.a(i.aC));
        this.u.setHint(i.a(i.aD));
        this.v.setHint(i.a(i.aD));
        this.w.setHint(i.a(i.aE));
        this.t.setText(i.a(i.aQ));
        this.p.setText(i.a(i.aP));
        this.A.setOnClickListener(new w(this));
        this.A.setOnTouchListener(new x(this));
        this.p.setOnClickListener(new y(this));
        this.u.setOnTouchListener(new z(this));
        this.v.setOnTouchListener(new aa(this));
        this.w.setOnTouchListener(new ab(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [com.bluepay.ui.PayUIActivity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    private void b() {
        this.b = (ListView) findViewById(aa.a((Context) this, "id", "listview"));
        this.c = (ScrollView) findViewById(aa.a((Context) this, "id", "scrollview"));
        this.d = (WrapListView) findViewById(aa.a((Context) this, "id", "list_exchange"));
        this.C = new af(this, f());
        this.b.setAdapter((ListAdapter) this.C);
        this.C.a(0);
        this.C.notifyDataSetInvalidated();
        this.d.setClickable(false);
        this.d.setAdapter((ListAdapter) new ai(this, Client.m_exchangeList, aa.a(Client.CONTRY_CODE), 1));
        this.F = (String) this.G.get(0);
        a(this.F);
        this.b.setOnItemClickListener(new ac(this));
    }

    /* access modifiers changed from: private */
    public void c() {
        this.C = new af(this, this.G);
        this.b.setAdapter((ListAdapter) this.C);
        this.C.a(0);
        this.C.notifyDataSetInvalidated();
        this.F = (String) this.G.get(0);
        a(this.F);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [com.bluepay.ui.PayUIActivity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    /* access modifiers changed from: private */
    public void d() {
        this.g = (TextView) findViewById(aa.a((Context) this, "id", "tv_label_payment"));
        this.y = (ImageView) findViewById(aa.a((Context) this, "id", "iv_channel_icon"));
        this.z = (RelativeLayout) findViewById(aa.a((Context) this, "id", "rl_select_channel"));
        this.g.setText(i.a(i.aF));
        this.F = (String) this.G.get(0);
        a(this.F);
        this.z.setOnClickListener(new ad(this));
        this.l.setOnClickListener(new k(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [com.bluepay.ui.PayUIActivity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    /* access modifiers changed from: private */
    public void e() {
        AlertDialog create = new AlertDialog.Builder(this).create();
        create.show();
        View inflate = LayoutInflater.from(this).inflate(aa.a((Context) this, "layout", "bluep_pay_list"), (ViewGroup) null);
        ImageView imageView = (ImageView) inflate.findViewById(aa.a((Context) this, "id", "btn_close"));
        ListView listView = (ListView) inflate.findViewById(aa.a((Context) this, "id", "listview"));
        ((TextView) inflate.findViewById(aa.a((Context) this, "id", "tv_title"))).setText(i.a(i.aQ));
        ai aiVar = new ai(this, Client.m_exchangeList, aa.a(Client.CONTRY_CODE), 2);
        int b2 = (aa.b((Activity) this) * 2) / 3;
        View view = aiVar.getView(0, null, listView);
        view.measure(0, 0);
        if (view.getMeasuredHeight() * Client.m_exchangeList.size() > b2) {
            ViewGroup.LayoutParams layoutParams = listView.getLayoutParams();
            layoutParams.width = -1;
            layoutParams.height = b2;
            listView.setLayoutParams(layoutParams);
        } else {
            ViewGroup.LayoutParams layoutParams2 = listView.getLayoutParams();
            layoutParams2.width = -1;
            layoutParams2.height = -2;
            listView.setLayoutParams(layoutParams2);
        }
        listView.setClickable(false);
        listView.setAdapter((ListAdapter) aiVar);
        create.setContentView(inflate);
        imageView.setOnClickListener(new l(this, create));
        create.setCanceledOnTouchOutside(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [com.bluepay.ui.PayUIActivity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    /* access modifiers changed from: private */
    @SuppressLint({"DefaultLocale"})
    public void a(String str) {
        this.x.setBackgroundResource(aa.a((Context) this, "drawable", "bluep_ui_confirm_bg_selector"));
        this.x.setEnabled(true);
        this.l.setVisibility(8);
        this.h.setVisibility(0);
        this.p.setVisibility(8);
        if (this.y != null) {
            this.y.setImageResource(aa.a((Context) this, "drawable", "bluep_icon_" + str.toLowerCase()));
        }
        if (this.c != null) {
            this.c.fullScroll(33);
        }
        if (this.g != null) {
            this.g.setText(aa.i(str));
        }
        this.n.setBackgroundResource(aa.a((Context) this, "drawable", "bluep_ui_input_bg"));
        this.m.setBackgroundResource(aa.a((Context) this, "drawable", "bluep_ui_input_bg"));
        this.u.getEditableText().clear();
        this.v.getEditableText().clear();
        this.w.getEditableText().clear();
        if (PublisherCode.PUBLISHER_SMS.equalsIgnoreCase(str)) {
            this.i.setVisibility(0);
            this.j.setVisibility(8);
            this.k.setVisibility(8);
            this.o.setText(i.a(i.aI));
        } else if (PublisherCode.PUBLISHER_LINE.equalsIgnoreCase(str)) {
            this.i.setVisibility(0);
            this.j.setVisibility(8);
            this.k.setVisibility(8);
            this.o.setText(i.a(i.aJ));
        } else if (PublisherCode.PUBLISHER_VN_BANK.equalsIgnoreCase(str)) {
            this.i.setVisibility(0);
            this.j.setVisibility(8);
            this.k.setVisibility(8);
            this.o.setText(i.a(i.aK));
        } else if (PublisherCode.PUBLISHER_ID_BANK.equalsIgnoreCase(str)) {
            this.i.setVisibility(0);
            this.j.setVisibility(8);
            this.k.setVisibility(8);
            this.o.setText(i.a(i.aN));
        } else if (PublisherCode.PUBLISHER_OFFLINE_ATM.equalsIgnoreCase(str)) {
            this.i.setVisibility(0);
            this.j.setVisibility(8);
            this.k.setVisibility(8);
            this.o.setText(i.a(i.aL));
        } else if (PublisherCode.PUBLISHER_OFFLINE_OTC.equalsIgnoreCase(str)) {
            this.i.setVisibility(0);
            this.j.setVisibility(8);
            this.k.setVisibility(8);
            this.o.setText(i.a(i.aM));
        } else if (PublisherCode.PUBLISHER_BLUECOIN.equalsIgnoreCase(str) || PublisherCode.PUBLISHER_12CALL.equalsIgnoreCase(str) || PublisherCode.PUBLISHER_TRUEMONEY.equalsIgnoreCase(str) || PublisherCode.PUBLISHER_MOGPLAY.equalsIgnoreCase(str) || PublisherCode.PUBLISHER_LYTOCARD.equalsIgnoreCase(str)) {
            this.h.setVisibility(8);
            this.i.setVisibility(8);
            this.j.setVisibility(0);
            this.k.setVisibility(8);
            if (Client.m_exchangeList != null && !Client.m_exchangeList.isEmpty()) {
                this.l.setVisibility(0);
            }
            if (PublisherCode.PUBLISHER_BLUECOIN.equalsIgnoreCase(str)) {
                this.p.setVisibility(0);
            }
        } else if (PublisherCode.PUBLISHER_MOBIFONE.equalsIgnoreCase(str) || PublisherCode.PUBLISHER_VINAPHONE.equalsIgnoreCase(str) || PublisherCode.PUBLISHER_VIETTEL.equalsIgnoreCase(str) || PublisherCode.PUBLISHER_VTC.equalsIgnoreCase(str) || PublisherCode.PUBLISHER_HAPPY.equalsIgnoreCase(str)) {
            this.h.setVisibility(8);
            this.i.setVisibility(8);
            this.j.setVisibility(8);
            this.k.setVisibility(0);
            if (Client.m_exchangeList != null && !Client.m_exchangeList.isEmpty()) {
                this.l.setVisibility(0);
            }
        } else {
            this.i.setVisibility(0);
            this.j.setVisibility(8);
            this.k.setVisibility(8);
            this.o.setText("Not support");
            this.x.setBackgroundResource(aa.a((Context) this, "drawable", "bluep_ui_confirm_bg_enable"));
            this.x.setEnabled(false);
        }
    }

    private void a(ah ahVar) {
        if (this.G == null) {
            this.G = new ArrayList();
        }
        if (Config.NETWORKTYPE_INVALID.equals(aa.a((Context) this))) {
            f();
            ahVar.a();
            return;
        }
        new Thread(new r(this, ahVar)).start();
    }

    /* access modifiers changed from: private */
    public List f() {
        if (this.G == null) {
            this.G = new ArrayList();
        }
        this.G.clear();
        switch (Client.CONTRY_CODE) {
            case 62:
                this.G.addAll(this.O);
                break;
            case 66:
                this.G.addAll(this.M);
                break;
            case 84:
                this.G.addAll(this.N);
                break;
            case 86:
                this.G.addAll(this.P);
                break;
            default:
                String l2 = aa.l(this);
                if (!l2.equals(Config.LAN_TH1) && !l2.equals("th-TH")) {
                    if (!l2.equals("vn")) {
                        if (!l2.equals("id")) {
                            if (!l2.equals("zh") && !l2.equals("zh-HK")) {
                                this.G.addAll(this.P);
                                break;
                            } else {
                                this.G.addAll(this.P);
                                break;
                            }
                        } else {
                            this.G.addAll(this.O);
                            break;
                        }
                    } else {
                        this.G.addAll(this.N);
                        break;
                    }
                } else {
                    this.G.addAll(this.M);
                    break;
                }
                break;
        }
        return this.G;
    }
}
