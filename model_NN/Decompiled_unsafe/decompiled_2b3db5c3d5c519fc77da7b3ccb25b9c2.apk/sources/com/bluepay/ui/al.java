package com.bluepay.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.bluepay.data.i;
import com.bluepay.sdk.c.aa;
import java.util.List;

/* compiled from: Proguard */
public class al extends PopupWindow {
    private View a;
    private TextView b;
    private ListView c;
    private ImageView d;
    private Context e;
    /* access modifiers changed from: private */
    public ap f;
    /* access modifiers changed from: private */
    public ar g;
    private String h;
    private List i;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    public al(Activity activity, String str, List list) {
        this.e = activity;
        this.h = str;
        this.i = list;
        this.a = ((LayoutInflater) activity.getSystemService("layout_inflater")).inflate(aa.a((Context) activity, "layout", "bluep_pay_list"), (ViewGroup) null);
        setContentView(this.a);
        setWidth(-2);
        setHeight(-2);
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        a(activity, 0.5f);
        setOnDismissListener(new am(this, activity));
        a(activity);
    }

    public void a(Activity activity, float f2) {
        WindowManager.LayoutParams attributes = activity.getWindow().getAttributes();
        attributes.alpha = f2;
        activity.getWindow().addFlags(2);
        activity.getWindow().setAttributes(attributes);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    private void a(Activity activity) {
        this.b = (TextView) this.a.findViewById(aa.a((Context) activity, "id", "tv_title"));
        this.d = (ImageView) this.a.findViewById(aa.a((Context) activity, "id", "btn_close"));
        this.c = (ListView) this.a.findViewById(aa.a((Context) activity, "id", "listview"));
        int b2 = (aa.b(activity) * 2) / 3;
        this.b.setText(i.a(i.aF));
        this.f = new ap(this, this.i, activity);
        this.c.setAdapter((ListAdapter) this.f);
        View view = this.f.getView(0, null, this.c);
        view.measure(0, 0);
        if (view.getMeasuredHeight() * this.i.size() > b2) {
            ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
            layoutParams.width = -1;
            layoutParams.height = b2;
            this.c.setLayoutParams(layoutParams);
        } else {
            ViewGroup.LayoutParams layoutParams2 = this.c.getLayoutParams();
            layoutParams2.width = -1;
            layoutParams2.height = -2;
            this.c.setLayoutParams(layoutParams2);
        }
        if (this.i.contains(this.h)) {
            this.f.a(this.i.indexOf(this.h));
            this.f.notifyDataSetInvalidated();
        } else {
            this.f.a(0);
            this.f.notifyDataSetInvalidated();
        }
        this.c.setOnItemClickListener(new an(this));
        this.d.setOnClickListener(new ao(this));
    }

    public void a(View view) {
        if (!isShowing()) {
            showAtLocation(view, 17, 0, 0);
        } else {
            dismiss();
        }
    }

    public void a(ar arVar) {
        this.g = arVar;
    }
}
