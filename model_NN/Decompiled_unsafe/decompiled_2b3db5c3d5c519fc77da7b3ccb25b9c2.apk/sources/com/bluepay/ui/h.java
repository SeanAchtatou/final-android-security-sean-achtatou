package com.bluepay.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.bluepay.sdk.c.aa;

/* compiled from: Proguard */
public class h extends PopupWindow {
    private View a;
    private TextView b;
    private Context c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, float):int
     arg types: [android.app.Activity, int]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, int):int
      com.bluepay.sdk.c.aa.a(java.lang.Object, int):int
      com.bluepay.sdk.c.aa.a(java.lang.String, int):int
      com.bluepay.sdk.c.aa.a(java.lang.String, java.lang.String):java.lang.String
      com.bluepay.sdk.c.aa.a(android.content.Context, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String):void
      com.bluepay.sdk.c.aa.a(java.lang.StringBuffer, byte):void
      com.bluepay.sdk.c.aa.a(java.util.List, java.lang.String):boolean
      com.bluepay.sdk.c.aa.a(byte[], byte[]):byte[]
      com.bluepay.sdk.c.aa.a(android.content.Context, float):int */
    public h(Activity activity) {
        this.c = activity;
        this.a = ((LayoutInflater) activity.getSystemService("layout_inflater")).inflate(aa.a((Context) activity, "layout", "bluep_error_tips"), (ViewGroup) null);
        setContentView(this.a);
        setWidth(aa.a((Context) activity, 200.0f));
        setHeight(-2);
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        a();
    }

    private void a() {
        this.b = (TextView) this.a.findViewById(aa.a(this.c, "id", "tv_error_msg"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, float):int
     arg types: [android.content.Context, int]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, int):int
      com.bluepay.sdk.c.aa.a(java.lang.Object, int):int
      com.bluepay.sdk.c.aa.a(java.lang.String, int):int
      com.bluepay.sdk.c.aa.a(java.lang.String, java.lang.String):java.lang.String
      com.bluepay.sdk.c.aa.a(android.content.Context, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String):void
      com.bluepay.sdk.c.aa.a(java.lang.StringBuffer, byte):void
      com.bluepay.sdk.c.aa.a(java.util.List, java.lang.String):boolean
      com.bluepay.sdk.c.aa.a(byte[], byte[]):byte[]
      com.bluepay.sdk.c.aa.a(android.content.Context, float):int */
    public void a(View view, String str) {
        if (!isShowing()) {
            this.b.setText(str);
            this.b.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            showAsDropDown(view, view.getMeasuredWidth() - getWidth(), (-view.getMeasuredHeight()) - (this.b.getMeasuredHeight() + aa.a(this.c, 35.0f)));
            return;
        }
        dismiss();
    }
}
