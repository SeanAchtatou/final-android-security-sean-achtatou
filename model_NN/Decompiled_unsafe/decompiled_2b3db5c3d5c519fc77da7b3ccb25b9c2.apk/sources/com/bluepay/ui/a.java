package com.bluepay.ui;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: Proguard */
class a implements View.OnTouchListener {
    final /* synthetic */ BankActivity a;

    a(BankActivity bankActivity) {
        this.a = bankActivity;
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0) {
            this.a.k.setTextColor(-7829368);
        }
        if (event.getAction() != 1) {
            return false;
        }
        this.a.k.setTextColor(-1);
        return false;
    }
}
