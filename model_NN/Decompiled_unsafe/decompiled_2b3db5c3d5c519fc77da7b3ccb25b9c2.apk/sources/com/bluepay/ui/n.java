package com.bluepay.ui;

import android.text.TextUtils;
import com.bluepay.pay.PublisherCode;

/* compiled from: Proguard */
class n implements Runnable {
    final /* synthetic */ m a;

    n(m mVar) {
        this.a = mVar;
    }

    public void run() {
        String unused = this.a.a.F = this.a.a.F.toLowerCase();
        if (this.a.a.K != null) {
            String unused2 = this.a.a.J = this.a.a.K.onPrepared();
        }
        if (PublisherCode.PUBLISHER_SMS.equalsIgnoreCase(this.a.a.F)) {
            this.a.a.I.payBySMS(this.a.a, this.a.a.J, this.a.a.H.h(), this.a.a.H.e(), this.a.a.H.f(), this.a.a.H.g(), true, this.a.a.a);
        } else if (PublisherCode.PUBLISHER_LINE.equalsIgnoreCase(this.a.a.F)) {
            this.a.a.I.payByWallet(this.a.a, this.a.a.H.d, this.a.a.J, this.a.a.H.h, this.a.a.H.e, this.a.a.H.g, this.a.a.F, this.a.a.H.j, true, this.a.a.a);
        } else if (PublisherCode.PUBLISHER_VN_BANK.equalsIgnoreCase(this.a.a.F)) {
            this.a.a.I.payByWallet(this.a.a, this.a.a.H.d, this.a.a.J, this.a.a.H.h, this.a.a.H.e, this.a.a.H.g, this.a.a.F, "bluepay://best.bluepay.asia", true, this.a.a.a);
        } else if (PublisherCode.PUBLISHER_ID_BANK.equalsIgnoreCase(this.a.a.F)) {
            this.a.a.I.payByWallet(this.a.a, this.a.a.H.d, this.a.a.J, this.a.a.H.h, this.a.a.H.e, this.a.a.H.g, this.a.a.F, "bluepay://best.bluepay.asia", true, this.a.a.a);
        } else if (PublisherCode.PUBLISHER_OFFLINE_ATM.equalsIgnoreCase(this.a.a.F)) {
            this.a.a.I.payByOffline(this.a.a, this.a.a.J, this.a.a.H.d, this.a.a.H.h, this.a.a.H.e, this.a.a.H.g, this.a.a.F, "123456", true, this.a.a.a);
        } else if (PublisherCode.PUBLISHER_OFFLINE_OTC.equalsIgnoreCase(this.a.a.F)) {
            this.a.a.I.payByOffline(this.a.a, this.a.a.J, this.a.a.H.d, this.a.a.H.h, this.a.a.H.e, this.a.a.H.g, this.a.a.F, "123456", true, this.a.a.a);
        } else if (PublisherCode.PUBLISHER_BLUECOIN.equalsIgnoreCase(this.a.a.F) || PublisherCode.PUBLISHER_12CALL.equalsIgnoreCase(this.a.a.F) || PublisherCode.PUBLISHER_TRUEMONEY.equalsIgnoreCase(this.a.a.F) || PublisherCode.PUBLISHER_MOGPLAY.equalsIgnoreCase(this.a.a.F) || PublisherCode.PUBLISHER_LYTOCARD.equalsIgnoreCase(this.a.a.F)) {
            String obj = this.a.a.u.getText().toString();
            if (TextUtils.isEmpty(obj)) {
                this.a.a.runOnUiThread(new o(this));
            } else {
                this.a.a.I.payByCashcard(this.a.a, this.a.a.H.d(), this.a.a.J, this.a.a.H.g(), this.a.a.F, obj, "", this.a.a.a);
            }
        } else if (PublisherCode.PUBLISHER_MOBIFONE.equalsIgnoreCase(this.a.a.F) || PublisherCode.PUBLISHER_VINAPHONE.equalsIgnoreCase(this.a.a.F) || PublisherCode.PUBLISHER_VIETTEL.equalsIgnoreCase(this.a.a.F) || PublisherCode.PUBLISHER_VTC.equalsIgnoreCase(this.a.a.F) || PublisherCode.PUBLISHER_HAPPY.equalsIgnoreCase(this.a.a.F)) {
            String obj2 = this.a.a.v.getText().toString();
            if (TextUtils.isEmpty(obj2)) {
                this.a.a.runOnUiThread(new p(this));
                return;
            }
            String obj3 = this.a.a.w.getText().toString();
            if (TextUtils.isEmpty(obj3)) {
                this.a.a.runOnUiThread(new q(this));
            } else {
                this.a.a.I.payByCashcard(this.a.a, this.a.a.H.d(), this.a.a.J, this.a.a.H.g(), this.a.a.F, obj2, obj3, this.a.a.a);
            }
        }
    }
}
