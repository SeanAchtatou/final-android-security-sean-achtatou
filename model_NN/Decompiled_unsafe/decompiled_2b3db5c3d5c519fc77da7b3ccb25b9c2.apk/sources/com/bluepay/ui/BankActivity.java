package com.bluepay.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.bluepay.a.b.a;
import com.bluepay.data.b;
import com.bluepay.data.j;
import com.bluepay.pay.BluePay;
import com.bluepay.pay.Client;
import com.bluepay.pay.PublisherCode;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.c.aa;

/* compiled from: Proguard */
public class BankActivity extends Activity {
    static final int c = -1000;
    WebView a;
    TextView b;
    Handler d = new Handler();
    Runnable e = new b(this);
    /* access modifiers changed from: private */
    public j f;
    private long g;
    /* access modifiers changed from: private */
    public int h = 1000;
    /* access modifiers changed from: private */
    public int i = 30000;
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public Button k;
    /* access modifiers changed from: private */
    public Button l;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [com.bluepay.ui.BankActivity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        if (getRequestedOrientation() != 1) {
            setRequestedOrientation(1);
        }
        getWindow().setFlags(1024, 1024);
        this.f = (j) getIntent().getExtras().getSerializable("entry");
        if (this.f == null) {
            finish();
            return;
        }
        getIntent().getExtras().clear();
        setContentView(aa.a((Context) this, "layout", "bluep_activity_pay_bank"));
        a();
        BluePay.setShowCardLoading(true);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public void backto(View v) {
        if (((Button) v).getText().toString().equals("Cancel")) {
            b bVar = new b(this, this.f.h(), 0, this.f.c());
            bVar.desc = "User cancel";
            bVar.setCPPayType(this.f.l());
            try {
                bVar.setPrice(Integer.parseInt(this.f.e()));
            } catch (NumberFormatException e2) {
                e2.printStackTrace();
            }
            a.o.a(14, Client.C_USER_CANCEL, 0, bVar);
        }
        finish();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
     arg types: [com.bluepay.ui.BankActivity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void */
    public void refresh(View v) {
        this.a.reload();
        aa.a((Activity) this, (CharSequence) "", (CharSequence) "");
        this.d.postDelayed(this.e, (long) this.h);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        c.c("onConfigurationChanged()");
        super.onConfigurationChanged(newConfig);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [com.bluepay.ui.BankActivity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
     arg types: [com.bluepay.ui.BankActivity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void */
    @SuppressLint({"JavascriptInterface", "SetJavaScriptEnabled", "ClickableViewAccessibility"})
    private void a() {
        this.a = (WebView) findViewById(aa.a((Context) this, "id", "wb_content"));
        this.b = (TextView) findViewById(aa.a((Context) this, "id", "tv_error"));
        this.a.setBackgroundColor(-1);
        this.k = (Button) findViewById(aa.a((Context) this, "id", "btn_back"));
        this.l = (Button) findViewById(aa.a((Context) this, "id", "btn_complete"));
        this.a.getSettings().setJavaScriptEnabled(true);
        this.a.addJavascriptInterface(new f(this), "entry");
        String b2 = b();
        String str = "";
        if (this.f.l().equalsIgnoreCase(PublisherCode.PUBLISHER_VN_BANK)) {
            str = Client.m_BankChargeInfo.g + "?" + b2;
        } else if (this.f.l().equalsIgnoreCase(PublisherCode.PUBLISHER_ID_BANK)) {
            str = Client.m_BankChargeInfo.h + "?" + b2;
        }
        aa.a((Activity) this, (CharSequence) "", (CharSequence) "");
        this.d.postDelayed(this.e, (long) this.h);
        this.a.loadUrl(str);
        this.a.setWebChromeClient(new d(this));
        this.a.setWebViewClient(new e(this));
        this.k.setOnTouchListener(new a(this));
    }

    private String b() {
        StringBuilder sb = new StringBuilder();
        sb.append("&productId=" + Client.getProductId());
        sb.append("&price=").append(this.f.e());
        sb.append("&promotionId=").append(Client.getPromotionId());
        sb.append("&transactionId=" + this.f.c());
        sb.append("&propsName=" + this.f.g());
        sb.append("&msisdn=" + this.f.k());
        sb.append("&customerId=" + this.f.d());
        return sb.toString();
    }

    public void finish() {
        aa.d();
        super.finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.a != null) {
            this.a.clearCache(true);
        }
        BluePay.setShowCardLoading(false);
        aa.d();
    }

    public void onBackPressed() {
        if (System.currentTimeMillis() - this.g <= 2000) {
            if (this.k.getText().toString().equals("Cancel")) {
                b bVar = new b(this, this.f.h(), 0, this.f.c());
                bVar.desc = "User cancel";
                bVar.setCPPayType(this.f.l());
                try {
                    bVar.setPrice(Integer.parseInt(this.f.e()));
                } catch (NumberFormatException e2) {
                    e2.printStackTrace();
                }
                a.o.a(14, Client.C_USER_CANCEL, 0, bVar);
            }
            finish();
            super.onBackPressed();
        } else {
            if (this.a.canGoBack()) {
                this.a.getSettings().setCacheMode(1);
                this.a.goBack();
            }
            Toast.makeText(this, "back press again quit this page in 2 seconds. ", 0).show();
        }
        this.g = System.currentTimeMillis();
    }
}
