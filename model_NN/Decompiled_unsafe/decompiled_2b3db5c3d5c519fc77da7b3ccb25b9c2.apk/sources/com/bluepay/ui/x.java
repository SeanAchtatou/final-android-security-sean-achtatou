package com.bluepay.ui;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: Proguard */
class x implements View.OnTouchListener {
    final /* synthetic */ PayUIActivity a;

    x(PayUIActivity payUIActivity) {
        this.a = payUIActivity;
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == 0) {
            this.a.B.setTextColor(-7829368);
        }
        if (event.getAction() != 1) {
            return false;
        }
        this.a.B.setTextColor(-1);
        return false;
    }
}
