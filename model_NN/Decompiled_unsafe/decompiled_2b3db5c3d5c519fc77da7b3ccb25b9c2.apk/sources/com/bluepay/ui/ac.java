package com.bluepay.ui;

import android.view.View;
import android.widget.AdapterView;

/* compiled from: Proguard */
class ac implements AdapterView.OnItemClickListener {
    final /* synthetic */ PayUIActivity a;

    ac(PayUIActivity payUIActivity) {
        this.a = payUIActivity;
    }

    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
        this.a.C.a(position);
        this.a.C.notifyDataSetInvalidated();
        String str = (String) this.a.C.getItem(position);
        String unused = this.a.F = str;
        this.a.a(str);
    }
}
