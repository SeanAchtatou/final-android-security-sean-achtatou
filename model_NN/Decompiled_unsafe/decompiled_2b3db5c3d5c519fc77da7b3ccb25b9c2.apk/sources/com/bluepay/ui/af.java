package com.bluepay.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bluepay.pay.R;
import com.bluepay.sdk.c.aa;
import java.util.List;

@SuppressLint({"DefaultLocale"})
/* compiled from: Proguard */
class af extends BaseAdapter {
    final /* synthetic */ PayUIActivity a;
    private List b;
    private int c = -1;

    public af(PayUIActivity payUIActivity, List list) {
        this.a = payUIActivity;
        this.b = list;
    }

    public int getCount() {
        return this.b.size();
    }

    public Object getItem(int position) {
        return this.b.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [com.bluepay.ui.PayUIActivity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    public View getView(int position, View convertView, ViewGroup parent) {
        ag agVar;
        if (convertView == null) {
            agVar = new ag(this);
            convertView = LayoutInflater.from(this.a).inflate(aa.a((Context) this.a, "layout", "bluep_item_channel1"), (ViewGroup) null);
            agVar.a = (ImageView) convertView.findViewById(aa.a((Context) this.a, "id", "iv_channel_icon"));
            agVar.b = (TextView) convertView.findViewById(aa.a((Context) this.a, "id", "tv_channel_name"));
            agVar.c = convertView.findViewById(aa.a((Context) this.a, "id", "divider"));
            convertView.setTag(agVar);
        } else {
            agVar = (ag) convertView.getTag();
        }
        String str = (String) this.b.get(position);
        agVar.a.setImageResource(aa.a((Context) this.a, "drawable", "bluep_icon_" + str.toLowerCase()));
        agVar.b.setText(aa.i(str));
        if (position == this.c) {
            convertView.setBackgroundResource(R.drawable.bluep_channel1_pressed);
            agVar.c.setVisibility(8);
        } else {
            convertView.setBackgroundResource(R.drawable.bluep_channel1_normal);
            agVar.c.setVisibility(0);
            if (position == this.b.size() - 1) {
                agVar.c.setVisibility(8);
            } else {
                agVar.c.setVisibility(0);
            }
        }
        return convertView;
    }

    public void a(int i) {
        this.c = i;
    }
}
