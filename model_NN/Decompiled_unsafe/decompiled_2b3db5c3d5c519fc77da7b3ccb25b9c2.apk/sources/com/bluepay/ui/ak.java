package com.bluepay.ui;

import android.content.Context;
import com.bluepay.pay.BlueMessage;
import com.bluepay.pay.IPayCallback;
import com.bluepay.pay.PublisherCode;
import com.bluepay.sdk.c.aa;

/* compiled from: Proguard */
class ak extends IPayCallback {
    private static final long b = 1;
    final /* synthetic */ PayUIActivity a;

    ak(PayUIActivity payUIActivity) {
        this.a = payUIActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [com.bluepay.ui.PayUIActivity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    public void onFinished(BlueMessage msg) {
        if (msg.getCode() != 200) {
            if (msg.getCode() != 201) {
                if (msg.getCode() != 603) {
                    String publisher = msg.getPublisher();
                    if (msg.getCode() == 509 || msg.getCode() == 508) {
                        if (PublisherCode.PUBLISHER_BLUECOIN.equalsIgnoreCase(publisher) || PublisherCode.PUBLISHER_12CALL.equalsIgnoreCase(publisher) || PublisherCode.PUBLISHER_TRUEMONEY.equalsIgnoreCase(publisher) || PublisherCode.PUBLISHER_MOGPLAY.equalsIgnoreCase(publisher) || PublisherCode.PUBLISHER_LYTOCARD.equalsIgnoreCase(publisher)) {
                            h unused = this.a.E = new h(this.a);
                            this.a.E.a(this.a.m, msg.getDesc());
                            this.a.m.setBackgroundResource(aa.a((Context) this.a, "drawable", "bluep_ui_input_error_bg"));
                        } else if (PublisherCode.PUBLISHER_MOBIFONE.equalsIgnoreCase(publisher) || PublisherCode.PUBLISHER_VINAPHONE.equalsIgnoreCase(publisher) || PublisherCode.PUBLISHER_VIETTEL.equalsIgnoreCase(publisher) || PublisherCode.PUBLISHER_VTC.equalsIgnoreCase(publisher) || PublisherCode.PUBLISHER_HAPPY.equalsIgnoreCase(publisher)) {
                            h unused2 = this.a.E = new h(this.a);
                            this.a.E.a(this.a.n, msg.getDesc());
                            this.a.n.setBackgroundResource(aa.a((Context) this.a, "drawable", "bluep_ui_input_error_bg"));
                        }
                    }
                }
            }
        }
        this.a.K.onFinished(msg);
    }

    public String onPrepared() {
        return null;
    }
}
