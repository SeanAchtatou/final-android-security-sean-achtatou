package com.bluepay.ui;

import android.view.View;
import android.widget.AdapterView;

/* compiled from: Proguard */
class an implements AdapterView.OnItemClickListener {
    final /* synthetic */ al a;

    an(al alVar) {
        this.a = alVar;
    }

    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
        this.a.f.a(position);
        this.a.f.notifyDataSetInvalidated();
        if (this.a.g != null) {
            this.a.g.a((String) this.a.f.getItem(position));
        }
        this.a.dismiss();
    }
}
