package com.bluepay.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.bluepay.sdk.c.aa;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: Proguard */
class ai extends BaseAdapter {
    final /* synthetic */ PayUIActivity a;
    private List b;
    private String c;
    private int d;

    public ai(PayUIActivity payUIActivity, List list, String str, int i) {
        this.a = payUIActivity;
        this.b = list;
        this.c = str;
        this.d = i;
    }

    public int getCount() {
        return this.b.size();
    }

    public Object getItem(int position) {
        return this.b.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public boolean isEnabled(int position) {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [com.bluepay.ui.PayUIActivity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    public View getView(int position, View convertView, ViewGroup parent) {
        aj ajVar;
        if (convertView == null) {
            aj ajVar2 = new aj(this);
            convertView = LayoutInflater.from(this.a).inflate(aa.a((Context) this.a, "layout", "bluep_item_price"), (ViewGroup) null);
            ajVar2.a = (TextView) convertView.findViewById(aa.a((Context) this.a, "id", "tv_name"));
            ajVar2.b = convertView.findViewById(aa.a((Context) this.a, "id", "divider"));
            convertView.setTag(ajVar2);
            ajVar = ajVar2;
        } else {
            ajVar = (aj) convertView.getTag();
        }
        Iterator it = ((HashMap) this.b.get(position)).entrySet().iterator();
        if (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            ajVar.a.setText(((String) entry.getValue()) + " = " + ((String) entry.getKey()) + this.c);
        }
        if (this.d == 1) {
            ajVar.b.setVisibility(8);
        } else if (position == this.b.size() - 1) {
            ajVar.b.setVisibility(8);
        } else {
            ajVar.b.setVisibility(0);
        }
        return convertView;
    }
}
