package com.bluepay.ui;

import android.util.Log;
import com.bluepay.data.m;
import com.bluepay.pay.Client;
import com.bluepay.sdk.a.a;
import com.bluepay.sdk.c.aa;
import com.bluepay.sdk.exception.BlueException;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Proguard */
class r implements Runnable {
    final /* synthetic */ ah a;
    final /* synthetic */ PayUIActivity b;

    r(PayUIActivity payUIActivity, ah ahVar) {
        this.b = payUIActivity;
        this.a = ahVar;
    }

    public void run() {
        String str;
        String l = aa.l(this.b);
        try {
            str = a.a(this.b, m.a(l), "fwflag=channel&countryCode=" + Client.CONTRY_CODE + "&lan=" + l + "&productId=" + Client.getProductId(), (Map) null).b();
        } catch (BlueException e) {
            e.printStackTrace();
            str = null;
        }
        try {
            Log.i(Client.TAG, str);
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("channels")) {
                this.b.G.clear();
                JSONArray jSONArray = jSONObject.getJSONArray("channels");
                for (int i = 0; i < jSONArray.length(); i++) {
                    this.b.G.add(jSONArray.getString(i));
                }
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        if (this.b.G.isEmpty()) {
            List unused = this.b.f();
        }
        this.a.a();
    }
}
