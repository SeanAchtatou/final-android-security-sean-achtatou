package com.bluepay.ui;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import com.bluepay.sdk.c.aa;

/* compiled from: Proguard */
class z implements View.OnTouchListener {
    final /* synthetic */ PayUIActivity a;

    z(PayUIActivity payUIActivity) {
        this.a = payUIActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int
     arg types: [com.bluepay.ui.PayUIActivity, java.lang.String, java.lang.String]
     candidates:
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence):android.app.AlertDialog
      com.bluepay.sdk.c.aa.a(android.app.Activity, java.lang.CharSequence, java.lang.CharSequence):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.CharSequence, com.bluepay.interfaceClass.d):void
      com.bluepay.sdk.c.aa.a(com.bluepay.data.Order, java.lang.String, int[]):void
      com.bluepay.sdk.c.aa.a(com.bluepay.pay.IPayCallback, android.app.Activity, com.bluepay.pay.BlueMessage):void
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, int):boolean
      com.bluepay.sdk.c.aa.a(android.content.Context, java.lang.String, java.lang.String):int */
    public boolean onTouch(View v, MotionEvent event) {
        this.a.m.setBackgroundResource(aa.a((Context) this.a, "drawable", "bluep_ui_input_bg"));
        return false;
    }
}
