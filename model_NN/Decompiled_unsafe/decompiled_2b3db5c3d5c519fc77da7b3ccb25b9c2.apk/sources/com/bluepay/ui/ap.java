package com.bluepay.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.bluepay.sdk.c.aa;
import java.util.List;

/* compiled from: Proguard */
class ap extends BaseAdapter {
    final /* synthetic */ al a;
    private List b;
    private Context c;
    private int d = -1;

    public ap(al alVar, List list, Context context) {
        this.a = alVar;
        this.b = list;
        this.c = context;
    }

    public int getCount() {
        return this.b.size();
    }

    public Object getItem(int position) {
        return this.b.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    @SuppressLint({"DefaultLocale"})
    public View getView(int position, View convertView, ViewGroup parent) {
        aq aqVar;
        if (convertView == null) {
            aqVar = new aq(this);
            convertView = LayoutInflater.from(this.c).inflate(aa.a(this.c, "layout", "bluep_item_channel2"), (ViewGroup) null);
            aqVar.a = (ImageView) convertView.findViewById(aa.a(this.c, "id", "iv_channel_icon"));
            aqVar.b = (TextView) convertView.findViewById(aa.a(this.c, "id", "tv_channel_name"));
            aqVar.c = (ImageView) convertView.findViewById(aa.a(this.c, "id", "iv_selected"));
            aqVar.d = convertView.findViewById(aa.a(this.c, "id", "divider"));
            convertView.setTag(aqVar);
        } else {
            aqVar = (aq) convertView.getTag();
        }
        String str = (String) this.b.get(position);
        aqVar.a.setImageResource(aa.a(this.c, "drawable", "bluep_icon_" + str.toLowerCase()));
        aqVar.b.setText(aa.i(str));
        if (position == this.d) {
            aqVar.c.setVisibility(0);
        } else {
            aqVar.c.setVisibility(8);
        }
        if (position == this.b.size() - 1) {
            aqVar.d.setVisibility(8);
        } else {
            aqVar.d.setVisibility(0);
        }
        return convertView;
    }

    public void a(int i) {
        this.d = i;
    }
}
