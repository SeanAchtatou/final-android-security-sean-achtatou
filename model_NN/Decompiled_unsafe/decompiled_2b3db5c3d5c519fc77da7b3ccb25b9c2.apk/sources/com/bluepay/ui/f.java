package com.bluepay.ui;

import android.webkit.JavascriptInterface;
import com.bluepay.a.b.a;
import com.bluepay.data.b;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Proguard */
class f {
    final /* synthetic */ BankActivity a;

    f(BankActivity bankActivity) {
        this.a = bankActivity;
    }

    @JavascriptInterface
    public void a(String str) {
        int i;
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("status")) {
                i = jSONObject.getInt("status");
            } else {
                i = 0;
            }
            b bVar = new b(this.a, this.a.f.h(), 0, this.a.f.c());
            bVar.setCPPayType(this.a.f.l());
            try {
                bVar.setPrice(Integer.parseInt(this.a.f.e()));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            this.a.runOnUiThread(new g(this));
            a.o.a(14, i, 0, bVar);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }
}
