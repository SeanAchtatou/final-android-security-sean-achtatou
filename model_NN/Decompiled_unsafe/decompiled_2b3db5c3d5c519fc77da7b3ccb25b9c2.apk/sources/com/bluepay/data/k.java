package com.bluepay.data;

import com.bluepay.a.b.a;
import com.bluepay.a.b.as;
import com.bluepay.a.b.aw;
import com.bluepay.pay.Client;
import com.bluepay.sdk.c.aa;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: Proguard */
public class k {
    private String a;
    private String b;
    private String c;
    private boolean d;
    private String e;
    private String f;

    private k() {
        this.d = false;
        this.b = "500";
        this.c = Config.K_CURRENCY_TRF;
        this.a = "Tools";
    }

    public k(String str, String str2, String str3, String str4) {
        this.d = false;
        this.b = new String(str);
        this.f = new String(str2);
        this.e = new String(str3);
        this.c = new String(str4);
        this.a = "";
    }

    public String a() {
        return this.a;
    }

    private void c(String str) {
        this.a = str;
    }

    public String b() {
        return this.c;
    }

    private void d(String str) {
        this.c = str;
    }

    public String c() {
        return this.b;
    }

    public String d() {
        return this.e;
    }

    public void a(String str) {
        this.e = str;
    }

    public String e() {
        return this.f;
    }

    public void b(String str) {
        this.f = str;
    }

    public boolean f() {
        return this.d;
    }

    public static HashMap a(aw awVar, String str) {
        if (Client.m_hashPriceList == null) {
            Client.m_hashPriceList = new ArrayList();
        }
        Client.m_hashPriceList.clear();
        Object e2 = awVar.e(str);
        HashMap hashMap = new HashMap();
        try {
            JSONArray jSONArray = (JSONArray) e2;
            if (a.l != null) {
                a.l = null;
            }
            a.l = new int[jSONArray.length()];
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                if (!jSONObject.has(FirebaseAnalytics.Param.PRICE) || !jSONObject.has("shortcode")) {
                    a.l[i] = 0;
                } else {
                    if (jSONObject.has("keyword")) {
                        hashMap.put(jSONObject.getString(FirebaseAnalytics.Param.PRICE), new k(jSONObject.getString(FirebaseAnalytics.Param.PRICE), jSONObject.getString("shortcode"), jSONObject.getString("keyword"), ""));
                    } else {
                        hashMap.put(jSONObject.getString(FirebaseAnalytics.Param.PRICE), new k(jSONObject.getString(FirebaseAnalytics.Param.PRICE), jSONObject.getString("shortcode"), "", ""));
                    }
                    int a2 = aa.a(jSONObject.getString(FirebaseAnalytics.Param.PRICE), 0);
                    if (i == 0) {
                        as.a = a2;
                    }
                    if (a2 < as.a) {
                        as.a = a2;
                    }
                    if (!Client.m_hashPriceList.contains(Integer.valueOf(a2))) {
                        Client.m_hashPriceList.add(Integer.valueOf(a2));
                    }
                    a.l[i] = a2;
                }
            }
            return hashMap;
        } catch (Exception e3) {
            e3.printStackTrace();
            return hashMap;
        }
    }
}
