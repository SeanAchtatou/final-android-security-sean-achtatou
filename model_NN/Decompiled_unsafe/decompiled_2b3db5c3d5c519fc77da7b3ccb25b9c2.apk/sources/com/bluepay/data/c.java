package com.bluepay.data;

import com.bluepay.a.b.aw;
import com.bluepay.sdk.exception.BlueException;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Proguard */
public class c {
    public String a;
    public String b;
    public String c;
    public int d;

    public c() {
    }

    public c(String str, String str2, String str3, int i) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = i;
    }

    public static c a(aw awVar) {
        JSONException jSONException;
        c cVar;
        BlueException blueException;
        c cVar2;
        c cVar3 = null;
        try {
            JSONObject jSONObject = (JSONObject) awVar.e("BlueWallet");
            if (jSONObject != null) {
                cVar3 = new c();
            }
            try {
                if (jSONObject.has("scheme")) {
                    cVar3.a = jSONObject.getString("scheme");
                }
                if (jSONObject.has("urlIos")) {
                    cVar3.b = jSONObject.getString("urlIos");
                }
                if (jSONObject.has("urlAndrdoid")) {
                    cVar3.c = jSONObject.getString("urlAndrdoid");
                }
                if (jSONObject.has("blueCoinsTip")) {
                    cVar3.d = jSONObject.getInt("blueCoinsTip");
                }
                return cVar3;
            } catch (BlueException e) {
                BlueException blueException2 = e;
                cVar2 = cVar3;
                blueException = blueException2;
                blueException.printStackTrace();
                return cVar2;
            } catch (JSONException e2) {
                JSONException jSONException2 = e2;
                cVar = cVar3;
                jSONException = jSONException2;
                jSONException.printStackTrace();
                return cVar;
            }
        } catch (BlueException e3) {
            BlueException blueException3 = e3;
            cVar2 = null;
            blueException = blueException3;
            blueException.printStackTrace();
            return cVar2;
        } catch (JSONException e4) {
            JSONException jSONException3 = e4;
            cVar = null;
            jSONException = jSONException3;
            jSONException.printStackTrace();
            return cVar;
        }
    }
}
