package com.bluepay.data;

import java.io.Serializable;

/* compiled from: Proguard */
public class j implements Serializable {
    private static final long n = 3427427686346955466L;
    public int a;
    public String b;
    public String c;
    public String d;
    public String e;
    public int f;
    public String g;
    public String h;
    public int i;
    public String j;
    public String k;
    public String l;
    public String m = "";

    public j(int i2, String str, String str2, String str3, String str4, int i3, String str5, String str6, int i4) {
        this.a = i2;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = i3;
        this.g = str5;
        this.h = str6;
        this.i = i4;
    }

    public int a() {
        return this.a;
    }

    public void a(int i2) {
        this.a = i2;
    }

    public String b() {
        return this.b;
    }

    public void a(String str) {
        this.b = str;
    }

    public String c() {
        return this.c;
    }

    public void b(String str) {
        this.c = str;
    }

    public String d() {
        return this.d;
    }

    public void c(String str) {
        this.d = str;
    }

    public String e() {
        return this.e;
    }

    public void d(String str) {
        this.e = str;
    }

    public int f() {
        return this.f;
    }

    public void b(int i2) {
        this.f = i2;
    }

    public String g() {
        return this.g;
    }

    public void e(String str) {
        this.g = str;
    }

    public String h() {
        return this.h;
    }

    public void f(String str) {
        this.h = str;
    }

    public int i() {
        return this.i;
    }

    public void c(int i2) {
        this.i = i2;
    }

    public String j() {
        return this.m;
    }

    public void g(String str) {
        this.m = str;
    }

    public String k() {
        return this.k;
    }

    public void h(String str) {
        this.k = str;
    }

    public String l() {
        return this.l;
    }

    public void i(String str) {
        this.l = str;
    }
}
