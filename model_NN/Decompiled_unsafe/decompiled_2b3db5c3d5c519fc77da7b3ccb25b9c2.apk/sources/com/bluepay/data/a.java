package com.bluepay.data;

import com.bluepay.a.b.aw;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.exception.BlueException;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Proguard */
public class a {
    public boolean a;
    public String b;
    public boolean c;
    public int d = 25;
    public int e;
    public int f;
    public String g;
    public String h;

    public a() {
    }

    public a(boolean z, String str) {
        this.a = z;
        this.b = str;
    }

    public a(boolean z, String str, boolean z2, int i, int i2, int i3, String str2, String str3) {
        this.a = z;
        this.b = str;
        this.c = z2;
        this.d = i;
        this.e = i2;
        this.f = i3;
        this.g = str2;
        this.h = str3;
    }

    public static a a(aw awVar) {
        JSONException jSONException;
        a aVar;
        BlueException blueException;
        a aVar2;
        a aVar3 = null;
        try {
            JSONObject jSONObject = (JSONObject) awVar.e("bankInfo");
            c.c("BankSupportInfo: " + jSONObject.toString());
            if (jSONObject != null) {
                aVar3 = new a();
            }
            try {
                if (jSONObject.has("IsBankSupport") && jSONObject.getInt("IsBankSupport") > 0) {
                    aVar3.a = true;
                }
                if (jSONObject.has("SmsCenter")) {
                    aVar3.b = jSONObject.getString("SmsCenter");
                }
                if (jSONObject.has("IsPayByOfflineSupport")) {
                    int i = jSONObject.getInt("IsPayByOfflineSupport");
                    aVar3.d = jSONObject.getInt("OfflineOperator");
                    if (i > 0) {
                        aVar3.c = true;
                    }
                }
                if (jSONObject.has("otc_fee")) {
                    aVar3.e = jSONObject.getInt("otc_fee");
                }
                if (jSONObject.has("atm_fee")) {
                    aVar3.f = jSONObject.getInt("atm_fee");
                }
                if (jSONObject.has("VtcBankUrl")) {
                    aVar3.g = jSONObject.getString("VtcBankUrl");
                }
                if (jSONObject.has("inBankUrl")) {
                    aVar3.h = jSONObject.getString("inBankUrl");
                }
                return aVar3;
            } catch (BlueException e2) {
                BlueException blueException2 = e2;
                aVar2 = aVar3;
                blueException = blueException2;
                blueException.printStackTrace();
                return aVar2;
            } catch (JSONException e3) {
                JSONException jSONException2 = e3;
                aVar = aVar3;
                jSONException = jSONException2;
                jSONException.printStackTrace();
                return aVar;
            }
        } catch (BlueException e4) {
            BlueException blueException3 = e4;
            aVar2 = null;
            blueException = blueException3;
            blueException.printStackTrace();
            return aVar2;
        } catch (JSONException e5) {
            JSONException jSONException3 = e5;
            aVar = null;
            jSONException = jSONException3;
            jSONException.printStackTrace();
            return aVar;
        }
    }
}
