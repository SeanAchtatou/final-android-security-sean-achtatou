package com.bluepay.data;

import android.annotation.SuppressLint;
import android.os.Build;
import com.bluepay.a.b.a;
import org.apache.http.HttpStatus;

/* compiled from: Proguard */
public class Config {
    public static final String ACTION_LINE = "com.bluepay.line";
    public static int AD_ACCESS_OUT = 607;
    public static int AD_DATA_ERROR = 404;
    public static int AD_ENCRY_ERROR = 401;
    public static final int AD_GET_CONFIRM = 53;
    public static int AD_LIMIT = 608;
    public static int AD_PARAS_ERROR = HttpStatus.SC_BAD_REQUEST;
    public static int AD_REPEAT_CONFIRM = 606;
    public static final String COUNTRY_CN = "CN";
    public static final String COUNTRY_ID = "ID";
    public static final String COUNTRY_TH = "TH";
    public static final String COUNTRY_VN = "VN";
    public static final String ERROR_C_BluePay_IMSI = "0000000000";
    public static final String ERROR_C_BluePay_KEY = "";
    public static final String ERROR_C_BluePay_LAN = "en";
    public static final String ERROR_C_BluePay_Price = "0";
    public static final int ERROR_C_BluePay_ProductID = 0;
    public static final String ERROR_C_BluePay_PromotionID = null;
    public static final String EXTRA_AD_INFO = "offer_info";
    public static final String EXTRA_AD_TID = "bt_id";
    public static final String EXTRA_LINE_URL = "line_pay_url";
    public static final String EXTRA_URL = "BluelURL";
    public static final int GET_URL = 12;
    public static final int GET_URL_ERROR = 13;
    public static final String K_BUY_MODE = "BuyMode";
    public static final String K_CURRENCY_ID = "IDR";
    public static final String K_CURRENCY_PRE = "t";
    public static final String K_CURRENCY_THB = "THB";
    public static final String K_CURRENCY_TRF = "TRF";
    public static final String K_CURRENCY_VND = "VND";
    public static final String K_GAME_CURRENCY_ID = "CurrencyID";
    public static final String K_GAME_CUSTOMID = "CustomID";
    public static final String K_GAME_T_ID = "TransactionID";
    public static final String K_GOODS_NAME = "Goods";
    public static final String K_IS_CLOSE_WINDOW = "CloseWindow";
    public static final String K_MSG_HANDLE = "Handler";
    public static final String K_MSG_WHAT = "What";
    public static final String K_PRICE = "Price";
    public static final String LAN_ID1 = "id";
    public static final String LAN_ID2 = "id-ID";
    public static final String LAN_ID3 = "in";
    public static final String LAN_ID4 = "in-ID";
    public static final String LAN_TH1 = "th";
    public static final String LAN_TH2 = "th_TH";
    public static final String LAN_VN1 = "vi-VN";
    public static final String LAN_VN2 = "vi";
    public static final byte MOBILE_PHONE_CARD = 5;
    public static final byte MOBILE_PHONE_KNOWN = 6;
    public static byte MOBILE_PHONE_TYPE = 5;
    public static final String NETWORKTYPE_2G = "2G";
    public static final String NETWORKTYPE_3G = "3G";
    public static final String NETWORKTYPE_INVALID = "unknow";
    public static final String NETWORKTYPE_WAP = "wap";
    public static final String NETWORKTYPE_WIFI = "wifi";
    public static final int OPERATOR_AIS = 2;
    public static final int OPERATOR_BANK = 21;
    public static final int OPERATOR_CARD = 4;
    public static final int OPERATOR_DTAC = 3;
    public static final int OPERATOR_HUAWEI = 27;
    public static final int OPERATOR_TDP = 1;
    public static final int OPERATOR_TELENOR = 20;
    public static final int OPERATOR_TEST = 9;
    public static final int OPERATOR_UNKNOWN = 0;
    public static final int ORDER_CASHCARD = 3;
    public static final int ORDER_DCB = 5;
    public static final int ORDER_DISMANTLE_SMS = 4;
    public static final int ORDER_HW = 8;
    public static final int ORDER_OFFLINE = 7;
    public static final int ORDER_SMS = 0;
    public static final int ORDER_WALLET = 6;
    public static final int PAY_AOC = 1;
    public static final int PAY_BANK = 8;
    public static final int PAY_CARD = 5;
    public static final int PAY_CREATETRANSACTION = 11;
    public static final int PAY_DCB = 6;
    public static final int PAY_OFFLINE = 9;
    public static final int PAY_OTP = 2;
    public static final int PAY_STANDARD = 0;
    public static final int PAY_STEPS_ACTIVE = 4;
    public static final int PAY_STEPS_PASSIVE = 3;
    public static final int PAY_WALLET = 7;
    public static final String PHONE_TYPE = Build.MODEL;
    public static int PhoneType = 0;
    public static final int PhoneType_MTK = 0;
    public static final int PhoneType_QUALCOMM = 3;
    public static final int PhoneType_SP1 = 1;
    public static final int PhoneType_SPEXLA = 2;
    @SuppressLint({"NewApi"})
    public static final String ROM_VERSION = Build.DISPLAY;
    public static final String SDK_LEVE = Build.VERSION.RELEASE;
    public static final int SERVER_ERROR = 500;
    public static final int SERVER_SUCCESS = 200;
    public static String SERVICE_BACK = "com.bluepay.service.BackService";
    public static final int SHOW_CREATE_TRANSACTION = 6;
    public static final int SHOW_FINAL_RESULT = 14;
    public static final int SHOW_LOGIC = 5;
    public static final int SHOW_SEND_CHECK_SAFE = 0;
    public static final int SHOW_SMS_SEND_FINISH = 2;
    public static final int SHOW_SMS_SEND_SUCCESS = 3;
    public static final int SHOW_STEP_CHECK_BILLING = 1;
    public static final String SMS_ERROR = "404";
    public static final String SMS_NO_MONEY = "601";
    public static final String SMS_STATE_OK = "200";
    public static final String SMS_STATE_SENDING = "201";
    public static final String SMS_STEPS_FAILED = "605";
    public static final String TELCO_NAME_HUTCHISON = "hutchison";
    public static final String TELCO_NAME_INDOSAT = "indosat";
    public static String TELCO_NAME_SMARTFREN = "smartfren";
    public static final String TELCO_NAME_TELKOMSEL = "telkomsel";
    public static final String TELCO_NAME_XL = "xl";
    public static final String URL_BIND_BANK_CARD = "http://www.jmtt.co.th/bank/";
    public static final int VERSION = 100038002;
    private static boolean a = false;
    private static final String b = "api.bluepay.asia";
    private static final int c = 9999;
    private static final String d = "api.bluepay.asia";
    private static final int e = 80;
    private static final String f = "http://asik.indosat.com/v1/baidu.jsp";
    private static boolean g = false;
    private static e h = e.Sms;

    public static void setAisVersionToDcb() {
        h = e.Dcb;
    }

    public static void setAisVersionToSms() {
        h = e.Sms;
    }

    public static boolean isAisVersionDcb() {
        return h == e.Dcb;
    }

    public static boolean isAisVersionSms() {
        return h == e.Sms;
    }

    public static void setDebug(boolean isDebug) {
        g = isDebug;
    }

    public static boolean isDebug() {
        return g;
    }

    public static void setAddressDebug(boolean isDebug) {
        a = isDebug;
    }

    public static boolean isAddressDebug() {
        return a;
    }

    public static String getCacheIp() {
        return a ? "api.bluepay.asia" : "api.bluepay.asia";
    }

    public static String getIp() {
        if (a.c == null) {
            return a ? "api.bluepay.asia" : "api.bluepay.asia";
        }
        return a.c;
    }

    public static int getPort() {
        if (a) {
            return c;
        }
        return 80;
    }

    public static String getBaiduExploreUrl() {
        return f;
    }
}
