package com.bluepay.data;

import android.app.Activity;
import android.util.Xml;
import com.bluepay.pay.Client;
import com.bluepay.sdk.c.aa;
import com.google.firebase.analytics.FirebaseAnalytics;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: Proguard */
public class l {
    private int a = -1;
    private String b = Config.LAN_TH1;
    private String c = null;
    private String d = null;

    public l(Activity activity, String str) {
        try {
            InputStream open = activity.getResources().getAssets().open(str);
            XmlPullParser newPullParser = Xml.newPullParser();
            newPullParser.setInput(open, "utf-8");
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                switch (eventType) {
                    case 2:
                        if (!newPullParser.getName().equals("lang")) {
                            if (!newPullParser.getName().equals("ek")) {
                                if (!newPullParser.getName().equals("productId")) {
                                    if (!newPullParser.getName().equals("promotionId") && !newPullParser.getName().equals("promotinId")) {
                                        if (!newPullParser.getName().equals("exchange")) {
                                            break;
                                        } else {
                                            HashMap hashMap = new HashMap();
                                            String str2 = null;
                                            for (int i = 0; i < newPullParser.getAttributeCount(); i++) {
                                                if (newPullParser.getAttributeName(i).equals(FirebaseAnalytics.Param.VALUE)) {
                                                    str2 = newPullParser.getAttributeValue(i).trim();
                                                }
                                            }
                                            hashMap.put(str2, newPullParser.nextText().trim());
                                            Client.m_exchangeList.add(hashMap);
                                            break;
                                        }
                                    } else {
                                        this.c = newPullParser.nextText();
                                        break;
                                    }
                                } else {
                                    this.a = aa.a(newPullParser.nextText(), -1);
                                    break;
                                }
                            } else {
                                this.d = newPullParser.nextText();
                                break;
                            }
                        } else {
                            this.b = new String(newPullParser.nextText().trim().toLowerCase());
                            break;
                        }
                        break;
                }
            }
            open.close();
        } catch (XmlPullParserException e) {
            throw e;
        } catch (IOException e2) {
            throw e2;
        }
    }

    public l(String str, int i, String str2) {
        this.b = new String(str);
        this.a = i;
        this.c = str2;
    }

    public int a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }
}
