package com.bluepay.data;

import android.app.Activity;
import com.bluepay.a.b.a;
import com.bluepay.pay.Client;
import com.bluepay.pay.PublisherCode;
import com.bluepay.sdk.exception.BlueException;
import java.io.Serializable;

/* compiled from: Proguard */
public class b extends Order implements Serializable {
    public static final String a = "huawei";
    private static final long u = -6919461967497580385L;
    private String A;
    private boolean B;
    private String C;
    private String D;
    private String E;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    public b(Order order) {
        super(order.getActivity(), order.getReference());
        this.g = order.getCustomId();
        this.d = order.getProductId();
        this.e = order.getPromotionId();
        this.f = order.getTransactionId();
        this.h = order.getPrice();
        this.i = order.getSmsId();
        this.j = order.getPropsName();
        this.n = order.getOperator();
        this.m = order.getDesMsisdn();
        this.o = order.getCard();
        this.r = order.getShowUI();
        this.k = order.getCurrency();
        this.s = order.getCheckNum();
        this.t = order.getScheme();
        if (order.desc != null) {
            this.desc = new String(order.desc);
        }
        if (order.o != null) {
            this.o = new String(order.o);
        } else {
            this.o = "";
        }
        if (order.p != null) {
            this.p = new String(order.p);
        } else {
            this.p = "";
        }
        if (order.q != null) {
            this.q = new String(order.q);
        } else {
            this.q = "";
        }
        this.A = order.getTransactionId();
        this.w = 0;
        this.x = 2;
        this.v = 0;
        this.z = 0;
        this.B = false;
    }

    public b(b bVar, int i) {
        super(bVar.getActivity(), bVar.getReference());
        this.g = bVar.getCustomId();
        this.d = bVar.getProductId();
        this.e = bVar.getPromotionId();
        this.f = bVar.getTransactionId();
        this.h = i;
        this.i = bVar.getSmsId();
        this.j = bVar.getPropsName();
        this.n = bVar.getOperator();
        this.m = bVar.getDesMsisdn();
        this.o = bVar.getCard();
        this.r = bVar.getShowUI();
        this.k = bVar.getCurrency();
        this.s = bVar.getCheckNum();
        if (bVar.desc != null) {
            this.desc = new String(bVar.desc);
        }
        this.w = bVar.b();
        this.x = bVar.c();
        this.v = bVar.a();
        this.z = bVar.f();
        this.B = bVar.i();
        this.A = bVar.g();
    }

    public b(Activity activity, Reference reference, String str) {
        super(activity, reference, str);
        this.w = 0;
        this.x = 2;
        this.v = 0;
        this.z = 0;
        this.B = false;
    }

    public b(Activity activity, String str, int i, String str2, String str3, String str4, String str5, Reference reference, boolean z2, int i2) {
        super(activity, str, i, str2, str3, str4, str5, reference, z2, i2);
        this.w = 0;
        this.x = 2;
        this.v = 0;
        this.z = 0;
        this.B = false;
    }

    public b(Activity activity, String str, int i, int i2, String str2) {
        super(activity, str, i, i2, str2);
        this.w = 0;
        this.x = 2;
        this.v = 0;
        this.z = 0;
        this.B = false;
    }

    public b(Activity activity, String str, int i, String str2) {
        super(activity, str, i, str2);
    }

    public void a(int i) {
        this.v = i;
    }

    public int a() {
        return this.v;
    }

    public int b() {
        return this.w;
    }

    public void b(int i) {
        this.w = i;
    }

    public int c() {
        return this.x;
    }

    public void c(int i) {
        this.x = i;
    }

    public int d() {
        return this.y;
    }

    public void d(int i) {
        this.y = i;
    }

    public void e() {
        this.z++;
    }

    public int f() {
        return this.z;
    }

    public void a(String str) {
        this.A = new String(str);
    }

    public String g() {
        return this.A;
    }

    public boolean h() {
        return this.x == 2;
    }

    public void a(boolean z2) {
        this.B = z2;
    }

    public boolean i() {
        return this.B;
    }

    public void a(Order order) {
    }

    public String j() {
        return this.C;
    }

    public void b(String str) {
        this.C = str;
    }

    public String k() {
        if (this.q.equals(PublisherCode.PUBLISHER_BANK)) {
            this.D = Client.m_BankChargeInfo.b;
        } else if (this.q.equals(a)) {
            return this.D;
        } else {
            if (Client.m_hashChargeList.containsKey(this.h + "")) {
                k kVar = (k) Client.m_hashChargeList.get(this.h + "");
                if (kVar != null) {
                    this.D = kVar.e();
                }
            } else {
                throw new BlueException(h.i, "PRICE IS ERROR! " + this.h, new Object[0]);
            }
        }
        return this.D;
    }

    public void c(String str) {
        this.D = str;
    }

    public void d(String str) {
        this.E = str;
    }

    public String l() {
        Reference reference = getReference();
        StringBuilder sb = new StringBuilder();
        String str = null;
        if (this.q.equals(PublisherCode.PUBLISHER_BANK)) {
            return a(this, a.h);
        }
        if (this.q.equals(PublisherCode.PUBLISHER_SMS)) {
            str = reference.getSMS_2_Content(this.A);
        } else if (this.q.equals(a)) {
            return this.E;
        }
        sb.append(str);
        sb.append(a.h);
        sb.append(this.i);
        sb.append(a.h);
        sb.append(this.e);
        sb.append(a.h);
        sb.append((int) Config.VERSION);
        sb.append(a.h);
        return sb.toString();
    }

    public String m() {
        k kVar;
        if (!Client.m_hashChargeList.containsKey(this.h + "") || (kVar = (k) Client.m_hashChargeList.get(this.h + "")) == null) {
            return "";
        }
        return kVar.d();
    }

    private String a(b bVar, String str) {
        Reference reference = getReference();
        return reference.getSMS_6_Content(this.f) + str + this.e + str + this.h + str + Client.getIMSI() + str + Client.getIMEI();
    }
}
