package com.bluepay.data;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.HashMap;
import java.util.Map;

/* compiled from: Proguard */
public class g {
    public static final String a = "ENV";

    public static void a(Context context, Map map) {
        StringBuffer stringBuffer = new StringBuffer();
        for (String str : map.keySet()) {
            stringBuffer.append(str + ":" + ((Long) map.get(str)) + ",");
        }
        if (stringBuffer.length() != 0 && stringBuffer.toString().contains(",")) {
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
            SharedPreferences.Editor edit = context.getSharedPreferences(a, 0).edit();
            edit.putString("task", stringBuffer.toString());
            edit.commit();
        }
    }

    public static Map a(Context context) {
        String string = context.getSharedPreferences(a, 0).getString("task", "");
        HashMap hashMap = new HashMap();
        for (String split : string.split(",")) {
            String[] split2 = split.split(":");
            hashMap.put(split2[0], Long.valueOf(Long.parseLong(split2[1])));
        }
        return hashMap;
    }
}
