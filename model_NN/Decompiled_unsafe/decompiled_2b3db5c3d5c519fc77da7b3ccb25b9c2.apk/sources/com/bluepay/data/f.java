package com.bluepay.data;

import android.text.TextUtils;
import com.bluepay.a.b.aw;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.c.aa;
import com.bluepay.sdk.exception.BlueException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Proguard */
public class f implements Serializable {
    public static final long a = -5724500763338508559L;
    public static final int d = 1;
    public static final int e = 2;
    public static final int f = 3;
    public static final int g = 4;
    public static final int h = 5;
    public static final int i = 6;
    public static final int j = 7;
    public static final int k = 0;
    public static final int l = 1;
    public int b;
    public String c;
    public String[] m;
    public String n;
    public String o;
    public int p;
    public int q;
    private Map r;

    public f() {
    }

    public f(int i2) {
        this.b = i2;
    }

    public f(int i2, String str, String[] strArr, String str2, String str3, int i3, int i4, Map map) {
        this.b = i2;
        this.c = str;
        this.m = strArr;
        this.n = str2;
        this.o = str3;
        this.p = i3;
        this.q = i4;
        this.r = map;
    }

    public static f a(aw awVar) {
        f fVar;
        BlueException blueException;
        JSONException jSONException;
        f fVar2;
        f fVar3 = null;
        try {
            JSONObject jSONObject = (JSONObject) awVar.e("dcbInfo");
            if (jSONObject != null) {
                fVar3 = new f();
            }
            try {
                if (jSONObject.has("CarrierBillingType")) {
                    fVar3.b = jSONObject.getInt("CarrierBillingType");
                }
                if (jSONObject.has("MsisdnDetect")) {
                    fVar3.c = jSONObject.getString("MsisdnDetect");
                }
                if (jSONObject.has("OTPKey")) {
                    fVar3.m = a(jSONObject.getJSONArray("OTPKey"));
                }
                if (jSONObject.has("OTPAdress")) {
                    fVar3.n = jSONObject.getString("OTPAdress");
                }
                if (jSONObject.has("keyLength")) {
                    fVar3.o = jSONObject.getString("keyLength");
                }
                if (jSONObject.has("queryPhoneType")) {
                    fVar3.p = jSONObject.getInt("queryPhoneType");
                }
                if (jSONObject.has("hwTSLsupport")) {
                    fVar3.q = jSONObject.getInt("hwTSLsupport");
                }
                if (jSONObject.has("tipsArray")) {
                    if (fVar3.r == null) {
                        fVar3.r = new HashMap();
                    } else {
                        fVar3.r.clear();
                    }
                    JSONObject jSONObject2 = jSONObject.getJSONObject("tipsArray");
                    fVar3.r.put(Config.TELCO_NAME_HUTCHISON, jSONObject2.getString(Config.TELCO_NAME_HUTCHISON));
                    fVar3.r.put(Config.TELCO_NAME_INDOSAT, jSONObject2.getString(Config.TELCO_NAME_INDOSAT));
                    fVar3.r.put(Config.TELCO_NAME_XL, jSONObject2.getString(Config.TELCO_NAME_XL));
                }
                c.c("get dcb info:" + jSONObject.toString());
                return fVar3;
            } catch (BlueException e2) {
                BlueException blueException2 = e2;
                fVar = fVar3;
                blueException = blueException2;
                blueException.printStackTrace();
                return fVar;
            } catch (JSONException e3) {
                JSONException jSONException2 = e3;
                fVar2 = fVar3;
                jSONException = jSONException2;
                jSONException.printStackTrace();
                return fVar2;
            }
        } catch (BlueException e4) {
            BlueException blueException3 = e4;
            fVar = null;
            blueException = blueException3;
            blueException.printStackTrace();
            return fVar;
        } catch (JSONException e5) {
            JSONException jSONException3 = e5;
            fVar2 = null;
            jSONException = jSONException3;
            jSONException.printStackTrace();
            return fVar2;
        }
    }

    public String a(String str) {
        if (this.r == null) {
            return "";
        }
        String g2 = aa.g(str);
        if (TextUtils.isEmpty(g2)) {
            return "";
        }
        try {
            return (String) this.r.get(g2);
        } catch (Exception e2) {
            e2.printStackTrace();
            return (String) this.r.get(Config.TELCO_NAME_INDOSAT);
        }
    }

    private static String[] a(JSONArray jSONArray) {
        if (jSONArray == null) {
            return null;
        }
        String[] strArr = new String[jSONArray.length()];
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            try {
                strArr[i2] = jSONArray.getString(i2);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return strArr;
    }
}
