package com.bluepay.data;

import android.app.Activity;
import com.bluepay.pay.Client;
import com.bluepay.sdk.c.aa;
import java.io.Serializable;

/* compiled from: Proguard */
public class Order implements Serializable {
    private static final long a = 1;
    protected static final String b = "0";
    protected final Activity c;
    protected int d;
    public String desc = "";
    protected String e;
    protected String f;
    protected String g;
    protected int h;
    protected int i;
    protected String j;
    protected String k;
    protected Reference l;
    protected String m;
    protected int n;
    protected String o;
    protected String p;
    protected String q;
    protected boolean r;
    protected int s = 6;
    protected String t;

    public Order(Activity activity, Reference reference) {
        this.c = activity;
        this.l = reference;
    }

    public Order(Activity activity, String customId, int productId, String promotionId, String transactionId, String dsMins, String propsName, Reference reference, boolean showUI, int operator) {
        this.c = activity;
        this.g = customId;
        this.d = productId;
        this.e = promotionId;
        this.f = transactionId == null ? "0" : transactionId;
        this.h = 0;
        this.i = 0;
        this.j = propsName == null ? "0" : propsName;
        this.l = reference;
        this.n = operator;
        this.m = dsMins;
        this.o = "00000";
        this.r = showUI;
        this.k = Config.K_CURRENCY_THB;
    }

    public Order(Activity activity, String transactionId, int price, int smsId, String propsName) {
        this.c = activity;
        this.g = "";
        this.d = 0;
        this.e = "1000";
        this.f = transactionId == null ? "0" : transactionId;
        this.h = price;
        this.i = smsId;
        this.j = propsName == null ? "0" : propsName;
        this.l = new Reference(Client.m_iOperatorId, Client.getProductId(), 2, price, Client.getPromotionId());
        this.n = 0;
        this.m = "";
        this.o = "";
        this.r = false;
        this.k = Config.K_CURRENCY_THB;
    }

    public Order(Activity activity, Reference reference, String card) {
        this.c = activity;
        this.g = "";
        this.d = 0;
        this.f = "0";
        this.h = 0;
        this.i = 0;
        this.j = "0";
        this.l = reference;
        this.n = 0;
        this.m = "";
        this.o = "";
        this.r = false;
        this.k = Config.K_CURRENCY_THB;
    }

    public Order(Activity activity, String currency, int price, String transId) {
        this.c = activity;
        this.k = currency;
        this.h = price;
        this.f = transId;
        this.l = null;
    }

    public String getCurrency() {
        return this.k;
    }

    public void setCurrency(String _currency) {
        this.k = _currency;
    }

    public Activity getActivity() {
        return this.c;
    }

    public void setCustomId(String _customId) {
        this.g = new String(_customId);
    }

    public String getCustomId() {
        return this.g;
    }

    public void setProductId(int _productId) {
        this.d = _productId;
    }

    public int getProductId() {
        return this.d;
    }

    public void setPromotionId(String _promotionId) {
        this.e = _promotionId;
    }

    public String getPromotionId() {
        return this.e;
    }

    public void setTransactionId(String _transactionId) {
        this.f = new String(_transactionId);
    }

    public String getTransactionId() {
        return this.f;
    }

    public void setPrice(int _price) {
        this.h = _price;
    }

    public int getPrice() {
        return this.h;
    }

    public void setSmsId(int _smsID) {
        this.i = _smsID;
    }

    public int getSmsId() {
        return this.i;
    }

    public void setPropsName(String _propsName) {
        this.j = new String(_propsName);
    }

    public String getPropsName() {
        return this.j;
    }

    public void setReference(Reference _reference) {
        this.l = _reference;
    }

    public Reference getReference() {
        return this.l;
    }

    public void setOperator(int _operator) {
        this.n = _operator;
    }

    public int getOperator() {
        return this.n;
    }

    public void setDesMsisdn(String _msisdn) {
        this.m = new String(_msisdn);
    }

    public String getDesMsisdn() {
        return this.m;
    }

    public void setShowUI(boolean _isShow) {
        this.r = _isShow;
    }

    public boolean getShowUI() {
        return this.r;
    }

    public void setCard(String _cardNo) {
        this.o = new String(_cardNo);
    }

    public String getCard() {
        return this.o;
    }

    public void setSerialNo(String _serialNo) {
        this.p = new String(_serialNo);
    }

    public String getSerialNo() {
        return this.p;
    }

    public void setCPPayType(String _payType) {
        this.q = new String(_payType);
    }

    public String getCPPayType() {
        return this.q;
    }

    public void setCheckNum(int _checkNum) {
        this.s = _checkNum;
    }

    public int getCheckNum() {
        return this.s;
    }

    public String getScheme() {
        return this.t;
    }

    public void setScheme(String scheme) {
        this.t = scheme;
    }

    public boolean isShowLoading2Dialog() {
        return this.r && this.s > 2;
    }

    public static int getSafePrice(String _currency, String _price) {
        if (Config.K_CURRENCY_TRF.equals(_currency)) {
            if (Client.m_hashTRFPriceList == null) {
                return 0;
            }
            if (Client.m_hashTRFPriceList.size() > 0 && Client.m_hashTRFPriceList.containsKey(_price + "")) {
                String str = (String) Client.m_hashTRFPriceList.get(_price + "");
                if (Client.m_hashChargeList.containsKey(str)) {
                    return aa.a(((k) Client.m_hashChargeList.get(str + "")).c(), 0);
                }
            }
        } else if (Client.m_hashChargeList.containsKey(_price + "")) {
            return aa.a(((k) Client.m_hashChargeList.get(_price + "")).c(), 0);
        }
        return 0;
    }
}
