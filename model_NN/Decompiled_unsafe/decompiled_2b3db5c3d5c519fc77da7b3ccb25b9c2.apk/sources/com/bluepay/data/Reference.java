package com.bluepay.data;

/* compiled from: Proguard */
public class Reference {
    public static final int a = 1;
    private static final int b = 2;
    private static final int c = 4;
    private static final String d = "A";
    private static final String e = "0";
    private final int f = 2;
    private final int g;
    private final int h;
    private final int i;
    private final int j;
    private final String k;
    private final String l;

    public Reference(int operator, int productId, int type, int price, String promotionId) {
        this.g = operator;
        this.h = productId;
        this.i = type;
        this.j = price;
        this.k = promotionId;
        this.l = String.valueOf(System.currentTimeMillis() % 10000);
    }

    public String getReferenceId() {
        if (this.g == 4) {
            throw new IllegalArgumentException("not support operator");
        }
        String a2 = a(String.valueOf(this.f), 1, d);
        String a3 = a(String.valueOf(this.g), 1, d);
        String a4 = a(String.valueOf(this.h), 3, d);
        String a5 = a(String.valueOf(this.i), 1, d);
        String a6 = a(String.valueOf(this.j), 8, d);
        String a7 = a(String.valueOf(this.k), 4, d);
        return a2 + a3 + a4 + a5 + a6 + a7 + a(this.l, 4, d);
    }

    public String getSMS_2_Content(String t_id) {
        if (this.g == 4) {
            throw new IllegalArgumentException("not support operator");
        }
        String a2 = a(String.valueOf(this.f), 1, "0");
        return a2 + a(String.valueOf(this.h), 4, "0") + t_id;
    }

    public String getSMS_4_Content(String t_id, int processFlag, int sequenceId) {
        if (this.g == 4) {
            throw new IllegalArgumentException("not support operator");
        }
        String a2 = a(String.valueOf(processFlag), 1, "0");
        String a3 = a(String.valueOf(sequenceId), 1, "0");
        String a4 = a(String.valueOf(4), 1, "0");
        return a4 + a2 + a3 + a(String.valueOf(this.h), 8, "0") + t_id;
    }

    public String getSMS_6_Content(String t_id) {
        if (this.g == 4) {
            throw new IllegalArgumentException("not support operator");
        }
        return "6" + a(String.valueOf(this.h), 4, "0") + t_id;
    }

    private String a(String str) {
        return str.replaceAll(d, "");
    }

    private String a(String str, int i2, String str2) {
        if (str.length() > i2) {
            throw new IllegalArgumentException(str + " length can not be more than " + i2);
        }
        StringBuilder sb = new StringBuilder();
        for (int i3 = 0; i3 < i2 - str.length(); i3++) {
            sb.append(str2);
        }
        sb.append(str);
        return sb.toString();
    }

    public int getVersion() {
        return this.f;
    }

    public int getOperator() {
        return this.g;
    }

    public int getProductId() {
        return this.h;
    }

    public int getType() {
        return this.i;
    }

    public int getPrice() {
        return this.j;
    }

    public String getPromotionId() {
        return this.k;
    }
}
