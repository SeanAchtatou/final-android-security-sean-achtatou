package com.bluepay.data;

import android.text.TextUtils;
import com.bluepay.a.b.a;
import com.bluepay.pay.Client;
import com.bluepay.pay.PublisherCode;
import com.bluepay.sdk.b.c;
import com.mol.seaplus.tool.connection.internet.InternetConnection;

/* compiled from: Proguard */
public class m {
    private static final String a = "/charge";
    private static final String b = "http://www.jmtt.co.th/msisdn";
    private static final String c = "http://203.151.93.125:8089/";
    private static final String d = "blueAdServer/getOffer";
    private static final String e = "blueAdServer/confirmInstal";
    private static final String f = "blueAdServer/offerDetail";
    private static final String g = "blueAdServer/uploadInstalList";

    private static String c(String str) {
        String str2 = Config.getPort() != 80 ? ":" + Config.getPort() : "";
        String ip = Config.getIp();
        if (ip.indexOf(InternetConnection.HTTP_PROTOCOL) != -1) {
            return ip + str2 + a + str;
        }
        return InternetConnection.HTTP_PROTOCOL + ip + str2 + a + str;
    }

    private static String b(String str, String str2) {
        return str + (Config.getPort() != 80 ? ":" + Config.getPort() : "") + a + str2;
    }

    public static String a() {
        return c("/charge/getConfig");
    }

    public static String b() {
        return c("/tdp/uploadStatistics");
    }

    public static String c() {
        return c("/ais/uploadStatistics");
    }

    public static String d() {
        return c("/dtac/uploadStatistics");
    }

    public static String e() {
        return c("/ais/getAisCharge");
    }

    public static String f() {
        return b;
    }

    public static String g() {
        return c("/dtacSms/mobileRequestDirect");
    }

    public static String h() {
        return c("/sms/smsDestination");
    }

    public static String i() {
        return c("/aisNew/mobileRequest3g");
    }

    public static String j() {
        return c("/aisNew/mobileRequestWifiOtp");
    }

    public static String k() {
        return c("/aisNew/mobileRequestWifi");
    }

    @Deprecated
    public static String l() {
        if (a.c == null) {
            return c("/service/queryTrans");
        }
        return b(a.c, "/service/queryTrans");
    }

    public static String m() {
        if (!TextUtils.isEmpty(a.e)) {
            return a.e;
        }
        return l();
    }

    public static String n() {
        return "http://203.151.93.97:9999/thaiCharge/service/queryTrans";
    }

    public static String o() {
        return c("/service/cashcard");
    }

    public static String a(String str, String str2) {
        String str3 = (String) a.i.get(c(str, str2));
        if (!TextUtils.isEmpty(str3)) {
            return b(str3, "/service/cashcard");
        }
        return c("/service/cashcard");
    }

    private static String c(String str, String str2) {
        if (Client.IS_DEBUG || str2.length() == 20) {
            return Config.COUNTRY_CN;
        }
        if (PublisherCode.PUBLISHER_12CALL.equals(str) || PublisherCode.PUBLISHER_TRUEMONEY.equals(str) || PublisherCode.PUBLISHER_HAPPY.equals(str) || PublisherCode.PUBLISHER_BLUECOIN.equals(str)) {
            return Config.COUNTRY_TH;
        }
        if (PublisherCode.PUBLISHER_UNIPIN.equals(str) || PublisherCode.PUBLISHER_MOGPLAY.equals(str) || PublisherCode.PUBLISHER_LYTOCARD.equals(str)) {
            return Config.COUNTRY_ID;
        }
        if (PublisherCode.PUBLISHER_MOBIFONE.equals(str) || PublisherCode.PUBLISHER_VINAPHONE.equals(str) || PublisherCode.PUBLISHER_VIETTEL.equals(str) || PublisherCode.PUBLISHER_VTC.equals(str) || PublisherCode.PUBLISHER_HOPE.equals(str)) {
            return Config.COUNTRY_VN;
        }
        return Config.COUNTRY_CN;
    }

    public static String p() {
        return c("/paysBySms/initPayment");
    }

    public static String q() {
        return "http://203.151.93.97:9999/thaiCharge/service/cashcard";
    }

    public static String r() {
        return "http://stat1.bluepay.asia/";
    }

    public static String s() {
        return c("/api/getOffer");
    }

    public static String a(int i, String str) {
        switch (i) {
            case 0:
                if (a.d != null && a.d.length() >= 3) {
                    return a.d;
                }
                if (str.equalsIgnoreCase("id") || str.equalsIgnoreCase(Config.LAN_ID2) || str.equalsIgnoreCase(Config.LAN_ID3) || str.equalsIgnoreCase(Config.LAN_ID4)) {
                    return "http://54.169.238.20:100/";
                }
                if (str.equalsIgnoreCase(Config.LAN_TH1) || str.equals(Config.LAN_TH2)) {
                    return "http://stat1.bluepay.asia/";
                }
                if (str.equalsIgnoreCase(Config.LAN_VN1) || str.equalsIgnoreCase(Config.LAN_VN2)) {
                    return "http://125.212.202.118:100/";
                }
                return "http://stat1.bluepay.asia/";
            case 1:
                return "http://stat1.bluepay.asia/";
            case 2:
                return "http://54.169.238.20:100/";
            case 3:
                return "http://125.212.202.118:100/";
            default:
                return "http://stat1.bluepay.asia/";
        }
    }

    public static String a(String str) {
        if (a.d != null && a.d.length() >= 3) {
            return a.d;
        }
        if (str.equalsIgnoreCase("id") || str.equalsIgnoreCase(Config.LAN_ID2) || str.equalsIgnoreCase(Config.LAN_ID3) || str.equalsIgnoreCase(Config.LAN_ID4)) {
            return "http://54.169.238.20:100/";
        }
        if (str.equalsIgnoreCase(Config.LAN_TH1) || str.equals(Config.LAN_TH2)) {
            return "http://stat1.bluepay.asia/";
        }
        if (str.equalsIgnoreCase(Config.LAN_VN1) || str.equalsIgnoreCase(Config.LAN_VN2)) {
            return "http://125.212.202.118:100/";
        }
        return "http://stat1.bluepay.asia/";
    }

    public static String t() {
        return "http://203.151.93.125:8089/blueAdServer/getOffer";
    }

    public static String u() {
        return "http://203.151.93.125:8089/blueAdServer/confirmInstal";
    }

    public static String v() {
        return "http://203.151.93.125:8089/blueAdServer/offerDetail";
    }

    public static String a(int i) {
        String str = new String();
        if (i == 3) {
            return a.c + "/thaiCharge/baiduPay/indosatPay";
        }
        if (i == 4) {
            return a.c + "/thaiCharge/baiduPay/smartFrenPay";
        }
        if (i == 7) {
            str = "/paysBySms/initPayment";
        } else if (i == 2) {
            return a.c + "";
        } else {
            if (i == 5) {
                return "http://203.151.93.97:9999" + "/thaiCharge/telenor/chargeByWifiPing";
            }
        }
        c.c("dcb charge url:" + str);
        return b(a.c, str);
    }

    public static String w() {
        String str = (String) a.i.get(Config.COUNTRY_ID);
        if (!TextUtils.isEmpty(str)) {
            return b(str, "/payByOffline/doPayByOffline");
        }
        return c("/payByOffline/doPayByOffline");
    }

    public static String b(int i) {
        if (i == 5) {
            return "http://203.151.93.97:9999/thaiCharge/telenor/getWifiPing";
        }
        return "";
    }

    public static String c(int i) {
        if (i == 1) {
            return a.f + "/userCenter/sendVerificationCode";
        }
        if (i == 2) {
            return a.f + "/userCenter/verificationCodeValidate";
        }
        return "";
    }

    public static String b(String str) {
        if (str.equals(PublisherCode.PUBLISHER_LINE)) {
            return "http://api.bluepay.asia/charge/linepay/linePayRequest";
        }
        return "";
    }

    public static String x() {
        return Config.getIp() + "/thaiCharge/service/getMsisdnByImsi";
    }

    public static String y() {
        if (a.d != null) {
            return a.d;
        }
        return r();
    }
}
