package com.bluepay.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import com.bluepay.a.b.a;
import com.bluepay.data.Config;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.data.i;
import com.bluepay.pay.Client;
import com.bluepay.sdk.b.c;
import com.bluepay.sdk.c.aa;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: Proguard */
public class SmsReceiver extends BroadcastReceiver {
    private static final String d = "android.provider.Telephony.SMS_RECEIVED";
    d a;
    boolean b;
    boolean c;
    /* access modifiers changed from: private */
    public b e;

    public SmsReceiver(b billing) {
        this.e = billing;
        c.c("statrt to listen the sms and start the countddown timer");
    }

    public void a() {
        this.e.getActivity().registerReceiver(this, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
        c.c("register receiver");
        this.e.getActivity().runOnUiThread(new a(this));
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            Object[] objArr = (Object[]) intent.getExtras().get("pdus");
            c.c("pdus length" + (objArr == null ? 0 : objArr.length));
            int length = objArr.length;
            int i = 0;
            while (i < length) {
                SmsMessage createFromPdu = SmsMessage.createFromPdu((byte[]) objArr[i]);
                String originatingAddress = createFromPdu.getOriginatingAddress();
                String messageBody = createFromPdu.getMessageBody();
                String e2 = aa.e(originatingAddress);
                c.c("content:" + messageBody);
                c.c("sender:" + e2);
                try {
                    if (!TextUtils.isEmpty(e2) && e2.equals(Client.m_DcbInfo.n)) {
                        this.c = true;
                        for (int i2 = 0; i2 < Client.m_DcbInfo.m.length; i2++) {
                            if (b(Client.m_DcbInfo.m[i2]) != null) {
                                String a2 = a(messageBody, Client.m_DcbInfo.m[i2]);
                                c.c("code:" + a2);
                                if (a2 != null) {
                                    this.e.d(a2);
                                    this.e.c(e2);
                                    this.e.a(0);
                                    a.j.add(this.e);
                                    a.o.a(5, this.e.a(), 0, this.e);
                                    this.e.getActivity().runOnUiThread(new b(this));
                                    if (!this.b) {
                                        a((BroadcastReceiver) this);
                                        return;
                                    }
                                    return;
                                }
                                this.e.desc = i.a(i.ad);
                                aa.b(this.e.getActivity(), this.e.getTransactionId() + "|can not match|" + this.e.getCPPayType(), "", Client.m_iIMSI, 13);
                                this.e.getActivity().runOnUiThread(new c(this));
                                a.o.a(14, h.b, 0, this.e);
                                if (!this.b) {
                                    a((BroadcastReceiver) this);
                                    return;
                                }
                                return;
                            }
                        }
                    }
                    if (!this.b) {
                        a((BroadcastReceiver) this);
                    }
                    i++;
                } catch (Exception e3) {
                    e3.printStackTrace();
                    this.e.desc = i.a(i.ad);
                    a.o.a(14, h.b, 0, this.e);
                    if (!this.b) {
                        a((BroadcastReceiver) this);
                        return;
                    }
                    return;
                } catch (Throwable th) {
                    if (!this.b) {
                        a((BroadcastReceiver) this);
                    }
                    throw th;
                }
            }
        }
    }

    public void a(BroadcastReceiver broadcastReceiver) {
        try {
            c.c("unregister receiveer");
            this.b = true;
            this.a.cancel();
            this.a = null;
            this.e.getActivity().unregisterReceiver(broadcastReceiver);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public String a(String str, String str2) {
        Matcher matcher = Pattern.compile(a(str2)).matcher(str);
        if (!matcher.find()) {
            c.b("message did not contains the opt");
            return null;
        } else if (Client.telcoName.equals(Config.TELCO_NAME_XL)) {
            return c(matcher.group(), Integer.parseInt(Client.m_DcbInfo.o));
        } else {
            String group = matcher.group();
            c.c(group);
            return group;
        }
    }

    private String a(String str) {
        c.c("getRegEx：" + str);
        String str2 = null;
        if (Client.telcoName.equals(Config.TELCO_NAME_INDOSAT)) {
            str2 = d(b(str)[1], Integer.parseInt(Client.m_DcbInfo.o));
        } else if (Client.telcoName.equals(Config.TELCO_NAME_XL)) {
            str2 = a(str, Integer.parseInt(Client.m_DcbInfo.o));
        } else if (Client.telcoName.equals(Config.TELCO_NAME_HUTCHISON)) {
            str2 = b(b(str)[1], Integer.parseInt(Client.m_DcbInfo.o));
        }
        c.c(str2);
        return str2;
    }

    private String a(String str, int i) {
        String[] b2 = b(str);
        return "\\d{" + i + "}" + b2[b2.length - 1];
    }

    private String b(String str, int i) {
        String str2 = str + " ";
        return str2 + "\\d{" + (i - str2.length()) + "}";
    }

    private String c(String str, int i) {
        Matcher matcher = Pattern.compile("\\d{" + i + "}").matcher(str);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    private String[] b(String str) {
        if (str == null || !str.contains("_")) {
            return null;
        }
        return str.split("_");
    }

    private String d(String str, int i) {
        String str2 = str + " ";
        return str2 + "\\d{" + (i - str2.length()) + "}";
    }
}
