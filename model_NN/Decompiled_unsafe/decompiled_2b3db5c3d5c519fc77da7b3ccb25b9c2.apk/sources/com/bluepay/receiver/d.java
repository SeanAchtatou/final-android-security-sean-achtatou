package com.bluepay.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.CountDownTimer;
import com.bluepay.a.b.a;
import com.bluepay.data.h;
import com.bluepay.data.i;
import com.bluepay.sdk.b.c;

/* compiled from: Proguard */
class d extends CountDownTimer {
    final /* synthetic */ SmsReceiver a;
    private BroadcastReceiver b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(SmsReceiver smsReceiver, long j, long j2) {
        super(j, j2);
        this.a = smsReceiver;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(SmsReceiver smsReceiver, long j, long j2, Context context, BroadcastReceiver broadcastReceiver) {
        super(j, j2);
        this.a = smsReceiver;
        this.b = broadcastReceiver;
    }

    public void onTick(long millisUntilFinished) {
        c.c("tiker:" + millisUntilFinished);
    }

    public void onFinish() {
        c.c("timer finish");
        if (!this.a.b && this.b != null) {
            this.a.a(this.b);
        }
        this.a.e.desc = i.a(i.ad);
        this.a.e.getActivity().runOnUiThread(new e(this));
        a.o.a(14, h.b, 0, this.a.e);
    }
}
