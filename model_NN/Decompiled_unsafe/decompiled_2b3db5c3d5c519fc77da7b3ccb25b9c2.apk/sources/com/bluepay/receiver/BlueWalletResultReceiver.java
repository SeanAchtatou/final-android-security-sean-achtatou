package com.bluepay.receiver;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import com.bluepay.a.b.au;
import com.bluepay.a.b.aw;
import com.bluepay.data.b;
import com.bluepay.data.h;
import com.bluepay.interfaceClass.c;
import com.bluepay.pay.PublisherCode;
import com.bluepay.sdk.c.aa;
import com.bluepay.sdk.c.x;
import com.bluepay.sdk.exception.BlueException;

/* compiled from: Proguard */
public class BlueWalletResultReceiver extends BroadcastReceiver {
    private b a;
    private boolean b = false;
    private c c;

    public BlueWalletResultReceiver(b billing, c callback) {
        this.a = billing;
        this.c = callback;
        this.a.getActivity().registerReceiver(this, new IntentFilter(this.a.getScheme()));
    }

    public void onReceive(Context context, Intent receiveIntent) {
        com.bluepay.sdk.b.c.c(receiveIntent.getAction());
        if (receiveIntent.getAction().equals(this.a.getScheme())) {
            try {
                String stringExtra = receiveIntent.getStringExtra("msg");
                com.bluepay.sdk.b.c.c(stringExtra);
                aw b2 = au.b(stringExtra);
                if (x.a(b2) && this.a.getTransactionId().equals(b2.b("tid"))) {
                    String e = x.e(b2.b("status"));
                    if (aa.f(e)) {
                        int parseInt = Integer.parseInt(e);
                        Intent intent = new Intent();
                        intent.setAction("android.intent.action.VIEW");
                        intent.addFlags(268435456);
                        intent.setData(Uri.parse(this.a.getScheme()));
                        intent.putExtra("status", parseInt);
                        intent.putExtra("transactionId", this.a.getTransactionId());
                        context.startActivity(intent);
                        this.c.a(14, parseInt, 0, this.a);
                    }
                }
            } catch (BlueException e2) {
                e2.printStackTrace();
            } catch (ActivityNotFoundException e3) {
                aa.a(this.a.getActivity(), this.a.getTransactionId(), this.a.getPrice() + "", h.i, PublisherCode.PUBLISHER_SMS, "Activity not found!");
            } catch (Throwable th) {
                if (!this.b) {
                    context.unregisterReceiver(this);
                    this.b = true;
                }
                throw th;
            }
            if (!this.b) {
                context.unregisterReceiver(this);
                this.b = true;
            }
        }
    }
}
