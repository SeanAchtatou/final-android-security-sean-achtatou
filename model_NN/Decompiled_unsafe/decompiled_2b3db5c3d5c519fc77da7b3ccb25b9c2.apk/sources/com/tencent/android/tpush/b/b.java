package com.tencent.android.tpush.b;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import com.facebook.internal.ServerProtocol;
import com.jg.EType;
import com.jg.JgMethodChecked;
import com.tencent.android.tpush.XGBasicPushNotificationBuilder;
import com.tencent.android.tpush.XGCustomPushNotificationBuilder;
import com.tencent.android.tpush.XGNotifaction;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.XGPushManager;
import com.tencent.android.tpush.XGPushNotifactionCallback;
import com.tencent.android.tpush.XGPushNotificationBuilder;
import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.common.MessageKey;
import com.tencent.android.tpush.common.e;
import com.tencent.android.tpush.common.m;
import com.tencent.android.tpush.common.p;
import com.tencent.android.tpush.encrypt.Rijndael;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class b {
    private static String a(int i) {
        return "TPUSH_NOTIF_BUILDID_" + String.valueOf(i);
    }

    public static synchronized XGPushNotificationBuilder a(Context context) {
        XGPushNotificationBuilder flags;
        synchronized (b.class) {
            flags = new XGBasicPushNotificationBuilder().setFlags(16);
        }
        return flags;
    }

    public static void a(Context context, int i, XGPushNotificationBuilder xGPushNotificationBuilder) {
        String a = a(i);
        JSONObject jSONObject = new JSONObject();
        xGPushNotificationBuilder.encode(jSONObject);
        JSONObject jSONObject2 = new JSONObject();
        e.a(jSONObject2, xGPushNotificationBuilder.getType(), jSONObject.toString());
        m.b(context, a, jSONObject2.toString());
    }

    public static XGPushNotificationBuilder a(Context context, int i) {
        String a;
        String string;
        XGBasicPushNotificationBuilder xGBasicPushNotificationBuilder = null;
        if (!(context == null || (a = m.a(context, a(i), (String) null)) == null)) {
            try {
                JSONObject jSONObject = new JSONObject(a);
                if (jSONObject.has(XGPushNotificationBuilder.BASIC_NOTIFICATION_BUILDER_TYPE)) {
                    XGBasicPushNotificationBuilder xGBasicPushNotificationBuilder2 = new XGBasicPushNotificationBuilder();
                    try {
                        xGBasicPushNotificationBuilder = xGBasicPushNotificationBuilder2;
                        string = jSONObject.getString(XGPushNotificationBuilder.BASIC_NOTIFICATION_BUILDER_TYPE);
                    } catch (JSONException e) {
                        JSONException jSONException = e;
                        xGBasicPushNotificationBuilder = xGBasicPushNotificationBuilder2;
                        e = jSONException;
                        a.c(Constants.LogTag, "", e);
                        return xGBasicPushNotificationBuilder;
                    }
                } else if (jSONObject.has(XGPushNotificationBuilder.CUSTOM_NOTIFICATION_BUILDER_TYPE)) {
                    XGCustomPushNotificationBuilder xGCustomPushNotificationBuilder = new XGCustomPushNotificationBuilder();
                    try {
                        xGBasicPushNotificationBuilder = xGCustomPushNotificationBuilder;
                        string = jSONObject.getString(XGPushNotificationBuilder.CUSTOM_NOTIFICATION_BUILDER_TYPE);
                    } catch (JSONException e2) {
                        JSONException jSONException2 = e2;
                        xGBasicPushNotificationBuilder = xGCustomPushNotificationBuilder;
                        e = jSONException2;
                        a.c(Constants.LogTag, "", e);
                        return xGBasicPushNotificationBuilder;
                    }
                }
                xGBasicPushNotificationBuilder.decode(string);
            } catch (JSONException e3) {
                e = e3;
            }
        }
        return xGBasicPushNotificationBuilder;
    }

    public static String b(Context context) {
        try {
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            intent.setPackage(context.getPackageName());
            for (ResolveInfo next : context.getPackageManager().queryIntentActivities(intent, 0)) {
                if (next.activityInfo != null) {
                    return next.activityInfo.name;
                }
            }
        } catch (Throwable th) {
            a.c("MessageHelper", "get Activity error", th);
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.content.Intent a(android.content.Context r5, com.tencent.android.tpush.b.e r6, boolean r7) {
        /*
            r4 = 1
            r0 = 0
            int r1 = r6.a
            switch(r1) {
                case 1: goto L_0x002b;
                case 2: goto L_0x0078;
                case 3: goto L_0x009d;
                case 4: goto L_0x00c3;
                default: goto L_0x0007;
            }
        L_0x0007:
            java.lang.String r1 = "MessageHelper"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "unkown type"
            java.lang.StringBuilder r2 = r2.append(r3)
            int r3 = r6.a
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.tencent.android.tpush.a.a.h(r1, r2)
        L_0x0021:
            if (r0 == 0) goto L_0x002a
            java.lang.String r1 = "action_confirm"
            int r2 = r6.g
            r0.putExtra(r1, r2)
        L_0x002a:
            return r0
        L_0x002b:
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r0 = "com.tencent.android.tpush.action.INTERNAL_PUSH_MESSAGE"
            r2.<init>(r0)
            java.lang.String r0 = r6.b
            boolean r1 = com.tencent.android.tpush.common.p.b(r0)
            if (r1 == 0) goto L_0x003e
            java.lang.String r0 = b(r5)
        L_0x003e:
            r1 = 538968064(0x20200000, float:1.3552527E-19)
            com.tencent.android.tpush.b.f r3 = r6.c
            if (r3 == 0) goto L_0x004a
            com.tencent.android.tpush.b.f r3 = r6.c
            int r3 = r3.a
            if (r3 > 0) goto L_0x0070
        L_0x004a:
            if (r7 == 0) goto L_0x004e
            r1 = 268435456(0x10000000, float:2.5243549E-29)
        L_0x004e:
            r2.addFlags(r1)
            r1 = 67239936(0x4020000, float:1.5281427E-36)
            r2.setFlags(r1)
        L_0x0056:
            java.lang.String r1 = "activity"
            r2.putExtra(r1, r0)
            java.lang.String r0 = "notificationActionType"
            r2.putExtra(r0, r4)
            java.lang.String r0 = "action_type"
            r2.putExtra(r0, r4)
            android.content.Context r0 = r5.getApplicationContext()
            java.lang.Class<com.tencent.android.tpush.XGPushActivity> r1 = com.tencent.android.tpush.XGPushActivity.class
            r2.setClass(r0, r1)
            r0 = r2
            goto L_0x0021
        L_0x0070:
            com.tencent.android.tpush.b.f r1 = r6.c
            int r1 = r1.a
            r2.setFlags(r1)
            goto L_0x0056
        L_0x0078:
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "com.tencent.android.tpush.action.INTERNAL_PUSH_MESSAGE"
            r0.<init>(r1)
            java.lang.String r1 = "activity"
            java.lang.String r2 = r6.f
            r0.putExtra(r1, r2)
            java.lang.String r1 = "action_type"
            int r2 = r6.a
            r0.putExtra(r1, r2)
            java.lang.String r1 = "notificationActionType"
            r2 = 2
            r0.putExtra(r1, r2)
            android.content.Context r1 = r5.getApplicationContext()
            java.lang.Class<com.tencent.android.tpush.XGPushActivity> r2 = com.tencent.android.tpush.XGPushActivity.class
            r0.setClass(r1, r2)
            goto L_0x0021
        L_0x009d:
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "com.tencent.android.tpush.action.INTERNAL_PUSH_MESSAGE"
            r0.<init>(r1)
            java.lang.String r1 = "activity"
            java.lang.String r2 = r6.d
            r0.putExtra(r1, r2)
            java.lang.String r1 = "action_type"
            int r2 = r6.a
            r0.putExtra(r1, r2)
            java.lang.String r1 = "notificationActionType"
            r2 = 3
            r0.putExtra(r1, r2)
            android.content.Context r1 = r5.getApplicationContext()
            java.lang.Class<com.tencent.android.tpush.XGPushActivity> r2 = com.tencent.android.tpush.XGPushActivity.class
            r0.setClass(r1, r2)
            goto L_0x0021
        L_0x00c3:
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "com.tencent.android.tpush.action.INTERNAL_PUSH_MESSAGE"
            r0.<init>(r1)
            java.lang.String r1 = r6.h
            boolean r2 = com.tencent.android.tpush.common.p.b(r1)
            if (r2 != 0) goto L_0x002a
            java.lang.String r2 = "action_type"
            int r3 = r6.a
            r0.putExtra(r2, r3)
            java.lang.String r2 = "packageDownloadUrl"
            java.lang.String r3 = r6.j
            r0.putExtra(r2, r3)
            java.lang.String r2 = "packageName"
            r0.putExtra(r2, r1)
            java.lang.String r2 = "activity"
            r0.putExtra(r2, r1)
            java.lang.String r1 = "notificationActionType"
            r2 = 4
            r0.putExtra(r1, r2)
            android.content.Context r1 = r5.getApplicationContext()
            java.lang.Class<com.tencent.android.tpush.XGPushActivity> r2 = com.tencent.android.tpush.XGPushActivity.class
            r0.setClass(r1, r2)
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.b.b.a(android.content.Context, com.tencent.android.tpush.b.e, boolean):android.content.Intent");
    }

    @JgMethodChecked(author = 1, fComment = "确认已进行安全校验", lastDate = "20150316", reviewer = 3, vComment = {EType.RECEIVERCHECK, EType.INTENTCHECK})
    public static void a(Context context, i iVar) {
        boolean z;
        int i;
        Integer num = null;
        d dVar = (d) iVar.g();
        e m = dVar.m();
        XGPushNotificationBuilder a = a(context, dVar.h());
        if (a == null || dVar.t() == 1) {
            if (a == null) {
                a = XGPushManager.getDefaultNotificationBuilder(context);
            }
            if (a == null) {
                a = a(context);
            }
            if (dVar.k() != 0) {
                a.setFlags(16);
            } else {
                a.setFlags(32);
            }
            if (dVar.i() == 0) {
                a.setSound(null);
            } else if (!TextUtils.isEmpty(dVar.p())) {
                int identifier = context.getResources().getIdentifier(dVar.p(), "raw", context.getPackageName());
                if (identifier > 0) {
                    a.setSound(Uri.parse("android.resource://" + context.getPackageName() + "/" + identifier));
                } else {
                    a.setDefaults(1);
                }
            } else {
                a.setDefaults(1);
            }
            if (dVar.j() != 0) {
                a.setDefaults(2);
            } else {
                a.setVibrate(null);
            }
            if (dVar.o() != 0) {
                a.setFlags(1);
            }
            String r = dVar.r();
            if (r == null || TextUtils.isEmpty(r)) {
                a.setSmallIcon(Integer.valueOf(context.getApplicationInfo().icon));
            } else {
                int identifier2 = context.getResources().getIdentifier(r, "drawable", context.getPackageName());
                if (identifier2 > 0) {
                    a.setSmallIcon(Integer.valueOf(identifier2));
                } else {
                    a.setSmallIcon(Integer.valueOf(context.getApplicationInfo().icon));
                }
            }
            int s = dVar.s();
            String q = dVar.q();
            if (a instanceof XGCustomPushNotificationBuilder) {
                num = ((XGCustomPushNotificationBuilder) a).getLayoutIconId();
            }
            if (q == null || TextUtils.isEmpty(q)) {
                a.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), context.getApplicationInfo().icon));
            } else if (s <= 0) {
                int identifier3 = context.getResources().getIdentifier(q, "drawable", context.getPackageName());
                if (identifier3 > 0) {
                    a.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), identifier3));
                    if (num != null) {
                        ((XGCustomPushNotificationBuilder) a).setLayoutIconDrawableId(identifier3);
                    }
                } else {
                    a.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), context.getApplicationInfo().icon));
                }
            } else {
                a(q, a, context, num);
            }
        }
        if (dVar.n() > 0) {
            a.setIcon(Integer.valueOf(dVar.n()));
        }
        if (a.getSmallIcon() == null && a.getLargeIcon() == null && a.getIcon() == null) {
            a.setSmallIcon(Integer.valueOf(context.getApplicationInfo().icon));
            a.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), context.getApplicationInfo().icon));
        }
        a.setTitle(dVar.e());
        a.setTickerText(dVar.f());
        String g = dVar.g();
        if (p.b(g) || "{}".equalsIgnoreCase(g)) {
            z = false;
        } else {
            z = true;
        }
        Intent a2 = a(context, m, z);
        if (a2 == null) {
            a.h("MessageHelper", "intent is null");
            return;
        }
        if (z) {
            a2.putExtra("custom_content", dVar.g());
        }
        a2.putExtra(Constants.TAG_TPUSH_MESSAGE, ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
        a2.putExtra("title", Rijndael.encrypt(dVar.e()));
        a2.putExtra(MessageKey.MSG_CONTENT, Rijndael.encrypt(dVar.f()));
        if (dVar.g() != null) {
            a2.putExtra("custom_content", Rijndael.encrypt(dVar.g()));
        }
        a2.putExtra(MessageKey.MSG_ID, iVar.b());
        a2.putExtra("accId", iVar.c());
        a2.putExtra(MessageKey.MSG_BUSI_MSG_ID, iVar.d());
        a2.putExtra(MessageKey.MSG_CREATE_TIMESTAMPS, iVar.e());
        a2.putExtra(MessageKey.MSG_PORTECT_TAG, Rijndael.encrypt("" + (System.currentTimeMillis() - 1000)));
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        int l = dVar.l();
        if (l <= 0) {
            i = b(context, dVar.h());
        } else {
            i = l;
        }
        if (i == -1) {
            notificationManager.cancelAll();
        }
        a2.putExtra(MessageKey.NOTIFACTION_ID, i);
        int i2 = 134217728;
        if (m.c != null && m.c.b > 0) {
            i2 = m.c.b;
        }
        Intent intent = new Intent("com.tencent.android.tpush.action.PUSH_CANCELLED.RESULT");
        intent.putExtra(Constants.FLAG_PACK_NAME, context.getPackageName());
        intent.putExtra("action", 2);
        intent.putExtras(a2);
        if (Build.VERSION.SDK_INT == 19) {
            PendingIntent.getActivity(context.getApplicationContext(), i, a2, i2).cancel();
        }
        a.setContentIntent(PendingIntent.getActivity(context.getApplicationContext(), i, a2, i2));
        Notification buildNotification = a.buildNotification(context);
        buildNotification.deleteIntent = PendingIntent.getBroadcast(context.getApplicationContext(), i, intent, i2);
        XGPushNotifactionCallback notifactionCallback = XGPushManager.getNotifactionCallback();
        if (notifactionCallback == null) {
            notificationManager.notify(i, buildNotification);
        } else {
            a.d("MessageHelper", "call notifactionCallback:" + buildNotification);
            notifactionCallback.handleNotify(new XGNotifaction(context, i, buildNotification, dVar));
        }
        Intent intent2 = new Intent(Constants.ACTION_FEEDBACK);
        intent2.putExtra(Constants.FEEDBACK_ERROR_CODE, 0);
        intent2.setPackage(context.getPackageName());
        intent2.putExtras(a2);
        intent2.putExtra(Constants.FEEDBACK_TAG, 5);
        intent2.putExtra(MessageKey.NOTIFACTION_ID, i);
        context.sendBroadcast(intent2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:106:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00c5 A[SYNTHETIC, Splitter:B:39:0x00c5] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00ca A[Catch:{ IOException -> 0x00e1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00cf A[Catch:{ IOException -> 0x00e1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00d4 A[Catch:{ IOException -> 0x00e1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00d9 A[Catch:{ IOException -> 0x00e1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0129 A[SYNTHETIC, Splitter:B:71:0x0129] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x012e A[Catch:{ IOException -> 0x0145 }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0133 A[Catch:{ IOException -> 0x0145 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0138 A[Catch:{ IOException -> 0x0145 }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x013d A[Catch:{ IOException -> 0x0145 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(java.lang.String r10, com.tencent.android.tpush.XGPushNotificationBuilder r11, android.content.Context r12, java.lang.Integer r13) {
        /*
            r3 = 4000(0xfa0, float:5.605E-42)
            r2 = 3000(0xbb8, float:4.204E-42)
            r1 = 0
            org.apache.http.params.BasicHttpParams r0 = new org.apache.http.params.BasicHttpParams
            r0.<init>()
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r0, r2)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r0, r2)
            org.apache.http.impl.client.DefaultHttpClient r6 = new org.apache.http.impl.client.DefaultHttpClient
            r6.<init>(r0)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r0, r3)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r0, r3)
            r2 = 4000(0xfa0, double:1.9763E-320)
            org.apache.http.conn.params.ConnManagerParams.setTimeout(r0, r2)
            r0 = 0
            r2 = 0
            r3 = 0
            java.net.URL r4 = new java.net.URL     // Catch:{ Exception -> 0x015f, all -> 0x0123 }
            r4.<init>(r10)     // Catch:{ Exception -> 0x015f, all -> 0x0123 }
            org.apache.http.client.methods.HttpGet r5 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x015f, all -> 0x0123 }
            java.net.URI r7 = r4.toURI()     // Catch:{ Exception -> 0x015f, all -> 0x0123 }
            r5.<init>(r7)     // Catch:{ Exception -> 0x015f, all -> 0x0123 }
            java.lang.String r7 = "X-Online-Host"
            java.lang.String r4 = r4.getHost()     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            r5.addHeader(r7, r4)     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            org.apache.http.params.HttpParams r4 = r6.getParams()     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            java.lang.String r7 = "http.socket.timeout"
            r8 = 20000(0x4e20, float:2.8026E-41)
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            r4.setParameter(r7, r8)     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            org.apache.http.params.HttpParams r4 = r6.getParams()     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            java.lang.String r7 = "http.connection.timeout"
            r8 = 20000(0x4e20, float:2.8026E-41)
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            r4.setParameter(r7, r8)     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            org.apache.http.HttpResponse r4 = r6.execute(r5)     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            org.apache.http.StatusLine r7 = r4.getStatusLine()     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            int r7 = r7.getStatusCode()     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            r8 = 200(0xc8, float:2.8E-43)
            if (r7 == r8) goto L_0x009c
            android.content.res.Resources r4 = r12.getResources()     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            android.content.pm.ApplicationInfo r7 = r12.getApplicationInfo()     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            int r7 = r7.icon     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeResource(r4, r7)     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            r11.setLargeIcon(r4)     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            if (r1 == 0) goto L_0x007e
            r3.consumeContent()     // Catch:{ IOException -> 0x0097 }
        L_0x007e:
            if (r1 == 0) goto L_0x0083
            r0.close()     // Catch:{ IOException -> 0x0097 }
        L_0x0083:
            if (r1 == 0) goto L_0x0088
            r2.close()     // Catch:{ IOException -> 0x0097 }
        L_0x0088:
            if (r5 == 0) goto L_0x008d
            r5.abort()     // Catch:{ IOException -> 0x0097 }
        L_0x008d:
            if (r6 == 0) goto L_0x0096
            org.apache.http.conn.ClientConnectionManager r0 = r6.getConnectionManager()     // Catch:{ IOException -> 0x0097 }
            r0.shutdown()     // Catch:{ IOException -> 0x0097 }
        L_0x0096:
            return
        L_0x0097:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0096
        L_0x009c:
            org.apache.http.HttpEntity r2 = r4.getEntity()     // Catch:{ Exception -> 0x0165, all -> 0x014a }
            if (r2 == 0) goto L_0x017b
            java.io.InputStream r4 = r2.getContent()     // Catch:{ Exception -> 0x016b, all -> 0x014e }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0173, all -> 0x0153 }
            r3.<init>()     // Catch:{ Exception -> 0x0173, all -> 0x0153 }
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x00bb, all -> 0x0157 }
        L_0x00af:
            int r1 = r4.read(r0)     // Catch:{ Exception -> 0x00bb, all -> 0x0157 }
            r7 = -1
            if (r1 == r7) goto L_0x00e6
            r7 = 0
            r3.write(r0, r7, r1)     // Catch:{ Exception -> 0x00bb, all -> 0x0157 }
            goto L_0x00af
        L_0x00bb:
            r0 = move-exception
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r5
        L_0x00c0:
            r0.printStackTrace()     // Catch:{ all -> 0x015a }
            if (r1 == 0) goto L_0x00c8
            r1.consumeContent()     // Catch:{ IOException -> 0x00e1 }
        L_0x00c8:
            if (r3 == 0) goto L_0x00cd
            r3.close()     // Catch:{ IOException -> 0x00e1 }
        L_0x00cd:
            if (r2 == 0) goto L_0x00d2
            r2.close()     // Catch:{ IOException -> 0x00e1 }
        L_0x00d2:
            if (r4 == 0) goto L_0x00d7
            r4.abort()     // Catch:{ IOException -> 0x00e1 }
        L_0x00d7:
            if (r6 == 0) goto L_0x0096
            org.apache.http.conn.ClientConnectionManager r0 = r6.getConnectionManager()     // Catch:{ IOException -> 0x00e1 }
            r0.shutdown()     // Catch:{ IOException -> 0x00e1 }
            goto L_0x0096
        L_0x00e1:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0096
        L_0x00e6:
            byte[] r0 = r3.toByteArray()     // Catch:{ Exception -> 0x00bb, all -> 0x0157 }
            r1 = 0
            byte[] r7 = r3.toByteArray()     // Catch:{ Exception -> 0x00bb, all -> 0x0157 }
            int r7 = r7.length     // Catch:{ Exception -> 0x00bb, all -> 0x0157 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeByteArray(r0, r1, r7)     // Catch:{ Exception -> 0x00bb, all -> 0x0157 }
            r11.setLargeIcon(r0)     // Catch:{ Exception -> 0x00bb, all -> 0x0157 }
            if (r13 == 0) goto L_0x00fe
            com.tencent.android.tpush.XGCustomPushNotificationBuilder r11 = (com.tencent.android.tpush.XGCustomPushNotificationBuilder) r11     // Catch:{ Exception -> 0x00bb, all -> 0x0157 }
            r11.setLayoutIconDrawableBmp(r0)     // Catch:{ Exception -> 0x00bb, all -> 0x0157 }
        L_0x00fe:
            if (r2 == 0) goto L_0x0103
            r2.consumeContent()     // Catch:{ IOException -> 0x011d }
        L_0x0103:
            if (r4 == 0) goto L_0x0108
            r4.close()     // Catch:{ IOException -> 0x011d }
        L_0x0108:
            if (r3 == 0) goto L_0x010d
            r3.close()     // Catch:{ IOException -> 0x011d }
        L_0x010d:
            if (r5 == 0) goto L_0x0112
            r5.abort()     // Catch:{ IOException -> 0x011d }
        L_0x0112:
            if (r6 == 0) goto L_0x0096
            org.apache.http.conn.ClientConnectionManager r0 = r6.getConnectionManager()     // Catch:{ IOException -> 0x011d }
            r0.shutdown()     // Catch:{ IOException -> 0x011d }
            goto L_0x0096
        L_0x011d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0096
        L_0x0123:
            r0 = move-exception
            r3 = r1
            r4 = r1
            r5 = r1
        L_0x0127:
            if (r1 == 0) goto L_0x012c
            r1.consumeContent()     // Catch:{ IOException -> 0x0145 }
        L_0x012c:
            if (r4 == 0) goto L_0x0131
            r4.close()     // Catch:{ IOException -> 0x0145 }
        L_0x0131:
            if (r3 == 0) goto L_0x0136
            r3.close()     // Catch:{ IOException -> 0x0145 }
        L_0x0136:
            if (r5 == 0) goto L_0x013b
            r5.abort()     // Catch:{ IOException -> 0x0145 }
        L_0x013b:
            if (r6 == 0) goto L_0x0144
            org.apache.http.conn.ClientConnectionManager r1 = r6.getConnectionManager()     // Catch:{ IOException -> 0x0145 }
            r1.shutdown()     // Catch:{ IOException -> 0x0145 }
        L_0x0144:
            throw r0
        L_0x0145:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0144
        L_0x014a:
            r0 = move-exception
            r3 = r1
            r4 = r1
            goto L_0x0127
        L_0x014e:
            r0 = move-exception
            r3 = r1
            r4 = r1
            r1 = r2
            goto L_0x0127
        L_0x0153:
            r0 = move-exception
            r3 = r1
            r1 = r2
            goto L_0x0127
        L_0x0157:
            r0 = move-exception
            r1 = r2
            goto L_0x0127
        L_0x015a:
            r0 = move-exception
            r5 = r4
            r4 = r3
            r3 = r2
            goto L_0x0127
        L_0x015f:
            r0 = move-exception
            r2 = r1
            r3 = r1
            r4 = r1
            goto L_0x00c0
        L_0x0165:
            r0 = move-exception
            r2 = r1
            r3 = r1
            r4 = r5
            goto L_0x00c0
        L_0x016b:
            r0 = move-exception
            r3 = r1
            r4 = r5
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x00c0
        L_0x0173:
            r0 = move-exception
            r3 = r4
            r4 = r5
            r9 = r2
            r2 = r1
            r1 = r9
            goto L_0x00c0
        L_0x017b:
            r3 = r1
            r4 = r1
            goto L_0x00fe
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.b.b.a(java.lang.String, com.tencent.android.tpush.XGPushNotificationBuilder, android.content.Context, java.lang.Integer):void");
    }

    public static void b(Context context, i iVar) {
        if (iVar.g() instanceof d) {
            if (XGPushConfig.enableDebug) {
                a.e("MessageHelper", "Action -> showNotification " + iVar.f());
            }
            d dVar = (d) iVar.g();
            if (dVar == null || dVar.m() == null) {
                a.h("MessageHelper", "showNotification holder == null || holder.getAction() == null");
            } else {
                a(context, iVar);
            }
        }
    }

    private static synchronized int b(Context context, int i) {
        Throwable th;
        int i2;
        synchronized (b.class) {
            try {
                String str = "_XINGE_NOTIF_NUMBER_" + String.valueOf(i);
                i2 = m.a(context, str, 0);
                if (i2 >= 2147483646) {
                    i2 = 0;
                }
                try {
                    m.b(context, str, i2 + 1);
                } catch (Throwable th2) {
                    th = th2;
                    a.c("MessageHelper", "", th);
                    return i2;
                }
            } catch (Throwable th3) {
                Throwable th4 = th3;
                i2 = 0;
                th = th4;
                a.c("MessageHelper", "", th);
                return i2;
            }
        }
        return i2;
    }
}
