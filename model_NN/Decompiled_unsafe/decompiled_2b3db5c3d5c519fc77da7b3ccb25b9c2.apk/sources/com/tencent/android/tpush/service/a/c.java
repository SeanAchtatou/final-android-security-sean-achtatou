package com.tencent.android.tpush.service.a;

import android.content.Context;
import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.service.d.e;

/* compiled from: ProGuard */
public class c {
    public static void a(Context context, d dVar) {
        if (context != null) {
            d a = a(context);
            String packageName = context.getPackageName();
            if (a.a != dVar.a || a.b != dVar.b) {
                try {
                    e.a(context, packageName + ".com.tencent.tpush.cache.ver", dVar.a);
                    e.a(context, packageName + ".com.tencent.tpush.cache.pri", dVar.b);
                } catch (Throwable th) {
                    a.c(Constants.ServiceLogTag, "setSetting", th);
                }
            }
        } else {
            a.h(Constants.ServiceLogTag, ">> context is null");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.d.e.b(android.content.Context, java.lang.String, float):float
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.service.d.e.b(android.content.Context, java.lang.String, int):int
      com.tencent.android.tpush.service.d.e.b(android.content.Context, java.lang.String, long):boolean
      com.tencent.android.tpush.service.d.e.b(android.content.Context, java.lang.String, float):float */
    public static d a(Context context) {
        if (context != null) {
            String packageName = context.getPackageName();
            return new d(e.b(context, packageName + ".com.tencent.tpush.cache.ver", 0.0f), e.b(context, packageName + ".com.tencent.tpush.cache.pri", 0));
        }
        a.h(Constants.LogTag, ">>> get version and priority from Settings error");
        return new d(0.0f, 0);
    }

    public static d a(Context context, String str) {
        if (context == null) {
            return null;
        }
        try {
            return a(context.createPackageContext(str, 2));
        } catch (Exception e) {
            a.c(Constants.LogTag, "Create package context exception:" + str, e);
            return null;
        }
    }
}
