package com.tencent.android.tpush.common;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.PowerManager;
import com.tencent.android.tpush.XGPush4Msdk;
import com.tencent.android.tpush.XGPushActivity;
import com.tencent.android.tpush.XGPushBaseReceiver;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.XGPushManager;
import com.tencent.android.tpush.XGPushReceiver;
import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.encrypt.Rijndael;
import com.tencent.android.tpush.rpc.XGRemoteService;
import com.tencent.android.tpush.service.XGPushService;
import com.tencent.android.tpush.service.channel.security.TpnsSecurity;
import com.tencent.android.tpush.service.d.e;
import com.tencent.android.tpush.service.m;
import com.tencent.android.tpush.service.v;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: ProGuard */
public class p {
    private static AtomicBoolean a = new AtomicBoolean(false);
    private static boolean b = false;

    public static String a(String str) {
        if (str == null) {
            return "0";
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                byte b3 = b2 & Constants.NETWORK_TYPE_UNCONNECTED;
                if (b3 < 16) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(Integer.toHexString(b3));
            }
            return stringBuffer.toString();
        } catch (Throwable th) {
            return "0";
        }
    }

    public static int a(Context context) {
        if (a.get()) {
            return 0;
        }
        try {
            if (XGPushManager.getContext() == null) {
                XGPushManager.setContext(context);
            }
            if (m.e() == null) {
                m.c(context);
            }
            if (!h(context)) {
                a.i("Util", "XG is disable");
                return Constants.CODE_SERVICE_DISABLED;
            } else if (!TpnsSecurity.checkTpnsSecurityLibSo(context)) {
                a.i("Util", "can not load library from so file");
                return Constants.CODE_SO_ERROR;
            } else if (!l.a()) {
                return Constants.CODE_PERMISSIONS_ERROR;
            } else {
                if (!b(context)) {
                    a.i("Util", "The service rpc.XGRemoteService is unfined, Please add it in AndroidManifest.xml");
                    return Constants.CODE_AIDL_ERROR;
                }
                e.m(context);
                a.set(true);
                return 0;
            }
        } catch (Throwable th) {
            a.c("Util", "Util -> initGlobal", th);
            return -1;
        }
    }

    public static boolean b(Context context) {
        try {
            List a2 = e.a(context, context.getPackageName() + Constants.RPC_SUFFIX);
            if (a2 == null || a2.size() <= 0) {
                return false;
            }
            return true;
        } catch (Throwable th) {
            a.c("Util", "Util -> isAIDLConfiged", th);
        }
    }

    public static boolean b(String str) {
        return str == null || str.trim().length() == 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, boolean):java.lang.String
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.service.d.e.a(int, java.lang.String, int):android.content.Intent
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, float):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, int):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, long):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, boolean):java.lang.String */
    public static String c(Context context) {
        if (context != null) {
            return Rijndael.decrypt(e.a(context, Constants.SETTINGS_SERVICE_PACKAGE_NAME, false));
        }
        return "";
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0029 A[Catch:{ Throwable -> 0x004b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int d(android.content.Context r5) {
        /*
            if (r5 == 0) goto L_0x0053
            java.lang.String r0 = "activity"
            java.lang.Object r0 = r5.getSystemService(r0)     // Catch:{ Throwable -> 0x004b }
            android.app.ActivityManager r0 = (android.app.ActivityManager) r0     // Catch:{ Throwable -> 0x004b }
            r1 = 2147483647(0x7fffffff, float:NaN)
            java.util.List r0 = r0.getRunningServices(r1)     // Catch:{ Throwable -> 0x004b }
            if (r0 == 0) goto L_0x0053
            int r1 = r0.size()     // Catch:{ Throwable -> 0x004b }
            if (r1 <= 0) goto L_0x0053
            java.lang.Class<com.tencent.android.tpush.service.XGPushService> r1 = com.tencent.android.tpush.service.XGPushService.class
            java.lang.String r1 = r1.getName()     // Catch:{ Throwable -> 0x004b }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ Throwable -> 0x004b }
        L_0x0023:
            boolean r0 = r2.hasNext()     // Catch:{ Throwable -> 0x004b }
            if (r0 == 0) goto L_0x0053
            java.lang.Object r0 = r2.next()     // Catch:{ Throwable -> 0x004b }
            android.app.ActivityManager$RunningServiceInfo r0 = (android.app.ActivityManager.RunningServiceInfo) r0     // Catch:{ Throwable -> 0x004b }
            android.content.ComponentName r3 = r0.service     // Catch:{ Throwable -> 0x004b }
            java.lang.String r3 = r3.getClassName()     // Catch:{ Throwable -> 0x004b }
            boolean r4 = r1.equals(r3)     // Catch:{ Throwable -> 0x004b }
            if (r4 != 0) goto L_0x0043
            java.lang.String r4 = "com.tencent.android.tpush.service.XGPushService"
            boolean r3 = r4.equals(r3)     // Catch:{ Throwable -> 0x004b }
            if (r3 == 0) goto L_0x0023
        L_0x0043:
            int r0 = r0.pid     // Catch:{ Throwable -> 0x004b }
            if (r0 == 0) goto L_0x0049
            r0 = 1
        L_0x0048:
            return r0
        L_0x0049:
            r0 = 2
            goto L_0x0048
        L_0x004b:
            r0 = move-exception
            java.lang.String r1 = "XGService"
            java.lang.String r2 = "getServiceStatus"
            com.tencent.android.tpush.a.a.c(r1, r2, r0)
        L_0x0053:
            r0 = 0
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.common.p.d(android.content.Context):int");
    }

    public static void e(Context context) {
        if (context != null) {
            try {
                m.c(context.getApplicationContext());
                List a2 = e.a(context);
                if (a2 == null || o.a(context).a() || a2.size() < 1 || (a2.size() < 2 && ((ResolveInfo) a2.get(0)).activityInfo.packageName.equals(context.getPackageName()))) {
                    m.a(context);
                    a.a(Constants.ServiceLogTag, "Action -> start Local Service()");
                    g.a().a(new q(context), 1500);
                }
                context.sendBroadcast(new Intent(Constants.ACTION_SDK_INSTALL));
                g.a().a(new q(context), 1500);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static boolean a(Context context, BroadcastReceiver broadcastReceiver) {
        try {
            context.unregisterReceiver(broadcastReceiver);
            return true;
        } catch (Exception e) {
            a.c("Util", "safeUnregisterReceiver error", e);
            return false;
        }
    }

    public static String f(Context context) {
        try {
            String str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            if (str == null) {
                return "";
            }
            return str;
        } catch (Throwable th) {
            Throwable th2 = th;
            String str2 = "";
            a.c("Util", "get app version error", th2);
            return str2;
        }
    }

    private static void a(Context context, String str) {
        PackageManager packageManager;
        if (context != null && str != null && str.trim().length() != 0 && (packageManager = context.getPackageManager()) != null) {
            ComponentName componentName = new ComponentName(context.getPackageName(), str);
            if (packageManager.getComponentEnabledSetting(componentName) != 1) {
                packageManager.setComponentEnabledSetting(componentName, 1, 1);
            }
        }
    }

    public static void g(Context context) {
        if (context != null && !b) {
            try {
                a(context, XGPushService.class.getName());
                a(context, XGPushActivity.class.getName());
                a(context, XGRemoteService.class.getName());
                for (ActivityInfo activityInfo : context.getPackageManager().getPackageInfo(context.getPackageName(), 2).receivers) {
                    String str = activityInfo.name;
                    Class<?> loadClass = context.getClassLoader().loadClass(str);
                    if (XGPushBaseReceiver.class.isAssignableFrom(loadClass) || loadClass.getName().equals(XGPushReceiver.class.getName())) {
                        a(context, str);
                    }
                }
            } catch (Exception e) {
                a.c("Util", "enableComponents", e);
            }
            b = true;
        }
    }

    public static String a(long j) {
        try {
            return new SimpleDateFormat("yyyyMMdd").format(Long.valueOf(j));
        } catch (Exception e) {
            a.c("Util", "getDateString", e);
            return "20141111";
        }
    }

    public static boolean h(Context context) {
        if (context == null) {
            return true;
        }
        XGPushManager.enableService = e.b(context, context.getPackageName() + XGPushManager.ENABLE_SERVICE_SUFFIX, XGPushManager.enableService);
        if (XGPushManager.enableService == -1) {
            XGPushManager.enableService = e.b(context, context.getPackageName() + XGPushManager.ENABLE_SERVICE_SUFFIX, 2);
        }
        if (XGPushManager.enableService == 2 && TpnsSecurity.checkTpnsSecurityLibSo(context)) {
            String str = com.tencent.android.tpush.service.a.a.a(context).x;
            if (!b(str)) {
                String[] split = Rijndael.decrypt(str).split(",");
                HashMap hashMap = new HashMap();
                for (String valueOf : split) {
                    try {
                        hashMap.put(Long.valueOf(valueOf), 0L);
                    } catch (NumberFormatException e) {
                    }
                }
                if (hashMap.size() > 0) {
                    if (XGPushConfig.getAccessId(context) > 0 && hashMap.containsKey(Long.valueOf(XGPushConfig.getAccessId(context)))) {
                        XGPushManager.enableService(context, false);
                        return false;
                    } else if (XGPush4Msdk.getQQAccessId(context) > 0 && hashMap.containsKey(Long.valueOf(XGPush4Msdk.getQQAccessId(context)))) {
                        XGPushManager.enableService(context, false);
                        return false;
                    }
                }
            }
        }
        if (XGPushManager.enableService != 0) {
            return true;
        }
        return false;
    }

    public static boolean i(Context context) {
        try {
            return ((PowerManager) context.getSystemService("power")).isScreenOn();
        } catch (Exception e) {
            a.c(Constants.LogTag, "Util -> isScreenOn", e);
            return false;
        }
    }

    public static int j(Context context) {
        try {
            Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int intExtra = registerReceiver.getIntExtra("status", -1);
            if (!(intExtra == 2 || intExtra == 5)) {
                return -1;
            }
            return registerReceiver.getIntExtra("plugged", -1);
        } catch (Exception e) {
            a.c(Constants.LogTag, "Util -> getChangedStatus", e);
            return -1;
        }
    }

    public static void k(Context context) {
        if (context == null) {
            a.h("Util", "Util -> getWakeCpu error null context");
            return;
        }
        try {
            v.a().a(((PowerManager) context.getSystemService("power")).newWakeLock(1, "TPUSH"));
            if (!v.a().b().isHeld()) {
                v.a().b().acquire();
            }
            a.c("Util", "get Wake Cpu ");
        } catch (Throwable th) {
            a.c("Util", "get Wake cpu", th);
        }
    }

    public static void a() {
        try {
            PowerManager.WakeLock b2 = v.a().b();
            if (b2 != null) {
                if (b2.isHeld()) {
                    b2.release();
                }
                a.c("Util", "stop WakeLock CPU");
            }
        } catch (Throwable th) {
            a.c("Util", "stopWakeLock", th);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x003d A[SYNTHETIC, Splitter:B:17:0x003d] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0042 A[Catch:{ IOException -> 0x006f }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x005b A[SYNTHETIC, Splitter:B:34:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0060 A[Catch:{ IOException -> 0x0064 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.io.File r7, java.io.File r8) {
        /*
            r3 = 0
            r0 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0071, all -> 0x0057 }
            r4.<init>(r7)     // Catch:{ IOException -> 0x0071, all -> 0x0057 }
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x0074, all -> 0x0066 }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0074, all -> 0x0066 }
            r1.<init>(r8)     // Catch:{ IOException -> 0x0074, all -> 0x0066 }
            r2.<init>(r1)     // Catch:{ IOException -> 0x0074, all -> 0x0066 }
            r1 = 4096(0x1000, float:5.74E-42)
            byte[] r1 = new byte[r1]     // Catch:{ IOException -> 0x0021, all -> 0x006a }
        L_0x0015:
            int r3 = r4.read(r1)     // Catch:{ IOException -> 0x0021, all -> 0x006a }
            r5 = -1
            if (r3 == r5) goto L_0x0046
            r5 = 0
            r2.write(r1, r5, r3)     // Catch:{ IOException -> 0x0021, all -> 0x006a }
            goto L_0x0015
        L_0x0021:
            r1 = move-exception
            r3 = r4
        L_0x0023:
            java.lang.String r4 = "Util"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x006d }
            r5.<init>()     // Catch:{ all -> 0x006d }
            java.lang.String r6 = "copyFile IOException: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x006d }
            java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ all -> 0x006d }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x006d }
            com.tencent.android.tpush.a.a.h(r4, r1)     // Catch:{ all -> 0x006d }
            if (r3 == 0) goto L_0x0040
            r3.close()     // Catch:{ IOException -> 0x006f }
        L_0x0040:
            if (r2 == 0) goto L_0x0045
            r2.close()     // Catch:{ IOException -> 0x006f }
        L_0x0045:
            return r0
        L_0x0046:
            r2.flush()     // Catch:{ IOException -> 0x0021, all -> 0x006a }
            r0 = 1
            if (r4 == 0) goto L_0x004f
            r4.close()     // Catch:{ IOException -> 0x0055 }
        L_0x004f:
            if (r2 == 0) goto L_0x0045
            r2.close()     // Catch:{ IOException -> 0x0055 }
            goto L_0x0045
        L_0x0055:
            r1 = move-exception
            goto L_0x0045
        L_0x0057:
            r0 = move-exception
            r2 = r3
        L_0x0059:
            if (r3 == 0) goto L_0x005e
            r3.close()     // Catch:{ IOException -> 0x0064 }
        L_0x005e:
            if (r2 == 0) goto L_0x0063
            r2.close()     // Catch:{ IOException -> 0x0064 }
        L_0x0063:
            throw r0
        L_0x0064:
            r1 = move-exception
            goto L_0x0063
        L_0x0066:
            r0 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x0059
        L_0x006a:
            r0 = move-exception
            r3 = r4
            goto L_0x0059
        L_0x006d:
            r0 = move-exception
            goto L_0x0059
        L_0x006f:
            r1 = move-exception
            goto L_0x0045
        L_0x0071:
            r1 = move-exception
            r2 = r3
            goto L_0x0023
        L_0x0074:
            r1 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.common.p.a(java.io.File, java.io.File):boolean");
    }
}
