package com.tencent.android.tpush.service;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.share.internal.ShareConstants;
import com.jg.EType;
import com.jg.JgClassChecked;
import com.tencent.android.tpush.XGPush4Msdk;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.common.p;
import com.tencent.android.tpush.encrypt.a;
import com.tencent.android.tpush.service.a.c;
import com.tencent.android.tpush.service.a.d;
import com.tencent.android.tpush.service.cache.CacheManager;
import com.tencent.android.tpush.service.d.e;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@JgClassChecked(author = 1, fComment = "确认已进行安全校验", lastDate = "20150316", reviewer = 3, vComment = {EType.SERVICESCHECK})
/* compiled from: ProGuard */
public class XGWatchdog {
    public static Integer CURRENT_WD_VERSION = 2;
    private static final String LIB_FULL_NAME = "libxguardian.so";
    private static final String LIB_NAME = "xguardian";
    public static final String TAG = "xguardian";
    private static String WatchdogPath = "";
    private static int defaultWatchdogPort = 55550;
    private static Handler handler = null;
    private static volatile XGWatchdog instance = null;
    private static Random random = new Random();
    private static final String watchdogPortName = a.a("com.tencent.tpnsWatchdogPort");
    private Context context = null;
    volatile boolean isStarted = false;

    private XGWatchdog(Context context2) {
        try {
            this.context = context2.getApplicationContext();
            m.c(this.context);
            HandlerThread handlerThread = new HandlerThread("XGWatchdog.thread");
            handlerThread.start();
            handler = new Handler(handlerThread.getLooper());
        } catch (Throwable th) {
            com.tencent.android.tpush.a.a.c("xguardian", "init XGWatchdog error", th);
        }
    }

    public static XGWatchdog getInstance(Context context2) {
        if (instance == null) {
            synchronized (XGWatchdog.class) {
                if (instance == null) {
                    instance = new XGWatchdog(context2);
                }
            }
        }
        return instance;
    }

    public static int getRandomInt(int i) {
        return random.nextInt(i);
    }

    public static int getRandomPort() {
        return getRandomInt(1000) + 55000;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x004b A[SYNTHETIC, Splitter:B:29:0x004b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int getWatchdogPort() {
        /*
            r5 = this;
            android.content.Context r0 = r5.context     // Catch:{ Throwable -> 0x000d }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Throwable -> 0x000d }
            java.lang.String r1 = com.tencent.android.tpush.service.XGWatchdog.watchdogPortName     // Catch:{ Throwable -> 0x000d }
            int r0 = android.provider.Settings.System.getInt(r0, r1)     // Catch:{ Throwable -> 0x000d }
        L_0x000c:
            return r0
        L_0x000d:
            r0 = move-exception
            boolean r0 = r0 instanceof android.provider.Settings.SettingNotFoundException
            if (r0 == 0) goto L_0x004f
            r0 = 0
            r3 = r0
        L_0x0014:
            r0 = 10
            if (r3 >= r0) goto L_0x004f
            r2 = 0
            int r0 = getRandomPort()     // Catch:{ Throwable -> 0x0035, all -> 0x0047 }
            java.net.ServerSocket r1 = new java.net.ServerSocket     // Catch:{ Throwable -> 0x0035, all -> 0x0047 }
            r1.<init>(r0)     // Catch:{ Throwable -> 0x0035, all -> 0x0047 }
            android.content.Context r2 = r5.context     // Catch:{ Throwable -> 0x0058 }
            com.tencent.android.tpush.service.channel.c.f r2 = com.tencent.android.tpush.service.channel.c.f.a(r2)     // Catch:{ Throwable -> 0x0058 }
            java.lang.String r4 = com.tencent.android.tpush.service.XGWatchdog.watchdogPortName     // Catch:{ Throwable -> 0x0058 }
            r2.a(r4, r0)     // Catch:{ Throwable -> 0x0058 }
            if (r1 == 0) goto L_0x000c
            r1.close()     // Catch:{ Exception -> 0x0033 }
            goto L_0x000c
        L_0x0033:
            r1 = move-exception
            goto L_0x000c
        L_0x0035:
            r0 = move-exception
            r1 = r2
        L_0x0037:
            java.lang.String r2 = "xguardian"
            java.lang.String r4 = "create ServerSocket error"
            com.tencent.android.tpush.a.a.c(r2, r4, r0)     // Catch:{ all -> 0x0056 }
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ Exception -> 0x0052 }
        L_0x0043:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x0014
        L_0x0047:
            r0 = move-exception
            r1 = r2
        L_0x0049:
            if (r1 == 0) goto L_0x004e
            r1.close()     // Catch:{ Exception -> 0x0054 }
        L_0x004e:
            throw r0
        L_0x004f:
            int r0 = com.tencent.android.tpush.service.XGWatchdog.defaultWatchdogPort
            goto L_0x000c
        L_0x0052:
            r0 = move-exception
            goto L_0x0043
        L_0x0054:
            r1 = move-exception
            goto L_0x004e
        L_0x0056:
            r0 = move-exception
            goto L_0x0049
        L_0x0058:
            r0 = move-exception
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.service.XGWatchdog.getWatchdogPort():int");
    }

    public void sendHeartbeat2Watchdog(String str) {
        sendHeartbeat2Watchdog(str, null);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0078 A[SYNTHETIC, Splitter:B:22:0x0078] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007d A[SYNTHETIC, Splitter:B:25:0x007d] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0082 A[SYNTHETIC, Splitter:B:28:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0087 A[SYNTHETIC, Splitter:B:31:0x0087] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x008c A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00fe A[SYNTHETIC, Splitter:B:61:0x00fe] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0103 A[SYNTHETIC, Splitter:B:64:0x0103] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0108 A[SYNTHETIC, Splitter:B:67:0x0108] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x010d A[SYNTHETIC, Splitter:B:70:0x010d] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x012f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String directSendContent(java.lang.String r11) {
        /*
            r10 = this;
            r0 = 0
            android.content.Context r1 = com.tencent.android.tpush.service.m.e()
            com.tencent.android.tpush.service.a.a r1 = com.tencent.android.tpush.service.a.a.a(r1)
            int r1 = r1.y
            if (r1 == 0) goto L_0x0015
            android.content.Context r1 = r10.context
            boolean r1 = com.tencent.android.tpush.common.p.h(r1)
            if (r1 != 0) goto L_0x0016
        L_0x0015:
            return r0
        L_0x0016:
            r5 = 0
            java.net.Socket r3 = new java.net.Socket     // Catch:{ Throwable -> 0x0177, all -> 0x00f8 }
            java.lang.String r1 = "127.0.0.1"
            int r2 = r10.getWatchdogPort()     // Catch:{ Throwable -> 0x0177, all -> 0x00f8 }
            r3.<init>(r1, r2)     // Catch:{ Throwable -> 0x0177, all -> 0x00f8 }
            r1 = 2000(0x7d0, float:2.803E-42)
            r3.setSoTimeout(r1)     // Catch:{ Throwable -> 0x017d, all -> 0x0166 }
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x017d, all -> 0x0166 }
            java.io.InputStream r2 = r3.getInputStream()     // Catch:{ Throwable -> 0x017d, all -> 0x0166 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x017d, all -> 0x0166 }
            java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ Throwable -> 0x017d, all -> 0x0166 }
            java.io.OutputStream r1 = r3.getOutputStream()     // Catch:{ Throwable -> 0x017d, all -> 0x0166 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x017d, all -> 0x0166 }
            if (r11 != 0) goto L_0x0052
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0182, all -> 0x016b }
            r1.<init>()     // Catch:{ Throwable -> 0x0182, all -> 0x016b }
            java.lang.String r4 = "xgapplist:"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Throwable -> 0x0182, all -> 0x016b }
            java.lang.String r4 = r10.getLocalXGApps()     // Catch:{ Throwable -> 0x0182, all -> 0x016b }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Throwable -> 0x0182, all -> 0x016b }
            java.lang.String r11 = r1.toString()     // Catch:{ Throwable -> 0x0182, all -> 0x016b }
        L_0x0052:
            byte[] r1 = com.tencent.android.tpush.service.channel.security.TpnsSecurity.oiSymmetryEncrypt2Byte(r11)     // Catch:{ Throwable -> 0x0182, all -> 0x016b }
            r2.write(r1)     // Catch:{ Throwable -> 0x0182, all -> 0x016b }
            r2.flush()     // Catch:{ Throwable -> 0x0182, all -> 0x016b }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Throwable -> 0x0182, all -> 0x016b }
            r1.<init>()     // Catch:{ Throwable -> 0x0182, all -> 0x016b }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r4]     // Catch:{ Throwable -> 0x0075, all -> 0x0170 }
        L_0x0065:
            java.io.InputStream r6 = r3.getInputStream()     // Catch:{ Throwable -> 0x0075, all -> 0x0170 }
            int r6 = r6.read(r4)     // Catch:{ Throwable -> 0x0075, all -> 0x0170 }
            r7 = -1
            if (r6 == r7) goto L_0x008f
            r7 = 0
            r1.write(r4, r7, r6)     // Catch:{ Throwable -> 0x0075, all -> 0x0170 }
            goto L_0x0065
        L_0x0075:
            r4 = move-exception
        L_0x0076:
            if (r3 == 0) goto L_0x007b
            r3.close()     // Catch:{ Exception -> 0x00da }
        L_0x007b:
            if (r0 == 0) goto L_0x0080
            r5.close()     // Catch:{ Exception -> 0x0157 }
        L_0x0080:
            if (r1 == 0) goto L_0x0085
            r1.close()     // Catch:{ IOException -> 0x015a }
        L_0x0085:
            if (r2 == 0) goto L_0x008a
            r2.close()     // Catch:{ Exception -> 0x015d }
        L_0x008a:
            if (r0 != 0) goto L_0x012f
            java.lang.String r0 = ""
            goto L_0x0015
        L_0x008f:
            byte[] r4 = r1.toByteArray()     // Catch:{ Throwable -> 0x0075, all -> 0x0170 }
            int r4 = r4.length     // Catch:{ Throwable -> 0x0075, all -> 0x0170 }
            if (r4 <= 0) goto L_0x0189
            byte[] r4 = r1.toByteArray()     // Catch:{ Throwable -> 0x0075, all -> 0x0170 }
            byte[] r6 = com.tencent.android.tpush.service.channel.security.TpnsSecurity.oiSymmetryDecrypt2Byte(r4)     // Catch:{ Throwable -> 0x0075, all -> 0x0170 }
            java.lang.String r4 = new java.lang.String     // Catch:{ Throwable -> 0x0075, all -> 0x0170 }
            r4.<init>(r6)     // Catch:{ Throwable -> 0x0075, all -> 0x0170 }
        L_0x00a3:
            if (r3 == 0) goto L_0x00a8
            r3.close()     // Catch:{ Exception -> 0x00b9 }
        L_0x00a8:
            if (r0 == 0) goto L_0x00ad
            r5.close()     // Catch:{ Exception -> 0x0151 }
        L_0x00ad:
            if (r1 == 0) goto L_0x00b2
            r1.close()     // Catch:{ IOException -> 0x0154 }
        L_0x00b2:
            if (r2 == 0) goto L_0x0186
            r2.close()     // Catch:{ Exception -> 0x00d7 }
            r0 = r4
            goto L_0x008a
        L_0x00b9:
            r3 = move-exception
            java.lang.String r6 = "xguardian"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "close socket failed "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r3 = r3.getMessage()
            java.lang.StringBuilder r3 = r7.append(r3)
            java.lang.String r3 = r3.toString()
            com.tencent.android.tpush.a.a.h(r6, r3)
            goto L_0x00a8
        L_0x00d7:
            r0 = move-exception
            r0 = r4
            goto L_0x008a
        L_0x00da:
            r3 = move-exception
            java.lang.String r4 = "xguardian"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "close socket failed "
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r3 = r3.getMessage()
            java.lang.StringBuilder r3 = r6.append(r3)
            java.lang.String r3 = r3.toString()
            com.tencent.android.tpush.a.a.h(r4, r3)
            goto L_0x007b
        L_0x00f8:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r4 = r0
        L_0x00fc:
            if (r4 == 0) goto L_0x0101
            r4.close()     // Catch:{ Exception -> 0x0111 }
        L_0x0101:
            if (r0 == 0) goto L_0x0106
            r5.close()     // Catch:{ Exception -> 0x0160 }
        L_0x0106:
            if (r2 == 0) goto L_0x010b
            r2.close()     // Catch:{ IOException -> 0x0162 }
        L_0x010b:
            if (r3 == 0) goto L_0x0110
            r3.close()     // Catch:{ Exception -> 0x0164 }
        L_0x0110:
            throw r1
        L_0x0111:
            r4 = move-exception
            java.lang.String r6 = "xguardian"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "close socket failed "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r4 = r4.getMessage()
            java.lang.StringBuilder r4 = r7.append(r4)
            java.lang.String r4 = r4.toString()
            com.tencent.android.tpush.a.a.h(r6, r4)
            goto L_0x0101
        L_0x012f:
            java.lang.String r1 = "|"
            java.lang.String r2 = ""
            java.lang.String r0 = r0.replace(r1, r2)
            java.lang.String r1 = "/"
            java.lang.String r2 = ""
            java.lang.String r0 = r0.replace(r1, r2)
            java.lang.String r1 = "&"
            java.lang.String r2 = ""
            java.lang.String r0 = r0.replace(r1, r2)
            java.lang.String r1 = " "
            java.lang.String r2 = ""
            java.lang.String r0 = r0.replace(r1, r2)
            goto L_0x0015
        L_0x0151:
            r0 = move-exception
            goto L_0x00ad
        L_0x0154:
            r0 = move-exception
            goto L_0x00b2
        L_0x0157:
            r3 = move-exception
            goto L_0x0080
        L_0x015a:
            r1 = move-exception
            goto L_0x0085
        L_0x015d:
            r1 = move-exception
            goto L_0x008a
        L_0x0160:
            r0 = move-exception
            goto L_0x0106
        L_0x0162:
            r0 = move-exception
            goto L_0x010b
        L_0x0164:
            r0 = move-exception
            goto L_0x0110
        L_0x0166:
            r1 = move-exception
            r2 = r0
            r4 = r3
            r3 = r0
            goto L_0x00fc
        L_0x016b:
            r1 = move-exception
            r4 = r3
            r3 = r2
            r2 = r0
            goto L_0x00fc
        L_0x0170:
            r4 = move-exception
            r9 = r4
            r4 = r3
            r3 = r2
            r2 = r1
            r1 = r9
            goto L_0x00fc
        L_0x0177:
            r1 = move-exception
            r1 = r0
            r2 = r0
            r3 = r0
            goto L_0x0076
        L_0x017d:
            r1 = move-exception
            r1 = r0
            r2 = r0
            goto L_0x0076
        L_0x0182:
            r1 = move-exception
            r1 = r0
            goto L_0x0076
        L_0x0186:
            r0 = r4
            goto L_0x008a
        L_0x0189:
            r4 = r0
            goto L_0x00a3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.service.XGWatchdog.directSendContent(java.lang.String):java.lang.String");
    }

    public void sendHeartbeat2Watchdog(String str, ab abVar) {
        if (handler != null) {
            handler.post(new z(this, str, abVar));
        }
    }

    public void sendXGApp(String str, long j) {
        sendHeartbeat2Watchdog(str + "," + j + ";");
    }

    public void sendAllLocalXGAppList() {
        sendHeartbeat2Watchdog(null);
    }

    public void sendDebugMode(boolean z) {
        sendHeartbeat2Watchdog("debug:" + (z ? AppEventsConstants.EVENT_PARAM_VALUE_YES : "0"));
    }

    public String getLocalXGApps() {
        com.tencent.android.tpush.data.a registerInfoByPkgName;
        if (m.e() == null) {
            m.c(this.context);
        }
        List<ResolveInfo> a = e.a(this.context);
        ArrayList<ac> arrayList = new ArrayList<>(10);
        if (a != null) {
            for (ResolveInfo resolveInfo : a) {
                String str = resolveInfo.activityInfo.packageName;
                if (!e.a(str) && (registerInfoByPkgName = CacheManager.getRegisterInfoByPkgName(str)) != null) {
                    d a2 = c.a(this.context, str);
                    float f = 1.0f;
                    if (a2 != null) {
                        f = a2.a;
                    }
                    ac acVar = new ac();
                    acVar.a = str;
                    acVar.c = registerInfoByPkgName.a;
                    acVar.b = f;
                    arrayList.add(acVar);
                }
            }
        }
        Collections.sort(arrayList);
        long accessId = XGPushConfig.getAccessId(this.context);
        if (accessId <= 0) {
            accessId = XGPush4Msdk.getQQAccessId(this.context);
        }
        ac acVar2 = new ac();
        acVar2.a = this.context.getPackageName();
        acVar2.c = accessId;
        acVar2.b = 2.45f;
        arrayList.add(0, acVar2);
        StringBuilder sb = new StringBuilder(1024);
        for (ac acVar3 : arrayList) {
            sb.append(acVar3.a).append(",").append(acVar3.c).append(";");
        }
        return sb.toString();
    }

    private String domainToIp() {
        try {
            return InetAddress.getByName(Constants.UNSTALL_DOMAIN).getHostAddress();
        } catch (Exception e) {
            e.printStackTrace();
            return "14.18.245.161";
        }
    }

    private boolean oldloadWatchdog(String str) {
        if (!e.a(WatchdogPath)) {
            return true;
        }
        WatchdogPath = "";
        try {
            File file = new File(new StringBuffer(this.context.getFilesDir().getParentFile().getAbsolutePath()).append(File.separator).append("lib").append(File.separator).append(LIB_FULL_NAME).toString());
            boolean exists = file.exists();
            if (!exists) {
                return exists;
            }
            WatchdogPath = file.getAbsolutePath();
            return exists;
        } catch (Exception e) {
            com.tencent.android.tpush.a.a.h("xguardian", "jniStartWatchdog loadWatchdog error:" + e.getMessage());
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void directStartWatchdog() {
        if (com.tencent.android.tpush.service.a.a.a(m.e()).y != 0 && p.h(this.context)) {
            try {
                if (!loadWatchdog(this.context.getPackageName())) {
                    return;
                }
                if (!p.h(this.context)) {
                    com.tencent.android.tpush.a.a.h("xguardian", "xg is disable.");
                    return;
                }
                int watchdogPort = getWatchdogPort();
                List<com.tencent.android.tpush.data.a> registerInfo = CacheManager.getRegisterInfo(this.context);
                StringBuffer stringBuffer = new StringBuffer();
                StringBuffer stringBuffer2 = new StringBuffer();
                StringBuffer stringBuffer3 = new StringBuffer();
                for (com.tencent.android.tpush.data.a aVar : registerInfo) {
                    stringBuffer.append(aVar.a).append(",");
                    stringBuffer2.append(aVar.b).append(",");
                    stringBuffer3.append(aVar.d).append(",");
                }
                String[] strArr = new String[7];
                strArr[0] = WatchdogPath;
                strArr[1] = getLocalXGApps();
                strArr[2] = String.valueOf(watchdogPort);
                strArr[3] = domainToIp();
                strArr[4] = new com.tencent.android.tpush.d.a(this.context).a();
                strArr[5] = "" + (XGPushConfig.isEnableDebug(this.context) ? AppEventsConstants.EVENT_PARAM_VALUE_YES : "0");
                strArr[6] = "" + Build.VERSION.SDK_INT;
                com.tencent.android.tpush.a.a.c("xguardian", "exec " + strArr);
                Process exec = Runtime.getRuntime().exec(strArr);
                ad adVar = new ad(this, exec.getErrorStream(), "Error");
                ad adVar2 = new ad(this, exec.getInputStream(), "Output");
                adVar.start();
                adVar2.start();
                com.tencent.android.tpush.a.a.c("xguardian", "proc.exitValue = " + exec.waitFor());
            } catch (Throwable th) {
                com.tencent.android.tpush.a.a.c("xguardian", "directStartWatchdog", th);
            }
        }
    }

    public void startWatchdog() {
        if (handler != null) {
            handler.post(new aa(this));
        }
    }

    private boolean loadWatchdog(String str) {
        boolean z;
        if (!e.a(WatchdogPath)) {
            return true;
        }
        WatchdogPath = "";
        try {
            File file = new File(new StringBuffer(File.separator).append(ShareConstants.WEB_DIALOG_PARAM_DATA).append(File.separator).append(ShareConstants.WEB_DIALOG_PARAM_DATA).append(File.separator).append(str).append(File.separator).append("lib").append(File.separator).append(LIB_FULL_NAME).toString());
            z = file.exists();
            if (z) {
                WatchdogPath = file.getAbsolutePath();
                boolean isInstalledOnSdCard = isInstalledOnSdCard(this.context);
                com.tencent.android.tpush.a.a.d("xguardian", "Application is install in SD Card: " + isInstalledOnSdCard);
                if (isInstalledOnSdCard) {
                    String stringBuffer = new StringBuffer(this.context.getDir("watchdog", 0).getAbsolutePath()).append(File.separator).append(LIB_FULL_NAME).toString();
                    File file2 = new File(stringBuffer);
                    if (file2.exists()) {
                        com.tencent.android.tpush.a.a.d("xguardian", "exeWatchDog exists!");
                    } else {
                        try {
                            file2.createNewFile();
                            if (!p.a(file, file2)) {
                                file2.delete();
                                return false;
                            } else if (Build.VERSION.SDK_INT >= 9) {
                                file2.getClass().getMethod("setExecutable", Boolean.TYPE).invoke(file2, true);
                            } else {
                                String str2 = "chmod 700 " + stringBuffer;
                                com.tencent.android.tpush.a.a.d("xguardian", " exec command: " + str2 + ",  exit:" + Runtime.getRuntime().exec(str2).waitFor());
                            }
                        } catch (IOException e) {
                            file2.delete();
                            com.tencent.android.tpush.a.a.h("xguardian", "exeWatchDog create error!");
                            return false;
                        }
                    }
                    WatchdogPath = stringBuffer;
                }
            }
        } catch (Exception e2) {
            com.tencent.android.tpush.a.a.h("xguardian", "jniStartWatchdog loadWatchdog error:" + e2.getMessage());
            z = false;
        }
        return z;
    }

    private static boolean isInstalledOnSdCard(Context context2) {
        if (Build.VERSION.SDK_INT >= 8) {
            try {
                if ((context2.getPackageManager().getPackageInfo(context2.getPackageName(), 0).applicationInfo.flags & 262144) == 262144) {
                    return true;
                }
                return false;
            } catch (PackageManager.NameNotFoundException e) {
                com.tencent.android.tpush.a.a.h("xguardian", "check install location err, maybe api level < 8");
            }
        }
        try {
            String absolutePath = context2.getFilesDir().getAbsolutePath();
            if (absolutePath.startsWith("/data/")) {
                return false;
            }
            if (absolutePath.contains("/mnt/") || absolutePath.contains("/sdcard/")) {
                return true;
            }
            return false;
        } catch (Throwable th) {
        }
    }
}
