package com.tencent.android.tpush.c;

import android.content.Context;
import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.common.g;

/* compiled from: ProGuard */
public class b {
    public static void a(Context context) {
        if (context == null) {
            a.i(Constants.OTHER_PUSH_TAG, "updateToken Error: context is null");
        } else {
            g.a().a(new c(context));
        }
    }
}
