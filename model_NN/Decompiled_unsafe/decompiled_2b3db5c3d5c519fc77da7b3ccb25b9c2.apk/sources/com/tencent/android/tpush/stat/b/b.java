package com.tencent.android.tpush.stat.b;

import android.content.Context;
import android.os.Environment;
import com.tencent.android.tpush.common.e;
import com.tencent.android.tpush.stat.a.h;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

/* compiled from: ProGuard */
class b extends g {
    b(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return h.a(this.a, "android.permission.WRITE_EXTERNAL_STORAGE") && Environment.getExternalStorageState().equals("mounted");
    }

    /* access modifiers changed from: protected */
    public String b() {
        String str;
        String str2 = null;
        synchronized (this) {
            File file = new File(Environment.getExternalStorageDirectory(), d());
            if (file != null) {
                try {
                    Iterator it = a.a(file).iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            str = null;
                            break;
                        }
                        String[] split = ((String) it.next()).split(",");
                        if (split.length == 2 && split[0].equals(f())) {
                            str = split[1];
                            break;
                        }
                    }
                    str2 = str;
                } catch (IOException e) {
                }
            }
        }
        return str2;
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        BufferedWriter bufferedWriter;
        synchronized (this) {
            a.a(Environment.getExternalStorageDirectory() + "/" + c());
            File file = new File(Environment.getExternalStorageDirectory(), d());
            if (file != null) {
                BufferedWriter bufferedWriter2 = null;
                try {
                    bufferedWriter = new BufferedWriter(new FileWriter(file));
                    try {
                        bufferedWriter.write(f() + "," + str);
                        bufferedWriter.write("\n");
                        e.a(bufferedWriter);
                    } catch (Exception e) {
                        e.a(bufferedWriter);
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        bufferedWriter2 = bufferedWriter;
                        th = th2;
                        e.a(bufferedWriter2);
                        throw th;
                    }
                } catch (Exception e2) {
                    bufferedWriter = null;
                    e.a(bufferedWriter);
                } catch (Throwable th3) {
                    th = th3;
                    e.a(bufferedWriter2);
                    throw th;
                }
            }
        }
    }
}
