package com.tencent.android.tpush.rpc;

import android.content.Intent;
import com.jg.EType;
import com.jg.JgMethodChecked;
import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.b.g;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.service.m;

/* compiled from: ProGuard */
public class h extends b {
    @JgMethodChecked(author = 1, fComment = "确认已进行安全校验", lastDate = "20150316", reviewer = 3, vComment = {EType.INTENTSCHEMECHECK, EType.INTENTCHECK})
    public void a(String str, d dVar) {
        try {
            g.a(m.e()).a(Intent.parseUri(str, 0));
            dVar.a();
        } catch (Throwable th) {
            a.c(Constants.ServiceLogTag, "Show", th);
        }
    }

    public void a() {
        try {
            m.a(m.e());
        } catch (Throwable th) {
            a.c(Constants.ServiceLogTag, "startService", th);
        }
    }

    public void b() {
    }
}
