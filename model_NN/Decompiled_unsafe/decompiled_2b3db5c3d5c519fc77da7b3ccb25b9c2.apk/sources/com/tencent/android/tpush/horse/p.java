package com.tencent.android.tpush.horse;

import com.tencent.android.tpush.horse.data.StrategyItem;
import java.util.List;

/* compiled from: ProGuard */
public class p {
    private static StrategyItem a(String str, int i, int i2) {
        if (str == null || i == 0) {
            return null;
        }
        return new StrategyItem(str, i, "", 80, i2, 0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0031  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.List a(java.util.List r7, short r8, java.lang.String r9) {
        /*
            r2 = 0
            if (r7 != 0) goto L_0x000b
            com.tencent.android.tpush.service.channel.exception.NullReturnException r0 = new com.tencent.android.tpush.service.channel.exception.NullReturnException
            java.lang.String r1 = "getStrategyItems return null, because [items] is null"
            r0.<init>(r1)
            throw r0
        L_0x000b:
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r1 = 0
            android.content.Context r0 = com.tencent.android.tpush.service.m.e()     // Catch:{ Exception -> 0x0058 }
            com.tencent.android.tpush.horse.data.OptStrategyList r0 = com.tencent.android.tpush.service.cache.CacheManager.getOptStrategyList(r0, r9)     // Catch:{ Exception -> 0x0058 }
            com.tencent.android.tpush.horse.data.StrategyItem r0 = r0.e()     // Catch:{ Exception -> 0x0058 }
            r1 = 0
            r0.a(r1)     // Catch:{ Exception -> 0x0066 }
            int r1 = r0.d()     // Catch:{ Exception -> 0x0066 }
            if (r1 != r8) goto L_0x002a
            r3.add(r0)     // Catch:{ Exception -> 0x0066 }
        L_0x002a:
            r1 = r0
        L_0x002b:
            int r0 = r7.size()
            if (r2 >= r0) goto L_0x0065
            java.lang.Object r0 = r7.get(r2)
            com.tencent.android.tpush.horse.data.ServerItem r0 = (com.tencent.android.tpush.horse.data.ServerItem) r0
            java.lang.String r4 = r0.a()
            java.lang.Object r0 = r7.get(r2)
            com.tencent.android.tpush.horse.data.ServerItem r0 = (com.tencent.android.tpush.horse.data.ServerItem) r0
            int r0 = r0.b()
            com.tencent.android.tpush.horse.data.StrategyItem r0 = a(r4, r0, r8)
            if (r0 == 0) goto L_0x0054
            boolean r4 = r0.equals(r1)
            if (r4 != 0) goto L_0x0054
            r3.add(r0)
        L_0x0054:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x002b
        L_0x0058:
            r0 = move-exception
            r6 = r0
            r0 = r1
            r1 = r6
        L_0x005c:
            java.lang.String r4 = "XGService"
            java.lang.String r5 = "getStrategyItems"
            com.tencent.android.tpush.a.a.c(r4, r5, r1)
            r1 = r0
            goto L_0x002b
        L_0x0065:
            return r3
        L_0x0066:
            r1 = move-exception
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.horse.p.a(java.util.List, short, java.lang.String):java.util.List");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.horse.p.a(java.util.List, short, java.lang.String):java.util.List
     arg types: [java.util.List, int, java.lang.String]
     candidates:
      com.tencent.android.tpush.horse.p.a(java.lang.String, int, int):com.tencent.android.tpush.horse.data.StrategyItem
      com.tencent.android.tpush.horse.p.a(java.util.List, short, java.lang.String):java.util.List */
    public static List a(List list, String str) {
        return a(list, (short) 0, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.horse.p.a(java.util.List, short, java.lang.String):java.util.List
     arg types: [java.util.List, int, java.lang.String]
     candidates:
      com.tencent.android.tpush.horse.p.a(java.lang.String, int, int):com.tencent.android.tpush.horse.data.StrategyItem
      com.tencent.android.tpush.horse.p.a(java.util.List, short, java.lang.String):java.util.List */
    public static List b(List list, String str) {
        return a(list, (short) 1, str);
    }
}
