package com.tencent.android.tpush;

/* compiled from: ProGuard */
public class XGPushTextMessage {
    String a = "";
    String b = "";
    String c = "";

    public String getTitle() {
        return this.a;
    }

    public String getContent() {
        return this.b;
    }

    public String getCustomContent() {
        return this.c;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("XGPushShowedResult [title=").append(this.a).append(", content=").append(this.b).append(", customContent=").append(this.c).append("]");
        return sb.toString();
    }
}
