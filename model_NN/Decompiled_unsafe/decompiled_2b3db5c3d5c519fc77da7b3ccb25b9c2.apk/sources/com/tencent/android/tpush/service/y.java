package com.tencent.android.tpush.service;

import com.tencent.android.tpush.common.m;
import com.tencent.android.tpush.service.a.a;

/* compiled from: ProGuard */
class y implements Runnable {
    final /* synthetic */ w a;

    y(w wVar) {
        this.a = wVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, int):int
      com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, long):long */
    public void run() {
        try {
            if (a.a(this.a.b).z != 0) {
                long currentTimeMillis = System.currentTimeMillis();
                if (this.a.g == 0) {
                    long unused = this.a.g = m.a(this.a.b, "last.monitor.ts", 0L);
                }
                if (currentTimeMillis - this.a.g >= ((long) a.a(this.a.b).A)) {
                    if (!this.a.d) {
                        this.a.f();
                    } else {
                        this.a.e();
                    }
                    long unused2 = this.a.g = currentTimeMillis;
                    m.b(this.a.b, "last.monitor.ts", this.a.g);
                }
            }
        } catch (Throwable th) {
            com.tencent.android.tpush.a.a.c(w.a, "monotor error.", th);
        }
    }
}
