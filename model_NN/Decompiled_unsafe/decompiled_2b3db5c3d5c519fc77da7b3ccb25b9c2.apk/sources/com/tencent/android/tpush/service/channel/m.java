package com.tencent.android.tpush.service.channel;

import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.service.a.a;
import com.tencent.android.tpush.service.channel.exception.ChannelException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
class m implements Runnable {
    final /* synthetic */ b a;

    private m(b bVar) {
        this.a = bVar;
    }

    /* synthetic */ m(b bVar, c cVar) {
        this(bVar);
    }

    public void run() {
        a aVar;
        boolean z;
        long j;
        long j2;
        boolean z2;
        boolean z3;
        try {
            long currentTimeMillis = System.currentTimeMillis();
            long j3 = Long.MAX_VALUE;
            long j4 = (long) a.a(com.tencent.android.tpush.service.m.e()).f;
            boolean z4 = false;
            long j5 = j4 < 15000 ? 15000 : j4;
            ChannelException channelException = new ChannelException(Constants.CODE_NETWORK_TIMEOUT_WAITING_FOR_RESPONSE, "TpnsMessage wait for response timeout!");
            for (com.tencent.android.tpush.service.channel.a.a aVar2 : this.a.k.keySet()) {
                ConcurrentHashMap concurrentHashMap = (ConcurrentHashMap) this.a.k.get(aVar2);
                if (concurrentHashMap == null || concurrentHashMap.size() == 0) {
                    z2 = z4;
                    j2 = j3;
                } else {
                    Iterator it = concurrentHashMap.entrySet().iterator();
                    a f = aVar2.f();
                    z2 = z4;
                    j2 = j3;
                    while (it.hasNext()) {
                        o oVar = (o) ((Map.Entry) it.next()).getValue();
                        if (oVar != null) {
                            long j6 = currentTimeMillis - oVar.b;
                            f.a(3, Long.valueOf(j6));
                            if (j6 >= 0) {
                                if (j6 > j5) {
                                    p pVar = oVar.d;
                                    if (pVar != null) {
                                        pVar.a(oVar.c, channelException, f);
                                        oVar.d = null;
                                    }
                                    it.remove();
                                    z3 = true;
                                } else {
                                    if (j5 - j6 < j2) {
                                        j2 = j5 - j6;
                                        z3 = z2;
                                    }
                                    z3 = z2;
                                }
                            }
                        } else {
                            it.remove();
                            z3 = z2;
                        }
                        z2 = z3;
                    }
                }
                j3 = j2;
                z4 = z2;
            }
            ChannelException channelException2 = new ChannelException(Constants.CODE_NETWORK_TIMEOUT_WAITING_TO_SEND, "TpnsMessage wait for response timeout!");
            a aVar3 = null;
            synchronized (this.a) {
                Iterator it2 = this.a.j.iterator();
                while (it2.hasNext()) {
                    o oVar2 = (o) it2.next();
                    if (oVar2 != null) {
                        long j7 = currentTimeMillis - oVar2.a;
                        if (j7 >= 0) {
                            if (j7 > j5) {
                                p pVar2 = oVar2.d;
                                if (pVar2 != null) {
                                    if (aVar3 == null) {
                                        if (this.a.m != null) {
                                            aVar3 = this.a.m.f();
                                        } else {
                                            aVar3 = new a();
                                        }
                                        aVar3.a(3, Long.valueOf(j7));
                                    }
                                    pVar2.a(oVar2.c, channelException2, aVar3);
                                    oVar2.d = null;
                                }
                                aVar = aVar3;
                                it2.remove();
                                z = true;
                                j = j3;
                            } else {
                                if (j5 - j7 < j3) {
                                    aVar = aVar3;
                                    z = z4;
                                    j = j5 - j7;
                                }
                                aVar = aVar3;
                                z = z4;
                                j = j3;
                            }
                        }
                    } else {
                        it2.remove();
                        aVar = aVar3;
                        z = z4;
                        j = j3;
                    }
                    j3 = j;
                    z4 = z;
                    aVar3 = aVar;
                }
            }
            if (z4) {
                this.a.d();
            }
        } catch (Exception e) {
            com.tencent.android.tpush.a.a.c("TpnsChannel", "TimeoutRunnable.run", e);
        }
    }
}
