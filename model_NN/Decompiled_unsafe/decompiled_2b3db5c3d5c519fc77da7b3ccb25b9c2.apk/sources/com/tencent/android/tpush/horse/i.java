package com.tencent.android.tpush.horse;

import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.horse.data.StrategyItem;
import java.nio.channels.SocketChannel;

/* compiled from: ProGuard */
class i implements b {
    final /* synthetic */ g a;

    i(g gVar) {
        this.a = gVar;
    }

    public void a(SocketChannel socketChannel, StrategyItem strategyItem) {
        int unused = g.m = 0;
        if (q.i().b()) {
            int unused2 = this.a.e = 1;
        }
        synchronized (this.a.d) {
            if (this.a.e == 1) {
                try {
                    this.a.d.wait();
                } catch (Exception e) {
                    a.c(Constants.HorseLogTag, "lock.wait", e);
                }
            }
        }
        if (!socketChannel.isConnected() || q.i().c()) {
            if (!socketChannel.isConnected() && !q.i().c()) {
                this.a.a(Constants.CODE_NETWORK_CREATE_OPTIOMAL_SC_FAILED, "create channel fail httpChannelCallback !");
            }
        } else if (this.a.h == null) {
            a.h(Constants.HorseLogTag, ">> mcreateSocket channelCallback is null ");
        } else if (strategyItem.j()) {
            try {
                socketChannel.close();
            } catch (Exception e2) {
                a.c(Constants.HorseLogTag, "socketChannel.close()", e2);
            }
        } else {
            this.a.h.a(socketChannel, strategyItem);
        }
    }

    public void a(StrategyItem strategyItem) {
        if (!q.i().b() && !f.i().b() && this.a.e == 0) {
            int unused = this.a.e = 2;
            if (this.a.h != null) {
                this.a.h.a((int) Constants.CODE_NETWORK_CREATE_OPTIOMAL_SC_FAILED, "create http channel fail!");
            }
        }
    }
}
