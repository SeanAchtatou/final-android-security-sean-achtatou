package com.tencent.android.tpush.service;

import android.app.ActivityManager;
import android.content.Context;
import android.net.LocalServerSocket;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.ActivityChooserView;
import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.common.g;
import com.tencent.android.tpush.common.p;
import com.tencent.android.tpush.service.a.c;
import com.tencent.android.tpush.service.a.d;
import com.tencent.android.tpush.service.cache.CacheManager;
import com.tencent.android.tpush.service.d.e;
import java.util.List;

/* compiled from: ProGuard */
public class m {
    /* access modifiers changed from: private */
    public static Context a = null;
    private static String b = "";
    private static LocalServerSocket c = null;
    private static volatile boolean e = false;
    /* access modifiers changed from: private */
    public static volatile boolean f = false;
    private Handler d;

    /* synthetic */ m(n nVar) {
        this();
    }

    private m() {
        this.d = null;
    }

    public static m a() {
        return o.a;
    }

    public void b() {
        c.a(a, new d(2.45f, 0));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0072, code lost:
        r2 = com.tencent.android.tpush.service.cache.CacheManager.getRegisterInfos(e());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007a, code lost:
        if (r2 == null) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0080, code lost:
        if (r2.size() <= 1) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0082, code lost:
        r0 = (long) (((int) (java.lang.Math.random() * 1000.0d)) + 900);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0094, code lost:
        if (r0 >= 1000) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0096, code lost:
        r0 = 1000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0098, code lost:
        r6.d.sendMessageDelayed(r6.d.obtainMessage(1), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.content.Intent r7) {
        /*
            r6 = this;
            r4 = 1
            r0 = 0
            android.os.Handler r2 = r6.d
            if (r2 != 0) goto L_0x000a
            r6.k()
        L_0x000a:
            monitor-enter(r6)
            boolean r2 = com.tencent.android.tpush.service.m.e     // Catch:{ all -> 0x0051 }
            if (r2 == 0) goto L_0x0071
            android.net.LocalServerSocket r2 = com.tencent.android.tpush.service.m.c     // Catch:{ all -> 0x0051 }
            if (r2 == 0) goto L_0x0071
            if (r7 == 0) goto L_0x0043
            java.lang.String r2 = r7.getAction()     // Catch:{ all -> 0x0051 }
            if (r2 == 0) goto L_0x0043
            java.lang.String r3 = "com.tencent.android.tpush.action.keepalive"
            boolean r3 = r3.equals(r2)     // Catch:{ all -> 0x0051 }
            if (r3 == 0) goto L_0x0054
            android.os.Handler r2 = r6.d     // Catch:{ all -> 0x0051 }
            r3 = 2
            android.os.Message r2 = r2.obtainMessage(r3)     // Catch:{ all -> 0x0051 }
            java.lang.String r3 = "delay_time"
            r4 = 0
            long r4 = r7.getLongExtra(r3, r4)     // Catch:{ all -> 0x0051 }
            int r0 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r0 != 0) goto L_0x0045
            android.os.Handler r0 = r6.d     // Catch:{ all -> 0x0051 }
            r1 = 2
            r0.removeMessages(r1)     // Catch:{ all -> 0x0051 }
            android.os.Handler r0 = r6.d     // Catch:{ all -> 0x0051 }
            r4 = 100
            r0.sendMessageDelayed(r2, r4)     // Catch:{ all -> 0x0051 }
        L_0x0043:
            monitor-exit(r6)     // Catch:{ all -> 0x0051 }
        L_0x0044:
            return
        L_0x0045:
            android.os.Handler r0 = r6.d     // Catch:{ all -> 0x0051 }
            r1 = 2
            r0.removeMessages(r1)     // Catch:{ all -> 0x0051 }
            android.os.Handler r0 = r6.d     // Catch:{ all -> 0x0051 }
            r0.sendMessageDelayed(r2, r4)     // Catch:{ all -> 0x0051 }
            goto L_0x0043
        L_0x0051:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0051 }
            throw r0
        L_0x0054:
            java.lang.String r0 = "com.tencent.android.tpush.action.stop_connect"
            boolean r0 = r0.equals(r2)     // Catch:{ all -> 0x0051 }
            if (r0 == 0) goto L_0x0043
            android.os.Handler r0 = r6.d     // Catch:{ all -> 0x0051 }
            r1 = 3
            android.os.Message r0 = r0.obtainMessage(r1)     // Catch:{ all -> 0x0051 }
            android.os.Handler r1 = r6.d     // Catch:{ all -> 0x0051 }
            r2 = 3
            r1.removeMessages(r2)     // Catch:{ all -> 0x0051 }
            android.os.Handler r1 = r6.d     // Catch:{ all -> 0x0051 }
            r2 = 100
            r1.sendMessageDelayed(r0, r2)     // Catch:{ all -> 0x0051 }
            goto L_0x0043
        L_0x0071:
            monitor-exit(r6)     // Catch:{ all -> 0x0051 }
            android.content.Context r2 = e()
            java.util.List r2 = com.tencent.android.tpush.service.cache.CacheManager.getRegisterInfos(r2)
            if (r2 == 0) goto L_0x0098
            int r2 = r2.size()
            if (r2 <= r4) goto L_0x0098
            double r0 = java.lang.Math.random()
            r2 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r0 = r0 * r2
            int r0 = (int) r0
            int r0 = r0 + 900
            long r0 = (long) r0
            r2 = 1000(0x3e8, double:4.94E-321)
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 >= 0) goto L_0x0098
            r0 = 1000(0x3e8, double:4.94E-321)
        L_0x0098:
            android.os.Handler r2 = r6.d
            android.os.Message r2 = r2.obtainMessage(r4)
            android.os.Handler r3 = r6.d
            r3.sendMessageDelayed(r2, r0)
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.service.m.a(android.content.Intent):void");
    }

    public static void a(Context context) {
        a(context, Constants.ACTION_KEEPALIVE, 0);
    }

    public static void a(Context context, long j) {
        a(context, Constants.ACTION_KEEPALIVE, j);
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0063 A[Catch:{ Throwable -> 0x0067 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x008c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r6, java.lang.String r7, long r8) {
        /*
            r2 = 0
            if (r6 == 0) goto L_0x0024
            android.content.Intent r1 = new android.content.Intent     // Catch:{ Throwable -> 0x0099 }
            r1.<init>()     // Catch:{ Throwable -> 0x0099 }
            java.lang.Class<com.tencent.android.tpush.service.XGPushService> r0 = com.tencent.android.tpush.service.XGPushService.class
            r1.setClass(r6, r0)     // Catch:{ Throwable -> 0x0030 }
            r1.setAction(r7)     // Catch:{ Throwable -> 0x0030 }
            r2 = 0
            int r0 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x001b
            java.lang.String r0 = "delay_time"
            r1.putExtra(r0, r8)     // Catch:{ Throwable -> 0x0030 }
        L_0x001b:
            int r0 = com.tencent.android.tpush.common.p.a(r6)     // Catch:{ Throwable -> 0x0030 }
            if (r0 > 0) goto L_0x0025
            r6.startService(r1)     // Catch:{ Throwable -> 0x0030 }
        L_0x0024:
            return
        L_0x0025:
            java.lang.String r0 = "PushServiceManager"
            java.lang.String r2 = "startService failed, libtpnsSecurity.so not found."
            com.tencent.android.tpush.a.a.h(r0, r2)     // Catch:{ Throwable -> 0x0030 }
            r6.stopService(r1)     // Catch:{ Throwable -> 0x0030 }
            goto L_0x0024
        L_0x0030:
            r0 = move-exception
        L_0x0031:
            java.lang.String r2 = "PushServiceManager"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "startService failed, intent:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r1)
            java.lang.String r4 = ", ex:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.android.tpush.a.a.h(r2, r0)
            android.content.Intent r2 = new android.content.Intent     // Catch:{ Throwable -> 0x0097 }
            r2.<init>()     // Catch:{ Throwable -> 0x0097 }
            java.lang.Class<com.tencent.android.tpush.service.XGPushService> r0 = com.tencent.android.tpush.service.XGPushService.class
            r2.setClass(r6, r0)     // Catch:{ Throwable -> 0x0067 }
            int r0 = com.tencent.android.tpush.common.p.a(r6)     // Catch:{ Throwable -> 0x0067 }
            if (r0 > 0) goto L_0x008c
            r6.startService(r2)     // Catch:{ Throwable -> 0x0067 }
            goto L_0x0024
        L_0x0067:
            r0 = move-exception
            r1 = r2
        L_0x0069:
            java.lang.String r2 = "PushServiceManager"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "222 startService failed, intent:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r3 = ", ex:"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.android.tpush.a.a.h(r2, r0)
            goto L_0x0024
        L_0x008c:
            java.lang.String r0 = "PushServiceManager"
            java.lang.String r1 = "startService failed, libtpnsSecurity.so not found."
            com.tencent.android.tpush.a.a.h(r0, r1)     // Catch:{ Throwable -> 0x0067 }
            r6.stopService(r2)     // Catch:{ Throwable -> 0x0067 }
            goto L_0x0024
        L_0x0097:
            r0 = move-exception
            goto L_0x0069
        L_0x0099:
            r0 = move-exception
            r1 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.service.m.a(android.content.Context, java.lang.String, long):void");
    }

    public static void b(Context context) {
        a.e("PushServiceManager", "Action -> stop Current Connect");
        a(context, Constants.ACTION_STOP_CONNECT, 0);
    }

    public void c() {
        a.c("PushServiceManager", "@@ serviceExit()");
        p.a();
        if (this.d != null) {
            this.d.removeCallbacksAndMessages(null);
            this.d = null;
        }
        if (g.a().b() != null) {
            g.a().b().removeCallbacksAndMessages(null);
        }
        a.a().b(a);
        d();
        e.p(e());
    }

    public void d() {
        synchronized (this) {
            if (c != null) {
                try {
                    c.close();
                    c = null;
                } catch (Exception e2) {
                    a.c(Constants.ServiceLogTag, ">> Destroy local socket exception", e2);
                }
            }
            Boolean bool = false;
            e = bool.booleanValue();
        }
    }

    public static void c(Context context) {
        if (context != null) {
            a = context;
            b = context.getPackageName();
        }
    }

    public static Context e() {
        return a;
    }

    public static String f() {
        return b;
    }

    private boolean i() {
        d dVar;
        try {
            List registerInfos = CacheManager.getRegisterInfos(e());
            if (registerInfos != null && registerInfos.size() >= 2) {
                d dVar2 = null;
                d a2 = c.a(a);
                List<ActivityManager.RunningServiceInfo> runningServices = ((ActivityManager) a.getSystemService(Constants.FLAG_ACTIVITY_NAME)).getRunningServices(ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
                if (runningServices != null && runningServices.size() > 0) {
                    String name = XGPushService.class.getName();
                    for (ActivityManager.RunningServiceInfo next : runningServices) {
                        if (name.equals(next.service.getClassName())) {
                            dVar = c.a(a, next.service.getPackageName());
                            if (dVar2 != null) {
                                if (dVar.a > dVar2.a) {
                                }
                            }
                            dVar2 = dVar;
                        }
                        dVar = dVar2;
                        dVar2 = dVar;
                    }
                }
                if (dVar2 == null || dVar2.a <= a2.a) {
                    return true;
                }
                return false;
            }
        } catch (Exception e2) {
            a.c("PushServiceManager", "isSurvive", e2);
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, boolean):java.lang.String
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.service.d.e.a(int, java.lang.String, int):android.content.Intent
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, float):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, int):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, long):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, long, boolean):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean */
    /* access modifiers changed from: private */
    public boolean j() {
        boolean i = i();
        synchronized (this) {
            if (i) {
                try {
                    String a2 = e.a(a, Constants.SETTINGS_SOCKET_NAME, true);
                    if (e.a(a2)) {
                        a2 = e.a();
                        e.a(a, Constants.SETTINGS_SOCKET_NAME, a2, true);
                    }
                    p.g(a);
                    c = new LocalServerSocket(a2);
                    Boolean bool = true;
                    e = bool.booleanValue();
                    XGWatchdog.getInstance(a).startWatchdog();
                    w.a(a).a();
                } catch (Throwable th) {
                    i = e;
                }
            }
        }
        return i;
    }

    private void k() {
        this.d = new n(this, Looper.getMainLooper());
    }
}
