package com.tencent.android.tpush;

import android.content.DialogInterface;
import android.content.Intent;

/* compiled from: ProGuard */
class i implements DialogInterface.OnCancelListener {
    final /* synthetic */ Intent a;
    final /* synthetic */ XGPushActivity b;

    i(XGPushActivity xGPushActivity, Intent intent) {
        this.b = xGPushActivity;
        this.a = intent;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.a.putExtra("action", 4);
        this.b.broadcastToTPushService(this.a);
        this.b.finish();
    }
}
