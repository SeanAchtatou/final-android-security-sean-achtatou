package com.tencent.android.tpush.stat;

import com.tencent.android.tpush.XGPush4Msdk;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.common.p;
import com.tencent.android.tpush.stat.event.c;
import java.lang.Thread;
import java.util.Arrays;

/* compiled from: ProGuard */
class r implements Thread.UncaughtExceptionHandler {
    r() {
    }

    public void uncaughtException(Thread thread, Throwable th) {
        boolean z;
        p.a();
        if (c.c() && h.f != null) {
            long accessId = XGPushConfig.getAccessId(h.f);
            if (accessId <= 0) {
                accessId = XGPush4Msdk.getQQAccessId(h.f);
            }
            if (h.g == null || th.toString().contains(h.g)) {
                z = true;
            } else {
                z = false;
            }
            if (z) {
                h.b(Arrays.asList(new c(h.f, h.b(h.f, accessId), 2, th, thread, accessId)));
                h.d.g("has caught the following uncaught exception:");
                h.d.a(th);
            }
            if (h.e != null) {
                h.d.h("Call the original uncaught exception handler.");
                if (!(h.e instanceof r)) {
                    h.e.uncaughtException(thread, th);
                }
            }
        }
    }
}
