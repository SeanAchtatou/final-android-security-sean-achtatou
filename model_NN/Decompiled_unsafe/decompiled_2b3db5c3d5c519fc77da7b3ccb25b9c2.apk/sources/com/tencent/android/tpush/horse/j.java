package com.tencent.android.tpush.horse;

import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.horse.data.StrategyItem;
import java.nio.channels.SocketChannel;

/* compiled from: ProGuard */
class j implements b {
    final /* synthetic */ g a;

    j(g gVar) {
        this.a = gVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.horse.g.a(com.tencent.android.tpush.horse.g, boolean):boolean
     arg types: [com.tencent.android.tpush.horse.g, int]
     candidates:
      com.tencent.android.tpush.horse.g.a(com.tencent.android.tpush.horse.g, int):int
      com.tencent.android.tpush.horse.g.a(com.tencent.android.tpush.horse.g, long):long
      com.tencent.android.tpush.horse.g.a(int, java.lang.String):void
      com.tencent.android.tpush.horse.g.a(com.tencent.android.tpush.horse.g, java.lang.String):void
      com.tencent.android.tpush.horse.g.a(com.tencent.android.tpush.horse.g, boolean):boolean */
    public void a(SocketChannel socketChannel, StrategyItem strategyItem) {
        int unused = g.m = 0;
        if (socketChannel == null || strategyItem == null) {
            this.a.a(Constants.CODE_NETWORK_CREATE_OPTIOMAL_SC_FAILED, "create channel fail!");
            return;
        }
        if (!socketChannel.isConnected()) {
            this.a.a(Constants.CODE_NETWORK_CREATE_OPTIOMAL_SC_FAILED, "create channel fail!");
        } else if (this.a.h != null) {
            if (!strategyItem.j() || this.a.f) {
                this.a.h.a(socketChannel, strategyItem);
            } else {
                try {
                    socketChannel.close();
                } catch (Exception e) {
                    a.c(Constants.HorseLogTag, "socketChannel.close()", e);
                }
            }
        }
        if (this.a.f) {
            boolean unused2 = this.a.f = false;
        }
        synchronized (this.a.d) {
            int unused3 = this.a.e = 2;
            this.a.d.notify();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.horse.g.a(com.tencent.android.tpush.horse.g, boolean):boolean
     arg types: [com.tencent.android.tpush.horse.g, int]
     candidates:
      com.tencent.android.tpush.horse.g.a(com.tencent.android.tpush.horse.g, int):int
      com.tencent.android.tpush.horse.g.a(com.tencent.android.tpush.horse.g, long):long
      com.tencent.android.tpush.horse.g.a(int, java.lang.String):void
      com.tencent.android.tpush.horse.g.a(com.tencent.android.tpush.horse.g, java.lang.String):void
      com.tencent.android.tpush.horse.g.a(com.tencent.android.tpush.horse.g, boolean):boolean */
    public void a(StrategyItem strategyItem) {
        if (this.a.f) {
            boolean unused = this.a.f = false;
            this.a.b();
        } else if (!q.i().b()) {
            if (this.a.e == 0 && !f.i().b()) {
                int unused2 = this.a.e = 2;
                if (this.a.h != null) {
                    this.a.a(Constants.CODE_NETWORK_CREATE_OPTIOMAL_SC_FAILED, "create channel fail!");
                }
            }
            if (this.a.e == 1) {
                synchronized (this.a.d) {
                    int unused3 = this.a.e = 2;
                    this.a.d.notify();
                }
            }
        } else {
            a.d(Constants.HorseLogTag, ">> tcp has remain");
        }
    }
}
