package com.tencent.android.tpush.service.channel.protocol;

import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;

/* compiled from: ProGuard */
public final class MutableInfo extends JceStruct {
    public String bssid = "";
    public String mac = "";
    public String ssid = "";
    public String wflist = "";

    public MutableInfo() {
    }

    public MutableInfo(String str, String str2, String str3, String str4) {
        this.ssid = str;
        this.bssid = str2;
        this.mac = str3;
        this.wflist = str4;
    }

    public void writeTo(JceOutputStream jceOutputStream) {
        if (this.ssid != null) {
            jceOutputStream.write(this.ssid, 0);
        }
        if (this.bssid != null) {
            jceOutputStream.write(this.bssid, 1);
        }
        if (this.mac != null) {
            jceOutputStream.write(this.mac, 2);
        }
        if (this.wflist != null) {
            jceOutputStream.write(this.wflist, 3);
        }
    }

    public void readFrom(JceInputStream jceInputStream) {
        this.ssid = jceInputStream.readString(0, false);
        this.bssid = jceInputStream.readString(1, false);
        this.mac = jceInputStream.readString(2, false);
        this.wflist = jceInputStream.readString(3, false);
    }
}
