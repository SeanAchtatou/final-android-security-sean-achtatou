package com.tencent.android.tpush.b;

import android.content.Context;
import android.content.Intent;
import com.tencent.android.tpush.XGIOperateCallback;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.common.MessageKey;
import com.tencent.android.tpush.service.d.e;

/* compiled from: ProGuard */
class h implements Runnable {
    final /* synthetic */ g a;
    private final String b = h.class.getSimpleName();
    private Context c;
    private Intent d;
    private XGIOperateCallback e;

    public h(g gVar, Context context, Intent intent, XGIOperateCallback xGIOperateCallback) {
        this.a = gVar;
        this.c = context;
        this.d = intent;
        this.e = xGIOperateCallback;
    }

    private void a() {
        Intent intent = new Intent(Constants.ACTION_PUSH_MESSAGE);
        intent.setPackage(this.c.getPackageName());
        intent.putExtras(this.d);
        this.c.sendBroadcast(intent);
        String stringExtra = this.d.getStringExtra(MessageKey.MSG_SERVICE_PACKAGE_NAME);
        if (!e.a(stringExtra)) {
            Intent intent2 = new Intent("com.tencent.android.tpush.action.ack.sdk2srv");
            intent2.setPackage(stringExtra);
            intent2.putExtras(this.d);
            this.c.sendBroadcast(intent2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, long, boolean):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x025c, code lost:
        r2 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        com.tencent.android.tpush.a.a.c(r0.b, "unknown error", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0267, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
        com.tencent.android.tpush.a.a.c(r0.b, "push parse error", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0279, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
        com.tencent.android.tpush.a.a.c(com.tencent.android.tpush.common.Constants.ServiceLogTag, "push msg type error", r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0249 A[Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0267 A[ExcHandler: JSONException (e org.json.JSONException), Splitter:B:7:0x0015] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0279 A[ExcHandler: IllegalArgumentException (e java.lang.IllegalArgumentException), Splitter:B:7:0x0015] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r18 = this;
            r0 = r18
            com.tencent.android.tpush.b.g r11 = r0.a
            monitor-enter(r11)
            boolean r2 = com.tencent.android.tpush.XGPushConfig.enableDebug     // Catch:{ all -> 0x0099 }
            if (r2 == 0) goto L_0x0012
            r0 = r18
            java.lang.String r2 = r0.b     // Catch:{ all -> 0x0099 }
            java.lang.String r3 = "Action -> handlerPushMessage"
            com.tencent.android.tpush.a.a.c(r2, r3)     // Catch:{ all -> 0x0099 }
        L_0x0012:
            r10 = 0
            r0 = r18
            android.content.Intent r2 = r0.d     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r3 = "expire_time"
            r4 = 0
            long r4 = r2.getLongExtra(r3, r4)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r18
            android.content.Intent r2 = r0.d     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r2 = r2.getPackage()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r18
            android.content.Intent r3 = r0.d     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r8 = "msgId"
            r12 = -1
            long r8 = r3.getLongExtra(r8, r12)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r18
            android.content.Context r3 = r0.c     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r18
            android.content.Intent r12 = r0.d     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            com.tencent.android.tpush.b.i r3 = com.tencent.android.tpush.b.i.a(r3, r12)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r12 = 0
            int r12 = (r4 > r12 ? 1 : (r4 == r12 ? 0 : -1))
            if (r12 <= 0) goto L_0x0086
            int r4 = (r6 > r4 ? 1 : (r6 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x0086
            java.lang.String r2 = "PushMessageHandler"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r4.<init>()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r5 = "msg is expired, currentTimeMillis="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r5 = "current="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r5 = "."
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r18
            android.content.Intent r5 = r0.d     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r4 = r4.toString()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            com.tencent.android.tpush.a.a.h(r2, r4)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r18
            android.content.Context r2 = r0.c     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            com.tencent.android.tpush.XGPushManager.msgAck(r2, r3)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            monitor-exit(r11)     // Catch:{ all -> 0x0099 }
        L_0x0085:
            return
        L_0x0086:
            java.lang.Long r4 = java.lang.Long.valueOf(r8)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            boolean r4 = com.tencent.android.tpush.b.g.a(r4)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            if (r4 != 0) goto L_0x009c
            r0 = r18
            android.content.Context r2 = r0.c     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            com.tencent.android.tpush.XGPushManager.msgAck(r2, r3)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            monitor-exit(r11)     // Catch:{ all -> 0x0099 }
            goto L_0x0085
        L_0x0099:
            r2 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x0099 }
            throw r2
        L_0x009c:
            r4 = 2
            com.tencent.android.tpush.a.a.a(r4, r8)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r18
            android.content.Intent r4 = r0.d     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r5 = "busiMsgId"
            r6 = 0
            long r6 = r4.getLongExtra(r5, r6)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r18
            android.content.Intent r4 = r0.d     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r5 = "timestamps"
            r12 = 0
            long r4 = r4.getLongExtra(r5, r12)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r12.<init>()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r13 = "@"
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r12 = r12.append(r8)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r2 = r12.append(r2)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r12 = "@"
            java.lang.StringBuilder r2 = r2.append(r12)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r18
            android.content.Intent r12 = r0.d     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r13 = "accId"
            r14 = -1
            long r12 = r12.getLongExtra(r13, r14)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r18
            android.content.Context r14 = r0.c     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.util.List r14 = com.tencent.android.tpush.XGPushConfig.getAccessidList(r14)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            if (r14 == 0) goto L_0x0133
            int r15 = r14.size()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            if (r15 <= 0) goto L_0x0133
            java.lang.Long r15 = java.lang.Long.valueOf(r12)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            boolean r15 = r14.contains(r15)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            if (r15 != 0) goto L_0x0133
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r2.<init>()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r4 = "PushMessageRunnable match accessId failed, message droped cause accessId:"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r12)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r4 = " not in "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r14)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r4 = " msgId = "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r18
            java.lang.String r4 = r0.b     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            com.tencent.android.tpush.a.a.i(r4, r2)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r18
            android.content.Context r2 = r0.c     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            com.tencent.android.tpush.XGPushManager.msgAck(r2, r3)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            monitor-exit(r11)     // Catch:{ all -> 0x0099 }
            goto L_0x0085
        L_0x0133:
            r0 = r18
            android.content.Context r14 = r0.c     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r14 = com.tencent.android.tpush.service.d.e.a(r14, r12)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            boolean r15 = r14.contains(r2)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            if (r15 != 0) goto L_0x0272
            r0 = r18
            android.content.Context r15 = r0.c     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r16 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r16.<init>()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r17 = "tpush_msgId_"
            java.lang.StringBuilder r16 = r16.append(r17)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r16
            java.lang.StringBuilder r16 = r0.append(r12)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r16 = r16.toString()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r17.<init>()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r17
            java.lang.StringBuilder r17 = r0.append(r2)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r17
            java.lang.StringBuilder r17 = r0.append(r14)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r17 = r17.toString()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            com.tencent.android.tpush.common.m.b(r15, r16, r17)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r18
            android.content.Context r15 = r0.c     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r16 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r16.<init>()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r17 = "tpush_msgId_"
            java.lang.StringBuilder r16 = r16.append(r17)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r16
            java.lang.StringBuilder r16 = r0.append(r12)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r16 = r16.toString()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r17.<init>()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r17
            java.lang.StringBuilder r17 = r0.append(r2)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r17
            java.lang.StringBuilder r14 = r0.append(r14)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r14 = r14.toString()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r17 = 1
            r0 = r16
            r1 = r17
            com.tencent.android.tpush.service.d.e.a(r15, r0, r14, r1)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r0 = r18
            android.content.Context r14 = r0.c     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r15.<init>()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r16 = "tpush_msgId_"
            java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r12 = r15.append(r12)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r12 = r12.toString()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r13 = ""
            java.lang.String r12 = com.tencent.android.tpush.common.m.a(r14, r12, r13)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            boolean r12 = r12.contains(r2)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            if (r12 != 0) goto L_0x01e9
            r0 = r18
            java.lang.String r3 = r0.b     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r4.<init>()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r4 = " flag write failed"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            com.tencent.android.tpush.a.a.h(r3, r2)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            monitor-exit(r11)     // Catch:{ all -> 0x0099 }
            goto L_0x0085
        L_0x01e9:
            boolean r2 = com.tencent.android.tpush.XGPushConfig.enableDebug     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            if (r2 == 0) goto L_0x020b
            r0 = r18
            java.lang.String r2 = r0.b     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            r12.<init>()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r13 = "Receiver msg from server :"
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r13 = r3.toString()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r12 = r12.toString()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            com.tencent.android.tpush.a.a.e(r2, r12)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
        L_0x020b:
            r0 = r18
            android.content.Context r2 = r0.c     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            com.tencent.android.tpush.XGPushManager.msgAck(r2, r3)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            com.tencent.android.tpush.b.a r12 = r3.g()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            if (r12 == 0) goto L_0x0277
            java.lang.String r2 = r3.f()     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            boolean r2 = com.tencent.android.tpush.service.d.e.a(r2)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            if (r2 != 0) goto L_0x0277
            com.tencent.android.tpush.b.c r2 = new com.tencent.android.tpush.b.c     // Catch:{ Throwable -> 0x025c, JSONException -> 0x0267, IllegalArgumentException -> 0x0279 }
            r0 = r18
            android.content.Context r13 = r0.c     // Catch:{ Throwable -> 0x025c, JSONException -> 0x0267, IllegalArgumentException -> 0x0279 }
            r0 = r18
            android.content.Intent r14 = r0.d     // Catch:{ Throwable -> 0x025c, JSONException -> 0x0267, IllegalArgumentException -> 0x0279 }
            r2.<init>(r13, r14)     // Catch:{ Throwable -> 0x025c, JSONException -> 0x0267, IllegalArgumentException -> 0x0279 }
            r18.a()     // Catch:{ Throwable -> 0x025c, JSONException -> 0x0267, IllegalArgumentException -> 0x0279 }
            int r12 = r12.c()     // Catch:{ Throwable -> 0x025c, JSONException -> 0x0267, IllegalArgumentException -> 0x0279 }
            r13 = 1
            if (r12 != r13) goto L_0x0242
            boolean r2 = r2.a(r3, r4, r6, r8)     // Catch:{ Throwable -> 0x025c, JSONException -> 0x0267, IllegalArgumentException -> 0x0279 }
            if (r2 == 0) goto L_0x0242
            r3.a()     // Catch:{ Throwable -> 0x025c, JSONException -> 0x0267, IllegalArgumentException -> 0x0279 }
        L_0x0242:
            r2 = r10
        L_0x0243:
            r0 = r18
            com.tencent.android.tpush.XGIOperateCallback r3 = r0.e     // Catch:{ all -> 0x0099 }
            if (r3 == 0) goto L_0x0259
            if (r2 == 0) goto L_0x028d
            r0 = r18
            com.tencent.android.tpush.XGIOperateCallback r3 = r0.e     // Catch:{ all -> 0x0099 }
            java.lang.String r4 = ""
            r5 = -1
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0099 }
            r3.onFail(r4, r5, r2)     // Catch:{ all -> 0x0099 }
        L_0x0259:
            monitor-exit(r11)     // Catch:{ all -> 0x0099 }
            goto L_0x0085
        L_0x025c:
            r2 = move-exception
            r0 = r18
            java.lang.String r3 = r0.b     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            java.lang.String r4 = "unknown error"
            com.tencent.android.tpush.a.a.c(r3, r4, r2)     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
            goto L_0x0243
        L_0x0267:
            r2 = move-exception
            r0 = r18
            java.lang.String r3 = r0.b     // Catch:{ all -> 0x0099 }
            java.lang.String r4 = "push parse error"
            com.tencent.android.tpush.a.a.c(r3, r4, r2)     // Catch:{ all -> 0x0099 }
            goto L_0x0243
        L_0x0272:
            r2 = 0
            r0 = r18
            r0.e = r2     // Catch:{ JSONException -> 0x0267, IllegalArgumentException -> 0x0279, Throwable -> 0x0282 }
        L_0x0277:
            r2 = r10
            goto L_0x0243
        L_0x0279:
            r2 = move-exception
            java.lang.String r3 = "XGService"
            java.lang.String r4 = "push msg type error"
            com.tencent.android.tpush.a.a.c(r3, r4, r2)     // Catch:{ all -> 0x0099 }
            goto L_0x0243
        L_0x0282:
            r2 = move-exception
            r0 = r18
            java.lang.String r3 = r0.b     // Catch:{ all -> 0x0099 }
            java.lang.String r4 = "unknown error"
            com.tencent.android.tpush.a.a.c(r3, r4, r2)     // Catch:{ all -> 0x0099 }
            goto L_0x0243
        L_0x028d:
            r0 = r18
            com.tencent.android.tpush.XGIOperateCallback r2 = r0.e     // Catch:{ all -> 0x0099 }
            java.lang.String r3 = ""
            r4 = 0
            r2.onSuccess(r3, r4)     // Catch:{ all -> 0x0099 }
            goto L_0x0259
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.b.h.run():void");
    }
}
