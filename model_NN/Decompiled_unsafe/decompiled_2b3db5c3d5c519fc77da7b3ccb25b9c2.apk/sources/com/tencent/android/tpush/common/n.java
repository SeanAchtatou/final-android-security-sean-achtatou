package com.tencent.android.tpush.common;

import android.content.Context;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.c.a;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class n {
    private static n a = null;
    private Context b = null;
    private String c = null;
    private String d = null;

    private n(Context context) {
        this.b = context.getApplicationContext();
        this.c = p.f(context);
        this.d = String.valueOf(2.45f);
    }

    public static synchronized n a(Context context) {
        n nVar;
        synchronized (n.class) {
            if (a == null) {
                a = new n(context);
            }
            nVar = a;
        }
        return nVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, int):int
      com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, long):long */
    public String a() {
        int i;
        boolean z = false;
        JSONObject jSONObject = new JSONObject();
        try {
            e.a(jSONObject, "appVer", this.c);
            e.a(jSONObject, "appSdkVer", this.d);
            e.a(jSONObject, "ch", XGPushConfig.getInstallChannel(this.b));
            e.a(jSONObject, "gs", XGPushConfig.getGameServer(this.b));
            if (o.a(this.b).b() && a.d(this.b)) {
                String b2 = a.b();
                String c2 = a.c(this.b);
                com.tencent.android.tpush.a.a.e(Constants.OTHER_PUSH_TAG, "Reservert info: other push token is : " + c2 + "  other push type: " + b2);
                if (!p.b(b2) && !p.b(c2)) {
                    e.a(jSONObject, b2, c2);
                    z = true;
                }
            }
            if (!z) {
                com.tencent.android.tpush.a.a.e(Constants.OTHER_PUSH_TAG, "Reservert info: use normal xg token register");
                e.a(jSONObject, "gcm", "");
                e.a(jSONObject, "miid", "");
            }
            int a2 = m.a(this.b, ".firstregister", 1);
            int a3 = m.a(this.b, ".usertype", 0);
            long a4 = m.a(this.b, ".installtime", 0L);
            long currentTimeMillis = System.currentTimeMillis();
            if (a4 == 0) {
                m.b(this.b, ".installtime", currentTimeMillis);
                i = a3;
            } else {
                if (a3 == 0 && a2 != 1) {
                    if (!p.a(a4).equals(p.a(System.currentTimeMillis()))) {
                        m.b(this.b, ".usertype", 1);
                        currentTimeMillis = a4;
                        i = 1;
                    }
                }
                currentTimeMillis = a4;
                i = a3;
            }
            jSONObject.put("ut", i);
            if (a2 == 1) {
                jSONObject.put("freg", 1);
            }
            jSONObject.put("it", (int) (currentTimeMillis / 1000));
            if (p.b(this.b)) {
                jSONObject.put("aidl", 1);
            }
        } catch (Exception e) {
            com.tencent.android.tpush.a.a.c("RegisterReservedInfo", "toSting", e);
        }
        return jSONObject.toString();
    }
}
