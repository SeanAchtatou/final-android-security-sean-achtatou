package com.tencent.android.tpush.b;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.common.m;
import com.tencent.android.tpush.encrypt.Rijndael;
import com.tencent.android.tpush.service.channel.security.f;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class c {
    private static String f = null;
    private long a = 0;
    private long b = 0;
    private String c = "";
    private String d = null;
    private Context e = null;
    private Intent g = null;

    public c(Context context, Intent intent) {
        this.e = context.getApplicationContext();
        this.g = intent;
    }

    private boolean a() {
        if (f == null) {
            f = m.a(this.e, ".xg.push.cm.vrf", "");
        }
        if (f.contains(this.c)) {
            return true;
        }
        f = this.c + "," + f;
        if (f.length() > 10240) {
            f = f.substring(0, 2048);
        }
        m.b(this.e, ".xg.push.cm.vrf", f);
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, int):int
      com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, long):long */
    public boolean a(i iVar, long j, long j2, long j3) {
        String str;
        String str2;
        a g2 = iVar.g();
        if (j3 > 0) {
            JSONObject jSONObject = new JSONObject(Rijndael.decrypt(this.g.getStringExtra("title")));
            a.d(Constants.LogTag, "title encry obj:" + jSONObject);
            this.c = f.a(com.tencent.android.tpush.service.channel.security.a.a(jSONObject.getString("cipher"), 0));
            String[] split = this.c.split("_");
            this.b = Long.valueOf(split[0]).longValue();
            this.d = split[1].toUpperCase();
            this.a = Long.valueOf(split[2]).longValue();
            boolean z = true;
            if (this.b != j2) {
                z = false;
            } else if (j2 == 0) {
                z = j == this.a;
            }
            if (!z || a() || j2 != this.b || !g2.a().equalsIgnoreCase(this.d)) {
                return false;
            }
            return true;
        }
        StringBuilder sb = new StringBuilder(128);
        sb.append(iVar.c()).append(j3).append(this.e.getPackageName()).append(TextUtils.isEmpty(g2.e()) ? "" : g2.e()).append(TextUtils.isEmpty(g2.f()) ? "" : g2.f());
        String g3 = g2.g();
        if (TextUtils.isEmpty(g3) || new JSONObject(g3).length() > 0) {
            g3 = "";
        }
        sb.append(g3);
        if (g2 instanceof d) {
            e m = ((d) g2).m();
            StringBuilder append = sb.append(TextUtils.isEmpty(m.f) ? "" : m.f);
            if (TextUtils.isEmpty(m.d)) {
                str = "";
            } else {
                str = m.d;
            }
            StringBuilder append2 = append.append(str);
            if (TextUtils.isEmpty(m.b)) {
                str2 = "";
            } else {
                str2 = m.b;
            }
            append2.append(str2);
        }
        String sb2 = sb.toString();
        String str3 = Constants.LOCAL_MESSAGE_FLAG + com.tencent.android.tpush.encrypt.a.a(sb2);
        long a2 = m.a(this.e, str3, 0L);
        m.b(this.e, str3);
        long currentTimeMillis = a2 - System.currentTimeMillis();
        a.c(Constants.LogTag, sb2 + ",localMsgTag:" + str3 + ",diff:" + currentTimeMillis);
        if (currentTimeMillis >= 0) {
            return true;
        }
        return false;
    }
}
