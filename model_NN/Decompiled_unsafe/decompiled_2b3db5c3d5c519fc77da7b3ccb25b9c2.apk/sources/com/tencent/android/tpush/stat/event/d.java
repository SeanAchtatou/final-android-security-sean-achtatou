package com.tencent.android.tpush.stat.event;

import android.content.Context;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.stat.a.e;
import com.tencent.android.tpush.stat.a.h;
import org.json.JSONObject;

/* compiled from: ProGuard */
public abstract class d {
    public static String f = "xgsdk";
    protected static String h = null;
    protected String b = null;
    protected long c = 0;
    protected long d;
    protected int e;
    protected String g = null;
    protected long i = 0;
    protected Context j;

    public abstract boolean a(JSONObject jSONObject);

    public abstract EventType b();

    d(Context context, int i2, long j2) {
        this.b = "Axg" + j2;
        a(context, i2, j2);
    }

    public d(Context context, String str) {
        this.b = str;
        a(context, 0, this.c);
    }

    private void a(Context context, int i2, long j2) {
        this.j = context;
        this.c = j2;
        this.d = System.currentTimeMillis() / 1000;
        this.e = i2;
        this.g = e.b(context, j2);
        if (h == null || h.trim().length() < 40) {
            h = XGPushConfig.getToken(context);
            if (!e.b(h)) {
                h = "0";
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.stat.a.e.a(android.content.Context, boolean):int
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.android.tpush.stat.a.e.a(android.content.Context, long):java.lang.String
      com.tencent.android.tpush.stat.a.e.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.android.tpush.stat.a.e.a(android.content.Context, int):void
      com.tencent.android.tpush.stat.a.e.a(android.content.Context, boolean):int */
    public boolean b(JSONObject jSONObject) {
        try {
            h.a(jSONObject, "ky", this.b);
            jSONObject.put("et", b().a());
            jSONObject.put("ui", h.e(this.j));
            h.a(jSONObject, "mc", h.f(this.j));
            jSONObject.put("ut", 1);
            if (b() != EventType.SESSION_ENV) {
                h.a(jSONObject, "av", this.g);
                h.a(jSONObject, "ch", f);
            }
            h.a(jSONObject, "mid", h);
            jSONObject.put("si", this.e);
            if (b() == EventType.CUSTOM) {
                jSONObject.put("ts", this.i);
                jSONObject.put("cts", this.d);
            } else {
                jSONObject.put("ts", this.d);
            }
            jSONObject.put("sv", e.a(this.j, this.c));
            jSONObject.put("dts", e.a(this.j, false));
            return a(jSONObject);
        } catch (Throwable th) {
            return false;
        }
    }

    public String c() {
        try {
            JSONObject jSONObject = new JSONObject();
            b(jSONObject);
            return jSONObject.toString();
        } catch (Throwable th) {
            th.printStackTrace();
            return "";
        }
    }

    public String toString() {
        return c();
    }
}
