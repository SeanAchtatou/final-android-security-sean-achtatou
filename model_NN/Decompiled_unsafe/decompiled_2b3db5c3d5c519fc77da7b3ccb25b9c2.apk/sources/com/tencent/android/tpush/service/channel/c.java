package com.tencent.android.tpush.service.channel;

import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.horse.data.StrategyItem;
import com.tencent.android.tpush.horse.k;
import com.tencent.android.tpush.service.channel.a.d;
import com.tencent.android.tpush.service.channel.exception.ChannelException;
import com.tencent.android.tpush.service.d.e;
import com.tencent.android.tpush.service.m;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: ProGuard */
class c implements k {
    final /* synthetic */ b a;

    c(b bVar) {
        this.a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.channel.b.a(com.tencent.android.tpush.service.channel.b, boolean):boolean
     arg types: [com.tencent.android.tpush.service.channel.b, int]
     candidates:
      com.tencent.android.tpush.service.channel.b.a(com.tencent.android.tpush.service.channel.b, com.tencent.android.tpush.service.channel.a.a):com.tencent.android.tpush.service.channel.a.a
      com.tencent.android.tpush.service.channel.b.a(int, com.tencent.android.tpush.service.channel.o):void
      com.tencent.android.tpush.service.channel.b.a(com.tencent.android.tpush.service.channel.a.a, int):java.util.ArrayList
      com.tencent.android.tpush.service.channel.b.a(com.qq.taf.jce.JceStruct, com.tencent.android.tpush.service.channel.p):void
      com.tencent.android.tpush.service.channel.b.a(com.tencent.android.tpush.service.channel.a.a, com.tencent.android.tpush.service.channel.b.i):void
      com.tencent.android.tpush.service.channel.b.a(com.tencent.android.tpush.service.channel.a.a, com.tencent.android.tpush.service.channel.exception.ChannelException):void
      com.tencent.android.tpush.service.channel.a.b.a(com.tencent.android.tpush.service.channel.a.a, int):java.util.ArrayList
      com.tencent.android.tpush.service.channel.a.b.a(com.tencent.android.tpush.service.channel.a.a, com.tencent.android.tpush.service.channel.b.i):void
      com.tencent.android.tpush.service.channel.a.b.a(com.tencent.android.tpush.service.channel.a.a, com.tencent.android.tpush.service.channel.exception.ChannelException):void
      com.tencent.android.tpush.service.channel.b.a(com.tencent.android.tpush.service.channel.b, boolean):boolean */
    public void a(int i, String str) {
        a.h("TpnsChannel", "ICreateSocketChannelCallback onFailure(" + i + "," + str + ")");
        synchronized (this.a) {
            boolean unused = this.a.n = false;
            if (!this.a.f()) {
                a.e("TpnsChannel", "Connect to Xinge Server failed!");
                ChannelException channelException = new ChannelException(i, str);
                Iterator it = this.a.j.iterator();
                while (it.hasNext()) {
                    o oVar = (o) it.next();
                    if (oVar.d != null) {
                        oVar.d.a(oVar.c, channelException, a.a());
                    } else {
                        a.h("TpnsChannel", oVar.toString());
                    }
                }
                this.a.j.clear();
            }
            b.a = 0;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.channel.b.a(com.tencent.android.tpush.service.channel.b, boolean):boolean
     arg types: [com.tencent.android.tpush.service.channel.b, int]
     candidates:
      com.tencent.android.tpush.service.channel.b.a(com.tencent.android.tpush.service.channel.b, com.tencent.android.tpush.service.channel.a.a):com.tencent.android.tpush.service.channel.a.a
      com.tencent.android.tpush.service.channel.b.a(int, com.tencent.android.tpush.service.channel.o):void
      com.tencent.android.tpush.service.channel.b.a(com.tencent.android.tpush.service.channel.a.a, int):java.util.ArrayList
      com.tencent.android.tpush.service.channel.b.a(com.qq.taf.jce.JceStruct, com.tencent.android.tpush.service.channel.p):void
      com.tencent.android.tpush.service.channel.b.a(com.tencent.android.tpush.service.channel.a.a, com.tencent.android.tpush.service.channel.b.i):void
      com.tencent.android.tpush.service.channel.b.a(com.tencent.android.tpush.service.channel.a.a, com.tencent.android.tpush.service.channel.exception.ChannelException):void
      com.tencent.android.tpush.service.channel.a.b.a(com.tencent.android.tpush.service.channel.a.a, int):java.util.ArrayList
      com.tencent.android.tpush.service.channel.a.b.a(com.tencent.android.tpush.service.channel.a.a, com.tencent.android.tpush.service.channel.b.i):void
      com.tencent.android.tpush.service.channel.a.b.a(com.tencent.android.tpush.service.channel.a.a, com.tencent.android.tpush.service.channel.exception.ChannelException):void
      com.tencent.android.tpush.service.channel.b.a(com.tencent.android.tpush.service.channel.b, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.channel.b.b(com.tencent.android.tpush.service.channel.b, boolean):boolean
     arg types: [com.tencent.android.tpush.service.channel.b, int]
     candidates:
      com.tencent.android.tpush.service.channel.b.b(com.tencent.android.tpush.service.channel.a.a, com.tencent.android.tpush.service.channel.b.i):void
      com.tencent.android.tpush.service.channel.a.b.b(com.tencent.android.tpush.service.channel.a.a, com.tencent.android.tpush.service.channel.b.i):void
      com.tencent.android.tpush.service.channel.b.b(com.tencent.android.tpush.service.channel.b, boolean):boolean */
    public void a(SocketChannel socketChannel, StrategyItem strategyItem) {
        com.tencent.android.tpush.service.channel.a.a aVar;
        a.a("TpnsChannel", "ICreateSocketChannelCallback onSuccess(" + socketChannel + "," + socketChannel + ")");
        synchronized (this.a) {
            boolean unused = this.a.n = false;
            b.g = 0;
            try {
                if (!b.t.equals(strategyItem.a())) {
                    switch (e.e(m.e())) {
                        case 1:
                            b.f = b.d;
                            String unused2 = b.t = strategyItem.a();
                            break;
                        case 2:
                            b.f = b.c;
                            String unused3 = b.t = strategyItem.a();
                            break;
                        case 3:
                            b.f = b.c;
                            String unused4 = b.t = strategyItem.a();
                            break;
                        case 4:
                            b.f = b.c;
                            String unused5 = b.t = strategyItem.a();
                            break;
                        default:
                            String unused6 = b.t = strategyItem.a();
                            break;
                    }
                }
                b.a = 0;
                b bVar = this.a;
                if (strategyItem.i()) {
                    aVar = strategyItem.h() ? new d(socketChannel, b.a(), strategyItem.a(), strategyItem.b()) : new com.tencent.android.tpush.service.channel.a.c(socketChannel, b.a());
                } else {
                    aVar = new com.tencent.android.tpush.service.channel.a.a(socketChannel, b.a());
                }
                com.tencent.android.tpush.service.channel.a.a unused7 = bVar.m = aVar;
                this.a.a(true);
                this.a.m.start();
                this.a.k.clear();
                this.a.k.put(this.a.m, new ConcurrentHashMap());
                boolean unused8 = this.a.q = true;
            } catch (Exception e) {
                a.c(Constants.ServiceLogTag, "", e);
            }
        }
    }
}
