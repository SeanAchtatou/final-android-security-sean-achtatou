package com.tencent.android.tpush.service;

import com.tencent.android.tpush.a.a;

/* compiled from: ProGuard */
class aa implements Runnable {
    final /* synthetic */ XGWatchdog a;

    aa(XGWatchdog xGWatchdog) {
        this.a = xGWatchdog;
    }

    public void run() {
        try {
            String access$000 = this.a.directSendContent("ver:");
            Integer num = 0;
            if (access$000 != null) {
                try {
                    num = Integer.valueOf(access$000);
                } catch (NumberFormatException e) {
                }
            }
            if (num.intValue() <= 2) {
                String unused = this.a.directSendContent("exit:");
                String unused2 = this.a.directSendContent("exit1:");
                String unused3 = this.a.directSendContent("exit2:");
                Thread.sleep(5000);
                this.a.directStartWatchdog();
            }
            this.a.isStarted = true;
        } catch (Throwable th) {
            a.h(XGWatchdog.TAG, "jniStartWatchdog error:" + th.getMessage());
        }
    }
}
