package com.tencent.android.tpush.a;

import java.util.ArrayList;

/* compiled from: ProGuard */
final class b implements Runnable {
    final /* synthetic */ ArrayList a;

    b(ArrayList arrayList) {
        this.a = arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x0140 A[Catch:{ Exception -> 0x0144 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r7 = this;
            r1 = 0
            java.lang.String r0 = com.tencent.android.tpush.a.a.d()     // Catch:{ Exception -> 0x0150 }
            if (r0 != 0) goto L_0x001d
            java.util.ArrayList r0 = com.tencent.android.tpush.a.a.r     // Catch:{ Exception -> 0x0014 }
            r0.clear()     // Catch:{ Exception -> 0x0014 }
            if (r1 == 0) goto L_0x0013
            r1.close()     // Catch:{ Exception -> 0x0014 }
        L_0x0013:
            return
        L_0x0014:
            r0 = move-exception
            java.lang.String r1 = "XGLogger"
            java.lang.String r2 = "close file stream error"
            android.util.Log.e(r1, r2, r0)
            goto L_0x0013
        L_0x001d:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0150 }
            r2.<init>()     // Catch:{ Exception -> 0x0150 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r2 = java.io.File.separator     // Catch:{ Exception -> 0x0150 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r2 = "log"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r4 = r0.toString()     // Catch:{ Exception -> 0x0150 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0150 }
            r0.<init>()     // Catch:{ Exception -> 0x0150 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r2 = "-"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r2 = com.tencent.android.tpush.service.d.b.a()     // Catch:{ Exception -> 0x0150 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r2 = "_1.txt"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x0150 }
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0150 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0150 }
            java.io.File r0 = r2.getParentFile()     // Catch:{ Exception -> 0x0150 }
            r0.mkdirs()     // Catch:{ Exception -> 0x0150 }
            r0 = 2
            r6 = r0
            r0 = r2
            r2 = r3
            r3 = r6
        L_0x0068:
            boolean r5 = r0.exists()     // Catch:{ Exception -> 0x0150 }
            if (r5 == 0) goto L_0x0152
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0150 }
            r0.<init>()     // Catch:{ Exception -> 0x0150 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r2 = "-"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r2 = com.tencent.android.tpush.service.d.b.a()     // Catch:{ Exception -> 0x0150 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r2 = "_"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0150 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r2 = ".txt"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r2 = r0.toString()     // Catch:{ Exception -> 0x0150 }
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0150 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0150 }
            r5 = 10
            if (r3 <= r5) goto L_0x0113
            java.lang.String r3 = "XGLogger"
            java.lang.String r4 = "Unexpected error here, so many existed error file."
            android.util.Log.w(r3, r4)     // Catch:{ Exception -> 0x0150 }
            r3 = r2
        L_0x00aa:
            java.lang.String r2 = "XGLogger"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0150 }
            r4.<init>()     // Catch:{ Exception -> 0x0150 }
            java.lang.String r5 = "Write log file: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x0150 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ Exception -> 0x0150 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0150 }
            android.util.Log.v(r2, r0)     // Catch:{ Exception -> 0x0150 }
            java.io.BufferedWriter r2 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x0150 }
            java.io.FileWriter r0 = new java.io.FileWriter     // Catch:{ Exception -> 0x0150 }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0150 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0150 }
            java.util.ArrayList r0 = r7.a     // Catch:{ Exception -> 0x00f9, all -> 0x014d }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ Exception -> 0x00f9, all -> 0x014d }
        L_0x00d6:
            boolean r0 = r1.hasNext()     // Catch:{ Exception -> 0x00f9, all -> 0x014d }
            if (r0 == 0) goto L_0x0117
            java.lang.Object r0 = r1.next()     // Catch:{ Exception -> 0x00f9, all -> 0x014d }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x00f9, all -> 0x014d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f9, all -> 0x014d }
            r3.<init>()     // Catch:{ Exception -> 0x00f9, all -> 0x014d }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Exception -> 0x00f9, all -> 0x014d }
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x00f9, all -> 0x014d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00f9, all -> 0x014d }
            r2.write(r0)     // Catch:{ Exception -> 0x00f9, all -> 0x014d }
            goto L_0x00d6
        L_0x00f9:
            r0 = move-exception
            r1 = r2
        L_0x00fb:
            java.lang.String r2 = "XGLogger"
            java.lang.String r3 = "write logs to file error"
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x0136 }
            java.util.ArrayList r0 = com.tencent.android.tpush.a.a.r     // Catch:{ Exception -> 0x012d }
            r0.clear()     // Catch:{ Exception -> 0x012d }
            if (r1 == 0) goto L_0x010e
            r1.close()     // Catch:{ Exception -> 0x012d }
        L_0x010e:
            com.tencent.android.tpush.a.a.e()
            goto L_0x0013
        L_0x0113:
            int r3 = r3 + 1
            goto L_0x0068
        L_0x0117:
            java.util.ArrayList r0 = com.tencent.android.tpush.a.a.r     // Catch:{ Exception -> 0x0124 }
            r0.clear()     // Catch:{ Exception -> 0x0124 }
            if (r2 == 0) goto L_0x010e
            r2.close()     // Catch:{ Exception -> 0x0124 }
            goto L_0x010e
        L_0x0124:
            r0 = move-exception
            java.lang.String r1 = "XGLogger"
            java.lang.String r2 = "close file stream error"
            android.util.Log.e(r1, r2, r0)
            goto L_0x010e
        L_0x012d:
            r0 = move-exception
            java.lang.String r1 = "XGLogger"
            java.lang.String r2 = "close file stream error"
            android.util.Log.e(r1, r2, r0)
            goto L_0x010e
        L_0x0136:
            r0 = move-exception
        L_0x0137:
            java.util.ArrayList r2 = com.tencent.android.tpush.a.a.r     // Catch:{ Exception -> 0x0144 }
            r2.clear()     // Catch:{ Exception -> 0x0144 }
            if (r1 == 0) goto L_0x0143
            r1.close()     // Catch:{ Exception -> 0x0144 }
        L_0x0143:
            throw r0
        L_0x0144:
            r1 = move-exception
            java.lang.String r2 = "XGLogger"
            java.lang.String r3 = "close file stream error"
            android.util.Log.e(r2, r3, r1)
            goto L_0x0143
        L_0x014d:
            r0 = move-exception
            r1 = r2
            goto L_0x0137
        L_0x0150:
            r0 = move-exception
            goto L_0x00fb
        L_0x0152:
            r3 = r2
            goto L_0x00aa
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.a.b.run():void");
    }
}
