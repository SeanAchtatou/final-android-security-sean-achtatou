package com.tencent.android.tpush;

import android.content.Context;
import android.content.Intent;
import com.jg.EType;
import com.jg.JgMethodChecked;
import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.service.d.e;

/* compiled from: ProGuard */
final class r implements Runnable {
    final /* synthetic */ Context a;
    final /* synthetic */ boolean b;

    r(Context context, boolean z) {
        this.a = context;
        this.b = z;
    }

    @JgMethodChecked(author = 1, fComment = "确认已进行安全校验", lastDate = "20150316", reviewer = 3, vComment = {EType.RECEIVERCHECK, EType.INTENTCHECK})
    public void run() {
        int i = 1;
        a.a(this.a, 1);
        a.a(2);
        Context context = this.a;
        String str = "com.tencent.android.tpush.debug," + this.a.getPackageName();
        if (!this.b) {
            i = 0;
        }
        e.a(context, str, i);
        Intent intent = new Intent("com.tencent.android.tpush.action.ENABLE_DEBUG");
        intent.putExtra("debugMode", this.b);
        this.a.sendBroadcast(intent);
    }
}
