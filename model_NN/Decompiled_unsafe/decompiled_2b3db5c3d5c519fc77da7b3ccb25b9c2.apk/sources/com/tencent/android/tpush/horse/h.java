package com.tencent.android.tpush.horse;

import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.horse.data.OptStrategyList;
import com.tencent.android.tpush.horse.data.StrategyItem;
import com.tencent.android.tpush.service.cache.CacheManager;
import com.tencent.android.tpush.service.channel.exception.HorseIgnoreException;
import com.tencent.android.tpush.service.channel.exception.NullReturnException;
import com.tencent.android.tpush.service.d.e;
import com.tencent.android.tpush.service.m;
import java.util.ArrayList;

/* compiled from: ProGuard */
class h implements Runnable {
    final /* synthetic */ g a;

    h(g gVar) {
        this.a = gVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.horse.g.a(com.tencent.android.tpush.horse.g, boolean):boolean
     arg types: [com.tencent.android.tpush.horse.g, int]
     candidates:
      com.tencent.android.tpush.horse.g.a(com.tencent.android.tpush.horse.g, int):int
      com.tencent.android.tpush.horse.g.a(com.tencent.android.tpush.horse.g, long):long
      com.tencent.android.tpush.horse.g.a(int, java.lang.String):void
      com.tencent.android.tpush.horse.g.a(com.tencent.android.tpush.horse.g, java.lang.String):void
      com.tencent.android.tpush.horse.g.a(com.tencent.android.tpush.horse.g, boolean):boolean */
    public void run() {
        String g;
        synchronized (this) {
            if (XGPushConfig.enableDebug) {
                a.c(Constants.HorseLogTag, "Action ->  createOptimalSocketChannel run");
            }
            if (q.i().b() || f.i().b()) {
                a.c(Constants.HorseLogTag, ">> horse task running");
            } else {
                try {
                    g = e.g(m.e());
                    OptStrategyList optStrategyList = CacheManager.getOptStrategyList(m.e(), g);
                    StrategyItem e = optStrategyList.e();
                    if (e == null || e.a(optStrategyList.g())) {
                        this.a.a(g);
                        return;
                    }
                    long unused = this.a.g = System.currentTimeMillis();
                    if (e.d() == 0) {
                        if (XGPushConfig.enableDebug) {
                            a.c(Constants.HorseLogTag, "Using the optStrategyItem" + e.toString());
                        }
                        boolean unused2 = this.a.f = true;
                        ArrayList arrayList = new ArrayList();
                        e.a(0);
                        arrayList.add(e);
                        q.i().a(this.a.p);
                        q.i().a(arrayList);
                        q.i().g();
                    } else {
                        if (XGPushConfig.enableDebug) {
                            a.d(Constants.HorseLogTag, "Using Http chanel");
                        }
                        n nVar = new n();
                        nVar.a(e);
                        if (nVar.a().isConnected() && this.a.h != null) {
                            this.a.h.a(nVar.a(), e);
                        }
                    }
                } catch (NullReturnException e2) {
                    a.c(Constants.HorseLogTag, "createOptimalSocketChannel error", e2);
                    this.a.a(g);
                } catch (HorseIgnoreException e3) {
                    a.c(Constants.HorseLogTag, "createOptimalSocketChannel error", e3);
                    this.a.b();
                } catch (Exception e4) {
                    a.c(Constants.HorseLogTag, "createOptimalSocketChannel error", e4);
                    this.a.b();
                }
            }
        }
    }
}
