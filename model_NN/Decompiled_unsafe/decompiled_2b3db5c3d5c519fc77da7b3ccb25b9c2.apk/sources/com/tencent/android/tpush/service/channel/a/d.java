package com.tencent.android.tpush.service.channel.a;

import com.mol.seaplus.tool.connection.internet.InternetConnection;
import com.tencent.android.tpush.service.channel.b.b;
import java.nio.channels.SocketChannel;

/* compiled from: ProGuard */
public class d extends c {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(SocketChannel socketChannel, b bVar, String str, int i) {
        super(socketChannel, bVar, str, i, InternetConnection.HTTP_PROTOCOL + str + (i == 80 ? "" : ":" + i) + "/");
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return super.a();
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        if (this.f == null && super.b()) {
            ((b) this.f).a("X-Online-Host", this.m);
        }
        return this.f != null;
    }
}
