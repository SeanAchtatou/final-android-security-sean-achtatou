package com.tencent.android.tpush.service.b;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.jg.EType;
import com.jg.JgClassChecked;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.common.MessageKey;
import com.tencent.android.tpush.common.g;
import com.tencent.android.tpush.common.k;
import com.tencent.android.tpush.data.CachedMessageIntent;
import com.tencent.android.tpush.data.MessageId;
import com.tencent.android.tpush.data.PushClickEntity;
import com.tencent.android.tpush.encrypt.Rijndael;
import com.tencent.android.tpush.service.cache.CacheManager;
import com.tencent.android.tpush.service.channel.b;
import com.tencent.android.tpush.service.channel.protocol.TpnsClickClientReport;
import com.tencent.android.tpush.service.channel.protocol.TpnsPushClientReport;
import com.tencent.android.tpush.service.channel.protocol.TpnsPushMsg;
import com.tencent.android.tpush.service.d.e;
import com.tencent.android.tpush.service.m;
import com.tencent.android.tpush.service.p;
import com.tencent.android.tpush.service.u;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

@JgClassChecked(author = 1, fComment = "确认已进行安全校验", lastDate = "20150316", reviewer = 3, vComment = {EType.INTENTSCHEMECHECK, EType.INTENTCHECK, EType.RECEIVERCHECK})
/* compiled from: ProGuard */
public class a {
    public static long a = 306000;
    private static a b = new a();
    private static final byte[] c = new byte[0];
    private static long d = 0;
    /* access modifiers changed from: private */
    public static volatile boolean e = false;
    /* access modifiers changed from: private */
    public static volatile boolean f = false;
    /* access modifiers changed from: private */
    public static volatile boolean g = false;
    private PendingIntent h = null;

    private a() {
    }

    public static a a() {
        return b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.b.a.b(android.content.Context, java.util.List):java.util.ArrayList
     arg types: [android.content.Context, java.util.ArrayList]
     candidates:
      com.tencent.android.tpush.service.b.a.b(android.content.Context, com.tencent.android.tpush.data.MessageId):void
      com.tencent.android.tpush.service.b.a.b(android.content.Context, android.content.Intent):void
      com.tencent.android.tpush.service.b.a.b(android.content.Context, java.lang.String):void
      com.tencent.android.tpush.service.b.a.b(android.content.Context, java.util.ArrayList):void
      com.tencent.android.tpush.service.b.a.b(android.content.Context, java.util.List):java.util.ArrayList */
    /* access modifiers changed from: private */
    public void a(Context context, Long l) {
        long currentTimeMillis = System.currentTimeMillis();
        ArrayList b2 = b(context);
        if (e) {
            com.tencent.android.tpush.a.a.c("MessageManager", ">> msg ack is uploading , this time will give up! MessageId = " + l);
            return;
        }
        ArrayList b3 = b(context, (List) b2);
        if (b3 == null || b3.size() <= 0) {
            com.tencent.android.tpush.a.a.c("MessageManager", "Null report list with msgId " + l);
        } else {
            e = true;
            d = currentTimeMillis;
        }
        com.tencent.android.tpush.a.a.a(5, b3);
        p.a().a(b3, new b(this, b3, context));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.b.a.a(android.content.Context, java.lang.String, long, short):void
     arg types: [android.content.Context, java.lang.String, long, int]
     candidates:
      com.tencent.android.tpush.service.b.a.a(android.content.Context, java.lang.String, java.lang.String, java.util.ArrayList):void
      com.tencent.android.tpush.service.b.a.a(android.content.Context, com.tencent.android.tpush.service.channel.protocol.TpnsPushMsg, long, com.tencent.android.tpush.service.channel.a):void
      com.tencent.android.tpush.service.b.a.a(android.content.Context, java.lang.String, long, short):void */
    public synchronized void a(Context context, Intent intent) {
        if (!(context == null || intent == null)) {
            long longExtra = intent.getLongExtra(MessageKey.MSG_ID, -1);
            String stringExtra = intent.getStringExtra(Constants.FLAG_PACK_NAME);
            com.tencent.android.tpush.a.a.a(4, longExtra);
            b(context, stringExtra, longExtra);
            a(context, stringExtra, longExtra, (short) 1);
            a(context, Long.valueOf(longExtra));
        }
    }

    public void b(Context context, Intent intent) {
        if (context != null && intent != null) {
            String stringExtra = intent.getStringExtra(Constants.FLAG_PACK_NAME);
            long longExtra = intent.getLongExtra(MessageKey.MSG_ID, -1);
            if (longExtra <= 0) {
                com.tencent.android.tpush.a.a.a(Constants.ServiceLogTag, "@@ msgClick: Not add LocalMsg");
                return;
            }
            Context context2 = context;
            a(context2, stringExtra, new PushClickEntity(longExtra, intent.getLongExtra("accId", -1), intent.getLongExtra(MessageKey.MSG_BUSI_MSG_ID, -1), intent.getLongExtra(MessageKey.MSG_CREATE_TIMESTAMPS, -1), stringExtra, 1, intent.getLongExtra(Constants.FLAG_CLICK_TIME, System.currentTimeMillis() / 1000), intent.getIntExtra("action", 0)));
            c(context, intent);
        }
    }

    public void a(Context context, TpnsPushMsg tpnsPushMsg, long j, com.tencent.android.tpush.service.channel.a aVar) {
        long currentTimeMillis = System.currentTimeMillis();
        MessageId messageId = new MessageId();
        messageId.id = tpnsPushMsg.msgId;
        messageId.accessId = tpnsPushMsg.accessId;
        messageId.host = e.b(aVar.d());
        messageId.port = aVar.e();
        messageId.pact = p.a(aVar.b());
        messageId.apn = e.e(m.e());
        messageId.isp = e.f(m.e());
        messageId.pushTime = j;
        messageId.serviceHost = m.e().getPackageName();
        messageId.receivedTime = currentTimeMillis;
        messageId.pkgName = tpnsPushMsg.appPkgName;
        messageId.busiMsgId = tpnsPushMsg.busiMsgId;
        messageId.timestamp = tpnsPushMsg.timestamp;
        messageId.msgType = tpnsPushMsg.type;
        messageId.multiPkg = tpnsPushMsg.multiPkg;
        messageId.date = tpnsPushMsg.date;
        a(context, tpnsPushMsg.appPkgName, messageId);
        b(context, messageId);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.b.a.a(android.content.Context, java.util.List):java.util.ArrayList
     arg types: [android.content.Context, java.util.ArrayList]
     candidates:
      com.tencent.android.tpush.service.b.a.a(android.content.Context, java.lang.Long):void
      com.tencent.android.tpush.service.b.a.a(android.content.Context, com.tencent.android.tpush.data.MessageId):java.util.ArrayList
      com.tencent.android.tpush.service.b.a.a(android.content.Context, java.lang.String):java.util.ArrayList
      com.tencent.android.tpush.service.b.a.a(android.content.Context, android.content.Intent):void
      com.tencent.android.tpush.service.b.a.a(android.content.Context, java.util.ArrayList):void
      com.tencent.android.tpush.service.b.a.a(android.content.Context, java.util.List):java.util.ArrayList */
    /* access modifiers changed from: private */
    public synchronized void b(Context context, MessageId messageId) {
        Long valueOf;
        ArrayList a2 = a(context, messageId);
        if (f) {
            StringBuilder append = new StringBuilder().append("requestServiceAck ack is uploading , this time will give up!  msgId =  ");
            if (messageId == null) {
                valueOf = null;
            } else {
                valueOf = Long.valueOf(messageId.id);
            }
            com.tencent.android.tpush.a.a.c("MessageManager", append.append(valueOf).toString());
        } else {
            ArrayList a3 = a(context, (List) a2);
            if (a3 == null || a3.size() == 0) {
                com.tencent.android.tpush.a.a.c("MessageManager", "requestServiceAck with null list , give up this time");
            } else {
                f = true;
                p.a().a(a3, new c(this, messageId, context));
            }
        }
    }

    public void a(Context context, ArrayList arrayList) {
        ArrayList arrayList2;
        boolean z;
        synchronized (c) {
            if (!(context == null || arrayList == null)) {
                if (arrayList.size() > 0) {
                    try {
                        ArrayList e2 = e(context, "all");
                        if (e2 != null && e2.size() > 0) {
                            HashMap hashMap = new HashMap();
                            Iterator it = e2.iterator();
                            while (it.hasNext()) {
                                MessageId messageId = (MessageId) it.next();
                                ArrayList arrayList3 = (ArrayList) hashMap.get(messageId.pkgName);
                                if (arrayList3 == null) {
                                    ArrayList arrayList4 = new ArrayList();
                                    hashMap.put(messageId.pkgName, arrayList4);
                                    arrayList2 = arrayList4;
                                } else {
                                    arrayList2 = arrayList3;
                                }
                                int i = 0;
                                while (true) {
                                    int i2 = i;
                                    if (i2 >= arrayList.size()) {
                                        z = true;
                                        break;
                                    } else if (messageId.id == ((TpnsPushClientReport) arrayList.get(i2)).msgId) {
                                        arrayList.remove(i2);
                                        z = false;
                                        break;
                                    } else {
                                        i = i2 + 1;
                                    }
                                }
                                if (z) {
                                    arrayList2.add(messageId);
                                    hashMap.put(messageId.pkgName, arrayList2);
                                }
                            }
                            for (String str : hashMap.keySet()) {
                                a(context, str, (ArrayList) hashMap.get(str));
                            }
                        }
                    } catch (Exception e3) {
                        com.tencent.android.tpush.a.a.c("MessageManager", "+++ clear msg id exception", e3);
                    }
                }
            }
            com.tencent.android.tpush.a.a.h("MessageManager", "deleteServiceMsgIdBatch with null context or null list");
        }
    }

    public ArrayList a(Context context, List list) {
        ArrayList arrayList = null;
        if (list != null && list.size() > 0) {
            ArrayList arrayList2 = new ArrayList();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                MessageId messageId = (MessageId) it.next();
                TpnsPushClientReport tpnsPushClientReport = new TpnsPushClientReport();
                tpnsPushClientReport.accessId = messageId.accessId;
                tpnsPushClientReport.msgId = messageId.id;
                tpnsPushClientReport.apn = messageId.apn;
                tpnsPushClientReport.isp = messageId.isp;
                tpnsPushClientReport.locip = messageId.host;
                tpnsPushClientReport.locport = messageId.port;
                tpnsPushClientReport.pack = messageId.pact;
                tpnsPushClientReport.timeUs = messageId.pushTime;
                tpnsPushClientReport.qua = CacheManager.getQua(context, tpnsPushClientReport.accessId);
                tpnsPushClientReport.serviceHost = messageId.serviceHost;
                tpnsPushClientReport.confirmMs = System.currentTimeMillis() - messageId.receivedTime;
                tpnsPushClientReport.broadcastId = messageId.busiMsgId;
                tpnsPushClientReport.timestamp = messageId.timestamp;
                tpnsPushClientReport.type = messageId.msgType;
                tpnsPushClientReport.ackType = 1;
                tpnsPushClientReport.receiveTime = messageId.receivedTime / 1000;
                if (XGPushConfig.enableDebug) {
                    com.tencent.android.tpush.a.a.c("MessageManager", "Ack to server : @msgId=" + tpnsPushClientReport.msgId + " @accId=" + tpnsPushClientReport.accessId + " @timeUs=" + tpnsPushClientReport.timeUs + " @confirmMs=" + tpnsPushClientReport.confirmMs + " @recTime=" + messageId.receivedTime + " @msgType=" + messageId.msgType + " @broadcastId=" + tpnsPushClientReport.broadcastId);
                }
                arrayList2.add(tpnsPushClientReport);
                if (arrayList2.size() > 30) {
                    return arrayList2;
                }
            }
            arrayList = arrayList2;
        }
        return arrayList;
    }

    public ArrayList a(Context context, MessageId messageId) {
        ArrayList arrayList;
        synchronized (c) {
            arrayList = null;
            if (context != null) {
                boolean z = false;
                List<String> registerInfos = CacheManager.getRegisterInfos(context);
                if (registerInfos != null && registerInfos.size() > 0) {
                    ArrayList arrayList2 = new ArrayList();
                    for (String str : registerInfos) {
                        ArrayList e2 = e(context, str);
                        if (messageId == null || str.equals(messageId.pkgName)) {
                            z = true;
                        }
                        if (e2 != null && e2.size() > 0) {
                            arrayList2.addAll(e2);
                        }
                    }
                    arrayList = arrayList2;
                }
                if (!z) {
                    try {
                        ArrayList e3 = e(context, messageId.pkgName);
                        if (e3 != null && e3.size() > 0) {
                            arrayList.retainAll(e3);
                            if (arrayList.size() > 0) {
                                arrayList.removeAll(arrayList);
                                arrayList.addAll(e3);
                            } else {
                                arrayList.addAll(e3);
                            }
                        }
                    } catch (Exception e4) {
                    }
                }
                a(context, "all", arrayList);
            }
        }
        return arrayList;
    }

    public void a(Context context, String str, MessageId messageId) {
        synchronized (c) {
            if (context != null) {
                if (!e.a(str) && messageId != null) {
                    ArrayList e2 = e(context, str);
                    ArrayList arrayList = new ArrayList();
                    for (int i = 0; i < e2.size(); i++) {
                        MessageId messageId2 = (MessageId) e2.get(i);
                        if (messageId2.id == messageId.id) {
                            arrayList.add(messageId2);
                        }
                    }
                    e2.removeAll(arrayList);
                    e2.add(messageId);
                    a(context, str, e2);
                }
            }
        }
    }

    public void a(Context context, String str, ArrayList arrayList) {
        synchronized (c) {
            if (!(context == null || arrayList == null)) {
                a(context, str, ".tpns.msg.id.service", arrayList);
            }
        }
    }

    private ArrayList e(Context context, String str) {
        ArrayList arrayList;
        Object b2;
        if (context == null || e.a(str) || (b2 = b(context, str, ".tpns.msg.id.service")) == null) {
            arrayList = null;
        } else {
            arrayList = (ArrayList) b2;
        }
        if (arrayList == null) {
            return new ArrayList();
        }
        return arrayList;
    }

    public void b(Context context, ArrayList arrayList) {
        ArrayList arrayList2;
        boolean z;
        synchronized (c) {
            if (!(context == null || arrayList == null)) {
                if (arrayList.size() > 0) {
                    try {
                        ArrayList c2 = c(context);
                        if (c2 != null && c2.size() > 0) {
                            HashMap hashMap = new HashMap();
                            Iterator it = c2.iterator();
                            while (it.hasNext()) {
                                PushClickEntity pushClickEntity = (PushClickEntity) it.next();
                                ArrayList arrayList3 = (ArrayList) hashMap.get(pushClickEntity.pkgName);
                                if (arrayList3 == null) {
                                    ArrayList arrayList4 = new ArrayList();
                                    hashMap.put(pushClickEntity.pkgName, arrayList4);
                                    arrayList2 = arrayList4;
                                } else {
                                    arrayList2 = arrayList3;
                                }
                                int i = 0;
                                while (true) {
                                    int i2 = i;
                                    if (i2 >= arrayList.size()) {
                                        z = true;
                                        break;
                                    } else if (pushClickEntity.msgId == ((TpnsClickClientReport) arrayList.get(i2)).msgId) {
                                        arrayList.remove(i2);
                                        z = false;
                                        break;
                                    } else {
                                        i = i2 + 1;
                                    }
                                }
                                if (z) {
                                    arrayList2.add(pushClickEntity);
                                    hashMap.put(pushClickEntity.pkgName, arrayList2);
                                }
                            }
                            for (String str : hashMap.keySet()) {
                                b(context, str, (ArrayList) hashMap.get(str));
                            }
                        }
                    } catch (Exception e2) {
                        com.tencent.android.tpush.a.a.c("MessageManager", "+++ clear msg id exception", e2);
                    }
                }
            }
        }
    }

    public ArrayList a(Context context) {
        ArrayList arrayList = null;
        ArrayList<PushClickEntity> c2 = c(context);
        if (c2 != null && c2.size() > 0) {
            ArrayList arrayList2 = new ArrayList();
            for (PushClickEntity pushClickEntity : c2) {
                TpnsClickClientReport tpnsClickClientReport = new TpnsClickClientReport();
                tpnsClickClientReport.accessId = pushClickEntity.accessId;
                tpnsClickClientReport.msgId = pushClickEntity.msgId;
                tpnsClickClientReport.broadcastId = pushClickEntity.broadcastId;
                tpnsClickClientReport.timestamp = pushClickEntity.timestamp;
                tpnsClickClientReport.type = pushClickEntity.type;
                tpnsClickClientReport.clickTime = pushClickEntity.clickTime;
                tpnsClickClientReport.action = (long) pushClickEntity.action;
                arrayList2.add(tpnsClickClientReport);
                if (arrayList2.size() > 30) {
                    return arrayList2;
                }
            }
            arrayList = arrayList2;
        }
        return arrayList;
    }

    public void c(Context context, Intent intent) {
        if (!g) {
            ArrayList a2 = a(context);
            if (a2 == null || a2.size() <= 0) {
                g = false;
                return;
            }
            g = true;
            p.a().b(a2, new d(this, a2, context, intent));
        }
    }

    public ArrayList b(Context context) {
        List<String> registerInfos;
        if (context == null || (registerInfos = CacheManager.getRegisterInfos(context)) == null || registerInfos.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (String g2 : registerInfos) {
            ArrayList g3 = g(context, g2);
            if (g3 != null && g3.size() > 0) {
                arrayList.addAll(g3);
            }
        }
        return arrayList;
    }

    public ArrayList c(Context context) {
        List<String> registerInfos;
        if (context == null || (registerInfos = CacheManager.getRegisterInfos(context)) == null || registerInfos.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (String f2 : registerInfos) {
            ArrayList f3 = f(context, f2);
            if (f3 != null && f3.size() > 0) {
                arrayList.addAll(f3);
            }
        }
        return arrayList;
    }

    private ArrayList f(Context context, String str) {
        ArrayList arrayList;
        Object b2;
        if (context == null || e.a(str) || (b2 = b(context, str, ".tpns.msg.id.clicked")) == null) {
            arrayList = null;
        } else {
            arrayList = (ArrayList) b2;
        }
        if (arrayList == null) {
            return new ArrayList();
        }
        return arrayList;
    }

    public void a(Context context, String str, PushClickEntity pushClickEntity) {
        synchronized (c) {
            if (context != null) {
                if (!e.a(str) && pushClickEntity != null) {
                    ArrayList f2 = f(context, str);
                    ArrayList arrayList = new ArrayList();
                    for (int i = 0; i < f2.size(); i++) {
                        PushClickEntity pushClickEntity2 = (PushClickEntity) f2.get(i);
                        if (pushClickEntity2.msgId == pushClickEntity.msgId) {
                            arrayList.add(pushClickEntity2);
                        }
                    }
                    f2.removeAll(arrayList);
                    f2.add(pushClickEntity);
                    b(context, str, f2);
                }
            }
        }
    }

    public void b(Context context, String str, ArrayList arrayList) {
        synchronized (c) {
            if (!(context == null || arrayList == null)) {
                a(context, str, ".tpns.msg.id.clicked", arrayList);
            }
        }
    }

    public ArrayList b(Context context, List list) {
        ArrayList arrayList = new ArrayList();
        if (list != null && list.size() > 0) {
            ArrayList arrayList2 = new ArrayList();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                MessageId messageId = (MessageId) it.next();
                TpnsPushClientReport tpnsPushClientReport = new TpnsPushClientReport();
                tpnsPushClientReport.accessId = messageId.accessId;
                tpnsPushClientReport.msgId = messageId.id;
                tpnsPushClientReport.apn = messageId.apn;
                tpnsPushClientReport.isp = messageId.isp;
                tpnsPushClientReport.locip = messageId.host;
                tpnsPushClientReport.locport = messageId.port;
                tpnsPushClientReport.pack = messageId.pact;
                tpnsPushClientReport.timeUs = messageId.pushTime;
                tpnsPushClientReport.qua = CacheManager.getQua(context, tpnsPushClientReport.accessId);
                tpnsPushClientReport.serviceHost = messageId.serviceHost;
                tpnsPushClientReport.confirmMs = System.currentTimeMillis() - messageId.receivedTime;
                tpnsPushClientReport.broadcastId = messageId.busiMsgId;
                tpnsPushClientReport.timestamp = messageId.timestamp;
                tpnsPushClientReport.type = messageId.msgType;
                tpnsPushClientReport.receiveTime = messageId.receivedTime / 1000;
                arrayList2.add(tpnsPushClientReport);
                if (arrayList2.size() > 30) {
                    return arrayList2;
                }
            }
            arrayList = arrayList2;
        }
        return arrayList;
    }

    private ArrayList g(Context context, String str) {
        ArrayList arrayList;
        ArrayList a2;
        synchronized (c) {
            arrayList = null;
            if (context != null) {
                if (!e.a(str) && (a2 = a(context, str)) != null && a2.size() > 0) {
                    ArrayList arrayList2 = new ArrayList();
                    Iterator it = a2.iterator();
                    while (it.hasNext()) {
                        MessageId messageId = (MessageId) it.next();
                        if (messageId.a()) {
                            arrayList2.add(messageId);
                        } else if (!d(context, str, messageId.id)) {
                            arrayList2.add(messageId);
                        }
                    }
                    arrayList = arrayList2;
                }
            }
        }
        return arrayList;
    }

    public ArrayList a(Context context, String str) {
        ArrayList arrayList;
        Object b2;
        if (context == null || e.a(str) || (b2 = b(context, str, ".tpns.msg.id")) == null) {
            arrayList = null;
        } else {
            arrayList = (ArrayList) b2;
        }
        if (arrayList == null) {
            return new ArrayList();
        }
        return arrayList;
    }

    public void c(Context context, String str, ArrayList arrayList) {
        synchronized (c) {
            if (!(context == null || arrayList == null)) {
                a(context, str, ".tpns.msg.id", arrayList);
            }
        }
    }

    public void a(Context context, String str, long j, short s) {
        boolean z;
        synchronized (c) {
            boolean z2 = false;
            if (context != null && j > 0) {
                ArrayList a2 = a(context, str);
                if (a2 == null || a2.size() <= 0) {
                    com.tencent.android.tpush.a.a.a(12, j);
                } else {
                    Iterator it = a2.iterator();
                    while (it.hasNext()) {
                        MessageId messageId = (MessageId) it.next();
                        if (messageId.id == j) {
                            messageId.isAck = s;
                            z = true;
                        } else {
                            z = z2;
                        }
                        z2 = z;
                    }
                    if (z2) {
                        c(context, str, a2);
                    } else {
                        com.tencent.android.tpush.a.a.h("MessageManager", "updateMsgIdFlag Failed with no equal MessageId = " + j + " pkgName = " + str);
                        com.tencent.android.tpush.a.a.a(11, j);
                    }
                }
            }
        }
    }

    public synchronized void b(Context context, String str) {
        if (context == null) {
            com.tencent.android.tpush.a.a.h(Constants.ServiceLogTag, "clearLocalMsg context==null");
        } else {
            com.tencent.android.tpush.a.a.a(Constants.ServiceLogTag, "@@ clearLocalMsg(current pkg:" + context.getPackageName() + ",remote pkg:" + str + ")");
            ArrayList c2 = c(context, str);
            if (c2 != null && c2.size() > 0) {
                ArrayList arrayList = new ArrayList();
                for (int i = 0; i < c2.size(); i++) {
                    CachedMessageIntent cachedMessageIntent = (CachedMessageIntent) c2.get(i);
                    try {
                        String decrypt = Rijndael.decrypt(cachedMessageIntent.intent);
                        if (!e.a(decrypt) && Intent.parseUri(decrypt, 1).getLongExtra(MessageKey.MSG_ID, 0) < 0) {
                            arrayList.add(cachedMessageIntent);
                        }
                    } catch (Exception e2) {
                        com.tencent.android.tpush.a.a.h(Constants.ServiceLogTag, e2.getMessage());
                    }
                }
                c2.removeAll(arrayList);
                d(context, str, c2);
            }
        }
    }

    public ArrayList d(Context context) {
        List<String> registerInfos;
        if (context == null || (registerInfos = CacheManager.getRegisterInfos(context)) == null || registerInfos.size() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (String c2 : registerInfos) {
            ArrayList c3 = c(context, c2);
            if (c3 != null && c3.size() > 0) {
                arrayList.addAll(c3);
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0015 A[Catch:{ Throwable -> 0x001b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList c(android.content.Context r3, java.lang.String r4) {
        /*
            r2 = this;
            r1 = 0
            if (r3 == 0) goto L_0x0022
            boolean r0 = com.tencent.android.tpush.service.d.e.a(r4)     // Catch:{ Throwable -> 0x001b }
            if (r0 != 0) goto L_0x0022
            java.lang.String r0 = ".tpns.msg.id.cached"
            java.lang.Object r0 = r2.b(r3, r4, r0)     // Catch:{ Throwable -> 0x001b }
            if (r0 == 0) goto L_0x0022
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ Throwable -> 0x001b }
        L_0x0013:
            if (r0 != 0) goto L_0x001a
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Throwable -> 0x001b }
            r0.<init>()     // Catch:{ Throwable -> 0x001b }
        L_0x001a:
            return r0
        L_0x001b:
            r0 = move-exception
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            goto L_0x001a
        L_0x0022:
            r0 = r1
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.service.b.a.c(android.content.Context, java.lang.String):java.util.ArrayList");
    }

    public void a(Context context, String str, Intent intent) {
        ArrayList arrayList;
        synchronized (c) {
            if (context != null) {
                if (!e.a(str) && intent != null) {
                    CachedMessageIntent cachedMessageIntent = new CachedMessageIntent();
                    cachedMessageIntent.pkgName = str;
                    cachedMessageIntent.msgId = intent.getLongExtra(MessageKey.MSG_ID, -1);
                    cachedMessageIntent.intent = Rijndael.encrypt(intent.toUri(1));
                    ArrayList c2 = c(context, str);
                    if (c2 == null) {
                        arrayList = new ArrayList();
                    } else {
                        ArrayList arrayList2 = new ArrayList();
                        for (int i = 0; i < c2.size(); i++) {
                            CachedMessageIntent cachedMessageIntent2 = (CachedMessageIntent) c2.get(i);
                            if (cachedMessageIntent2.equals(cachedMessageIntent)) {
                                arrayList2.add(cachedMessageIntent2);
                            }
                        }
                        c2.removeAll(arrayList2);
                        arrayList = c2;
                    }
                    int size = arrayList.size() / 2;
                    if (size >= 100) {
                        com.tencent.android.tpush.a.a.f("MessageManager", "too much cache msg, try to cut " + size);
                        arrayList.subList(0, size).clear();
                    }
                    arrayList.add(cachedMessageIntent);
                    d(context, str, arrayList);
                }
            }
        }
    }

    public void d(Context context, String str, ArrayList arrayList) {
        synchronized (c) {
            if (!(context == null || arrayList == null)) {
                com.tencent.android.tpush.a.a.a(Constants.ServiceLogTag, "updateCachedMsgIntentByPkgName, size: " + arrayList.size());
                a(context, str, ".tpns.msg.id.cached", arrayList);
            }
        }
    }

    public void d(Context context, String str) {
        synchronized (c) {
            if (context != null) {
                d(context, str, new ArrayList());
            }
        }
    }

    public void a(Context context, String str, long j) {
        synchronized (c) {
            if (context != null) {
                ArrayList c2 = c(context, str);
                if (c2 != null && c2.size() > 0) {
                    ArrayList arrayList = new ArrayList();
                    for (int i = 0; i < c2.size(); i++) {
                        CachedMessageIntent cachedMessageIntent = (CachedMessageIntent) c2.get(i);
                        if (j == Intent.parseUri(Rijndael.decrypt(cachedMessageIntent.intent), 1).getLongExtra(MessageKey.MSG_BUSI_MSG_ID, -1)) {
                            arrayList.add(cachedMessageIntent);
                        }
                    }
                    c2.removeAll(arrayList);
                }
                d(context, str, c2);
            }
        }
    }

    public void b(Context context, String str, long j) {
        synchronized (c) {
            if (context != null) {
                ArrayList c2 = c(context, str);
                if (c2 != null && c2.size() > 0) {
                    ArrayList arrayList = new ArrayList();
                    for (int i = 0; i < c2.size(); i++) {
                        CachedMessageIntent cachedMessageIntent = (CachedMessageIntent) c2.get(i);
                        if (cachedMessageIntent.msgId == j) {
                            arrayList.add(cachedMessageIntent);
                        }
                    }
                    if (arrayList != null && arrayList.size() == 0) {
                        com.tencent.android.tpush.a.a.h("MessageManager", "deleteCachedMsgIntentByPkgName do not have MessageId = " + j);
                    }
                    c2.removeAll(arrayList);
                }
                d(context, str, c2);
            }
        }
    }

    public void a(Context context, List list, ArrayList arrayList) {
        synchronized (c) {
            if (!(context == null || list == null)) {
                if (list.size() > 0) {
                    try {
                        ArrayList arrayList2 = new ArrayList();
                        if (arrayList != null && arrayList.size() > 0) {
                            HashMap hashMap = new HashMap();
                            int i = 0;
                            while (true) {
                                int i2 = i;
                                if (i2 >= arrayList.size()) {
                                    break;
                                }
                                CachedMessageIntent cachedMessageIntent = (CachedMessageIntent) arrayList.get(i2);
                                Iterator it = list.iterator();
                                while (it.hasNext()) {
                                    CachedMessageIntent cachedMessageIntent2 = (CachedMessageIntent) it.next();
                                    if (cachedMessageIntent.equals(cachedMessageIntent2)) {
                                        arrayList2.add(cachedMessageIntent);
                                        ArrayList arrayList3 = (ArrayList) hashMap.get(cachedMessageIntent2.pkgName);
                                        if (arrayList3 == null) {
                                            arrayList3 = new ArrayList();
                                        }
                                        hashMap.put(cachedMessageIntent2.pkgName, arrayList3);
                                    }
                                }
                                i = i2 + 1;
                            }
                            arrayList.removeAll(arrayList2);
                            Iterator it2 = arrayList.iterator();
                            while (it2.hasNext()) {
                                CachedMessageIntent cachedMessageIntent3 = (CachedMessageIntent) it2.next();
                                ArrayList arrayList4 = (ArrayList) hashMap.get(cachedMessageIntent3.pkgName);
                                if (arrayList4 == null) {
                                    arrayList4 = new ArrayList();
                                }
                                arrayList4.add(cachedMessageIntent3);
                                hashMap.put(cachedMessageIntent3.pkgName, arrayList4);
                            }
                            for (String str : hashMap.keySet()) {
                                d(context, str, (ArrayList) hashMap.get(str));
                            }
                        }
                    } catch (Exception e2) {
                        com.tencent.android.tpush.a.a.c("MessageManager", "deleteCachedMsgIntent", e2);
                    }
                }
            }
        }
    }

    public void c(Context context, List list) {
        ArrayList arrayList;
        boolean z;
        synchronized (c) {
            if (!(context == null || list == null)) {
                if (list.size() > 0) {
                    try {
                        ArrayList b2 = b(context);
                        if (b2 != null && b2.size() > 0) {
                            HashMap hashMap = new HashMap();
                            Iterator it = b2.iterator();
                            while (it.hasNext()) {
                                MessageId messageId = (MessageId) it.next();
                                ArrayList arrayList2 = (ArrayList) hashMap.get(messageId.pkgName);
                                if (arrayList2 == null) {
                                    ArrayList arrayList3 = new ArrayList();
                                    hashMap.put(messageId.pkgName, arrayList3);
                                    arrayList = arrayList3;
                                } else {
                                    arrayList = arrayList2;
                                }
                                int i = 0;
                                while (true) {
                                    int i2 = i;
                                    if (i2 >= list.size()) {
                                        z = true;
                                        break;
                                    } else if (messageId.id == ((TpnsPushClientReport) list.get(i2)).msgId) {
                                        z = false;
                                        break;
                                    } else {
                                        i = i2 + 1;
                                    }
                                }
                                if (z) {
                                    arrayList.add(messageId);
                                    hashMap.put(messageId.pkgName, arrayList);
                                }
                            }
                            for (String str : hashMap.keySet()) {
                                c(context, str, (ArrayList) hashMap.get(str));
                            }
                        }
                    } catch (Exception e2) {
                        com.tencent.android.tpush.a.a.c("MessageManager", "deleteMsgIdBatch", e2);
                    }
                }
            }
        }
    }

    public void b(Context context, String str, MessageId messageId) {
        ArrayList arrayList;
        synchronized (c) {
            if (context != null) {
                if (!e.a(str) && messageId != null) {
                    ArrayList a2 = a(context, str);
                    if (a2 != null) {
                        int i = 0;
                        while (true) {
                            if (i >= a2.size()) {
                                arrayList = a2;
                                break;
                            } else if (((MessageId) a2.get(i)).id == messageId.id) {
                                a2.remove(i);
                                arrayList = a2;
                                break;
                            } else {
                                i++;
                            }
                        }
                    } else {
                        arrayList = new ArrayList();
                    }
                    arrayList.add(messageId);
                    c(context, str, arrayList);
                }
            }
        }
    }

    public boolean c(Context context, String str, long j) {
        ArrayList<MessageId> a2;
        if (context != null && !e.a(str) && j > 0 && (a2 = a(context, str)) != null && a2.size() > 0) {
            for (MessageId messageId : a2) {
                if (messageId.id == j) {
                    return messageId.a();
                }
            }
        }
        return false;
    }

    public boolean d(Context context, String str, long j) {
        ArrayList<CachedMessageIntent> c2;
        if (context != null && !e.a(str) && (c2 = c(context, str)) != null && c2.size() > 0) {
            for (CachedMessageIntent cachedMessageIntent : c2) {
                if (cachedMessageIntent.msgId == j && str.equals(cachedMessageIntent.pkgName)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void a(Intent intent, String str, MessageId messageId) {
        List a2;
        com.tencent.android.tpush.a.a.h("MessageManager", "dispatchMessageOnTime");
        long j = intent.getExtras().getLong(MessageKey.MSG_CREATE_MULTIPKG);
        long j2 = intent.getExtras().getLong("accId");
        List arrayList = new ArrayList();
        if (j == 0) {
            arrayList.add(intent.getPackage());
            a2 = arrayList;
        } else {
            a2 = j.a().a(m.e(), j2);
        }
        com.tencent.android.tpush.a.a.h("MessageManager", "dispatchMessageOnTime pkgs " + a2.size());
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < a2.size()) {
                try {
                    String str2 = (String) a2.get(i2);
                    if (e.a(str2)) {
                        com.tencent.android.tpush.a.a.c("MessageManager", ">> msg.appPkgName is null!");
                    } else if (!e.a(m.e(), str2, j2)) {
                        com.tencent.android.tpush.a.a.h("MessageManager", "dispatchMessageOnTime appPkgName " + str2 + " is not installed.");
                        p.a().a(str2);
                        j.a().a(m.e(), str2);
                        d(m.e(), str2, new ArrayList());
                    } else {
                        com.tencent.android.tpush.data.a registerInfoByPkgName = CacheManager.getRegisterInfoByPkgName(str2);
                        if (registerInfoByPkgName != null) {
                            if (registerInfoByPkgName.e > 0) {
                                d(m.e(), str2, new ArrayList());
                            } else if (!d(m.e(), str2, messageId.id) && !e.a(m.e(), registerInfoByPkgName.a).contains("@" + messageId.id + str2 + "@")) {
                                if (c(m.e(), str2, messageId.id)) {
                                    com.tencent.android.tpush.a.a.h(Constants.ServiceLogTag, ">> msgId:" + messageId.id + " has been acked.");
                                } else {
                                    intent.setPackage(str2);
                                    messageId.pkgName = str2;
                                    if (messageId.id > 0) {
                                        b(m.e(), str2, messageId);
                                    }
                                    a(m.e(), str, intent);
                                    c();
                                    a(messageId.date, intent, str2);
                                }
                            }
                        }
                    }
                } catch (Exception e2) {
                    com.tencent.android.tpush.a.a.c("MessageManager", "dispatchMessageOnTime", e2);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void a(String str, Intent intent, String str2) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        if (e.a(str) || (!e.a(str) && simpleDateFormat.parse(str).compareTo(simpleDateFormat.parse(simpleDateFormat.format(new Date()))) == 0)) {
            if (e.a(intent)) {
                List a2 = e.a(m.e(), str2 + Constants.RPC_SUFFIX);
                if (a2 == null || a2.size() < 1) {
                    if (XGPushConfig.enableDebug) {
                        com.tencent.android.tpush.a.a.c(Constants.ServiceLogTag, ">> send message intent:" + intent);
                    }
                    m.e().sendBroadcast(intent);
                    return;
                }
                com.tencent.android.tpush.a.a.c(Constants.ServiceLogTag, ">> send rpc message intent:" + intent);
                a(intent);
            }
        } else if (!e.a(str) && simpleDateFormat.parse(str).compareTo(simpleDateFormat.parse(simpleDateFormat.format(new Date()))) < 0) {
            List a3 = e.a(m.e(), str2 + Constants.RPC_SUFFIX);
            if (a3 == null || a3.size() < 1) {
                m.e().sendBroadcast(intent);
            } else {
                a(intent);
            }
        }
    }

    /* JADX WARN: Type inference failed for: r1v4, types: [java.lang.String[], java.io.Serializable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void a(TpnsPushMsg tpnsPushMsg, long j, com.tencent.android.tpush.service.channel.a aVar) {
        long j2;
        long currentTimeMillis = System.currentTimeMillis();
        MessageId messageId = new MessageId();
        messageId.id = tpnsPushMsg.msgId;
        messageId.isAck = 0;
        messageId.accessId = tpnsPushMsg.accessId;
        messageId.host = e.b(aVar.d());
        messageId.port = aVar.e();
        messageId.pact = p.a(aVar.b());
        messageId.apn = e.e(m.e());
        messageId.isp = e.f(m.e());
        messageId.pushTime = j;
        messageId.serviceHost = m.e().getPackageName();
        messageId.receivedTime = currentTimeMillis;
        messageId.pkgName = tpnsPushMsg.appPkgName;
        messageId.busiMsgId = tpnsPushMsg.busiMsgId;
        messageId.timestamp = tpnsPushMsg.timestamp;
        messageId.msgType = tpnsPushMsg.type;
        messageId.multiPkg = tpnsPushMsg.multiPkg;
        messageId.date = tpnsPushMsg.date;
        long j3 = 259200000;
        if (tpnsPushMsg.ttl > 0) {
            j3 = (long) (tpnsPushMsg.ttl * 1000);
        } else if (tpnsPushMsg.msgId > 0 && tpnsPushMsg.ttl == 0) {
            j3 = 30000;
        }
        if (tpnsPushMsg.serverTime <= 0 || tpnsPushMsg.timestamp <= 0) {
            j2 = j3 + currentTimeMillis;
        } else {
            j2 = j3 + ((tpnsPushMsg.serverTime - tpnsPushMsg.timestamp) * 1000) + currentTimeMillis;
        }
        com.tencent.android.tpush.a.a.h("confirmMs", ">> msg distribute @msgId=" + messageId.id + " @accId=" + messageId.accessId + " @timeUs=" + j + " @recTime=" + messageId.receivedTime + " @msg.date=" + tpnsPushMsg.date + " @msg.busiMsgId=" + tpnsPushMsg.busiMsgId + " @msg.timestamp=" + tpnsPushMsg.timestamp + " @msg.type=" + tpnsPushMsg.type + " @msg.multiPkg=" + tpnsPushMsg.multiPkg + " @msg.serverTime=" + tpnsPushMsg.serverTime + " @msg.ttl=" + tpnsPushMsg.ttl + " @expire_time=" + j2 + " @currentTimeMillis=" + currentTimeMillis);
        Intent intent = new Intent(Constants.ACTION_INTERNAL_PUSH_MESSAGE);
        intent.setPackage(tpnsPushMsg.appPkgName);
        intent.putExtra(MessageKey.MSG_ID, tpnsPushMsg.msgId);
        intent.putExtra("title", Rijndael.encrypt(tpnsPushMsg.title));
        intent.putExtra(MessageKey.MSG_CONTENT, Rijndael.encrypt(tpnsPushMsg.content));
        intent.putExtra(MessageKey.MSG_DATE, tpnsPushMsg.date);
        intent.putExtra("type", tpnsPushMsg.type);
        intent.putExtra("accId", tpnsPushMsg.accessId);
        intent.putExtra(MessageKey.MSG_BUSI_MSG_ID, tpnsPushMsg.busiMsgId);
        intent.putExtra(MessageKey.MSG_CREATE_TIMESTAMPS, tpnsPushMsg.timestamp);
        intent.putExtra(MessageKey.MSG_CREATE_MULTIPKG, tpnsPushMsg.multiPkg);
        intent.putExtra(MessageKey.MSG_SERVER_TIME, tpnsPushMsg.serverTime * 1000);
        intent.putExtra(MessageKey.MSG_TIME_GAP, currentTimeMillis - (tpnsPushMsg.serverTime * 1000));
        intent.putExtra(MessageKey.MSG_TTL, tpnsPushMsg.ttl * 1000);
        intent.putExtra(MessageKey.MSG_EXPIRE_TIME, j2);
        intent.putExtra(MessageKey.MSG_SERVICE_ACK, true);
        intent.putExtra(MessageKey.MSG_SERVICE_PACKAGE_NAME, m.f());
        try {
            intent.putExtra("enKeySet", k.a((Serializable) new String[]{"title", MessageKey.MSG_CONTENT}));
        } catch (Exception e2) {
            com.tencent.android.tpush.a.a.c("MessageManager", "distribute2SDK", e2);
        }
        a(intent, tpnsPushMsg.appPkgName, messageId);
    }

    public void a(ArrayList arrayList, long j, com.tencent.android.tpush.service.channel.a aVar) {
        b(arrayList, j, aVar);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void a(Context context, String str, String str2) {
        try {
            JSONObject jSONObject = new JSONObject(str2);
            switch (jSONObject.optInt("action", 2)) {
                case 1:
                    for (String valueOf : jSONObject.optString("pushIdList", "").split(",")) {
                        a(context, str, Long.valueOf(valueOf).longValue());
                    }
                    return;
                case 2:
                    d(context, str);
                    return;
                case 3:
                    int optInt = jSONObject.optInt("enabled", -1);
                    com.tencent.android.tpush.a.a.e("MessageManager", "setLogToFile with cmd = " + optInt);
                    com.tencent.android.tpush.a.a.a(optInt);
                    return;
                default:
                    return;
            }
        } catch (Exception e2) {
            com.tencent.android.tpush.a.a.c("MessageManager", "onCrtlMsgHandle", e2);
        }
        com.tencent.android.tpush.a.a.c("MessageManager", "onCrtlMsgHandle", e2);
    }

    public void a(TpnsPushMsg tpnsPushMsg) {
        com.tencent.android.tpush.data.a registerInfoByPkgName;
        String str = tpnsPushMsg.appPkgName;
        if (tpnsPushMsg.multiPkg == 1 && (registerInfoByPkgName = CacheManager.getRegisterInfoByPkgName(str)) != null && !e.a(registerInfoByPkgName.d)) {
            str = registerInfoByPkgName.d;
        }
        a(m.e(), str, tpnsPushMsg.content);
    }

    public void b(ArrayList arrayList, long j, com.tencent.android.tpush.service.channel.a aVar) {
        if (!(m.e() == null || arrayList == null || arrayList.size() <= 0)) {
            com.tencent.android.tpush.a.a.b(0, arrayList);
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                TpnsPushMsg tpnsPushMsg = (TpnsPushMsg) it.next();
                com.tencent.android.tpush.a.a.c("MessageManager", "distributeFromServer : accid=" + tpnsPushMsg.accessId + ",busiId=" + tpnsPushMsg.busiMsgId + ",pkg=" + tpnsPushMsg.appPkgName + ",msgId=" + tpnsPushMsg.msgId + ",type=" + tpnsPushMsg.type + ",ts=" + tpnsPushMsg.timestamp + ",multi=" + tpnsPushMsg.multiPkg + ",date=" + tpnsPushMsg.date + ",serverTime=" + tpnsPushMsg.serverTime + ",ttl=" + tpnsPushMsg.ttl);
                a(m.e(), tpnsPushMsg, j, aVar);
                if (e.a(tpnsPushMsg.appPkgName)) {
                    com.tencent.android.tpush.a.a.c("MessageManager", ">> messageDistribute, msg.appPkgName is null!");
                } else {
                    if (tpnsPushMsg.type == 3) {
                        a(tpnsPushMsg);
                    }
                    if (tpnsPushMsg.timestamp > 0) {
                        long currentTimeMillis = System.currentTimeMillis() / 1000;
                        long j2 = (currentTimeMillis - (currentTimeMillis - tpnsPushMsg.serverTime)) - tpnsPushMsg.timestamp;
                        if (tpnsPushMsg.msgId >= 0 && tpnsPushMsg.ttl > 0 && ((long) tpnsPushMsg.ttl) < j2) {
                            com.tencent.android.tpush.a.a.h("MessageManager", "messageDistribute check server time failed, msg discarded cause msg is timeout, msg.ttl:" + tpnsPushMsg.ttl + "<reviseMaxTimeoutSec:" + j2);
                        }
                    }
                    a(tpnsPushMsg, j, aVar);
                }
            }
        }
        com.tencent.android.tpush.service.c.a.a(arrayList);
    }

    /* access modifiers changed from: private */
    public synchronized void b() {
        if (b.a().b(true) > 0) {
            c();
        }
    }

    private void c() {
        if (this.h == null) {
            m.e().registerReceiver(new f(this), new IntentFilter("com.tencent.android.tpush.service.channel.cacheMsgBeatIntent"));
            this.h = PendingIntent.getBroadcast(m.e(), 0, new Intent("com.tencent.android.tpush.service.channel.cacheMsgBeatIntent"), 134217728);
        }
        u.a().a(0, System.currentTimeMillis() + a, this.h);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, long, boolean):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean */
    private void a(Context context, String str, String str2, ArrayList arrayList) {
        try {
            if (arrayList.size() > 50) {
                arrayList.subList(0, 10).clear();
            }
            String encrypt = Rijndael.encrypt(k.a(arrayList));
            if (str2.equals(".tpns.msg.id.cached")) {
                e.a(context, str + str2, encrypt, false);
            } else {
                e.a(context, str + str2, encrypt, true);
            }
        } catch (Exception e2) {
            com.tencent.android.tpush.a.a.c("MessageManager", "putSettings", e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, boolean):java.lang.String
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.service.d.e.a(int, java.lang.String, int):android.content.Intent
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, float):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, int):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, long):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, boolean):java.lang.String */
    private Object b(Context context, String str, String str2) {
        String a2;
        try {
            if (str2.equals(".tpns.msg.id.cached")) {
                a2 = e.a(context, str + str2, false);
            } else {
                a2 = e.a(context, str + str2, true);
            }
            return k.a(Rijndael.decrypt(a2));
        } catch (Exception e2) {
            com.tencent.android.tpush.a.a.c("MessageManager", "getSettings", e2);
            return null;
        }
    }

    public void a(Intent intent) {
        g.a().a(new g(this, intent));
    }
}
