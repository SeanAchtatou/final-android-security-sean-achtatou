package com.tencent.android.tpush.service;

/* compiled from: ProGuard */
class ac implements Comparable {
    public String a = "";
    public float b = 1.0f;
    public long c = 0;

    ac() {
    }

    /* renamed from: a */
    public int compareTo(ac acVar) {
        if (this.b > acVar.b) {
            return -1;
        }
        if (this.b < acVar.b) {
            return 1;
        }
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("pkgName:").append(this.a).append(",accid:").append(this.c).append(",ver:").append(this.b);
        return sb.toString();
    }
}
