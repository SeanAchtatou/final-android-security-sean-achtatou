package com.tencent.android.tpush;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.tencent.android.tpush.encrypt.Rijndael;
import com.tencent.android.tpush.service.channel.security.TpnsSecurity;

/* compiled from: ProGuard */
final class p implements Runnable {
    final /* synthetic */ Context a;
    final /* synthetic */ long b;

    p(Context context, long j) {
        this.a = context;
        this.b = j;
    }

    public void run() {
        SharedPreferences defaultSharedPreferences;
        if (TpnsSecurity.checkTpnsSecurityLibSo(this.a) && (defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.a)) != null) {
            SharedPreferences.Editor edit = defaultSharedPreferences.edit();
            edit.putString(XGPushConfig.TPUSH_ACCESS_ID, Rijndael.encrypt(String.valueOf(this.b)));
            edit.commit();
        }
    }
}
