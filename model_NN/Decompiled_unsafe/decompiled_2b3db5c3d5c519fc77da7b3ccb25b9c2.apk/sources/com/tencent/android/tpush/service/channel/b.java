package com.tencent.android.tpush.service.channel;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import com.jg.EType;
import com.jg.JgClassChecked;
import com.qq.taf.jce.JceStruct;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.common.MessageKey;
import com.tencent.android.tpush.common.p;
import com.tencent.android.tpush.data.CachedMessageIntent;
import com.tencent.android.tpush.encrypt.Rijndael;
import com.tencent.android.tpush.horse.g;
import com.tencent.android.tpush.horse.k;
import com.tencent.android.tpush.horse.l;
import com.tencent.android.tpush.service.XGWatchdog;
import com.tencent.android.tpush.service.b.j;
import com.tencent.android.tpush.service.cache.CacheManager;
import com.tencent.android.tpush.service.channel.a.a;
import com.tencent.android.tpush.service.channel.a.c;
import com.tencent.android.tpush.service.channel.b.h;
import com.tencent.android.tpush.service.channel.b.i;
import com.tencent.android.tpush.service.channel.exception.ChannelException;
import com.tencent.android.tpush.service.channel.protocol.TpnsPushVerifyReq;
import com.tencent.android.tpush.service.channel.protocol.TpnsReconnectReq;
import com.tencent.android.tpush.service.d.e;
import com.tencent.android.tpush.service.m;
import com.tencent.android.tpush.service.u;
import com.tencent.android.tpush.service.w;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@JgClassChecked(author = 1, fComment = "确认已进行安全校验", lastDate = "20150316", reviewer = 3, vComment = {EType.RECEIVERCHECK, EType.INTENTCHECK, EType.INTENTSCHEMECHECK})
/* compiled from: ProGuard */
public final class b implements l, com.tencent.android.tpush.service.channel.a.b {
    public static int a = 0;
    public static int b = 0;
    public static int c = 290000;
    public static int d = 180000;
    public static int e = 300000;
    public static int f = c;
    protected static int g = 0;
    protected static Boolean h = null;
    private static volatile long r = 0;
    private static volatile long s = 0;
    /* access modifiers changed from: private */
    public static String t = "";
    /* access modifiers changed from: private */
    public Handler i;
    /* access modifiers changed from: private */
    public ArrayList j;
    /* access modifiers changed from: private */
    public Map k;
    /* access modifiers changed from: private */
    public Map l;
    /* access modifiers changed from: private */
    public a m;
    /* access modifiers changed from: private */
    public volatile boolean n;
    private PendingIntent o;
    /* access modifiers changed from: private */
    public o p;
    /* access modifiers changed from: private */
    public volatile boolean q;
    private k u;
    private Handler v;
    /* access modifiers changed from: private */
    public p w;

    /* synthetic */ b(c cVar) {
        this();
    }

    public static b a() {
        return n.a;
    }

    private b() {
        this.i = null;
        this.j = new ArrayList();
        this.k = new ConcurrentHashMap();
        this.l = new ConcurrentHashMap();
        this.m = null;
        this.n = false;
        this.o = null;
        this.p = null;
        this.q = true;
        this.u = new c(this);
        this.v = new d(this);
        this.w = new e(this);
        g.a().a(this);
        this.i = com.tencent.android.tpush.common.g.a().b();
    }

    public void b() {
        e();
    }

    public void c() {
        this.n = false;
        if (this.m != null) {
            this.m.c();
            this.m = null;
        }
    }

    public void d() {
        c();
        e();
    }

    public void e() {
        if (XGPushConfig.enableDebug) {
            com.tencent.android.tpush.a.a.a("TpnsChannel", "Action -> checkAndSetupClient( tpnsClient = " + this.m + ", isClientCreating = " + this.n + ")");
        }
        synchronized (this) {
            if (this.m == null && !this.n) {
                this.n = true;
                try {
                    g.a().a(this.u);
                } catch (Exception e2) {
                    com.tencent.android.tpush.a.a.c("TpnsChannel", "createOptimalSocketChannel error", e2);
                }
            } else if (!this.n && this.m != null && !this.m.d()) {
                com.tencent.android.tpush.a.a.h("TpnsChannel", "The socket Channel is unconnected");
                try {
                    this.m.c();
                    g.a().a(this.u);
                } catch (Exception e3) {
                    com.tencent.android.tpush.a.a.c(Constants.ServiceLogTag, "createOptimalSocketChannel error", e3);
                }
            }
        }
        return;
    }

    /* access modifiers changed from: protected */
    public synchronized boolean f() {
        boolean z = false;
        synchronized (this) {
            if (com.tencent.android.tpush.service.d.a.d(m.e())) {
                int j2 = p.j(m.e());
                if (p.i(m.e()) || j2 > 0) {
                    int i2 = (g + 1) * 2 * 1000;
                    g++;
                    if (g <= 3) {
                        if (i2 > d) {
                            i2 = d;
                        }
                        if (g <= 3 || j2 == 1) {
                            if (!this.v.hasMessages(1000)) {
                                if (XGPushConfig.enableDebug) {
                                    com.tencent.android.tpush.a.a.c("TpnsChannel", "onDisconnected and retry HANDLER_CHECKANDSETUP " + i2 + " retry times = " + g);
                                }
                                this.v.sendEmptyMessageDelayed(1000, (long) i2);
                            }
                            z = true;
                        }
                    }
                }
            }
        }
        return z;
    }

    private void a(int i2, o oVar) {
        if (oVar.a()) {
            p.k(m.e());
        }
        try {
            synchronized (this) {
                if (this.j.size() < 128) {
                    oVar.a = System.currentTimeMillis();
                    if (i2 == -1) {
                        this.j.add(oVar);
                    } else {
                        this.j.add(i2, oVar);
                    }
                } else {
                    com.tencent.android.tpush.a.a.h(Constants.ServiceLogTag, ">>FG messageInQueue is full,size:" + this.j.size());
                }
                if (this.m != null) {
                    this.m.h();
                }
                e();
            }
            m mVar = new m(this, null);
            this.l.put(oVar, mVar);
            this.i.postDelayed(mVar, (long) com.tencent.android.tpush.service.a.a.a(m.e()).f);
            if (oVar.a()) {
                p.a();
            }
        } catch (Throwable th) {
            try {
                com.tencent.android.tpush.a.a.c(Constants.ServiceLogTag, "messageInQueue", th);
            } finally {
                if (oVar.a()) {
                    p.a();
                }
            }
        }
    }

    public void a(JceStruct jceStruct, p pVar) {
        if (jceStruct != null) {
            try {
                a(-1, new o(jceStruct, pVar));
            } catch (Exception e2) {
                com.tencent.android.tpush.a.a.c(Constants.ServiceLogTag, "sendMessage error ", e2);
            }
        } else {
            com.tencent.android.tpush.a.a.h(Constants.ServiceLogTag, "sendMessage null jceMessage");
        }
    }

    public void a(boolean z) {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - s > 120000 || z) {
            s = currentTimeMillis;
            o b2 = com.tencent.android.tpush.service.p.a().b();
            if (b2 != null) {
                if (XGPushConfig.enableDebug) {
                    com.tencent.android.tpush.a.a.c("TpnsChannel", "Action -> sendReconnMessage with token - " + CacheManager.getToken(m.e()));
                }
                if (m.e() != null && !"0".equals(CacheManager.getToken(m.e()))) {
                    a(0, b2);
                    XGWatchdog.getInstance(m.e()).sendAllLocalXGAppList();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void h() {
        if (this.j.isEmpty()) {
            if (this.p == null) {
                this.p = new o(7, null, this.w);
            }
            if (this.p.d == null) {
                this.p = new o(7, null, this.w);
            }
            if (XGPushConfig.enableDebug) {
                com.tencent.android.tpush.a.a.c("TpnsChannel", "Action -> send heartbeat ");
            }
            a(-1, this.p);
            if (a % 4 == 0) {
                XGWatchdog.getInstance(m.e()).sendAllLocalXGAppList();
            } else if (b >= 5) {
                com.tencent.android.tpush.a.a.h(Constants.ServiceLogTag, "heartbeat to watchdog failed too many time , start watchdog again");
                b = 0;
                XGWatchdog.getInstance(m.e()).startWatchdog();
            } else {
                XGWatchdog.getInstance(m.e()).sendHeartbeat2Watchdog("heartbeat:", new f(this));
            }
        }
        i();
        w.a(m.e()).a();
    }

    public int b(boolean z) {
        ArrayList d2;
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - r > 120000 || z) {
            r = currentTimeMillis;
            Context e2 = m.e();
            if (e2 != null && !e.a(e2.getPackageName()) && (d2 = com.tencent.android.tpush.service.b.a.a().d(e2)) != null && d2.size() > 0) {
                if (XGPushConfig.enableDebug) {
                    com.tencent.android.tpush.a.a.c(Constants.ServiceLogTag, "Action -> trySendCachedMsgIntent with CachedMsgList size = " + d2.size());
                }
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < d2.size(); i2++) {
                    CachedMessageIntent cachedMessageIntent = (CachedMessageIntent) d2.get(i2);
                    try {
                        String decrypt = Rijndael.decrypt(cachedMessageIntent.intent);
                        if (e.a(decrypt)) {
                            arrayList.add(cachedMessageIntent);
                        } else {
                            Intent parseUri = Intent.parseUri(decrypt, 1);
                            String str = parseUri.getPackage();
                            parseUri.getLongExtra(MessageKey.MSG_CREATE_MULTIPKG, 0);
                            if (!e.d(m.e(), str, parseUri.getLongExtra("accId", 0))) {
                                arrayList.add(cachedMessageIntent);
                                com.tencent.android.tpush.a.a.h("TpnsChannel", str + " is uninstalled , discard the msg and report to the server");
                                com.tencent.android.tpush.service.p.a().a(str);
                                j.a().a(m.e(), str);
                                com.tencent.android.tpush.service.b.a.a().d(m.e(), str, new ArrayList());
                            } else {
                                com.tencent.android.tpush.data.a registerInfoByPkgName = CacheManager.getRegisterInfoByPkgName(str);
                                if (registerInfoByPkgName == null || registerInfoByPkgName.e <= 0) {
                                    long longExtra = parseUri.getLongExtra(MessageKey.MSG_ID, 0);
                                    parseUri.getLongExtra(MessageKey.MSG_SERVER_TIME, 0);
                                    long longExtra2 = parseUri.getLongExtra(MessageKey.MSG_EXPIRE_TIME, 0);
                                    com.tencent.android.tpush.a.a.c(Constants.ServiceLogTag, "Action -> trySendCachedMsgIntent msgId = " + longExtra + ",appPkgName=" + str);
                                    if (longExtra2 <= 0) {
                                        int intExtra = parseUri.getIntExtra(MessageKey.MSG_TTL, 0);
                                        if (intExtra < 0 || intExtra > 259200000) {
                                            intExtra = 259200000;
                                        }
                                        parseUri.putExtra(MessageKey.MSG_EXPIRE_TIME, ((long) intExtra) + currentTimeMillis);
                                    } else if (currentTimeMillis > longExtra2) {
                                        arrayList.add(cachedMessageIntent);
                                        com.tencent.android.tpush.a.a.h(Constants.ServiceLogTag, "currentTime:" + currentTimeMillis + " > expire_time:" + longExtra2 + ", remove msg:" + cachedMessageIntent);
                                    }
                                    com.tencent.android.tpush.service.b.a.a().a(parseUri.getStringExtra(MessageKey.MSG_DATE), parseUri, str);
                                }
                            }
                        }
                    } catch (Exception e3) {
                        com.tencent.android.tpush.a.a.c("TpnsChannel", "", e3);
                    }
                }
                if (arrayList.size() > 0) {
                    com.tencent.android.tpush.service.b.a.a().a(e2, arrayList, d2);
                }
                return d2.size();
            }
        }
        return 0;
    }

    private void i() {
        try {
            if (this.o == null) {
                m.e().registerReceiver(new g(this), new IntentFilter("com.tencent.android.tpush.service.channel.heartbeatIntent"));
                this.o = PendingIntent.getBroadcast(m.e(), 0, new Intent("com.tencent.android.tpush.service.channel.heartbeatIntent"), 134217728);
            }
            long currentTimeMillis = System.currentTimeMillis();
            if (f > e) {
                f = e;
            }
            f = com.tencent.android.tpush.common.m.a(m.e(), "com.tencent.android.xg.wx.HeartbeatIntervalMs", f);
            u.a().a(0, currentTimeMillis + ((long) f), this.o);
        } catch (Throwable th) {
            com.tencent.android.tpush.a.a.c("TpnsChannel", "scheduleHeartbeat error", th);
        }
    }

    public synchronized ArrayList a(a aVar, int i2) {
        ArrayList arrayList;
        if (i2 < 1) {
            i2 = 1;
        }
        long currentTimeMillis = System.currentTimeMillis();
        ConcurrentHashMap concurrentHashMap = (ConcurrentHashMap) this.k.get(aVar);
        arrayList = new ArrayList(i2);
        if (!this.j.isEmpty()) {
            Iterator it = this.j.iterator();
            o oVar = (o) it.next();
            h hVar = new h(oVar.b());
            oVar.a(hVar);
            arrayList.add(hVar);
            oVar.b = currentTimeMillis;
            if (!oVar.a()) {
                concurrentHashMap.put(Integer.valueOf(oVar.c()), oVar);
            }
            it.remove();
            int i3 = i2 - 1;
            boolean z = oVar.c instanceof TpnsReconnectReq;
            while (it.hasNext()) {
                o oVar2 = (o) it.next();
                if (!z || (!(oVar2.c instanceof TpnsReconnectReq) && !(oVar2.c instanceof TpnsPushVerifyReq))) {
                    int i4 = i3 - 1;
                    if (i3 > 0) {
                        h hVar2 = new h(oVar2.b());
                        oVar2.a(hVar2);
                        arrayList.add(hVar2);
                        oVar2.b = currentTimeMillis;
                        if (!oVar2.a()) {
                            concurrentHashMap.put(Integer.valueOf(oVar2.c()), oVar2);
                        }
                        it.remove();
                    }
                    i3 = i4;
                } else {
                    if (oVar2.d != null) {
                        this.i.post(new h(this, oVar2));
                    }
                    it.remove();
                }
            }
        }
        return arrayList;
    }

    public void a(a aVar, ChannelException channelException) {
        com.tencent.android.tpush.a.a.h("TpnsChannel", "clientExceptionOccurs(isHttpClient : " + (aVar instanceof c) + "," + channelException + ")");
        this.i.post(new k(this, aVar, channelException, true));
    }

    public void a(a aVar) {
        if (XGPushConfig.enableDebug) {
            com.tencent.android.tpush.a.a.h("TpnsChannel", "Action -> clientDidCancelled " + aVar);
        }
        this.i.post(new k(this, aVar, new ChannelException(Constants.CODE_NETWORK_CHANNEL_CANCELLED, "TpnsClient is cancelled!"), false));
    }

    public void b(a aVar) {
        if (XGPushConfig.enableDebug) {
            com.tencent.android.tpush.a.a.h("TpnsChannel", "Action -> clientDidRetired " + aVar);
        }
        this.i.post(new k(this, aVar, new ChannelException(Constants.CODE_NETWORK_TIMEOUT_EXCEPTION_OCCUR, "TpnsMessage timeout!"), false));
    }

    public void a(a aVar, i iVar) {
        if (XGPushConfig.enableDebug) {
            com.tencent.android.tpush.a.a.c("TpnsChannel", "Action -> clientDidSendPacket packet : " + iVar.m());
        }
        o oVar = (o) ((ConcurrentHashMap) this.k.get(aVar)).get(Integer.valueOf(iVar.i()));
        if (oVar != null) {
            oVar.b = System.currentTimeMillis();
        } else {
            com.tencent.android.tpush.a.a.h("TpnsChannel", ">> message(" + iVar.i() + ") not in the sentQueue!");
        }
    }

    public synchronized void b(a aVar, i iVar) {
        a().b(false);
        if (XGPushConfig.enableDebug) {
            com.tencent.android.tpush.a.a.c("TpnsChannel", "Action -> clientDidReceivePacket packet : " + iVar.m());
        }
        switch (iVar.j()) {
            case 1:
            case 10:
                if (iVar.e()) {
                    com.tencent.android.tpush.a.a.c("TpnsChannel", "Action -> clientDidReceivePacket RequestSuccRunnable NEV1 : " + iVar.m());
                    this.i.post(new l(this, aVar, iVar));
                } else {
                    com.tencent.android.tpush.a.a.c("TpnsChannel", "Action -> clientDidReceivePacket PushMessageRunnable NEV1 : " + iVar.m());
                    this.i.post(new j(this, aVar, iVar));
                }
                i();
                break;
            case 20:
                this.i.post(new i(this, aVar, iVar));
                break;
            default:
                com.tencent.android.tpush.a.a.h("TpnsChannel", "Action -> clientDidReceivePacket unkonwn protocol : " + iVar.m());
                i();
                break;
        }
    }
}
