package com.tencent.android.tpush.horse;

import android.content.Intent;
import android.net.NetworkInfo;
import android.os.Handler;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.service.d.e;
import com.tencent.android.tpush.service.m;
import java.util.Timer;

/* compiled from: ProGuard */
public class g {
    static long a = 1;
    static long b = 0;
    public static int c = -1;
    private static long j = 0;
    private static long k = 0;
    /* access modifiers changed from: private */
    public static int m;
    /* access modifiers changed from: private */
    public final Object d;
    /* access modifiers changed from: private */
    public volatile int e;
    /* access modifiers changed from: private */
    public volatile boolean f;
    /* access modifiers changed from: private */
    public long g;
    /* access modifiers changed from: private */
    public k h;
    private l i;
    private Timer l;
    private Handler n;
    private b o;
    /* access modifiers changed from: private */
    public b p;

    /* synthetic */ g(h hVar) {
        this();
    }

    public void a(l lVar) {
        this.i = lVar;
    }

    public static g a() {
        return m.a;
    }

    private g() {
        this.d = new Object();
        this.e = 0;
        this.f = false;
        this.l = new Timer();
        this.n = null;
        this.o = new i(this);
        this.p = new j(this);
        this.n = com.tencent.android.tpush.common.g.a().b();
    }

    public synchronized void a(k kVar) {
        this.e = 0;
        this.h = kVar;
        this.n.post(new h(this));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0094 A[SYNTHETIC, Splitter:B:24:0x0094] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0116 A[SYNTHETIC, Splitter:B:38:0x0116] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x017b A[SYNTHETIC, Splitter:B:44:0x017b] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01e0 A[SYNTHETIC, Splitter:B:50:0x01e0] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r9) {
        /*
            r8 = this;
            r2 = 0
            r6 = 10101(0x2775, float:1.4155E-41)
            android.content.Context r0 = com.tencent.android.tpush.service.m.e()
            boolean r0 = com.tencent.android.tpush.service.d.a.d(r0)
            if (r0 != 0) goto L_0x001c
            java.lang.String r0 = "OptimalLinkSelector"
            java.lang.String r1 = "Network can't reachable"
            com.tencent.android.tpush.a.a.h(r0, r1)
            r0 = 10100(0x2774, float:1.4153E-41)
            java.lang.String r1 = "network can't reachable!"
            r8.a(r0, r1)
        L_0x001b:
            return
        L_0x001c:
            com.tencent.android.tpush.horse.q r0 = com.tencent.android.tpush.horse.q.i()
            boolean r0 = r0.b()
            if (r0 != 0) goto L_0x0030
            com.tencent.android.tpush.horse.f r0 = com.tencent.android.tpush.horse.f.i()
            boolean r0 = r0.b()
            if (r0 == 0) goto L_0x0038
        L_0x0030:
            java.lang.String r0 = "OptimalLinkSelector"
            java.lang.String r1 = "Horse task running"
            com.tencent.android.tpush.a.a.a(r0, r1)
            goto L_0x001b
        L_0x0038:
            boolean r0 = com.tencent.android.tpush.XGPushConfig.enableDebug
            if (r0 == 0) goto L_0x0054
            java.lang.String r0 = "OptimalLinkSelector"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Action -> startHorseTask with key = "
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r1 = r1.append(r9)
            java.lang.String r1 = r1.toString()
            com.tencent.android.tpush.a.a.c(r0, r1)
        L_0x0054:
            android.content.Context r0 = com.tencent.android.tpush.service.m.e()
            com.tencent.android.tpush.service.cache.CacheManager.removeOptStrategyList(r0, r9)
            java.lang.String r0 = "3"
            boolean r0 = r9.equals(r0)     // Catch:{ Exception -> 0x00d4 }
            if (r0 != 0) goto L_0x0073
            java.lang.String r0 = "1"
            boolean r0 = r9.equals(r0)     // Catch:{ Exception -> 0x00d4 }
            if (r0 != 0) goto L_0x0073
            java.lang.String r0 = "2"
            boolean r0 = r9.equals(r0)     // Catch:{ Exception -> 0x00d4 }
            if (r0 == 0) goto L_0x00cf
        L_0x0073:
            android.content.Context r0 = com.tencent.android.tpush.service.m.e()     // Catch:{ Exception -> 0x00d4 }
            java.util.ArrayList r0 = com.tencent.android.tpush.service.cache.CacheManager.getServerItems(r0, r9)     // Catch:{ Exception -> 0x00d4 }
        L_0x007b:
            if (r0 != 0) goto L_0x0082
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
        L_0x0082:
            java.util.ArrayList r1 = com.tencent.android.tpush.horse.DefaultServer.a()
            r0.addAll(r1)
            android.content.Context r1 = com.tencent.android.tpush.service.m.e()
            int r1 = com.tencent.android.tpush.horse.Tools.getChannelType(r1)
            switch(r1) {
                case 1: goto L_0x0116;
                case 2: goto L_0x017b;
                case 3: goto L_0x01e0;
                default: goto L_0x0094;
            }
        L_0x0094:
            java.util.List r1 = com.tencent.android.tpush.horse.p.a(r0, r9)     // Catch:{ NullReturnException -> 0x0264, Exception -> 0x028b }
            java.util.List r0 = com.tencent.android.tpush.horse.p.b(r0, r9)     // Catch:{ NullReturnException -> 0x02b7, Exception -> 0x02b2 }
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x009f:
            com.tencent.android.tpush.horse.q r2 = com.tencent.android.tpush.horse.q.i()
            com.tencent.android.tpush.horse.b r3 = r8.p
            r2.a(r3)
            com.tencent.android.tpush.horse.q r2 = com.tencent.android.tpush.horse.q.i()
            r2.a(r0)
            com.tencent.android.tpush.horse.q r0 = com.tencent.android.tpush.horse.q.i()
            r0.g()
            com.tencent.android.tpush.horse.f r0 = com.tencent.android.tpush.horse.f.i()
            com.tencent.android.tpush.horse.b r2 = r8.o
            r0.a(r2)
            com.tencent.android.tpush.horse.f r0 = com.tencent.android.tpush.horse.f.i()
            r0.a(r1)
            com.tencent.android.tpush.horse.f r0 = com.tencent.android.tpush.horse.f.i()
            r0.g()
            goto L_0x001b
        L_0x00cf:
            java.util.ArrayList r0 = com.tencent.android.tpush.horse.DefaultServer.a(r9)     // Catch:{ Exception -> 0x00d4 }
            goto L_0x007b
        L_0x00d4:
            r0 = move-exception
            java.lang.String r1 = "XGHorse"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = ">> Can not get local serverItems : "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.android.tpush.a.a.h(r1, r0)
            java.util.ArrayList r0 = com.tencent.android.tpush.horse.DefaultServer.a(r9)     // Catch:{ Exception -> 0x00f6 }
            goto L_0x007b
        L_0x00f6:
            r0 = move-exception
            java.lang.String r1 = "XGHorse"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = ">> Can not get default serverItems : "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.android.tpush.a.a.h(r1, r0)
            r0 = r2
            goto L_0x007b
        L_0x0116:
            java.util.List r0 = com.tencent.android.tpush.horse.p.a(r0, r9)     // Catch:{ NullReturnException -> 0x0133, Exception -> 0x0157 }
            com.tencent.android.tpush.horse.q r1 = com.tencent.android.tpush.horse.q.i()     // Catch:{ NullReturnException -> 0x0133, Exception -> 0x0157 }
            com.tencent.android.tpush.horse.b r2 = r8.p     // Catch:{ NullReturnException -> 0x0133, Exception -> 0x0157 }
            r1.a(r2)     // Catch:{ NullReturnException -> 0x0133, Exception -> 0x0157 }
            com.tencent.android.tpush.horse.q r1 = com.tencent.android.tpush.horse.q.i()     // Catch:{ NullReturnException -> 0x0133, Exception -> 0x0157 }
            r1.a(r0)     // Catch:{ NullReturnException -> 0x0133, Exception -> 0x0157 }
            com.tencent.android.tpush.horse.q r0 = com.tencent.android.tpush.horse.q.i()     // Catch:{ NullReturnException -> 0x0133, Exception -> 0x0157 }
            r0.g()     // Catch:{ NullReturnException -> 0x0133, Exception -> 0x0157 }
            goto L_0x001b
        L_0x0133:
            r0 = move-exception
            java.lang.String r1 = "XGHorse"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ">> Can not get strategyItems(create tcp channel fail!) >> "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.android.tpush.a.a.h(r1, r0)
            java.lang.String r0 = "create tcp channel fail!"
            r8.a(r6, r0)
            goto L_0x001b
        L_0x0157:
            r0 = move-exception
            java.lang.String r1 = "XGHorse"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ">> (create tcp channel fail!) >> "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.android.tpush.a.a.h(r1, r0)
            java.lang.String r0 = "create tcp channel fail!"
            r8.a(r6, r0)
            goto L_0x001b
        L_0x017b:
            java.util.List r0 = com.tencent.android.tpush.horse.p.b(r0, r9)     // Catch:{ NullReturnException -> 0x0198, Exception -> 0x01bc }
            com.tencent.android.tpush.horse.f r1 = com.tencent.android.tpush.horse.f.i()     // Catch:{ NullReturnException -> 0x0198, Exception -> 0x01bc }
            com.tencent.android.tpush.horse.b r2 = r8.o     // Catch:{ NullReturnException -> 0x0198, Exception -> 0x01bc }
            r1.a(r2)     // Catch:{ NullReturnException -> 0x0198, Exception -> 0x01bc }
            com.tencent.android.tpush.horse.f r1 = com.tencent.android.tpush.horse.f.i()     // Catch:{ NullReturnException -> 0x0198, Exception -> 0x01bc }
            r1.a(r0)     // Catch:{ NullReturnException -> 0x0198, Exception -> 0x01bc }
            com.tencent.android.tpush.horse.f r0 = com.tencent.android.tpush.horse.f.i()     // Catch:{ NullReturnException -> 0x0198, Exception -> 0x01bc }
            r0.g()     // Catch:{ NullReturnException -> 0x0198, Exception -> 0x01bc }
            goto L_0x001b
        L_0x0198:
            r0 = move-exception
            java.lang.String r1 = "XGHorse"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ">> Can not get strategyItems(create http channel fail!)>>"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.android.tpush.a.a.h(r1, r0)
            java.lang.String r0 = "create http channel fail!"
            r8.a(r6, r0)
            goto L_0x001b
        L_0x01bc:
            r0 = move-exception
            java.lang.String r1 = "XGHorse"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ">> (create http channel fail!) >> "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.android.tpush.a.a.h(r1, r0)
            java.lang.String r0 = "create http channel fail!"
            r8.a(r6, r0)
            goto L_0x001b
        L_0x01e0:
            java.util.List r0 = com.tencent.android.tpush.horse.p.b(r0, r9)     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
            r1.<init>()     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
        L_0x01ed:
            boolean r0 = r2.hasNext()     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
            if (r0 == 0) goto L_0x0227
            java.lang.Object r0 = r2.next()     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
            com.tencent.android.tpush.horse.data.StrategyItem r0 = (com.tencent.android.tpush.horse.data.StrategyItem) r0     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
            boolean r3 = r0.h()     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
            if (r3 == 0) goto L_0x01ed
            r1.add(r0)     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
            goto L_0x01ed
        L_0x0203:
            r0 = move-exception
            java.lang.String r1 = "XGHorse"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ">> Can not get strategyItems(create wap channel fail!)>>"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.android.tpush.a.a.h(r1, r0)
            java.lang.String r0 = "create wap channel fail!"
            r8.a(r6, r0)
            goto L_0x001b
        L_0x0227:
            com.tencent.android.tpush.horse.f r0 = com.tencent.android.tpush.horse.f.i()     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
            com.tencent.android.tpush.horse.b r2 = r8.o     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
            r0.a(r2)     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
            com.tencent.android.tpush.horse.f r0 = com.tencent.android.tpush.horse.f.i()     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
            r0.a(r1)     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
            com.tencent.android.tpush.horse.f r0 = com.tencent.android.tpush.horse.f.i()     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
            r0.g()     // Catch:{ NullReturnException -> 0x0203, Exception -> 0x0240 }
            goto L_0x001b
        L_0x0240:
            r0 = move-exception
            java.lang.String r1 = "XGHorse"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = ">> (create wap channel fail!) >> "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.android.tpush.a.a.h(r1, r0)
            java.lang.String r0 = "create wap channel fail!"
            r8.a(r6, r0)
            goto L_0x001b
        L_0x0264:
            r0 = move-exception
            r1 = r0
            r0 = r2
        L_0x0267:
            java.lang.String r3 = "XGHorse"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = ">> Can not get strategyItems(create default channel fail!)>>"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r1 = r1.getMessage()
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r1 = r1.toString()
            com.tencent.android.tpush.a.a.h(r3, r1)
            java.lang.String r1 = "create default channel fail!"
            r8.a(r6, r1)
            r1 = r2
            goto L_0x009f
        L_0x028b:
            r0 = move-exception
            r1 = r0
            r0 = r2
        L_0x028e:
            java.lang.String r3 = "XGHorse"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = ">> (create default channel fail!) >> "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r1 = r1.getMessage()
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r1 = r1.toString()
            com.tencent.android.tpush.a.a.h(r3, r1)
            java.lang.String r1 = "create default channel fail!"
            r8.a(r6, r1)
            r1 = r2
            goto L_0x009f
        L_0x02b2:
            r0 = move-exception
            r7 = r0
            r0 = r1
            r1 = r7
            goto L_0x028e
        L_0x02b7:
            r0 = move-exception
            r7 = r0
            r0 = r1
            r1 = r7
            goto L_0x0267
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.horse.g.a(java.lang.String):void");
    }

    /* access modifiers changed from: private */
    public void a(int i2, String str) {
        if (this.h != null) {
            this.h.a(i2, str);
        }
    }

    public synchronized void a(Intent intent) {
        try {
            NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
            if (networkInfo != null) {
                if (XGPushConfig.enableDebug) {
                    a.c("OptimalLinkSelector", "Connection state changed to - " + networkInfo.toString());
                }
                boolean booleanExtra = intent.getBooleanExtra("noConnectivity", false);
                int type = networkInfo.getType();
                if (booleanExtra) {
                    if (XGPushConfig.enableDebug) {
                        a.c("OptimalLinkSelector", "DisConnected with network type " + networkInfo.getTypeName());
                    }
                    m.b(m.e());
                } else if (NetworkInfo.State.CONNECTED == networkInfo.getState()) {
                    if (XGPushConfig.enableDebug) {
                        a.c("OptimalLinkSelector", "Connected with network type " + networkInfo.getTypeName());
                    }
                    c = type;
                    m.a(m.e(), 2000);
                } else if (NetworkInfo.State.DISCONNECTED == networkInfo.getState()) {
                    if (XGPushConfig.enableDebug) {
                        a.c("OptimalLinkSelector", "NetworkInfo.State.DISCONNECTED with network type = " + networkInfo.getTypeName());
                    }
                    if (c == -1 || c == type) {
                        m.b(m.e());
                    }
                } else if (XGPushConfig.enableDebug) {
                    a.c("OptimalLinkSelector", "other network state - " + networkInfo.getState() + ". Do nothing.");
                }
            }
        } catch (Throwable th) {
            a.a("OptimalLinkSelector", "onNetworkChanged", th);
        }
        return;
    }

    public void b() {
        m++;
        if (m < com.tencent.android.tpush.service.a.a.a(m.e()).t) {
            a().a(e.g(m.e()));
        } else {
            a((int) Constants.CODE_NETWORK_CREATE_OPTIOMAL_SC_FAILED, "create socket err");
        }
    }
}
