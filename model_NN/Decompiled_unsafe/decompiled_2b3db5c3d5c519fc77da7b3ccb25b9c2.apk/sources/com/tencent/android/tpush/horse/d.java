package com.tencent.android.tpush.horse;

import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.horse.data.StrategyItem;

/* compiled from: ProGuard */
class d implements o {
    final /* synthetic */ c a;

    d(c cVar) {
        this.a = cVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0080, code lost:
        if (r5.d() != 0) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0082, code lost:
        r4.a.b.e();
        r4.a.b.f();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0090, code lost:
        com.tencent.android.tpush.service.cache.CacheManager.addOptStrategy(r5);
        r4.a.b.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00a2, code lost:
        if (r4.a.b.d == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00a4, code lost:
        r4.a.b.d.a(r4.a.a().a(), r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.tencent.android.tpush.horse.data.StrategyItem r5) {
        /*
            r4 = this;
            r3 = 1
            boolean r0 = com.tencent.android.tpush.XGPushConfig.enableDebug
            if (r0 == 0) goto L_0x0045
            java.lang.String r0 = "BaseTask"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Horse run onSuccess("
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r2 = ","
            java.lang.StringBuilder r1 = r1.append(r2)
            com.tencent.android.tpush.horse.c r2 = r4.a
            int r2 = r2.d
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ","
            java.lang.StringBuilder r1 = r1.append(r2)
            com.tencent.android.tpush.horse.c r2 = r4.a
            com.tencent.android.tpush.horse.a r2 = r2.b
            boolean r2 = r2.f
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ")"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.tencent.android.tpush.a.a.a(r0, r1)
        L_0x0045:
            java.lang.Object r1 = com.tencent.android.tpush.horse.a.a
            monitor-enter(r1)
            com.tencent.android.tpush.horse.c r0 = r4.a     // Catch:{ all -> 0x00c8 }
            com.tencent.android.tpush.horse.a r0 = r0.b     // Catch:{ all -> 0x00c8 }
            java.util.concurrent.LinkedBlockingQueue r0 = r0.b     // Catch:{ all -> 0x00c8 }
            r0.clear()     // Catch:{ all -> 0x00c8 }
            com.tencent.android.tpush.horse.c r0 = r4.a     // Catch:{ all -> 0x00c8 }
            com.tencent.android.tpush.horse.a r0 = r0.b     // Catch:{ all -> 0x00c8 }
            boolean r0 = r0.f     // Catch:{ all -> 0x00c8 }
            if (r0 == 0) goto L_0x0067
            boolean r0 = r5.j()     // Catch:{ all -> 0x00c8 }
            if (r0 != 0) goto L_0x0067
            monitor-exit(r1)     // Catch:{ all -> 0x00c8 }
        L_0x0066:
            return
        L_0x0067:
            com.tencent.android.tpush.horse.c r0 = r4.a     // Catch:{ all -> 0x00c8 }
            com.tencent.android.tpush.horse.a r0 = r0.b     // Catch:{ all -> 0x00c8 }
            r2 = 1
            boolean unused = r0.f = r2     // Catch:{ all -> 0x00c8 }
            int r0 = r5.d()     // Catch:{ all -> 0x00c8 }
            if (r0 != 0) goto L_0x00ba
            int r0 = r5.f()     // Catch:{ all -> 0x00c8 }
            if (r0 != r3) goto L_0x00ba
        L_0x007b:
            monitor-exit(r1)     // Catch:{ all -> 0x00c8 }
            int r0 = r5.d()
            if (r0 != 0) goto L_0x0090
            com.tencent.android.tpush.horse.c r0 = r4.a
            com.tencent.android.tpush.horse.a r0 = r0.b
            r0.e()
            com.tencent.android.tpush.horse.c r0 = r4.a
            com.tencent.android.tpush.horse.a r0 = r0.b
            r0.f()
        L_0x0090:
            com.tencent.android.tpush.service.cache.CacheManager.addOptStrategy(r5)
            com.tencent.android.tpush.horse.c r0 = r4.a
            com.tencent.android.tpush.horse.a r0 = r0.b
            r0.a()
            com.tencent.android.tpush.horse.c r0 = r4.a
            com.tencent.android.tpush.horse.a r0 = r0.b
            com.tencent.android.tpush.horse.b r0 = r0.d
            if (r0 == 0) goto L_0x0066
            com.tencent.android.tpush.horse.c r0 = r4.a
            com.tencent.android.tpush.horse.a r0 = r0.b
            com.tencent.android.tpush.horse.b r0 = r0.d
            com.tencent.android.tpush.horse.c r1 = r4.a
            com.tencent.android.tpush.horse.n r1 = r1.a()
            java.nio.channels.SocketChannel r1 = r1.a()
            r0.a(r1, r5)
            goto L_0x0066
        L_0x00ba:
            com.tencent.android.tpush.horse.c r0 = r4.a     // Catch:{ all -> 0x00c8 }
            com.tencent.android.tpush.horse.a r0 = r0.b     // Catch:{ all -> 0x00c8 }
            com.tencent.android.tpush.horse.c r2 = r4.a     // Catch:{ all -> 0x00c8 }
            int r2 = r2.d     // Catch:{ all -> 0x00c8 }
            r0.a(r2)     // Catch:{ all -> 0x00c8 }
            goto L_0x007b
        L_0x00c8:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00c8 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.horse.d.a(com.tencent.android.tpush.horse.data.StrategyItem):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x007b, code lost:
        com.tencent.android.tpush.service.cache.CacheManager.addOptStrategy(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0082, code lost:
        if (r4.equals(r5) == false) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0084, code lost:
        r3.a.b.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0093, code lost:
        if (r3.a.b.d == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0095, code lost:
        r3.a.b.d.a(r3.a.a().a(), r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00b2, code lost:
        if (r4.f() != 0) goto L_0x00e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00b4, code lost:
        r3.a.b.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00bf, code lost:
        if (r5.g() == false) goto L_0x00cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00c1, code lost:
        r3.a.b.b.add(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00cc, code lost:
        r3.a.b.d.a(r3.a.a().a(), r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00e3, code lost:
        r3.a.b.a();
        r3.a.b.d.a(r3.a.a().a(), r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.tencent.android.tpush.horse.data.StrategyItem r4, com.tencent.android.tpush.horse.data.StrategyItem r5) {
        /*
            r3 = this;
            java.lang.String r0 = "BaseTask"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Horse run onRedirect(org:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r2 = ",redirect:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r2 = ")"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.tencent.android.tpush.a.a.a(r0, r1)
            java.lang.Object r1 = com.tencent.android.tpush.horse.a.a
            monitor-enter(r1)
            com.tencent.android.tpush.horse.c r0 = r3.a     // Catch:{ all -> 0x00ab }
            com.tencent.android.tpush.horse.a r0 = r0.b     // Catch:{ all -> 0x00ab }
            java.util.concurrent.LinkedBlockingQueue r0 = r0.b     // Catch:{ all -> 0x00ab }
            r0.clear()     // Catch:{ all -> 0x00ab }
            com.tencent.android.tpush.horse.c r0 = r3.a     // Catch:{ all -> 0x00ab }
            com.tencent.android.tpush.horse.a r0 = r0.b     // Catch:{ all -> 0x00ab }
            boolean r0 = r0.f     // Catch:{ all -> 0x00ab }
            if (r0 == 0) goto L_0x0051
            boolean r0 = r4.j()     // Catch:{ all -> 0x00ab }
            if (r0 != 0) goto L_0x0051
            java.lang.String r0 = "XGHorse"
            java.lang.String r2 = ">> hasSuccessCallback && !strategyItem.isRedirected()"
            com.tencent.android.tpush.a.a.c(r0, r2)     // Catch:{ all -> 0x00ab }
            monitor-exit(r1)     // Catch:{ all -> 0x00ab }
        L_0x0050:
            return
        L_0x0051:
            com.tencent.android.tpush.horse.c r0 = r3.a     // Catch:{ all -> 0x00ab }
            com.tencent.android.tpush.horse.a r0 = r0.b     // Catch:{ all -> 0x00ab }
            r2 = 1
            boolean unused = r0.f = r2     // Catch:{ all -> 0x00ab }
            com.tencent.android.tpush.horse.c r0 = r3.a     // Catch:{ all -> 0x00ab }
            com.tencent.android.tpush.horse.a r0 = r0.b     // Catch:{ all -> 0x00ab }
            com.tencent.android.tpush.horse.c r2 = r3.a     // Catch:{ all -> 0x00ab }
            int r2 = r2.d     // Catch:{ all -> 0x00ab }
            r0.a(r2)     // Catch:{ all -> 0x00ab }
            int r0 = r4.d()     // Catch:{ all -> 0x00ab }
            if (r0 != 0) goto L_0x007a
            com.tencent.android.tpush.horse.c r0 = r3.a     // Catch:{ all -> 0x00ab }
            com.tencent.android.tpush.horse.a r0 = r0.b     // Catch:{ all -> 0x00ab }
            r0.e()     // Catch:{ all -> 0x00ab }
            com.tencent.android.tpush.horse.c r0 = r3.a     // Catch:{ all -> 0x00ab }
            com.tencent.android.tpush.horse.a r0 = r0.b     // Catch:{ all -> 0x00ab }
            r0.f()     // Catch:{ all -> 0x00ab }
        L_0x007a:
            monitor-exit(r1)     // Catch:{ all -> 0x00ab }
            com.tencent.android.tpush.service.cache.CacheManager.addOptStrategy(r4)
            boolean r0 = r4.equals(r5)
            if (r0 == 0) goto L_0x00ae
            com.tencent.android.tpush.horse.c r0 = r3.a
            com.tencent.android.tpush.horse.a r0 = r0.b
            r0.a()
            com.tencent.android.tpush.horse.c r0 = r3.a
            com.tencent.android.tpush.horse.a r0 = r0.b
            com.tencent.android.tpush.horse.b r0 = r0.d
            if (r0 == 0) goto L_0x0050
            com.tencent.android.tpush.horse.c r0 = r3.a
            com.tencent.android.tpush.horse.a r0 = r0.b
            com.tencent.android.tpush.horse.b r0 = r0.d
            com.tencent.android.tpush.horse.c r1 = r3.a
            com.tencent.android.tpush.horse.n r1 = r1.a()
            java.nio.channels.SocketChannel r1 = r1.a()
            r0.a(r1, r4)
            goto L_0x0050
        L_0x00ab:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00ab }
            throw r0
        L_0x00ae:
            int r0 = r4.f()
            if (r0 != 0) goto L_0x00e3
            com.tencent.android.tpush.horse.c r0 = r3.a
            com.tencent.android.tpush.horse.a r0 = r0.b
            r0.a()
            boolean r0 = r5.g()
            if (r0 == 0) goto L_0x00cc
            com.tencent.android.tpush.horse.c r0 = r3.a
            com.tencent.android.tpush.horse.a r0 = r0.b
            java.util.concurrent.LinkedBlockingQueue r0 = r0.b
            r0.add(r5)
        L_0x00cc:
            com.tencent.android.tpush.horse.c r0 = r3.a
            com.tencent.android.tpush.horse.a r0 = r0.b
            com.tencent.android.tpush.horse.b r0 = r0.d
            com.tencent.android.tpush.horse.c r1 = r3.a
            com.tencent.android.tpush.horse.n r1 = r1.a()
            java.nio.channels.SocketChannel r1 = r1.a()
            r0.a(r1, r4)
            goto L_0x0050
        L_0x00e3:
            com.tencent.android.tpush.horse.c r0 = r3.a
            com.tencent.android.tpush.horse.a r0 = r0.b
            r0.a()
            com.tencent.android.tpush.horse.c r0 = r3.a
            com.tencent.android.tpush.horse.a r0 = r0.b
            com.tencent.android.tpush.horse.b r0 = r0.d
            com.tencent.android.tpush.horse.c r1 = r3.a
            com.tencent.android.tpush.horse.n r1 = r1.a()
            java.nio.channels.SocketChannel r1 = r1.a()
            r0.a(r1, r4)
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.horse.d.a(com.tencent.android.tpush.horse.data.StrategyItem, com.tencent.android.tpush.horse.data.StrategyItem):void");
    }

    public void b(StrategyItem strategyItem) {
        a.h("BaseTask", "Horse onFail(" + strategyItem + ")");
        if (strategyItem.f() != 1) {
            this.a.b.e.decrementAndGet();
            if (this.a.b.d != null && !this.a.b.b()) {
                this.a.b.d.a(strategyItem);
            }
        } else if (!this.a.b.f) {
            this.a.b.e.decrementAndGet();
            if (this.a.b.d != null && !this.a.b.b()) {
                this.a.b.d.a(strategyItem);
            }
        }
    }
}
