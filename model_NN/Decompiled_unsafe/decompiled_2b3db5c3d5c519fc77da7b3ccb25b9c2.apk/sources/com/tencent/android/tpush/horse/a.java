package com.tencent.android.tpush.horse;

import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.horse.data.StrategyItem;
import java.lang.Thread;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: ProGuard */
public abstract class a {
    /* access modifiers changed from: private */
    public static final Object a = new Object();
    /* access modifiers changed from: private */
    public LinkedBlockingQueue b = new LinkedBlockingQueue();
    private ConcurrentHashMap c = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public b d;
    /* access modifiers changed from: private */
    public AtomicInteger e = new AtomicInteger(0);
    /* access modifiers changed from: private */
    public volatile boolean f = false;

    public abstract void e();

    public abstract void f();

    public void a() {
        this.e.set(0);
    }

    public boolean b() {
        return this.e.get() > 0;
    }

    public boolean c() {
        return this.f;
    }

    public LinkedBlockingQueue d() {
        return this.b;
    }

    public void g() {
        if (XGPushConfig.enableDebug) {
            com.tencent.android.tpush.a.a.c("BaseTask", "startTask() with strategyItems size = " + this.b.size());
        }
        int i = 0;
        while (i < 2) {
            try {
                if (this.c.get(Integer.valueOf(i)) == null || ((c) this.c.get(Integer.valueOf(i))).getState() == Thread.State.TERMINATED) {
                    c cVar = new c(this, i);
                    this.c.put(Integer.valueOf(i), cVar);
                    cVar.start();
                    i++;
                } else {
                    if (!((c) this.c.get(Integer.valueOf(i))).isAlive()) {
                        ((c) this.c.get(Integer.valueOf(i))).start();
                    }
                    i++;
                }
            } catch (Exception e2) {
                this.c.remove(Integer.valueOf(i));
                com.tencent.android.tpush.a.a.c(Constants.HorseLogTag, "startTask", e2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(List list) {
        if (list != null) {
            if (1 <= list.size()) {
                this.b.clear();
                this.f = false;
                a();
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    StrategyItem strategyItem = (StrategyItem) it.next();
                    if (!this.b.contains(strategyItem)) {
                        this.b.add(strategyItem);
                        this.e.incrementAndGet();
                    }
                }
            }
        }
        if (this.d != null && !b()) {
            this.d.a(null);
        }
    }

    public void a(int i) {
        c cVar;
        try {
            if (!this.c.isEmpty()) {
                for (Integer intValue : this.c.keySet()) {
                    int intValue2 = intValue.intValue();
                    if (!(intValue2 == i || (cVar = (c) this.c.get(Integer.valueOf(intValue2))) == null || cVar.a() == null)) {
                        cVar.a().c();
                    }
                }
                c cVar2 = (c) this.c.remove(Integer.valueOf(i));
                if (cVar2 != null) {
                    cVar2.interrupt();
                }
            }
        } catch (Exception e2) {
            com.tencent.android.tpush.a.a.c("BaseTask", "stopOtherHorse", e2);
        }
    }

    public void a(b bVar) {
        this.d = bVar;
    }
}
