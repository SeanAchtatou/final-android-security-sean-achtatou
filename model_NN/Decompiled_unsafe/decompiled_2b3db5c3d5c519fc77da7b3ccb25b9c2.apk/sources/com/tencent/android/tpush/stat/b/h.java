package com.tencent.android.tpush.stat.b;

import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: ProGuard */
class h {
    private static h b = null;
    private Map a;

    private h(Context context) {
        this.a = null;
        this.a = new HashMap(3);
        this.a.put(1, new f(context));
        this.a.put(2, new b(context));
        this.a.put(4, new e(context));
    }

    static synchronized h a(Context context) {
        h hVar;
        synchronized (h.class) {
            if (b == null) {
                b = new h(context);
            }
            hVar = b;
        }
        return hVar;
    }

    /* access modifiers changed from: package-private */
    public d a() {
        return a(new ArrayList(Arrays.asList(1, 2, 4)));
    }

    /* access modifiers changed from: package-private */
    public d a(List list) {
        d e;
        if (list != null && list.size() >= 0) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                g gVar = (g) this.a.get((Integer) it.next());
                if (gVar != null && (e = gVar.e()) != null && e.a()) {
                    return e;
                }
            }
        }
        return new d();
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar) {
        for (Map.Entry value : this.a.entrySet()) {
            ((g) value.getValue()).a(dVar);
        }
    }
}
