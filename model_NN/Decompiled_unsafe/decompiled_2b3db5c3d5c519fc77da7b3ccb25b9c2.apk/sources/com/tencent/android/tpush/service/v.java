package com.tencent.android.tpush.service;

import android.os.PowerManager;

/* compiled from: ProGuard */
public class v {
    private static v a = null;
    private PowerManager.WakeLock b = null;

    private v() {
    }

    public static v a() {
        if (a == null) {
            a = new v();
        }
        return a;
    }

    public PowerManager.WakeLock b() {
        return this.b;
    }

    public void a(PowerManager.WakeLock wakeLock) {
        this.b = wakeLock;
    }
}
