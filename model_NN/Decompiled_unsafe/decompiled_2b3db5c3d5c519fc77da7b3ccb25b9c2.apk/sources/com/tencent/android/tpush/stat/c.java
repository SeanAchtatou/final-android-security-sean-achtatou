package com.tencent.android.tpush.stat;

import android.content.Context;
import com.tencent.android.tpush.service.a.a;
import com.tencent.android.tpush.stat.a.e;
import com.tencent.android.tpush.stat.a.f;
import com.tencent.android.tpush.stat.a.g;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class c {
    static d a = new d(2);
    static d b = new d(1);
    static String c = "__HIBERNATE__";
    static volatile String d = "pingma.qq.com:80";
    private static f e = e.b();
    private static StatReportStrategy f = StatReportStrategy.APP_LAUNCH;
    private static boolean g = false;
    private static boolean h = true;
    private static volatile String i = "http://pingma.qq.com:80/mstat/report";
    private static boolean j = false;
    private static short k = 6;

    public static StatReportStrategy a() {
        return f;
    }

    public static void a(StatReportStrategy statReportStrategy) {
        f = statReportStrategy;
        if (b()) {
            e.h("Change to statSendStrategy: " + statReportStrategy);
        }
    }

    public static boolean b() {
        return g;
    }

    public static boolean c() {
        return h && a.a(h.a(null)).B == 1;
    }

    public static void a(boolean z) {
        h = z;
        if (!z) {
            e.c("!!!!!!MTA StatService has been disabled!!!!!!");
        }
    }

    static void a(Context context, JSONObject jSONObject) {
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next.equalsIgnoreCase(Integer.toString(b.a))) {
                    a(context, b, jSONObject.getJSONObject(next));
                } else if (next.equalsIgnoreCase(Integer.toString(a.a))) {
                    a(context, a, jSONObject.getJSONObject(next));
                }
            }
        } catch (JSONException e2) {
            e.b((Throwable) e2);
        }
    }

    static void a(Context context, d dVar, JSONObject jSONObject) {
        boolean z;
        boolean z2 = false;
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next.equalsIgnoreCase("v")) {
                    int i2 = jSONObject.getInt(next);
                    if (dVar.d != i2) {
                        z = true;
                    } else {
                        z = z2;
                    }
                    dVar.d = i2;
                } else if (next.equalsIgnoreCase("c")) {
                    String string = jSONObject.getString("c");
                    if (string.length() > 0) {
                        dVar.b = new JSONObject(string);
                    }
                    z = z2;
                } else {
                    if (next.equalsIgnoreCase("m")) {
                        dVar.c = jSONObject.getString("m");
                    }
                    z = z2;
                }
                z2 = z;
            }
            if (z2 && dVar.a == b.a) {
                a(dVar.b);
                b(dVar.b);
            }
            a(context, dVar);
        } catch (JSONException e2) {
            e.b((Throwable) e2);
        } catch (Throwable th) {
            e.b(th);
        }
    }

    static void a(JSONObject jSONObject) {
        try {
            StatReportStrategy a2 = StatReportStrategy.a(jSONObject.getInt("rs"));
            if (a2 != null) {
                a(a2);
            }
        } catch (JSONException e2) {
            if (b()) {
                e.b("rs not found.");
            }
        }
    }

    static void a(Context context, d dVar) {
        if (dVar.a == b.a) {
            b = dVar;
            a(b.b);
        } else if (dVar.a == a.a) {
            a = dVar;
        }
    }

    static void b(JSONObject jSONObject) {
        if (jSONObject != null && jSONObject.length() != 0) {
            try {
                String string = jSONObject.getString(c);
                if (b()) {
                    e.h("hibernateVer:" + string + ", current version:" + "2.0.6");
                }
                long a2 = e.a(string);
                if (e.a("2.0.6") <= a2) {
                    a(a2);
                }
            } catch (JSONException e2) {
                e.h("__HIBERNATE__ not found.");
            }
        }
    }

    static void a(long j2) {
        g.b(f.a(), c, j2);
        a(false);
        e.c("MTA is disable for current SDK version");
    }

    public static String d() {
        return i;
    }

    public static boolean e() {
        return j;
    }

    public static void b(boolean z) {
        j = z;
    }

    public static short f() {
        return k;
    }
}
