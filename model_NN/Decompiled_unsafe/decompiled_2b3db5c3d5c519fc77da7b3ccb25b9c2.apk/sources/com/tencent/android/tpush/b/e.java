package com.tencent.android.tpush.b;

import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.common.p;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class e {
    public int a = 1;
    public String b = "";
    public f c = new f();
    public String d = "";
    public String e = "";
    public String f = "";
    public int g = 0;
    public String h = "";
    public String i = "";
    public String j = "";

    /* access modifiers changed from: private */
    public void a(String str) {
        JSONObject jSONObject = new JSONObject(str);
        if (!jSONObject.isNull("action_type")) {
            this.a = jSONObject.getInt("action_type");
        }
        if (!jSONObject.isNull(Constants.FLAG_ACTIVITY_NAME)) {
            this.b = jSONObject.getString(Constants.FLAG_ACTIVITY_NAME);
        }
        if (!jSONObject.isNull("aty_attr")) {
            String optString = jSONObject.optString("aty_attr");
            if (!p.b(optString)) {
                try {
                    JSONObject jSONObject2 = new JSONObject(optString);
                    this.c.a = jSONObject2.optInt("if");
                    this.c.b = jSONObject2.optInt("pf");
                } catch (Exception e2) {
                    a.c(Constants.LogTag, "decode activityAttribute error", e2);
                }
            }
        }
        if (!jSONObject.isNull("intent")) {
            this.d = jSONObject.getString("intent");
        }
        if (!jSONObject.isNull("browser")) {
            this.e = jSONObject.getString("browser");
            JSONObject jSONObject3 = new JSONObject(this.e);
            if (!jSONObject3.isNull("url")) {
                this.f = jSONObject3.getString("url");
            }
            if (!jSONObject3.isNull("confirm")) {
                this.g = jSONObject3.getInt("confirm");
            }
        }
        if (!jSONObject.isNull("package_name")) {
            this.i = jSONObject.getString("package_name");
            JSONObject jSONObject4 = new JSONObject(this.i);
            if (!jSONObject4.isNull(Constants.FLAG_PACKAGE_DOWNLOAD_URL)) {
                this.j = jSONObject4.getString(Constants.FLAG_PACKAGE_DOWNLOAD_URL);
            }
            if (!jSONObject4.isNull(Constants.FLAG_PACKAGE_NAME)) {
                this.h = jSONObject4.getString(Constants.FLAG_PACKAGE_NAME);
            }
            if (!jSONObject4.isNull("confirm")) {
                this.g = jSONObject4.getInt("confirm");
            }
        }
    }
}
