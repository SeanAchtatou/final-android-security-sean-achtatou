package com.tencent.android.tpush.stat.a;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.os.Environment;
import android.os.Process;
import android.os.StatFs;
import android.support.v7.widget.ActivityChooserView;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.data.a;
import com.tencent.android.tpush.service.a.c;
import com.tencent.android.tpush.service.a.d;
import com.tencent.android.tpush.service.cache.CacheManager;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.zip.GZIPInputStream;
import org.apache.http.HttpHost;

/* compiled from: ProGuard */
public class e {
    private static String a = null;
    private static String b = null;
    private static String c = null;
    private static Random d = null;
    private static Map e = new HashMap(10);
    private static DisplayMetrics f = null;
    private static String g = null;
    private static String h = "";
    private static f i = null;
    private static String j = null;
    private static String k = null;
    private static String l = null;
    private static long m = -1;
    private static long n = -1;
    private static int o = 0;

    private static synchronized Random f() {
        Random random;
        synchronized (e.class) {
            if (d == null) {
                d = new Random();
            }
            random = d;
        }
        return random;
    }

    public static String a(Context context, long j2) {
        List<ResolveInfo> a2;
        a registerInfoByPkgName;
        try {
            if (e.containsKey(Long.valueOf(j2))) {
                return (String) e.get(Long.valueOf(j2));
            }
            if (!(context == null || (a2 = com.tencent.android.tpush.service.d.e.a(context)) == null)) {
                for (ResolveInfo resolveInfo : a2) {
                    String str = resolveInfo.activityInfo.packageName;
                    if (!(str == null || (registerInfoByPkgName = CacheManager.getRegisterInfoByPkgName(str)) == null)) {
                        d a3 = c.a(context, str);
                        if (registerInfoByPkgName.a == j2) {
                            String str2 = a3.a + "";
                            e.put(Long.valueOf(registerInfoByPkgName.a), str2);
                            return str2;
                        }
                    }
                }
            }
            return "0";
        } catch (Throwable th) {
        }
    }

    public static int a() {
        return f().nextInt(ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
    }

    public static byte[] a(byte[] bArr) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
        byte[] bArr2 = new byte[4096];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bArr.length * 2);
        while (true) {
            int read = gZIPInputStream.read(bArr2);
            if (read != -1) {
                byteArrayOutputStream.write(bArr2, 0, read);
            } else {
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                byteArrayInputStream.close();
                gZIPInputStream.close();
                byteArrayOutputStream.close();
                return byteArray;
            }
        }
    }

    public static HttpHost a(Context context) {
        if (context == null) {
            return null;
        }
        try {
            if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0) {
                return null;
            }
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return null;
            }
            if (activeNetworkInfo.getTypeName() != null && activeNetworkInfo.getTypeName().equalsIgnoreCase("WIFI")) {
                return null;
            }
            String extraInfo = activeNetworkInfo.getExtraInfo();
            if (extraInfo == null) {
                return null;
            }
            if (extraInfo.equals("cmwap") || extraInfo.equals("3gwap") || extraInfo.equals("uniwap")) {
                return new HttpHost("10.0.0.172", 80);
            }
            if (extraInfo.equals("ctwap")) {
                return new HttpHost("10.0.0.200", 80);
            }
            String defaultHost = Proxy.getDefaultHost();
            if (defaultHost != null && defaultHost.trim().length() > 0) {
                return new HttpHost(defaultHost, Proxy.getDefaultPort());
            }
            return null;
        } catch (Throwable th) {
            i.b(th);
        }
    }

    public static DisplayMetrics b(Context context) {
        if (f == null) {
            f = new DisplayMetrics();
            ((WindowManager) context.getApplicationContext().getSystemService("window")).getDefaultDisplay().getMetrics(f);
        }
        return f;
    }

    public static String c(Context context) {
        TelephonyManager telephonyManager;
        if (g != null) {
            return g;
        }
        try {
            if (d(context) && (telephonyManager = (TelephonyManager) context.getSystemService("phone")) != null) {
                g = telephonyManager.getSimOperator();
            }
        } catch (Throwable th) {
            i.b(th);
        }
        return g;
    }

    public static String b(Context context, long j2) {
        return com.tencent.android.tpush.service.d.d.a(context).a(j2);
    }

    public static boolean d(Context context) {
        if (context.getPackageManager().checkPermission("android.permission.READ_PHONE_STATE", context.getPackageName()) != 0) {
            return false;
        }
        return true;
    }

    public static String e(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                String typeName = activeNetworkInfo.getTypeName();
                String extraInfo = activeNetworkInfo.getExtraInfo();
                if (typeName != null) {
                    if (typeName.equalsIgnoreCase("WIFI")) {
                        return "WIFI";
                    }
                    if (typeName.equalsIgnoreCase("MOBILE")) {
                        if (extraInfo == null || extraInfo.trim().length() <= 0) {
                            return "MOBILE";
                        }
                        return extraInfo;
                    } else if (extraInfo == null || extraInfo.trim().length() <= 0) {
                        return typeName;
                    } else {
                        return extraInfo;
                    }
                }
            }
        } catch (Throwable th) {
            i.b(th);
        }
        return "";
    }

    public static Integer f(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager != null) {
                return Integer.valueOf(telephonyManager.getNetworkType());
            }
        } catch (Throwable th) {
        }
        return null;
    }

    public static synchronized f b() {
        f fVar;
        synchronized (e.class) {
            if (i == null) {
                i = new f("XgStat");
                i.a(false);
            }
            fVar = i;
        }
        return fVar;
    }

    public static long c() {
        try {
            Calendar instance = Calendar.getInstance();
            instance.set(11, 0);
            instance.set(12, 0);
            instance.set(13, 0);
            instance.set(14, 0);
            return instance.getTimeInMillis() + 86400000;
        } catch (Throwable th) {
            i.b(th);
            return System.currentTimeMillis() + 86400000;
        }
    }

    public static Long a(String str, String str2, int i2, int i3, Long l2) {
        if (str == null || str2 == null) {
            return l2;
        }
        if (str2.equalsIgnoreCase(".") || str2.equalsIgnoreCase("|")) {
            str2 = "\\" + str2;
        }
        String[] split = str.split(str2);
        if (split.length != i3) {
            return l2;
        }
        try {
            Long l3 = 0L;
            int i4 = 0;
            while (i4 < split.length) {
                Long valueOf = Long.valueOf(((long) i2) * (l3.longValue() + Long.valueOf(split[i4]).longValue()));
                i4++;
                l3 = valueOf;
            }
            return l3;
        } catch (NumberFormatException e2) {
            return l2;
        }
    }

    public static long a(String str) {
        return a(str, ".", 100, 3, 0L).longValue();
    }

    public static boolean b(String str) {
        if (str == null || str.trim().length() == 0) {
            return false;
        }
        return true;
    }

    public static String g(Context context) {
        String path;
        if (b(j)) {
            return j;
        }
        try {
            String externalStorageState = Environment.getExternalStorageState();
            if (!(externalStorageState == null || !externalStorageState.equals("mounted") || (path = Environment.getExternalStorageDirectory().getPath()) == null)) {
                StatFs statFs = new StatFs(path);
                j = String.valueOf((((long) statFs.getBlockSize()) * ((long) statFs.getAvailableBlocks())) / 1000000) + "/" + String.valueOf((((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize())) / 1000000);
                return j;
            }
        } catch (Throwable th) {
            i.b(th);
        }
        return null;
    }

    public static String h(Context context) {
        try {
            if (k != null) {
                return k;
            }
            int myPid = Process.myPid();
            Iterator<ActivityManager.RunningAppProcessInfo> it = ((ActivityManager) context.getSystemService(Constants.FLAG_ACTIVITY_NAME)).getRunningAppProcesses().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ActivityManager.RunningAppProcessInfo next = it.next();
                if (next.pid == myPid) {
                    k = next.processName;
                    break;
                }
            }
            return k;
        } catch (Throwable th) {
        }
    }

    public static String a(Context context, String str) {
        if (!com.tencent.android.tpush.stat.c.e()) {
            return str;
        }
        if (k == null) {
            k = h(context);
        }
        if (k != null) {
            return str + "_" + k;
        }
        return str;
    }

    public static String d() {
        if (b(l)) {
            return l;
        }
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        l = String.valueOf((((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / 1000000) + "/" + String.valueOf(e() / 1000000);
        return l;
    }

    public static long e() {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
    }

    public static String i(Context context) {
        try {
            return String.valueOf(m(context) / 1000000) + "/" + String.valueOf(g() / 1000000);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    private static long m(Context context) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) context.getSystemService(Constants.FLAG_ACTIVITY_NAME)).getMemoryInfo(memoryInfo);
        return memoryInfo.availMem;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x004f A[ExcHandler: all (th java.lang.Throwable), Splitter:B:6:0x001c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long g() {
        /*
            long r0 = com.tencent.android.tpush.stat.a.e.m
            r2 = 0
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x000b
            long r0 = com.tencent.android.tpush.stat.a.e.m
        L_0x000a:
            return r0
        L_0x000b:
            java.lang.String r1 = "/proc/meminfo"
            r4 = 1
            r0 = 0
            java.io.FileReader r3 = new java.io.FileReader     // Catch:{ Exception -> 0x0041, all -> 0x0048 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0041, all -> 0x0048 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0041, all -> 0x0048 }
            r1 = 8192(0x2000, float:1.14794E-41)
            r2.<init>(r3, r1)     // Catch:{ Exception -> 0x0041, all -> 0x0048 }
            java.lang.String r0 = r2.readLine()     // Catch:{ Exception -> 0x0051, all -> 0x004f }
            if (r0 == 0) goto L_0x0056
            java.lang.String r1 = "\\s+"
            java.lang.String[] r0 = r0.split(r1)     // Catch:{ Exception -> 0x0051, all -> 0x004f }
            r1 = 1
            r0 = r0[r1]     // Catch:{ Exception -> 0x0051, all -> 0x004f }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x0051, all -> 0x004f }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0051, all -> 0x004f }
            int r0 = r0 * 1024
            long r0 = (long) r0
        L_0x0036:
            r2.close()     // Catch:{ Exception -> 0x0054, all -> 0x004f }
            com.tencent.android.tpush.common.e.a(r2)
        L_0x003c:
            com.tencent.android.tpush.stat.a.e.m = r0
            long r0 = com.tencent.android.tpush.stat.a.e.m
            goto L_0x000a
        L_0x0041:
            r1 = move-exception
            r2 = r0
            r0 = r4
        L_0x0044:
            com.tencent.android.tpush.common.e.a(r2)
            goto L_0x003c
        L_0x0048:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x004b:
            com.tencent.android.tpush.common.e.a(r2)
            throw r0
        L_0x004f:
            r0 = move-exception
            goto L_0x004b
        L_0x0051:
            r0 = move-exception
            r0 = r4
            goto L_0x0044
        L_0x0054:
            r3 = move-exception
            goto L_0x0044
        L_0x0056:
            r0 = r4
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.stat.a.e.g():long");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.stat.a.g.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.stat.a.g.a(android.content.Context, java.lang.String, int):int
      com.tencent.android.tpush.stat.a.g.a(android.content.Context, java.lang.String, long):long */
    public static boolean j(Context context) {
        if (n < 0) {
            n = g.a(context, "mta.qq.com.checktime", 0L);
        }
        return Math.abs(System.currentTimeMillis() - n) > 86400000;
    }

    public static void k(Context context) {
        n = System.currentTimeMillis();
        g.b(context, "mta.qq.com.checktime", n);
    }

    public static int a(Context context, boolean z) {
        if (z) {
            o = l(context);
        }
        return o;
    }

    public static int l(Context context) {
        return g.a(context, "mta.qq.com.difftime", 0);
    }

    public static void a(Context context, int i2) {
        o = i2;
        g.b(context, "mta.qq.com.difftime", i2);
    }
}
