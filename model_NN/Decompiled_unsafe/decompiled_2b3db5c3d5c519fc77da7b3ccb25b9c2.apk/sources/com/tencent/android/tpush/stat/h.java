package com.tencent.android.tpush.stat;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import com.tencent.android.tpush.stat.a.e;
import com.tencent.android.tpush.stat.a.f;
import com.tencent.android.tpush.stat.a.g;
import com.tencent.android.tpush.stat.event.b;
import com.tencent.android.tpush.stat.event.d;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class h {
    static volatile long a = 0;
    private static volatile Handler b = null;
    private static volatile int c = 0;
    /* access modifiers changed from: private */
    public static f d = e.b();
    /* access modifiers changed from: private */
    public static Thread.UncaughtExceptionHandler e = null;
    /* access modifiers changed from: private */
    public static Context f = null;
    /* access modifiers changed from: private */
    public static String g = null;
    /* access modifiers changed from: private */
    public static volatile SharedPreferences h = null;

    public static Context a(Context context) {
        return context != null ? context : f;
    }

    public static void b(Context context) {
        if (context != null) {
            f = context.getApplicationContext();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.stat.a.g.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.stat.a.g.a(android.content.Context, java.lang.String, int):int
      com.tencent.android.tpush.stat.a.g.a(android.content.Context, java.lang.String, long):long */
    static boolean c(Context context) {
        long a2 = g.a(context, c.c, 0L);
        long a3 = e.a("2.0.6");
        boolean z = true;
        if (a3 <= a2) {
            d.e("MTA is disable for current version:" + a3 + ",wakeup version:" + a2);
            z = false;
        }
        c.a(z);
        return z;
    }

    static boolean a(String str) {
        if (str == null || str.length() == 0) {
            return true;
        }
        return false;
    }

    static synchronized void d(Context context) {
        synchronized (h.class) {
            if (context != null) {
                if (b == null && c(context)) {
                    Context applicationContext = context.getApplicationContext();
                    f = applicationContext;
                    HandlerThread handlerThread = new HandlerThread("XgStat");
                    handlerThread.start();
                    b = new Handler(handlerThread.getLooper());
                    b.post(new i(applicationContext));
                }
            }
        }
    }

    public static Handler e(Context context) {
        if (b == null) {
            synchronized (h.class) {
                if (b == null) {
                    try {
                        d(context);
                    } catch (Throwable th) {
                        d.a(th);
                        c.a(false);
                    }
                }
            }
        }
        return b;
    }

    static JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            if (c.b.d != 0) {
                jSONObject2.put("v", c.b.d);
            }
            jSONObject.put(Integer.toString(c.b.a), jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            if (c.a.d != 0) {
                jSONObject3.put("v", c.a.d);
            }
            jSONObject.put(Integer.toString(c.a.a), jSONObject3);
        } catch (JSONException e2) {
            d.b((Throwable) e2);
        }
        return jSONObject;
    }

    static void a(Context context, long j) {
        a(new com.tencent.android.tpush.stat.event.e(context, c, a(), j));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.stat.a.g.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.stat.a.g.a(android.content.Context, java.lang.String, int):int
      com.tencent.android.tpush.stat.a.g.a(android.content.Context, java.lang.String, long):long */
    static int b(Context context, long j) {
        boolean z = true;
        long currentTimeMillis = System.currentTimeMillis();
        if (a == 0) {
            a = g.a(f, "_INTER_MTA_NEXT_DAY", 0L);
        }
        if (c != 0 && currentTimeMillis < a) {
            z = false;
        }
        if (z) {
            c = e.a();
            a = e.c();
            g.b(f, "_INTER_MTA_NEXT_DAY", a);
            a(context, j);
        }
        return c;
    }

    public static void a(Context context, String str, Properties properties, long j, long j2) {
        if (c.c()) {
            Context a2 = a(context);
            if (a2 == null) {
                d.e("The Context of StatService.trackCustomEvent() can not be null!");
            } else if (a(str)) {
                d.e("The event_id of StatService.trackCustomEvent() can not be null or empty.");
            } else {
                b bVar = new b(str, null, properties);
                if (e(a2) != null) {
                    b.post(new l(a2, j, bVar, j2));
                }
            }
        }
    }

    public static void a(Context context, ArrayList arrayList) {
        if (c.c()) {
            Context a2 = a(context);
            if (a2 == null) {
                d.e("The Context of StatService.trackCustomEvent() can not be null!");
            } else if (arrayList == null || arrayList.size() == 0) {
                d.e("The reportList of StatService.trackCustomEvent() can not be null or empty.");
            } else if (e(a2) != null) {
                b.post(new m(arrayList, a2));
            }
        }
    }

    public static void b(Context context, ArrayList arrayList) {
        if (c.c()) {
            Context a2 = a(context);
            if (a2 == null) {
                d.e("The Context of StatService.trackCustomEvent() can not be null!");
            } else if (arrayList == null || arrayList.size() == 0) {
                d.e("The reportList of StatService.trackCustomEvent() can not be null or empty.");
            } else if (e(a2) != null) {
                b.post(new n(arrayList, a2));
            }
        }
    }

    public static void c(Context context, ArrayList arrayList) {
        if (c.c()) {
            Context a2 = a(context);
            if (a2 == null) {
                d.e("The Context of StatService.trackCustomEvent() can not be null!");
            } else if (arrayList == null || arrayList.size() == 0) {
                d.e("The reportList of StatService.trackCustomEvent() can not be null or empty.");
            } else if (e(a2) != null) {
                b.post(new o(arrayList, a2));
            }
        }
    }

    public static void a(Context context, int i) {
        if (c.c()) {
            if (c.b()) {
                d.b("commitEvents, maxNumber=" + i);
            }
            Context a2 = a(context);
            if (a2 == null) {
                d.e("The Context of StatService.commitEvents() can not be null!");
            } else if (i < -1 || i == 0) {
                d.e("The maxNumber of StatService.commitEvents() should be -1 or bigger than 0.");
            } else if (a.a(a2).c() && e(a2) != null) {
                b.post(new p());
            }
        }
    }

    static void a(List list) {
        d.h("sentEventList size:" + list.size());
        if (a.a(f).c()) {
            f.b(f).b(list, new q(list));
        } else {
            b(list);
        }
    }

    static void a(d dVar) {
        d.h("send Event:" + dVar);
        if (a.a(f).c()) {
            f.b(f).a(dVar, new j(dVar));
            return;
        }
        b(Arrays.asList(dVar));
    }

    static synchronized void b(List list) {
        synchronized (h.class) {
            if (list != null) {
                try {
                    if (h != null) {
                        d.h("store event size:" + list.size());
                        SharedPreferences.Editor edit = h.edit();
                        for (Object obj : list) {
                            edit.putLong(obj.toString(), System.currentTimeMillis());
                        }
                        edit.commit();
                    }
                } catch (Exception e2) {
                    d.b((Throwable) e2);
                }
            }
        }
        return;
    }

    static synchronized void c(List list) {
        synchronized (h.class) {
            if (list != null) {
                try {
                    if (h != null) {
                        d.h("delete event size:" + list.size());
                        SharedPreferences.Editor edit = h.edit();
                        for (Object obj : list) {
                            edit.remove(obj.toString());
                        }
                        edit.commit();
                    }
                } catch (Exception e2) {
                    d.b((Throwable) e2);
                }
            }
        }
        return;
    }

    static synchronized void d(List list) {
        synchronized (h.class) {
            if (list != null) {
                try {
                    if (h != null) {
                        SharedPreferences.Editor edit = h.edit();
                        for (Object obj : list) {
                            String obj2 = obj.toString();
                            int i = h.getInt(obj2, 1);
                            if (i <= 0 || i > c.f()) {
                                edit.remove(obj2);
                            } else {
                                edit.putInt(obj2, i + 1);
                            }
                        }
                        edit.commit();
                    }
                } catch (Exception e2) {
                    d.b((Throwable) e2);
                }
            }
        }
        return;
    }

    static void b() {
        Map<String, ?> all;
        if (h != null && (all = h.getAll()) != null && all.size() > 0) {
            ArrayList arrayList = new ArrayList(10);
            for (Map.Entry<String, ?> next : all.entrySet()) {
                if (arrayList.size() == 10) {
                    f.b(f).b(arrayList, new k(arrayList));
                }
                arrayList.clear();
            }
        }
    }
}
