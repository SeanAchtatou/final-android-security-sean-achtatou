package com.tencent.android.tpush.service.channel.b;

import com.facebook.login.widget.ProfilePictureView;
import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.service.channel.c.e;
import com.tencent.android.tpush.service.channel.exception.IORefusedException;
import com.tencent.android.tpush.service.channel.exception.InnerException;
import com.tencent.android.tpush.service.channel.exception.UnexpectedDataException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

/* compiled from: ProGuard */
public class h extends i implements e {
    protected HashMap a = new HashMap(4);
    protected int b = 0;
    protected int c = -1;

    public h(int i) {
        this.d = 80;
        this.e = i;
    }

    public synchronized void d() {
        super.d();
        this.a.clear();
    }

    public int a(OutputStream outputStream) {
        int i;
        IORefusedException e;
        c();
        try {
            this.b = 0;
            i = 0;
            while (!b()) {
                try {
                    int i2 = this.b;
                    this.b = i2 + 1;
                    if (i2 > 2) {
                        throw new InnerException("the duration of the current step is too long!");
                    }
                    switch (this.c) {
                        case -5:
                            i += f(outputStream);
                            break;
                        case ProfilePictureView.LARGE:
                            i += e(outputStream);
                            break;
                        case ProfilePictureView.NORMAL:
                            i += d(outputStream);
                            break;
                        case -2:
                            i += c(outputStream);
                            break;
                        case -1:
                            i += b(outputStream);
                            break;
                        case 0:
                            d();
                            break;
                        default:
                            throw new InnerException("illegal step value!");
                    }
                } catch (IORefusedException e2) {
                    e = e2;
                    a.c("Channel.SendPacket", "write >>> IORefusedException thrown", e);
                    return i;
                }
            }
        } catch (IORefusedException e3) {
            IORefusedException iORefusedException = e3;
            i = 0;
            e = iORefusedException;
            a.c("Channel.SendPacket", "write >>> IORefusedException thrown", e);
            return i;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        if (this.c != i) {
            this.b = 0;
        }
        this.c = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, int):int
     arg types: [java.io.OutputStream, short]
     candidates:
      com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, long):int
      com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, byte[]):int
      com.tencent.android.tpush.service.channel.c.e.a(java.io.InputStream, int):boolean
      com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, int):int */
    /* access modifiers changed from: protected */
    public int b(OutputStream outputStream) {
        e.a(outputStream, (int) this.d);
        a(-2);
        return 1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, int):int
     arg types: [java.io.OutputStream, short]
     candidates:
      com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, long):int
      com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, byte[]):int
      com.tencent.android.tpush.service.channel.c.e.a(java.io.InputStream, int):boolean
      com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, int):int */
    /* access modifiers changed from: protected */
    public int c(OutputStream outputStream) {
        e.a(outputStream, (int) this.k);
        switch (this.k) {
            case 1:
            case 10:
                a(-3);
                return 1;
            case 20:
                a(0);
                return 1;
            default:
                throw new UnexpectedDataException("protocol: " + ((int) this.k));
        }
    }

    /* access modifiers changed from: protected */
    public int d(OutputStream outputStream) {
        e.b(outputStream, this.e);
        a(-5);
        return 4;
    }

    /* access modifiers changed from: protected */
    public int e(OutputStream outputStream) {
        e.a(outputStream, this.f);
        a(-5);
        return 4;
    }

    /* access modifiers changed from: protected */
    public int f(OutputStream outputStream) {
        byte[] bArr = (byte[]) this.a.get("packetData");
        if (bArr == null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                if (this.k == 10) {
                    h(byteArrayOutputStream);
                } else {
                    g(byteArrayOutputStream);
                }
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                this.f = (long) (byteArray.length + 10);
                this.a.put("packetData", byteArray);
                this.a.put("packetDataLeftLength", Integer.valueOf(byteArray.length));
                a(-4);
                return 0;
            } catch (IOException e) {
                throw new UnexpectedDataException("packetData can not be write correctly!", e);
            }
        } else {
            int intValue = ((Integer) this.a.get("packetDataLeftLength")).intValue();
            if (intValue == 0) {
                a(0);
                return 0;
            }
            int a2 = e.a(outputStream, bArr);
            this.a.put("packetDataLeftLength", Integer.valueOf(intValue - a2));
            return a2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, int):int
     arg types: [java.io.OutputStream, short]
     candidates:
      com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, long):int
      com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, byte[]):int
      com.tencent.android.tpush.service.channel.c.e.a(java.io.InputStream, int):boolean
      com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, int):int */
    private void g(OutputStream outputStream) {
        this.i = 0;
        if (this.j.needsUpdate()) {
            this.i = 1;
            this.j.update();
        }
        e.a(outputStream, (int) this.i);
        this.g = this.j.getRandom();
        e.a(outputStream, this.g);
        if (this.i != 0) {
            e.a(outputStream, this.j.getEncKey());
        }
        h(outputStream);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, int):int
     arg types: [java.io.ByteArrayOutputStream, short]
     candidates:
      com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, long):int
      com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, byte[]):int
      com.tencent.android.tpush.service.channel.c.e.a(java.io.InputStream, int):boolean
      com.tencent.android.tpush.service.channel.c.e.a(java.io.OutputStream, int):int */
    private void h(OutputStream outputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        e.a(byteArrayOutputStream, this.k != 1 ? 0 : this.j.getInc());
        e.a((OutputStream) byteArrayOutputStream, (int) this.l);
        e.a((OutputStream) byteArrayOutputStream, (int) this.h);
        e.a((OutputStream) byteArrayOutputStream, (int) this.m);
        byteArrayOutputStream.write(this.n);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        if (this.k == 1) {
            byteArray = this.j.encryptData(byteArray);
        }
        e.a(outputStream, byteArray);
    }
}
