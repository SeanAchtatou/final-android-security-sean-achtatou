package com.tencent.android.tpush.service;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.facebook.share.internal.ShareConstants;
import com.jg.EType;
import com.jg.JgClassChecked;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.common.g;
import com.tencent.android.tpush.common.m;
import com.tencent.android.tpush.common.p;
import com.tencent.android.tpush.encrypt.Rijndael;
import com.tencent.android.tpush.service.b.j;
import com.tencent.android.tpush.service.cache.CacheManager;
import com.tencent.android.tpush.service.channel.b;
import com.tencent.android.tpush.service.channel.protocol.TpnsRegisterReq;
import com.tencent.android.tpush.service.channel.protocol.TpnsRegisterRsp;
import com.tencent.android.tpush.service.channel.protocol.TpnsUnregisterReq;
import com.tencent.android.tpush.service.channel.security.TpnsSecurity;
import com.tencent.android.tpush.service.d.c;
import com.tencent.android.tpush.service.d.e;
import org.json.JSONException;
import org.json.JSONObject;

@JgClassChecked(author = 1, fComment = "确认已进行安全校验", lastDate = "20150316", reviewer = 3, vComment = {EType.INTENTCHECK})
/* compiled from: ProGuard */
public class a {
    public static final String a = a.class.getSimpleName();
    private static a b = null;
    private static volatile j c = null;
    private static volatile l d = null;

    private a() {
    }

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (b == null) {
                b = new a();
            }
            aVar = b;
        }
        return aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent} */
    public synchronized void a(Context context) {
        if (context != null) {
            try {
                if (c == null) {
                    c = new j(this, null);
                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addDataScheme("package");
                    intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
                    intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
                    intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
                    context.registerReceiver(c, intentFilter);
                }
            } catch (Exception e) {
                com.tencent.android.tpush.a.a.c(a, "registerReceiver", e);
            }
            try {
                if (d == null) {
                    d = new l(this, null);
                    IntentFilter intentFilter2 = new IntentFilter();
                    intentFilter2.addAction("com.tencent.android.tpush.action.REGISTER");
                    intentFilter2.addAction("com.tencent.android.tpush.action.UNREGISTER");
                    intentFilter2.addAction("com.tencent.android.tpush.action.ENABLE_DEBUG");
                    intentFilter2.addAction("com.tencent.android.tpush.action.MSG_ACK");
                    intentFilter2.addAction("com.tencent.android.tpush.action.TAG");
                    intentFilter2.addAction("com.tencent.android.tpush.action.PUSH_CLICK.RESULT");
                    intentFilter2.addAction("com.tencent.android.tpush.action.PUSH_CANCELLED.RESULT");
                    intentFilter2.addAction("com.tencent.android.tpush.action.CUSTOM_NOTIFICATION");
                    intentFilter2.addAction("com.tencent.android.tpush.action.CLEAR_CUSTOM_NOTIFICATION");
                    intentFilter2.addAction("com.tencent.android.tpush.action.ack.sdk2srv");
                    intentFilter2.addAction("com.tencent.android.tpush.action.reserved.act");
                    intentFilter2.addAction("com.tencent.android.tpush.action.UPDATE_OTHER_PUSH_TOKEN");
                    context.registerReceiver(d, intentFilter2);
                }
            } catch (Exception e2) {
                com.tencent.android.tpush.a.a.c(a, "registerReceiver", e2);
            }
            Intent intent = new Intent("com.tencent.android.tpush.action.SERVICE_START");
            intent.putExtra("pkg", m.e().getPackageName());
            intent.putExtra("ver", 2.45f);
            m.e().sendBroadcast(intent);
        }
        return;
    }

    public void b(Context context) {
        if (context != null) {
            if (c != null) {
                p.a(context, c);
                c = null;
            }
            if (d != null) {
                p.a(context, d);
                d = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Context context, Intent intent) {
        String dataString = intent.getDataString();
        if (dataString != null && context != null && e.d(context, dataString.substring(8))) {
            m.a().d();
            g.a().a(new b(this, context), 2000);
        }
    }

    /* access modifiers changed from: private */
    public void b(Context context, Intent intent) {
        String dataString = intent.getDataString();
        if (dataString != null && context != null) {
            g.a().a(new c(this, context, dataString.substring(8)), 30000);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, int):int
      com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.tencent.android.tpush.common.m.a(android.content.Context, java.lang.String, long):long */
    /* access modifiers changed from: private */
    public void c(Context context) {
        boolean z = true;
        if (com.tencent.android.tpush.service.d.a.a(context) == 3) {
            long currentTimeMillis = System.currentTimeMillis();
            long c2 = e.c(context, "com.tencent.android.tpush.action.next.applist.ts", 0);
            if (c2 == 0) {
                c2 = m.a(context, "com.tencent.android.tpush.action.next.applist.ts", 0L);
            }
            if (c2 != 0 && currentTimeMillis <= c2 && Math.abs(c2 - currentTimeMillis) <= 172800000) {
                z = false;
            }
            if (z) {
                g.a().a(new d(this, context, currentTimeMillis), 5000);
            }
        }
    }

    /* access modifiers changed from: private */
    public void c(Context context, Intent intent) {
        if (context != null && intent != null) {
            String decrypt = Rijndael.decrypt(intent.getStringExtra("accId"));
            String decrypt2 = Rijndael.decrypt(intent.getStringExtra("accKey"));
            String decrypt3 = Rijndael.decrypt(intent.getStringExtra(Constants.FLAG_PACK_NAME));
            String decrypt4 = Rijndael.decrypt(intent.getStringExtra(Constants.FLAG_ACCOUNT));
            String decrypt5 = Rijndael.decrypt(intent.getStringExtra(Constants.FLAG_TICKET));
            int intExtra = intent.getIntExtra(Constants.FLAG_TICKET_TYPE, -1);
            String decrypt6 = Rijndael.decrypt(intent.getStringExtra("qua"));
            String stringExtra = intent.getStringExtra("appVer");
            String decrypt7 = Rijndael.decrypt(intent.getStringExtra("reserved"));
            boolean booleanExtra = intent.getBooleanExtra("aidl", false);
            try {
                if (!e.a(decrypt6)) {
                    CacheManager.setQua(context, Long.parseLong(decrypt), decrypt6);
                }
                String encryptAPKSignature = TpnsSecurity.getEncryptAPKSignature(context.createPackageContext(decrypt3, 0));
                p.a().a(Long.parseLong(decrypt), decrypt2, c.a(), decrypt4, decrypt5, intExtra, encryptAPKSignature, stringExtra, decrypt7, new f(this, decrypt, decrypt3, booleanExtra, context));
                XGWatchdog.getInstance(context).sendAllLocalXGAppList();
            } catch (Exception e) {
                com.tencent.android.tpush.a.a.h(a, ">> register error " + e);
            }
        }
    }

    /* access modifiers changed from: private */
    public void d(Context context, Intent intent) {
        if (context != null && intent != null) {
            long longExtra = intent.getLongExtra("accId", -1);
            String decrypt = Rijndael.decrypt(intent.getStringExtra(Constants.FLAG_PACK_NAME));
            int intExtra = intent.getIntExtra(Constants.FLAG_TAG_TYPE, -1);
            String decrypt2 = Rijndael.decrypt(intent.getStringExtra(Constants.FLAG_TAG_NAME));
            p.a().a(longExtra, decrypt, intExtra, decrypt2, new g(this, longExtra, intExtra, decrypt2, decrypt));
        }
    }

    /* access modifiers changed from: private */
    public void e(Context context, Intent intent) {
        if (context != null && intent != null) {
            String decrypt = Rijndael.decrypt(intent.getStringExtra("accId"));
            String decrypt2 = Rijndael.decrypt(intent.getStringExtra("accKey"));
            String decrypt3 = Rijndael.decrypt(intent.getStringExtra(Constants.FLAG_PACK_NAME));
            String decrypt4 = Rijndael.decrypt(intent.getStringExtra(Constants.FLAG_TOKEN));
            CacheManager.UnregisterInfoByPkgName(decrypt3);
            try {
                p.a().a(decrypt4, c.a(), Long.parseLong(decrypt), decrypt2, decrypt3, new h(this, decrypt, decrypt3));
            } catch (Exception e) {
                com.tencent.android.tpush.a.a.h(a, ">>> unregister error " + e);
            }
        }
    }

    /* access modifiers changed from: private */
    public void f(Context context, Intent intent) {
        if (context != null && intent != null) {
            String decrypt = Rijndael.decrypt(intent.getStringExtra("accId"));
            p.a().a(Long.parseLong(decrypt), Rijndael.decrypt(intent.getStringExtra(Constants.FLAG_TOKEN)), Rijndael.decrypt(intent.getStringExtra("other_push_type")), Rijndael.decrypt(intent.getStringExtra("other_push_token")), new i(this, decrypt));
        }
    }

    /* access modifiers changed from: private */
    public void g(Context context, Intent intent) {
        if (intent != null && context != null) {
            intent.getBooleanExtra("debugMode", false);
        }
    }

    private void a(int i, TpnsRegisterReq tpnsRegisterReq, String str, String str2) {
        Intent a2 = e.a(i, str2, 1);
        a2.putExtra("accId", tpnsRegisterReq.accessId);
        if (!(tpnsRegisterReq.account == null || tpnsRegisterReq.account.length() == 0)) {
            a2.putExtra(Constants.FLAG_ACCOUNT, tpnsRegisterReq.account);
        }
        if (!(str == null || str.length() == 0)) {
            a2.putExtra(Constants.FLAG_TOKEN, str);
        }
        if (!(tpnsRegisterReq.ticket == null || tpnsRegisterReq.ticket.length() == 0)) {
            a2.putExtra(Constants.FLAG_TICKET, tpnsRegisterReq.ticket);
            a2.putExtra(Constants.FLAG_TICKET_TYPE, tpnsRegisterReq.ticketType);
        }
        if (!(tpnsRegisterReq.deviceId == null || tpnsRegisterReq.deviceId.length() == 0)) {
            a2.putExtra(Constants.FLAG_DEVICE_ID, tpnsRegisterReq.deviceId);
        }
        m.e().sendBroadcast(a2);
    }

    private void a(int i, String str) {
        m.e().sendBroadcast(e.a(i, str, 2));
    }

    /* access modifiers changed from: private */
    public void a(int i, String str, int i2, String str2) {
        Intent a2 = e.a(i, str2, 3);
        a2.putExtra(Constants.FLAG_TAG_NAME, Rijndael.encrypt(str));
        a2.putExtra(Constants.FLAG_TAG_TYPE, i2);
        m.e().sendBroadcast(a2);
    }

    /* access modifiers changed from: private */
    public void a(int i, TpnsRegisterRsp tpnsRegisterRsp, TpnsRegisterReq tpnsRegisterReq, com.tencent.android.tpush.service.channel.a aVar, String str, boolean z) {
        Intent intent = new Intent("com.tencent.android.tpush.action.REGISTER.RESULT");
        intent.putExtra(ShareConstants.WEB_DIALOG_PARAM_DATA, tpnsRegisterRsp.token);
        intent.putExtra("flag", 0);
        intent.putExtra("code", i);
        intent.putExtra("operation", 0);
        com.tencent.android.tpush.data.a aVar2 = new com.tencent.android.tpush.data.a();
        aVar2.a = tpnsRegisterReq.accessId;
        aVar2.b = tpnsRegisterReq.accessKey;
        aVar2.c = tpnsRegisterRsp.token;
        aVar2.d = str;
        CacheManager.addRegisterInfo(aVar2);
        CacheManager.addRegisterInfos(str);
        j.a().a(m.e(), tpnsRegisterReq.accessId, str);
        CacheManager.setToken(m.e(), tpnsRegisterRsp.token);
        if (!e.a(str)) {
            intent.setPackage(str);
        }
        m.e().sendBroadcast(intent);
        a(i, tpnsRegisterReq, tpnsRegisterRsp.token, str);
        b.a().a(false);
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("aidl", z);
            jSONObject.toString();
        } catch (JSONException e) {
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, String str, TpnsRegisterReq tpnsRegisterReq, com.tencent.android.tpush.service.channel.a aVar, String str2) {
        Intent intent = new Intent("com.tencent.android.tpush.action.REGISTER.RESULT");
        intent.putExtra(ShareConstants.WEB_DIALOG_PARAM_DATA, "");
        intent.putExtra("code", i);
        intent.putExtra("msg", str);
        intent.putExtra("flag", 0);
        intent.putExtra("operation", 1);
        if (!e.a(str2)) {
            intent.setPackage(str2);
        }
        m.e().sendBroadcast(intent);
        a(i, tpnsRegisterReq, tpnsRegisterReq.token, str2);
    }

    /* access modifiers changed from: private */
    public void a(int i, TpnsUnregisterReq tpnsUnregisterReq, com.tencent.android.tpush.service.channel.a aVar, String str) {
        Intent intent = new Intent("com.tencent.android.tpush.action.UNREGISTER.RESULT");
        intent.putExtra("flag", 0);
        intent.putExtra("operation", 0);
        CacheManager.UnregisterInfoSuccessByPkgName(str);
        CacheManager.removeRegisterInfos(str);
        j.a().a(m.e(), str);
        if (!p.b(str)) {
            intent.setPackage(str);
        }
        m.e().sendBroadcast(intent);
        a(i, str);
    }

    /* access modifiers changed from: private */
    public void a(int i, String str, TpnsUnregisterReq tpnsUnregisterReq, com.tencent.android.tpush.service.channel.a aVar, String str2) {
        com.tencent.android.tpush.a.a.h(a, "unregisterFailHandler failed with (" + i + "," + str + "," + tpnsUnregisterReq + "," + aVar + "," + str2 + ")");
        Intent intent = new Intent("com.tencent.android.tpush.action.UNREGISTER.RESULT");
        intent.putExtra("flag", 0);
        intent.putExtra("code", i);
        intent.putExtra("msg", str);
        intent.putExtra("operation", 1);
        if (!p.b(str2)) {
            intent.setPackage(str2);
        }
        m.e().sendBroadcast(intent);
        a(i, str2);
    }
}
