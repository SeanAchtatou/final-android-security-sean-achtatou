package com.tencent.android.tpush.common;

import java.lang.reflect.Method;

/* compiled from: ProGuard */
public class j {
    public static String a(String str) {
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            Method declaredMethod = cls.getDeclaredMethod("get", String.class);
            declaredMethod.setAccessible(true);
            return (String) declaredMethod.invoke(cls, str);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean a() {
        try {
            return !p.b(a("ro.miui.ui.version.code")) || !p.b(a(a("ro.miui.ui.version.name"))) || !p.b(a(a("ro.miui.internal.storage")));
        } catch (Throwable th) {
            return false;
        }
    }
}
