package com.tencent.android.tpush;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;
import com.tencent.android.tpush.common.MessageKey;
import com.tencent.android.tpush.common.e;
import org.json.JSONObject;

/* compiled from: ProGuard */
public abstract class XGPushNotificationBuilder {
    public static final String BASIC_NOTIFICATION_BUILDER_TYPE = "basic";
    public static final String CUSTOM_NOTIFICATION_BUILDER_TYPE = "custom";
    protected Integer a = null;
    protected PendingIntent b = null;
    protected RemoteViews c = null;
    protected Integer d = null;
    protected PendingIntent e = null;
    protected Integer f = null;
    protected Integer g = null;
    protected Integer h = null;
    protected Integer i = null;
    protected Integer j = null;
    protected Integer k = null;
    protected Integer l = null;
    protected Uri m = null;
    protected CharSequence n = null;
    protected long[] o = null;
    protected Long p = null;
    protected Integer q = null;
    protected Bitmap r = null;
    protected Integer s = null;
    protected String t;
    protected Integer u = null;

    /* access modifiers changed from: protected */
    public abstract void a(JSONObject jSONObject);

    /* access modifiers changed from: protected */
    public abstract void b(JSONObject jSONObject);

    public abstract Notification buildNotification(Context context);

    public abstract String getType();

    public void encode(JSONObject jSONObject) {
        a(jSONObject);
        e.a(jSONObject, "audioStringType", this.a);
        e.a(jSONObject, "defaults", this.d);
        e.a(jSONObject, "flags", this.f);
        e.a(jSONObject, MessageKey.MSG_ICON, this.g);
        e.a(jSONObject, "iconLevel", this.h);
        e.a(jSONObject, "ledARGB", this.i);
        e.a(jSONObject, "ledOffMS", this.j);
        e.a(jSONObject, "ledOnMS", this.k);
        e.a(jSONObject, "number", this.l);
        e.a(jSONObject, "sound", this.m);
        e.a(jSONObject, "smallIcon", this.q);
        e.a(jSONObject, "notificationLargeIcon", this.s);
        if (this.o != null) {
            StringBuilder sb = new StringBuilder();
            for (int i2 = 0; i2 < this.o.length; i2++) {
                sb.append(String.valueOf(this.o[i2]));
                if (i2 != this.o.length - 1) {
                    sb.append(",");
                }
            }
            e.a(jSONObject, MessageKey.MSG_VIBRATE, sb.toString());
        }
        e.a(jSONObject, "notificationId", this.u);
    }

    public void decode(String str) {
        JSONObject jSONObject = new JSONObject(str);
        b(jSONObject);
        this.a = (Integer) e.b(jSONObject, "audioStringType", null);
        this.d = (Integer) e.b(jSONObject, "defaults", null);
        this.f = (Integer) e.b(jSONObject, "flags", null);
        this.g = (Integer) e.b(jSONObject, MessageKey.MSG_ICON, null);
        this.h = (Integer) e.b(jSONObject, "iconLevel", null);
        this.i = (Integer) e.b(jSONObject, "ledARGB", null);
        this.j = (Integer) e.b(jSONObject, "ledOffMS", null);
        this.k = (Integer) e.b(jSONObject, "ledOnMS", null);
        this.l = (Integer) e.b(jSONObject, "number", null);
        String str2 = (String) e.b(jSONObject, "sound", null);
        this.q = (Integer) e.b(jSONObject, "smallIcon", null);
        this.s = (Integer) e.b(jSONObject, "notificationLargeIcon", null);
        if (str2 != null) {
            this.m = Uri.parse(str2);
        }
        String str3 = (String) e.b(jSONObject, MessageKey.MSG_VIBRATE, null);
        if (str3 != null) {
            String[] split = str3.split(",");
            int length = split.length;
            this.o = new long[length];
            for (int i2 = 0; i2 < length; i2++) {
                try {
                    this.o[i2] = Long.valueOf(split[i2]).longValue();
                } catch (NumberFormatException e2) {
                }
            }
        }
        this.u = (Integer) e.b(jSONObject, "notificationId", null);
    }

    public String getTitle(Context context) {
        if (this.t == null) {
            this.t = (String) context.getApplicationContext().getPackageManager().getApplicationLabel(context.getApplicationInfo());
        }
        return this.t;
    }

    public void setTitle(String str) {
        this.t = str;
    }

    public int getApplicationIcon(Context context) {
        return context.getApplicationInfo().icon;
    }

    /* access modifiers changed from: protected */
    public Notification a(Context context) {
        new Notification();
        if (this.u == null) {
            this.u = 0;
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        if (this.q != null) {
            builder.setSmallIcon(this.q.intValue());
        }
        if (this.r != null) {
            builder.setLargeIcon(this.r);
        }
        if (this.s != null) {
            builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), this.s.intValue()));
        }
        if (this.t == null) {
            this.t = getTitle(context);
        } else {
            builder.setContentTitle(this.t);
        }
        if (this.n == null || this.c != null) {
            builder.setContentText(this.n);
            builder.setTicker(this.n);
        } else {
            bigTextStyle.bigText(this.n);
            builder.setStyle(bigTextStyle);
            builder.setContentText(this.n);
            builder.setTicker(this.n);
        }
        Notification build = builder.build();
        if (this.a != null) {
            build.audioStreamType = this.a.intValue();
        }
        if (this.b != null) {
            build.contentIntent = this.b;
        }
        if (this.c != null) {
            build.contentView = this.c;
        }
        if (this.d != null) {
            build.defaults = this.d.intValue();
        }
        if (this.g != null) {
            build.icon = this.g.intValue();
        }
        if (this.e != null) {
            build.deleteIntent = this.e;
        }
        if (this.f != null) {
            build.flags = this.f.intValue();
        } else {
            build.flags = 16;
        }
        if (this.h != null) {
            build.iconLevel = this.h.intValue();
        }
        if (this.i != null) {
            build.ledARGB = this.i.intValue();
        }
        if (this.j != null) {
            build.ledOffMS = this.j.intValue();
        }
        if (this.k != null) {
            build.ledOnMS = this.k.intValue();
        }
        if (this.l != null) {
            build.number = this.l.intValue();
        }
        if (this.m != null) {
            build.sound = this.m;
        }
        if (this.o != null) {
            build.vibrate = this.o;
        }
        if (this.p != null) {
            build.when = this.p.longValue();
        } else {
            build.when = System.currentTimeMillis();
        }
        return build;
    }

    public int getAudioStringType() {
        return this.a.intValue();
    }

    public XGPushNotificationBuilder setAudioStringType(int i2) {
        this.a = Integer.valueOf(i2);
        return this;
    }

    public PendingIntent getContentIntent() {
        return this.b;
    }

    public XGPushNotificationBuilder setContentIntent(PendingIntent pendingIntent) {
        this.b = pendingIntent;
        return this;
    }

    public XGPushNotificationBuilder setContentView(RemoteViews remoteViews) {
        this.c = remoteViews;
        return this;
    }

    public int getDefaults() {
        return this.d.intValue();
    }

    public XGPushNotificationBuilder setDefaults(int i2) {
        if (this.d == null) {
            this.d = Integer.valueOf(i2);
        } else {
            this.d = Integer.valueOf(this.d.intValue() | i2);
        }
        return this;
    }

    public int getFlags() {
        return this.f.intValue();
    }

    public XGPushNotificationBuilder setFlags(int i2) {
        if (this.f == null) {
            this.f = Integer.valueOf(i2);
        } else {
            this.f = Integer.valueOf(this.f.intValue() | i2);
        }
        return this;
    }

    public Integer getIcon() {
        return this.g;
    }

    public XGPushNotificationBuilder setIcon(Integer num) {
        this.g = num;
        return this;
    }

    public Integer getSmallIcon() {
        return this.q;
    }

    public XGPushNotificationBuilder setSmallIcon(Integer num) {
        this.q = num;
        return this;
    }

    public Bitmap getLargeIcon() {
        return this.r;
    }

    public XGPushNotificationBuilder setLargeIcon(Bitmap bitmap) {
        this.r = bitmap;
        return this;
    }

    public XGPushNotificationBuilder setNotificationLargeIcon(int i2) {
        this.s = Integer.valueOf(i2);
        return this;
    }

    public int getIconLevel() {
        return this.h.intValue();
    }

    public XGPushNotificationBuilder setIconLevel(int i2) {
        this.h = Integer.valueOf(i2);
        return this;
    }

    public int getLedARGB() {
        return this.i.intValue();
    }

    public XGPushNotificationBuilder setLedARGB(int i2) {
        this.i = Integer.valueOf(i2);
        return this;
    }

    public int getLedOffMS() {
        return this.j.intValue();
    }

    public XGPushNotificationBuilder setLedOffMS(int i2) {
        this.j = Integer.valueOf(i2);
        return this;
    }

    public int getLedOnMS() {
        return this.k.intValue();
    }

    public XGPushNotificationBuilder setLedOnMS(int i2) {
        this.k = Integer.valueOf(i2);
        return this;
    }

    public int getNumber() {
        return this.l.intValue();
    }

    public XGPushNotificationBuilder setNumber(int i2) {
        this.l = Integer.valueOf(i2);
        return this;
    }

    public Uri getSound() {
        return this.m;
    }

    public XGPushNotificationBuilder setSound(Uri uri) {
        this.m = uri;
        return this;
    }

    public CharSequence getTickerText() {
        return this.n;
    }

    public XGPushNotificationBuilder setTickerText(CharSequence charSequence) {
        this.n = charSequence;
        return this;
    }

    public long[] getVibrate() {
        return this.o;
    }

    public XGPushNotificationBuilder setVibrate(long[] jArr) {
        this.o = jArr;
        return this;
    }

    public long getWhen() {
        return this.p.longValue();
    }

    public XGPushNotificationBuilder setWhen(long j2) {
        this.p = Long.valueOf(j2);
        return this;
    }
}
