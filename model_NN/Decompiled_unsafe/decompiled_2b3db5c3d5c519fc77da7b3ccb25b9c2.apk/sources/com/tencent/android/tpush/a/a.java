package com.tencent.android.tpush.a;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.service.channel.protocol.TpnsPushClientReport;
import com.tencent.android.tpush.service.channel.protocol.TpnsPushMsg;
import com.tencent.android.tpush.service.d.b;
import com.tencent.android.tpush.service.d.e;
import com.tencent.android.tpush.service.m;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.http.client.methods.HttpTrace;

/* compiled from: ProGuard */
public class a {
    public static boolean a = false;
    public static String b = ("tencent" + File.separator + Constants.LogTag + File.separator + "Logs");
    protected static volatile ExecutorService c = Executors.newSingleThreadExecutor(new c());
    public static AtomicInteger d = new AtomicInteger();
    public static AtomicInteger e = new AtomicInteger();
    public static AtomicInteger f = new AtomicInteger();
    public static AtomicInteger g = new AtomicInteger();
    public static AtomicInteger h = new AtomicInteger();
    public static AtomicInteger i = new AtomicInteger();
    public static AtomicInteger j = new AtomicInteger();
    public static AtomicInteger k = new AtomicInteger();
    public static AtomicInteger l = new AtomicInteger();
    public static AtomicInteger m = new AtomicInteger();
    public static AtomicInteger n = new AtomicInteger();
    private static boolean o = false;
    private static Boolean p = null;
    private static final SimpleDateFormat q = new SimpleDateFormat("MM.dd_HH:mm:ss_SSS");
    /* access modifiers changed from: private */
    public static ArrayList r = new ArrayList();
    private static boolean s = false;
    private static boolean t = false;
    private static String u = null;

    public static void a(int i2) {
        switch (i2) {
            case 0:
                a = true;
                return;
            case 1:
                o = true;
                return;
            case 2:
                o = true;
                a = true;
                return;
            case 3:
                o = false;
                a = false;
                return;
            default:
                Log.e("XGLogger", "TLogger ->setLogToFile unknown cmd " + i2);
                return;
        }
    }

    public static boolean a(Context context) {
        if (context == null) {
            return false;
        }
        if (p == null) {
            if (e.b(context, "_isReportEnable", -1) == 1) {
                p = true;
            } else {
                p = false;
            }
        }
        return p.booleanValue();
    }

    public static void a(Context context, int i2) {
        if (context != null) {
            if (i2 == 1) {
                p = true;
            } else {
                p = false;
            }
            e.a(context, "_isReportEnable", i2);
        }
    }

    public static void a(String str, String str2) {
        if (a && b(2)) {
            Log.v("XINGE", "[" + str + "] " + str2);
        }
        a(HttpTrace.METHOD_NAME, str, str2, null);
    }

    public static void b(String str, String str2) {
        if (b(2)) {
            Log.v("XINGE", "[" + str + "] " + str2);
        }
        a(HttpTrace.METHOD_NAME, str, str2, null);
    }

    public static void c(String str, String str2) {
        if (a && b(3)) {
            Log.d("XINGE", "[" + str + "] " + str2);
        }
        a("DEBUG", str, str2, null);
    }

    public static void d(String str, String str2) {
        if (a && b(4)) {
            Log.i("XINGE", "[" + str + "] " + str2);
        }
        a("INFO", str, str2, null);
    }

    public static void e(String str, String str2) {
        if (b(4)) {
            Log.i("XINGE", "[" + str + "] " + str2);
        }
        a("INFO", str, str2, null);
    }

    public static void f(String str, String str2) {
        if (a && b(5)) {
            Log.w("XINGE", "[" + str + "] " + str2);
        }
        a("WARN", str, str2, null);
    }

    public static void g(String str, String str2) {
        if (b(5)) {
            Log.w("XINGE", "[" + str + "] " + str2);
        }
        a("WARN", str, str2, null);
    }

    public static void h(String str, String str2) {
        if (a && b(6)) {
            Log.e("XINGE", "[" + str + "] " + str2);
        }
        a("ERROR", str, str2, null);
    }

    public static void i(String str, String str2) {
        if (b(6)) {
            Log.e("XINGE", "[" + str + "] " + str2);
        }
        a("ERROR", str, str2, null);
    }

    public static void a(String str, String str2, Throwable th) {
        if (a && b(2)) {
            Log.v("XINGE", "[" + str + "] " + str2, th);
        }
        a(HttpTrace.METHOD_NAME, str, str2, th);
    }

    public static void b(String str, String str2, Throwable th) {
        if (a && b(5)) {
            Log.w("XINGE", "[" + str + "] " + str2, th);
        }
        a("WARN", str, str2, th);
    }

    public static void c(String str, String str2, Throwable th) {
        if (a && b(6)) {
            Log.e("XINGE", "[" + str + "] " + str2, th);
        }
        a("ERROR", str, str2, th);
    }

    public static void d(String str, String str2, Throwable th) {
        if (b(6)) {
            Log.e("XINGE", "[" + str + "] " + str2, th);
        }
        a("ERROR", str, str2, th);
    }

    private static boolean b(int i2) {
        return true;
    }

    private static void a(String str, String str2, String str3, Throwable th) {
        if (o || a(m.e())) {
            if (str2 == null || str2.trim().equals("")) {
                str2 = "XGLogger";
            }
            String format = q.format(new Date());
            if (str3 == null) {
                str3 = "";
            }
            BufferedReader bufferedReader = new BufferedReader(new StringReader(str3), 256);
            String a2 = e.a("[" + str2 + "]", 24);
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    a(((Object) format) + " " + e.a(str, 5) + " " + a2 + " " + readLine);
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            if (th != null) {
                StringWriter stringWriter = new StringWriter();
                th.printStackTrace(new PrintWriter(stringWriter));
                a(((Object) format) + " " + str + stringWriter.toString());
            }
        }
    }

    private static void a(String str) {
        if (!t) {
            r.add(str);
            if (r.size() == 100) {
                ArrayList arrayList = r;
                r = new ArrayList();
                s = e.f();
                if (s) {
                    Log.v("XGLogger", "have writable external storage, write log file");
                    a(arrayList);
                    return;
                }
                Log.v("XGLogger", "no writable external storage");
            }
        }
    }

    /* access modifiers changed from: private */
    public static String d() {
        if (u != null) {
            return u;
        }
        try {
            u = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + b;
            return u;
        } catch (Throwable th) {
            Log.e("XGLogger", "TLogger ->getFileNamePre", th);
            return null;
        }
    }

    private static void a(ArrayList arrayList) {
        if (a(m.e())) {
        }
        if (o) {
            try {
                c.execute(new b(arrayList));
            } catch (Exception e2) {
                Log.e("XGLogger", "savelog error", e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void e() {
        try {
            String d2 = d();
            if (d2 != null) {
                File file = new File(d2);
                if (file.exists()) {
                    int length = d2.length() + 5;
                    int length2 = length + b.a.length();
                    for (File file2 : file.listFiles()) {
                        try {
                            if (file2.isFile()) {
                                String absolutePath = file2.getAbsolutePath();
                                if (b.a(b.a(absolutePath.substring(length, length2)), 7)) {
                                    Log.d("XGLogger", "delete logs file " + absolutePath);
                                    file2.delete();
                                }
                            }
                        } catch (Exception e2) {
                            Log.e("XGLogger", "removeOldDebugLogFiles" + e2);
                        }
                    }
                }
            }
        } catch (Exception e3) {
            Log.e("XGLogger", "removeOldDebugLogFiles", e3);
        }
    }

    public static void a(int i2, List list) {
        if (o) {
            ArrayList arrayList = new ArrayList();
            if (list != null && list.size() > 0) {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    arrayList.add(Long.valueOf(((TpnsPushClientReport) it.next()).msgId));
                }
            }
            if (arrayList != null && arrayList.size() > 0) {
                c(i2, arrayList);
            }
        }
    }

    public static void b(int i2, List list) {
        if (o) {
            ArrayList arrayList = new ArrayList();
            if (list != null && list.size() > 0) {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    arrayList.add(Long.valueOf(((TpnsPushMsg) it.next()).msgId));
                }
            }
            if (arrayList != null && arrayList.size() > 0) {
                c(i2, arrayList);
            }
        }
    }

    public static void a(int i2, long j2) {
        if (o) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(Long.valueOf(j2));
            if (arrayList != null && arrayList.size() > 0) {
                c(i2, arrayList);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException} */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005d A[Catch:{ Throwable -> 0x0202 }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00be A[SYNTHETIC, Splitter:B:32:0x00be] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x01e8  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01eb A[SYNTHETIC, Splitter:B:49:0x01eb] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01f6 A[SYNTHETIC, Splitter:B:54:0x01f6] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void c(int r8, java.util.List r9) {
        /*
            java.lang.Class<com.tencent.android.tpush.a.a> r4 = com.tencent.android.tpush.a.a.class
            monitor-enter(r4)
            boolean r0 = com.tencent.android.tpush.a.a.o     // Catch:{ all -> 0x01fa }
            if (r0 != 0) goto L_0x0009
        L_0x0007:
            monitor-exit(r4)
            return
        L_0x0009:
            r1 = 0
            java.text.SimpleDateFormat r5 = new java.text.SimpleDateFormat     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r0 = "yyyy-MM-dd HH:mm:ss"
            r5.<init>(r0)     // Catch:{ Throwable -> 0x0202 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0202 }
            r0.<init>()     // Catch:{ Throwable -> 0x0202 }
            java.io.File r2 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Throwable -> 0x0202 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = "/"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0202 }
            switch(r8) {
                case 0: goto L_0x003a;
                case 1: goto L_0x00c6;
                case 2: goto L_0x00e3;
                case 3: goto L_0x0100;
                case 4: goto L_0x011d;
                case 5: goto L_0x013a;
                case 6: goto L_0x0157;
                case 7: goto L_0x0174;
                case 8: goto L_0x0191;
                case 9: goto L_0x002b;
                case 10: goto L_0x002b;
                case 11: goto L_0x01ae;
                case 12: goto L_0x01cb;
                default: goto L_0x002b;
            }     // Catch:{ Throwable -> 0x0202 }
        L_0x002b:
            java.lang.String r0 = "XGLogger"
            java.lang.String r2 = "unknown case"
            h(r0, r2)     // Catch:{ Throwable -> 0x0202 }
            if (r1 == 0) goto L_0x0007
            r1.close()     // Catch:{ IOException -> 0x0038 }
            goto L_0x0007
        L_0x0038:
            r0 = move-exception
            goto L_0x0007
        L_0x003a:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0202 }
            r2.<init>()     // Catch:{ Throwable -> 0x0202 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = "_0ServerSendToService.txt"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = r0.toString()     // Catch:{ Throwable -> 0x0202 }
            java.util.concurrent.atomic.AtomicInteger r0 = com.tencent.android.tpush.a.a.d     // Catch:{ Throwable -> 0x0202 }
            int r0 = r0.getAndIncrement()     // Catch:{ Throwable -> 0x0202 }
            r3 = r0
            r0 = r2
        L_0x0055:
            java.lang.String r2 = "android.permission.WRITE_EXTERNAL_STORAGE"
            boolean r2 = com.tencent.android.tpush.common.l.a(r2)     // Catch:{ Throwable -> 0x0202 }
            if (r2 == 0) goto L_0x01e8
            java.io.FileWriter r2 = new java.io.FileWriter     // Catch:{ Throwable -> 0x0202 }
            r6 = 1
            r2.<init>(r0, r6)     // Catch:{ Throwable -> 0x0202 }
            java.util.Iterator r1 = r9.iterator()     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
        L_0x0067:
            boolean r0 = r1.hasNext()     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            if (r0 == 0) goto L_0x01e9
            java.lang.Object r0 = r1.next()     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            r6.<init>()     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            java.lang.String r7 = ""
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            java.lang.StringBuilder r6 = r6.append(r3)     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            java.lang.String r7 = "\t"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            java.util.Date r7 = new java.util.Date     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            r7.<init>()     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            java.lang.String r7 = r5.format(r7)     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            java.lang.String r7 = "\t"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            java.lang.String r7 = "msgid: "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            java.lang.StringBuilder r0 = r6.append(r0)     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            java.lang.String r6 = "\n"
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            r2.write(r0)     // Catch:{ Throwable -> 0x00b3, all -> 0x01ff }
            goto L_0x0067
        L_0x00b3:
            r0 = move-exception
            r1 = r2
        L_0x00b5:
            java.lang.String r2 = "XGLogger"
            java.lang.String r3 = "writeMsgSession error"
            c(r2, r3, r0)     // Catch:{ all -> 0x01f3 }
            if (r1 == 0) goto L_0x0007
            r1.close()     // Catch:{ IOException -> 0x00c3 }
            goto L_0x0007
        L_0x00c3:
            r0 = move-exception
            goto L_0x0007
        L_0x00c6:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0202 }
            r2.<init>()     // Catch:{ Throwable -> 0x0202 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = "_1ServiceAckToServer.txt"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = r0.toString()     // Catch:{ Throwable -> 0x0202 }
            java.util.concurrent.atomic.AtomicInteger r0 = com.tencent.android.tpush.a.a.e     // Catch:{ Throwable -> 0x0202 }
            int r0 = r0.getAndIncrement()     // Catch:{ Throwable -> 0x0202 }
            r3 = r0
            r0 = r2
            goto L_0x0055
        L_0x00e3:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0202 }
            r2.<init>()     // Catch:{ Throwable -> 0x0202 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = "_2XgSdkReceiveFromXGService.txt"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = r0.toString()     // Catch:{ Throwable -> 0x0202 }
            java.util.concurrent.atomic.AtomicInteger r0 = com.tencent.android.tpush.a.a.f     // Catch:{ Throwable -> 0x0202 }
            int r0 = r0.getAndIncrement()     // Catch:{ Throwable -> 0x0202 }
            r3 = r0
            r0 = r2
            goto L_0x0055
        L_0x0100:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0202 }
            r2.<init>()     // Catch:{ Throwable -> 0x0202 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = "_3SdkSendAckToService.txt"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = r0.toString()     // Catch:{ Throwable -> 0x0202 }
            java.util.concurrent.atomic.AtomicInteger r0 = com.tencent.android.tpush.a.a.g     // Catch:{ Throwable -> 0x0202 }
            int r0 = r0.getAndIncrement()     // Catch:{ Throwable -> 0x0202 }
            r3 = r0
            r0 = r2
            goto L_0x0055
        L_0x011d:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0202 }
            r2.<init>()     // Catch:{ Throwable -> 0x0202 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = "_4ServiceRecAckFromSdk1.txt"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = r0.toString()     // Catch:{ Throwable -> 0x0202 }
            java.util.concurrent.atomic.AtomicInteger r0 = com.tencent.android.tpush.a.a.h     // Catch:{ Throwable -> 0x0202 }
            int r0 = r0.getAndIncrement()     // Catch:{ Throwable -> 0x0202 }
            r3 = r0
            r0 = r2
            goto L_0x0055
        L_0x013a:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0202 }
            r2.<init>()     // Catch:{ Throwable -> 0x0202 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = "_5ServiceRecAckFromSdk2.txt"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = r0.toString()     // Catch:{ Throwable -> 0x0202 }
            java.util.concurrent.atomic.AtomicInteger r0 = com.tencent.android.tpush.a.a.i     // Catch:{ Throwable -> 0x0202 }
            int r0 = r0.getAndIncrement()     // Catch:{ Throwable -> 0x0202 }
            r3 = r0
            r0 = r2
            goto L_0x0055
        L_0x0157:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0202 }
            r2.<init>()     // Catch:{ Throwable -> 0x0202 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = "_6ServiceRecAckFromSdk3.txt"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = r0.toString()     // Catch:{ Throwable -> 0x0202 }
            java.util.concurrent.atomic.AtomicInteger r0 = com.tencent.android.tpush.a.a.j     // Catch:{ Throwable -> 0x0202 }
            int r0 = r0.getAndIncrement()     // Catch:{ Throwable -> 0x0202 }
            r3 = r0
            r0 = r2
            goto L_0x0055
        L_0x0174:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0202 }
            r2.<init>()     // Catch:{ Throwable -> 0x0202 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = "_7ServiceRecAckFromServer.txt"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = r0.toString()     // Catch:{ Throwable -> 0x0202 }
            java.util.concurrent.atomic.AtomicInteger r0 = com.tencent.android.tpush.a.a.k     // Catch:{ Throwable -> 0x0202 }
            int r0 = r0.getAndIncrement()     // Catch:{ Throwable -> 0x0202 }
            r3 = r0
            r0 = r2
            goto L_0x0055
        L_0x0191:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0202 }
            r2.<init>()     // Catch:{ Throwable -> 0x0202 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = "_8ServiceRecAckFromServer_failed"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = r0.toString()     // Catch:{ Throwable -> 0x0202 }
            java.util.concurrent.atomic.AtomicInteger r0 = com.tencent.android.tpush.a.a.l     // Catch:{ Throwable -> 0x0202 }
            int r0 = r0.getAndIncrement()     // Catch:{ Throwable -> 0x0202 }
            r3 = r0
            r0 = r2
            goto L_0x0055
        L_0x01ae:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0202 }
            r2.<init>()     // Catch:{ Throwable -> 0x0202 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = "_11unequal"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = r0.toString()     // Catch:{ Throwable -> 0x0202 }
            java.util.concurrent.atomic.AtomicInteger r0 = com.tencent.android.tpush.a.a.m     // Catch:{ Throwable -> 0x0202 }
            int r0 = r0.getAndIncrement()     // Catch:{ Throwable -> 0x0202 }
            r3 = r0
            r0 = r2
            goto L_0x0055
        L_0x01cb:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0202 }
            r2.<init>()     // Catch:{ Throwable -> 0x0202 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = "_12notList"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x0202 }
            java.lang.String r2 = r0.toString()     // Catch:{ Throwable -> 0x0202 }
            java.util.concurrent.atomic.AtomicInteger r0 = com.tencent.android.tpush.a.a.n     // Catch:{ Throwable -> 0x0202 }
            int r0 = r0.getAndIncrement()     // Catch:{ Throwable -> 0x0202 }
            r3 = r0
            r0 = r2
            goto L_0x0055
        L_0x01e8:
            r2 = r1
        L_0x01e9:
            if (r2 == 0) goto L_0x0007
            r2.close()     // Catch:{ IOException -> 0x01f0 }
            goto L_0x0007
        L_0x01f0:
            r0 = move-exception
            goto L_0x0007
        L_0x01f3:
            r0 = move-exception
        L_0x01f4:
            if (r1 == 0) goto L_0x01f9
            r1.close()     // Catch:{ IOException -> 0x01fd }
        L_0x01f9:
            throw r0     // Catch:{ all -> 0x01fa }
        L_0x01fa:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x01fd:
            r1 = move-exception
            goto L_0x01f9
        L_0x01ff:
            r0 = move-exception
            r1 = r2
            goto L_0x01f4
        L_0x0202:
            r0 = move-exception
            goto L_0x00b5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.a.a.c(int, java.util.List):void");
    }
}
