package com.tencent.android.tpush.stat.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/* compiled from: ProGuard */
class e extends g {
    public e(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public String b() {
        String string;
        synchronized (this) {
            string = PreferenceManager.getDefaultSharedPreferences(this.a).getString(f(), null);
        }
        return string;
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        synchronized (this) {
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.a).edit();
            edit.putString(f(), str);
            edit.commit();
        }
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return true;
    }
}
