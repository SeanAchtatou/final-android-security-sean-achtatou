package com.tencent.android.tpush.service;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import com.jg.EType;
import com.jg.JgClassChecked;
import com.tencent.android.tpush.XGPushConfig;
import com.tencent.android.tpush.common.p;
import com.tencent.android.tpush.service.c.a;
import com.tencent.android.tpush.service.d.e;

@JgClassChecked(author = 1, fComment = "确认已进行安全校验", lastDate = "20150316", reviewer = 3, vComment = {EType.SERVICESCHECK})
/* compiled from: ProGuard */
public class XGPushService extends Service {
    private static Service a = null;

    public IBinder onBind(Intent intent) {
        return null;
    }

    private void b() {
        if (e.b(getApplicationContext(), "com.tencent.android.tpush.debug," + getApplicationContext().getPackageName(), 0) == 1) {
        }
    }

    public void onCreate() {
        super.onCreate();
        a = this;
        if (Build.VERSION.SDK_INT < 18) {
            startForeground(-1998, new Notification());
        }
        Context applicationContext = getApplicationContext();
        a.a(applicationContext);
        m.c(applicationContext);
        b();
        if (XGPushConfig.enableDebug) {
            com.tencent.android.tpush.a.a.a("XGPushService", "onCreate() : " + getPackageName());
        }
        m.a().b();
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
    }

    public static Service a() {
        return a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, long, boolean):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean */
    public int onStartCommand(Intent intent, int i, int i2) {
        super.onStartCommand(intent, 1, i2);
        if (p.a(getApplicationContext()) > 0) {
            e.p(getApplicationContext());
            return 2;
        }
        b();
        e.a(m.e(), "tpush.wifi.bandon", "", true);
        m.a().a(intent);
        return 1;
    }

    public void onDestroy() {
        m.a().c();
        super.onDestroy();
    }
}
