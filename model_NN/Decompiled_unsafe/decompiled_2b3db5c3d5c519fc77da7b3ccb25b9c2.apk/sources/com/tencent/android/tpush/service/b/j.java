package com.tencent.android.tpush.service.b;

import android.content.Context;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.encrypt.Rijndael;
import com.tencent.android.tpush.encrypt.a;
import com.tencent.android.tpush.service.d.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class j {
    private static j a = new j();
    private static JSONObject b = new JSONObject();
    private static String c = a.a("com.tencent.tpush.multpkgs");

    private j() {
    }

    public static j a() {
        return a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, boolean):java.lang.String
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.service.d.e.a(int, java.lang.String, int):android.content.Intent
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, float):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, int):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, long):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, boolean):java.lang.String */
    private JSONObject b(Context context) {
        String a2 = e.a(context, c, true);
        JSONObject jSONObject = new JSONObject();
        if (!(a2 == null || a2.length() == 0)) {
            try {
                return new JSONObject(Rijndael.decrypt(a2));
            } catch (JSONException e) {
                com.tencent.android.tpush.a.a.c(Constants.ServiceLogTag, "loadRegisterPkgs", e);
            }
        }
        return jSONObject;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, long, boolean):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean */
    private void c(Context context) {
        if (b != null && b.length() != 0) {
            e.a(context, c, Rijndael.encrypt(b.toString()), true);
        }
    }

    public List a(Context context, long j) {
        ArrayList arrayList = new ArrayList();
        if (context != null) {
            try {
                b = b(context);
                Collections.addAll(arrayList, b.getString(j + "").split(","));
            } catch (Exception e) {
                com.tencent.android.tpush.a.a.b(Constants.ServiceLogTag, "getPkgs", e);
            }
        }
        return arrayList;
    }

    public void a(Context context, long j, String str) {
        List list;
        if (context != null && !e.a(str)) {
            try {
                List<String> a2 = a(context, j);
                if (a2 == null) {
                    list = new ArrayList();
                } else {
                    for (String equals : a2) {
                        if (str.equals(equals)) {
                            return;
                        }
                    }
                    list = a2;
                }
                list.add(str);
                String replace = list.toString().replace(" ", "").replace("\t", "");
                b.put(j + "", replace.substring(1, replace.length() - 1));
                c(context);
            } catch (Exception e) {
                com.tencent.android.tpush.a.a.c(Constants.ServiceLogTag, "putPkg", e);
            }
        }
    }

    public void a(Context context, String str) {
        if (context != null && str != null && str.length() != 0) {
            try {
                Iterator<String> keys = b.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    List a2 = a(context, Long.valueOf(next).longValue());
                    ArrayList arrayList = new ArrayList();
                    for (int i = 0; i < a2.size(); i++) {
                        if (str.equals(a2.get(i))) {
                            arrayList.add(a2.get(i));
                        }
                    }
                    a2.removeAll(arrayList);
                    String replace = a2.toString().replace(" ", "").replace("\t", "");
                    b.put(next, replace.substring(1, replace.length() - 1));
                    c(context);
                }
            } catch (Exception e) {
                com.tencent.android.tpush.a.a.c(Constants.LogTag, "delPkg", e);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, java.lang.String, int]
     candidates:
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, long, boolean):boolean
      com.tencent.android.tpush.service.d.e.a(android.content.Context, java.lang.String, java.lang.String, boolean):boolean */
    public void a(Context context) {
        e.a(context, c, "", true);
    }
}
