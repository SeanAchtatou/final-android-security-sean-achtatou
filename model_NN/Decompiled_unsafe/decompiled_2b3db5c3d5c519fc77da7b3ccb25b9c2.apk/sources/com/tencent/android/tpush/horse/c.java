package com.tencent.android.tpush.horse;

import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.horse.data.StrategyItem;
import com.tencent.android.tpush.service.channel.protocol.TpnsRedirectReq;
import com.tencent.android.tpush.service.d.e;
import com.tencent.android.tpush.service.m;

/* compiled from: ProGuard */
class c extends Thread {
    protected o a = new d(this);
    final /* synthetic */ a b;
    private n c;
    /* access modifiers changed from: private */
    public int d;
    private StrategyItem e;

    public c(a aVar, int i) {
        this.b = aVar;
        this.d = i;
    }

    public n a() {
        return this.c;
    }

    public void run() {
        while (this.b.b.size() > 0) {
            try {
                this.e = (StrategyItem) this.b.b.remove();
                TpnsRedirectReq tpnsRedirectReq = new TpnsRedirectReq();
                tpnsRedirectReq.network = e.e(m.e());
                tpnsRedirectReq.op = e.f(m.e());
                this.c = new n();
                this.c.a(this.a);
                try {
                    a.c("HorseThread", " HorseThread:" + getClass().getSimpleName() + Thread.currentThread() + "current NetworkType:" + ((int) tpnsRedirectReq.network) + ",strategyItem:" + this.e);
                    this.c.a(this.e);
                    this.c.a(tpnsRedirectReq);
                    this.c.b();
                } catch (Throwable th) {
                    a.c("HorseThread", "HorseThread error", th);
                }
            } catch (Exception e2) {
                a.c("HorseThread", "Can not get strateItem from strategyItems>>", e2);
                try {
                    Thread.sleep(5000);
                } catch (Exception e3) {
                    a.h(Constants.HorseLogTag, e3.toString());
                }
            }
        }
    }
}
