package com.tencent.android.tpush;

import android.content.DialogInterface;
import android.content.Intent;

/* compiled from: ProGuard */
class f implements DialogInterface.OnClickListener {
    final /* synthetic */ Intent a;
    final /* synthetic */ XGPushActivity b;

    f(XGPushActivity xGPushActivity, Intent intent) {
        this.b = xGPushActivity;
        this.a = intent;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.a.putExtra("action", 4);
        this.b.broadcastToTPushService(this.a);
        dialogInterface.dismiss();
        this.b.finish();
    }
}
