package com.tencent.android.tpush;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import com.facebook.internal.ServerProtocol;
import com.jg.EType;
import com.jg.JgClassChecked;
import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.common.MessageKey;
import com.tencent.android.tpush.common.p;
import com.tencent.android.tpush.encrypt.Rijndael;
import java.util.Collections;
import java.util.List;

@JgClassChecked(author = 1, fComment = "确认已进行安全校验", lastDate = "20150316", reviewer = 3, vComment = {EType.ACTIVITYCHECK, EType.INTENTCHECK, EType.INTENTSCHEMECHECK})
/* compiled from: ProGuard */
public class XGPushActivity extends Activity {
    private Context mContext;

    private boolean checkIntent(Intent intent) {
        if (intent == null || !intent.hasExtra(MessageKey.MSG_PORTECT_TAG)) {
            return false;
        }
        String stringExtra = intent.getStringExtra(MessageKey.MSG_PORTECT_TAG);
        if (p.b(stringExtra)) {
            return false;
        }
        try {
            Long valueOf = Long.valueOf(Rijndael.decrypt(stringExtra));
            if (valueOf.longValue() <= 0 || System.currentTimeMillis() < valueOf.longValue()) {
                return false;
            }
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mContext = this;
        try {
            Intent intent = getIntent();
            if (XGPushConfig.enableDebug) {
                a.d(Constants.LogTag, "XGPushActivity receiver intent:" + intent);
            }
            if (checkIntent(intent)) {
                int intExtra = intent.getIntExtra("action_type", 1);
                if (intExtra == 1) {
                    pushClickedResult(intent);
                } else if (intExtra == 4) {
                    pushClickedPackageResult(intent);
                } else if (intExtra == 2) {
                    showAlertDialog(0, intent);
                } else if (intExtra == 3) {
                    showAlertDialog(1, intent);
                } else {
                    finish();
                }
            } else {
                finish();
            }
        } catch (Throwable th) {
        }
    }

    private void pushClickedResult(Intent intent) {
        String stringExtra = intent.getStringExtra(Constants.FLAG_ACTIVITY_NAME) != null ? intent.getStringExtra(Constants.FLAG_ACTIVITY_NAME) : "";
        if (XGPushConfig.enableDebug) {
            a.d(Constants.PushMessageLogTag, "activity intent =" + intent + "activity = " + stringExtra + "intent.getFlags()" + intent.getFlags());
        }
        Intent intent2 = new Intent();
        intent2.addFlags(intent.getFlags());
        intent2.setClassName(getApplicationContext(), stringExtra);
        intent.putExtra(Constants.TAG_TPUSH_MESSAGE, ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
        intent2.putExtras(intent);
        intent2.putExtra(Constants.TAG_TPUSH_NOTIFICATION, XGPushManager.a((Activity) this));
        try {
            startActivity(intent2);
        } catch (ActivityNotFoundException e) {
        }
        finish();
    }

    private void pushClickedPackageResult(Intent intent) {
        broadcastToTPushService(intent);
        ResolveInfo appMainActivity = getAppMainActivity(intent.getStringExtra(Constants.FLAG_PACKAGE_NAME));
        if (appMainActivity != null) {
            String str = appMainActivity.activityInfo.name;
            String str2 = appMainActivity.activityInfo.packageName;
            Intent intent2 = new Intent();
            intent2.putExtras(intent);
            intent2.setComponent(new ComponentName(str2, str));
            creatDialog(0, intent2);
            return;
        }
        creatDialog(1, intent);
    }

    private ResolveInfo getAppMainActivity(String str) {
        try {
            PackageManager packageManager = getPackageManager();
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
            Collections.sort(queryIntentActivities, new ResolveInfo.DisplayNameComparator(packageManager));
            for (ResolveInfo next : queryIntentActivities) {
                String str2 = next.activityInfo.name;
                String str3 = next.activityInfo.packageName;
                System.out.println(" activityName---" + str2 + " pkgName---" + str3);
                if (str3.equals(str)) {
                    return next;
                }
            }
        } catch (Throwable th) {
            a.c(Constants.LogTag, "查找主Activity出错", th);
        }
        return null;
    }

    private void creatDialog(int i, Intent intent) {
        if (i == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setOnCancelListener(new i(this, intent)).setTitle("提示").setMessage("是否确定打开此应用？").setPositiveButton("打开", new h(this, intent)).setNegativeButton("取消", new f(this, intent));
            builder.create().show();
        } else if (i == 1) {
            AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
            builder2.setOnCancelListener(new l(this, intent)).setTitle("提示").setMessage("本地未发现此应用，建议去下载！").setPositiveButton("下载", new k(this, intent)).setNegativeButton("取消", new j(this, intent));
            builder2.create().show();
        }
    }

    private void showAlertDialog(int i, Intent intent) {
        if (i == 0) {
            String stringExtra = intent.getStringExtra(Constants.FLAG_ACTIVITY_NAME);
            if (intent.getIntExtra(Constants.FLAG_ACTION_CONFIRM, 0) == 1) {
                new AlertDialog.Builder(this).setTitle("提示").setCancelable(false).setMessage("是否打开网站:" + stringExtra + "?").setPositiveButton("确认", new n(this, stringExtra, intent)).setNegativeButton("取消", new m(this, intent)).show();
            } else {
                openUrl(stringExtra, intent);
            }
        } else if (i != 1) {
        } else {
            if (intent.getIntExtra(Constants.FLAG_ACTION_CONFIRM, 0) == 1) {
                new AlertDialog.Builder(this).setTitle("提示").setCancelable(false).setMessage("继续打开Intent?").setPositiveButton("确认", new g(this, intent)).setNegativeButton("取消", new o(this, intent)).show();
            } else {
                openIntent(intent);
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void openIntent(android.content.Intent r6) {
        /*
            r5 = this;
            java.lang.String r0 = "activity"
            java.lang.String r0 = r6.getStringExtra(r0)     // Catch:{ Throwable -> 0x004e }
            r1 = 1
            android.content.Intent r1 = android.content.Intent.parseUri(r0, r1)     // Catch:{ Throwable -> 0x004e }
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r0)     // Catch:{ Throwable -> 0x004e }
            java.lang.String r0 = "android.intent.category.BROWSABLE"
            r1.addCategory(r0)     // Catch:{ Throwable -> 0x004e }
            r0 = 0
            r1.setComponent(r0)     // Catch:{ Throwable -> 0x004e }
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ Throwable -> 0x004e }
            r2 = 15
            if (r0 < r2) goto L_0x003b
            r0 = 1
            java.lang.Class[] r0 = new java.lang.Class[r0]     // Catch:{ Exception -> 0x0045 }
            r2 = 0
            java.lang.Class<android.content.Intent> r3 = android.content.Intent.class
            r0[r2] = r3     // Catch:{ Exception -> 0x0045 }
            java.lang.Class r2 = r1.getClass()     // Catch:{ Exception -> 0x0045 }
            java.lang.String r3 = "setSelector"
            java.lang.reflect.Method r0 = r2.getMethod(r3, r0)     // Catch:{ Exception -> 0x0045 }
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0045 }
            r3 = 0
            r4 = 0
            r2[r3] = r4     // Catch:{ Exception -> 0x0045 }
            r0.invoke(r1, r2)     // Catch:{ Exception -> 0x0045 }
        L_0x003b:
            r5.broadcastToTPushService(r6)     // Catch:{ Throwable -> 0x004e }
            r5.startActivity(r1)     // Catch:{ Throwable -> 0x004e }
            r5.finish()     // Catch:{ Throwable -> 0x004e }
        L_0x0044:
            return
        L_0x0045:
            r0 = move-exception
            java.lang.String r2 = "TPush"
            java.lang.String r3 = "invoke intent.setComponent error."
            com.tencent.android.tpush.a.a.b(r2, r3, r0)     // Catch:{ Throwable -> 0x004e }
            goto L_0x003b
        L_0x004e:
            r0 = move-exception
            java.lang.String r1 = "TPush"
            java.lang.String r2 = "openIntent error."
            com.tencent.android.tpush.a.a.c(r1, r2, r0)
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.XGPushActivity.openIntent(android.content.Intent):void");
    }

    /* access modifiers changed from: private */
    public void openUrl(String str, Intent intent) {
        try {
            Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent2.setFlags(268435456);
            broadcastToTPushService(intent);
            startActivity(intent2);
        } catch (Throwable th) {
            a.c(Constants.LogTag, "openUrl error.", th);
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void broadcastToTPushService(Intent intent) {
        Intent intent2 = new Intent("com.tencent.android.tpush.action.PUSH_CLICK.RESULT");
        intent2.putExtras(intent);
        intent2.putExtra(Constants.FLAG_PACK_NAME, this.mContext.getPackageName());
        intent2.putExtra(Constants.FLAG_CLICK_TIME, System.currentTimeMillis() / 1000);
        sendBroadcast(intent2);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
