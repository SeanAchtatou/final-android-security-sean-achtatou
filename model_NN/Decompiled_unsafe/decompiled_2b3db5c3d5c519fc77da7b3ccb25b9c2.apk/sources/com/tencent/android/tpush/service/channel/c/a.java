package com.tencent.android.tpush.service.channel.c;

import java.io.InputStream;
import java.io.OutputStream;

/* compiled from: ProGuard */
public class a {
    protected byte[] a;
    protected volatile int b;
    protected volatile int c;
    protected volatile int d;
    protected volatile int e;
    protected volatile boolean f;
    protected boolean g;
    protected InputStream h;
    protected boolean i;
    protected OutputStream j;
    protected boolean k;

    public OutputStream a() {
        return this.j;
    }

    public InputStream b() {
        return this.h;
    }

    public int c() {
        int g2;
        synchronized (this) {
            g2 = g();
        }
        return g2;
    }

    public int d() {
        int f2;
        synchronized (this) {
            f2 = f();
        }
        return f2;
    }

    /* access modifiers changed from: private */
    public void e() {
        byte[] bArr = new byte[(this.a.length * 2)];
        int h2 = h();
        int g2 = g();
        if (this.d <= this.c) {
            System.arraycopy(this.a, this.d, bArr, 0, this.c - this.d);
        } else {
            int length = this.a.length - this.d;
            System.arraycopy(this.a, this.d, bArr, 0, length);
            System.arraycopy(this.a, 0, bArr, length, this.c);
        }
        this.a = bArr;
        this.d = 0;
        this.b = h2;
        this.c = h2 + g2;
    }

    /* access modifiers changed from: private */
    public int f() {
        if (this.c < this.d) {
            return (this.d - this.c) - 1;
        }
        return (this.a.length - 1) - (this.c - this.d);
    }

    /* access modifiers changed from: private */
    public int g() {
        if (this.b <= this.c) {
            return this.c - this.b;
        }
        return this.a.length - (this.b - this.c);
    }

    private int h() {
        if (this.d <= this.b) {
            return this.b - this.d;
        }
        return this.a.length - (this.d - this.b);
    }

    /* access modifiers changed from: private */
    public void i() {
        if (h() >= this.e) {
            this.d = this.b;
            this.e = 0;
        }
    }

    public a() {
        this(4096, true);
    }

    public a(int i2, boolean z) {
        this.b = 0;
        this.c = 0;
        this.d = 0;
        this.e = 0;
        this.f = false;
        this.g = true;
        this.h = new b(this);
        this.i = false;
        this.j = new c(this);
        this.k = false;
        if (i2 == -1) {
            this.a = new byte[4096];
            this.f = true;
        } else {
            this.a = new byte[i2];
            this.f = false;
        }
        this.g = z;
    }
}
