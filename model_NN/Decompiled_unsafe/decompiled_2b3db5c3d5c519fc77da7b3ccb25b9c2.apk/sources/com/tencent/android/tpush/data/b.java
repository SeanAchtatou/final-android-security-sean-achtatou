package com.tencent.android.tpush.data;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
final class b implements Parcelable.Creator {
    b() {
    }

    /* renamed from: a */
    public StorageEntity createFromParcel(Parcel parcel) {
        return new StorageEntity(parcel);
    }

    /* renamed from: a */
    public StorageEntity[] newArray(int i) {
        return new StorageEntity[i];
    }
}
