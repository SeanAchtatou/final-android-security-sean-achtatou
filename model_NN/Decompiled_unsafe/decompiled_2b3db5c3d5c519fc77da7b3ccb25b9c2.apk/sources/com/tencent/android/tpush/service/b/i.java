package com.tencent.android.tpush.service.b;

import android.content.Context;
import com.tencent.android.tpush.a.a;
import com.tencent.android.tpush.data.MessageId;

/* compiled from: ProGuard */
class i implements Runnable {
    int a;
    Context b;
    final /* synthetic */ a c;

    public i(a aVar, Context context, int i) {
        this.c = aVar;
        this.b = context;
        this.a = i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.service.b.a.a(com.tencent.android.tpush.service.b.a, android.content.Context, java.lang.Long):void
     arg types: [com.tencent.android.tpush.service.b.a, android.content.Context, long]
     candidates:
      com.tencent.android.tpush.service.b.a.a(android.content.Intent, java.lang.String, com.tencent.android.tpush.data.MessageId):void
      com.tencent.android.tpush.service.b.a.a(com.tencent.android.tpush.service.b.a, android.content.Context, com.tencent.android.tpush.data.MessageId):void
      com.tencent.android.tpush.service.b.a.a(com.tencent.android.tpush.service.channel.protocol.TpnsPushMsg, long, com.tencent.android.tpush.service.channel.a):void
      com.tencent.android.tpush.service.b.a.a(android.content.Context, java.lang.String, long):void
      com.tencent.android.tpush.service.b.a.a(android.content.Context, java.lang.String, android.content.Intent):void
      com.tencent.android.tpush.service.b.a.a(android.content.Context, java.lang.String, com.tencent.android.tpush.data.MessageId):void
      com.tencent.android.tpush.service.b.a.a(android.content.Context, java.lang.String, com.tencent.android.tpush.data.PushClickEntity):void
      com.tencent.android.tpush.service.b.a.a(android.content.Context, java.lang.String, java.lang.String):void
      com.tencent.android.tpush.service.b.a.a(android.content.Context, java.lang.String, java.util.ArrayList):void
      com.tencent.android.tpush.service.b.a.a(android.content.Context, java.util.List, java.util.ArrayList):void
      com.tencent.android.tpush.service.b.a.a(java.lang.String, android.content.Intent, java.lang.String):void
      com.tencent.android.tpush.service.b.a.a(java.util.ArrayList, long, com.tencent.android.tpush.service.channel.a):void
      com.tencent.android.tpush.service.b.a.a(com.tencent.android.tpush.service.b.a, android.content.Context, java.lang.Long):void */
    public void run() {
        switch (this.a) {
            case 1:
                this.c.b(this.b, (MessageId) null);
                return;
            case 2:
                this.c.a(this.b, (Long) -1L);
                return;
            default:
                a.h("MessageManager", "unknown report type");
                return;
        }
    }
}
