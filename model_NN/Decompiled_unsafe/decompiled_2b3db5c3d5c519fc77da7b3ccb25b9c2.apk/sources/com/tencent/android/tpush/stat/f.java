package com.tencent.android.tpush.stat;

import android.content.Context;
import com.tencent.android.tpush.common.Constants;
import com.tencent.android.tpush.stat.a.e;
import com.tencent.android.tpush.stat.event.d;
import java.util.Arrays;
import java.util.List;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class f {
    private static com.tencent.android.tpush.stat.a.f c = e.b();
    private static volatile f d = null;
    private static Context e = null;
    DefaultHttpClient a = null;
    StringBuilder b = new StringBuilder(4096);
    private long f = 0;

    private f(Context context) {
        try {
            e = context.getApplicationContext();
            this.f = System.currentTimeMillis() / 1000;
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setStaleCheckingEnabled(basicHttpParams, false);
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, Constants.ERRORCODE_UNKNOWN);
            HttpConnectionParams.setSoTimeout(basicHttpParams, Constants.ERRORCODE_UNKNOWN);
            this.a = new DefaultHttpClient(basicHttpParams);
            this.a.setKeepAliveStrategy(new g(this));
        } catch (Throwable th) {
            c.b(th);
        }
    }

    static void a(Context context) {
        e = context.getApplicationContext();
    }

    static Context a() {
        return e;
    }

    public static f b(Context context) {
        if (d == null) {
            synchronized (f.class) {
                if (d == null) {
                    d = new f(context);
                }
            }
        }
        return d;
    }

    private void a(JSONObject jSONObject) {
        try {
            if (!jSONObject.isNull("cfg")) {
                c.a(e, jSONObject.getJSONObject("cfg"));
            }
            if (!jSONObject.isNull("ncts")) {
                int i = jSONObject.getInt("ncts");
                int currentTimeMillis = (int) (((long) i) - (System.currentTimeMillis() / 1000));
                if (c.b()) {
                    c.b("server time:" + i + ", diff time:" + currentTimeMillis);
                }
                e.k(e);
                e.a(e, currentTimeMillis);
            }
        } catch (Throwable th) {
            c.d(th);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x01cc, code lost:
        r0 = th;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.util.List r11, com.tencent.android.tpush.stat.e r12) {
        /*
            r10 = this;
            if (r11 == 0) goto L_0x0008
            boolean r0 = r11.isEmpty()
            if (r0 == 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            int r2 = r11.size()
            r0 = 0
            java.lang.StringBuilder r1 = r10.b     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r3 = 0
            java.lang.StringBuilder r4 = r10.b     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            int r4 = r4.length()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r1.delete(r3, r4)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r1 = r10.b     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r3 = "["
            r1.append(r3)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r3 = "rc4"
            r1 = 0
        L_0x0024:
            if (r1 >= r2) goto L_0x0041
            java.lang.StringBuilder r4 = r10.b     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.Object r5 = r11.get(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r4.append(r5)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            int r4 = r2 + -1
            if (r1 == r4) goto L_0x003e
            java.lang.StringBuilder r4 = r10.b     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r5 = ","
            r4.append(r5)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
        L_0x003e:
            int r1 = r1 + 1
            goto L_0x0024
        L_0x0041:
            java.lang.StringBuilder r1 = r10.b     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r4 = "]"
            r1.append(r4)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r1 = r10.b     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            int r4 = r1.length()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r5.<init>()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r6 = com.tencent.android.tpush.stat.c.d()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r6 = "/?index="
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            long r6 = r10.f     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            long r6 = r10.f     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r8 = 1
            long r6 = r6 + r8
            r10.f = r6     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            boolean r6 = com.tencent.android.tpush.stat.c.b()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r6 == 0) goto L_0x00b2
            com.tencent.android.tpush.stat.a.f r6 = com.tencent.android.tpush.stat.f.c     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r7.<init>()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r8 = "["
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r7 = r7.append(r5)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r8 = "]Send request(eventsize:"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r2 = r7.append(r2)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r7 = ","
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r7 = "bytes), content:"
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r6.b(r2)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
        L_0x00b2:
            org.apache.http.client.methods.HttpPost r6 = new org.apache.http.client.methods.HttpPost     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r6.<init>(r5)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r2 = "Accept-Encoding"
            java.lang.String r5 = "gzip"
            r6.addHeader(r2, r5)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r2 = "Connection"
            java.lang.String r5 = "Keep-Alive"
            r6.setHeader(r2, r5)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r2 = "Cache-Control"
            r6.removeHeaders(r2)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            android.content.Context r2 = com.tencent.android.tpush.stat.f.e     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            org.apache.http.HttpHost r5 = com.tencent.android.tpush.stat.a.e.a(r2)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r2 = "Content-Encoding"
            r6.addHeader(r2, r3)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r5 != 0) goto L_0x01ee
            org.apache.http.impl.client.DefaultHttpClient r2 = r10.a     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            org.apache.http.params.HttpParams r2 = r2.getParams()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r7 = "http.route.default-proxy"
            r2.removeParameter(r7)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
        L_0x00e2:
            java.io.ByteArrayOutputStream r7 = new java.io.ByteArrayOutputStream     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r7.<init>(r4)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r2 = "UTF-8"
            byte[] r1 = r1.getBytes(r2)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            int r8 = r1.length     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r2 = 512(0x200, float:7.175E-43)
            if (r4 <= r2) goto L_0x0239
            r2 = 1
        L_0x00f3:
            if (r2 == 0) goto L_0x016b
            java.lang.String r2 = "Content-Encoding"
            r6.removeHeaders(r2)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r2.<init>()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r3 = ",gzip"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r3 = "Content-Encoding"
            r6.addHeader(r3, r2)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r5 == 0) goto L_0x011e
            java.lang.String r3 = "X-Content-Encoding"
            r6.removeHeaders(r3)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r3 = "X-Content-Encoding"
            r6.addHeader(r3, r2)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
        L_0x011e:
            r2 = 4
            byte[] r2 = new byte[r2]     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r7.write(r2)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.util.zip.GZIPOutputStream r2 = new java.util.zip.GZIPOutputStream     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r2.<init>(r7)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r2.write(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r2.close()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            byte[] r1 = r7.toByteArray()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r2 = 0
            r3 = 4
            java.nio.ByteBuffer r2 = java.nio.ByteBuffer.wrap(r1, r2, r3)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r2.putInt(r8)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            boolean r2 = com.tencent.android.tpush.stat.c.b()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r2 == 0) goto L_0x016b
            com.tencent.android.tpush.stat.a.f r2 = com.tencent.android.tpush.stat.f.c     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r3.<init>()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r4 = "before Gzip:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r3 = r3.append(r8)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r4 = " bytes, after Gzip:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            int r4 = r1.length     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r4 = " bytes"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r2.h(r3)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
        L_0x016b:
            byte[] r1 = com.tencent.android.tpush.stat.a.d.a(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            org.apache.http.entity.ByteArrayEntity r2 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r6.setEntity(r2)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            org.apache.http.impl.client.DefaultHttpClient r1 = r10.a     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            org.apache.http.HttpResponse r2 = r1.execute(r6)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            org.apache.http.HttpEntity r1 = r2.getEntity()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            org.apache.http.StatusLine r3 = r2.getStatusLine()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            int r3 = r3.getStatusCode()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            long r4 = r1.getContentLength()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            boolean r6 = com.tencent.android.tpush.stat.c.b()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r6 == 0) goto L_0x01b5
            com.tencent.android.tpush.stat.a.f r6 = com.tencent.android.tpush.stat.f.c     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r8.<init>()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r9 = "http recv response status code:"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r8 = r8.append(r3)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r9 = ", content length:"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r8 = r8.append(r4)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r8 = r8.toString()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r6.b(r8)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
        L_0x01b5:
            r8 = 0
            int r6 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r6 > 0) goto L_0x023c
            com.tencent.android.tpush.stat.a.f r0 = com.tencent.android.tpush.stat.f.c     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r2 = "Server response no data."
            r0.f(r2)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r12 == 0) goto L_0x01c7
            r12.b()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
        L_0x01c7:
            org.apache.http.util.EntityUtils.toString(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            goto L_0x0008
        L_0x01cc:
            r0 = move-exception
        L_0x01cd:
            if (r0 == 0) goto L_0x0008
            com.tencent.android.tpush.stat.a.f r1 = com.tencent.android.tpush.stat.f.c
            r1.a(r0)
            if (r12 == 0) goto L_0x01d9
            r12.b()     // Catch:{ Throwable -> 0x0332 }
        L_0x01d9:
            boolean r1 = r0 instanceof java.lang.OutOfMemoryError
            if (r1 == 0) goto L_0x033a
            java.lang.System.gc()
            r0 = 0
            r10.b = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r1 = 2048(0x800, float:2.87E-42)
            r0.<init>(r1)
            r10.b = r0
            goto L_0x0008
        L_0x01ee:
            boolean r2 = com.tencent.android.tpush.stat.c.b()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r2 == 0) goto L_0x0210
            com.tencent.android.tpush.stat.a.f r2 = com.tencent.android.tpush.stat.f.c     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r7.<init>()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r8 = "proxy:"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r8 = r5.toHostString()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r2.h(r7)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
        L_0x0210:
            java.lang.String r2 = "X-Content-Encoding"
            r6.addHeader(r2, r3)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            org.apache.http.impl.client.DefaultHttpClient r2 = r10.a     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            org.apache.http.params.HttpParams r2 = r2.getParams()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r7 = "http.route.default-proxy"
            r2.setParameter(r7, r5)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r2 = "X-Online-Host"
            java.lang.String r7 = com.tencent.android.tpush.stat.c.d     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r6.addHeader(r2, r7)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r2 = "Accept"
            java.lang.String r7 = "*/*"
            r6.addHeader(r2, r7)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r2 = "Content-Type"
            java.lang.String r7 = "json"
            r6.addHeader(r2, r7)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            goto L_0x00e2
        L_0x0237:
            r0 = move-exception
            throw r0
        L_0x0239:
            r2 = 0
            goto L_0x00f3
        L_0x023c:
            r8 = 0
            int r4 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r4 <= 0) goto L_0x032e
            java.io.InputStream r4 = r1.getContent()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.io.DataInputStream r5 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r5.<init>(r4)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            long r8 = r1.getContentLength()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            int r1 = (int) r8     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            byte[] r1 = new byte[r1]     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r5.readFully(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r4.close()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r5.close()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r5 = "Content-Encoding"
            org.apache.http.Header r2 = r2.getFirstHeader(r5)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r2 == 0) goto L_0x0277
            java.lang.String r5 = r2.getValue()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r6 = "gzip,rc4"
            boolean r5 = r5.equalsIgnoreCase(r6)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r5 == 0) goto L_0x02bd
            byte[] r1 = com.tencent.android.tpush.stat.a.e.a(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            byte[] r1 = com.tencent.android.tpush.stat.a.d.b(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
        L_0x0277:
            java.lang.String r2 = new java.lang.String     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r5 = "UTF-8"
            r2.<init>(r1, r5)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            boolean r5 = com.tencent.android.tpush.stat.c.b()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r5 == 0) goto L_0x029c
            com.tencent.android.tpush.stat.a.f r5 = com.tencent.android.tpush.stat.f.c     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r6.<init>()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r8 = "http get response data:"
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r6 = r6.append(r2)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r5.b(r6)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
        L_0x029c:
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r5.<init>(r2)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r2 = 200(0xc8, float:2.8E-43)
            if (r3 != r2) goto L_0x02ff
            r10.a(r5)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r12 == 0) goto L_0x02b5
            java.lang.String r1 = "ret"
            int r1 = r5.optInt(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r1 != 0) goto L_0x02f4
            r12.a()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
        L_0x02b5:
            r4.close()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
        L_0x02b8:
            r7.close()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            goto L_0x01cd
        L_0x02bd:
            java.lang.String r5 = r2.getValue()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r6 = "rc4,gzip"
            boolean r5 = r5.equalsIgnoreCase(r6)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r5 == 0) goto L_0x02d2
            byte[] r1 = com.tencent.android.tpush.stat.a.d.b(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            byte[] r1 = com.tencent.android.tpush.stat.a.e.a(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            goto L_0x0277
        L_0x02d2:
            java.lang.String r5 = r2.getValue()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r6 = "gzip"
            boolean r5 = r5.equalsIgnoreCase(r6)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r5 == 0) goto L_0x02e3
            byte[] r1 = com.tencent.android.tpush.stat.a.e.a(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            goto L_0x0277
        L_0x02e3:
            java.lang.String r2 = r2.getValue()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r5 = "rc4"
            boolean r2 = r2.equalsIgnoreCase(r5)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r2 == 0) goto L_0x0277
            byte[] r1 = com.tencent.android.tpush.stat.a.d.b(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            goto L_0x0277
        L_0x02f4:
            com.tencent.android.tpush.stat.a.f r1 = com.tencent.android.tpush.stat.f.c     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r2 = "response error data."
            r1.e(r2)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r12.b()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            goto L_0x02b5
        L_0x02ff:
            com.tencent.android.tpush.stat.a.f r2 = com.tencent.android.tpush.stat.f.c     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r5.<init>()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r6 = "Server response error code:"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r5 = ", error:"
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r5 = new java.lang.String     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r6 = "UTF-8"
            r5.<init>(r1, r6)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.StringBuilder r1 = r3.append(r5)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            r2.e(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            if (r12 == 0) goto L_0x02b5
            r12.b()     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            goto L_0x02b5
        L_0x032e:
            org.apache.http.util.EntityUtils.toString(r1)     // Catch:{ Throwable -> 0x01cc, all -> 0x0237 }
            goto L_0x02b8
        L_0x0332:
            r1 = move-exception
            com.tencent.android.tpush.stat.a.f r2 = com.tencent.android.tpush.stat.f.c
            r2.b(r1)
            goto L_0x01d9
        L_0x033a:
            boolean r1 = r0 instanceof java.net.UnknownHostException
            if (r1 != 0) goto L_0x0008
            boolean r0 = r0 instanceof java.net.SocketTimeoutException
            if (r0 == 0) goto L_0x0008
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.android.tpush.stat.f.a(java.util.List, com.tencent.android.tpush.stat.e):void");
    }

    /* access modifiers changed from: package-private */
    public void b(List list, e eVar) {
        a(list, eVar);
    }

    public void a(d dVar, e eVar) {
        b(Arrays.asList(dVar.c()), eVar);
    }
}
