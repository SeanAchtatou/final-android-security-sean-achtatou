package com.tencent.android.tpush.service.b;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.tencent.android.tpush.rpc.a;
import com.tencent.android.tpush.rpc.b;
import com.tencent.android.tpush.rpc.g;

/* compiled from: ProGuard */
class h implements ServiceConnection {
    final /* synthetic */ g a;

    h(g gVar) {
        this.a = gVar;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            a unused = this.a.c = b.a(iBinder);
            if (this.a.c != null) {
                this.a.c.a(this.a.a.toURI(), this.a.d);
            }
        } catch (Throwable th) {
            com.tencent.android.tpush.a.a.c("MessageManager", "SendBroadcastByRPC", th);
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        ServiceConnection unused = this.a.e = (ServiceConnection) null;
        a unused2 = this.a.c = (a) null;
        g unused3 = this.a.d = (g) null;
    }
}
