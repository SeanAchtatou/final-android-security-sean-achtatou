package com.tencent.android.tpush.stat;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import com.tencent.android.tpush.stat.a.e;
import java.lang.Thread;

/* compiled from: ProGuard */
final class i implements Runnable {
    final /* synthetic */ Context a;

    i(Context context) {
        this.a = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.android.tpush.stat.a.e.a(android.content.Context, boolean):int
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.android.tpush.stat.a.e.a(android.content.Context, long):java.lang.String
      com.tencent.android.tpush.stat.a.e.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.android.tpush.stat.a.e.a(android.content.Context, int):void
      com.tencent.android.tpush.stat.a.e.a(android.content.Context, boolean):int */
    public void run() {
        a.a(h.f).e();
        e.a(this.a, true);
        f.b(this.a);
        Thread.UncaughtExceptionHandler unused = h.e = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(new r());
        if (c.a() == StatReportStrategy.APP_LAUNCH) {
            h.a(this.a, -1);
        }
        if (c.b()) {
            h.d.h("Init MTA StatService success.");
        }
        String h = e.h(h.f);
        if (h == null || h.trim().length() == 0) {
            h = "default";
        }
        String str = h + ".xg.stat.";
        if (Build.VERSION.SDK_INT >= 11) {
            SharedPreferences unused2 = h.h = this.a.getSharedPreferences("." + str, 0);
        } else {
            SharedPreferences unused3 = h.h = this.a.getSharedPreferences("." + str, 0);
        }
    }
}
