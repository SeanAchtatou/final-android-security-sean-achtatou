package android.support.v7.cardview;

public final class R {

    public static final class attr {
        public static final int cardBackgroundColor = 2130772125;
        public static final int cardCornerRadius = 2130772126;
        public static final int cardElevation = 2130772127;
        public static final int cardMaxElevation = 2130772128;
        public static final int cardPreventCornerOverlap = 2130772130;
        public static final int cardUseCompatPadding = 2130772129;
        public static final int contentPadding = 2130772131;
        public static final int contentPaddingBottom = 2130772135;
        public static final int contentPaddingLeft = 2130772132;
        public static final int contentPaddingRight = 2130772133;
        public static final int contentPaddingTop = 2130772134;
    }

    public static final class color {
        public static final int cardview_dark_background = 2131361811;
        public static final int cardview_light_background = 2131361812;
        public static final int cardview_shadow_end_color = 2131361813;
        public static final int cardview_shadow_start_color = 2131361814;
    }

    public static final class dimen {
        public static final int cardview_compat_inset_shadow = 2131165262;
        public static final int cardview_default_elevation = 2131165263;
        public static final int cardview_default_radius = 2131165264;
    }

    public static final class style {
        public static final int Base_CardView = 2131230883;
        public static final int CardView = 2131230870;
        public static final int CardView_Dark = 2131230928;
        public static final int CardView_Light = 2131230929;
    }

    public static final class styleable {
        public static final int[] CardView = {16843071, 16843072, com.seantech.nineke.R.attr.cardBackgroundColor, com.seantech.nineke.R.attr.cardCornerRadius, com.seantech.nineke.R.attr.cardElevation, com.seantech.nineke.R.attr.cardMaxElevation, com.seantech.nineke.R.attr.cardUseCompatPadding, com.seantech.nineke.R.attr.cardPreventCornerOverlap, com.seantech.nineke.R.attr.contentPadding, com.seantech.nineke.R.attr.contentPaddingLeft, com.seantech.nineke.R.attr.contentPaddingRight, com.seantech.nineke.R.attr.contentPaddingTop, com.seantech.nineke.R.attr.contentPaddingBottom};
        public static final int CardView_android_minHeight = 1;
        public static final int CardView_android_minWidth = 0;
        public static final int CardView_cardBackgroundColor = 2;
        public static final int CardView_cardCornerRadius = 3;
        public static final int CardView_cardElevation = 4;
        public static final int CardView_cardMaxElevation = 5;
        public static final int CardView_cardPreventCornerOverlap = 7;
        public static final int CardView_cardUseCompatPadding = 6;
        public static final int CardView_contentPadding = 8;
        public static final int CardView_contentPaddingBottom = 12;
        public static final int CardView_contentPaddingLeft = 9;
        public static final int CardView_contentPaddingRight = 10;
        public static final int CardView_contentPaddingTop = 11;
    }
}
