package org.apache.http.pool;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.http.annotation.ThreadSafe;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.pool.PoolEntry;
import org.apache.http.util.Args;
import org.apache.http.util.Asserts;

@ThreadSafe
public abstract class AbstractConnPool<T, C, E extends PoolEntry<T, C>> implements ConnPool<T, E>, ConnPoolControl<T> {
    private final LinkedList<E> available = new LinkedList<>();
    private final ConnFactory<T, C> connFactory;
    private volatile int defaultMaxPerRoute;
    private volatile boolean isShutDown;
    private final Set<E> leased = new HashSet();
    private final Lock lock = new ReentrantLock();
    private final Map<T, Integer> maxPerRoute = new HashMap();
    private volatile int maxTotal;
    private final LinkedList<PoolEntryFuture<E>> pending = new LinkedList<>();
    private final Map<T, RouteSpecificPool<T, C, E>> routeToPool = new HashMap();

    /* access modifiers changed from: protected */
    public abstract E createEntry(Object obj, Object obj2);

    public AbstractConnPool(ConnFactory<T, C> connFactory2, int defaultMaxPerRoute2, int maxTotal2) {
        this.connFactory = (ConnFactory) Args.notNull(connFactory2, "Connection factory");
        this.defaultMaxPerRoute = Args.notNegative(defaultMaxPerRoute2, "Max per route value");
        this.maxTotal = Args.notNegative(maxTotal2, "Max total value");
    }

    /* access modifiers changed from: protected */
    public void onLease(E e) {
    }

    /* access modifiers changed from: protected */
    public void onRelease(E e) {
    }

    public boolean isShutdown() {
        return this.isShutDown;
    }

    public void shutdown() throws IOException {
        if (!this.isShutDown) {
            this.isShutDown = true;
            this.lock.lock();
            try {
                Iterator i$ = this.available.iterator();
                while (i$.hasNext()) {
                    ((PoolEntry) i$.next()).close();
                }
                for (E entry : this.leased) {
                    entry.close();
                }
                for (RouteSpecificPool<T, C, E> pool : this.routeToPool.values()) {
                    pool.shutdown();
                }
                this.routeToPool.clear();
                this.leased.clear();
                this.available.clear();
            } finally {
                this.lock.unlock();
            }
        }
    }

    private RouteSpecificPool<T, C, E> getPool(final T route) {
        RouteSpecificPool<T, C, E> pool = this.routeToPool.get(route);
        if (pool != null) {
            return pool;
        }
        RouteSpecificPool<T, C, E> pool2 = new RouteSpecificPool<T, C, E>(route) {
            /* access modifiers changed from: protected */
            public E createEntry(C conn) {
                return AbstractConnPool.this.createEntry(route, conn);
            }
        };
        this.routeToPool.put(route, pool2);
        return pool2;
    }

    public Future<E> lease(T route, Object state, FutureCallback<E> callback) {
        Args.notNull(route, "Route");
        Asserts.check(!this.isShutDown, "Connection pool shut down");
        final T t = route;
        final Object obj = state;
        return new PoolEntryFuture<E>(this.lock, callback) {
            public E getPoolEntry(long timeout, TimeUnit tunit) throws InterruptedException, TimeoutException, IOException {
                E entry = AbstractConnPool.this.getPoolEntryBlocking(t, obj, timeout, tunit, this);
                AbstractConnPool.this.onLease(entry);
                return entry;
            }
        };
    }

    public Future<E> lease(T route, Object state) {
        return lease(route, state, null);
    }

    /* access modifiers changed from: private */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: E
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public E getPoolEntryBlocking(T r24, java.lang.Object r25, long r26, java.util.concurrent.TimeUnit r28, org.apache.http.pool.PoolEntryFuture<E> r29) throws java.io.IOException, java.lang.InterruptedException, java.util.concurrent.TimeoutException {
        /*
            r23 = this;
            r5 = 0
            r18 = 0
            int r18 = (r26 > r18 ? 1 : (r26 == r18 ? 0 : -1))
            if (r18 <= 0) goto L_0x001c
            java.util.Date r5 = new java.util.Date
            long r18 = java.lang.System.currentTimeMillis()
            r0 = r28
            r1 = r26
            long r20 = r0.toMillis(r1)
            long r18 = r18 + r20
            r0 = r18
            r5.<init>(r0)
        L_0x001c:
            r0 = r23
            java.util.concurrent.locks.Lock r0 = r0.lock
            r18 = r0
            r18.lock()
            org.apache.http.pool.RouteSpecificPool r14 = r23.getPool(r24)     // Catch:{ all -> 0x0091 }
            r6 = 0
        L_0x002a:
            if (r6 != 0) goto L_0x0190
            r0 = r23
            boolean r0 = r0.isShutDown     // Catch:{ all -> 0x0091 }
            r18 = r0
            if (r18 != 0) goto L_0x0066
            r18 = 1
        L_0x0036:
            java.lang.String r19 = "Connection pool shut down"
            org.apache.http.util.Asserts.check(r18, r19)     // Catch:{ all -> 0x0091 }
        L_0x003b:
            r0 = r25
            org.apache.http.pool.PoolEntry r6 = r14.getFree(r0)     // Catch:{ all -> 0x0091 }
            if (r6 != 0) goto L_0x0069
        L_0x0043:
            if (r6 == 0) goto L_0x009c
            r0 = r23
            java.util.LinkedList<E> r0 = r0.available     // Catch:{ all -> 0x0091 }
            r18 = r0
            r0 = r18
            r0.remove(r6)     // Catch:{ all -> 0x0091 }
            r0 = r23
            java.util.Set<E> r0 = r0.leased     // Catch:{ all -> 0x0091 }
            r18 = r0
            r0 = r18
            r0.add(r6)     // Catch:{ all -> 0x0091 }
            r0 = r23
            java.util.concurrent.locks.Lock r0 = r0.lock
            r18 = r0
            r18.unlock()
            r7 = r6
        L_0x0065:
            return r7
        L_0x0066:
            r18 = 0
            goto L_0x0036
        L_0x0069:
            boolean r18 = r6.isClosed()     // Catch:{ all -> 0x0091 }
            if (r18 != 0) goto L_0x007b
            long r18 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0091 }
            r0 = r18
            boolean r18 = r6.isExpired(r0)     // Catch:{ all -> 0x0091 }
            if (r18 == 0) goto L_0x0043
        L_0x007b:
            r6.close()     // Catch:{ all -> 0x0091 }
            r0 = r23
            java.util.LinkedList<E> r0 = r0.available     // Catch:{ all -> 0x0091 }
            r18 = r0
            r0 = r18
            r0.remove(r6)     // Catch:{ all -> 0x0091 }
            r18 = 0
            r0 = r18
            r14.free(r6, r0)     // Catch:{ all -> 0x0091 }
            goto L_0x003b
        L_0x0091:
            r18 = move-exception
            r0 = r23
            java.util.concurrent.locks.Lock r0 = r0.lock
            r19 = r0
            r19.unlock()
            throw r18
        L_0x009c:
            int r12 = r23.getMax(r24)     // Catch:{ all -> 0x0091 }
            r18 = 0
            int r19 = r14.getAllocatedCount()     // Catch:{ all -> 0x0091 }
            int r19 = r19 + 1
            int r19 = r19 - r12
            int r8 = java.lang.Math.max(r18, r19)     // Catch:{ all -> 0x0091 }
            if (r8 <= 0) goto L_0x00b9
            r10 = 0
        L_0x00b1:
            if (r10 >= r8) goto L_0x00b9
            org.apache.http.pool.PoolEntry r11 = r14.getLastUsed()     // Catch:{ all -> 0x0091 }
            if (r11 != 0) goto L_0x0140
        L_0x00b9:
            int r18 = r14.getAllocatedCount()     // Catch:{ all -> 0x0091 }
            r0 = r18
            if (r0 >= r12) goto L_0x0155
            r0 = r23
            java.util.Set<E> r0 = r0.leased     // Catch:{ all -> 0x0091 }
            r18 = r0
            int r17 = r18.size()     // Catch:{ all -> 0x0091 }
            r0 = r23
            int r0 = r0.maxTotal     // Catch:{ all -> 0x0091 }
            r18 = r0
            int r18 = r18 - r17
            r19 = 0
            int r9 = java.lang.Math.max(r18, r19)     // Catch:{ all -> 0x0091 }
            if (r9 <= 0) goto L_0x0155
            r0 = r23
            java.util.LinkedList<E> r0 = r0.available     // Catch:{ all -> 0x0091 }
            r18 = r0
            int r16 = r18.size()     // Catch:{ all -> 0x0091 }
            int r18 = r9 + -1
            r0 = r16
            r1 = r18
            if (r0 <= r1) goto L_0x0117
            r0 = r23
            java.util.LinkedList<E> r0 = r0.available     // Catch:{ all -> 0x0091 }
            r18 = r0
            boolean r18 = r18.isEmpty()     // Catch:{ all -> 0x0091 }
            if (r18 != 0) goto L_0x0117
            r0 = r23
            java.util.LinkedList<E> r0 = r0.available     // Catch:{ all -> 0x0091 }
            r18 = r0
            java.lang.Object r11 = r18.removeLast()     // Catch:{ all -> 0x0091 }
            org.apache.http.pool.PoolEntry r11 = (org.apache.http.pool.PoolEntry) r11     // Catch:{ all -> 0x0091 }
            r11.close()     // Catch:{ all -> 0x0091 }
            java.lang.Object r18 = r11.getRoute()     // Catch:{ all -> 0x0091 }
            r0 = r23
            r1 = r18
            org.apache.http.pool.RouteSpecificPool r13 = r0.getPool(r1)     // Catch:{ all -> 0x0091 }
            r13.remove(r11)     // Catch:{ all -> 0x0091 }
        L_0x0117:
            r0 = r23
            org.apache.http.pool.ConnFactory<T, C> r0 = r0.connFactory     // Catch:{ all -> 0x0091 }
            r18 = r0
            r0 = r18
            r1 = r24
            java.lang.Object r4 = r0.create(r1)     // Catch:{ all -> 0x0091 }
            org.apache.http.pool.PoolEntry r6 = r14.add(r4)     // Catch:{ all -> 0x0091 }
            r0 = r23
            java.util.Set<E> r0 = r0.leased     // Catch:{ all -> 0x0091 }
            r18 = r0
            r0 = r18
            r0.add(r6)     // Catch:{ all -> 0x0091 }
            r0 = r23
            java.util.concurrent.locks.Lock r0 = r0.lock
            r18 = r0
            r18.unlock()
            r7 = r6
            goto L_0x0065
        L_0x0140:
            r11.close()     // Catch:{ all -> 0x0091 }
            r0 = r23
            java.util.LinkedList<E> r0 = r0.available     // Catch:{ all -> 0x0091 }
            r18 = r0
            r0 = r18
            r0.remove(r11)     // Catch:{ all -> 0x0091 }
            r14.remove(r11)     // Catch:{ all -> 0x0091 }
            int r10 = r10 + 1
            goto L_0x00b1
        L_0x0155:
            r15 = 0
            r0 = r29
            r14.queue(r0)     // Catch:{ all -> 0x0198 }
            r0 = r23
            java.util.LinkedList<org.apache.http.pool.PoolEntryFuture<E>> r0 = r0.pending     // Catch:{ all -> 0x0198 }
            r18 = r0
            r0 = r18
            r1 = r29
            r0.add(r1)     // Catch:{ all -> 0x0198 }
            r0 = r29
            boolean r15 = r0.await(r5)     // Catch:{ all -> 0x0198 }
            r0 = r29
            r14.unqueue(r0)     // Catch:{ all -> 0x0091 }
            r0 = r23
            java.util.LinkedList<org.apache.http.pool.PoolEntryFuture<E>> r0 = r0.pending     // Catch:{ all -> 0x0091 }
            r18 = r0
            r0 = r18
            r1 = r29
            r0.remove(r1)     // Catch:{ all -> 0x0091 }
            if (r15 != 0) goto L_0x002a
            if (r5 == 0) goto L_0x002a
            long r18 = r5.getTime()     // Catch:{ all -> 0x0091 }
            long r20 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0091 }
            int r18 = (r18 > r20 ? 1 : (r18 == r20 ? 0 : -1))
            if (r18 > 0) goto L_0x002a
        L_0x0190:
            java.util.concurrent.TimeoutException r18 = new java.util.concurrent.TimeoutException     // Catch:{ all -> 0x0091 }
            java.lang.String r19 = "Timeout waiting for connection"
            r18.<init>(r19)     // Catch:{ all -> 0x0091 }
            throw r18     // Catch:{ all -> 0x0091 }
        L_0x0198:
            r18 = move-exception
            r0 = r29
            r14.unqueue(r0)     // Catch:{ all -> 0x0091 }
            r0 = r23
            java.util.LinkedList<org.apache.http.pool.PoolEntryFuture<E>> r0 = r0.pending     // Catch:{ all -> 0x0091 }
            r19 = r0
            r0 = r19
            r1 = r29
            r0.remove(r1)     // Catch:{ all -> 0x0091 }
            throw r18     // Catch:{ all -> 0x0091 }
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.http.pool.AbstractConnPool.getPoolEntryBlocking(java.lang.Object, java.lang.Object, long, java.util.concurrent.TimeUnit, org.apache.http.pool.PoolEntryFuture):org.apache.http.pool.PoolEntry");
    }

    public void release(PoolEntry poolEntry, boolean reusable) {
        this.lock.lock();
        try {
            if (this.leased.remove(poolEntry)) {
                RouteSpecificPool<T, C, E> pool = getPool(poolEntry.getRoute());
                pool.free(poolEntry, reusable);
                if (!reusable || this.isShutDown) {
                    poolEntry.close();
                } else {
                    this.available.addFirst(poolEntry);
                    onRelease(poolEntry);
                }
                PoolEntryFuture<E> future = pool.nextPending();
                if (future != null) {
                    this.pending.remove(future);
                } else {
                    future = this.pending.poll();
                }
                if (future != null) {
                    future.wakeup();
                }
            }
        } finally {
            this.lock.unlock();
        }
    }

    private int getMax(T route) {
        Integer v = this.maxPerRoute.get(route);
        if (v != null) {
            return v.intValue();
        }
        return this.defaultMaxPerRoute;
    }

    public void setMaxTotal(int max) {
        Args.notNegative(max, "Max value");
        this.lock.lock();
        try {
            this.maxTotal = max;
        } finally {
            this.lock.unlock();
        }
    }

    public int getMaxTotal() {
        this.lock.lock();
        try {
            return this.maxTotal;
        } finally {
            this.lock.unlock();
        }
    }

    public void setDefaultMaxPerRoute(int max) {
        Args.notNegative(max, "Max per route value");
        this.lock.lock();
        try {
            this.defaultMaxPerRoute = max;
        } finally {
            this.lock.unlock();
        }
    }

    public int getDefaultMaxPerRoute() {
        this.lock.lock();
        try {
            return this.defaultMaxPerRoute;
        } finally {
            this.lock.unlock();
        }
    }

    public void setMaxPerRoute(T route, int max) {
        Args.notNull(route, "Route");
        Args.notNegative(max, "Max per route value");
        this.lock.lock();
        try {
            this.maxPerRoute.put(route, Integer.valueOf(max));
        } finally {
            this.lock.unlock();
        }
    }

    public int getMaxPerRoute(T route) {
        Args.notNull(route, "Route");
        this.lock.lock();
        try {
            return getMax(route);
        } finally {
            this.lock.unlock();
        }
    }

    public PoolStats getTotalStats() {
        this.lock.lock();
        try {
            return new PoolStats(this.leased.size(), this.pending.size(), this.available.size(), this.maxTotal);
        } finally {
            this.lock.unlock();
        }
    }

    public PoolStats getStats(T route) {
        Args.notNull(route, "Route");
        this.lock.lock();
        try {
            RouteSpecificPool<T, C, E> pool = getPool(route);
            return new PoolStats(pool.getLeasedCount(), pool.getPendingCount(), pool.getAvailableCount(), getMax(route));
        } finally {
            this.lock.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public void enumAvailable(PoolEntryCallback<T, C> callback) {
        this.lock.lock();
        try {
            Iterator<E> it = this.available.iterator();
            while (it.hasNext()) {
                E entry = (PoolEntry) it.next();
                callback.process(entry);
                if (entry.isClosed()) {
                    getPool(entry.getRoute()).remove(entry);
                    it.remove();
                }
            }
            purgePoolMap();
        } finally {
            this.lock.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public void enumLeased(PoolEntryCallback<T, C> callback) {
        this.lock.lock();
        try {
            for (E entry : this.leased) {
                callback.process(entry);
            }
        } finally {
            this.lock.unlock();
        }
    }

    private void purgePoolMap() {
        Iterator<Map.Entry<T, RouteSpecificPool<T, C, E>>> it = this.routeToPool.entrySet().iterator();
        while (it.hasNext()) {
            RouteSpecificPool<T, C, E> pool = it.next().getValue();
            if (pool.getPendingCount() + pool.getAllocatedCount() == 0) {
                it.remove();
            }
        }
    }

    public void closeIdle(long idletime, TimeUnit tunit) {
        Args.notNull(tunit, "Time unit");
        long time = tunit.toMillis(idletime);
        if (time < 0) {
            time = 0;
        }
        final long deadline = System.currentTimeMillis() - time;
        enumAvailable(new PoolEntryCallback<T, C>() {
            public void process(PoolEntry<T, C> entry) {
                if (entry.getUpdated() <= deadline) {
                    entry.close();
                }
            }
        });
    }

    public void closeExpired() {
        final long now = System.currentTimeMillis();
        enumAvailable(new PoolEntryCallback<T, C>() {
            public void process(PoolEntry<T, C> entry) {
                if (entry.isExpired(now)) {
                    entry.close();
                }
            }
        });
    }

    public String toString() {
        return "[leased: " + this.leased + "][available: " + this.available + "][pending: " + this.pending + "]";
    }
}
