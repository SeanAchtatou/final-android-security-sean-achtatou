package org.apache.http.protocol;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.annotation.NotThreadSafe;

@NotThreadSafe
final class ChainBuilder<E> {
    private final LinkedList<E> list = new LinkedList<>();
    private final Map<Class<?>, E> uniqueClasses = new HashMap();

    private void ensureUnique(E e) {
        E previous = this.uniqueClasses.remove(e.getClass());
        if (previous != null) {
            this.list.remove(previous);
        }
        this.uniqueClasses.put(e.getClass(), e);
    }

    public ChainBuilder<E> addFirst(E e) {
        if (e != null) {
            ensureUnique(e);
            this.list.addFirst(e);
        }
        return this;
    }

    public ChainBuilder<E> addLast(E e) {
        if (e != null) {
            ensureUnique(e);
            this.list.addLast(e);
        }
        return this;
    }

    public ChainBuilder<E> addAllFirst(Collection<HttpRequestInterceptor> collection) {
        if (collection != null) {
            Iterator i$ = collection.iterator();
            while (i$.hasNext()) {
                addFirst(i$.next());
            }
        }
        return this;
    }

    public ChainBuilder<E> addAllFirst(Object... arr$) {
        if (arr$ != null) {
            for (Object addFirst : arr$) {
                addFirst(addFirst);
            }
        }
        return this;
    }

    public ChainBuilder<E> addAllLast(Collection<HttpRequestInterceptor> collection) {
        if (collection != null) {
            Iterator i$ = collection.iterator();
            while (i$.hasNext()) {
                addLast(i$.next());
            }
        }
        return this;
    }

    public ChainBuilder<E> addAllLast(Object... arr$) {
        if (arr$ != null) {
            for (Object addLast : arr$) {
                addLast(addLast);
            }
        }
        return this;
    }

    public LinkedList<E> build() {
        return new LinkedList<>(this.list);
    }
}
