package org.apache.http.impl.auth;

import java.io.IOException;
import java.util.Queue;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthOption;
import org.apache.http.auth.AuthProtocolState;
import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.ContextAwareAuthScheme;
import org.apache.http.auth.Credentials;
import org.apache.http.client.AuthenticationStrategy;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.Asserts;

public class HttpAuthenticator {
    private final Log log;

    public HttpAuthenticator(Log log2) {
        this.log = log2 == null ? LogFactory.getLog(getClass()) : log2;
    }

    public HttpAuthenticator() {
        this(null);
    }

    public boolean isAuthenticationRequested(HttpHost host, HttpResponse response, AuthenticationStrategy authStrategy, AuthState authState, HttpContext context) {
        if (authStrategy.isAuthenticationRequested(host, response, context)) {
            this.log.debug("Authentication required");
            if (authState.getState() == AuthProtocolState.SUCCESS) {
                authStrategy.authFailed(host, authState.getAuthScheme(), context);
            }
            return true;
        }
        switch (authState.getState()) {
            case CHALLENGED:
            case HANDSHAKE:
                this.log.debug("Authentication succeeded");
                authState.setState(AuthProtocolState.SUCCESS);
                authStrategy.authSucceeded(host, authState.getAuthScheme(), context);
                break;
            case SUCCESS:
                break;
            default:
                authState.setState(AuthProtocolState.UNCHALLENGED);
                break;
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0050 A[Catch:{ MalformedChallengeException -> 0x0086 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ca A[Catch:{ MalformedChallengeException -> 0x0086 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean handleAuthChallenge(org.apache.http.HttpHost r10, org.apache.http.HttpResponse r11, org.apache.http.client.AuthenticationStrategy r12, org.apache.http.auth.AuthState r13, org.apache.http.protocol.HttpContext r14) {
        /*
            r9 = this;
            org.apache.commons.logging.Log r6 = r9.log     // Catch:{ MalformedChallengeException -> 0x0086 }
            boolean r6 = r6.isDebugEnabled()     // Catch:{ MalformedChallengeException -> 0x0086 }
            if (r6 == 0) goto L_0x0024
            org.apache.commons.logging.Log r6 = r9.log     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ MalformedChallengeException -> 0x0086 }
            r7.<init>()     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.lang.String r8 = r10.toHostString()     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.lang.String r8 = " requested authentication"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.lang.String r7 = r7.toString()     // Catch:{ MalformedChallengeException -> 0x0086 }
            r6.debug(r7)     // Catch:{ MalformedChallengeException -> 0x0086 }
        L_0x0024:
            java.util.Map r3 = r12.getChallenges(r10, r11, r14)     // Catch:{ MalformedChallengeException -> 0x0086 }
            boolean r6 = r3.isEmpty()     // Catch:{ MalformedChallengeException -> 0x0086 }
            if (r6 == 0) goto L_0x0037
            org.apache.commons.logging.Log r6 = r9.log     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.lang.String r7 = "Response contains no authentication challenges"
            r6.debug(r7)     // Catch:{ MalformedChallengeException -> 0x0086 }
            r6 = 0
        L_0x0036:
            return r6
        L_0x0037:
            org.apache.http.auth.AuthScheme r1 = r13.getAuthScheme()     // Catch:{ MalformedChallengeException -> 0x0086 }
            int[] r6 = org.apache.http.impl.auth.HttpAuthenticator.AnonymousClass1.$SwitchMap$org$apache$http$auth$AuthProtocolState     // Catch:{ MalformedChallengeException -> 0x0086 }
            org.apache.http.auth.AuthProtocolState r7 = r13.getState()     // Catch:{ MalformedChallengeException -> 0x0086 }
            int r7 = r7.ordinal()     // Catch:{ MalformedChallengeException -> 0x0086 }
            r6 = r6[r7]     // Catch:{ MalformedChallengeException -> 0x0086 }
            switch(r6) {
                case 1: goto L_0x00b0;
                case 2: goto L_0x00b0;
                case 3: goto L_0x0082;
                case 4: goto L_0x0080;
                case 5: goto L_0x00c8;
                default: goto L_0x004a;
            }     // Catch:{ MalformedChallengeException -> 0x0086 }
        L_0x004a:
            java.util.Queue r0 = r12.select(r3, r10, r11, r14)     // Catch:{ MalformedChallengeException -> 0x0086 }
            if (r0 == 0) goto L_0x0112
            boolean r6 = r0.isEmpty()     // Catch:{ MalformedChallengeException -> 0x0086 }
            if (r6 != 0) goto L_0x0112
            org.apache.commons.logging.Log r6 = r9.log     // Catch:{ MalformedChallengeException -> 0x0086 }
            boolean r6 = r6.isDebugEnabled()     // Catch:{ MalformedChallengeException -> 0x0086 }
            if (r6 == 0) goto L_0x0076
            org.apache.commons.logging.Log r6 = r9.log     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ MalformedChallengeException -> 0x0086 }
            r7.<init>()     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.lang.String r8 = "Selected authentication options: "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.lang.String r7 = r7.toString()     // Catch:{ MalformedChallengeException -> 0x0086 }
            r6.debug(r7)     // Catch:{ MalformedChallengeException -> 0x0086 }
        L_0x0076:
            org.apache.http.auth.AuthProtocolState r6 = org.apache.http.auth.AuthProtocolState.CHALLENGED     // Catch:{ MalformedChallengeException -> 0x0086 }
            r13.setState(r6)     // Catch:{ MalformedChallengeException -> 0x0086 }
            r13.update(r0)     // Catch:{ MalformedChallengeException -> 0x0086 }
            r6 = 1
            goto L_0x0036
        L_0x0080:
            r6 = 0
            goto L_0x0036
        L_0x0082:
            r13.reset()     // Catch:{ MalformedChallengeException -> 0x0086 }
            goto L_0x004a
        L_0x0086:
            r4 = move-exception
            org.apache.commons.logging.Log r6 = r9.log
            boolean r6 = r6.isWarnEnabled()
            if (r6 == 0) goto L_0x00ab
            org.apache.commons.logging.Log r6 = r9.log
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "Malformed challenge: "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = r4.getMessage()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r6.warn(r7)
        L_0x00ab:
            r13.reset()
            r6 = 0
            goto L_0x0036
        L_0x00b0:
            if (r1 != 0) goto L_0x00c8
            org.apache.commons.logging.Log r6 = r9.log     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.lang.String r7 = "Auth scheme is null"
            r6.debug(r7)     // Catch:{ MalformedChallengeException -> 0x0086 }
            r6 = 0
            r12.authFailed(r10, r6, r14)     // Catch:{ MalformedChallengeException -> 0x0086 }
            r13.reset()     // Catch:{ MalformedChallengeException -> 0x0086 }
            org.apache.http.auth.AuthProtocolState r6 = org.apache.http.auth.AuthProtocolState.FAILURE     // Catch:{ MalformedChallengeException -> 0x0086 }
            r13.setState(r6)     // Catch:{ MalformedChallengeException -> 0x0086 }
            r6 = 0
            goto L_0x0036
        L_0x00c8:
            if (r1 == 0) goto L_0x004a
            java.lang.String r5 = r1.getSchemeName()     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.util.Locale r6 = java.util.Locale.US     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.lang.String r6 = r5.toLowerCase(r6)     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.lang.Object r2 = r3.get(r6)     // Catch:{ MalformedChallengeException -> 0x0086 }
            org.apache.http.Header r2 = (org.apache.http.Header) r2     // Catch:{ MalformedChallengeException -> 0x0086 }
            if (r2 == 0) goto L_0x010d
            org.apache.commons.logging.Log r6 = r9.log     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.lang.String r7 = "Authorization challenge processed"
            r6.debug(r7)     // Catch:{ MalformedChallengeException -> 0x0086 }
            r1.processChallenge(r2)     // Catch:{ MalformedChallengeException -> 0x0086 }
            boolean r6 = r1.isComplete()     // Catch:{ MalformedChallengeException -> 0x0086 }
            if (r6 == 0) goto L_0x0105
            org.apache.commons.logging.Log r6 = r9.log     // Catch:{ MalformedChallengeException -> 0x0086 }
            java.lang.String r7 = "Authentication failed"
            r6.debug(r7)     // Catch:{ MalformedChallengeException -> 0x0086 }
            org.apache.http.auth.AuthScheme r6 = r13.getAuthScheme()     // Catch:{ MalformedChallengeException -> 0x0086 }
            r12.authFailed(r10, r6, r14)     // Catch:{ MalformedChallengeException -> 0x0086 }
            r13.reset()     // Catch:{ MalformedChallengeException -> 0x0086 }
            org.apache.http.auth.AuthProtocolState r6 = org.apache.http.auth.AuthProtocolState.FAILURE     // Catch:{ MalformedChallengeException -> 0x0086 }
            r13.setState(r6)     // Catch:{ MalformedChallengeException -> 0x0086 }
            r6 = 0
            goto L_0x0036
        L_0x0105:
            org.apache.http.auth.AuthProtocolState r6 = org.apache.http.auth.AuthProtocolState.HANDSHAKE     // Catch:{ MalformedChallengeException -> 0x0086 }
            r13.setState(r6)     // Catch:{ MalformedChallengeException -> 0x0086 }
            r6 = 1
            goto L_0x0036
        L_0x010d:
            r13.reset()     // Catch:{ MalformedChallengeException -> 0x0086 }
            goto L_0x004a
        L_0x0112:
            r6 = 0
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.http.impl.auth.HttpAuthenticator.handleAuthChallenge(org.apache.http.HttpHost, org.apache.http.HttpResponse, org.apache.http.client.AuthenticationStrategy, org.apache.http.auth.AuthState, org.apache.http.protocol.HttpContext):boolean");
    }

    public void generateAuthResponse(HttpRequest request, AuthState authState, HttpContext context) throws HttpException, IOException {
        AuthScheme authScheme = authState.getAuthScheme();
        Credentials creds = authState.getCredentials();
        switch (authState.getState()) {
            case CHALLENGED:
                Queue<AuthOption> authOptions = authState.getAuthOptions();
                if (authOptions == null) {
                    ensureAuthScheme(authScheme);
                    break;
                } else {
                    while (!authOptions.isEmpty()) {
                        AuthOption authOption = authOptions.remove();
                        AuthScheme authScheme2 = authOption.getAuthScheme();
                        Credentials creds2 = authOption.getCredentials();
                        authState.update(authScheme2, creds2);
                        if (this.log.isDebugEnabled()) {
                            this.log.debug("Generating response to an authentication challenge using " + authScheme2.getSchemeName() + " scheme");
                        }
                        try {
                            request.addHeader(doAuth(authScheme2, creds2, request, context));
                            return;
                        } catch (AuthenticationException ex) {
                            if (this.log.isWarnEnabled()) {
                                this.log.warn(authScheme2 + " authentication error: " + ex.getMessage());
                            }
                        }
                    }
                    return;
                }
            case SUCCESS:
                ensureAuthScheme(authScheme);
                if (authScheme.isConnectionBased()) {
                    return;
                }
                break;
            case FAILURE:
                return;
        }
        if (authScheme != null) {
            try {
                request.addHeader(doAuth(authScheme, creds, request, context));
            } catch (AuthenticationException ex2) {
                if (this.log.isErrorEnabled()) {
                    this.log.error(authScheme + " authentication error: " + ex2.getMessage());
                }
            }
        }
    }

    private void ensureAuthScheme(AuthScheme authScheme) {
        Asserts.notNull(authScheme, "Auth scheme");
    }

    private Header doAuth(AuthScheme authScheme, Credentials creds, HttpRequest request, HttpContext context) throws AuthenticationException {
        if (authScheme instanceof ContextAwareAuthScheme) {
            return ((ContextAwareAuthScheme) authScheme).authenticate(creds, request, context);
        }
        return authScheme.authenticate(creds, request);
    }
}
