package org.apache.http.impl.execchain;

import java.io.Closeable;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.http.HttpClientConnection;
import org.apache.http.annotation.ThreadSafe;
import org.apache.http.concurrent.Cancellable;
import org.apache.http.conn.ConnectionReleaseTrigger;
import org.apache.http.conn.HttpClientConnectionManager;

@ThreadSafe
class ConnectionHolder implements ConnectionReleaseTrigger, Cancellable, Closeable {
    private final Log log;
    private final HttpClientConnection managedConn;
    private final HttpClientConnectionManager manager;
    private volatile boolean released;
    private volatile boolean reusable;
    private volatile Object state;
    private volatile TimeUnit tunit;
    private volatile long validDuration;

    public ConnectionHolder(Log log2, HttpClientConnectionManager manager2, HttpClientConnection managedConn2) {
        this.log = log2;
        this.manager = manager2;
        this.managedConn = managedConn2;
    }

    public boolean isReusable() {
        return this.reusable;
    }

    public void markReusable() {
        this.reusable = true;
    }

    public void markNonReusable() {
        this.reusable = false;
    }

    public void setState(Object state2) {
        this.state = state2;
    }

    public void setValidFor(long duration, TimeUnit tunit2) {
        synchronized (this.managedConn) {
            this.validDuration = duration;
            this.tunit = tunit2;
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void releaseConnection() {
        /*
            r9 = this;
            org.apache.http.HttpClientConnection r8 = r9.managedConn
            monitor-enter(r8)
            boolean r1 = r9.released     // Catch:{ all -> 0x001f }
            if (r1 == 0) goto L_0x0009
            monitor-exit(r8)     // Catch:{ all -> 0x001f }
        L_0x0008:
            return
        L_0x0009:
            r1 = 1
            r9.released = r1     // Catch:{ all -> 0x001f }
            boolean r1 = r9.reusable     // Catch:{ all -> 0x001f }
            if (r1 == 0) goto L_0x0022
            org.apache.http.conn.HttpClientConnectionManager r1 = r9.manager     // Catch:{ all -> 0x001f }
            org.apache.http.HttpClientConnection r2 = r9.managedConn     // Catch:{ all -> 0x001f }
            java.lang.Object r3 = r9.state     // Catch:{ all -> 0x001f }
            long r4 = r9.validDuration     // Catch:{ all -> 0x001f }
            java.util.concurrent.TimeUnit r6 = r9.tunit     // Catch:{ all -> 0x001f }
            r1.releaseConnection(r2, r3, r4, r6)     // Catch:{ all -> 0x001f }
        L_0x001d:
            monitor-exit(r8)     // Catch:{ all -> 0x001f }
            goto L_0x0008
        L_0x001f:
            r1 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x001f }
            throw r1
        L_0x0022:
            org.apache.http.HttpClientConnection r1 = r9.managedConn     // Catch:{ IOException -> 0x003b }
            r1.close()     // Catch:{ IOException -> 0x003b }
            org.apache.commons.logging.Log r1 = r9.log     // Catch:{ IOException -> 0x003b }
            java.lang.String r2 = "Connection discarded"
            r1.debug(r2)     // Catch:{ IOException -> 0x003b }
            org.apache.http.conn.HttpClientConnectionManager r1 = r9.manager     // Catch:{ all -> 0x001f }
            org.apache.http.HttpClientConnection r2 = r9.managedConn     // Catch:{ all -> 0x001f }
            r3 = 0
            r4 = 0
            java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ all -> 0x001f }
            r1.releaseConnection(r2, r3, r4, r6)     // Catch:{ all -> 0x001f }
            goto L_0x001d
        L_0x003b:
            r0 = move-exception
            org.apache.commons.logging.Log r1 = r9.log     // Catch:{ all -> 0x005a }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x005a }
            if (r1 == 0) goto L_0x004d
            org.apache.commons.logging.Log r1 = r9.log     // Catch:{ all -> 0x005a }
            java.lang.String r2 = r0.getMessage()     // Catch:{ all -> 0x005a }
            r1.debug(r2, r0)     // Catch:{ all -> 0x005a }
        L_0x004d:
            org.apache.http.conn.HttpClientConnectionManager r1 = r9.manager     // Catch:{ all -> 0x001f }
            org.apache.http.HttpClientConnection r2 = r9.managedConn     // Catch:{ all -> 0x001f }
            r3 = 0
            r4 = 0
            java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ all -> 0x001f }
            r1.releaseConnection(r2, r3, r4, r6)     // Catch:{ all -> 0x001f }
            goto L_0x001d
        L_0x005a:
            r1 = move-exception
            r7 = r1
            org.apache.http.conn.HttpClientConnectionManager r1 = r9.manager     // Catch:{ all -> 0x001f }
            org.apache.http.HttpClientConnection r2 = r9.managedConn     // Catch:{ all -> 0x001f }
            r3 = 0
            r4 = 0
            java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ all -> 0x001f }
            r1.releaseConnection(r2, r3, r4, r6)     // Catch:{ all -> 0x001f }
            throw r7     // Catch:{ all -> 0x001f }
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.http.impl.execchain.ConnectionHolder.releaseConnection():void");
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void abortConnection() {
        /*
            r9 = this;
            org.apache.http.HttpClientConnection r8 = r9.managedConn
            monitor-enter(r8)
            boolean r1 = r9.released     // Catch:{ all -> 0x0026 }
            if (r1 == 0) goto L_0x0009
            monitor-exit(r8)     // Catch:{ all -> 0x0026 }
        L_0x0008:
            return
        L_0x0009:
            r1 = 1
            r9.released = r1     // Catch:{ all -> 0x0026 }
            org.apache.http.HttpClientConnection r1 = r9.managedConn     // Catch:{ IOException -> 0x0029 }
            r1.shutdown()     // Catch:{ IOException -> 0x0029 }
            org.apache.commons.logging.Log r1 = r9.log     // Catch:{ IOException -> 0x0029 }
            java.lang.String r2 = "Connection discarded"
            r1.debug(r2)     // Catch:{ IOException -> 0x0029 }
            org.apache.http.conn.HttpClientConnectionManager r1 = r9.manager     // Catch:{ all -> 0x0026 }
            org.apache.http.HttpClientConnection r2 = r9.managedConn     // Catch:{ all -> 0x0026 }
            r3 = 0
            r4 = 0
            java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ all -> 0x0026 }
            r1.releaseConnection(r2, r3, r4, r6)     // Catch:{ all -> 0x0026 }
        L_0x0024:
            monitor-exit(r8)     // Catch:{ all -> 0x0026 }
            goto L_0x0008
        L_0x0026:
            r1 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x0026 }
            throw r1
        L_0x0029:
            r0 = move-exception
            org.apache.commons.logging.Log r1 = r9.log     // Catch:{ all -> 0x0048 }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x0048 }
            if (r1 == 0) goto L_0x003b
            org.apache.commons.logging.Log r1 = r9.log     // Catch:{ all -> 0x0048 }
            java.lang.String r2 = r0.getMessage()     // Catch:{ all -> 0x0048 }
            r1.debug(r2, r0)     // Catch:{ all -> 0x0048 }
        L_0x003b:
            org.apache.http.conn.HttpClientConnectionManager r1 = r9.manager     // Catch:{ all -> 0x0026 }
            org.apache.http.HttpClientConnection r2 = r9.managedConn     // Catch:{ all -> 0x0026 }
            r3 = 0
            r4 = 0
            java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ all -> 0x0026 }
            r1.releaseConnection(r2, r3, r4, r6)     // Catch:{ all -> 0x0026 }
            goto L_0x0024
        L_0x0048:
            r1 = move-exception
            r7 = r1
            org.apache.http.conn.HttpClientConnectionManager r1 = r9.manager     // Catch:{ all -> 0x0026 }
            org.apache.http.HttpClientConnection r2 = r9.managedConn     // Catch:{ all -> 0x0026 }
            r3 = 0
            r4 = 0
            java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ all -> 0x0026 }
            r1.releaseConnection(r2, r3, r4, r6)     // Catch:{ all -> 0x0026 }
            throw r7     // Catch:{ all -> 0x0026 }
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.http.impl.execchain.ConnectionHolder.abortConnection():void");
    }

    public boolean cancel() {
        boolean alreadyReleased = this.released;
        this.log.debug("Cancelling request execution");
        abortConnection();
        return !alreadyReleased;
    }

    public boolean isReleased() {
        return this.released;
    }

    public void close() throws IOException {
        abortConnection();
    }
}
