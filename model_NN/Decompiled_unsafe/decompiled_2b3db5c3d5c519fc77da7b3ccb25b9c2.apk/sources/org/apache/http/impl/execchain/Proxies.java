package org.apache.http.impl.execchain;

import java.lang.reflect.Proxy;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.annotation.NotThreadSafe;
import org.apache.http.client.methods.CloseableHttpResponse;

@NotThreadSafe
class Proxies {
    Proxies() {
    }

    static void enhanceEntity(HttpEntityEnclosingRequest request) {
        HttpEntity entity = request.getEntity();
        if (entity != null && !entity.isRepeatable() && !isEnhanced(entity)) {
            request.setEntity((HttpEntity) Proxy.newProxyInstance(HttpEntity.class.getClassLoader(), new Class[]{HttpEntity.class}, new RequestEntityExecHandler(entity)));
        }
    }

    static boolean isEnhanced(HttpEntity entity) {
        if (entity == null || !Proxy.isProxyClass(entity.getClass())) {
            return false;
        }
        return Proxy.getInvocationHandler(entity) instanceof RequestEntityExecHandler;
    }

    static boolean isRepeatable(HttpRequest request) {
        HttpEntity entity;
        if (!(request instanceof HttpEntityEnclosingRequest) || (entity = ((HttpEntityEnclosingRequest) request).getEntity()) == null) {
            return true;
        }
        if (!isEnhanced(entity) || ((RequestEntityExecHandler) Proxy.getInvocationHandler(entity)).isConsumed()) {
            return entity.isRepeatable();
        }
        return true;
    }

    public static CloseableHttpResponse enhanceResponse(HttpResponse original, ConnectionHolder connHolder) {
        return (CloseableHttpResponse) Proxy.newProxyInstance(ResponseProxyHandler.class.getClassLoader(), new Class[]{CloseableHttpResponse.class}, new ResponseProxyHandler(original, connHolder));
    }
}
