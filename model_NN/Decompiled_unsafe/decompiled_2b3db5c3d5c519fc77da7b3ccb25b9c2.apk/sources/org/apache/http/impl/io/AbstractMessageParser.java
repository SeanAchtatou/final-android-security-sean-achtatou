package org.apache.http.impl.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.HttpMessage;
import org.apache.http.MessageConstraintException;
import org.apache.http.ParseException;
import org.apache.http.ProtocolException;
import org.apache.http.annotation.NotThreadSafe;
import org.apache.http.config.MessageConstraints;
import org.apache.http.io.HttpMessageParser;
import org.apache.http.io.SessionInputBuffer;
import org.apache.http.message.BasicLineParser;
import org.apache.http.message.LineParser;
import org.apache.http.params.HttpParamConfig;
import org.apache.http.params.HttpParams;
import org.apache.http.util.Args;
import org.apache.http.util.CharArrayBuffer;

@NotThreadSafe
public abstract class AbstractMessageParser<T extends HttpMessage> implements HttpMessageParser<T> {
    private static final int HEADERS = 1;
    private static final int HEAD_LINE = 0;
    private final List<CharArrayBuffer> headerLines;
    protected final LineParser lineParser;
    private T message;
    private final MessageConstraints messageConstraints;
    private final SessionInputBuffer sessionBuffer;
    private int state;

    /* access modifiers changed from: protected */
    public abstract T parseHead(SessionInputBuffer sessionInputBuffer) throws IOException, HttpException, ParseException;

    @Deprecated
    public AbstractMessageParser(SessionInputBuffer buffer, LineParser parser, HttpParams params) {
        Args.notNull(buffer, "Session input buffer");
        Args.notNull(params, "HTTP parameters");
        this.sessionBuffer = buffer;
        this.messageConstraints = HttpParamConfig.getMessageConstraints(params);
        this.lineParser = parser == null ? BasicLineParser.INSTANCE : parser;
        this.headerLines = new ArrayList();
        this.state = 0;
    }

    public AbstractMessageParser(SessionInputBuffer buffer, LineParser lineParser2, MessageConstraints constraints) {
        this.sessionBuffer = (SessionInputBuffer) Args.notNull(buffer, "Session input buffer");
        this.lineParser = lineParser2 == null ? BasicLineParser.INSTANCE : lineParser2;
        this.messageConstraints = constraints == null ? MessageConstraints.DEFAULT : constraints;
        this.headerLines = new ArrayList();
        this.state = 0;
    }

    public static Header[] parseHeaders(SessionInputBuffer inbuffer, int maxHeaderCount, int maxLineLen, LineParser parser) throws HttpException, IOException {
        List<CharArrayBuffer> headerLines2 = new ArrayList<>();
        if (parser == null) {
            parser = BasicLineParser.INSTANCE;
        }
        return parseHeaders(inbuffer, maxHeaderCount, maxLineLen, parser, headerLines2);
    }

    public static Header[] parseHeaders(SessionInputBuffer inbuffer, int maxHeaderCount, int maxLineLen, LineParser parser, List<CharArrayBuffer> headerLines2) throws HttpException, IOException {
        Args.notNull(inbuffer, "Session input buffer");
        Args.notNull(parser, "Line parser");
        Args.notNull(headerLines2, "Header line list");
        CharArrayBuffer current = null;
        CharArrayBuffer previous = null;
        while (true) {
            if (current == null) {
                current = new CharArrayBuffer(64);
            } else {
                current.clear();
            }
            if (inbuffer.readLine(current) == -1 || current.length() < 1) {
                Header[] headers = new Header[headerLines2.size()];
                int i = 0;
            } else {
                if ((current.charAt(0) == ' ' || current.charAt(0) == 9) && previous != null) {
                    int i2 = 0;
                    while (i2 < current.length() && ((ch = current.charAt(i2)) == ' ' || ch == 9)) {
                        i2++;
                    }
                    if (maxLineLen <= 0 || ((previous.length() + 1) + current.length()) - i2 <= maxLineLen) {
                        previous.append(' ');
                        previous.append(current, i2, current.length() - i2);
                    } else {
                        throw new MessageConstraintException("Maximum line length limit exceeded");
                    }
                } else {
                    headerLines2.add(current);
                    previous = current;
                    current = null;
                }
                if (maxHeaderCount > 0 && headerLines2.size() >= maxHeaderCount) {
                    throw new MessageConstraintException("Maximum header count exceeded");
                }
            }
        }
        Header[] headers2 = new Header[headerLines2.size()];
        int i3 = 0;
        while (i3 < headerLines2.size()) {
            try {
                headers2[i3] = parser.parseHeader(headerLines2.get(i3));
                i3++;
            } catch (ParseException ex) {
                throw new ProtocolException(ex.getMessage());
            }
        }
        return headers2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public T parse() throws java.io.IOException, org.apache.http.HttpException {
        /*
            r9 = this;
            int r3 = r9.state
            switch(r3) {
                case 0: goto L_0x000d;
                case 1: goto L_0x0018;
                default: goto L_0x0005;
            }
        L_0x0005:
            java.lang.IllegalStateException r4 = new java.lang.IllegalStateException
            java.lang.String r5 = "Inconsistent parser state"
            r4.<init>(r5)
            throw r4
        L_0x000d:
            org.apache.http.io.SessionInputBuffer r4 = r9.sessionBuffer     // Catch:{ ParseException -> 0x0041 }
            org.apache.http.HttpMessage r4 = r9.parseHead(r4)     // Catch:{ ParseException -> 0x0041 }
            r9.message = r4     // Catch:{ ParseException -> 0x0041 }
            r4 = 1
            r9.state = r4
        L_0x0018:
            org.apache.http.io.SessionInputBuffer r4 = r9.sessionBuffer
            org.apache.http.config.MessageConstraints r5 = r9.messageConstraints
            int r5 = r5.getMaxHeaderCount()
            org.apache.http.config.MessageConstraints r6 = r9.messageConstraints
            int r6 = r6.getMaxLineLength()
            org.apache.http.message.LineParser r7 = r9.lineParser
            java.util.List<org.apache.http.util.CharArrayBuffer> r8 = r9.headerLines
            org.apache.http.Header[] r0 = parseHeaders(r4, r5, r6, r7, r8)
            T r4 = r9.message
            r4.setHeaders(r0)
            T r2 = r9.message
            r4 = 0
            r9.message = r4
            java.util.List<org.apache.http.util.CharArrayBuffer> r4 = r9.headerLines
            r4.clear()
            r4 = 0
            r9.state = r4
            return r2
        L_0x0041:
            r1 = move-exception
            org.apache.http.ProtocolException r4 = new org.apache.http.ProtocolException
            java.lang.String r5 = r1.getMessage()
            r4.<init>(r5, r1)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.http.impl.io.AbstractMessageParser.parse():org.apache.http.HttpMessage");
    }
}
