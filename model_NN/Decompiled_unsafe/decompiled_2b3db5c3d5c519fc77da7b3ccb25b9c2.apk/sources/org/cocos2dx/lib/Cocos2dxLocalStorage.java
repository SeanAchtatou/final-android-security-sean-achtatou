package org.cocos2dx.lib;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.facebook.share.internal.ShareConstants;

public class Cocos2dxLocalStorage {
    /* access modifiers changed from: private */
    public static String DATABASE_NAME = "jsb.sqlite";
    private static final int DATABASE_VERSION = 1;
    /* access modifiers changed from: private */
    public static String TABLE_NAME = ShareConstants.WEB_DIALOG_PARAM_DATA;
    private static final String TAG = "Cocos2dxLocalStorage";
    private static SQLiteDatabase mDatabase = null;
    private static DBOpenHelper mDatabaseOpenHelper = null;

    public static boolean init(String dbName, String tableName) {
        if (Cocos2dxActivity.getContext() == null) {
            return false;
        }
        DATABASE_NAME = dbName;
        TABLE_NAME = tableName;
        mDatabaseOpenHelper = new DBOpenHelper(Cocos2dxActivity.getContext());
        mDatabase = mDatabaseOpenHelper.getWritableDatabase();
        return true;
    }

    public static void destory() {
        if (mDatabase != null) {
            mDatabase.close();
        }
    }

    public static void setItem(String key, String value) {
        try {
            mDatabase.execSQL("replace into " + TABLE_NAME + "(key,value)values(?,?)", new Object[]{key, value});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        android.util.Log.e(org.cocos2dx.lib.Cocos2dxLocalStorage.TAG, "The key contains more than one value.");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getItem(java.lang.String r7) {
        /*
            r2 = 0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x003c }
            java.lang.String r5 = "select value from "
            r4.<init>(r5)     // Catch:{ Exception -> 0x003c }
            java.lang.String r5 = org.cocos2dx.lib.Cocos2dxLocalStorage.TABLE_NAME     // Catch:{ Exception -> 0x003c }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x003c }
            java.lang.String r5 = " where key=?"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x003c }
            java.lang.String r3 = r4.toString()     // Catch:{ Exception -> 0x003c }
            android.database.sqlite.SQLiteDatabase r4 = org.cocos2dx.lib.Cocos2dxLocalStorage.mDatabase     // Catch:{ Exception -> 0x003c }
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Exception -> 0x003c }
            r6 = 0
            r5[r6] = r7     // Catch:{ Exception -> 0x003c }
            android.database.Cursor r0 = r4.rawQuery(r3, r5)     // Catch:{ Exception -> 0x003c }
        L_0x0024:
            boolean r4 = r0.moveToNext()     // Catch:{ Exception -> 0x003c }
            if (r4 != 0) goto L_0x0032
        L_0x002a:
            r0.close()     // Catch:{ Exception -> 0x003c }
        L_0x002d:
            if (r2 != 0) goto L_0x0031
            java.lang.String r2 = ""
        L_0x0031:
            return r2
        L_0x0032:
            if (r2 == 0) goto L_0x0041
            java.lang.String r4 = "Cocos2dxLocalStorage"
            java.lang.String r5 = "The key contains more than one value."
            android.util.Log.e(r4, r5)     // Catch:{ Exception -> 0x003c }
            goto L_0x002a
        L_0x003c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002d
        L_0x0041:
            java.lang.String r4 = "value"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x003c }
            java.lang.String r2 = r0.getString(r4)     // Catch:{ Exception -> 0x003c }
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: org.cocos2dx.lib.Cocos2dxLocalStorage.getItem(java.lang.String):java.lang.String");
    }

    public static void removeItem(String key) {
        try {
            mDatabase.execSQL("delete from " + TABLE_NAME + " where key=?", new Object[]{key});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void clear() {
        try {
            mDatabase.execSQL("delete from " + TABLE_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class DBOpenHelper extends SQLiteOpenHelper {
        DBOpenHelper(Context context) {
            super(context, Cocos2dxLocalStorage.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE IF NOT EXISTS " + Cocos2dxLocalStorage.TABLE_NAME + "(key TEXT PRIMARY KEY,value TEXT);");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(Cocos2dxLocalStorage.TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
        }
    }
}
