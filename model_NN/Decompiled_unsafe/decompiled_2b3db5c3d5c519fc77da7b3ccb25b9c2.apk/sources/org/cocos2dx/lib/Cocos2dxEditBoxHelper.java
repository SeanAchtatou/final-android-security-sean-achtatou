package org.cocos2dx.lib;

import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.TextView;

public class Cocos2dxEditBoxHelper {
    /* access modifiers changed from: private */
    public static final String TAG = Cocos2dxEditBoxHelper.class.getSimpleName();
    /* access modifiers changed from: private */
    public static Cocos2dxActivity mCocos2dxActivity;
    /* access modifiers changed from: private */
    public static SparseArray<Cocos2dxEditBox> mEditBoxArray;
    /* access modifiers changed from: private */
    public static ResizeLayout mFrameLayout;
    private static int mViewTag = 0;

    private static native void editBoxEditingChanged(int i, String str);

    private static native void editBoxEditingDidBegin(int i);

    private static native void editBoxEditingDidEnd(int i, String str);

    public static void __editBoxEditingDidBegin(int index) {
        editBoxEditingDidBegin(index);
    }

    public static void __editBoxEditingChanged(int index, String text) {
        if (containsEmoji(text)) {
            editBoxEditingChanged(index, "");
        } else {
            editBoxEditingChanged(index, text);
        }
    }

    public static void __editBoxEditingDidEnd(int index, String text) {
        if (containsEmoji(text)) {
            editBoxEditingDidEnd(index, "");
        } else {
            editBoxEditingDidEnd(index, text);
        }
    }

    private static boolean containsEmoji(String source) {
        if (source == null || source.length() == 0) {
            return false;
        }
        int len = source.length();
        for (int i = 0; i < len; i++) {
            if (isEmojiCharacter(source.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    private static boolean isEmojiCharacter(char codePoint) {
        return (codePoint == 0 || codePoint == 9 || codePoint == 10 || codePoint == 13 || (codePoint >= ' ' && codePoint <= 55295) || ((codePoint >= 57344 && codePoint <= 65533) || (codePoint >= 0 && codePoint <= 65535))) ? false : true;
    }

    public static String filterEmoji(String source) {
        if (!containsEmoji(source)) {
            return source;
        }
        StringBuilder buf = null;
        int len = source.length();
        for (int i = 0; i < len; i++) {
            char codePoint = source.charAt(i);
            if (!isEmojiCharacter(codePoint)) {
                if (buf == null) {
                    buf = new StringBuilder(source.length());
                }
                buf.append(codePoint);
            }
        }
        if (buf == null) {
            return null;
        }
        return buf.length() == len ? source : buf.toString();
    }

    public Cocos2dxEditBoxHelper(ResizeLayout layout) {
        mFrameLayout = layout;
        mCocos2dxActivity = (Cocos2dxActivity) Cocos2dxActivity.getContext();
        mEditBoxArray = new SparseArray<>();
    }

    public static int convertToSP(float point) {
        return (int) TypedValue.applyDimension(2, point, mCocos2dxActivity.getResources().getDisplayMetrics());
    }

    public static int createEditBox(int left, int top, int width, int height, float scaleX) {
        final int index = mViewTag;
        final float f = scaleX;
        final int i = height;
        final int i2 = left;
        final int i3 = top;
        final int i4 = width;
        mCocos2dxActivity.runOnUiThread(new Runnable() {
            public void run() {
                final Cocos2dxEditBox editBox = new Cocos2dxEditBox(Cocos2dxEditBoxHelper.mCocos2dxActivity);
                editBox.setFocusable(true);
                editBox.setFocusableInTouchMode(true);
                editBox.setInputFlag(4);
                editBox.setInputMode(6);
                editBox.setReturnType(0);
                editBox.setHintTextColor(-7829368);
                editBox.setVisibility(4);
                editBox.setBackgroundColor(0);
                editBox.setTextColor(-1);
                editBox.setSingleLine();
                editBox.setOpenGLViewScaleX(f);
                float density = Cocos2dxEditBoxHelper.mCocos2dxActivity.getResources().getDisplayMetrics().density;
                int paddingBottom = Cocos2dxEditBoxHelper.convertToSP(((float) ((int) ((((float) i) * 0.33f) / density))) - ((f * 5.0f) / density)) / 2;
                editBox.setPadding(Cocos2dxEditBoxHelper.convertToSP((float) ((int) ((f * 5.0f) / density))), paddingBottom, 0, paddingBottom);
                FrameLayout.LayoutParams lParams = new FrameLayout.LayoutParams(-2, -2);
                lParams.leftMargin = i2;
                lParams.topMargin = i3;
                lParams.width = i4;
                lParams.height = i;
                lParams.gravity = 51;
                Cocos2dxEditBoxHelper.mFrameLayout.addView(editBox, lParams);
                final int i = index;
                editBox.addTextChangedListener(new TextWatcher() {
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    public void onTextChanged(final CharSequence s, int start, int before, int count) {
                        Cocos2dxActivity access$0 = Cocos2dxEditBoxHelper.mCocos2dxActivity;
                        final int i = i;
                        access$0.runOnGLThread(new Runnable() {
                            public void run() {
                                Cocos2dxEditBoxHelper.__editBoxEditingChanged(i, s.toString());
                            }
                        });
                    }

                    public void afterTextChanged(Editable s) {
                    }
                });
                final int i2 = index;
                editBox.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
                            Cocos2dxActivity access$0 = Cocos2dxEditBoxHelper.mCocos2dxActivity;
                            final int i = i2;
                            access$0.runOnGLThread(new Runnable() {
                                public void run() {
                                    Cocos2dxEditBoxHelper.__editBoxEditingDidBegin(i);
                                }
                            });
                            editBox.setSelection(editBox.getText().length());
                            Cocos2dxEditBoxHelper.mFrameLayout.setEnableForceDoLayout(true);
                            Cocos2dxEditBoxHelper.mCocos2dxActivity.getGLSurfaceView().setSoftKeyboardShown(true);
                            Log.d(Cocos2dxEditBoxHelper.TAG, "edit box get focus");
                            return;
                        }
                        editBox.setVisibility(8);
                        Cocos2dxActivity access$02 = Cocos2dxEditBoxHelper.mCocos2dxActivity;
                        final int i2 = i2;
                        final Cocos2dxEditBox cocos2dxEditBox = editBox;
                        access$02.runOnGLThread(new Runnable() {
                            public void run() {
                                Cocos2dxEditBoxHelper.__editBoxEditingDidEnd(i2, cocos2dxEditBox.getText().toString());
                            }
                        });
                        Cocos2dxEditBoxHelper.mFrameLayout.setEnableForceDoLayout(false);
                        Log.d(Cocos2dxEditBoxHelper.TAG, "edit box lose focus");
                    }
                });
                final int i3 = index;
                editBox.setOnKeyListener(new View.OnKeyListener() {
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() != 0 || keyCode != 66 || (editBox.getInputType() & 131072) == 131072) {
                            return false;
                        }
                        Cocos2dxEditBoxHelper.closeKeyboard(i3);
                        Cocos2dxEditBoxHelper.mCocos2dxActivity.getGLSurfaceView().requestFocus();
                        return true;
                    }
                });
                final int i4 = index;
                editBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId != 6) {
                            return false;
                        }
                        Cocos2dxEditBoxHelper.closeKeyboard(i4);
                        Cocos2dxEditBoxHelper.mCocos2dxActivity.getGLSurfaceView().requestFocus();
                        return false;
                    }
                });
                Cocos2dxEditBoxHelper.mEditBoxArray.put(index, editBox);
            }
        });
        int i5 = mViewTag;
        mViewTag = i5 + 1;
        return i5;
    }

    public static void removeEditBox(final int index) {
        mCocos2dxActivity.runOnUiThread(new Runnable() {
            public void run() {
                Cocos2dxEditBox editBox = (Cocos2dxEditBox) Cocos2dxEditBoxHelper.mEditBoxArray.get(index);
                if (editBox != null) {
                    Cocos2dxEditBoxHelper.mEditBoxArray.remove(index);
                    Cocos2dxEditBoxHelper.mFrameLayout.removeView(editBox);
                    Log.e(Cocos2dxEditBoxHelper.TAG, "remove EditBox");
                }
            }
        });
    }

    public static void setFont(final int index, final String fontName, final float fontSize) {
        mCocos2dxActivity.runOnUiThread(new Runnable() {
            public void run() {
                Typeface tf;
                Cocos2dxEditBox editBox = (Cocos2dxEditBox) Cocos2dxEditBoxHelper.mEditBoxArray.get(index);
                if (editBox != null) {
                    if (!fontName.isEmpty()) {
                        tf = Typeface.create(fontName, 0);
                    } else {
                        tf = Typeface.DEFAULT;
                    }
                    if (fontSize >= 0.0f) {
                        editBox.setTextSize(2, fontSize / Cocos2dxEditBoxHelper.mCocos2dxActivity.getResources().getDisplayMetrics().density);
                    }
                    editBox.setTypeface(tf);
                }
            }
        });
    }

    public static void setFontColor(int index, int red, int green, int blue, int alpha) {
        final int i = index;
        final int i2 = alpha;
        final int i3 = red;
        final int i4 = green;
        final int i5 = blue;
        mCocos2dxActivity.runOnUiThread(new Runnable() {
            public void run() {
                Cocos2dxEditBox editBox = (Cocos2dxEditBox) Cocos2dxEditBoxHelper.mEditBoxArray.get(i);
                if (editBox != null) {
                    editBox.setTextColor(Color.argb(i2, i3, i4, i5));
                }
            }
        });
    }

    public static void setPlaceHolderText(final int index, final String text) {
        mCocos2dxActivity.runOnUiThread(new Runnable() {
            public void run() {
                Cocos2dxEditBox editBox = (Cocos2dxEditBox) Cocos2dxEditBoxHelper.mEditBoxArray.get(index);
                if (editBox != null) {
                    editBox.setHint(text);
                }
            }
        });
    }

    public static void setPlaceHolderTextColor(int index, int red, int green, int blue, int alpha) {
        final int i = index;
        final int i2 = alpha;
        final int i3 = red;
        final int i4 = green;
        final int i5 = blue;
        mCocos2dxActivity.runOnUiThread(new Runnable() {
            public void run() {
                Cocos2dxEditBox editBox = (Cocos2dxEditBox) Cocos2dxEditBoxHelper.mEditBoxArray.get(i);
                if (editBox != null) {
                    editBox.setHintTextColor(Color.argb(i2, i3, i4, i5));
                }
            }
        });
    }

    public static void setMaxLength(final int index, final int maxLength) {
        mCocos2dxActivity.runOnUiThread(new Runnable() {
            public void run() {
                Cocos2dxEditBox editBox = (Cocos2dxEditBox) Cocos2dxEditBoxHelper.mEditBoxArray.get(index);
                if (editBox != null) {
                    editBox.setMaxLength(maxLength);
                }
            }
        });
    }

    public static void setVisible(final int index, final boolean visible) {
        mCocos2dxActivity.runOnUiThread(new Runnable() {
            public void run() {
                Cocos2dxEditBox editBox = (Cocos2dxEditBox) Cocos2dxEditBoxHelper.mEditBoxArray.get(index);
                if (editBox != null) {
                    editBox.setVisibility(visible ? 0 : 8);
                    if (visible) {
                        editBox.requestFocus();
                        Cocos2dxEditBoxHelper.openKeyboard(index);
                        return;
                    }
                    Cocos2dxEditBoxHelper.mCocos2dxActivity.getGLSurfaceView().requestFocus();
                    Cocos2dxEditBoxHelper.closeKeyboard(index);
                }
            }
        });
    }

    public static void setText(final int index, final String text) {
        mCocos2dxActivity.runOnUiThread(new Runnable() {
            public void run() {
                Cocos2dxEditBox editBox = (Cocos2dxEditBox) Cocos2dxEditBoxHelper.mEditBoxArray.get(index);
                if (editBox != null) {
                    editBox.setText(text);
                }
            }
        });
    }

    public static void setReturnType(final int index, final int returnType) {
        mCocos2dxActivity.runOnUiThread(new Runnable() {
            public void run() {
                Cocos2dxEditBox editBox = (Cocos2dxEditBox) Cocos2dxEditBoxHelper.mEditBoxArray.get(index);
                if (editBox != null) {
                    editBox.setReturnType(returnType);
                }
            }
        });
    }

    public static void setInputMode(final int index, final int inputMode) {
        mCocos2dxActivity.runOnUiThread(new Runnable() {
            public void run() {
                Cocos2dxEditBox editBox = (Cocos2dxEditBox) Cocos2dxEditBoxHelper.mEditBoxArray.get(index);
                if (editBox != null) {
                    editBox.setInputMode(inputMode);
                }
            }
        });
    }

    public static void setInputFlag(final int index, final int inputFlag) {
        mCocos2dxActivity.runOnUiThread(new Runnable() {
            public void run() {
                Cocos2dxEditBox editBox = (Cocos2dxEditBox) Cocos2dxEditBoxHelper.mEditBoxArray.get(index);
                if (editBox != null) {
                    editBox.setInputFlag(inputFlag);
                }
            }
        });
    }

    public static void setEditBoxViewRect(int index, int left, int top, int maxWidth, int maxHeight) {
        final int i = index;
        final int i2 = left;
        final int i3 = top;
        final int i4 = maxWidth;
        final int i5 = maxHeight;
        mCocos2dxActivity.runOnUiThread(new Runnable() {
            public void run() {
                Cocos2dxEditBox editBox = (Cocos2dxEditBox) Cocos2dxEditBoxHelper.mEditBoxArray.get(i);
                if (editBox != null) {
                    editBox.setEditBoxViewRect(i2, i3, i4, i5);
                }
            }
        });
    }

    public static void openKeyboard(int index) {
        InputMethodManager imm = (InputMethodManager) Cocos2dxActivity.getContext().getSystemService("input_method");
        Cocos2dxEditBox editBox = mEditBoxArray.get(index);
        if (editBox != null) {
            imm.showSoftInput(editBox, 0);
            mCocos2dxActivity.getGLSurfaceView().setSoftKeyboardShown(true);
        }
    }

    public static void closeKeyboard(int index) {
        InputMethodManager imm = (InputMethodManager) Cocos2dxActivity.getContext().getSystemService("input_method");
        Cocos2dxEditBox editBox = mEditBoxArray.get(index);
        if (editBox != null) {
            imm.hideSoftInputFromWindow(editBox.getWindowToken(), 0);
            mCocos2dxActivity.getGLSurfaceView().setSoftKeyboardShown(false);
        }
    }
}
