package org.cocos2dx.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Vibrator;
import android.telephony.TelephonyManager;
import com.bluepay.data.Config;
import java.util.Vector;
import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.utils.PSDialog;

public class PSNative {
    static Drawable mAppIcon = null;
    static Cocos2dxActivity mContext = null;
    static PSDialog mCreatingDialog = null;
    static PSDialog.PSDialogListener mPSDialogListener = new PSDialog.PSDialogListener() {
        public void onDismiss(PSDialog dialog) {
            PSNative.showPreAlert();
        }
    };
    static PSDialog mShowingDialog = null;
    static Vector<PSDialog> mShowingDialogs = null;
    static TelephonyManager mTelephonyManager = null;
    static Vibrator mVibrator = null;

    public static void init(Cocos2dxActivity context) {
        mContext = context;
        mTelephonyManager = (TelephonyManager) context.getSystemService("phone");
        mVibrator = (Vibrator) context.getSystemService("vibrator");
        mShowingDialogs = new Vector<>();
    }

    public static void setAppIcon(Drawable icon) {
        mAppIcon = icon;
    }

    public static void createAlert(final String title, final String message, final Vector<String> buttonTitles, final int listener) {
        if (mContext != null) {
            mContext.runOnUiThread(new Runnable() {
                public void run() {
                    PSNative.mCreatingDialog = new PSDialog(PSNative.mContext).setCancelable(false).setMessage(message).setTitle(title).setLuaListener(listener).setListener(PSNative.mPSDialogListener).setIcon(PSNative.mAppIcon);
                    for (int i = 0; i < buttonTitles.size(); i++) {
                        PSNative.addAlertButton((String) buttonTitles.get(i));
                    }
                    if (PSNative.mShowingDialog != null && PSNative.mShowingDialog.isShowing()) {
                        PSNative.mShowingDialogs.add(PSNative.mShowingDialog);
                        PSNative.mShowingDialog.hide();
                    }
                    PSNative.mCreatingDialog.show();
                    PSNative.mShowingDialog = PSNative.mCreatingDialog;
                    PSNative.mCreatingDialog = null;
                }
            });
        }
    }

    @Deprecated
    public static void createAlert(final String title, final String message, final String defalutButtonTitle, final int listener) {
        if (mContext != null) {
            mContext.runOnUiThread(new Runnable() {
                public void run() {
                    PSNative.mCreatingDialog = new PSDialog(PSNative.mContext).setCancelable(false).setMessage(message).setTitle(title).setLuaListener(listener).setListener(PSNative.mPSDialogListener);
                    PSNative.addAlertButton(defalutButtonTitle);
                    if (PSNative.mShowingDialog != null && PSNative.mShowingDialog.isShowing()) {
                        PSNative.mShowingDialogs.add(PSNative.mShowingDialog);
                        PSNative.mShowingDialog.hide();
                    }
                    PSNative.mCreatingDialog.show();
                    PSNative.mShowingDialog = PSNative.mCreatingDialog;
                    PSNative.mCreatingDialog = null;
                }
            });
        }
    }

    public static int addAlertButton(String buttonTitle) {
        if (mCreatingDialog == null) {
            return 0;
        }
        return mCreatingDialog.addAlertButton(buttonTitle);
    }

    public static void showAlert() {
        if (mCreatingDialog != null) {
            mContext.runOnUiThread(new Runnable() {
                public void run() {
                    if (PSNative.mShowingDialog != null && PSNative.mShowingDialog.isShowing()) {
                        PSNative.mShowingDialogs.add(PSNative.mShowingDialog);
                        PSNative.mShowingDialog.hide();
                    }
                    PSNative.mCreatingDialog.show();
                    PSNative.mShowingDialog = PSNative.mCreatingDialog;
                    PSNative.mCreatingDialog = null;
                }
            });
        }
    }

    public static void showAlertLua(final int luaFunctionId) {
        if (mCreatingDialog != null) {
            mContext.runOnGLThread(new Runnable() {
                public void run() {
                    PSNative.mCreatingDialog.setLuaListener(luaFunctionId);
                    PSNative.showAlert();
                }
            });
        }
    }

    public static void cancelAlert() {
        if (mShowingDialog != null) {
            mContext.runOnUiThread(new Runnable() {
                public void run() {
                    PSNative.mShowingDialog.dismiss();
                    PSNative.mShowingDialog = null;
                }
            });
        }
    }

    public static void showPreAlert() {
        if (mShowingDialogs.size() > 0) {
            mShowingDialog = mShowingDialogs.firstElement();
            mShowingDialogs.remove(0);
            mShowingDialog.show();
            return;
        }
        mShowingDialog = null;
    }

    public static void openURL(String url) {
        if (mContext != null) {
            mContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
        }
    }

    public static String getInputText(String title, String message, String defaultValue) {
        return "";
    }

    private static String getMacAddress() {
        WifiInfo info = ((WifiManager) mContext.getSystemService(Config.NETWORKTYPE_WIFI)).getConnectionInfo();
        if (info == null) {
            return null;
        }
        return info.getMacAddress();
    }

    public static String getOpenUDID() {
        String id = null;
        if (mTelephonyManager != null) {
            id = mTelephonyManager.getDeviceId();
        }
        if (id == null) {
            id = getMacAddress();
        }
        if (id == null) {
            return "";
        }
        return id;
    }

    public static String getDeviceName() {
        return Build.USER;
    }

    public static void vibrate(long time) {
        if (mVibrator != null) {
            mVibrator.vibrate(time);
        }
    }

    public static void vibrate(long[] pattern, int repeatcout) {
        if (mVibrator != null) {
            mVibrator.vibrate(pattern, repeatcout);
        }
    }

    public static Context getAppContext() {
        return mContext;
    }
}
