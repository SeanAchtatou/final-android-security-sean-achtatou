
varying vec4 v_fragmentColor; 
varying vec2 v_texCoord;

uniform vec4 u_effectColor;
uniform vec4 u_textColor;

uniform float u_ctime;
uniform float u_gtime;
uniform float u_radius;
uniform vec4 u_color;
 
void main() 
{
	///*
    vec4 accum = vec4(0.0);
    vec4 normal = vec4(0.0);
	vec4 color = vec4(0x2a, 0xa7, 0xff, 0x01);
	float ctime = 10;
	float gtime = 10000;
	float radius = 1;
    
    //normal = texture2D(CC_Texture0, vec2(v_texCoord.x, v_texCoord.y));
    normal = texture2D(CC_Texture0, v_texCoord);
    
    for(float i = 1.0; i <= radius; i += 1.0)
    {
    	accum += texture2D(CC_Texture0, vec2(v_texCoord.x - 0.01 * i, v_texCoord.y - 0.01 * i));
	    accum += texture2D(CC_Texture0, vec2(v_texCoord.x + 0.01 * i, v_texCoord.y - 0.01 * i));
	    accum += texture2D(CC_Texture0, vec2(v_texCoord.x + 0.01 * i, v_texCoord.y + 0.01 * i));
	    accum += texture2D(CC_Texture0, vec2(v_texCoord.x - 0.01 * i, v_texCoord.y + 0.01 * i));
    }
    
    accum.rgb =  color.rgb * color.a * accum.a * 0.6;
    float opacity = ((1.0 - normal.a) / radius) * (ctime / gtime);
    
    normal = (accum * opacity) + (normal * normal.a);
    
    gl_FragColor = v_fragmentColor * normal;
	//*/
	
	/*
    float dist = texture2D(CC_Texture0, v_texCoord).a;
    //TODO: Implementation 'fwidth' for glsl 1.0 \n
    //float width = fwidth(dist); \n
    //assign width for constant will lead to a little bit fuzzy,it's temporary measure.\n
    float width = 0.04; 
    float alpha = smoothstep(0.5-width, 0.5+width, dist); 
    //glow \n
    float mu = smoothstep(0.5, 1.0, sqrt(dist)); 
	vec4 u_effectColor = vec4(0x2a, 0xa7, 0xff, 0x01);
	vec4 u_textColor = vec4(0x2a, 0xa7, 0xff, 0x01);
    vec4 color = u_effectColor*(1.0-alpha) + u_textColor*alpha;
    gl_FragColor = v_fragmentColor * vec4(color.rgb, max(alpha,mu)*color.a); 
	//*/
	
	/*
    vec4 sample = texture2D(CC_Texture0, v_texCoord);
    float fontAlpha = sample.a; 
    float outlineAlpha = sample.r; 
	vec4 u_effectColor = vec4(0x2a, 0xa7, 0xff, 0xff);
	vec4 u_textColor = vec4(0xff, 0xff, 0xff, 0xff);
    if (outlineAlpha > 0.0){ 
        vec4 color = u_textColor * fontAlpha + u_effectColor * (1.0 - fontAlpha);
        gl_FragColor = v_fragmentColor * vec4( color.rgb,max(fontAlpha,outlineAlpha)*color.a);
    }
    else {
        discard;
    } 
	//*/
}
