package com.iPhand.FirstAid;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;

public class ClassesListView extends Activity {
    int[] Icons = {R.drawable.icon_01, R.drawable.icon_02, R.drawable.icon_03, R.drawable.icon_04, R.drawable.icon_05, R.drawable.icon_06, R.drawable.icon_07, R.drawable.icon_08, R.drawable.icon_09, R.drawable.icon_10};
    int Num = 0;
    String Title = null;
    private SQLiteDatabase database;
    ListView mListView;
    String[] mQ = {"common", "necessaryknowhow", "outdoor", "internal", "external", "face", "gynaecological", "paediatrics", "skin", "psychological"};

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialogueclass_list);
        this.database = DBUtil.openDatabase(this);
        this.mListView = (ListView) findViewById(R.id.DialogueClassList);
        this.Num = getIntent().getExtras().getInt("Num");
        this.Title = getIntent().getExtras().getString("Title");
        setTitle(this.Title);
        setListAdapter();
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                Intent intent = new Intent(ClassesListView.this, DetailView.class);
                Bundle bundle = new Bundle();
                bundle.putInt("Num", ClassesListView.this.getIntent().getExtras().getInt("Num"));
                bundle.putInt("ItemNum", i);
                intent.putExtras(bundle);
                ClassesListView.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.database.close();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        this.database = DBUtil.openDatabase(this);
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.database.close();
        super.onStop();
    }

    public void setListAdapter() {
        int i = 0;
        Cursor rawQuery = this.database.rawQuery("select topic from firstaid where category =?", new String[]{this.mQ[this.Num]});
        if (rawQuery.getCount() > 0) {
            rawQuery.moveToFirst();
        }
        try {
            ArrayList arrayList = new ArrayList();
            while (true) {
                int i2 = i;
                if (i >= rawQuery.getCount()) {
                    this.mListView.setAdapter((ListAdapter) new SimpleAdapter(this, arrayList, R.layout.dialogue_details_item, new String[]{"Image", "ItemTitle"}, new int[]{R.id.DialogueclassImage, R.id.DialogueclassTitle}));
                    rawQuery.close();
                    return;
                }
                HashMap hashMap = new HashMap();
                hashMap.put("Image", Integer.valueOf(this.Icons[this.Num]));
                hashMap.put("ItemTitle", rawQuery.getString(rawQuery.getColumnIndex("topic")));
                arrayList.add(hashMap);
                rawQuery.moveToNext();
                i = i2 + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
