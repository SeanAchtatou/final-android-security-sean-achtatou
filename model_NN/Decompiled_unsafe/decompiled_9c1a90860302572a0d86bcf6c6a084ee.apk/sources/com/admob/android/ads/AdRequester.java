package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

class AdRequester {
    private static final String ADMOB_ENDPOINT = "http://r.admob.com/ad_source.php";
    private static int REQUEST_TIMEOUT = 3000;

    AdRequester() {
    }

    private static /* synthetic */ void appendParams(StringBuilder sb, String str, String str2) {
        if (str2 != null && str2.length() > 0) {
            try {
                sb.append("&").append(URLEncoder.encode(str, "UTF-8")).append("=").append(URLEncoder.encode(str2, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                Log.e("AdMob SDK", "UTF-8 encoding is not supported on this device.  Ad requests are impossible.", e);
            }
        }
    }

    static String buildParamString(Context context, String str, String str2) {
        StringBuilder sb = new StringBuilder();
        long currentTimeMillis = System.currentTimeMillis();
        sb.append("z").append("=").append(currentTimeMillis / 1000).append(".").append(currentTimeMillis % 1000);
        appendParams(sb, "rt", "0");
        String publisherId = AdManager.getPublisherId(context);
        if (publisherId == null) {
            throw new IllegalStateException("Publisher ID is not set!  To serve ads you must set your publisher ID assigned from www.admob.com.  Either add it to AndroidManifest.xml under the <application> tag or call AdManager.setPublisherId().");
        }
        appendParams(sb, "s", publisherId);
        appendParams(sb, "f", "html_no_js");
        appendParams(sb, "y", "text");
        appendParams(sb, "client_sdk", "1");
        appendParams(sb, "ex", "1");
        appendParams(sb, "v", AdManager.SDK_VERSION);
        appendParams(sb, "t", AdManager.getUserId(context));
        appendParams(sb, "d[coord]", AdManager.getCoordinatesAsString(context));
        appendParams(sb, "d[dob]", AdManager.getBirthdayAsString());
        appendParams(sb, "k", str);
        appendParams(sb, "search", str2);
        if (AdManager.isInTestMode()) {
            appendParams(sb, "m", "test");
        }
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00d6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.admob.android.ads.Ad requestAd(android.content.Context r13, java.lang.String r14, java.lang.String r15) {
        /*
            r12 = 0
            r1 = 0
            java.lang.String r0 = "android.permission.INTERNET"
            int r0 = r13.checkCallingOrSelfPermission(r0)
            r2 = -1
            if (r0 != r2) goto L_0x0010
            java.lang.String r0 = "Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />"
            com.admob.android.ads.AdManager.clientError(r0)
        L_0x0010:
            com.admob.android.ads.AdMobLocalizer.init(r13)
            long r6 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r3 = buildParamString(r13, r14, r15)     // Catch:{ Exception -> 0x0141 }
            java.net.URL r0 = new java.net.URL     // Catch:{ all -> 0x0136 }
            java.lang.String r2 = "http://r.admob.com/ad_source.php"
            r0.<init>(r2)     // Catch:{ all -> 0x0136 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ all -> 0x0136 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x0136 }
            java.lang.String r2 = "AdMob SDK"
            java.lang.String r4 = "Content-Length"
            java.lang.String r8 = "Content-Type"
            java.lang.String r9 = "User-Agent"
            r10 = 1
            java.lang.String r11 = "POST"
            r0.setRequestMethod(r11)     // Catch:{ all -> 0x0136 }
            r0.setDoOutput(r10)     // Catch:{ all -> 0x0136 }
            java.lang.String r10 = com.admob.android.ads.AdManager.getUserAgent()     // Catch:{ all -> 0x0136 }
            r0.setRequestProperty(r9, r10)     // Catch:{ all -> 0x0136 }
            java.lang.String r9 = "application/x-www-form-urlencoded"
            r0.setRequestProperty(r8, r9)     // Catch:{ all -> 0x0136 }
            int r8 = r3.length()     // Catch:{ all -> 0x0136 }
            java.lang.String r8 = java.lang.Integer.toString(r8)     // Catch:{ all -> 0x0136 }
            r0.setRequestProperty(r4, r8)     // Catch:{ all -> 0x0136 }
            int r4 = com.admob.android.ads.AdRequester.REQUEST_TIMEOUT     // Catch:{ all -> 0x0136 }
            r0.setConnectTimeout(r4)     // Catch:{ all -> 0x0136 }
            int r4 = com.admob.android.ads.AdRequester.REQUEST_TIMEOUT     // Catch:{ all -> 0x0136 }
            r0.setReadTimeout(r4)     // Catch:{ all -> 0x0136 }
            r4 = 3
            boolean r2 = android.util.Log.isLoggable(r2, r4)     // Catch:{ all -> 0x0136 }
            if (r2 == 0) goto L_0x007f
            java.lang.String r2 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0136 }
            r4.<init>()     // Catch:{ all -> 0x0136 }
            r8 = 0
            java.lang.String r9 = "Requesting an ad with POST parmams:  "
            java.lang.StringBuilder r4 = r4.insert(r8, r9)     // Catch:{ all -> 0x0136 }
            java.lang.StringBuilder r4 = r4.append(r3)     // Catch:{ all -> 0x0136 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0136 }
            android.util.Log.d(r2, r4)     // Catch:{ all -> 0x0136 }
        L_0x007f:
            java.io.BufferedWriter r2 = new java.io.BufferedWriter     // Catch:{ all -> 0x0136 }
            java.io.OutputStreamWriter r4 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x0136 }
            java.io.OutputStream r8 = r0.getOutputStream()     // Catch:{ all -> 0x0136 }
            r4.<init>(r8)     // Catch:{ all -> 0x0136 }
            r2.<init>(r4)     // Catch:{ all -> 0x0136 }
            r2.write(r3)     // Catch:{ all -> 0x013c }
            r2.close()     // Catch:{ all -> 0x013c }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ all -> 0x013c }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ all -> 0x013c }
            java.io.InputStream r8 = r0.getInputStream()     // Catch:{ all -> 0x013c }
            r4.<init>(r8)     // Catch:{ all -> 0x013c }
            r3.<init>(r4)     // Catch:{ all -> 0x013c }
            r4 = r3
        L_0x00a2:
            java.lang.String r4 = r4.readLine()     // Catch:{ all -> 0x00ad }
            if (r4 == 0) goto L_0x00fc
            r5.append(r4)     // Catch:{ all -> 0x00ad }
            r4 = r3
            goto L_0x00a2
        L_0x00ad:
            r0 = move-exception
            r4 = r2
        L_0x00af:
            if (r2 == 0) goto L_0x00b4
            r4.close()     // Catch:{ Exception -> 0x0144 }
        L_0x00b4:
            if (r3 == 0) goto L_0x00b9
            r3.close()     // Catch:{ Exception -> 0x0144 }
        L_0x00b9:
            throw r0     // Catch:{ Exception -> 0x00ba }
        L_0x00ba:
            r0 = move-exception
        L_0x00bb:
            java.lang.String r2 = "AdMob SDK"
            java.lang.String r3 = "Could not get ad from AdMob servers."
            android.util.Log.w(r2, r3, r0)
            r0 = r1
        L_0x00c3:
            java.lang.String r2 = r5.toString()
            if (r2 == 0) goto L_0x00cd
            com.admob.android.ads.Ad r1 = com.admob.android.ads.Ad.createAd(r13, r2, r0)
        L_0x00cd:
            java.lang.String r0 = "AdMob SDK"
            r2 = 4
            boolean r0 = android.util.Log.isLoggable(r0, r2)
            if (r0 == 0) goto L_0x00fb
            long r2 = java.lang.System.currentTimeMillis()
            long r2 = r2 - r6
            if (r1 != 0) goto L_0x0113
            java.lang.String r0 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Server replied that no ads are available ("
            java.lang.StringBuilder r4 = r4.insert(r12, r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r3 = "ms)"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.i(r0, r2)
        L_0x00fb:
            return r1
        L_0x00fc:
            java.lang.String r4 = "X-AdMob-Android-Category-Icon"
            java.lang.String r0 = r0.getHeaderField(r4)     // Catch:{ all -> 0x00ad }
            if (r0 != 0) goto L_0x0106
            java.lang.String r0 = "http://mm.admob.com/static/android/tiles/default.png"
        L_0x0106:
            if (r2 == 0) goto L_0x010b
            r2.close()     // Catch:{ Exception -> 0x0111 }
        L_0x010b:
            if (r3 == 0) goto L_0x00c3
            r3.close()     // Catch:{ Exception -> 0x0111 }
            goto L_0x00c3
        L_0x0111:
            r2 = move-exception
            goto L_0x00c3
        L_0x0113:
            java.lang.String r0 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Ad returned in "
            java.lang.StringBuilder r4 = r4.insert(r12, r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r3 = "ms:  "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r2 = r2.toString()
            android.util.Log.i(r0, r2)
            goto L_0x00fb
        L_0x0136:
            r0 = move-exception
            r2 = r1
            r3 = r1
            r4 = r1
            goto L_0x00af
        L_0x013c:
            r0 = move-exception
            r3 = r1
            r4 = r2
            goto L_0x00af
        L_0x0141:
            r0 = move-exception
            goto L_0x00bb
        L_0x0144:
            r2 = move-exception
            goto L_0x00b9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.AdRequester.requestAd(android.content.Context, java.lang.String, java.lang.String):com.admob.android.ads.Ad");
    }
}
