package defpackage;

/* renamed from: b  reason: default package */
public class b {
    private static final char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static String a(String str) {
        char[] charArray = str.toCharArray();
        int i = 0;
        int i2 = 0;
        while (i < charArray.length) {
            int i3 = 0;
            int i4 = 0;
            while (i3 < a.length) {
                charArray[i2] = (char) (charArray[i2] ^ a[i4]);
                if (((byte) charArray[i2]) > 32 && ((byte) charArray[i2]) < 125 && ((byte) charArray[i2]) != 92) {
                    break;
                }
                i3 = i4 + 1;
                i4 = i3;
            }
            i = i2 + 1;
            i2 = i;
        }
        return new String(charArray);
    }
}
