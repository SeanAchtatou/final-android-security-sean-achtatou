package defpackage;

import android.util.Xml;
import com.iPhand.FirstAid.R;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: g  reason: default package */
public class g {
    public static boolean b = true;
    byte[] a = null;
    private String c = "";
    private String d = "";
    private String e = "";
    private int f = 0;
    private String g = "";
    private HttpURLConnection h = null;
    private OutputStream i;
    private InputStream j;
    private final String k = "10.0.0.172:80";
    private final String l = "http://";
    private String m;
    private String n;

    public g() {
    }

    public g(String str) {
        a(str, "", "GET", 0);
    }

    public g(String str, String str2) {
        a(str, "", str2, 0);
    }

    public g(String str, String str2, int i2) {
        a(str, "", str2, i2);
    }

    private /* synthetic */ void a(String str, String str2, String str3, int i2) {
        try {
            this.c = str;
            this.d = str3;
            this.e = str2;
            this.f = i2;
            if (str != null) {
                String[] d2 = d(str);
                this.m = d2[0];
                this.n = d2[1];
                if (b) {
                    this.h = (HttpURLConnection) new URL(new StringBuilder().insert(0, "http://10.0.0.172:80").append(this.n).toString()).openConnection();
                    this.h.setRequestProperty("X-Online-Host", this.m);
                } else {
                    this.h = (HttpURLConnection) new URL(str).openConnection();
                }
                this.h.setConnectTimeout(50000);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private /* synthetic */ void b() {
        String headerField = this.h.getHeaderField("Result-Code");
        if (headerField == null) {
            headerField = "";
        }
        a.l = headerField;
        String headerField2 = this.h.getHeaderField("Content-Length");
        if (headerField2 == null) {
            headerField2 = "";
        }
        a.m = headerField2;
        String headerField3 = this.h.getHeaderField("Set-Cookie");
        if (headerField3 == null) {
            headerField3 = "";
        }
        a.k = headerField3;
        Iterator<Map.Entry<String, List<String>>> it = this.h.getHeaderFields().entrySet().iterator();
        a.j = new StringBuffer();
        while (it.hasNext()) {
            Map.Entry next = it.next();
            Object key = next.getKey();
            a.j.append(key + ":" + next.getValue() + "/n");
        }
    }

    private /* synthetic */ void d(byte[] bArr) {
        if (bArr != null && bArr.length != 0) {
            this.i = this.h.getOutputStream();
            this.i.write(bArr);
            this.i.flush();
            this.i.close();
        }
    }

    private /* synthetic */ String[] d(String str) {
        String[] strArr = new String[2];
        int length = "http://".length();
        if (str.toLowerCase().indexOf("http://") == -1) {
            return null;
        }
        int indexOf = str.indexOf("/", length);
        if (indexOf == -1) {
            strArr[0] = str.substring(length, str.length());
            strArr[1] = "/";
            return strArr;
        }
        strArr[0] = str.substring(length, indexOf);
        strArr[1] = str.substring(indexOf);
        return strArr;
    }

    private /* synthetic */ void e(byte[] bArr) {
        int i2 = 0;
        try {
            this.h.setDoOutput(true);
            this.h.setDoInput(true);
            this.h.setRequestMethod(this.d);
            HashMap hashMap = new HashMap();
            hashMap.put("Accept-Charset", "utf-8, iso-8859-1, utf-16, *;q=0.7");
            hashMap.put("Accept", "application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5");
            hashMap.put("Accept-Language", "zh-CN, en-US");
            hashMap.put("Cache-Control", "no-store");
            hashMap.put("Connection", "Keep-Alive");
            hashMap.put("Proxy-Connection", "Keep-Alive");
            if (this.g != null && !this.g.equals("")) {
                if (this.g.contains("#")) {
                    String[] split = this.g.split("#");
                    int length = split.length;
                    int i3 = 0;
                    while (i2 < length) {
                        String[] split2 = split[i3].split(":");
                        hashMap.put(split2[0], split2[1]);
                        i2 = i3 + 1;
                        i3 = i2;
                    }
                } else if (this.g.contains(":")) {
                    String[] split3 = this.g.split(":");
                    hashMap.put(split3[0], split3[1]);
                }
            }
            Iterator it = hashMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                this.h.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
            }
            if (!a.i.equals("")) {
                this.h.setRequestProperty("User-Agent", a.i);
            }
            if (a.k.equals("") || !this.c.contains("opcode")) {
            }
            if (this.e != null && !this.e.equals("")) {
                this.h.setRequestProperty("Action", this.e);
                this.h.setRequestProperty("ClientHash", a.h);
                this.h.setRequestProperty("Version", a.d);
                this.h.setRequestProperty("User-Id", a.f != null ? a.f : "");
                this.h.setRequestProperty("APIVersion", "1.1.0");
                this.h.setRequestProperty("x-up-calling-line-id", "13867222424");
            }
            if (this.d.equals("POST")) {
                if (bArr == null || bArr.length == 0) {
                    this.h.setRequestProperty("Content-length", "0");
                } else {
                    this.h.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    this.h.setRequestProperty("Content-length", bArr.length + "");
                }
            }
            Iterator<Map.Entry<String, List<String>>> it2 = this.h.getRequestProperties().entrySet().iterator();
            for (Iterator<Map.Entry<String, List<String>>> it3 = it2; it3.hasNext(); it3 = it2) {
                Map.Entry next = it2.next();
                next.getKey();
                next.getValue();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public InputStream a(byte[] bArr) {
        e(bArr);
        if (!(bArr == null || bArr.length == 0)) {
            d(bArr);
        }
        this.j = this.h.getInputStream();
        b();
        return this.j;
    }

    public void a() {
        if (this.h != null) {
            this.h.disconnect();
            this.h = null;
        }
        if (this.j != null) {
            this.j.close();
            this.j = null;
        }
        if (this.i != null) {
            this.i.close();
            this.i = null;
        }
    }

    public byte[] a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (this.f <= 0) {
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        } else {
            byte[] bArr2 = new byte[1024];
            int i2 = 1;
            while (true) {
                int i3 = i2;
                int read2 = inputStream.read(bArr2);
                if (read2 == -1 || this.f < i3) {
                    break;
                }
                byteArrayOutputStream.write(bArr2, 0, read2);
                i2 = i3 + 1;
            }
        }
        byteArrayOutputStream.flush();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        return byteArray;
    }

    public byte[] a(String str) {
        return b(str.getBytes());
    }

    public String b(String str) {
        return new String(b(str.getBytes()));
    }

    public byte[] b(byte[] bArr) {
        byte[] a2 = a(a(bArr));
        a();
        return a2;
    }

    public List c(byte[] bArr) {
        XmlPullParser xmlPullParser;
        ArrayList arrayList;
        e eVar;
        InputStream a2 = a(bArr);
        if (a.m == null || a.m.equals("") || a.m.equals("0")) {
            return null;
        }
        XmlPullParser newPullParser = Xml.newPullParser();
        newPullParser.setInput(a2, "UTF-8");
        int eventType = newPullParser.getEventType();
        int i2 = eventType;
        int i3 = eventType;
        ArrayList arrayList2 = null;
        e eVar2 = null;
        while (i2 != 1) {
            switch (i3) {
                case R.styleable.com_admob_android_ads_AdView_testing:
                    arrayList = new ArrayList();
                    eVar = eVar2;
                    xmlPullParser = newPullParser;
                    continue;
                    int next = xmlPullParser.next();
                    i3 = next;
                    int i4 = next;
                    eVar2 = eVar;
                    arrayList2 = arrayList;
                    i2 = i4;
                case R.styleable.com_admob_android_ads_AdView_textColor:
                    if ("request".equalsIgnoreCase(newPullParser.getName())) {
                        eVar2 = new e();
                    }
                    if (eVar2 != null) {
                        if ("url".equalsIgnoreCase(newPullParser.getName())) {
                            eVar2.b(newPullParser.nextText());
                        } else if ("method".equalsIgnoreCase(newPullParser.getName())) {
                            eVar2.c(newPullParser.nextText());
                        } else if ("data".equalsIgnoreCase(newPullParser.getName())) {
                            eVar2.d(newPullParser.nextText());
                        } else if ("isReturn".equalsIgnoreCase(newPullParser.getName())) {
                            eVar2.e(newPullParser.nextText());
                        } else if ("downSize".equalsIgnoreCase(newPullParser.getName())) {
                            eVar2.f(newPullParser.nextText());
                        } else if ("requestPropertys".equalsIgnoreCase(newPullParser.getName())) {
                            eVar2.a(newPullParser.nextText());
                        }
                    }
                    if ("opcode".equalsIgnoreCase(newPullParser.getName())) {
                        e.a = newPullParser.nextText();
                        arrayList = arrayList2;
                        eVar = eVar2;
                        xmlPullParser = newPullParser;
                        continue;
                    } else if ("returnUrls".equalsIgnoreCase(newPullParser.getName())) {
                        e.b = newPullParser.nextText();
                        arrayList = arrayList2;
                        eVar = eVar2;
                        xmlPullParser = newPullParser;
                    } else if ("returnMsgs".equalsIgnoreCase(newPullParser.getName())) {
                        e.c = newPullParser.nextText();
                        arrayList = arrayList2;
                        eVar = eVar2;
                        xmlPullParser = newPullParser;
                    } else if ("sequence".equalsIgnoreCase(newPullParser.getName())) {
                        e.d = newPullParser.nextText();
                        arrayList = arrayList2;
                        eVar = eVar2;
                        xmlPullParser = newPullParser;
                    } else if ("sync".equalsIgnoreCase(newPullParser.getName())) {
                        e.e = newPullParser.nextText();
                        arrayList = arrayList2;
                        eVar = eVar2;
                        xmlPullParser = newPullParser;
                    }
                    int next2 = xmlPullParser.next();
                    i3 = next2;
                    int i42 = next2;
                    eVar2 = eVar;
                    arrayList2 = arrayList;
                    i2 = i42;
                    break;
                case R.styleable.com_admob_android_ads_AdView_keywords:
                    if ("request".equalsIgnoreCase(newPullParser.getName())) {
                        arrayList2.add(eVar2);
                        xmlPullParser = newPullParser;
                        arrayList = arrayList2;
                        eVar = null;
                        continue;
                        int next22 = xmlPullParser.next();
                        i3 = next22;
                        int i422 = next22;
                        eVar2 = eVar;
                        arrayList2 = arrayList;
                        i2 = i422;
                    }
                    break;
            }
            arrayList = arrayList2;
            eVar = eVar2;
            xmlPullParser = newPullParser;
            int next222 = xmlPullParser.next();
            i3 = next222;
            int i4222 = next222;
            eVar2 = eVar;
            arrayList2 = arrayList;
            i2 = i4222;
        }
        return arrayList2;
    }

    public void c(String str) {
        this.g = str;
    }
}
