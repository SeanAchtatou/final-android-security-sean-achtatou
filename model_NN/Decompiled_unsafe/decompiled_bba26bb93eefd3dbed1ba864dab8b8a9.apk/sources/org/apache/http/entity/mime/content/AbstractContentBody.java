package org.apache.http.entity.mime.content;

import java.util.Collections;
import java.util.Map;
import org.apache.http.annotation.NotThreadSafe;
import org.apache.james.mime4j.message.Entity;
import org.apache.james.mime4j.message.SingleBody;

@NotThreadSafe
public abstract class AbstractContentBody extends SingleBody implements ContentBody {
    private final String mediaType;
    private final String mimeType;
    private Entity parent = null;
    private final String subType;

    public void dispose() {
        xdispose();
    }

    public Map getContentTypeParameters() {
        return xgetContentTypeParameters();
    }

    public String getMediaType() {
        return xgetMediaType();
    }

    public String getMimeType() {
        return xgetMimeType();
    }

    public Entity getParent() {
        return xgetParent();
    }

    public String getSubType() {
        return xgetSubType();
    }

    public void setParent(Entity entity) {
        xsetParent(entity);
    }

    public AbstractContentBody(String mimeType2) {
        if (mimeType2 == null) {
            throw new IllegalArgumentException("MIME type may not be null");
        }
        this.mimeType = mimeType2;
        int i = mimeType2.indexOf(47);
        if (i != -1) {
            this.mediaType = mimeType2.substring(0, i);
            this.subType = mimeType2.substring(i + 1);
            return;
        }
        this.mediaType = mimeType2;
        this.subType = null;
    }

    private Entity xgetParent() {
        return this.parent;
    }

    private void xsetParent(Entity parent2) {
        this.parent = parent2;
    }

    private String xgetMimeType() {
        return this.mimeType;
    }

    private String xgetMediaType() {
        return this.mediaType;
    }

    private String xgetSubType() {
        return this.subType;
    }

    private Map<String, String> xgetContentTypeParameters() {
        return Collections.emptyMap();
    }

    private void xdispose() {
    }
}
