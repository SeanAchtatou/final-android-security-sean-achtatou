package org.apache.http.entity.mime.content;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.http.annotation.NotThreadSafe;

@NotThreadSafe
public class InputStreamBody extends AbstractContentBody {
    private final String filename;
    private final InputStream in;

    public String getCharset() {
        return xgetCharset();
    }

    public long getContentLength() {
        return xgetContentLength();
    }

    public String getFilename() {
        return xgetFilename();
    }

    public InputStream getInputStream() {
        return xgetInputStream();
    }

    public String getTransferEncoding() {
        return xgetTransferEncoding();
    }

    public void writeTo(OutputStream outputStream) {
        xwriteTo(outputStream);
    }

    public void writeTo(OutputStream outputStream, int i) {
        xwriteTo(outputStream, i);
    }

    public InputStreamBody(InputStream in2, String mimeType, String filename2) {
        super(mimeType);
        if (in2 == null) {
            throw new IllegalArgumentException("Input stream may not be null");
        }
        this.in = in2;
        this.filename = filename2;
    }

    public InputStreamBody(InputStream in2, String filename2) {
        this(in2, "application/octet-stream", filename2);
    }

    private InputStream xgetInputStream() {
        return this.in;
    }

    @Deprecated
    private void xwriteTo(OutputStream out, int mode) throws IOException {
        writeTo(out);
    }

    private void xwriteTo(OutputStream out) throws IOException {
        if (out == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        try {
            byte[] tmp = new byte[4096];
            while (true) {
                int l = this.in.read(tmp);
                if (l != -1) {
                    out.write(tmp, 0, l);
                } else {
                    out.flush();
                    return;
                }
            }
        } finally {
            this.in.close();
        }
    }

    private String xgetTransferEncoding() {
        return "binary";
    }

    private String xgetCharset() {
        return null;
    }

    private long xgetContentLength() {
        return -1;
    }

    private String xgetFilename() {
        return this.filename;
    }
}
