package org.apache.http.entity.mime.content;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.annotation.NotThreadSafe;
import org.apache.james.mime4j.field.ContentTypeField;

@NotThreadSafe
public class StringBody extends AbstractContentBody {
    private final Charset charset;
    private final byte[] content;

    public String getCharset() {
        return xgetCharset();
    }

    public long getContentLength() {
        return xgetContentLength();
    }

    public Map getContentTypeParameters() {
        return xgetContentTypeParameters();
    }

    public String getFilename() {
        return xgetFilename();
    }

    public Reader getReader() {
        return xgetReader();
    }

    public String getTransferEncoding() {
        return xgetTransferEncoding();
    }

    public void writeTo(OutputStream outputStream) {
        xwriteTo(outputStream);
    }

    public void writeTo(OutputStream outputStream, int i) {
        xwriteTo(outputStream, i);
    }

    public StringBody(String text, String mimeType, Charset charset2) throws UnsupportedEncodingException {
        super(mimeType);
        if (text == null) {
            throw new IllegalArgumentException("Text may not be null");
        }
        charset2 = charset2 == null ? Charset.defaultCharset() : charset2;
        this.content = text.getBytes(charset2.name());
        this.charset = charset2;
    }

    public StringBody(String text, Charset charset2) throws UnsupportedEncodingException {
        this(text, ContentTypeField.TYPE_TEXT_PLAIN, charset2);
    }

    public StringBody(String text) throws UnsupportedEncodingException {
        this(text, ContentTypeField.TYPE_TEXT_PLAIN, null);
    }

    private Reader xgetReader() {
        return new InputStreamReader(new ByteArrayInputStream(this.content), this.charset);
    }

    @Deprecated
    private void xwriteTo(OutputStream out, int mode) throws IOException {
        writeTo(out);
    }

    private void xwriteTo(OutputStream out) throws IOException {
        if (out == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        InputStream in = new ByteArrayInputStream(this.content);
        byte[] tmp = new byte[4096];
        while (true) {
            int l = in.read(tmp);
            if (l != -1) {
                out.write(tmp, 0, l);
            } else {
                out.flush();
                return;
            }
        }
    }

    private String xgetTransferEncoding() {
        return "8bit";
    }

    private String xgetCharset() {
        return this.charset.name();
    }

    private Map<String, String> xgetContentTypeParameters() {
        Map<String, String> map = new HashMap<>();
        map.put(ContentTypeField.PARAM_CHARSET, this.charset.name());
        return map;
    }

    private long xgetContentLength() {
        return (long) this.content.length;
    }

    private String xgetFilename() {
        return null;
    }
}
