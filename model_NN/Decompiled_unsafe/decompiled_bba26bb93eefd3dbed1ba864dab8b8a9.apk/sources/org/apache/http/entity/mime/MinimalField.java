package org.apache.http.entity.mime;

import org.apache.http.annotation.Immutable;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.ContentUtil;

@Immutable
public class MinimalField implements Field {
    private final String name;
    private ByteSequence raw = null;
    private final String value;

    public String getBody() {
        return xgetBody();
    }

    public String getName() {
        return xgetName();
    }

    public ByteSequence getRaw() {
        return xgetRaw();
    }

    public String toString() {
        return xtoString();
    }

    MinimalField(String name2, String value2) {
        this.name = name2;
        this.value = value2;
    }

    private String xgetName() {
        return this.name;
    }

    private String xgetBody() {
        return this.value;
    }

    private ByteSequence xgetRaw() {
        if (this.raw == null) {
            this.raw = ContentUtil.encode(toString());
        }
        return this.raw;
    }

    private String xtoString() {
        return this.name + ": " + this.value;
    }
}
