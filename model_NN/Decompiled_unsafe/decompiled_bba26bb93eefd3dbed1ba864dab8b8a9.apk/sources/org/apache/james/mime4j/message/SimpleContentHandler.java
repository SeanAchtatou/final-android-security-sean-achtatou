package org.apache.james.mime4j.message;

import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.codec.Base64InputStream;
import org.apache.james.mime4j.codec.QuotedPrintableInputStream;
import org.apache.james.mime4j.descriptor.BodyDescriptor;
import org.apache.james.mime4j.field.AbstractField;
import org.apache.james.mime4j.parser.AbstractContentHandler;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.util.MimeUtil;

public abstract class SimpleContentHandler extends AbstractContentHandler {
    private Header currHeader;

    public final void body(BodyDescriptor bodyDescriptor, InputStream inputStream) {
        xbody(bodyDescriptor, inputStream);
    }

    public abstract void bodyDecoded(BodyDescriptor bodyDescriptor, InputStream inputStream) throws IOException;

    public final void endHeader() {
        xendHeader();
    }

    public final void field(Field field) {
        xfield(field);
    }

    public abstract void headers(Header header);

    public final void startHeader() {
        xstartHeader();
    }

    private final void xstartHeader() {
        this.currHeader = new Header();
    }

    private final void xfield(Field field) throws MimeException {
        this.currHeader.addField(AbstractField.parse(field.getRaw()));
    }

    private final void xendHeader() {
        Header tmp = this.currHeader;
        this.currHeader = null;
        headers(tmp);
    }

    private final void xbody(BodyDescriptor bd, InputStream is) throws IOException {
        if (MimeUtil.isBase64Encoding(bd.getTransferEncoding())) {
            bodyDecoded(bd, new Base64InputStream(is));
        } else if (MimeUtil.isQuotedPrintableEncoded(bd.getTransferEncoding())) {
            bodyDecoded(bd, new QuotedPrintableInputStream(is));
        } else {
            bodyDecoded(bd, is);
        }
    }
}
