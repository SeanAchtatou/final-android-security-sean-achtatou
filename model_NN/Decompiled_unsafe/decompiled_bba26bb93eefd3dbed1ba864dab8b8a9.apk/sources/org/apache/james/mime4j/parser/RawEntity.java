package org.apache.james.mime4j.parser;

import java.io.InputStream;
import org.apache.james.mime4j.descriptor.BodyDescriptor;

public class RawEntity implements EntityStateMachine {
    private int state = 2;
    private final InputStream stream;

    public EntityStateMachine advance() {
        return xadvance();
    }

    public BodyDescriptor getBodyDescriptor() {
        return xgetBodyDescriptor();
    }

    public InputStream getContentStream() {
        return xgetContentStream();
    }

    public Field getField() {
        return xgetField();
    }

    public String getFieldName() {
        return xgetFieldName();
    }

    public String getFieldValue() {
        return xgetFieldValue();
    }

    public int getState() {
        return xgetState();
    }

    public void setRecursionMode(int i) {
        xsetRecursionMode(i);
    }

    RawEntity(InputStream stream2) {
        this.stream = stream2;
    }

    private int xgetState() {
        return this.state;
    }

    private void xsetRecursionMode(int recursionMode) {
    }

    private EntityStateMachine xadvance() {
        this.state = -1;
        return null;
    }

    private InputStream xgetContentStream() {
        return this.stream;
    }

    private BodyDescriptor xgetBodyDescriptor() {
        return null;
    }

    private Field xgetField() {
        return null;
    }

    private String xgetFieldName() {
        return null;
    }

    private String xgetFieldValue() {
        return null;
    }
}
