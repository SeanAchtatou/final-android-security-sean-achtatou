package org.apache.james.mime4j.field.contentdisposition.parser;

public class Token {
    public int beginColumn;
    public int beginLine;
    public int endColumn;
    public int endLine;
    public String image;
    public int kind;
    public Token next;
    public Token specialToken;

    public static final Token newToken(int i) {
        return xnewToken(i);
    }

    public String toString() {
        return xtoString();
    }

    private String xtoString() {
        return this.image;
    }

    private static final Token xnewToken(int ofKind) {
        return new Token();
    }
}
