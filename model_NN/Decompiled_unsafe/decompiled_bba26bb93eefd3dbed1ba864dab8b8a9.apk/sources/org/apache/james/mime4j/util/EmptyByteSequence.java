package org.apache.james.mime4j.util;

final class EmptyByteSequence implements ByteSequence {
    private static final byte[] EMPTY_BYTES = new byte[0];

    public byte byteAt(int i) {
        return xbyteAt(i);
    }

    public int length() {
        return xlength();
    }

    public byte[] toByteArray() {
        return xtoByteArray();
    }

    EmptyByteSequence() {
    }

    private int xlength() {
        return 0;
    }

    private byte xbyteAt(int index) {
        throw new IndexOutOfBoundsException();
    }

    private byte[] xtoByteArray() {
        return EMPTY_BYTES;
    }
}
