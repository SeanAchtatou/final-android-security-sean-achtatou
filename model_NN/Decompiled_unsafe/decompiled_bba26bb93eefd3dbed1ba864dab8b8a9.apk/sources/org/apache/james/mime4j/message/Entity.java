package org.apache.james.mime4j.message;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.james.mime4j.field.ContentDispositionField;
import org.apache.james.mime4j.field.ContentTransferEncodingField;
import org.apache.james.mime4j.field.ContentTypeField;
import org.apache.james.mime4j.field.Fields;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.util.MimeUtil;

public abstract class Entity implements Disposable {
    private Body body = null;
    private Header header = null;
    private Entity parent = null;

    public void dispose() {
        xdispose();
    }

    public Body getBody() {
        return xgetBody();
    }

    public String getCharset() {
        return xgetCharset();
    }

    public String getContentTransferEncoding() {
        return xgetContentTransferEncoding();
    }

    public String getDispositionType() {
        return xgetDispositionType();
    }

    public String getFilename() {
        return xgetFilename();
    }

    public Header getHeader() {
        return xgetHeader();
    }

    public String getMimeType() {
        return xgetMimeType();
    }

    public Entity getParent() {
        return xgetParent();
    }

    public boolean isMimeType(String str) {
        return xisMimeType(str);
    }

    public boolean isMultipart() {
        return xisMultipart();
    }

    /* access modifiers changed from: package-private */
    public Field obtainField(String str) {
        return xobtainField(str);
    }

    /* access modifiers changed from: package-private */
    public Header obtainHeader() {
        return xobtainHeader();
    }

    public Body removeBody() {
        return xremoveBody();
    }

    public void setBody(Body body2) {
        xsetBody(body2);
    }

    public void setBody(Body body2, String str) {
        xsetBody(body2, str);
    }

    public void setBody(Body body2, String str, Map map) {
        xsetBody(body2, str, map);
    }

    public void setContentDisposition(String str) {
        xsetContentDisposition(str);
    }

    public void setContentDisposition(String str, String str2) {
        xsetContentDisposition(str, str2);
    }

    public void setContentDisposition(String str, String str2, long j) {
        xsetContentDisposition(str, str2, j);
    }

    public void setContentDisposition(String str, String str2, long j, Date date, Date date2, Date date3) {
        xsetContentDisposition(str, str2, j, date, date2, date3);
    }

    public void setContentTransferEncoding(String str) {
        xsetContentTransferEncoding(str);
    }

    public void setFilename(String str) {
        xsetFilename(str);
    }

    public void setHeader(Header header2) {
        xsetHeader(header2);
    }

    public void setMessage(Message message) {
        xsetMessage(message);
    }

    public void setMultipart(Multipart multipart) {
        xsetMultipart(multipart);
    }

    public void setMultipart(Multipart multipart, Map map) {
        xsetMultipart(multipart, map);
    }

    public void setParent(Entity entity) {
        xsetParent(entity);
    }

    public void setText(TextBody textBody) {
        xsetText(textBody);
    }

    public void setText(TextBody textBody, String str) {
        xsetText(textBody, str);
    }

    protected Entity() {
    }

    protected Entity(Entity other) {
        if (other.header != null) {
            this.header = new Header(other.header);
        }
        if (other.body != null) {
            setBody(BodyCopier.copy(other.body));
        }
    }

    private Entity xgetParent() {
        return this.parent;
    }

    private void xsetParent(Entity parent2) {
        this.parent = parent2;
    }

    private Header xgetHeader() {
        return this.header;
    }

    private void xsetHeader(Header header2) {
        this.header = header2;
    }

    private Body xgetBody() {
        return this.body;
    }

    private void xsetBody(Body body2) {
        if (this.body != null) {
            throw new IllegalStateException("body already set");
        }
        this.body = body2;
        body2.setParent(this);
    }

    private Body xremoveBody() {
        if (this.body == null) {
            return null;
        }
        Body body2 = this.body;
        this.body = null;
        body2.setParent(null);
        return body2;
    }

    private void xsetMessage(Message message) {
        setBody(message, ContentTypeField.TYPE_MESSAGE_RFC822, null);
    }

    private void xsetMultipart(Multipart multipart) {
        setBody(multipart, ContentTypeField.TYPE_MULTIPART_PREFIX + multipart.getSubType(), Collections.singletonMap(ContentTypeField.PARAM_BOUNDARY, MimeUtil.createUniqueBoundary()));
    }

    private void xsetMultipart(Multipart multipart, Map<String, String> parameters) {
        String mimeType = ContentTypeField.TYPE_MULTIPART_PREFIX + multipart.getSubType();
        if (!parameters.containsKey(ContentTypeField.PARAM_BOUNDARY)) {
            Map<String, String> parameters2 = new HashMap<>(parameters);
            parameters2.put(ContentTypeField.PARAM_BOUNDARY, MimeUtil.createUniqueBoundary());
            parameters = parameters2;
        }
        setBody(multipart, mimeType, parameters);
    }

    private void xsetText(TextBody textBody) {
        setText(textBody, "plain");
    }

    private void xsetText(TextBody textBody, String subtype) {
        String mimeType = "text/" + subtype;
        Map<String, String> parameters = null;
        String mimeCharset = textBody.getMimeCharset();
        if (mimeCharset != null && !mimeCharset.equalsIgnoreCase("us-ascii")) {
            parameters = Collections.singletonMap(ContentTypeField.PARAM_CHARSET, mimeCharset);
        }
        setBody(textBody, mimeType, parameters);
    }

    private void xsetBody(Body body2, String mimeType) {
        setBody(body2, mimeType, null);
    }

    private void xsetBody(Body body2, String mimeType, Map<String, String> parameters) {
        setBody(body2);
        obtainHeader().setField(Fields.contentType(mimeType, parameters));
    }

    private String xgetMimeType() {
        return ContentTypeField.getMimeType((ContentTypeField) getHeader().getField("Content-Type"), getParent() != null ? (ContentTypeField) getParent().getHeader().getField("Content-Type") : null);
    }

    private String xgetCharset() {
        return ContentTypeField.getCharset((ContentTypeField) getHeader().getField("Content-Type"));
    }

    private String xgetContentTransferEncoding() {
        return ContentTransferEncodingField.getEncoding((ContentTransferEncodingField) getHeader().getField("Content-Transfer-Encoding"));
    }

    private void xsetContentTransferEncoding(String contentTransferEncoding) {
        obtainHeader().setField(Fields.contentTransferEncoding(contentTransferEncoding));
    }

    private String xgetDispositionType() {
        ContentDispositionField field = (ContentDispositionField) obtainField("Content-Disposition");
        if (field == null) {
            return null;
        }
        return field.getDispositionType();
    }

    private void xsetContentDisposition(String dispositionType) {
        obtainHeader().setField(Fields.contentDisposition(dispositionType, null, -1, null, null, null));
    }

    private void xsetContentDisposition(String dispositionType, String filename) {
        obtainHeader().setField(Fields.contentDisposition(dispositionType, filename, -1, null, null, null));
    }

    private void xsetContentDisposition(String dispositionType, String filename, long size) {
        obtainHeader().setField(Fields.contentDisposition(dispositionType, filename, size, null, null, null));
    }

    private void xsetContentDisposition(String dispositionType, String filename, long size, Date creationDate, Date modificationDate, Date readDate) {
        obtainHeader().setField(Fields.contentDisposition(dispositionType, filename, size, creationDate, modificationDate, readDate));
    }

    private String xgetFilename() {
        ContentDispositionField field = (ContentDispositionField) obtainField("Content-Disposition");
        if (field == null) {
            return null;
        }
        return field.getFilename();
    }

    private void xsetFilename(String filename) {
        Header header2 = obtainHeader();
        ContentDispositionField field = (ContentDispositionField) header2.getField("Content-Disposition");
        if (field != null) {
            String dispositionType = field.getDispositionType();
            Map<String, String> parameters = new HashMap<>(field.getParameters());
            if (filename == null) {
                parameters.remove("filename");
            } else {
                parameters.put("filename", filename);
            }
            header2.setField(Fields.contentDisposition(dispositionType, parameters));
        } else if (filename != null) {
            header2.setField(Fields.contentDisposition(ContentDispositionField.DISPOSITION_TYPE_ATTACHMENT, filename, -1, null, null, null));
        }
    }

    private boolean xisMimeType(String type) {
        return getMimeType().equalsIgnoreCase(type);
    }

    private boolean xisMultipart() {
        ContentTypeField f = (ContentTypeField) getHeader().getField("Content-Type");
        return (f == null || f.getBoundary() == null || !getMimeType().startsWith(ContentTypeField.TYPE_MULTIPART_PREFIX)) ? false : true;
    }

    private void xdispose() {
        if (this.body != null) {
            this.body.dispose();
        }
    }

    private Header xobtainHeader() {
        if (this.header == null) {
            this.header = new Header();
        }
        return this.header;
    }

    private <F extends Field> F xobtainField(String fieldName) {
        Header header2 = getHeader();
        if (header2 == null) {
            return null;
        }
        return header2.getField(fieldName);
    }
}
