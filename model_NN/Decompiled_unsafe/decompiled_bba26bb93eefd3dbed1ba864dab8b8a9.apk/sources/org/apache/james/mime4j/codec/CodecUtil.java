package org.apache.james.mime4j.codec;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CodecUtil {
    static final int DEFAULT_ENCODING_BUFFER_SIZE = 1024;

    public static void copy(InputStream inputStream, OutputStream outputStream) {
        xcopy(inputStream, outputStream);
    }

    public static void encodeBase64(InputStream inputStream, OutputStream outputStream) {
        xencodeBase64(inputStream, outputStream);
    }

    public static void encodeQuotedPrintable(InputStream inputStream, OutputStream outputStream) {
        xencodeQuotedPrintable(inputStream, outputStream);
    }

    public static void encodeQuotedPrintableBinary(InputStream inputStream, OutputStream outputStream) {
        xencodeQuotedPrintableBinary(inputStream, outputStream);
    }

    public static OutputStream wrapBase64(OutputStream outputStream) {
        return xwrapBase64(outputStream);
    }

    public static OutputStream wrapQuotedPrintable(OutputStream outputStream, boolean z) {
        return xwrapQuotedPrintable(outputStream, z);
    }

    private static void xcopy(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[DEFAULT_ENCODING_BUFFER_SIZE];
        while (true) {
            int inputLength = in.read(buffer);
            if (-1 != inputLength) {
                out.write(buffer, 0, inputLength);
            } else {
                return;
            }
        }
    }

    private static void xencodeQuotedPrintableBinary(InputStream in, OutputStream out) throws IOException {
        new QuotedPrintableEncoder(DEFAULT_ENCODING_BUFFER_SIZE, true).encode(in, out);
    }

    private static void xencodeQuotedPrintable(InputStream in, OutputStream out) throws IOException {
        new QuotedPrintableEncoder(DEFAULT_ENCODING_BUFFER_SIZE, false).encode(in, out);
    }

    private static void xencodeBase64(InputStream in, OutputStream out) throws IOException {
        Base64OutputStream b64Out = new Base64OutputStream(out);
        copy(in, b64Out);
        b64Out.close();
    }

    private static OutputStream xwrapQuotedPrintable(OutputStream out, boolean binary) throws IOException {
        return new QuotedPrintableOutputStream(out, binary);
    }

    private static OutputStream xwrapBase64(OutputStream out) throws IOException {
        return new Base64OutputStream(out);
    }
}
