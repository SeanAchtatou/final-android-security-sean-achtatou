package org.apache.james.mime4j.field.address.parser;

public class ASTphrase extends SimpleNode {
    public Object jjtAccept(AddressListParserVisitor addressListParserVisitor, Object obj) {
        return xjjtAccept(addressListParserVisitor, obj);
    }

    public ASTphrase(int id) {
        super(id);
    }

    public ASTphrase(AddressListParser p, int id) {
        super(p, id);
    }

    private Object xjjtAccept(AddressListParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }
}
