package org.apache.james.mime4j.io;

import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.util.ByteArrayBuffer;

public class LineReaderInputStreamAdaptor extends LineReaderInputStream {
    private final LineReaderInputStream bis;
    private boolean eof;
    private final int maxLineLen;
    private boolean used;

    private int doReadLine(ByteArrayBuffer byteArrayBuffer) {
        return xdoReadLine(byteArrayBuffer);
    }

    public boolean eof() {
        return xeof();
    }

    public boolean isUsed() {
        return xisUsed();
    }

    public int read() {
        return xread();
    }

    public int read(byte[] bArr, int i, int i2) {
        return xread(bArr, i, i2);
    }

    public int readLine(ByteArrayBuffer byteArrayBuffer) {
        return xreadLine(byteArrayBuffer);
    }

    public String toString() {
        return xtoString();
    }

    public LineReaderInputStreamAdaptor(InputStream is, int maxLineLen2) {
        super(is);
        this.used = false;
        this.eof = false;
        if (is instanceof LineReaderInputStream) {
            this.bis = (LineReaderInputStream) is;
        } else {
            this.bis = null;
        }
        this.maxLineLen = maxLineLen2;
    }

    public LineReaderInputStreamAdaptor(InputStream is) {
        this(is, -1);
    }

    private int xread() throws IOException {
        int i = this.in.read();
        this.eof = i == -1;
        this.used = true;
        return i;
    }

    private int xread(byte[] b, int off, int len) throws IOException {
        int i = this.in.read(b, off, len);
        this.eof = i == -1;
        this.used = true;
        return i;
    }

    private int xreadLine(ByteArrayBuffer dst) throws IOException {
        int i;
        if (this.bis != null) {
            i = this.bis.readLine(dst);
        } else {
            i = doReadLine(dst);
        }
        this.eof = i == -1;
        this.used = true;
        return i;
    }

    private int xdoReadLine(ByteArrayBuffer dst) throws IOException {
        int ch;
        int total = 0;
        do {
            ch = this.in.read();
            if (ch == -1) {
                break;
            }
            dst.append(ch);
            total++;
            if (this.maxLineLen > 0 && dst.length() >= this.maxLineLen) {
                throw new MaxLineLimitException("Maximum line length limit exceeded");
            }
        } while (ch != 10);
        if (total == 0 && ch == -1) {
            return -1;
        }
        return total;
    }

    private boolean xeof() {
        return this.eof;
    }

    private boolean xisUsed() {
        return this.used;
    }

    private String xtoString() {
        return "[LineReaderInputStreamAdaptor: " + this.bis + "]";
    }
}
