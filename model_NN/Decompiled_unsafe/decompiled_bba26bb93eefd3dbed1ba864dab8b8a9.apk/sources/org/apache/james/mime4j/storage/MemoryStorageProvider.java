package org.apache.james.mime4j.storage;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.util.ByteArrayBuffer;

public class MemoryStorageProvider extends AbstractStorageProvider {
    public StorageOutputStream createStorageOutputStream() {
        return xcreateStorageOutputStream();
    }

    private StorageOutputStream xcreateStorageOutputStream() {
        return new MemoryStorageOutputStream();
    }

    private static final class MemoryStorageOutputStream extends StorageOutputStream {
        ByteArrayBuffer bab;

        /* access modifiers changed from: protected */
        public Storage toStorage0() {
            return xtoStorage0();
        }

        /* access modifiers changed from: protected */
        public void write0(byte[] bArr, int i, int i2) {
            xwrite0(bArr, i, i2);
        }

        private MemoryStorageOutputStream() {
            this.bab = new ByteArrayBuffer(1024);
        }

        private void xwrite0(byte[] buffer, int offset, int length) throws IOException {
            this.bab.append(buffer, offset, length);
        }

        private Storage xtoStorage0() throws IOException {
            return new MemoryStorage(this.bab.buffer(), this.bab.length());
        }
    }

    static final class MemoryStorage implements Storage {
        private final int count;
        private byte[] data;

        public void delete() {
            xdelete();
        }

        public InputStream getInputStream() {
            return xgetInputStream();
        }

        public MemoryStorage(byte[] data2, int count2) {
            this.data = data2;
            this.count = count2;
        }

        private InputStream xgetInputStream() throws IOException {
            if (this.data != null) {
                return new ByteArrayInputStream(this.data, 0, this.count);
            }
            throw new IllegalStateException("storage has been deleted");
        }

        private void xdelete() {
            this.data = null;
        }
    }
}
