package org.apache.james.mime4j.util;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;

public class ContentUtil {
    public static String decode(Charset charset, ByteSequence byteSequence) {
        return xdecode(charset, byteSequence);
    }

    public static String decode(Charset charset, ByteSequence byteSequence, int i, int i2) {
        return xdecode(charset, byteSequence, i, i2);
    }

    private static String decode(Charset charset, byte[] bArr, int i, int i2) {
        return xdecode(charset, bArr, i, i2);
    }

    public static String decode(ByteSequence byteSequence) {
        return xdecode(byteSequence);
    }

    public static String decode(ByteSequence byteSequence, int i, int i2) {
        return xdecode(byteSequence, i, i2);
    }

    public static ByteSequence encode(String str) {
        return xencode(str);
    }

    public static ByteSequence encode(Charset charset, String str) {
        return xencode(charset, str);
    }

    private ContentUtil() {
    }

    private static ByteSequence xencode(String string) {
        return encode(CharsetUtil.US_ASCII, string);
    }

    private static ByteSequence xencode(Charset charset, String string) {
        ByteBuffer encoded = charset.encode(CharBuffer.wrap(string));
        ByteArrayBuffer bab = new ByteArrayBuffer(encoded.remaining());
        bab.append(encoded.array(), encoded.position(), encoded.remaining());
        return bab;
    }

    private static String xdecode(ByteSequence byteSequence) {
        return decode(CharsetUtil.US_ASCII, byteSequence, 0, byteSequence.length());
    }

    private static String xdecode(Charset charset, ByteSequence byteSequence) {
        return decode(charset, byteSequence, 0, byteSequence.length());
    }

    private static String xdecode(ByteSequence byteSequence, int offset, int length) {
        return decode(CharsetUtil.US_ASCII, byteSequence, offset, length);
    }

    private static String xdecode(Charset charset, ByteSequence byteSequence, int offset, int length) {
        if (byteSequence instanceof ByteArrayBuffer) {
            return decode(charset, ((ByteArrayBuffer) byteSequence).buffer(), offset, length);
        }
        return decode(charset, byteSequence.toByteArray(), offset, length);
    }

    private static String xdecode(Charset charset, byte[] buffer, int offset, int length) {
        return charset.decode(ByteBuffer.wrap(buffer, offset, length)).toString();
    }
}
