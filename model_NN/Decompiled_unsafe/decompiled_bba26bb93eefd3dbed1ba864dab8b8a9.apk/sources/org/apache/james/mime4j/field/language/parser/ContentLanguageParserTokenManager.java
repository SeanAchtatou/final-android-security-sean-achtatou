package org.apache.james.mime4j.field.language.parser;

import java.io.IOException;
import java.io.PrintStream;
import org.apache.james.mime4j.field.datetime.parser.DateTimeParserConstants;

public class ContentLanguageParserTokenManager implements ContentLanguageParserConstants {
    static final long[] jjbitVec0 = {0, 0, -1, -1};
    public static final int[] jjnewLexState = {-1, -1, -1, -1, 1, 0, -1, 2, -1, -1, -1, -1, -1, 3, -1, -1, 0, -1, -1, -1, -1, -1, -1};
    static final int[] jjnextStates = new int[0];
    public static final String[] jjstrLiteralImages = {"", ",", "-", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ".", null, null};
    static final long[] jjtoMore = {65488};
    static final long[] jjtoSkip = {40};
    static final long[] jjtoSpecial = {8};
    static final long[] jjtoToken = {2031623};
    public static final String[] lexStateNames = {"DEFAULT", "INCOMMENT", "NESTED_COMMENT", "INQUOTEDSTRING"};
    int commentNest;
    protected char curChar;
    int curLexState;
    public PrintStream debugStream;
    int defaultLexState;
    StringBuffer image;
    protected SimpleCharStream input_stream;
    int jjimageLen;
    int jjmatchedKind;
    int jjmatchedPos;
    int jjnewStateCnt;
    int jjround;
    private final int[] jjrounds;
    private final int[] jjstateSet;
    int lengthOfMatch;

    private final void ReInitRounds() {
        xReInitRounds();
    }

    private final void jjAddStates(int i, int i2) {
        xjjAddStates(i, i2);
    }

    private final void jjCheckNAdd(int i) {
        xjjCheckNAdd(i);
    }

    private final void jjCheckNAddStates(int i) {
        xjjCheckNAddStates(i);
    }

    private final void jjCheckNAddStates(int i, int i2) {
        xjjCheckNAddStates(i, i2);
    }

    private final void jjCheckNAddTwoStates(int i, int i2) {
        xjjCheckNAddTwoStates(i, i2);
    }

    private final int jjMoveNfa_0(int i, int i2) {
        return xjjMoveNfa_0(i, i2);
    }

    private final int jjMoveNfa_1(int i, int i2) {
        return xjjMoveNfa_1(i, i2);
    }

    private final int jjMoveNfa_2(int i, int i2) {
        return xjjMoveNfa_2(i, i2);
    }

    private final int jjMoveNfa_3(int i, int i2) {
        return xjjMoveNfa_3(i, i2);
    }

    private final int jjMoveStringLiteralDfa0_0() {
        return xjjMoveStringLiteralDfa0_0();
    }

    private final int jjMoveStringLiteralDfa0_1() {
        return xjjMoveStringLiteralDfa0_1();
    }

    private final int jjMoveStringLiteralDfa0_2() {
        return xjjMoveStringLiteralDfa0_2();
    }

    private final int jjMoveStringLiteralDfa0_3() {
        return xjjMoveStringLiteralDfa0_3();
    }

    private final int jjStartNfaWithStates_0(int i, int i2, int i3) {
        return xjjStartNfaWithStates_0(i, i2, i3);
    }

    private final int jjStartNfaWithStates_1(int i, int i2, int i3) {
        return xjjStartNfaWithStates_1(i, i2, i3);
    }

    private final int jjStartNfaWithStates_2(int i, int i2, int i3) {
        return xjjStartNfaWithStates_2(i, i2, i3);
    }

    private final int jjStartNfaWithStates_3(int i, int i2, int i3) {
        return xjjStartNfaWithStates_3(i, i2, i3);
    }

    private final int jjStartNfa_0(int i, long j) {
        return xjjStartNfa_0(i, j);
    }

    private final int jjStartNfa_1(int i, long j) {
        return xjjStartNfa_1(i, j);
    }

    private final int jjStartNfa_2(int i, long j) {
        return xjjStartNfa_2(i, j);
    }

    private final int jjStartNfa_3(int i, long j) {
        return xjjStartNfa_3(i, j);
    }

    private final int jjStopAtPos(int i, int i2) {
        return xjjStopAtPos(i, i2);
    }

    private final int jjStopStringLiteralDfa_0(int i, long j) {
        return xjjStopStringLiteralDfa_0(i, j);
    }

    private final int jjStopStringLiteralDfa_1(int i, long j) {
        return xjjStopStringLiteralDfa_1(i, j);
    }

    private final int jjStopStringLiteralDfa_2(int i, long j) {
        return xjjStopStringLiteralDfa_2(i, j);
    }

    private final int jjStopStringLiteralDfa_3(int i, long j) {
        return xjjStopStringLiteralDfa_3(i, j);
    }

    /* access modifiers changed from: package-private */
    public void MoreLexicalActions() {
        xMoreLexicalActions();
    }

    public void ReInit(SimpleCharStream simpleCharStream) {
        xReInit(simpleCharStream);
    }

    public void ReInit(SimpleCharStream simpleCharStream, int i) {
        xReInit(simpleCharStream, i);
    }

    public void SwitchTo(int i) {
        xSwitchTo(i);
    }

    /* access modifiers changed from: package-private */
    public void TokenLexicalActions(Token token) {
        xTokenLexicalActions(token);
    }

    public Token getNextToken() {
        return xgetNextToken();
    }

    /* access modifiers changed from: protected */
    public Token jjFillToken() {
        return xjjFillToken();
    }

    public void setDebugStream(PrintStream printStream) {
        xsetDebugStream(printStream);
    }

    private void xsetDebugStream(PrintStream ds) {
        this.debugStream = ds;
    }

    private final int xjjStopStringLiteralDfa_0(int pos, long active0) {
        return -1;
    }

    private final int xjjStartNfa_0(int pos, long active0) {
        return jjMoveNfa_0(jjStopStringLiteralDfa_0(pos, active0), pos + 1);
    }

    private final int xjjStopAtPos(int pos, int kind) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        return pos + 1;
    }

    private final int xjjStartNfaWithStates_0(int pos, int kind, int state) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        try {
            this.curChar = this.input_stream.readChar();
            return jjMoveNfa_0(state, pos + 1);
        } catch (IOException e) {
            return pos + 1;
        }
    }

    private final int xjjMoveStringLiteralDfa0_0() {
        switch (this.curChar) {
            case '\"':
                return jjStopAtPos(0, 13);
            case '(':
                return jjStopAtPos(0, 4);
            case ',':
                return jjStopAtPos(0, 1);
            case '-':
                return jjStopAtPos(0, 2);
            case DateTimeParserConstants.DIGITS:
                return jjStopAtPos(0, 20);
            default:
                return jjMoveNfa_0(4, 0);
        }
    }

    private final void xjjCheckNAdd(int state) {
        if (this.jjrounds[state] != this.jjround) {
            int[] iArr = this.jjstateSet;
            int i = this.jjnewStateCnt;
            this.jjnewStateCnt = i + 1;
            iArr[i] = state;
            this.jjrounds[state] = this.jjround;
        }
    }

    private final void xjjAddStates(int start, int end) {
        while (true) {
            int[] iArr = this.jjstateSet;
            int i = this.jjnewStateCnt;
            this.jjnewStateCnt = i + 1;
            iArr[i] = jjnextStates[start];
            int start2 = start + 1;
            if (start != end) {
                start = start2;
            } else {
                return;
            }
        }
    }

    private final void xjjCheckNAddTwoStates(int state1, int state2) {
        jjCheckNAdd(state1);
        jjCheckNAdd(state2);
    }

    private final void xjjCheckNAddStates(int start, int end) {
        while (true) {
            jjCheckNAdd(jjnextStates[start]);
            int start2 = start + 1;
            if (start != end) {
                start = start2;
            } else {
                return;
            }
        }
    }

    private final void xjjCheckNAddStates(int start) {
        jjCheckNAdd(jjnextStates[start]);
        jjCheckNAdd(jjnextStates[start + 1]);
    }

    private final int xjjMoveNfa_0(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 4;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            int i2 = this.jjround + 1;
            this.jjround = i2;
            if (i2 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long l = 1 << this.curChar;
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((4294977024L & l) != 0) {
                                kind = 3;
                                jjCheckNAdd(0);
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if ((287948901175001088L & l) == 0) {
                                continue;
                            } else {
                                if (kind > 17) {
                                    kind = 17;
                                }
                                jjCheckNAdd(1);
                                continue;
                            }
                        case 3:
                            if ((287948901175001088L & l) == 0) {
                                continue;
                            } else {
                                if (kind > 19) {
                                    kind = 19;
                                }
                                jjCheckNAdd(3);
                                continue;
                            }
                        case 4:
                            if ((287948901175001088L & l) != 0) {
                                if (kind > 19) {
                                    kind = 19;
                                }
                                jjCheckNAdd(3);
                            } else if ((4294977024L & l) != 0) {
                                if (kind > 3) {
                                    kind = 3;
                                }
                                jjCheckNAdd(0);
                            }
                            if ((287948901175001088L & l) == 0) {
                                continue;
                            } else {
                                if (kind > 17) {
                                    kind = 17;
                                }
                                jjCheckNAdd(1);
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else if (this.curChar < 128) {
                long l2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 2:
                            if ((576460743847706622L & l2) == 0) {
                                continue;
                            } else {
                                if (kind > 18) {
                                    kind = 18;
                                }
                                jjCheckNAdd(2);
                                continue;
                            }
                        case 3:
                            if ((576460743847706622L & l2) == 0) {
                                continue;
                            } else {
                                if (kind > 19) {
                                    kind = 19;
                                }
                                jjCheckNAdd(3);
                                continue;
                            }
                        case 4:
                            if ((576460743847706622L & l2) != 0) {
                                if (kind > 19) {
                                    kind = 19;
                                }
                                jjCheckNAdd(3);
                            }
                            if ((576460743847706622L & l2) == 0) {
                                continue;
                            } else {
                                if (kind > 18) {
                                    kind = 18;
                                }
                                jjCheckNAdd(2);
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else {
                int i3 = (this.curChar & 255) >> 6;
                long j = 1 << (this.curChar & '?');
                do {
                    i--;
                    int i4 = this.jjstateSet[i];
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            curPos++;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 4 - startsAt;
            if (i != startsAt) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return curPos;
        }
    }

    private final int xjjStopStringLiteralDfa_1(int pos, long active0) {
        return -1;
    }

    private final int xjjStartNfa_1(int pos, long active0) {
        return jjMoveNfa_1(jjStopStringLiteralDfa_1(pos, active0), pos + 1);
    }

    private final int xjjStartNfaWithStates_1(int pos, int kind, int state) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        try {
            this.curChar = this.input_stream.readChar();
            return jjMoveNfa_1(state, pos + 1);
        } catch (IOException e) {
            return pos + 1;
        }
    }

    private final int xjjMoveStringLiteralDfa0_1() {
        switch (this.curChar) {
            case '(':
                return jjStopAtPos(0, 7);
            case ')':
                return jjStopAtPos(0, 5);
            default:
                return jjMoveNfa_1(0, 0);
        }
    }

    private final int xjjMoveNfa_1(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 3;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            int i2 = this.jjround + 1;
            this.jjround = i2;
            if (i2 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long j = 1 << this.curChar;
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if (kind > 8) {
                                kind = 8;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (kind > 6) {
                                kind = 6;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else if (this.curChar < 128) {
                long j2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if (kind > 8) {
                                kind = 8;
                            }
                            if (this.curChar == '\\') {
                                int[] iArr = this.jjstateSet;
                                int i3 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i3 + 1;
                                iArr[i3] = 1;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (kind > 6) {
                                kind = 6;
                                continue;
                            } else {
                                continue;
                            }
                        case 2:
                            if (kind > 8) {
                                kind = 8;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else {
                int i22 = (this.curChar & 255) >> 6;
                long l2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((jjbitVec0[i22] & l2) != 0 && kind > 8) {
                                kind = 8;
                                continue;
                            }
                        case 1:
                            if ((jjbitVec0[i22] & l2) != 0 && kind > 6) {
                                kind = 6;
                                continue;
                            }
                    }
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            curPos++;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 3 - startsAt;
            if (i != startsAt) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return curPos;
        }
    }

    private final int xjjStopStringLiteralDfa_3(int pos, long active0) {
        return -1;
    }

    private final int xjjStartNfa_3(int pos, long active0) {
        return jjMoveNfa_3(jjStopStringLiteralDfa_3(pos, active0), pos + 1);
    }

    private final int xjjStartNfaWithStates_3(int pos, int kind, int state) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        try {
            this.curChar = this.input_stream.readChar();
            return jjMoveNfa_3(state, pos + 1);
        } catch (IOException e) {
            return pos + 1;
        }
    }

    private final int xjjMoveStringLiteralDfa0_3() {
        switch (this.curChar) {
            case '\"':
                return jjStopAtPos(0, 16);
            default:
                return jjMoveNfa_3(0, 0);
        }
    }

    private final int xjjMoveNfa_3(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 3;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            int i2 = this.jjround + 1;
            this.jjround = i2;
            if (i2 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long l = 1 << this.curChar;
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                        case 2:
                            if ((-17179869185L & l) == 0) {
                                continue;
                            } else {
                                if (kind > 15) {
                                    kind = 15;
                                }
                                jjCheckNAdd(2);
                                continue;
                            }
                        case 1:
                            if (kind > 14) {
                                kind = 14;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else if (this.curChar < 128) {
                long l2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((-268435457 & l2) != 0) {
                                if (kind > 15) {
                                    kind = 15;
                                }
                                jjCheckNAdd(2);
                                continue;
                            } else if (this.curChar == '\\') {
                                int[] iArr = this.jjstateSet;
                                int i3 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i3 + 1;
                                iArr[i3] = 1;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (kind > 14) {
                                kind = 14;
                                continue;
                            } else {
                                continue;
                            }
                        case 2:
                            if ((-268435457 & l2) == 0) {
                                continue;
                            } else {
                                if (kind > 15) {
                                    kind = 15;
                                }
                                jjCheckNAdd(2);
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else {
                int i22 = (this.curChar & 255) >> 6;
                long l22 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                        case 2:
                            if ((jjbitVec0[i22] & l22) == 0) {
                                continue;
                            } else {
                                if (kind > 15) {
                                    kind = 15;
                                }
                                jjCheckNAdd(2);
                                continue;
                            }
                        case 1:
                            if ((jjbitVec0[i22] & l22) != 0 && kind > 14) {
                                kind = 14;
                                continue;
                            }
                    }
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            curPos++;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 3 - startsAt;
            if (i != startsAt) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return curPos;
        }
    }

    private final int xjjStopStringLiteralDfa_2(int pos, long active0) {
        return -1;
    }

    private final int xjjStartNfa_2(int pos, long active0) {
        return jjMoveNfa_2(jjStopStringLiteralDfa_2(pos, active0), pos + 1);
    }

    private final int xjjStartNfaWithStates_2(int pos, int kind, int state) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        try {
            this.curChar = this.input_stream.readChar();
            return jjMoveNfa_2(state, pos + 1);
        } catch (IOException e) {
            return pos + 1;
        }
    }

    private final int xjjMoveStringLiteralDfa0_2() {
        switch (this.curChar) {
            case '(':
                return jjStopAtPos(0, 10);
            case ')':
                return jjStopAtPos(0, 11);
            default:
                return jjMoveNfa_2(0, 0);
        }
    }

    private final int xjjMoveNfa_2(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 3;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            int i2 = this.jjround + 1;
            this.jjround = i2;
            if (i2 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long j = 1 << this.curChar;
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if (kind > 12) {
                                kind = 12;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (kind > 9) {
                                kind = 9;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else if (this.curChar < 128) {
                long j2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if (kind > 12) {
                                kind = 12;
                            }
                            if (this.curChar == '\\') {
                                int[] iArr = this.jjstateSet;
                                int i3 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i3 + 1;
                                iArr[i3] = 1;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (kind > 9) {
                                kind = 9;
                                continue;
                            } else {
                                continue;
                            }
                        case 2:
                            if (kind > 12) {
                                kind = 12;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else {
                int i22 = (this.curChar & 255) >> 6;
                long l2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((jjbitVec0[i22] & l2) != 0 && kind > 12) {
                                kind = 12;
                                continue;
                            }
                        case 1:
                            if ((jjbitVec0[i22] & l2) != 0 && kind > 9) {
                                kind = 9;
                                continue;
                            }
                    }
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            curPos++;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 3 - startsAt;
            if (i != startsAt) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return curPos;
        }
    }

    public ContentLanguageParserTokenManager(SimpleCharStream stream) {
        this.debugStream = System.out;
        this.jjrounds = new int[4];
        this.jjstateSet = new int[8];
        this.curLexState = 0;
        this.defaultLexState = 0;
        this.input_stream = stream;
    }

    public ContentLanguageParserTokenManager(SimpleCharStream stream, int lexState) {
        this(stream);
        SwitchTo(lexState);
    }

    private void xReInit(SimpleCharStream stream) {
        this.jjnewStateCnt = 0;
        this.jjmatchedPos = 0;
        this.curLexState = this.defaultLexState;
        this.input_stream = stream;
        ReInitRounds();
    }

    private final void xReInitRounds() {
        this.jjround = -2147483647;
        int i = 4;
        while (true) {
            int i2 = i;
            i = i2 - 1;
            if (i2 > 0) {
                this.jjrounds[i] = Integer.MIN_VALUE;
            } else {
                return;
            }
        }
    }

    private void xReInit(SimpleCharStream stream, int lexState) {
        ReInit(stream);
        SwitchTo(lexState);
    }

    private void xSwitchTo(int lexState) {
        if (lexState >= 4 || lexState < 0) {
            throw new TokenMgrError("Error: Ignoring invalid lexical state : " + lexState + ". State unchanged.", 2);
        }
        this.curLexState = lexState;
    }

    private Token xjjFillToken() {
        Token t = Token.newToken(this.jjmatchedKind);
        t.kind = this.jjmatchedKind;
        String im = jjstrLiteralImages[this.jjmatchedKind];
        if (im == null) {
            im = this.input_stream.GetImage();
        }
        t.image = im;
        t.beginLine = this.input_stream.getBeginLine();
        t.beginColumn = this.input_stream.getBeginColumn();
        t.endLine = this.input_stream.getEndLine();
        t.endColumn = this.input_stream.getEndColumn();
        return t;
    }

    private Token xgetNextToken() {
        Token specialToken = null;
        int curPos = 0;
        while (true) {
            try {
                this.curChar = this.input_stream.BeginToken();
                this.image = null;
                this.jjimageLen = 0;
                while (true) {
                    switch (this.curLexState) {
                        case 0:
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            this.jjmatchedPos = 0;
                            curPos = jjMoveStringLiteralDfa0_0();
                            break;
                        case 1:
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            this.jjmatchedPos = 0;
                            curPos = jjMoveStringLiteralDfa0_1();
                            break;
                        case 2:
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            this.jjmatchedPos = 0;
                            curPos = jjMoveStringLiteralDfa0_2();
                            break;
                        case 3:
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            this.jjmatchedPos = 0;
                            curPos = jjMoveStringLiteralDfa0_3();
                            break;
                    }
                    if (this.jjmatchedKind != Integer.MAX_VALUE) {
                        if (this.jjmatchedPos + 1 < curPos) {
                            this.input_stream.backup((curPos - this.jjmatchedPos) - 1);
                        }
                        if ((jjtoToken[this.jjmatchedKind >> 6] & (1 << (this.jjmatchedKind & 63))) != 0) {
                            Token matchedToken = jjFillToken();
                            matchedToken.specialToken = specialToken;
                            TokenLexicalActions(matchedToken);
                            if (jjnewLexState[this.jjmatchedKind] != -1) {
                                this.curLexState = jjnewLexState[this.jjmatchedKind];
                            }
                            return matchedToken;
                        } else if ((jjtoSkip[this.jjmatchedKind >> 6] & (1 << (this.jjmatchedKind & 63))) != 0) {
                            if ((jjtoSpecial[this.jjmatchedKind >> 6] & (1 << (this.jjmatchedKind & 63))) != 0) {
                                Token matchedToken2 = jjFillToken();
                                if (specialToken == null) {
                                    specialToken = matchedToken2;
                                } else {
                                    matchedToken2.specialToken = specialToken;
                                    specialToken.next = matchedToken2;
                                    specialToken = matchedToken2;
                                }
                            }
                            if (jjnewLexState[this.jjmatchedKind] != -1) {
                                this.curLexState = jjnewLexState[this.jjmatchedKind];
                            }
                        } else {
                            MoreLexicalActions();
                            if (jjnewLexState[this.jjmatchedKind] != -1) {
                                this.curLexState = jjnewLexState[this.jjmatchedKind];
                            }
                            curPos = 0;
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            try {
                                this.curChar = this.input_stream.readChar();
                            } catch (IOException e) {
                            }
                        }
                    }
                }
            } catch (IOException e2) {
                this.jjmatchedKind = 0;
                Token matchedToken3 = jjFillToken();
                matchedToken3.specialToken = specialToken;
                return matchedToken3;
            }
        }
        int error_line = this.input_stream.getEndLine();
        int error_column = this.input_stream.getEndColumn();
        String error_after = null;
        boolean EOFSeen = false;
        try {
            this.input_stream.readChar();
            this.input_stream.backup(1);
        } catch (IOException e3) {
            EOFSeen = true;
            error_after = curPos <= 1 ? "" : this.input_stream.GetImage();
            if (this.curChar == 10 || this.curChar == 13) {
                error_line++;
                error_column = 0;
            } else {
                error_column++;
            }
        }
        if (!EOFSeen) {
            this.input_stream.backup(1);
            if (curPos <= 1) {
                error_after = "";
            } else {
                error_after = this.input_stream.GetImage();
            }
        }
        throw new TokenMgrError(EOFSeen, this.curLexState, error_line, error_column, error_after, this.curChar, 0);
    }

    private void xMoreLexicalActions() {
        int i = this.jjimageLen;
        int i2 = this.jjmatchedPos + 1;
        this.lengthOfMatch = i2;
        this.jjimageLen = i + i2;
        switch (this.jjmatchedKind) {
            case 6:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                this.image.append(this.input_stream.GetSuffix(this.jjimageLen));
                this.jjimageLen = 0;
                this.image.deleteCharAt(this.image.length() - 2);
                return;
            case 7:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                this.image.append(this.input_stream.GetSuffix(this.jjimageLen));
                this.jjimageLen = 0;
                this.commentNest = 1;
                return;
            case 8:
            case 12:
            default:
                return;
            case 9:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                this.image.append(this.input_stream.GetSuffix(this.jjimageLen));
                this.jjimageLen = 0;
                this.image.deleteCharAt(this.image.length() - 2);
                return;
            case 10:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                this.image.append(this.input_stream.GetSuffix(this.jjimageLen));
                this.jjimageLen = 0;
                this.commentNest++;
                return;
            case 11:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                this.image.append(this.input_stream.GetSuffix(this.jjimageLen));
                this.jjimageLen = 0;
                this.commentNest--;
                if (this.commentNest == 0) {
                    SwitchTo(1);
                    return;
                }
                return;
            case 13:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                this.image.append(this.input_stream.GetSuffix(this.jjimageLen));
                this.jjimageLen = 0;
                this.image.deleteCharAt(this.image.length() - 1);
                return;
            case 14:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                this.image.append(this.input_stream.GetSuffix(this.jjimageLen));
                this.jjimageLen = 0;
                this.image.deleteCharAt(this.image.length() - 2);
                return;
        }
    }

    private void xTokenLexicalActions(Token matchedToken) {
        switch (this.jjmatchedKind) {
            case 16:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                StringBuffer stringBuffer = this.image;
                SimpleCharStream simpleCharStream = this.input_stream;
                int i = this.jjimageLen;
                int i2 = this.jjmatchedPos + 1;
                this.lengthOfMatch = i2;
                stringBuffer.append(simpleCharStream.GetSuffix(i + i2));
                matchedToken.image = this.image.substring(0, this.image.length() - 1);
                return;
            default:
                return;
        }
    }
}
