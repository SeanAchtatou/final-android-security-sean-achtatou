package org.apache.james.mime4j.field.address;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.james.mime4j.codec.DecoderUtil;
import org.apache.james.mime4j.field.address.parser.ASTaddr_spec;
import org.apache.james.mime4j.field.address.parser.ASTaddress;
import org.apache.james.mime4j.field.address.parser.ASTaddress_list;
import org.apache.james.mime4j.field.address.parser.ASTangle_addr;
import org.apache.james.mime4j.field.address.parser.ASTdomain;
import org.apache.james.mime4j.field.address.parser.ASTgroup_body;
import org.apache.james.mime4j.field.address.parser.ASTlocal_part;
import org.apache.james.mime4j.field.address.parser.ASTmailbox;
import org.apache.james.mime4j.field.address.parser.ASTname_addr;
import org.apache.james.mime4j.field.address.parser.ASTphrase;
import org.apache.james.mime4j.field.address.parser.ASTroute;
import org.apache.james.mime4j.field.address.parser.Node;
import org.apache.james.mime4j.field.address.parser.SimpleNode;
import org.apache.james.mime4j.field.address.parser.Token;

class Builder {
    private static Builder singleton = new Builder();

    private void addSpecials(StringBuilder sb, Token token) {
        xaddSpecials(sb, token);
    }

    private Mailbox buildAddrSpec(DomainList domainList, ASTaddr_spec aSTaddr_spec) {
        return xbuildAddrSpec(domainList, aSTaddr_spec);
    }

    private Mailbox buildAddrSpec(ASTaddr_spec aSTaddr_spec) {
        return xbuildAddrSpec(aSTaddr_spec);
    }

    private Mailbox buildAngleAddr(ASTangle_addr aSTangle_addr) {
        return xbuildAngleAddr(aSTangle_addr);
    }

    private MailboxList buildGroupBody(ASTgroup_body aSTgroup_body) {
        return xbuildGroupBody(aSTgroup_body);
    }

    private Mailbox buildNameAddr(ASTname_addr aSTname_addr) {
        return xbuildNameAddr(aSTname_addr);
    }

    private DomainList buildRoute(ASTroute aSTroute) {
        return xbuildRoute(aSTroute);
    }

    private String buildString(SimpleNode simpleNode, boolean z) {
        return xbuildString(simpleNode, z);
    }

    public static Builder getInstance() {
        return xgetInstance();
    }

    public Address buildAddress(ASTaddress aSTaddress) {
        return xbuildAddress(aSTaddress);
    }

    public AddressList buildAddressList(ASTaddress_list aSTaddress_list) {
        return xbuildAddressList(aSTaddress_list);
    }

    public Mailbox buildMailbox(ASTmailbox aSTmailbox) {
        return xbuildMailbox(aSTmailbox);
    }

    Builder() {
    }

    private static Builder xgetInstance() {
        return singleton;
    }

    private AddressList xbuildAddressList(ASTaddress_list node) {
        List<Address> list = new ArrayList<>();
        for (int i = 0; i < node.jjtGetNumChildren(); i++) {
            list.add(buildAddress((ASTaddress) node.jjtGetChild(i)));
        }
        return new AddressList(list, true);
    }

    private Address xbuildAddress(ASTaddress node) {
        ChildNodeIterator it = new ChildNodeIterator(node);
        Node n = it.xnext();
        if (n instanceof ASTaddr_spec) {
            return buildAddrSpec((ASTaddr_spec) n);
        }
        if (n instanceof ASTangle_addr) {
            return buildAngleAddr((ASTangle_addr) n);
        }
        if (n instanceof ASTphrase) {
            String name = buildString((ASTphrase) n, false);
            Node n2 = it.xnext();
            if (n2 instanceof ASTgroup_body) {
                return new Group(name, buildGroupBody((ASTgroup_body) n2));
            }
            if (n2 instanceof ASTangle_addr) {
                return new Mailbox(DecoderUtil.decodeEncodedWords(name), buildAngleAddr((ASTangle_addr) n2));
            }
            throw new IllegalStateException();
        }
        throw new IllegalStateException();
    }

    private MailboxList xbuildGroupBody(ASTgroup_body node) {
        List<Mailbox> results = new ArrayList<>();
        ChildNodeIterator it = new ChildNodeIterator(node);
        while (it.hasNext()) {
            Node n = it.xnext();
            if (n instanceof ASTmailbox) {
                results.add(buildMailbox((ASTmailbox) n));
            } else {
                throw new IllegalStateException();
            }
        }
        return new MailboxList(results, true);
    }

    private Mailbox xbuildMailbox(ASTmailbox node) {
        Node n = new ChildNodeIterator(node).xnext();
        if (n instanceof ASTaddr_spec) {
            return buildAddrSpec((ASTaddr_spec) n);
        }
        if (n instanceof ASTangle_addr) {
            return buildAngleAddr((ASTangle_addr) n);
        }
        if (n instanceof ASTname_addr) {
            return buildNameAddr((ASTname_addr) n);
        }
        throw new IllegalStateException();
    }

    private Mailbox xbuildNameAddr(ASTname_addr node) {
        ChildNodeIterator it = new ChildNodeIterator(node);
        Node n = it.xnext();
        if (n instanceof ASTphrase) {
            String name = buildString((ASTphrase) n, false);
            Node n2 = it.xnext();
            if (n2 instanceof ASTangle_addr) {
                return new Mailbox(DecoderUtil.decodeEncodedWords(name), buildAngleAddr((ASTangle_addr) n2));
            }
            throw new IllegalStateException();
        }
        throw new IllegalStateException();
    }

    private Mailbox xbuildAngleAddr(ASTangle_addr node) {
        ChildNodeIterator it = new ChildNodeIterator(node);
        DomainList route = null;
        Node n = it.xnext();
        if (n instanceof ASTroute) {
            route = buildRoute((ASTroute) n);
            n = it.xnext();
        } else if (!(n instanceof ASTaddr_spec)) {
            throw new IllegalStateException();
        }
        if (n instanceof ASTaddr_spec) {
            return buildAddrSpec(route, (ASTaddr_spec) n);
        }
        throw new IllegalStateException();
    }

    private DomainList xbuildRoute(ASTroute node) {
        List<String> results = new ArrayList<>(node.jjtGetNumChildren());
        ChildNodeIterator it = new ChildNodeIterator(node);
        while (it.hasNext()) {
            Node n = it.xnext();
            if (n instanceof ASTdomain) {
                results.add(buildString((ASTdomain) n, true));
            } else {
                throw new IllegalStateException();
            }
        }
        return new DomainList(results, true);
    }

    private Mailbox xbuildAddrSpec(ASTaddr_spec node) {
        return buildAddrSpec(null, node);
    }

    private Mailbox xbuildAddrSpec(DomainList route, ASTaddr_spec node) {
        ChildNodeIterator it = new ChildNodeIterator(node);
        return new Mailbox(route, buildString((ASTlocal_part) it.xnext(), true), buildString((ASTdomain) it.xnext(), true));
    }

    private String xbuildString(SimpleNode node, boolean stripSpaces) {
        Token head = node.firstToken;
        Token tail = node.lastToken;
        StringBuilder out = new StringBuilder();
        while (head != tail) {
            out.append(head.image);
            head = head.next;
            if (!stripSpaces) {
                addSpecials(out, head.specialToken);
            }
        }
        out.append(tail.image);
        return out.toString();
    }

    private void xaddSpecials(StringBuilder out, Token specialToken) {
        if (specialToken != null) {
            addSpecials(out, specialToken.specialToken);
            out.append(specialToken.image);
        }
    }

    private static class ChildNodeIterator implements Iterator<Node> {
        private int index = 0;
        private int len;
        private SimpleNode simpleNode;

        public boolean hasNext() {
            return xhasNext();
        }

        /* renamed from: next */
        public Node xnext() {
            return xnext();
        }

        public void remove() {
            xremove();
        }

        public ChildNodeIterator(SimpleNode simpleNode2) {
            this.simpleNode = simpleNode2;
            this.len = simpleNode2.jjtGetNumChildren();
        }

        private void xremove() {
            throw new UnsupportedOperationException();
        }

        private boolean xhasNext() {
            return this.index < this.len;
        }

        private Node xnext() {
            SimpleNode simpleNode2 = this.simpleNode;
            int i = this.index;
            this.index = i + 1;
            return simpleNode2.jjtGetChild(i);
        }
    }
}
