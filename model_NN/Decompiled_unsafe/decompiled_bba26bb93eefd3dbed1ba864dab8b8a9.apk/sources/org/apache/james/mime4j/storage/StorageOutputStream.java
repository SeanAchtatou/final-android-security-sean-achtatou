package org.apache.james.mime4j.storage;

import java.io.IOException;
import java.io.OutputStream;

public abstract class StorageOutputStream extends OutputStream {
    private boolean closed;
    private byte[] singleByte;
    private boolean usedUp;

    public void close() {
        xclose();
    }

    public final Storage toStorage() {
        return xtoStorage();
    }

    /* access modifiers changed from: protected */
    public abstract Storage toStorage0() throws IOException;

    public final void write(int i) {
        xwrite(i);
    }

    public final void write(byte[] bArr) {
        xwrite(bArr);
    }

    public final void write(byte[] bArr, int i, int i2) {
        xwrite(bArr, i, i2);
    }

    /* access modifiers changed from: protected */
    public abstract void write0(byte[] bArr, int i, int i2) throws IOException;

    protected StorageOutputStream() {
    }

    private final Storage xtoStorage() throws IOException {
        if (this.usedUp) {
            throw new IllegalStateException("toStorage may be invoked only once");
        }
        if (!this.closed) {
            close();
        }
        this.usedUp = true;
        return toStorage0();
    }

    private final void xwrite(int b) throws IOException {
        if (this.closed) {
            throw new IOException("StorageOutputStream has been closed");
        }
        if (this.singleByte == null) {
            this.singleByte = new byte[1];
        }
        this.singleByte[0] = (byte) b;
        write0(this.singleByte, 0, 1);
    }

    private final void xwrite(byte[] buffer) throws IOException {
        if (this.closed) {
            throw new IOException("StorageOutputStream has been closed");
        } else if (buffer == null) {
            throw new NullPointerException();
        } else if (buffer.length != 0) {
            write0(buffer, 0, buffer.length);
        }
    }

    private final void xwrite(byte[] buffer, int offset, int length) throws IOException {
        if (this.closed) {
            throw new IOException("StorageOutputStream has been closed");
        } else if (buffer == null) {
            throw new NullPointerException();
        } else if (offset < 0 || length < 0 || offset + length > buffer.length) {
            throw new IndexOutOfBoundsException();
        } else if (length != 0) {
            write0(buffer, offset, length);
        }
    }

    private void xclose() throws IOException {
        this.closed = true;
    }
}
