package org.apache.james.mime4j.field.address.parser;

public class ASTroute extends SimpleNode {
    public Object jjtAccept(AddressListParserVisitor addressListParserVisitor, Object obj) {
        return xjjtAccept(addressListParserVisitor, obj);
    }

    public ASTroute(int id) {
        super(id);
    }

    public ASTroute(AddressListParser p, int id) {
        super(p, id);
    }

    private Object xjjtAccept(AddressListParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }
}
