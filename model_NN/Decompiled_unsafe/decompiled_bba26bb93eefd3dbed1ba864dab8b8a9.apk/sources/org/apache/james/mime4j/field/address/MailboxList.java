package org.apache.james.mime4j.field.address;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MailboxList extends AbstractList<Mailbox> implements Serializable {
    private static final long serialVersionUID = 1;
    private final List<Mailbox> mailboxes;

    /* renamed from: get */
    public Mailbox xget(int i) {
        return xget(i);
    }

    public void print() {
        xprint();
    }

    public int size() {
        return xsize();
    }

    public MailboxList(List<Mailbox> mailboxes2, boolean dontCopy) {
        if (mailboxes2 != null) {
            this.mailboxes = !dontCopy ? new ArrayList<>(mailboxes2) : mailboxes2;
        } else {
            this.mailboxes = Collections.emptyList();
        }
    }

    private int xsize() {
        return this.mailboxes.size();
    }

    private Mailbox xget(int index) {
        return this.mailboxes.get(index);
    }

    private void xprint() {
        for (int i = 0; i < size(); i++) {
            System.out.println(xget(i).toString());
        }
    }
}
