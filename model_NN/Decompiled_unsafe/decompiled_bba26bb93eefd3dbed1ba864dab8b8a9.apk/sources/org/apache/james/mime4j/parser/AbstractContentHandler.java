package org.apache.james.mime4j.parser;

import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.descriptor.BodyDescriptor;

public abstract class AbstractContentHandler implements ContentHandler {
    public void body(BodyDescriptor bodyDescriptor, InputStream inputStream) {
        xbody(bodyDescriptor, inputStream);
    }

    public void endBodyPart() {
        xendBodyPart();
    }

    public void endHeader() {
        xendHeader();
    }

    public void endMessage() {
        xendMessage();
    }

    public void endMultipart() {
        xendMultipart();
    }

    public void epilogue(InputStream inputStream) {
        xepilogue(inputStream);
    }

    public void field(Field field) {
        xfield(field);
    }

    public void preamble(InputStream inputStream) {
        xpreamble(inputStream);
    }

    public void raw(InputStream inputStream) {
        xraw(inputStream);
    }

    public void startBodyPart() {
        xstartBodyPart();
    }

    public void startHeader() {
        xstartHeader();
    }

    public void startMessage() {
        xstartMessage();
    }

    public void startMultipart(BodyDescriptor bodyDescriptor) {
        xstartMultipart(bodyDescriptor);
    }

    private void xendMultipart() throws MimeException {
    }

    private void xstartMultipart(BodyDescriptor bd) throws MimeException {
    }

    private void xbody(BodyDescriptor bd, InputStream is) throws MimeException, IOException {
    }

    private void xendBodyPart() throws MimeException {
    }

    private void xendHeader() throws MimeException {
    }

    private void xendMessage() throws MimeException {
    }

    private void xepilogue(InputStream is) throws MimeException, IOException {
    }

    private void xfield(Field field) throws MimeException {
    }

    private void xpreamble(InputStream is) throws MimeException, IOException {
    }

    private void xstartBodyPart() throws MimeException {
    }

    private void xstartHeader() throws MimeException {
    }

    private void xstartMessage() throws MimeException {
    }

    private void xraw(InputStream is) throws MimeException, IOException {
    }
}
