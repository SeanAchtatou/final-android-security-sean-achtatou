package org.apache.james.mime4j.field.address;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.apache.james.mime4j.codec.EncoderUtil;

public class Group extends Address {
    private static final long serialVersionUID = 1;
    private final MailboxList mailboxList;
    private final String name;

    public static Group parse(String str) {
        return xparse(str);
    }

    /* access modifiers changed from: protected */
    public void doAddMailboxesTo(List list) {
        xdoAddMailboxesTo(list);
    }

    public String getDisplayString(boolean z) {
        return xgetDisplayString(z);
    }

    public String getEncodedString() {
        return xgetEncodedString();
    }

    public MailboxList getMailboxes() {
        return xgetMailboxes();
    }

    public String getName() {
        return xgetName();
    }

    public Group(String name2, Mailbox... mailboxes) {
        this(name2, new MailboxList(Arrays.asList(mailboxes), true));
    }

    public Group(String name2, Collection<Mailbox> mailboxes) {
        this(name2, new MailboxList(new ArrayList(mailboxes), true));
    }

    public Group(String name2, MailboxList mailboxes) {
        if (name2 == null) {
            throw new IllegalArgumentException();
        } else if (mailboxes == null) {
            throw new IllegalArgumentException();
        } else {
            this.name = name2;
            this.mailboxList = mailboxes;
        }
    }

    private static Group xparse(String rawGroupString) {
        Address address = Address.parse(rawGroupString);
        if (address instanceof Group) {
            return (Group) address;
        }
        throw new IllegalArgumentException("Not a group address");
    }

    private String xgetName() {
        return this.name;
    }

    private MailboxList xgetMailboxes() {
        return this.mailboxList;
    }

    private String xgetDisplayString(boolean includeRoute) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.name);
        sb.append(':');
        boolean first = true;
        Iterator i$ = this.mailboxList.iterator();
        while (i$.hasNext()) {
            Mailbox mailbox = (Mailbox) i$.next();
            if (first) {
                first = false;
            } else {
                sb.append(',');
            }
            sb.append(' ');
            sb.append(mailbox.getDisplayString(includeRoute));
        }
        sb.append(";");
        return sb.toString();
    }

    private String xgetEncodedString() {
        StringBuilder sb = new StringBuilder();
        sb.append(EncoderUtil.encodeAddressDisplayName(this.name));
        sb.append(':');
        boolean first = true;
        Iterator i$ = this.mailboxList.iterator();
        while (i$.hasNext()) {
            Mailbox mailbox = (Mailbox) i$.next();
            if (first) {
                first = false;
            } else {
                sb.append(',');
            }
            sb.append(' ');
            sb.append(mailbox.getEncodedString());
        }
        sb.append(';');
        return sb.toString();
    }

    private void xdoAddMailboxesTo(List<Mailbox> results) {
        Iterator i$ = this.mailboxList.iterator();
        while (i$.hasNext()) {
            results.add((Mailbox) i$.next());
        }
    }
}
