package org.apache.james.mime4j.field.datetime;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DateTime {
    private final Date date;
    private final int day;
    private final int hour;
    private final int minute;
    private final int month;
    private final int second;
    private final int timeZone;
    private final int year;

    public static Date convertToDate(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        return xconvertToDate(i, i2, i3, i4, i5, i6, i7);
    }

    private int convertToYear(String str) {
        return xconvertToYear(str);
    }

    public boolean equals(Object obj) {
        return xequals(obj);
    }

    public Date getDate() {
        return xgetDate();
    }

    public int getDay() {
        return xgetDay();
    }

    public int getHour() {
        return xgetHour();
    }

    public int getMinute() {
        return xgetMinute();
    }

    public int getMonth() {
        return xgetMonth();
    }

    public int getSecond() {
        return xgetSecond();
    }

    public int getTimeZone() {
        return xgetTimeZone();
    }

    public int getYear() {
        return xgetYear();
    }

    public int hashCode() {
        return xhashCode();
    }

    public void print() {
        xprint();
    }

    public String toString() {
        return xtoString();
    }

    public DateTime(String yearString, int month2, int day2, int hour2, int minute2, int second2, int timeZone2) {
        this.year = convertToYear(yearString);
        this.date = convertToDate(this.year, month2, day2, hour2, minute2, second2, timeZone2);
        this.month = month2;
        this.day = day2;
        this.hour = hour2;
        this.minute = minute2;
        this.second = second2;
        this.timeZone = timeZone2;
    }

    private int xconvertToYear(String yearString) {
        int year2 = Integer.parseInt(yearString);
        switch (yearString.length()) {
            case 1:
            case 2:
                if (year2 < 0 || year2 >= 50) {
                    return year2 + 1900;
                }
                return year2 + 2000;
            case 3:
                return year2 + 1900;
            default:
                return year2;
        }
    }

    private static Date xconvertToDate(int year2, int month2, int day2, int hour2, int minute2, int second2, int timeZone2) {
        Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT+0"));
        c.set(year2, month2 - 1, day2, hour2, minute2, second2);
        c.set(14, 0);
        if (timeZone2 != Integer.MIN_VALUE) {
            c.add(12, (((timeZone2 / 100) * 60) + (timeZone2 % 100)) * -1);
        }
        return c.getTime();
    }

    private Date xgetDate() {
        return this.date;
    }

    private int xgetYear() {
        return this.year;
    }

    private int xgetMonth() {
        return this.month;
    }

    private int xgetDay() {
        return this.day;
    }

    private int xgetHour() {
        return this.hour;
    }

    private int xgetMinute() {
        return this.minute;
    }

    private int xgetSecond() {
        return this.second;
    }

    private int xgetTimeZone() {
        return this.timeZone;
    }

    private void xprint() {
        System.out.println(toString());
    }

    private String xtoString() {
        return getYear() + " " + getMonth() + " " + getDay() + "; " + getHour() + " " + getMinute() + " " + getSecond() + " " + getTimeZone();
    }

    private int xhashCode() {
        return (((((((((((((((this.date == null ? 0 : this.date.hashCode()) + 31) * 31) + this.day) * 31) + this.hour) * 31) + this.minute) * 31) + this.month) * 31) + this.second) * 31) + this.timeZone) * 31) + this.year;
    }

    private boolean xequals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DateTime other = (DateTime) obj;
        if (this.date == null) {
            if (other.date != null) {
                return false;
            }
        } else if (!this.date.equals(other.date)) {
            return false;
        }
        if (this.day != other.day) {
            return false;
        }
        if (this.hour != other.hour) {
            return false;
        }
        if (this.minute != other.minute) {
            return false;
        }
        if (this.month != other.month) {
            return false;
        }
        if (this.second != other.second) {
            return false;
        }
        if (this.timeZone != other.timeZone) {
            return false;
        }
        if (this.year != other.year) {
            return false;
        }
        return true;
    }
}
