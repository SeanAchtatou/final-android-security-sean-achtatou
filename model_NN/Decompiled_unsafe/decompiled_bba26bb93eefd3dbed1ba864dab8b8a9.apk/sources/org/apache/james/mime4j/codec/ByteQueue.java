package org.apache.james.mime4j.codec;

import java.util.Iterator;

public class ByteQueue implements Iterable<Byte> {
    private UnboundedFifoByteBuffer buf;
    private int initialCapacity;

    public void clear() {
        xclear();
    }

    public int count() {
        return xcount();
    }

    public byte dequeue() {
        return xdequeue();
    }

    public void enqueue(byte b) {
        xenqueue(b);
    }

    public Iterator iterator() {
        return xiterator();
    }

    public ByteQueue() {
        this.initialCapacity = -1;
        this.buf = new UnboundedFifoByteBuffer();
    }

    public ByteQueue(int initialCapacity2) {
        this.initialCapacity = -1;
        this.buf = new UnboundedFifoByteBuffer(initialCapacity2);
        this.initialCapacity = initialCapacity2;
    }

    private void xenqueue(byte b) {
        this.buf.add(b);
    }

    private byte xdequeue() {
        return this.buf.remove();
    }

    private int xcount() {
        return this.buf.size();
    }

    private void xclear() {
        if (this.initialCapacity != -1) {
            this.buf = new UnboundedFifoByteBuffer(this.initialCapacity);
        } else {
            this.buf = new UnboundedFifoByteBuffer();
        }
    }

    private Iterator<Byte> xiterator() {
        return this.buf.iterator();
    }
}
