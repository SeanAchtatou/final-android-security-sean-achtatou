package org.apache.james.mime4j.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class LineNumberInputStream extends FilterInputStream implements LineNumberSource {
    private int lineNumber = 1;

    public int getLineNumber() {
        return xgetLineNumber();
    }

    public int read() {
        return xread();
    }

    public int read(byte[] bArr, int i, int i2) {
        return xread(bArr, i, i2);
    }

    public LineNumberInputStream(InputStream is) {
        super(is);
    }

    private int xgetLineNumber() {
        return this.lineNumber;
    }

    private int xread() throws IOException {
        int b = this.in.read();
        if (b == 10) {
            this.lineNumber++;
        }
        return b;
    }

    private int xread(byte[] b, int off, int len) throws IOException {
        int n = this.in.read(b, off, len);
        for (int i = off; i < off + n; i++) {
            if (b[i] == 10) {
                this.lineNumber++;
            }
        }
        return n;
    }
}
