package org.apache.james.mime4j.field.address.parser;

import java.util.Stack;

class JJTAddressListParserState {
    private Stack<Integer> marks = new Stack<>();
    private int mk = 0;
    private boolean node_created;
    private Stack<Node> nodes = new Stack<>();
    private int sp = 0;

    /* access modifiers changed from: package-private */
    public void clearNodeScope(Node node) {
        xclearNodeScope(node);
    }

    /* access modifiers changed from: package-private */
    public void closeNodeScope(Node node, int i) {
        xcloseNodeScope(node, i);
    }

    /* access modifiers changed from: package-private */
    public void closeNodeScope(Node node, boolean z) {
        xcloseNodeScope(node, z);
    }

    /* access modifiers changed from: package-private */
    public int nodeArity() {
        return xnodeArity();
    }

    /* access modifiers changed from: package-private */
    public boolean nodeCreated() {
        return xnodeCreated();
    }

    /* access modifiers changed from: package-private */
    public void openNodeScope(Node node) {
        xopenNodeScope(node);
    }

    /* access modifiers changed from: package-private */
    public Node peekNode() {
        return xpeekNode();
    }

    /* access modifiers changed from: package-private */
    public Node popNode() {
        return xpopNode();
    }

    /* access modifiers changed from: package-private */
    public void pushNode(Node node) {
        xpushNode(node);
    }

    /* access modifiers changed from: package-private */
    public void reset() {
        xreset();
    }

    /* access modifiers changed from: package-private */
    public Node rootNode() {
        return xrootNode();
    }

    JJTAddressListParserState() {
    }

    private boolean xnodeCreated() {
        return this.node_created;
    }

    private void xreset() {
        this.nodes.removeAllElements();
        this.marks.removeAllElements();
        this.sp = 0;
        this.mk = 0;
    }

    private Node xrootNode() {
        return this.nodes.elementAt(0);
    }

    private void xpushNode(Node n) {
        this.nodes.push(n);
        this.sp++;
    }

    private Node xpopNode() {
        int i = this.sp - 1;
        this.sp = i;
        if (i < this.mk) {
            this.mk = this.marks.pop().intValue();
        }
        return this.nodes.pop();
    }

    private Node xpeekNode() {
        return this.nodes.peek();
    }

    private int xnodeArity() {
        return this.sp - this.mk;
    }

    private void xclearNodeScope(Node n) {
        while (this.sp > this.mk) {
            popNode();
        }
        this.mk = this.marks.pop().intValue();
    }

    private void xopenNodeScope(Node n) {
        this.marks.push(new Integer(this.mk));
        this.mk = this.sp;
        n.jjtOpen();
    }

    private void xcloseNodeScope(Node n, int num) {
        this.mk = this.marks.pop().intValue();
        while (true) {
            int num2 = num;
            num = num2 - 1;
            if (num2 > 0) {
                Node c = popNode();
                c.jjtSetParent(n);
                n.jjtAddChild(c, num);
            } else {
                n.jjtClose();
                pushNode(n);
                this.node_created = true;
                return;
            }
        }
    }

    private void xcloseNodeScope(Node n, boolean condition) {
        if (condition) {
            int a = nodeArity();
            this.mk = this.marks.pop().intValue();
            while (true) {
                int a2 = a;
                a = a2 - 1;
                if (a2 > 0) {
                    Node c = popNode();
                    c.jjtSetParent(n);
                    n.jjtAddChild(c, a);
                } else {
                    n.jjtClose();
                    pushNode(n);
                    this.node_created = true;
                    return;
                }
            }
        } else {
            this.mk = this.marks.pop().intValue();
            this.node_created = false;
        }
    }
}
