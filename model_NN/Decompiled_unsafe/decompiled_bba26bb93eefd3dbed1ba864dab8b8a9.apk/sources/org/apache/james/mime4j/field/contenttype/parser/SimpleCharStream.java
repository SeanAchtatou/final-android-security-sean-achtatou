package org.apache.james.mime4j.field.contenttype.parser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

public class SimpleCharStream {
    public static final boolean staticFlag = false;
    int available;
    protected int[] bufcolumn;
    protected char[] buffer;
    protected int[] bufline;
    public int bufpos;
    int bufsize;
    protected int column;
    protected int inBuf;
    protected Reader inputStream;
    protected int line;
    protected int maxNextCharInd;
    protected boolean prevCharIsCR;
    protected boolean prevCharIsLF;
    protected int tabSize;
    int tokenBegin;

    public char BeginToken() {
        return xBeginToken();
    }

    public void Done() {
        xDone();
    }

    /* access modifiers changed from: protected */
    public void ExpandBuff(boolean z) {
        xExpandBuff(z);
    }

    /* access modifiers changed from: protected */
    public void FillBuff() {
        xFillBuff();
    }

    public String GetImage() {
        return xGetImage();
    }

    public char[] GetSuffix(int i) {
        return xGetSuffix(i);
    }

    public void ReInit(InputStream inputStream2) {
        xReInit(inputStream2);
    }

    public void ReInit(InputStream inputStream2, int i, int i2) {
        xReInit(inputStream2, i, i2);
    }

    public void ReInit(InputStream inputStream2, int i, int i2, int i3) {
        xReInit(inputStream2, i, i2, i3);
    }

    public void ReInit(InputStream inputStream2, String str) {
        xReInit(inputStream2, str);
    }

    public void ReInit(InputStream inputStream2, String str, int i, int i2) {
        xReInit(inputStream2, str, i, i2);
    }

    public void ReInit(InputStream inputStream2, String str, int i, int i2, int i3) {
        xReInit(inputStream2, str, i, i2, i3);
    }

    public void ReInit(Reader reader) {
        xReInit(reader);
    }

    public void ReInit(Reader reader, int i, int i2) {
        xReInit(reader, i, i2);
    }

    public void ReInit(Reader reader, int i, int i2, int i3) {
        xReInit(reader, i, i2, i3);
    }

    /* access modifiers changed from: protected */
    public void UpdateLineColumn(char c) {
        xUpdateLineColumn(c);
    }

    public void adjustBeginLineColumn(int i, int i2) {
        xadjustBeginLineColumn(i, i2);
    }

    public void backup(int i) {
        xbackup(i);
    }

    public int getBeginColumn() {
        return xgetBeginColumn();
    }

    public int getBeginLine() {
        return xgetBeginLine();
    }

    public int getColumn() {
        return xgetColumn();
    }

    public int getEndColumn() {
        return xgetEndColumn();
    }

    public int getEndLine() {
        return xgetEndLine();
    }

    public int getLine() {
        return xgetLine();
    }

    /* access modifiers changed from: protected */
    public int getTabSize(int i) {
        return xgetTabSize(i);
    }

    public char readChar() {
        return xreadChar();
    }

    /* access modifiers changed from: protected */
    public void setTabSize(int i) {
        xsetTabSize(i);
    }

    private void xsetTabSize(int i) {
        this.tabSize = i;
    }

    private int xgetTabSize(int i) {
        return this.tabSize;
    }

    private void xExpandBuff(boolean wrapAround) {
        char[] newbuffer = new char[(this.bufsize + 2048)];
        int[] newbufline = new int[(this.bufsize + 2048)];
        int[] newbufcolumn = new int[(this.bufsize + 2048)];
        if (wrapAround) {
            try {
                System.arraycopy(this.buffer, this.tokenBegin, newbuffer, 0, this.bufsize - this.tokenBegin);
                System.arraycopy(this.buffer, 0, newbuffer, this.bufsize - this.tokenBegin, this.bufpos);
                this.buffer = newbuffer;
                System.arraycopy(this.bufline, this.tokenBegin, newbufline, 0, this.bufsize - this.tokenBegin);
                System.arraycopy(this.bufline, 0, newbufline, this.bufsize - this.tokenBegin, this.bufpos);
                this.bufline = newbufline;
                System.arraycopy(this.bufcolumn, this.tokenBegin, newbufcolumn, 0, this.bufsize - this.tokenBegin);
                System.arraycopy(this.bufcolumn, 0, newbufcolumn, this.bufsize - this.tokenBegin, this.bufpos);
                this.bufcolumn = newbufcolumn;
                int i = this.bufpos + (this.bufsize - this.tokenBegin);
                this.bufpos = i;
                this.maxNextCharInd = i;
            } catch (Throwable t) {
                throw new Error(t.getMessage());
            }
        } else {
            System.arraycopy(this.buffer, this.tokenBegin, newbuffer, 0, this.bufsize - this.tokenBegin);
            this.buffer = newbuffer;
            System.arraycopy(this.bufline, this.tokenBegin, newbufline, 0, this.bufsize - this.tokenBegin);
            this.bufline = newbufline;
            System.arraycopy(this.bufcolumn, this.tokenBegin, newbufcolumn, 0, this.bufsize - this.tokenBegin);
            this.bufcolumn = newbufcolumn;
            int i2 = this.bufpos - this.tokenBegin;
            this.bufpos = i2;
            this.maxNextCharInd = i2;
        }
        this.bufsize += 2048;
        this.available = this.bufsize;
        this.tokenBegin = 0;
    }

    private void xFillBuff() throws IOException {
        if (this.maxNextCharInd == this.available) {
            if (this.available == this.bufsize) {
                if (this.tokenBegin > 2048) {
                    this.maxNextCharInd = 0;
                    this.bufpos = 0;
                    this.available = this.tokenBegin;
                } else if (this.tokenBegin < 0) {
                    this.maxNextCharInd = 0;
                    this.bufpos = 0;
                } else {
                    ExpandBuff(false);
                }
            } else if (this.available > this.tokenBegin) {
                this.available = this.bufsize;
            } else if (this.tokenBegin - this.available < 2048) {
                ExpandBuff(true);
            } else {
                this.available = this.tokenBegin;
            }
        }
        try {
            int i = this.inputStream.read(this.buffer, this.maxNextCharInd, this.available - this.maxNextCharInd);
            if (i == -1) {
                this.inputStream.close();
                throw new IOException();
            } else {
                this.maxNextCharInd += i;
            }
        } catch (IOException e) {
            this.bufpos--;
            backup(0);
            if (this.tokenBegin == -1) {
                this.tokenBegin = this.bufpos;
            }
            throw e;
        }
    }

    private char xBeginToken() throws IOException {
        this.tokenBegin = -1;
        char c = readChar();
        this.tokenBegin = this.bufpos;
        return c;
    }

    private void xUpdateLineColumn(char c) {
        this.column++;
        if (this.prevCharIsLF) {
            this.prevCharIsLF = false;
            int i = this.line;
            this.column = 1;
            this.line = i + 1;
        } else if (this.prevCharIsCR) {
            this.prevCharIsCR = false;
            if (c == 10) {
                this.prevCharIsLF = true;
            } else {
                int i2 = this.line;
                this.column = 1;
                this.line = i2 + 1;
            }
        }
        switch (c) {
            case 9:
                this.column--;
                this.column += this.tabSize - (this.column % this.tabSize);
                break;
            case 10:
                this.prevCharIsLF = true;
                break;
            case 13:
                this.prevCharIsCR = true;
                break;
        }
        this.bufline[this.bufpos] = this.line;
        this.bufcolumn[this.bufpos] = this.column;
    }

    private char xreadChar() throws IOException {
        if (this.inBuf > 0) {
            this.inBuf--;
            int i = this.bufpos + 1;
            this.bufpos = i;
            if (i == this.bufsize) {
                this.bufpos = 0;
            }
            return this.buffer[this.bufpos];
        }
        int i2 = this.bufpos + 1;
        this.bufpos = i2;
        if (i2 >= this.maxNextCharInd) {
            FillBuff();
        }
        char c = this.buffer[this.bufpos];
        UpdateLineColumn(c);
        return c;
    }

    private int xgetColumn() {
        return this.bufcolumn[this.bufpos];
    }

    private int xgetLine() {
        return this.bufline[this.bufpos];
    }

    private int xgetEndColumn() {
        return this.bufcolumn[this.bufpos];
    }

    private int xgetEndLine() {
        return this.bufline[this.bufpos];
    }

    private int xgetBeginColumn() {
        return this.bufcolumn[this.tokenBegin];
    }

    private int xgetBeginLine() {
        return this.bufline[this.tokenBegin];
    }

    private void xbackup(int amount) {
        this.inBuf += amount;
        int i = this.bufpos - amount;
        this.bufpos = i;
        if (i < 0) {
            this.bufpos += this.bufsize;
        }
    }

    public SimpleCharStream(Reader dstream, int startline, int startcolumn, int buffersize) {
        this.bufpos = -1;
        this.column = 0;
        this.line = 1;
        this.prevCharIsCR = false;
        this.prevCharIsLF = false;
        this.maxNextCharInd = 0;
        this.inBuf = 0;
        this.tabSize = 8;
        this.inputStream = dstream;
        this.line = startline;
        this.column = startcolumn - 1;
        this.bufsize = buffersize;
        this.available = buffersize;
        this.buffer = new char[buffersize];
        this.bufline = new int[buffersize];
        this.bufcolumn = new int[buffersize];
    }

    public SimpleCharStream(Reader dstream, int startline, int startcolumn) {
        this(dstream, startline, startcolumn, 4096);
    }

    public SimpleCharStream(Reader dstream) {
        this(dstream, 1, 1, 4096);
    }

    private void xReInit(Reader dstream, int startline, int startcolumn, int buffersize) {
        this.inputStream = dstream;
        this.line = startline;
        this.column = startcolumn - 1;
        if (this.buffer == null || buffersize != this.buffer.length) {
            this.bufsize = buffersize;
            this.available = buffersize;
            this.buffer = new char[buffersize];
            this.bufline = new int[buffersize];
            this.bufcolumn = new int[buffersize];
        }
        this.prevCharIsCR = false;
        this.prevCharIsLF = false;
        this.maxNextCharInd = 0;
        this.inBuf = 0;
        this.tokenBegin = 0;
        this.bufpos = -1;
    }

    private void xReInit(Reader dstream, int startline, int startcolumn) {
        ReInit(dstream, startline, startcolumn, 4096);
    }

    private void xReInit(Reader dstream) {
        ReInit(dstream, 1, 1, 4096);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SimpleCharStream(InputStream dstream, String encoding, int startline, int startcolumn, int buffersize) throws UnsupportedEncodingException {
        this(encoding == null ? new InputStreamReader(dstream) : new InputStreamReader(dstream, encoding), startline, startcolumn, buffersize);
    }

    public SimpleCharStream(InputStream dstream, int startline, int startcolumn, int buffersize) {
        this(new InputStreamReader(dstream), startline, startcolumn, buffersize);
    }

    public SimpleCharStream(InputStream dstream, String encoding, int startline, int startcolumn) throws UnsupportedEncodingException {
        this(dstream, encoding, startline, startcolumn, 4096);
    }

    public SimpleCharStream(InputStream dstream, int startline, int startcolumn) {
        this(dstream, startline, startcolumn, 4096);
    }

    public SimpleCharStream(InputStream dstream, String encoding) throws UnsupportedEncodingException {
        this(dstream, encoding, 1, 1, 4096);
    }

    public SimpleCharStream(InputStream dstream) {
        this(dstream, 1, 1, 4096);
    }

    private void xReInit(InputStream dstream, String encoding, int startline, int startcolumn, int buffersize) throws UnsupportedEncodingException {
        ReInit(encoding == null ? new InputStreamReader(dstream) : new InputStreamReader(dstream, encoding), startline, startcolumn, buffersize);
    }

    private void xReInit(InputStream dstream, int startline, int startcolumn, int buffersize) {
        ReInit(new InputStreamReader(dstream), startline, startcolumn, buffersize);
    }

    private void xReInit(InputStream dstream, String encoding) throws UnsupportedEncodingException {
        ReInit(dstream, encoding, 1, 1, 4096);
    }

    private void xReInit(InputStream dstream) {
        ReInit(dstream, 1, 1, 4096);
    }

    private void xReInit(InputStream dstream, String encoding, int startline, int startcolumn) throws UnsupportedEncodingException {
        ReInit(dstream, encoding, startline, startcolumn, 4096);
    }

    private void xReInit(InputStream dstream, int startline, int startcolumn) {
        ReInit(dstream, startline, startcolumn, 4096);
    }

    private String xGetImage() {
        if (this.bufpos >= this.tokenBegin) {
            return new String(this.buffer, this.tokenBegin, (this.bufpos - this.tokenBegin) + 1);
        }
        return new String(this.buffer, this.tokenBegin, this.bufsize - this.tokenBegin) + new String(this.buffer, 0, this.bufpos + 1);
    }

    private char[] xGetSuffix(int len) {
        char[] ret = new char[len];
        if (this.bufpos + 1 >= len) {
            System.arraycopy(this.buffer, (this.bufpos - len) + 1, ret, 0, len);
        } else {
            System.arraycopy(this.buffer, this.bufsize - ((len - this.bufpos) - 1), ret, 0, (len - this.bufpos) - 1);
            System.arraycopy(this.buffer, 0, ret, (len - this.bufpos) - 1, this.bufpos + 1);
        }
        return ret;
    }

    private void xDone() {
        this.buffer = null;
        this.bufline = null;
        this.bufcolumn = null;
    }

    private void xadjustBeginLineColumn(int newLine, int newCol) {
        int len;
        int start = this.tokenBegin;
        if (this.bufpos >= this.tokenBegin) {
            len = (this.bufpos - this.tokenBegin) + this.inBuf + 1;
        } else {
            len = (this.bufsize - this.tokenBegin) + this.bufpos + 1 + this.inBuf;
        }
        int i = 0;
        int j = 0;
        int columnDiff = 0;
        while (i < len) {
            int[] iArr = this.bufline;
            j = start % this.bufsize;
            int i2 = iArr[j];
            int[] iArr2 = this.bufline;
            start++;
            int k = start % this.bufsize;
            if (i2 != iArr2[k]) {
                break;
            }
            this.bufline[j] = newLine;
            int nextColDiff = (this.bufcolumn[k] + columnDiff) - this.bufcolumn[j];
            this.bufcolumn[j] = newCol + columnDiff;
            columnDiff = nextColDiff;
            i++;
        }
        if (i < len) {
            int newLine2 = newLine + 1;
            this.bufline[j] = newLine;
            this.bufcolumn[j] = newCol + columnDiff;
            int i3 = i;
            while (true) {
                int i4 = i3 + 1;
                if (i3 >= len) {
                    break;
                }
                int[] iArr3 = this.bufline;
                j = start % this.bufsize;
                start++;
                if (iArr3[j] != this.bufline[start % this.bufsize]) {
                    this.bufline[j] = newLine2;
                    i3 = i4;
                    newLine2++;
                } else {
                    this.bufline[j] = newLine2;
                    i3 = i4;
                }
            }
        }
        this.line = this.bufline[j];
        this.column = this.bufcolumn[j];
    }
}
