package org.apache.james.mime4j.field;

import org.apache.james.mime4j.codec.DecoderUtil;
import org.apache.james.mime4j.util.ByteSequence;

public class UnstructuredField extends AbstractField {
    static final FieldParser PARSER = new FieldParser() {
        public ParsedField parse(String str, String str2, ByteSequence byteSequence) {
            return xparse(str, str2, byteSequence);
        }

        private ParsedField xparse(String name, String body, ByteSequence raw) {
            return new UnstructuredField(name, body, raw);
        }
    };
    private boolean parsed = false;
    private String value;

    private void parse() {
        xparse();
    }

    public String getValue() {
        return xgetValue();
    }

    UnstructuredField(String name, String body, ByteSequence raw) {
        super(name, body, raw);
    }

    private String xgetValue() {
        if (!this.parsed) {
            parse();
        }
        return this.value;
    }

    private void xparse() {
        this.value = DecoderUtil.decodeEncodedWords(getBody());
        this.parsed = true;
    }
}
