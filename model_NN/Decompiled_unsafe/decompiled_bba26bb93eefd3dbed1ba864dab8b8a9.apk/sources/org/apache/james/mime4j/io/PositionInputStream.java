package org.apache.james.mime4j.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class PositionInputStream extends FilterInputStream {
    private long markedPosition = 0;
    protected long position = 0;

    public int available() {
        return xavailable();
    }

    public void close() {
        xclose();
    }

    public long getPosition() {
        return xgetPosition();
    }

    public void mark(int i) {
        xmark(i);
    }

    public boolean markSupported() {
        return xmarkSupported();
    }

    public int read() {
        return xread();
    }

    public int read(byte[] bArr, int i, int i2) {
        return xread(bArr, i, i2);
    }

    public void reset() {
        xreset();
    }

    public long skip(long j) {
        return xskip(j);
    }

    public PositionInputStream(InputStream inputStream) {
        super(inputStream);
    }

    private long xgetPosition() {
        return this.position;
    }

    private int xavailable() throws IOException {
        return this.in.available();
    }

    private int xread() throws IOException {
        int b = this.in.read();
        if (b != -1) {
            this.position++;
        }
        return b;
    }

    private void xclose() throws IOException {
        this.in.close();
    }

    private void xreset() throws IOException {
        this.in.reset();
        this.position = this.markedPosition;
    }

    private boolean xmarkSupported() {
        return this.in.markSupported();
    }

    private void xmark(int readlimit) {
        this.in.mark(readlimit);
        this.markedPosition = this.position;
    }

    private long xskip(long n) throws IOException {
        long c = this.in.skip(n);
        if (c > 0) {
            this.position += c;
        }
        return c;
    }

    private int xread(byte[] b, int off, int len) throws IOException {
        int c = this.in.read(b, off, len);
        if (c > 0) {
            this.position += (long) c;
        }
        return c;
    }
}
