package org.apache.james.mime4j.message;

import java.io.IOException;
import java.io.OutputStream;

public abstract class SingleBody implements Body {
    private Entity parent = null;

    public SingleBody copy() {
        return xcopy();
    }

    public void dispose() {
        xdispose();
    }

    public Entity getParent() {
        return xgetParent();
    }

    public void setParent(Entity entity) {
        xsetParent(entity);
    }

    public abstract void writeTo(OutputStream outputStream) throws IOException;

    protected SingleBody() {
    }

    private Entity xgetParent() {
        return this.parent;
    }

    private void xsetParent(Entity parent2) {
        this.parent = parent2;
    }

    private SingleBody xcopy() {
        throw new UnsupportedOperationException();
    }

    private void xdispose() {
    }
}
