package com.apperhand.device.android.b;

import com.apperhand.device.a.d.f;
import java.io.StringWriter;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.map.ObjectMapper;

/* compiled from: PojoMapper */
public final class a {
    private static ObjectMapper a = new ObjectMapper();
    private static JsonFactory b = new JsonFactory();

    public static <T> T a(String str, Class<T> cls) throws f {
        try {
            return a.readValue(str, cls);
        } catch (Exception e) {
            throw new f(f.a.GENERAL_ERROR, "Could not write JSON, string=[" + str.toString() + "] " + e.getMessage(), e);
        }
    }

    public static String a(Object obj) throws f {
        StringWriter stringWriter = new StringWriter();
        try {
            a.writeValue(b.createJsonGenerator(stringWriter), obj);
            return stringWriter.toString();
        } catch (Exception e) {
            throw new f(f.a.GENERAL_ERROR, "Could not read JSON, object=[" + obj.toString() + "] " + e.getMessage(), e);
        }
    }
}
