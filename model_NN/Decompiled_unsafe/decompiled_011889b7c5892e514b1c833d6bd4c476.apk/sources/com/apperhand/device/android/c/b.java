package com.apperhand.device.android.c;

import android.util.Log;
import com.apperhand.device.a.d.c;
import org.codehaus.jackson.impl.JsonWriteContext;

/* compiled from: LoggerDecorator */
public final class b implements c {
    public final void a(c.a aVar, String str, String str2) {
        a(aVar, str, str2, null);
    }

    /* renamed from: com.apperhand.device.android.c.b$1  reason: invalid class name */
    /* compiled from: LoggerDecorator */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[c.a.values().length];

        static {
            try {
                a[c.a.DEBUG.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[c.a.ERROR.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[c.a.INFO.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public final void a(c.a aVar, String str, String str2, Throwable th) {
        switch (AnonymousClass1.a[aVar.ordinal()]) {
            case JsonWriteContext.STATUS_OK_AFTER_COMMA /*1*/:
                Log.d(str, str2, th);
                return;
            case JsonWriteContext.STATUS_OK_AFTER_COLON /*2*/:
                Log.e(str, str2, th);
                return;
            case JsonWriteContext.STATUS_OK_AFTER_SPACE /*3*/:
                Log.i(str, str2, th);
                return;
            default:
                return;
        }
    }
}
