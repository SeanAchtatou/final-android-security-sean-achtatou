package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandInformation;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.InfoRequest;
import com.apperhand.common.dto.protocol.InfoResponse;
import com.apperhand.device.a.a;
import com.apperhand.device.a.a.b;
import com.apperhand.device.a.a.d;
import com.apperhand.device.a.d.c;
import com.apperhand.device.a.d.f;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.impl.JsonWriteContext;

/* compiled from: InfoService */
public final class e extends b {
    Map<String, Object> g;
    private d h;
    private b i;

    public e(com.apperhand.device.a.b bVar, a aVar, String str, Command command) {
        super(bVar, aVar, str, command.getCommand());
        this.h = aVar.e();
        this.i = aVar.d();
        this.g = command.getParameters();
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws f {
        return null;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws f {
        CommandInformation a;
        InfoRequest infoRequest = new InfoRequest();
        ArrayList arrayList = new ArrayList();
        infoRequest.setApplicationDetails(this.e.i());
        infoRequest.setInformation(arrayList);
        for (String next : this.g.keySet()) {
            List list = (List) this.g.get(next);
            if (list != null) {
                switch (AnonymousClass1.a[Command.getCommandByName(next).ordinal()]) {
                    case JsonWriteContext.STATUS_OK_AFTER_COMMA /*1*/:
                        this.h.a();
                        a = this.h.a(list);
                        break;
                    case JsonWriteContext.STATUS_OK_AFTER_COLON /*2*/:
                        a = this.i.a(list);
                        break;
                    default:
                        a = null;
                        break;
                }
                arrayList.add(a);
            }
        }
        return a(infoRequest);
    }

    private BaseResponse a(InfoRequest infoRequest) {
        try {
            return (InfoResponse) this.e.b().a(infoRequest, Command.Commands.INFO, InfoResponse.class);
        } catch (f e) {
            this.e.a().a(c.a.DEBUG, this.a, "Unable to handle Info command!!!!", e);
            return null;
        }
    }

    /* renamed from: com.apperhand.device.a.b.e$1  reason: invalid class name */
    /* compiled from: InfoService */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[Command.Commands.values().length];

        static {
            try {
                a[Command.Commands.SHORTCUTS.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Command.Commands.BOOKMARKS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws f {
    }
}
