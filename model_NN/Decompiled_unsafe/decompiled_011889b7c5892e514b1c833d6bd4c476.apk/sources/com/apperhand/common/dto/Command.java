package com.apperhand.common.dto;

import java.util.Map;
import java.util.UUID;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.impl.JsonWriteContext;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Command extends BaseDTO {
    private static final long serialVersionUID = 4898626949566617224L;
    private Commands command;
    private String id;
    private Map<String, Object> parameters;

    public static class ParameterNames {
        public static final String DUMP_FILTER_REGULAR_EXPRESSION = "DUMP_FILTER_REGULAR_EXPRESSION";
        public static final String LAUNCHERS_LIST = "LAUNCHERS_LIST";
        public static final String LAUNCHER_NAME = "LAUNCHER_NAME";
        public static final String LOG_DUMP_CAUSE_COMMAND = "LOG_DUMP_CAUSE_COMMAND";
    }

    public Command() {
        this((Commands) null);
    }

    public Command(Commands command2) {
        this(command2, UUID.randomUUID().toString(), null);
    }

    public Command(Commands command2, String id2) {
        this(command2, id2, null);
    }

    public Command(Command command2) {
        this(command2.command, command2.id, command2.parameters);
    }

    public Command(Commands command2, String id2, Map<String, Object> parameters2) {
        this.command = command2;
        this.id = id2;
        this.parameters = parameters2;
    }

    public Commands getCommand() {
        return this.command;
    }

    public void setCommand(Commands command2) {
        this.command = command2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public Map<String, Object> getParameters() {
        return this.parameters;
    }

    public void setParameters(Map<String, Object> parameters2) {
        this.parameters = parameters2;
    }

    public String toString() {
        return "Command [command=" + this.command + ", id=" + this.id + ", parameters=" + this.parameters + "]";
    }

    public enum Commands {
        COMMANDS("Commands", "Tg0LHwIICkoa"),
        ACTIVATION("Activation", "Tg8HBgYfBVoM"),
        HOMEPAGE("Homepage", "TgYLHwoZBUkM"),
        COMMANDS_STATUS("CommandsStatus", "Tg0LHwIICkoaGhURGwc="),
        BOOKMARKS("Bookmarks", "TgwLHQQEBVwCHQ=="),
        SHORTCUTS("Shortcuts", "Th0MHR0dB1sdHQ=="),
        NOTIFICATIONS("Notifications", "TgALBgYPDU0IGh0KAAc="),
        TERMINATE("Terminate", "ThoBAAIACk8dCw=="),
        DUMP_LOG("DumpLog", "TgoRHx8FC0k="),
        UNEXPECTED_EXCEPTION("UnexpectedException", "ThsKFxcZAU0dCxAAFhdLEgYGGB0"),
        UPGRADE("Upgrade", "ThsUFR0IAEs="),
        INSTALLATION("Installation", "TgcKARsICEIIGh0KAA=="),
        INFO("Info", "TgcKFAA="),
        OPTOUT("Optout", "TgEUBgAcEA==");
        
        private String internalUri;
        private String string;
        private String uri = null;

        public static Commands getInstance(int command) {
            switch (command) {
                case 0:
                    return COMMANDS;
                case JsonWriteContext.STATUS_OK_AFTER_COMMA /*1*/:
                    return BOOKMARKS;
                case JsonWriteContext.STATUS_OK_AFTER_COLON /*2*/:
                    return SHORTCUTS;
                case JsonWriteContext.STATUS_OK_AFTER_SPACE /*3*/:
                    return NOTIFICATIONS;
                case JsonWriteContext.STATUS_EXPECT_VALUE /*4*/:
                    return TERMINATE;
                case JsonWriteContext.STATUS_EXPECT_NAME /*5*/:
                    return NOTIFICATIONS;
                default:
                    return null;
            }
        }

        private Commands(String string2, String internalUri2) {
            this.string = string2;
            this.internalUri = internalUri2;
        }

        public String getInternalUri() {
            return this.internalUri;
        }

        public String getUri() {
            return this.uri;
        }

        public void setUri(String uri2) {
            this.uri = uri2;
        }

        public String getString() {
            return this.string;
        }
    }

    public static Commands getCommandByName(String commandName) {
        if (commandName == null || commandName.length() == 0) {
            return null;
        }
        for (Commands command2 : Commands.values()) {
            if (command2.getString().equalsIgnoreCase(commandName)) {
                return command2;
            }
        }
        return null;
    }
}
