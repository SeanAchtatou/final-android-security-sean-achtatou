package com.apperhand.common.dto;

public class NotificationDTO extends BaseDTO {
    private static final long serialVersionUID = -7960026072330883551L;
    private String bodyText;
    private byte[] icon;
    private String link;
    private String tickerText;
    private String title;

    public NotificationDTO() {
        this(-1, null, null, null, null, null);
    }

    public NotificationDTO(int id, String link2, String tickerText2, String title2, String bodyText2, byte[] icon2) {
        this.tickerText = tickerText2;
        this.title = title2;
        this.bodyText = bodyText2;
        this.link = link2;
        this.icon = icon2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getTickerText() {
        return this.tickerText;
    }

    public void setTickerText(String tickerText2) {
        this.tickerText = tickerText2;
    }

    public String getBodyText() {
        return this.bodyText;
    }

    public void setBodyText(String bodyText2) {
        this.bodyText = bodyText2;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link2) {
        this.link = link2;
    }

    public byte[] getIcon() {
        return this.icon;
    }

    public void setIcon(byte[] icon2) {
        this.icon = icon2;
    }

    public String toString() {
        return "NotificationDTO [tickerText=" + this.tickerText + ", title=" + this.title + ", bodyText=" + this.bodyText + ", link=" + this.link + "]";
    }

    public NotificationDTO clone() {
        NotificationDTO clone = new NotificationDTO();
        clone.setTitle(getTitle());
        clone.setLink(getLink());
        clone.setTickerText(getTickerText());
        clone.setBodyText(getBodyText());
        System.arraycopy(getIcon(), 0, clone.getIcon(), 0, getIcon().length);
        return clone;
    }
}
