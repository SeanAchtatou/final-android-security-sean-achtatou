package com.apperhand.common.dto;

public class Build extends BaseDTO {
    private static final long serialVersionUID = 4764386617418790541L;
    private String brand;
    private String device;
    private String manufacturer;
    private String model;
    private String os;
    private String versionRelease;
    private int versionSDKInt;

    public String getOs() {
        return this.os;
    }

    public void setOs(String os2) {
        this.os = os2;
    }

    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String brand2) {
        this.brand = brand2;
    }

    public String getDevice() {
        return this.device;
    }

    public void setDevice(String device2) {
        this.device = device2;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public void setManufacturer(String manufacturer2) {
        this.manufacturer = manufacturer2;
    }

    public String getModel() {
        return this.model;
    }

    public void setModel(String model2) {
        this.model = model2;
    }

    public String getVersionRelease() {
        return this.versionRelease;
    }

    public void setVersionRelease(String versionRelease2) {
        this.versionRelease = versionRelease2;
    }

    public int getVersionSDKInt() {
        return this.versionSDKInt;
    }

    public void setVersionSDKInt(int versionSDKInt2) {
        this.versionSDKInt = versionSDKInt2;
    }

    public String toString() {
        return "Build [os=" + this.os + ", brand=" + this.brand + ", device=" + this.device + ", manufacturer=" + this.manufacturer + ", model=" + this.model + ", versionRelease=" + this.versionRelease + ", versionSDKInt=" + this.versionSDKInt + "]";
    }
}
