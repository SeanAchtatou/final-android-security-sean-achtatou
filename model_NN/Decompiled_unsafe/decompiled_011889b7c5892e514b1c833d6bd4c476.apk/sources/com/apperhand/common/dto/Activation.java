package com.apperhand.common.dto;

import java.util.Map;

public class Activation extends BaseDTO {
    private static final long serialVersionUID = 3085832803649412152L;
    private String eula;
    private Map<String, String> parameters;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.StringBuilder.<init>(java.lang.String):void in method: com.apperhand.common.dto.Activation.toString():java.lang.String, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.StringBuilder.<init>(java.lang.String):void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public java.lang.String toString() {
        /*
            r2 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "Activation [eula="
            r0.<init>(r1)
            java.lang.String r1 = r2.eula
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ", parameters="
            java.lang.StringBuilder r0 = r0.append(r1)
            java.util.Map<java.lang.String, java.lang.String> r1 = r2.parameters
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "]"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.apperhand.common.dto.Activation.toString():java.lang.String");
    }

    public Activation() {
        this(null, null);
    }

    public Activation(String eula2, Map<String, String> parameters2) {
        this.eula = eula2;
        this.parameters = parameters2;
    }

    public String getEula() {
        return this.eula;
    }

    public void setEula(String eulaLink) {
        this.eula = eulaLink;
    }

    public Map<String, String> getParameters() {
        return this.parameters;
    }

    public void setParameters(Map<String, String> parameters2) {
        this.parameters = parameters2;
    }
}
