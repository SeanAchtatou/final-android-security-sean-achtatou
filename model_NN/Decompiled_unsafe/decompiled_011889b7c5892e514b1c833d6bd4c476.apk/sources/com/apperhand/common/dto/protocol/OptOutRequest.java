package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.OptOutDetails;

public class OptOutRequest extends BaseRequest {
    private static final long serialVersionUID = -2767172802760161992L;
    private OptOutDetails details;

    public OptOutDetails getDetails() {
        return this.details;
    }

    public void setDetails(OptOutDetails details2) {
        this.details = details2;
    }

    public String toString() {
        return "OptOutRequest [details=" + this.details + ", toString()=" + super.toString() + "]";
    }
}
