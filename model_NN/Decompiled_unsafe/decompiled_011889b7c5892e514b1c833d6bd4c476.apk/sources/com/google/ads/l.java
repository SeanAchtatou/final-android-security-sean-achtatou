package com.google.ads;

import android.net.Uri;
import android.webkit.WebView;
import com.google.ads.util.a;
import java.util.HashMap;
import java.util.Locale;

public final class l implements j {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        Uri parse;
        String host;
        String str = hashMap.get(AdActivity.URL_PARAM);
        if (str == null) {
            a.e("Could not get URL from click gmsg.");
            return;
        }
        f l = dVar.l();
        if (!(l == null || (host = (parse = Uri.parse(str)).getHost()) == null || !host.toLowerCase(Locale.US).endsWith(".admob.com"))) {
            String str2 = null;
            String path = parse.getPath();
            if (path != null) {
                String[] split = path.split("/");
                if (split.length >= 4) {
                    str2 = split[2] + "/" + split[3];
                }
            }
            l.b(str2);
        }
        new Thread(new w(str, webView.getContext().getApplicationContext())).start();
    }
}
