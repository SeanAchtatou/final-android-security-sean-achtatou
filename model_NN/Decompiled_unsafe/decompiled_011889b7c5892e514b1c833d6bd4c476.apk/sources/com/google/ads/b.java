package com.google.ads;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.google.ads.AdRequest;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.StringTokenizer;

public final class b implements Runnable {
    private c a;
    private d b;
    private volatile boolean c;
    private boolean d;
    private String e;

    b(c cVar, d dVar) {
        this.a = cVar;
        this.b = dVar;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.c = true;
    }

    private void a(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.b.a(stringTokenizer.nextToken());
            }
        }
        b(httpURLConnection);
        String headerField2 = httpURLConnection.getHeaderField("X-Afma-Refresh-Rate");
        if (headerField2 != null) {
            try {
                float parseFloat = Float.parseFloat(headerField2);
                if (parseFloat > 0.0f) {
                    this.b.a(parseFloat);
                    if (!this.b.p()) {
                        this.b.d();
                    }
                } else if (this.b.p()) {
                    this.b.c();
                }
            } catch (NumberFormatException e2) {
                a.b("Could not get refresh value: " + headerField2, e2);
            }
        }
        String headerField3 = httpURLConnection.getHeaderField("X-Afma-Interstitial-Timeout");
        if (headerField3 != null) {
            try {
                this.b.a((long) (Float.parseFloat(headerField3) * 1000.0f));
            } catch (NumberFormatException e3) {
                a.b("Could not get timeout value: " + headerField3, e3);
            }
        }
        String headerField4 = httpURLConnection.getHeaderField("X-Afma-Orientation");
        if (headerField4 != null) {
            if (headerField4.equals("portrait")) {
                this.a.a(AdUtil.b());
            } else if (headerField4.equals("landscape")) {
                this.a.a(AdUtil.a());
            }
        }
        if (!TextUtils.isEmpty(httpURLConnection.getHeaderField("X-Afma-Doritos-Cache-Life"))) {
            try {
                this.b.b(Long.parseLong(httpURLConnection.getHeaderField("X-Afma-Doritos-Cache-Life")));
            } catch (NumberFormatException e4) {
                a.e("Got bad value of Doritos cookie cache life from header: " + httpURLConnection.getHeaderField("X-Afma-Doritos-Cache-Life") + ". Using default value instead.");
            }
        }
    }

    private void b(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Click-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.a.a(stringTokenizer.nextToken());
            }
        }
    }

    public final void a(boolean z) {
        this.d = z;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.e = str;
        this.c = false;
        new Thread(this).start();
    }

    public final void run() {
        HttpURLConnection httpURLConnection;
        String readLine;
        while (!this.c) {
            try {
                httpURLConnection = (HttpURLConnection) new URL(this.e).openConnection();
                Activity e2 = this.b.e();
                if (e2 == null) {
                    a.c("activity was null in AdHtmlLoader.");
                    this.a.a(AdRequest.ErrorCode.INTERNAL_ERROR);
                    httpURLConnection.disconnect();
                    return;
                }
                SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(e2);
                if (this.d && !TextUtils.isEmpty(defaultSharedPreferences.getString("drt", ""))) {
                    if (AdUtil.a == 8) {
                        httpURLConnection.addRequestProperty("X-Afma-drt-Cookie", defaultSharedPreferences.getString("drt", ""));
                    } else {
                        httpURLConnection.addRequestProperty("Cookie", defaultSharedPreferences.getString("drt", ""));
                    }
                }
                AdUtil.a(httpURLConnection, e2.getApplicationContext());
                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.connect();
                int responseCode = httpURLConnection.getResponseCode();
                if (300 <= responseCode && responseCode < 400) {
                    String headerField = httpURLConnection.getHeaderField("Location");
                    if (headerField == null) {
                        a.c("Could not get redirect location from a " + responseCode + " redirect.");
                        this.a.a(AdRequest.ErrorCode.INTERNAL_ERROR);
                        httpURLConnection.disconnect();
                        return;
                    }
                    a(httpURLConnection);
                    this.e = headerField;
                    httpURLConnection.disconnect();
                } else if (responseCode == 200) {
                    a(httpURLConnection);
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()), 4096);
                    StringBuilder sb = new StringBuilder();
                    while (!this.c && (readLine = bufferedReader.readLine()) != null) {
                        sb.append(readLine);
                        sb.append("\n");
                    }
                    String sb2 = sb.toString();
                    a.a("Response content is: " + sb2);
                    if (sb2 == null || sb2.trim().length() <= 0) {
                        a.a("Response message is null or zero length: " + sb2);
                        this.a.a(AdRequest.ErrorCode.NO_FILL);
                        httpURLConnection.disconnect();
                        return;
                    }
                    this.a.a(sb2, this.e);
                    httpURLConnection.disconnect();
                    return;
                } else if (responseCode == 400) {
                    a.c("Bad request");
                    this.a.a(AdRequest.ErrorCode.INVALID_REQUEST);
                    httpURLConnection.disconnect();
                    return;
                } else {
                    a.c("Invalid response code: " + responseCode);
                    this.a.a(AdRequest.ErrorCode.INTERNAL_ERROR);
                    httpURLConnection.disconnect();
                    return;
                }
            } catch (MalformedURLException e3) {
                a.a("Received malformed ad url from javascript.", e3);
                this.a.a(AdRequest.ErrorCode.INTERNAL_ERROR);
                return;
            } catch (IOException e4) {
                a.b("IOException connecting to ad url.", e4);
                this.a.a(AdRequest.ErrorCode.NETWORK_ERROR);
                return;
            } catch (Exception e5) {
                a.a("An unknown error occurred in AdHtmlLoader.", e5);
                this.a.a(AdRequest.ErrorCode.INTERNAL_ERROR);
                return;
            } catch (Throwable th) {
                httpURLConnection.disconnect();
                throw th;
            }
        }
    }
}
