package com.google.ads;

import android.app.Activity;
import android.content.Intent;
import android.os.SystemClock;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.google.ads.util.a;

public class AdActivity extends Activity implements View.OnClickListener {
    public static final String BASE_URL_PARAM = "baseurl";
    public static final String HTML_PARAM = "html";
    public static final String INTENT_ACTION_PARAM = "i";
    public static final String ORIENTATION_PARAM = "o";
    public static final String TYPE_PARAM = "m";
    public static final String URL_PARAM = "u";
    private static final Object a = new Object();
    private static AdActivity b = null;
    private static d c = null;
    private static AdActivity d = null;
    private static AdActivity e = null;
    private h f;
    private boolean g;
    private long h;
    private RelativeLayout i;
    private AdActivity j = null;
    private boolean k;
    private g l;

    private void a(String str) {
        a.b(str);
        finish();
    }

    private void a(String str, Throwable th) {
        a.a(str, th);
        finish();
    }

    public g getAdVideoView() {
        return this.l;
    }

    public h getOpeningAdWebView() {
        if (this.j != null) {
            return this.j.f;
        }
        synchronized (a) {
            if (c == null) {
                a.e("currentAdManager was null while trying to get the opening AdWebView.");
                return null;
            }
            h i2 = c.i();
            if (i2 != this.f) {
                return i2;
            }
            return null;
        }
    }

    public static boolean isShowing() {
        boolean z;
        synchronized (a) {
            z = d != null;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0024, code lost:
        r1 = new android.content.Intent(r0.getApplicationContext(), com.google.ads.AdActivity.class);
        r1.putExtra("com.google.ads.AdOpener", r5.a());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        com.google.ads.util.a.a("Launching AdActivity.");
        r0.startActivity(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0041, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0042, code lost:
        com.google.ads.util.a.a(r0.getMessage(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000a, code lost:
        r0 = r4.e();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x000e, code lost:
        if (r0 != null) goto L_0x0024;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0010, code lost:
        com.google.ads.util.a.e("activity was null while launching an AdActivity.");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void launchAdActivity(com.google.ads.d r4, com.google.ads.e r5) {
        /*
            java.lang.Object r0 = com.google.ads.AdActivity.a
            monitor-enter(r0)
            com.google.ads.d r1 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0021 }
            if (r1 != 0) goto L_0x0016
            com.google.ads.AdActivity.c = r4     // Catch:{ all -> 0x0021 }
        L_0x0009:
            monitor-exit(r0)
            android.app.Activity r0 = r4.e()
            if (r0 != 0) goto L_0x0024
            java.lang.String r0 = "activity was null while launching an AdActivity."
            com.google.ads.util.a.e(r0)
        L_0x0015:
            return
        L_0x0016:
            com.google.ads.d r1 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0021 }
            if (r1 == r4) goto L_0x0009
            java.lang.String r1 = "Tried to launch a new AdActivity with a different AdManager."
            com.google.ads.util.a.b(r1)     // Catch:{ all -> 0x0021 }
            monitor-exit(r0)     // Catch:{ all -> 0x0021 }
            goto L_0x0015
        L_0x0021:
            r4 = move-exception
            monitor-exit(r0)
            throw r4
        L_0x0024:
            android.content.Intent r1 = new android.content.Intent
            android.content.Context r2 = r0.getApplicationContext()
            java.lang.Class<com.google.ads.AdActivity> r3 = com.google.ads.AdActivity.class
            r1.<init>(r2, r3)
            java.lang.String r2 = "com.google.ads.AdOpener"
            android.os.Bundle r3 = r5.a()
            r1.putExtra(r2, r3)
            java.lang.String r2 = "Launching AdActivity."
            com.google.ads.util.a.a(r2)     // Catch:{ ActivityNotFoundException -> 0x0041 }
            r0.startActivity(r1)     // Catch:{ ActivityNotFoundException -> 0x0041 }
            goto L_0x0015
        L_0x0041:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            com.google.ads.util.a.a(r1, r0)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.launchAdActivity(com.google.ads.d, com.google.ads.e):void");
    }

    private void a(d dVar) {
        this.f = null;
        this.h = SystemClock.elapsedRealtime();
        this.k = true;
        synchronized (a) {
            if (b == null) {
                b = this;
                dVar.t();
            }
        }
    }

    public void moveAdVideoView(int x, int y, int width, int height) {
        if (this.l != null) {
            this.l.setLayoutParams(a(x, y, width, height));
            this.l.requestLayout();
        }
    }

    public void newAdVideoView(int x, int y, int width, int height) {
        if (this.l == null) {
            this.l = new g(this, this.f);
            this.i.addView(this.l, 0, a(x, y, width, height));
            synchronized (a) {
                if (c == null) {
                    a.e("currentAdManager was null while trying to get the opening AdWebView.");
                } else {
                    c.j().a();
                }
            }
        }
    }

    private static RelativeLayout.LayoutParams a(int i2, int i3, int i4, int i5) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(i4, i5);
        layoutParams.setMargins(i2, i3, 0, 0);
        layoutParams.addRule(10);
        layoutParams.addRule(9);
        return layoutParams;
    }

    public void onClick(View view) {
        finish();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0042, code lost:
        r9.i = null;
        r9.k = false;
        r9.l = null;
        r0 = getIntent().getBundleExtra("com.google.ads.AdOpener");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0052, code lost:
        if (r0 != null) goto L_0x0064;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0054, code lost:
        a("Could not get the Bundle used to create AdActivity.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0064, code lost:
        r1 = new com.google.ads.e(r0);
        r0 = r1.b();
        r7 = r1.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0077, code lost:
        if (r0.equals("plusone") == false) goto L_0x00d3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0079, code lost:
        r1 = new android.content.Intent();
        r1.setComponent(new android.content.ComponentName("com.google.android.apps.plus", "com.google.android.apps.circles.platform.PlusOneActivity"));
        r1.addCategory("android.intent.category.LAUNCHER");
        r1.putExtras(getIntent().getExtras());
        r1.putExtra("com.google.circles.platform.intent.extra.ENTITY", r7.get(com.google.ads.AdActivity.URL_PARAM));
        r1.putExtra("com.google.circles.platform.intent.extra.ENTITY_TYPE", com.google.ads.ab.b.a.c);
        r1.putExtra("com.google.circles.platform.intent.extra.ACTION", r7.get("a"));
        a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        com.google.ads.util.a.a("Launching Google+ intent from AdActivity.");
        startActivityForResult(r1, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00ca, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00cb, code lost:
        a(r0.getMessage(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00d9, code lost:
        if (r0.equals("intent") == false) goto L_0x0134;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00db, code lost:
        if (r7 != null) goto L_0x00e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00dd, code lost:
        a("Could not get the paramMap in launchIntent()");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00e4, code lost:
        r0 = r7.get(com.google.ads.AdActivity.URL_PARAM);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ec, code lost:
        if (r0 != null) goto L_0x00f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00ee, code lost:
        a("Could not get the URL parameter in launchIntent().");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00f5, code lost:
        r1 = r7.get(com.google.ads.AdActivity.INTENT_ACTION_PARAM);
        r2 = r7.get(com.google.ads.AdActivity.TYPE_PARAM);
        r3 = android.net.Uri.parse(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0109, code lost:
        if (r1 != null) goto L_0x012e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x010b, code lost:
        r0 = new android.content.Intent("android.intent.action.VIEW", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0112, code lost:
        if (r2 == null) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0114, code lost:
        r0.setDataAndType(r3, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0117, code lost:
        a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        com.google.ads.util.a.a("Launching an intent from AdActivity.");
        startActivity(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0124, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0125, code lost:
        a(r0.getMessage(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x012e, code lost:
        r0 = new android.content.Intent(r1, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0134, code lost:
        r9.i = new android.widget.RelativeLayout(getApplicationContext());
        r9.i.setGravity(17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x014c, code lost:
        if (r0.equals("webapp") == false) goto L_0x01cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x014e, code lost:
        r9.f = new com.google.ads.h(getApplicationContext(), null);
        r0 = new com.google.ads.i(r6, com.google.ads.a.b, true, true);
        r0.c();
        r9.f.setWebViewClient(r0);
        r0 = r7.get(com.google.ads.AdActivity.URL_PARAM);
        r1 = r7.get(com.google.ads.AdActivity.BASE_URL_PARAM);
        r2 = r7.get(com.google.ads.AdActivity.HTML_PARAM);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0180, code lost:
        if (r0 == null) goto L_0x01a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0182, code lost:
        r9.f.loadUrl(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0187, code lost:
        r0 = r7.get(com.google.ads.AdActivity.ORIENTATION_PARAM);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0195, code lost:
        if ("p".equals(r0) == false) goto L_0x01b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0197, code lost:
        r0 = com.google.ads.util.AdUtil.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x019b, code lost:
        a(r9.f, false, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01a2, code lost:
        if (r2 == null) goto L_0x01ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01a4, code lost:
        r9.f.loadDataWithBaseURL(r1, r2, "text/html", "utf-8", null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x01ae, code lost:
        a("Could not get the URL or HTML parameter to show a web app.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01bb, code lost:
        if ("l".equals(r0) == false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x01bd, code lost:
        r0 = com.google.ads.util.AdUtil.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01c4, code lost:
        if (r9 != com.google.ads.AdActivity.d) goto L_0x01cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01c6, code lost:
        r0 = r6.m();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01cb, code lost:
        r0 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01d3, code lost:
        if (r0.equals("interstitial") == false) goto L_0x01e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x01d5, code lost:
        r9.f = r6.i();
        a(r9.f, true, r6.m());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01e6, code lost:
        a("Unknown AdOpener, <action: " + r0 + ">");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
            r9 = this;
            r3 = 1
            r5 = 0
            r8 = 0
            super.onCreate(r10)
            r9.g = r8
            java.lang.Object r1 = com.google.ads.AdActivity.a
            monitor-enter(r1)
            com.google.ads.d r0 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0061 }
            if (r0 == 0) goto L_0x005a
            com.google.ads.d r6 = com.google.ads.AdActivity.c     // Catch:{ all -> 0x0061 }
            com.google.ads.AdActivity r0 = com.google.ads.AdActivity.d     // Catch:{ all -> 0x0061 }
            if (r0 != 0) goto L_0x001a
            com.google.ads.AdActivity.d = r9     // Catch:{ all -> 0x0061 }
            r6.s()     // Catch:{ all -> 0x0061 }
        L_0x001a:
            com.google.ads.AdActivity r0 = r9.j     // Catch:{ all -> 0x0061 }
            if (r0 != 0) goto L_0x0026
            com.google.ads.AdActivity r0 = com.google.ads.AdActivity.e     // Catch:{ all -> 0x0061 }
            if (r0 == 0) goto L_0x0026
            com.google.ads.AdActivity r0 = com.google.ads.AdActivity.e     // Catch:{ all -> 0x0061 }
            r9.j = r0     // Catch:{ all -> 0x0061 }
        L_0x0026:
            com.google.ads.AdActivity.e = r9     // Catch:{ all -> 0x0061 }
            com.google.ads.Ad r0 = r6.f()     // Catch:{ all -> 0x0061 }
            boolean r2 = r0 instanceof com.google.ads.AdView     // Catch:{ all -> 0x0061 }
            if (r2 == 0) goto L_0x0034
            com.google.ads.AdActivity r2 = com.google.ads.AdActivity.d     // Catch:{ all -> 0x0061 }
            if (r2 == r9) goto L_0x003e
        L_0x0034:
            boolean r0 = r0 instanceof com.google.ads.InterstitialAd     // Catch:{ all -> 0x0061 }
            if (r0 == 0) goto L_0x0041
            com.google.ads.AdActivity r0 = r9.j     // Catch:{ all -> 0x0061 }
            com.google.ads.AdActivity r2 = com.google.ads.AdActivity.d     // Catch:{ all -> 0x0061 }
            if (r0 != r2) goto L_0x0041
        L_0x003e:
            r6.u()     // Catch:{ all -> 0x0061 }
        L_0x0041:
            monitor-exit(r1)     // Catch:{ all -> 0x0061 }
            r9.i = r5
            r9.k = r8
            r9.l = r5
            android.content.Intent r0 = r9.getIntent()
            java.lang.String r1 = "com.google.ads.AdOpener"
            android.os.Bundle r0 = r0.getBundleExtra(r1)
            if (r0 != 0) goto L_0x0064
            java.lang.String r0 = "Could not get the Bundle used to create AdActivity."
            r9.a(r0)
        L_0x0059:
            return
        L_0x005a:
            java.lang.String r0 = "Could not get currentAdManager."
            r9.a(r0)     // Catch:{ all -> 0x0061 }
            monitor-exit(r1)     // Catch:{ all -> 0x0061 }
            goto L_0x0059
        L_0x0061:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0064:
            com.google.ads.e r1 = new com.google.ads.e
            r1.<init>(r0)
            java.lang.String r0 = r1.b()
            java.util.HashMap r7 = r1.c()
            java.lang.String r1 = "plusone"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x00d3
            android.content.Intent r1 = new android.content.Intent
            r1.<init>()
            android.content.ComponentName r0 = new android.content.ComponentName
            java.lang.String r2 = "com.google.android.apps.plus"
            java.lang.String r3 = "com.google.android.apps.circles.platform.PlusOneActivity"
            r0.<init>(r2, r3)
            r1.setComponent(r0)
            java.lang.String r0 = "android.intent.category.LAUNCHER"
            r1.addCategory(r0)
            android.content.Intent r0 = r9.getIntent()
            android.os.Bundle r0 = r0.getExtras()
            r1.putExtras(r0)
            java.lang.String r2 = "com.google.circles.platform.intent.extra.ENTITY"
            java.lang.String r0 = "u"
            java.lang.Object r0 = r7.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            r1.putExtra(r2, r0)
            java.lang.String r0 = "com.google.circles.platform.intent.extra.ENTITY_TYPE"
            com.google.ads.ab$b r2 = com.google.ads.ab.b.AD
            java.lang.String r2 = r2.c
            r1.putExtra(r0, r2)
            java.lang.String r2 = "com.google.circles.platform.intent.extra.ACTION"
            java.lang.String r0 = "a"
            java.lang.Object r0 = r7.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            r1.putExtra(r2, r0)
            r9.a(r6)
            java.lang.String r0 = "Launching Google+ intent from AdActivity."
            com.google.ads.util.a.a(r0)     // Catch:{ ActivityNotFoundException -> 0x00ca }
            r0 = 0
            r9.startActivityForResult(r1, r0)     // Catch:{ ActivityNotFoundException -> 0x00ca }
            goto L_0x0059
        L_0x00ca:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            r9.a(r1, r0)
            goto L_0x0059
        L_0x00d3:
            java.lang.String r1 = "intent"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0134
            if (r7 != 0) goto L_0x00e4
            java.lang.String r0 = "Could not get the paramMap in launchIntent()"
            r9.a(r0)
            goto L_0x0059
        L_0x00e4:
            java.lang.String r0 = "u"
            java.lang.Object r0 = r7.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 != 0) goto L_0x00f5
            java.lang.String r0 = "Could not get the URL parameter in launchIntent()."
            r9.a(r0)
            goto L_0x0059
        L_0x00f5:
            java.lang.String r1 = "i"
            java.lang.Object r1 = r7.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "m"
            java.lang.Object r2 = r7.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            android.net.Uri r3 = android.net.Uri.parse(r0)
            if (r1 != 0) goto L_0x012e
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "android.intent.action.VIEW"
            r0.<init>(r1, r3)
        L_0x0112:
            if (r2 == 0) goto L_0x0117
            r0.setDataAndType(r3, r2)
        L_0x0117:
            r9.a(r6)
            java.lang.String r1 = "Launching an intent from AdActivity."
            com.google.ads.util.a.a(r1)     // Catch:{ ActivityNotFoundException -> 0x0124 }
            r9.startActivity(r0)     // Catch:{ ActivityNotFoundException -> 0x0124 }
            goto L_0x0059
        L_0x0124:
            r0 = move-exception
            java.lang.String r1 = r0.getMessage()
            r9.a(r1, r0)
            goto L_0x0059
        L_0x012e:
            android.content.Intent r0 = new android.content.Intent
            r0.<init>(r1, r3)
            goto L_0x0112
        L_0x0134:
            android.widget.RelativeLayout r1 = new android.widget.RelativeLayout
            android.content.Context r2 = r9.getApplicationContext()
            r1.<init>(r2)
            r9.i = r1
            android.widget.RelativeLayout r1 = r9.i
            r2 = 17
            r1.setGravity(r2)
            java.lang.String r1 = "webapp"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x01cd
            com.google.ads.h r0 = new com.google.ads.h
            android.content.Context r1 = r9.getApplicationContext()
            r0.<init>(r1, r5)
            r9.f = r0
            com.google.ads.i r0 = new com.google.ads.i
            java.util.Map<java.lang.String, com.google.ads.j> r1 = com.google.ads.a.b
            r0.<init>(r6, r1, r3, r3)
            r0.c()
            com.google.ads.h r1 = r9.f
            r1.setWebViewClient(r0)
            java.lang.String r0 = "u"
            java.lang.Object r0 = r7.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r1 = "baseurl"
            java.lang.Object r1 = r7.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "html"
            java.lang.Object r2 = r7.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            if (r0 == 0) goto L_0x01a2
            com.google.ads.h r1 = r9.f
            r1.loadUrl(r0)
        L_0x0187:
            java.lang.String r0 = "o"
            java.lang.Object r0 = r7.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r1 = "p"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x01b5
            int r0 = com.google.ads.util.AdUtil.b()
        L_0x019b:
            com.google.ads.h r1 = r9.f
            r9.a(r1, r8, r0)
            goto L_0x0059
        L_0x01a2:
            if (r2 == 0) goto L_0x01ae
            com.google.ads.h r0 = r9.f
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "utf-8"
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)
            goto L_0x0187
        L_0x01ae:
            java.lang.String r0 = "Could not get the URL or HTML parameter to show a web app."
            r9.a(r0)
            goto L_0x0059
        L_0x01b5:
            java.lang.String r1 = "l"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x01c2
            int r0 = com.google.ads.util.AdUtil.a()
            goto L_0x019b
        L_0x01c2:
            com.google.ads.AdActivity r0 = com.google.ads.AdActivity.d
            if (r9 != r0) goto L_0x01cb
            int r0 = r6.m()
            goto L_0x019b
        L_0x01cb:
            r0 = -1
            goto L_0x019b
        L_0x01cd:
            java.lang.String r1 = "interstitial"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x01e6
            com.google.ads.h r0 = r6.i()
            r9.f = r0
            int r0 = r6.m()
            com.google.ads.h r1 = r9.f
            r9.a(r1, r3, r0)
            goto L_0x0059
        L_0x01e6:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown AdOpener, <action: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = ">"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r9.a(r0)
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.onCreate(android.os.Bundle):void");
    }

    private void a(h hVar, boolean z, int i2) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        if (hVar.getParent() != null) {
            a("Interstitial created with an AdWebView that has a parent.");
        } else if (hVar.b() != null) {
            a("Interstitial created with an AdWebView that is already in use by another AdActivity.");
        } else {
            setRequestedOrientation(i2);
            hVar.a(this);
            ImageButton imageButton = new ImageButton(getApplicationContext());
            imageButton.setImageResource(17301527);
            imageButton.setBackgroundColor(0);
            imageButton.setOnClickListener(this);
            imageButton.setPadding(0, 0, 0, 0);
            int applyDimension = (int) TypedValue.applyDimension(1, 32.0f, getResources().getDisplayMetrics());
            FrameLayout frameLayout = new FrameLayout(getApplicationContext());
            frameLayout.addView(imageButton, applyDimension, applyDimension);
            this.i.addView(hVar, -1, -1);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(10);
            layoutParams.addRule(9);
            this.i.addView(frameLayout, layoutParams);
            this.i.setKeepScreenOn(true);
            setContentView(this.i);
            if (z) {
                a.a(hVar);
            }
        }
    }

    public void onDestroy() {
        if (this.i != null) {
            this.i.removeAllViews();
        }
        if (isFinishing()) {
            a();
            if (this.f != null) {
                this.f.stopLoading();
                this.f.destroy();
                this.f = null;
            }
        }
        super.onDestroy();
    }

    public void onPause() {
        if (isFinishing()) {
            a();
        }
        super.onPause();
    }

    private void a() {
        if (!this.g) {
            if (this.f != null) {
                a.b(this.f);
                this.f.a(null);
            }
            if (this.l != null) {
                this.l.d();
                this.l = null;
            }
            if (this == b) {
                b = null;
            }
            e = this.j;
            synchronized (a) {
                if (!(c == null || this.f == null)) {
                    if (this.f == c.i()) {
                        c.a();
                    }
                    this.f.stopLoading();
                }
                if (this == d) {
                    d = null;
                    if (c != null) {
                        c.r();
                        c = null;
                    } else {
                        a.e("currentAdManager is null while trying to destroy AdActivity.");
                    }
                }
            }
            this.g = true;
            a.a("AdActivity is closing.");
        }
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        if (this.k && hasFocus && SystemClock.elapsedRealtime() - this.h > 250) {
            a.d("Launcher AdActivity got focus and is closing.");
            finish();
        }
        super.onWindowFocusChanged(hasFocus);
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        super.onActivityResult(reqCode, resCode, data);
        if (!(getOpeningAdWebView() == null || data == null || data.getExtras() == null || data.getExtras().getString("com.google.circles.platform.result.extra.CONFIRMATION") == null || data.getExtras().getString("com.google.circles.platform.result.extra.ACTION") == null)) {
            String string = data.getExtras().getString("com.google.circles.platform.result.extra.CONFIRMATION");
            String string2 = data.getExtras().getString("com.google.circles.platform.result.extra.ACTION");
            if (string.equals("yes")) {
                if (string2.equals("insert")) {
                    z.a(getOpeningAdWebView(), true);
                } else if (string2.equals("delete")) {
                    z.a(getOpeningAdWebView(), false);
                }
            }
        }
        finish();
    }
}
