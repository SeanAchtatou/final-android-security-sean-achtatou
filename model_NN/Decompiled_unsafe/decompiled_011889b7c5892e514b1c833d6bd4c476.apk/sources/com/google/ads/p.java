package com.google.ads;

import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.util.a;
import java.util.HashMap;

public final class p implements j {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        a.e("Invalid " + hashMap.get("type") + " request error: " + hashMap.get("errors"));
        c g = dVar.g();
        if (g != null) {
            g.a(AdRequest.ErrorCode.INVALID_REQUEST);
        }
    }
}
