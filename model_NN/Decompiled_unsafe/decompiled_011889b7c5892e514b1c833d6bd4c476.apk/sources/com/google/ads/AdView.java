package com.google.ads;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;

public class AdView extends RelativeLayout implements Ad {
    private d a;

    public AdView(Activity activity, AdSize adSize, String adUnitId) {
        super(activity.getApplicationContext());
        a(activity, adSize, (AttributeSet) null);
        b(activity, adSize, null);
        a(activity, adSize, adUnitId);
    }

    public AdView(Context context, AttributeSet attrs) {
        super(context, attrs);
        a(context, attrs);
    }

    public AdView(Context context, AttributeSet attrs, int i) {
        this(context, attrs);
    }

    private void a(Context context, String str, int i, AdSize adSize, AttributeSet attributeSet) {
        if (getChildCount() == 0) {
            TextView textView = attributeSet == null ? new TextView(context) : new TextView(context, attributeSet);
            textView.setGravity(17);
            textView.setText(str);
            textView.setTextColor(i);
            textView.setBackgroundColor(-16777216);
            LinearLayout linearLayout = attributeSet == null ? new LinearLayout(context) : new LinearLayout(context, attributeSet);
            linearLayout.setGravity(17);
            LinearLayout linearLayout2 = attributeSet == null ? new LinearLayout(context) : new LinearLayout(context, attributeSet);
            linearLayout2.setGravity(17);
            linearLayout2.setBackgroundColor(i);
            int applyDimension = (int) TypedValue.applyDimension(1, (float) adSize.getWidth(), context.getResources().getDisplayMetrics());
            int applyDimension2 = (int) TypedValue.applyDimension(1, (float) adSize.getHeight(), context.getResources().getDisplayMetrics());
            linearLayout.addView(textView, applyDimension - 2, applyDimension2 - 2);
            linearLayout2.addView(linearLayout);
            addView(linearLayout2, applyDimension, applyDimension2);
        }
    }

    private boolean a(Context context, AdSize adSize, AttributeSet attributeSet) {
        if (AdUtil.c(context)) {
            return true;
        }
        a(context, "You must have AdActivity declared in AndroidManifest.xml with configChanges.", adSize, attributeSet);
        return false;
    }

    private boolean b(Context context, AdSize adSize, AttributeSet attributeSet) {
        if (AdUtil.b(context)) {
            return true;
        }
        a(context, "You must have INTERNET and ACCESS_NETWORK_STATE permissions in AndroidManifest.xml.", adSize, attributeSet);
        return false;
    }

    public void destroy() {
        this.a.b();
    }

    private void a(Context context, String str, AdSize adSize, AttributeSet attributeSet) {
        a.b(str);
        a(context, str, -65536, adSize, attributeSet);
        if (isInEditMode()) {
            return;
        }
        if (context instanceof Activity) {
            a((Activity) context, adSize, "");
        } else {
            a.b("AdView was initialized with a Context that wasn't an Activity.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [java.lang.String, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    private void a(Context context, AttributeSet attributeSet) {
        AdSize adSize;
        if (attributeSet != null) {
            String attributeValue = attributeSet.getAttributeValue("http://schemas.android.com/apk/lib/com.google.ads", "adSize");
            if (attributeValue == null) {
                a(context, "AdView missing required XML attribute \"adSize\".", AdSize.BANNER, attributeSet);
                return;
            }
            if ("BANNER".equals(attributeValue)) {
                adSize = AdSize.BANNER;
            } else if ("IAB_MRECT".equals(attributeValue)) {
                adSize = AdSize.IAB_MRECT;
            } else if ("IAB_BANNER".equals(attributeValue)) {
                adSize = AdSize.IAB_BANNER;
            } else if ("IAB_LEADERBOARD".equals(attributeValue)) {
                adSize = AdSize.IAB_LEADERBOARD;
            } else {
                a(context, "Invalid \"adSize\" value in XML layout: " + attributeValue + ".", AdSize.BANNER, attributeSet);
                return;
            }
            String attributeValue2 = attributeSet.getAttributeValue("http://schemas.android.com/apk/lib/com.google.ads", "testDevices");
            if (attributeValue2 != null && attributeValue2.startsWith("@string/")) {
                String substring = attributeValue2.substring("@string/".length());
                String packageName = context.getPackageName();
                TypedValue typedValue = new TypedValue();
                try {
                    getResources().getValue(packageName + ":string/" + substring, typedValue, true);
                    if (typedValue.string != null) {
                        attributeValue2 = typedValue.string.toString();
                    } else {
                        a(context, "\"testDevices\" was not a string: \"" + attributeValue2 + "\".", adSize, attributeSet);
                        return;
                    }
                } catch (Resources.NotFoundException e) {
                    a(context, "Could not find resource for \"testDevices\": \"" + attributeValue2 + "\".", adSize, attributeSet);
                    return;
                }
            }
            String attributeValue3 = attributeSet.getAttributeValue("http://schemas.android.com/apk/lib/com.google.ads", "adUnitId");
            if (attributeValue3 == null) {
                a(context, "AdView missing required XML attribute \"adUnitId\".", adSize, attributeSet);
            } else if (isInEditMode()) {
                a(context, "Ads by Google", -1, adSize, attributeSet);
            } else {
                if (attributeValue3.startsWith("@string/")) {
                    String substring2 = attributeValue3.substring("@string/".length());
                    String packageName2 = context.getPackageName();
                    TypedValue typedValue2 = new TypedValue();
                    try {
                        getResources().getValue(packageName2 + ":string/" + substring2, typedValue2, true);
                        if (typedValue2.string != null) {
                            attributeValue3 = typedValue2.string.toString();
                        } else {
                            a(context, "\"adUnitId\" was not a string: \"" + attributeValue3 + "\".", adSize, attributeSet);
                            return;
                        }
                    } catch (Resources.NotFoundException e2) {
                        a(context, "Could not find resource for \"adUnitId\": \"" + attributeValue3 + "\".", adSize, attributeSet);
                        return;
                    }
                }
                boolean attributeBooleanValue = attributeSet.getAttributeBooleanValue("http://schemas.android.com/apk/lib/com.google.ads", "loadAdOnCreate", false);
                if (context instanceof Activity) {
                    Activity activity = (Activity) context;
                    a(activity, adSize, attributeSet);
                    b(activity, adSize, attributeSet);
                    a(activity, adSize, attributeValue3);
                    if (attributeBooleanValue) {
                        AdRequest adRequest = new AdRequest();
                        if (attributeValue2 != null) {
                            for (String trim : attributeValue2.split(",")) {
                                String trim2 = trim.trim();
                                if (trim2.length() != 0) {
                                    if (trim2.equals("TEST_EMULATOR")) {
                                        adRequest.addTestDevice(AdRequest.TEST_EMULATOR);
                                    } else {
                                        adRequest.addTestDevice(trim2);
                                    }
                                }
                            }
                        }
                        String attributeValue4 = attributeSet.getAttributeValue("http://schemas.android.com/apk/lib/com.google.ads", "keywords");
                        if (attributeValue4 != null) {
                            for (String trim3 : attributeValue4.split(",")) {
                                String trim4 = trim3.trim();
                                if (trim4.length() != 0) {
                                    adRequest.addKeyword(trim4);
                                }
                            }
                        }
                        loadAd(adRequest);
                        return;
                    }
                    return;
                }
                a.b("AdView was initialized with a Context that wasn't an Activity.");
            }
        }
    }

    private void a(Activity activity, AdSize adSize, String str) {
        this.a = new d(activity, this, adSize, str, false);
        setGravity(17);
        setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        addView(this.a.i(), (int) TypedValue.applyDimension(1, (float) adSize.getWidth(), activity.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(1, (float) adSize.getHeight(), activity.getResources().getDisplayMetrics()));
    }

    public boolean isReady() {
        if (this.a == null) {
            return false;
        }
        return this.a.o();
    }

    public boolean isRefreshing() {
        return this.a.p();
    }

    public void loadAd(AdRequest adRequest) {
        if (isRefreshing()) {
            this.a.c();
        }
        this.a.a(adRequest);
    }

    public void setAdListener(AdListener adListener) {
        this.a.a(adListener);
    }

    public void stopLoading() {
        this.a.z();
    }
}
