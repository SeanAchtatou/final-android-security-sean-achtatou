package com.google.ads;

import android.webkit.WebView;
import com.google.ads.util.a;
import java.util.HashMap;

public final class q implements j {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get("url");
        boolean equals = "1".equals(hashMap.get("drt_include"));
        a.c("Received ad url: <\"url\": \"" + str + "\", \"afmaNotifyDt\": \"" + hashMap.get("afma_notify_dt") + "\">");
        c g = dVar.g();
        if (g != null) {
            g.a(equals);
            g.b(str);
        }
    }
}
