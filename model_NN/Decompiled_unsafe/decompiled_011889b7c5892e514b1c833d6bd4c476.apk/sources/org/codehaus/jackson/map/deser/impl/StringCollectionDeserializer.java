package org.codehaus.jackson.map.deser.impl;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Collection;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.TypeDeserializer;
import org.codehaus.jackson.map.annotate.JacksonStdImpl;
import org.codehaus.jackson.map.deser.ContainerDeserializer;
import org.codehaus.jackson.type.JavaType;

@JacksonStdImpl
public final class StringCollectionDeserializer extends ContainerDeserializer<Collection<String>> {
    protected final JavaType _collectionType;
    final Constructor<Collection<String>> _defaultCtor;
    protected final boolean _isDefaultDeserializer;
    protected final JsonDeserializer<String> _valueDeserializer;

    public /* bridge */ /* synthetic */ Object deserialize(JsonParser x0, DeserializationContext x1, Object x2) throws IOException, JsonProcessingException {
        return deserialize(x0, x1, (Collection<String>) ((Collection) x2));
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [org.codehaus.jackson.map.JsonDeserializer<?>, org.codehaus.jackson.map.JsonDeserializer, org.codehaus.jackson.map.JsonDeserializer<java.lang.String>] */
    /* JADX WARN: Type inference failed for: r4v0, types: [java.lang.reflect.Constructor<java.util.Collection<java.lang.String>>, java.lang.reflect.Constructor<?>] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public StringCollectionDeserializer(org.codehaus.jackson.type.JavaType r2, org.codehaus.jackson.map.JsonDeserializer<?> r3, java.lang.reflect.Constructor<?> r4) {
        /*
            r1 = this;
            java.lang.Class r0 = r2.getRawClass()
            r1.<init>(r0)
            r1._collectionType = r2
            r1._valueDeserializer = r3
            r1._defaultCtor = r4
            boolean r0 = r1.isDefaultSerializer(r3)
            r1._isDefaultDeserializer = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.deser.impl.StringCollectionDeserializer.<init>(org.codehaus.jackson.type.JavaType, org.codehaus.jackson.map.JsonDeserializer, java.lang.reflect.Constructor):void");
    }

    public JavaType getContentType() {
        return this._collectionType.getContentType();
    }

    public JsonDeserializer<Object> getContentDeserializer() {
        return this._valueDeserializer;
    }

    public Collection<String> deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        if (!jp.isExpectedStartArrayToken()) {
            throw ctxt.mappingException(this._collectionType.getRawClass());
        }
        try {
            return deserialize(jp, ctxt, this._defaultCtor.newInstance(new Object[0]));
        } catch (Exception e) {
            throw ctxt.instantiationException(this._collectionType.getRawClass(), e);
        }
    }

    public Collection<String> deserialize(JsonParser jp, DeserializationContext ctxt, Collection<String> result) throws IOException, JsonProcessingException {
        if (!this._isDefaultDeserializer) {
            return deserializeUsingCustom(jp, ctxt, result);
        }
        while (true) {
            JsonToken t = jp.nextToken();
            if (t == JsonToken.END_ARRAY) {
                return result;
            }
            result.add(t == JsonToken.VALUE_NULL ? null : jp.getText());
        }
    }

    private Collection<String> deserializeUsingCustom(JsonParser jp, DeserializationContext ctxt, Collection<String> result) throws IOException, JsonProcessingException {
        String value;
        JsonDeserializer<String> deser = this._valueDeserializer;
        while (true) {
            JsonToken t = jp.nextToken();
            if (t == JsonToken.END_ARRAY) {
                return result;
            }
            if (t == JsonToken.VALUE_NULL) {
                value = null;
            } else {
                value = deser.deserialize(jp, ctxt);
            }
            result.add(value);
        }
    }

    public Object deserializeWithType(JsonParser jp, DeserializationContext ctxt, TypeDeserializer typeDeserializer) throws IOException, JsonProcessingException {
        return typeDeserializer.deserializeTypedFromArray(jp, ctxt);
    }
}
