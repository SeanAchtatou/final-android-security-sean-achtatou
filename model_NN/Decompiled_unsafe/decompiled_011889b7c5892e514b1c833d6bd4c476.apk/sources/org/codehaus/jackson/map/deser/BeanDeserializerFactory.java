package org.codehaus.jackson.map.deser;

import java.lang.reflect.Constructor;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.map.AbstractTypeResolver;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.DeserializerFactory;
import org.codehaus.jackson.map.DeserializerProvider;
import org.codehaus.jackson.map.Deserializers;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.KeyDeserializer;
import org.codehaus.jackson.map.TypeDeserializer;
import org.codehaus.jackson.map.deser.SettableBeanProperty;
import org.codehaus.jackson.map.introspect.AnnotatedClass;
import org.codehaus.jackson.map.introspect.AnnotatedConstructor;
import org.codehaus.jackson.map.introspect.AnnotatedField;
import org.codehaus.jackson.map.introspect.AnnotatedMember;
import org.codehaus.jackson.map.introspect.AnnotatedMethod;
import org.codehaus.jackson.map.introspect.AnnotatedParameter;
import org.codehaus.jackson.map.introspect.BasicBeanDescription;
import org.codehaus.jackson.map.introspect.VisibilityChecker;
import org.codehaus.jackson.map.type.ArrayType;
import org.codehaus.jackson.map.type.CollectionType;
import org.codehaus.jackson.map.type.MapType;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.map.util.ArrayBuilders;
import org.codehaus.jackson.map.util.ClassUtil;
import org.codehaus.jackson.type.JavaType;

public class BeanDeserializerFactory extends BasicDeserializerFactory {
    private static final Class<?>[] INIT_CAUSE_PARAMS = {Throwable.class};
    public static final BeanDeserializerFactory instance = new BeanDeserializerFactory(null);
    protected final DeserializerFactory.Config _factoryConfig;

    public static class ConfigImpl extends DeserializerFactory.Config {
        protected static final BeanDeserializerModifier[] NO_MODIFIERS = new BeanDeserializerModifier[0];
        protected final Deserializers[] _additionalDeserializers;
        protected final BeanDeserializerModifier[] _modifiers;

        public ConfigImpl() {
            this(null, null);
        }

        protected ConfigImpl(Deserializers[] allAdditionalDeserializers, BeanDeserializerModifier[] modifiers) {
            this._additionalDeserializers = allAdditionalDeserializers == null ? BeanDeserializerFactory.NO_DESERIALIZERS : allAdditionalDeserializers;
            this._modifiers = modifiers == null ? NO_MODIFIERS : modifiers;
        }

        public DeserializerFactory.Config withAdditionalDeserializers(Deserializers additional) {
            if (additional != null) {
                return new ConfigImpl((Deserializers[]) ArrayBuilders.insertInList(this._additionalDeserializers, additional), this._modifiers);
            }
            throw new IllegalArgumentException("Can not pass null Deserializers");
        }

        public DeserializerFactory.Config withDeserializerModifier(BeanDeserializerModifier modifier) {
            if (modifier == null) {
                throw new IllegalArgumentException("Can not pass null modifier");
            }
            return new ConfigImpl(this._additionalDeserializers, (BeanDeserializerModifier[]) ArrayBuilders.insertInList(this._modifiers, modifier));
        }

        public boolean hasDeserializers() {
            return this._additionalDeserializers.length > 0;
        }

        public boolean hasDeserializerModifiers() {
            return this._modifiers.length > 0;
        }

        public Iterable<Deserializers> deserializers() {
            return ArrayBuilders.arrayAsIterable(this._additionalDeserializers);
        }

        public Iterable<BeanDeserializerModifier> deserializerModifiers() {
            return ArrayBuilders.arrayAsIterable(this._modifiers);
        }
    }

    @Deprecated
    public BeanDeserializerFactory() {
        this(null);
    }

    public BeanDeserializerFactory(DeserializerFactory.Config config) {
        this._factoryConfig = config == null ? new ConfigImpl() : config;
    }

    public final DeserializerFactory.Config getConfig() {
        return this._factoryConfig;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public DeserializerFactory withConfig(DeserializerFactory.Config config) {
        if (this._factoryConfig == config) {
            return this;
        }
        if (getClass() == BeanDeserializerFactory.class) {
            return new BeanDeserializerFactory(config);
        }
        throw new IllegalStateException("Subtype of BeanDeserializerFactory (" + getClass().getName() + ") has not properly overridden method 'withAdditionalDeserializers': can not instantiate subtype with " + "additional deserializer definitions");
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<?> _findCustomArrayDeserializer(ArrayType type, DeserializationConfig config, DeserializerProvider provider, BeanProperty property, TypeDeserializer elementTypeDeserializer, JsonDeserializer<?> elementDeserializer) throws JsonMappingException {
        for (Deserializers d : this._factoryConfig.deserializers()) {
            JsonDeserializer<?> deser = d.findArrayDeserializer(type, config, provider, property, elementTypeDeserializer, elementDeserializer);
            if (deser != null) {
                return deser;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<?> _findCustomCollectionDeserializer(CollectionType type, DeserializationConfig config, DeserializerProvider provider, BasicBeanDescription beanDesc, BeanProperty property, TypeDeserializer elementTypeDeserializer, JsonDeserializer<?> elementDeserializer) throws JsonMappingException {
        for (Deserializers d : this._factoryConfig.deserializers()) {
            JsonDeserializer<?> deser = d.findCollectionDeserializer(type, config, provider, beanDesc, property, elementTypeDeserializer, elementDeserializer);
            if (deser != null) {
                return deser;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<?> _findCustomEnumDeserializer(Class<?> type, DeserializationConfig config, BasicBeanDescription beanDesc, BeanProperty property) throws JsonMappingException {
        for (Deserializers d : this._factoryConfig.deserializers()) {
            JsonDeserializer<?> deser = d.findEnumDeserializer(type, config, beanDesc, property);
            if (deser != null) {
                return deser;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<?> _findCustomMapDeserializer(MapType type, DeserializationConfig config, DeserializerProvider provider, BasicBeanDescription beanDesc, BeanProperty property, KeyDeserializer keyDeserializer, TypeDeserializer elementTypeDeserializer, JsonDeserializer<?> elementDeserializer) throws JsonMappingException {
        for (Deserializers d : this._factoryConfig.deserializers()) {
            JsonDeserializer<?> deser = d.findMapDeserializer(type, config, provider, beanDesc, property, keyDeserializer, elementTypeDeserializer, elementDeserializer);
            if (deser != null) {
                return deser;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<?> _findCustomTreeNodeDeserializer(Class<? extends JsonNode> type, DeserializationConfig config, BeanProperty property) throws JsonMappingException {
        for (Deserializers d : this._factoryConfig.deserializers()) {
            JsonDeserializer<?> deser = d.findTreeNodeDeserializer(type, config, property);
            if (deser != null) {
                return deser;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> _findCustomBeanDeserializer(JavaType type, DeserializationConfig config, DeserializerProvider provider, BasicBeanDescription beanDesc, BeanProperty property) throws JsonMappingException {
        for (Deserializers d : this._factoryConfig.deserializers()) {
            JsonDeserializer<?> deser = d.findBeanDeserializer(type, config, provider, beanDesc, property);
            if (deser != null) {
                return deser;
            }
        }
        return null;
    }

    public JsonDeserializer<Object> createBeanDeserializer(DeserializationConfig config, DeserializerProvider p, JavaType type, BeanProperty property) throws JsonMappingException {
        JavaType concrete;
        BasicBeanDescription beanDesc = (BasicBeanDescription) config.introspect(type);
        JsonDeserializer<Object> ad = findDeserializerFromAnnotation(config, beanDesc.getClassInfo(), property);
        if (ad != null) {
            return ad;
        }
        JavaType newType = modifyTypeByAnnotation(config, beanDesc.getClassInfo(), type, null);
        if (newType.getRawClass() != type.getRawClass()) {
            type = newType;
            beanDesc = (BasicBeanDescription) config.introspect(type);
        }
        JsonDeserializer<Object> custom = _findCustomBeanDeserializer(type, config, p, beanDesc, property);
        if (custom != null) {
            return custom;
        }
        JsonDeserializer<Object> deser = super.createBeanDeserializer(config, p, type, property);
        if (deser != null) {
            return deser;
        }
        if (!isPotentialBeanType(type.getRawClass())) {
            return null;
        }
        if (type.isThrowable()) {
            return buildThrowableDeserializer(config, type, beanDesc, property);
        }
        if (!type.isAbstract()) {
            return buildBeanDeserializer(config, type, beanDesc, property);
        }
        AbstractTypeResolver res = config.getAbstractTypeResolver();
        if (res == null || config.getAnnotationIntrospector().findTypeResolver(beanDesc.getClassInfo(), type) != null || (concrete = res.resolveAbstractType(config, type)) == null) {
            return new AbstractDeserializer(type);
        }
        return buildBeanDeserializer(config, concrete, (BasicBeanDescription) config.introspect(concrete), property);
    }

    public JsonDeserializer<Object> buildBeanDeserializer(DeserializationConfig config, JavaType type, BasicBeanDescription beanDesc, BeanProperty property) throws JsonMappingException {
        BeanDeserializerBuilder builder = constructBeanDeserializerBuilder(beanDesc);
        builder.setCreators(findDeserializerCreators(config, beanDesc));
        addBeanProps(config, beanDesc, builder);
        addReferenceProperties(config, beanDesc, builder);
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (BeanDeserializerModifier mod : this._factoryConfig.deserializerModifiers()) {
                builder = mod.updateBuilder(config, beanDesc, builder);
            }
        }
        JsonDeserializer<?> deserializer = builder.build(property);
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (BeanDeserializerModifier mod2 : this._factoryConfig.deserializerModifiers()) {
                deserializer = mod2.modifyDeserializer(config, beanDesc, deserializer);
            }
        }
        return deserializer;
    }

    public JsonDeserializer<Object> buildThrowableDeserializer(DeserializationConfig config, JavaType type, BasicBeanDescription beanDesc, BeanProperty property) throws JsonMappingException {
        SettableBeanProperty prop;
        BeanDeserializerBuilder builder = constructBeanDeserializerBuilder(beanDesc);
        builder.setCreators(findDeserializerCreators(config, beanDesc));
        addBeanProps(config, beanDesc, builder);
        AnnotatedMethod am = beanDesc.findMethod("initCause", INIT_CAUSE_PARAMS);
        if (!(am == null || (prop = constructSettableProperty(config, beanDesc, "cause", am)) == null)) {
            builder.addProperty(prop);
        }
        builder.addIgnorable("localizedMessage");
        builder.addIgnorable("message");
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (BeanDeserializerModifier mod : this._factoryConfig.deserializerModifiers()) {
                builder = mod.updateBuilder(config, beanDesc, builder);
            }
        }
        JsonDeserializer<?> deserializer = builder.build(property);
        if (deserializer instanceof BeanDeserializer) {
            deserializer = new ThrowableDeserializer((BeanDeserializer) deserializer);
        }
        if (this._factoryConfig.hasDeserializerModifiers()) {
            for (BeanDeserializerModifier mod2 : this._factoryConfig.deserializerModifiers()) {
                deserializer = mod2.modifyDeserializer(config, beanDesc, deserializer);
            }
        }
        return deserializer;
    }

    /* access modifiers changed from: protected */
    public BeanDeserializerBuilder constructBeanDeserializerBuilder(BasicBeanDescription beanDesc) {
        return new BeanDeserializerBuilder(beanDesc);
    }

    /* access modifiers changed from: protected */
    public CreatorContainer findDeserializerCreators(DeserializationConfig config, BasicBeanDescription beanDesc) throws JsonMappingException {
        Constructor<?> defaultCtor;
        boolean fixAccess = config.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS);
        CreatorContainer creators = new CreatorContainer(beanDesc.getBeanClass(), fixAccess);
        AnnotationIntrospector intr = config.getAnnotationIntrospector();
        if (beanDesc.getType().isConcrete() && (defaultCtor = beanDesc.findDefaultConstructor()) != null) {
            if (fixAccess) {
                ClassUtil.checkAndFixAccess(defaultCtor);
            }
            creators.setDefaultConstructor(defaultCtor);
        }
        VisibilityChecker defaultVisibilityChecker = config.getDefaultVisibilityChecker();
        if (!config.isEnabled(DeserializationConfig.Feature.AUTO_DETECT_CREATORS)) {
            defaultVisibilityChecker = defaultVisibilityChecker.withCreatorVisibility(JsonAutoDetect.Visibility.NONE);
        }
        VisibilityChecker<?> vchecker = config.getAnnotationIntrospector().findAutoDetectVisibility(beanDesc.getClassInfo(), defaultVisibilityChecker);
        _addDeserializerConstructors(config, beanDesc, vchecker, intr, creators);
        _addDeserializerFactoryMethods(config, beanDesc, vchecker, intr, creators);
        return creators;
    }

    /* access modifiers changed from: protected */
    public void _addDeserializerConstructors(DeserializationConfig config, BasicBeanDescription beanDesc, VisibilityChecker<?> vchecker, AnnotationIntrospector intr, CreatorContainer creators) throws JsonMappingException {
        for (AnnotatedConstructor ctor : beanDesc.getConstructors()) {
            int argCount = ctor.getParameterCount();
            if (argCount >= 1) {
                boolean isCreator = intr.hasCreatorAnnotation(ctor);
                if (argCount == 1) {
                    String name = intr.findPropertyNameForParam(ctor.getParameter(0));
                    if (name == null || name.length() == 0) {
                        Class<?> type = ctor.getParameterClass(0);
                        if (type == String.class) {
                            if (isCreator || vchecker.isCreatorVisible(ctor)) {
                                creators.addStringConstructor(ctor);
                            }
                        } else if (type == Integer.TYPE || type == Integer.class) {
                            if (isCreator || vchecker.isCreatorVisible(ctor)) {
                                creators.addIntConstructor(ctor);
                            }
                        } else if (type == Long.TYPE || type == Long.class) {
                            if (isCreator || vchecker.isCreatorVisible(ctor)) {
                                creators.addLongConstructor(ctor);
                            }
                        } else if (intr.hasCreatorAnnotation(ctor)) {
                            creators.addDelegatingConstructor(ctor);
                        }
                    }
                } else if (!intr.hasCreatorAnnotation(ctor)) {
                    continue;
                }
                SettableBeanProperty[] properties = new SettableBeanProperty[argCount];
                for (int i = 0; i < argCount; i++) {
                    AnnotatedParameter param = ctor.getParameter(i);
                    String name2 = param == null ? null : intr.findPropertyNameForParam(param);
                    if (name2 == null || name2.length() == 0) {
                        throw new IllegalArgumentException("Argument #" + i + " of constructor " + ctor + " has no property name annotation; must have name when multiple-paramater constructor annotated as Creator");
                    }
                    properties[i] = constructCreatorProperty(config, beanDesc, name2, i, param);
                }
                creators.addPropertyConstructor(ctor, properties);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _addDeserializerFactoryMethods(DeserializationConfig config, BasicBeanDescription beanDesc, VisibilityChecker<?> vchecker, AnnotationIntrospector intr, CreatorContainer creators) throws JsonMappingException {
        for (AnnotatedMethod factory : beanDesc.getFactoryMethods()) {
            int argCount = factory.getParameterCount();
            if (argCount >= 1) {
                boolean isCreator = intr.hasCreatorAnnotation(factory);
                if (argCount == 1) {
                    String name = intr.findPropertyNameForParam(factory.getParameter(0));
                    if (name == null || name.length() == 0) {
                        Class<?> type = factory.getParameterClass(0);
                        if (type == String.class) {
                            if (isCreator || vchecker.isCreatorVisible(factory)) {
                                creators.addStringFactory(factory);
                            }
                        } else if (type == Integer.TYPE || type == Integer.class) {
                            if (isCreator || vchecker.isCreatorVisible(factory)) {
                                creators.addIntFactory(factory);
                            }
                        } else if (type == Long.TYPE || type == Long.class) {
                            if (isCreator || vchecker.isCreatorVisible(factory)) {
                                creators.addLongFactory(factory);
                            }
                        } else if (intr.hasCreatorAnnotation(factory)) {
                            creators.addDelegatingFactory(factory);
                        }
                    }
                } else if (!intr.hasCreatorAnnotation(factory)) {
                    continue;
                }
                SettableBeanProperty[] properties = new SettableBeanProperty[argCount];
                for (int i = 0; i < argCount; i++) {
                    AnnotatedParameter param = factory.getParameter(i);
                    String name2 = intr.findPropertyNameForParam(param);
                    if (name2 == null || name2.length() == 0) {
                        throw new IllegalArgumentException("Argument #" + i + " of factory method " + factory + " has no property name annotation; must have when multiple-paramater static method annotated as Creator");
                    }
                    properties[i] = constructCreatorProperty(config, beanDesc, name2, i, param);
                }
                creators.addPropertyFactory(factory, properties);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void addBeanProps(DeserializationConfig config, BasicBeanDescription beanDesc, BeanDeserializerBuilder builder) throws JsonMappingException {
        VisibilityChecker defaultVisibilityChecker = config.getDefaultVisibilityChecker();
        if (!config.isEnabled(DeserializationConfig.Feature.AUTO_DETECT_SETTERS)) {
            defaultVisibilityChecker = defaultVisibilityChecker.withSetterVisibility(JsonAutoDetect.Visibility.NONE);
        }
        if (!config.isEnabled(DeserializationConfig.Feature.AUTO_DETECT_FIELDS)) {
            defaultVisibilityChecker = defaultVisibilityChecker.withFieldVisibility(JsonAutoDetect.Visibility.NONE);
        }
        VisibilityChecker<?> vchecker = config.getAnnotationIntrospector().findAutoDetectVisibility(beanDesc.getClassInfo(), defaultVisibilityChecker);
        Map<String, AnnotatedMethod> setters = beanDesc.findSetters(vchecker);
        AnnotatedMethod anySetter = beanDesc.findAnySetter();
        AnnotationIntrospector intr = config.getAnnotationIntrospector();
        Boolean B = intr.findIgnoreUnknownProperties(beanDesc.getClassInfo());
        if (B != null) {
            builder.setIgnoreUnknownProperties(B.booleanValue());
        }
        HashSet<String> ignored = ArrayBuilders.arrayToSet(intr.findPropertiesToIgnore(beanDesc.getClassInfo()));
        Iterator i$ = ignored.iterator();
        while (i$.hasNext()) {
            builder.addIgnorable((String) i$.next());
        }
        AnnotatedClass ac = beanDesc.getClassInfo();
        for (AnnotatedMethod am : ac.ignoredMemberMethods()) {
            String name = beanDesc.okNameForSetter(am);
            if (name != null) {
                builder.addIgnorable(name);
            }
        }
        for (AnnotatedField af : ac.ignoredFields()) {
            builder.addIgnorable(af.getName());
        }
        HashMap<Class<?>, Boolean> ignoredTypes = new HashMap<>();
        for (Map.Entry<String, AnnotatedMethod> en : setters.entrySet()) {
            String name2 = (String) en.getKey();
            if (!ignored.contains(name2)) {
                AnnotatedMethod setter = (AnnotatedMethod) en.getValue();
                if (isIgnorableType(config, beanDesc, setter.getParameterClass(0), ignoredTypes)) {
                    builder.addIgnorable(name2);
                } else {
                    SettableBeanProperty prop = constructSettableProperty(config, beanDesc, name2, setter);
                    if (prop != null) {
                        builder.addProperty(prop);
                    }
                }
            }
        }
        if (anySetter != null) {
            builder.setAnySetter(constructAnySetter(config, beanDesc, anySetter));
        }
        HashSet<String> addedProps = new HashSet<>(setters.keySet());
        for (Map.Entry<String, AnnotatedField> en2 : beanDesc.findDeserializableFields(vchecker, addedProps).entrySet()) {
            String name3 = (String) en2.getKey();
            if (!ignored.contains(name3) && !builder.hasProperty(name3)) {
                AnnotatedField field = (AnnotatedField) en2.getValue();
                if (isIgnorableType(config, beanDesc, field.getRawType(), ignoredTypes)) {
                    builder.addIgnorable(name3);
                } else {
                    SettableBeanProperty prop2 = constructSettableProperty(config, beanDesc, name3, field);
                    if (prop2 != null) {
                        builder.addProperty(prop2);
                        addedProps.add(name3);
                    }
                }
            }
        }
        if (config.isEnabled(DeserializationConfig.Feature.USE_GETTERS_AS_SETTERS)) {
            for (Map.Entry<String, AnnotatedMethod> en3 : beanDesc.findGetters(vchecker, addedProps).entrySet()) {
                AnnotatedMethod getter = (AnnotatedMethod) en3.getValue();
                Class<?> rt = getter.getRawType();
                if (Collection.class.isAssignableFrom(rt) || Map.class.isAssignableFrom(rt)) {
                    String name4 = (String) en3.getKey();
                    if (!ignored.contains(name4) && !builder.hasProperty(name4)) {
                        builder.addProperty(constructSetterlessProperty(config, beanDesc, name4, getter));
                        addedProps.add(name4);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void addReferenceProperties(DeserializationConfig config, BasicBeanDescription beanDesc, BeanDeserializerBuilder builder) throws JsonMappingException {
        Map<String, AnnotatedMember> refs = beanDesc.findBackReferenceProperties();
        if (refs != null) {
            for (Map.Entry<String, AnnotatedMember> en : refs.entrySet()) {
                String name = (String) en.getKey();
                AnnotatedMember m = (AnnotatedMember) en.getValue();
                if (m instanceof AnnotatedMethod) {
                    builder.addBackReferenceProperty(name, constructSettableProperty(config, beanDesc, m.getName(), (AnnotatedMethod) m));
                } else {
                    builder.addBackReferenceProperty(name, constructSettableProperty(config, beanDesc, m.getName(), (AnnotatedField) m));
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public SettableAnyProperty constructAnySetter(DeserializationConfig config, BasicBeanDescription beanDesc, AnnotatedMethod setter) throws JsonMappingException {
        if (config.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            setter.fixAccess();
        }
        JavaType type = TypeFactory.type(setter.getParameterType(1), beanDesc.bindingsForBeanType());
        BeanProperty.Std property = new BeanProperty.Std(setter.getName(), type, beanDesc.getClassAnnotations(), setter);
        JavaType type2 = resolveType(config, beanDesc, type, setter, property);
        JsonDeserializer<Object> deser = findDeserializerFromAnnotation(config, setter, property);
        if (deser == null) {
            return new SettableAnyProperty(property, setter, modifyTypeByAnnotation(config, setter, type2, property.getName()));
        }
        SettableAnyProperty prop = new SettableAnyProperty(property, setter, type2);
        prop.setValueDeserializer(deser);
        return prop;
    }

    /* access modifiers changed from: protected */
    public SettableBeanProperty constructSettableProperty(DeserializationConfig config, BasicBeanDescription beanDesc, String name, AnnotatedMethod setter) throws JsonMappingException {
        if (config.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            setter.fixAccess();
        }
        JavaType t0 = TypeFactory.type(setter.getParameterType(0), beanDesc.bindingsForBeanType());
        BeanProperty.Std property = new BeanProperty.Std(name, t0, beanDesc.getClassAnnotations(), setter);
        JavaType type = resolveType(config, beanDesc, t0, setter, property);
        if (type != t0) {
            property = property.withType(type);
        }
        JsonDeserializer<Object> propDeser = findDeserializerFromAnnotation(config, setter, property);
        JavaType type2 = modifyTypeByAnnotation(config, setter, type, name);
        SettableBeanProperty prop = new SettableBeanProperty.MethodProperty(name, type2, (TypeDeserializer) type2.getTypeHandler(), beanDesc.getClassAnnotations(), setter);
        if (propDeser != null) {
            prop.setValueDeserializer(propDeser);
        }
        AnnotationIntrospector.ReferenceProperty ref = config.getAnnotationIntrospector().findReferenceType(setter);
        if (ref != null && ref.isManagedReference()) {
            prop.setManagedReferenceName(ref.getName());
        }
        return prop;
    }

    /* access modifiers changed from: protected */
    public SettableBeanProperty constructSettableProperty(DeserializationConfig config, BasicBeanDescription beanDesc, String name, AnnotatedField field) throws JsonMappingException {
        if (config.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            field.fixAccess();
        }
        JavaType t0 = TypeFactory.type(field.getGenericType(), beanDesc.bindingsForBeanType());
        BeanProperty.Std property = new BeanProperty.Std(name, t0, beanDesc.getClassAnnotations(), field);
        JavaType type = resolveType(config, beanDesc, t0, field, property);
        if (type != t0) {
            property = property.withType(type);
        }
        JsonDeserializer<Object> propDeser = findDeserializerFromAnnotation(config, field, property);
        JavaType type2 = modifyTypeByAnnotation(config, field, type, name);
        SettableBeanProperty prop = new SettableBeanProperty.FieldProperty(name, type2, (TypeDeserializer) type2.getTypeHandler(), beanDesc.getClassAnnotations(), field);
        if (propDeser != null) {
            prop.setValueDeserializer(propDeser);
        }
        AnnotationIntrospector.ReferenceProperty ref = config.getAnnotationIntrospector().findReferenceType(field);
        if (ref != null && ref.isManagedReference()) {
            prop.setManagedReferenceName(ref.getName());
        }
        return prop;
    }

    /* access modifiers changed from: protected */
    public SettableBeanProperty constructSetterlessProperty(DeserializationConfig config, BasicBeanDescription beanDesc, String name, AnnotatedMethod getter) throws JsonMappingException {
        if (config.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            getter.fixAccess();
        }
        JavaType type = getter.getType(beanDesc.bindingsForBeanType());
        JsonDeserializer<Object> propDeser = findDeserializerFromAnnotation(config, getter, new BeanProperty.Std(name, type, beanDesc.getClassAnnotations(), getter));
        JavaType type2 = modifyTypeByAnnotation(config, getter, type, name);
        SettableBeanProperty prop = new SettableBeanProperty.SetterlessProperty(name, type2, (TypeDeserializer) type2.getTypeHandler(), beanDesc.getClassAnnotations(), getter);
        if (propDeser != null) {
            prop.setValueDeserializer(propDeser);
        }
        return prop;
    }

    /* access modifiers changed from: protected */
    public boolean isPotentialBeanType(Class<?> type) {
        String typeStr = ClassUtil.canBeABeanType(type);
        if (typeStr != null) {
            throw new IllegalArgumentException("Can not deserialize Class " + type.getName() + " (of type " + typeStr + ") as a Bean");
        } else if (ClassUtil.isProxyType(type)) {
            throw new IllegalArgumentException("Can not deserialize Proxy class " + type.getName() + " as a Bean");
        } else {
            String typeStr2 = ClassUtil.isLocalType(type);
            if (typeStr2 == null) {
                return true;
            }
            throw new IllegalArgumentException("Can not deserialize Class " + type.getName() + " (of type " + typeStr2 + ") as a Bean");
        }
    }

    /* access modifiers changed from: protected */
    public boolean isIgnorableType(DeserializationConfig config, BasicBeanDescription beanDesc, Class<?> type, Map<Class<?>, Boolean> ignoredTypes) {
        Boolean status = ignoredTypes.get(type);
        if (status == null && (status = config.getAnnotationIntrospector().isIgnorableType(((BasicBeanDescription) config.introspectClassAnnotations(type)).getClassInfo())) == null) {
            status = Boolean.FALSE;
        }
        return status.booleanValue();
    }
}
