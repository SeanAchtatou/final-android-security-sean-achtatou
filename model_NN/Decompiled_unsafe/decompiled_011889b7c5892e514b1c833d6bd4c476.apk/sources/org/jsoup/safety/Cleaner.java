package org.jsoup.safety;

import java.util.Iterator;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.parser.Tag;

public class Cleaner {
    private Whitelist whitelist;

    public Cleaner(Whitelist whitelist2) {
        Validate.notNull(whitelist2);
        this.whitelist = whitelist2;
    }

    public Document clean(Document dirtyDocument) {
        Validate.notNull(dirtyDocument);
        Document clean = Document.createShell(dirtyDocument.baseUri());
        copySafeNodes(dirtyDocument.body(), clean.body());
        return clean;
    }

    public boolean isValid(Document dirtyDocument) {
        Validate.notNull(dirtyDocument);
        return copySafeNodes(dirtyDocument.body(), Document.createShell(dirtyDocument.baseUri()).body()) == 0;
    }

    private int copySafeNodes(Element source, Element dest) {
        int numDiscarded = 0;
        for (Node sourceChild : source.childNodes()) {
            if (sourceChild instanceof Element) {
                Element sourceEl = (Element) sourceChild;
                if (this.whitelist.isSafeTag(sourceEl.tagName())) {
                    ElementMeta meta = createSafeElement(sourceEl);
                    Element destChild = meta.el;
                    dest.appendChild(destChild);
                    numDiscarded = numDiscarded + meta.numAttribsDiscarded + copySafeNodes(sourceEl, destChild);
                } else {
                    numDiscarded = numDiscarded + 1 + copySafeNodes(sourceEl, dest);
                }
            } else if (sourceChild instanceof TextNode) {
                dest.appendChild(new TextNode(((TextNode) sourceChild).getWholeText(), sourceChild.baseUri()));
            }
        }
        return numDiscarded;
    }

    private ElementMeta createSafeElement(Element sourceEl) {
        String sourceTag = sourceEl.tagName();
        Attributes destAttrs = new Attributes();
        Element dest = new Element(Tag.valueOf(sourceTag), sourceEl.baseUri(), destAttrs);
        int numDiscarded = 0;
        Iterator i$ = sourceEl.attributes().iterator();
        while (i$.hasNext()) {
            Attribute sourceAttr = i$.next();
            if (this.whitelist.isSafeAttribute(sourceTag, sourceEl, sourceAttr)) {
                destAttrs.put(sourceAttr);
            } else {
                numDiscarded++;
            }
        }
        destAttrs.addAll(this.whitelist.getEnforcedAttributes(sourceTag));
        return new ElementMeta(dest, numDiscarded);
    }

    private static class ElementMeta {
        Element el;
        int numAttribsDiscarded;

        ElementMeta(Element el2, int numAttribsDiscarded2) {
            this.el = el2;
            this.numAttribsDiscarded = numAttribsDiscarded2;
        }
    }
}
