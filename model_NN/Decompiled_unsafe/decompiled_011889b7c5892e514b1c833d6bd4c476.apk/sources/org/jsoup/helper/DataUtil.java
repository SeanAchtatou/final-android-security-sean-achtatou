package org.jsoup.helper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class DataUtil {
    private static final int bufferSize = 131072;
    private static final Pattern charsetPattern = Pattern.compile("(?i)\\bcharset=\\s*\"?([^\\s;\"]*)");
    static final String defaultCharset = "UTF-8";

    private DataUtil() {
    }

    public static Document load(File in, String charsetName, String baseUri) throws IOException {
        FileInputStream inStream = null;
        try {
            FileInputStream inStream2 = new FileInputStream(in);
            try {
                Document parseByteData = parseByteData(readToByteBuffer(inStream2), charsetName, baseUri);
                if (inStream2 != null) {
                    inStream2.close();
                }
                return parseByteData;
            } catch (Throwable th) {
                th = th;
                inStream = inStream2;
            }
        } catch (Throwable th2) {
            th = th2;
            if (inStream != null) {
                inStream.close();
            }
            throw th;
        }
    }

    public static Document load(InputStream in, String charsetName, String baseUri) throws IOException {
        return parseByteData(readToByteBuffer(in), charsetName, baseUri);
    }

    static Document parseByteData(ByteBuffer byteData, String charsetName, String baseUri) {
        String docData;
        Document doc = null;
        if (charsetName == null) {
            docData = Charset.forName(defaultCharset).decode(byteData).toString();
            doc = Jsoup.parse(docData, baseUri);
            Element meta = doc.select("meta[http-equiv=content-type], meta[charset]").first();
            if (meta != null) {
                String foundCharset = meta.hasAttr("http-equiv") ? getCharsetFromContentType(meta.attr("content")) : meta.attr("charset");
                if (!(foundCharset == null || foundCharset.length() == 0 || foundCharset.equals(defaultCharset))) {
                    charsetName = foundCharset;
                    byteData.rewind();
                    docData = Charset.forName(foundCharset).decode(byteData).toString();
                    doc = null;
                }
            }
        } else {
            Validate.notEmpty(charsetName, "Must set charset arg to character set of file to parse. Set to null to attempt to detect from HTML");
            docData = Charset.forName(charsetName).decode(byteData).toString();
        }
        if (doc != null) {
            return doc;
        }
        Document doc2 = Jsoup.parse(docData, baseUri);
        doc2.outputSettings().charset(charsetName);
        return doc2;
    }

    static ByteBuffer readToByteBuffer(InputStream inStream) throws IOException {
        byte[] buffer = new byte[bufferSize];
        ByteArrayOutputStream outStream = new ByteArrayOutputStream(bufferSize);
        while (true) {
            int read = inStream.read(buffer);
            if (read == -1) {
                return ByteBuffer.wrap(outStream.toByteArray());
            }
            outStream.write(buffer, 0, read);
        }
    }

    static String getCharsetFromContentType(String contentType) {
        if (contentType == null) {
            return null;
        }
        Matcher m = charsetPattern.matcher(contentType);
        if (m.find()) {
            return m.group(1).trim().toUpperCase();
        }
        return null;
    }
}
