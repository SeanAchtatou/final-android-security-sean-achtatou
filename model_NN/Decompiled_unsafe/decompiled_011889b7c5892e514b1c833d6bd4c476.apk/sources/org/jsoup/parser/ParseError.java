package org.jsoup.parser;

class ParseError {
    private char c;
    private String errorMsg;
    private int pos;
    private Token token;
    private TokeniserState tokeniserState;
    private TreeBuilderState treeBuilderState;

    ParseError(String errorMsg2, char c2, TokeniserState tokeniserState2, int pos2) {
        this.errorMsg = errorMsg2;
        this.c = c2;
        this.tokeniserState = tokeniserState2;
        this.pos = pos2;
    }

    ParseError(String errorMsg2, TokeniserState tokeniserState2, int pos2) {
        this.errorMsg = errorMsg2;
        this.tokeniserState = tokeniserState2;
        this.pos = pos2;
    }

    ParseError(String errorMsg2, int pos2) {
        this.errorMsg = errorMsg2;
        this.pos = pos2;
    }

    ParseError(String errorMsg2, TreeBuilderState treeBuilderState2, Token token2, int pos2) {
        this.errorMsg = errorMsg2;
        this.treeBuilderState = treeBuilderState2;
        this.token = token2;
        this.pos = pos2;
    }

    /* access modifiers changed from: package-private */
    public String getErrorMsg() {
        return this.errorMsg;
    }

    /* access modifiers changed from: package-private */
    public int getPos() {
        return this.pos;
    }
}
