package org.jsoup.parser;

import com.google.ads.AdActivity;
import java.util.Iterator;
import org.codehaus.jackson.impl.JsonWriteContext;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Token;

enum TreeBuilderState {
    Initial {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                return true;
            }
            if (t.isComment()) {
                tb.insert(t.asComment());
                return true;
            } else if (t.isDoctype()) {
                Token.Doctype d = t.asDoctype();
                tb.getDocument().appendChild(new DocumentType(d.getName(), d.getPublicIdentifier(), d.getSystemIdentifier(), tb.getBaseUri()));
                if (d.isForceQuirks()) {
                    tb.getDocument().quirksMode(Document.QuirksMode.quirks);
                }
                tb.transition(BeforeHtml);
                return true;
            } else {
                tb.transition(BeforeHtml);
                return tb.process(t);
            }
        }
    },
    BeforeHtml {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isDoctype()) {
                tb.error(this);
                return false;
            }
            if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (TreeBuilderState.access$100(t)) {
                return true;
            } else {
                if (!t.isStartTag() || !t.asStartTag().name().equals(AdActivity.HTML_PARAM)) {
                    if (t.isEndTag()) {
                        if (StringUtil.in(t.asEndTag().name(), "head", "body", AdActivity.HTML_PARAM, "br")) {
                            return anythingElse(t, tb);
                        }
                    }
                    if (!t.isEndTag()) {
                        return anythingElse(t, tb);
                    }
                    tb.error(this);
                    return false;
                }
                tb.insert(t.asStartTag());
                tb.transition(BeforeHead);
            }
            return true;
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            tb.insert(AdActivity.HTML_PARAM);
            tb.transition(BeforeHead);
            return tb.process(t);
        }
    },
    BeforeHead {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                return true;
            }
            if (t.isComment()) {
                tb.insert(t.asComment());
                return true;
            } else if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isStartTag() && t.asStartTag().name().equals(AdActivity.HTML_PARAM)) {
                return InBody.process(t, tb);
            } else {
                if (!t.isStartTag() || !t.asStartTag().name().equals("head")) {
                    if (t.isEndTag()) {
                        if (StringUtil.in(t.asEndTag().name(), "head", "body", AdActivity.HTML_PARAM, "br")) {
                            tb.process(new Token.StartTag("head"));
                            return tb.process(t);
                        }
                    }
                    if (t.isEndTag()) {
                        tb.error(this);
                        return false;
                    }
                    tb.process(new Token.StartTag("head"));
                    return tb.process(t);
                }
                tb.setHeadElement(tb.insert(t.asStartTag()));
                tb.transition(InHead);
                return true;
            }
        }
    },
    InHead {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                tb.insert(t.asCharacter());
                return true;
            }
            switch (AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType[t.type.ordinal()]) {
                case JsonWriteContext.STATUS_OK_AFTER_COMMA:
                    tb.insert(t.asComment());
                    return true;
                case JsonWriteContext.STATUS_OK_AFTER_COLON:
                    tb.error(this);
                    return false;
                case JsonWriteContext.STATUS_OK_AFTER_SPACE:
                    Token.StartTag start = t.asStartTag();
                    String name = start.name();
                    if (name.equals(AdActivity.HTML_PARAM)) {
                        return InBody.process(t, tb);
                    }
                    if (StringUtil.in(name, "base", "basefont", "bgsound", "command", "link")) {
                        Element el = tb.insertEmpty(start);
                        if (!name.equals("base") || !el.hasAttr("href")) {
                            return true;
                        }
                        tb.setBaseUri(el);
                        return true;
                    } else if (name.equals("meta")) {
                        tb.insertEmpty(start);
                        return true;
                    } else if (name.equals("title")) {
                        TreeBuilderState.access$200(start, tb);
                        return true;
                    } else {
                        if (StringUtil.in(name, "noframes", "style")) {
                            TreeBuilderState.access$300(start, tb);
                            return true;
                        } else if (name.equals("noscript")) {
                            tb.insert(start);
                            tb.transition(InHeadNoscript);
                            return true;
                        } else if (name.equals("script")) {
                            tb.insert(start);
                            tb.tokeniser.transition(TokeniserState.ScriptData);
                            tb.markInsertionMode();
                            tb.transition(Text);
                            return true;
                        } else if (!name.equals("head")) {
                            return anythingElse(t, tb);
                        } else {
                            tb.error(this);
                            return false;
                        }
                    }
                case JsonWriteContext.STATUS_EXPECT_VALUE:
                    String name2 = t.asEndTag().name();
                    if (name2.equals("head")) {
                        tb.pop();
                        tb.transition(AfterHead);
                        return true;
                    }
                    if (StringUtil.in(name2, "body", AdActivity.HTML_PARAM, "br")) {
                        return anythingElse(t, tb);
                    }
                    tb.error(this);
                    return false;
                default:
                    return anythingElse(t, tb);
            }
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            tb.process(new Token.EndTag("head"));
            return tb.process(t);
        }
    },
    InHeadNoscript {
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0086, code lost:
            if (org.jsoup.helper.StringUtil.in(r8.asStartTag().name(), "basefont", "bgsound", "link", "meta", "noframes", "style") != false) goto L_0x0088;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00c8, code lost:
            if (org.jsoup.helper.StringUtil.in(r8.asStartTag().name(), "head", "noscript") == false) goto L_0x00ca;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean process(org.jsoup.parser.Token r8, org.jsoup.parser.TreeBuilder r9) {
            /*
                r7 = this;
                r6 = 2
                r1 = 1
                r0 = 0
                boolean r2 = r8.isDoctype()
                if (r2 == 0) goto L_0x000e
                r9.error(r7)
            L_0x000c:
                r0 = r1
            L_0x000d:
                return r0
            L_0x000e:
                boolean r2 = r8.isStartTag()
                if (r2 == 0) goto L_0x002b
                org.jsoup.parser.Token$StartTag r2 = r8.asStartTag()
                java.lang.String r2 = r2.name()
                java.lang.String r3 = "html"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x002b
                org.jsoup.parser.TreeBuilderState r0 = org.jsoup.parser.TreeBuilderState.AnonymousClass5.InBody
                boolean r0 = r9.process(r8, r0)
                goto L_0x000d
            L_0x002b:
                boolean r2 = r8.isEndTag()
                if (r2 == 0) goto L_0x004a
                org.jsoup.parser.Token$EndTag r2 = r8.asEndTag()
                java.lang.String r2 = r2.name()
                java.lang.String r3 = "noscript"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x004a
                r9.pop()
                org.jsoup.parser.TreeBuilderState r0 = org.jsoup.parser.TreeBuilderState.AnonymousClass5.InHead
                r9.transition(r0)
                goto L_0x000c
            L_0x004a:
                boolean r2 = org.jsoup.parser.TreeBuilderState.access$100(r8)
                if (r2 != 0) goto L_0x0088
                boolean r2 = r8.isComment()
                if (r2 != 0) goto L_0x0088
                boolean r2 = r8.isStartTag()
                if (r2 == 0) goto L_0x0090
                org.jsoup.parser.Token$StartTag r2 = r8.asStartTag()
                java.lang.String r2 = r2.name()
                r3 = 6
                java.lang.String[] r3 = new java.lang.String[r3]
                java.lang.String r4 = "basefont"
                r3[r0] = r4
                java.lang.String r4 = "bgsound"
                r3[r1] = r4
                java.lang.String r4 = "link"
                r3[r6] = r4
                r4 = 3
                java.lang.String r5 = "meta"
                r3[r4] = r5
                r4 = 4
                java.lang.String r5 = "noframes"
                r3[r4] = r5
                r4 = 5
                java.lang.String r5 = "style"
                r3[r4] = r5
                boolean r2 = org.jsoup.helper.StringUtil.in(r2, r3)
                if (r2 == 0) goto L_0x0090
            L_0x0088:
                org.jsoup.parser.TreeBuilderState r0 = org.jsoup.parser.TreeBuilderState.AnonymousClass5.InHead
                boolean r0 = r9.process(r8, r0)
                goto L_0x000d
            L_0x0090:
                boolean r2 = r8.isEndTag()
                if (r2 == 0) goto L_0x00ac
                org.jsoup.parser.Token$EndTag r2 = r8.asEndTag()
                java.lang.String r2 = r2.name()
                java.lang.String r3 = "br"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x00ac
                boolean r0 = r7.anythingElse(r8, r9)
                goto L_0x000d
            L_0x00ac:
                boolean r2 = r8.isStartTag()
                if (r2 == 0) goto L_0x00ca
                org.jsoup.parser.Token$StartTag r2 = r8.asStartTag()
                java.lang.String r2 = r2.name()
                java.lang.String[] r3 = new java.lang.String[r6]
                java.lang.String r4 = "head"
                r3[r0] = r4
                java.lang.String r4 = "noscript"
                r3[r1] = r4
                boolean r1 = org.jsoup.helper.StringUtil.in(r2, r3)
                if (r1 != 0) goto L_0x00d0
            L_0x00ca:
                boolean r1 = r8.isEndTag()
                if (r1 == 0) goto L_0x00d5
            L_0x00d0:
                r9.error(r7)
                goto L_0x000d
            L_0x00d5:
                boolean r0 = r7.anythingElse(r8, r9)
                goto L_0x000d
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.TreeBuilderState.AnonymousClass5.process(org.jsoup.parser.Token, org.jsoup.parser.TreeBuilder):boolean");
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            tb.error(this);
            tb.process(new Token.EndTag("noscript"));
            return tb.process(t);
        }
    },
    AfterHead {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                tb.insert(t.asCharacter());
            } else if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype()) {
                tb.error(this);
            } else if (t.isStartTag()) {
                Token.StartTag startTag = t.asStartTag();
                String name = startTag.name();
                if (name.equals(AdActivity.HTML_PARAM)) {
                    return tb.process(t, InBody);
                }
                if (name.equals("body")) {
                    tb.insert(startTag);
                    tb.framesetOk(false);
                    tb.transition(InBody);
                } else if (name.equals("frameset")) {
                    tb.insert(startTag);
                    tb.transition(InFrameset);
                } else {
                    if (StringUtil.in(name, "base", "basefont", "bgsound", "link", "meta", "noframes", "script", "style", "title")) {
                        tb.error(this);
                        Element head = tb.getHeadElement();
                        tb.push(head);
                        tb.process(t, InHead);
                        tb.removeFromStack(head);
                    } else if (name.equals("head")) {
                        tb.error(this);
                        return false;
                    } else {
                        anythingElse(t, tb);
                    }
                }
            } else if (t.isEndTag()) {
                if (StringUtil.in(t.asEndTag().name(), "body", AdActivity.HTML_PARAM)) {
                    anythingElse(t, tb);
                } else {
                    tb.error(this);
                    return false;
                }
            } else {
                anythingElse(t, tb);
            }
            return true;
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            tb.process(new Token.StartTag("body"));
            tb.framesetOk(true);
            return tb.process(t);
        }
    },
    InBody {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Element
         arg types: [java.lang.String, java.lang.String]
         candidates:
          org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node
          org.jsoup.nodes.Node.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Node
          org.jsoup.nodes.Element.attr(java.lang.String, java.lang.String):org.jsoup.nodes.Element */
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Removed duplicated region for block: B:365:0x10ec  */
        /* JADX WARNING: Removed duplicated region for block: B:372:0x1131 A[LOOP:9: B:370:0x112b->B:372:0x1131, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:379:0x117e  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean process(org.jsoup.parser.Token r43, org.jsoup.parser.TreeBuilder r44) {
            /*
                r42 = this;
                int[] r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType
                r0 = r43
                org.jsoup.parser.Token$TokenType r0 = r0.type
                r39 = r0
                int r39 = r39.ordinal()
                r38 = r38[r39]
                switch(r38) {
                    case 1: goto L_0x0051;
                    case 2: goto L_0x005d;
                    case 3: goto L_0x0067;
                    case 4: goto L_0x0bf8;
                    case 5: goto L_0x0014;
                    default: goto L_0x0011;
                }
            L_0x0011:
                r38 = 1
            L_0x0013:
                return r38
            L_0x0014:
                org.jsoup.parser.Token$Character r9 = r43.asCharacter()
                java.lang.String r38 = r9.getData()
                java.lang.String r39 = org.jsoup.parser.TreeBuilderState.access$400()
                boolean r38 = r38.equals(r39)
                if (r38 == 0) goto L_0x0030
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0030:
                boolean r38 = org.jsoup.parser.TreeBuilderState.access$100(r9)
                if (r38 == 0) goto L_0x003f
                r44.reconstructFormattingElements()
                r0 = r44
                r0.insert(r9)
                goto L_0x0011
            L_0x003f:
                r44.reconstructFormattingElements()
                r0 = r44
                r0.insert(r9)
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                goto L_0x0011
            L_0x0051:
                org.jsoup.parser.Token$Comment r38 = r43.asComment()
                r0 = r44
                r1 = r38
                r0.insert(r1)
                goto L_0x0011
            L_0x005d:
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0067:
                org.jsoup.parser.Token$StartTag r36 = r43.asStartTag()
                java.lang.String r26 = r36.name()
                java.lang.String r38 = "html"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x00b8
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.helper.DescendableLinkedList r38 = r44.getStack()
                java.lang.Object r19 = r38.getFirst()
                org.jsoup.nodes.Element r19 = (org.jsoup.nodes.Element) r19
                org.jsoup.nodes.Attributes r38 = r36.getAttributes()
                java.util.Iterator r21 = r38.iterator()
            L_0x0094:
                boolean r38 = r21.hasNext()
                if (r38 == 0) goto L_0x0011
                java.lang.Object r7 = r21.next()
                org.jsoup.nodes.Attribute r7 = (org.jsoup.nodes.Attribute) r7
                java.lang.String r38 = r7.getKey()
                r0 = r19
                r1 = r38
                boolean r38 = r0.hasAttr(r1)
                if (r38 != 0) goto L_0x0094
                org.jsoup.nodes.Attributes r38 = r19.attributes()
                r0 = r38
                r0.put(r7)
                goto L_0x0094
            L_0x00b8:
                r38 = 10
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "base"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "basefont"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "bgsound"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "command"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "link"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "meta"
                r38[r39] = r40
                r39 = 6
                java.lang.String r40 = "noframes"
                r38[r39] = r40
                r39 = 7
                java.lang.String r40 = "script"
                r38[r39] = r40
                r39 = 8
                java.lang.String r40 = "style"
                r38[r39] = r40
                r39 = 9
                java.lang.String r40 = "title"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x0114
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InHead
                r0 = r44
                r1 = r43
                r2 = r38
                boolean r38 = r0.process(r1, r2)
                goto L_0x0013
            L_0x0114:
                java.lang.String r38 = "body"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x019e
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.helper.DescendableLinkedList r35 = r44.getStack()
                int r38 = r35.size()
                r39 = 1
                r0 = r38
                r1 = r39
                if (r0 == r1) goto L_0x015b
                int r38 = r35.size()
                r39 = 2
                r0 = r38
                r1 = r39
                if (r0 <= r1) goto L_0x015f
                r38 = 1
                r0 = r35
                r1 = r38
                java.lang.Object r38 = r0.get(r1)
                org.jsoup.nodes.Element r38 = (org.jsoup.nodes.Element) r38
                java.lang.String r38 = r38.nodeName()
                java.lang.String r39 = "body"
                boolean r38 = r38.equals(r39)
                if (r38 != 0) goto L_0x015f
            L_0x015b:
                r38 = 0
                goto L_0x0013
            L_0x015f:
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                r38 = 1
                r0 = r35
                r1 = r38
                java.lang.Object r8 = r0.get(r1)
                org.jsoup.nodes.Element r8 = (org.jsoup.nodes.Element) r8
                org.jsoup.nodes.Attributes r38 = r36.getAttributes()
                java.util.Iterator r21 = r38.iterator()
            L_0x017c:
                boolean r38 = r21.hasNext()
                if (r38 == 0) goto L_0x0011
                java.lang.Object r7 = r21.next()
                org.jsoup.nodes.Attribute r7 = (org.jsoup.nodes.Attribute) r7
                java.lang.String r38 = r7.getKey()
                r0 = r38
                boolean r38 = r8.hasAttr(r0)
                if (r38 != 0) goto L_0x017c
                org.jsoup.nodes.Attributes r38 = r8.attributes()
                r0 = r38
                r0.put(r7)
                goto L_0x017c
            L_0x019e:
                java.lang.String r38 = "frameset"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x022a
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.helper.DescendableLinkedList r35 = r44.getStack()
                int r38 = r35.size()
                r39 = 1
                r0 = r38
                r1 = r39
                if (r0 == r1) goto L_0x01e5
                int r38 = r35.size()
                r39 = 2
                r0 = r38
                r1 = r39
                if (r0 <= r1) goto L_0x01e9
                r38 = 1
                r0 = r35
                r1 = r38
                java.lang.Object r38 = r0.get(r1)
                org.jsoup.nodes.Element r38 = (org.jsoup.nodes.Element) r38
                java.lang.String r38 = r38.nodeName()
                java.lang.String r39 = "body"
                boolean r38 = r38.equals(r39)
                if (r38 != 0) goto L_0x01e9
            L_0x01e5:
                r38 = 0
                goto L_0x0013
            L_0x01e9:
                boolean r38 = r44.framesetOk()
                if (r38 != 0) goto L_0x01f3
                r38 = 0
                goto L_0x0013
            L_0x01f3:
                r38 = 1
                r0 = r35
                r1 = r38
                java.lang.Object r32 = r0.get(r1)
                org.jsoup.nodes.Element r32 = (org.jsoup.nodes.Element) r32
                org.jsoup.nodes.Element r38 = r32.parent()
                if (r38 == 0) goto L_0x0208
                r32.remove()
            L_0x0208:
                int r38 = r35.size()
                r39 = 1
                r0 = r38
                r1 = r39
                if (r0 <= r1) goto L_0x0218
                r35.removeLast()
                goto L_0x0208
            L_0x0218:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InFrameset
                r0 = r44
                r1 = r38
                r0.transition(r1)
                goto L_0x0011
            L_0x022a:
                r38 = 22
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "address"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "article"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "aside"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "blockquote"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "center"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "details"
                r38[r39] = r40
                r39 = 6
                java.lang.String r40 = "dir"
                r38[r39] = r40
                r39 = 7
                java.lang.String r40 = "div"
                r38[r39] = r40
                r39 = 8
                java.lang.String r40 = "dl"
                r38[r39] = r40
                r39 = 9
                java.lang.String r40 = "fieldset"
                r38[r39] = r40
                r39 = 10
                java.lang.String r40 = "figcaption"
                r38[r39] = r40
                r39 = 11
                java.lang.String r40 = "figure"
                r38[r39] = r40
                r39 = 12
                java.lang.String r40 = "footer"
                r38[r39] = r40
                r39 = 13
                java.lang.String r40 = "header"
                r38[r39] = r40
                r39 = 14
                java.lang.String r40 = "hgroup"
                r38[r39] = r40
                r39 = 15
                java.lang.String r40 = "menu"
                r38[r39] = r40
                r39 = 16
                java.lang.String r40 = "nav"
                r38[r39] = r40
                r39 = 17
                java.lang.String r40 = "ol"
                r38[r39] = r40
                r39 = 18
                java.lang.String r40 = "p"
                r38[r39] = r40
                r39 = 19
                java.lang.String r40 = "section"
                r38[r39] = r40
                r39 = 20
                java.lang.String r40 = "summary"
                r38[r39] = r40
                r39 = 21
                java.lang.String r40 = "ul"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x02e3
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x02da
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x02da:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                goto L_0x0011
            L_0x02e3:
                r38 = 6
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "h1"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "h2"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "h3"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "h4"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "h5"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "h6"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x0380
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x0333
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0333:
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r39 = 6
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "h1"
                r39[r40] = r41
                r40 = 1
                java.lang.String r41 = "h2"
                r39[r40] = r41
                r40 = 2
                java.lang.String r41 = "h3"
                r39[r40] = r41
                r40 = 3
                java.lang.String r41 = "h4"
                r39[r40] = r41
                r40 = 4
                java.lang.String r41 = "h5"
                r39[r40] = r41
                r40 = 5
                java.lang.String r41 = "h6"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 == 0) goto L_0x0377
                r0 = r44
                r1 = r42
                r0.error(r1)
                r44.pop()
            L_0x0377:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                goto L_0x0011
            L_0x0380:
                r38 = 2
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "pre"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "listing"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x03ca
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x03b8
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x03b8:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                goto L_0x0011
            L_0x03ca:
                java.lang.String r38 = "form"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0412
                org.jsoup.nodes.Element r38 = r44.getFormElement()
                if (r38 == 0) goto L_0x03e7
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x03e7:
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x0401
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0401:
                r0 = r44
                r1 = r36
                org.jsoup.nodes.Element r16 = r0.insert(r1)
                r0 = r44
                r1 = r16
                r0.setFormElement(r1)
                goto L_0x0011
            L_0x0412:
                java.lang.String r38 = "li"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x04a9
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                org.jsoup.helper.DescendableLinkedList r35 = r44.getStack()
                int r38 = r35.size()
                int r20 = r38 + -1
            L_0x0431:
                if (r20 <= 0) goto L_0x0457
                r0 = r35
                r1 = r20
                java.lang.Object r14 = r0.get(r1)
                org.jsoup.nodes.Element r14 = (org.jsoup.nodes.Element) r14
                java.lang.String r38 = r14.nodeName()
                java.lang.String r39 = "li"
                boolean r38 = r38.equals(r39)
                if (r38 == 0) goto L_0x047a
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "li"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0457:
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x0471
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0471:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                goto L_0x0011
            L_0x047a:
                r0 = r44
                boolean r38 = r0.isSpecial(r14)
                if (r38 == 0) goto L_0x04a6
                java.lang.String r38 = r14.nodeName()
                r39 = 3
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "address"
                r39[r40] = r41
                r40 = 1
                java.lang.String r41 = "div"
                r39[r40] = r41
                r40 = 2
                java.lang.String r41 = "p"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 == 0) goto L_0x0457
            L_0x04a6:
                int r20 = r20 + -1
                goto L_0x0431
            L_0x04a9:
                r38 = 2
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "dd"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "dt"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x0567
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                org.jsoup.helper.DescendableLinkedList r35 = r44.getStack()
                int r38 = r35.size()
                int r20 = r38 + -1
            L_0x04da:
                if (r20 <= 0) goto L_0x0514
                r0 = r35
                r1 = r20
                java.lang.Object r14 = r0.get(r1)
                org.jsoup.nodes.Element r14 = (org.jsoup.nodes.Element) r14
                java.lang.String r38 = r14.nodeName()
                r39 = 2
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "dd"
                r39[r40] = r41
                r40 = 1
                java.lang.String r41 = "dt"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 == 0) goto L_0x0537
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = r14.nodeName()
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0514:
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x052e
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x052e:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                goto L_0x0011
            L_0x0537:
                r0 = r44
                boolean r38 = r0.isSpecial(r14)
                if (r38 == 0) goto L_0x0563
                java.lang.String r38 = r14.nodeName()
                r39 = 3
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "address"
                r39[r40] = r41
                r40 = 1
                java.lang.String r41 = "div"
                r39[r40] = r41
                r40 = 2
                java.lang.String r41 = "p"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 == 0) goto L_0x0514
            L_0x0563:
                int r20 = r20 + -1
                goto L_0x04da
            L_0x0567:
                java.lang.String r38 = "plaintext"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x05a1
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x058d
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x058d:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r0 = r44
                org.jsoup.parser.Tokeniser r0 = r0.tokeniser
                r38 = r0
                org.jsoup.parser.TokeniserState r39 = org.jsoup.parser.TokeniserState.PLAINTEXT
                r38.transition(r39)
                goto L_0x0011
            L_0x05a1:
                java.lang.String r38 = "button"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x05ec
                java.lang.String r38 = "button"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x05d7
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "button"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                r0 = r44
                r1 = r36
                r0.process(r1)
                goto L_0x0011
            L_0x05d7:
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                goto L_0x0011
            L_0x05ec:
                java.lang.String r38 = "a"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0645
                java.lang.String r38 = "a"
                r0 = r44
                r1 = r38
                org.jsoup.nodes.Element r38 = r0.getActiveFormattingElement(r1)
                if (r38 == 0) goto L_0x0633
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "a"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                java.lang.String r38 = "a"
                r0 = r44
                r1 = r38
                org.jsoup.nodes.Element r30 = r0.getFromStack(r1)
                if (r30 == 0) goto L_0x0633
                r0 = r44
                r1 = r30
                r0.removeFromActiveFormattingElements(r1)
                r0 = r44
                r1 = r30
                r0.removeFromStack(r1)
            L_0x0633:
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                org.jsoup.nodes.Element r3 = r0.insert(r1)
                r0 = r44
                r0.pushActiveFormattingElements(r3)
                goto L_0x0011
            L_0x0645:
                r38 = 12
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "b"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "big"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "code"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "em"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "font"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "i"
                r38[r39] = r40
                r39 = 6
                java.lang.String r40 = "s"
                r38[r39] = r40
                r39 = 7
                java.lang.String r40 = "small"
                r38[r39] = r40
                r39 = 8
                java.lang.String r40 = "strike"
                r38[r39] = r40
                r39 = 9
                java.lang.String r40 = "strong"
                r38[r39] = r40
                r39 = 10
                java.lang.String r40 = "tt"
                r38[r39] = r40
                r39 = 11
                java.lang.String r40 = "u"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x06b1
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                org.jsoup.nodes.Element r14 = r0.insert(r1)
                r0 = r44
                r0.pushActiveFormattingElements(r14)
                goto L_0x0011
            L_0x06b1:
                java.lang.String r38 = "nobr"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x06f3
                r44.reconstructFormattingElements()
                java.lang.String r38 = "nobr"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inScope(r1)
                if (r38 == 0) goto L_0x06e4
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "nobr"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                r44.reconstructFormattingElements()
            L_0x06e4:
                r0 = r44
                r1 = r36
                org.jsoup.nodes.Element r14 = r0.insert(r1)
                r0 = r44
                r0.pushActiveFormattingElements(r14)
                goto L_0x0011
            L_0x06f3:
                r38 = 3
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "applet"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "marquee"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "object"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x072f
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r44.insertMarkerToFormattingElements()
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                goto L_0x0011
            L_0x072f:
                java.lang.String r38 = "table"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0780
                org.jsoup.nodes.Document r38 = r44.getDocument()
                org.jsoup.nodes.Document$QuirksMode r38 = r38.quirksMode()
                org.jsoup.nodes.Document$QuirksMode r39 = org.jsoup.nodes.Document.QuirksMode.quirks
                r0 = r38
                r1 = r39
                if (r0 == r1) goto L_0x0765
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x0765
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0765:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InTable
                r0 = r44
                r1 = r38
                r0.transition(r1)
                goto L_0x0011
            L_0x0780:
                r38 = 6
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "area"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "br"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "embed"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "img"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "keygen"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "wbr"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x07cb
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insertEmpty(r1)
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                goto L_0x0011
            L_0x07cb:
                java.lang.String r38 = "input"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x07fd
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                org.jsoup.nodes.Element r14 = r0.insertEmpty(r1)
                java.lang.String r38 = "type"
                r0 = r38
                java.lang.String r38 = r14.attr(r0)
                java.lang.String r39 = "hidden"
                boolean r38 = r38.equalsIgnoreCase(r39)
                if (r38 != 0) goto L_0x0011
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                goto L_0x0011
            L_0x07fd:
                r38 = 3
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "param"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "source"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "track"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x082a
                r0 = r44
                r1 = r36
                r0.insertEmpty(r1)
                goto L_0x0011
            L_0x082a:
                java.lang.String r38 = "hr"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0862
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x0850
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0850:
                r0 = r44
                r1 = r36
                r0.insertEmpty(r1)
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                goto L_0x0011
            L_0x0862:
                java.lang.String r38 = "image"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0881
                java.lang.String r38 = "img"
                r0 = r36
                r1 = r38
                r0.name(r1)
                r0 = r44
                r1 = r36
                boolean r38 = r0.process(r1)
                goto L_0x0013
            L_0x0881:
                java.lang.String r38 = "isindex"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x09b9
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.nodes.Element r38 = r44.getFormElement()
                if (r38 == 0) goto L_0x089e
                r38 = 0
                goto L_0x0013
            L_0x089e:
                r0 = r44
                org.jsoup.parser.Tokeniser r0 = r0.tokeniser
                r38 = r0
                r38.acknowledgeSelfClosingFlag()
                org.jsoup.parser.Token$StartTag r38 = new org.jsoup.parser.Token$StartTag
                java.lang.String r39 = "form"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                r0 = r36
                org.jsoup.nodes.Attributes r0 = r0.attributes
                r38 = r0
                java.lang.String r39 = "action"
                boolean r38 = r38.hasKey(r39)
                if (r38 == 0) goto L_0x08de
                org.jsoup.nodes.Element r16 = r44.getFormElement()
                java.lang.String r38 = "action"
                r0 = r36
                org.jsoup.nodes.Attributes r0 = r0.attributes
                r39 = r0
                java.lang.String r40 = "action"
                java.lang.String r39 = r39.get(r40)
                r0 = r16
                r1 = r38
                r2 = r39
                r0.attr(r1, r2)
            L_0x08de:
                org.jsoup.parser.Token$StartTag r38 = new org.jsoup.parser.Token$StartTag
                java.lang.String r39 = "hr"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                org.jsoup.parser.Token$StartTag r38 = new org.jsoup.parser.Token$StartTag
                java.lang.String r39 = "label"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                r0 = r36
                org.jsoup.nodes.Attributes r0 = r0.attributes
                r38 = r0
                java.lang.String r39 = "prompt"
                boolean r38 = r38.hasKey(r39)
                if (r38 == 0) goto L_0x0969
                r0 = r36
                org.jsoup.nodes.Attributes r0 = r0.attributes
                r38 = r0
                java.lang.String r39 = "prompt"
                java.lang.String r29 = r38.get(r39)
            L_0x0914:
                org.jsoup.parser.Token$Character r38 = new org.jsoup.parser.Token$Character
                r0 = r38
                r1 = r29
                r0.<init>(r1)
                r0 = r44
                r1 = r38
                r0.process(r1)
                org.jsoup.nodes.Attributes r22 = new org.jsoup.nodes.Attributes
                r22.<init>()
                r0 = r36
                org.jsoup.nodes.Attributes r0 = r0.attributes
                r38 = r0
                java.util.Iterator r21 = r38.iterator()
            L_0x0933:
                boolean r38 = r21.hasNext()
                if (r38 == 0) goto L_0x096c
                java.lang.Object r6 = r21.next()
                org.jsoup.nodes.Attribute r6 = (org.jsoup.nodes.Attribute) r6
                java.lang.String r38 = r6.getKey()
                r39 = 3
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "name"
                r39[r40] = r41
                r40 = 1
                java.lang.String r41 = "action"
                r39[r40] = r41
                r40 = 2
                java.lang.String r41 = "prompt"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 != 0) goto L_0x0933
                r0 = r22
                r0.put(r6)
                goto L_0x0933
            L_0x0969:
                java.lang.String r29 = "This is a searchable index. Enter search keywords: "
                goto L_0x0914
            L_0x096c:
                java.lang.String r38 = "name"
                java.lang.String r39 = "isindex"
                r0 = r22
                r1 = r38
                r2 = r39
                r0.put(r1, r2)
                org.jsoup.parser.Token$StartTag r38 = new org.jsoup.parser.Token$StartTag
                java.lang.String r39 = "input"
                r0 = r38
                r1 = r39
                r2 = r22
                r0.<init>(r1, r2)
                r0 = r44
                r1 = r38
                r0.process(r1)
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "label"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                org.jsoup.parser.Token$StartTag r38 = new org.jsoup.parser.Token$StartTag
                java.lang.String r39 = "hr"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "form"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                goto L_0x0011
            L_0x09b9:
                java.lang.String r38 = "textarea"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x09ee
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r0 = r44
                org.jsoup.parser.Tokeniser r0 = r0.tokeniser
                r38 = r0
                org.jsoup.parser.TokeniserState r39 = org.jsoup.parser.TokeniserState.Rcdata
                r38.transition(r39)
                r44.markInsertionMode()
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.Text
                r0 = r44
                r1 = r38
                r0.transition(r1)
                goto L_0x0011
            L_0x09ee:
                java.lang.String r38 = "xmp"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0a29
                java.lang.String r38 = "p"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inButtonScope(r1)
                if (r38 == 0) goto L_0x0a14
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "p"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0a14:
                r44.reconstructFormattingElements()
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                r0 = r36
                r1 = r44
                org.jsoup.parser.TreeBuilderState.access$300(r0, r1)
                goto L_0x0011
            L_0x0a29:
                java.lang.String r38 = "iframe"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0a47
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                r0 = r36
                r1 = r44
                org.jsoup.parser.TreeBuilderState.access$300(r0, r1)
                goto L_0x0011
            L_0x0a47:
                java.lang.String r38 = "noembed"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0a5c
                r0 = r36
                r1 = r44
                org.jsoup.parser.TreeBuilderState.access$300(r0, r1)
                goto L_0x0011
            L_0x0a5c:
                java.lang.String r38 = "select"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0abd
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r38 = 0
                r0 = r44
                r1 = r38
                r0.framesetOk(r1)
                org.jsoup.parser.TreeBuilderState r37 = r44.state()
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InTable
                boolean r38 = r37.equals(r38)
                if (r38 != 0) goto L_0x0aa7
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InCaption
                boolean r38 = r37.equals(r38)
                if (r38 != 0) goto L_0x0aa7
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InTableBody
                boolean r38 = r37.equals(r38)
                if (r38 != 0) goto L_0x0aa7
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InRow
                boolean r38 = r37.equals(r38)
                if (r38 != 0) goto L_0x0aa7
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InCell
                boolean r38 = r37.equals(r38)
                if (r38 == 0) goto L_0x0ab2
            L_0x0aa7:
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InSelectInTable
                r0 = r44
                r1 = r38
                r0.transition(r1)
                goto L_0x0011
            L_0x0ab2:
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.InSelect
                r0 = r44
                r1 = r38
                r0.transition(r1)
                goto L_0x0011
            L_0x0abd:
                java.lang.String r38 = "optgroup"
                r39 = 1
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "option"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 == 0) goto L_0x0afd
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                java.lang.String r39 = "option"
                boolean r38 = r38.equals(r39)
                if (r38 == 0) goto L_0x0af1
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "option"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
            L_0x0af1:
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insert(r1)
                goto L_0x0011
            L_0x0afd:
                java.lang.String r38 = "rp"
                r39 = 1
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "rt"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 == 0) goto L_0x0b4b
                java.lang.String r38 = "ruby"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inScope(r1)
                if (r38 == 0) goto L_0x0011
                r44.generateImpliedEndTags()
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                java.lang.String r39 = "ruby"
                boolean r38 = r38.equals(r39)
                if (r38 != 0) goto L_0x0b42
                r0 = r44
                r1 = r42
                r0.error(r1)
                java.lang.String r38 = "ruby"
                r0 = r44
                r1 = r38
                r0.popStackToBefore(r1)
            L_0x0b42:
                r0 = r44
                r1 = r36
                r0.insert(r1)
                goto L_0x0011
            L_0x0b4b:
                java.lang.String r38 = "math"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0b6c
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r0 = r44
                org.jsoup.parser.Tokeniser r0 = r0.tokeniser
                r38 = r0
                r38.acknowledgeSelfClosingFlag()
                goto L_0x0011
            L_0x0b6c:
                java.lang.String r38 = "svg"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0b8d
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insert(r1)
                r0 = r44
                org.jsoup.parser.Tokeniser r0 = r0.tokeniser
                r38 = r0
                r38.acknowledgeSelfClosingFlag()
                goto L_0x0011
            L_0x0b8d:
                r38 = 11
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "caption"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "col"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "colgroup"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "frame"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "head"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "tbody"
                r38[r39] = r40
                r39 = 6
                java.lang.String r40 = "td"
                r38[r39] = r40
                r39 = 7
                java.lang.String r40 = "tfoot"
                r38[r39] = r40
                r39 = 8
                java.lang.String r40 = "th"
                r38[r39] = r40
                r39 = 9
                java.lang.String r40 = "thead"
                r38[r39] = r40
                r39 = 10
                java.lang.String r40 = "tr"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x0bec
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0bec:
                r44.reconstructFormattingElements()
                r0 = r44
                r1 = r36
                r0.insert(r1)
                goto L_0x0011
            L_0x0bf8:
                org.jsoup.parser.Token$EndTag r15 = r43.asEndTag()
                java.lang.String r26 = r15.name()
                java.lang.String r38 = "body"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0c2e
                java.lang.String r38 = "body"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x0c23
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0c23:
                org.jsoup.parser.TreeBuilderState r38 = org.jsoup.parser.TreeBuilderState.AnonymousClass7.AfterBody
                r0 = r44
                r1 = r38
                r0.transition(r1)
                goto L_0x0011
            L_0x0c2e:
                java.lang.String r38 = "html"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0c53
                org.jsoup.parser.Token$EndTag r38 = new org.jsoup.parser.Token$EndTag
                java.lang.String r39 = "body"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                boolean r28 = r0.process(r1)
                if (r28 == 0) goto L_0x0011
                r0 = r44
                boolean r38 = r0.process(r15)
                goto L_0x0013
            L_0x0c53:
                r38 = 24
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "address"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "article"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "aside"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "blockquote"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "button"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "center"
                r38[r39] = r40
                r39 = 6
                java.lang.String r40 = "details"
                r38[r39] = r40
                r39 = 7
                java.lang.String r40 = "dir"
                r38[r39] = r40
                r39 = 8
                java.lang.String r40 = "div"
                r38[r39] = r40
                r39 = 9
                java.lang.String r40 = "dl"
                r38[r39] = r40
                r39 = 10
                java.lang.String r40 = "fieldset"
                r38[r39] = r40
                r39 = 11
                java.lang.String r40 = "figcaption"
                r38[r39] = r40
                r39 = 12
                java.lang.String r40 = "figure"
                r38[r39] = r40
                r39 = 13
                java.lang.String r40 = "footer"
                r38[r39] = r40
                r39 = 14
                java.lang.String r40 = "header"
                r38[r39] = r40
                r39 = 15
                java.lang.String r40 = "hgroup"
                r38[r39] = r40
                r39 = 16
                java.lang.String r40 = "listing"
                r38[r39] = r40
                r39 = 17
                java.lang.String r40 = "menu"
                r38[r39] = r40
                r39 = 18
                java.lang.String r40 = "nav"
                r38[r39] = r40
                r39 = 19
                java.lang.String r40 = "ol"
                r38[r39] = r40
                r39 = 20
                java.lang.String r40 = "pre"
                r38[r39] = r40
                r39 = 21
                java.lang.String r40 = "section"
                r38[r39] = r40
                r39 = 22
                java.lang.String r40 = "summary"
                r38[r39] = r40
                r39 = 23
                java.lang.String r40 = "ul"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x0d2f
                r0 = r44
                r1 = r26
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x0d0a
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0d0a:
                r44.generateImpliedEndTags()
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r0 = r38
                r1 = r26
                boolean r38 = r0.equals(r1)
                if (r38 != 0) goto L_0x0d26
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x0d26:
                r0 = r44
                r1 = r26
                r0.popStackToClose(r1)
                goto L_0x0011
            L_0x0d2f:
                java.lang.String r38 = "form"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0d82
                org.jsoup.nodes.Element r13 = r44.getFormElement()
                r38 = 0
                r0 = r44
                r1 = r38
                r0.setFormElement(r1)
                if (r13 == 0) goto L_0x0d54
                r0 = r44
                r1 = r26
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x0d5f
            L_0x0d54:
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0d5f:
                r44.generateImpliedEndTags()
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r0 = r38
                r1 = r26
                boolean r38 = r0.equals(r1)
                if (r38 != 0) goto L_0x0d7b
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x0d7b:
                r0 = r44
                r0.removeFromStack(r13)
                goto L_0x0011
            L_0x0d82:
                java.lang.String r38 = "p"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0de0
                r0 = r44
                r1 = r26
                boolean r38 = r0.inButtonScope(r1)
                if (r38 != 0) goto L_0x0db7
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.parser.Token$StartTag r38 = new org.jsoup.parser.Token$StartTag
                r0 = r38
                r1 = r26
                r0.<init>(r1)
                r0 = r44
                r1 = r38
                r0.process(r1)
                r0 = r44
                boolean r38 = r0.process(r15)
                goto L_0x0013
            L_0x0db7:
                r0 = r44
                r1 = r26
                r0.generateImpliedEndTags(r1)
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r0 = r38
                r1 = r26
                boolean r38 = r0.equals(r1)
                if (r38 != 0) goto L_0x0dd7
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x0dd7:
                r0 = r44
                r1 = r26
                r0.popStackToClose(r1)
                goto L_0x0011
            L_0x0de0:
                java.lang.String r38 = "li"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0e2a
                r0 = r44
                r1 = r26
                boolean r38 = r0.inListItemScope(r1)
                if (r38 != 0) goto L_0x0e01
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0e01:
                r0 = r44
                r1 = r26
                r0.generateImpliedEndTags(r1)
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r0 = r38
                r1 = r26
                boolean r38 = r0.equals(r1)
                if (r38 != 0) goto L_0x0e21
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x0e21:
                r0 = r44
                r1 = r26
                r0.popStackToClose(r1)
                goto L_0x0011
            L_0x0e2a:
                r38 = 2
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "dd"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "dt"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x0e86
                r0 = r44
                r1 = r26
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x0e5d
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0e5d:
                r0 = r44
                r1 = r26
                r0.generateImpliedEndTags(r1)
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r0 = r38
                r1 = r26
                boolean r38 = r0.equals(r1)
                if (r38 != 0) goto L_0x0e7d
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x0e7d:
                r0 = r44
                r1 = r26
                r0.popStackToClose(r1)
                goto L_0x0011
            L_0x0e86:
                r38 = 6
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "h1"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "h2"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "h3"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "h4"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "h5"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "h6"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x0f52
                r38 = 6
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "h1"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "h2"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "h3"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "h4"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "h5"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "h6"
                r38[r39] = r40
                r0 = r44
                r1 = r38
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x0efd
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x0efd:
                r0 = r44
                r1 = r26
                r0.generateImpliedEndTags(r1)
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r0 = r38
                r1 = r26
                boolean r38 = r0.equals(r1)
                if (r38 != 0) goto L_0x0f1d
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x0f1d:
                r38 = 6
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "h1"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "h2"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "h3"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "h4"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "h5"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "h6"
                r38[r39] = r40
                r0 = r44
                r1 = r38
                r0.popStackToClose(r1)
                goto L_0x0011
            L_0x0f52:
                java.lang.String r38 = "sarcasm"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x0f64
                boolean r38 = r42.anyOtherEndTag(r43, r44)
                goto L_0x0013
            L_0x0f64:
                r38 = 14
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "a"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "b"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "big"
                r38[r39] = r40
                r39 = 3
                java.lang.String r40 = "code"
                r38[r39] = r40
                r39 = 4
                java.lang.String r40 = "em"
                r38[r39] = r40
                r39 = 5
                java.lang.String r40 = "font"
                r38[r39] = r40
                r39 = 6
                java.lang.String r40 = "i"
                r38[r39] = r40
                r39 = 7
                java.lang.String r40 = "nobr"
                r38[r39] = r40
                r39 = 8
                java.lang.String r40 = "s"
                r38[r39] = r40
                r39 = 9
                java.lang.String r40 = "small"
                r38[r39] = r40
                r39 = 10
                java.lang.String r40 = "strike"
                r38[r39] = r40
                r39 = 11
                java.lang.String r40 = "strong"
                r38[r39] = r40
                r39 = 12
                java.lang.String r40 = "tt"
                r38[r39] = r40
                r39 = 13
                java.lang.String r40 = "u"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x11ac
                r20 = 0
            L_0x0fcc:
                r38 = 8
                r0 = r20
                r1 = r38
                if (r0 >= r1) goto L_0x0011
                r0 = r44
                r1 = r26
                org.jsoup.nodes.Element r17 = r0.getActiveFormattingElement(r1)
                if (r17 != 0) goto L_0x0fe4
                boolean r38 = r42.anyOtherEndTag(r43, r44)
                goto L_0x0013
            L_0x0fe4:
                r0 = r44
                r1 = r17
                boolean r38 = r0.onStack(r1)
                if (r38 != 0) goto L_0x1000
                r0 = r44
                r1 = r42
                r0.error(r1)
                r0 = r44
                r1 = r17
                r0.removeFromActiveFormattingElements(r1)
                r38 = 1
                goto L_0x0013
            L_0x1000:
                java.lang.String r38 = r17.nodeName()
                r0 = r44
                r1 = r38
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x1019
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x1019:
                org.jsoup.nodes.Element r38 = r44.currentElement()
                r0 = r38
                r1 = r17
                if (r0 == r1) goto L_0x102a
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x102a:
                r18 = 0
                r12 = 0
                r33 = 0
                org.jsoup.helper.DescendableLinkedList r35 = r44.getStack()
                r34 = 0
            L_0x1035:
                int r38 = r35.size()
                r0 = r34
                r1 = r38
                if (r0 >= r1) goto L_0x106a
                r0 = r35
                r1 = r34
                java.lang.Object r14 = r0.get(r1)
                org.jsoup.nodes.Element r14 = (org.jsoup.nodes.Element) r14
                r0 = r17
                if (r14 != r0) goto L_0x105e
                int r38 = r34 + -1
                r0 = r35
                r1 = r38
                java.lang.Object r12 = r0.get(r1)
                org.jsoup.nodes.Element r12 = (org.jsoup.nodes.Element) r12
                r33 = 1
            L_0x105b:
                int r34 = r34 + 1
                goto L_0x1035
            L_0x105e:
                if (r33 == 0) goto L_0x105b
                r0 = r44
                boolean r38 = r0.isSpecial(r14)
                if (r38 == 0) goto L_0x105b
                r18 = r14
            L_0x106a:
                if (r18 != 0) goto L_0x1082
                java.lang.String r38 = r17.nodeName()
                r0 = r44
                r1 = r38
                r0.popStackToClose(r1)
                r0 = r44
                r1 = r17
                r0.removeFromActiveFormattingElements(r1)
                r38 = 1
                goto L_0x0013
            L_0x1082:
                r27 = r18
                r24 = r18
                r23 = 0
            L_0x1088:
                r38 = 3
                r0 = r23
                r1 = r38
                if (r0 >= r1) goto L_0x10bc
                r0 = r44
                r1 = r27
                boolean r38 = r0.onStack(r1)
                if (r38 == 0) goto L_0x10a2
                r0 = r44
                r1 = r27
                org.jsoup.nodes.Element r27 = r0.aboveOnStack(r1)
            L_0x10a2:
                r0 = r44
                r1 = r27
                boolean r38 = r0.isInActiveFormattingElements(r1)
                if (r38 != 0) goto L_0x10b6
                r0 = r44
                r1 = r27
                r0.removeFromStack(r1)
            L_0x10b3:
                int r23 = r23 + 1
                goto L_0x1088
            L_0x10b6:
                r0 = r27
                r1 = r17
                if (r0 != r1) goto L_0x1139
            L_0x10bc:
                java.lang.String r38 = r12.nodeName()
                r39 = 5
                r0 = r39
                java.lang.String[] r0 = new java.lang.String[r0]
                r39 = r0
                r40 = 0
                java.lang.String r41 = "table"
                r39[r40] = r41
                r40 = 1
                java.lang.String r41 = "tbody"
                r39[r40] = r41
                r40 = 2
                java.lang.String r41 = "tfoot"
                r39[r40] = r41
                r40 = 3
                java.lang.String r41 = "thead"
                r39[r40] = r41
                r40 = 4
                java.lang.String r41 = "tr"
                r39[r40] = r41
                boolean r38 = org.jsoup.helper.StringUtil.in(r38, r39)
                if (r38 == 0) goto L_0x117e
                org.jsoup.nodes.Element r38 = r24.parent()
                if (r38 == 0) goto L_0x10f5
                r24.remove()
            L_0x10f5:
                r0 = r44
                r1 = r24
                r0.insertInFosterParent(r1)
            L_0x10fc:
                org.jsoup.nodes.Element r4 = new org.jsoup.nodes.Element
                org.jsoup.parser.Tag r38 = org.jsoup.parser.Tag.valueOf(r26)
                java.lang.String r39 = r44.getBaseUri()
                r0 = r38
                r1 = r39
                r4.<init>(r0, r1)
                java.util.List r38 = r18.childNodes()
                java.util.List r39 = r18.childNodes()
                int r39 = r39.size()
                r0 = r39
                org.jsoup.nodes.Node[] r0 = new org.jsoup.nodes.Node[r0]
                r39 = r0
                java.lang.Object[] r11 = r38.toArray(r39)
                org.jsoup.nodes.Node[] r11 = (org.jsoup.nodes.Node[]) r11
                r5 = r11
                int r0 = r5.length
                r25 = r0
                r21 = 0
            L_0x112b:
                r0 = r21
                r1 = r25
                if (r0 >= r1) goto L_0x118e
                r10 = r5[r21]
                r4.appendChild(r10)
                int r21 = r21 + 1
                goto L_0x112b
            L_0x1139:
                org.jsoup.nodes.Element r31 = new org.jsoup.nodes.Element
                java.lang.String r38 = r27.nodeName()
                org.jsoup.parser.Tag r38 = org.jsoup.parser.Tag.valueOf(r38)
                java.lang.String r39 = r44.getBaseUri()
                r0 = r31
                r1 = r38
                r2 = r39
                r0.<init>(r1, r2)
                r0 = r44
                r1 = r27
                r2 = r31
                r0.replaceActiveFormattingElement(r1, r2)
                r0 = r44
                r1 = r27
                r2 = r31
                r0.replaceOnStack(r1, r2)
                r27 = r31
                r0 = r24
                r1 = r18
                if (r0 != r1) goto L_0x116a
            L_0x116a:
                org.jsoup.nodes.Element r38 = r24.parent()
                if (r38 == 0) goto L_0x1173
                r24.remove()
            L_0x1173:
                r0 = r27
                r1 = r24
                r0.appendChild(r1)
                r24 = r27
                goto L_0x10b3
            L_0x117e:
                org.jsoup.nodes.Element r38 = r24.parent()
                if (r38 == 0) goto L_0x1187
                r24.remove()
            L_0x1187:
                r0 = r24
                r12.appendChild(r0)
                goto L_0x10fc
            L_0x118e:
                r0 = r18
                r0.appendChild(r4)
                r0 = r44
                r1 = r17
                r0.removeFromActiveFormattingElements(r1)
                r0 = r44
                r1 = r17
                r0.removeFromStack(r1)
                r0 = r44
                r1 = r18
                r0.insertOnStackAfter(r1, r4)
                int r20 = r20 + 1
                goto L_0x0fcc
            L_0x11ac:
                r38 = 3
                r0 = r38
                java.lang.String[] r0 = new java.lang.String[r0]
                r38 = r0
                r39 = 0
                java.lang.String r40 = "applet"
                r38[r39] = r40
                r39 = 1
                java.lang.String r40 = "marquee"
                r38[r39] = r40
                r39 = 2
                java.lang.String r40 = "object"
                r38[r39] = r40
                r0 = r26
                r1 = r38
                boolean r38 = org.jsoup.helper.StringUtil.in(r0, r1)
                if (r38 == 0) goto L_0x1219
                java.lang.String r38 = "name"
                r0 = r44
                r1 = r38
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x0011
                r0 = r44
                r1 = r26
                boolean r38 = r0.inScope(r1)
                if (r38 != 0) goto L_0x11f1
                r0 = r44
                r1 = r42
                r0.error(r1)
                r38 = 0
                goto L_0x0013
            L_0x11f1:
                r44.generateImpliedEndTags()
                org.jsoup.nodes.Element r38 = r44.currentElement()
                java.lang.String r38 = r38.nodeName()
                r0 = r38
                r1 = r26
                boolean r38 = r0.equals(r1)
                if (r38 != 0) goto L_0x120d
                r0 = r44
                r1 = r42
                r0.error(r1)
            L_0x120d:
                r0 = r44
                r1 = r26
                r0.popStackToClose(r1)
                r44.clearFormattingElementsToLastMarker()
                goto L_0x0011
            L_0x1219:
                java.lang.String r38 = "br"
                r0 = r26
                r1 = r38
                boolean r38 = r0.equals(r1)
                if (r38 == 0) goto L_0x123e
                r0 = r44
                r1 = r42
                r0.error(r1)
                org.jsoup.parser.Token$StartTag r38 = new org.jsoup.parser.Token$StartTag
                java.lang.String r39 = "br"
                r38.<init>(r39)
                r0 = r44
                r1 = r38
                r0.process(r1)
                r38 = 0
                goto L_0x0013
            L_0x123e:
                boolean r38 = r42.anyOtherEndTag(r43, r44)
                goto L_0x0013
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.TreeBuilderState.AnonymousClass7.process(org.jsoup.parser.Token, org.jsoup.parser.TreeBuilder):boolean");
        }

        /* access modifiers changed from: package-private */
        public boolean anyOtherEndTag(Token t, TreeBuilder tb) {
            Element node;
            String name = t.asEndTag().name();
            Iterator<Element> it = tb.getStack().descendingIterator();
            do {
                if (it.hasNext()) {
                    node = it.next();
                    if (node.nodeName().equals(name)) {
                        tb.generateImpliedEndTags(name);
                        if (!name.equals(tb.currentElement().nodeName())) {
                            tb.error(this);
                        }
                        tb.popStackToClose(name);
                    }
                }
                return true;
            } while (!tb.isSpecial(node));
            tb.error(this);
            return false;
        }
    },
    Text {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isCharacter()) {
                tb.insert(t.asCharacter());
            } else if (t.isEOF()) {
                tb.error(this);
                tb.pop();
                tb.transition(tb.originalState());
                return tb.process(t);
            } else if (t.isEndTag()) {
                tb.pop();
                tb.transition(tb.originalState());
            }
            return true;
        }
    },
    InTable {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isCharacter()) {
                tb.newPendingTableCharacters();
                tb.markInsertionMode();
                tb.transition(InTableText);
                return tb.process(t);
            }
            if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isStartTag()) {
                Token.StartTag startTag = t.asStartTag();
                String name = startTag.name();
                if (name.equals("caption")) {
                    tb.clearStackToTableContext();
                    tb.insertMarkerToFormattingElements();
                    tb.insert(startTag);
                    tb.transition(InCaption);
                } else if (name.equals("colgroup")) {
                    tb.clearStackToTableContext();
                    tb.insert(startTag);
                    tb.transition(InColumnGroup);
                } else if (name.equals("col")) {
                    tb.process(new Token.StartTag("colgroup"));
                    return tb.process(t);
                } else {
                    if (StringUtil.in(name, "tbody", "tfoot", "thead")) {
                        tb.clearStackToTableContext();
                        tb.insert(startTag);
                        tb.transition(InTableBody);
                    } else {
                        if (StringUtil.in(name, "td", "th", "tr")) {
                            tb.process(new Token.StartTag("tbody"));
                            return tb.process(t);
                        } else if (name.equals("table")) {
                            tb.error(this);
                            if (tb.process(new Token.EndTag("table"))) {
                                return tb.process(t);
                            }
                        } else {
                            if (StringUtil.in(name, "style", "script")) {
                                return tb.process(t, InHead);
                            }
                            if (name.equals("input")) {
                                if (!startTag.attributes.get("type").equalsIgnoreCase("hidden")) {
                                    return anythingElse(t, tb);
                                }
                                tb.insertEmpty(startTag);
                            } else if (!name.equals("form")) {
                                return anythingElse(t, tb);
                            } else {
                                tb.error(this);
                                if (tb.getFormElement() != null) {
                                    return false;
                                }
                                tb.setFormElement(tb.insertEmpty(startTag));
                            }
                        }
                    }
                }
            } else if (t.isEndTag()) {
                String name2 = t.asEndTag().name();
                if (!name2.equals("table")) {
                    if (!StringUtil.in(name2, "body", "caption", "col", "colgroup", AdActivity.HTML_PARAM, "tbody", "td", "tfoot", "th", "thead", "tr")) {
                        return anythingElse(t, tb);
                    }
                    tb.error(this);
                    return false;
                } else if (!tb.inTableScope(name2)) {
                    tb.error(this);
                    return false;
                } else {
                    tb.popStackToClose("table");
                    tb.resetInsertionMode();
                }
            } else if (t.isEOF()) {
                if (tb.currentElement().nodeName().equals(AdActivity.HTML_PARAM)) {
                    tb.error(this);
                }
                return true;
            }
            return anythingElse(t, tb);
        }

        /* access modifiers changed from: package-private */
        public boolean anythingElse(Token t, TreeBuilder tb) {
            tb.error(this);
            if (!StringUtil.in(tb.currentElement().nodeName(), "table", "tbody", "tfoot", "thead", "tr")) {
                return tb.process(t, InBody);
            }
            tb.setFosterInserts(true);
            boolean processed = tb.process(t, InBody);
            tb.setFosterInserts(false);
            return processed;
        }
    },
    InTableText {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            switch (AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType[t.type.ordinal()]) {
                case JsonWriteContext.STATUS_EXPECT_NAME:
                    Token.Character c = t.asCharacter();
                    if (c.getData().equals(TreeBuilderState.access$400())) {
                        tb.error(this);
                        return false;
                    }
                    tb.getPendingTableCharacters().add(c);
                    return true;
                default:
                    if (tb.getPendingTableCharacters().size() > 0) {
                        for (Token.Character character : tb.getPendingTableCharacters()) {
                            if (!TreeBuilderState.access$100(character)) {
                                tb.error(this);
                                if (StringUtil.in(tb.currentElement().nodeName(), "table", "tbody", "tfoot", "thead", "tr")) {
                                    tb.setFosterInserts(true);
                                    tb.process(character, InBody);
                                    tb.setFosterInserts(false);
                                } else {
                                    tb.process(character, InBody);
                                }
                            } else {
                                tb.insert(character);
                            }
                        }
                        tb.newPendingTableCharacters();
                    }
                    tb.transition(tb.originalState());
                    return tb.process(t);
            }
        }
    },
    InCaption {
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0091, code lost:
            if (org.jsoup.helper.StringUtil.in(r13.asStartTag().name(), "caption", "col", "colgroup", "tbody", "td", "tfoot", "th", "thead", "tr") == false) goto L_0x0093;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean process(org.jsoup.parser.Token r13, org.jsoup.parser.TreeBuilder r14) {
            /*
                r12 = this;
                r11 = 4
                r10 = 3
                r9 = 2
                r4 = 1
                r3 = 0
                boolean r5 = r13.isEndTag()
                if (r5 == 0) goto L_0x0052
                org.jsoup.parser.Token$EndTag r5 = r13.asEndTag()
                java.lang.String r5 = r5.name()
                java.lang.String r6 = "caption"
                boolean r5 = r5.equals(r6)
                if (r5 == 0) goto L_0x0052
                org.jsoup.parser.Token$EndTag r0 = r13.asEndTag()
                java.lang.String r1 = r0.name()
                boolean r5 = r14.inTableScope(r1)
                if (r5 != 0) goto L_0x002d
                r14.error(r12)
            L_0x002c:
                return r3
            L_0x002d:
                r14.generateImpliedEndTags()
                org.jsoup.nodes.Element r3 = r14.currentElement()
                java.lang.String r3 = r3.nodeName()
                java.lang.String r5 = "caption"
                boolean r3 = r3.equals(r5)
                if (r3 != 0) goto L_0x0043
                r14.error(r12)
            L_0x0043:
                java.lang.String r3 = "caption"
                r14.popStackToClose(r3)
                r14.clearFormattingElementsToLastMarker()
                org.jsoup.parser.TreeBuilderState r3 = org.jsoup.parser.TreeBuilderState.AnonymousClass11.InTable
                r14.transition(r3)
            L_0x0050:
                r3 = r4
                goto L_0x002c
            L_0x0052:
                boolean r5 = r13.isStartTag()
                if (r5 == 0) goto L_0x0093
                org.jsoup.parser.Token$StartTag r5 = r13.asStartTag()
                java.lang.String r5 = r5.name()
                r6 = 9
                java.lang.String[] r6 = new java.lang.String[r6]
                java.lang.String r7 = "caption"
                r6[r3] = r7
                java.lang.String r7 = "col"
                r6[r4] = r7
                java.lang.String r7 = "colgroup"
                r6[r9] = r7
                java.lang.String r7 = "tbody"
                r6[r10] = r7
                java.lang.String r7 = "td"
                r6[r11] = r7
                r7 = 5
                java.lang.String r8 = "tfoot"
                r6[r7] = r8
                r7 = 6
                java.lang.String r8 = "th"
                r6[r7] = r8
                r7 = 7
                java.lang.String r8 = "thead"
                r6[r7] = r8
                r7 = 8
                java.lang.String r8 = "tr"
                r6[r7] = r8
                boolean r5 = org.jsoup.helper.StringUtil.in(r5, r6)
                if (r5 != 0) goto L_0x00a9
            L_0x0093:
                boolean r5 = r13.isEndTag()
                if (r5 == 0) goto L_0x00bf
                org.jsoup.parser.Token$EndTag r5 = r13.asEndTag()
                java.lang.String r5 = r5.name()
                java.lang.String r6 = "table"
                boolean r5 = r5.equals(r6)
                if (r5 == 0) goto L_0x00bf
            L_0x00a9:
                r14.error(r12)
                org.jsoup.parser.Token$EndTag r3 = new org.jsoup.parser.Token$EndTag
                java.lang.String r5 = "caption"
                r3.<init>(r5)
                boolean r2 = r14.process(r3)
                if (r2 == 0) goto L_0x0050
                boolean r3 = r14.process(r13)
                goto L_0x002c
            L_0x00bf:
                boolean r5 = r13.isEndTag()
                if (r5 == 0) goto L_0x010b
                org.jsoup.parser.Token$EndTag r5 = r13.asEndTag()
                java.lang.String r5 = r5.name()
                r6 = 10
                java.lang.String[] r6 = new java.lang.String[r6]
                java.lang.String r7 = "body"
                r6[r3] = r7
                java.lang.String r7 = "col"
                r6[r4] = r7
                java.lang.String r4 = "colgroup"
                r6[r9] = r4
                java.lang.String r4 = "html"
                r6[r10] = r4
                java.lang.String r4 = "tbody"
                r6[r11] = r4
                r4 = 5
                java.lang.String r7 = "td"
                r6[r4] = r7
                r4 = 6
                java.lang.String r7 = "tfoot"
                r6[r4] = r7
                r4 = 7
                java.lang.String r7 = "th"
                r6[r4] = r7
                r4 = 8
                java.lang.String r7 = "thead"
                r6[r4] = r7
                r4 = 9
                java.lang.String r7 = "tr"
                r6[r4] = r7
                boolean r4 = org.jsoup.helper.StringUtil.in(r5, r6)
                if (r4 == 0) goto L_0x010b
                r14.error(r12)
                goto L_0x002c
            L_0x010b:
                org.jsoup.parser.TreeBuilderState r3 = org.jsoup.parser.TreeBuilderState.AnonymousClass11.InBody
                boolean r3 = r14.process(r13, r3)
                goto L_0x002c
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.TreeBuilderState.AnonymousClass11.process(org.jsoup.parser.Token, org.jsoup.parser.TreeBuilder):boolean");
        }
    },
    InColumnGroup {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                tb.insert(t.asCharacter());
                return true;
            }
            switch (AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType[t.type.ordinal()]) {
                case JsonWriteContext.STATUS_OK_AFTER_COMMA:
                    tb.insert(t.asComment());
                    return true;
                case JsonWriteContext.STATUS_OK_AFTER_COLON:
                    tb.error(this);
                    return true;
                case JsonWriteContext.STATUS_OK_AFTER_SPACE:
                    Token.StartTag startTag = t.asStartTag();
                    String name = startTag.name();
                    if (name.equals(AdActivity.HTML_PARAM)) {
                        return tb.process(t, InBody);
                    }
                    if (!name.equals("col")) {
                        return anythingElse(t, tb);
                    }
                    tb.insertEmpty(startTag);
                    return true;
                case JsonWriteContext.STATUS_EXPECT_VALUE:
                    if (!t.asEndTag().name().equals("colgroup")) {
                        return anythingElse(t, tb);
                    }
                    if (tb.currentElement().nodeName().equals(AdActivity.HTML_PARAM)) {
                        tb.error(this);
                        return false;
                    }
                    tb.pop();
                    tb.transition(InTable);
                    return true;
                case JsonWriteContext.STATUS_EXPECT_NAME:
                default:
                    return anythingElse(t, tb);
                case 6:
                    if (!tb.currentElement().nodeName().equals(AdActivity.HTML_PARAM)) {
                        return anythingElse(t, tb);
                    }
                    return true;
            }
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            if (tb.process(new Token.EndTag("colgroup"))) {
                return tb.process(t);
            }
            return true;
        }
    },
    InTableBody {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            switch (AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType[t.type.ordinal()]) {
                case JsonWriteContext.STATUS_OK_AFTER_SPACE:
                    Token.StartTag startTag = t.asStartTag();
                    String name = startTag.name();
                    if (name.equals("tr")) {
                        tb.clearStackToTableBodyContext();
                        tb.insert(startTag);
                        tb.transition(InRow);
                        break;
                    } else {
                        if (StringUtil.in(name, "th", "td")) {
                            tb.error(this);
                            tb.process(new Token.StartTag("tr"));
                            return tb.process(startTag);
                        }
                        if (StringUtil.in(name, "caption", "col", "colgroup", "tbody", "tfoot", "thead")) {
                            return exitTableBody(t, tb);
                        }
                        return anythingElse(t, tb);
                    }
                case JsonWriteContext.STATUS_EXPECT_VALUE:
                    String name2 = t.asEndTag().name();
                    if (StringUtil.in(name2, "tbody", "tfoot", "thead")) {
                        if (tb.inTableScope(name2)) {
                            tb.clearStackToTableBodyContext();
                            tb.pop();
                            tb.transition(InTable);
                            break;
                        } else {
                            tb.error(this);
                            return false;
                        }
                    } else if (name2.equals("table")) {
                        return exitTableBody(t, tb);
                    } else {
                        if (!StringUtil.in(name2, "body", "caption", "col", "colgroup", AdActivity.HTML_PARAM, "td", "th", "tr")) {
                            return anythingElse(t, tb);
                        }
                        tb.error(this);
                        return false;
                    }
                default:
                    return anythingElse(t, tb);
            }
            return true;
        }

        private boolean exitTableBody(Token t, TreeBuilder tb) {
            if (tb.inTableScope("tbody") || tb.inTableScope("thead") || tb.inScope("tfoot")) {
                tb.clearStackToTableBodyContext();
                tb.process(new Token.EndTag(tb.currentElement().nodeName()));
                return tb.process(t);
            }
            tb.error(this);
            return false;
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            return tb.process(t, InTable);
        }
    },
    InRow {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isStartTag()) {
                Token.StartTag startTag = t.asStartTag();
                String name = startTag.name();
                if (StringUtil.in(name, "th", "td")) {
                    tb.clearStackToTableRowContext();
                    tb.insert(startTag);
                    tb.transition(InCell);
                    tb.insertMarkerToFormattingElements();
                } else {
                    if (StringUtil.in(name, "caption", "col", "colgroup", "tbody", "tfoot", "thead", "tr")) {
                        return handleMissingTr(t, tb);
                    }
                    return anythingElse(t, tb);
                }
            } else if (!t.isEndTag()) {
                return anythingElse(t, tb);
            } else {
                String name2 = t.asEndTag().name();
                if (name2.equals("tr")) {
                    if (!tb.inTableScope(name2)) {
                        tb.error(this);
                        return false;
                    }
                    tb.clearStackToTableRowContext();
                    tb.pop();
                    tb.transition(InTableBody);
                } else if (name2.equals("table")) {
                    return handleMissingTr(t, tb);
                } else {
                    if (!StringUtil.in(name2, "tbody", "tfoot", "thead")) {
                        if (!StringUtil.in(name2, "body", "caption", "col", "colgroup", AdActivity.HTML_PARAM, "td", "th")) {
                            return anythingElse(t, tb);
                        }
                        tb.error(this);
                        return false;
                    } else if (!tb.inTableScope(name2)) {
                        tb.error(this);
                        return false;
                    } else {
                        tb.process(new Token.EndTag("tr"));
                        return tb.process(t);
                    }
                }
            }
            return true;
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            return tb.process(t, InTable);
        }

        private boolean handleMissingTr(Token t, TreeBuilder tb) {
            if (tb.process(new Token.EndTag("tr"))) {
                return tb.process(t);
            }
            return false;
        }
    },
    InCell {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isEndTag()) {
                String name = t.asEndTag().name();
                if (!StringUtil.in(name, "td", "th")) {
                    if (StringUtil.in(name, "body", "caption", "col", "colgroup", AdActivity.HTML_PARAM)) {
                        tb.error(this);
                        return false;
                    }
                    if (!StringUtil.in(name, "table", "tbody", "tfoot", "thead", "tr")) {
                        return anythingElse(t, tb);
                    }
                    if (!tb.inTableScope(name)) {
                        tb.error(this);
                        return false;
                    }
                    closeCell(tb);
                    return tb.process(t);
                } else if (!tb.inTableScope(name)) {
                    tb.error(this);
                    tb.transition(InRow);
                    return false;
                } else {
                    tb.generateImpliedEndTags();
                    if (!tb.currentElement().nodeName().equals(name)) {
                        tb.error(this);
                    }
                    tb.popStackToClose(name);
                    tb.clearFormattingElementsToLastMarker();
                    tb.transition(InRow);
                    return true;
                }
            } else {
                if (t.isStartTag()) {
                    if (StringUtil.in(t.asStartTag().name(), "caption", "col", "colgroup", "tbody", "td", "tfoot", "th", "thead", "tr")) {
                        if (tb.inTableScope("td") || tb.inTableScope("th")) {
                            closeCell(tb);
                            return tb.process(t);
                        }
                        tb.error(this);
                        return false;
                    }
                }
                return anythingElse(t, tb);
            }
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            return tb.process(t, InBody);
        }

        private void closeCell(TreeBuilder tb) {
            if (tb.inTableScope("td")) {
                tb.process(new Token.EndTag("td"));
            } else {
                tb.process(new Token.EndTag("th"));
            }
        }
    },
    InSelect {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            switch (AnonymousClass24.$SwitchMap$org$jsoup$parser$Token$TokenType[t.type.ordinal()]) {
                case JsonWriteContext.STATUS_OK_AFTER_COMMA:
                    tb.insert(t.asComment());
                    break;
                case JsonWriteContext.STATUS_OK_AFTER_COLON:
                    tb.error(this);
                    return false;
                case JsonWriteContext.STATUS_OK_AFTER_SPACE:
                    Token.StartTag start = t.asStartTag();
                    String name = start.name();
                    if (name.equals(AdActivity.HTML_PARAM)) {
                        return tb.process(start, InBody);
                    }
                    if (name.equals("option")) {
                        tb.process(new Token.EndTag("option"));
                        tb.insert(start);
                        break;
                    } else if (name.equals("optgroup")) {
                        if (tb.currentElement().nodeName().equals("option")) {
                            tb.process(new Token.EndTag("option"));
                        } else if (tb.currentElement().nodeName().equals("optgroup")) {
                            tb.process(new Token.EndTag("optgroup"));
                        }
                        tb.insert(start);
                        break;
                    } else if (name.equals("select")) {
                        tb.error(this);
                        return tb.process(new Token.EndTag("select"));
                    } else {
                        if (StringUtil.in(name, "input", "keygen", "textarea")) {
                            tb.error(this);
                            if (!tb.inSelectScope("select")) {
                                return false;
                            }
                            tb.process(new Token.EndTag("select"));
                            return tb.process(start);
                        } else if (name.equals("script")) {
                            return tb.process(t, InHead);
                        } else {
                            return anythingElse(t, tb);
                        }
                    }
                case JsonWriteContext.STATUS_EXPECT_VALUE:
                    String name2 = t.asEndTag().name();
                    if (name2.equals("optgroup")) {
                        if (tb.currentElement().nodeName().equals("option") && tb.aboveOnStack(tb.currentElement()) != null && tb.aboveOnStack(tb.currentElement()).nodeName().equals("optgroup")) {
                            tb.process(new Token.EndTag("option"));
                        }
                        if (!tb.currentElement().nodeName().equals("optgroup")) {
                            tb.error(this);
                            break;
                        } else {
                            tb.pop();
                            break;
                        }
                    } else if (name2.equals("option")) {
                        if (!tb.currentElement().nodeName().equals("option")) {
                            tb.error(this);
                            break;
                        } else {
                            tb.pop();
                            break;
                        }
                    } else if (name2.equals("select")) {
                        if (tb.inSelectScope(name2)) {
                            tb.popStackToClose(name2);
                            tb.resetInsertionMode();
                            break;
                        } else {
                            tb.error(this);
                            return false;
                        }
                    } else {
                        return anythingElse(t, tb);
                    }
                case JsonWriteContext.STATUS_EXPECT_NAME:
                    Token.Character c = t.asCharacter();
                    if (!c.getData().equals(TreeBuilderState.access$400())) {
                        tb.insert(c);
                        break;
                    } else {
                        tb.error(this);
                        return false;
                    }
                case 6:
                    if (!tb.currentElement().nodeName().equals(AdActivity.HTML_PARAM)) {
                        tb.error(this);
                        break;
                    }
                    break;
                default:
                    return anythingElse(t, tb);
            }
            return true;
        }

        private boolean anythingElse(Token t, TreeBuilder tb) {
            tb.error(this);
            return false;
        }
    },
    InSelectInTable {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isStartTag()) {
                if (StringUtil.in(t.asStartTag().name(), "caption", "table", "tbody", "tfoot", "thead", "tr", "td", "th")) {
                    tb.error(this);
                    tb.process(new Token.EndTag("select"));
                    return tb.process(t);
                }
            }
            if (t.isEndTag()) {
                if (StringUtil.in(t.asEndTag().name(), "caption", "table", "tbody", "tfoot", "thead", "tr", "td", "th")) {
                    tb.error(this);
                    if (!tb.inTableScope(t.asEndTag().name())) {
                        return false;
                    }
                    tb.process(new Token.EndTag("select"));
                    return tb.process(t);
                }
            }
            return tb.process(t, InSelect);
        }
    },
    AfterBody {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                return tb.process(t, InBody);
            }
            if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isStartTag() && t.asStartTag().name().equals(AdActivity.HTML_PARAM)) {
                return tb.process(t, InBody);
            } else {
                if (!t.isEndTag() || !t.asEndTag().name().equals(AdActivity.HTML_PARAM)) {
                    if (!t.isEOF()) {
                        tb.error(this);
                        tb.transition(InBody);
                        return tb.process(t);
                    }
                } else if (tb.isFragmentParsing()) {
                    tb.error(this);
                    return false;
                } else {
                    tb.transition(AfterAfterBody);
                }
            }
            return true;
        }
    },
    InFrameset {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                tb.insert(t.asCharacter());
            } else if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isStartTag()) {
                Token.StartTag start = t.asStartTag();
                String name = start.name();
                if (name.equals(AdActivity.HTML_PARAM)) {
                    return tb.process(start, InBody);
                }
                if (name.equals("frameset")) {
                    tb.insert(start);
                } else if (name.equals("frame")) {
                    tb.insertEmpty(start);
                } else if (name.equals("noframes")) {
                    return tb.process(start, InHead);
                } else {
                    tb.error(this);
                    return false;
                }
            } else if (!t.isEndTag() || !t.asEndTag().name().equals("frameset")) {
                if (!t.isEOF()) {
                    tb.error(this);
                    return false;
                } else if (!tb.currentElement().nodeName().equals(AdActivity.HTML_PARAM)) {
                    tb.error(this);
                    return true;
                }
            } else if (tb.currentElement().nodeName().equals(AdActivity.HTML_PARAM)) {
                tb.error(this);
                return false;
            } else {
                tb.pop();
                if (!tb.isFragmentParsing() && !tb.currentElement().nodeName().equals("frameset")) {
                    tb.transition(AfterFrameset);
                }
            }
            return true;
        }
    },
    AfterFrameset {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (TreeBuilderState.access$100(t)) {
                tb.insert(t.asCharacter());
            } else if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype()) {
                tb.error(this);
                return false;
            } else if (t.isStartTag() && t.asStartTag().name().equals(AdActivity.HTML_PARAM)) {
                return tb.process(t, InBody);
            } else {
                if (t.isEndTag() && t.asEndTag().name().equals(AdActivity.HTML_PARAM)) {
                    tb.transition(AfterAfterFrameset);
                } else if (t.isStartTag() && t.asStartTag().name().equals("noframes")) {
                    return tb.process(t, InHead);
                } else {
                    if (!t.isEOF()) {
                        tb.error(this);
                        return false;
                    }
                }
            }
            return true;
        }
    },
    AfterAfterBody {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype() || TreeBuilderState.access$100(t) || (t.isStartTag() && t.asStartTag().name().equals(AdActivity.HTML_PARAM))) {
                return tb.process(t, InBody);
            } else {
                if (!t.isEOF()) {
                    tb.error(this);
                    tb.transition(InBody);
                    return tb.process(t);
                }
            }
            return true;
        }
    },
    AfterAfterFrameset {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            if (t.isComment()) {
                tb.insert(t.asComment());
            } else if (t.isDoctype() || TreeBuilderState.access$100(t) || (t.isStartTag() && t.asStartTag().name().equals(AdActivity.HTML_PARAM))) {
                return tb.process(t, InBody);
            } else {
                if (!t.isEOF()) {
                    if (t.isStartTag() && t.asStartTag().name().equals("nofrmes")) {
                        return tb.process(t, InHead);
                    }
                    tb.error(this);
                    tb.transition(InBody);
                    return tb.process(t);
                }
            }
            return true;
        }
    },
    ForeignContent {
        /* access modifiers changed from: package-private */
        public boolean process(Token t, TreeBuilder tb) {
            return true;
        }
    };
    
    private static String nullString = String.valueOf(0);

    /* access modifiers changed from: package-private */
    public abstract boolean process(Token token, TreeBuilder treeBuilder);

    /* renamed from: org.jsoup.parser.TreeBuilderState$24  reason: invalid class name */
    static /* synthetic */ class AnonymousClass24 {
        static final /* synthetic */ int[] $SwitchMap$org$jsoup$parser$Token$TokenType = new int[Token.TokenType.values().length];

        static {
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.Comment.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.Doctype.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.StartTag.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.EndTag.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.Character.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$org$jsoup$parser$Token$TokenType[Token.TokenType.EOF.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
        }
    }

    private static boolean isWhitespace(Token t) {
        if (!t.isCharacter()) {
            return false;
        }
        String data = t.asCharacter().getData();
        for (int i = 0; i < data.length(); i++) {
            if (!Character.isWhitespace(data.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    private static void handleRcData(Token.StartTag startTag, TreeBuilder tb) {
        tb.insert(startTag);
        tb.tokeniser.transition(TokeniserState.Rcdata);
        tb.markInsertionMode();
        tb.transition(Text);
    }

    private static void handleRawtext(Token.StartTag startTag, TreeBuilder tb) {
        tb.insert(startTag);
        tb.tokeniser.transition(TokeniserState.Rawtext);
        tb.markInsertionMode();
        tb.transition(Text);
    }
}
