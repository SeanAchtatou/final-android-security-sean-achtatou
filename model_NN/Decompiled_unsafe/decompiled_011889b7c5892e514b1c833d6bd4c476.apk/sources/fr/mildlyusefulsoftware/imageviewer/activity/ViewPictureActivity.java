package fr.mildlyusefulsoftware.imageviewer.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import com.apperhand.device.android.AndroidSDKProvider;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import fr.mildlyusefulsoftware.imageviewer.R;
import fr.mildlyusefulsoftware.imageviewer.service.Picture;
import java.io.IOException;

public abstract class ViewPictureActivity extends Activity {
    /* access modifiers changed from: private */
    public static String TAG = "cutekitty";
    private AdView adView;
    /* access modifiers changed from: private */
    public int currentPosition = 0;

    /* access modifiers changed from: protected */
    public abstract String getAdMobId();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "onCreate");
        AndroidSDKProvider.initSDK(this);
        requestWindowFeature(3);
        setContentView(R.layout.view_picture_layout);
        setFeatureDrawableResource(3, R.drawable.icon);
        Gallery pictureList = (Gallery) findViewById(R.id.pictureList);
        if (savedInstanceState != null) {
            this.currentPosition = savedInstanceState.getInt("POSITION");
            Log.d(TAG, "restore position " + this.currentPosition);
            pictureList.setSelection(this.currentPosition, true);
            loadImageFromPosition(this.currentPosition);
        } else {
            loadImageFromPosition(this.currentPosition);
        }
        pictureList.setAdapter((SpinnerAdapter) new ImageAdapter(this));
        pictureList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                ViewPictureActivity.this.currentPosition = position;
                ViewPictureActivity.this.loadImageFromPosition(position);
            }
        });
        initAdBannerView();
    }

    /* access modifiers changed from: private */
    public void loadImageFromPosition(final int pos) {
        new AsyncTask<Void, Void, Bitmap>() {
            /* access modifiers changed from: protected */
            public Bitmap doInBackground(Void... params) {
                try {
                    return Picture.getBitmapFromPicture(PicturePager.getInstance(this).getPictureAt(pos));
                } catch (IOException e1) {
                    Log.e(ViewPictureActivity.TAG, e1.getMessage(), e1);
                    return null;
                }
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Bitmap b) {
                if (b != null) {
                    ((ImageView) ViewPictureActivity.this.findViewById(R.id.pictureView)).setImageBitmap(b);
                }
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void initAdBannerView() {
        try {
            this.adView = new AdView(this, AdSize.BANNER, getAdMobId());
            LinearLayout adLayout = new LinearLayout(this);
            adLayout.addView(adLayout);
            ((ViewGroup) findViewById(R.id.view_picutre_root_layout)).addView(this.adView);
            this.adView.loadAd(new AdRequest());
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onPause();
        if (this.adView != null) {
            this.adView.destroy();
        }
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt("POSITION", this.currentPosition);
        Log.d(TAG, "position " + this.currentPosition);
        super.onSaveInstanceState(savedInstanceState);
    }
}
