package fr.mildlyusefulsoftware.imageviewer.activity;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;

public class ImageAdapter extends BaseAdapter implements SpinnerAdapter {
    private Context mContext;
    int mGalleryItemBackground;

    public ImageAdapter(Context c) {
        this.mContext = c;
    }

    public int getCount() {
        return PicturePager.getInstance(this.mContext).getNumberOfPictures();
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(this.mContext);
        byte[] thumbnailBytes = PicturePager.getInstance(this.mContext).getPictureAt(position).getThumbnail();
        imageView.setImageBitmap(BitmapFactory.decodeByteArray(thumbnailBytes, 0, thumbnailBytes.length));
        imageView.setLayoutParams(new Gallery.LayoutParams(150, 100));
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setBackgroundResource(this.mGalleryItemBackground);
        return imageView;
    }
}
