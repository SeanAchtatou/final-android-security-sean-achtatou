package fr.mildlyusefulsoftware.imageviewer.activity;

import android.content.Context;
import android.util.Log;
import fr.mildlyusefulsoftware.imageviewer.service.DatabaseHelper;
import fr.mildlyusefulsoftware.imageviewer.service.Picture;
import java.io.IOException;
import java.util.List;

public class PicturePager {
    private static String TAG = "cutekitty";
    private static PicturePager instance;
    private List<Picture> pictures;

    private PicturePager(Context context) {
        DatabaseHelper db = DatabaseHelper.connect(context);
        try {
            this.pictures = db.getPictures();
            Log.d("picturepager", String.valueOf(this.pictures.size()));
            if (this.pictures.isEmpty()) {
                db.copyDatabase();
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage(), e);
        } catch (Throwable th) {
            db.release();
            throw th;
        }
        db.release();
        DatabaseHelper db2 = DatabaseHelper.connect(context);
        try {
            this.pictures = db2.getPictures();
        } finally {
            db2.release();
        }
    }

    public static PicturePager getInstance(Context context) {
        if (instance == null) {
            instance = new PicturePager(context);
        }
        return instance;
    }

    public Picture getPictureAt(int i) {
        return this.pictures.get(i);
    }

    public int getNumberOfPictures() {
        return this.pictures.size();
    }
}
