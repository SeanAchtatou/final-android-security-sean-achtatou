package fr.mildlyusefulsoftware.imageviewer;

public final class R {

    public static final class attr {
    }

    public static final class dimen {
        public static final int widget_margin = 2130968576;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int pictureList = 2131099650;
        public static final int pictureView = 2131099649;
        public static final int view_picutre_root_layout = 2131099648;
        public static final int widgetPictureView = 2131099651;
    }

    public static final class layout {
        public static final int view_picture_layout = 2130903040;
        public static final int widget_layout = 2130903041;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int startapp_appid = 2131034114;
        public static final int startapp_devid = 2131034113;
    }
}
