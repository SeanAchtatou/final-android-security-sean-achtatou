package fr.mildlyusefulsoftware.imageviewer.service;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class DatabaseHelper {
    private static final String DB_NAME = "PICTURES.DB";
    private static ReentrantLock LOCK = new ReentrantLock();
    /* access modifiers changed from: private */
    public static String TAG = "cutekitty";
    private SQLiteDatabase db = this.internalDBHelper.getWritableDatabase();
    private InternalDBHelper internalDBHelper;

    public static DatabaseHelper connect(Context context) {
        LOCK.lock();
        return new DatabaseHelper(context);
    }

    private DatabaseHelper(Context context) {
        this.internalDBHelper = new InternalDBHelper(context, DB_NAME, null, 2);
    }

    public void release() {
        try {
            this.db.close();
        } finally {
            LOCK.unlock();
        }
    }

    public void putPictures(Collection<Picture> pictures) {
        for (Picture p : pictures) {
            ContentValues values = new ContentValues();
            values.put(InternalDBHelper.COL_ID, Integer.valueOf(p.getId()));
            values.put(InternalDBHelper.COL_IMAGE_URL, p.getImageURL());
            values.put(InternalDBHelper.COL_THUMBNAIL, p.getThumbnail());
            this.db.insert(InternalDBHelper.TABLE_PICTURES, null, values);
        }
    }

    public List<Picture> getPictures() {
        List<Picture> pictures = new ArrayList<>();
        Cursor c = this.db.query(InternalDBHelper.TABLE_PICTURES, new String[]{InternalDBHelper.COL_ID, InternalDBHelper.COL_IMAGE_URL, InternalDBHelper.COL_THUMBNAIL}, null, null, null, null, "ID DESC");
        if (c.getCount() != 0) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                pictures.add(new Picture(c.getInt(0), c.getString(1), c.getBlob(2)));
                c.moveToNext();
            }
        }
        c.close();
        return pictures;
    }

    public void copyDatabase() throws IOException {
        this.internalDBHelper.copyDatabase();
    }

    class InternalDBHelper extends SQLiteOpenHelper {
        public static final String COL_ID = "ID";
        public static final String COL_IMAGE_URL = "URL";
        public static final String COL_THUMBNAIL = "THUMBNAIL";
        private static final String CREATE_BDD = "CREATE TABLE PICTURES (ID INTEGER PRIMARY KEY, URL TEXT NOT NULL, THUMBNAIL BLOB NOT NULL);";
        public static final String TABLE_PICTURES = "PICTURES";
        private Context context;

        public InternalDBHelper(Context context2, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context2, name, factory, version);
            this.context = context2;
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_BDD);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                Log.i(DatabaseHelper.TAG, "Upgrading database");
                copyDatabase();
            } catch (IOException e) {
                Log.e(DatabaseHelper.TAG, e.getMessage());
            }
        }

        public void copyDatabase() throws IOException {
            Log.i(DatabaseHelper.TAG, "copying database");
            File dbFile = this.context.getDatabasePath(DatabaseHelper.DB_NAME);
            ZipInputStream zis = new ZipInputStream(this.context.getAssets().open("PICTURES.DB.zip"));
            while (true) {
                ZipEntry entry = zis.getNextEntry();
                if (entry == null) {
                    zis.close();
                    return;
                }
                System.out.println("Extracting: " + entry);
                byte[] data = new byte[2048];
                BufferedOutputStream dest = new BufferedOutputStream(new FileOutputStream(dbFile), 2048);
                while (true) {
                    int count = zis.read(data, 0, 2048);
                    if (count == -1) {
                        break;
                    }
                    dest.write(data, 0, count);
                }
                dest.flush();
                dest.close();
            }
        }
    }
}
