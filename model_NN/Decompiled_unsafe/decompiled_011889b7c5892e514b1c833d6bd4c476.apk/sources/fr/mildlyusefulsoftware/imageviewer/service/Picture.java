package fr.mildlyusefulsoftware.imageviewer.service;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class Picture {
    private int id;
    private String imageURL;
    private byte[] thumbnail;

    public Picture(int id2, String imageURL2, byte[] thumbnail2) {
        this.thumbnail = thumbnail2;
        this.imageURL = imageURL2;
        this.id = id2;
    }

    public byte[] getThumbnail() {
        return this.thumbnail;
    }

    public void setThumbnail(byte[] thumbnail2) {
        this.thumbnail = thumbnail2;
    }

    public String getImageURL() {
        return this.imageURL;
    }

    public void setImageURL(String imageURL2) {
        this.imageURL = imageURL2;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public static Bitmap getBitmapFromPicture(Picture picture) throws IOException {
        return BitmapFactory.decodeStream(new BufferedInputStream(new URL(picture.getImageURL()).openStream()));
    }

    public static byte[] getPictureThumbnail(String url) throws IOException {
        InputStream in = null;
        try {
            InputStream in2 = new BufferedInputStream(new URL(url).openStream());
            try {
                Bitmap imageBitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeStream(in2), 128, 128, false);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
                byte[] byteArray = baos.toByteArray();
                in2.close();
                return byteArray;
            } catch (Throwable th) {
                th = th;
                in = in2;
                in.close();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            in.close();
            throw th;
        }
    }
}
