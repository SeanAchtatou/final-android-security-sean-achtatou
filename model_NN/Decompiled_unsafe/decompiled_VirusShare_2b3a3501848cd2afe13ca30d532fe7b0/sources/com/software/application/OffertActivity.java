package com.software.application;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class OffertActivity extends Activity {
    private static final int RESULT_OK = 1;
    private Button backButton;
    private Button yesButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(0);
        setContentView((int) R.layout.off);
        setListeners();
    }

    private void setListeners() {
        this.yesButton = (Button) findViewById(R.id.yes_button_off);
        this.yesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OffertActivity.this.setResult(1);
                OffertActivity.this.finish();
            }
        });
        this.backButton = (Button) findViewById(R.id.back_button_off);
        this.backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OffertActivity.this.setResult(0);
                OffertActivity.this.finish();
            }
        });
    }
}
