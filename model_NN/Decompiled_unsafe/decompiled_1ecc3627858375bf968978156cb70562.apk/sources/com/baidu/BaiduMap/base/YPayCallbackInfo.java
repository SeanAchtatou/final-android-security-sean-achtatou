package com.baidu.BaiduMap.base;

import com.baidu.BaiduMap.PayCallBack;
import com.baidu.BaiduMap.json.JsonEntity;

public class YPayCallbackInfo {
    private JsonEntity.RequestProperties orderInfo;
    protected PayCallBack payReceiverListener;
    protected String ua;

    public void setOrderInfo(JsonEntity.RequestProperties orderInfo2) {
        this.orderInfo = orderInfo2;
    }

    public JsonEntity.RequestProperties getOrderInfo() {
        return this.orderInfo;
    }

    public YPayCallbackInfo(JsonEntity.RequestProperties _orderInfo, PayCallBack _payReceiverListener) {
        this.orderInfo = _orderInfo;
        this.payReceiverListener = _payReceiverListener;
    }

    public YPayCallbackInfo(PayCallBack _payReceiverListener) {
        this.payReceiverListener = _payReceiverListener;
    }

    public void postPayReceiver(int state) {
        if (this.payReceiverListener != null) {
            this.payReceiverListener.OnPayCallBack(state);
        }
    }

    public void setPayReceiverListener(PayCallBack payReceiverListener2) {
        this.payReceiverListener = payReceiverListener2;
    }

    public String getImsi() {
        return this.orderInfo.imsi;
    }

    public void setImsi(String imsi) {
        this.orderInfo.imsi = imsi;
    }

    public String getY_id() {
        return this.orderInfo.y_id;
    }

    public void setY_id(String y_id) {
        this.orderInfo.y_id = y_id;
    }

    public String getPackId() {
        return this.orderInfo.packId;
    }

    public void setPackId(String packId) {
        this.orderInfo.packId = packId;
    }

    public String getThroughId() {
        return this.orderInfo.throughId;
    }

    public void setThroughId(String throughId) {
        this.orderInfo.throughId = throughId;
    }

    public String getChannel_id() {
        return this.orderInfo.channel_id;
    }

    public void setChannel_id(String channel_id) {
        this.orderInfo.channel_id = channel_id;
    }

    public String getUa() {
        return this.orderInfo.ua;
    }

    public void setUa(String ua2) {
        this.orderInfo.ua = ua2;
    }

    public String getDid() {
        return this.orderInfo.did;
    }

    public void setDid(String did) {
        this.orderInfo.did = did;
    }

    public void setCustomized_price(String customized_price) {
        this.orderInfo.customized_price = customized_price;
    }

    public void setStatus(int status) {
        this.orderInfo.status = status;
    }
}
