package com.baidu.BaiduMap.util;

public class Kode {
    private static int[] k = {17, 50, 97, 4, 94, 34, 66, 22, 24, 70};

    public static String a(String str) {
        if (str == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        char[] result = str.toCharArray();
        int i = 0;
        while (i < result.length) {
            try {
                sb.append((char) (k[i % k.length] + result[i]));
                i++;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static String e(String code) {
        if (code == null) {
            return "";
        }
        char[] result = new StringBuilder(code).toString().toCharArray();
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < result.length) {
            try {
                sb.append((char) (result[i] - k[i % k.length]));
                i++;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
    }
}
