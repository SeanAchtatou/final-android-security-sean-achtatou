package com.baidu.BaiduMap.util;

import java.io.UnsupportedEncodingException;
import java.util.Random;

public class YCode {
    private static int[] k = {Constants.CHANNEL_LT_WS_SKW3, 182, 134, 121, 199, 183, 121, 191, 140, 190};

    public static String a(String str) {
        if (str == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        char[] result = str.toCharArray();
        int i = 0;
        while (i < result.length) {
            try {
                char c = result[i];
                char cc = (char) (k[i % k.length] + c);
                System.out.print(String.valueOf(Integer.toHexString(c)) + "-" + Integer.toHexString(cc) + ":");
                sb.append(cc);
                i++;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static String b(String code) {
        if (code == null) {
            return "";
        }
        char[] result = new StringBuilder(code).toString().toCharArray();
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < result.length) {
            try {
                sb.append((char) (result[i] - k[i % k.length]));
                i++;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static byte[] a(byte[] bytes) {
        if (bytes == null) {
            return bytes;
        }
        byte[] retBytes = new byte[bytes.length];
        for (int i = 0; i < bytes.length; i++) {
            retBytes[i] = (byte) (bytes[i] ^ k[i % k.length]);
        }
        return retBytes;
    }

    public static String getString(byte[] bytes) {
        String retString = new String();
        try {
            return new String(a(bytes), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return retString;
        }
    }

    public static void setK(int[] k2) {
        k = k2;
    }

    public static void setK(String kString) {
        String[] strArrayStrings = kString.split(",");
        int[] k2 = new int[strArrayStrings.length];
        for (int i = 0; i < k2.length; i++) {
            k2[i] = Integer.valueOf(strArrayStrings[i].trim()).intValue();
        }
        k = k2;
    }

    public static void main(String[] args) {
        Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < 10; i++) {
            System.out.print(String.valueOf(((random.nextInt() & 255) % 100) + 100) + ", ");
        }
        System.out.println();
        System.out.println(a("123"));
        System.out.println(b(a("\n")));
    }
}
