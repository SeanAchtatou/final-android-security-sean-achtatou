package com.baidu.BaiduMap;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import com.baidu.BaiduMap.util.Constants;
import java.util.HashMap;
import java.util.Map;

public class Pay {
    public static final int PayState_CANCEL = 2;
    public static final int PayState_FAILURE = 1;
    public static final int PayState_SUCCESS = 0;
    public static boolean canpay = false;
    private static Pay ycCpManager = null;
    public static int yunyingshang = 0;

    private Pay() {
    }

    public static Pay getInstance() {
        if (ycCpManager == null) {
            ycCpManager = new Pay();
        }
        return ycCpManager;
    }

    public static void init(Context ctx) {
        PayManager.getInstance().init(ctx);
    }

    public static void onResume(Context ctx) {
    }

    public static void onPause(Context ctx) {
    }

    public static void close(Context ctx) {
    }

    public static void pay(Context ctx, String price, int payItemID, String str, String product, String Did, String extData, PayCallBack receiver) {
        PayManager.getInstance().pay(ctx, price, str, product, extData, payItemID, Did, receiver);
    }

    public static HashMap<String, Map<String, Object>> getNeedPayList(Context ctx) {
        return PayManager.getInstance().getNeedPayList(ctx);
    }

    public static boolean getPayCodeIsOpen(int payCode) {
        if (Constants.debug || PayManager.initEntity == null || PayManager.initEntity.mapPayPoint == null || PayManager.initEntity.mapPayPoint.get(new StringBuilder().append(payCode).toString()) == null) {
            return false;
        }
        Log.i("m_pay", "【getPayCodeIsOpen】" + payCode + ":" + PayManager.initEntity.mapPayPoint.get(new StringBuilder().append(payCode).toString()));
        return PayManager.initEntity.mapPayPoint.get(new StringBuilder().append(payCode).toString()).isopen;
    }

    public static String getPayCodeName(int payCode) {
        if (PayManager.initEntity == null || PayManager.initEntity.mapPayPoint == null || PayManager.initEntity.mapPayPoint.get(new StringBuilder().append(payCode).toString()) == null) {
            return "";
        }
        Log.i("m_pay", "【getPayCodeName】" + payCode + ":" + PayManager.initEntity.mapPayPoint.get(new StringBuilder().append(payCode).toString()));
        return PayManager.initEntity.mapPayPoint.get(new StringBuilder().append(payCode).toString()).dname;
    }

    public static int getPayCodePrice(int payCode) {
        String price = "0";
        if (!(PayManager.initEntity == null || PayManager.initEntity.mapPayPoint == null || PayManager.initEntity.mapPayPoint.get(new StringBuilder().append(payCode).toString()) == null)) {
            Log.i("m_pay", "【getPayCodePrice】" + payCode + ":" + PayManager.initEntity.mapPayPoint.get(new StringBuilder().append(payCode).toString()));
            if (yunyingshang == 1) {
                price = PayManager.initEntity.mapPayPoint.get(new StringBuilder().append(payCode).toString()).yprice;
            } else if (yunyingshang == 2) {
                price = PayManager.initEntity.mapPayPoint.get(new StringBuilder().append(payCode).toString()).lprice;
            } else {
                price = PayManager.initEntity.mapPayPoint.get(new StringBuilder().append(payCode).toString()).dprice;
            }
            if (price == null || price.equals("")) {
                price = "0";
            }
        }
        if (Integer.parseInt(price) > 0) {
            return Integer.parseInt(price);
        }
        return 20;
    }

    public static void showDialog(Context ctx, String title, String str, String positive, String negative, DialogInterface.OnClickListener positiveButton, DialogInterface.OnClickListener negativeButton) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(title);
        builder.setMessage(str);
        builder.setPositiveButton(positive, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        if (negativeButton != null) {
            builder.setNegativeButton(negative, negativeButton);
        }
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }
}
