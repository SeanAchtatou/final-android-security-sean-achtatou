package com.baidu.BaiduMap.json;

import android.content.Context;
import com.baidu.BaiduMap.json.JsonEntity;
import com.baidu.BaiduMap.util.Utils;
import org.json.JSONObject;

public class MessageEntity implements JsonEntity.JsonInterface {
    public String customized_price = "";
    public String did = "";
    public String gameId = "";
    public String imei = "";
    public String imsi = "";
    public String message = "";
    public String orderNo = "";
    public String packId = "";
    public int status;
    public String throughId = "";
    public String tophone = "";
    public String y_id = "";

    private MessageEntity() {
    }

    public MessageEntity(Context ctx, String orderid, String _tophone, String _message, int _price, int _status, int _throughId, String _did) {
        this.orderNo = orderid;
        this.y_id = Utils.getAppID(ctx);
        this.imsi = Utils.getIMSI(ctx);
        this.imei = Utils.getIMEI(ctx);
        this.packId = Utils.getPackId(ctx);
        this.gameId = Utils.getGameId(ctx);
        this.customized_price = new StringBuilder(String.valueOf(_price)).toString();
        this.throughId = new StringBuilder(String.valueOf(_throughId)).toString();
        this.status = _status;
        this.tophone = _tophone;
        this.message = _message;
        this.did = _did;
    }

    public MessageEntity(Context ctx, String orderid, String _tophone, String _message, int _status) {
        this.orderNo = orderid;
        this.y_id = "0";
        this.imsi = "0";
        this.imei = "0";
        this.packId = "0";
        this.gameId = "0";
        this.customized_price = "0";
        this.throughId = "0";
        this.tophone = _tophone;
        this.message = _message;
        this.status = _status;
    }

    public String toString() {
        return "RequestProperties [orderNo=" + this.orderNo + ", y_id=" + this.y_id + ", imsi=" + this.imsi + ", imei=" + this.imei + ", customized_price=" + this.customized_price + ", status=" + this.status + ", packId=" + this.packId + ", gameId=" + this.gameId + ", throughId=" + this.throughId + ", tophone=" + this.tophone + ", message=" + this.message + ", did=" + this.did + "]";
    }

    public JSONObject buildJson() {
        JSONObject json = new JSONObject();
        JSONObject json2 = new JSONObject();
        try {
            json.put("orderNo", this.orderNo);
            json.put("y_id", this.y_id);
            json.put("imsi", this.imsi);
            json.put("imei", this.imei);
            json.put("customized_price", this.customized_price);
            json.put("status", this.status);
            json.put("throughId", this.throughId);
            json.put("packId", this.packId);
            json.put("tophone", this.tophone);
            json.put("message", this.message);
            json.put("did", this.did);
            json2.put(getShortName(), json);
            return json2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void parseJson(JSONObject json) {
        String string;
        String string2;
        String str = null;
        if (json != null) {
            try {
                this.orderNo = json.isNull("orderid") ? null : json.getString("orderid");
                this.y_id = json.isNull("y_id") ? null : json.getString("y_id");
                this.imsi = json.isNull("imsi") ? null : json.getString("imsi");
                this.imei = json.isNull("imei") ? null : json.getString("imei");
                if (json.isNull("customized_price")) {
                    string = null;
                } else {
                    string = json.getString("customized_price");
                }
                this.customized_price = string;
                this.status = json.isNull("status") ? 0 : json.getInt("status");
                if (json.isNull("throughId")) {
                    string2 = null;
                } else {
                    string2 = json.getString("throughId");
                }
                this.throughId = string2;
                this.packId = json.isNull("packId") ? null : json.getString("packId");
                this.tophone = json.isNull("tophone") ? null : json.getString("tophone");
                this.message = json.isNull("message") ? null : json.getString("message");
                if (!json.isNull("did")) {
                    str = json.getString("did");
                }
                this.did = str;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getShortName() {
        return "a";
    }

    public MessageEntity clone() {
        MessageEntity cloneObj = new MessageEntity();
        cloneObj.orderNo = this.orderNo;
        cloneObj.y_id = this.y_id;
        cloneObj.imsi = this.imsi;
        cloneObj.imei = this.imei;
        cloneObj.customized_price = this.customized_price;
        cloneObj.status = this.status;
        cloneObj.throughId = this.throughId;
        cloneObj.packId = this.packId;
        cloneObj.tophone = this.tophone;
        cloneObj.message = this.message;
        cloneObj.did = this.did;
        return cloneObj;
    }
}
