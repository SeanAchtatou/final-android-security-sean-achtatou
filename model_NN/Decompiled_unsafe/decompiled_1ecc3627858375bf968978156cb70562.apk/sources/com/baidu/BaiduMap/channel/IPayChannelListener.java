package com.baidu.BaiduMap.channel;

public interface IPayChannelListener {
    void onPayCanceled();

    void onPayFailed();

    void onPaySucceeded();
}
