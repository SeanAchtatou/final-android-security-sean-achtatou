package com.imocha;

import MobWin.cnst.PROTOCOL_ENCODING;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

final class au extends WebViewClient {
    private /* synthetic */ a a;

    /* synthetic */ au(a aVar) {
        this(aVar, (byte) 0);
    }

    private au(a aVar, byte b) {
        this.a = aVar;
    }

    public final void onPageFinished(WebView webView, String str) {
        Log.v("MyWebView", "onPageFinished");
        super.onPageFinished(webView, str);
        if (webView.getVisibility() != 0) {
            webView.setVisibility(0);
        }
        if (this.a.b != null) {
            this.a.b.a();
        }
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        if (this.a.b != null) {
            this.a.b.c();
        }
        Log.v("MyWebView", "WebViewClient  onPageStarted");
        super.onPageStarted(webView, str, bitmap);
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        if (this.a.b != null) {
            this.a.b.d();
        }
        Log.v("MyWebView", "WebViewClient  onReceivedError");
        super.onReceivedError(webView, i, str, str2);
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        String str2;
        String str3;
        String substring;
        String substring2;
        Log.v("MyWebView", "shouldOverrideUrlLoading");
        if ((this.a.a == 0 || this.a.a == 1) && System.currentTimeMillis() - this.a.c < 2000) {
            return true;
        }
        this.a.c = System.currentTimeMillis();
        if (str.startsWith("tel:")) {
            String substring3 = str.substring(4);
            try {
                Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + substring3));
                intent.addFlags(268435456);
                this.a.getContext().startActivity(intent);
            } catch (Exception e) {
                try {
                    this.a.getContext().startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + substring3)));
                } catch (Exception e2) {
                    e.printStackTrace();
                    e2.printStackTrace();
                }
            }
        } else if (str.startsWith("sms:")) {
            String str4 = "";
            if (str.contains("?body=")) {
                int indexOf = str.indexOf("?body=");
                String substring4 = str.substring(indexOf + 6);
                substring2 = str.substring(4, indexOf);
                str4 = substring4;
            } else {
                substring2 = str.substring(4);
            }
            Intent intent2 = new Intent();
            intent2.setAction("android.intent.action.SENDTO");
            intent2.setData(Uri.parse("smsto:" + substring2));
            try {
                str4 = URLDecoder.decode(str4, PROTOCOL_ENCODING.value);
            } catch (UnsupportedEncodingException e3) {
                e3.printStackTrace();
            }
            intent2.putExtra("sms_body", str4);
            this.a.getContext().startActivity(intent2);
        } else if (str.startsWith("mailto:")) {
            Context context = this.a.getContext();
            String str5 = "";
            int indexOf2 = str.indexOf("?subject=");
            if (indexOf2 < 0) {
                indexOf2 = str.indexOf("?Subject=");
            }
            int indexOf3 = str.indexOf("&body=");
            if (indexOf3 <= 0) {
                indexOf3 = str.indexOf("&Body=");
            }
            if (indexOf2 != -1 && indexOf3 != -1) {
                substring = str.substring(7, indexOf2);
                String substring5 = str.substring(indexOf2 + 9, indexOf3);
                str3 = substring5;
                str5 = str.substring(indexOf3 + 6);
            } else if (indexOf2 != -1 && indexOf3 == -1) {
                substring = str.substring(7, indexOf2);
                str3 = str.substring(indexOf2 + 9);
            } else if (indexOf2 != -1 || str.indexOf("&body=") == -1) {
                str3 = "";
                substring = str.substring(7);
            } else {
                String substring6 = str.substring(7, indexOf3);
                str5 = str.substring(indexOf3 + 6);
                str3 = "";
                substring = substring6;
            }
            try {
                str3 = URLDecoder.decode(str3, PROTOCOL_ENCODING.value);
                str5 = URLDecoder.decode(str5, PROTOCOL_ENCODING.value);
            } catch (UnsupportedEncodingException e4) {
                UnsupportedEncodingException unsupportedEncodingException = e4;
                unsupportedEncodingException.printStackTrace();
                str3 = str3;
            }
            Intent intent3 = new Intent("android.intent.action.SEND");
            intent3.setType("plain/text");
            intent3.putExtra("android.intent.extra.EMAIL", new String[]{substring});
            intent3.putExtra("android.intent.extra.SUBJECT", str3);
            intent3.putExtra("android.intent.extra.TEXT", str5);
            context.startActivity(intent3);
        } else if (str.contains("#hdtshortcut")) {
            String decode = Uri.decode(str);
            int indexOf4 = decode.indexOf("#hdtshortcut=");
            if (indexOf4 < 0) {
                return true;
            }
            String substring7 = decode.substring(indexOf4 + 13);
            try {
                Intent intent4 = new Intent("android.intent.action.VIEW", Uri.parse(decode.replace("#hdtshortcut=" + substring7, "")));
                intent4.addCategory("android.intent.category.LAUNCHER");
                intent4.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                Intent intent5 = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
                intent5.putExtra("android.intent.extra.shortcut.INTENT", intent4);
                intent5.putExtra("android.intent.extra.shortcut.NAME", substring7);
                this.a.getContext().sendBroadcast(intent5);
                Toast.makeText(this.a.getContext(), "创建\"" + substring7 + "\"快捷方式成功", 100).show();
                return true;
            } catch (Exception e5) {
            }
        } else if ((this.a.a == 0 || this.a.a == 1) && str.contains("#hdtsysbrw")) {
            try {
                String replace = str.replace("#hdtsysbrw", "");
                try {
                    Intent intent6 = new Intent();
                    intent6.setAction("android.intent.action.VIEW");
                    intent6.setData(Uri.parse(replace));
                    intent6.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
                    this.a.getContext().startActivity(intent6);
                    this.a.b((am) this.a.d.get());
                    return true;
                } catch (Exception e6) {
                    str2 = replace;
                    Intent intent7 = new Intent();
                    intent7.setClass(this.a.getContext(), BrowserActivity.class);
                    intent7.putExtra("url", str2);
                    this.a.getContext().startActivity(intent7);
                    this.a.b((am) this.a.d.get());
                    return false;
                }
            } catch (Exception e7) {
                str2 = str;
            }
        } else if (this.a.b == null || !this.a.b.b()) {
            return super.shouldOverrideUrlLoading(webView, str);
        } else {
            Intent intent8 = new Intent();
            intent8.setClass(this.a.getContext(), BrowserActivity.class);
            intent8.putExtra("url", str);
            this.a.getContext().startActivity(intent8);
            this.a.b((am) this.a.d.get());
            return true;
        }
        return true;
    }
}
