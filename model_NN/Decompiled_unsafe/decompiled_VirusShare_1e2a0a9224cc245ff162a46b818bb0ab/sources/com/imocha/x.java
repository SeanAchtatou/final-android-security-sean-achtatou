package com.imocha;

import java.util.ArrayList;

public final class x {
    private static String[] a = {"jpg", "jpeg", "gif", "png"};
    private static String[] b = {"aac", "amr", "imelody", "mid", "midi", "mp3", "mpeg3", "mpg", "mp4", "x-mid", "ogg"};
    private static String[] c = {"3gpp", "3gp", "mp4"};
    private static final ArrayList d = new ArrayList();
    private static final ArrayList e = new ArrayList();
    private static final ArrayList f = new ArrayList();
    private static final ArrayList g = new ArrayList();

    static {
        for (String add : a) {
            d.add(add);
        }
        for (String add2 : b) {
            e.add(add2);
        }
        for (String add3 : c) {
            f.add(add3);
        }
        g.add("zip");
        g.add("html.zip");
    }

    private x() {
    }

    public static boolean a(String str) {
        return str != null && d.contains(str);
    }

    public static boolean b(String str) {
        return str != null && g.contains(str);
    }
}
