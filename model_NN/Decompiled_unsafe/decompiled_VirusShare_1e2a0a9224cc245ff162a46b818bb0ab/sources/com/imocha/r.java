package com.imocha;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import org.xml.sax.Attributes;

final class r extends b {
    r() {
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, String str2) {
        if (str.equals("content")) {
            this.c = str2;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(Context context, am amVar) {
        Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + this.a));
        intent.putExtra("sms_body", this.c);
        context.startActivity(intent);
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void b(String str, Attributes attributes) {
    }
}
