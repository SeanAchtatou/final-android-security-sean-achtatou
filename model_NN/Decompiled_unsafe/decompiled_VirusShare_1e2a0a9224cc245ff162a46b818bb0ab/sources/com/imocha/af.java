package com.imocha;

import android.content.Context;
import android.location.Location;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

final class af {
    static aj a;
    private static af b = new af();
    private Handler c = new Handler();
    private Random d = new Random();
    private bb e = new bb(this);
    private boolean f = true;

    private af() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0070  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long a(byte[] r11, int r12) {
        /*
            r6 = 65535(0xffff, float:9.1834E-41)
            r5 = 256(0x100, float:3.59E-43)
            r10 = 31
            r9 = 30
            r8 = 0
            int[] r0 = new int[r5]
            r1 = r8
        L_0x000d:
            if (r1 < r5) goto L_0x001a
            r1 = r8
            r2 = r8
            r3 = r12
        L_0x0012:
            int r4 = r3 + -1
            if (r3 != 0) goto L_0x0038
            if (r2 < 0) goto L_0x007b
            long r0 = (long) r2
        L_0x0019:
            return r0
        L_0x001a:
            int r2 = r1 << 8
            r3 = r8
        L_0x001d:
            r4 = 8
            if (r3 < r4) goto L_0x0026
            r0[r1] = r2
            int r1 = r1 + 1
            goto L_0x000d
        L_0x0026:
            r4 = 32768(0x8000, float:4.5918E-41)
            r4 = r4 & r2
            if (r4 == 0) goto L_0x0034
            int r2 = r2 << 1
            r2 = r2 & r6
            r2 = r2 ^ 4129(0x1021, float:5.786E-42)
        L_0x0031:
            int r3 = r3 + 1
            goto L_0x001d
        L_0x0034:
            int r2 = r2 << 1
            r2 = r2 & r6
            goto L_0x0031
        L_0x0038:
            byte r3 = r11[r1]
            r5 = 36
            if (r3 != r5) goto L_0x0086
            char[] r3 = new char[r10]
            char[] r5 = new char[r10]
            r5[r9] = r8
            java.lang.String r6 = "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"
            java.lang.System.arraycopy(r6, r8, r5, r8, r9)
            java.lang.System.arraycopy(r11, r1, r3, r8, r9)
            r3[r9] = r8
            boolean r3 = java.util.Arrays.equals(r3, r5)
            if (r3 != 0) goto L_0x0086
            int r1 = r1 + 150
            int r3 = r4 + -150
        L_0x0058:
            if (r2 < 0) goto L_0x0070
            long r4 = (long) r2
        L_0x005b:
            r6 = 256(0x100, double:1.265E-321)
            long r4 = r4 / r6
            r6 = 255(0xff, double:1.26E-321)
            long r4 = r4 & r6
            int r4 = (int) r4
            byte r4 = (byte) r4
            int r2 = r2 << 8
            byte r5 = r11[r1]
            r4 = r4 ^ r5
            r4 = r4 & 255(0xff, float:3.57E-43)
            r4 = r0[r4]
            r2 = r2 ^ r4
            int r1 = r1 + 1
            goto L_0x0012
        L_0x0070:
            long r4 = (long) r2
            r6 = 4294967295(0xffffffff, double:2.1219957905E-314)
            long r4 = r4 + r6
            r6 = 1
            long r4 = r4 + r6
            goto L_0x005b
        L_0x007b:
            r0 = 4294967295(0xffffffff, double:2.1219957905E-314)
            long r2 = (long) r2
            long r0 = r0 + r2
            r2 = 1
            long r0 = r0 + r2
            goto L_0x0019
        L_0x0086:
            r3 = r4
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imocha.af.a(byte[], int):long");
    }

    static af a() {
        return b;
    }

    static String a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    try {
                        inputStream.close();
                        return sb.toString();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                        return null;
                    }
                } else {
                    sb.append(readLine);
                }
            } catch (IOException e3) {
                e3.printStackTrace();
                try {
                    inputStream.close();
                    return null;
                } catch (IOException e4) {
                    e4.printStackTrace();
                    return null;
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                    throw th;
                } catch (IOException e5) {
                    e5.printStackTrace();
                    return null;
                }
            }
        }
    }

    static String a(String str, String str2) {
        int indexOf = str.indexOf(91);
        if (indexOf < 0) {
            return str;
        }
        int indexOf2 = str.indexOf(93) + 1;
        return indexOf2 <= 0 ? str : str.replace(str.substring(indexOf, indexOf2), str2);
    }

    static String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b2 : bArr) {
            byte b3 = (b2 >>> 4) & 15;
            int i = 0;
            while (true) {
                if (b3 < 0 || b3 > 9) {
                    stringBuffer.append((char) ((b3 - 10) + 97));
                } else {
                    stringBuffer.append((char) (b3 + 48));
                }
                b3 = b2 & 15;
                int i2 = i + 1;
                if (i > 0) {
                    break;
                }
                i = i2;
            }
        }
        return stringBuffer.toString();
    }

    private static void a(Exception exc) {
        if (exc != null) {
            Log.e("", exc.getMessage());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x005f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static boolean a(android.content.Context r5, java.lang.String r6, java.lang.String r7, byte[] r8) {
        /*
            r4 = 0
            r3 = 0
            if (r5 == 0) goto L_0x0009
            if (r8 == 0) goto L_0x0009
            int r0 = r8.length
            if (r0 != 0) goto L_0x000b
        L_0x0009:
            r0 = r3
        L_0x000a:
            return r0
        L_0x000b:
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x004f, all -> 0x005b }
            r0.<init>(r6)     // Catch:{ Exception -> 0x004f, all -> 0x005b }
            boolean r1 = r0.isDirectory()     // Catch:{ Exception -> 0x004f, all -> 0x005b }
            if (r1 != 0) goto L_0x0019
            r0.mkdirs()     // Catch:{ Exception -> 0x004f, all -> 0x005b }
        L_0x0019:
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x004f, all -> 0x005b }
            java.lang.String r1 = "temp.bin"
            r0.<init>(r6, r1)     // Catch:{ Exception -> 0x004f, all -> 0x005b }
            boolean r1 = r0.exists()     // Catch:{ Exception -> 0x004f, all -> 0x005b }
            if (r1 == 0) goto L_0x0029
            r0.delete()     // Catch:{ Exception -> 0x004f, all -> 0x005b }
        L_0x0029:
            r0.createNewFile()     // Catch:{ Exception -> 0x004f, all -> 0x005b }
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x004f, all -> 0x005b }
            r1.<init>(r0)     // Catch:{ Exception -> 0x004f, all -> 0x005b }
            r1.write(r8)     // Catch:{ Exception -> 0x0065 }
            r1.close()     // Catch:{ Exception -> 0x0065 }
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x004f, all -> 0x005b }
            r1.<init>(r6, r7)     // Catch:{ Exception -> 0x004f, all -> 0x005b }
            boolean r2 = r1.exists()     // Catch:{ Exception -> 0x004f, all -> 0x005b }
            if (r2 == 0) goto L_0x0045
            r1.delete()     // Catch:{ Exception -> 0x004f, all -> 0x005b }
        L_0x0045:
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x004f, all -> 0x005b }
            r1.<init>(r6, r7)     // Catch:{ Exception -> 0x004f, all -> 0x005b }
            r0.renameTo(r1)     // Catch:{ Exception -> 0x004f, all -> 0x005b }
            r0 = 1
            goto L_0x000a
        L_0x004f:
            r0 = move-exception
            r1 = r4
        L_0x0051:
            r0.printStackTrace()     // Catch:{ all -> 0x0063 }
            if (r1 == 0) goto L_0x0059
            r1.close()
        L_0x0059:
            r0 = r3
            goto L_0x000a
        L_0x005b:
            r0 = move-exception
            r1 = r4
        L_0x005d:
            if (r1 == 0) goto L_0x0062
            r1.close()
        L_0x0062:
            throw r0
        L_0x0063:
            r0 = move-exception
            goto L_0x005d
        L_0x0065:
            r0 = move-exception
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imocha.af.a(android.content.Context, java.lang.String, java.lang.String, byte[]):boolean");
    }

    static InputStream b(Context context, String str) {
        if (context == null || str == null) {
            return null;
        }
        return context.getAssets().open(str);
    }

    static String b() {
        try {
            return Build.VERSION.RELEASE;
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    private static String b(byte[] bArr) {
        try {
            return a(MessageDigest.getInstance("MD5").digest(bArr));
        } catch (NoSuchAlgorithmException e2) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0033, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x003d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x003e, code lost:
        r3 = r2;
        r2 = r1;
        r1 = r0;
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x001f A[SYNTHETIC, Splitter:B:14:0x001f] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x002b A[SYNTHETIC, Splitter:B:22:0x002b] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0033 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x000b] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0043  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static byte[] b(java.lang.String r4, java.lang.String r5) {
        /*
            r2 = 0
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            r0.<init>(r4, r5)     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0018, all -> 0x0027 }
            int r0 = r1.available()     // Catch:{ Exception -> 0x0038, all -> 0x0033 }
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0038, all -> 0x0033 }
            r1.read(r0)     // Catch:{ Exception -> 0x003d, all -> 0x0033 }
            r1.close()     // Catch:{ Exception -> 0x0031 }
        L_0x0017:
            return r0
        L_0x0018:
            r0 = move-exception
            r1 = r2
        L_0x001a:
            a(r0)     // Catch:{ all -> 0x0035 }
            if (r2 == 0) goto L_0x0043
            r2.close()     // Catch:{ Exception -> 0x0024 }
            r0 = r1
            goto L_0x0017
        L_0x0024:
            r0 = move-exception
            r0 = r1
            goto L_0x0017
        L_0x0027:
            r0 = move-exception
            r1 = r2
        L_0x0029:
            if (r1 == 0) goto L_0x002e
            r1.close()     // Catch:{ Exception -> 0x002f }
        L_0x002e:
            throw r0
        L_0x002f:
            r1 = move-exception
            goto L_0x002e
        L_0x0031:
            r1 = move-exception
            goto L_0x0017
        L_0x0033:
            r0 = move-exception
            goto L_0x0029
        L_0x0035:
            r0 = move-exception
            r1 = r2
            goto L_0x0029
        L_0x0038:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r3
            goto L_0x001a
        L_0x003d:
            r2 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
            r0 = r3
            goto L_0x001a
        L_0x0043:
            r0 = r1
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imocha.af.b(java.lang.String, java.lang.String):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0028 A[SYNTHETIC, Splitter:B:23:0x0028] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static android.graphics.drawable.Drawable c(android.content.Context r4, java.lang.String r5) {
        /*
            r2 = 0
            java.io.InputStream r0 = b(r4, r5)     // Catch:{ Exception -> 0x0012, all -> 0x0024 }
            java.lang.String r1 = "src"
            android.graphics.drawable.Drawable r1 = android.graphics.drawable.Drawable.createFromStream(r0, r1)     // Catch:{ Exception -> 0x003e, all -> 0x0037 }
            if (r0 == 0) goto L_0x0035
            r0.close()     // Catch:{ Exception -> 0x0031 }
            r0 = r1
        L_0x0011:
            return r0
        L_0x0012:
            r0 = move-exception
            r1 = r2
        L_0x0014:
            a(r0)     // Catch:{ all -> 0x003c }
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ Exception -> 0x001e }
            r0 = r2
            goto L_0x0011
        L_0x001e:
            r0 = move-exception
            a(r0)
            r0 = r2
            goto L_0x0011
        L_0x0024:
            r0 = move-exception
            r1 = r2
        L_0x0026:
            if (r1 == 0) goto L_0x002b
            r1.close()     // Catch:{ Exception -> 0x002c }
        L_0x002b:
            throw r0
        L_0x002c:
            r1 = move-exception
            a(r1)
            goto L_0x002b
        L_0x0031:
            r0 = move-exception
            a(r0)
        L_0x0035:
            r0 = r1
            goto L_0x0011
        L_0x0037:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x0026
        L_0x003c:
            r0 = move-exception
            goto L_0x0026
        L_0x003e:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x0014
        L_0x0043:
            r0 = r2
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imocha.af.c(android.content.Context, java.lang.String):android.graphics.drawable.Drawable");
    }

    static String c() {
        return Build.MODEL;
    }

    static boolean c(Context context) {
        if (context != null) {
            try {
                if (((WifiManager) context.getSystemService("wifi")).getWifiState() == 3) {
                    return true;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                return false;
            }
        }
        return false;
    }

    static boolean c(String str, String str2) {
        return new File(String.valueOf(str) + "/" + str2).exists();
    }

    static String d() {
        return String.format("%d", Integer.valueOf((int) (Math.random() * 1.0E8d)));
    }

    private static String h(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    /* access modifiers changed from: package-private */
    public final int a(Context context) {
        if (this.e.a != -1) {
            return this.e.a;
        }
        if (e(context) > 640) {
            this.e.a = 1;
            return 1;
        }
        this.e.a = 0;
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        try {
            b(str);
            new File(str.toString()).delete();
        } catch (Exception e2) {
            System.out.println("删除文件夹操作出错");
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(Context context, String str) {
        if (context == null) {
            return false;
        }
        File file = new File(str);
        if (file.isDirectory()) {
            a(str);
        }
        if (file.isFile()) {
            return file.delete();
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(Context context, String str, byte[] bArr) {
        FileOutputStream fileOutputStream;
        Throwable th;
        Exception exc;
        if (context == null || bArr == null || bArr.length <= 0) {
            return true;
        }
        if (c(context.getFilesDir().getPath(), str)) {
            a(context, String.valueOf(context.getFilesDir().getPath()) + "/" + str);
        }
        try {
            FileOutputStream openFileOutput = context.openFileOutput(str, 3);
            try {
                openFileOutput.write(bArr);
                try {
                    openFileOutput.close();
                    return true;
                } catch (Exception e2) {
                    a(e2);
                    return false;
                }
            } catch (Exception e3) {
                Exception exc2 = e3;
                fileOutputStream = openFileOutput;
                exc = exc2;
                try {
                    a(exc);
                    try {
                        fileOutputStream.close();
                        return false;
                    } catch (Exception e4) {
                        a(e4);
                        return false;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    try {
                        fileOutputStream.close();
                        throw th;
                    } catch (Exception e5) {
                        a(e5);
                        return false;
                    }
                }
            } catch (Throwable th3) {
                Throwable th4 = th3;
                fileOutputStream = openFileOutput;
                th = th4;
                fileOutputStream.close();
                throw th;
            }
        } catch (Exception e6) {
            Exception exc3 = e6;
            fileOutputStream = null;
            exc = exc3;
        } catch (Throwable th5) {
            Throwable th6 = th5;
            fileOutputStream = null;
            th = th6;
            fileOutputStream.close();
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public final Location b(Context context) {
        if (a == null) {
            a = new aj(context, this.c);
        }
        this.c.post(new z(this));
        return a.c();
    }

    /* access modifiers changed from: package-private */
    public final void b(String str) {
        File file = new File(str);
        if (file.exists() && file.isDirectory()) {
            String[] list = file.list();
            for (int i = 0; i < list.length; i++) {
                File file2 = str.endsWith(File.separator) ? new File(String.valueOf(str) + list[i]) : new File(String.valueOf(str) + File.separator + list[i]);
                if (file2.isFile()) {
                    file2.delete();
                }
                if (file2.isDirectory()) {
                    a(String.valueOf(str) + "/" + list[i]);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final String d(Context context) {
        if (this.e.b == null) {
            String h = h(context);
            String str = "";
            for (int i = 0; i < 16 - (h == null ? 0 : h.length()); i++) {
                str = String.valueOf(str) + "0";
            }
            String b2 = b((h == null ? str : String.valueOf(str) + h).getBytes());
            String substring = b2.substring(b2.length() - 14, b2.length());
            this.e.b = String.valueOf(substring) + Long.toHexString(a(substring.getBytes(), 14) & 255);
        }
        return this.e.b;
    }

    /* access modifiers changed from: package-private */
    public final String d(String str, String str2) {
        if (str == null || str2 == null) {
            return null;
        }
        File file = new File(str);
        if (file.list() == null) {
            return "";
        }
        for (String file2 : file.list()) {
            File file3 = new File(str, file2);
            if (file3.isDirectory()) {
                String name = file3.getName();
                System.out.println(name);
                String d2 = d(file3.getPath(), str2);
                if (d2 != null) {
                    return String.valueOf(name) + "/" + d2;
                }
            } else if (file3.isFile() && file3.getName().equals(str2)) {
                return str2;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public final int e(Context context) {
        if (this.e.c == -1) {
            this.e.c = context.getResources().getDisplayMetrics().widthPixels;
            this.e.d = context.getResources().getDisplayMetrics().heightPixels;
            if (this.e.c > this.e.d) {
                int i = this.e.c;
                this.e.c = this.e.d;
                this.e.d = i;
            }
        }
        return this.e.c;
    }

    /* access modifiers changed from: package-private */
    public final boolean e() {
        return this.f && Environment.getExternalStorageState().equals("mounted");
    }

    /* access modifiers changed from: package-private */
    public final int f(Context context) {
        if (this.e.c == -1) {
            this.e.c = context.getResources().getDisplayMetrics().widthPixels;
            this.e.d = context.getResources().getDisplayMetrics().heightPixels;
            if (this.e.c > this.e.d) {
                int i = this.e.c;
                this.e.c = this.e.d;
                this.e.d = i;
            }
        }
        return this.e.d;
    }

    /* access modifiers changed from: package-private */
    public final String g(Context context) {
        return e() ? String.valueOf(Environment.getExternalStorageDirectory().toString()) + "/com/imocha" : context.getFilesDir().getPath();
    }
}
