package com.imocha;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.widget.ImageView;

public final class t extends ImageView {
    Bitmap[] a;
    private Bitmap b = null;

    public t(Context context) {
        super(context);
    }

    public final void a(Bitmap bitmap) {
        this.b = bitmap;
        setImageBitmap(this.b);
        invalidate();
    }

    public final void draw(Canvas canvas) {
        super.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        if (isClickable()) {
            if (motionEvent.getAction() == 0) {
                a(this.a[0]);
            } else if (motionEvent.getAction() == 1) {
                a(this.a[1]);
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    public final void setEnabled(boolean z) {
        super.setEnabled(z);
        setClickable(z);
        if (this.a.length > 2) {
            if (z) {
                a(this.a[1]);
            } else {
                a(this.a[2]);
            }
        } else if (this.a.length == 1) {
            a(this.a[0]);
        }
    }
}
