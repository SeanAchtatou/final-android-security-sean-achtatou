package com.imocha;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class IMochaAdView extends RelativeLayout implements AdView {
    protected static IMochaAdListener a;
    protected static ScheduledExecutorService b = Executors.newScheduledThreadPool(5);
    static HashMap f = new HashMap();
    private static Handler y = new Handler();
    protected final Handler c = new Handler();
    protected long d = 30;
    protected String e = null;
    protected ai g;
    protected boolean h = false;
    protected int i = 1;
    protected IMochaAdType j;
    protected String k = null;
    /* access modifiers changed from: private */
    public a l;
    /* access modifiers changed from: private */
    public IMochaAdListener m;
    private ScheduledExecutorService n;
    private boolean o = true;
    private boolean p = false;
    /* access modifiers changed from: private */
    public boolean q = false;
    /* access modifiers changed from: private */
    public boolean r = false;
    /* access modifiers changed from: private */
    public RelativeLayout s;
    private boolean t = true;
    /* access modifiers changed from: private */
    public boolean u = true;
    private int v;
    private ScheduledFuture w;
    private long x = 0;
    private boolean z = false;

    public IMochaAdView(Activity activity, IMochaAdType iMochaAdType, String str) {
        super(activity);
        this.e = str;
        this.j = iMochaAdType;
        new ak().a(getContext());
        RelativeLayout relativeLayout = new RelativeLayout(getContext());
        this.s = new RelativeLayout(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(13);
        addView(relativeLayout, layoutParams);
        int e2 = af.a().e(getContext());
        double d2 = 50.0d * (((double) e2) / 320.0d) * (e2 >= 640 ? 0.75d : 1.0d);
        this.v = (int) d2;
        relativeLayout.addView(this.s, e2, (int) d2);
        setVisibility(8);
        if (this.j == IMochaAdType.BANNER) {
            this.l = new a(getContext(), this, (am) f.get(this.e), 0);
            this.l.b = new l(this);
        }
    }

    static /* synthetic */ void g(IMochaAdView iMochaAdView) {
        int i2;
        String str;
        int i3;
        ImageView imageView = new ImageView(iMochaAdView.getContext());
        int a2 = af.a().a(iMochaAdView.getContext());
        double e2 = (double) af.a().e(iMochaAdView.getContext());
        if (a2 != 0) {
            i2 = 88;
            new RelativeLayout.LayoutParams(-2, -2);
            str = "banner_320x480.png";
            i3 = 15;
        } else if (e2 == 240.0d) {
            i2 = -2;
            str = "banner_320x480.png";
            i3 = -2;
        } else if (e2 == 320.0d) {
            i2 = -2;
            str = "banner_320x480.png";
            i3 = -2;
        } else {
            i2 = -2;
            str = "banner_480x800.png";
            i3 = -2;
        }
        try {
            imageView.setImageBitmap(BitmapFactory.decodeStream(af.b(iMochaAdView.getContext(), str)));
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(i2, i3);
        layoutParams.addRule(11, -1);
        layoutParams.addRule(12, -1);
        layoutParams.setMargins(5, 0, 0, 0);
        iMochaAdView.s.addView(imageView, layoutParams);
    }

    protected static void onEvent(AdView adView, IMochaEventCode iMochaEventCode) {
        y.post(new ax(adView, iMochaEventCode));
    }

    protected static void sendClickAdLog(AdView adView, am amVar) {
        if (amVar != null) {
            onEvent(adView, IMochaEventCode.AdWillInteract);
            Iterator it = amVar.d.b.iterator();
            while (it.hasNext()) {
                b.schedule(new i(af.a((String) it.next(), af.d())), 0, TimeUnit.SECONDS);
            }
        }
    }

    protected static void sendImpressAdLog(AdView adView, am amVar) {
        if (amVar != null) {
            onEvent(adView, IMochaEventCode.adWillDisplay);
            Iterator it = amVar.d.a.iterator();
            while (it.hasNext()) {
                b.schedule(new i(af.a((String) it.next(), af.d())), 0, TimeUnit.SECONDS);
            }
        }
    }

    public void downloadAd() {
        this.p = true;
        this.q = true;
        if (this.e != null) {
            this.z = false;
            if (this.n == null) {
                this.n = Executors.newScheduledThreadPool(1);
            }
            this.w = this.n.schedule(new av(this), 0, TimeUnit.SECONDS);
        }
    }

    /* access modifiers changed from: protected */
    public String getAdSpace(Context context) {
        String packageName = context.getPackageName();
        String name = context.getClass().getName();
        PackageManager packageManager = context.getPackageManager();
        try {
            Bundle bundle = packageManager.getActivityInfo(new ComponentName(packageName, name), 128).metaData;
            if (bundle != null) {
                return bundle.getString("SPACE_ID");
            }
            try {
                Bundle bundle2 = packageManager.getApplicationInfo(packageName, 128).metaData;
                if (bundle2 == null) {
                    return null;
                }
                return new StringBuilder().append(bundle2.getInt("SPACE_ID")).toString();
            } catch (PackageManager.NameNotFoundException e2) {
                return null;
            }
        } catch (PackageManager.NameNotFoundException e3) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        Log.d("iMocha", "adView onWindowVisibilityChanged:" + i2);
        boolean z2 = i2 == 0;
        this.r = z2;
        if (z2 && !this.q && this.p && this.j == IMochaAdType.BANNER) {
            this.q = true;
            refurbishAd(new av(this), 0);
        }
    }

    public void playFullscreenAd() {
        am amVar = (am) f.get(this.e);
        if (amVar == null) {
            onEvent(this, IMochaEventCode.downloadError2);
        } else if (this.j == IMochaAdType.BANNER || ((String) amVar.g.get("type")).equals("banner")) {
            onEvent(this, IMochaEventCode.adTypeError);
        } else if (this.z) {
            y.post(new ax(this, IMochaEventCode.downloadError2));
        } else {
            this.z = true;
            Intent intent = new Intent();
            intent.putExtra("SPACE_ID", this.e);
            intent.setClass(getContext(), FullScreenAdActivity.class);
            ((Activity) getContext()).startActivityForResult(intent, AdView.REQUEST_CODE);
        }
    }

    /* access modifiers changed from: protected */
    public void refurbishAd(Runnable runnable, long j2) {
        if (this.t) {
            if (!this.r) {
                this.q = false;
                return;
            }
            if (this.n == null) {
                this.n = Executors.newScheduledThreadPool(1);
            }
            this.w = this.n.schedule(runnable, j2, TimeUnit.SECONDS);
        }
    }

    public void setAppId(String str) {
        this.k = str;
    }

    public void setCacheSize(int i2) {
        ak.a((double) i2);
    }

    public void setIMochaAdListener(IMochaAdListener iMochaAdListener) {
        if (this.j == IMochaAdType.BANNER) {
            this.m = iMochaAdListener;
        } else if (this.j == IMochaAdType.FULLSCREEN) {
            a = iMochaAdListener;
        }
    }

    public void setRefresh(boolean z2) {
        this.t = z2;
    }

    public void setServiceId(int i2) {
        this.i = i2;
        if (i2 != 0) {
            this.h = true;
        }
    }

    public void setTestMode(boolean z2) {
        this.h = z2;
    }

    public void setUpdateTime(long j2) {
        if (j2 >= 30 && j2 <= 120) {
            this.d = j2;
        }
    }
}
