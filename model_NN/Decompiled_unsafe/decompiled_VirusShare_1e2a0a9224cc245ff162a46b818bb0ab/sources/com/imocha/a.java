package com.imocha;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ScrollView;
import java.lang.ref.WeakReference;

class a extends WebView {
    public int a = 0;
    az b;
    long c = 0;
    /* access modifiers changed from: private */
    public WeakReference d;
    private IMochaAdView e;

    public a(Context context, IMochaAdView iMochaAdView, am amVar, int i) {
        super(context);
        this.e = iMochaAdView;
        this.d = new WeakReference(amVar);
        this.a = i;
        a();
    }

    private void a() {
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        setVisibility(8);
        setWebViewClient(new au(this));
        setWebChromeClient(new o(this));
        WebView.enablePlatformNotifications();
        setHorizontalScrollbarOverlay(false);
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        getSettings().setJavaScriptEnabled(true);
        getSettings().setSupportZoom(false);
        setScrollContainer(false);
        getSettings().setBuiltInZoomControls(false);
        int childCount = getChildCount();
        if (this.a == 0 || this.a == 1) {
            for (int i = 0; i < childCount; i++) {
                View childAt = getChildAt(i);
                if (childAt.getClass().getName().equals(ScrollView.class.getName())) {
                    ((ScrollView) childAt).setScrollContainer(false);
                }
            }
        }
        setDownloadListener(new ay(this));
    }

    /* access modifiers changed from: private */
    public void b(am amVar) {
        if (this.b == null) {
            return;
        }
        if (this.a == 0 || this.a == 1) {
            IMochaAdView.sendClickAdLog(this.e, amVar);
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(am amVar) {
        this.d = new WeakReference(amVar);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        am amVar = (am) this.d.get();
        if (amVar == null) {
            return super.onTouchEvent(motionEvent);
        }
        b bVar = amVar.b;
        if (bVar instanceof ae) {
            return super.onTouchEvent(motionEvent);
        }
        if (motionEvent.getAction() != 0) {
            return false;
        }
        if (bVar == null) {
            return false;
        }
        boolean a2 = amVar.b.a(getContext(), amVar);
        if (a2) {
            b(amVar);
        }
        return a2;
    }
}
