package com.baidu;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.widget.TextView;

class aw extends TextView {
    private float a;
    private float b;
    private float c;
    private boolean d;
    private Paint e;
    private String f = "";
    private float g = 50.0f;
    private final float h = 1.0f;

    public aw(Context context) {
        super(context);
    }

    private void b() {
        this.b = this.g;
        this.d = true;
    }

    private void c() {
        this.b = 0.0f;
        this.d = false;
    }

    public void a() {
        this.a = getPaint().measureText(this.f);
        this.g = (float) (getWidth() / 4);
        if (this.g == 0.0f) {
            this.g = 50.0f;
        }
        this.c = getTextSize() + ((float) getPaddingTop());
        c();
        if (getWidth() > 0 && this.a > ((float) getWidth())) {
            b();
        }
    }

    public void a(String str) {
        this.f = str;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        try {
            canvas.drawText(this.f, this.b, this.c, this.e);
            if (this.d) {
                this.b -= 1.0f;
                if (this.b < ((float) (-((int) (this.a - ((float) ((getWidth() * 3) / 4))))))) {
                    this.b = this.g;
                }
                postInvalidateDelayed(35);
            }
        } catch (Exception e2) {
            bk.a("AutoScrollTextView.onDraw", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        a();
    }

    public void setTextColor(int i) {
        super.setTextColor(i);
        this.e = getPaint();
        this.e.setColor(i);
    }

    public void setTextSize(float f2) {
        super.setTextSize(f2);
        this.e = getPaint();
        this.e.setTextSize(f2);
    }

    public void setTypeface(Typeface typeface) {
        super.setTypeface(typeface);
        this.e = getPaint();
        this.e.setTypeface(typeface);
    }
}
