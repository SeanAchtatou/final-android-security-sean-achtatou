package com.baidu;

import android.content.Context;
import android.graphics.Bitmap;
import java.net.URL;

final class a extends Thread {
    final /* synthetic */ Context a;
    final /* synthetic */ String b;
    final /* synthetic */ Ad c;

    a(Context context, String str, Ad ad) {
        this.a = context;
        this.b = str;
        this.c = ad;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.w.b(boolean, android.content.Context, java.net.URL, java.lang.String):boolean
     arg types: [int, android.content.Context, java.net.URL, java.lang.String]
     candidates:
      com.baidu.w.b(android.content.Context, java.lang.String, int, int):java.net.HttpURLConnection
      com.baidu.w.b(boolean, android.content.Context, java.net.URL, java.lang.String):boolean */
    public void run() {
        try {
            w.b(false, this.a, new URL(this.b), this.c.i);
        } catch (Exception e) {
            bk.a("createAd", e);
            Bitmap unused = this.c.e = Bitmap.createBitmap(1, 1, Bitmap.Config.ALPHA_8);
        }
    }
}
