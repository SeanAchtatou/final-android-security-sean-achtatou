package com.baidu;

import MobWin.cnst.FUNCTION_CLICK;
import android.content.Context;
import android.graphics.Bitmap;
import java.io.File;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class c {
    public static Map<k, String> a = new HashMap();
    private static c b = new c();
    private static final SimpleDateFormat d = new SimpleDateFormat("yyyyMMdd");
    /* access modifiers changed from: private */
    public static WeakReference<Context> i;
    private static boolean l = false;
    private Map<k, Bitmap> c = new HashMap();
    private String e;
    private boolean f;
    private boolean g;
    private String h;
    /* access modifiers changed from: private */
    public JSONObject j;
    /* access modifiers changed from: private */
    public JSONObject k;
    private boolean m;

    static {
        for (k kVar : k.a()) {
            a.put(kVar, "__sdk_" + kVar.toString().toLowerCase());
        }
    }

    private c() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.w.a(java.net.URL, boolean):android.graphics.Bitmap
     arg types: [java.net.URL, int]
     candidates:
      com.baidu.w.a(android.content.Context, java.net.URL):android.graphics.Bitmap
      com.baidu.w.a(android.content.Context, java.lang.String):java.lang.String
      com.baidu.w.a(java.lang.String, boolean):boolean
      com.baidu.w.a(java.net.URL, boolean):android.graphics.Bitmap */
    private Bitmap a(k kVar, boolean z) {
        if (this.c.get(kVar) == null) {
            if (a(a.get(kVar)) != null) {
                this.c.put(kVar, w.a(a(a.get(kVar)), false));
            } else if (w.c(false, r(), a.get(kVar))) {
                this.c.put(kVar, w.d(false, r(), a.get(kVar)));
            } else {
                this.c.put(kVar, Bitmap.createBitmap(1, 1, z ? Bitmap.Config.ALPHA_8 : Bitmap.Config.RGB_565));
            }
        }
        if (this.c.get(kVar) != null) {
            return this.c.get(kVar);
        }
        w.b(false, r(), a.get(kVar));
        throw new IllegalArgumentException("no resource");
    }

    public static c a() {
        return b;
    }

    public static URL a(String str) {
        return c.class.getResource("/res/" + str);
    }

    public static void a(Context context) {
        i = new WeakReference<>(context);
        if (!l) {
            l = true;
            b.s();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.w.a(boolean, android.content.Context, java.net.URL, java.lang.String):boolean
     arg types: [int, android.content.Context, java.net.URL, java.lang.String]
     candidates:
      com.baidu.w.a(android.content.Context, java.lang.String, int, int):java.lang.String
      com.baidu.w.a(android.content.Context, java.lang.String, java.lang.String, boolean):void
      com.baidu.w.a(boolean, android.content.Context, java.net.URL, java.lang.String):boolean */
    private void a(JSONObject jSONObject) {
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String e2 = w.e(jSONObject.getJSONObject(keys.next().toString()).getJSONObject("content").getString("w_picurl"));
                if (!w.c(false, r(), e2)) {
                    bk.b("AdCache.transferRes", e2);
                    w.a(false, r(), a(e2), e2);
                }
            }
        } catch (JSONException e3) {
            bk.a("AdCache.transferRes", "", e3);
        }
    }

    private Context r() {
        return i.get();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.w.a(boolean, android.content.Context, java.net.URL, java.lang.String):boolean
     arg types: [int, android.content.Context, java.net.URL, java.lang.String]
     candidates:
      com.baidu.w.a(android.content.Context, java.lang.String, int, int):java.lang.String
      com.baidu.w.a(android.content.Context, java.lang.String, java.lang.String, boolean):void
      com.baidu.w.a(boolean, android.content.Context, java.net.URL, java.lang.String):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.w.a(boolean, android.content.Context, java.lang.String):java.lang.String
     arg types: [int, android.content.Context, java.lang.String]
     candidates:
      com.baidu.w.a(java.lang.String, java.lang.String, boolean):void
      com.baidu.w.a(boolean, android.content.Context, java.lang.String):java.lang.String */
    private void s() {
        String str;
        boolean z;
        bk.b("AdCache.init");
        try {
            if (!w.c(false, r(), "__local_cache.json")) {
                bk.b("AdCache.init", "copying new cahce.json");
                if (a("__local_cache.json") != null) {
                    w.a(false, r(), a("__local_cache.json"), "__local_cache.json");
                    z = true;
                    str = null;
                } else {
                    w.a(false, r(), "__local_cache.json", "{\"install\": 11010,\"hangup\": false,\"stopped\": false,\"index_server\": \"http://mobads.baidu.com:80\",\"text\": {},\"image\": {},\"app\": {},\"click\": {}}", false);
                    z = true;
                    str = null;
                }
            } else {
                String a2 = w.a(false, r(), "__local_cache.json");
                try {
                    this.k = new JSONObject(a2);
                    long j2 = (a("__local_cache.json") != null ? new JSONObject(w.a(a("__local_cache.json"))) : new JSONObject("{\"install\": 11010,\"hangup\": false,\"stopped\": false,\"index_server\": \"http://mobads.baidu.com:80\",\"text\": {},\"image\": {},\"app\": {},\"click\": {}}")).getLong("install");
                    long j3 = this.k.getLong("install");
                    bk.b("AdCache.init", "newVersion: " + j2 + " oldVersion: " + j3);
                    if (j2 != j3) {
                        w.b(false, r(), "__local_cache.json");
                        w.a(false, r(), a("__local_cache.json"), "__local_cache.json");
                        this.k = null;
                        z = true;
                        str = null;
                    } else {
                        str = a2;
                        z = false;
                    }
                } catch (JSONException e2) {
                    bk.b("AdCache", "Cache parsing error");
                    w.b(false, r(), "__local_cache.json");
                    s();
                    return;
                }
            }
            if (str == null) {
                str = w.a(false, r(), "__local_cache.json");
                this.k = new JSONObject(str);
            }
            this.j = new JSONObject(str);
            bk.b("AdCache", "raw cache length: " + str.length());
            if (this.j == null || this.k == null) {
                throw new IllegalArgumentException("displayCache == null || cache == null");
            }
            if (z) {
                for (t tVar : t.a()) {
                    a(this.k.getJSONObject(tVar.toString()));
                }
                d();
            }
            if (!t()) {
                bk.b("AdCache", "Validation: [Failed]");
                w.b(false, r(), "__local_cache.json");
                s();
                return;
            }
            this.e = this.k.getString("index_server");
            this.f = this.k.getBoolean("hangup");
            this.g = this.k.getBoolean("stopped");
            this.h = String.format("android_%s", Long.valueOf(this.k.getLong("install")));
        } catch (JSONException e3) {
            bk.b("AdCache.init", "", e3);
        }
    }

    private boolean t() {
        bk.b("AdCache.validate");
        ArrayList<JSONObject> arrayList = new ArrayList<>();
        for (t tVar : t.a()) {
            arrayList.add(this.k.getJSONObject(tVar.toString()));
        }
        for (JSONObject jSONObject : arrayList) {
            Iterator<String> keys = jSONObject.keys();
            while (true) {
                if (keys.hasNext()) {
                    String string = jSONObject.getJSONObject(keys.next().toString()).getJSONObject("content").getString("w_picurl");
                    if (!string.trim().equals("") && !w.c(false, r(), w.e(string))) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void u() {
        bk.b("AdCache.flush");
        try {
            w.a(false, r(), "__local_cache.json", this.k.toString(), false);
        } catch (JSONException e2) {
            throw new RuntimeException();
        }
    }

    private void v() {
        if (!this.m) {
            this.m = true;
            bk.b("AdCache.validateClicklog");
            synchronized (this.k) {
                try {
                    Iterator<String> keys = this.k.getJSONObject(FUNCTION_CLICK.a).keys();
                    while (keys.hasNext()) {
                        String obj = keys.next().toString();
                        JSONArray jSONArray = this.k.getJSONObject(FUNCTION_CLICK.a).getJSONObject(obj).getJSONArray("record");
                        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                            if (!jSONArray.isNull(i2)) {
                                String string = jSONArray.getJSONObject(i2).getString("date");
                                if (d.parse(string).before(new Date(System.currentTimeMillis() - 259200000))) {
                                    bk.b("AdCache.validateClicklog", obj + " " + string);
                                    jSONArray.put(i2, JSONObject.NULL);
                                }
                            }
                        }
                    }
                    u();
                } catch (JSONException e2) {
                    bk.a("AdCache.validateClicklog", "", e2);
                } catch (ParseException e3) {
                    bk.a("AdCache.validateClicklog", "", e3);
                }
            }
        }
    }

    private String w() {
        return d.format(new Date(System.currentTimeMillis()));
    }

    public Vector<Ad> a(t tVar) {
        Vector<Ad> vector = new Vector<>();
        try {
            JSONObject jSONObject = this.j.getJSONObject(tVar.toString());
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String obj = keys.next().toString();
                Ad a2 = Ad.a(r(), jSONObject.getJSONObject(obj).getJSONObject("content"), obj);
                if (a2 != null) {
                    vector.add(a2);
                }
            }
            Collections.sort(vector, new g(this));
        } catch (JSONException e2) {
            bk.a("AdCache.init", "", e2);
        }
        if (vector.size() != 0) {
            return vector;
        }
        bk.b("AdCache.getAds", "No Ads(" + tVar + ")");
        return null;
    }

    public void a(t tVar, int i2) {
        if (r.k(r())) {
            new h(this, tVar).start();
        }
    }

    public void a(t tVar, long j2, JSONObject jSONObject) {
        synchronized (this.k) {
            JSONObject jSONObject2 = this.k.getJSONObject(tVar.toString());
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("content", jSONObject);
            jSONObject2.put(j2 + "", jSONObject3);
            u();
            bk.b("AdCache.updateAd", String.format("[%d] at [%d]", Long.valueOf(j2), Integer.valueOf(jSONObject2.length() - 1)));
        }
    }

    public void a(boolean z) {
        this.f = z;
        new i(this, z).start();
    }

    public boolean a(Ad ad, Map<ay, Integer> map) {
        bk.b("AdCache.setClicklog", ad + " " + map);
        if (ad == null) {
            return false;
        }
        v();
        synchronized (this.k) {
            try {
                JSONObject jSONObject = this.k.getJSONObject(FUNCTION_CLICK.a);
                String m2 = ad.m();
                if (!jSONObject.has(m2)) {
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("clklogurl", ad.h());
                    jSONObject2.put("record", new JSONArray());
                    jSONObject.put(m2, jSONObject2);
                }
                JSONArray jSONArray = jSONObject.getJSONObject(m2).getJSONArray("record");
                int i2 = 0;
                while (i2 < jSONArray.length() && (jSONArray.isNull(i2) || !w().equals(jSONArray.getJSONObject(i2).getString("date")))) {
                    i2++;
                }
                if (i2 == jSONArray.length()) {
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put("date", w());
                    jSONObject3.put("stamp", r.d());
                    jSONObject3.put(FUNCTION_CLICK.a, 0);
                    jSONObject3.put("show", 0);
                    jSONObject3.put("phone", 0);
                    jSONArray.put(jSONObject3);
                }
                JSONObject jSONObject4 = jSONArray.getJSONObject(i2);
                for (ay ayVar : ay.a()) {
                    String lowerCase = ayVar.toString().toLowerCase();
                    jSONObject4.put(lowerCase, map.get(ayVar).intValue() + jSONObject4.getInt(lowerCase));
                }
                u();
                bk.b("AdCache.setClicklog", map.toString());
            } catch (Exception e2) {
                bk.a("AdCache.setClicklog", "", e2);
                return false;
            }
        }
        return true;
    }

    public String b() {
        return this.h;
    }

    public void b(t tVar) {
        synchronized (this.k) {
            Iterator<String> keys = this.k.getJSONObject(tVar.toString()).keys();
            while (keys.hasNext()) {
                bk.b("AdCache.clearAds", keys.next().toString());
                keys.remove();
            }
            u();
        }
    }

    public void b(boolean z) {
        this.g = z;
        new j(this, z).start();
    }

    public void c() {
        try {
            this.j = new JSONObject(this.k.toString());
        } catch (JSONException e2) {
            bk.a("AdCache.refresh", "", e2);
        }
    }

    public void d() {
        bk.b("AdCache.validateResVolumn");
        File filesDir = r().getFilesDir();
        long j2 = 0;
        for (File file : filesDir.listFiles(new d(this, a.values()))) {
            bk.b(file.toString() + " " + file.length());
            j2 += file.length();
        }
        bk.b("AdCache.validateResVolumn", j2 + "/" + 1048576);
        if (j2 > 1048576) {
            File[] listFiles = filesDir.listFiles(new e(this));
            ArrayList arrayList = new ArrayList();
            for (File add : listFiles) {
                arrayList.add(add);
            }
            Collections.sort(arrayList, new f(this));
            long j3 = j2;
            for (int i2 = 0; i2 < arrayList.size() && j3 > 1048576; i2++) {
                File file2 = (File) arrayList.get(i2);
                bk.b(file2.lastModified() + " " + file2.getName() + " " + file2.length());
                j3 -= file2.length();
                w.b(false, r(), file2.getName());
                bk.b("AdCache.validateResVolumn", j3 + "/" + 1048576);
            }
        }
    }

    public boolean e() {
        bk.b("AdCache.isHangup", this.f + "");
        return this.f;
    }

    public boolean f() {
        bk.b("AdCache.isStopped", this.g + "");
        return this.g;
    }

    public String g() {
        return this.e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap
     arg types: [com.baidu.k, int]
     candidates:
      com.baidu.c.a(com.baidu.t, int):void
      com.baidu.c.a(com.baidu.Ad, java.util.Map<com.baidu.ay, java.lang.Integer>):boolean
      com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap */
    public Bitmap h() {
        return a(k.CLICK, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap
     arg types: [com.baidu.k, int]
     candidates:
      com.baidu.c.a(com.baidu.t, int):void
      com.baidu.c.a(com.baidu.Ad, java.util.Map<com.baidu.ay, java.lang.Integer>):boolean
      com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap */
    public Bitmap i() {
        return a(k.CLICK2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap
     arg types: [com.baidu.k, int]
     candidates:
      com.baidu.c.a(com.baidu.t, int):void
      com.baidu.c.a(com.baidu.Ad, java.util.Map<com.baidu.ay, java.lang.Integer>):boolean
      com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap */
    public Bitmap j() {
        return a(k.PHONE, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap
     arg types: [com.baidu.k, int]
     candidates:
      com.baidu.c.a(com.baidu.t, int):void
      com.baidu.c.a(com.baidu.Ad, java.util.Map<com.baidu.ay, java.lang.Integer>):boolean
      com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap */
    public Bitmap k() {
        return a(k.PHONE2, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap
     arg types: [com.baidu.k, int]
     candidates:
      com.baidu.c.a(com.baidu.t, int):void
      com.baidu.c.a(com.baidu.Ad, java.util.Map<com.baidu.ay, java.lang.Integer>):boolean
      com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap */
    public Bitmap l() {
        return a(k.CORNER, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap
     arg types: [com.baidu.k, int]
     candidates:
      com.baidu.c.a(com.baidu.t, int):void
      com.baidu.c.a(com.baidu.Ad, java.util.Map<com.baidu.ay, java.lang.Integer>):boolean
      com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap */
    public Bitmap m() {
        return a(k.BG, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap
     arg types: [com.baidu.k, int]
     candidates:
      com.baidu.c.a(com.baidu.t, int):void
      com.baidu.c.a(com.baidu.Ad, java.util.Map<com.baidu.ay, java.lang.Integer>):boolean
      com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap */
    public Bitmap n() {
        return a(k.DOWNLOAD, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap
     arg types: [com.baidu.k, int]
     candidates:
      com.baidu.c.a(com.baidu.t, int):void
      com.baidu.c.a(com.baidu.Ad, java.util.Map<com.baidu.ay, java.lang.Integer>):boolean
      com.baidu.c.a(com.baidu.k, boolean):android.graphics.Bitmap */
    public Bitmap o() {
        return a(k.DOWNLOAD2, false);
    }

    public int p() {
        return 5;
    }
}
