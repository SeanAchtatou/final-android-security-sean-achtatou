package com.baidu;

import android.content.Context;
import android.graphics.Typeface;
import android.telephony.TelephonyManager;
import android.view.MotionEvent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.tencent.mobwin.core.m;
import com.tencent.mobwin.utils.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

abstract class l extends RelativeLayout {
    protected static final Typeface a = Typeface.create(Typeface.SANS_SERIF, 1);
    public static List<AnimationSet> i = new ArrayList();
    private static bm l;
    protected Ad b;
    protected int c = 5;
    protected boolean d;
    protected ImageView e;
    protected ImageView f;
    protected int g = b.a;
    protected int h = 48;
    Map<ay, Integer> j;
    private boolean k;
    private long m;
    private AdView n;
    private int o = 0;
    private int p = 1;

    public l(AdView adView) {
        super(adView.getContext());
        Context context = adView.getContext();
        a(adView);
        if (l == null) {
            l = new bm();
            ((TelephonyManager) context.getSystemService("phone")).listen(l, 32);
        }
        this.e = new ImageView(context);
        this.f = new ImageView(context);
        this.e.setScaleType(ImageView.ScaleType.FIT_XY);
        this.f.setScaleType(ImageView.ScaleType.FIT_XY);
        c(context);
        b();
    }

    private void a(AdView adView) {
        this.n = adView;
    }

    private void c(Context context) {
        if (i.size() <= 0) {
            AnimationSet animationSet = new AnimationSet(true);
            ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, 1, 0.5f, 1, 0.5f);
            scaleAnimation.setDuration((long) 200);
            animationSet.addAnimation(scaleAnimation);
            ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.2f, 0.001f, 1.2f, 0.001f, 1, 0.5f, 1, 0.5f);
            scaleAnimation2.setDuration(250);
            scaleAnimation2.setStartOffset((long) 200);
            animationSet.addAnimation(scaleAnimation2);
            i.add(animationSet);
            AnimationSet animationSet2 = new AnimationSet(true);
            RotateAnimation rotateAnimation = new RotateAnimation(0.0f, (float) 10, 1, 0.5f, 1, 0.5f);
            rotateAnimation.setStartOffset((long) 0);
            rotateAnimation.setDuration((long) 80);
            animationSet2.addAnimation(rotateAnimation);
            RotateAnimation rotateAnimation2 = new RotateAnimation((float) 10, 0.0f, 1, 0.5f, 1, 0.5f);
            int i2 = 0 + 80;
            rotateAnimation2.setStartOffset((long) i2);
            rotateAnimation2.setDuration((long) 80);
            animationSet2.addAnimation(rotateAnimation2);
            RotateAnimation rotateAnimation3 = new RotateAnimation(0.0f, (float) (-10), 1, 0.5f, 1, 0.5f);
            int i3 = 80 * 1;
            rotateAnimation3.setStartOffset((long) (i2 + 80));
            rotateAnimation3.setDuration((long) 80);
            animationSet2.addAnimation(rotateAnimation3);
            RotateAnimation rotateAnimation4 = new RotateAnimation((float) (-10), 0.0f, 1, 0.5f, 1, 0.5f);
            rotateAnimation4.setStartOffset((long) (80 + 160));
            rotateAnimation4.setDuration((long) 80);
            animationSet2.addAnimation(rotateAnimation4);
            i.add(animationSet2);
            AnimationSet animationSet3 = new AnimationSet(true);
            ScaleAnimation scaleAnimation3 = new ScaleAnimation(1.0f, 0.95f, 1.0f, 0.95f, 1, 0.5f, 1, 0.5f);
            scaleAnimation3.setDuration((long) j.i);
            animationSet3.addAnimation(scaleAnimation3);
            ScaleAnimation scaleAnimation4 = new ScaleAnimation(0.95f, 1.0f, 0.95f, 1.0f, 1, 0.5f, 1, 0.5f);
            scaleAnimation4.setDuration((long) j.i);
            scaleAnimation4.setStartOffset((long) j.i);
            animationSet3.addAnimation(scaleAnimation4);
            i.add(animationSet3);
            AnimationSet animationSet4 = new AnimationSet(true);
            AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.4f);
            alphaAnimation.setStartOffset((long) 0);
            alphaAnimation.setDuration((long) 300);
            animationSet4.addAnimation(alphaAnimation);
            AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.4f, 1.0f);
            alphaAnimation2.setStartOffset((long) (0 + 300));
            alphaAnimation2.setDuration((long) 300);
            animationSet4.addAnimation(alphaAnimation2);
            i.add(animationSet4);
            AnimationSet animationSet5 = new AnimationSet(true);
            Animation loadAnimation = AnimationUtils.loadAnimation(context, 17432579);
            loadAnimation.setStartOffset((long) 0);
            loadAnimation.setDuration((long) m.a);
            animationSet5.addAnimation(loadAnimation);
            i.add(animationSet5);
            AnimationSet animationSet6 = new AnimationSet(true);
            Animation loadAnimation2 = AnimationUtils.loadAnimation(context, 17432578);
            loadAnimation2.setStartOffset((long) 0);
            loadAnimation2.setDuration((long) m.a);
            animationSet6.addAnimation(loadAnimation2);
            i.add(animationSet6);
        }
    }

    private AdView l() {
        return this.n;
    }

    public long a() {
        return this.m;
    }

    public abstract void a(int i2);

    public void a(Context context) {
        if (h().getClickURL() != null) {
            new m(this, context).start();
        }
    }

    /* access modifiers changed from: protected */
    public void a(MotionEvent motionEvent) {
        boolean z;
        if (isPressed()) {
            setPressed(false);
            if (motionEvent != null) {
                try {
                    z = motionEvent.getX() >= ((float) this.e.getLeft()) && motionEvent.getX() <= ((float) this.e.getRight()) && motionEvent.getY() >= ((float) this.e.getTop()) && motionEvent.getY() <= ((float) this.e.getBottom());
                } catch (Exception e2) {
                    bk.a(e2);
                    return;
                }
            } else {
                z = false;
            }
            if (z) {
                bk.b("pressAnimation", "Hit!");
                this.e.startAnimation(i.get(0));
            }
            if (motionEvent != null && z && this.b.getPhone() != null && !this.b.getPhone().equals("")) {
                b(getContext());
            } else if (r.k(getContext())) {
                a(getContext());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Ad ad, t tVar, int i2, ag agVar) {
        this.o++;
        bk.b("AdContainer.setAd", "showCount: " + this.o);
        if (this.o == this.p) {
            if (this.p <= 8) {
                this.p <<= 1;
            } else {
                this.p += 10;
            }
            c.a().a(tVar, i2);
        }
        k();
    }

    public void b() {
        this.m = System.currentTimeMillis();
        this.j = new HashMap();
        for (ay put : ay.a()) {
            this.j.put(put, 0);
        }
    }

    public abstract void b(int i2);

    public void b(Context context) {
        if (!h().getPhone().equals("")) {
            f();
            new n(this, context).start();
        }
    }

    public Map<ay, Integer> c() {
        return this.j;
    }

    public boolean d() {
        boolean z;
        ay[] a2 = ay.a();
        int length = a2.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                z = true;
                break;
            }
            if (this.j.get(a2[i2]).intValue() > 0) {
                z = false;
                break;
            }
            i2++;
        }
        if (z) {
            return false;
        }
        if (this.j.get(ay.Show).intValue() <= 0) {
            return false;
        }
        return System.currentTimeMillis() - a() >= 10000;
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (!l().a()) {
            try {
                int action = motionEvent.getAction();
                bk.b("BaiduMobAds SDK", String.format("dispatchTouchEvent:: %s (%f, %f)", Integer.valueOf(action), Float.valueOf(motionEvent.getX()), Float.valueOf(motionEvent.getY())));
                if (action == 0) {
                    setPressed(true);
                } else if (action == 2) {
                    float x = motionEvent.getX();
                    float y = motionEvent.getY();
                    int left = getLeft();
                    int top = getTop();
                    int right = getRight();
                    int bottom = getBottom();
                    if (x < ((float) left) || x > ((float) right) || y < ((float) top) || y > ((float) bottom)) {
                        setPressed(false);
                    } else {
                        setPressed(true);
                    }
                } else if (action == 1) {
                    if (isPressed()) {
                        a(motionEvent);
                    }
                    setPressed(false);
                } else if (action == 3) {
                    setPressed(false);
                }
            } catch (Exception e2) {
                bk.a("dispatchTouchEvent", e2);
            }
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public void e() {
        this.j.put(ay.Phone, Integer.valueOf(this.j.get(ay.Phone).intValue() + 1));
    }

    /* access modifiers changed from: package-private */
    public void f() {
        l.a(this);
    }

    public Animation g() {
        bk.b("AdContainer.initingAnimation");
        int abs = Math.abs(new Random().nextInt()) % (i.size() - 1);
        int size = i.size() - 2;
        return i.get(1 + 1);
    }

    public Ad h() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public boolean i() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public void j() {
        this.k = true;
    }

    public void k() {
        c().put(ay.Show, Integer.valueOf(c().get(ay.Show).intValue() + 1));
    }

    public abstract void setBackgroundColor(int i2);

    public String toString() {
        return String.format("[val:%s %s]", Boolean.valueOf(d()), this.j);
    }
}
