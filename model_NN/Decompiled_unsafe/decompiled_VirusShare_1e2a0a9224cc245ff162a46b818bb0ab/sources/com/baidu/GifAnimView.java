package com.baidu;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.view.View;

class GifAnimView extends View implements az {
    /* access modifiers changed from: private */
    public ba a = null;
    /* access modifiers changed from: private */
    public Bitmap b = null;
    /* access modifiers changed from: private */
    public boolean c = true;
    /* access modifiers changed from: private */
    public boolean d = false;
    private int e = -1;
    private int f = -1;
    private Rect g = null;
    private be h = null;
    private GifImageType i = GifImageType.SYNC_DECODER;
    /* access modifiers changed from: private */
    public bf j;
    private int k = 255;
    /* access modifiers changed from: private */
    public Handler l = new bc(this);

    public enum GifImageType {
        WAIT_FINISH(0),
        SYNC_DECODER(1),
        COVER(2);
        
        final int a;

        private GifImageType(int i) {
            this.a = i;
        }
    }

    public GifAnimView(Context context, bf bfVar) {
        super(context);
        this.j = bfVar;
    }

    private void b(byte[] bArr) {
        if (this.a != null) {
            this.a.a();
            this.a = null;
        }
        this.a = new ba(bArr, this);
        this.a.start();
    }

    private void c() {
        if (this.l != null) {
            this.l.sendMessage(this.l.obtainMessage());
        }
    }

    public void a() {
        this.d = true;
    }

    public void a(int i2) {
        this.k = i2;
        invalidate();
    }

    public void a(int i2, int i3) {
        if (i2 > 0 && i3 > 0) {
            this.e = i2;
            this.f = i3;
            bk.a(this.f + "");
            this.g = new Rect();
            this.g.left = 0;
            this.g.top = 0;
            this.g.right = i2;
            this.g.bottom = i3;
        }
    }

    public void a(boolean z, int i2) {
        if (!z) {
            return;
        }
        if (this.a != null) {
            switch (bd.a[this.i.ordinal()]) {
                case 1:
                    if (i2 != -1) {
                        return;
                    }
                    if (this.a.b() > 1) {
                        new be(this, null).start();
                        return;
                    } else {
                        c();
                        return;
                    }
                case 2:
                    if (i2 == 1) {
                        this.b = this.a.c();
                        c();
                        return;
                    } else if (i2 != -1) {
                        return;
                    } else {
                        if (this.a.b() <= 1) {
                            c();
                            return;
                        } else if (this.h == null) {
                            this.h = new be(this, null);
                            this.h.start();
                            return;
                        } else {
                            return;
                        }
                    }
                case 3:
                    if (i2 == 1) {
                        this.b = this.a.c();
                        c();
                        return;
                    } else if (i2 == -1) {
                        c();
                        return;
                    } else if (this.h == null) {
                        this.h = new be(this, null);
                        this.h.start();
                        return;
                    } else {
                        return;
                    }
                default:
                    return;
            }
        } else {
            bk.b("gif", "parse error");
        }
    }

    public void a(byte[] bArr) {
        b(bArr);
    }

    public void b() {
        this.d = false;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.a != null) {
            if (this.b == null) {
                this.b = this.a.c();
            }
            if (this.b != null) {
                int saveCount = canvas.getSaveCount();
                canvas.save();
                canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
                Paint paint = new Paint();
                paint.setAlpha(this.k);
                if (this.e == -1) {
                    canvas.drawBitmap(this.b, 0.0f, 0.0f, paint);
                } else {
                    canvas.drawBitmap(this.b, (Rect) null, this.g, paint);
                }
                canvas.restoreToCount(saveCount);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5 = 1;
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        if (this.a == null) {
            i4 = 1;
        } else {
            i5 = this.a.a;
            i4 = this.a.b;
        }
        setMeasuredDimension(resolveSize(Math.max(paddingLeft + paddingRight + i5, getSuggestedMinimumWidth()), i2), resolveSize(Math.max(paddingTop + paddingBottom + i4, getSuggestedMinimumHeight()), i3));
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        super.onWindowVisibilityChanged(i2);
        bk.b("GifAnimView.onWindowVisibilityChanged", "visibility: " + i2);
        this.d = i2 != 0;
    }
}
