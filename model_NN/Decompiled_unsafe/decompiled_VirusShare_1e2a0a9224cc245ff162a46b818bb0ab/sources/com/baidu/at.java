package com.baidu;

import android.content.pm.PackageInfo;
import android.os.Environment;
import java.util.List;
import java.util.TimerTask;

class at extends TimerTask {
    final /* synthetic */ as a;

    at(as asVar) {
        this.a = asVar;
    }

    public void run() {
        try {
            String externalStorageState = Environment.getExternalStorageState();
            bk.b(String.format("AppMonitor(%s, %s) SDCARD: %s, context: %s", Integer.valueOf(this.a.e.size()), Integer.valueOf(this.a.f.size()), externalStorageState, this.a.g.get()));
            if ("mounted".equals(externalStorageState) && this.a.g.get() != null) {
                if (this.a.e.size() != 0 || this.a.f.size() != 0) {
                    for (bl blVar : this.a.e) {
                        if (this.a.c(blVar)) {
                            bk.b(String.format("[%s] downloaded\n", blVar.f()));
                            String f = this.a.f(blVar);
                            if (f != null) {
                                this.a.e.remove(blVar);
                                blVar.a(f);
                                new au(this, blVar).start();
                                this.a.e(blVar);
                            }
                        } else {
                            bk.b(String.format("[%s] downloading\n", blVar.f()));
                        }
                    }
                    List<PackageInfo> installedPackages = this.a.h.getInstalledPackages(0);
                    for (bl blVar2 : this.a.f) {
                        if (this.a.a(installedPackages, blVar2)) {
                            bk.b(String.format("[%s %s] installed\n", blVar2.f(), blVar2.n()));
                            this.a.f.remove(blVar2);
                            new av(this, blVar2).start();
                        } else {
                            bk.b(String.format("[%s %s] installing\n", blVar2.f(), blVar2.n()));
                        }
                    }
                }
            }
        } catch (Exception e) {
            bk.a(e);
        }
    }
}
