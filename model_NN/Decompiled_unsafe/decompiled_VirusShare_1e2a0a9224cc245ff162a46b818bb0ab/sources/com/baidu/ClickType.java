package com.baidu;

public enum ClickType {
    BROWSE("浏览网页"),
    PHONE("拨打电话"),
    DOWNLOAD("下载资源");
    
    private String a;

    private ClickType(String str) {
        this.a = str;
    }

    public String getValue() {
        return this.a;
    }
}
