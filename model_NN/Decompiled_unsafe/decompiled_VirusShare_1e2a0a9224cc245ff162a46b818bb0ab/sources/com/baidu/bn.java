package com.baidu;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

class bn extends l {
    private LinearLayout k;
    private aw l;
    private aw m;
    private ImageView n;
    private ImageView o;
    private BitmapDrawable p;
    private float q;
    private int r = -11184811;
    private int s = -1;
    private int t = 255;
    private int u;

    public bn(AdView adView) {
        super(adView);
        bk.b("TextAdContainer", "{");
        setFocusable(true);
        setClickable(true);
        this.n = new ImageView(getContext());
        this.o = new ImageView(getContext());
        this.l = new aw(getContext());
        this.m = new aw(getContext());
        this.n.setScaleType(ImageView.ScaleType.FIT_XY);
        this.o.setScaleType(ImageView.ScaleType.FIT_XY);
        bk.b("TextAdContainer", "}");
    }

    private BitmapDrawable a(Rect rect, int i, int i2, int i3) {
        bk.b(String.format("TextAdContainer.refreshBackgroundDrawable tc:%X bc:%X bt:%d", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3)));
        try {
            Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint();
            paint.setColor(Color.argb(i3, Color.red(i), Color.green(i), Color.blue(i)));
            paint.setAntiAlias(true);
            canvas.drawRect(rect, paint);
            int argb = Color.argb(i3 / 2, Color.red(i2), Color.green(i2), Color.blue(i2));
            int argb2 = Color.argb(i3, Color.red(i2), Color.green(i2), Color.blue(i2));
            GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{argb, argb2});
            int height = ((int) (((double) rect.height()) * 0.5d)) + rect.top;
            gradientDrawable.setBounds(rect.left, rect.top, rect.right, height);
            gradientDrawable.draw(canvas);
            Paint paint2 = new Paint();
            paint2.setColor(argb2);
            canvas.drawRect(new Rect(rect.left, height, rect.right, rect.bottom), paint2);
            return new BitmapDrawable(createBitmap);
        } catch (Exception e) {
            bk.a(e);
            return null;
        }
    }

    private void m() {
        this.p = a(new Rect(0, 0, this.g, this.h), this.s, this.r, this.t);
        setBackgroundDrawable(this.p);
    }

    public void a(int i) {
        boolean z = this.s != i;
        this.s = Color.argb(this.t, Color.red(i), Color.green(i), Color.blue(i));
        if (this.l != null) {
            this.l.setTextColor(this.s);
        }
        if (this.m != null) {
            this.m.setTextColor(this.s);
        }
        if (z) {
            m();
        }
    }

    public void a(Ad ad, t tVar, int i, ag agVar) {
        bk.b("TextAdContainer", "setAd " + agVar);
        this.g = agVar.a;
        this.h = agVar.b;
        this.c = (int) (0.1041666641831398d * ((double) this.h));
        l();
        if (ad == null || ad.c() == null) {
            throw new IllegalArgumentException();
        }
        this.b = ad;
        this.d = false;
        if (ad.i() == 1) {
            a(ad.k());
            this.t = ad.l();
            this.r = ad.j();
        }
        this.l.a(ad.getTitle());
        this.l.a();
        this.m.a(ad.getDescription());
        this.m.a();
        if ("".equals(ad.getTitle())) {
            bk.b("empty title " + ad);
            this.l.setVisibility(8);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
            layoutParams.setMargins(0, (int) ((((float) (this.h - (this.c * 3))) - this.q) / 2.0f), 0, 0);
            this.m.setLayoutParams(layoutParams);
        } else {
            this.l.setVisibility(0);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1);
            layoutParams2.setMargins(0, 0, 0, 0);
            this.m.setLayoutParams(layoutParams2);
        }
        this.n.setImageBitmap(ad.d());
        this.o.setImageBitmap(ad.f());
        this.e.setImageBitmap(ad.e());
        this.f.setImageBitmap(ad.g());
        RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) this.k.getLayoutParams();
        layoutParams3.setMargins(ad.n() == bj.NONE ? this.c : this.h, this.c, this.h + this.u, this.c);
        this.k.setLayoutParams(layoutParams3);
        super.a(ad, tVar, i, agVar);
    }

    public void b(int i) {
        boolean z = this.t != i;
        this.t = i;
        this.n.setAlpha(this.t);
        this.o.setAlpha(this.t);
        this.e.setAlpha(this.t);
        this.f.setAlpha((int) (((double) this.t) * 1.0d));
        a(this.s);
        if (z) {
            m();
        }
    }

    /* access modifiers changed from: protected */
    public void l() {
        if (!i()) {
            j();
            int i = this.h - (this.c * 2);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(i, i);
            layoutParams.addRule(9);
            layoutParams.setMargins(this.c, this.c, this.c, this.c);
            this.n.setLayoutParams(layoutParams);
            addView(this.n);
            this.u = (int) ((2.0d * ((double) this.h)) / 48.0d);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(this.h + this.u, this.h);
            layoutParams2.addRule(11);
            layoutParams2.addRule(10);
            this.o.setLayoutParams(layoutParams2);
            addView(this.o);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(this.h, this.h);
            layoutParams3.addRule(10);
            layoutParams3.addRule(11);
            this.e.setLayoutParams(layoutParams3);
            addView(this.e);
            int i2 = (int) (0.4166666666666667d * ((double) i));
            RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(i2, i2);
            layoutParams4.addRule(11);
            layoutParams4.addRule(12);
            this.f.setLayoutParams(layoutParams4);
            addView(this.f);
            this.k = new LinearLayout(getContext());
            RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, -1);
            layoutParams5.setMargins(this.h, this.c, this.h + this.u, this.c);
            this.k.setLayoutParams(layoutParams5);
            this.k.setOrientation(1);
            addView(this.k);
            float f = 0.27083334f * ((float) this.h);
            this.l.setTextColor(-1);
            this.l.setTypeface(a);
            this.l.setTextSize(f);
            this.l.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            this.k.addView(this.l);
            this.q = 0.33333334f * ((float) this.h);
            this.m.setTextColor(-1);
            this.m.setTypeface(a);
            this.m.setTextSize(this.q);
            this.m.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            this.k.addView(this.m);
            m();
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
    }

    public void setBackgroundColor(int i) {
        boolean z = this.r != i;
        this.r = -16777216 | i;
        if (z) {
            m();
        }
    }
}
