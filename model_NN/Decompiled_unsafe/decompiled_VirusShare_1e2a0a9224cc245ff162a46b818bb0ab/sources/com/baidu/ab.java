package com.baidu;

import android.view.ViewGroup;
import android.widget.RelativeLayout;

class ab implements Runnable {
    final /* synthetic */ AdView a;
    final /* synthetic */ AdView b;

    ab(AdView adView, AdView adView2) {
        this.b = adView;
        this.a = adView2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.AdView.a(com.baidu.AdView, boolean):void
     arg types: [com.baidu.AdView, int]
     candidates:
      com.baidu.AdView.a(com.baidu.AdView, com.baidu.l):com.baidu.l
      com.baidu.AdView.a(com.baidu.AdView, com.baidu.t):void
      com.baidu.AdView.a(com.baidu.AdView, java.lang.String):void
      com.baidu.AdView.a(com.baidu.AdView, boolean):void */
    public void run() {
        try {
            this.b.j();
            this.b.m();
            Ad a2 = o.a(this.b.d()).a(this.b.getContext(), this.b.n(), this.b.o());
            if (this.b.a()) {
                bk.b("AdLoaderHandler.run", "[has spam] do nothing, wait next timeout");
            } else if (a2 == null) {
                bk.b("AdLoaderHandler.run", "No Ads in cache");
            } else {
                this.b.l().b();
                ag a3 = this.b.a(this.b.getWidth(), this.b.getHeight(), a2);
                bk.b("AdLoaderHandler.run", String.format("adapter size(%d, %d)", Integer.valueOf(a3.a), Integer.valueOf(a3.b)));
                ViewGroup.LayoutParams layoutParams = this.b.getLayoutParams();
                layoutParams.width = a3.a;
                layoutParams.height = a3.b;
                this.a.setLayoutParams(layoutParams);
                int i = r.i(this.b.getContext());
                int h = r.h(this.b.getContext());
                if (this.b.d() == t.IMAGE) {
                    a3.a = Math.min(i, h);
                    a3.b = (int) ((((double) a3.a) / ((double) a2.c().getWidth())) * ((double) a2.c().getHeight()));
                }
                this.b.l().setVisibility(this.b.getVisibility());
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(a3.a, a3.b);
                layoutParams2.addRule(13);
                this.b.l().setLayoutParams(layoutParams2);
                this.b.l().a(a2, this.b.d(), this.b.e(), a3);
                this.b.l().setBackgroundColor(this.b.getBackgroundColor());
                this.b.l().b(this.b.getBackgroundTransparent());
                this.b.l().a(this.b.getTextColor());
                this.a.startAnimation(this.b.l().g());
            }
            this.b.d(false);
        } catch (Exception e) {
            this.b.g();
            bk.a("AdLoadHandler.run", "Unhandled exception while requesting an ad.", e);
        }
    }
}
