package com.baidu;

class ax {
    private static final byte[] a = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] b = new byte[128];

    static {
        for (int i = 0; i < 128; i++) {
            b[i] = -1;
        }
        for (int i2 = 65; i2 <= 90; i2++) {
            b[i2] = (byte) (i2 - 65);
        }
        for (int i3 = 97; i3 <= 122; i3++) {
            b[i3] = (byte) ((i3 - 97) + 26);
        }
        for (int i4 = 48; i4 <= 57; i4++) {
            b[i4] = (byte) ((i4 - 48) + 52);
        }
        b[43] = 62;
        b[47] = 63;
    }

    ax() {
    }

    public static String a(String str) {
        int i = 0;
        String str2 = str;
        while (str2.length() % 3 != 0) {
            str2 = str2 + "$";
        }
        byte[] bytes = str2.getBytes();
        byte[] bArr = new byte[((bytes.length / 3) * 4)];
        int i2 = 0;
        while (i2 < bytes.length) {
            bArr[i] = a[(bytes[i2] & 252) >> 2];
            bArr[i + 1] = a[((bytes[i2] & 3) << 4) + ((bytes[i2 + 1] & 240) >> 4)];
            bArr[i + 2] = a[((bytes[i2 + 1] & 15) << 2) + ((bytes[i2 + 2] & 192) >> 6)];
            bArr[i + 3] = a[bytes[i2 + 2] & 63];
            i2 += 3;
            i += 4;
        }
        return new String(bArr);
    }
}
