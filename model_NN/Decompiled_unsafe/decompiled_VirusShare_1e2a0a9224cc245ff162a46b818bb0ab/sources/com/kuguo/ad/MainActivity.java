package com.kuguo.ad;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.kuguo.a.d;
import com.kuguo.a.f;

public class MainActivity extends Activity {
    private o a;

    protected static void a(Context context, o oVar) {
        if (oVar.q == null || oVar.q.trim().equals("")) {
            b(context, null, oVar);
            return;
        }
        d dVar = new d(oVar.q, a.b(context, "icon.png", oVar.h), 0);
        dVar.a((Object) 0);
        r.a(context, dVar, new d(context, oVar));
    }

    private void a(o oVar, boolean z) {
        if (!z) {
            switch (oVar.e) {
                case 0:
                case 1:
                case 2:
                case 5:
                case 7:
                case 8:
                case 9:
                case 10:
                    a.a(this, q.b(this, oVar), q.a(oVar.F), oVar, a.a, false, false);
                    break;
            }
        }
        if (oVar.s == 1) {
            new l(this, oVar, false).a();
        } else if (oVar.e == 2) {
            r.a(this, oVar.d);
            oVar.l = 2;
            a.b(this, oVar);
            finish();
        }
    }

    /* access modifiers changed from: private */
    public static void b(Context context, String str, o oVar) {
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 100011 && i2 == 1200 && this.a != null) {
            Toast.makeText(this, "您可稍候通过桌面上 [" + this.a.g + "] 快捷方式进入软件列表页面！", 1).show();
            a(this, this.a);
            a.a(this, null, -1, this.a, a.a, false, true);
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        f.a(this);
        requestWindowFeature(1);
        Intent intent = getIntent();
        this.a = new o();
        if (this.a.a(intent.getStringExtra("message"))) {
            boolean booleanExtra = intent.getBooleanExtra("shortcut", false);
            this.a.f = intent.getIntExtra("sharedid", -1);
            if (intent.getBooleanExtra("applist", false)) {
                new l(this, this.a, true).a();
                return;
            }
            this.a.l = 1;
            a.b(this, this.a);
            a(this.a, booleanExtra);
            return;
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
