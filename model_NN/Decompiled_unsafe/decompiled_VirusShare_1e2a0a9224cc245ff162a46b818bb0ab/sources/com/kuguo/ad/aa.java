package com.kuguo.ad;

import android.content.Intent;
import android.view.View;
import com.kuguo.a.d;

class aa implements View.OnClickListener {
    final /* synthetic */ l a;

    aa(l lVar) {
        this.a = lVar;
    }

    private void a() {
        d dVar = new d(this.a.c.d, a.b(this.a.e, this.a.c.i + ".apk", this.a.c.h), 1);
        if (this.a.c.E == 1) {
            dVar.a(r.d(this.a.e));
        }
        dVar.a((Object) -2);
        r.a(this.a.e, 17301633, "正在下载...", this.a.c.h, 16, new Intent(), this.a.c.g, 0);
        r.a(this.a.e, dVar, new x(this.a.e, this.a.c, null));
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x02bd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onClick(android.view.View r11) {
        /*
            r10 = this;
            r9 = 2
            r2 = -1
            r8 = 268435456(0x10000000, float:2.5243549E-29)
            r5 = 0
            r6 = 1
            java.lang.Object r0 = r11.getTag()
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            switch(r0) {
                case 0: goto L_0x0014;
                case 1: goto L_0x0078;
                case 2: goto L_0x02dc;
                case 3: goto L_0x0303;
                case 4: goto L_0x0013;
                case 5: goto L_0x0013;
                case 6: goto L_0x0013;
                case 7: goto L_0x0013;
                case 8: goto L_0x0013;
                case 9: goto L_0x0013;
                case 10: goto L_0x0013;
                case 11: goto L_0x0358;
                default: goto L_0x0013;
            }
        L_0x0013:
            return
        L_0x0014:
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            com.kuguo.ad.l r1 = r10.a
            com.kuguo.ad.o r1 = r1.c
            com.kuguo.ad.MainActivity.a(r0, r1)
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "您可稍候通过桌面上 ["
            java.lang.StringBuilder r1 = r1.append(r3)
            com.kuguo.ad.l r3 = r10.a
            com.kuguo.ad.o r3 = r3.c
            java.lang.String r3 = r3.g
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = "] 快捷方式进入该软件的下载页面！"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r1, r6)
            r0.show()
            com.kuguo.ad.l r0 = r10.a
            com.kuguo.ad.o r0 = r0.c
            byte r0 = r0.e
            r1 = 7
            if (r0 != r1) goto L_0x006e
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            r1 = 0
            com.kuguo.ad.l r3 = r10.a
            com.kuguo.ad.o r3 = r3.c
            int r4 = com.kuguo.ad.a.a
            com.kuguo.ad.a.a(r0, r1, r2, r3, r4, r5, r6)
        L_0x006e:
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            r0.finish()
            goto L_0x0013
        L_0x0078:
            com.kuguo.ad.l r0 = r10.a
            boolean r0 = r0.g
            if (r0 == 0) goto L_0x0111
            java.lang.String r0 = "android__log"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "update ad status "
            java.lang.StringBuilder r1 = r1.append(r3)
            com.kuguo.ad.l r3 = r10.a
            com.kuguo.ad.o r3 = r3.c
            java.lang.String r3 = r3.g
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = " id : "
            java.lang.StringBuilder r1 = r1.append(r3)
            com.kuguo.ad.l r3 = r10.a
            com.kuguo.ad.o r3 = r3.c
            int r3 = r3.h
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = " status : "
            java.lang.StringBuilder r1 = r1.append(r3)
            r3 = 3
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            com.kuguo.c.a.a(r0, r1)
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            java.lang.String r0 = com.kuguo.ad.a.k(r0)
            if (r0 == 0) goto L_0x00df
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            java.lang.String r0 = com.kuguo.ad.a.k(r0)
            com.kuguo.ad.l r1 = r10.a
            com.kuguo.ad.o r1 = r1.c
            int r1 = r1.h
            r3 = 3
            com.kuguo.ad.a.a(r0, r1, r3)
        L_0x00df:
            com.kuguo.ad.l r0 = r10.a
            com.kuguo.ad.o r0 = r0.c
            byte r0 = r0.e
            if (r0 != r6) goto L_0x00fb
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            r1 = 0
            com.kuguo.ad.l r3 = r10.a
            com.kuguo.ad.o r3 = r3.c
            int r4 = com.kuguo.ad.a.a
            com.kuguo.ad.a.a(r0, r1, r2, r3, r4, r5, r6)
        L_0x00fb:
            com.kuguo.ad.l r0 = r10.a
            com.kuguo.ad.o r0 = r0.c
            byte r0 = r0.e
            switch(r0) {
                case 0: goto L_0x0131;
                case 1: goto L_0x012d;
                case 2: goto L_0x0106;
                case 3: goto L_0x012d;
                case 4: goto L_0x0106;
                case 5: goto L_0x012d;
                case 6: goto L_0x0106;
                case 7: goto L_0x012d;
                case 8: goto L_0x0106;
                case 9: goto L_0x01bc;
                case 10: goto L_0x0223;
                case 11: goto L_0x012d;
                default: goto L_0x0106;
            }
        L_0x0106:
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            r0.finish()
            goto L_0x0013
        L_0x0111:
            com.kuguo.ad.l r0 = r10.a
            com.kuguo.ad.o r0 = r0.c
            java.lang.Integer r1 = java.lang.Integer.valueOf(r9)
            r0.l = r1
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            com.kuguo.ad.l r1 = r10.a
            com.kuguo.ad.o r1 = r1.c
            com.kuguo.ad.a.b(r0, r1)
            goto L_0x00df
        L_0x012d:
            r10.a()
            goto L_0x0106
        L_0x0131:
            com.kuguo.ad.l r0 = r10.a
            com.kuguo.ad.o r0 = r0.c
            java.lang.String r0 = r0.d
            java.lang.String r1 = "["
            int r0 = r0.indexOf(r1)
            int r0 = r0 + 1
            com.kuguo.ad.l r1 = r10.a
            com.kuguo.ad.o r1 = r1.c
            java.lang.String r1 = r1.d
            java.lang.String r2 = "]"
            int r1 = r1.indexOf(r2, r0)
            com.kuguo.ad.l r2 = r10.a
            com.kuguo.ad.o r2 = r2.c
            java.lang.String r2 = r2.d
            java.lang.String r0 = r2.substring(r0, r1)
            com.kuguo.ad.l r2 = r10.a
            com.kuguo.ad.o r2 = r2.c
            java.lang.String r2 = r2.d
            java.lang.String r3 = "["
            int r1 = r2.indexOf(r3, r1)
            int r1 = r1 + 1
            com.kuguo.ad.l r2 = r10.a
            com.kuguo.ad.o r2 = r2.c
            java.lang.String r2 = r2.d
            java.lang.String r3 = "]"
            int r2 = r2.indexOf(r3, r1)
            com.kuguo.ad.l r3 = r10.a
            com.kuguo.ad.o r3 = r3.c
            java.lang.String r3 = r3.d
            java.lang.String r1 = r3.substring(r1, r2)
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            java.lang.String r3 = "android.intent.action.SENDTO"
            r2.setAction(r3)
            r2.addFlags(r8)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "smsto:"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            android.net.Uri r0 = android.net.Uri.parse(r0)
            r2.setData(r0)
            java.lang.String r0 = "sms_body"
            r2.putExtra(r0, r1)
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            r0.startActivity(r2)
            goto L_0x0106
        L_0x01bc:
            com.kuguo.ad.l r0 = r10.a
            com.kuguo.ad.o r0 = r0.c
            java.lang.String r0 = r0.d
            java.lang.String r1 = "["
            int r0 = r0.indexOf(r1)
            int r0 = r0 + 1
            com.kuguo.ad.l r1 = r10.a
            com.kuguo.ad.o r1 = r1.c
            java.lang.String r1 = r1.d
            java.lang.String r2 = "]"
            int r1 = r1.indexOf(r2, r0)
            com.kuguo.ad.l r2 = r10.a
            com.kuguo.ad.o r2 = r2.c
            java.lang.String r2 = r2.d
            r2.substring(r0, r1)
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            java.lang.String r3 = "android.intent.action.DIAL"
            r2.setAction(r3)
            r2.addFlags(r8)
            com.kuguo.ad.l r3 = r10.a
            com.kuguo.ad.o r3 = r3.c
            java.lang.String r3 = r3.d
            java.lang.String r0 = r3.substring(r0, r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "tel:"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            android.net.Uri r0 = android.net.Uri.parse(r0)
            r2.setData(r0)
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            r0.startActivity(r2)
            goto L_0x0106
        L_0x0223:
            com.kuguo.ad.l r0 = r10.a
            com.kuguo.ad.o r0 = r0.c
            java.lang.String r0 = r0.d
            com.kuguo.ad.l r1 = r10.a
            com.kuguo.ad.o r1 = r1.c
            java.lang.String r1 = r1.d
            java.lang.String r2 = "["
            int r1 = r1.indexOf(r2)
            int r1 = r1 + 1
            com.kuguo.ad.l r2 = r10.a
            com.kuguo.ad.o r2 = r2.c
            java.lang.String r2 = r2.d
            java.lang.String r3 = "]"
            int r2 = r2.indexOf(r3)
            java.lang.String r0 = r0.substring(r1, r2)
            java.lang.String r1 = ","
            java.lang.String[] r0 = r0.split(r1)
            if (r0 == 0) goto L_0x0013
            int r1 = r0.length
            if (r1 != r9) goto L_0x0013
            r1 = r0[r5]
            java.lang.Double r1 = java.lang.Double.valueOf(r1)
            double r1 = r1.doubleValue()
            r0 = r0[r6]
            java.lang.Double r0 = java.lang.Double.valueOf(r0)
            double r3 = r0.doubleValue()
            android.content.Intent r7 = new android.content.Intent
            r7.<init>()
            java.lang.String r0 = "android.intent.action.VIEW"
            r7.setAction(r0)
            r7.addFlags(r8)
            java.lang.String r0 = "http://ditu.google.cn/maps?hl=zh&mrt=loc&q=%s,%s"
            java.lang.Object[] r8 = new java.lang.Object[r9]
            java.lang.Double r3 = java.lang.Double.valueOf(r3)
            r8[r5] = r3
            java.lang.Double r1 = java.lang.Double.valueOf(r1)
            r8[r6] = r1
            java.lang.String r0 = java.lang.String.format(r0, r8)
            android.net.Uri r0 = android.net.Uri.parse(r0)
            r7.setData(r0)
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            java.util.List r0 = r0.queryIntentActivities(r7, r5)
            if (r0 == 0) goto L_0x02aa
            int r1 = r0.size()
            if (r1 != 0) goto L_0x02b3
        L_0x02aa:
            java.lang.String r0 = "android__log"
            java.lang.String r1 = "not find the matched Activity click action not work!"
            android.util.Log.d(r0, r1)
            goto L_0x0013
        L_0x02b3:
            java.util.Iterator r1 = r0.iterator()
        L_0x02b7:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x02d1
            java.lang.Object r0 = r1.next()
            android.content.pm.ResolveInfo r0 = (android.content.pm.ResolveInfo) r0
            java.lang.String r2 = "com.google.android.apps.maps"
            android.content.pm.ActivityInfo r0 = r0.activityInfo
            android.content.pm.ApplicationInfo r0 = r0.applicationInfo
            java.lang.String r0 = r0.packageName
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x02b7
        L_0x02d1:
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            r0.startActivity(r7)
            goto L_0x0106
        L_0x02dc:
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            r0.finish()
            com.kuguo.ad.l r0 = r10.a
            com.kuguo.ad.o r0 = r0.c
            int r0 = r0.B
            if (r0 != r6) goto L_0x0013
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            r1 = 0
            com.kuguo.ad.l r3 = r10.a
            com.kuguo.ad.o r3 = r3.c
            int r4 = com.kuguo.ad.a.a
            com.kuguo.ad.a.a(r0, r1, r2, r3, r4, r5, r6)
            goto L_0x0013
        L_0x0303:
            com.kuguo.ad.l r0 = r10.a
            boolean r0 = r0.c()
            if (r0 == 0) goto L_0x0329
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            java.lang.String r1 = "正在打开360手机卫士..."
            r2 = 1500(0x5dc, float:2.102E-42)
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r1, r2)
            r0.show()
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            java.lang.String r1 = "com.qihoo360.mobilesafe"
            com.kuguo.ad.a.d(r0, r1)
            goto L_0x0013
        L_0x0329:
            com.kuguo.ad.l r0 = r10.a
            android.app.Activity r0 = r0.e
            java.lang.String r1 = "进入浏览器了解360手机卫士详细内容..."
            r2 = 1500(0x5dc, float:2.102E-42)
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r1, r2)
            r0.show()
            android.content.Intent r0 = new android.content.Intent
            r0.<init>()
            java.lang.String r1 = "android.intent.action.VIEW"
            r0.setAction(r1)
            java.lang.String r1 = "http://video.cooguo.com/cooguogw/wap/360Push.jsp"
            android.net.Uri r1 = android.net.Uri.parse(r1)
            r0.setData(r1)
            com.kuguo.ad.l r1 = r10.a
            android.app.Activity r1 = r1.e
            r1.startActivity(r0)
            goto L_0x0013
        L_0x0358:
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "android.intent.action.SEND"
            r0.<init>(r1)
            java.lang.String r1 = "text/plain"
            r0.setType(r1)
            java.lang.String r1 = "android.intent.extra.TEXT"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            com.kuguo.ad.l r3 = r10.a
            com.kuguo.ad.o r3 = r3.c
            java.lang.String r3 = r3.m
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "  \n"
            java.lang.StringBuilder r2 = r2.append(r3)
            com.kuguo.ad.l r3 = r10.a
            com.kuguo.ad.o r3 = r3.c
            java.lang.String r3 = r3.b
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " \n"
            java.lang.StringBuilder r2 = r2.append(r3)
            com.kuguo.ad.l r3 = r10.a
            com.kuguo.ad.o r3 = r3.c
            java.lang.String r3 = r3.d
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.putExtra(r1, r2)
            r0.addFlags(r8)
            com.kuguo.ad.l r1 = r10.a
            android.app.Activity r1 = r1.e
            java.lang.String r2 = "请选择共享方式"
            android.content.Intent r0 = android.content.Intent.createChooser(r0, r2)
            r1.startActivity(r0)
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kuguo.ad.aa.onClick(android.view.View):void");
    }
}
