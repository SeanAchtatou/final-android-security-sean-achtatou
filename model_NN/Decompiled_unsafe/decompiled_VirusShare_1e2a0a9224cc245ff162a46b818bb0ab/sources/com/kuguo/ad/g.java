package com.kuguo.ad;

import android.content.Context;
import android.location.Location;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import com.kuguo.a.d;
import com.kuguo.a.f;
import com.kuguo.c.a;
import com.kuguo.ui.c;
import java.io.File;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;

public class g {
    static Object a = new Object();
    /* access modifiers changed from: private */
    public Context b;
    /* access modifiers changed from: private */
    public ap c;
    /* access modifiers changed from: private */
    public int d = 4;
    private z e;
    private x f;
    private int g = 4;
    private Handler h = new w(this);
    /* access modifiers changed from: private */
    public c i;

    public g(Context context) {
        this.c = new ap(context);
        this.b = context;
        f.a(context);
    }

    private void a(o[] oVarArr) {
        int length = oVarArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            if (!new File(Environment.getExternalStorageDirectory(), ("download/ads/" + oVarArr[i2].h) + "/icon.png").exists()) {
                d dVar = new d(oVarArr[i2].q, a.b(this.b, "icon.png", oVarArr[i2].h), 0);
                dVar.a(Integer.valueOf(i2));
                this.f = new x(this.b, oVarArr[i2], this.h);
                r.a(this.b, dVar, this.f);
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e2) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        int i2 = 0;
        boolean z = false;
        while (!z && i2 < this.g) {
            String externalStorageState = Environment.getExternalStorageState();
            if ("mounted".equals(externalStorageState)) {
                z = c();
            } else {
                Log.d("android__log", "externalStorageState = " + externalStorageState);
            }
            i2++;
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
    }

    private boolean c() {
        boolean z;
        boolean z2;
        HttpPost httpPost = new HttpPost("http://www.cooguo.com/cooguogw/list.action");
        d();
        this.c.a();
        try {
            httpPost.setEntity(new ByteArrayEntity(this.c.b()));
            a.a("android__log", "post url is " + httpPost.getURI());
        } catch (Exception e2) {
        }
        try {
            HttpClient a2 = r.a(this.b);
            if (a2 == null) {
                return false;
            }
            HttpResponse execute = a2.execute(httpPost);
            int statusCode = execute.getStatusLine().getStatusCode();
            Log.d("android__log", "b -----------status: " + statusCode);
            if (statusCode == 200) {
                try {
                    a.d(this.c.n);
                    a(execute.getEntity().getContent());
                    return true;
                } catch (ClientProtocolException e3) {
                    ClientProtocolException clientProtocolException = e3;
                    z = true;
                    e = clientProtocolException;
                } catch (IOException e4) {
                    IOException iOException = e4;
                    z2 = true;
                    e = iOException;
                    e.printStackTrace();
                    return z2;
                } catch (Exception e5) {
                    return true;
                }
            } else {
                httpPost.abort();
                return false;
            }
        } catch (ClientProtocolException e6) {
            e = e6;
            z = false;
            e.printStackTrace();
            return z;
        } catch (IOException e7) {
            e = e7;
            z2 = false;
            e.printStackTrace();
            return z2;
        } catch (Exception e8) {
            return false;
        }
    }

    private void d() {
        String k = a.k(this.b);
        if (k != null) {
            this.c.n = k;
        }
        this.c.l = r.d(this.b);
        String e2 = a.e(this.c.n);
        if (e2 != null) {
            e2 = "-13," + a.s(this.b) + ";" + e2;
        }
        a.a("android__log", "first request statu is ------------ " + e2);
        this.c.j = e2;
        Location q = a.q(this.b);
        a.a("location : " + q);
        if (q != null) {
            this.c.f = q.getLongitude();
            this.c.e = q.getLatitude();
            this.c.s = a.a(q.getLongitude(), q.getLatitude());
        }
    }

    public void a() {
        new u(this).start();
    }

    public void a(z zVar) {
        this.e = zVar;
    }

    public void a(c cVar) {
        this.i = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kuguo.ad.a.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.kuguo.ad.a.a(android.content.Context, int):int
      com.kuguo.ad.a.a(android.content.Context, java.lang.String):com.kuguo.ad.o
      com.kuguo.ad.a.a(double, double):java.lang.String
      com.kuguo.ad.a.a(java.lang.String, int):java.lang.String
      com.kuguo.ad.a.a(android.content.Context, com.kuguo.ad.o):boolean
      com.kuguo.ad.a.a(android.content.Context, com.kuguo.ad.o[]):boolean
      com.kuguo.ad.a.a(java.io.File, java.lang.String):boolean
      com.kuguo.ad.a.a(java.lang.String, java.lang.String):java.lang.String[]
      com.kuguo.ad.a.a(android.content.Context, boolean):void */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.io.InputStream r9) {
        /*
            r8 = this;
            r2 = 0
            r6 = 1
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0103, all -> 0x00f2 }
            r0.<init>()     // Catch:{ IOException -> 0x0103, all -> 0x00f2 }
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ IOException -> 0x0107, all -> 0x00f6 }
            r1.<init>(r9)     // Catch:{ IOException -> 0x0107, all -> 0x00f6 }
            r2 = 512(0x200, float:7.175E-43)
            byte[] r2 = new byte[r2]     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
        L_0x0010:
            int r3 = r1.read(r2)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            r4 = -1
            if (r3 == r4) goto L_0x0036
            r4 = 0
            r0.write(r2, r4, r3)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            r0.flush()     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            goto L_0x0010
        L_0x001f:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r0
            r0 = r7
        L_0x0024:
            java.lang.Exception r3 = new java.lang.Exception     // Catch:{ all -> 0x002a }
            r3.<init>(r0)     // Catch:{ all -> 0x002a }
            throw r3     // Catch:{ all -> 0x002a }
        L_0x002a:
            r0 = move-exception
        L_0x002b:
            if (r1 == 0) goto L_0x0030
            r1.close()
        L_0x0030:
            if (r2 == 0) goto L_0x0035
            r2.close()
        L_0x0035:
            throw r0
        L_0x0036:
            byte[] r2 = r0.toByteArray()     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            if (r2 == 0) goto L_0x003f
            int r3 = r2.length     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            if (r3 != 0) goto L_0x004a
        L_0x003f:
            if (r0 == 0) goto L_0x0044
            r0.close()
        L_0x0044:
            if (r1 == 0) goto L_0x0049
            r1.close()
        L_0x0049:
            return
        L_0x004a:
            com.kuguo.ad.o[] r2 = com.kuguo.ad.o.a(r2)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            if (r2 != 0) goto L_0x007e
            com.kuguo.ad.z r3 = r8.e     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            if (r3 == 0) goto L_0x0059
            com.kuguo.ad.z r3 = r8.e     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            r3.a(r2)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
        L_0x0059:
            com.kuguo.ad.KuguoAdsManager r2 = com.kuguo.ad.KuguoAdsManager.getInstance()     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            android.content.Context r3 = r8.b     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            r4 = 0
            r2.removeKuguoSprite(r3, r4)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            com.kuguo.ad.KuguoAdsManager r2 = com.kuguo.ad.KuguoAdsManager.getInstance()     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            android.content.Context r3 = r8.b     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            r4 = 1
            r2.removeKuguoSprite(r3, r4)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            android.content.Context r2 = r8.b     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            r3 = 0
            com.kuguo.ad.a.a(r2, r3)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            if (r0 == 0) goto L_0x0078
            r0.close()
        L_0x0078:
            if (r1 == 0) goto L_0x0049
            r1.close()
            goto L_0x0049
        L_0x007e:
            android.content.Context r3 = r8.b     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            r4 = 1
            com.kuguo.ad.a.a(r3, r4)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            com.kuguo.ad.KuguoAdsManager r3 = com.kuguo.ad.KuguoAdsManager.getInstance()     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            android.content.Context r4 = r8.b     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            r3.showSprite(r4)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            com.kuguo.ad.z r3 = r8.e     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            if (r3 == 0) goto L_0x0096
            com.kuguo.ad.z r3 = r8.e     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            r3.a(r2)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
        L_0x0096:
            java.lang.String r3 = "android__log"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            r4.<init>()     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            java.lang.String r5 = "msgs size is "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            int r5 = r2.length     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            com.kuguo.c.a.a(r3, r4)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            android.content.Context r3 = r8.b     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            com.kuguo.ad.a.a(r3, r2)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            int r3 = r2.length     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            if (r3 <= r6) goto L_0x00de
            java.lang.String r3 = "android__log"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            r4.<init>()     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            java.lang.String r5 = "msg[0].liststyle------------- "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            r5 = 0
            r5 = r2[r5]     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            int r5 = r5.J     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            com.kuguo.c.a.a(r3, r4)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            android.content.Context r3 = r8.b     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            r4 = 0
            r4 = r2[r4]     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            int r4 = r4.J     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            com.kuguo.ad.a.b(r3, r4)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
        L_0x00de:
            r8.a(r2)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            android.content.Context r2 = r8.b     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            com.kuguo.ad.a.p(r2)     // Catch:{ IOException -> 0x001f, all -> 0x00fc }
            if (r0 == 0) goto L_0x00eb
            r0.close()
        L_0x00eb:
            if (r1 == 0) goto L_0x0049
            r1.close()
            goto L_0x0049
        L_0x00f2:
            r0 = move-exception
            r1 = r2
            goto L_0x002b
        L_0x00f6:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x002b
        L_0x00fc:
            r2 = move-exception
            r7 = r2
            r2 = r1
            r1 = r0
            r0 = r7
            goto L_0x002b
        L_0x0103:
            r0 = move-exception
            r1 = r2
            goto L_0x0024
        L_0x0107:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kuguo.ad.g.a(java.io.InputStream):void");
    }
}
