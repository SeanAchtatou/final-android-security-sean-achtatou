package com.kuguo.ad;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.kuguo.c.a;

class w extends Handler {
    final /* synthetic */ g a;

    w(g gVar) {
        this.a = gVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kuguo.ad.a.a(android.content.Context, java.lang.String, boolean):android.graphics.Bitmap
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.kuguo.ad.a.a(android.content.Context, java.lang.String, int):int
      com.kuguo.ad.a.a(android.content.Context, java.lang.String, java.lang.String):int
      com.kuguo.ad.a.a(android.content.Context, java.io.File, int):android.content.Intent
      com.kuguo.ad.a.a(android.content.Context, int, int):boolean
      com.kuguo.ad.a.a(java.lang.String, int, int):boolean
      com.kuguo.ad.a.a(android.content.Context, java.lang.String, boolean):android.graphics.Bitmap */
    public synchronized void handleMessage(Message message) {
        Bundle data = message.getData();
        data.getInt("tag");
        String string = data.getString("filePath");
        if (!(string == null || a.a(this.a.b, string, false) == null)) {
            a.a("android__log", "receiver icon first request !!! ");
            if (this.a.i != null) {
                this.a.i.a();
            }
        }
    }
}
