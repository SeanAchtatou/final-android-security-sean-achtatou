package com.kuguo.ad;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.kuguo.c.a;

class c extends BroadcastReceiver {
    final /* synthetic */ KuguoAdsManager a;

    c(KuguoAdsManager kuguoAdsManager) {
        this.a = kuguoAdsManager;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kuguo.ad.a.a(android.content.Context, java.lang.String, int, boolean):void
     arg types: [android.content.Context, java.lang.String, int, int]
     candidates:
      com.kuguo.ad.a.a(android.content.Context, java.lang.String, java.lang.String, int):void
      com.kuguo.ad.a.a(android.content.Context, java.lang.String, int, boolean):void */
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.PACKAGE_ADDED".equals(intent.getAction())) {
            String uri = intent.getData().toString();
            String substring = uri.substring("package:".length(), uri.length());
            a.a("android__log", "j " + substring);
            o g = a.g(context, substring);
            if (g != null) {
                if (!KuguoAdsManager.listener.isEmpty()) {
                    for (al a2 : KuguoAdsManager.listener) {
                        a2.a(g);
                    }
                }
                a.a("android__log", "update ad status " + g.g + " id : " + g.h + " status : " + 5);
                if (a.k(context) != null) {
                    a.a(a.k(context), g.h, 5);
                }
                new ab(context).a();
                a.a(context, substring + ".apk", g.h, true);
                a.a(context, "ct.dat", g.h, true);
            }
        }
    }
}
