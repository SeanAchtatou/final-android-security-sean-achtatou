package com.kuguo.ad;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.kuguo.c.a;
import com.kuguo.ui.Workspace;
import java.util.ArrayList;
import java.util.List;

public class BoutiqueActivity extends Activity implements AdapterView.OnItemClickListener, z {
    private static Activity g;
    private LinearLayout a;
    private FrameLayout b;
    private FrameLayout c;
    /* access modifiers changed from: private */
    public Workspace d;
    /* access modifiers changed from: private */
    public List e;
    /* access modifiers changed from: private */
    public Handler f = new Handler();
    private int h = 355;
    /* access modifiers changed from: private */
    public int i;

    public static Activity a() {
        if (g != null) {
            return g;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void a(List list) {
        int i2;
        KuguoAdsManager.resetOnAppInstallListener();
        this.d = new Workspace(this);
        int size = list.size();
        int ceil = (int) Math.ceil((((double) size) * 1.0d) / 16.0d);
        int i3 = 0;
        while (i3 < ceil) {
            GridView gridView = new GridView(this);
            gridView.setNumColumns(4);
            gridView.setVerticalSpacing(10);
            if (a.n(this) == 120) {
                gridView.setPadding(a.a(this, 7), 0, a.a(this, 7), 0);
            } else {
                gridView.setPadding(a.a(this, 7), a.a(this, 10), a.a(this, 7), 0);
            }
            gridView.setOnItemClickListener(this);
            int i4 = i3 * 16;
            gridView.setAdapter((ListAdapter) new n(this, list.subList(i4, i3 < ceil - 1 ? i4 + 16 : (size - ((ceil - 1) * 16)) + i4), 1));
            this.d.addView(gridView, new ViewGroup.LayoutParams(-2, -2));
            i3++;
        }
        WindowManager windowManager = getWindowManager();
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        if (a.n(this) <= 120 || width >= height) {
            i2 = -2;
        } else if (size >= 16) {
            i2 = a.a(this, this.h);
        } else {
            int i5 = size / 4;
            i2 = a.a(this, (size % 4 != 0 ? i5 + 1 : i5) * (this.h / 4));
        }
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a.a(this, 295), i2);
        layoutParams.gravity = 17;
        this.b.addView(this.d, layoutParams);
        this.b.removeView(this.c);
    }

    /* access modifiers changed from: private */
    public List b(o[] oVarArr) {
        if (oVarArr == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (o oVar : oVarArr) {
            if (oVar != null) {
                arrayList.add(oVar);
            }
        }
        return arrayList;
    }

    private void b() {
        if (a.o(this)) {
            new aj(this).start();
            return;
        }
        a.a("android__log", "has not got app list msgs, request now !!! ");
        g gVar = new g(this);
        gVar.a(this);
        gVar.a(new ak(this));
        gVar.a();
    }

    /* access modifiers changed from: private */
    public void b(List list) {
        ListView listView = new ListView(this);
        listView.setAdapter((ListAdapter) new n(this, list, 2));
        listView.setOnItemClickListener(this);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a.a(this, 295), -2);
        layoutParams.gravity = 17;
        this.b.addView(listView, layoutParams);
        this.b.removeView(this.c);
    }

    private void c() {
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-11364418, -11364418, -11364418}));
        TextView textView = new TextView(this);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        int a2 = a.a(this, 10);
        textView.setPadding(0, a2, 0, a2);
        textView.setText("APP精品推荐");
        textView.setTextColor(-1);
        textView.setTextSize(22.0f);
        frameLayout.addView(textView, layoutParams);
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-1, -2);
        layoutParams2.gravity = 17;
        this.a.addView(frameLayout, layoutParams2);
    }

    /* access modifiers changed from: private */
    public void c(List list) {
        GridView gridView = new GridView(this);
        gridView.setNumColumns(2);
        gridView.setVerticalSpacing(2);
        gridView.setHorizontalSpacing(2);
        gridView.setPadding(2, 2, 2, 2);
        gridView.setBackgroundColor(-2892318);
        gridView.setOnItemClickListener(this);
        gridView.setAdapter((ListAdapter) new n(this, list, 3));
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a.a(this, 295), -2);
        layoutParams.gravity = 17;
        this.b.addView(gridView, layoutParams);
        this.b.removeView(this.c);
    }

    private void d() {
        this.b = new FrameLayout(this);
        this.b.setBackgroundColor(-1);
        WindowManager windowManager = getWindowManager();
        int a2 = windowManager.getDefaultDisplay().getWidth() < windowManager.getDefaultDisplay().getHeight() ? a.a(this, this.h) : a.a(this, 300);
        this.c = new FrameLayout(this);
        ProgressBar progressBar = new ProgressBar(this);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        this.c.addView(progressBar, layoutParams);
        this.b.addView(this.c, new FrameLayout.LayoutParams(a.a(this, 295), a2));
        this.a.addView(this.b);
    }

    private void e() {
        switch (a.s(this)) {
            case 0:
                this.i = 1;
                return;
            case 1:
                this.i = 2;
                return;
            case 2:
                this.i = 3;
                return;
            default:
                this.i = 1;
                return;
        }
    }

    public void a(o[] oVarArr) {
        this.f.post(new ao(this, oVarArr));
    }

    public void finish() {
        super.finish();
        g = null;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        g = this;
        e();
        requestWindowFeature(1);
        this.a = new LinearLayout(this);
        this.a.setPadding(a.a(this, 2), a.a(this, 2), a.a(this, 2), a.a(this, 4));
        this.a.setBackgroundColor(-7158214);
        this.a.setOrientation(1);
        c();
        d();
        b();
        setContentView(this.a, new LinearLayout.LayoutParams(-1, -2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onItemClick(AdapterView adapterView, View view, int i2, long j) {
        o oVar = (o) adapterView.getAdapter().getItem(i2);
        Intent intent = new Intent();
        if (!oVar.j || oVar.k.equals("")) {
            intent.putExtra("message", oVar.toString());
            intent.putExtra("sharedid", oVar.f);
            intent.putExtra("shortcut", false);
            intent.putExtra("applist", true);
            intent.setClass(this, MainActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "正在打开" + oVar.g + "...", 1500).show();
            intent.setComponent(new ComponentName(oVar.i, oVar.k));
            startActivity(intent);
        }
        a.a("android__log", "update ad status " + oVar.g + " id : " + oVar.h + " status : " + 2);
        if (a.k(this) != null) {
            a.a(a.k(this), oVar.h, 2);
        }
    }
}
