package com.kuguo.ad;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import com.kuguo.a.c;
import com.kuguo.a.d;
import com.kuguo.a.f;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class r {
    protected static HttpClient a(Context context) {
        if (d(context) == null) {
            return null;
        }
        if (!b(context)) {
            return new DefaultHttpClient();
        }
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 30000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        HttpConnectionParams.setSocketBufferSize(basicHttpParams, 204800);
        HttpClientParams.setRedirecting(basicHttpParams, true);
        basicHttpParams.setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80));
        return new DefaultHttpClient(basicHttpParams);
    }

    protected static void a(Context context, int i, String str, int i2, int i3, Intent intent, String str2, int i4) {
        Notification notification = new Notification(i, "", System.currentTimeMillis());
        PendingIntent activity = PendingIntent.getActivity(context, 0, intent, 134217728);
        notification.flags = i3;
        StringBuffer stringBuffer = new StringBuffer();
        if (i4 > -1 && i4 <= 100) {
            stringBuffer.append(i4);
            stringBuffer.append("%    ");
        }
        stringBuffer.append(str2);
        notification.setLatestEventInfo(context, str, stringBuffer.toString(), activity);
        ((NotificationManager) context.getSystemService("notification")).notify(i2 + 10000, notification);
    }

    protected static void a(Context context, d dVar, c cVar) {
        a.a("NetworkImpl.java downloadProxy() ");
        f a = f.a();
        if (a == null) {
            a = f.a(context);
        }
        d b = a.b(dVar);
        if (b == null || b.g() == 5 || b.g() == 4) {
            a.a("dt == null ");
            if (cVar != null) {
                dVar.a(cVar);
            }
            dVar.d();
            return;
        }
        a.a("dt != null  = " + b.g());
        b.a(dVar.n());
        b.a(cVar);
        if (cVar != null) {
            cVar.a(b, b.g());
        }
    }

    protected static void a(Context context, String str) {
        context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
    }

    protected static boolean b(Context context) {
        String c = c(context);
        if (c != null) {
            return c.compareTo("cmwap") == 0 || c.compareTo("3gwap") == 0 || c.compareTo("uniwap") == 0;
        }
        return false;
    }

    protected static String c(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return null;
        }
        return activeNetworkInfo.getExtraInfo();
    }

    protected static String d(Context context) {
        String c = c(context);
        if (c != null && !"".equals(c.trim())) {
            return c;
        }
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return null;
        }
        return activeNetworkInfo.getTypeName();
    }
}
