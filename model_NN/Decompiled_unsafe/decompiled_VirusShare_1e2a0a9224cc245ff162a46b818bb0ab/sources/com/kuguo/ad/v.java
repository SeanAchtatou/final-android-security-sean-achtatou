package com.kuguo.ad;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.io.IOException;

final class v extends Handler {
    final /* synthetic */ MainService a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v(MainService mainService, Looper looper) {
        super(looper);
        this.a = mainService;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kuguo.ad.MainService.a(com.kuguo.ad.MainService, boolean):void
     arg types: [com.kuguo.ad.MainService, int]
     candidates:
      com.kuguo.ad.MainService.a(android.content.Context, int):void
      com.kuguo.ad.MainService.a(com.kuguo.ad.MainService, boolean):void */
    public void handleMessage(Message message) {
        this.a.c();
        Log.d("android__log", "cooId == " + this.a.b.n);
        Log.d("android__log", "channelId == " + this.a.b.o);
        int intExtra = ((Intent) message.obj).getIntExtra("requestMode", 0);
        try {
            String[] list = this.a.getAssets().list("kuguo_res");
            if (list == null || list.length == 0) {
                Log.d("android__log", "App has not resource picture under assets director , cancel request Ad!!!!!!!");
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("android__log", " ready to request...");
        String j = a.j(this.a);
        int e2 = a.e(this.a);
        if (this.a.b.n.equals("f946b3d4086249a6968aabec7c752027")) {
            if (intExtra != 10) {
                Log.d("android__log", " ----------------------- request test ad!");
                this.a.b.m = MainService.e;
                this.a.a(false);
            }
        } else if (intExtra == 10 || e2 == 2) {
            this.a.b.m = 10;
            this.a.a(false);
        } else if (this.a.e() && (this.a.d() || MainService.e != 2)) {
            if (j == null && intExtra != 10) {
                this.a.b.m = 3;
                this.a.a(false);
                this.a.b.r = a.j(this.a);
                this.a.b.m = MainService.e;
                this.a.a(true);
            } else if (e2 == 0 || e2 == 1) {
                this.a.b.m = MainService.e;
                this.a.a(true);
            } else {
                Log.d("android__log", " ----------------------- noon request");
            }
        }
        MainReceiver.a(this.a, message.arg1);
    }
}
