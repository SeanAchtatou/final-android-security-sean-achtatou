package com.kuguo.ad;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.kuguo.c.d;

public class h extends LinearLayout {
    private ImageView a;
    private TextView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private ImageView f;
    private Drawable g;

    public h(Context context) {
        super(context);
        setOrientation(0);
        setPadding(a.a(context, 6), a.a(context, 6), a.a(context, 6), a.a(context, 6));
        this.g = d.b(context, "kuguo_res/icon.png");
        FrameLayout frameLayout = new FrameLayout(context);
        this.a = new ImageView(context);
        this.a.setImageDrawable(this.g);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a.a(context, 46), a.a(context, 46));
        layoutParams.gravity = 17;
        frameLayout.addView(this.a, layoutParams);
        this.f = new ImageView(context);
        this.f.setImageDrawable(d.b(context, "kuguo_res/installed.png"));
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(a.a(context, 18), a.a(context, 18));
        layoutParams2.gravity = 53;
        frameLayout.addView(this.f, layoutParams2);
        this.f.setVisibility(4);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.gravity = 17;
        addView(frameLayout, layoutParams3);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setPadding(10, 0, 0, 0);
        linearLayout.setOrientation(1);
        this.b = new TextView(context);
        this.b.setTextColor(-14974791);
        this.b.setTextSize(16.0f);
        linearLayout.addView(this.b, new LinearLayout.LayoutParams(-2, -2));
        this.c = new TextView(context);
        this.c.setTextColor(-7829368);
        linearLayout.addView(this.c, new LinearLayout.LayoutParams(-2, -2));
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setOrientation(0);
        this.d = new TextView(context);
        this.d.setTextColor(-7829368);
        this.d.setTextSize(14.0f);
        linearLayout2.addView(this.d, new LinearLayout.LayoutParams(-2, -2));
        this.e = new TextView(context);
        this.e.setTextColor(-7829368);
        this.e.setTextSize(14.0f);
        this.e.setPadding(5, 0, 0, 0);
        linearLayout2.addView(this.e, new LinearLayout.LayoutParams(-2, -2));
        linearLayout.addView(linearLayout2, new LinearLayout.LayoutParams(-1, -2));
        addView(linearLayout, new LinearLayout.LayoutParams(-1, -2));
    }

    private String a(float f2) {
        return f2 > 1024.0f ? (((float) ((int) ((f2 / 1024.0f) * 100.0f))) / 100.0f) + "M" : ((int) Math.ceil((double) f2)) + "K";
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        this.e.setText("大小：" + a((float) i));
    }

    /* access modifiers changed from: protected */
    public void a(Drawable drawable) {
        this.a.setImageDrawable(drawable);
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        this.b.setText(str);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        if (z) {
            this.f.setVisibility(0);
        } else {
            this.f.setVisibility(4);
        }
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        this.c.setText("类型：" + str);
    }

    /* access modifiers changed from: protected */
    public void c(String str) {
        this.d.setText("版本：" + str);
    }
}
