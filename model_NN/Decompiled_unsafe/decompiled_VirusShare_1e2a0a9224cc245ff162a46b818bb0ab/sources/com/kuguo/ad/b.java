package com.kuguo.ad;

public class b {
    private static final char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    protected static long a(byte[] bArr) {
        switch (bArr.length) {
            case 1:
                return (long) (bArr[0] & 255);
            case 2:
                return (long) c(bArr);
            case 3:
            case 5:
            case 6:
            case 7:
            default:
                return 0;
            case 4:
                return (long) b(bArr);
            case 8:
                return ((((long) bArr[0]) << 56) & -72057594037927936L) | ((((long) bArr[1]) << 48) & 71776119061217280L) | ((((long) bArr[2]) << 40) & 280375465082880L) | ((((long) bArr[3]) << 32) & 1095216660480L) | ((((long) bArr[4]) << 24) & 4278190080L) | ((((long) bArr[5]) << 16) & 16711680) | ((((long) bArr[6]) << 8) & 65280) | (((long) bArr[7]) & 255);
        }
    }

    protected static byte[] a(int i) {
        return new byte[]{(byte) ((-16777216 & i) >> 24), (byte) ((16711680 & i) >> 16), (byte) ((65280 & i) >> 8), (byte) (i & 255)};
    }

    protected static byte[] a(short s) {
        return new byte[]{(byte) ((65280 & s) >> 8), (byte) (s & 255)};
    }

    protected static int b(byte[] bArr) {
        return ((bArr[0] << 24) & -16777216) | ((bArr[1] << 16) & 16711680) | ((bArr[2] << 8) & 65280) | (bArr[3] & 255);
    }

    protected static int c(byte[] bArr) {
        return ((bArr[0] << 8) & 65280) | (bArr[1] & 255);
    }
}
