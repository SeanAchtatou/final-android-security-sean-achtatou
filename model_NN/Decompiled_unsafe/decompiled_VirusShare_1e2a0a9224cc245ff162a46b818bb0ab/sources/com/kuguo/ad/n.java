package com.kuguo.ad;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import com.kuguo.a.d;
import com.kuguo.a.f;
import com.kuguo.c.c;
import java.io.File;
import java.util.Iterator;
import java.util.List;

public class n extends BaseAdapter implements al {
    /* access modifiers changed from: private */
    public Context a;
    /* access modifiers changed from: private */
    public List b;
    private x c;
    private int d;
    private Handler e = new Handler();
    private Handler f = new y(this);

    public n(Context context, List list, int i) {
        this.a = context;
        this.b = list;
        this.d = i;
        f.a(context);
        a();
        b();
        KuguoAdsManager.addOnAppInstallListener(this);
    }

    private void a() {
        new p(this).start();
    }

    /* access modifiers changed from: private */
    public void a(List list) {
        for (PackageInfo next : this.a.getPackageManager().getInstalledPackages(0)) {
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            Iterator it = list.iterator();
            while (it.hasNext()) {
                o oVar = (o) it.next();
                if (oVar.i.equals(next.packageName)) {
                    oVar.j = true;
                    intent.setPackage(oVar.i);
                    for (ResolveInfo resolveInfo : this.a.getPackageManager().queryIntentActivities(intent, 0)) {
                        oVar.k = resolveInfo.activityInfo.name;
                    }
                }
            }
        }
    }

    private void b() {
        this.e.post(new m(this));
    }

    /* access modifiers changed from: private */
    public void b(o oVar) {
        d dVar = new d(oVar.q, a.b(this.a, "icon.png", oVar.h), 0);
        dVar.a(Integer.valueOf(oVar.h));
        this.c = new x(this.a, oVar, this.f);
        r.a(this.a, dVar, this.c);
    }

    public void a(o oVar) {
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        PackageManager packageManager = this.a.getPackageManager();
        for (o oVar2 : this.b) {
            if (oVar.h == oVar2.h) {
                oVar2.j = true;
                intent.setPackage(oVar2.i);
                Iterator<ResolveInfo> it = packageManager.queryIntentActivities(intent, 0).iterator();
                if (it.hasNext()) {
                    oVar2.k = it.next().activityInfo.name;
                    notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    public int getCount() {
        return this.b.size();
    }

    public Object getItem(int i) {
        if (i < this.b.size()) {
            return this.b.get(i);
        }
        return null;
    }

    public long getItemId(int i) {
        if (i < this.b.size()) {
            return (long) ((o) this.b.get(i)).h;
        }
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ac acVar;
        Drawable b2;
        Drawable b3;
        Drawable b4;
        ac acVar2;
        Drawable b5;
        if (this.d == 1) {
            if (view instanceof ac) {
                ac acVar3 = (ac) view;
                acVar3.a();
                acVar3.a(false);
                acVar2 = acVar3;
            } else {
                ac acVar4 = new ac(this.a);
                acVar4.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
                acVar2 = acVar4;
            }
            o oVar = (o) this.b.get(i);
            File file = new File(Environment.getExternalStorageDirectory(), ("download/ads/" + oVar.h) + "/icon.png");
            if (file.exists() && (b5 = c.b(this.a, file.getAbsolutePath())) != null) {
                acVar2.a(b5);
            }
            if (oVar.j && !"".equals(oVar.k)) {
                acVar2.a(true);
            }
            acVar2.a(oVar.g);
            return acVar2;
        } else if (this.d == 2) {
            h hVar = new h(this.a);
            hVar.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
            o oVar2 = (o) this.b.get(i);
            File file2 = new File(Environment.getExternalStorageDirectory(), ("download/ads/" + oVar2.h) + "/icon.png");
            if (file2.exists() && (b4 = c.b(this.a, file2.getAbsolutePath())) != null) {
                hVar.a(b4);
            }
            hVar.a(oVar2.g);
            hVar.b(oVar2.n);
            hVar.c(oVar2.p);
            hVar.a(oVar2.o);
            if (oVar2.j && !"".equals(oVar2.k)) {
                hVar.a(true);
            }
            StateListDrawable stateListDrawable = new StateListDrawable();
            if (i % 2 == 0) {
                stateListDrawable.addState(new int[]{16842919}, new ColorDrawable(-7158214));
                stateListDrawable.addState(new int[]{16842910}, new ColorDrawable(-1));
            } else {
                stateListDrawable.addState(new int[]{16842919}, new ColorDrawable(-7158214));
                stateListDrawable.addState(new int[]{16842910}, new ColorDrawable(-1381396));
            }
            hVar.setBackgroundDrawable(stateListDrawable);
            return hVar;
        } else if (this.d == 3) {
            f fVar = new f(this.a);
            fVar.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
            o oVar3 = (o) this.b.get(i);
            File file3 = new File(Environment.getExternalStorageDirectory(), ("download/ads/" + oVar3.h) + "/icon.png");
            if (file3.exists() && (b3 = c.b(this.a, file3.getAbsolutePath())) != null) {
                fVar.a(b3);
            }
            fVar.a(oVar3.g);
            fVar.b(oVar3.G);
            if (i >= 0 && i < 10) {
                fVar.a(5.0f);
            } else if (i < 10 || i >= 20) {
                fVar.a(4.0f);
            } else {
                fVar.a(4.5f);
            }
            if (oVar3.j && !"".equals(oVar3.k)) {
                fVar.a(true);
            }
            return fVar;
        } else {
            if (view instanceof ac) {
                ac acVar5 = (ac) view;
                acVar5.a();
                acVar5.a(false);
                acVar = acVar5;
            } else {
                ac acVar6 = new ac(this.a);
                acVar6.setLayoutParams(new AbsListView.LayoutParams(-2, -2));
                acVar = acVar6;
            }
            o oVar4 = (o) this.b.get(i);
            File file4 = new File(Environment.getExternalStorageDirectory(), ("download/ads/" + oVar4.h) + "/icon.png");
            if (file4.exists() && (b2 = c.b(this.a, file4.getAbsolutePath())) != null) {
                acVar.a(b2);
            }
            if (oVar4.j && !oVar4.k.equals("")) {
                acVar.a(true);
            }
            acVar.a(oVar4.g);
            return acVar;
        }
    }
}
