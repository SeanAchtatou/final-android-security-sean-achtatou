package com.kuguo.ad;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.kuguo.c.c;

class y extends Handler {
    final /* synthetic */ n a;

    y(n nVar) {
        this.a = nVar;
    }

    public void handleMessage(Message message) {
        Bundle data = message.getData();
        data.getInt("tag");
        String string = data.getString("filePath");
        if (string != null && c.a(this.a.a, string) != null) {
            this.a.notifyDataSetChanged();
        }
    }
}
