package com.kuguo.ad;

import android.content.Context;
import android.util.Log;
import com.kuguo.c.a;

class e implements Runnable {
    final /* synthetic */ Context a;
    final /* synthetic */ KuguoAdsManager b;

    e(KuguoAdsManager kuguoAdsManager, Context context) {
        this.b = kuguoAdsManager;
        this.a = context;
    }

    public void run() {
        Log.d("android__log", "b ---- show kuguo sprite !! ");
        Context applicationContext = this.a.getApplicationContext();
        if (this.b.showKuguoSide) {
            a.a("android__log", "show kuguo side");
            s unused = this.b.kuguoSide = s.a(applicationContext, 0);
        }
        if (this.b.showKuguoFree) {
            a.a("android__log", "show kuguo free");
            s unused2 = this.b.kuguoFree = s.a(applicationContext, 1);
        }
    }
}
