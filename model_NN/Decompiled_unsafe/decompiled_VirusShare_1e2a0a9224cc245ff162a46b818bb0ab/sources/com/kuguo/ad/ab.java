package com.kuguo.ad;

import android.content.Context;
import android.location.Location;
import android.os.Environment;
import android.util.Log;
import com.kuguo.c.a;
import java.io.IOException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;

public class ab implements Runnable {
    private Context a;
    private ap b;
    private int c = 4;

    public ab(Context context) {
        this.a = context;
        this.b = new ap(context);
    }

    private void b() {
        int i = 0;
        boolean z = false;
        while (!z && i < this.c) {
            String externalStorageState = Environment.getExternalStorageState();
            if ("mounted".equals(externalStorageState)) {
                z = d();
            } else {
                Log.d("android__log", "externalStorageState = " + externalStorageState);
            }
            i++;
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void c() {
        String k = a.k(this.a);
        if (k != null) {
            this.b.n = k;
        }
        this.b.l = r.d(this.a);
        this.b.m = 11;
        String e = a.e(this.b.n);
        if (e != null) {
            e = "-13," + a.s(this.a) + ";" + e;
        }
        a.a("android__log", "request statu is ------------ " + e);
        this.b.j = e;
        Location q = a.q(this.a);
        a.a("location : " + q);
        if (q != null) {
            this.b.f = q.getLongitude();
            this.b.e = q.getLatitude();
            this.b.s = a.a(q.getLongitude(), q.getLatitude());
        }
    }

    private boolean d() {
        boolean z;
        boolean z2;
        HttpPost httpPost = new HttpPost("http://www.cooguo.com/cooguogw/list.action");
        c();
        this.b.a();
        try {
            httpPost.setEntity(new ByteArrayEntity(this.b.b()));
        } catch (Exception e) {
        }
        try {
            HttpClient a2 = r.a(this.a);
            if (a2 == null) {
                return false;
            }
            int statusCode = a2.execute(httpPost).getStatusLine().getStatusCode();
            Log.d("android__log", "b-----------status: " + statusCode);
            if (statusCode == 200) {
                try {
                    a.d(this.b.n);
                    a.a("android__log", "clear status cooId is --------- " + this.b.n);
                    return true;
                } catch (ClientProtocolException e2) {
                    ClientProtocolException clientProtocolException = e2;
                    z = true;
                    e = clientProtocolException;
                } catch (IOException e3) {
                    IOException iOException = e3;
                    z2 = true;
                    e = iOException;
                    e.printStackTrace();
                    return z2;
                } catch (Exception e4) {
                    return true;
                }
            } else {
                httpPost.abort();
                return false;
            }
        } catch (ClientProtocolException e5) {
            e = e5;
            z = false;
            e.printStackTrace();
            return z;
        } catch (IOException e6) {
            e = e6;
            z2 = false;
            e.printStackTrace();
            return z2;
        } catch (Exception e7) {
            return false;
        }
    }

    public void a() {
        new Thread(this).start();
    }

    public void run() {
        b();
    }
}
