package com.kuguo.ad;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.kuguo.a.c;
import com.kuguo.a.d;
import com.kuguo.c.a;
import java.io.File;

class x implements c {
    private Context a;
    private o b;
    private Handler c;
    private int d = 0;

    public x(Context context, o oVar, Handler handler) {
        this.a = context;
        this.b = oVar;
        this.c = handler;
    }

    public void a(d dVar, int i) {
        File c2 = dVar.c();
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            default:
                return;
            case 4:
                if (c2.getName().endsWith(".apk")) {
                    if (dVar.c() != null) {
                        r.a(this.a, 17301634, "下载完成", this.b.h, 16, a.a(this.a, dVar.c(), this.b.w), this.b.g, -1);
                        this.b.l = 4;
                        a.b(this.a, this.b);
                        a.a(this.a, dVar.c().getPath(), this.b.i, this.b.w);
                        if (a.g(this.a, this.b.i) != null) {
                            a.a("android__log", "update ad status " + this.b.g + " id : " + this.b.h + " status : " + 4);
                            if (a.k(this.a) != null) {
                                a.a(a.k(this.a), this.b.h, 4);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                } else if (this.c != null) {
                    Message obtain = Message.obtain();
                    Bundle bundle = new Bundle();
                    bundle.putString("filePath", dVar.c().getPath());
                    bundle.putInt("tag", ((Integer) dVar.n()).intValue());
                    obtain.setData(bundle);
                    this.c.sendMessage(obtain);
                    return;
                } else {
                    return;
                }
            case 5:
                if (c2.getName().endsWith(".apk")) {
                    r.a(this.a, 17301624, dVar.l() == 1 ? "网络异常，下载暂停" : "网络异常，下载终止", this.b.h, 16, new Intent(), this.b.m, -1);
                    return;
                }
                return;
        }
    }

    public void a(d dVar, long j) {
        File c2 = dVar.c();
        if (c2 != null && c2.getName().endsWith(".apk")) {
            int i = (dVar.i() * 100) / dVar.h();
            if (i - this.d > 3) {
                this.d = i;
                r.a(this.a, 17301633, "正在下载...", this.b.h, 16, new Intent(), this.b.g, this.d);
            }
        }
    }
}
