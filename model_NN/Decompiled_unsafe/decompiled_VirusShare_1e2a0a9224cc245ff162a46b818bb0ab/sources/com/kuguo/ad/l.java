package com.kuguo.ad;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import com.kuguo.a.d;
import java.util.HashMap;
import java.util.Map;

public class l {
    View.OnClickListener a = new aa(this);
    /* access modifiers changed from: private */
    public int b = 0;
    /* access modifiers changed from: private */
    public o c;
    /* access modifiers changed from: private */
    public i d;
    /* access modifiers changed from: private */
    public Activity e;
    private x f;
    /* access modifiers changed from: private */
    public boolean g;
    private Handler h = new ad(this);

    public l(Activity activity, o oVar, boolean z) {
        this.e = activity;
        this.c = oVar;
        this.g = z;
        this.f = new x(activity, oVar, this.h);
        d dVar = new d(oVar.q, a.b(activity, "icon.png", oVar.h), 0);
        dVar.a((Object) -1);
        r.a(activity, dVar, this.f);
        String[] a2 = a.a(oVar.r, ";");
        if (a2 != null) {
            this.b = a2.length;
        }
        b();
        for (int i = 0; i < this.b; i++) {
            d dVar2 = new d(a2[i], a.b(activity, i + ".png", oVar.h), 0);
            dVar2.a(Integer.valueOf(i));
            r.a(this.e, dVar2, this.f);
        }
    }

    private Map a(String str) {
        HashMap hashMap = new HashMap();
        String[] a2 = a.a(str, "|");
        if (a2 != null) {
            for (String a3 : a2) {
                String[] a4 = a.a(a3, "=");
                if (a4 != null && a4.length == 2) {
                    hashMap.put(a4[0], a4[1]);
                }
            }
        }
        return hashMap;
    }

    private void b() {
        this.d = new i(this.e, this.c.D);
        this.d.a(this.b);
        this.d.d(this.c.b);
        this.d.a(this.c.m);
        this.d.e(this.c.G);
        this.d.a(a(this.c.x));
        this.d.a(this.a);
        switch (this.c.e) {
            case 0:
            case 2:
            case 4:
            case 9:
            case 10:
                this.d.a();
                return;
            case 1:
            case 3:
            case 5:
            case 7:
                this.d.b(this.c.n);
                this.d.c(this.c.p);
                this.d.a((float) this.c.o);
                return;
            case 6:
            case 8:
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public boolean c() {
        for (PackageInfo packageInfo : this.e.getPackageManager().getInstalledPackages(0)) {
            if (packageInfo.packageName.equals("com.qihoo360.mobilesafe")) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.e.setContentView(this.d, new LinearLayout.LayoutParams(-1, -1));
    }
}
