package com.kuguo.ad;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import com.kuguo.a.f;
import java.util.Calendar;

public class q {
    private static q b;
    /* access modifiers changed from: private */
    public Context a;
    private Handler c = new t(this);

    private q(Context context) {
        this.a = context;
    }

    protected static int a(int i) {
        switch (i) {
            case 0:
            default:
                return 17301586;
            case 1:
                return 17301646;
            case 2:
                return 17301647;
            case 3:
                return 17301547;
            case 4:
                return 17301651;
        }
    }

    public static q a(Context context) {
        if (b == null) {
            b = new q(context);
        }
        return b;
    }

    protected static void a(Context context, o oVar) {
        switch (oVar.e) {
            case 0:
            case 1:
            case 2:
            case 5:
            case 7:
            case 8:
            case 9:
            case 10:
                a.a(context, b(context, oVar), a(oVar.F), oVar, oVar.v == 1 ? 32 : a.a, true, false);
                return;
            case 3:
            case 4:
            case 6:
                MainActivity.a(context, oVar);
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    protected static Intent b(Context context, o oVar) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(268435456);
        intent.putExtra("message", oVar.a());
        intent.putExtra("sharedid", oVar.f);
        intent.putExtra("shortcut", false);
        intent.putExtra("applist", false);
        return intent;
    }

    private void b(o oVar) {
        Intent intent = new Intent(this.a, MainReceiver.class);
        intent.setExtrasClassLoader(oVar.getClass().getClassLoader());
        intent.putExtra("type", "AdAlarm");
        intent.putExtra("sharedid", oVar.f);
        intent.putExtra("message", oVar.a());
        PendingIntent broadcast = PendingIntent.getBroadcast(this.a, oVar.h, intent, 134217728);
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        ((AlarmManager) this.a.getSystemService("alarm")).set(0, instance.getTimeInMillis(), broadcast);
        a.a("have set the alarm!");
    }

    /* access modifiers changed from: protected */
    public void a() {
        a(a.d(this.a));
    }

    /* access modifiers changed from: protected */
    public void a(o oVar) {
        Message message = new Message();
        message.obj = oVar;
        this.c.sendMessage(message);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.io.InputStream r10) {
        /*
            r9 = this;
            r2 = 0
            r5 = 0
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0095, all -> 0x0087 }
            r0.<init>()     // Catch:{ IOException -> 0x0095, all -> 0x0087 }
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ IOException -> 0x0098, all -> 0x008a }
            r1.<init>(r10)     // Catch:{ IOException -> 0x0098, all -> 0x008a }
            r2 = 512(0x200, float:7.175E-43)
            byte[] r2 = new byte[r2]     // Catch:{ IOException -> 0x001f, all -> 0x008f }
        L_0x0010:
            int r3 = r1.read(r2)     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            r4 = -1
            if (r3 == r4) goto L_0x0036
            r4 = 0
            r0.write(r2, r4, r3)     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            r0.flush()     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            goto L_0x0010
        L_0x001f:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r0
            r0 = r8
        L_0x0024:
            java.lang.Exception r3 = new java.lang.Exception     // Catch:{ all -> 0x002a }
            r3.<init>(r0)     // Catch:{ all -> 0x002a }
            throw r3     // Catch:{ all -> 0x002a }
        L_0x002a:
            r0 = move-exception
        L_0x002b:
            if (r1 == 0) goto L_0x0030
            r1.close()
        L_0x0030:
            if (r2 == 0) goto L_0x0035
            r2.close()
        L_0x0035:
            throw r0
        L_0x0036:
            byte[] r2 = r0.toByteArray()     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            if (r2 == 0) goto L_0x003f
            int r3 = r2.length     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            if (r3 != 0) goto L_0x004a
        L_0x003f:
            if (r0 == 0) goto L_0x0044
            r0.close()
        L_0x0044:
            if (r1 == 0) goto L_0x0049
            r1.close()
        L_0x0049:
            return
        L_0x004a:
            com.kuguo.ad.o[] r2 = com.kuguo.ad.o.a(r2)     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            if (r2 != 0) goto L_0x005b
            if (r0 == 0) goto L_0x0055
            r0.close()
        L_0x0055:
            if (r1 == 0) goto L_0x0049
            r1.close()
            goto L_0x0049
        L_0x005b:
            int r3 = r2.length     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            r4 = r5
        L_0x005d:
            if (r4 >= r3) goto L_0x0079
            r5 = r2[r4]     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            android.content.Context r6 = r9.a     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            com.kuguo.ad.a.a(r6, r5)     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            int r6 = r5.A     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            r7 = 1
            if (r6 == r7) goto L_0x0076
            java.lang.String r6 = r5.H     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            com.kuguo.ad.a.c(r6)     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            int r5 = r5.C     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            long r5 = (long) r5     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            com.kuguo.ad.a.a(r5)     // Catch:{ IOException -> 0x001f, all -> 0x008f }
        L_0x0076:
            int r4 = r4 + 1
            goto L_0x005d
        L_0x0079:
            r9.a(r2)     // Catch:{ IOException -> 0x001f, all -> 0x008f }
            if (r0 == 0) goto L_0x0081
            r0.close()
        L_0x0081:
            if (r1 == 0) goto L_0x0049
            r1.close()
            goto L_0x0049
        L_0x0087:
            r0 = move-exception
            r1 = r2
            goto L_0x002b
        L_0x008a:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x002b
        L_0x008f:
            r2 = move-exception
            r8 = r2
            r2 = r1
            r1 = r0
            r0 = r8
            goto L_0x002b
        L_0x0095:
            r0 = move-exception
            r1 = r2
            goto L_0x0024
        L_0x0098:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kuguo.ad.q.a(java.io.InputStream):void");
    }

    /* access modifiers changed from: protected */
    public void a(o... oVarArr) {
        if (oVarArr != null) {
            f.a(this.a);
            for (o oVar : oVarArr) {
                if (oVar.h != -1) {
                    oVar.b();
                    if ((!a.c(this.a, oVar.i) || oVar.A == 1) && oVar.l.intValue() == -1) {
                        b(oVar);
                    }
                }
            }
        }
    }
}
