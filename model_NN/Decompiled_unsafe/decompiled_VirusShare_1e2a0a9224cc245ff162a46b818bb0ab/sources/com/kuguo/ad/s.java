package com.kuguo.ad;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.ImageView;
import com.kuguo.c.a;
import com.kuguo.c.d;

public class s extends ImageView {
    private static WindowManager g = null;
    private static int j = 1;
    private static int k = 0;
    private static s m;
    private static s n;
    private static Context q;
    private float a;
    private float b;
    private float c;
    private float d;
    private float e;
    private float f;
    private int h;
    private int i;
    private j l;
    private WindowManager.LayoutParams o;
    private int p;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public s(Context context, int i2) {
        super(context);
        int i3;
        int i4 = 72;
        this.h = 0;
        this.o = null;
        this.h = g.getDefaultDisplay().getWidth();
        this.i = i2;
        this.o = new WindowManager.LayoutParams();
        if (i2 == j) {
            Bitmap a2 = d.a(q, "kuguo_res/kuguo_top.png");
            i4 = a2.getWidth();
            int height = a2.getHeight();
            setImageBitmap(a2);
            i3 = height;
        } else if (i2 == k) {
            Bitmap a3 = d.a(q, "kuguo_res/kuguo_side.png");
            i4 = a3.getWidth();
            int height2 = a3.getHeight();
            setImageBitmap(a3);
            i3 = height2;
        } else {
            i3 = 72;
        }
        this.o.type = u.l;
        this.o.format = 1;
        this.o.flags = 40;
        this.o.gravity = 51;
        int width = g.getDefaultDisplay().getWidth();
        int height3 = g.getDefaultDisplay().getHeight();
        if (i2 == j) {
            this.o.x = width;
            this.o.y = height3 / 9;
        } else if (i2 == k) {
            this.o.x = 0;
            this.o.y = height3 / 2;
        }
        this.o.width = a.a(q, (int) (((float) i4) / 1.5f));
        this.o.height = a.a(q, (int) (((float) i3) / 1.5f));
        try {
            Class<?> cls = Class.forName("com.android.internal.R$dimen");
            this.p = q.getResources().getDimensionPixelSize(Integer.parseInt(cls.getField("status_bar_height").get(cls.newInstance()).toString()));
        } catch (Exception e2) {
            this.p = 0;
            e2.printStackTrace();
        }
        g.addView(this, this.o);
    }

    public static s a(Context context, int i2) {
        q = context;
        g = (WindowManager) q.getApplicationContext().getSystemService("window");
        try {
            if (i2 == j && m != null) {
                g.removeView(m);
            }
            if (i2 == k && n != null) {
                g.removeView(n);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (i2 == j) {
            m = new s(context, i2);
            return m;
        } else if (i2 != k) {
            return null;
        } else {
            n = new s(context, i2);
            return n;
        }
    }

    private void a() {
        a.a("android__log", "sprite be clicked !!!!!!!!!! ");
        if (this.l != null) {
            this.l.a();
            return;
        }
        Activity a2 = BoutiqueActivity.a();
        if (a2 != null) {
            a2.finish();
            return;
        }
        Intent intent = new Intent();
        intent.setClass(q, BoutiqueActivity.class);
        intent.setFlags(268435456);
        q.startActivity(intent);
        if (a.k(q) == null) {
            return;
        }
        if (this.i == j) {
            a.a(a.k(q), -11, 1);
        } else if (this.i == k) {
            a.a(a.k(q), -12, 1);
        }
    }

    private void b() {
        this.o.x = (int) (this.c - this.a);
        this.o.y = (int) (this.d - this.b);
        try {
            g.updateViewLayout(this, this.o);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void c() {
        Bitmap a2 = d.a(q, "kuguo_res/kuguo.png");
        setImageBitmap(a2);
        int width = a2.getWidth();
        int height = a2.getHeight();
        this.o.width = a.a(q, (int) (((float) width) / 1.5f));
        this.o.height = a.a(q, (int) (((float) height) / 1.5f));
        try {
            g.updateViewLayout(this, this.o);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void d() {
        Bitmap a2 = d.a(q, "kuguo_res/kuguo_top_pressed.png");
        setImageBitmap(a2);
        int width = a2.getWidth();
        int height = a2.getHeight();
        this.o.width = a.a(q, (int) (((float) width) / 1.5f));
        this.o.height = a.a(q, (int) (((float) height) / 1.5f));
        try {
            g.updateViewLayout(this, this.o);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void e() {
        Bitmap a2 = d.a(q, "kuguo_res/kuguo_side2.png");
        setImageBitmap(a2);
        setImageBitmap(a2);
        int width = a2.getWidth();
        int height = a2.getHeight();
        this.o.width = a.a(q, (int) (((float) width) / 1.5f));
        this.o.height = a.a(q, (int) (((float) height) / 1.5f));
        try {
            g.updateViewLayout(this, this.o);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void f() {
        Bitmap a2 = d.a(q, "kuguo_res/kuguo_side.png");
        setImageBitmap(a2);
        setImageBitmap(a2);
        int width = a2.getWidth();
        int height = a2.getHeight();
        this.o.width = a.a(q, (int) (((float) width) / 1.5f));
        this.o.height = a.a(q, (int) (((float) height) / 1.5f));
        try {
            g.updateViewLayout(this, this.o);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void g() {
        Bitmap a2 = d.a(q, "kuguo_res/kuguo_top.png");
        setImageBitmap(a2);
        setImageBitmap(a2);
        int width = a2.getWidth();
        int height = a2.getHeight();
        this.o.width = a.a(q, (int) (((float) width) / 1.5f));
        this.o.height = a.a(q, (int) (((float) height) / 1.5f));
        try {
            g.updateViewLayout(this, this.o);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.c = motionEvent.getRawX();
        this.d = motionEvent.getRawY() - ((float) this.p);
        switch (motionEvent.getAction()) {
            case 0:
                if (this.i == k) {
                    c();
                } else if (this.i == j) {
                    d();
                }
                this.a = motionEvent.getX();
                this.b = motionEvent.getY();
                this.e = this.c;
                this.f = this.d;
                this.h = g.getDefaultDisplay().getWidth();
                return true;
            case 1:
                if (this.i == k) {
                    if (this.c > ((float) (this.h / 2))) {
                        this.c = (float) this.h;
                        e();
                    } else {
                        this.c = 0.0f;
                        f();
                    }
                } else if (this.i == j) {
                    g();
                }
                b();
                float abs = Math.abs(this.c - this.e);
                float abs2 = Math.abs(this.d - this.f);
                if (abs < ((float) a.a(q, 40)) && abs2 < ((float) a.a(q, 13))) {
                    a();
                }
                this.b = 0.0f;
                this.a = 0.0f;
                return true;
            case 2:
                b();
                return true;
            default:
                return true;
        }
    }
}
