package com.kuguo.ad;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RemoteViews;
import com.tencent.lbsapi.core.e;
import com.tencent.mobwin.core.m;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class a {
    protected static int a = -99;
    private static final Pattern b = Pattern.compile("\\d+");

    protected static int a() {
        String[] list;
        int i = 0;
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return 0;
        }
        File file = new File(Environment.getExternalStorageDirectory(), "download/ads/");
        if (!file.exists() || (list = file.list()) == null) {
            return 0;
        }
        for (String valueOf : list) {
            try {
                if (Integer.valueOf(valueOf).intValue() != 121) {
                    i++;
                }
            } catch (Exception e) {
            }
        }
        return i;
    }

    public static int a(Context context, int i) {
        return (int) ((context.getResources().getDisplayMetrics().density * ((float) i)) + 0.5f);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x004a A[SYNTHETIC, Splitter:B:18:0x004a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static int a(android.content.Context r6, java.lang.String r7, int r8) {
        /*
            r0 = 0
            android.content.res.AssetManager r1 = r6.getAssets()     // Catch:{ Exception -> 0x0026, all -> 0x0044 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0026, all -> 0x0044 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0026, all -> 0x0044 }
            r4 = 3
            java.io.InputStream r1 = r1.open(r7, r4)     // Catch:{ Exception -> 0x0026, all -> 0x0044 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0026, all -> 0x0044 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0026, all -> 0x0044 }
            java.lang.String r0 = r2.readLine()     // Catch:{ Exception -> 0x005c, all -> 0x0054 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x005c, all -> 0x0054 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x005c, all -> 0x0054 }
            if (r2 == 0) goto L_0x0025
            r2.close()     // Catch:{ IOException -> 0x004e }
        L_0x0025:
            return r0
        L_0x0026:
            r1 = move-exception
        L_0x0027:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            r1.<init>()     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = " is not exist!"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0057 }
            a(r1)     // Catch:{ all -> 0x0057 }
            if (r0 == 0) goto L_0x0042
            r0.close()     // Catch:{ IOException -> 0x0050 }
        L_0x0042:
            r0 = r8
            goto L_0x0025
        L_0x0044:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0048:
            if (r1 == 0) goto L_0x004d
            r1.close()     // Catch:{ IOException -> 0x0052 }
        L_0x004d:
            throw r0
        L_0x004e:
            r1 = move-exception
            goto L_0x0025
        L_0x0050:
            r0 = move-exception
            goto L_0x0042
        L_0x0052:
            r1 = move-exception
            goto L_0x004d
        L_0x0054:
            r0 = move-exception
            r1 = r2
            goto L_0x0048
        L_0x0057:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0048
        L_0x005c:
            r0 = move-exception
            r0 = r2
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kuguo.ad.a.a(android.content.Context, java.lang.String, int):int");
    }

    protected static final int a(Context context, String str, String str2) {
        try {
            for (Class<?> cls : Class.forName(context.getPackageName() + ".R").getDeclaredClasses()) {
                if (str.equals(cls.getSimpleName())) {
                    return cls.getDeclaredField(str2).getInt(cls.newInstance());
                }
            }
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchFieldException | SecurityException e) {
        }
        return -1;
    }

    protected static Intent a(Context context, File file, int i) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        intent.addFlags(268435456);
        return intent;
    }

    protected static Bitmap a(Context context, String str, boolean z) {
        if (z) {
            try {
                return BitmapFactory.decodeStream(context.getAssets().open(str));
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                return BitmapFactory.decodeStream(new FileInputStream(str));
            } catch (FileNotFoundException e2) {
                e2.printStackTrace();
                return null;
            } catch (Exception e3) {
                e3.printStackTrace();
                return null;
            }
        }
    }

    protected static ImageView a(View view) {
        if (view instanceof ImageView) {
            return (ImageView) view;
        }
        if (view instanceof ViewGroup) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= ((ViewGroup) view).getChildCount()) {
                    break;
                }
                View childAt = ((ViewGroup) view).getChildAt(i2);
                if (childAt instanceof ImageView) {
                    return (ImageView) childAt;
                }
                if (childAt instanceof ViewGroup) {
                    return a(childAt);
                }
                i = i2 + 1;
            }
        }
        return null;
    }

    protected static o a(Context context, String str) {
        if (str == null || str.trim().equals("")) {
            return null;
        }
        o[] d = d(context);
        if (d == null || d.length <= 0) {
            return null;
        }
        for (o oVar : d) {
            if (str.equals(oVar.i)) {
                return oVar;
            }
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0050 A[SYNTHETIC, Splitter:B:14:0x0050] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055 A[SYNTHETIC, Splitter:B:17:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00d5 A[SYNTHETIC, Splitter:B:58:0x00d5] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00da A[SYNTHETIC, Splitter:B:61:0x00da] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00f2 A[SYNTHETIC, Splitter:B:72:0x00f2] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x00f7 A[SYNTHETIC, Splitter:B:75:0x00f7] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x010c A[SYNTHETIC, Splitter:B:84:0x010c] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0111 A[SYNTHETIC, Splitter:B:87:0x0111] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:55:0x00d0=Splitter:B:55:0x00d0, B:11:0x004b=Splitter:B:11:0x004b, B:69:0x00ed=Splitter:B:69:0x00ed} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.lang.String a(double r9, double r11) {
        /*
            r7 = 0
            r5 = 0
            java.lang.String r0 = "http://ugc.map.soso.com/rgeoc/?lnglat=%s,%s&reqsrc=wb"
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.Double r2 = java.lang.Double.valueOf(r9)
            r1[r5] = r2
            r2 = 1
            java.lang.Double r3 = java.lang.Double.valueOf(r11)
            r1[r2] = r3
            java.lang.String r0 = java.lang.String.format(r0, r1)
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r1]
            java.net.URL r2 = new java.net.URL     // Catch:{ IOException -> 0x0142, JSONException -> 0x00cd, Exception -> 0x00ea, all -> 0x0107 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0142, JSONException -> 0x00cd, Exception -> 0x00ea, all -> 0x0107 }
            java.net.URLConnection r9 = r2.openConnection()     // Catch:{ IOException -> 0x0142, JSONException -> 0x00cd, Exception -> 0x00ea, all -> 0x0107 }
            java.net.HttpURLConnection r9 = (java.net.HttpURLConnection) r9     // Catch:{ IOException -> 0x0142, JSONException -> 0x00cd, Exception -> 0x00ea, all -> 0x0107 }
            r0 = 10000(0x2710, float:1.4013E-41)
            r9.setReadTimeout(r0)     // Catch:{ IOException -> 0x0142, JSONException -> 0x00cd, Exception -> 0x00ea, all -> 0x0107 }
            r0 = 10000(0x2710, float:1.4013E-41)
            r9.setConnectTimeout(r0)     // Catch:{ IOException -> 0x0142, JSONException -> 0x00cd, Exception -> 0x00ea, all -> 0x0107 }
            java.io.InputStream r0 = r9.getInputStream()     // Catch:{ IOException -> 0x0142, JSONException -> 0x00cd, Exception -> 0x00ea, all -> 0x0107 }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0147, JSONException -> 0x0137, Exception -> 0x012c, all -> 0x011f }
            r2.<init>()     // Catch:{ IOException -> 0x0147, JSONException -> 0x0137, Exception -> 0x012c, all -> 0x011f }
        L_0x003a:
            int r3 = r0.read(r1)     // Catch:{ IOException -> 0x0046, JSONException -> 0x013c, Exception -> 0x0131, all -> 0x0124 }
            r4 = -1
            if (r3 == r4) goto L_0x005a
            r4 = 0
            r2.write(r1, r4, r3)     // Catch:{ IOException -> 0x0046, JSONException -> 0x013c, Exception -> 0x0131, all -> 0x0124 }
            goto L_0x003a
        L_0x0046:
            r1 = move-exception
            r8 = r1
            r1 = r2
            r2 = r0
            r0 = r8
        L_0x004b:
            r0.printStackTrace()     // Catch:{ all -> 0x012a }
            if (r2 == 0) goto L_0x0053
            r2.close()     // Catch:{ IOException -> 0x00c3 }
        L_0x0053:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ IOException -> 0x00c8 }
        L_0x0058:
            r0 = r7
        L_0x0059:
            return r0
        L_0x005a:
            java.lang.String r1 = new java.lang.String     // Catch:{ IOException -> 0x0046, JSONException -> 0x013c, Exception -> 0x0131, all -> 0x0124 }
            byte[] r3 = r2.toByteArray()     // Catch:{ IOException -> 0x0046, JSONException -> 0x013c, Exception -> 0x0131, all -> 0x0124 }
            java.lang.String r4 = "gbk"
            r1.<init>(r3, r4)     // Catch:{ IOException -> 0x0046, JSONException -> 0x013c, Exception -> 0x0131, all -> 0x0124 }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ IOException -> 0x0046, JSONException -> 0x013c, Exception -> 0x0131, all -> 0x0124 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x0046, JSONException -> 0x013c, Exception -> 0x0131, all -> 0x0124 }
            java.lang.String r1 = "detail"
            org.json.JSONObject r1 = r3.getJSONObject(r1)     // Catch:{ IOException -> 0x0046, JSONException -> 0x013c, Exception -> 0x0131, all -> 0x0124 }
            java.lang.String r3 = "results"
            org.json.JSONArray r1 = r1.getJSONArray(r3)     // Catch:{ IOException -> 0x0046, JSONException -> 0x013c, Exception -> 0x0131, all -> 0x0124 }
            r3 = r5
        L_0x0077:
            int r4 = r1.length()     // Catch:{ IOException -> 0x0046, JSONException -> 0x013c, Exception -> 0x0131, all -> 0x0124 }
            if (r3 >= r4) goto L_0x00ae
            org.json.JSONObject r4 = r1.getJSONObject(r3)     // Catch:{ IOException -> 0x0046, JSONException -> 0x013c, Exception -> 0x0131, all -> 0x0124 }
            java.lang.String r5 = "dtype"
            java.lang.String r5 = r4.getString(r5)     // Catch:{ IOException -> 0x0046, JSONException -> 0x013c, Exception -> 0x0131, all -> 0x0124 }
            java.lang.String r6 = "AD"
            boolean r5 = r6.equals(r5)     // Catch:{ IOException -> 0x0046, JSONException -> 0x013c, Exception -> 0x0131, all -> 0x0124 }
            if (r5 == 0) goto L_0x00ab
            java.lang.String r1 = "name"
            java.lang.String r1 = r4.getString(r1)     // Catch:{ IOException -> 0x0046, JSONException -> 0x013c, Exception -> 0x0131, all -> 0x0124 }
            if (r0 == 0) goto L_0x009a
            r0.close()     // Catch:{ IOException -> 0x00a1 }
        L_0x009a:
            if (r2 == 0) goto L_0x009f
            r2.close()     // Catch:{ IOException -> 0x00a6 }
        L_0x009f:
            r0 = r1
            goto L_0x0059
        L_0x00a1:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x009a
        L_0x00a6:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x009f
        L_0x00ab:
            int r3 = r3 + 1
            goto L_0x0077
        L_0x00ae:
            if (r0 == 0) goto L_0x00b3
            r0.close()     // Catch:{ IOException -> 0x00be }
        L_0x00b3:
            if (r2 == 0) goto L_0x0058
            r2.close()     // Catch:{ IOException -> 0x00b9 }
            goto L_0x0058
        L_0x00b9:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0058
        L_0x00be:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00b3
        L_0x00c3:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0053
        L_0x00c8:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0058
        L_0x00cd:
            r0 = move-exception
            r1 = r7
            r2 = r7
        L_0x00d0:
            r0.printStackTrace()     // Catch:{ all -> 0x012a }
            if (r2 == 0) goto L_0x00d8
            r2.close()     // Catch:{ IOException -> 0x00e5 }
        L_0x00d8:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ IOException -> 0x00df }
            goto L_0x0058
        L_0x00df:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0058
        L_0x00e5:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00d8
        L_0x00ea:
            r0 = move-exception
            r1 = r7
            r2 = r7
        L_0x00ed:
            r0.printStackTrace()     // Catch:{ all -> 0x012a }
            if (r2 == 0) goto L_0x00f5
            r2.close()     // Catch:{ IOException -> 0x0102 }
        L_0x00f5:
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ IOException -> 0x00fc }
            goto L_0x0058
        L_0x00fc:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0058
        L_0x0102:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00f5
        L_0x0107:
            r0 = move-exception
            r1 = r7
            r2 = r7
        L_0x010a:
            if (r2 == 0) goto L_0x010f
            r2.close()     // Catch:{ IOException -> 0x0115 }
        L_0x010f:
            if (r1 == 0) goto L_0x0114
            r1.close()     // Catch:{ IOException -> 0x011a }
        L_0x0114:
            throw r0
        L_0x0115:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x010f
        L_0x011a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0114
        L_0x011f:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r7
            goto L_0x010a
        L_0x0124:
            r1 = move-exception
            r8 = r1
            r1 = r2
            r2 = r0
            r0 = r8
            goto L_0x010a
        L_0x012a:
            r0 = move-exception
            goto L_0x010a
        L_0x012c:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r7
            goto L_0x00ed
        L_0x0131:
            r1 = move-exception
            r8 = r1
            r1 = r2
            r2 = r0
            r0 = r8
            goto L_0x00ed
        L_0x0137:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r7
            goto L_0x00d0
        L_0x013c:
            r1 = move-exception
            r8 = r1
            r1 = r2
            r2 = r0
            r0 = r8
            goto L_0x00d0
        L_0x0142:
            r0 = move-exception
            r1 = r7
            r2 = r7
            goto L_0x004b
        L_0x0147:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r7
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kuguo.ad.a.a(double, double):java.lang.String");
    }

    public static String a(Context context) {
        String subscriberId = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        a("imsi == " + subscriberId);
        return subscriberId;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0048 A[SYNTHETIC, Splitter:B:29:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x004d A[SYNTHETIC, Splitter:B:32:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0062 A[SYNTHETIC, Splitter:B:42:0x0062] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0067 A[SYNTHETIC, Splitter:B:45:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x008b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static java.lang.String a(java.io.File r5) {
        /*
            r3 = 0
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x000f
            r0 = r3
        L_0x000e:
            return r0
        L_0x000f:
            boolean r0 = r5.exists()
            if (r0 != 0) goto L_0x0017
            r0 = r3
            goto L_0x000e
        L_0x0017:
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0040, all -> 0x005d }
            r0.<init>(r5)     // Catch:{ IOException -> 0x0040, all -> 0x005d }
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ IOException -> 0x0081, all -> 0x0075 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0081, all -> 0x0075 }
            java.lang.String r2 = r1.readUTF()     // Catch:{ IOException -> 0x0086, all -> 0x007a }
            if (r1 == 0) goto L_0x002a
            r1.close()     // Catch:{ IOException -> 0x0035 }
        L_0x002a:
            if (r0 == 0) goto L_0x008d
            r0.close()     // Catch:{ IOException -> 0x003a }
            r0 = r2
        L_0x0030:
            java.lang.String r0 = g(r0)
            goto L_0x000e
        L_0x0035:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x002a
        L_0x003a:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x0030
        L_0x0040:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0043:
            r0.printStackTrace()     // Catch:{ all -> 0x007f }
            if (r1 == 0) goto L_0x004b
            r1.close()     // Catch:{ IOException -> 0x0052 }
        L_0x004b:
            if (r2 == 0) goto L_0x008b
            r2.close()     // Catch:{ IOException -> 0x0057 }
            r0 = r3
            goto L_0x0030
        L_0x0052:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004b
        L_0x0057:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r3
            goto L_0x0030
        L_0x005d:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0060:
            if (r1 == 0) goto L_0x0065
            r1.close()     // Catch:{ IOException -> 0x006b }
        L_0x0065:
            if (r2 == 0) goto L_0x006a
            r2.close()     // Catch:{ IOException -> 0x0070 }
        L_0x006a:
            throw r0
        L_0x006b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0065
        L_0x0070:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006a
        L_0x0075:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x0060
        L_0x007a:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x0060
        L_0x007f:
            r0 = move-exception
            goto L_0x0060
        L_0x0081:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x0043
        L_0x0086:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x0043
        L_0x008b:
            r0 = r3
            goto L_0x0030
        L_0x008d:
            r0 = r2
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kuguo.ad.a.a(java.io.File):java.lang.String");
    }

    protected static String a(String str, int i) {
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return null;
        }
        File file = new File(Environment.getExternalStorageDirectory(), "download/ads/" + i + "/" + str);
        if (file.exists()) {
            return file.getPath();
        }
        return null;
    }

    protected static void a(long j) {
        a("spaceTime ------------------------- " + j);
        b("download/ads/st.dat", String.valueOf(j));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kuguo.ad.a.a(android.content.Context, java.lang.String, boolean):android.graphics.Bitmap
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.kuguo.ad.a.a(android.content.Context, java.lang.String, int):int
      com.kuguo.ad.a.a(android.content.Context, java.lang.String, java.lang.String):int
      com.kuguo.ad.a.a(android.content.Context, java.io.File, int):android.content.Intent
      com.kuguo.ad.a.a(android.content.Context, int, int):boolean
      com.kuguo.ad.a.a(java.lang.String, int, int):boolean
      com.kuguo.ad.a.a(android.content.Context, java.lang.String, boolean):android.graphics.Bitmap */
    protected static void a(Context context, Intent intent, int i, o oVar, int i2, boolean z, boolean z2) {
        ImageView a2;
        int i3 = oVar.h + 10000;
        String str = oVar.I == 1 ? ((Object) context.getApplicationContext().getApplicationInfo().loadLabel(context.getPackageManager())) + "给您推荐：" + oVar.b : oVar.b;
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        if (z2) {
            notificationManager.cancel(i3);
            return;
        }
        Notification notification = new Notification(i, oVar.m, System.currentTimeMillis());
        PendingIntent activity = PendingIntent.getActivity(context, i3, intent, 134217728);
        int a3 = a(context, "layout", "kuguo_notify");
        int a4 = a(context, "id", "icon");
        int a5 = a(context, "id", "title");
        int a6 = a(context, "id", "body");
        int a7 = a(context, "id", "time");
        Bitmap a8 = a(context, a("icon.png", oVar.h), false);
        if (a3 == -1 || a6 == -1 || a4 == -1 || a5 == -1 || a7 == -1 || a8 == null) {
            notification.setLatestEventInfo(context, oVar.m, str, activity);
            try {
                View inflate = LayoutInflater.from(context).inflate(notification.contentView.getLayoutId(), (ViewGroup) null);
                if (!(inflate == null || (a2 = a(inflate)) == null || a8 == null)) {
                    notification.contentView.setImageViewBitmap(a2.getId(), a8);
                }
            } catch (Exception e) {
            }
        } else {
            notification.contentIntent = activity;
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), a3);
            notification.contentView = remoteViews;
            remoteViews.setImageViewBitmap(a4, a8);
            remoteViews.setTextViewText(a5, Html.fromHtml("<big>" + oVar.m + "</big>"));
            remoteViews.setTextViewText(a6, str);
            remoteViews.setTextViewText(a7, new SimpleDateFormat("H:mm").format(new Date()));
        }
        if (i2 != a) {
            notification.flags = i2;
        }
        if (z) {
            notification.defaults |= 4;
            notification.flags |= 1;
            notification.ledARGB = -16711936;
            notification.ledOnMS = m.a;
            notification.ledOffMS = u.j;
        }
        notificationManager.notify(i3, notification);
    }

    protected static void a(Context context, String str, int i, boolean z) {
        if ("mounted".equals(Environment.getExternalStorageState())) {
            new File(Environment.getExternalStorageDirectory(), "download/ads/" + i + "/" + str).delete();
        }
    }

    public static void a(Context context, String str, String str2, int i) {
        if (str != null && !"".equals(str) && str2 != null && !"".equals(str2)) {
            context.startActivity(a(context, new File(str), i));
        }
    }

    public static void a(Context context, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences("toggle_flag_pref", 0).edit();
        edit.putBoolean("showsprite", z);
        edit.commit();
    }

    protected static void a(String str) {
    }

    private static boolean a(Context context, int i, int i2) {
        if (i2 == -1) {
            return false;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences("request_prefs", 0);
        String string = sharedPreferences.getString("adsState", "");
        SharedPreferences.Editor edit = sharedPreferences.edit();
        if ("".equals(string)) {
            edit.putString("adsState", "" + i + "," + i2);
        } else {
            HashMap hashMap = new HashMap();
            String[] a2 = a(string, ";");
            if (a2 != null) {
                for (String a3 : a2) {
                    String[] a4 = a(a3, ",");
                    if (a4 != null && a4.length == 2) {
                        hashMap.put(a4[0], a4[1]);
                    }
                }
            }
            hashMap.put(String.valueOf(i), String.valueOf(i2));
            StringBuffer stringBuffer = null;
            for (String str : hashMap.keySet()) {
                if (stringBuffer == null) {
                    stringBuffer = new StringBuffer();
                } else {
                    stringBuffer.append(";");
                }
                StringBuffer stringBuffer2 = stringBuffer;
                stringBuffer2.append(str);
                stringBuffer2.append(",");
                stringBuffer2.append((String) hashMap.get(str));
                stringBuffer = stringBuffer2;
            }
            if (stringBuffer != null) {
                edit.putString("adsState", stringBuffer.toString());
            }
        }
        return edit.commit();
    }

    protected static boolean a(Context context, o oVar) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("message_prefs", 0);
        c(context, oVar.y);
        if (oVar.h == -1) {
            return false;
        }
        int i = 0;
        while (sharedPreferences.contains("" + i)) {
            i++;
        }
        oVar.f = i;
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("" + i, oVar.a());
        return edit.commit();
    }

    protected static synchronized boolean a(Context context, o[] oVarArr) {
        boolean z;
        synchronized (a.class) {
            com.kuguo.c.a.a("android__log", "save applist messages to shared prefer.....");
            SharedPreferences.Editor edit = context.getSharedPreferences("applist_message_prefs", 0).edit();
            edit.clear();
            if (edit.commit()) {
                int length = oVarArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        z = true;
                        break;
                    }
                    if (oVarArr[i].h != -1) {
                        oVarArr[i].f = i;
                        edit.putString("" + i, oVarArr[i].a());
                        if (!edit.commit()) {
                            z = false;
                            break;
                        }
                    }
                    i++;
                }
            } else {
                z = false;
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x007c A[SYNTHETIC, Splitter:B:45:0x007c] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0081 A[SYNTHETIC, Splitter:B:48:0x0081] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0094 A[SYNTHETIC, Splitter:B:57:0x0094] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0099 A[SYNTHETIC, Splitter:B:60:0x0099] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(java.io.File r7, java.lang.String r8) {
        /*
            r5 = 0
            r4 = 0
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0010
            r0 = r4
        L_0x000f:
            return r0
        L_0x0010:
            java.lang.String r0 = f(r8)
            java.io.File r1 = new java.io.File
            java.lang.String r2 = r7.getParent()
            r1.<init>(r2)
            boolean r2 = r1.exists()
            if (r2 != 0) goto L_0x004f
            r1.mkdirs()
        L_0x0026:
            r1 = 0
            r2 = 0
            boolean r3 = r7.createNewFile()     // Catch:{ IOException -> 0x0074, all -> 0x008f }
            if (r3 == 0) goto L_0x005e
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0074, all -> 0x008f }
            r1.<init>(r7)     // Catch:{ IOException -> 0x0074, all -> 0x008f }
            java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x00b2, all -> 0x00a7 }
            r2.<init>(r1)     // Catch:{ IOException -> 0x00b2, all -> 0x00a7 }
            r2.writeUTF(r0)     // Catch:{ IOException -> 0x00b6, all -> 0x00ab }
            r2.flush()     // Catch:{ IOException -> 0x00b6, all -> 0x00ab }
            r0 = 1
            if (r2 == 0) goto L_0x0044
            r2.close()     // Catch:{ IOException -> 0x0059 }
        L_0x0044:
            if (r1 == 0) goto L_0x000f
            r1.close()     // Catch:{ IOException -> 0x004a }
            goto L_0x000f
        L_0x004a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x000f
        L_0x004f:
            boolean r1 = r7.exists()
            if (r1 == 0) goto L_0x0026
            r7.delete()
            goto L_0x0026
        L_0x0059:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0044
        L_0x005e:
            if (r5 == 0) goto L_0x0063
            r2.close()     // Catch:{ IOException -> 0x006a }
        L_0x0063:
            if (r5 == 0) goto L_0x0068
            r1.close()     // Catch:{ IOException -> 0x006f }
        L_0x0068:
            r0 = r4
            goto L_0x000f
        L_0x006a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0063
        L_0x006f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0068
        L_0x0074:
            r0 = move-exception
            r1 = r5
            r2 = r5
        L_0x0077:
            r0.printStackTrace()     // Catch:{ all -> 0x00b0 }
            if (r1 == 0) goto L_0x007f
            r1.close()     // Catch:{ IOException -> 0x008a }
        L_0x007f:
            if (r2 == 0) goto L_0x0068
            r2.close()     // Catch:{ IOException -> 0x0085 }
            goto L_0x0068
        L_0x0085:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0068
        L_0x008a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x007f
        L_0x008f:
            r0 = move-exception
            r1 = r5
            r2 = r5
        L_0x0092:
            if (r1 == 0) goto L_0x0097
            r1.close()     // Catch:{ IOException -> 0x009d }
        L_0x0097:
            if (r2 == 0) goto L_0x009c
            r2.close()     // Catch:{ IOException -> 0x00a2 }
        L_0x009c:
            throw r0
        L_0x009d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0097
        L_0x00a2:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x009c
        L_0x00a7:
            r0 = move-exception
            r2 = r1
            r1 = r5
            goto L_0x0092
        L_0x00ab:
            r0 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x0092
        L_0x00b0:
            r0 = move-exception
            goto L_0x0092
        L_0x00b2:
            r0 = move-exception
            r2 = r1
            r1 = r5
            goto L_0x0077
        L_0x00b6:
            r0 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x0077
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kuguo.ad.a.a(java.io.File, java.lang.String):boolean");
    }

    protected static boolean a(String str, int i, int i2) {
        boolean z;
        String str2;
        if (i == -11 || i == -12) {
            b(str, i, i2);
        } else {
            File file = new File(Environment.getExternalStorageDirectory(), "download/ads/" + i + "/sct.dat");
            com.kuguo.c.a.a("android__log", "updateAppListStatus id : cooId : " + str + i + " status : " + i2 + " ,ct file : " + file.getAbsolutePath());
            File parentFile = file.getParentFile();
            if (!parentFile.exists()) {
                parentFile.mkdirs();
                b(str, i, i2);
                return a(file, str + "," + i + "," + i2 + ";");
            } else if (!file.exists()) {
                if (i2 != 2) {
                    return false;
                }
                b(str, i, i2);
                return a(file, str + "," + i + "," + i2 + ";");
            } else if (file.exists()) {
                String[] a2 = a(a(file), ";");
                if (a2 == null) {
                    b(str, i, i2);
                    return a(file, str + "," + i + "," + i2 + ";");
                } else if (a2.length > 0) {
                    boolean z2 = false;
                    StringBuffer stringBuffer = null;
                    for (String str3 : a2) {
                        if (stringBuffer == null) {
                            stringBuffer = new StringBuffer();
                        }
                        String[] a3 = a(str3, ",");
                        if (a3.length == 3) {
                            String str4 = a3[0] + "," + a3[1] + "," + a3[2] + ";";
                            if (!str.equals(a3[0]) || "".equals(a3[0]) || !String.valueOf(i).equals(a3[1])) {
                                z = z2;
                                str2 = str4;
                            } else if (i2 - Integer.valueOf(a3[2]).intValue() == 1) {
                                a3[2] = "" + i2;
                                str2 = a3[0] + "," + a3[1] + "," + a3[2] + ";";
                                b(str, i, i2);
                                if (i2 == 5) {
                                    str2 = "";
                                    z = true;
                                } else {
                                    z = true;
                                }
                            } else {
                                str2 = str4;
                                z = true;
                            }
                            stringBuffer.append(str2);
                            z2 = z;
                        }
                    }
                    if (!z2 && stringBuffer != null) {
                        b(str, i, i2);
                        stringBuffer.append(str + "," + i + "," + i2 + ";");
                    }
                    return a(file, stringBuffer.toString());
                }
            }
        }
        return false;
    }

    protected static String[] a(String str, String str2) {
        if (str == null || str.equals("") || str2 == null || str2.equals("")) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (true) {
            int indexOf = str.indexOf(str2, i);
            if (indexOf == -1) {
                break;
            }
            arrayList.add(str.substring(i, indexOf));
            i = str2.length() + indexOf;
        }
        if (str.length() > i) {
            arrayList.add(str.substring(i));
        }
        String[] strArr = new String[arrayList.size()];
        ListIterator listIterator = arrayList.listIterator();
        int i2 = 0;
        while (listIterator.hasNext()) {
            strArr[i2] = (String) listIterator.next();
            i2++;
        }
        return strArr;
    }

    protected static int b(Context context) {
        int a2 = a(context, "kuguo_res/p.txt", 100061);
        a("projectId == " + a2);
        return a2;
    }

    protected static long b() {
        long j;
        try {
            j = Long.valueOf(b("download/ads/st.dat")).longValue();
        } catch (Exception e) {
            j = -1;
        }
        if (j == -1) {
            j = 60;
        }
        a("spaceTime ----------getSpaceTime-------------- " + j);
        return j;
    }

    protected static o b(Context context, String str) {
        if (str == null || str.trim().equals("")) {
            return null;
        }
        o[] d = d(context);
        if (d == null || d.length <= 0) {
            return null;
        }
        for (o oVar : d) {
            if (str.equals(oVar.d)) {
                return oVar;
            }
        }
        return null;
    }

    protected static File b(Context context, String str, int i) {
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return null;
        }
        String str2 = "download/ads/" + i;
        File file = new File(Environment.getExternalStorageDirectory(), str2);
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(Environment.getExternalStorageDirectory(), str2 + "/" + str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x004d A[SYNTHETIC, Splitter:B:28:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0052 A[SYNTHETIC, Splitter:B:31:0x0052] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0067 A[SYNTHETIC, Splitter:B:41:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x006c A[SYNTHETIC, Splitter:B:44:0x006c] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0090  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static java.lang.String b(java.lang.String r5) {
        /*
            r3 = 0
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x000f
            r0 = r3
        L_0x000e:
            return r0
        L_0x000f:
            java.io.File r0 = new java.io.File
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()
            r0.<init>(r1, r5)
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x0020
            r0 = r3
            goto L_0x000e
        L_0x0020:
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0045, all -> 0x0062 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0045, all -> 0x0062 }
            java.io.DataInputStream r0 = new java.io.DataInputStream     // Catch:{ IOException -> 0x0086, all -> 0x007a }
            r0.<init>(r1)     // Catch:{ IOException -> 0x0086, all -> 0x007a }
            java.lang.String r2 = r0.readUTF()     // Catch:{ IOException -> 0x008a, all -> 0x007e }
            if (r0 == 0) goto L_0x0033
            r0.close()     // Catch:{ IOException -> 0x003a }
        L_0x0033:
            if (r1 == 0) goto L_0x0093
            r1.close()     // Catch:{ IOException -> 0x003f }
            r0 = r2
            goto L_0x000e
        L_0x003a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0033
        L_0x003f:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x000e
        L_0x0045:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0048:
            r0.printStackTrace()     // Catch:{ all -> 0x0084 }
            if (r1 == 0) goto L_0x0050
            r1.close()     // Catch:{ IOException -> 0x0057 }
        L_0x0050:
            if (r2 == 0) goto L_0x0090
            r2.close()     // Catch:{ IOException -> 0x005c }
            r0 = r3
            goto L_0x000e
        L_0x0057:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0050
        L_0x005c:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r3
            goto L_0x000e
        L_0x0062:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0065:
            if (r1 == 0) goto L_0x006a
            r1.close()     // Catch:{ IOException -> 0x0070 }
        L_0x006a:
            if (r2 == 0) goto L_0x006f
            r2.close()     // Catch:{ IOException -> 0x0075 }
        L_0x006f:
            throw r0
        L_0x0070:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006a
        L_0x0075:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006f
        L_0x007a:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0065
        L_0x007e:
            r2 = move-exception
            r4 = r2
            r2 = r1
            r1 = r0
            r0 = r4
            goto L_0x0065
        L_0x0084:
            r0 = move-exception
            goto L_0x0065
        L_0x0086:
            r0 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0048
        L_0x008a:
            r2 = move-exception
            r4 = r2
            r2 = r1
            r1 = r0
            r0 = r4
            goto L_0x0048
        L_0x0090:
            r0 = r3
            goto L_0x000e
        L_0x0093:
            r0 = r2
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kuguo.ad.a.b(java.lang.String):java.lang.String");
    }

    protected static void b(long j) {
        b("download/ads/rt.dat", String.valueOf(j));
        a("spaceTime ------------------------- " + j);
    }

    public static void b(Context context, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences("toggle_flag_pref", 0).edit();
        edit.putInt("list_style", i);
        edit.commit();
    }

    protected static boolean b(Context context, o oVar) {
        o[] d;
        if (oVar == null) {
            return false;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences("message_prefs", 0);
        String str = "" + oVar.f;
        if (sharedPreferences.contains(str) && (d = d(context)) != null) {
            int length = d.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                o oVar2 = d[i];
                if (oVar2.f != oVar.f) {
                    i++;
                } else if (oVar2.l.intValue() == 3) {
                    return false;
                } else {
                    if (oVar2.l.intValue() < oVar.l.intValue() || (oVar.l.intValue() == 3 && oVar2.l.intValue() == 4)) {
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.remove(str);
                        edit.putString(str, oVar.a());
                        a(context, oVar.h, oVar.l.intValue());
                        return edit.commit();
                    }
                }
            }
        }
        return false;
    }

    protected static boolean b(String str, int i, int i2) {
        File file = new File(Environment.getExternalStorageDirectory(), "download/ads//asct.dat");
        com.kuguo.c.a.a("android__log", "updateAllAppListStatu id : " + i + " status : " + "act file " + file.getAbsolutePath());
        File parentFile = file.getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs();
        } else if (!file.exists()) {
            return a(file, str + "," + i + "," + i2 + ";");
        } else {
            String a2 = a(file);
            if (a2 == null || "".equals(a2)) {
                return a(file, str + "," + i + "," + i2 + ";");
            }
            String[] a3 = a(a2, ";");
            if (a3 != null) {
                boolean z = false;
                StringBuffer stringBuffer = null;
                for (String str2 : a3) {
                    if (stringBuffer == null) {
                        stringBuffer = new StringBuffer();
                    }
                    String[] a4 = a(str2, ",");
                    if (a4 != null && a4.length == 3) {
                        if (str.equals(a4[0]) && a4[1].equals(String.valueOf(i))) {
                            if (Integer.valueOf(a4[1]).intValue() == -11 || Integer.valueOf(a4[1]).intValue() == -12) {
                                int intValue = Integer.valueOf(a4[2]).intValue() + 1;
                                com.kuguo.c.a.a("android__log", "-11 or -12 value is " + intValue);
                                a4[2] = "" + intValue;
                                z = true;
                            } else {
                                if (i2 - Integer.valueOf(a4[2]).intValue() == 1) {
                                    a4[2] = "" + i2;
                                }
                                z = true;
                            }
                        }
                        stringBuffer.append(a4[0] + "," + a4[1] + "," + a4[2] + ";");
                    }
                }
                if (!z) {
                    stringBuffer.append(str + "," + i + "," + i2 + ";");
                }
                return a(file, stringBuffer.toString());
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x0082 A[SYNTHETIC, Splitter:B:46:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0087 A[SYNTHETIC, Splitter:B:49:0x0087] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x009a A[SYNTHETIC, Splitter:B:58:0x009a] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x009f A[SYNTHETIC, Splitter:B:61:0x009f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean b(java.lang.String r7, java.lang.String r8) {
        /*
            r5 = 0
            r4 = 0
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0010
            r0 = r4
        L_0x000f:
            return r0
        L_0x0010:
            java.io.File r0 = new java.io.File
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()
            r0.<init>(r1, r7)
            java.io.File r1 = new java.io.File
            java.lang.String r2 = r0.getParent()
            r1.<init>(r2)
            boolean r2 = r1.exists()
            if (r2 != 0) goto L_0x0050
            r1.mkdirs()
        L_0x002b:
            r1 = 0
            r2 = 0
            boolean r3 = r0.createNewFile()     // Catch:{ IOException -> 0x007a, all -> 0x0095 }
            if (r3 == 0) goto L_0x0064
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x007a, all -> 0x0095 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x007a, all -> 0x0095 }
            java.io.DataOutputStream r0 = new java.io.DataOutputStream     // Catch:{ IOException -> 0x00b9, all -> 0x00ad }
            r0.<init>(r1)     // Catch:{ IOException -> 0x00b9, all -> 0x00ad }
            r0.writeUTF(r8)     // Catch:{ IOException -> 0x00bd, all -> 0x00b1 }
            r0.flush()     // Catch:{ IOException -> 0x00bd, all -> 0x00b1 }
            r2 = 1
            if (r0 == 0) goto L_0x0049
            r0.close()     // Catch:{ IOException -> 0x005a }
        L_0x0049:
            if (r1 == 0) goto L_0x004e
            r1.close()     // Catch:{ IOException -> 0x005f }
        L_0x004e:
            r0 = r2
            goto L_0x000f
        L_0x0050:
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x002b
            r0.delete()
            goto L_0x002b
        L_0x005a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0049
        L_0x005f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004e
        L_0x0064:
            if (r5 == 0) goto L_0x0069
            r2.close()     // Catch:{ IOException -> 0x0070 }
        L_0x0069:
            if (r5 == 0) goto L_0x006e
            r1.close()     // Catch:{ IOException -> 0x0075 }
        L_0x006e:
            r0 = r4
            goto L_0x000f
        L_0x0070:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0069
        L_0x0075:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x006e
        L_0x007a:
            r0 = move-exception
            r1 = r5
            r2 = r5
        L_0x007d:
            r0.printStackTrace()     // Catch:{ all -> 0x00b7 }
            if (r1 == 0) goto L_0x0085
            r1.close()     // Catch:{ IOException -> 0x0090 }
        L_0x0085:
            if (r2 == 0) goto L_0x006e
            r2.close()     // Catch:{ IOException -> 0x008b }
            goto L_0x006e
        L_0x008b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x006e
        L_0x0090:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0085
        L_0x0095:
            r0 = move-exception
            r1 = r5
            r2 = r5
        L_0x0098:
            if (r1 == 0) goto L_0x009d
            r1.close()     // Catch:{ IOException -> 0x00a3 }
        L_0x009d:
            if (r2 == 0) goto L_0x00a2
            r2.close()     // Catch:{ IOException -> 0x00a8 }
        L_0x00a2:
            throw r0
        L_0x00a3:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x009d
        L_0x00a8:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00a2
        L_0x00ad:
            r0 = move-exception
            r2 = r1
            r1 = r5
            goto L_0x0098
        L_0x00b1:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r0
            r0 = r6
            goto L_0x0098
        L_0x00b7:
            r0 = move-exception
            goto L_0x0098
        L_0x00b9:
            r0 = move-exception
            r2 = r1
            r1 = r5
            goto L_0x007d
        L_0x00bd:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r0
            r0 = r6
            goto L_0x007d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kuguo.ad.a.b(java.lang.String, java.lang.String):boolean");
    }

    protected static int c(Context context) {
        int a2 = a(context, "kuguo_res/m.txt", 1);
        Log.d("android__log", "-------startMode == " + a2);
        return a2;
    }

    protected static long c() {
        long j;
        try {
            j = Long.valueOf(b("download/ads/rt.dat")).longValue();
        } catch (Exception e) {
            j = -1;
        }
        if (j == -1) {
            j = 0;
        }
        a("requestTime ----------getRequestTime-------------- " + j);
        return j;
    }

    private static boolean c(Context context, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences("request_prefs", 0).edit();
        edit.putInt("nextRequest", i);
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        int i2 = instance.get(1);
        int i3 = instance.get(2);
        edit.putString("date", i2 + "-" + i3 + "-" + instance.get(5));
        return edit.commit();
    }

    protected static boolean c(Context context, String str) {
        try {
            context.getPackageManager().getPackageInfo(str.trim(), 256);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    protected static boolean c(String str) {
        if (str == null || "".equals(str)) {
            return false;
        }
        a("ids--- " + str);
        return b("download/ads/verify.dat", str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0042 A[SYNTHETIC, Splitter:B:18:0x0042] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0047 A[SYNTHETIC, Splitter:B:21:0x0047] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0089 A[SYNTHETIC, Splitter:B:43:0x0089] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x008e A[SYNTHETIC, Splitter:B:46:0x008e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static java.lang.String d() {
        /*
            r6 = 4
            r5 = 0
            java.io.File r0 = new java.io.File
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r2 = "/.android_"
            r0.<init>(r1, r2)
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x0019
            boolean r1 = r0.isFile()
            if (r1 == 0) goto L_0x001b
        L_0x0019:
            r0 = r5
        L_0x001a:
            return r0
        L_0x001b:
            java.io.File r1 = new java.io.File
            java.lang.String r2 = "b"
            r1.<init>(r0, r2)
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r0]
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00a1, all -> 0x0084 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x00a1, all -> 0x0084 }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x00a5, all -> 0x009c }
            r1.<init>()     // Catch:{ Exception -> 0x00a5, all -> 0x009c }
        L_0x0030:
            int r3 = r2.read(r0)     // Catch:{ Exception -> 0x003c }
            r4 = -1
            if (r3 == r4) goto L_0x004c
            r4 = 0
            r1.write(r0, r4, r3)     // Catch:{ Exception -> 0x003c }
            goto L_0x0030
        L_0x003c:
            r0 = move-exception
        L_0x003d:
            r0.printStackTrace()     // Catch:{ all -> 0x009f }
            if (r2 == 0) goto L_0x0045
            r2.close()     // Catch:{ IOException -> 0x007a }
        L_0x0045:
            if (r1 == 0) goto L_0x004a
            r1.close()     // Catch:{ IOException -> 0x007f }
        L_0x004a:
            r0 = r5
            goto L_0x001a
        L_0x004c:
            java.lang.String r0 = new java.lang.String     // Catch:{ Exception -> 0x003c }
            byte[] r3 = r1.toByteArray()     // Catch:{ Exception -> 0x003c }
            java.lang.String r4 = "utf-8"
            r0.<init>(r3, r4)     // Catch:{ Exception -> 0x003c }
            java.lang.String r0 = g(r0)     // Catch:{ Exception -> 0x003c }
            r3 = 4
            int r4 = r0.length()     // Catch:{ Exception -> 0x003c }
            int r4 = r4 - r6
            java.lang.String r0 = r0.substring(r3, r4)     // Catch:{ Exception -> 0x003c }
            if (r2 == 0) goto L_0x006a
            r2.close()     // Catch:{ IOException -> 0x0075 }
        L_0x006a:
            if (r1 == 0) goto L_0x001a
            r1.close()     // Catch:{ IOException -> 0x0070 }
            goto L_0x001a
        L_0x0070:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x001a
        L_0x0075:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x006a
        L_0x007a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0045
        L_0x007f:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004a
        L_0x0084:
            r0 = move-exception
            r1 = r5
            r2 = r5
        L_0x0087:
            if (r2 == 0) goto L_0x008c
            r2.close()     // Catch:{ IOException -> 0x0092 }
        L_0x008c:
            if (r1 == 0) goto L_0x0091
            r1.close()     // Catch:{ IOException -> 0x0097 }
        L_0x0091:
            throw r0
        L_0x0092:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x008c
        L_0x0097:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0091
        L_0x009c:
            r0 = move-exception
            r1 = r5
            goto L_0x0087
        L_0x009f:
            r0 = move-exception
            goto L_0x0087
        L_0x00a1:
            r0 = move-exception
            r1 = r5
            r2 = r5
            goto L_0x003d
        L_0x00a5:
            r0 = move-exception
            r1 = r5
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kuguo.ad.a.d():java.lang.String");
    }

    protected static boolean d(Context context, String str) {
        Intent e = e(context, str);
        if (e == null) {
            return false;
        }
        context.startActivity(e);
        return true;
    }

    protected static boolean d(String str) {
        StringBuffer stringBuffer;
        String[] a2;
        File file = new File(Environment.getExternalStorageDirectory(), "download/ads//asct.dat");
        if (file.exists()) {
            String[] a3 = a(a(file), ";");
            if (a3 == null || a3.length <= 0) {
                stringBuffer = null;
            } else {
                StringBuffer stringBuffer2 = new StringBuffer();
                for (String str2 : a3) {
                    if (!"".equals(str2) && str2 != null && (a2 = a(str2, ",")) != null && a2.length == 3 && !str.equals(a2[0])) {
                        stringBuffer2.append(a2[0] + "," + a2[1] + "," + a2[2] + ";");
                    }
                }
                stringBuffer = stringBuffer2;
            }
            if (stringBuffer != null) {
                return a(file, stringBuffer.toString());
            }
        }
        return false;
    }

    protected static o[] d(Context context) {
        int i = 0;
        SharedPreferences sharedPreferences = context.getSharedPreferences("message_prefs", 0);
        Map<String, ?> all = sharedPreferences.getAll();
        if (all == null || all.size() <= 0) {
            return null;
        }
        o[] oVarArr = new o[all.size()];
        for (String next : all.keySet()) {
            String string = sharedPreferences.getString(next, null);
            o oVar = new o();
            if (oVar.a(string)) {
                oVar.f = Integer.valueOf(next).intValue();
                oVarArr[i] = oVar;
                i++;
            } else {
                sharedPreferences.edit().clear().commit();
                return null;
            }
        }
        return oVarArr;
    }

    protected static int e(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("request_prefs", 0);
        if (sharedPreferences.getInt("nextRequest", 1) == 1) {
            return 0;
        }
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        String str = instance.get(1) + "-" + instance.get(2) + "-" + instance.get(5);
        if (!str.equals(sharedPreferences.getString("date", str))) {
            return 1;
        }
        return sharedPreferences.getString("adsState", null) != null ? 2 : -1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.content.Intent e(android.content.Context r5, java.lang.String r6) {
        /*
            r3 = 0
            r4 = 0
            if (r6 == 0) goto L_0x0010
            java.lang.String r0 = r6.trim()
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0012
        L_0x0010:
            r0 = r4
        L_0x0011:
            return r0
        L_0x0012:
            android.content.pm.PackageManager r0 = r5.getPackageManager()
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = "android.intent.action.MAIN"
            r1.<init>(r2, r4)
            java.lang.String r2 = "android.intent.category.LAUNCHER"
            r1.addCategory(r2)
            java.util.List r0 = r0.queryIntentActivities(r1, r3)
            if (r0 == 0) goto L_0x005e
            int r1 = r0.size()
            r2 = r3
        L_0x002d:
            if (r2 >= r1) goto L_0x005e
            java.lang.Object r5 = r0.get(r2)
            android.content.pm.ResolveInfo r5 = (android.content.pm.ResolveInfo) r5
            android.content.pm.ActivityInfo r3 = r5.activityInfo
            java.lang.String r3 = r3.packageName
            boolean r3 = r6.equals(r3)
            if (r3 == 0) goto L_0x0044
            r0 = r5
        L_0x0040:
            if (r0 != 0) goto L_0x0047
            r0 = r4
            goto L_0x0011
        L_0x0044:
            int r2 = r2 + 1
            goto L_0x002d
        L_0x0047:
            android.content.ComponentName r1 = new android.content.ComponentName
            android.content.pm.ActivityInfo r0 = r0.activityInfo
            java.lang.String r0 = r0.name
            r1.<init>(r6, r0)
            android.content.Intent r0 = new android.content.Intent
            r0.<init>()
            r0.setComponent(r1)
            r1 = 268435456(0x10000000, float:2.5243549E-29)
            r0.setFlags(r1)
            goto L_0x0011
        L_0x005e:
            r0 = r4
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kuguo.ad.a.e(android.content.Context, java.lang.String):android.content.Intent");
    }

    protected static String e(String str) {
        StringBuffer stringBuffer;
        File file = new File(Environment.getExternalStorageDirectory(), "download/ads//asct.dat");
        if (file.exists()) {
            String[] a2 = a(a(file), ";");
            if (a2 == null || a2.length <= 0) {
                file.delete();
                stringBuffer = null;
            } else {
                stringBuffer = new StringBuffer();
                for (String str2 : a2) {
                    if (!"".equals(str2) && str2 != null) {
                        String[] a3 = a(str2, ",");
                        if (!"".equals(a3) && a3.length == 3 && str.equals(a3[0])) {
                            stringBuffer.append(a3[1] + "," + a3[2] + ";");
                        }
                    }
                }
            }
            if (stringBuffer != null && !"".equals(stringBuffer.toString())) {
                return stringBuffer.toString();
            }
        }
        return null;
    }

    protected static String f(String str) {
        try {
            byte[] bytes = str.getBytes(e.e);
            byte[] bytes2 = "www.cooguo.com".getBytes();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append("%" + ((bytes[i] & 255) + (bytes2[i % bytes2.length] & 255)));
            }
            return sb.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return str;
        }
    }

    protected static boolean f(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("request_prefs", 0).edit();
        edit.remove("adsState");
        return edit.commit();
    }

    protected static boolean f(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences("coo_id", 0).edit();
        edit.putString("cooId", str);
        return edit.commit();
    }

    protected static o g(Context context, String str) {
        if (str == null || str.trim().equals("")) {
            return null;
        }
        o[] m = m(context);
        if (m == null || m.length <= 0) {
            return null;
        }
        for (o oVar : m) {
            if (str.equals(oVar.i)) {
                return oVar;
            }
        }
        return null;
    }

    protected static String g(String str) {
        if (str == null || str.length() == 0) {
            return str;
        }
        Matcher matcher = b.matcher(str);
        ArrayList arrayList = new ArrayList();
        while (matcher.find()) {
            try {
                arrayList.add(Integer.valueOf(matcher.group()));
            } catch (Exception e) {
                e.printStackTrace();
                return str;
            }
        }
        if (arrayList.size() <= 0) {
            return str;
        }
        try {
            byte[] bArr = new byte[arrayList.size()];
            byte[] bytes = "www.cooguo.com".getBytes();
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = (byte) (((Integer) arrayList.get(i)).intValue() - (bytes[i % bytes.length] & 255));
            }
            return new String(bArr, e.e);
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return str;
        }
    }

    protected static void g(Context context) {
        o[] d = d(context);
        if (d != null) {
            SharedPreferences.Editor edit = context.getSharedPreferences("message_prefs", 0).edit();
            for (o oVar : d) {
                String str = "" + oVar.f;
                if (oVar.c + 864000000 < System.currentTimeMillis()) {
                    edit.remove(str);
                } else {
                    switch (oVar.e) {
                        case 0:
                        case 2:
                        case 4:
                        case 6:
                        case 8:
                        case 9:
                        case 10:
                            if (oVar.l.intValue() == 2) {
                                edit.remove(str);
                                break;
                            } else {
                                continue;
                            }
                        case 1:
                        case 3:
                            if (oVar.l.intValue() == 3) {
                                edit.remove(str);
                                break;
                            } else {
                                continue;
                            }
                    }
                }
            }
            edit.commit();
        }
    }

    protected static String h(Context context) {
        String string = context.getSharedPreferences("request_prefs", 0).getString("adsState", null);
        a("adsState == " + string);
        return string;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0070 A[SYNTHETIC, Splitter:B:24:0x0070] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007f A[SYNTHETIC, Splitter:B:31:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:46:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void h(java.lang.String r4) {
        /*
            if (r4 == 0) goto L_0x0008
            int r0 = r4.length()
            if (r0 != 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            java.io.File r0 = new java.io.File
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r2 = "/.android_"
            r0.<init>(r1, r2)
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x0020
            boolean r1 = r0.isFile()
            if (r1 == 0) goto L_0x0026
        L_0x0020:
            boolean r1 = r0.mkdir()
            if (r1 == 0) goto L_0x0008
        L_0x0026:
            java.io.File r1 = new java.io.File
            java.lang.String r2 = "b"
            r1.<init>(r0, r2)
            r0 = 0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0067, all -> 0x0079 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0067, all -> 0x0079 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008d, all -> 0x0088 }
            r0.<init>()     // Catch:{ Exception -> 0x008d, all -> 0x0088 }
            java.lang.String r1 = "9527"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x008d, all -> 0x0088 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ Exception -> 0x008d, all -> 0x0088 }
            java.lang.String r1 = "2012"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x008d, all -> 0x0088 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x008d, all -> 0x0088 }
            java.lang.String r0 = f(r0)     // Catch:{ Exception -> 0x008d, all -> 0x0088 }
            java.lang.String r1 = "utf-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ Exception -> 0x008d, all -> 0x0088 }
            r2.write(r0)     // Catch:{ Exception -> 0x008d, all -> 0x0088 }
            r2.flush()     // Catch:{ Exception -> 0x008d, all -> 0x0088 }
            if (r2 == 0) goto L_0x0008
            r2.close()     // Catch:{ IOException -> 0x0062 }
            goto L_0x0008
        L_0x0062:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0008
        L_0x0067:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x006b:
            r0.printStackTrace()     // Catch:{ all -> 0x008b }
            if (r1 == 0) goto L_0x0008
            r1.close()     // Catch:{ IOException -> 0x0074 }
            goto L_0x0008
        L_0x0074:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0008
        L_0x0079:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x007d:
            if (r1 == 0) goto L_0x0082
            r1.close()     // Catch:{ IOException -> 0x0083 }
        L_0x0082:
            throw r0
        L_0x0083:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0082
        L_0x0088:
            r0 = move-exception
            r1 = r2
            goto L_0x007d
        L_0x008b:
            r0 = move-exception
            goto L_0x007d
        L_0x008d:
            r0 = move-exception
            r1 = r2
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kuguo.ad.a.h(java.lang.String):void");
    }

    public static boolean i(Context context) {
        return (context.getApplicationInfo().flags & 1) > 0;
    }

    protected static String j(Context context) {
        StringBuffer stringBuffer = null;
        String b2 = b("download/ads/verify.dat");
        if (b2 == null || "".equals(b2)) {
            return null;
        }
        a("ids--- " + b2);
        String[] a2 = a(b2, ";");
        if (a2 == null) {
            return null;
        }
        for (String a3 : a2) {
            String[] a4 = a(a3, ",");
            if (a4 != null && a4.length == 2 && c(context, a4[1])) {
                if (stringBuffer == null) {
                    stringBuffer = new StringBuffer(a4[0]);
                } else {
                    stringBuffer.append(",");
                    stringBuffer.append(a4[0]);
                }
            }
        }
        return stringBuffer != null ? stringBuffer.toString() : "";
    }

    protected static String k(Context context) {
        String string = context.getSharedPreferences("coo_id", 0).getString("cooId", null);
        return string != null ? string : l(context);
    }

    protected static String l(Context context) {
        Bundle bundle;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null || (bundle = applicationInfo.metaData) == null) {
                return null;
            }
            return bundle.getString("cooId");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    protected static o[] m(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("applist_message_prefs", 0);
        Map<String, ?> all = sharedPreferences.getAll();
        if (all == null || all.size() <= 0) {
            return null;
        }
        o[] oVarArr = new o[all.size()];
        for (String next : all.keySet()) {
            String string = sharedPreferences.getString(next, null);
            o oVar = new o();
            if (oVar.a(string)) {
                oVar.f = Integer.valueOf(next).intValue();
                try {
                    oVarArr[oVar.f] = oVar;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                sharedPreferences.edit().clear().commit();
                return null;
            }
        }
        return oVarArr;
    }

    public static int n(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.densityDpi;
    }

    public static boolean o(Context context) {
        Date date = new Date();
        return new StringBuilder().append("").append(date.getYear() + 1900).append(date.getMonth() + 1).append(date.getDate()).toString().equals(context.getSharedPreferences("toggle_flag_pref", 0).getString("date", "19000000"));
    }

    public static boolean p(Context context) {
        Date date = new Date();
        SharedPreferences.Editor edit = context.getSharedPreferences("toggle_flag_pref", 0).edit();
        edit.putString("date", "" + (date.getYear() + 1900) + (date.getMonth() + 1) + date.getDate());
        return edit.commit();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0195 A[SYNTHETIC, Splitter:B:62:0x0195] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x01ab A[SYNTHETIC, Splitter:B:72:0x01ab] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01c1 A[SYNTHETIC, Splitter:B:82:0x01c1] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x01d2 A[SYNTHETIC, Splitter:B:89:0x01d2] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static android.location.Location q(android.content.Context r12) {
        /*
            r5 = 4669142098048450560(0x40cc200000000000, double:14400.0)
            r10 = 0
            android.location.Location r1 = new android.location.Location
            java.lang.String r0 = "network"
            r1.<init>(r0)
            r2 = 0
            java.lang.String r0 = "phone"
            java.lang.Object r12 = r12.getSystemService(r0)
            android.telephony.TelephonyManager r12 = (android.telephony.TelephonyManager) r12
            android.telephony.CellLocation r0 = r12.getCellLocation()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            if (r0 != 0) goto L_0x002a
            if (r10 == 0) goto L_0x0021
            r2.close()     // Catch:{ IOException -> 0x0023 }
        L_0x0021:
            r0 = r10
        L_0x0022:
            return r0
        L_0x0023:
            r0 = move-exception
            java.lang.String r0 = "network get the latitude and longitude when closed BufferedReader ocurr IOException error"
            a(r0)
            goto L_0x0021
        L_0x002a:
            boolean r3 = r0 instanceof android.telephony.gsm.GsmCellLocation     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            if (r3 == 0) goto L_0x014f
            android.telephony.gsm.GsmCellLocation r0 = (android.telephony.gsm.GsmCellLocation) r0     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            int r3 = r0.getCid()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            int r0 = r0.getLac()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.String r4 = r12.getNetworkOperator()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            r5 = 0
            r6 = 3
            java.lang.String r4 = r4.substring(r5, r6)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            int r4 = r4.intValue()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.String r5 = r12.getNetworkOperator()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            r6 = 3
            r7 = 5
            java.lang.String r5 = r5.substring(r6, r7)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            int r5 = r5.intValue()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            r6.<init>()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.String r7 = "version"
            java.lang.String r8 = "1.1.0"
            r6.put(r7, r8)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.String r7 = "host"
            java.lang.String r8 = "maps.google.com"
            r6.put(r7, r8)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.String r7 = "request_address"
            r8 = 1
            r6.put(r7, r8)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            org.json.JSONArray r7 = new org.json.JSONArray     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            r7.<init>()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            org.json.JSONObject r8 = new org.json.JSONObject     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            r8.<init>()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.String r9 = "cell_id"
            r8.put(r9, r3)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.String r3 = "location_area_code"
            r8.put(r3, r0)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.String r0 = "mobile_country_code"
            r8.put(r0, r4)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.String r0 = "mobile_network_code"
            r8.put(r0, r5)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            r7.put(r8)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.String r0 = "cell_towers"
            r6.put(r0, r7)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            r0.<init>()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            org.apache.http.params.HttpParams r3 = r0.getParams()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.String r4 = "http.connection.timeout"
            r5 = 15000(0x3a98, float:2.102E-41)
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            r3.setParameter(r4, r5)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            org.apache.http.params.HttpParams r3 = r0.getParams()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.String r4 = "http.socket.timeout"
            r5 = 15000(0x3a98, float:2.102E-41)
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            r3.setParameter(r4, r5)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            org.apache.http.client.methods.HttpPost r3 = new org.apache.http.client.methods.HttpPost     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.String r4 = "http://www.google.com/loc/json"
            r3.<init>(r4)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            org.apache.http.entity.StringEntity r4 = new org.apache.http.entity.StringEntity     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.String r5 = r6.toString()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            r4.<init>(r5)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            r3.setEntity(r4)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            org.apache.http.HttpResponse r0 = r0.execute(r3)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            org.apache.http.StatusLine r3 = r0.getStatusLine()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            int r3 = r3.getStatusCode()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            r4 = 200(0xc8, float:2.8E-43)
            if (r3 != r4) goto L_0x0176
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.io.InputStream r0 = r0.getContent()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            r3.<init>(r0)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            r2.<init>(r3)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            r0.<init>()     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
        L_0x00f8:
            java.lang.String r3 = r2.readLine()     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            if (r3 == 0) goto L_0x0111
            r0.append(r3)     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            goto L_0x00f8
        L_0x0102:
            r0 = move-exception
            r0 = r2
        L_0x0104:
            java.lang.String r1 = "network get the latitude and longitude ocurr JSONException error"
            a(r1)     // Catch:{ all -> 0x01e0 }
            if (r0 == 0) goto L_0x010e
            r0.close()     // Catch:{ IOException -> 0x0185 }
        L_0x010e:
            r0 = r10
            goto L_0x0022
        L_0x0111:
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            r3.<init>(r0)     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            java.lang.String r0 = "location"
            java.lang.Object r12 = r3.get(r0)     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            org.json.JSONObject r12 = (org.json.JSONObject) r12     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            java.lang.String r0 = "latitude"
            java.lang.Object r0 = r12.get(r0)     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            java.lang.Double r0 = (java.lang.Double) r0     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            double r3 = r0.doubleValue()     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            r1.setLatitude(r3)     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            java.lang.String r0 = "longitude"
            java.lang.Object r12 = r12.get(r0)     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            java.lang.Double r12 = (java.lang.Double) r12     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            double r3 = r12.doubleValue()     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            r1.setLongitude(r3)     // Catch:{ JSONException -> 0x0102, ClientProtocolException -> 0x01eb, IOException -> 0x01e8, Exception -> 0x01e5, all -> 0x01dd }
            if (r2 == 0) goto L_0x0145
            r2.close()     // Catch:{ IOException -> 0x0148 }
        L_0x0145:
            r0 = r1
            goto L_0x0022
        L_0x0148:
            r0 = move-exception
            java.lang.String r0 = "network get the latitude and longitude when closed BufferedReader ocurr IOException error"
            a(r0)
            goto L_0x0145
        L_0x014f:
            boolean r3 = r0 instanceof android.telephony.cdma.CdmaCellLocation     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            if (r3 == 0) goto L_0x0176
            android.telephony.cdma.CdmaCellLocation r0 = (android.telephony.cdma.CdmaCellLocation) r0     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            int r3 = r0.getBaseStationLongitude()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            int r0 = r0.getBaseStationLatitude()     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            double r3 = (double) r3     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            double r3 = r3 / r5
            r1.setLongitude(r3)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            double r3 = (double) r0     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            double r3 = r3 / r5
            r1.setLatitude(r3)     // Catch:{ JSONException -> 0x01ee, ClientProtocolException -> 0x018c, IOException -> 0x01a2, Exception -> 0x01b8, all -> 0x01ce }
            if (r10 == 0) goto L_0x016c
            r2.close()     // Catch:{ IOException -> 0x016f }
        L_0x016c:
            r0 = r1
            goto L_0x0022
        L_0x016f:
            r0 = move-exception
            java.lang.String r0 = "network get the latitude and longitude when closed BufferedReader ocurr IOException error"
            a(r0)
            goto L_0x016c
        L_0x0176:
            if (r10 == 0) goto L_0x017b
            r2.close()     // Catch:{ IOException -> 0x017e }
        L_0x017b:
            r0 = r10
            goto L_0x0022
        L_0x017e:
            r0 = move-exception
            java.lang.String r0 = "network get the latitude and longitude when closed BufferedReader ocurr IOException error"
            a(r0)
            goto L_0x017b
        L_0x0185:
            r0 = move-exception
            java.lang.String r0 = "network get the latitude and longitude when closed BufferedReader ocurr IOException error"
            a(r0)
            goto L_0x010e
        L_0x018c:
            r0 = move-exception
            r0 = r10
        L_0x018e:
            java.lang.String r1 = "network get the latitude and longitude ocurr ClientProtocolException error"
            a(r1)     // Catch:{ all -> 0x01e0 }
            if (r0 == 0) goto L_0x010e
            r0.close()     // Catch:{ IOException -> 0x019a }
            goto L_0x010e
        L_0x019a:
            r0 = move-exception
            java.lang.String r0 = "network get the latitude and longitude when closed BufferedReader ocurr IOException error"
            a(r0)
            goto L_0x010e
        L_0x01a2:
            r0 = move-exception
            r0 = r10
        L_0x01a4:
            java.lang.String r1 = "network get the latitude and longitude ocurr IOException error"
            a(r1)     // Catch:{ all -> 0x01e0 }
            if (r0 == 0) goto L_0x010e
            r0.close()     // Catch:{ IOException -> 0x01b0 }
            goto L_0x010e
        L_0x01b0:
            r0 = move-exception
            java.lang.String r0 = "network get the latitude and longitude when closed BufferedReader ocurr IOException error"
            a(r0)
            goto L_0x010e
        L_0x01b8:
            r0 = move-exception
            r0 = r10
        L_0x01ba:
            java.lang.String r1 = "network get the latitude and longitude ocurr Exception error"
            a(r1)     // Catch:{ all -> 0x01e0 }
            if (r0 == 0) goto L_0x010e
            r0.close()     // Catch:{ IOException -> 0x01c6 }
            goto L_0x010e
        L_0x01c6:
            r0 = move-exception
            java.lang.String r0 = "network get the latitude and longitude when closed BufferedReader ocurr IOException error"
            a(r0)
            goto L_0x010e
        L_0x01ce:
            r0 = move-exception
            r1 = r10
        L_0x01d0:
            if (r1 == 0) goto L_0x01d5
            r1.close()     // Catch:{ IOException -> 0x01d6 }
        L_0x01d5:
            throw r0
        L_0x01d6:
            r1 = move-exception
            java.lang.String r1 = "network get the latitude and longitude when closed BufferedReader ocurr IOException error"
            a(r1)
            goto L_0x01d5
        L_0x01dd:
            r0 = move-exception
            r1 = r2
            goto L_0x01d0
        L_0x01e0:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x01d0
        L_0x01e5:
            r0 = move-exception
            r0 = r2
            goto L_0x01ba
        L_0x01e8:
            r0 = move-exception
            r0 = r2
            goto L_0x01a4
        L_0x01eb:
            r0 = move-exception
            r0 = r2
            goto L_0x018e
        L_0x01ee:
            r0 = move-exception
            r0 = r10
            goto L_0x0104
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kuguo.ad.a.q(android.content.Context):android.location.Location");
    }

    public static boolean r(Context context) {
        return context.getSharedPreferences("toggle_flag_pref", 0).getBoolean("showsprite", false);
    }

    public static int s(Context context) {
        return context.getSharedPreferences("toggle_flag_pref", 0).getInt("list_style", 0);
    }

    protected static int t(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 1;
        }
    }
}
