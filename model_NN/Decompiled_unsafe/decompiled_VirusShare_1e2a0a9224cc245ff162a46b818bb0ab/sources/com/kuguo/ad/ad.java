package com.kuguo.ad;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

class ad extends Handler {
    final /* synthetic */ l a;

    ad(l lVar) {
        this.a = lVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kuguo.ad.a.a(android.content.Context, java.lang.String, boolean):android.graphics.Bitmap
     arg types: [android.app.Activity, java.lang.String, int]
     candidates:
      com.kuguo.ad.a.a(android.content.Context, java.lang.String, int):int
      com.kuguo.ad.a.a(android.content.Context, java.lang.String, java.lang.String):int
      com.kuguo.ad.a.a(android.content.Context, java.io.File, int):android.content.Intent
      com.kuguo.ad.a.a(android.content.Context, int, int):boolean
      com.kuguo.ad.a.a(java.lang.String, int, int):boolean
      com.kuguo.ad.a.a(android.content.Context, java.lang.String, boolean):android.graphics.Bitmap */
    public void handleMessage(Message message) {
        Bitmap a2;
        Bundle data = message.getData();
        int i = data.getInt("tag");
        String string = data.getString("filePath");
        if (string != null && (a2 = a.a((Context) this.a.e, string, false)) != null && this.a.d != null) {
            if (i == -1) {
                this.a.d.a(a2);
            } else if (i > -1 && i < this.a.b) {
                this.a.d.a(a2, i);
            }
        }
    }
}
