package com.kuguo.ad;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Environment;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.io.IOException;
import java.util.Calendar;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;

public class MainService extends Service {
    /* access modifiers changed from: private */
    public static int e = 0;
    private q a;
    /* access modifiers changed from: private */
    public ap b;
    private v c;
    private Looper d;

    public static void a(Context context) {
        ((AlarmManager) context.getSystemService("alarm")).cancel(PendingIntent.getBroadcast(context, 0, new Intent(context, MainReceiver.class), 134217728));
    }

    public static void a(Context context, int i) {
        if (b(context)) {
            Log.d("android__log", "-------startMode == " + i);
            b(context, i);
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (z) {
            a.b(System.currentTimeMillis());
        }
        int i = 0;
        boolean z2 = false;
        while (!z2 && i < 4) {
            String externalStorageState = Environment.getExternalStorageState();
            if ("mounted".equals(externalStorageState)) {
                z2 = b();
            } else {
                Log.d("android__log", "externalStorageState = " + externalStorageState);
            }
            i++;
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        e = a.a(this, "kuguo_res/m.txt", 1) + 1;
    }

    private static void b(Context context, int i) {
        e = i;
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, new Intent(context, MainReceiver.class), 134217728);
        alarmManager.cancel(broadcast);
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        if (i == 0) {
            alarmManager.set(0, instance.getTimeInMillis() + 30000, broadcast);
        } else {
            alarmManager.setRepeating(0, instance.getTimeInMillis() + 30000, 7200000, broadcast);
        }
    }

    private boolean b() {
        boolean z;
        boolean z2;
        HttpPost httpPost = new HttpPost("http://www.cooguo.com/cooguogw/iris.action");
        this.b.a();
        try {
            httpPost.setEntity(new ByteArrayEntity(this.b.b()));
        } catch (Exception e2) {
        }
        try {
            HttpClient a2 = r.a(this);
            if (a2 == null) {
                return false;
            }
            HttpResponse execute = a2.execute(httpPost);
            int statusCode = execute.getStatusLine().getStatusCode();
            Log.d("android__log", "-----------status: " + statusCode);
            if (statusCode == 200) {
                try {
                    a.g(this);
                    a.f(this);
                    this.a.a(execute.getEntity().getContent());
                    return true;
                } catch (ClientProtocolException e3) {
                    ClientProtocolException clientProtocolException = e3;
                    z = true;
                    e = clientProtocolException;
                } catch (IOException e4) {
                    IOException iOException = e4;
                    z2 = true;
                    e = iOException;
                    e.printStackTrace();
                    return z2;
                } catch (Exception e5) {
                    return true;
                }
            } else {
                httpPost.abort();
                return false;
            }
        } catch (ClientProtocolException e6) {
            e = e6;
            z = false;
            e.printStackTrace();
            return z;
        } catch (IOException e7) {
            e = e7;
            z2 = false;
            e.printStackTrace();
            return z2;
        } catch (Exception e8) {
            return false;
        }
    }

    private static boolean b(Context context) {
        String a2 = a.a(context);
        return a2 != null && !"".equals(a2.trim());
    }

    /* access modifiers changed from: private */
    public void c() {
        String k = a.k(this);
        if (k != null) {
            this.b.n = k;
        }
        this.b.l = r.d(this);
        this.b.j = a.h(this);
        this.b.q = a.a();
        this.b.r = a.j(this);
        Location q = a.q(this);
        a.a("location : " + q);
        if (q != null) {
            this.b.f = q.getLongitude();
            this.b.e = q.getLatitude();
            this.b.s = a.a(q.getLongitude(), q.getLatitude());
        }
    }

    /* access modifiers changed from: private */
    public boolean d() {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        int i = instance.get(11);
        if (i >= 7 || i <= 1) {
            return true;
        }
        Log.d("android__log", "not in the request time area!");
        return false;
    }

    /* access modifiers changed from: private */
    public boolean e() {
        long c2 = a.c();
        if ((System.currentTimeMillis() - c2) / 60000 >= a.b()) {
            return true;
        }
        Log.d("android__log", "not meet the space time!");
        return false;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        this.b = new ap(this);
        this.a = q.a(this);
        this.a.a();
        HandlerThread handlerThread = new HandlerThread("AdsService", 10);
        handlerThread.start();
        this.d = handlerThread.getLooper();
        this.c = new v(this, this.d);
    }

    public void onDestroy() {
        this.d.quit();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        Message obtainMessage = this.c.obtainMessage();
        obtainMessage.arg1 = i2;
        obtainMessage.obj = intent;
        this.c.sendMessage(obtainMessage);
        return 2;
    }
}
