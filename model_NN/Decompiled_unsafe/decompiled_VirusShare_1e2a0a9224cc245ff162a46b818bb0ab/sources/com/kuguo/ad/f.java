package com.kuguo.ad;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.text.TextUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.kuguo.c.d;

public class f extends LinearLayout {
    private ImageView a;
    private TextView b;
    private ImageView c;
    private TextView d;
    private ImageView e;
    private Drawable f;

    public f(Context context) {
        super(context);
        setOrientation(0);
        setPadding(a.a(context, 6), a.a(context, 6), a.a(context, 6), a.a(context, 6));
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, new ColorDrawable(-7158214));
        stateListDrawable.addState(new int[]{16842910}, new ColorDrawable(-1));
        setBackgroundDrawable(stateListDrawable);
        this.f = d.b(context, "kuguo_res/icon.png");
        FrameLayout frameLayout = new FrameLayout(context);
        this.a = new ImageView(context);
        this.a.setImageDrawable(this.f);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a.a(context, 46), a.a(context, 46));
        layoutParams.gravity = 17;
        frameLayout.addView(this.a, layoutParams);
        this.c = new ImageView(context);
        this.c.setImageDrawable(d.b(context, "kuguo_res/installed.png"));
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(a.a(context, 18), a.a(context, 18));
        layoutParams2.gravity = 53;
        frameLayout.addView(this.c, layoutParams2);
        this.c.setVisibility(4);
        addView(frameLayout, new LinearLayout.LayoutParams(-2, -2));
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setPadding(a.a(context, 6), 0, 0, 0);
        linearLayout.setOrientation(1);
        this.b = new TextView(context);
        this.b.setTextColor(-16777216);
        this.b.setTextSize(16.0f);
        this.b.setSingleLine(true);
        this.b.setEllipsize(TextUtils.TruncateAt.END);
        linearLayout.addView(this.b, new LinearLayout.LayoutParams(-2, -2));
        this.d = new TextView(context);
        this.d.setTextColor(-12303292);
        this.d.setTextSize(14.0f);
        this.d.setSingleLine(true);
        this.d.setEllipsize(TextUtils.TruncateAt.END);
        linearLayout.addView(this.d, new LinearLayout.LayoutParams(-2, -2));
        this.e = new ImageView(context);
        linearLayout.addView(this.e, new LinearLayout.LayoutParams(-2, -2));
        addView(linearLayout, new LinearLayout.LayoutParams(-1, -2));
    }

    /* access modifiers changed from: protected */
    public void a(float f2) {
        this.e.setImageDrawable(new com.kuguo.ui.d(getContext(), f2));
    }

    /* access modifiers changed from: protected */
    public void a(Drawable drawable) {
        this.a.setImageDrawable(drawable);
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        this.b.setText(str);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        if (z) {
            this.c.setVisibility(0);
        } else {
            this.c.setVisibility(4);
        }
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        this.d.setText(str);
    }
}
