package com.kuguo.ad;

import MobWin.cnst.PROTOCOL_ENCODING;
import android.util.Log;
import com.android.mmreader140.chartact;
import com.android.mmreader140.mmabout;
import com.tencent.mobwin.core.a.a;

public class o {
    protected int A = 0;
    protected int B = 0;
    protected int C = 60;
    protected int D = 1;
    protected int E = 1;
    protected int F = -1;
    protected String G = "";
    protected String H = null;
    protected int I = 0;
    protected int J = 0;
    protected String a = "";
    protected String b = "";
    protected long c = -1;
    protected String d = "";
    protected byte e = 0;
    protected int f = -1;
    protected String g = "";
    protected int h = -1;
    protected String i = "";
    protected boolean j = false;
    protected String k = "";
    protected Integer l = -1;
    protected String m = "推荐";
    protected String n = "应用软件";
    protected int o = 1048576;
    protected String p = "1.0";
    protected String q = "";
    protected String r = "";
    protected int s = 1;
    protected int t = 0;
    protected String u = "";
    protected int v = 1;
    protected int w = 0;
    protected String x = "";
    protected int y = 1;
    protected int z = 1;

    protected static o[] a(byte[] bArr) {
        int i2;
        byte b2 = bArr[0];
        if (b2 < 0) {
            Log.d("android__log", "the first byte  -- " + ((int) b2));
            if (b2 == -20) {
                Log.d("android__log", "the first byte is -- " + ((int) b2) + "no show Sprite !!! ");
            }
            return null;
        }
        byte[] bArr2 = new byte[2];
        System.arraycopy(bArr, 3, bArr2, 0, 2);
        int c2 = b.c(bArr2);
        o[] oVarArr = new o[c2];
        int i3 = 3 + 2;
        int i4 = 0;
        while (i4 < c2) {
            o oVar = new o();
            while (true) {
                int i5 = i2 + 1;
                byte b3 = bArr[i2];
                if (b3 != -1) {
                    switch (b3) {
                        case 2:
                            byte[] bArr3 = new byte[2];
                            System.arraycopy(bArr, i5, bArr3, 0, 2);
                            int c3 = b.c(bArr3);
                            int i6 = i5 + 2;
                            byte[] bArr4 = new byte[c3];
                            System.arraycopy(bArr, i6, bArr4, 0, c3);
                            oVar.a = new String(bArr4, PROTOCOL_ENCODING.value);
                            i2 = c3 + i6;
                            break;
                        case 3:
                            byte[] bArr5 = new byte[2];
                            System.arraycopy(bArr, i5, bArr5, 0, 2);
                            int c4 = b.c(bArr5);
                            int i7 = i5 + 2;
                            byte[] bArr6 = new byte[c4];
                            System.arraycopy(bArr, i7, bArr6, 0, c4);
                            oVar.b = new String(bArr6, PROTOCOL_ENCODING.value);
                            i2 = c4 + i7;
                            break;
                        case 4:
                            byte[] bArr7 = new byte[2];
                            System.arraycopy(bArr, i5, bArr7, 0, 2);
                            int c5 = b.c(bArr7);
                            int i8 = i5 + 2;
                            byte[] bArr8 = new byte[c5];
                            System.arraycopy(bArr, i8, bArr8, 0, c5);
                            oVar.c = b.a(bArr8);
                            i2 = c5 + i8;
                            break;
                        case 5:
                            byte[] bArr9 = new byte[2];
                            System.arraycopy(bArr, i5, bArr9, 0, 2);
                            int c6 = b.c(bArr9);
                            int i9 = i5 + 2;
                            byte[] bArr10 = new byte[c6];
                            System.arraycopy(bArr, i9, bArr10, 0, c6);
                            oVar.d = new String(bArr10);
                            i2 = c6 + i9;
                            break;
                        case 6:
                            int i10 = i5 + 2;
                            oVar.e = bArr[i10];
                            i2 = i10 + 1;
                            break;
                        case 7:
                            byte[] bArr11 = new byte[2];
                            System.arraycopy(bArr, i5, bArr11, 0, 2);
                            int c7 = b.c(bArr11);
                            int i11 = i5 + 2;
                            byte[] bArr12 = new byte[c7];
                            System.arraycopy(bArr, i11, bArr12, 0, c7);
                            oVar.g = new String(bArr12, PROTOCOL_ENCODING.value);
                            i2 = c7 + i11;
                            break;
                        case 8:
                            byte[] bArr13 = new byte[2];
                            System.arraycopy(bArr, i5, bArr13, 0, 2);
                            int c8 = b.c(bArr13);
                            int i12 = i5 + 2;
                            byte[] bArr14 = new byte[c8];
                            System.arraycopy(bArr, i12, bArr14, 0, c8);
                            oVar.h = b.b(bArr14);
                            i2 = c8 + i12;
                            break;
                        case 9:
                            byte[] bArr15 = new byte[2];
                            System.arraycopy(bArr, i5, bArr15, 0, 2);
                            int c9 = b.c(bArr15);
                            int i13 = i5 + 2;
                            byte[] bArr16 = new byte[c9];
                            System.arraycopy(bArr, i13, bArr16, 0, c9);
                            oVar.i = new String(bArr16, PROTOCOL_ENCODING.value);
                            i2 = c9 + i13;
                            break;
                        case 10:
                            byte[] bArr17 = new byte[2];
                            System.arraycopy(bArr, i5, bArr17, 0, 2);
                            int c10 = b.c(bArr17);
                            int i14 = i5 + 2;
                            byte[] bArr18 = new byte[c10];
                            System.arraycopy(bArr, i14, bArr18, 0, c10);
                            oVar.m = new String(bArr18, PROTOCOL_ENCODING.value);
                            i2 = c10 + i14;
                            break;
                        case 11:
                            byte[] bArr19 = new byte[2];
                            System.arraycopy(bArr, i5, bArr19, 0, 2);
                            int c11 = b.c(bArr19);
                            int i15 = i5 + 2;
                            byte[] bArr20 = new byte[c11];
                            System.arraycopy(bArr, i15, bArr20, 0, c11);
                            oVar.n = new String(bArr20, PROTOCOL_ENCODING.value);
                            i2 = c11 + i15;
                            break;
                        case 12:
                            byte[] bArr21 = new byte[2];
                            System.arraycopy(bArr, i5, bArr21, 0, 2);
                            int c12 = b.c(bArr21);
                            int i16 = i5 + 2;
                            byte[] bArr22 = new byte[c12];
                            System.arraycopy(bArr, i16, bArr22, 0, c12);
                            oVar.o = b.b(bArr22);
                            i2 = c12 + i16;
                            break;
                        case 13:
                            byte[] bArr23 = new byte[2];
                            System.arraycopy(bArr, i5, bArr23, 0, 2);
                            int c13 = b.c(bArr23);
                            int i17 = i5 + 2;
                            byte[] bArr24 = new byte[c13];
                            System.arraycopy(bArr, i17, bArr24, 0, c13);
                            oVar.p = new String(bArr24, PROTOCOL_ENCODING.value);
                            i2 = c13 + i17;
                            break;
                        case t.n /*14*/:
                            byte[] bArr25 = new byte[2];
                            System.arraycopy(bArr, i5, bArr25, 0, 2);
                            int c14 = b.c(bArr25);
                            int i18 = i5 + 2;
                            byte[] bArr26 = new byte[c14];
                            System.arraycopy(bArr, i18, bArr26, 0, c14);
                            oVar.q = new String(bArr26, PROTOCOL_ENCODING.value);
                            i2 = c14 + i18;
                            break;
                        case 15:
                            byte[] bArr27 = new byte[2];
                            System.arraycopy(bArr, i5, bArr27, 0, 2);
                            int c15 = b.c(bArr27);
                            int i19 = i5 + 2;
                            byte[] bArr28 = new byte[c15];
                            System.arraycopy(bArr, i19, bArr28, 0, c15);
                            oVar.r = new String(bArr28, PROTOCOL_ENCODING.value);
                            i2 = c15 + i19;
                            break;
                        case t.p /*16*/:
                            byte[] bArr29 = new byte[2];
                            System.arraycopy(bArr, i5, bArr29, 0, 2);
                            int c16 = b.c(bArr29);
                            int i20 = i5 + 2;
                            byte[] bArr30 = new byte[c16];
                            System.arraycopy(bArr, i20, bArr30, 0, c16);
                            oVar.s = b.b(bArr30);
                            i2 = c16 + i20;
                            break;
                        case 17:
                            byte[] bArr31 = new byte[2];
                            System.arraycopy(bArr, i5, bArr31, 0, 2);
                            int c17 = b.c(bArr31);
                            int i21 = i5 + 2;
                            byte[] bArr32 = new byte[c17];
                            System.arraycopy(bArr, i21, bArr32, 0, c17);
                            oVar.t = b.b(bArr32);
                            i2 = c17 + i21;
                            break;
                        case 18:
                            byte[] bArr33 = new byte[2];
                            System.arraycopy(bArr, i5, bArr33, 0, 2);
                            int c18 = b.c(bArr33);
                            int i22 = i5 + 2;
                            byte[] bArr34 = new byte[c18];
                            System.arraycopy(bArr, i22, bArr34, 0, c18);
                            oVar.u = new String(bArr34, PROTOCOL_ENCODING.value);
                            i2 = c18 + i22;
                            break;
                        case 19:
                            byte[] bArr35 = new byte[2];
                            System.arraycopy(bArr, i5, bArr35, 0, 2);
                            int c19 = b.c(bArr35);
                            int i23 = i5 + 2;
                            byte[] bArr36 = new byte[c19];
                            System.arraycopy(bArr, i23, bArr36, 0, c19);
                            oVar.v = b.b(bArr36);
                            i2 = c19 + i23;
                            break;
                        case 20:
                            byte[] bArr37 = new byte[2];
                            System.arraycopy(bArr, i5, bArr37, 0, 2);
                            int c20 = b.c(bArr37);
                            int i24 = i5 + 2;
                            byte[] bArr38 = new byte[c20];
                            System.arraycopy(bArr, i24, bArr38, 0, c20);
                            oVar.w = b.b(bArr38);
                            i2 = c20 + i24;
                            break;
                        case chartact.START_ID:
                            byte[] bArr39 = new byte[2];
                            System.arraycopy(bArr, i5, bArr39, 0, 2);
                            int c21 = b.c(bArr39);
                            int i25 = i5 + 2;
                            byte[] bArr40 = new byte[c21];
                            System.arraycopy(bArr, i25, bArr40, 0, c21);
                            oVar.x = new String(bArr40, PROTOCOL_ENCODING.value);
                            i2 = c21 + i25;
                            break;
                        case chartact.GOOD_ID:
                            byte[] bArr41 = new byte[2];
                            System.arraycopy(bArr, i5, bArr41, 0, 2);
                            int c22 = b.c(bArr41);
                            int i26 = i5 + 2;
                            byte[] bArr42 = new byte[c22];
                            System.arraycopy(bArr, i26, bArr42, 0, c22);
                            oVar.y = b.b(bArr42);
                            i2 = c22 + i26;
                            break;
                        case chartact.EXIT_ID:
                            byte[] bArr43 = new byte[2];
                            System.arraycopy(bArr, i5, bArr43, 0, 2);
                            int c23 = b.c(bArr43);
                            int i27 = i5 + 2;
                            byte[] bArr44 = new byte[c23];
                            System.arraycopy(bArr, i27, bArr44, 0, c23);
                            oVar.z = b.b(bArr44);
                            i2 = c23 + i27;
                            break;
                        case a.n /*24*/:
                            byte[] bArr45 = new byte[2];
                            System.arraycopy(bArr, i5, bArr45, 0, 2);
                            int c24 = b.c(bArr45);
                            int i28 = i5 + 2;
                            byte[] bArr46 = new byte[c24];
                            System.arraycopy(bArr, i28, bArr46, 0, c24);
                            oVar.A = b.b(bArr46);
                            i2 = c24 + i28;
                            break;
                        case 25:
                            byte[] bArr47 = new byte[2];
                            System.arraycopy(bArr, i5, bArr47, 0, 2);
                            int c25 = b.c(bArr47);
                            int i29 = i5 + 2;
                            byte[] bArr48 = new byte[c25];
                            System.arraycopy(bArr, i29, bArr48, 0, c25);
                            oVar.B = b.b(bArr48);
                            i2 = c25 + i29;
                            break;
                        case a.m /*26*/:
                            byte[] bArr49 = new byte[2];
                            System.arraycopy(bArr, i5, bArr49, 0, 2);
                            int c26 = b.c(bArr49);
                            int i30 = i5 + 2;
                            byte[] bArr50 = new byte[c26];
                            System.arraycopy(bArr, i30, bArr50, 0, c26);
                            oVar.C = b.b(bArr50);
                            i2 = c26 + i30;
                            break;
                        case 27:
                            byte[] bArr51 = new byte[2];
                            System.arraycopy(bArr, i5, bArr51, 0, 2);
                            int c27 = b.c(bArr51);
                            int i31 = i5 + 2;
                            byte[] bArr52 = new byte[c27];
                            System.arraycopy(bArr, i31, bArr52, 0, c27);
                            oVar.D = b.b(bArr52);
                            i2 = c27 + i31;
                            break;
                        case 28:
                            byte[] bArr53 = new byte[2];
                            System.arraycopy(bArr, i5, bArr53, 0, 2);
                            int c28 = b.c(bArr53);
                            int i32 = i5 + 2;
                            byte[] bArr54 = new byte[c28];
                            System.arraycopy(bArr, i32, bArr54, 0, c28);
                            oVar.E = b.b(bArr54);
                            i2 = c28 + i32;
                            break;
                        case 29:
                            byte[] bArr55 = new byte[2];
                            System.arraycopy(bArr, i5, bArr55, 0, 2);
                            int c29 = b.c(bArr55);
                            int i33 = i5 + 2;
                            byte[] bArr56 = new byte[c29];
                            System.arraycopy(bArr, i33, bArr56, 0, c29);
                            oVar.F = b.b(bArr56);
                            i2 = c29 + i33;
                            break;
                        case j.g /*30*/:
                            byte[] bArr57 = new byte[2];
                            System.arraycopy(bArr, i5, bArr57, 0, 2);
                            int c30 = b.c(bArr57);
                            int i34 = i5 + 2;
                            byte[] bArr58 = new byte[c30];
                            System.arraycopy(bArr, i34, bArr58, 0, c30);
                            oVar.G = new String(bArr58, PROTOCOL_ENCODING.value);
                            i2 = c30 + i34;
                            break;
                        case mmabout.SITE_ID:
                            byte[] bArr59 = new byte[2];
                            System.arraycopy(bArr, i5, bArr59, 0, 2);
                            int c31 = b.c(bArr59);
                            int i35 = i5 + 2;
                            byte[] bArr60 = new byte[c31];
                            System.arraycopy(bArr, i35, bArr60, 0, c31);
                            oVar.H = new String(bArr60, PROTOCOL_ENCODING.value);
                            i2 = c31 + i35;
                            break;
                        case 32:
                            com.kuguo.c.a.a("android__log", "ANONYMOUS_ID -----------  !!! ");
                            byte[] bArr61 = new byte[2];
                            System.arraycopy(bArr, i5, bArr61, 0, 2);
                            int c32 = b.c(bArr61);
                            int i36 = i5 + 2;
                            byte[] bArr62 = new byte[c32];
                            System.arraycopy(bArr, i36, bArr62, 0, c32);
                            oVar.I = b.b(bArr62);
                            i2 = c32 + i36;
                            break;
                        case mmabout.MORE_ID:
                            com.kuguo.c.a.a("android__log", "list style is before ----------- " + oVar.J);
                            byte[] bArr63 = new byte[2];
                            System.arraycopy(bArr, i5, bArr63, 0, 2);
                            int c33 = b.c(bArr63);
                            int i37 = i5 + 2;
                            byte[] bArr64 = new byte[c33];
                            System.arraycopy(bArr, i37, bArr64, 0, c33);
                            oVar.J = b.b(bArr64);
                            i2 = c33 + i37;
                            com.kuguo.c.a.a("android__log", "list style is --- -------- " + oVar.J);
                            break;
                        default:
                            byte[] bArr65 = new byte[2];
                            System.arraycopy(bArr, i5, bArr65, 0, 2);
                            i2 = b.c(bArr65) + i5 + 2;
                            break;
                    }
                } else {
                    i3 = i5 + 2;
                    oVarArr[i4] = oVar;
                    i4++;
                }
            }
        }
        return oVarArr;
    }

    public String a() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.a);
        stringBuffer.append("|#*");
        stringBuffer.append(this.b);
        stringBuffer.append("|#*");
        stringBuffer.append(this.d);
        stringBuffer.append("|#*");
        stringBuffer.append(this.c);
        stringBuffer.append("|#*");
        stringBuffer.append((int) this.e);
        stringBuffer.append("|#*");
        stringBuffer.append(this.g);
        stringBuffer.append("|#*");
        stringBuffer.append(this.h);
        stringBuffer.append("|#*");
        stringBuffer.append(this.i);
        stringBuffer.append("|#*");
        stringBuffer.append(this.l);
        stringBuffer.append("|#*");
        stringBuffer.append(this.m);
        stringBuffer.append("|#*");
        stringBuffer.append(this.n);
        stringBuffer.append("|#*");
        stringBuffer.append(this.o);
        stringBuffer.append("|#*");
        stringBuffer.append(this.p);
        stringBuffer.append("|#*");
        stringBuffer.append(this.q);
        stringBuffer.append("|#*");
        stringBuffer.append(this.r);
        stringBuffer.append("|#*");
        stringBuffer.append(this.s);
        stringBuffer.append("|#*");
        stringBuffer.append(this.t);
        stringBuffer.append("|#*");
        stringBuffer.append(this.x);
        stringBuffer.append("|#*");
        stringBuffer.append(this.u);
        stringBuffer.append("|#*");
        stringBuffer.append(this.G);
        stringBuffer.append("|#*");
        stringBuffer.append(this.w);
        stringBuffer.append("|#*");
        stringBuffer.append(this.z);
        stringBuffer.append("|#*");
        stringBuffer.append(this.A);
        stringBuffer.append("|#*");
        stringBuffer.append(this.B);
        stringBuffer.append("|#*");
        stringBuffer.append(this.D);
        stringBuffer.append("|#*");
        stringBuffer.append(this.E);
        stringBuffer.append("|#*");
        stringBuffer.append(this.F);
        stringBuffer.append("|#*");
        stringBuffer.append(this.I);
        stringBuffer.append("|#*");
        stringBuffer.append(this.v);
        stringBuffer.append("|#*");
        stringBuffer.append(this.J);
        return stringBuffer.toString();
    }

    public boolean a(String str) {
        String[] a2 = a.a(str, "|#*");
        if (a2 == null) {
            return false;
        }
        try {
            this.a = a2[0];
            this.b = a2[1];
            this.d = a2[2];
            this.c = Long.valueOf(a2[3]).longValue();
            this.e = Byte.valueOf(a2[4]).byteValue();
            this.g = a2[5];
            this.h = Integer.valueOf(a2[6]).intValue();
            this.i = a2[7];
            this.l = Integer.valueOf(a2[8]);
            this.m = a2[9];
            this.n = a2[10];
            this.o = Integer.valueOf(a2[11]).intValue();
            this.p = a2[12];
            this.q = a2[13];
            this.r = a2[14];
            this.s = Integer.valueOf(a2[15]).intValue();
            this.t = Integer.valueOf(a2[16]).intValue();
            this.x = a2[17];
            this.u = a2[18];
            this.G = a2[19];
            this.w = Integer.valueOf(a2[20]).intValue();
            this.z = Integer.valueOf(a2[21]).intValue();
            this.A = Integer.valueOf(a2[22]).intValue();
            this.B = Integer.valueOf(a2[23]).intValue();
            this.D = Integer.valueOf(a2[24]).intValue();
            this.E = Integer.valueOf(a2[25]).intValue();
            this.F = Integer.valueOf(a2[26]).intValue();
            this.I = Integer.valueOf(a2[27]).intValue();
            this.v = Integer.valueOf(a2[28]).intValue();
            this.J = Integer.valueOf(a2[29]).intValue();
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        a.a("--------------------AdsResponsePackage print------------------");
        a.a("address: " + this.a);
        a.a("body: " + this.b);
        a.a("date: " + this.c);
        a.a("type: " + ((int) this.e));
        a.a("url: " + this.d);
        a.a("apkName: " + this.g);
        a.a("advertId: " + this.h);
        a.a("apkPackage: " + this.i);
        a.a("title" + this.m);
        a.a("appType: " + this.n);
        a.a("appSize: " + this.o);
        a.a("appVersion: " + this.p);
        a.a("iconUrl: " + this.q);
        a.a("captureUrls: " + this.r);
        a.a("showDialog: " + this.s);
        a.a("autoDownload: " + this.t);
        a.a("forceClick: " + this.v);
        a.a("autoInstall: " + this.w);
        a.a("permissions: " + this.x);
        a.a("marketPackage: " + this.u);
        a.a("permissions: " + this.x);
        a.a("nextRequest: " + this.y);
        a.a("vibrate: " + this.z);
        a.a("testAd: " + this.A);
        a.a("spaceTime: " + this.C);
        a.a("style: " + this.D);
        a.a("downloadMode: " + this.E);
        a.a("provider: " + this.G);
        a.a("idAndPakcageNames: " + this.H);
        a.a("anonymous: " + this.I);
        a.a("listStyle: " + this.J);
        a.a("----------------------------");
    }

    public String toString() {
        return a();
    }
}
