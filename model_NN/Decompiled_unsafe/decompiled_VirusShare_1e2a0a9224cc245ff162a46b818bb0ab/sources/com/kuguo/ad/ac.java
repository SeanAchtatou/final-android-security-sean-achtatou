package com.kuguo.ad;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.text.TextUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.kuguo.c.d;

public class ac extends LinearLayout {
    private ImageView a;
    private ImageView b;
    private TextView c;
    private Drawable d;

    public ac(Context context) {
        super(context);
        this.d = d.b(context, "kuguo_res/icon.png");
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.weight = 1.0f;
        setLayoutParams(layoutParams);
        setOrientation(1);
        if (a.n(context) <= 120) {
            setPadding(a.a(context, 4), 0, a.a(context, 4), 0);
        } else {
            setPadding(a.a(context, 4), a.a(context, 4), a.a(context, 4), a.a(context, 10));
        }
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, new ColorDrawable(-7158214));
        stateListDrawable.addState(new int[]{16842910}, new ColorDrawable(-1));
        setBackgroundDrawable(stateListDrawable);
        FrameLayout frameLayout = new FrameLayout(context);
        this.a = new ImageView(context);
        this.a.setImageDrawable(this.d);
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(a.a(context, 46), a.a(context, 46));
        layoutParams2.gravity = 17;
        frameLayout.addView(this.a, layoutParams2);
        this.b = new ImageView(context);
        this.b.setImageDrawable(d.b(context, "kuguo_res/installed.png"));
        FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(a.a(context, 18), a.a(context, 18));
        layoutParams3.gravity = 53;
        frameLayout.addView(this.b, layoutParams3);
        this.b.setVisibility(4);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams4.gravity = 17;
        addView(frameLayout, layoutParams4);
        this.c = new TextView(context);
        this.c.setText("xx软件");
        this.c.setTextSize(12.0f);
        this.c.setTextColor(-16777216);
        this.c.setSingleLine(true);
        this.c.setEllipsize(TextUtils.TruncateAt.END);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams5.gravity = 17;
        addView(this.c, layoutParams5);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.a.setImageDrawable(this.d);
    }

    /* access modifiers changed from: protected */
    public void a(Drawable drawable) {
        this.a.setImageDrawable(drawable);
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        String[] split = str.split("：");
        if (split == null || split.length != 2) {
            this.c.setText(str);
        } else {
            this.c.setText(split[1]);
        }
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        if (z) {
            this.b.setVisibility(0);
        } else {
            this.b.setVisibility(4);
        }
    }
}
