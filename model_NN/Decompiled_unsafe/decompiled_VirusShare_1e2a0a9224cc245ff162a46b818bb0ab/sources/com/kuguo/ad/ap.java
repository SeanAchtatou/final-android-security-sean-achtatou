package com.kuguo.ad;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.qq.taf.jce.JceStruct;

public class ap {
    protected String a = Build.VERSION.SDK;
    protected String b = "";
    protected String c = "";
    protected String d = Build.PRODUCT;
    protected double e = -500.0d;
    protected double f = -500.0d;
    protected String g = "";
    protected String h = "";
    protected int i = 100044;
    protected String j = "";
    protected int k = 160;
    protected String l = "unknown";
    protected int m = 1;
    protected String n = "";
    protected String o = "";
    protected int p = 0;
    protected int q = 0;
    protected String r = null;
    protected String s = "";
    protected String t = "";
    protected int u = 1;

    protected ap(Context context) {
        this.h = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        this.g = context.getPackageName();
        this.p = a.i(context) ? 1 : 0;
        this.c = "unknow";
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        this.b = a.a(context);
        this.i = a.b(context);
        this.k = displayMetrics.densityDpi;
        try {
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            if (bundle != null) {
                this.n = bundle.getString("cooId");
                this.o = bundle.getString("channelId");
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.u = a.t(context);
    }

    private void c() {
        this.t = a.d();
        if (this.t == null) {
            this.t = this.b;
        }
        a.h(this.t);
    }

    private int d() {
        if (this.b == null) {
            this.b = "";
        }
        c();
        if (this.d == null) {
            this.d = "";
        }
        if (this.c == null) {
            this.c = "umknown";
        }
        if (this.h == null) {
            this.h = "";
        }
        if (this.n == null) {
            this.n = "";
        }
        if (this.o == null) {
            this.o = "";
        }
        if (this.s == null) {
            this.s = "";
        }
        if (this.t == null) {
            this.t = "";
        }
        if (this.l == null || "".equals(this.l)) {
            this.l = "unknown";
        }
        return this.a.getBytes().length + 30 + this.b.getBytes().length + this.c.getBytes().length + this.d.getBytes().length + String.valueOf(this.e).getBytes().length + String.valueOf(this.f).getBytes().length + this.g.getBytes().length + this.h.getBytes().length + 7 + "1.1.2".getBytes().length + ((this.j == null || "".equals(this.j)) ? 0 : this.j.trim().getBytes().length + 3) + 7 + 3 + this.l.getBytes().length + 7 + 3 + this.n.getBytes().length + 3 + this.o.getBytes().length + 7 + 7 + ((this.r == null || "".equals(this.r)) ? 0 : this.r.trim().getBytes().length + 3) + 3 + this.s.getBytes().length + 3 + this.t.getBytes().length + 7;
    }

    /* access modifiers changed from: protected */
    public void a() {
        a.a("imsi: " + this.b);
        a.a("sdkVersion: " + this.a);
        a.a("hardware: " + this.c);
        a.a("projectId: " + this.i);
        a.a("packageName: " + this.g);
        a.a("requestMode: " + this.m);
        a.a("advertState: " + this.j);
        a.a("networkInfo: " + this.l);
        a.a("requestMode: " + this.m);
        a.a("cooId: " + this.n);
        a.a("channelId: " + this.o);
        a.a("isSystemApp: " + this.p);
        a.a("historyMessageCount: " + this.q);
        a.a("verifyIds: " + this.r);
        a.a("longitude: " + this.f);
        a.a("latitude: " + this.e);
        a.a("address:" + this.s);
        a.a("versionCode:" + this.u);
        a.a("_______________________________");
    }

    /* access modifiers changed from: protected */
    public byte[] b() {
        int d2 = d();
        byte[] bArr = new byte[d2];
        int i2 = 0 + 1;
        bArr[0] = 17;
        System.arraycopy(b.a((short) (d2 - 3)), 0, bArr, i2, 2);
        int i3 = i2 + 2;
        int i4 = i3 + 1;
        bArr[i3] = 1;
        System.arraycopy(b.a((short) this.b.getBytes().length), 0, bArr, i4, 2);
        int i5 = i4 + 2;
        byte[] bytes = this.b.getBytes();
        System.arraycopy(bytes, 0, bArr, i5, bytes.length);
        int length = bytes.length + 6;
        if (this.j != null && !"".equals(this.j)) {
            int i6 = length + 1;
            bArr[length] = 10;
            byte[] a2 = b.a((short) this.j.getBytes().length);
            System.arraycopy(a2, 0, bArr, i6, a2.length);
            int i7 = i6 + 2;
            byte[] bytes2 = this.j.getBytes();
            System.arraycopy(bytes2, 0, bArr, i7, bytes2.length);
            length = i7 + bytes2.length;
        }
        int i8 = length + 1;
        bArr[length] = JceStruct.SIMPLE_LIST;
        byte[] a3 = b.a((short) 4);
        System.arraycopy(a3, 0, bArr, i8, a3.length);
        int i9 = i8 + 2;
        byte[] a4 = b.a(this.m);
        System.arraycopy(a4, 0, bArr, i9, a4.length);
        int length2 = i9 + a4.length;
        int i10 = length2 + 1;
        bArr[length2] = 0;
        System.arraycopy(b.a((short) this.a.getBytes().length), 0, bArr, i10, 2);
        int i11 = i10 + 2;
        byte[] bytes3 = this.a.getBytes();
        System.arraycopy(bytes3, 0, bArr, i11, bytes3.length);
        int length3 = i11 + bytes3.length;
        int i12 = length3 + 1;
        bArr[length3] = 2;
        System.arraycopy(b.a((short) this.c.getBytes().length), 0, bArr, i12, 2);
        int i13 = i12 + 2;
        byte[] bytes4 = this.c.getBytes();
        System.arraycopy(bytes4, 0, bArr, i13, bytes4.length);
        int length4 = i13 + bytes4.length;
        int i14 = length4 + 1;
        bArr[length4] = 3;
        System.arraycopy(b.a((short) this.d.getBytes().length), 0, bArr, i14, 2);
        int i15 = i14 + 2;
        byte[] bytes5 = this.d.getBytes();
        System.arraycopy(bytes5, 0, bArr, i15, bytes5.length);
        int length5 = i15 + bytes5.length;
        int i16 = length5 + 1;
        bArr[length5] = 4;
        String valueOf = String.valueOf(this.e);
        System.arraycopy(b.a((short) valueOf.getBytes().length), 0, bArr, i16, 2);
        int i17 = i16 + 2;
        byte[] bytes6 = valueOf.getBytes();
        System.arraycopy(bytes6, 0, bArr, i17, bytes6.length);
        int length6 = bytes6.length + i17;
        int i18 = length6 + 1;
        bArr[length6] = 5;
        String valueOf2 = String.valueOf(this.f);
        System.arraycopy(b.a((short) valueOf2.getBytes().length), 0, bArr, i18, 2);
        int i19 = i18 + 2;
        byte[] bytes7 = valueOf2.getBytes();
        System.arraycopy(bytes7, 0, bArr, i19, bytes7.length);
        int length7 = bytes7.length + i19;
        int i20 = length7 + 1;
        bArr[length7] = 6;
        byte[] a5 = b.a((short) this.g.getBytes().length);
        System.arraycopy(a5, 0, bArr, i20, a5.length);
        int i21 = i20 + 2;
        byte[] bytes8 = this.g.getBytes();
        System.arraycopy(bytes8, 0, bArr, i21, bytes8.length);
        int length8 = i21 + bytes8.length;
        int i22 = length8 + 1;
        bArr[length8] = 7;
        byte[] a6 = b.a((short) this.h.getBytes().length);
        System.arraycopy(a6, 0, bArr, i22, a6.length);
        int i23 = i22 + 2;
        byte[] bytes9 = this.h.getBytes();
        System.arraycopy(bytes9, 0, bArr, i23, bytes9.length);
        int length9 = i23 + bytes9.length;
        int i24 = length9 + 1;
        bArr[length9] = 8;
        byte[] a7 = b.a((short) 4);
        System.arraycopy(a7, 0, bArr, i24, a7.length);
        int i25 = i24 + 2;
        byte[] a8 = b.a(this.i);
        System.arraycopy(a8, 0, bArr, i25, a8.length);
        int length10 = i25 + a8.length;
        int i26 = length10 + 1;
        bArr[length10] = 9;
        byte[] a9 = b.a((short) "1.1.2".getBytes().length);
        System.arraycopy(a9, 0, bArr, i26, a9.length);
        int i27 = i26 + 2;
        byte[] bytes10 = "1.1.2".getBytes();
        System.arraycopy(bytes10, 0, bArr, i27, bytes10.length);
        int length11 = i27 + bytes10.length;
        int i28 = length11 + 1;
        bArr[length11] = JceStruct.STRUCT_END;
        byte[] a10 = b.a((short) 4);
        System.arraycopy(a10, 0, bArr, i28, a10.length);
        int i29 = i28 + 2;
        byte[] a11 = b.a(this.k);
        System.arraycopy(a11, 0, bArr, i29, a11.length);
        int length12 = i29 + a11.length;
        int i30 = length12 + 1;
        bArr[length12] = JceStruct.ZERO_TAG;
        byte[] a12 = b.a((short) this.l.getBytes().length);
        System.arraycopy(a12, 0, bArr, i30, a12.length);
        int i31 = i30 + 2;
        byte[] bytes11 = this.l.getBytes();
        System.arraycopy(bytes11, 0, bArr, i31, bytes11.length);
        int length13 = i31 + bytes11.length;
        int i32 = length13 + 1;
        bArr[length13] = 14;
        byte[] a13 = b.a((short) this.n.getBytes().length);
        System.arraycopy(a13, 0, bArr, i32, a13.length);
        int i33 = i32 + 2;
        byte[] bytes12 = this.n.getBytes();
        System.arraycopy(bytes12, 0, bArr, i33, bytes12.length);
        int length14 = i33 + bytes12.length;
        int i34 = length14 + 1;
        bArr[length14] = 15;
        byte[] a14 = b.a((short) this.o.getBytes().length);
        System.arraycopy(a14, 0, bArr, i34, a14.length);
        int i35 = i34 + 2;
        byte[] bytes13 = this.o.getBytes();
        System.arraycopy(bytes13, 0, bArr, i35, bytes13.length);
        int length15 = i35 + bytes13.length;
        int i36 = length15 + 1;
        bArr[length15] = 16;
        byte[] a15 = b.a((short) 4);
        System.arraycopy(a15, 0, bArr, i36, a15.length);
        int i37 = i36 + 2;
        byte[] a16 = b.a(this.p);
        System.arraycopy(a16, 0, bArr, i37, a16.length);
        int length16 = i37 + a16.length;
        int i38 = length16 + 1;
        bArr[length16] = 17;
        byte[] a17 = b.a((short) 4);
        System.arraycopy(a17, 0, bArr, i38, a17.length);
        int i39 = i38 + 2;
        byte[] a18 = b.a(this.q);
        System.arraycopy(a18, 0, bArr, i39, a18.length);
        int length17 = i39 + a18.length;
        if (this.r != null && !"".equals(this.r)) {
            int i40 = length17 + 1;
            bArr[length17] = 18;
            byte[] a19 = b.a((short) this.r.getBytes().length);
            System.arraycopy(a19, 0, bArr, i40, a19.length);
            int i41 = i40 + 2;
            byte[] bytes14 = this.r.getBytes();
            System.arraycopy(bytes14, 0, bArr, i41, bytes14.length);
            length17 = i41 + bytes14.length;
        }
        int i42 = length17 + 1;
        bArr[length17] = 19;
        byte[] a20 = b.a((short) this.s.getBytes().length);
        System.arraycopy(a20, 0, bArr, i42, a20.length);
        int i43 = i42 + 2;
        byte[] bytes15 = this.s.getBytes();
        System.arraycopy(bytes15, 0, bArr, i43, bytes15.length);
        int length18 = i43 + bytes15.length;
        int i44 = length18 + 1;
        bArr[length18] = 100;
        byte[] a21 = b.a((short) this.t.getBytes().length);
        System.arraycopy(a21, 0, bArr, i44, a21.length);
        int i45 = i44 + 2;
        byte[] bytes16 = this.t.getBytes();
        System.arraycopy(bytes16, 0, bArr, i45, bytes16.length);
        int length19 = i45 + bytes16.length;
        int i46 = length19 + 1;
        bArr[length19] = 20;
        byte[] a22 = b.a((short) 4);
        System.arraycopy(a22, 0, bArr, i46, a22.length);
        int i47 = i46 + 2;
        byte[] a23 = b.a(this.u);
        System.arraycopy(a23, 0, bArr, i47, a23.length);
        int length20 = i47 + a23.length;
        return bArr;
    }
}
