package com.kuguo.ad;

import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;
import com.kuguo.c.a;
import java.util.HashSet;
import java.util.Set;

public class KuguoAdsManager {
    public static final int STYLE_FREE = 1;
    public static final int STYLE_SIDE = 0;
    static String cooId = null;
    public static boolean hasRequest = false;
    /* access modifiers changed from: private */
    public static Set listener = new HashSet();
    private static KuguoAdsManager manager;
    private Handler handler = new Handler();
    /* access modifiers changed from: private */
    public s kuguoFree = null;
    /* access modifiers changed from: private */
    public s kuguoSide = null;
    private c mInstallPackageReceiver;
    /* access modifiers changed from: private */
    public boolean showKuguoFree = false;
    /* access modifiers changed from: private */
    public boolean showKuguoSide = false;

    private KuguoAdsManager() {
    }

    protected static void addOnAppInstallListener(al alVar) {
        listener.add(alVar);
    }

    public static KuguoAdsManager getInstance() {
        if (manager == null) {
            manager = new KuguoAdsManager();
        }
        return manager;
    }

    private void requestAppList(Context context) {
        if (!a.o(context)) {
            Log.d("android__log", "b has not request !! ");
            new g(context).a();
        } else {
            Log.d("android__log", "b has request !! ");
            if (a.r(context)) {
                showSprite(context);
            }
        }
        hasRequest = true;
    }

    protected static void resetOnAppInstallListener() {
        listener.clear();
    }

    public void receivePushMessage(Context context, boolean z) {
        MainService.a(context, z ? 1 : 0);
    }

    public void recycle(Context context) {
        a.a("android__log", "recycle !! ");
        if (this.mInstallPackageReceiver != null) {
            try {
                context.getApplicationContext().unregisterReceiver(this.mInstallPackageReceiver);
                a.a("android__log", "recycle with ------------- unregisterReceiver !!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        removeKuguoSprite(context, 1);
        removeKuguoSprite(context, 0);
        hasRequest = false;
        this.mInstallPackageReceiver = null;
        this.showKuguoSide = false;
        this.showKuguoFree = false;
    }

    public void removeKuguoSprite(Context context, int i) {
        WindowManager windowManager = (WindowManager) context.getApplicationContext().getSystemService("window");
        if (i == 0) {
            try {
                if (this.kuguoSide != null) {
                    windowManager.removeView(this.kuguoSide);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (i == 1 && this.kuguoFree != null) {
            windowManager.removeView(this.kuguoFree);
        }
    }

    public void setCooId(Context context, String str) {
        a.f(context, str);
    }

    public void showKuguoSprite(Context context, int i) {
        Context applicationContext = context.getApplicationContext();
        if (i == 0) {
            this.showKuguoSide = true;
        } else {
            this.showKuguoFree = true;
        }
        if (!hasRequest) {
            requestAppList(applicationContext);
        } else if (a.r(applicationContext)) {
            showSprite(context);
        }
        if (this.mInstallPackageReceiver == null) {
            this.mInstallPackageReceiver = new c(this);
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
            intentFilter.addDataScheme("package");
            context.getApplicationContext().registerReceiver(this.mInstallPackageReceiver, intentFilter);
            a.a("android__log", " ----------registerReceiver !! ");
        }
    }

    /* access modifiers changed from: protected */
    public void showSprite(Context context) {
        this.handler.post(new e(this, context));
    }

    public void stopPushMessage(Context context) {
        MainService.a(context);
    }
}
