package com.kuguo.ad;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.kuguo.c.d;
import com.kuguo.ui.FoldView;
import com.kuguo.ui.ImageGallery;
import com.tencent.lbsapi.core.e;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class i extends LinearLayout implements View.OnClickListener {
    public boolean a = true;
    private ImageView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private TextView f;
    private ImageGallery g;
    private FoldView h;
    private TextView i;
    private ImageView j;
    private TextView k;
    private TextView l;
    private LinearLayout m;
    /* access modifiers changed from: private */
    public Context n;
    private View.OnClickListener o;
    private an p;

    public i(Context context, int i2) {
        super(context);
        this.n = context;
        this.p = new an(this, i2);
        setOrientation(1);
        setBackgroundColor(this.p.e);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setBackgroundColor(-1);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.setMargins(2, 2, 2, 2);
        addView(linearLayout, layoutParams);
        int parseColor = Color.parseColor("#5297be");
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setOrientation(0);
        linearLayout2.setBackgroundColor(parseColor);
        linearLayout.addView(linearLayout2, -1, -2);
        linearLayout2.setBackgroundDrawable(this.p.a);
        this.b = new ImageView(context);
        this.b.setImageResource(17301651);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.gravity = 16;
        layoutParams2.setMargins(5, 5, 3, 5);
        linearLayout2.addView(this.b, layoutParams2);
        LinearLayout linearLayout3 = new LinearLayout(context);
        linearLayout3.setOrientation(1);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2, 1.0f);
        layoutParams3.gravity = 16;
        layoutParams3.leftMargin = 5;
        linearLayout2.addView(linearLayout3, layoutParams3);
        this.c = new TextView(context);
        this.c.setTextColor(-256);
        this.c.setTextSize(16.0f);
        linearLayout3.addView(this.c, -1, -2);
        this.i = new TextView(context);
        this.i.setTextColor(-1);
        linearLayout3.addView(this.i, -1, -2);
        this.i.setVisibility(8);
        LinearLayout linearLayout4 = new LinearLayout(context);
        linearLayout4.setOrientation(0);
        linearLayout3.addView(linearLayout4, -1, -2);
        this.d = new TextView(context);
        this.d.setTextColor(-1);
        linearLayout4.addView(this.d, -2, -2);
        this.d.setVisibility(8);
        this.e = new TextView(context);
        this.e.setTextColor(-1);
        this.e.setPadding(this.e.getPaddingLeft() + 10, this.e.getPaddingTop(), this.e.getPaddingRight(), this.e.getPaddingBottom());
        linearLayout4.addView(this.e, -1, -2);
        this.e.setVisibility(8);
        ScrollView scrollView = new ScrollView(context);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, -2, 1.0f);
        layoutParams4.topMargin = 10;
        linearLayout.addView(scrollView, layoutParams4);
        LinearLayout linearLayout5 = new LinearLayout(context);
        linearLayout5.setOrientation(1);
        linearLayout5.setPadding(0, 0, 0, 15);
        scrollView.addView(linearLayout5, -1, -2);
        this.f = new TextView(context);
        this.f.setTextSize(16.0f);
        this.f.setTextColor(-16777216);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams5.rightMargin = 10;
        layoutParams5.leftMargin = 10;
        linearLayout5.addView(this.f, layoutParams5);
        this.g = new ImageGallery(context);
        this.g.setBackgroundColor(Color.rgb(236, 236, 236));
        this.g.setPadding(0, 20, 0, 40);
        this.g.setSpacing(10);
        this.g.a(d.a(context, "kuguo_res/spot_default.png"), d.a(context, "kuguo_res/spot_light.png"));
        LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams6.topMargin = 10;
        linearLayout5.addView(this.g, layoutParams6);
        this.k = new TextView(context);
        new LinearLayout.LayoutParams(-2, -2).setMargins(10, 20, 10, 0);
        this.k.setTextColor(-16777216);
        this.k.setVisibility(8);
        this.h = new FoldView(context);
        this.h.a("软件权限");
        this.h.a(18);
        this.h.b(parseColor);
        this.h.a(d.a(context, "kuguo_res/fold_arrow.png"));
        this.h.a(-7829368, 2);
        this.h.setVisibility(8);
        LinearLayout.LayoutParams layoutParams7 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams7.bottomMargin = 10;
        layoutParams7.rightMargin = 10;
        layoutParams7.leftMargin = 10;
        layoutParams7.topMargin = 10;
        linearLayout5.addView(this.h, layoutParams7);
        LinearLayout linearLayout6 = new LinearLayout(context);
        linearLayout6.setOrientation(1);
        linearLayout6.setPadding(2, 10, 2, 10);
        LinearLayout linearLayout7 = new LinearLayout(context);
        TextView textView = new TextView(context);
        textView.setText("安全认证");
        textView.setTextColor(-1);
        textView.setPadding(10, 0, 0, 0);
        LinearLayout.LayoutParams layoutParams8 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams8.gravity = 3;
        linearLayout7.addView(textView, layoutParams8);
        linearLayout7.setBackgroundColor(-11364418);
        LinearLayout linearLayout8 = new LinearLayout(context);
        TextView textView2 = new TextView(context);
        textView2.setText("本软件由");
        textView2.setTextSize(16.0f);
        textView2.setTextColor(-16777216);
        LinearLayout.LayoutParams layoutParams9 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams9.gravity = 16;
        linearLayout8.addView(textView2, layoutParams9);
        ImageView imageView = new ImageView(context);
        imageView.setTag(3);
        imageView.setOnClickListener(this);
        imageView.setImageDrawable(d.b(context, "kuguo_res/360.png"));
        linearLayout8.addView(imageView, new LinearLayout.LayoutParams(a.a(context, 48), a.a(context, 48)));
        TextView textView3 = new TextView(context);
        textView3.setTextColor(-16777216);
        textView3.setText("安全认证");
        textView3.setTextSize(16.0f);
        LinearLayout.LayoutParams layoutParams10 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams10.gravity = 16;
        linearLayout8.addView(textView3, layoutParams10);
        TextView textView4 = new TextView(context);
        new LinearLayout.LayoutParams(-2, -2).gravity = 16;
        textView4.setText("【本软件已经过");
        textView4.setTextColor(-16777216);
        new ImageView(context).setImageDrawable(d.b(context, "kuguo_res/360.png"));
        new LinearLayout.LayoutParams(-2, -2).gravity = 16;
        TextView textView5 = new TextView(context);
        new LinearLayout.LayoutParams(-2, -2).gravity = 16;
        textView5.setText("安全认证】");
        textView5.setTextColor(-16777216);
        this.m = new LinearLayout(context);
        this.m.setOrientation(1);
        TextView textView6 = new TextView(context);
        textView6.setText("版权声明");
        textView6.setTextColor(-1);
        textView6.setPadding(10, 0, 0, 0);
        LinearLayout.LayoutParams layoutParams11 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams11.gravity = 3;
        LinearLayout linearLayout9 = new LinearLayout(context);
        linearLayout9.addView(textView6, layoutParams11);
        linearLayout9.setBackgroundColor(-11364418);
        this.l = new TextView(context);
        this.l.setVisibility(8);
        this.l.setTextColor(-16777216);
        this.l.setPadding(20, 0, 0, 0);
        LinearLayout.LayoutParams layoutParams12 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams12.gravity = 17;
        this.m.addView(linearLayout9, layoutParams12);
        LinearLayout.LayoutParams layoutParams13 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams13.gravity = 16;
        layoutParams13.topMargin = 5;
        this.m.addView(this.l, layoutParams13);
        this.m.setVisibility(8);
        LinearLayout.LayoutParams layoutParams14 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams14.gravity = 17;
        linearLayout6.addView(linearLayout7, layoutParams14);
        LinearLayout.LayoutParams layoutParams15 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams15.gravity = 17;
        layoutParams15.bottomMargin = 5;
        layoutParams15.topMargin = 5;
        linearLayout6.addView(linearLayout8, layoutParams15);
        LinearLayout.LayoutParams layoutParams16 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams16.gravity = 17;
        linearLayout6.addView(this.m, layoutParams16);
        LinearLayout.LayoutParams layoutParams17 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams17.gravity = 17;
        layoutParams17.topMargin = 10;
        linearLayout5.addView(linearLayout6, layoutParams17);
        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setBackgroundColor(parseColor);
        LinearLayout.LayoutParams layoutParams18 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams18.gravity = 80;
        linearLayout.addView(frameLayout, layoutParams18);
        LinearLayout linearLayout10 = new LinearLayout(context);
        linearLayout10.setOrientation(1);
        LinearLayout linearLayout11 = new LinearLayout(context);
        linearLayout10.addView(linearLayout11);
        FrameLayout.LayoutParams layoutParams19 = new FrameLayout.LayoutParams(-1, -2);
        layoutParams19.gravity = 17;
        frameLayout.addView(linearLayout10, layoutParams19);
        frameLayout.setBackgroundDrawable(this.p.b);
        LinearLayout linearLayout12 = new LinearLayout(context);
        linearLayout12.setGravity(17);
        this.j = new ImageView(context);
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, d.b(context, "kuguo_res/download_btn2_pressed.png"));
        stateListDrawable.addState(new int[]{16842910}, d.b(context, "kuguo_res/download_btn2.png"));
        this.j.setBackgroundDrawable(stateListDrawable);
        LinearLayout.LayoutParams layoutParams20 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams20.weight = 1.0f;
        layoutParams20.gravity = 17;
        this.j.setTag(1);
        linearLayout12.addView(this.j, -2, -2);
        linearLayout11.addView(linearLayout12, layoutParams20);
        this.j.setOnClickListener(this);
        LinearLayout linearLayout13 = new LinearLayout(context);
        linearLayout13.setGravity(17);
        ImageView imageView2 = new ImageView(context);
        StateListDrawable stateListDrawable2 = new StateListDrawable();
        stateListDrawable2.addState(new int[]{16842919}, d.b(context, "kuguo_res/push_cancel_btn_pressed.png"));
        stateListDrawable2.addState(new int[]{16842910}, d.b(context, "kuguo_res/push_cancel_btn.png"));
        imageView2.setBackgroundDrawable(stateListDrawable2);
        LinearLayout.LayoutParams layoutParams21 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams21.weight = 1.0f;
        layoutParams21.gravity = 17;
        imageView2.setTag(2);
        linearLayout13.addView(imageView2, -2, -2);
        linearLayout11.addView(linearLayout13, layoutParams21);
        imageView2.setOnClickListener(this);
    }

    private String b(float f2) {
        return f2 > 1024.0f ? (((float) ((int) ((f2 / 1024.0f) * 100.0f))) / 100.0f) + "M" : ((int) Math.ceil((double) f2)) + "K";
    }

    public void a() {
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, d.b(getContext(), "kuguo_res/confirm_btn_pressed.png"));
        stateListDrawable.addState(new int[]{16842910}, d.b(getContext(), "kuguo_res/confirm_btn.png"));
        this.j.setBackgroundDrawable(stateListDrawable);
    }

    public void a(float f2) {
        this.e.setText("大小: " + b(f2));
        this.e.setVisibility(0);
    }

    public void a(int i2) {
        Bitmap a2 = d.a(getContext(), "kuguo_res/snapshot_empty.png");
        ArrayList arrayList = new ArrayList(i2);
        for (int i3 = 0; i3 < i2; i3++) {
            arrayList.add(a2);
        }
        a(arrayList);
    }

    public void a(Bitmap bitmap) {
        this.b.setImageDrawable(d.a(getContext(), bitmap));
    }

    public void a(Bitmap bitmap, int i2) {
        this.g.a(bitmap, i2);
    }

    public void a(View.OnClickListener onClickListener) {
        this.o = onClickListener;
    }

    public void a(String str) {
        if (this.c != null) {
            this.c.setText(str);
        }
    }

    public void a(List list) {
        this.g.a(list);
    }

    public void a(Map map) {
        if (!map.isEmpty()) {
            new TextView(getContext()).setTextColor(-16777216);
            StringBuilder sb = new StringBuilder();
            sb.append("<ul>");
            for (String str : map.keySet()) {
                sb.append("<li><b>");
                sb.append(str);
                String str2 = (String) map.get(str);
                if (str2 != null) {
                    sb.append("</b><br/>");
                    sb.append(str2);
                }
                sb.append("</li>");
            }
            sb.append("</ul>");
            String sb2 = sb.toString();
            WebView webView = new WebView(getContext());
            webView.loadDataWithBaseURL(null, sb2, "text/html", e.e, null);
            this.h.a(webView);
            this.h.setVisibility(0);
        }
    }

    public void b(String str) {
        this.i.setText("类型: " + str);
        this.i.setVisibility(0);
    }

    public void c(String str) {
        this.d.setText("版本: " + str);
        this.d.setVisibility(0);
    }

    public void d(String str) {
        this.f.setText(str);
    }

    public void e(String str) {
        if (str != null && !"".equals(str)) {
            this.l.setText("" + str + "");
            this.l.setVisibility(0);
            this.m.setVisibility(0);
        }
    }

    public void onClick(View view) {
        if (this.o != null) {
            this.o.onClick(view);
        }
    }
}
