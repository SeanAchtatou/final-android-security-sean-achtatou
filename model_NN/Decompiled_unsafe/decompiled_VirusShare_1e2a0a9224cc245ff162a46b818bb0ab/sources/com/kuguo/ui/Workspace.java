package com.kuguo.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import com.kuguo.ad.a;
import com.kuguo.ad.n;
import com.kuguo.c.d;
import java.util.ArrayList;
import java.util.List;

public class Workspace extends ViewGroup {
    private static Bitmap b;
    private static Bitmap c;
    private static Bitmap d;
    private static Bitmap e;
    boolean a;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private a k;
    private int l;
    private Context m;
    private List n;

    public Workspace(Context context) {
        this(context, null);
        this.m = context;
        b = d.a(this.m, "kuguo_res/spot_light.png");
        c = d.a(this.m, "kuguo_res/spot_default.png");
        d = d.a(this.m, "kuguo_res/arrow_right.png");
        e = d.a(this.m, "kuguo_res/arrow_left.png");
        setBackgroundColor(-1);
    }

    public Workspace(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = false;
        this.n = new ArrayList();
        this.l = a.a(context, 5);
    }

    public int a() {
        return this.j;
    }

    public void a(int i2) {
        int scrollX = getScrollX() + i2;
        if (scrollX >= 0 && scrollX <= getWidth() * (getChildCount() - 1)) {
            scrollBy(i2, 0);
        } else if (scrollX < 0) {
            scrollBy(-getScrollX(), 0);
        } else {
            scrollBy((getWidth() * (getChildCount() - 1)) - getScrollX(), 0);
        }
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, layoutParams);
        if (view instanceof GridView) {
            this.n.add((GridView) view);
        }
    }

    public void b() {
        if (this.n != null) {
            for (GridView adapter : this.n) {
                ((n) adapter.getAdapter()).notifyDataSetChanged();
            }
        }
    }

    public void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        int childCount = getChildCount();
        if (childCount > 1) {
            int width = c.getWidth();
            int width2 = (getWidth() - ((width * childCount) + ((childCount - 1) * 7))) / 2;
            int height = getHeight() - 12;
            int i2 = 0;
            while (i2 < childCount) {
                canvas.drawBitmap(i2 != a() ? c : b, (float) (((width + 7) * i2) + width2 + getScrollX()), (float) (height - this.l), (Paint) null);
                i2++;
            }
            a.a(this.m, 10);
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        switch (action) {
            case 0:
                this.a = false;
                this.f = x;
                this.g = y;
                this.h = x;
                this.i = y;
                break;
            case 1:
                int i2 = x - this.f;
                if (i2 < -20 || i2 > 20) {
                    if (i2 <= 20 || this.j <= 0) {
                        if (i2 < -20 && this.j < getChildCount() - 1) {
                            this.j++;
                            scrollTo(getWidth() * this.j, 0);
                            if (this.k != null) {
                                this.k.a(this.j);
                                break;
                            }
                        }
                    } else {
                        this.j--;
                        scrollTo(getWidth() * this.j, 0);
                        if (this.k != null) {
                            this.k.a(this.j);
                            break;
                        }
                    }
                } else {
                    scrollTo(getWidth() * this.j, 0);
                    break;
                }
                break;
            case 2:
                if (Math.abs(x - this.h) > a.a(this.m, 13)) {
                    this.a = true;
                    a(this.h - x);
                    this.h = x;
                    break;
                }
                break;
        }
        if (this.a) {
            motionEvent.setAction(3);
        }
        super.dispatchTouchEvent(motionEvent);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int childCount = getChildCount();
        int i6 = 0;
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() != 8) {
                int measuredWidth = childAt.getMeasuredWidth();
                childAt.layout(i6, 0, i6 + measuredWidth, childAt.getMeasuredHeight());
                i6 += measuredWidth;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int childCount = getChildCount();
        for (int i4 = 0; i4 < childCount; i4++) {
            getChildAt(i4).measure(i2, i3);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    public void removeAllViews() {
        super.removeAllViews();
        this.n.clear();
    }

    public void removeView(View view) {
        super.removeView(view);
        if (view instanceof GridView) {
            this.n.remove((GridView) view);
        }
    }
}
