package com.kuguo.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public class d extends Drawable {
    private static Drawable a;
    private static Drawable b;
    private static Drawable c;
    private static int d;
    private static int e;
    private float f;

    public d(Context context, float f2) {
        this.f = f2;
        a = com.kuguo.c.d.b(context, "kuguo_res/rating_empty.png");
        b = com.kuguo.c.d.b(context, "kuguo_res/rating_half.png");
        c = com.kuguo.c.d.b(context, "kuguo_res/rating_full.png");
        d = c.getIntrinsicWidth();
        e = c.getIntrinsicHeight();
    }

    public void draw(Canvas canvas) {
        int i;
        int i2;
        int floor = (int) Math.floor((double) this.f);
        Rect bounds = getBounds();
        int i3 = bounds.left;
        for (int i4 = 0; i4 < floor; i4++) {
            c.setBounds(i3, bounds.top, d + i3, bounds.bottom);
            c.draw(canvas);
            i3 += d;
        }
        if (this.f > ((float) floor)) {
            b.setBounds(i3, bounds.top, d + i3, bounds.bottom);
            b.draw(canvas);
            i = floor + 1;
            i2 = d + i3;
        } else {
            i = floor;
            i2 = i3;
        }
        int i5 = 5 - i;
        int i6 = i2;
        for (int i7 = 0; i7 < i5; i7++) {
            a.setBounds(i6, bounds.top, d + i6, bounds.top + a.getIntrinsicHeight());
            a.draw(canvas);
            i6 += d;
        }
    }

    public int getIntrinsicHeight() {
        return e;
    }

    public int getIntrinsicWidth() {
        return d * 5;
    }

    public int getOpacity() {
        return 0;
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
