package com.kuguo.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.kuguo.c.d;

public class FoldView extends LinearLayout implements View.OnClickListener {
    private TextView a;
    private ImageView b;
    private ImageView c;
    private View d;
    private boolean e;

    public FoldView(Context context) {
        this(context, null);
    }

    public FoldView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.e = true;
        setOrientation(1);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(0);
        linearLayout.setOnClickListener(this);
        addView(linearLayout, -1, -2);
        this.a = new TextView(context);
        linearLayout.addView(this.a, new LinearLayout.LayoutParams(-2, -1, 1.0f));
        this.b = new e(this, context);
        this.b.setScaleType(ImageView.ScaleType.MATRIX);
        linearLayout.addView(this.b, -2, -1);
        this.c = new ImageView(context);
        this.c.setScaleType(ImageView.ScaleType.FIT_XY);
        addView(this.c, -1, -2);
    }

    public void a() {
        this.e = true;
        if (this.d != null) {
            this.d.setVisibility(8);
        }
        this.b.setImageMatrix(new Matrix());
    }

    public void a(int i) {
        this.a.setTextSize((float) i);
    }

    public void a(int i, int i2) {
        Bitmap createBitmap = Bitmap.createBitmap(10, i2, Bitmap.Config.RGB_565);
        new Canvas(createBitmap).drawColor(i);
        b(createBitmap);
    }

    public void a(Bitmap bitmap) {
        this.b.setImageDrawable(d.a(getContext(), bitmap));
    }

    public void a(View view) {
        if (this.d != null) {
            removeView(this.d);
        }
        this.d = view;
        view.setVisibility(this.e ? 8 : 0);
        addView(view, -1, -2);
    }

    public void a(String str) {
        this.a.setText(str);
    }

    public void b() {
        this.e = false;
        if (this.d != null) {
            this.d.setVisibility(0);
        }
        Drawable drawable = this.b.getDrawable();
        if (drawable != null) {
            Matrix matrix = new Matrix();
            matrix.setRotate(180.0f, (float) (drawable.getIntrinsicWidth() / 2), (float) (drawable.getIntrinsicHeight() / 2));
            this.b.setImageMatrix(matrix);
        }
    }

    public void b(int i) {
        this.a.setTextColor(i);
    }

    public void b(Bitmap bitmap) {
        this.c.setImageDrawable(d.a(getContext(), bitmap));
    }

    public void onClick(View view) {
        if (this.e) {
            b();
        } else {
            a();
        }
    }
}
