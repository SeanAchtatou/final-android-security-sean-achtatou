package com.kuguo.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

class e extends ImageView {
    final /* synthetic */ FoldView a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(FoldView foldView, Context context) {
        super(context);
        this.a = foldView;
    }

    public void onDraw(Canvas canvas) {
        canvas.save();
        Drawable drawable = getDrawable();
        if (drawable != null) {
            canvas.translate((float) ((getWidth() - drawable.getIntrinsicWidth()) / 2), (float) ((getHeight() - drawable.getIntrinsicHeight()) / 2));
        }
        super.onDraw(canvas);
        canvas.restore();
    }
}
