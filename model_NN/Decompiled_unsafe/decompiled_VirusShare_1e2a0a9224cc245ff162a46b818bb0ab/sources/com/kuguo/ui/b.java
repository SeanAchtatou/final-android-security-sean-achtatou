package com.kuguo.ui;

import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.kuguo.c.d;
import java.util.ArrayList;
import java.util.List;

class b extends BaseAdapter {
    List a = new ArrayList();
    final /* synthetic */ ImageGallery b;

    b(ImageGallery imageGallery) {
        this.b = imageGallery;
    }

    public List a() {
        return this.a;
    }

    public void a(List list) {
        this.a = list;
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.a.size();
    }

    public Object getItem(int i) {
        return this.a.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView = view == null ? new ImageView(this.b.getContext()) : view;
        ((ImageView) imageView).setImageDrawable(d.a(this.b.getContext(), (Bitmap) this.a.get(i)));
        return imageView;
    }
}
