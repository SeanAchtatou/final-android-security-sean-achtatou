package com.kuguo.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.Gallery;
import android.widget.SpinnerAdapter;
import java.util.List;

public class ImageGallery extends Gallery {
    private Bitmap a;
    private Bitmap b;

    public ImageGallery(Context context) {
        this(context, null);
    }

    public ImageGallery(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setAdapter((SpinnerAdapter) new b(this));
    }

    private void a(Canvas canvas) {
        int count = getAdapter().getCount();
        if (count > 0 && this.a != null && this.b != null) {
            int width = this.a.getWidth();
            int height = this.a.getHeight();
            int selectedItemPosition = getSelectedItemPosition();
            int height2 = (getHeight() - height) - 10;
            int width2 = ((getWidth() - (width * count)) - ((count - 1) * 10)) / 2;
            for (int i = 0; i < count; i++) {
                if (i != selectedItemPosition) {
                    canvas.drawBitmap(this.a, (float) width2, (float) height2, (Paint) null);
                } else {
                    canvas.drawBitmap(this.b, (float) width2, (float) height2, (Paint) null);
                }
                width2 += width + 10;
            }
        }
    }

    public void a(Bitmap bitmap, int i) {
        b bVar = (b) getAdapter();
        bVar.a().set(i, bitmap);
        bVar.notifyDataSetChanged();
    }

    public void a(Bitmap bitmap, Bitmap bitmap2) {
        this.a = bitmap;
        this.b = bitmap2;
    }

    public void a(List list) {
        ((b) getAdapter()).a(list);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        a(canvas);
    }
}
