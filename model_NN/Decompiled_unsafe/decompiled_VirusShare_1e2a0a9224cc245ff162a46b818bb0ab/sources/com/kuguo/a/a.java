package com.kuguo.a;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

class a extends SQLiteOpenHelper {
    a(Context context) {
        super(context, "downloads", (SQLiteDatabase.CursorFactory) null, 3);
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE downloads (_id INTEGER PRIMARY KEY,url TEXT,file TEXT,size INTEGER,total_size INTEGER,state INTEGER,file_name TEXT,network_info TEXT,download_mode INTEGER, eTag TEXT );");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        com.kuguo.c.a.a("Upgrading database from version " + i + " to " + i2 + ", which will destroy all old data");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS downloads");
        onCreate(sQLiteDatabase);
    }
}
