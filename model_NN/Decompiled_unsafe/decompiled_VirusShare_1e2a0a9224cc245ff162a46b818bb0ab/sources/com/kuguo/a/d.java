package com.kuguo.a;

import android.content.Context;
import com.kuguo.b.a;
import com.kuguo.b.b;
import com.kuguo.b.c;
import com.kuguo.b.h;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.Map;

public class d implements c {
    private b a;
    private File b;
    private int c;
    private int d;
    private int e;
    private c f;
    private int g;
    private f h;
    private BufferedInputStream i;
    private int j;
    private int k = 3;
    private Exception l;
    private String m;
    private Object n;
    private String o;
    private String p;
    private String q;

    d(Context context, e eVar) {
        this.a = new a(context, new h(eVar.a));
        this.h = f.a();
        this.m = eVar.a;
        this.b = new File(eVar.b);
        this.d = eVar.c;
        this.e = eVar.d;
        this.c = eVar.e;
        this.p = eVar.f;
        this.o = eVar.g;
        this.g = eVar.h;
        this.q = eVar.i;
    }

    public d(String str, File file, int i2) {
        this.b = file;
        this.m = str;
        this.a = new a(f.a().a, new h(str));
        this.e = -1;
        this.c = 0;
        this.g = i2;
        q();
    }

    private void a(InputStream inputStream) {
        RandomAccessFile randomAccessFile;
        com.kuguo.c.a.a("android__log", "-------------writeFile with mode : " + this.g);
        try {
            File file = new File(this.b.getAbsolutePath() + ".tmp");
            RandomAccessFile randomAccessFile2 = new RandomAccessFile(file, "rw");
            try {
                if (this.g == 1) {
                    if (!"bytes".equalsIgnoreCase((String) this.a.g().get("Accept-Ranges"))) {
                        throw new RuntimeException("服务器出错，重新下载!");
                    } else if (this.q == null || this.q.length() == 0 || this.q.equals(this.a.g().get("ETag"))) {
                        String str = (String) this.a.g().get("Content-Range");
                        if (str != null) {
                            if (((long) Integer.valueOf(str.substring(6).split("[-/]")[0]).intValue()) == randomAccessFile2.length()) {
                                randomAccessFile2.seek(randomAccessFile2.length());
                            } else {
                                throw new RuntimeException("网络返回断点数据不正确");
                            }
                        }
                    } else {
                        s();
                        this.q = null;
                        throw new RuntimeException("服务器中文件eTag发生变化，重新下载!");
                    }
                }
                if (this.g == 1 && (this.q == null || this.q.length() == 0 || !this.q.equals(this.a.g().get("ETag")))) {
                    this.q = (String) this.a.g().get("ETag");
                    this.h.f(this);
                }
                com.kuguo.c.a.a("android__log", "-----------------downloadsize == " + i());
                com.kuguo.c.a.a("android__log", "-----------------totalsize == " + h());
                com.kuguo.c.a.a("android__log", "url------------ " + this.a.d().c());
                com.kuguo.c.a.a("android__log", "----------------mode == " + this.g);
                byte[] bArr = new byte[com.tencent.mobwin.core.b.b.c];
                int i2 = 0;
                while (this.c == 2 && (i2 = inputStream.read(bArr)) != -1) {
                    randomAccessFile2.write(bArr, 0, i2);
                    this.d += i2;
                    if (this.f != null) {
                        this.f.a(this, (long) i2);
                    }
                }
                int i3 = i2;
                file.renameTo(this.b);
                if (i3 == -1) {
                    a(4);
                }
                try {
                    randomAccessFile2.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                this.a.b();
            } catch (Exception e3) {
                e = e3;
                randomAccessFile = randomAccessFile2;
                try {
                    e.printStackTrace();
                    a(e);
                    try {
                        randomAccessFile.close();
                    } catch (IOException e4) {
                        e4.printStackTrace();
                    }
                    this.a.b();
                } catch (Throwable th) {
                    th = th;
                    try {
                        randomAccessFile.close();
                    } catch (IOException e5) {
                        e5.printStackTrace();
                    }
                    this.a.b();
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                randomAccessFile = randomAccessFile2;
                randomAccessFile.close();
                this.a.b();
                throw th;
            }
        } catch (Exception e6) {
            e = e6;
            randomAccessFile = null;
            e.printStackTrace();
            a(e);
            randomAccessFile.close();
            this.a.b();
        } catch (Throwable th3) {
            th = th3;
            randomAccessFile = null;
            randomAccessFile.close();
            this.a.b();
            throw th;
        }
    }

    private void a(Exception exc) {
        com.kuguo.c.a.a("android__log", "trytorecoverdownload");
        int i2 = this.j;
        this.j = i2 + 1;
        if (i2 < this.k) {
            r();
            return;
        }
        this.j = 0;
        this.l = exc;
        a(5);
    }

    private void q() {
        this.a.a(this);
        if (this.g == 1) {
            long o2 = o();
            this.d = (int) o2;
            this.a.d().a("Range", "bytes=" + o2 + "-");
        } else {
            this.d = 0;
            this.a.d().a("Range", "bytes=0-");
        }
        this.h = f.a();
    }

    private void r() {
        this.a = new a(f.a().a, new h(this.m));
        q();
        this.l = null;
        a(1);
        this.h.e(this);
    }

    private void s() {
        File file = new File(this.b.getAbsoluteFile() + ".tmp");
        if (file.exists() && file.isFile()) {
            file.delete();
        }
    }

    public String a() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        com.kuguo.c.a.a("android__log", "DownloadTask.java setState() " + i2);
        if (i2 != this.c) {
            this.c = i2;
            if ((i2 != 4 || !this.h.b()) && (i2 != 5 || !this.h.c())) {
                this.h.f(this);
            } else {
                f();
            }
            if (this.f != null) {
                this.f.a(this, i2);
            }
        }
    }

    public void a(c cVar) {
        this.f = cVar;
    }

    public void a(b bVar, int i2) {
        switch (this.c) {
            case 1:
            case 2:
                switch (i2) {
                    case -4:
                    case -3:
                        a(bVar.i());
                        return;
                    case -2:
                        return;
                    case 200:
                        a(2);
                        this.i = new BufferedInputStream(bVar.f(), com.tencent.mobwin.core.b.b.c);
                        this.e = -1;
                        this.e = this.d + h();
                        a((InputStream) this.i);
                        return;
                    default:
                        bVar.b();
                        return;
                }
            default:
                return;
        }
    }

    public void a(Object obj) {
        com.kuguo.c.a.a("android__log", "setTag : " + obj);
        this.n = obj;
    }

    public void a(String str) {
        this.o = str;
    }

    public b b() {
        return this.a;
    }

    public File c() {
        return this.b;
    }

    public void d() {
        if (p()) {
            a(4);
            return;
        }
        switch (this.c) {
            case 0:
                d b2 = this.h.b(this);
                if ((!this.h.a(this) || b2.c == 4 || b2.c == 5) && this.b != null) {
                    this.d = 0;
                    a(1);
                    this.h.c(this);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void e() {
        com.kuguo.c.a.a("android__log", "-------------task resume ++++++++" + this.c);
        switch (this.c) {
            case 3:
            case 5:
                r();
                return;
            case 4:
            default:
                return;
        }
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        String m2 = m();
        if (m2 == null || this.b == null) {
            return false;
        }
        return m2.equals(dVar.m()) && this.b.equals(dVar.c());
    }

    public void f() {
        a(0);
        this.h.d(this);
    }

    public int g() {
        return this.c;
    }

    public int h() {
        Map g2;
        String str;
        if (this.a == null) {
            return this.e;
        }
        if (this.e == -1 && this.a.h() == 200 && (g2 = this.a.g()) != null && (str = (String) g2.get("Content-Range")) != null) {
            this.e = Integer.valueOf(str.substring(str.lastIndexOf("/") + 1).trim()).intValue();
        }
        return this.e;
    }

    public int i() {
        return this.d;
    }

    public String j() {
        Map g2;
        if (this.p == null && this.a.h() == 200 && (g2 = this.a.g()) != null) {
            this.p = (String) g2.get("filename");
        }
        return this.p;
    }

    public String k() {
        return this.o;
    }

    public int l() {
        return this.g;
    }

    public String m() {
        return this.m;
    }

    public Object n() {
        com.kuguo.c.a.a("android__log", "getTag : " + this.n);
        return this.n;
    }

    public long o() {
        File file = new File(this.b.getAbsoluteFile() + ".tmp");
        if (!file.exists()) {
            return 0;
        }
        if (!file.isFile()) {
            return 0;
        }
        return file.length();
    }

    public boolean p() {
        if (this.b != null) {
            return this.b.exists();
        }
        return false;
    }

    public String toString() {
        return "[" + m() + " " + this.b + " " + this.c + " " + this.d + " " + this.e + "]";
    }
}
