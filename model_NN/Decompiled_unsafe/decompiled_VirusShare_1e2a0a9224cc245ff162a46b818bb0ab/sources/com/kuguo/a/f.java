package com.kuguo.a;

import android.content.Context;
import com.kuguo.b.d;
import com.kuguo.c.a;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class f {
    private static f b;
    Context a;
    private d c = new d();
    private LinkedList d = new LinkedList();
    private b e;
    private boolean f;
    private boolean g;

    private f(Context context) {
        a.a("DownloadManager实例化");
        b = this;
        this.a = context;
        this.e = new b(context);
        for (e dVar : this.e.a()) {
            d dVar2 = new d(context, dVar);
            int g2 = dVar2.g();
            if (g2 != 4) {
                switch (g2) {
                    case 1:
                    case 2:
                    case 3:
                        dVar2.a(5);
                        break;
                }
                this.d.add(dVar2);
            }
        }
        a.a("task size == " + this.d.size());
        Iterator it = this.d.iterator();
        while (it.hasNext()) {
            a.a(((d) it.next()).toString());
        }
    }

    public static f a() {
        return b;
    }

    public static f a(Context context) {
        if (b == null) {
            b = new f(context.getApplicationContext());
        }
        return b;
    }

    private e g(d dVar) {
        e eVar = new e();
        eVar.a = dVar.m();
        eVar.b = dVar.c().toString();
        eVar.e = dVar.g();
        eVar.d = dVar.h();
        eVar.c = dVar.i();
        eVar.f = dVar.j();
        eVar.g = dVar.k();
        eVar.h = dVar.l();
        eVar.i = dVar.a();
        return eVar;
    }

    public boolean a(d dVar) {
        return this.d.contains(dVar);
    }

    public d b(d dVar) {
        d dVar2;
        synchronized (this.d) {
            int i = 0;
            while (true) {
                if (i >= this.d.size()) {
                    dVar2 = null;
                    break;
                }
                dVar2 = (d) this.d.get(i);
                if (dVar2.equals(dVar)) {
                    break;
                }
                i++;
            }
        }
        return dVar2;
    }

    public boolean b() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public void c(d dVar) {
        synchronized (this.d) {
            this.d.add(dVar);
            this.e.a(g(dVar));
            this.c.a(dVar.b());
        }
    }

    public boolean c() {
        return this.g;
    }

    public List d() {
        return this.d;
    }

    public void d(d dVar) {
        synchronized (this.d) {
            if (this.d.remove(dVar)) {
                this.e.b(g(dVar));
                dVar.b().b();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void e(d dVar) {
        synchronized (this.d) {
            if (this.d.contains(dVar)) {
                dVar.b().e();
                this.c.a(dVar.b());
            }
        }
    }

    public void f(d dVar) {
        synchronized (this.d) {
            if (this.d.contains(dVar)) {
                this.e.c(g(dVar));
            }
        }
    }
}
