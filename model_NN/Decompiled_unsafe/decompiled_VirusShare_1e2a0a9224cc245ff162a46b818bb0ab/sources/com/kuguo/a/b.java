package com.kuguo.a;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;

class b {
    private a a;

    public b(Context context) {
        this.a = new a(context);
    }

    public List a() {
        ArrayList arrayList = new ArrayList();
        SQLiteDatabase readableDatabase = this.a.getReadableDatabase();
        Cursor query = readableDatabase.query("downloads", null, null, null, null, null, null);
        while (query.moveToNext()) {
            e eVar = new e();
            eVar.a = query.getString(query.getColumnIndex("url"));
            eVar.b = query.getString(query.getColumnIndex("file"));
            eVar.c = query.getInt(query.getColumnIndex("size"));
            eVar.d = query.getInt(query.getColumnIndex("total_size"));
            eVar.e = query.getInt(query.getColumnIndex("state"));
            eVar.f = query.getString(query.getColumnIndex("file_name"));
            eVar.g = query.getString(query.getColumnIndex("network_info"));
            eVar.h = query.getInt(query.getColumnIndex("download_mode"));
            eVar.i = query.getString(query.getColumnIndex("eTag"));
            arrayList.add(eVar);
        }
        query.close();
        readableDatabase.close();
        return arrayList;
    }

    public synchronized void a(e eVar) {
        SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("url", eVar.a);
        contentValues.put("file", eVar.b);
        contentValues.put("size", Integer.valueOf(eVar.c));
        contentValues.put("total_size", Integer.valueOf(eVar.d));
        contentValues.put("state", Integer.valueOf(eVar.e));
        contentValues.put("file_name", eVar.f);
        contentValues.put("network_info", eVar.g);
        contentValues.put("download_mode", Integer.valueOf(eVar.h));
        contentValues.put("eTag", eVar.i);
        writableDatabase.insert("downloads", null, contentValues);
        writableDatabase.close();
    }

    public void b(e eVar) {
        SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
        writableDatabase.delete("downloads", "url = ? and file = ?", new String[]{eVar.a, eVar.b});
        writableDatabase.close();
    }

    public synchronized void c(e eVar) {
        SQLiteDatabase writableDatabase = this.a.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("url", eVar.a);
        contentValues.put("file", eVar.b);
        contentValues.put("size", Integer.valueOf(eVar.c));
        contentValues.put("total_size", Integer.valueOf(eVar.d));
        contentValues.put("state", Integer.valueOf(eVar.e));
        contentValues.put("file_name", eVar.f);
        contentValues.put("network_info", eVar.g);
        contentValues.put("download_mode", Integer.valueOf(eVar.h));
        contentValues.put("eTag", eVar.i);
        writableDatabase.update("downloads", contentValues, "url = ? and file = ?", new String[]{eVar.a, eVar.b});
        writableDatabase.close();
    }
}
