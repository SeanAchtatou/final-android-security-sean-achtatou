package com.kuguo.c;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class c {
    private static Map a = new HashMap();
    private static int b;

    public static Bitmap a(Context context, String str) {
        if (a.containsKey(str)) {
            return (Bitmap) a.get(str);
        }
        try {
            Bitmap decodeStream = BitmapFactory.decodeStream(new FileInputStream(str));
            a.put(str, decodeStream);
            return decodeStream;
        } catch (IOException e) {
            return Bitmap.createBitmap(50, 50, Bitmap.Config.RGB_565);
        }
    }

    public static Drawable a(Context context, Bitmap bitmap) {
        if (b == 0) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            b = displayMetrics.densityDpi;
        }
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bitmap);
        bitmapDrawable.setTargetDensity((int) (((float) b) * ((((float) b) * 1.0f) / 240.0f)));
        return bitmapDrawable;
    }

    public static Drawable b(Context context, String str) {
        return a(context, a(context, str));
    }
}
