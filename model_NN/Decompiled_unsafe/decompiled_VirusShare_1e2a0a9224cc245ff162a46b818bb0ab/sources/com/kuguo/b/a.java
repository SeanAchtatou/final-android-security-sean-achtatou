package com.kuguo.b;

import android.content.Context;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

public class a extends b {
    private HttpClient b;

    public a(Context context, h hVar) {
        super(context, hVar);
    }

    public void a() {
        HttpGet httpPost;
        com.kuguo.c.a.a("android__log", "------ establish-------------");
        if (!c()) {
            return;
        }
        if (!l()) {
            a(-4);
            return;
        }
        a((Exception) null);
        a(-2);
        this.b = new DefaultHttpClient();
        float j = j();
        if (j > 0.0f) {
            this.b.getParams().setParameter("http.socket.timeout", Integer.valueOf((int) (j * 1000.0f)));
            this.b.getParams().setParameter("http.connection.timeout", Integer.valueOf((int) (j * 1000.0f)));
        }
        e k = k();
        if (k != null) {
            this.b.getParams().setParameter("http.route.default-proxy", new HttpHost(k.a(), k.b(), "http"));
        }
        h d = d();
        if (d.b() == 0) {
            httpPost = new HttpGet(d.toString());
        } else {
            httpPost = new HttpPost(d.c());
            try {
                httpPost.setEntity(new StringEntity(d.d()));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        Map a = d.a();
        for (String str : a.keySet()) {
            httpPost.addHeader(str, a.get(str).toString());
        }
        try {
            com.kuguo.c.a.a("jerry", "---uri === " + httpPost.getURI());
            HttpResponse execute = this.b.execute(httpPost);
            int statusCode = execute.getStatusLine().getStatusCode();
            com.kuguo.c.a.a("android__log", "connection status : " + statusCode);
            HashMap hashMap = new HashMap();
            Header[] allHeaders = execute.getAllHeaders();
            com.kuguo.c.a.a("android__log", "--------------response header start------------------");
            for (Header header : allHeaders) {
                hashMap.put(header.getName(), header.getValue());
                com.kuguo.c.a.a("android__log", header.getName() + " --- " + header.getValue());
            }
            com.kuguo.c.a.a("android__log", "--------------response header end ------------------");
            a(hashMap);
            a(execute.getEntity().getContent());
            if (("" + statusCode).startsWith("2")) {
                a(200);
            }
            com.kuguo.c.a.a("网络访问完成");
        } catch (Exception e2) {
            com.kuguo.c.a.a("网络链接异常");
            a(e2);
            a(-3);
        }
    }

    public void b() {
        if (!c()) {
            if (this.b != null) {
                this.b.getConnectionManager().shutdown();
            }
            a((Map) null);
            a((InputStream) null);
            a((Exception) null);
            a(-3);
        }
    }
}
