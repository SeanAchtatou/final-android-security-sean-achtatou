package com.kuguo.b;

import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

class f extends ConcurrentLinkedQueue {
    final /* synthetic */ d a;
    /* access modifiers changed from: private */
    public Thread b;
    private int c = new Random().nextInt();

    public f(d dVar) {
        this.a = dVar;
    }

    public void a() {
        Iterator it = iterator();
        while (it.hasNext()) {
            if (((b) it.next()).h() == -3) {
                it.remove();
            }
        }
    }

    /* renamed from: a */
    public boolean add(b bVar) {
        boolean add = super.add(bVar);
        if (add && this.b == null) {
            this.b = new g(this);
            this.b.start();
        }
        return add;
    }

    public String toString() {
        return "HttpConnectionQueue@" + this.c;
    }
}
