package com.kuguo.b;

import com.kuguo.c.b;
import com.tencent.lbsapi.core.e;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

public class h {
    private String a;
    private Map b = new LinkedHashMap();
    private Map c = new LinkedHashMap();
    private int d = 0;
    private boolean e;

    public h(String str) {
        this.a = str;
    }

    public Map a() {
        return this.c;
    }

    public void a(String str, Object obj) {
        this.c.put(str, obj);
    }

    public int b() {
        return this.d;
    }

    public String c() {
        return this.a;
    }

    public String d() {
        if (this.b.isEmpty()) {
            return "";
        }
        ArrayList arrayList = new ArrayList();
        for (String str : this.b.keySet()) {
            Object obj = this.b.get(str);
            if (obj != null) {
                arrayList.add(new BasicNameValuePair(str, obj.toString()));
            }
        }
        String format = URLEncodedUtils.format(arrayList, e.e);
        return this.e ? b.a(format) : format;
    }

    public String toString() {
        String d2 = d();
        return d2.equals("") ? this.a : this.a + "?" + d2;
    }
}
