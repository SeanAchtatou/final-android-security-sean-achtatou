package com.kuguo.b;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.io.InputStream;
import java.util.Map;

public abstract class b {
    protected Context a;
    private h b;
    private Exception c;
    private int d = -1;
    private Map e;
    private c f;
    private float g = 30.0f;
    private e h;
    private InputStream i;

    public b(Context context, h hVar) {
        this.a = context;
        this.b = hVar;
    }

    public abstract void a();

    /* access modifiers changed from: protected */
    public void a(int i2) {
        if (i2 != this.d) {
            this.d = i2;
            if (this.f != null) {
                this.f.a(this, i2);
            }
        }
    }

    public void a(c cVar) {
        this.f = cVar;
    }

    /* access modifiers changed from: protected */
    public void a(InputStream inputStream) {
        this.i = inputStream;
    }

    /* access modifiers changed from: protected */
    public void a(Exception exc) {
        this.c = exc;
    }

    /* access modifiers changed from: protected */
    public void a(Map map) {
        this.e = map;
    }

    public abstract void b();

    public boolean c() {
        switch (this.d) {
            case -4:
            case -3:
            case -1:
                return true;
            case -2:
            default:
                return false;
        }
    }

    public h d() {
        return this.b;
    }

    public void e() {
        b();
        this.d = -1;
    }

    public InputStream f() {
        return this.i;
    }

    public Map g() {
        return this.e;
    }

    public int h() {
        return this.d;
    }

    public Exception i() {
        return this.c;
    }

    public float j() {
        return this.g;
    }

    public e k() {
        if (this.h == null) {
            try {
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.a.getSystemService("connectivity")).getActiveNetworkInfo();
                if (activeNetworkInfo != null) {
                    String extraInfo = activeNetworkInfo.getExtraInfo();
                    if ("cmwap".equals(extraInfo) || "uniwap".equals(extraInfo) || "cmwap".equals(extraInfo) || "ctwap:cdma".equals(extraInfo)) {
                        return new e("10.0.0.172", 80);
                    }
                }
            } catch (Exception e2) {
            }
        }
        return this.h;
    }

    public boolean l() {
        try {
            return ((ConnectivityManager) this.a.getSystemService("connectivity")).getActiveNetworkInfo() != null;
        } catch (Exception e2) {
            return true;
        }
    }
}
