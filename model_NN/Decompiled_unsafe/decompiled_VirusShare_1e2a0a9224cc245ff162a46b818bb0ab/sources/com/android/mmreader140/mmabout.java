package com.android.mmreader140;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import java.io.FileNotFoundException;

public class mmabout extends Activity implements View.OnClickListener {
    public static final int MORE_ID = 33;
    static final int RG_REQUEST = 0;
    public static final int SITE_ID = 31;
    public static final int TELL_ID = 32;
    private final int WC = -2;
    private appcell appCell;
    TextView buy;
    TextView help;
    TextView mail;
    private int myid;
    ImageView sitelogo;
    TextView sitemsg;
    TextView sitename;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.appCell = (appcell) getApplication();
        RelativeLayout relativeLayout = new RelativeLayout(this);
        setContentView(relativeLayout);
        relativeLayout.setGravity(1);
        int displayHeight = this.appCell.getdisplayHeight();
        int topmargin = 80;
        int eachmargin = 20;
        int adsmargin = 250;
        if (displayHeight <= 800 && displayHeight > 480) {
            topmargin = 60;
            eachmargin = 15;
            adsmargin = 200;
        } else if (displayHeight <= 480 && displayHeight > 320) {
            topmargin = 50;
            eachmargin = 10;
            adsmargin = 50;
        } else if (displayHeight <= 320 && displayHeight > 240) {
            topmargin = 35;
            eachmargin = 5;
            adsmargin = 30;
        } else if (displayHeight <= 240) {
            topmargin = 25;
            eachmargin = 2;
            adsmargin = 10;
        }
        relativeLayout.setPadding(0, topmargin, 0, 0);
        TableLayout tableLayout = new TableLayout(this);
        relativeLayout.addView(tableLayout, new RelativeLayout.LayoutParams(-2, -2));
        tableLayout.setId(1);
        TableRow tableRow = new TableRow(this);
        tableLayout.addView(tableRow, new TableLayout.LayoutParams(-2, -2));
        this.sitelogo = new ImageView(this);
        tableRow.addView(this.sitelogo);
        this.sitelogo.setImageResource(R.drawable.weblogo);
        this.sitelogo.setScaleType(ImageView.ScaleType.CENTER);
        TableLayout.LayoutParams layoutParams = new TableLayout.LayoutParams(-2, -2);
        layoutParams.topMargin = eachmargin;
        TableRow tableRow2 = new TableRow(this);
        tableLayout.addView(tableRow2, layoutParams);
        this.sitename = new TextView(this);
        this.sitename.setTextColor(-16776961);
        this.sitename.setTextSize(18.0f);
        this.sitename.setText(Html.fromHtml("<a href=http://www.welovemm.cn>搜象文学网</a>"));
        this.sitename.setMovementMethod(LinkMovementMethod.getInstance());
        this.sitename.setGravity(1);
        tableRow2.addView(this.sitename);
        TableLayout.LayoutParams layoutParams2 = new TableLayout.LayoutParams(-2, -2);
        layoutParams2.topMargin = eachmargin / 2;
        TableRow tableRow3 = new TableRow(this);
        tableLayout.addView(tableRow3, layoutParams2);
        this.sitemsg = new TextView(this);
        this.sitemsg.setTextColor(-16776961);
        this.sitemsg.setTextSize(15.0f);
        this.sitemsg.setText("打开搜象文学，被窝里也可以读书");
        this.sitemsg.setGravity(1);
        tableRow3.addView(this.sitemsg);
        TableLayout.LayoutParams layoutParams3 = new TableLayout.LayoutParams(-2, -2);
        layoutParams3.topMargin = eachmargin;
        TableRow tableRow4 = new TableRow(this);
        tableLayout.addView(tableRow4, layoutParams3);
        this.mail = new TextView(this);
        this.mail.setTextColor(-16777216);
        this.mail.setTextSize(14.0f);
        this.mail.setText("联系方式：chushula@sina.com\n搜象QQ群：186706166,186714030\n南京搜象网络科技有限公司制作");
        this.mail.setGravity(1);
        tableRow4.addView(this.mail);
        TableLayout.LayoutParams layoutParams4 = new TableLayout.LayoutParams(-2, -2);
        layoutParams4.topMargin = eachmargin;
        TableRow tableRow5 = new TableRow(this);
        tableLayout.addView(tableRow5, layoutParams4);
        this.buy = new TextView(this);
        this.buy.setTextSize(15.0f);
        String imsi = this.appCell.getimsi();
        String imei = this.appCell.getimei();
        String tel = this.appCell.getitel();
        int width = this.appCell.getdisplayWidth();
        int height = this.appCell.getdisplayHeight();
        String str = "http://www.welovemm.cn/wostore3.php?os=1&vs=25&xmlid=" + this.myid + "&" + "type=4" + "&" + "tel=" + tel + "&" + "imei=" + imei + "&" + "imsi=" + imsi + "&" + "width=" + String.valueOf(width) + "&" + "height=" + String.valueOf(height) + "&" + "session=" + this.appCell.getsession();
        String str2 = this.appCell.getbuyname();
        this.buy.setText(Html.fromHtml(""));
        this.buy.setMovementMethod(LinkMovementMethod.getInstance());
        this.buy.setGravity(1);
        tableRow5.addView(this.buy);
        TableLayout.LayoutParams layoutParams5 = new TableLayout.LayoutParams(-2, -2);
        layoutParams5.topMargin = eachmargin * 2;
        TableRow tableRow6 = new TableRow(this);
        tableLayout.addView(tableRow6, layoutParams5);
        this.help = new TextView(this);
        this.help.setTextColor(-16777216);
        this.help.setTextSize(14.0f);
        this.help.setText("小技巧(当前版本v1.67.01)：\n单击屏幕上下方可以上下滚屏\n长按屏幕中间可切换夜晚模式\n夜晚模式晚上十点后自动开启");
        this.help.setGravity(1);
        tableRow6.addView(this.help);
        TableRow tableRow7 = new TableRow(this);
        TableLayout.LayoutParams layoutParams6 = new TableLayout.LayoutParams(-1, -1);
        layoutParams6.topMargin = adsmargin;
        tableLayout.addView(tableRow7, layoutParams6);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setGravity(1);
        tableRow7.addView(linearLayout);
        relativeLayout.setBackgroundResource(R.drawable.bg);
        this.myid = this.appCell.getmyid();
        boolean isselfads = false;
        String adsids = "";
        int i = 1;
        while (true) {
            if (i > 20) {
                break;
            }
            adsids = String.valueOf(this.appCell.getid(i));
            String charts = String.valueOf(this.appCell.getchart(i));
            if (charts.length() > 0) {
                int chart = -5;
                try {
                    chart = Integer.parseInt(charts);
                } catch (NumberFormatException e) {
                }
                if (chart == 0 || chart == -4) {
                    isselfads = true;
                }
            }
            i++;
        }
        isselfads = true;
        if (isselfads) {
            String adsimgfile = "adsimg" + adsids + ".png";
            if (this.appCell.fileIsExists("/data/data/com.android.mmreader" + this.myid + "/files/adsimg" + adsids + ".png")) {
                int id = 0;
                try {
                    id = Integer.parseInt(adsids);
                } catch (NumberFormatException e2) {
                }
                int bmpwidth = 0;
                int bmpheight = 0;
                ImageView imageView = new ImageView(this);
                linearLayout.addView(imageView);
                try {
                    Bitmap bitmap = BitmapFactory.decodeStream(openFileInput(adsimgfile));
                    bmpwidth = bitmap.getWidth();
                    bmpheight = bitmap.getHeight();
                    imageView.setImageBitmap(bitmap);
                } catch (FileNotFoundException e3) {
                }
                imageView.setOnClickListener(this);
                imageView.setId(id);
                int adsWidth = this.appCell.getdisplayWidth() - 10;
                ViewGroup.LayoutParams para = imageView.getLayoutParams();
                para.width = adsWidth;
                para.height = (int) (((float) adsWidth) * (((float) bmpheight) / ((float) bmpwidth)));
                imageView.setLayoutParams(para);
                imageView.setAdjustViewBounds(false);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        }
    }

    public void onClick(View v) {
        int adsid = v.getId();
        String imsi = this.appCell.getimsi();
        String imei = this.appCell.getimei();
        String tel = this.appCell.getitel();
        int width = this.appCell.getdisplayWidth();
        int height = this.appCell.getdisplayHeight();
        Intent intent1 = new Intent("android.intent.action.VIEW");
        intent1.setData(Uri.parse("http://www.welovemm.cn/phoneadsclick3.php?os=1&vs=25&xmlid=" + this.myid + "&" + "adsid=" + adsid + "&" + "tel=" + tel + "&" + "imei=" + imei + "&" + "imsi=" + imsi + "&" + "width=" + String.valueOf(width) + "&" + "height=" + String.valueOf(height) + "&" + "session=" + this.appCell.getsession()));
        startActivity(intent1);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 31, 0, "搜象");
        menu.add(0, 32, 1, "牢骚");
        menu.add(0, 33, 2, "更多 >>");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        String imsi = this.appCell.getimsi();
        String imei = this.appCell.getimei();
        String tel = this.appCell.getitel();
        int width = this.appCell.getdisplayWidth();
        int height = this.appCell.getdisplayHeight();
        String session = this.appCell.getsession();
        switch (item.getItemId()) {
            case SITE_ID /*31*/:
                Intent intent1 = new Intent("android.intent.action.VIEW");
                intent1.setData(Uri.parse("http://www.welovemm.cn/wostore3.php?os=1&vs=25&xmlid=" + this.myid + "&" + "type=5" + "&" + "tel=" + tel + "&" + "imei=" + imei + "&" + "imsi=" + imsi + "&" + "width=" + String.valueOf(width) + "&" + "height=" + String.valueOf(height) + "&" + "session=" + session));
                startActivity(intent1);
                break;
            case 32:
                Intent intent12 = new Intent("android.intent.action.VIEW");
                intent12.setData(Uri.parse("http://www.welovemm.cn/wostore3.php?os=1&vs=25&xmlid=" + this.myid + "&" + "type=6" + "&" + "tel=" + tel + "&" + "imei=" + imei + "&" + "imsi=" + imsi + "&" + "width=" + String.valueOf(width) + "&" + "height=" + String.valueOf(height) + "&" + "session=" + session));
                startActivity(intent12);
                break;
            case MORE_ID /*33*/:
                Intent intent13 = new Intent("android.intent.action.VIEW");
                intent13.setData(Uri.parse("http://www.welovemm.cn/wostore3.php?os=1&vs=25&xmlid=" + this.myid + "&" + "type=7" + "&" + "tel=" + tel + "&" + "imei=" + imei + "&" + "imsi=" + imsi + "&" + "width=" + String.valueOf(width) + "&" + "height=" + String.valueOf(height) + "&" + "session=" + session));
                startActivity(intent13);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
