package com.tencent.mobwin.core.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.StateListDrawable;
import android.view.View;
import android.widget.ImageButton;
import com.tencent.mobwin.core.A;

public class g extends ImageButton {
    private Context a;
    private StateListDrawable b;
    private StateListDrawable c;
    private Bitmap d = null;

    public g(Context context) {
        super(context);
        this.a = context;
        setPadding(0, 0, 0, 0);
    }

    private StateListDrawable a(Bitmap bitmap, Bitmap bitmap2) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        BitmapDrawable bitmapDrawable = new BitmapDrawable(this.a.getResources(), bitmap);
        stateListDrawable.addState(View.PRESSED_ENABLED_STATE_SET, new BitmapDrawable(this.a.getResources(), bitmap2));
        stateListDrawable.addState(View.ENABLED_FOCUSED_STATE_SET, bitmapDrawable);
        stateListDrawable.addState(View.ENABLED_STATE_SET, bitmapDrawable);
        stateListDrawable.addState(View.EMPTY_STATE_SET, bitmapDrawable);
        return stateListDrawable;
    }

    private StateListDrawable b(Bitmap bitmap, Bitmap bitmap2) {
        StateListDrawable stateListDrawable = new StateListDrawable();
        BitmapDrawable bitmapDrawable = new BitmapDrawable(this.a.getResources(), bitmap);
        stateListDrawable.addState(View.PRESSED_ENABLED_STATE_SET, new BitmapDrawable(this.a.getResources(), bitmap2));
        stateListDrawable.addState(View.ENABLED_FOCUSED_STATE_SET, bitmapDrawable);
        stateListDrawable.addState(View.ENABLED_STATE_SET, bitmapDrawable);
        stateListDrawable.addState(View.EMPTY_STATE_SET, bitmapDrawable);
        return stateListDrawable;
    }

    public void a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3) {
        this.b = a(bitmap, bitmap2);
        this.d = bitmap3;
        setImageDrawable(this.b);
        this.c = b(null, A.a().a("toolbar_body_pressed.png", 0));
        setBackgroundDrawable(this.c);
    }

    public void b(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3) {
        this.b = a(bitmap, bitmap2);
        this.d = bitmap3;
        setImageDrawable(this.b);
        this.c = b(null, null);
        setBackgroundDrawable(this.c);
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (this.d != null) {
            if (z) {
                setImageDrawable(this.b);
            } else {
                setImageBitmap(this.d);
            }
        }
    }
}
