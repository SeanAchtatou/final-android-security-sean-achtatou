package com.tencent.mobwin.core.a;

import MobWin.AppInfo;
import MobWin.Cell;
import MobWin.GPS;
import MobWin.UserInfo;
import MobWin.UserLocation;
import android.content.Context;
import com.tencent.mobwin.core.o;
import com.tencent.mobwin.core.w;
import com.tencent.mobwin.utils.b;
import java.util.Map;

public class c {
    private Map a;
    private Map b = f.b();
    private UserInfo c;
    private AppInfo d;

    public c(Context context) {
        this.a = f.a(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.mobwin.utils.b.a(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.mobwin.utils.b.a(int, android.content.Context):int
      com.tencent.mobwin.utils.b.a(java.lang.String, int):int
      com.tencent.mobwin.utils.b.a(java.lang.String, short):short
      com.tencent.mobwin.utils.b.a(java.lang.String, long):long */
    public UserInfo a(Context context) {
        String str = (String) this.a.get(f.f);
        String str2 = (String) this.a.get(f.g);
        String str3 = (String) this.a.get(f.h);
        String str4 = (String) this.a.get(f.c);
        String str5 = (String) this.a.get(f.d);
        String str6 = (String) this.a.get(f.e);
        o.b("IORY", "安全等级:" + d.a());
        o.b("IORY", "IMEI:" + str);
        o.b("IORY", "meid:" + str2);
        switch (d.a()) {
            case 0:
                this.c = new UserInfo();
                this.c.setBrand((String) this.b.get(f.m));
                this.c.setManufacturer((String) this.b.get(f.n));
                this.c.setModel((String) this.b.get(f.o));
                this.c.setOs("Android " + ((String) this.b.get(f.q)));
                UserInfo userInfo = this.c;
                if (str == null) {
                    str = "-";
                }
                userInfo.setImei(str);
                UserInfo userInfo2 = this.c;
                if (str2 == null) {
                    str2 = "-";
                }
                userInfo2.setMeid(str2);
                this.c.setImsi(str3 == null ? "-" : str3);
                this.c.setMac((String) this.a.get(f.r));
                this.c.setLanguage((String) this.a.get(f.s));
                this.c.setPhone_number(b.a((String) this.a.get(f.b), -1L));
                this.c.setScreen_x((short) b.a(context));
                this.c.setScreen_y((short) b.b(context));
                this.c.setDm((short) b.c(context));
                if (str4 == null || "-".equals(str4)) {
                    this.c.setTel_support((byte) 0);
                } else if ("true".equals(str4)) {
                    this.c.setTel_support((byte) 1);
                } else if ("false".equals(str4)) {
                    this.c.setTel_support((byte) -1);
                }
                if (str5 == null || "-".equals(str5)) {
                    this.c.setWifi_support((byte) 0);
                } else if ("true".equals(str5)) {
                    this.c.setWifi_support((byte) 1);
                } else if ("false".equals(str5)) {
                    this.c.setWifi_support((byte) -1);
                }
                if (str6 == null || "-".equals(str6)) {
                    this.c.setLwp_support((byte) 0);
                } else if ("true".equals(str6)) {
                    this.c.setLwp_support((byte) 1);
                } else if ("false".equals(str6)) {
                    this.c.setLwp_support((byte) -1);
                }
                f.f(context);
                this.c.setNet_type(f.z);
                String str7 = f.A;
                if (str7.equals("-")) {
                    str7 = f.a();
                }
                this.c.setIp(str7);
                break;
            case 1:
                this.c = new UserInfo();
                this.c.setBrand((String) this.b.get(f.m));
                this.c.setModel((String) this.b.get(f.o));
                this.c.setManufacturer((String) this.b.get(f.n));
                this.c.setOs("Android " + ((String) this.b.get(f.q)));
                if (str4 == null || "-".equals(str4)) {
                    this.c.setTel_support((byte) 0);
                } else if ("true".equals(str4)) {
                    this.c.setTel_support((byte) 1);
                } else if ("false".equals(str4)) {
                    this.c.setTel_support((byte) -1);
                }
                if (str5 == null || "-".equals(str5)) {
                    this.c.setWifi_support((byte) 0);
                } else if ("true".equals(str5)) {
                    this.c.setWifi_support((byte) 1);
                } else if ("false".equals(str5)) {
                    this.c.setWifi_support((byte) -1);
                }
                if (str6 == null || "-".equals(str6)) {
                    this.c.setLwp_support((byte) 0);
                } else if ("true".equals(str6)) {
                    this.c.setLwp_support((byte) 1);
                } else if ("false".equals(str6)) {
                    this.c.setLwp_support((byte) -1);
                }
                UserInfo userInfo3 = this.c;
                if (str == null) {
                    str = "-";
                }
                userInfo3.setImei(str);
                UserInfo userInfo4 = this.c;
                if (str2 == null) {
                    str2 = "-";
                }
                userInfo4.setMeid(str2);
                this.c.setMac((String) this.a.get(f.r));
                this.c.setLanguage((String) this.a.get(f.s));
                this.c.setScreen_x((short) b.a(context));
                this.c.setScreen_y((short) b.b(context));
                this.c.setDm((short) b.c(context));
                f.f(context);
                this.c.setNet_type(f.z);
                String str8 = f.A;
                if (str8.equals("-")) {
                    str8 = f.a();
                }
                this.c.setIp(str8);
                break;
            case 2:
                this.c = new UserInfo();
                this.c.setBrand((String) this.b.get(f.m));
                this.c.setManufacturer((String) this.b.get(f.n));
                this.c.setModel((String) this.b.get(f.o));
                if (str4 == null || "-".equals(str4)) {
                    this.c.setTel_support((byte) 0);
                } else if ("true".equals(str4)) {
                    this.c.setTel_support((byte) 1);
                } else if ("false".equals(str4)) {
                    this.c.setTel_support((byte) -1);
                }
                if (str5 == null || "-".equals(str5)) {
                    this.c.setWifi_support((byte) 0);
                } else if ("true".equals(str5)) {
                    this.c.setWifi_support((byte) 1);
                } else if ("false".equals(str5)) {
                    this.c.setWifi_support((byte) -1);
                }
                if (str6 == null || "-".equals(str6)) {
                    this.c.setLwp_support((byte) 0);
                } else if ("true".equals(str6)) {
                    this.c.setLwp_support((byte) 1);
                } else if ("false".equals(str6)) {
                    this.c.setLwp_support((byte) -1);
                }
                UserInfo userInfo5 = this.c;
                if (str == null) {
                    str = "-";
                }
                userInfo5.setImei(str);
                UserInfo userInfo6 = this.c;
                if (str2 == null) {
                    str2 = "-";
                }
                userInfo6.setMeid(str2);
                this.c.setOs("Android " + ((String) this.b.get(f.q)));
                this.c.setLanguage((String) this.a.get(f.s));
                this.c.setScreen_x((short) b.a(context));
                this.c.setScreen_y((short) b.b(context));
                this.c.setDm((short) b.c(context));
                break;
            case 3:
                this.c = new UserInfo();
                break;
        }
        o.a("SDK", new StringBuilder().append(this.c).toString());
        return this.c;
    }

    public AppInfo b(Context context) {
        if (this.d == null) {
            this.d = new AppInfo();
            this.d.setApp_id("0");
            this.d.setApp_key(w.c());
            this.d.setRelease_channel(w.d());
            this.d.setApp_name(context.getPackageName());
            this.d.setApp_signature(f.b(context));
            String c2 = f.c(context);
            this.d.setIntegrateId(w.a());
            this.d.setApp_version(c2);
            this.d.setAux_key("");
            this.d.setSdk_version(w.h());
        }
        int i = 1;
        if (w.f()) {
            i = 0;
        }
        this.d.setApp_mode(i);
        return this.d;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.mobwin.utils.b.a(java.lang.String, short):short
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.mobwin.utils.b.a(int, android.content.Context):int
      com.tencent.mobwin.utils.b.a(java.lang.String, int):int
      com.tencent.mobwin.utils.b.a(java.lang.String, long):long
      com.tencent.mobwin.utils.b.a(java.lang.String, short):short */
    public UserLocation c(Context context) {
        if (d.a() != 0) {
            return null;
        }
        UserLocation userLocation = new UserLocation();
        f.e(context);
        GPS gps = new GPS();
        gps.setEType(0);
        gps.setIAlt(f.y);
        gps.setILat(f.x);
        gps.setILon(f.w);
        gps.setVLBSKeyData(w.b());
        userLocation.setGps(gps);
        Cell cell = new Cell();
        cell.setICellId(b.a((String) this.a.get(f.k), -1));
        cell.setILac(b.a((String) this.a.get(f.l), -1));
        cell.setShMcc(b.a((String) this.a.get(f.i), (short) -1));
        cell.setShMnc(b.a((String) this.a.get(f.j), (short) -1));
        userLocation.setCell(cell);
        return userLocation;
    }
}
