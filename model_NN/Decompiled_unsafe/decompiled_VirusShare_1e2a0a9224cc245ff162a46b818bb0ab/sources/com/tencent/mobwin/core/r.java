package com.tencent.mobwin.core;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.tencent.mobwin.core.b.a;
import com.tencent.mobwin.core.b.b;
import org.apache.http.HttpResponse;

class r implements a {
    final /* synthetic */ t h;
    private final /* synthetic */ Context i;
    private final /* synthetic */ int j;
    private final /* synthetic */ Handler k;

    r(t tVar, Context context, int i2, Handler handler) {
        this.h = tVar;
        this.i = context;
        this.j = i2;
        this.k = handler;
    }

    public void a(int i2, b bVar) {
        Message obtainMessage = this.k.obtainMessage(8);
        obtainMessage.arg1 = i2;
        this.k.sendMessage(obtainMessage);
    }

    public void a(HttpResponse httpResponse, b bVar) {
        try {
            byte[] a = t.b(httpResponse.getEntity().getContent());
            x.a(com.tencent.mobwin.utils.b.b(bVar.f), a, this.i);
            Message obtain = Message.obtain();
            if (this.j == 1) {
                obtain.what = 13;
            } else if (this.j == 2) {
                obtain.what = 17;
            } else if (this.j == 3) {
                obtain.what = 15;
            } else {
                obtain.what = 7;
            }
            if (com.tencent.mobwin.utils.b.c(bVar.f).toLowerCase().endsWith(".gif")) {
                obtain.arg1 = 1;
                obtain.obj = a;
            } else {
                Bitmap decodeByteArray = BitmapFactory.decodeByteArray(a, 0, a.length);
                obtain.arg1 = 0;
                obtain.obj = decodeByteArray;
            }
            Bundle bundle = new Bundle();
            bundle.putString("url", bVar.f);
            obtain.setData(bundle);
            this.k.sendMessage(obtain);
        } catch (Exception e) {
            throw new z();
        }
    }
}
