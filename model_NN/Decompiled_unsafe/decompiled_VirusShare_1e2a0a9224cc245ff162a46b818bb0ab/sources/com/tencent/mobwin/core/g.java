package com.tencent.mobwin.core;

import MobWin.ResAppLaunch;
import MobWin.ResGetAD;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import com.tencent.mobwin.core.a.e;
import com.tencent.mobwin.core.view.d;
import com.tencent.mobwin.utils.b;

class g extends Handler {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ w a;
    /* access modifiers changed from: private */
    public d b;

    g(w wVar) {
        this.a = wVar;
    }

    public void handleMessage(Message message) {
        if (!this.a.ae) {
            try {
                switch (message.what) {
                    case 1:
                        o.a("SDK", "ACTIVE_SUCCESS");
                        e.a(this.a.getContext());
                        w.U = true;
                        return;
                    case 2:
                        o.a("SDK", "ACTIVE_Failed:" + message.arg1);
                        return;
                    case 3:
                        o.a("SDK", "APP_LAUNCH_REPORT_SUCCESS:" + message.arg1);
                        w.U = true;
                        if (message.obj != null) {
                            ResAppLaunch resAppLaunch = (ResAppLaunch) message.obj;
                            w.n.a(this.a.getContext(), resAppLaunch.c, resAppLaunch.b);
                            if (!(w.R == null || w.n == null)) {
                                w.R.setAppSetting(w.n.b());
                                w.R.setSysSetting(w.n.c());
                            }
                            this.a.g();
                            this.a.n();
                            this.a.q();
                            return;
                        }
                        return;
                    case 4:
                        o.a("SDK", "APP_LAUNCH_REPORT_FAILED:" + message.arg1);
                        this.a.n();
                        return;
                    case 5:
                        if (message.obj instanceof ResGetAD) {
                            this.a.a((ResGetAD) message.obj);
                            return;
                        } else {
                            this.a.a(message.arg1);
                            return;
                        }
                    case 6:
                        this.a.a(message.arg1);
                        return;
                    case 7:
                        String string = message.getData().getString("url");
                        if (string != null && string.equals(this.a.V.d) && message.obj != null) {
                            if (message.arg1 == 1) {
                                try {
                                    this.b = new d((byte[]) message.obj, new v(this));
                                    new u(this).start();
                                    return;
                                } catch (Exception e) {
                                    x.a(b.b(string), this.a.getContext());
                                    this.a.b(11);
                                    return;
                                }
                            } else {
                                this.a.W = this.a.ai.b;
                                this.a.ai.b = (Bitmap) message.obj;
                                this.a.ai.a = this.a.V;
                                this.a.z();
                                return;
                            }
                        } else {
                            return;
                        }
                    case 8:
                        this.a.b(12);
                        return;
                    case 9:
                        e.c(this.a.getContext(), String.valueOf(this.a.ai.a.a));
                        return;
                    case 10:
                        return;
                    case 11:
                    case t.n /*14*/:
                    case t.p /*16*/:
                    default:
                        return;
                    case 12:
                        SharedPreferences.Editor edit = this.a.getContext().getSharedPreferences("mobwin", 0).edit();
                        edit.putBoolean("REPORTADPLAYDATAFAILED", true);
                        edit.commit();
                        com.tencent.mobwin.core.a.b.a(this.a.getContext(), this.a.ag);
                        return;
                    case 13:
                        if (this.a.ai.c == null) {
                            this.a.ai.c = (Bitmap) message.obj;
                            this.a.l();
                            return;
                        }
                        return;
                    case 15:
                        String string2 = message.getData().getString("url");
                        try {
                            Bitmap bitmap = (Bitmap) message.obj;
                            if (bitmap != null) {
                                bitmap.setDensity(0);
                            }
                            String lastPathSegment = Uri.parse(string2).getLastPathSegment();
                            A.a().e().put(lastPathSegment, bitmap);
                            o.a("AdModel", "download bannerRes ok file:" + lastPathSegment);
                        } catch (Exception e2) {
                        }
                        if (this.a.ai != null && this.a.ai.a != null) {
                            this.a.l();
                            return;
                        }
                        return;
                    case 17:
                        String string3 = message.getData().getString("url");
                        try {
                            Bitmap bitmap2 = (Bitmap) message.obj;
                            if (bitmap2 != null) {
                                bitmap2.setDensity(0);
                            }
                            String lastPathSegment2 = Uri.parse(string3).getLastPathSegment();
                            A.a().e().put(lastPathSegment2, bitmap2);
                            o.a("AdModel", "download embedbrowserRes ok file:" + lastPathSegment2);
                            return;
                        } catch (Exception e3) {
                            return;
                        }
                }
            } catch (Exception e4) {
            }
        }
    }
}
