package com.tencent.mobwin;

import android.view.View;

class g implements View.OnClickListener {
    final /* synthetic */ MobinWINBrowserActivity a;

    g(MobinWINBrowserActivity mobinWINBrowserActivity) {
        this.a = mobinWINBrowserActivity;
    }

    public void onClick(View view) {
        String url;
        if (this.a.j != null && (url = this.a.j.getUrl()) != null) {
            MobinWINBrowserActivity.c(url, this.a);
        }
    }
}
