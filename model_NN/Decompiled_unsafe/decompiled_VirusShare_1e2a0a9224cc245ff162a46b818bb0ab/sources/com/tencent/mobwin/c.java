package com.tencent.mobwin;

import android.webkit.WebChromeClient;
import android.webkit.WebView;

class c extends WebChromeClient {
    final /* synthetic */ MobinWINBrowserActivity a;

    private c(MobinWINBrowserActivity mobinWINBrowserActivity) {
        this.a = mobinWINBrowserActivity;
    }

    /* synthetic */ c(MobinWINBrowserActivity mobinWINBrowserActivity, c cVar) {
        this(mobinWINBrowserActivity);
    }

    public void onProgressChanged(WebView webView, int i) {
        if (i != 100) {
            ((b) webView).a().setVisibility(0);
        } else {
            ((b) webView).a().setVisibility(8);
        }
    }
}
