package com.tencent.mobwin;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.tencent.mobwin.core.a.a;
import com.tencent.mobwin.core.a.f;
import com.tencent.mobwin.core.o;
import com.tencent.mobwin.core.w;

public final class AdView extends RelativeLayout {
    private static final String c = "AdView:";
    private String a = "http://mobwin.android.com/apk/res/";
    private AdListener b;

    public AdView(Context context) {
        super(context);
        a a2 = a.a();
        a(context, a2.a, a2.c, a2.e, a2.g);
    }

    public AdView(Context context, int i, int i2, int i3, int i4) {
        super(context);
        a(context, i, i2, i3, i4);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        int i;
        int i2;
        int i3;
        a a2 = a.a();
        String str = String.valueOf(this.a) + context.getPackageName();
        int i4 = a2.a;
        try {
            i4 = Color.parseColor(attributeSet.getAttributeValue(str, "backColor"));
        } catch (Exception e) {
        }
        int i5 = a2.e;
        try {
            i = Color.parseColor(attributeSet.getAttributeValue(str, "textColor"));
        } catch (Exception e2) {
            i = i5;
        }
        int i6 = a2.g;
        try {
            i2 = Integer.parseInt(attributeSet.getAttributeValue(str, "backAlpha"));
        } catch (Exception e3) {
            i2 = i6;
        }
        int i7 = a2.c;
        try {
            i3 = Color.parseColor(attributeSet.getAttributeValue(str, "titleColor"));
        } catch (Exception e4) {
            i3 = i7;
        }
        a(context, i4, i3, i, i2);
    }

    public AdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        int i2;
        int i3;
        int i4;
        String str = String.valueOf(this.a) + context.getPackageName();
        a a2 = a.a();
        int i5 = a2.a;
        try {
            i5 = Color.parseColor(attributeSet.getAttributeValue(str, "backColor"));
        } catch (Exception e) {
        }
        int i6 = a2.e;
        try {
            i2 = Color.parseColor(attributeSet.getAttributeValue(str, "textColor"));
        } catch (Exception e2) {
            i2 = i6;
        }
        int i7 = a2.g;
        try {
            i3 = Integer.parseInt(attributeSet.getAttributeValue(str, "backAlpha"));
        } catch (Exception e3) {
            i3 = i7;
        }
        try {
            i4 = Color.parseColor(attributeSet.getAttributeValue(str, "titleColor"));
        } catch (Exception e4) {
            i4 = a2.c;
        }
        a(context, i5, i4, i2, i3);
    }

    public AdView(Context context, String str, String str2, String str3) {
        super(context);
        try {
            f.e(context);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            w wVar = new w(context, this, str, str2, str3);
            layoutParams.addRule(13);
            addView(wVar, layoutParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void a(Context context, int i, int i2, int i3, int i4) {
        int i5 = i4 < 0 ? 0 : i4 > 255 ? 255 : i4;
        try {
            f.e(context);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            w wVar = new w(context, this, i, i2, i3, i5);
            layoutParams.addRule(13);
            addView(wVar, layoutParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getSDKVersion() {
        return w.h();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.b != null) {
            try {
                this.b.onReceiveAd();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        if (this.b != null) {
            try {
                o.b("Error", "event " + i);
                this.b.onReceiveFailed(i);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public AdListener getAdListener() {
        return this.b;
    }

    public void setAdListener(AdListener adListener) {
        this.b = adListener;
    }
}
