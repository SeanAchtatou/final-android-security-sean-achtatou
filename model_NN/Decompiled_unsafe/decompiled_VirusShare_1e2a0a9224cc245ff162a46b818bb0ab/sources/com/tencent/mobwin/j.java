package com.tencent.mobwin;

import android.view.View;

class j implements View.OnClickListener {
    final /* synthetic */ MobinWINBrowserActivity a;

    j(MobinWINBrowserActivity mobinWINBrowserActivity) {
        this.a = mobinWINBrowserActivity;
    }

    public void onClick(View view) {
        if (this.a.j != null && this.a.j.canGoBack()) {
            this.a.j.goBack();
        }
    }
}
