package com.tencent.mobwin.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;

public class a {
    private a() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, float, int[], float[], android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void} */
    public static Bitmap a(int i, int i2, int i3, int i4) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        canvas.setBitmap(createBitmap);
        Paint paint = new Paint();
        paint.setColor(i3);
        paint.setAlpha(i4);
        canvas.drawRect(new Rect(0, 0, i, i2), paint);
        paint.reset();
        paint.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, (float) i2, new int[]{1442840575, 872415231, 16777215}, new float[]{0.0f, 0.3f, 1.0f}, Shader.TileMode.CLAMP));
        canvas.drawRect(new Rect(0, 0, i, i2), paint);
        return createBitmap;
    }

    public static Bitmap a(int i, int i2, int i3, int i4, Context context) {
        Bitmap createBitmap = Bitmap.createBitmap(b.a(i, context), b.a(i2, context), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        canvas.setBitmap(createBitmap);
        int a = b.a(i / i3, context);
        int a2 = b.a(i2, context);
        Paint paint = new Paint();
        for (int i5 = 0; i5 < i3; i5++) {
            if (i5 == i4) {
                paint.setColor(55551);
            } else {
                paint.setColor(7237230);
            }
            Rect rect = new Rect((a * i5) + 1, 1, (a - 2) + (a * i5), a2 - 2);
            paint.setStyle(Paint.Style.FILL);
            paint.setAlpha(255);
            canvas.drawRect(rect, paint);
            Rect rect2 = new Rect((a * i5) + 0, 0, (a - 1) + (a * i5), a2 - 1);
            paint.setColor(-1);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(0.0f);
            paint.setAlpha(255);
            canvas.drawRect(rect2, paint);
        }
        return createBitmap;
    }

    public static Bitmap a(Bitmap bitmap, float f) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-12434878);
        canvas.drawRoundRect(rectF, f, f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return createBitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.mobwin.utils.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap
     arg types: [android.graphics.Bitmap, int]
     candidates:
      com.tencent.mobwin.utils.a.a(android.graphics.Bitmap, int):android.graphics.Bitmap
      com.tencent.mobwin.utils.a.a(android.graphics.Bitmap, float):android.graphics.Bitmap */
    public static Bitmap a(Bitmap bitmap, int i) {
        Bitmap createBitmap = Bitmap.createBitmap(i + 2, i + 3, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        canvas.setBitmap(createBitmap);
        if (bitmap != null) {
            Canvas canvas2 = new Canvas();
            Bitmap createBitmap2 = Bitmap.createBitmap(i, i, Bitmap.Config.ARGB_8888);
            canvas2.setBitmap(createBitmap2);
            canvas2.drawBitmap(bitmap, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), new Rect(0, 0, i, i), (Paint) null);
            Bitmap a = a(createBitmap2, 9.0f);
            canvas.drawBitmap(a, new Rect(0, 0, a.getWidth(), a.getHeight()), new Rect(1, 1, i, i), (Paint) null);
            a.recycle();
            createBitmap2.recycle();
        }
        return createBitmap;
    }

    public static Bitmap b(int i, int i2, int i3, int i4) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        canvas.setBitmap(createBitmap);
        Paint paint = new Paint();
        for (int i5 = 0; i5 < i3; i5++) {
            paint.setColor(-65536);
            if (i5 == i4) {
                paint.setAlpha(255);
            } else {
                paint.setAlpha(80);
            }
            canvas.drawCircle((float) ((i5 * 30) + 10), 20.0f, 5.0f, paint);
        }
        return createBitmap;
    }

    public static Bitmap c(int i, int i2, int i3, int i4) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        canvas.setBitmap(createBitmap);
        Paint paint = new Paint();
        paint.setColor(i3);
        paint.setAlpha(i4);
        canvas.drawRect(new Rect(0, 0, i, i2), paint);
        return createBitmap;
    }
}
