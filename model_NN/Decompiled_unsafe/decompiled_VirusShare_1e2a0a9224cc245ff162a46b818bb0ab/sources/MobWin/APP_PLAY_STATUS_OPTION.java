package MobWin;

public final class APP_PLAY_STATUS_OPTION {
    static final /* synthetic */ boolean $assertionsDisabled = (!APP_PLAY_STATUS_OPTION.class.desiredAssertionStatus());
    public static final APP_PLAY_STATUS_OPTION FROZEN = new APP_PLAY_STATUS_OPTION(0, 0, "FROZEN");
    public static final APP_PLAY_STATUS_OPTION PLAYING = new APP_PLAY_STATUS_OPTION(2, 2, "PLAYING");
    public static final APP_PLAY_STATUS_OPTION STOP = new APP_PLAY_STATUS_OPTION(1, 1, "STOP");
    public static final int _FROZEN = 0;
    public static final int _PLAYING = 2;
    public static final int _STOP = 1;
    private static APP_PLAY_STATUS_OPTION[] __values = new APP_PLAY_STATUS_OPTION[3];
    private String __T = new String();
    private int __value;

    public static APP_PLAY_STATUS_OPTION convert(int val) {
        for (int __i = 0; __i < __values.length; __i++) {
            if (__values[__i].value() == val) {
                return __values[__i];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public static APP_PLAY_STATUS_OPTION convert(String val) {
        for (int __i = 0; __i < __values.length; __i++) {
            if (__values[__i].toString().equals(val)) {
                return __values[__i];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public int value() {
        return this.__value;
    }

    public String toString() {
        return this.__T;
    }

    private APP_PLAY_STATUS_OPTION(int index, int val, String s) {
        this.__T = s;
        this.__value = val;
        __values[index] = this;
    }
}
