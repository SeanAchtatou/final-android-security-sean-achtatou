package MobWin;

public final class AD_CLICK_EFFECTIVE_TYPE {
    static final /* synthetic */ boolean $assertionsDisabled = (!AD_CLICK_EFFECTIVE_TYPE.class.desiredAssertionStatus());
    public static final AD_CLICK_EFFECTIVE_TYPE ALBUM = new AD_CLICK_EFFECTIVE_TYPE(5, 5, "ALBUM");
    public static final AD_CLICK_EFFECTIVE_TYPE DOWNLOAD = new AD_CLICK_EFFECTIVE_TYPE(1, 1, "DOWNLOAD");
    public static final AD_CLICK_EFFECTIVE_TYPE LOCATION = new AD_CLICK_EFFECTIVE_TYPE(4, 4, "LOCATION");
    public static final AD_CLICK_EFFECTIVE_TYPE PHONE = new AD_CLICK_EFFECTIVE_TYPE(2, 2, "PHONE");
    public static final AD_CLICK_EFFECTIVE_TYPE SMS = new AD_CLICK_EFFECTIVE_TYPE(3, 3, "SMS");
    public static final AD_CLICK_EFFECTIVE_TYPE WWW = new AD_CLICK_EFFECTIVE_TYPE(0, 0, "WWW");
    public static final int _ALBUM = 5;
    public static final int _DOWNLOAD = 1;
    public static final int _LOCATION = 4;
    public static final int _PHONE = 2;
    public static final int _SMS = 3;
    public static final int _WWW = 0;
    private static AD_CLICK_EFFECTIVE_TYPE[] __values = new AD_CLICK_EFFECTIVE_TYPE[6];
    private String __T = new String();
    private int __value;

    public static AD_CLICK_EFFECTIVE_TYPE convert(int val) {
        for (int __i = 0; __i < __values.length; __i++) {
            if (__values[__i].value() == val) {
                return __values[__i];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public static AD_CLICK_EFFECTIVE_TYPE convert(String val) {
        for (int __i = 0; __i < __values.length; __i++) {
            if (__values[__i].toString().equals(val)) {
                return __values[__i];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public int value() {
        return this.__value;
    }

    public String toString() {
        return this.__T;
    }

    private AD_CLICK_EFFECTIVE_TYPE(int index, int val, String s) {
        this.__T = s;
        this.__value = val;
        __values[index] = this;
    }
}
