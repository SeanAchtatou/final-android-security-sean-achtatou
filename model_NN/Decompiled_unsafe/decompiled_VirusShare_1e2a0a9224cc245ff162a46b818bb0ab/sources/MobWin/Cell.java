package MobWin;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;

public final class Cell extends JceStruct {
    static final /* synthetic */ boolean $assertionsDisabled = (!Cell.class.desiredAssertionStatus());
    public int iCellId = -1;
    public int iLac = -1;
    public short shMcc = -1;
    public short shMnc = -1;

    public String className() {
        return "MobWin.Cell";
    }

    public short getShMcc() {
        return this.shMcc;
    }

    public void setShMcc(short shMcc2) {
        this.shMcc = shMcc2;
    }

    public short getShMnc() {
        return this.shMnc;
    }

    public void setShMnc(short shMnc2) {
        this.shMnc = shMnc2;
    }

    public int getILac() {
        return this.iLac;
    }

    public void setILac(int iLac2) {
        this.iLac = iLac2;
    }

    public int getICellId() {
        return this.iCellId;
    }

    public void setICellId(int iCellId2) {
        this.iCellId = iCellId2;
    }

    public Cell() {
        setShMcc(this.shMcc);
        setShMnc(this.shMnc);
        setILac(this.iLac);
        setICellId(this.iCellId);
    }

    public Cell(short shMcc2, short shMnc2, int iLac2, int iCellId2) {
        setShMcc(shMcc2);
        setShMnc(shMnc2);
        setILac(iLac2);
        setICellId(iCellId2);
    }

    public boolean equals(Object o) {
        Cell t = (Cell) o;
        return JceUtil.equals(this.shMcc, t.shMcc) && JceUtil.equals(this.shMnc, t.shMnc) && JceUtil.equals(this.iLac, t.iLac) && JceUtil.equals(this.iCellId, t.iCellId);
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            if ($assertionsDisabled) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void writeTo(JceOutputStream _os) {
        _os.write(this.shMcc, 0);
        _os.write(this.shMnc, 1);
        _os.write(this.iLac, 2);
        _os.write(this.iCellId, 3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
     arg types: [short, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    public void readFrom(JceInputStream _is) {
        setShMcc(_is.read(this.shMcc, 0, true));
        setShMnc(_is.read(this.shMnc, 1, true));
        setILac(_is.read(this.iLac, 2, true));
        setICellId(_is.read(this.iCellId, 3, true));
    }

    public void display(StringBuilder _os, int _level) {
        JceDisplayer _ds = new JceDisplayer(_os, _level);
        _ds.display(this.shMcc, "shMcc");
        _ds.display(this.shMnc, "shMnc");
        _ds.display(this.iLac, "iLac");
        _ds.display(this.iCellId, "iCellId");
    }
}
