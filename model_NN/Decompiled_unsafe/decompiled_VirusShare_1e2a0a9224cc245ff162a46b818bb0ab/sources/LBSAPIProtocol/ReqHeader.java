package LBSAPIProtocol;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;

public final class ReqHeader extends JceStruct {
    static final /* synthetic */ boolean $assertionsDisabled = (!ReqHeader.class.desiredAssertionStatus());
    static int cache_eDeviceType;
    static int cache_eUinType;
    public int eDeviceType = 0;
    public int eUinType = 0;
    public short shVersion = 1;
    public String strAuthName = "";
    public String strAuthPassword = "";
    public String strUin = "";

    public ReqHeader() {
        setShVersion(this.shVersion);
        setEUinType(this.eUinType);
        setStrUin(this.strUin);
        setStrAuthName(this.strAuthName);
        setStrAuthPassword(this.strAuthPassword);
        setEDeviceType(this.eDeviceType);
    }

    public ReqHeader(short s, int i, String str, String str2, String str3, int i2) {
        setShVersion(s);
        setEUinType(i);
        setStrUin(str);
        setStrAuthName(str2);
        setStrAuthPassword(str3);
        setEDeviceType(i2);
    }

    public String className() {
        return "LBSAPIProtocol.ReqHeader";
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            if ($assertionsDisabled) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void display(StringBuilder sb, int i) {
        JceDisplayer jceDisplayer = new JceDisplayer(sb, i);
        jceDisplayer.display(this.shVersion, "shVersion");
        jceDisplayer.display(this.eUinType, "eUinType");
        jceDisplayer.display(this.strUin, "strUin");
        jceDisplayer.display(this.strAuthName, "strAuthName");
        jceDisplayer.display(this.strAuthPassword, "strAuthPassword");
        jceDisplayer.display(this.eDeviceType, "eDeviceType");
    }

    public boolean equals(Object obj) {
        ReqHeader reqHeader = (ReqHeader) obj;
        return JceUtil.equals(this.shVersion, reqHeader.shVersion) && JceUtil.equals(this.eUinType, reqHeader.eUinType) && JceUtil.equals(this.strUin, reqHeader.strUin) && JceUtil.equals(this.strAuthName, reqHeader.strAuthName) && JceUtil.equals(this.strAuthPassword, reqHeader.strAuthPassword) && JceUtil.equals(this.eDeviceType, reqHeader.eDeviceType);
    }

    public String fullClassName() {
        return "LBSAPIProtocol.ReqHeader";
    }

    public int getEDeviceType() {
        return this.eDeviceType;
    }

    public int getEUinType() {
        return this.eUinType;
    }

    public short getShVersion() {
        return this.shVersion;
    }

    public String getStrAuthName() {
        return this.strAuthName;
    }

    public String getStrAuthPassword() {
        return this.strAuthPassword;
    }

    public String getStrUin() {
        return this.strUin;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
     arg types: [short, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    public void readFrom(JceInputStream jceInputStream) {
        setShVersion(jceInputStream.read(this.shVersion, 0, true));
        setEUinType(jceInputStream.read(this.eUinType, 1, true));
        setStrUin(jceInputStream.readString(2, true));
        setStrAuthName(jceInputStream.readString(3, true));
        setStrAuthPassword(jceInputStream.readString(4, true));
        setEDeviceType(jceInputStream.read(this.eDeviceType, 5, false));
    }

    public void setEDeviceType(int i) {
        this.eDeviceType = i;
    }

    public void setEUinType(int i) {
        this.eUinType = i;
    }

    public void setShVersion(short s) {
        this.shVersion = s;
    }

    public void setStrAuthName(String str) {
        this.strAuthName = str;
    }

    public void setStrAuthPassword(String str) {
        this.strAuthPassword = str;
    }

    public void setStrUin(String str) {
        this.strUin = str;
    }

    public void writeTo(JceOutputStream jceOutputStream) {
        jceOutputStream.write(this.shVersion, 0);
        jceOutputStream.write(this.eUinType, 1);
        jceOutputStream.write(this.strUin, 2);
        jceOutputStream.write(this.strAuthName, 3);
        jceOutputStream.write(this.strAuthPassword, 4);
        jceOutputStream.write(this.eDeviceType, 5);
    }
}
