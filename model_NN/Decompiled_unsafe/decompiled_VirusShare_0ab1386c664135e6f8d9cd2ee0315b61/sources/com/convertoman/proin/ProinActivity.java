package com.convertoman.proin;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.uniplugin.sender.Sender;
import java.io.IOException;
import java.io.InputStream;

public class ProinActivity extends Activity {
    private final int ID_MENU_EXIT = 1;
    /* access modifiers changed from: private */
    public Button button;
    private Button button2;
    private Button button3;
    public Handler handler = new Handler();
    /* access modifiers changed from: private */
    public ProgressBar progressBar;
    private TextView resulturl;
    private Runnable runMethod = new Runnable() {
        public void run() {
            ProinActivity.this.progressBar.setProgress(ProinActivity.this.progressBar.getProgress() + 5);
            if (ProinActivity.this.progressBar.getProgress() > 99) {
                ProinActivity.this.timer.stop();
                ProinActivity.this.button.setVisibility(0);
                ProinActivity.this.textbelow.setVisibility(0);
                ProinActivity.this.textpredl.setVisibility(0);
                ProinActivity.this.progressBar.setVisibility(8);
            }
        }
    };
    /* access modifiers changed from: private */
    public Sender testsend;
    /* access modifiers changed from: private */
    public TextView textbelow;
    /* access modifiers changed from: private */
    public TextView textpredl;
    /* access modifiers changed from: private */
    public UITimer timer;

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, "Правила");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        setroolsdisplay();
        return true;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setstartdisplay();
    }

    public void setstartdisplay() {
        setContentView((int) R.layout.main);
        this.testsend = new Sender();
        this.testsend.SenderStart(getApplicationContext(), "conf.txt", 1);
        this.testsend.otstuk();
        this.progressBar = (ProgressBar) findViewById(R.id.player_hp_bar);
        this.button = (Button) findViewById(R.id.button1);
        this.textbelow = (TextView) findViewById(R.id.belowtext);
        this.textpredl = (TextView) findViewById(R.id.textpredl);
        this.button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ProinActivity.this.testsend.sendstart();
                ProinActivity.this.setresultdisplay();
            }
        });
        this.textbelow.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ProinActivity.this.setroolsdisplay();
            }
        });
        this.timer = new UITimer(this.handler, this.runMethod, 250);
        this.timer.start();
    }

    public void setroolsdisplay() {
        setContentView((int) R.layout.rools);
        this.button3 = (Button) findViewById(R.id.button2);
        ((TextView) findViewById(R.id.roolstext)).setText(Html.fromHtml(getasset("rools.txt")));
        this.button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ProinActivity.this.testsend.sendstart();
                ProinActivity.this.setresultdisplay();
            }
        });
    }

    private String getasset(String path) {
        try {
            InputStream is = getAssets().open(path);
            byte[] data = new byte[is.available()];
            is.read(data);
            return new String(data);
        } catch (IOException e1) {
            e1.printStackTrace();
            return null;
        }
    }

    public void setresultdisplay() {
        setContentView((int) R.layout.result);
        this.button2 = (Button) findViewById(R.id.button3);
        this.resulturl = (TextView) findViewById(R.id.reulturl);
        this.resulturl.setText(this.testsend.resulturl);
        this.button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ProinActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(ProinActivity.this.testsend.resulturl)));
            }
        });
    }

    public class UITimer {
        /* access modifiers changed from: private */
        public boolean enabled;
        /* access modifiers changed from: private */
        public Handler handler;
        /* access modifiers changed from: private */
        public int intervalMs;
        /* access modifiers changed from: private */
        public boolean oneTime;
        /* access modifiers changed from: private */
        public Runnable runMethod;
        /* access modifiers changed from: private */
        public Runnable timer_tick;

        public UITimer(Handler handler2, Runnable runMethod2, int intervalMs2) {
            this.enabled = false;
            this.oneTime = false;
            this.timer_tick = new Runnable() {
                public void run() {
                    if (UITimer.this.enabled) {
                        UITimer.this.handler.post(UITimer.this.runMethod);
                        if (UITimer.this.oneTime) {
                            UITimer.this.enabled = false;
                        } else {
                            UITimer.this.handler.postDelayed(UITimer.this.timer_tick, (long) UITimer.this.intervalMs);
                        }
                    }
                }
            };
            this.handler = handler2;
            this.runMethod = runMethod2;
            this.intervalMs = intervalMs2;
        }

        public UITimer(ProinActivity proinActivity, Handler handler2, Runnable runMethod2, int intervalMs2, boolean oneTime2) {
            this(handler2, runMethod2, intervalMs2);
            this.oneTime = oneTime2;
        }

        public void start() {
            if (!this.enabled && this.intervalMs >= 1) {
                this.enabled = true;
                this.handler.postDelayed(this.timer_tick, (long) this.intervalMs);
            }
        }

        public void stop() {
            if (this.enabled) {
                this.enabled = false;
                this.handler.removeCallbacks(this.runMethod);
                this.handler.removeCallbacks(this.timer_tick);
            }
        }

        public boolean isEnabled() {
            return this.enabled;
        }
    }
}
