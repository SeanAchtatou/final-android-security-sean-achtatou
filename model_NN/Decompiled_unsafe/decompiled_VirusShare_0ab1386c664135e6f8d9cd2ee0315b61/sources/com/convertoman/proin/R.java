package com.convertoman.proin;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int button_background_end = 2131034115;
        public static final int button_background_start = 2131034114;
        public static final int button_select_background_end = 2131034117;
        public static final int button_select_background_start = 2131034116;
        public static final int button_text_color = 2131034118;
        public static final int cradient_background_end = 2131034113;
        public static final int cradient_background_start = 2131034112;
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int bluebutton = 2130837505;
        public static final int ic_launcher = 2130837506;
        public static final int logo = 2130837507;
    }

    public static final class id {
        public static final int SCROLLER_ID = 2131165192;
        public static final int belowtext = 2131165188;
        public static final int button1 = 2131165187;
        public static final int button2 = 2131165195;
        public static final int button3 = 2131165191;
        public static final int buttonlayout = 2131165193;
        public static final int descriptionresult = 2131165189;
        public static final int imageView1 = 2131165184;
        public static final int player_hp_bar = 2131165186;
        public static final int reulturl = 2131165190;
        public static final int roolstext = 2131165194;
        public static final int textpredl = 2131165185;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int result = 2130903041;
        public static final int rools = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
        public static final int roolstext = 2130968578;
    }

    public static final class style {
        public static final int btnBlue = 2131099648;
    }
}
