package com.convertoman.proin;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class FServices extends Service {
    final String LOG_TAG = "myLogs";

    public void onCreate() {
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return 1;
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d("myLogs", "onDestroy");
    }

    public IBinder onBind(Intent intent) {
        Log.d("myLogs", "onBind");
        return null;
    }
}
