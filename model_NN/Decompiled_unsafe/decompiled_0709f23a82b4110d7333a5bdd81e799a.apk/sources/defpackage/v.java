package defpackage;

import android.app.Activity;
import android.webkit.WebView;
import android.widget.VideoView;
import com.google.ads.AdActivity;
import com.google.ads.util.a;
import java.util.HashMap;

/* renamed from: v  reason: default package */
public final class v implements i {
    public final void a(d dVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get("action");
        if (webView instanceof g) {
            AdActivity b = ((g) webView).b();
            if (b == null) {
                a.a("Could not get adActivity to create the video.");
            } else if (str.equals("load")) {
                String str2 = hashMap.get("url");
                Activity e = dVar.e();
                if (e == null) {
                    a.e("activity was null while loading a video.");
                    return;
                }
                VideoView videoView = new VideoView(e);
                videoView.setVideoPath(str2);
                b.a(videoView);
            } else {
                VideoView a = b.a();
                if (a == null) {
                    a.e("Could not get the VideoView for a video GMSG.");
                } else if (str.equals("play")) {
                    a.setVisibility(0);
                    a.start();
                    a.d("Video is now playing.");
                    a.a(webView, "onVideoEvent", "{'event': 'playing'}");
                } else if (str.equals("pause")) {
                    a.pause();
                } else if (str.equals("stop")) {
                    a.stopPlayback();
                } else if (str.equals("remove")) {
                    a.setVisibility(8);
                } else if (str.equals("replay")) {
                    a.setVisibility(0);
                    a.seekTo(0);
                    a.start();
                } else if (str.equals("currentTime")) {
                    String str3 = hashMap.get("time");
                    if (str3 == null) {
                        a.e("No \"time\" parameter!");
                    } else {
                        a.seekTo((int) (Float.valueOf(str3).floatValue() * 1000.0f));
                    }
                } else if (!str.equals("position")) {
                    a.e("Unknown movie action: " + str);
                }
            }
        } else {
            a.a("Could not get adWebView to create the video.");
        }
    }
}
