package org.codehaus.jackson.sym;

import java.util.Arrays;

public final class CharsToNameCanonicalizer {
    protected static final int DEFAULT_TABLE_SIZE = 64;
    static final int MAX_ENTRIES_FOR_REUSE = 12000;
    protected static final int MAX_TABLE_SIZE = 65536;
    static final CharsToNameCanonicalizer sBootstrapSymbolTable = new CharsToNameCanonicalizer();
    protected Bucket[] _buckets;
    protected final boolean _canonicalize;
    protected boolean _dirty;
    protected int _indexMask;
    protected final boolean _intern;
    protected CharsToNameCanonicalizer _parent;
    protected int _size;
    protected int _sizeThreshold;
    protected String[] _symbols;

    public static CharsToNameCanonicalizer createRoot() {
        return sBootstrapSymbolTable.makeOrphan();
    }

    private CharsToNameCanonicalizer() {
        this._canonicalize = true;
        this._intern = true;
        this._dirty = true;
        initTables(DEFAULT_TABLE_SIZE);
    }

    private void initTables(int i) {
        this._symbols = new String[i];
        this._buckets = new Bucket[(i >> 1)];
        this._indexMask = i - 1;
        this._size = 0;
        this._sizeThreshold = i - (i >> 2);
    }

    private CharsToNameCanonicalizer(CharsToNameCanonicalizer charsToNameCanonicalizer, boolean z, boolean z2, String[] strArr, Bucket[] bucketArr, int i) {
        this._parent = charsToNameCanonicalizer;
        this._canonicalize = z;
        this._intern = z2;
        this._symbols = strArr;
        this._buckets = bucketArr;
        this._size = i;
        int length = strArr.length;
        this._sizeThreshold = length - (length >> 2);
        this._indexMask = length - 1;
        this._dirty = false;
    }

    public synchronized CharsToNameCanonicalizer makeChild(boolean z, boolean z2) {
        return new CharsToNameCanonicalizer(this, z, z2, this._symbols, this._buckets, this._size);
    }

    private CharsToNameCanonicalizer makeOrphan() {
        return new CharsToNameCanonicalizer(null, true, true, this._symbols, this._buckets, this._size);
    }

    private synchronized void mergeChild(CharsToNameCanonicalizer charsToNameCanonicalizer) {
        if (charsToNameCanonicalizer.size() > MAX_ENTRIES_FOR_REUSE) {
            initTables(DEFAULT_TABLE_SIZE);
        } else if (charsToNameCanonicalizer.size() > size()) {
            this._symbols = charsToNameCanonicalizer._symbols;
            this._buckets = charsToNameCanonicalizer._buckets;
            this._size = charsToNameCanonicalizer._size;
            this._sizeThreshold = charsToNameCanonicalizer._sizeThreshold;
            this._indexMask = charsToNameCanonicalizer._indexMask;
        }
        this._dirty = false;
    }

    public void release() {
        if (maybeDirty() && this._parent != null) {
            this._parent.mergeChild(this);
            this._dirty = false;
        }
    }

    public int size() {
        return this._size;
    }

    public boolean maybeDirty() {
        return this._dirty;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String findSymbol(char[] r7, int r8, int r9, int r10) {
        /*
            r6 = this;
            r5 = 1
            if (r9 >= r5) goto L_0x0006
            java.lang.String r1 = ""
        L_0x0005:
            return r1
        L_0x0006:
            boolean r0 = r6._canonicalize
            if (r0 != 0) goto L_0x0010
            java.lang.String r1 = new java.lang.String
            r1.<init>(r7, r8, r9)
            goto L_0x0005
        L_0x0010:
            int r0 = r6._indexMask
            r1 = r10 & r0
            java.lang.String[] r0 = r6._symbols
            r2 = r0[r1]
            if (r2 == 0) goto L_0x0044
            int r0 = r2.length()
            if (r0 != r9) goto L_0x0034
            r0 = 0
        L_0x0021:
            char r3 = r2.charAt(r0)
            int r4 = r8 + r0
            char r4 = r7[r4]
            if (r3 == r4) goto L_0x002f
        L_0x002b:
            if (r0 != r9) goto L_0x0034
            r1 = r2
            goto L_0x0005
        L_0x002f:
            int r0 = r0 + 1
            if (r0 < r9) goto L_0x0021
            goto L_0x002b
        L_0x0034:
            org.codehaus.jackson.sym.CharsToNameCanonicalizer$Bucket[] r0 = r6._buckets
            int r2 = r1 >> 1
            r0 = r0[r2]
            if (r0 == 0) goto L_0x0044
            java.lang.String r0 = r0.find(r7, r8, r9)
            if (r0 == 0) goto L_0x0044
            r1 = r0
            goto L_0x0005
        L_0x0044:
            boolean r0 = r6._dirty
            if (r0 != 0) goto L_0x006e
            r6.copyArrays()
            r6._dirty = r5
            r0 = r1
        L_0x004e:
            int r1 = r6._size
            int r1 = r1 + 1
            r6._size = r1
            java.lang.String r1 = new java.lang.String
            r1.<init>(r7, r8, r9)
            boolean r2 = r6._intern
            if (r2 == 0) goto L_0x0063
            org.codehaus.jackson.util.InternCache r2 = org.codehaus.jackson.util.InternCache.instance
            java.lang.String r1 = r2.intern(r1)
        L_0x0063:
            java.lang.String[] r2 = r6._symbols
            r2 = r2[r0]
            if (r2 != 0) goto L_0x007f
            java.lang.String[] r2 = r6._symbols
            r2[r0] = r1
            goto L_0x0005
        L_0x006e:
            int r0 = r6._size
            int r2 = r6._sizeThreshold
            if (r0 < r2) goto L_0x0090
            r6.rehash()
            int r0 = calcHash(r7, r8, r9)
            int r1 = r6._indexMask
            r0 = r0 & r1
            goto L_0x004e
        L_0x007f:
            int r0 = r0 >> 1
            org.codehaus.jackson.sym.CharsToNameCanonicalizer$Bucket[] r2 = r6._buckets
            org.codehaus.jackson.sym.CharsToNameCanonicalizer$Bucket r3 = new org.codehaus.jackson.sym.CharsToNameCanonicalizer$Bucket
            org.codehaus.jackson.sym.CharsToNameCanonicalizer$Bucket[] r4 = r6._buckets
            r4 = r4[r0]
            r3.<init>(r1, r4)
            r2[r0] = r3
            goto L_0x0005
        L_0x0090:
            r0 = r1
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.sym.CharsToNameCanonicalizer.findSymbol(char[], int, int, int):java.lang.String");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public static int calcHash(char[] cArr, int i, int i2) {
        int i3 = cArr[0];
        for (int i4 = 1; i4 < i2; i4++) {
            i3 = (i3 * 31) + cArr[i4];
        }
        return i3;
    }

    public static int calcHash(String str) {
        int charAt = str.charAt(0);
        int length = str.length();
        for (int i = 1; i < length; i++) {
            charAt = (charAt * 31) + str.charAt(i);
        }
        return charAt;
    }

    private void copyArrays() {
        String[] strArr = this._symbols;
        int length = strArr.length;
        this._symbols = new String[length];
        System.arraycopy(strArr, 0, this._symbols, 0, length);
        Bucket[] bucketArr = this._buckets;
        int length2 = bucketArr.length;
        this._buckets = new Bucket[length2];
        System.arraycopy(bucketArr, 0, this._buckets, 0, length2);
    }

    private void rehash() {
        int length = this._symbols.length;
        int i = length + length;
        if (i > MAX_TABLE_SIZE) {
            this._size = 0;
            Arrays.fill(this._symbols, (Object) null);
            Arrays.fill(this._buckets, (Object) null);
            this._dirty = true;
            return;
        }
        String[] strArr = this._symbols;
        Bucket[] bucketArr = this._buckets;
        this._symbols = new String[i];
        this._buckets = new Bucket[(i >> 1)];
        this._indexMask = i - 1;
        this._sizeThreshold += this._sizeThreshold;
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            String str = strArr[i3];
            if (str != null) {
                i2++;
                int calcHash = calcHash(str) & this._indexMask;
                if (this._symbols[calcHash] == null) {
                    this._symbols[calcHash] = str;
                } else {
                    int i4 = calcHash >> 1;
                    this._buckets[i4] = new Bucket(str, this._buckets[i4]);
                }
            }
        }
        int i5 = length >> 1;
        int i6 = i2;
        for (int i7 = 0; i7 < i5; i7++) {
            for (Bucket bucket = bucketArr[i7]; bucket != null; bucket = bucket.getNext()) {
                i6++;
                String symbol = bucket.getSymbol();
                int calcHash2 = calcHash(symbol) & this._indexMask;
                if (this._symbols[calcHash2] == null) {
                    this._symbols[calcHash2] = symbol;
                } else {
                    int i8 = calcHash2 >> 1;
                    this._buckets[i8] = new Bucket(symbol, this._buckets[i8]);
                }
            }
        }
        if (i6 != this._size) {
            throw new Error("Internal error on SymbolTable.rehash(): had " + this._size + " entries; now have " + i6 + ".");
        }
    }

    static final class Bucket {
        private final String _symbol;
        private final Bucket mNext;

        public Bucket(String str, Bucket bucket) {
            this._symbol = str;
            this.mNext = bucket;
        }

        public String getSymbol() {
            return this._symbol;
        }

        public Bucket getNext() {
            return this.mNext;
        }

        /* JADX WARNING: Removed duplicated region for block: B:7:0x0018  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String find(char[] r6, int r7, int r8) {
            /*
                r5 = this;
                java.lang.String r1 = r5._symbol
                org.codehaus.jackson.sym.CharsToNameCanonicalizer$Bucket r0 = r5.mNext
            L_0x0004:
                int r2 = r1.length()
                if (r2 != r8) goto L_0x001d
                r2 = 0
            L_0x000b:
                char r3 = r1.charAt(r2)
                int r4 = r7 + r2
                char r4 = r6[r4]
                if (r3 == r4) goto L_0x0018
            L_0x0015:
                if (r2 != r8) goto L_0x001d
            L_0x0017:
                return r1
            L_0x0018:
                int r2 = r2 + 1
                if (r2 < r8) goto L_0x000b
                goto L_0x0015
            L_0x001d:
                if (r0 != 0) goto L_0x0021
                r1 = 0
                goto L_0x0017
            L_0x0021:
                java.lang.String r1 = r0.getSymbol()
                org.codehaus.jackson.sym.CharsToNameCanonicalizer$Bucket r0 = r0.getNext()
                goto L_0x0004
            */
            throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.sym.CharsToNameCanonicalizer.Bucket.find(char[], int, int):java.lang.String");
        }
    }
}
