package org.codehaus.jackson.map.deser;

import java.io.IOException;
import java.util.EnumSet;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.TypeDeserializer;

public final class EnumSetDeserializer extends StdDeserializer<EnumSet<?>> {
    final Class<Enum> _enumClass;
    final EnumDeserializer _enumDeserializer;

    public EnumSetDeserializer(EnumResolver enumResolver) {
        super(EnumSet.class);
        this._enumDeserializer = new EnumDeserializer(enumResolver);
        this._enumClass = enumResolver.getEnumClass();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.deser.EnumDeserializer.deserialize(org.codehaus.jackson.JsonParser, org.codehaus.jackson.map.DeserializationContext):java.lang.Enum<?>
     arg types: [org.codehaus.jackson.JsonParser, org.codehaus.jackson.map.DeserializationContext]
     candidates:
      org.codehaus.jackson.map.deser.EnumDeserializer.deserialize(org.codehaus.jackson.JsonParser, org.codehaus.jackson.map.DeserializationContext):java.lang.Object
      org.codehaus.jackson.map.JsonDeserializer.deserialize(org.codehaus.jackson.JsonParser, org.codehaus.jackson.map.DeserializationContext):T
      org.codehaus.jackson.map.deser.EnumDeserializer.deserialize(org.codehaus.jackson.JsonParser, org.codehaus.jackson.map.DeserializationContext):java.lang.Enum<?> */
    public EnumSet<?> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        if (!jsonParser.isExpectedStartArrayToken()) {
            throw deserializationContext.mappingException(EnumSet.class);
        }
        EnumSet<?> constructSet = constructSet();
        while (true) {
            JsonToken nextToken = jsonParser.nextToken();
            if (nextToken == JsonToken.END_ARRAY) {
                return constructSet;
            }
            if (nextToken == JsonToken.VALUE_NULL) {
                throw deserializationContext.mappingException(this._enumClass);
            }
            constructSet.add(this._enumDeserializer.deserialize(jsonParser, deserializationContext));
        }
    }

    public Object deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, TypeDeserializer typeDeserializer) throws IOException, JsonProcessingException {
        return typeDeserializer.deserializeTypedFromArray(jsonParser, deserializationContext);
    }

    private EnumSet constructSet() {
        return EnumSet.noneOf(this._enumClass);
    }
}
