package org.codehaus.jackson.map.introspect;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.ClassIntrospector;
import org.codehaus.jackson.map.util.Annotations;
import org.codehaus.jackson.map.util.ArrayBuilders;
import org.codehaus.jackson.map.util.ClassUtil;

public final class AnnotatedClass extends Annotated {
    protected final AnnotationIntrospector _annotationIntrospector;
    protected final Class<?> _class;
    protected AnnotationMap _classAnnotations;
    protected List<AnnotatedConstructor> _constructors;
    protected List<AnnotatedMethod> _creatorMethods;
    protected AnnotatedConstructor _defaultConstructor;
    protected List<AnnotatedField> _fields;
    protected List<AnnotatedField> _ignoredFields;
    protected List<AnnotatedMethod> _ignoredMethods;
    protected AnnotatedMethodMap _memberMethods;
    protected final ClassIntrospector.MixInResolver _mixInResolver;
    protected final Class<?> _primaryMixIn;
    protected final Collection<Class<?>> _superTypes;

    private AnnotatedClass(Class<?> cls, List<Class<?>> list, AnnotationIntrospector annotationIntrospector, ClassIntrospector.MixInResolver mixInResolver) {
        this._class = cls;
        this._superTypes = list;
        this._annotationIntrospector = annotationIntrospector;
        this._mixInResolver = mixInResolver;
        this._primaryMixIn = this._mixInResolver == null ? null : this._mixInResolver.findMixInClassFor(this._class);
    }

    public static AnnotatedClass construct(Class<?> cls, AnnotationIntrospector annotationIntrospector, ClassIntrospector.MixInResolver mixInResolver) {
        AnnotatedClass annotatedClass = new AnnotatedClass(cls, ClassUtil.findSuperTypes(cls, null), annotationIntrospector, mixInResolver);
        annotatedClass.resolveClassAnnotations();
        return annotatedClass;
    }

    public static AnnotatedClass constructWithoutSuperTypes(Class<?> cls, AnnotationIntrospector annotationIntrospector, ClassIntrospector.MixInResolver mixInResolver) {
        AnnotatedClass annotatedClass = new AnnotatedClass(cls, Collections.emptyList(), annotationIntrospector, mixInResolver);
        annotatedClass.resolveClassAnnotations();
        return annotatedClass;
    }

    public Class<?> getAnnotated() {
        return this._class;
    }

    public int getModifiers() {
        return this._class.getModifiers();
    }

    public String getName() {
        return this._class.getName();
    }

    public <A extends Annotation> A getAnnotation(Class<A> cls) {
        if (this._classAnnotations == null) {
            return null;
        }
        return this._classAnnotations.get(cls);
    }

    public Type getGenericType() {
        return this._class;
    }

    public Class<?> getRawType() {
        return this._class;
    }

    public Annotations getAnnotations() {
        return this._classAnnotations;
    }

    public boolean hasAnnotations() {
        return this._classAnnotations.size() > 0;
    }

    public AnnotatedConstructor getDefaultConstructor() {
        return this._defaultConstructor;
    }

    public List<AnnotatedConstructor> getConstructors() {
        if (this._constructors == null) {
            return Collections.emptyList();
        }
        return this._constructors;
    }

    public List<AnnotatedMethod> getStaticMethods() {
        if (this._creatorMethods == null) {
            return Collections.emptyList();
        }
        return this._creatorMethods;
    }

    public Iterable<AnnotatedMethod> memberMethods() {
        return this._memberMethods;
    }

    public Iterable<AnnotatedMethod> ignoredMemberMethods() {
        if (this._ignoredMethods == null) {
            return Collections.emptyList();
        }
        return this._ignoredMethods;
    }

    public int getMemberMethodCount() {
        return this._memberMethods.size();
    }

    public AnnotatedMethod findMethod(String str, Class<?>[] clsArr) {
        return this._memberMethods.find(str, clsArr);
    }

    public int getFieldCount() {
        if (this._fields == null) {
            return 0;
        }
        return this._fields.size();
    }

    public Iterable<AnnotatedField> fields() {
        if (this._fields == null) {
            return Collections.emptyList();
        }
        return this._fields;
    }

    public Iterable<AnnotatedField> ignoredFields() {
        if (this._ignoredFields == null) {
            return Collections.emptyList();
        }
        return this._ignoredFields;
    }

    /* access modifiers changed from: protected */
    public void resolveClassAnnotations() {
        this._classAnnotations = new AnnotationMap();
        if (this._primaryMixIn != null) {
            _addClassMixIns(this._classAnnotations, this._class, this._primaryMixIn);
        }
        for (Annotation annotation : this._class.getDeclaredAnnotations()) {
            if (this._annotationIntrospector.isHandled(annotation)) {
                this._classAnnotations.addIfNotPresent(annotation);
            }
        }
        for (Class next : this._superTypes) {
            _addClassMixIns(this._classAnnotations, next);
            for (Annotation annotation2 : next.getDeclaredAnnotations()) {
                if (this._annotationIntrospector.isHandled(annotation2)) {
                    this._classAnnotations.addIfNotPresent(annotation2);
                }
            }
        }
        _addClassMixIns(this._classAnnotations, Object.class);
    }

    /* access modifiers changed from: protected */
    public void _addClassMixIns(AnnotationMap annotationMap, Class<?> cls) {
        if (this._mixInResolver != null) {
            _addClassMixIns(annotationMap, cls, this._mixInResolver.findMixInClassFor(cls));
        }
    }

    /* access modifiers changed from: protected */
    public void _addClassMixIns(AnnotationMap annotationMap, Class<?> cls, Class<?> cls2) {
        if (cls2 != null) {
            for (Annotation annotation : cls2.getDeclaredAnnotations()) {
                if (this._annotationIntrospector.isHandled(annotation)) {
                    annotationMap.addIfNotPresent(annotation);
                }
            }
            for (Class<?> declaredAnnotations : ClassUtil.findSuperTypes(cls2, cls)) {
                for (Annotation annotation2 : declaredAnnotations.getDeclaredAnnotations()) {
                    if (this._annotationIntrospector.isHandled(annotation2)) {
                        annotationMap.addIfNotPresent(annotation2);
                    }
                }
            }
        }
    }

    public void resolveCreators(boolean z) {
        this._constructors = null;
        for (Constructor<?> constructor : this._class.getDeclaredConstructors()) {
            switch (constructor.getParameterTypes().length) {
                case 0:
                    this._defaultConstructor = _constructConstructor(constructor, true);
                    break;
                default:
                    if (z) {
                        if (this._constructors == null) {
                            this._constructors = new ArrayList();
                        }
                        this._constructors.add(_constructConstructor(constructor, false));
                        break;
                    } else {
                        break;
                    }
            }
        }
        if (!(this._primaryMixIn == null || (this._defaultConstructor == null && this._constructors == null))) {
            _addConstructorMixIns(this._primaryMixIn);
        }
        if (this._defaultConstructor != null && this._annotationIntrospector.isIgnorableConstructor(this._defaultConstructor)) {
            this._defaultConstructor = null;
        }
        if (this._constructors != null) {
            int size = this._constructors.size();
            while (true) {
                int i = size - 1;
                if (i >= 0) {
                    if (this._annotationIntrospector.isIgnorableConstructor(this._constructors.get(i))) {
                        this._constructors.remove(i);
                        size = i;
                    } else {
                        size = i;
                    }
                }
            }
        }
        this._creatorMethods = null;
        if (z) {
            for (Method method : this._class.getDeclaredMethods()) {
                if (Modifier.isStatic(method.getModifiers()) && method.getParameterTypes().length >= 1) {
                    if (this._creatorMethods == null) {
                        this._creatorMethods = new ArrayList();
                    }
                    this._creatorMethods.add(_constructCreatorMethod(method));
                }
            }
            if (!(this._primaryMixIn == null || this._creatorMethods == null)) {
                _addFactoryMixIns(this._primaryMixIn);
            }
            if (this._creatorMethods != null) {
                int size2 = this._creatorMethods.size();
                while (true) {
                    int i2 = size2 - 1;
                    if (i2 < 0) {
                        return;
                    }
                    if (this._annotationIntrospector.isIgnorableMethod(this._creatorMethods.get(i2))) {
                        this._creatorMethods.remove(i2);
                        size2 = i2;
                    } else {
                        size2 = i2;
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Constructor<?>, org.codehaus.jackson.map.introspect.AnnotatedConstructor, boolean):void
     arg types: [java.lang.reflect.Constructor<?>, org.codehaus.jackson.map.introspect.AnnotatedConstructor, int]
     candidates:
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, boolean):void
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Constructor<?>, org.codehaus.jackson.map.introspect.AnnotatedConstructor, boolean):void */
    /* access modifiers changed from: protected */
    public void _addConstructorMixIns(Class<?> cls) {
        MemberKey[] memberKeyArr;
        int size = this._constructors == null ? 0 : this._constructors.size();
        MemberKey[] memberKeyArr2 = null;
        for (Constructor<?> constructor : cls.getDeclaredConstructors()) {
            switch (constructor.getParameterTypes().length) {
                case 0:
                    if (this._defaultConstructor == null) {
                        break;
                    } else {
                        _addMixOvers(constructor, this._defaultConstructor, false);
                        break;
                    }
                default:
                    if (memberKeyArr2 == null) {
                        memberKeyArr = new MemberKey[size];
                        for (int i = 0; i < size; i++) {
                            memberKeyArr[i] = new MemberKey(this._constructors.get(i).getAnnotated());
                        }
                    } else {
                        memberKeyArr = memberKeyArr2;
                    }
                    MemberKey memberKey = new MemberKey(constructor);
                    int i2 = 0;
                    while (true) {
                        if (i2 < size) {
                            if (memberKey.equals(memberKeyArr[i2])) {
                                _addMixOvers(constructor, this._constructors.get(i2), true);
                                memberKeyArr2 = memberKeyArr;
                                break;
                            } else {
                                i2++;
                            }
                        } else {
                            memberKeyArr2 = memberKeyArr;
                            break;
                        }
                    }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, boolean):void
     arg types: [java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, int]
     candidates:
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Constructor<?>, org.codehaus.jackson.map.introspect.AnnotatedConstructor, boolean):void
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, boolean):void */
    /* access modifiers changed from: protected */
    public void _addFactoryMixIns(Class<?> cls) {
        MemberKey[] memberKeyArr;
        MemberKey[] memberKeyArr2 = null;
        int size = this._creatorMethods.size();
        for (Method method : cls.getDeclaredMethods()) {
            if (Modifier.isStatic(method.getModifiers()) && method.getParameterTypes().length != 0) {
                if (memberKeyArr2 == null) {
                    memberKeyArr = new MemberKey[size];
                    for (int i = 0; i < size; i++) {
                        memberKeyArr[i] = new MemberKey(this._creatorMethods.get(i).getAnnotated());
                    }
                } else {
                    memberKeyArr = memberKeyArr2;
                }
                MemberKey memberKey = new MemberKey(method);
                int i2 = 0;
                while (true) {
                    if (i2 < size) {
                        if (memberKey.equals(memberKeyArr[i2])) {
                            _addMixOvers(method, this._creatorMethods.get(i2), true);
                            memberKeyArr2 = memberKeyArr;
                            break;
                        }
                        i2++;
                    } else {
                        memberKeyArr2 = memberKeyArr;
                        break;
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, boolean):void
     arg types: [java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, int]
     candidates:
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Constructor<?>, org.codehaus.jackson.map.introspect.AnnotatedConstructor, boolean):void
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, boolean):void */
    public void resolveMemberMethods(MethodFilter methodFilter, boolean z) {
        Class<?> findMixInClassFor;
        this._memberMethods = new AnnotatedMethodMap();
        AnnotatedMethodMap annotatedMethodMap = new AnnotatedMethodMap();
        _addMemberMethods(this._class, methodFilter, this._memberMethods, this._primaryMixIn, annotatedMethodMap);
        for (Class next : this._superTypes) {
            _addMemberMethods(next, methodFilter, this._memberMethods, this._mixInResolver == null ? null : this._mixInResolver.findMixInClassFor(next), annotatedMethodMap);
        }
        if (!(this._mixInResolver == null || (findMixInClassFor = this._mixInResolver.findMixInClassFor(Object.class)) == null)) {
            _addMethodMixIns(methodFilter, this._memberMethods, findMixInClassFor, annotatedMethodMap);
        }
        if (!annotatedMethodMap.isEmpty()) {
            Iterator<AnnotatedMethod> it = annotatedMethodMap.iterator();
            while (it.hasNext()) {
                AnnotatedMethod next2 = it.next();
                try {
                    Method declaredMethod = Object.class.getDeclaredMethod(next2.getName(), next2.getParameterClasses());
                    if (declaredMethod != null) {
                        AnnotatedMethod _constructMethod = _constructMethod(declaredMethod);
                        _addMixOvers(next2.getAnnotated(), _constructMethod, false);
                        this._memberMethods.add(_constructMethod);
                    }
                } catch (Exception e) {
                }
            }
        }
        Iterator<AnnotatedMethod> it2 = this._memberMethods.iterator();
        while (it2.hasNext()) {
            AnnotatedMethod next3 = it2.next();
            if (this._annotationIntrospector.isIgnorableMethod(next3)) {
                it2.remove();
                if (z) {
                    this._ignoredMethods = ArrayBuilders.addToList(this._ignoredMethods, next3);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, boolean):void
     arg types: [java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, int]
     candidates:
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Constructor<?>, org.codehaus.jackson.map.introspect.AnnotatedConstructor, boolean):void
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, boolean):void */
    /* access modifiers changed from: protected */
    public void _addMemberMethods(Class<?> cls, MethodFilter methodFilter, AnnotatedMethodMap annotatedMethodMap, Class<?> cls2, AnnotatedMethodMap annotatedMethodMap2) {
        if (cls2 != null) {
            _addMethodMixIns(methodFilter, annotatedMethodMap, cls2, annotatedMethodMap2);
        }
        if (cls != null) {
            for (Method method : cls.getDeclaredMethods()) {
                if (_isIncludableMethod(method, methodFilter)) {
                    AnnotatedMethod find = annotatedMethodMap.find(method);
                    if (find == null) {
                        AnnotatedMethod _constructMethod = _constructMethod(method);
                        annotatedMethodMap.add(_constructMethod);
                        AnnotatedMethod remove = annotatedMethodMap2.remove(method);
                        if (remove != null) {
                            _addMixOvers(remove.getAnnotated(), _constructMethod, false);
                        }
                    } else {
                        _addMixUnders(method, find);
                        if (find.getDeclaringClass().isInterface() && !method.getDeclaringClass().isInterface()) {
                            annotatedMethodMap.add(find.withMethod(method));
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _addMethodMixIns(MethodFilter methodFilter, AnnotatedMethodMap annotatedMethodMap, Class<?> cls, AnnotatedMethodMap annotatedMethodMap2) {
        for (Method method : cls.getDeclaredMethods()) {
            if (_isIncludableMethod(method, methodFilter)) {
                AnnotatedMethod find = annotatedMethodMap.find(method);
                if (find != null) {
                    _addMixUnders(method, find);
                } else {
                    annotatedMethodMap2.add(_constructMethod(method));
                }
            }
        }
    }

    public void resolveFields(boolean z) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        _addFields(linkedHashMap, this._class);
        Iterator it = linkedHashMap.entrySet().iterator();
        while (it.hasNext()) {
            AnnotatedField annotatedField = (AnnotatedField) ((Map.Entry) it.next()).getValue();
            if (this._annotationIntrospector.isIgnorableField(annotatedField)) {
                it.remove();
                if (z) {
                    this._ignoredFields = ArrayBuilders.addToList(this._ignoredFields, annotatedField);
                }
            }
        }
        if (linkedHashMap.isEmpty()) {
            this._fields = Collections.emptyList();
            return;
        }
        this._fields = new ArrayList(linkedHashMap.size());
        this._fields.addAll(linkedHashMap.values());
    }

    /* access modifiers changed from: protected */
    public void _addFields(Map<String, AnnotatedField> map, Class<?> cls) {
        Class<?> findMixInClassFor;
        Class<? super Object> superclass = cls.getSuperclass();
        if (superclass != null) {
            _addFields(map, superclass);
            for (Field field : cls.getDeclaredFields()) {
                if (_isIncludableField(field)) {
                    map.put(field.getName(), _constructField(field));
                }
            }
            if (this._mixInResolver != null && (findMixInClassFor = this._mixInResolver.findMixInClassFor(cls)) != null) {
                _addFieldMixIns(findMixInClassFor, map);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _addFieldMixIns(Class<?> cls, Map<String, AnnotatedField> map) {
        AnnotatedField annotatedField;
        for (Field field : cls.getDeclaredFields()) {
            if (_isIncludableField(field) && (annotatedField = map.get(field.getName())) != null) {
                for (Annotation annotation : field.getDeclaredAnnotations()) {
                    if (this._annotationIntrospector.isHandled(annotation)) {
                        annotatedField.addOrOverride(annotation);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public AnnotatedMethod _constructMethod(Method method) {
        return new AnnotatedMethod(method, _collectRelevantAnnotations(method.getDeclaredAnnotations()), null);
    }

    /* access modifiers changed from: protected */
    public AnnotatedConstructor _constructConstructor(Constructor<?> constructor, boolean z) {
        return new AnnotatedConstructor(constructor, _collectRelevantAnnotations(constructor.getDeclaredAnnotations()), z ? null : _collectRelevantAnnotations(constructor.getParameterAnnotations()));
    }

    /* access modifiers changed from: protected */
    public AnnotatedMethod _constructCreatorMethod(Method method) {
        return new AnnotatedMethod(method, _collectRelevantAnnotations(method.getDeclaredAnnotations()), _collectRelevantAnnotations(method.getParameterAnnotations()));
    }

    /* access modifiers changed from: protected */
    public AnnotatedField _constructField(Field field) {
        return new AnnotatedField(field, _collectRelevantAnnotations(field.getDeclaredAnnotations()));
    }

    /* access modifiers changed from: protected */
    public AnnotationMap[] _collectRelevantAnnotations(Annotation[][] annotationArr) {
        int length = annotationArr.length;
        AnnotationMap[] annotationMapArr = new AnnotationMap[length];
        for (int i = 0; i < length; i++) {
            annotationMapArr[i] = _collectRelevantAnnotations(annotationArr[i]);
        }
        return annotationMapArr;
    }

    /* access modifiers changed from: protected */
    public AnnotationMap _collectRelevantAnnotations(Annotation[] annotationArr) {
        AnnotationMap annotationMap = new AnnotationMap();
        if (annotationArr != null) {
            for (Annotation annotation : annotationArr) {
                if (this._annotationIntrospector.isHandled(annotation)) {
                    annotationMap.add(annotation);
                }
            }
        }
        return annotationMap;
    }

    /* access modifiers changed from: protected */
    public boolean _isIncludableMethod(Method method, MethodFilter methodFilter) {
        if ((methodFilter == null || methodFilter.includeMethod(method)) && !method.isSynthetic() && !method.isBridge()) {
            return true;
        }
        return false;
    }

    private boolean _isIncludableField(Field field) {
        if (field.isSynthetic()) {
            return false;
        }
        int modifiers = field.getModifiers();
        if (Modifier.isStatic(modifiers) || Modifier.isTransient(modifiers)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void _addMixOvers(Constructor<?> constructor, AnnotatedConstructor annotatedConstructor, boolean z) {
        for (Annotation annotation : constructor.getDeclaredAnnotations()) {
            if (this._annotationIntrospector.isHandled(annotation)) {
                annotatedConstructor.addOrOverride(annotation);
            }
        }
        if (z) {
            Annotation[][] parameterAnnotations = constructor.getParameterAnnotations();
            int length = parameterAnnotations.length;
            for (int i = 0; i < length; i++) {
                for (Annotation addOrOverrideParam : parameterAnnotations[i]) {
                    annotatedConstructor.addOrOverrideParam(i, addOrOverrideParam);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _addMixOvers(Method method, AnnotatedMethod annotatedMethod, boolean z) {
        for (Annotation annotation : method.getDeclaredAnnotations()) {
            if (this._annotationIntrospector.isHandled(annotation)) {
                annotatedMethod.addOrOverride(annotation);
            }
        }
        if (z) {
            Annotation[][] parameterAnnotations = method.getParameterAnnotations();
            int length = parameterAnnotations.length;
            for (int i = 0; i < length; i++) {
                for (Annotation addOrOverrideParam : parameterAnnotations[i]) {
                    annotatedMethod.addOrOverrideParam(i, addOrOverrideParam);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _addMixUnders(Method method, AnnotatedMethod annotatedMethod) {
        for (Annotation annotation : method.getDeclaredAnnotations()) {
            if (this._annotationIntrospector.isHandled(annotation)) {
                annotatedMethod.addIfNotPresent(annotation);
            }
        }
    }

    public String toString() {
        return "[AnnotedClass " + this._class.getName() + "]";
    }
}
