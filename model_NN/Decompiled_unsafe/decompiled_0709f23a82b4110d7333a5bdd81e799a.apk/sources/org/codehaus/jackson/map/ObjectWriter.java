package org.codehaus.jackson.map;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.PrettyPrinter;
import org.codehaus.jackson.Version;
import org.codehaus.jackson.Versioned;
import org.codehaus.jackson.io.SegmentedStringWriter;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.introspect.VisibilityChecker;
import org.codehaus.jackson.map.jsontype.SubtypeResolver;
import org.codehaus.jackson.map.jsontype.TypeResolverBuilder;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.type.JavaType;
import org.codehaus.jackson.type.TypeReference;
import org.codehaus.jackson.util.ByteArrayBuilder;
import org.codehaus.jackson.util.DefaultPrettyPrinter;
import org.codehaus.jackson.util.MinimalPrettyPrinter;
import org.codehaus.jackson.util.VersionUtil;

public class ObjectWriter implements Versioned {
    protected static final PrettyPrinter NULL_PRETTY_PRINTER = new MinimalPrettyPrinter();
    protected final SerializationConfig _config;
    protected final TypeResolverBuilder<?> _defaultTyper;
    protected final JsonFactory _jsonFactory;
    protected final PrettyPrinter _prettyPrinter;
    protected final SerializerProvider _provider;
    protected final JavaType _rootType;
    protected final Class<?> _serializationView;
    protected final SerializerFactory _serializerFactory;
    protected final SubtypeResolver _subtypeResolver;
    protected final VisibilityChecker<?> _visibilityChecker;

    protected ObjectWriter(ObjectMapper objectMapper, Class<?> cls, JavaType javaType, PrettyPrinter prettyPrinter) {
        this._defaultTyper = objectMapper._defaultTyper;
        this._visibilityChecker = objectMapper._visibilityChecker;
        this._subtypeResolver = objectMapper._subtypeResolver;
        this._config = objectMapper._serializationConfig.createUnshared(this._defaultTyper, this._visibilityChecker, this._subtypeResolver, null);
        this._config.setSerializationView(cls);
        this._provider = objectMapper._serializerProvider;
        this._serializerFactory = objectMapper._serializerFactory;
        this._jsonFactory = objectMapper._jsonFactory;
        this._serializationView = cls;
        this._rootType = javaType;
        this._prettyPrinter = prettyPrinter;
    }

    protected ObjectWriter(ObjectMapper objectMapper, FilterProvider filterProvider) {
        this._defaultTyper = objectMapper._defaultTyper;
        this._visibilityChecker = objectMapper._visibilityChecker;
        this._subtypeResolver = objectMapper._subtypeResolver;
        this._config = objectMapper._serializationConfig.createUnshared(this._defaultTyper, this._visibilityChecker, this._subtypeResolver, filterProvider);
        this._provider = objectMapper._serializerProvider;
        this._serializerFactory = objectMapper._serializerFactory;
        this._jsonFactory = objectMapper._jsonFactory;
        this._serializationView = null;
        this._rootType = null;
        this._prettyPrinter = null;
    }

    protected ObjectWriter(ObjectWriter objectWriter, SerializationConfig serializationConfig, Class<?> cls, JavaType javaType, PrettyPrinter prettyPrinter) {
        this._config = serializationConfig;
        this._provider = objectWriter._provider;
        this._serializerFactory = objectWriter._serializerFactory;
        this._jsonFactory = objectWriter._jsonFactory;
        this._defaultTyper = objectWriter._defaultTyper;
        this._visibilityChecker = objectWriter._visibilityChecker;
        this._subtypeResolver = objectWriter._subtypeResolver;
        this._serializationView = cls;
        this._rootType = javaType;
        this._prettyPrinter = prettyPrinter;
    }

    protected ObjectWriter(ObjectWriter objectWriter, SerializationConfig serializationConfig) {
        this._config = serializationConfig;
        this._provider = objectWriter._provider;
        this._serializerFactory = objectWriter._serializerFactory;
        this._jsonFactory = objectWriter._jsonFactory;
        this._defaultTyper = objectWriter._defaultTyper;
        this._visibilityChecker = objectWriter._visibilityChecker;
        this._subtypeResolver = objectWriter._subtypeResolver;
        this._serializationView = objectWriter._serializationView;
        this._rootType = objectWriter._rootType;
        this._prettyPrinter = objectWriter._prettyPrinter;
    }

    public Version version() {
        return VersionUtil.versionFor(getClass());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.SerializationConfig.createUnshared(org.codehaus.jackson.map.jsontype.TypeResolverBuilder<?>, org.codehaus.jackson.map.introspect.VisibilityChecker<?>, org.codehaus.jackson.map.jsontype.SubtypeResolver):org.codehaus.jackson.map.SerializationConfig
     arg types: [org.codehaus.jackson.map.jsontype.TypeResolverBuilder<?>, org.codehaus.jackson.map.introspect.VisibilityChecker<?>, org.codehaus.jackson.map.jsontype.SubtypeResolver]
     candidates:
      org.codehaus.jackson.map.SerializationConfig.createUnshared(org.codehaus.jackson.map.jsontype.TypeResolverBuilder, org.codehaus.jackson.map.introspect.VisibilityChecker, org.codehaus.jackson.map.jsontype.SubtypeResolver):org.codehaus.jackson.map.MapperConfig
      org.codehaus.jackson.map.MapperConfig.createUnshared(org.codehaus.jackson.map.jsontype.TypeResolverBuilder<?>, org.codehaus.jackson.map.introspect.VisibilityChecker<?>, org.codehaus.jackson.map.jsontype.SubtypeResolver):T
      org.codehaus.jackson.map.SerializationConfig.createUnshared(org.codehaus.jackson.map.jsontype.TypeResolverBuilder<?>, org.codehaus.jackson.map.introspect.VisibilityChecker<?>, org.codehaus.jackson.map.jsontype.SubtypeResolver):org.codehaus.jackson.map.SerializationConfig */
    public ObjectWriter withView(Class<?> cls) {
        if (cls == this._serializationView) {
            return this;
        }
        SerializationConfig createUnshared = this._config.createUnshared(this._defaultTyper, this._visibilityChecker, this._subtypeResolver);
        createUnshared.setSerializationView(cls);
        return new ObjectWriter(this, createUnshared);
    }

    public ObjectWriter withType(JavaType javaType) {
        if (javaType == this._rootType) {
            return this;
        }
        return new ObjectWriter(this, this._config, this._serializationView, javaType, this._prettyPrinter);
    }

    public ObjectWriter withType(Class<?> cls) {
        return withType(TypeFactory.type(cls));
    }

    public ObjectWriter withType(TypeReference<?> typeReference) {
        return withType(TypeFactory.type(typeReference));
    }

    public ObjectWriter withPrettyPrinter(PrettyPrinter prettyPrinter) {
        PrettyPrinter prettyPrinter2;
        if (prettyPrinter == null) {
            prettyPrinter2 = NULL_PRETTY_PRINTER;
        } else {
            prettyPrinter2 = prettyPrinter;
        }
        return new ObjectWriter(this, this._config, this._serializationView, this._rootType, prettyPrinter2);
    }

    public ObjectWriter withDefaultPrettyPrinter() {
        return withPrettyPrinter(new DefaultPrettyPrinter());
    }

    public ObjectWriter withFilters(FilterProvider filterProvider) {
        return filterProvider == this._config.getFilterProvider() ? this : new ObjectWriter(this, this._config.withFilters(filterProvider));
    }

    public void writeValue(JsonGenerator jsonGenerator, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        if (!this._config.isEnabled(SerializationConfig.Feature.CLOSE_CLOSEABLE) || !(obj instanceof Closeable)) {
            if (this._rootType == null) {
                this._provider.serializeValue(this._config, jsonGenerator, obj, this._serializerFactory);
            } else {
                this._provider.serializeValue(this._config, jsonGenerator, obj, this._rootType, this._serializerFactory);
            }
            if (this._config.isEnabled(SerializationConfig.Feature.FLUSH_AFTER_WRITE_VALUE)) {
                jsonGenerator.flush();
                return;
            }
            return;
        }
        _writeCloseableValue(jsonGenerator, obj, this._config);
    }

    public void writeValue(File file, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._jsonFactory.createJsonGenerator(file, JsonEncoding.UTF8), obj);
    }

    public void writeValue(OutputStream outputStream, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._jsonFactory.createJsonGenerator(outputStream, JsonEncoding.UTF8), obj);
    }

    public void writeValue(Writer writer, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        _configAndWriteValue(this._jsonFactory.createJsonGenerator(writer), obj);
    }

    public String writeValueAsString(Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        SegmentedStringWriter segmentedStringWriter = new SegmentedStringWriter(this._jsonFactory._getBufferRecycler());
        _configAndWriteValue(this._jsonFactory.createJsonGenerator(segmentedStringWriter), obj);
        return segmentedStringWriter.getAndClear();
    }

    public byte[] writeValueAsBytes(Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        ByteArrayBuilder byteArrayBuilder = new ByteArrayBuilder(this._jsonFactory._getBufferRecycler());
        _configAndWriteValue(this._jsonFactory.createJsonGenerator(byteArrayBuilder, JsonEncoding.UTF8), obj);
        byte[] byteArray = byteArrayBuilder.toByteArray();
        byteArrayBuilder.release();
        return byteArray;
    }

    public boolean canSerialize(Class<?> cls) {
        return this._provider.hasSerializerFor(this._config, cls, this._serializerFactory);
    }

    /* access modifiers changed from: protected */
    public final void _configAndWriteValue(JsonGenerator jsonGenerator, Object obj) throws IOException, JsonGenerationException, JsonMappingException {
        boolean z;
        if (this._prettyPrinter != null) {
            PrettyPrinter prettyPrinter = this._prettyPrinter;
            if (prettyPrinter == NULL_PRETTY_PRINTER) {
                prettyPrinter = null;
            }
            jsonGenerator.setPrettyPrinter(prettyPrinter);
        } else if (this._config.isEnabled(SerializationConfig.Feature.INDENT_OUTPUT)) {
            jsonGenerator.useDefaultPrettyPrinter();
        }
        if (!this._config.isEnabled(SerializationConfig.Feature.CLOSE_CLOSEABLE) || !(obj instanceof Closeable)) {
            try {
                if (this._rootType == null) {
                    this._provider.serializeValue(this._config, jsonGenerator, obj, this._serializerFactory);
                } else {
                    this._provider.serializeValue(this._config, jsonGenerator, obj, this._rootType, this._serializerFactory);
                }
                z = true;
                try {
                    jsonGenerator.close();
                    return;
                } catch (Throwable th) {
                    th = th;
                }
            } catch (Throwable th2) {
                th = th2;
                z = false;
            }
        } else {
            _configAndWriteCloseable(jsonGenerator, obj, this._config);
            return;
        }
        if (!z) {
            try {
                jsonGenerator.close();
            } catch (IOException e) {
            }
        }
        throw th;
        throw th;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0032 A[SYNTHETIC, Splitter:B:21:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0037 A[SYNTHETIC, Splitter:B:24:0x0037] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void _configAndWriteCloseable(org.codehaus.jackson.JsonGenerator r9, java.lang.Object r10, org.codehaus.jackson.map.SerializationConfig r11) throws java.io.IOException, org.codehaus.jackson.JsonGenerationException, org.codehaus.jackson.map.JsonMappingException {
        /*
            r8 = this;
            r7 = 0
            r6 = r10
            java.io.Closeable r6 = (java.io.Closeable) r6
            org.codehaus.jackson.type.JavaType r0 = r8._rootType     // Catch:{ all -> 0x002f }
            if (r0 != 0) goto L_0x0022
            org.codehaus.jackson.map.SerializerProvider r0 = r8._provider     // Catch:{ all -> 0x002f }
            org.codehaus.jackson.map.SerializerFactory r1 = r8._serializerFactory     // Catch:{ all -> 0x002f }
            r0.serializeValue(r11, r9, r10, r1)     // Catch:{ all -> 0x002f }
        L_0x000f:
            r0 = 0
            r9.close()     // Catch:{ all -> 0x0043 }
            r1 = 0
            r6.close()     // Catch:{ all -> 0x0046 }
            if (r7 == 0) goto L_0x001c
            r0.close()     // Catch:{ IOException -> 0x003b }
        L_0x001c:
            if (r7 == 0) goto L_0x0021
            r1.close()     // Catch:{ IOException -> 0x003d }
        L_0x0021:
            return
        L_0x0022:
            org.codehaus.jackson.map.SerializerProvider r0 = r8._provider     // Catch:{ all -> 0x002f }
            org.codehaus.jackson.type.JavaType r4 = r8._rootType     // Catch:{ all -> 0x002f }
            org.codehaus.jackson.map.SerializerFactory r5 = r8._serializerFactory     // Catch:{ all -> 0x002f }
            r1 = r11
            r2 = r9
            r3 = r10
            r0.serializeValue(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x002f }
            goto L_0x000f
        L_0x002f:
            r0 = move-exception
        L_0x0030:
            if (r9 == 0) goto L_0x0035
            r9.close()     // Catch:{ IOException -> 0x003f }
        L_0x0035:
            if (r6 == 0) goto L_0x003a
            r6.close()     // Catch:{ IOException -> 0x0041 }
        L_0x003a:
            throw r0
        L_0x003b:
            r0 = move-exception
            goto L_0x001c
        L_0x003d:
            r0 = move-exception
            goto L_0x0021
        L_0x003f:
            r1 = move-exception
            goto L_0x0035
        L_0x0041:
            r1 = move-exception
            goto L_0x003a
        L_0x0043:
            r0 = move-exception
            r9 = r7
            goto L_0x0030
        L_0x0046:
            r0 = move-exception
            r6 = r7
            r9 = r7
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.ObjectWriter._configAndWriteCloseable(org.codehaus.jackson.JsonGenerator, java.lang.Object, org.codehaus.jackson.map.SerializationConfig):void");
    }

    private final void _writeCloseableValue(JsonGenerator jsonGenerator, Object obj, SerializationConfig serializationConfig) throws IOException, JsonGenerationException, JsonMappingException {
        Closeable closeable = (Closeable) obj;
        try {
            if (this._rootType == null) {
                this._provider.serializeValue(serializationConfig, jsonGenerator, obj, this._serializerFactory);
            } else {
                this._provider.serializeValue(serializationConfig, jsonGenerator, obj, this._rootType, this._serializerFactory);
            }
            if (this._config.isEnabled(SerializationConfig.Feature.FLUSH_AFTER_WRITE_VALUE)) {
                jsonGenerator.flush();
            }
            Closeable closeable2 = null;
            try {
                closeable.close();
                if (closeable2 != null) {
                    try {
                        closeable2.close();
                    } catch (IOException e) {
                    }
                }
            } catch (Throwable th) {
                th = th;
                closeable = closeable2;
                if (closeable != null) {
                    try {
                        closeable.close();
                    } catch (IOException e2) {
                    }
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
        }
    }
}
