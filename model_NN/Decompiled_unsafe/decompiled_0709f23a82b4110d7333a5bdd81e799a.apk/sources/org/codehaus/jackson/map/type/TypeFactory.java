package org.codehaus.jackson.map.type;

import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.type.JavaType;
import org.codehaus.jackson.type.TypeReference;

public class TypeFactory {
    private static final JavaType[] NO_TYPES = new JavaType[0];
    public static final TypeFactory instance = new TypeFactory();
    protected final TypeParser _parser = new TypeParser(this);

    private TypeFactory() {
    }

    public static JavaType type(Type type) {
        return instance._fromType(type, null);
    }

    public static JavaType type(Type type, Class<?> cls) {
        return type(type, new TypeBindings(cls));
    }

    public static JavaType type(Type type, JavaType javaType) {
        return type(type, new TypeBindings(javaType));
    }

    public static JavaType type(Type type, TypeBindings typeBindings) {
        return instance._fromType(type, typeBindings);
    }

    public static JavaType type(TypeReference<?> typeReference) {
        return type(typeReference.getType());
    }

    public static JavaType arrayType(Class<?> cls) {
        return arrayType(type(cls));
    }

    public static JavaType arrayType(JavaType javaType) {
        return ArrayType.construct(javaType);
    }

    public static JavaType collectionType(Class<? extends Collection> cls, Class<?> cls2) {
        return collectionType(cls, type(cls2));
    }

    public static JavaType collectionType(Class<? extends Collection> cls, JavaType javaType) {
        return CollectionType.construct(cls, javaType);
    }

    public static JavaType mapType(Class<? extends Map> cls, Class<?> cls2, Class<?> cls3) {
        return mapType(cls, type(cls2), type(cls3));
    }

    public static JavaType mapType(Class<? extends Map> cls, JavaType javaType, JavaType javaType2) {
        return MapType.construct(cls, javaType, javaType2);
    }

    public static JavaType parametricType(Class<?> cls, Class<?>... clsArr) {
        int length = clsArr.length;
        JavaType[] javaTypeArr = new JavaType[length];
        for (int i = 0; i < length; i++) {
            javaTypeArr[i] = instance._fromClass(clsArr[i], null);
        }
        return parametricType(cls, javaTypeArr);
    }

    public static JavaType parametricType(Class<?> cls, JavaType... javaTypeArr) {
        if (cls.isArray()) {
            if (javaTypeArr.length == 1) {
                return ArrayType.construct(javaTypeArr[0]);
            }
            throw new IllegalArgumentException("Need exactly 1 parameter type for arrays (" + cls.getName() + ")");
        } else if (Map.class.isAssignableFrom(cls)) {
            if (javaTypeArr.length == 2) {
                return MapType.construct(cls, javaTypeArr[0], javaTypeArr[1]);
            }
            throw new IllegalArgumentException("Need exactly 2 parameter types for Map types (" + cls.getName() + ")");
        } else if (!Collection.class.isAssignableFrom(cls)) {
            return _constructSimple(cls, javaTypeArr);
        } else {
            if (javaTypeArr.length == 1) {
                return CollectionType.construct(cls, javaTypeArr[0]);
            }
            throw new IllegalArgumentException("Need exactly 1 parameter type for Collection types (" + cls.getName() + ")");
        }
    }

    public static JavaType fromCanonical(String str) throws IllegalArgumentException {
        return instance._parser.parse(str);
    }

    public static JavaType specialize(JavaType javaType, Class<?> cls) {
        if (!(javaType instanceof SimpleType) || (!cls.isArray() && !Map.class.isAssignableFrom(cls) && !Collection.class.isAssignableFrom(cls))) {
            return javaType.narrowBy(cls);
        }
        if (!javaType.getRawClass().isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Class " + cls.getClass().getName() + " not subtype of " + javaType);
        }
        JavaType _fromClass = instance._fromClass(cls, new TypeBindings(javaType.getRawClass()));
        Object valueHandler = javaType.getValueHandler();
        if (valueHandler != null) {
            _fromClass.setValueHandler(valueHandler);
        }
        Object typeHandler = javaType.getTypeHandler();
        if (typeHandler != null) {
            return _fromClass.withTypeHandler(typeHandler);
        }
        return _fromClass;
    }

    public static JavaType fastSimpleType(Class<?> cls) {
        return new SimpleType(cls, null, null);
    }

    public static JavaType[] findParameterTypes(Class<?> cls, Class<?> cls2) {
        return findParameterTypes(cls, cls2, new TypeBindings(cls));
    }

    public static JavaType[] findParameterTypes(Class<?> cls, Class<?> cls2, TypeBindings typeBindings) {
        HierarchicType _findSuperTypeChain = _findSuperTypeChain(cls, cls2);
        if (_findSuperTypeChain == null) {
            throw new IllegalArgumentException("Class " + cls.getName() + " is not a subtype of " + cls2.getName());
        }
        while (_findSuperTypeChain.getSuperType() != null) {
            _findSuperTypeChain = _findSuperTypeChain.getSuperType();
            Class<?> rawClass = _findSuperTypeChain.getRawClass();
            TypeBindings typeBindings2 = new TypeBindings(rawClass);
            if (_findSuperTypeChain.isGeneric()) {
                Type[] actualTypeArguments = _findSuperTypeChain.asGeneric().getActualTypeArguments();
                TypeVariable[] typeParameters = rawClass.getTypeParameters();
                int length = actualTypeArguments.length;
                for (int i = 0; i < length; i++) {
                    typeBindings2.addBinding(typeParameters[i].getName(), instance._fromType(actualTypeArguments[i], typeBindings));
                }
            }
            typeBindings = typeBindings2;
        }
        if (!_findSuperTypeChain.isGeneric()) {
            return null;
        }
        return typeBindings.typesAsArray();
    }

    public static JavaType[] findParameterTypes(JavaType javaType, Class<?> cls) {
        Class<?> rawClass = javaType.getRawClass();
        if (rawClass != cls) {
            return findParameterTypes(rawClass, cls, new TypeBindings(javaType));
        }
        int containedTypeCount = javaType.containedTypeCount();
        if (containedTypeCount == 0) {
            return null;
        }
        JavaType[] javaTypeArr = new JavaType[containedTypeCount];
        for (int i = 0; i < containedTypeCount; i++) {
            javaTypeArr[i] = javaType.containedType(i);
        }
        return javaTypeArr;
    }

    @Deprecated
    public static JavaType fromClass(Class<?> cls) {
        return instance._fromClass(cls, null);
    }

    @Deprecated
    public static JavaType fromTypeReference(TypeReference<?> typeReference) {
        return type(typeReference.getType());
    }

    @Deprecated
    public static JavaType fromType(Type type) {
        return instance._fromType(type, null);
    }

    /* access modifiers changed from: protected */
    public JavaType _fromClass(Class<?> cls, TypeBindings typeBindings) {
        if (cls.isArray()) {
            return ArrayType.construct(_fromType(cls.getComponentType(), null));
        }
        if (cls.isEnum()) {
            return new SimpleType(cls);
        }
        if (Map.class.isAssignableFrom(cls)) {
            return _mapType(cls);
        }
        if (Collection.class.isAssignableFrom(cls)) {
            return _collectionType(cls);
        }
        return new SimpleType(cls);
    }

    /* access modifiers changed from: protected */
    public JavaType _fromParameterizedClass(Class<?> cls, List<JavaType> list) {
        if (cls.isArray()) {
            return ArrayType.construct(_fromType(cls.getComponentType(), null));
        }
        if (cls.isEnum()) {
            return new SimpleType(cls);
        }
        if (Map.class.isAssignableFrom(cls)) {
            if (list.size() <= 0) {
                return _mapType(cls);
            }
            return MapType.construct(cls, list.get(0), list.size() >= 2 ? list.get(1) : _unknownType());
        } else if (Collection.class.isAssignableFrom(cls)) {
            if (list.size() >= 1) {
                return CollectionType.construct(cls, list.get(0));
            }
            return _collectionType(cls);
        } else if (list.size() == 0) {
            return new SimpleType(cls);
        } else {
            return _constructSimple(cls, (JavaType[]) list.toArray(new JavaType[list.size()]));
        }
    }

    public JavaType _fromType(Type type, TypeBindings typeBindings) {
        if (type instanceof Class) {
            Class cls = (Class) type;
            if (typeBindings == null) {
                typeBindings = new TypeBindings(cls);
            }
            return _fromClass(cls, typeBindings);
        } else if (type instanceof ParameterizedType) {
            return _fromParamType((ParameterizedType) type, typeBindings);
        } else {
            if (type instanceof GenericArrayType) {
                return _fromArrayType((GenericArrayType) type, typeBindings);
            }
            if (type instanceof TypeVariable) {
                return _fromVariable((TypeVariable) type, typeBindings);
            }
            if (type instanceof WildcardType) {
                return _fromWildcard((WildcardType) type, typeBindings);
            }
            throw new IllegalArgumentException("Unrecognized Type: " + type.toString());
        }
    }

    /* access modifiers changed from: protected */
    public JavaType _fromParamType(ParameterizedType parameterizedType, TypeBindings typeBindings) {
        JavaType[] javaTypeArr;
        Class cls = (Class) parameterizedType.getRawType();
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        int length = actualTypeArguments == null ? 0 : actualTypeArguments.length;
        if (length == 0) {
            javaTypeArr = NO_TYPES;
        } else {
            javaTypeArr = new JavaType[length];
            for (int i = 0; i < length; i++) {
                javaTypeArr[i] = _fromType(actualTypeArguments[i], typeBindings);
            }
        }
        if (Map.class.isAssignableFrom(cls)) {
            JavaType[] findParameterTypes = findParameterTypes(_constructSimple(cls, javaTypeArr), Map.class);
            if (findParameterTypes.length == 2) {
                return MapType.construct(cls, findParameterTypes[0], findParameterTypes[1]);
            }
            throw new IllegalArgumentException("Could not find 2 type parameters for Map class " + cls.getName() + " (found " + findParameterTypes.length + ")");
        } else if (Collection.class.isAssignableFrom(cls)) {
            JavaType[] findParameterTypes2 = findParameterTypes(_constructSimple(cls, javaTypeArr), Collection.class);
            if (findParameterTypes2.length == 1) {
                return CollectionType.construct(cls, findParameterTypes2[0]);
            }
            throw new IllegalArgumentException("Could not find 1 type parameter for Collection class " + cls.getName() + " (found " + findParameterTypes2.length + ")");
        } else if (length == 0) {
            return new SimpleType(cls);
        } else {
            return _constructSimple(cls, javaTypeArr);
        }
    }

    protected static SimpleType _constructSimple(Class<?> cls, JavaType[] javaTypeArr) {
        TypeVariable[] typeParameters = cls.getTypeParameters();
        if (typeParameters.length != javaTypeArr.length) {
            throw new IllegalArgumentException("Parameter type mismatch for " + cls.getName() + ": expected " + typeParameters.length + " parameters, was given " + javaTypeArr.length);
        }
        String[] strArr = new String[typeParameters.length];
        int length = typeParameters.length;
        for (int i = 0; i < length; i++) {
            strArr[i] = typeParameters[i].getName();
        }
        return new SimpleType(cls, strArr, javaTypeArr);
    }

    /* access modifiers changed from: protected */
    public JavaType _fromArrayType(GenericArrayType genericArrayType, TypeBindings typeBindings) {
        return ArrayType.construct(_fromType(genericArrayType.getGenericComponentType(), typeBindings));
    }

    /* access modifiers changed from: protected */
    public JavaType _fromVariable(TypeVariable<?> typeVariable, TypeBindings typeBindings) {
        if (typeBindings == null) {
            return _unknownType();
        }
        String name = typeVariable.getName();
        JavaType findType = typeBindings.findType(name);
        if (findType != null) {
            return findType;
        }
        Type[] bounds = typeVariable.getBounds();
        typeBindings._addPlaceholder(name);
        return _fromType(bounds[0], typeBindings);
    }

    /* access modifiers changed from: protected */
    public JavaType _fromWildcard(WildcardType wildcardType, TypeBindings typeBindings) {
        return _fromType(wildcardType.getUpperBounds()[0], typeBindings);
    }

    private JavaType _mapType(Class<?> cls) {
        JavaType[] findParameterTypes = findParameterTypes(cls, Map.class);
        if (findParameterTypes == null) {
            return MapType.construct(cls, _unknownType(), _unknownType());
        }
        if (findParameterTypes.length == 2) {
            return MapType.construct(cls, findParameterTypes[0], findParameterTypes[1]);
        }
        throw new IllegalArgumentException("Strange Map type " + cls.getName() + ": can not determine type parameters");
    }

    private JavaType _collectionType(Class<?> cls) {
        JavaType[] findParameterTypes = findParameterTypes(cls, Collection.class);
        if (findParameterTypes == null) {
            return CollectionType.construct(cls, _unknownType());
        }
        if (findParameterTypes.length == 1) {
            return CollectionType.construct(cls, findParameterTypes[0]);
        }
        throw new IllegalArgumentException("Strange Collection type " + cls.getName() + ": can not determine type parameters");
    }

    protected static JavaType _resolveVariableViaSubTypes(HierarchicType hierarchicType, String str, TypeBindings typeBindings) {
        if (hierarchicType != null && hierarchicType.isGeneric()) {
            TypeVariable[] typeParameters = hierarchicType.getRawClass().getTypeParameters();
            int length = typeParameters.length;
            for (int i = 0; i < length; i++) {
                if (str.equals(typeParameters[i].getName())) {
                    Type type = hierarchicType.asGeneric().getActualTypeArguments()[i];
                    if (type instanceof TypeVariable) {
                        return _resolveVariableViaSubTypes(hierarchicType.getSubType(), ((TypeVariable) type).getName(), typeBindings);
                    }
                    return instance._fromType(type, typeBindings);
                }
            }
        }
        return instance._unknownType();
    }

    /* access modifiers changed from: protected */
    public JavaType _unknownType() {
        return _fromClass(Object.class, null);
    }

    protected static HierarchicType _findSuperTypeChain(Class<?> cls, Class<?> cls2) {
        if (cls2.isInterface()) {
            return _findSuperInterfaceChain(cls, cls2);
        }
        return _findSuperClassChain(cls, cls2);
    }

    protected static HierarchicType _findSuperClassChain(Type type, Class<?> cls) {
        HierarchicType _findSuperClassChain;
        HierarchicType hierarchicType = new HierarchicType(type);
        Class<?> rawClass = hierarchicType.getRawClass();
        if (rawClass == cls) {
            return hierarchicType;
        }
        Type genericSuperclass = rawClass.getGenericSuperclass();
        if (genericSuperclass == null || (_findSuperClassChain = _findSuperClassChain(genericSuperclass, cls)) == null) {
            return null;
        }
        _findSuperClassChain.setSubType(hierarchicType);
        hierarchicType.setSuperType(_findSuperClassChain);
        return hierarchicType;
    }

    protected static HierarchicType _findSuperInterfaceChain(Type type, Class<?> cls) {
        HierarchicType _findSuperInterfaceChain;
        HierarchicType hierarchicType = new HierarchicType(type);
        Class<?> rawClass = hierarchicType.getRawClass();
        if (rawClass == cls) {
            return hierarchicType;
        }
        Type[] genericInterfaces = rawClass.getGenericInterfaces();
        if (genericInterfaces != null) {
            for (Type _findSuperInterfaceChain2 : genericInterfaces) {
                HierarchicType _findSuperInterfaceChain3 = _findSuperInterfaceChain(_findSuperInterfaceChain2, cls);
                if (_findSuperInterfaceChain3 != null) {
                    _findSuperInterfaceChain3.setSubType(hierarchicType);
                    hierarchicType.setSuperType(_findSuperInterfaceChain3);
                    return hierarchicType;
                }
            }
        }
        Type genericSuperclass = rawClass.getGenericSuperclass();
        if (genericSuperclass == null || (_findSuperInterfaceChain = _findSuperInterfaceChain(genericSuperclass, cls)) == null) {
            return null;
        }
        _findSuperInterfaceChain.setSubType(hierarchicType);
        hierarchicType.setSuperType(_findSuperInterfaceChain);
        return hierarchicType;
    }
}
