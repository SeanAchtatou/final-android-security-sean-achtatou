package org.codehaus.jackson.map.deser;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicReference;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.DeserializerFactory;
import org.codehaus.jackson.map.DeserializerProvider;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.KeyDeserializer;
import org.codehaus.jackson.map.TypeDeserializer;
import org.codehaus.jackson.map.deser.SettableBeanProperty;
import org.codehaus.jackson.map.deser.StdDeserializer;
import org.codehaus.jackson.map.deser.impl.StringCollectionDeserializer;
import org.codehaus.jackson.map.ext.OptionalHandlerFactory;
import org.codehaus.jackson.map.introspect.Annotated;
import org.codehaus.jackson.map.introspect.AnnotatedClass;
import org.codehaus.jackson.map.introspect.AnnotatedConstructor;
import org.codehaus.jackson.map.introspect.AnnotatedMember;
import org.codehaus.jackson.map.introspect.AnnotatedMethod;
import org.codehaus.jackson.map.introspect.AnnotatedParameter;
import org.codehaus.jackson.map.introspect.BasicBeanDescription;
import org.codehaus.jackson.map.jsontype.NamedType;
import org.codehaus.jackson.map.jsontype.TypeResolverBuilder;
import org.codehaus.jackson.map.type.ArrayType;
import org.codehaus.jackson.map.type.CollectionType;
import org.codehaus.jackson.map.type.MapType;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.map.util.ClassUtil;
import org.codehaus.jackson.type.JavaType;

public abstract class BasicDeserializerFactory extends DeserializerFactory {
    static final JavaType TYPE_STRING = TypeFactory.type(String.class);
    protected static final HashMap<JavaType, JsonDeserializer<Object>> _arrayDeserializers = ArrayDeserializers.getAll();
    static final HashMap<String, Class<? extends Collection>> _collectionFallbacks = new HashMap<>();
    static final HashMap<String, Class<? extends Map>> _mapFallbacks = new HashMap<>();
    static final HashMap<JavaType, JsonDeserializer<Object>> _simpleDeserializers = StdDeserializers.constructAll();
    protected OptionalHandlerFactory optionalHandlers = OptionalHandlerFactory.instance;

    /* access modifiers changed from: protected */
    public abstract JsonDeserializer<?> _findCustomArrayDeserializer(ArrayType arrayType, DeserializationConfig deserializationConfig, DeserializerProvider deserializerProvider, BeanProperty beanProperty, TypeDeserializer typeDeserializer, JsonDeserializer<?> jsonDeserializer) throws JsonMappingException;

    /* access modifiers changed from: protected */
    public abstract JsonDeserializer<?> _findCustomCollectionDeserializer(CollectionType collectionType, DeserializationConfig deserializationConfig, DeserializerProvider deserializerProvider, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty, TypeDeserializer typeDeserializer, JsonDeserializer<?> jsonDeserializer) throws JsonMappingException;

    /* access modifiers changed from: protected */
    public abstract JsonDeserializer<?> _findCustomEnumDeserializer(Class<?> cls, DeserializationConfig deserializationConfig, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty) throws JsonMappingException;

    /* access modifiers changed from: protected */
    public abstract JsonDeserializer<?> _findCustomMapDeserializer(MapType mapType, DeserializationConfig deserializationConfig, DeserializerProvider deserializerProvider, BasicBeanDescription basicBeanDescription, BeanProperty beanProperty, KeyDeserializer keyDeserializer, TypeDeserializer typeDeserializer, JsonDeserializer<?> jsonDeserializer) throws JsonMappingException;

    /* access modifiers changed from: protected */
    public abstract JsonDeserializer<?> _findCustomTreeNodeDeserializer(Class<? extends JsonNode> cls, DeserializationConfig deserializationConfig, BeanProperty beanProperty) throws JsonMappingException;

    public abstract DeserializerFactory withConfig(DeserializerFactory.Config config);

    static {
        _mapFallbacks.put(Map.class.getName(), LinkedHashMap.class);
        _mapFallbacks.put(ConcurrentMap.class.getName(), ConcurrentHashMap.class);
        _mapFallbacks.put(SortedMap.class.getName(), TreeMap.class);
        _mapFallbacks.put("java.util.NavigableMap", TreeMap.class);
        try {
            Class<?> cls = Class.forName("java.util.ConcurrentNavigableMap");
            _mapFallbacks.put(cls.getName(), Class.forName("java.util.ConcurrentSkipListMap"));
        } catch (ClassNotFoundException e) {
        }
        _collectionFallbacks.put(Collection.class.getName(), ArrayList.class);
        _collectionFallbacks.put(List.class.getName(), ArrayList.class);
        _collectionFallbacks.put(Set.class.getName(), HashSet.class);
        _collectionFallbacks.put(SortedSet.class.getName(), TreeSet.class);
        _collectionFallbacks.put(Queue.class.getName(), LinkedList.class);
        _collectionFallbacks.put("java.util.Deque", LinkedList.class);
        _collectionFallbacks.put("java.util.NavigableSet", TreeSet.class);
    }

    protected BasicDeserializerFactory() {
    }

    public JsonDeserializer<?> createArrayDeserializer(DeserializationConfig deserializationConfig, DeserializerProvider deserializerProvider, ArrayType arrayType, BeanProperty beanProperty) throws JsonMappingException {
        TypeDeserializer typeDeserializer;
        JavaType contentType = arrayType.getContentType();
        JsonDeserializer<Object> jsonDeserializer = (JsonDeserializer) contentType.getValueHandler();
        if (jsonDeserializer == null) {
            JsonDeserializer<?> jsonDeserializer2 = _arrayDeserializers.get(contentType);
            if (jsonDeserializer2 != null) {
                JsonDeserializer<?> _findCustomArrayDeserializer = _findCustomArrayDeserializer(arrayType, deserializationConfig, deserializerProvider, beanProperty, null, null);
                return _findCustomArrayDeserializer != null ? _findCustomArrayDeserializer : jsonDeserializer2;
            } else if (contentType.isPrimitive()) {
                throw new IllegalArgumentException("Internal error: primitive type (" + arrayType + ") passed, no array deserializer found");
            }
        }
        TypeDeserializer typeDeserializer2 = (TypeDeserializer) contentType.getTypeHandler();
        if (typeDeserializer2 == null) {
            typeDeserializer = findTypeDeserializer(deserializationConfig, contentType, beanProperty);
        } else {
            typeDeserializer = typeDeserializer2;
        }
        JsonDeserializer<?> _findCustomArrayDeserializer2 = _findCustomArrayDeserializer(arrayType, deserializationConfig, deserializerProvider, beanProperty, typeDeserializer, jsonDeserializer);
        if (_findCustomArrayDeserializer2 != null) {
            return _findCustomArrayDeserializer2;
        }
        if (jsonDeserializer == null) {
            jsonDeserializer = deserializerProvider.findValueDeserializer(deserializationConfig, contentType, beanProperty);
        }
        return new ArrayDeserializer(arrayType, jsonDeserializer, typeDeserializer);
    }

    public JsonDeserializer<?> createCollectionDeserializer(DeserializationConfig deserializationConfig, DeserializerProvider deserializerProvider, CollectionType collectionType, BeanProperty beanProperty) throws JsonMappingException {
        TypeDeserializer typeDeserializer;
        Class<?> cls;
        Class<?> rawClass = collectionType.getRawClass();
        BasicBeanDescription basicBeanDescription = (BasicBeanDescription) deserializationConfig.introspectClassAnnotations(rawClass);
        JsonDeserializer<?> findDeserializerFromAnnotation = findDeserializerFromAnnotation(deserializationConfig, basicBeanDescription.getClassInfo(), beanProperty);
        if (findDeserializerFromAnnotation != null) {
            return findDeserializerFromAnnotation;
        }
        CollectionType collectionType2 = (CollectionType) modifyTypeByAnnotation(deserializationConfig, basicBeanDescription.getClassInfo(), collectionType, null);
        JavaType contentType = collectionType2.getContentType();
        JsonDeserializer<Object> jsonDeserializer = (JsonDeserializer) contentType.getValueHandler();
        TypeDeserializer typeDeserializer2 = (TypeDeserializer) contentType.getTypeHandler();
        if (typeDeserializer2 == null) {
            typeDeserializer = findTypeDeserializer(deserializationConfig, contentType, beanProperty);
        } else {
            typeDeserializer = typeDeserializer2;
        }
        JsonDeserializer<?> _findCustomCollectionDeserializer = _findCustomCollectionDeserializer(collectionType2, deserializationConfig, deserializerProvider, basicBeanDescription, beanProperty, typeDeserializer, jsonDeserializer);
        if (_findCustomCollectionDeserializer != null) {
            return _findCustomCollectionDeserializer;
        }
        if (jsonDeserializer == null) {
            if (EnumSet.class.isAssignableFrom(rawClass)) {
                return new EnumSetDeserializer(constructEnumResolver(contentType.getRawClass(), deserializationConfig));
            }
            jsonDeserializer = deserializerProvider.findValueDeserializer(deserializationConfig, contentType, beanProperty);
        }
        if (collectionType2.isInterface() || collectionType2.isAbstract()) {
            cls = _collectionFallbacks.get(rawClass.getName());
            if (cls == null) {
                throw new IllegalArgumentException("Can not find a deserializer for non-concrete Collection type " + collectionType2);
            }
        } else {
            cls = rawClass;
        }
        Constructor findConstructor = ClassUtil.findConstructor(cls, deserializationConfig.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS));
        if (contentType.getRawClass() == String.class) {
            return new StringCollectionDeserializer(collectionType2, jsonDeserializer, findConstructor);
        }
        return new CollectionDeserializer(collectionType2, jsonDeserializer, typeDeserializer, findConstructor);
    }

    public JsonDeserializer<?> createMapDeserializer(DeserializationConfig deserializationConfig, DeserializerProvider deserializerProvider, MapType mapType, BeanProperty beanProperty) throws JsonMappingException {
        KeyDeserializer keyDeserializer;
        TypeDeserializer typeDeserializer;
        BasicBeanDescription basicBeanDescription;
        KeyDeserializer findKeyDeserializer;
        Class<?> rawClass = mapType.getRawClass();
        BasicBeanDescription basicBeanDescription2 = (BasicBeanDescription) deserializationConfig.introspectForCreation(mapType);
        JsonDeserializer<?> findDeserializerFromAnnotation = findDeserializerFromAnnotation(deserializationConfig, basicBeanDescription2.getClassInfo(), beanProperty);
        if (findDeserializerFromAnnotation != null) {
            return findDeserializerFromAnnotation;
        }
        MapType mapType2 = (MapType) modifyTypeByAnnotation(deserializationConfig, basicBeanDescription2.getClassInfo(), mapType, null);
        JavaType keyType = mapType2.getKeyType();
        JavaType contentType = mapType2.getContentType();
        JsonDeserializer<Object> jsonDeserializer = (JsonDeserializer) contentType.getValueHandler();
        KeyDeserializer keyDeserializer2 = (KeyDeserializer) keyType.getValueHandler();
        if (keyDeserializer2 == null) {
            if (TYPE_STRING.equals(keyType)) {
                findKeyDeserializer = null;
            } else {
                findKeyDeserializer = deserializerProvider.findKeyDeserializer(deserializationConfig, keyType, beanProperty);
            }
            keyDeserializer = findKeyDeserializer;
        } else {
            keyDeserializer = keyDeserializer2;
        }
        TypeDeserializer typeDeserializer2 = (TypeDeserializer) contentType.getTypeHandler();
        if (typeDeserializer2 == null) {
            typeDeserializer = findTypeDeserializer(deserializationConfig, contentType, beanProperty);
        } else {
            typeDeserializer = typeDeserializer2;
        }
        JsonDeserializer<?> _findCustomMapDeserializer = _findCustomMapDeserializer(mapType2, deserializationConfig, deserializerProvider, basicBeanDescription2, beanProperty, keyDeserializer, typeDeserializer, jsonDeserializer);
        if (_findCustomMapDeserializer != null) {
            return _findCustomMapDeserializer;
        }
        if (jsonDeserializer == null) {
            jsonDeserializer = deserializerProvider.findValueDeserializer(deserializationConfig, contentType, beanProperty);
        }
        if (EnumMap.class.isAssignableFrom(rawClass)) {
            Class<?> rawClass2 = keyType.getRawClass();
            if (rawClass2 != null && rawClass2.isEnum()) {
                return new EnumMapDeserializer(constructEnumResolver(rawClass2, deserializationConfig), jsonDeserializer);
            }
            throw new IllegalArgumentException("Can not construct EnumMap; generic (key) type not available");
        }
        if (mapType2.isInterface() || mapType2.isAbstract()) {
            Class cls = _mapFallbacks.get(rawClass.getName());
            if (cls == null) {
                throw new IllegalArgumentException("Can not find a deserializer for non-concrete Map type " + mapType2);
            }
            MapType mapType3 = (MapType) mapType2.forcedNarrowBy(cls);
            basicBeanDescription = (BasicBeanDescription) deserializationConfig.introspectForCreation(mapType3);
            mapType2 = mapType3;
        } else {
            basicBeanDescription = basicBeanDescription2;
        }
        boolean isEnabled = deserializationConfig.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS);
        Constructor<?> findDefaultConstructor = basicBeanDescription.findDefaultConstructor();
        if (findDefaultConstructor != null && isEnabled) {
            ClassUtil.checkAndFixAccess(findDefaultConstructor);
        }
        MapDeserializer mapDeserializer = new MapDeserializer(mapType2, findDefaultConstructor, keyDeserializer, jsonDeserializer, typeDeserializer);
        mapDeserializer.setIgnorableProperties(deserializationConfig.getAnnotationIntrospector().findPropertiesToIgnore(basicBeanDescription.getClassInfo()));
        mapDeserializer.setCreators(findMapCreators(deserializationConfig, basicBeanDescription));
        return mapDeserializer;
    }

    public JsonDeserializer<?> createEnumDeserializer(DeserializationConfig deserializationConfig, DeserializerProvider deserializerProvider, JavaType javaType, BeanProperty beanProperty) throws JsonMappingException {
        BasicBeanDescription basicBeanDescription = (BasicBeanDescription) deserializationConfig.introspectForCreation(javaType);
        JsonDeserializer<?> findDeserializerFromAnnotation = findDeserializerFromAnnotation(deserializationConfig, basicBeanDescription.getClassInfo(), beanProperty);
        if (findDeserializerFromAnnotation != null) {
            return findDeserializerFromAnnotation;
        }
        Class<?> rawClass = javaType.getRawClass();
        JsonDeserializer<?> _findCustomEnumDeserializer = _findCustomEnumDeserializer(rawClass, deserializationConfig, basicBeanDescription, beanProperty);
        if (_findCustomEnumDeserializer != null) {
            return _findCustomEnumDeserializer;
        }
        for (AnnotatedMethod next : basicBeanDescription.getFactoryMethods()) {
            if (deserializationConfig.getAnnotationIntrospector().hasCreatorAnnotation(next)) {
                if (next.getParameterCount() == 1 && next.getRawType().isAssignableFrom(rawClass)) {
                    return EnumDeserializer.deserializerForCreator(deserializationConfig, rawClass, next);
                }
                throw new IllegalArgumentException("Unsuitable method (" + next + ") decorated with @JsonCreator (for Enum type " + rawClass.getName() + ")");
            }
        }
        return new EnumDeserializer(constructEnumResolver(rawClass, deserializationConfig));
    }

    public JsonDeserializer<?> createTreeDeserializer(DeserializationConfig deserializationConfig, DeserializerProvider deserializerProvider, JavaType javaType, BeanProperty beanProperty) throws JsonMappingException {
        Class<?> rawClass = javaType.getRawClass();
        JsonDeserializer<?> _findCustomTreeNodeDeserializer = _findCustomTreeNodeDeserializer(rawClass, deserializationConfig, beanProperty);
        return _findCustomTreeNodeDeserializer != null ? _findCustomTreeNodeDeserializer : JsonNodeDeserializer.getDeserializer(rawClass);
    }

    public JsonDeserializer<Object> createBeanDeserializer(DeserializationConfig deserializationConfig, DeserializerProvider deserializerProvider, JavaType javaType, BeanProperty beanProperty) throws JsonMappingException {
        JsonDeserializer<Object> jsonDeserializer = _simpleDeserializers.get(javaType);
        if (jsonDeserializer != null) {
            return jsonDeserializer;
        }
        if (AtomicReference.class.isAssignableFrom(javaType.getRawClass())) {
            return new StdDeserializer.AtomicReferenceDeserializer(javaType, beanProperty);
        }
        JsonDeserializer<?> findDeserializer = this.optionalHandlers.findDeserializer(javaType, deserializationConfig, deserializerProvider);
        if (findDeserializer == null) {
            return null;
        }
        return findDeserializer;
    }

    public TypeDeserializer findTypeDeserializer(DeserializationConfig deserializationConfig, JavaType javaType, BeanProperty beanProperty) {
        TypeResolverBuilder<?> typeResolverBuilder;
        Collection<NamedType> collection;
        AnnotatedClass classInfo = ((BasicBeanDescription) deserializationConfig.introspectClassAnnotations(javaType.getRawClass())).getClassInfo();
        AnnotationIntrospector annotationIntrospector = deserializationConfig.getAnnotationIntrospector();
        TypeResolverBuilder<?> findTypeResolver = annotationIntrospector.findTypeResolver(classInfo, javaType);
        if (findTypeResolver == null) {
            TypeResolverBuilder<?> defaultTyper = deserializationConfig.getDefaultTyper(javaType);
            if (defaultTyper == null) {
                return null;
            }
            typeResolverBuilder = defaultTyper;
            collection = null;
        } else {
            collection = deserializationConfig.getSubtypeResolver().collectAndResolveSubtypes(classInfo, deserializationConfig, annotationIntrospector);
            typeResolverBuilder = findTypeResolver;
        }
        return typeResolverBuilder.buildTypeDeserializer(javaType, collection, beanProperty);
    }

    public TypeDeserializer findPropertyTypeDeserializer(DeserializationConfig deserializationConfig, JavaType javaType, AnnotatedMember annotatedMember, BeanProperty beanProperty) {
        AnnotationIntrospector annotationIntrospector = deserializationConfig.getAnnotationIntrospector();
        TypeResolverBuilder<?> findPropertyTypeResolver = annotationIntrospector.findPropertyTypeResolver(annotatedMember, javaType);
        if (findPropertyTypeResolver == null) {
            return findTypeDeserializer(deserializationConfig, javaType, beanProperty);
        }
        return findPropertyTypeResolver.buildTypeDeserializer(javaType, deserializationConfig.getSubtypeResolver().collectAndResolveSubtypes(annotatedMember, deserializationConfig, annotationIntrospector), beanProperty);
    }

    public TypeDeserializer findPropertyContentTypeDeserializer(DeserializationConfig deserializationConfig, JavaType javaType, AnnotatedMember annotatedMember, BeanProperty beanProperty) {
        AnnotationIntrospector annotationIntrospector = deserializationConfig.getAnnotationIntrospector();
        TypeResolverBuilder<?> findPropertyContentTypeResolver = annotationIntrospector.findPropertyContentTypeResolver(annotatedMember, javaType);
        JavaType contentType = javaType.getContentType();
        if (findPropertyContentTypeResolver == null) {
            return findTypeDeserializer(deserializationConfig, contentType, beanProperty);
        }
        return findPropertyContentTypeResolver.buildTypeDeserializer(contentType, deserializationConfig.getSubtypeResolver().collectAndResolveSubtypes(annotatedMember, deserializationConfig, annotationIntrospector), beanProperty);
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> findDeserializerFromAnnotation(DeserializationConfig deserializationConfig, Annotated annotated, BeanProperty beanProperty) {
        Object findDeserializer = deserializationConfig.getAnnotationIntrospector().findDeserializer(annotated, beanProperty);
        if (findDeserializer != null) {
            return _constructDeserializer(deserializationConfig, findDeserializer);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public JsonDeserializer<Object> _constructDeserializer(DeserializationConfig deserializationConfig, Object obj) {
        if (obj instanceof JsonDeserializer) {
            return (JsonDeserializer) obj;
        }
        if (!(obj instanceof Class)) {
            throw new IllegalStateException("AnnotationIntrospector returned deserializer definition of type " + obj.getClass().getName() + "; expected type JsonDeserializer or Class<JsonDeserializer> instead");
        }
        Class cls = (Class) obj;
        if (JsonDeserializer.class.isAssignableFrom(cls)) {
            return (JsonDeserializer) ClassUtil.createInstance(cls, deserializationConfig.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS));
        }
        throw new IllegalStateException("AnnotationIntrospector returned Class " + cls.getName() + "; expected Class<JsonDeserializer>");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected <T extends org.codehaus.jackson.type.JavaType> T modifyTypeByAnnotation(org.codehaus.jackson.map.DeserializationConfig r9, org.codehaus.jackson.map.introspect.Annotated r10, T r11, java.lang.String r12) throws org.codehaus.jackson.map.JsonMappingException {
        /*
            r8 = this;
            r7 = 0
            org.codehaus.jackson.map.AnnotationIntrospector r3 = r9.getAnnotationIntrospector()
            java.lang.Class r1 = r3.findDeserializationType(r10, r11, r12)
            if (r1 == 0) goto L_0x0109
            org.codehaus.jackson.type.JavaType r2 = r11.narrowBy(r1)     // Catch:{ IllegalArgumentException -> 0x0042 }
        L_0x000f:
            boolean r1 = r2.isContainerType()
            if (r1 == 0) goto L_0x009c
            org.codehaus.jackson.type.JavaType r1 = r2.getKeyType()
            java.lang.Class r4 = r3.findDeserializationKeyType(r10, r1, r12)
            if (r4 == 0) goto L_0x008e
            boolean r1 = r2 instanceof org.codehaus.jackson.map.type.MapType
            if (r1 != 0) goto L_0x0086
            org.codehaus.jackson.map.JsonMappingException r1 = new org.codehaus.jackson.map.JsonMappingException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Illegal key-type annotation: type "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r2 = r3.append(r2)
            java.lang.String r3 = " is not a Map type"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x0042:
            r2 = move-exception
            org.codehaus.jackson.map.JsonMappingException r3 = new org.codehaus.jackson.map.JsonMappingException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Failed to narrow type "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r11)
            java.lang.String r5 = " with concrete-type annotation (value "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r1 = r1.getName()
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r4 = "), method '"
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r4 = r10.getName()
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r4 = "': "
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r4 = r2.getMessage()
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            r3.<init>(r1, r7, r2)
            throw r3
        L_0x0086:
            r0 = r2
            org.codehaus.jackson.map.type.MapType r0 = (org.codehaus.jackson.map.type.MapType) r0     // Catch:{ IllegalArgumentException -> 0x009d }
            r1 = r0
            org.codehaus.jackson.type.JavaType r2 = r1.narrowKey(r4)     // Catch:{ IllegalArgumentException -> 0x009d }
        L_0x008e:
            org.codehaus.jackson.type.JavaType r1 = r2.getContentType()
            java.lang.Class r1 = r3.findDeserializationContentType(r10, r1, r12)
            if (r1 == 0) goto L_0x009c
            org.codehaus.jackson.type.JavaType r2 = r2.narrowContentsBy(r1)     // Catch:{ IllegalArgumentException -> 0x00d3 }
        L_0x009c:
            return r2
        L_0x009d:
            r1 = move-exception
            org.codehaus.jackson.map.JsonMappingException r3 = new org.codehaus.jackson.map.JsonMappingException
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Failed to narrow key type "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.String r5 = " with key-type annotation ("
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r4 = r4.getName()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = "): "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = r1.getMessage()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r3.<init>(r2, r7, r1)
            throw r3
        L_0x00d3:
            r3 = move-exception
            org.codehaus.jackson.map.JsonMappingException r4 = new org.codehaus.jackson.map.JsonMappingException
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Failed to narrow content type "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.String r5 = " with content-type annotation ("
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r1 = r1.getName()
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "): "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r3.getMessage()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r4.<init>(r1, r7, r3)
            throw r4
        L_0x0109:
            r2 = r11
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.deser.BasicDeserializerFactory.modifyTypeByAnnotation(org.codehaus.jackson.map.DeserializationConfig, org.codehaus.jackson.map.introspect.Annotated, org.codehaus.jackson.type.JavaType, java.lang.String):org.codehaus.jackson.type.JavaType");
    }

    /* access modifiers changed from: protected */
    public JavaType resolveType(DeserializationConfig deserializationConfig, BasicBeanDescription basicBeanDescription, JavaType javaType, AnnotatedMember annotatedMember, BeanProperty beanProperty) {
        TypeDeserializer findTypeDeserializer;
        TypeDeserializer findPropertyContentTypeDeserializer;
        Class<? extends KeyDeserializer> findKeyDeserializer;
        if (javaType.isContainerType()) {
            AnnotationIntrospector annotationIntrospector = deserializationConfig.getAnnotationIntrospector();
            boolean isEnabled = deserializationConfig.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS);
            JavaType keyType = javaType.getKeyType();
            if (!(keyType == null || (findKeyDeserializer = annotationIntrospector.findKeyDeserializer(annotatedMember)) == null || findKeyDeserializer == KeyDeserializer.None.class)) {
                keyType.setValueHandler((KeyDeserializer) ClassUtil.createInstance(findKeyDeserializer, isEnabled));
            }
            Class<? extends JsonDeserializer<?>> findContentDeserializer = annotationIntrospector.findContentDeserializer(annotatedMember);
            if (!(findContentDeserializer == null || findContentDeserializer == JsonDeserializer.None.class)) {
                javaType.getContentType().setValueHandler((JsonDeserializer) ClassUtil.createInstance(findContentDeserializer, isEnabled));
            }
            if ((annotatedMember instanceof AnnotatedMember) && (findPropertyContentTypeDeserializer = findPropertyContentTypeDeserializer(deserializationConfig, javaType, annotatedMember, beanProperty)) != null) {
                javaType = javaType.withContentTypeHandler(findPropertyContentTypeDeserializer);
            }
        }
        if (annotatedMember instanceof AnnotatedMember) {
            findTypeDeserializer = findPropertyTypeDeserializer(deserializationConfig, javaType, annotatedMember, beanProperty);
        } else {
            findTypeDeserializer = findTypeDeserializer(deserializationConfig, javaType, null);
        }
        if (findTypeDeserializer != null) {
            return javaType.withTypeHandler(findTypeDeserializer);
        }
        return javaType;
    }

    /* access modifiers changed from: protected */
    public EnumResolver<?> constructEnumResolver(Class<?> cls, DeserializationConfig deserializationConfig) {
        if (deserializationConfig.isEnabled(DeserializationConfig.Feature.READ_ENUMS_USING_TO_STRING)) {
            return EnumResolver.constructUnsafeUsingToString(cls);
        }
        return EnumResolver.constructUnsafe(cls, deserializationConfig.getAnnotationIntrospector());
    }

    /* access modifiers changed from: protected */
    public CreatorContainer findMapCreators(DeserializationConfig deserializationConfig, BasicBeanDescription basicBeanDescription) throws JsonMappingException {
        Class<?> beanClass = basicBeanDescription.getBeanClass();
        AnnotationIntrospector annotationIntrospector = deserializationConfig.getAnnotationIntrospector();
        CreatorContainer creatorContainer = new CreatorContainer(beanClass, deserializationConfig.isEnabled(DeserializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS));
        for (AnnotatedConstructor next : basicBeanDescription.getConstructors()) {
            int parameterCount = next.getParameterCount();
            if (parameterCount >= 1 && annotationIntrospector.hasCreatorAnnotation(next)) {
                SettableBeanProperty[] settableBeanPropertyArr = new SettableBeanProperty[parameterCount];
                int i = 0;
                int i2 = 0;
                while (i2 < parameterCount) {
                    AnnotatedParameter parameter = next.getParameter(i2);
                    String findPropertyNameForParam = parameter == null ? null : annotationIntrospector.findPropertyNameForParam(parameter);
                    if (findPropertyNameForParam == null || findPropertyNameForParam.length() == 0) {
                        throw new IllegalArgumentException("Parameter #" + i2 + " of constructor " + next + " has no property name annotation: must have for @JsonCreator for a Map type");
                    }
                    settableBeanPropertyArr[i2] = constructCreatorProperty(deserializationConfig, basicBeanDescription, findPropertyNameForParam, i2, parameter);
                    i2++;
                    i++;
                }
                creatorContainer.addPropertyConstructor(next, settableBeanPropertyArr);
            }
        }
        for (AnnotatedMethod next2 : basicBeanDescription.getFactoryMethods()) {
            int parameterCount2 = next2.getParameterCount();
            if (parameterCount2 >= 1 && annotationIntrospector.hasCreatorAnnotation(next2)) {
                SettableBeanProperty[] settableBeanPropertyArr2 = new SettableBeanProperty[parameterCount2];
                int i3 = 0;
                int i4 = 0;
                while (i4 < parameterCount2) {
                    AnnotatedParameter parameter2 = next2.getParameter(i4);
                    String findPropertyNameForParam2 = parameter2 == null ? null : annotationIntrospector.findPropertyNameForParam(parameter2);
                    if (findPropertyNameForParam2 == null || findPropertyNameForParam2.length() == 0) {
                        throw new IllegalArgumentException("Parameter #" + i4 + " of factory method " + next2 + " has no property name annotation: must have for @JsonCreator for a Map type");
                    }
                    settableBeanPropertyArr2[i4] = constructCreatorProperty(deserializationConfig, basicBeanDescription, findPropertyNameForParam2, i4, parameter2);
                    i4++;
                    i3++;
                }
                creatorContainer.addPropertyFactory(next2, settableBeanPropertyArr2);
            }
        }
        return creatorContainer;
    }

    /* access modifiers changed from: protected */
    public SettableBeanProperty constructCreatorProperty(DeserializationConfig deserializationConfig, BasicBeanDescription basicBeanDescription, String str, int i, AnnotatedParameter annotatedParameter) throws JsonMappingException {
        JavaType type = TypeFactory.type(annotatedParameter.getParameterType(), basicBeanDescription.bindingsForBeanType());
        BeanProperty.Std std = new BeanProperty.Std(str, type, basicBeanDescription.getClassAnnotations(), annotatedParameter);
        JavaType resolveType = resolveType(deserializationConfig, basicBeanDescription, type, annotatedParameter, std);
        if (resolveType != type) {
            std = std.withType(resolveType);
        }
        JsonDeserializer<Object> findDeserializerFromAnnotation = findDeserializerFromAnnotation(deserializationConfig, annotatedParameter, std);
        JavaType modifyTypeByAnnotation = modifyTypeByAnnotation(deserializationConfig, annotatedParameter, resolveType, str);
        SettableBeanProperty.CreatorProperty creatorProperty = new SettableBeanProperty.CreatorProperty(str, modifyTypeByAnnotation, findTypeDeserializer(deserializationConfig, modifyTypeByAnnotation, std), basicBeanDescription.getClassAnnotations(), annotatedParameter, i);
        if (findDeserializerFromAnnotation != null) {
            creatorProperty.setValueDeserializer(findDeserializerFromAnnotation);
        }
        return creatorProperty;
    }
}
