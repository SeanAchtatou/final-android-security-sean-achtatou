package org.codehaus.jackson.map.introspect;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Map;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.ClassIntrospector;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.MapperConfig;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.map.util.ClassUtil;
import org.codehaus.jackson.type.JavaType;

public class BasicClassIntrospector extends ClassIntrospector<BasicBeanDescription> {
    public static final BasicClassIntrospector instance = new BasicClassIntrospector();

    public static class GetterMethodFilter implements MethodFilter {
        public static final GetterMethodFilter instance = new GetterMethodFilter();

        private GetterMethodFilter() {
        }

        public boolean includeMethod(Method method) {
            return ClassUtil.hasGetterSignature(method);
        }
    }

    public static class SetterMethodFilter implements MethodFilter {
        public static final SetterMethodFilter instance = new SetterMethodFilter();

        public boolean includeMethod(Method method) {
            if (Modifier.isStatic(method.getModifiers())) {
                return false;
            }
            switch (method.getParameterTypes().length) {
                case 1:
                    return true;
                case 2:
                    return true;
                default:
                    return false;
            }
        }
    }

    public static final class SetterAndGetterMethodFilter extends SetterMethodFilter {
        public static final SetterAndGetterMethodFilter instance = new SetterAndGetterMethodFilter();

        public boolean includeMethod(Method method) {
            if (super.includeMethod(method)) {
                return true;
            }
            if (!ClassUtil.hasGetterSignature(method)) {
                return false;
            }
            Class<?> returnType = method.getReturnType();
            if (Collection.class.isAssignableFrom(returnType) || Map.class.isAssignableFrom(returnType)) {
                return true;
            }
            return false;
        }
    }

    public BasicBeanDescription forSerialization(SerializationConfig serializationConfig, JavaType javaType, ClassIntrospector.MixInResolver mixInResolver) {
        AnnotationIntrospector annotationIntrospector = serializationConfig.getAnnotationIntrospector();
        AnnotatedClass construct = AnnotatedClass.construct(javaType.getRawClass(), annotationIntrospector, mixInResolver);
        construct.resolveMemberMethods(getSerializationMethodFilter(serializationConfig), false);
        construct.resolveCreators(true);
        construct.resolveFields(false);
        return new BasicBeanDescription(javaType, construct, annotationIntrospector);
    }

    public BasicBeanDescription forDeserialization(DeserializationConfig deserializationConfig, JavaType javaType, ClassIntrospector.MixInResolver mixInResolver) {
        AnnotationIntrospector annotationIntrospector = deserializationConfig.getAnnotationIntrospector();
        AnnotatedClass construct = AnnotatedClass.construct(javaType.getRawClass(), annotationIntrospector, mixInResolver);
        construct.resolveMemberMethods(getDeserializationMethodFilter(deserializationConfig), true);
        construct.resolveCreators(true);
        construct.resolveFields(true);
        return new BasicBeanDescription(javaType, construct, annotationIntrospector);
    }

    public BasicBeanDescription forCreation(DeserializationConfig deserializationConfig, JavaType javaType, ClassIntrospector.MixInResolver mixInResolver) {
        AnnotationIntrospector annotationIntrospector = deserializationConfig.getAnnotationIntrospector();
        AnnotatedClass construct = AnnotatedClass.construct(javaType.getRawClass(), annotationIntrospector, mixInResolver);
        construct.resolveCreators(true);
        return new BasicBeanDescription(javaType, construct, annotationIntrospector);
    }

    public BasicBeanDescription forClassAnnotations(MapperConfig<?> mapperConfig, Class<?> cls, ClassIntrospector.MixInResolver mixInResolver) {
        AnnotationIntrospector annotationIntrospector = mapperConfig.getAnnotationIntrospector();
        return new BasicBeanDescription(TypeFactory.type(cls), AnnotatedClass.construct(cls, annotationIntrospector, mixInResolver), annotationIntrospector);
    }

    public BasicBeanDescription forDirectClassAnnotations(MapperConfig<?> mapperConfig, Class<?> cls, ClassIntrospector.MixInResolver mixInResolver) {
        AnnotationIntrospector annotationIntrospector = mapperConfig.getAnnotationIntrospector();
        return new BasicBeanDescription(TypeFactory.type(cls), AnnotatedClass.constructWithoutSuperTypes(cls, annotationIntrospector, mixInResolver), annotationIntrospector);
    }

    /* access modifiers changed from: protected */
    public MethodFilter getSerializationMethodFilter(SerializationConfig serializationConfig) {
        return GetterMethodFilter.instance;
    }

    /* access modifiers changed from: protected */
    public MethodFilter getDeserializationMethodFilter(DeserializationConfig deserializationConfig) {
        if (deserializationConfig.isEnabled(DeserializationConfig.Feature.USE_GETTERS_AS_SETTERS)) {
            return SetterAndGetterMethodFilter.instance;
        }
        return SetterMethodFilter.instance;
    }
}
