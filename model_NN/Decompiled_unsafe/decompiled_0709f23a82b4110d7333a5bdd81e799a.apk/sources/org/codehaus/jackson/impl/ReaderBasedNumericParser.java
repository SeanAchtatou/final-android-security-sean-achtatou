package org.codehaus.jackson.impl;

import java.io.IOException;
import java.io.Reader;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.io.IOContext;

public abstract class ReaderBasedNumericParser extends ReaderBasedParserBase {
    public ReaderBasedNumericParser(IOContext iOContext, int i, Reader reader) {
        super(iOContext, i, reader);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0068  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0017 A[EDGE_INSN: B:60:0x0017->B:7:0x0017 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0019  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final org.codehaus.jackson.JsonToken parseNumberText(int r14) throws java.io.IOException, org.codehaus.jackson.JsonParseException {
        /*
            r13 = this;
            r11 = 45
            r1 = 1
            r2 = 0
            r10 = 57
            r9 = 48
            if (r14 != r11) goto L_0x0022
            r0 = r1
        L_0x000b:
            int r4 = r13._inputPtr
            int r5 = r4 + -1
            int r7 = r13._inputEnd
            if (r0 == 0) goto L_0x00cb
            int r3 = r13._inputEnd
            if (r4 < r3) goto L_0x0024
        L_0x0017:
            if (r0 == 0) goto L_0x00bf
            int r1 = r5 + 1
        L_0x001b:
            r13._inputPtr = r1
            org.codehaus.jackson.JsonToken r0 = r13.parseNumberText2(r0)
        L_0x0021:
            return r0
        L_0x0022:
            r0 = r2
            goto L_0x000b
        L_0x0024:
            char[] r6 = r13._inputBuffer
            int r3 = r4 + 1
            char r4 = r6[r4]
            if (r4 > r10) goto L_0x002e
            if (r4 >= r9) goto L_0x0033
        L_0x002e:
            java.lang.String r6 = "expected digit (0-9) to follow minus sign, for valid numeric value"
            r13.reportUnexpectedNumberChar(r4, r6)
        L_0x0033:
            int r4 = r13._inputEnd
            if (r3 >= r4) goto L_0x0017
            char[] r6 = r13._inputBuffer
            int r4 = r3 + 1
            char r3 = r6[r3]
            if (r3 < r9) goto L_0x0041
            if (r3 <= r10) goto L_0x008e
        L_0x0041:
            r6 = 46
            if (r3 != r6) goto L_0x00c6
            r3 = r2
            r6 = r4
        L_0x0047:
            if (r6 >= r7) goto L_0x0017
            char[] r8 = r13._inputBuffer
            int r4 = r6 + 1
            char r6 = r8[r6]
            if (r6 < r9) goto L_0x0053
            if (r6 <= r10) goto L_0x00a2
        L_0x0053:
            if (r3 != 0) goto L_0x005a
            java.lang.String r8 = "Decimal point not followed by a digit"
            r13.reportUnexpectedNumberChar(r6, r8)
        L_0x005a:
            r12 = r3
            r3 = r4
            r4 = r6
            r6 = r12
        L_0x005e:
            r8 = 101(0x65, float:1.42E-43)
            if (r4 == r8) goto L_0x0066
            r8 = 69
            if (r4 != r8) goto L_0x00ad
        L_0x0066:
            if (r3 >= r7) goto L_0x0017
            char[] r8 = r13._inputBuffer
            int r4 = r3 + 1
            char r3 = r8[r3]
            if (r3 == r11) goto L_0x0074
            r8 = 43
            if (r3 != r8) goto L_0x00c2
        L_0x0074:
            if (r4 >= r7) goto L_0x0017
            char[] r8 = r13._inputBuffer
            int r3 = r4 + 1
            char r4 = r8[r4]
        L_0x007c:
            if (r4 > r10) goto L_0x00a6
            if (r4 < r9) goto L_0x00a6
            int r2 = r2 + 1
            if (r3 >= r7) goto L_0x0017
            char[] r8 = r13._inputBuffer
            int r4 = r3 + 1
            char r3 = r8[r3]
            r12 = r4
            r4 = r3
            r3 = r12
            goto L_0x007c
        L_0x008e:
            int r1 = r1 + 1
            r3 = 2
            if (r1 != r3) goto L_0x00cb
            char[] r3 = r13._inputBuffer
            int r6 = r4 + -2
            char r3 = r3[r6]
            if (r3 != r9) goto L_0x00cb
            java.lang.String r3 = "Leading zeroes not allowed"
            r13.reportInvalidNumber(r3)
            r3 = r4
            goto L_0x0033
        L_0x00a2:
            int r3 = r3 + 1
            r6 = r4
            goto L_0x0047
        L_0x00a6:
            if (r2 != 0) goto L_0x00ad
            java.lang.String r7 = "Exponent indicator not followed by a digit"
            r13.reportUnexpectedNumberChar(r4, r7)
        L_0x00ad:
            int r3 = r3 + -1
            r13._inputPtr = r3
            int r3 = r3 - r5
            org.codehaus.jackson.util.TextBuffer r4 = r13._textBuffer
            char[] r7 = r13._inputBuffer
            r4.resetWithShared(r7, r5, r3)
            org.codehaus.jackson.JsonToken r0 = r13.reset(r0, r1, r6, r2)
            goto L_0x0021
        L_0x00bf:
            r1 = r5
            goto L_0x001b
        L_0x00c2:
            r12 = r4
            r4 = r3
            r3 = r12
            goto L_0x007c
        L_0x00c6:
            r6 = r2
            r12 = r4
            r4 = r3
            r3 = r12
            goto L_0x005e
        L_0x00cb:
            r3 = r4
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.ReaderBasedNumericParser.parseNumberText(int):org.codehaus.jackson.JsonToken");
    }

    private final JsonToken parseNumberText2(boolean z) throws IOException, JsonParseException {
        int i;
        boolean z2;
        char c;
        int i2;
        int i3;
        boolean z3;
        char c2;
        int i4;
        char nextChar;
        char c3;
        int i5;
        int i6;
        char nextChar2;
        boolean z4;
        int i7;
        boolean z5 = true;
        int i8 = 0;
        char[] emptyAndGetCurrentSegment = this._textBuffer.emptyAndGetCurrentSegment();
        if (z) {
            emptyAndGetCurrentSegment[0] = '-';
            i = 1;
        } else {
            i = 0;
        }
        char[] cArr = emptyAndGetCurrentSegment;
        int i9 = i;
        int i10 = 0;
        while (true) {
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                z2 = true;
                c = 0;
                break;
            }
            char[] cArr2 = this._inputBuffer;
            int i11 = this._inputPtr;
            this._inputPtr = i11 + 1;
            char c4 = cArr2[i11];
            if (c4 < '0') {
                z2 = false;
                c = c4;
                break;
            } else if (c4 > '9') {
                z2 = false;
                c = c4;
                break;
            } else {
                i10++;
                if (i10 == 2 && cArr[i9 - 1] == '0') {
                    reportInvalidNumber("Leading zeroes not allowed");
                }
                if (i9 >= cArr.length) {
                    cArr = this._textBuffer.finishCurrentSegment();
                    i2 = 0;
                } else {
                    i2 = i9;
                }
                i9 = i2 + 1;
                cArr[i2] = c4;
            }
        }
        if (i10 == 0) {
            reportInvalidNumber("Missing integer part (next char " + _getCharDesc(c) + ")");
        }
        if (c == '.') {
            int i12 = i9 + 1;
            cArr[i9] = c;
            int i13 = 0;
            char c5 = c;
            int i14 = i12;
            char[] cArr3 = cArr;
            char c6 = c5;
            while (true) {
                if (this._inputPtr >= this._inputEnd && !loadMore()) {
                    c2 = c6;
                    z4 = true;
                    break;
                }
                char[] cArr4 = this._inputBuffer;
                int i15 = this._inputPtr;
                this._inputPtr = i15 + 1;
                c6 = cArr4[i15];
                if (c6 < '0') {
                    c2 = c6;
                    z4 = z2;
                    break;
                } else if (c6 > '9') {
                    c2 = c6;
                    z4 = z2;
                    break;
                } else {
                    i13++;
                    if (i14 >= cArr3.length) {
                        cArr3 = this._textBuffer.finishCurrentSegment();
                        i7 = 0;
                    } else {
                        i7 = i14;
                    }
                    i14 = i7 + 1;
                    cArr3[i7] = c6;
                }
            }
            if (i13 == 0) {
                reportUnexpectedNumberChar(c2, "Decimal point not followed by a digit");
            }
            i3 = i13;
            i9 = i14;
            boolean z6 = z4;
            cArr = cArr3;
            z3 = z6;
        } else {
            i3 = 0;
            z3 = z2;
            c2 = c;
        }
        if (c2 == 'e' || c2 == 'E') {
            if (i4 >= cArr.length) {
                cArr = this._textBuffer.finishCurrentSegment();
                i4 = 0;
            }
            int i16 = i4 + 1;
            cArr[i4] = c2;
            if (this._inputPtr < this._inputEnd) {
                char[] cArr5 = this._inputBuffer;
                int i17 = this._inputPtr;
                this._inputPtr = i17 + 1;
                nextChar = cArr5[i17];
            } else {
                nextChar = getNextChar("expected a digit for number exponent");
            }
            if (nextChar == '-' || nextChar == '+') {
                if (i16 >= cArr.length) {
                    cArr = this._textBuffer.finishCurrentSegment();
                    i6 = 0;
                } else {
                    i6 = i16;
                }
                int i18 = i6 + 1;
                cArr[i6] = nextChar;
                if (this._inputPtr < this._inputEnd) {
                    char[] cArr6 = this._inputBuffer;
                    int i19 = this._inputPtr;
                    this._inputPtr = i19 + 1;
                    nextChar2 = cArr6[i19];
                } else {
                    nextChar2 = getNextChar("expected a digit for number exponent");
                }
                c3 = nextChar2;
                i4 = i18;
                i5 = 0;
            } else {
                c3 = nextChar;
                i4 = i16;
                i5 = 0;
            }
            while (true) {
                if (c3 <= '9' && c3 >= '0') {
                    i5++;
                    if (i4 >= cArr.length) {
                        cArr = this._textBuffer.finishCurrentSegment();
                        i4 = 0;
                    }
                    int i20 = i4 + 1;
                    cArr[i4] = c3;
                    if (this._inputPtr >= this._inputEnd && !loadMore()) {
                        i8 = i5;
                        i4 = i20;
                        break;
                    }
                    char[] cArr7 = this._inputBuffer;
                    int i21 = this._inputPtr;
                    this._inputPtr = i21 + 1;
                    c3 = cArr7[i21];
                    i4 = i20;
                } else {
                    i8 = i5;
                    z5 = z3;
                }
            }
            i8 = i5;
            z5 = z3;
            if (i8 == 0) {
                reportUnexpectedNumberChar(c3, "Exponent indicator not followed by a digit");
            }
        } else {
            z5 = z3;
        }
        if (!z5) {
            this._inputPtr--;
        }
        this._textBuffer.setCurrentLength(i4);
        return reset(z, i10, i3, i8);
    }
}
