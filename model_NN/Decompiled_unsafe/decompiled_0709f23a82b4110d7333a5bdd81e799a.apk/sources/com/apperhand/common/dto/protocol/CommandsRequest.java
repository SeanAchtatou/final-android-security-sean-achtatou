package com.apperhand.common.dto.protocol;

public class CommandsRequest extends BaseRequest {
    private static final long serialVersionUID = -4744028654419893695L;
    private String initiationType;
    private boolean needSpecificParameters;

    public boolean isNeedSpecificParameters() {
        return this.needSpecificParameters;
    }

    public void setNeedSpecificParameters(boolean z) {
        this.needSpecificParameters = z;
    }

    public String getInitiationType() {
        return this.initiationType;
    }

    public void setInitiationType(String str) {
        this.initiationType = str;
    }

    public String toString() {
        return "CommandsRequest [initiationType=" + this.initiationType + ", needSpecificParameters=" + this.needSpecificParameters + ", toString()=" + super.toString() + "]";
    }
}
