package com.apperhand.common.dto;

public class NotificationDTO extends BaseDTO {
    private static final long serialVersionUID = -7960026072330883551L;
    private String bodyText;
    private byte[] icon;
    private String link;
    private String tickerText;
    private String title;

    public NotificationDTO() {
        this(-1, null, null, null, null, null);
    }

    public NotificationDTO(int i, String str, String str2, String str3, String str4, byte[] bArr) {
        this.tickerText = str2;
        this.title = str3;
        this.bodyText = str4;
        this.link = str;
        this.icon = bArr;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public String getTickerText() {
        return this.tickerText;
    }

    public void setTickerText(String str) {
        this.tickerText = str;
    }

    public String getBodyText() {
        return this.bodyText;
    }

    public void setBodyText(String str) {
        this.bodyText = str;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String str) {
        this.link = str;
    }

    public byte[] getIcon() {
        return this.icon;
    }

    public void setIcon(byte[] bArr) {
        this.icon = bArr;
    }

    public String toString() {
        return "NotificationDTO [tickerText=" + this.tickerText + ", title=" + this.title + ", bodyText=" + this.bodyText + ", link=" + this.link + "]";
    }

    public NotificationDTO clone() {
        NotificationDTO notificationDTO = new NotificationDTO();
        notificationDTO.setTitle(getTitle());
        notificationDTO.setLink(getLink());
        notificationDTO.setTickerText(getTickerText());
        notificationDTO.setBodyText(getBodyText());
        System.arraycopy(getIcon(), 0, notificationDTO.getIcon(), 0, getIcon().length);
        return notificationDTO;
    }
}
