package com.apperhand.common.dto;

public class DisplayMetrics extends BaseDTO {
    private static final long serialVersionUID = 2546376529787034622L;
    public float density;
    public int densityDpi;
    public int heightPixels;
    public float scaledDensity;
    public int widthPixels;
    public float xdpi;
    public float ydpi;

    public DisplayMetrics() {
    }

    public DisplayMetrics(float f, int i, int i2, float f2, int i3, float f3, float f4) {
        this.density = f;
        this.densityDpi = i;
        this.heightPixels = i2;
        this.scaledDensity = f2;
        this.widthPixels = i3;
        this.xdpi = f3;
        this.ydpi = f4;
    }

    public float getDensity() {
        return this.density;
    }

    public void setDensity(float f) {
        this.density = f;
    }

    public int getDensityDpi() {
        return this.densityDpi;
    }

    public void setDensityDpi(int i) {
        this.densityDpi = i;
    }

    public int getHeightPixels() {
        return this.heightPixels;
    }

    public void setHeightPixels(int i) {
        this.heightPixels = i;
    }

    public float getScaledDensity() {
        return this.scaledDensity;
    }

    public void setScaledDensity(float f) {
        this.scaledDensity = f;
    }

    public int getWidthPixels() {
        return this.widthPixels;
    }

    public void setWidthPixels(int i) {
        this.widthPixels = i;
    }

    public float getXdpi() {
        return this.xdpi;
    }

    public void setXdpi(float f) {
        this.xdpi = f;
    }

    public float getYdpi() {
        return this.ydpi;
    }

    public void setYdpi(float f) {
        this.ydpi = f;
    }

    public String toString() {
        return "DisplayMetrics [density=" + this.density + ", densityDpi=" + this.densityDpi + ", heightPixels=" + this.heightPixels + ", scaledDensity=" + this.scaledDensity + ", widthPixels=" + this.widthPixels + ", xdpi=" + this.xdpi + ", ydpi=" + this.ydpi + "]";
    }
}
