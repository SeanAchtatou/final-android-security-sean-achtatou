package com.apperhand.common.dto;

import com.apperhand.common.dto.Command;

public class CommandStatus extends Command {
    private static final long serialVersionUID = 4005495549492869561L;
    private String message;
    private Status status;

    public enum Status {
        SUCCESS,
        FAILURE,
        EXCEPTION,
        SUCCESS_WITH_WARNING,
        WAITING_FOR_LOG_DUMP,
        LOG_DUMP,
        OPT_OUT
    }

    public CommandStatus() {
    }

    public CommandStatus(Status status2, String str, Command.Commands commands, String str2) {
        super(commands, str2);
        this.status = status2;
        this.message = str;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status2) {
        this.status = status2;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String str) {
        this.message = str;
    }

    public String toString() {
        return "CommandStatus [status=" + this.status + ", message=" + (this.message != null ? this.message.length() > 200 ? this.message.substring(0, 200) : this.message : "null") + ", command=" + super.toString() + "]";
    }
}
