package com.apperhand.common.dto.protocol;

public class InfoResponse extends BaseResponse {
    private static final long serialVersionUID = -6549210717995475041L;
    private long nextCommandInterval = -1;

    public InfoResponse() {
    }

    public InfoResponse(long j) {
        this.nextCommandInterval = j;
    }

    public long getNextCommandInterval() {
        return this.nextCommandInterval;
    }

    public void setNextCommandInterval(long j) {
        this.nextCommandInterval = j;
    }

    public String toString() {
        return "InfoResponse [nextCommandInterval=" + this.nextCommandInterval + ", toString()=" + super.toString() + "]";
    }
}
