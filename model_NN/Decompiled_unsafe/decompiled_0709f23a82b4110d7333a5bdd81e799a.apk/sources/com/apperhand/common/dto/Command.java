package com.apperhand.common.dto;

import java.util.Map;
import java.util.UUID;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Command extends BaseDTO {
    private static final long serialVersionUID = 4898626949566617224L;
    private Commands command;
    private String id;
    private Map<String, Object> parameters;

    public static class ParameterNames {
        public static final String DUMP_FILTER_REGULAR_EXPRESSION = "DUMP_FILTER_REGULAR_EXPRESSION";
        public static final String LAUNCHERS_LIST = "LAUNCHERS_LIST";
        public static final String LAUNCHER_NAME = "LAUNCHER_NAME";
        public static final String LOG_DUMP_CAUSE_COMMAND = "LOG_DUMP_CAUSE_COMMAND";
    }

    public Command() {
        this((Commands) null);
    }

    public Command(Commands commands) {
        this(commands, UUID.randomUUID().toString(), null);
    }

    public Command(Commands commands, String str) {
        this(commands, str, null);
    }

    public Command(Command command2) {
        this(command2.command, command2.id, command2.parameters);
    }

    public Command(Commands commands, String str, Map<String, Object> map) {
        this.command = commands;
        this.id = str;
        this.parameters = map;
    }

    public Commands getCommand() {
        return this.command;
    }

    public void setCommand(Commands commands) {
        this.command = commands;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String str) {
        this.id = str;
    }

    public Map<String, Object> getParameters() {
        return this.parameters;
    }

    public void setParameters(Map<String, Object> map) {
        this.parameters = map;
    }

    public String toString() {
        return "Command [command=" + this.command + ", id=" + this.id + ", parameters=" + this.parameters + "]";
    }

    public enum Commands {
        COMMANDS("Commands", "Tg0LHwIICkoa"),
        ACTIVATION("Activation", "Tg8HBgYfBVoM"),
        HOMEPAGE("Homepage", "TgYLHwoZBUkM"),
        COMMANDS_STATUS("CommandsStatus", "Tg0LHwIICkoaGhURGwc="),
        BOOKMARKS("Bookmarks", "TgwLHQQEBVwCHQ=="),
        SHORTCUTS("Shortcuts", "Th0MHR0dB1sdHQ=="),
        NOTIFICATIONS("Notifications", "TgALBgYPDU0IGh0KAAc="),
        TERMINATE("Terminate", "ThoBAAIACk8dCw=="),
        DUMP_LOG("DumpLog", "TgoRHx8FC0k="),
        UNEXPECTED_EXCEPTION("UnexpectedException", "ThsKFxcZAU0dCxAAFhdLEgYGGB0"),
        UPGRADE("Upgrade", "ThsUFR0IAEs="),
        INSTALLATION("Installation", "TgcKARsICEIIGh0KAA=="),
        INFO("Info", "TgcKFAA="),
        OPTOUT("Optout", "TgEUBgAcEA==");
        
        private String internalUri;
        private String string;
        private String uri = null;

        public static Commands getInstance(int i) {
            switch (i) {
                case 0:
                    return COMMANDS;
                case 1:
                    return BOOKMARKS;
                case 2:
                    return SHORTCUTS;
                case 3:
                    return NOTIFICATIONS;
                case 4:
                    return TERMINATE;
                case 5:
                    return NOTIFICATIONS;
                default:
                    return null;
            }
        }

        private Commands(String str, String str2) {
            this.string = str;
            this.internalUri = str2;
        }

        public String getInternalUri() {
            return this.internalUri;
        }

        public String getUri() {
            return this.uri;
        }

        public void setUri(String str) {
            this.uri = str;
        }

        public String getString() {
            return this.string;
        }
    }

    public static Commands getCommandByName(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        for (Commands commands : Commands.values()) {
            if (commands.getString().equalsIgnoreCase(str)) {
                return commands;
            }
        }
        return null;
    }
}
