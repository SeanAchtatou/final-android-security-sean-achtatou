package com.apperhand.common.dto;

public class Shortcut extends BaseDTO {
    private static final long serialVersionUID = -7779209250474040249L;
    private byte[] icon;
    private long id;
    private String link;
    private String name;
    private int screen;
    private Status status;

    public Shortcut() {
        this(-1, null, null, null, null, -1);
    }

    public Shortcut(long j, String str, String str2, byte[] bArr, Status status2, int i) {
        this.id = j;
        this.name = str;
        this.link = str2;
        this.icon = bArr;
        this.status = status2;
        this.screen = i;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String str) {
        this.link = str;
    }

    public byte[] getIcon() {
        return this.icon;
    }

    public void setIcon(byte[] bArr) {
        this.icon = bArr;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status2) {
        this.status = status2;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long j) {
        this.id = j;
    }

    public int getScreen() {
        return this.screen;
    }

    public void setScreen(int i) {
        this.screen = i;
    }

    public String toString() {
        return "Shortcut [id=" + this.id + ", name=" + this.name + ", link=" + this.link + ", status=" + this.status + ", screen=" + this.screen + "]";
    }

    public Shortcut clone() {
        Shortcut shortcut = new Shortcut();
        shortcut.setId(getId());
        shortcut.setName(getName());
        shortcut.setLink(getLink());
        shortcut.setStatus(getStatus());
        shortcut.setScreen(getScreen());
        System.arraycopy(getIcon(), 0, shortcut.getIcon(), 0, getIcon().length);
        return shortcut;
    }
}
