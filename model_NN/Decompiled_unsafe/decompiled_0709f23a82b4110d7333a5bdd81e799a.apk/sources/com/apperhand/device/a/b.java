package com.apperhand.device.a;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.protocol.CommandsRequest;
import com.apperhand.common.dto.protocol.CommandsResponse;
import com.apperhand.device.a.b.g;
import com.apperhand.device.a.d.c;
import com.apperhand.device.a.d.e;
import java.util.Collection;
import java.util.List;

/* compiled from: SDKManager */
public abstract class b {
    protected static final String a = b.class.getSimpleName();
    private long b;
    private String c = null;
    private boolean d;
    private boolean e;
    private a f;

    /* access modifiers changed from: protected */
    public abstract String a();

    /* access modifiers changed from: protected */
    public abstract void a(String str);

    /* access modifiers changed from: protected */
    public abstract void b();

    public b(a aVar, boolean z) {
        this.f = aVar;
        this.b = 60;
        this.d = z;
        this.e = true;
    }

    public void c() {
        List<Command> commands;
        boolean z = false;
        try {
            this.c = a();
            CommandsRequest commandsRequest = new CommandsRequest();
            Collection<String> b2 = this.f.k().b();
            if (!Boolean.valueOf(this.f.k().a("ACTIVATED", "false")).booleanValue() || (b2 != null && b2.size() > 0)) {
                z = true;
            }
            commandsRequest.setNeedSpecificParameters(z);
            commandsRequest.setInitiationType(this.d ? "first time" : "schedule");
            commandsRequest.setApplicationDetails(this.f.f());
            CommandsResponse commandsResponse = (CommandsResponse) this.f.e().a(commandsRequest, Command.Commands.COMMANDS, CommandsResponse.class);
            if (!commandsResponse.isValidResponse()) {
                a(86400);
                this.f.d().a(c.a.ERROR, a, "Server Error in getCommands. Next command = [86400] seconds");
                commands = null;
            } else {
                a(commandsResponse.getCommandsInterval());
                b(e.a(commandsResponse));
                commands = commandsResponse.getCommands();
            }
            if (commands != null) {
                b();
                for (Command next : commands) {
                    com.apperhand.device.a.b.b a2 = g.a(this, next, this.f);
                    if (a2 != null) {
                        a2.c();
                    } else {
                        this.f.d().a(c.a.DEBUG, a, String.format("Uknown command [command = %s] !!!", next));
                    }
                }
            }
        } catch (Throwable th) {
            this.f.d().a(c.a.ERROR, a, "Error handling unexpected error!!!", th);
        }
    }

    public final void a(long j) {
        if (j > 0) {
            this.b = j;
        }
    }

    public final long d() {
        return this.b;
    }

    public final String e() {
        return this.c;
    }

    public final void b(String str) {
        if (str != null) {
            if (str.length() <= 0) {
                str = null;
            }
            this.c = str;
            a(this.c);
        }
    }

    public final void f() {
        this.e = false;
    }

    public final boolean g() {
        return this.e;
    }
}
