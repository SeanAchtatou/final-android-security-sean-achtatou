package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandInformation;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.InfoRequest;
import com.apperhand.common.dto.protocol.InfoResponse;
import com.apperhand.device.a.a;
import com.apperhand.device.a.a.b;
import com.apperhand.device.a.a.d;
import com.apperhand.device.a.d.c;
import com.apperhand.device.a.d.f;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: InfoService */
public final class e extends b {
    Map<String, Object> g;
    private d h;
    private b i;

    public e(com.apperhand.device.a.b bVar, a aVar, String str, Command command) {
        super(bVar, aVar, str, command.getCommand());
        this.h = aVar.i();
        this.i = aVar.h();
        this.g = command.getParameters();
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws f {
        return null;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws f {
        CommandInformation a;
        InfoRequest infoRequest = new InfoRequest();
        ArrayList arrayList = new ArrayList();
        infoRequest.setApplicationDetails(this.e.f());
        infoRequest.setInformation(arrayList);
        for (String str : this.g.keySet()) {
            List list = (List) this.g.get(str);
            if (list != null) {
                switch (Command.getCommandByName(str)) {
                    case SHORTCUTS:
                        this.h.a();
                        a = this.h.a(list);
                        break;
                    case BOOKMARKS:
                        a = this.i.a(list);
                        break;
                    default:
                        a = null;
                        break;
                }
                arrayList.add(a);
            }
        }
        return a(infoRequest);
    }

    private BaseResponse a(InfoRequest infoRequest) {
        try {
            return (InfoResponse) this.e.e().a(infoRequest, Command.Commands.INFO, InfoResponse.class);
        } catch (f e) {
            this.e.d().a(c.a.DEBUG, this.a, "Unable to handle Info command!!!!", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws f {
    }
}
