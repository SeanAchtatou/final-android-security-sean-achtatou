package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.OptOutDetails;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.OptOutRequest;
import com.apperhand.common.dto.protocol.OptOutResponse;
import com.apperhand.device.a.a;
import com.apperhand.device.a.b;
import com.apperhand.device.a.d.c;
import java.util.Map;

/* compiled from: OptoutService */
public final class f extends b {
    Map<String, Object> g;

    public f(b bVar, a aVar, String str, Command command) {
        super(bVar, aVar, str, command.getCommand());
        this.g = command.getParameters();
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws com.apperhand.device.a.d.f {
        return null;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws com.apperhand.device.a.d.f {
        OptOutRequest optOutRequest = new OptOutRequest();
        optOutRequest.setApplicationDetails(this.e.f());
        OptOutDetails optOutDetails = new OptOutDetails();
        optOutDetails.setCommand((Command.Commands) this.g.get("command"));
        optOutDetails.setMessage((String) this.g.get("message"));
        optOutDetails.setPermanent(((Boolean) this.g.get("permanent")).booleanValue());
        optOutRequest.setDetails(optOutDetails);
        return a(optOutRequest);
    }

    private BaseResponse a(OptOutRequest optOutRequest) {
        try {
            return (OptOutResponse) this.e.e().a(optOutRequest, Command.Commands.OPTOUT, OptOutResponse.class);
        } catch (com.apperhand.device.a.d.f e) {
            this.e.d().a(c.a.DEBUG, this.a, "Unable to handle Optout command!!!!", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws com.apperhand.device.a.d.f {
    }
}
