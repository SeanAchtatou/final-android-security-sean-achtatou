package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.device.a.a;
import com.apperhand.device.a.b;

/* compiled from: ServiceFactory */
public final class g {
    private g() {
    }

    public static b a(b bVar, Command command, a aVar) {
        switch (command.getCommand()) {
            case ACTIVATION:
                return new a(bVar, aVar, command.getId(), command.getCommand());
            case BOOKMARKS:
                return new c(bVar, aVar, command.getId(), command.getCommand());
            case SHORTCUTS:
                return new h(bVar, aVar, command.getId(), command);
            case TERMINATE:
                return new i(bVar, aVar, command.getId(), command.getCommand());
            case INFO:
                return new e(bVar, aVar, command.getId(), command);
            case OPTOUT:
                return new f(bVar, aVar, command.getId(), command);
            case HOMEPAGE:
                return new d(bVar, aVar, command.getId(), command);
            default:
                return null;
        }
    }
}
