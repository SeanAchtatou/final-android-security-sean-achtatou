package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.ActivationRequest;
import com.apperhand.common.dto.protocol.ActivationResponse;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusRequest;
import com.apperhand.device.a.b;
import com.apperhand.device.a.d.c;
import com.apperhand.device.a.d.f;
import java.util.Map;

/* compiled from: ActivationService */
public final class a extends b {
    private com.apperhand.device.a.a.a g;

    public a(b bVar, com.apperhand.device.a.a aVar, String str, Command.Commands commands) {
        super(bVar, aVar, str, commands);
        this.g = aVar.g();
    }

    private ActivationResponse a(ActivationRequest activationRequest) {
        try {
            return (ActivationResponse) this.e.e().a(activationRequest, Command.Commands.ACTIVATION, ActivationResponse.class);
        } catch (f e) {
            this.e.d().a(c.a.DEBUG, this.a, "Unable to handle Activation command!!!!", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws f {
        Map<String, String> parameters = ((ActivationResponse) baseResponse).getActivation().getParameters();
        this.e.k().b("ACTIVATED", "true");
        if (parameters == null || parameters.size() <= 0) {
            return null;
        }
        for (String next : parameters.keySet()) {
            this.e.k().b(next, parameters.get(next));
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() {
        ActivationRequest activationRequest = new ActivationRequest();
        activationRequest.setApplicationDetails(this.e.f());
        activationRequest.setMissingParameters(this.e.k().b());
        activationRequest.setFirstTimeActivation(!Boolean.getBoolean(this.e.k().a("ACTIVATED", "false")));
        return a(activationRequest);
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws f {
        a(b());
    }

    /* access modifiers changed from: protected */
    public final CommandStatusRequest b() throws f {
        CommandStatusRequest b = super.b();
        b.setStatuses(a(Command.Commands.ACTIVATION, CommandStatus.Status.SUCCESS, this.e.a() + " was activated, SABABA!!!", this.g.a()));
        return b;
    }
}
