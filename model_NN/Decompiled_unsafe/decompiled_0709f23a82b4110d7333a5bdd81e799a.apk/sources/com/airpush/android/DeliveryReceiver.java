package com.airpush.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.mobclick.android.UmengConstants;
import java.util.List;
import org.apache.http.NameValuePair;

public class DeliveryReceiver extends BroadcastReceiver {
    protected static Context b = null;
    List<NameValuePair> a = null;
    private String c = null;
    private String d = null;
    private String e = null;
    private String f = null;
    private String g = null;
    private String h = null;
    private String i = null;
    private String j = null;
    private String k = null;
    private String l = null;
    private String m = "http://api.airpush.com/redirect.php?market=";
    private String n = null;
    private String o;
    private Long p;
    private String q;
    private String r;
    private String s;
    private String t;

    public void onReceive(Context context, Intent intent) {
        b = context;
        try {
            Intent intent2 = new Intent();
            if (Constants.a(b)) {
                Log.i("AirpushSDK", "Delivering Message");
                if (intent.getAction().equals("setDeliveryReceiverPhone")) {
                    this.n = intent.getStringExtra("apikey");
                    this.d = new String(intent.getStringExtra("appId"));
                    this.c = intent.getStringExtra("imei");
                    this.r = new String(intent.getStringExtra("number"));
                    this.q = new String(intent.getStringExtra("title"));
                    this.e = new String(intent.getStringExtra("text"));
                    this.o = new String(intent.getStringExtra("imageurl"));
                    this.p = Long.valueOf(intent.getLongExtra("expiry_time", 60));
                    this.g = intent.getStringExtra("campId");
                    this.h = intent.getStringExtra("creativeId");
                    intent2.setAction("com.airpush.android.PushServiceStart" + this.d);
                    intent2.putExtra("adType", "CC");
                    intent2.putExtra("appId", this.d);
                    intent2.putExtra(UmengConstants.AtomKey_Type, "delivery");
                    intent2.putExtra("number", this.r);
                    intent2.putExtra("title", this.q);
                    intent2.putExtra("text", this.e);
                    intent2.putExtra("apikey", this.n);
                    intent2.putExtra("imageurl", this.o);
                    intent2.putExtra("expiry_time", this.p);
                } else if (intent.getAction().equals("setDeliveryReceiverSMS")) {
                    this.n = intent.getStringExtra("apikey");
                    this.d = new String(intent.getStringExtra("appId"));
                    this.c = intent.getStringExtra("imei");
                    this.r = new String(intent.getStringExtra("number"));
                    this.s = new String(intent.getStringExtra("sms"));
                    this.q = new String(intent.getStringExtra("title"));
                    this.e = new String(intent.getStringExtra("text"));
                    this.o = new String(intent.getStringExtra("imageurl"));
                    this.p = Long.valueOf(intent.getLongExtra("expiry_time", 60));
                    this.g = intent.getStringExtra("campId");
                    this.h = intent.getStringExtra("creativeId");
                    intent2.setAction("com.airpush.android.PushServiceStart" + this.d);
                    intent2.putExtra("adType", "CM");
                    intent2.putExtra("appId", this.d);
                    intent2.putExtra(UmengConstants.AtomKey_Type, "delivery");
                    intent2.putExtra("number", this.r);
                    intent2.putExtra("title", this.q);
                    intent2.putExtra("text", this.e);
                    intent2.putExtra("sms", this.s);
                    intent2.putExtra("apikey", this.n);
                    intent2.putExtra("imageurl", this.o);
                    intent2.putExtra("expiry_time", this.p);
                    intent2.putExtra("campId", this.g);
                    intent2.putExtra("creativeId", this.h);
                } else if (intent.getAction().equals("setDeliveryReceiverWEB")) {
                    this.n = intent.getStringExtra("apikey");
                    this.d = new String(intent.getStringExtra("appId"));
                    this.c = intent.getStringExtra("imei");
                    this.f = new String(intent.getStringExtra("url"));
                    this.q = new String(intent.getStringExtra("title"));
                    this.e = new String(intent.getStringExtra("text"));
                    this.o = new String(intent.getStringExtra("imageurl"));
                    this.t = new String(intent.getStringExtra("header"));
                    this.p = Long.valueOf(intent.getLongExtra("expiry_time", 60));
                    this.g = intent.getStringExtra("campId");
                    this.h = intent.getStringExtra("creativeId");
                    intent2.setAction("com.airpush.android.PushServiceStart" + this.d);
                    intent2.putExtra("adType", "W");
                    intent2.putExtra("appId", this.d);
                    intent2.putExtra(UmengConstants.AtomKey_Type, "delivery");
                    intent2.putExtra("link", this.f);
                    intent2.putExtra("header", this.t);
                    intent2.putExtra("title", this.q);
                    intent2.putExtra("text", this.e);
                    intent2.putExtra("apikey", this.n);
                    intent2.putExtra("imageurl", this.o);
                    intent2.putExtra("expiry_time", this.p);
                    intent2.putExtra("campId", this.g);
                    intent2.putExtra("creativeId", this.h);
                } else if (intent.getAction().equals("setDeliveryReceiverMARKET")) {
                    this.n = intent.getStringExtra("apikey");
                    this.d = new String(intent.getStringExtra("appId"));
                    this.c = intent.getStringExtra("imei");
                    this.f = new String(intent.getStringExtra("url"));
                    this.q = new String(intent.getStringExtra("title"));
                    this.e = new String(intent.getStringExtra("text"));
                    this.o = new String(intent.getStringExtra("imageurl"));
                    this.p = Long.valueOf(intent.getLongExtra("expiry_time", 60));
                    this.g = intent.getStringExtra("campId");
                    this.h = intent.getStringExtra("creativeId");
                    intent2.setAction("com.airpush.android.PushServiceStart" + this.d);
                    intent2.putExtra("adType", "A");
                    intent2.putExtra("appId", this.d);
                    intent2.putExtra(UmengConstants.AtomKey_Type, "delivery");
                    intent2.putExtra("link", this.f);
                    intent2.putExtra("title", this.q);
                    intent2.putExtra("text", this.e);
                    intent2.putExtra("apikey", this.n);
                    intent2.putExtra("imageurl", this.o);
                    intent2.putExtra("expiry_time", this.p);
                    intent2.putExtra("campId", this.g);
                    intent2.putExtra("creativeId", this.h);
                } else if (intent.getAction().equals("SetIconReceiver")) {
                    this.n = intent.getStringExtra("apikey");
                    this.d = new String(intent.getStringExtra("appId"));
                    this.c = intent.getStringExtra("imei");
                    this.f = new String(intent.getStringExtra("url"));
                    this.q = new String(intent.getStringExtra("title"));
                    this.e = new String(intent.getStringExtra("text"));
                    this.o = new String(intent.getStringExtra("imageurl"));
                    this.p = Long.valueOf(intent.getLongExtra("expiry_time", 60));
                    this.g = intent.getStringExtra("campId");
                    this.h = intent.getStringExtra("creativeId");
                }
                context.startService(intent2);
            }
        } catch (Exception e2) {
            Log.e("AirpushSDK", "Delivering Message Failed");
        }
    }
}
