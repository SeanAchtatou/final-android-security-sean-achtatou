package com.airpush.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import com.mobclick.android.UmengConstants;
import java.io.InputStream;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class PushAds extends Activity implements View.OnClickListener {
    private static String B = "1";
    private static HttpPost C;
    private static BasicHttpParams D;
    private static int E;
    private static int F;
    private static DefaultHttpClient G;
    private static BasicHttpResponse H;
    private static HttpEntity I;
    private static String b = null;
    /* access modifiers changed from: private */
    public static List<NameValuePair> f = null;
    private static Context m;
    /* access modifiers changed from: private */
    public boolean A;
    /* access modifiers changed from: private */
    public String J;
    private String K = "#FFFFFF";
    private String L = "#FFFFFF";
    private Runnable M = new Runnable() {
        public void run() {
            PushAds.this.f();
        }
    };
    private View.OnClickListener N = new View.OnClickListener() {
        public void onClick(View view) {
            try {
                PushAds.this.b(PushAds.this.u);
                PushAds.f = SetPreferences.a(PushAds.this.getApplicationContext());
                PushAds.f.add(new BasicNameValuePair("model", "log"));
                PushAds.f.add(new BasicNameValuePair("action", "setfptracking"));
                PushAds.f.add(new BasicNameValuePair("APIKEY", PushAds.this.k));
                PushAds.f.add(new BasicNameValuePair("event", "fclick"));
                PushAds.f.add(new BasicNameValuePair("campId", PushAds.this.J));
                PushAds.f.add(new BasicNameValuePair("creativeId", PushAds.this.d));
                new Handler().postDelayed(PushAds.this.Q, 3000);
            } catch (Exception e) {
                Log.i("AirpushSDK", "Display Ad Network Error, please try again later. ");
            }
        }
    };
    private View.OnClickListener O = new View.OnClickListener() {
        public void onClick(View view) {
            try {
                PushAds.this.b(PushAds.this.u);
                PushAds.f = SetPreferences.a(PushAds.this.getApplicationContext());
                PushAds.f.add(new BasicNameValuePair("model", "log"));
                PushAds.f.add(new BasicNameValuePair("action", "setfptracking"));
                PushAds.f.add(new BasicNameValuePair("APIKEY", PushAds.this.k));
                PushAds.f.add(new BasicNameValuePair("event", "fclick"));
                PushAds.f.add(new BasicNameValuePair("campId", PushAds.this.J));
                PushAds.f.add(new BasicNameValuePair("creativeId", PushAds.this.d));
                new Handler().postDelayed(PushAds.this.Q, 5000);
            } catch (Exception e) {
                Log.i("AirpushSDK", "Display Ad Network Error, please try again later. ");
            }
        }
    };
    private Runnable P = new Runnable() {
        public void run() {
            if (!PushAds.this.A) {
                PushAds.this.g();
            }
        }
    };
    /* access modifiers changed from: private */
    public Runnable Q = new Runnable() {
        public void run() {
            if (!PushAds.this.A) {
                PushAds.this.h();
            }
        }
    };
    private View.OnClickListener R = new View.OnClickListener() {
        public void onClick(View view) {
            PushAds.this.finish();
        }
    };
    String a = "http://api.airpush.com/api.php";
    private String c = null;
    /* access modifiers changed from: private */
    public String d = null;
    private String e = null;
    private String g;
    private String h;
    private String i;
    private String j;
    /* access modifiers changed from: private */
    public String k = null;
    private String l;
    /* access modifiers changed from: private */
    public boolean n = true;
    private boolean o = false;
    private int p = 17301620;
    /* access modifiers changed from: private */
    public boolean q = true;
    private Intent r;
    private HttpEntity s;
    private String t;
    /* access modifiers changed from: private */
    public String u;
    private int v;
    private int w;
    private boolean x = true;
    private String y = "#000000";
    private String z = "#000000";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        m = getApplicationContext();
        this.r = getIntent();
        this.t = this.r.getAction();
        getWindowManager().getDefaultDisplay();
        this.w = 320;
        this.v = 350;
        this.g = this.r.getStringExtra("adType");
        if (this.g.equals("searchad")) {
            Log.i("AirpushSDK", "Search Clicked");
            return;
        }
        if (this.g.equals("ShoWDialog")) {
            this.e = this.r.getStringExtra("appId");
            this.k = this.r.getStringExtra("apikey");
            this.o = this.r.getBooleanExtra("test", false);
            this.p = this.r.getIntExtra("icon", 17301620);
            a();
        }
        if (this.t.equals("CC")) {
            if (this.g.equals("CC")) {
                Log.i("AirpushSDK", "Pushing Ads.....");
                if (m.getSharedPreferences("airpushNotificationPref", 1) != null) {
                    SharedPreferences sharedPreferences = m.getSharedPreferences("airpushNotificationPref", 1);
                    this.e = sharedPreferences.getString("appId", this.r.getStringExtra("appId"));
                    this.k = sharedPreferences.getString("apikey", this.r.getStringExtra("apikey"));
                    this.j = sharedPreferences.getString("number", this.r.getStringExtra("number"));
                    this.c = sharedPreferences.getString("campId", this.r.getStringExtra("campId"));
                    this.d = sharedPreferences.getString("creativeId", this.r.getStringExtra("creativeId"));
                } else {
                    this.e = this.r.getStringExtra("appId");
                    this.k = this.r.getStringExtra("apikey");
                    this.c = this.r.getStringExtra("campId");
                    this.d = this.r.getStringExtra("creativeId");
                    this.j = this.r.getStringExtra("number");
                }
                Intent intent = new Intent();
                intent.setAction("com.airpush.android.PushServiceStart" + this.e);
                intent.putExtra(UmengConstants.AtomKey_Type, "PostAdValues");
                intent.putExtras(this.r);
                startService(intent);
                c();
            }
        } else if (this.t.equals("CM")) {
            if (this.g.equals("CM")) {
                Log.i("AirpushSDK", "Pushing Ads.....");
                if (m.getSharedPreferences("airpushNotificationPref", 1) != null) {
                    SharedPreferences sharedPreferences2 = m.getSharedPreferences("airpushNotificationPref", 1);
                    this.e = sharedPreferences2.getString("appId", this.r.getStringExtra("appId"));
                    this.k = sharedPreferences2.getString("apikey", this.r.getStringExtra("apikey"));
                    this.h = sharedPreferences2.getString("sms", this.r.getStringExtra("sms"));
                    this.c = sharedPreferences2.getString("campId", this.r.getStringExtra("campId"));
                    this.d = sharedPreferences2.getString("creativeId", this.r.getStringExtra("creativeId"));
                    this.i = sharedPreferences2.getString("number", this.r.getStringExtra("number"));
                } else {
                    this.e = this.r.getStringExtra("appId");
                    this.k = this.r.getStringExtra("apikey");
                    this.c = this.r.getStringExtra("campId");
                    this.d = this.r.getStringExtra("creativeId");
                    this.h = this.r.getStringExtra("sms");
                    this.i = this.r.getStringExtra("number");
                }
                Intent intent2 = new Intent();
                intent2.setAction("com.airpush.android.PushServiceStart" + this.e);
                intent2.putExtra(UmengConstants.AtomKey_Type, "PostAdValues");
                intent2.putExtras(this.r);
                startService(intent2);
                d();
            }
        } else if (!this.t.equals("Web And App")) {
        } else {
            if (this.g.equals("W") || this.g.equals("A")) {
                Log.i("AirpushSDK", "Pushing Ads.....");
                if (m.getSharedPreferences("airpushNotificationPref", 1) != null) {
                    SharedPreferences sharedPreferences3 = m.getSharedPreferences("airpushNotificationPref", 1);
                    this.e = sharedPreferences3.getString("appId", this.r.getStringExtra("appId"));
                    this.k = sharedPreferences3.getString("apikey", this.r.getStringExtra("apikey"));
                    b = sharedPreferences3.getString("url", this.r.getStringExtra("url"));
                    this.c = sharedPreferences3.getString("campId", this.r.getStringExtra("campId"));
                    this.d = sharedPreferences3.getString("creativeId", this.r.getStringExtra("creativeId"));
                    this.l = sharedPreferences3.getString("header", this.r.getStringExtra("header"));
                } else {
                    this.e = this.r.getStringExtra("appId");
                    this.k = this.r.getStringExtra("apikey");
                    this.c = this.r.getStringExtra("campId");
                    this.d = this.r.getStringExtra("creativeId");
                    b = this.r.getStringExtra("url");
                    this.l = this.r.getStringExtra("header");
                }
                Intent intent3 = new Intent();
                intent3.setAction("com.airpush.android.PushServiceStart" + this.e);
                intent3.putExtra(UmengConstants.AtomKey_Type, "PostAdValues");
                intent3.putExtras(this.r);
                startService(intent3);
                setTitle(this.l);
                a(b);
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }

    private void c() {
        Log.i("AirpushSDK", "Pushing CC Ads.....");
        startActivity(new Intent("android.intent.action.DIAL", Uri.parse("tel:" + this.j)));
    }

    private void d() {
        Log.i("AirpushSDK", "Pushing CM Ads.....");
        Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse("smsto:" + this.i));
        intent.putExtra("sms_body", this.h);
        startActivity(intent);
    }

    private void a(String str) {
        Log.i("AirpushSDK", "Pushing Web and App Ads.....");
        CustomWebView customWebView = new CustomWebView(this);
        customWebView.loadUrl(str);
        setContentView(customWebView);
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setMessage("Support the App developer by enabling ads in the notification tray, limited to 1 per day.");
            builder.setPositiveButton("I Agree", new DialogInterface.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.airpush.android.PushAds.a(com.airpush.android.PushAds, boolean):void
                 arg types: [com.airpush.android.PushAds, int]
                 candidates:
                  com.airpush.android.PushAds.a(java.util.List<org.apache.http.NameValuePair>, android.content.Context):org.apache.http.HttpEntity
                  com.airpush.android.PushAds.a(com.airpush.android.PushAds, java.lang.String):void
                  com.airpush.android.PushAds.a(com.airpush.android.PushAds, boolean):void */
                public void onClick(DialogInterface dialogInterface, int i) {
                    PushAds.this.n = false;
                    PushAds.this.q = true;
                    PushAds.this.e();
                    PushAds.this.finish();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.airpush.android.PushAds.a(com.airpush.android.PushAds, boolean):void
                 arg types: [com.airpush.android.PushAds, int]
                 candidates:
                  com.airpush.android.PushAds.a(java.util.List<org.apache.http.NameValuePair>, android.content.Context):org.apache.http.HttpEntity
                  com.airpush.android.PushAds.a(com.airpush.android.PushAds, java.lang.String):void
                  com.airpush.android.PushAds.a(com.airpush.android.PushAds, boolean):void */
                public void onClick(DialogInterface dialogInterface, int i) {
                    PushAds.this.n = false;
                    PushAds.this.q = false;
                    PushAds.this.e();
                    PushAds.this.finish();
                }
            });
            builder.create();
            builder.show();
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        new Airpush().a(m, this.e, this.k, this.o, this.n, this.p, this.q);
    }

    /* access modifiers changed from: private */
    public void f() {
        this.s = HttpPostData.a(f, getApplicationContext());
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        finish();
    }

    /* access modifiers changed from: private */
    public void g() {
        f = SetPreferences.a(m);
        f.add(new BasicNameValuePair("model", "log"));
        f.add(new BasicNameValuePair("action", "settexttracking"));
        f.add(new BasicNameValuePair("APIKEY", this.k));
        f.add(new BasicNameValuePair("event", "trayDelivered"));
        f.add(new BasicNameValuePair("campId", this.c));
        f.add(new BasicNameValuePair("creativeId", this.d));
        this.s = a(f, getApplicationContext());
        StringBuffer stringBuffer = new StringBuffer();
        try {
            InputStream content = this.s.getContent();
            while (true) {
                int read = content.read();
                if (read == -1) {
                    break;
                }
                stringBuffer.append((char) read);
            }
        } catch (Exception e2) {
        }
        stringBuffer.toString();
    }

    /* access modifiers changed from: private */
    public void h() {
        this.s = HttpPostData.a(f, getApplicationContext());
        StringBuffer stringBuffer = new StringBuffer();
        try {
            InputStream content = this.s.getContent();
            while (true) {
                int read = content.read();
                if (read == -1) {
                    break;
                }
                stringBuffer.append((char) read);
            }
        } catch (Exception e2) {
        }
        stringBuffer.toString();
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        Log.i("AirpushSDK", "Displaying Ad.");
        CustomWebView customWebView = new CustomWebView(this);
        customWebView.loadUrl(str);
        setContentView(customWebView);
    }

    public void onClick(View view) {
    }

    protected static HttpEntity a(List<NameValuePair> list, Context context) {
        if (Constants.a(context)) {
            try {
                C = new HttpPost("http://api.airpush.com/v2/api.php");
                C.setEntity(new UrlEncodedFormEntity(list));
                D = new BasicHttpParams();
                E = 3000;
                HttpConnectionParams.setConnectionTimeout(D, E);
                F = 3000;
                HttpConnectionParams.setSoTimeout(D, F);
                G = new DefaultHttpClient(D);
                H = G.execute(C);
                I = H.getEntity();
                return I;
            } catch (Exception e2) {
                Airpush.a(context, 1800000);
                return null;
            }
        } else {
            Airpush.a(context, 3600000);
            return null;
        }
    }
}
