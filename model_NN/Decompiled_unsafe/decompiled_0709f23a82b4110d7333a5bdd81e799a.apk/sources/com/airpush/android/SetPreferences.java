package com.airpush.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.WebView;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class SetPreferences {
    private static String A = "0";
    private static String B = "0";
    private static String C = "0";
    private static String D = "0";
    private static String E = "0";
    private static String F = "0";
    /* access modifiers changed from: private */
    public static String G = "0";
    /* access modifiers changed from: private */
    public static String H = "0";
    private static Context I;
    private static String J;
    protected static JSONObject a = null;
    protected static String b = "0";
    protected static List<NameValuePair> c;
    protected static String d;
    private static int h;
    private static String n;
    private static String o;
    private static String p;
    private static boolean q;
    private static String r = "4.02";
    private static String s = "unknown";
    private static String t = "unknown";
    private static String u = "unknown";
    private static String v = "00";
    private static String w = "invalid";
    private static String x = "0";
    private static String y = "0";
    private static String z = "0";
    private String e = "0";
    private String f = "0";
    private boolean g;
    private boolean i;
    private String j;
    private boolean k;
    private boolean l;
    private boolean m;

    /* access modifiers changed from: protected */
    public void a(Context context, String str, String str2, boolean z2, boolean z3, boolean z4, boolean z5) {
        I = context;
        C = str;
        D = str2;
        this.g = this.g;
        this.k = z5;
        this.l = z3;
        this.m = z4;
        q = z2;
        o = new WebView(I).getSettings().getUserAgentString();
        Log.i("User Agent", "User Agent : " + this.k);
        p = b();
        c(I);
        TelephonyManager telephonyManager = (TelephonyManager) I.getSystemService("phone");
        this.f = telephonyManager.getDeviceId();
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(this.f.getBytes(), 0, this.f.length());
            b = new BigInteger(1, instance.digest()).toString(16);
            v = new Date().toString();
            A = Build.MODEL;
            B = Build.MANUFACTURER;
            z = telephonyManager.getNetworkOperatorName();
            y = telephonyManager.getSimOperatorName();
            x = Build.VERSION.SDK;
            n = Settings.Secure.getString(I.getContentResolver(), "android_id");
            w = I.getPackageName();
            F = String.valueOf(b) + C + v;
            MessageDigest instance2 = MessageDigest.getInstance("MD5");
            instance2.update(F.getBytes(), 0, F.length());
            F = new BigInteger(1, instance2.digest()).toString(16);
            a();
        } catch (NoSuchAlgorithmException e2) {
            Log.i("AirpushSDK", "IMEI conversion Error ");
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            v = new Date().toString();
            SharedPreferences.Editor edit = I.getSharedPreferences("dataPrefs", 2).edit();
            edit.putString("apikey", D);
            edit.putString("appId", C);
            edit.putString("imei", b);
            edit.putString("connectionType", p);
            edit.putString("token", F);
            edit.putString("request_timestamp", v);
            edit.putString("packageName", w);
            edit.putString("version", x);
            edit.putString("carrier", y);
            edit.putString("networkOperator", z);
            edit.putString("phoneModel", A);
            edit.putString("manufacturer", B);
            edit.putString("longitude", G);
            edit.putString("latitude", H);
            edit.putString("sdkversion", "4.02");
            edit.putString("android_id", n);
            edit.putBoolean("showDialog", this.g);
            edit.putBoolean("showAd", this.i);
            edit.putBoolean("testMode", q);
            edit.putBoolean("doPush", this.k);
            edit.putBoolean("doSearch", this.l);
            edit.putBoolean("searchIconTestMode", this.m);
            edit.putInt("icon", h);
            edit.putString("useragent", o);
            this.j = Base64.encodeString(String.valueOf(C) + this.f + p + F + v + w + x + y + z + A + B + G + H + o);
            edit.putString("asp", this.j);
            edit.putString("imeinumber", this.f);
            edit.commit();
        } catch (Exception e2) {
        }
    }

    private static void b(Context context) {
        try {
            if (!context.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("dataPrefs", 1);
                C = sharedPreferences.getString("appId", "invalid");
                D = sharedPreferences.getString("apikey", "airpush");
                b = sharedPreferences.getString("imei", "invalid");
                F = sharedPreferences.getString("token", "invalid");
                v = new Date().toString();
                w = sharedPreferences.getString("packageName", "invalid");
                x = sharedPreferences.getString("version", "invalid");
                y = sharedPreferences.getString("carrier", "invalid");
                z = sharedPreferences.getString("networkOperator", "invalid");
                A = sharedPreferences.getString("phoneModel", "invalid");
                B = sharedPreferences.getString("manufacturer", "invalid");
                G = sharedPreferences.getString("longitude", "invalid");
                H = sharedPreferences.getString("latitude", "invalid");
                r = sharedPreferences.getString("sdkversion", "4.02");
                p = sharedPreferences.getString("connectionType", "0");
                q = sharedPreferences.getBoolean("testMode", false);
                o = sharedPreferences.getString("useragent", "Default");
                h = sharedPreferences.getInt("icon", 17301514);
                n = sharedPreferences.getString("android_id", "Android_id");
                return;
            }
            w = I.getPackageName();
            J = HttpPostData.a("http://api.airpush.com/model/user/getappinfo.php?packageName=" + w, "default", "default", I);
            C = c(J);
            D = d(J);
        } catch (Exception e2) {
        }
    }

    private static String c(String str) {
        try {
            a = new JSONObject(str);
            return a.getString("appid");
        } catch (JSONException e2) {
            return "invalid Id";
        }
    }

    private static String d(String str) {
        try {
            a = new JSONObject(str);
            return a.getString("authkey");
        } catch (JSONException e2) {
            return "invalid key";
        }
    }

    protected static List<NameValuePair> a(Context context) {
        try {
            b(context);
            c = new ArrayList();
            c.add(new BasicNameValuePair("apikey", D));
            c.add(new BasicNameValuePair("appId", C));
            c.add(new BasicNameValuePair("imei", b));
            c.add(new BasicNameValuePair("token", F));
            c.add(new BasicNameValuePair("request_timestamp", v));
            c.add(new BasicNameValuePair("packageName", w));
            c.add(new BasicNameValuePair("version", x));
            c.add(new BasicNameValuePair("carrier", y));
            c.add(new BasicNameValuePair("networkOperator", z));
            c.add(new BasicNameValuePair("phoneModel", A));
            c.add(new BasicNameValuePair("manufacturer", B));
            c.add(new BasicNameValuePair("longitude", G));
            c.add(new BasicNameValuePair("latitude", H));
            c.add(new BasicNameValuePair("sdkversion", r));
            c.add(new BasicNameValuePair("wifi", p));
            c.add(new BasicNameValuePair("useragent", o));
            c.add(new BasicNameValuePair("android_id", n));
            c.add(new BasicNameValuePair("longitude", G));
            c.add(new BasicNameValuePair("latitude", H));
            d = "http://api.airpush.com/v2/api.php?apikey=" + D + "&appId=" + C + "&imei=" + b + "&token=" + F + "&request_timestamp=" + v + "&packageName=" + w + "&version=" + x + "&carrier=" + y + "&networkOperator=" + z + "&phoneModel=" + A + "&manufacturer=" + B + "&longitude=" + G + "&latitude=" + H + "&sdkversion=" + r + "&wifi=" + p + "&useragent=" + o;
        } catch (Exception e2) {
        }
        return c;
    }

    private String b() {
        if (((ConnectivityManager) I.getSystemService("connectivity")).getActiveNetworkInfo().getTypeName().equals("WIFI")) {
            return "1";
        }
        return "0";
    }

    public static boolean isEnabled(Context context) {
        if (context.getSharedPreferences("sdkPrefs", 1).equals(null)) {
            return true;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences("sdkPrefs", 1);
        if (sharedPreferences.contains("SDKEnabled")) {
            return sharedPreferences.getBoolean("SDKEnabled", false);
        }
        return true;
    }

    private void c(Context context) {
        if (context.getPackageManager().checkPermission("android.permission.ACCESS_COARSE_LOCATION", context.getPackageName()) == 0 && context.getPackageManager().checkPermission("android.permission.ACCESS_FINE_LOCATION", context.getPackageName()) == 0) {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            Location lastKnownLocation = locationManager.getLastKnownLocation("network");
            if (lastKnownLocation == null) {
                locationManager.requestLocationUpdates("network", 0, 0.0f, new MyLocationListener());
                return;
            }
            G = String.valueOf(lastKnownLocation.getLongitude());
            H = String.valueOf(lastKnownLocation.getLatitude());
        }
    }

    public class MyLocationListener implements LocationListener {
        public MyLocationListener() {
        }

        public void onLocationChanged(Location location) {
            try {
                SetPreferences.G = String.valueOf(location.getLongitude());
                SetPreferences.H = String.valueOf(location.getLatitude());
            } catch (Exception e) {
            }
        }

        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }
    }
}
