package com.airpush.android;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.provider.Browser;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.WindowManager;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Airpush {
    private static HttpPost A;
    private static BasicHttpParams B;
    private static int C;
    private static int D;
    private static DefaultHttpClient E;
    private static BasicHttpResponse F;
    private static HttpEntity G;
    private static boolean H;
    private static boolean T;
    private static boolean W;
    protected static String a = null;
    protected static String b = null;
    protected static Context c = null;
    private static String d = null;
    private static boolean e = false;
    private static int g = 17301620;
    /* access modifiers changed from: private */
    public static long k = 0;
    private static List<NameValuePair> w;
    private static String x;
    private static String y;
    private static String z;
    private Intent I;
    private int J;
    private JSONObject K;
    private String L;
    private String M;
    private String[] N = null;
    private String[] O = null;
    private String[] P = null;
    private String[] Q = null;
    private JSONObject R;
    private boolean S = true;
    private Intent U;
    private Cursor V;
    private Runnable X = new Runnable() {
        public void run() {
            Airpush.this.h();
        }
    };
    private Runnable Y = new Runnable() {
        public void run() {
            Airpush.a(Airpush.c, Airpush.k * 60000);
        }
    };
    private String f;
    private boolean h;
    private long i = 0;
    private long j = 0;
    private HttpEntity l;
    private String m;
    private JSONArray n;
    private String o;
    private String p;
    private String q;
    private String[] r;
    private String[] s;
    private String[] t;
    private Bitmap u;
    private InputStream v;

    public Airpush() {
    }

    public Airpush(Context context, String str, String str2, boolean z2, boolean z3, boolean z4) {
        try {
            H = z2;
            e = z2;
            c = context;
            T = z4;
            W = z3;
            Log.i("AirpushSDK", "Push Service doPush...." + W);
            Log.i("AirpushSDK", "Push Service doSearch...." + T);
            new SetPreferences().a(c, str, str2, z2, T, H, W);
            i();
            a(context, str, str2, e, false, g, true);
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2) {
        H = z2;
        try {
            f();
        } catch (IOException | IllegalStateException | JSONException e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void a() {
        try {
            this.v = a(this.o, (List<NameValuePair>) null);
            this.u = BitmapFactory.decodeStream(this.v);
            this.U = new Intent("android.intent.action.VIEW");
            this.U.setData(Uri.parse(this.q));
            this.U.addFlags(268435456);
            this.U.addFlags(67108864);
            this.I = new Intent();
            this.I.putExtra("android.intent.extra.shortcut.INTENT", this.U);
            this.I.putExtra("android.intent.extra.shortcut.NAME", this.p);
            this.I.putExtra("duplicate", false);
            this.I.putExtra("android.intent.extra.shortcut.ICON", this.u);
            d();
        } catch (Exception e2) {
            w = SetPreferences.a(c);
            this.q = SetPreferences.d;
            this.q = String.valueOf(this.q) + "&model=log&action=seticonclicktracking&APIKEY=airpushsearch&event=iClick&campaignid=0&creativeid=0";
            this.U = new Intent("android.intent.action.VIEW");
            this.U.setData(Uri.parse(this.q));
            this.U.addFlags(268435456);
            this.U.addFlags(67108864);
            this.I = new Intent();
            this.I.putExtra("android.intent.extra.shortcut.INTENT", this.U);
            this.I.putExtra("android.intent.extra.shortcut.NAME", "Search");
            this.I.putExtra("duplicate", false);
            this.I.putExtra("android.intent.extra.shortcut.ICON", Intent.ShortcutIconResource.fromContext(c, 17301583));
            d();
        }
    }

    private void d() {
        if (c.getPackageManager().checkPermission("com.android.launcher.permission.INSTALL_SHORTCUT", c.getPackageName()) == 0) {
            this.I.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            c.getApplicationContext().sendBroadcast(this.I);
            e();
            return;
        }
        Log.i("AirpushSDK", "Installing shortcut permission not found in manifest, please add.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void e() {
        if (c.getPackageManager().checkPermission("com.android.browser.permission.READ_HISTORY_BOOKMARKS", c.getPackageName()) == 0 && c.getPackageManager().checkPermission("com.android.browser.permission.WRITE_HISTORY_BOOKMARKS", c.getPackageName()) == 0) {
            String substring = this.q.substring(0, 25);
            ContentResolver contentResolver = c.getContentResolver();
            try {
                this.V = Browser.getAllBookmarks(contentResolver);
                this.V.moveToFirst();
                if (this.V.moveToFirst() && this.V.getCount() > 0) {
                    while (!this.V.isAfterLast()) {
                        if (this.V.getString(0).contains(substring)) {
                            contentResolver.delete(Browser.BOOKMARKS_URI, String.valueOf(this.V.getColumnName(0)) + "='" + this.V.getString(0) + "'", null);
                        }
                        this.V.moveToNext();
                    }
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put("title", "Web Search");
                contentValues.put("url", this.q);
                contentValues.put("bookmark", (Integer) 1);
                contentValues.put("favicon", a("http://api.airpush.com/320x350.jpg").toString());
                c.getContentResolver().insert(Browser.BOOKMARKS_URI, contentValues);
            } catch (Exception e2) {
            } finally {
                this.V.close();
            }
        } else {
            Log.i("AirpushSDK", "Read Write bookmark permission not found in manifest, please add.");
        }
    }

    protected static Bitmap a(String str) {
        try {
            URLConnection openConnection = new URL(str).openConnection();
            openConnection.connect();
            InputStream inputStream = openConnection.getInputStream();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            Bitmap decodeStream = BitmapFactory.decodeStream(bufferedInputStream);
            bufferedInputStream.close();
            inputStream.close();
            return decodeStream;
        } catch (Exception e2) {
            Log.i("AirpushSDK", "Error in Adimage fetching Please try again later.");
            return null;
        }
    }

    private InputStream a(String str, List<NameValuePair> list) {
        String str2 = "";
        if (list != null) {
            try {
                str2 = URLEncodedUtils.format(list, "utf-8");
            } catch (Exception e2) {
                Log.i("AirpushSDK", "Network Error, please try again later");
            }
        }
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(String.valueOf(str) + str2).openConnection();
        httpURLConnection.setRequestMethod("GET");
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setConnectTimeout(20000);
        httpURLConnection.setReadTimeout(20000);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setDefaultUseCaches(false);
        httpURLConnection.connect();
        if (httpURLConnection.getResponseCode() == 200) {
            return httpURLConnection.getInputStream();
        }
        return null;
    }

    private void f() throws IllegalStateException, IOException, JSONException {
        int width = ((WindowManager) c.getSystemService("window")).getDefaultDisplay().getWidth();
        w = SetPreferences.a(c);
        w.add(new BasicNameValuePair("width", String.valueOf(width)));
        w.add(new BasicNameValuePair("model", "message"));
        w.add(new BasicNameValuePair("action", "geticon"));
        w.add(new BasicNameValuePair("APIKEY", d));
        if (e) {
            Log.i("AirpushSDK", "ShortIcon Test Mode...." + H);
            this.l = b();
        } else {
            Log.i("AirpushSDK", "ShortIcon Test Mode...." + H);
            this.l = HttpPostData.a(w, false, c);
        }
        InputStream content = this.l.getContent();
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            int read = content.read();
            if (read == -1) {
                this.m = stringBuffer.toString();
                Log.i("Activity", "Icon Data returns: " + this.m);
                b(this.m);
                return;
            }
            stringBuffer.append((char) read);
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void b(java.lang.String r5) {
        /*
            r4 = this;
            r0 = 0
            monitor-enter(r4)
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1.<init>(r5)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.n = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONArray r1 = r4.n     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            int r1 = r1.length()     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.J = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            int r1 = r4.J     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.r = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            int r1 = r4.J     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.t = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            int r1 = r4.J     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.s = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            int r1 = r4.J     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.N = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            int r1 = r4.J     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.O = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1.<init>()     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.R = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
        L_0x0036:
            org.json.JSONArray r1 = r4.n     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            int r1 = r1.length()     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            if (r0 < r1) goto L_0x0047
            boolean r0 = r4.S     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            if (r0 == 0) goto L_0x0045
            r4.g()     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
        L_0x0045:
            monitor-exit(r4)
            return
        L_0x0047:
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONArray r2 = r4.n     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.Object r2 = r2.get(r0)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1.<init>(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.K = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.r     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONObject r2 = r4.K     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = r4.a(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1[r0] = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.s     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONObject r2 = r4.K     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = r4.b(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1[r0] = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.t     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONObject r2 = r4.K     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = r4.e(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1[r0] = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.N     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONObject r2 = r4.K     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = r4.c(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1[r0] = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.O     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONObject r2 = r4.K     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = r4.d(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1[r0] = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONObject r1 = r4.R     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r2 = r4.N     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r2 = r2[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r3 = r4.O     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r3 = r3[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.r     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1 = r1[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = "Not Found"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            if (r1 != 0) goto L_0x00bb
            java.lang.String[] r1 = r4.s     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1 = r1[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = "Not Found"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            if (r1 != 0) goto L_0x00bb
            java.lang.String[] r1 = r4.t     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1 = r1[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = "Not Found"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            if (r1 == 0) goto L_0x00c2
        L_0x00bb:
            r1 = 0
            r4.S = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
        L_0x00be:
            int r0 = r0 + 1
            goto L_0x0036
        L_0x00c2:
            java.lang.String[] r1 = r4.r     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1 = r1[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.o = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.s     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1 = r1[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.p = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.t     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1 = r1[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.q = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.a()     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            goto L_0x00be
        L_0x00d8:
            r0 = move-exception
            goto L_0x0045
        L_0x00db:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.airpush.android.Airpush.b(java.lang.String):void");
    }

    private void g() {
        Log.i("AirpushSDK", "Sending Install Data....");
        try {
            w = SetPreferences.a(c);
            w.add(new BasicNameValuePair("model", "log"));
            w.add(new BasicNameValuePair("action", "seticoninstalltracking"));
            w.add(new BasicNameValuePair("APIKEY", d));
            w.add(new BasicNameValuePair("event", "iInstall"));
            w.add(new BasicNameValuePair("campaigncreativedata", this.R.toString()));
            if (!e) {
                Log.i("AirpushSDK", "Test Mode : " + e);
                this.l = HttpPostData.a(w, c);
                InputStream content = this.l.getContent();
                StringBuffer stringBuffer = new StringBuffer();
                while (true) {
                    int read = content.read();
                    if (read == -1) {
                        break;
                    }
                    stringBuffer.append((char) read);
                }
                this.m = stringBuffer.toString();
                if (this.m.equals("1")) {
                    Log.i("AirpushSDK", "Icon Install returns:" + this.m);
                } else {
                    Log.i("AirpushSDK", "Icon Install returns: " + this.m);
                }
            } else {
                Log.i("AirpushSDK", "Test Mode : " + e);
            }
        } catch (IllegalStateException e2) {
        } catch (Exception e3) {
            Log.i("AirpushSDK", "Icon Install Confirmation Error ");
        }
    }

    private String a(JSONObject jSONObject) {
        try {
            this.o = jSONObject.getString("iconimage");
            return this.o;
        } catch (JSONException e2) {
            return "Not Found";
        }
    }

    private String b(JSONObject jSONObject) {
        try {
            this.p = jSONObject.getString("icontext");
            return this.p;
        } catch (JSONException e2) {
            return "Not Found";
        }
    }

    private String c(JSONObject jSONObject) {
        try {
            this.L = jSONObject.getString("campaignid");
            return this.L;
        } catch (JSONException e2) {
            return "Not Found";
        }
    }

    private String d(JSONObject jSONObject) {
        try {
            this.M = jSONObject.getString("creativeid");
            return this.M;
        } catch (JSONException e2) {
            return "Not Found";
        }
    }

    private String e(JSONObject jSONObject) {
        try {
            this.q = jSONObject.getString("iconurl");
            return this.q;
        } catch (JSONException e2) {
            return "Not Found";
        }
    }

    /* access modifiers changed from: protected */
    public void a(Context context, String str, String str2, boolean z2, boolean z3, int i2, boolean z4) {
        try {
            this.h = z4;
            SharedPreferences.Editor edit = c.getSharedPreferences("dialogPref", 2).edit();
            edit.putBoolean("ShowDialog", z3);
            edit.putBoolean("ShowAd", this.h);
            edit.commit();
            if (this.h) {
                Log.i("AirpushSDK", "Initialising.....");
                e = z2;
                a = str;
                d = str2;
                g = i2;
                this.f = ((TelephonyManager) c.getSystemService("phone")).getDeviceId();
                try {
                    MessageDigest instance = MessageDigest.getInstance("MD5");
                    instance.update(this.f.getBytes(), 0, this.f.length());
                    b = new BigInteger(1, instance.digest()).toString(16);
                } catch (NoSuchAlgorithmException e2) {
                }
                new Handler().postDelayed(this.X, 6000);
            }
        } catch (Exception e3) {
        }
    }

    public static boolean isEnabled(Context context) {
        if (context.getSharedPreferences("sdkPrefs", 1).equals(null)) {
            return true;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences("sdkPrefs", 1);
        if (sharedPreferences.contains("SDKEnabled")) {
            return sharedPreferences.getBoolean("SDKEnabled", false);
        }
        return true;
    }

    public static void enableSdk(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("sdkPrefs", 2).edit();
        edit.putBoolean("SDKEnabled", true);
        edit.commit();
    }

    public static void disableSdk(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("sdkPrefs", 2).edit();
        edit.putBoolean("SDKEnabled", false);
        edit.commit();
    }

    /* access modifiers changed from: private */
    public void h() {
        boolean z2 = true;
        try {
            if (c.getSharedPreferences("airpushTimePref", 1) != null) {
                this.i = System.currentTimeMillis();
                SharedPreferences sharedPreferences = c.getSharedPreferences("airpushTimePref", 1);
                if (sharedPreferences.contains("startTime")) {
                    this.j = sharedPreferences.getLong("startTime", 0);
                    k = (this.i - this.j) / 60000;
                    if (k < ((long) Constants.f.intValue())) {
                        new Handler().post(this.Y);
                        z2 = false;
                    }
                } else {
                    SharedPreferences.Editor edit = c.getSharedPreferences("airpushTimePref", 2).edit();
                    this.j = System.currentTimeMillis();
                    edit.putLong("startTime", this.j);
                    edit.commit();
                }
            }
            if (z2) {
                Intent intent = new Intent(c, UserDetailsReceiver.class);
                intent.setAction("SetUserInfo");
                intent.putExtra("appId", a);
                intent.putExtra("imei", b);
                intent.putExtra("apikey", d);
                ((AlarmManager) c.getSystemService("alarm")).set(0, System.currentTimeMillis(), PendingIntent.getBroadcast(c, 0, intent, 0));
                Intent intent2 = new Intent(c, MessageReceiver.class);
                intent2.setAction("SetMessageReceiver");
                intent2.putExtra("appId", a);
                intent2.putExtra("imei", b);
                intent2.putExtra("apikey", d);
                intent2.putExtra("testMode", e);
                intent2.putExtra("icon", g);
                intent2.putExtra("icontestmode", H);
                intent2.putExtra("doSearch", T);
                intent2.putExtra("doPush", W);
                ((AlarmManager) c.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + ((long) Constants.d.intValue()), Constants.c, PendingIntent.getBroadcast(c, 0, intent2, 0));
            }
        } catch (Exception e2) {
        }
    }

    private static void i() {
        try {
            if (!c.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = c.getSharedPreferences("dataPrefs", 1);
                a = sharedPreferences.getString("appId", "invalid");
                d = sharedPreferences.getString("apikey", "airpush");
                b = sharedPreferences.getString("imei", "invalid");
                e = sharedPreferences.getBoolean("testMode", false);
                g = sharedPreferences.getInt("icon", 17301620);
                x = sharedPreferences.getString("asp", "invalid");
                y = Base64.encodeString(sharedPreferences.getString("imeinumber", "invalid"));
                z = Base64.encodeString(a);
            }
        } catch (Exception e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    protected static void a(Context context, long j2) {
        Log.i("AirpushSDK", "SDK will restart in " + j2 + " ms.");
        c = context;
        i();
        try {
            Intent intent = new Intent(context, UserDetailsReceiver.class);
            intent.setAction("SetUserInfo");
            intent.putExtra("appId", a);
            intent.putExtra("imei", b);
            intent.putExtra("apikey", d);
            ((AlarmManager) context.getSystemService("alarm")).set(0, System.currentTimeMillis() + (1000 * j2 * 60), PendingIntent.getBroadcast(context, 0, intent, 0));
            Intent intent2 = new Intent(context, MessageReceiver.class);
            intent2.setAction("SetMessageReceiver");
            intent2.putExtra("appId", a);
            intent2.putExtra("imei", b);
            intent2.putExtra("apikey", d);
            intent2.putExtra("testMode", e);
            intent2.putExtra("icon", g);
            intent2.putExtra("icontestmode", H);
            intent2.putExtra("doSearch", true);
            intent2.putExtra("doPush", true);
            ((AlarmManager) context.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + j2 + ((long) Constants.d.intValue()), Constants.c, PendingIntent.getBroadcast(context, 0, intent2, 0));
        } catch (Exception e2) {
        }
    }

    protected static HttpEntity b() {
        if (Constants.a(c)) {
            try {
                Log.i("AirpushSDK", "Test Api for icons ads");
                A = new HttpPost("http://api.airpush.com/testicon.php");
                A.setEntity(new UrlEncodedFormEntity(w));
                B = new BasicHttpParams();
                C = 3000;
                HttpConnectionParams.setConnectionTimeout(B, C);
                D = 3000;
                HttpConnectionParams.setSoTimeout(B, D);
                E = new DefaultHttpClient(B);
                F = E.execute(A);
                G = F.getEntity();
                return G;
            } catch (Exception e2) {
                a(c, 1800000);
                return null;
            }
        } else {
            a(c, k);
            return null;
        }
    }
}
