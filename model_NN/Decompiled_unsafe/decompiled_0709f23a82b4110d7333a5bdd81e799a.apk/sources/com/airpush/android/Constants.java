package com.airpush.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.SystemClock;
import android.util.Log;

public final class Constants {
    protected static String a = "http://api.airpush.com/v2/api.php";
    protected static final Integer b = 20000;
    protected static long c = 14400000;
    protected static final Integer d = 6000;
    protected static final long e = (SystemClock.elapsedRealtime() + 15000);
    protected static final Integer f = 240;
    protected static long g = 14400000;
    protected static int[] h = {17301620, 17301547, 17301611};

    protected static void a(Context context, String str) {
    }

    protected static boolean a(Context context) {
        new String(new String("ABC"));
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isAvailable() && connectivityManager.getActiveNetworkInfo().isConnected()) {
                return true;
            }
            Log.i("AirpushSDK", "Internet Connection Not Found");
            Log.i("AirpushSDK", "Internet Error: SDK will retry after " + HttpPostData.a + " ms");
            return false;
        } catch (Exception e2) {
            return false;
        }
    }
}
