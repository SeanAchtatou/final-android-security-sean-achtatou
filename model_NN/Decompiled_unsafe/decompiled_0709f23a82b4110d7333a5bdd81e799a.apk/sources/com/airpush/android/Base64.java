package com.airpush.android;

public class Base64 {
    private static final String a = System.getProperty("line.separator");
    private static final char[] b = new char[64];
    private static final byte[] c = new byte[128];

    static {
        char c2 = 'A';
        int i = 0;
        while (c2 <= 'Z') {
            b[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = 'a';
        while (c3 <= 'z') {
            b[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        char c4 = '0';
        while (c4 <= '9') {
            b[i] = c4;
            c4 = (char) (c4 + 1);
            i++;
        }
        int i2 = i + 1;
        b[i] = '+';
        int i3 = i2 + 1;
        b[i2] = '/';
        for (int i4 = 0; i4 < c.length; i4++) {
            c[i4] = -1;
        }
        for (int i5 = 0; i5 < 64; i5++) {
            c[b[i5]] = (byte) i5;
        }
    }

    public static String encodeString(String str) {
        return new String(encode(str.getBytes()));
    }

    public static String encodeLines(byte[] bArr) {
        return encodeLines(bArr, 0, bArr.length, 76, a);
    }

    public static String encodeLines(byte[] bArr, int i, int i2, int i3, String str) {
        int i4 = (i3 * 3) / 4;
        if (i4 <= 0) {
            throw new IllegalArgumentException();
        }
        StringBuilder sb = new StringBuilder(((((i2 + i4) - 1) / i4) * str.length()) + (((i2 + 2) / 3) * 4));
        int i5 = 0;
        while (i5 < i2) {
            int min = Math.min(i2 - i5, i4);
            sb.append(encode(bArr, i + i5, min));
            sb.append(str);
            i5 += min;
        }
        return sb.toString();
    }

    public static char[] encode(byte[] bArr) {
        return encode(bArr, 0, bArr.length);
    }

    public static char[] encode(byte[] bArr, int i) {
        return encode(bArr, 0, i);
    }

    public static char[] encode(byte[] bArr, int i, int i2) {
        byte b2;
        int i3;
        byte b3;
        char c2;
        char c3;
        int i4 = ((i2 * 4) + 2) / 3;
        char[] cArr = new char[(((i2 + 2) / 3) * 4)];
        int i5 = i + i2;
        int i6 = 0;
        while (i < i5) {
            int i7 = i + 1;
            byte b4 = bArr[i] & 255;
            if (i7 < i5) {
                b2 = bArr[i7] & 255;
                i7++;
            } else {
                b2 = 0;
            }
            if (i7 < i5) {
                i3 = i7 + 1;
                b3 = bArr[i7] & 255;
            } else {
                i3 = i7;
                b3 = 0;
            }
            int i8 = b4 >>> 2;
            int i9 = ((b4 & 3) << 4) | (b2 >>> 4);
            int i10 = ((b2 & 15) << 2) | (b3 >>> 6);
            byte b5 = b3 & 63;
            int i11 = i6 + 1;
            cArr[i6] = b[i8];
            int i12 = i11 + 1;
            cArr[i11] = b[i9];
            if (i12 < i4) {
                c2 = b[i10];
            } else {
                c2 = '=';
            }
            cArr[i12] = c2;
            int i13 = i12 + 1;
            if (i13 < i4) {
                c3 = b[b5];
            } else {
                c3 = '=';
            }
            cArr[i13] = c3;
            i6 = i13 + 1;
            i = i3;
        }
        return cArr;
    }

    public static String decodeString(String str) {
        return new String(decode(str));
    }

    public static byte[] decodeLines(String str) {
        char[] cArr = new char[str.length()];
        int i = 0;
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if (!(charAt == ' ' || charAt == 13 || charAt == 10 || charAt == 9)) {
                cArr[i] = charAt;
                i++;
            }
        }
        return decode(cArr, 0, i);
    }

    public static byte[] decode(String str) {
        return decode(str.toCharArray());
    }

    public static byte[] decode(char[] cArr) {
        return decode(cArr, 0, cArr.length);
    }

    public static byte[] decode(char[] cArr, int i, int i2) {
        char c2;
        char c3;
        int i3;
        if (i2 % 4 != 0) {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }
        while (i2 > 0 && cArr[(i + i2) - 1] == '=') {
            i2--;
        }
        int i4 = (i2 * 3) / 4;
        byte[] bArr = new byte[i4];
        int i5 = i + i2;
        int i6 = 0;
        int i7 = i;
        while (i7 < i5) {
            int i8 = i7 + 1;
            char c4 = cArr[i7];
            int i9 = i8 + 1;
            char c5 = cArr[i8];
            if (i9 < i5) {
                c2 = cArr[i9];
                i9++;
            } else {
                c2 = 'A';
            }
            if (i9 < i5) {
                int i10 = i9 + 1;
                c3 = cArr[i9];
                i7 = i10;
            } else {
                i7 = i9;
                c3 = 'A';
            }
            if (c4 > 127 || c5 > 127 || c2 > 127 || c3 > 127) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            byte b2 = c[c4];
            byte b3 = c[c5];
            byte b4 = c[c2];
            byte b5 = c[c3];
            if (b2 < 0 || b3 < 0 || b4 < 0 || b5 < 0) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int i11 = (b3 >>> 4) | (b2 << 2);
            int i12 = (b4 >>> 2) | ((b3 & 15) << 4);
            byte b6 = ((b4 & 3) << 6) | b5;
            int i13 = i6 + 1;
            bArr[i6] = (byte) i11;
            if (i13 < i4) {
                i3 = i13 + 1;
                bArr[i13] = (byte) i12;
            } else {
                i3 = i13;
            }
            if (i3 < i4) {
                bArr[i3] = (byte) b6;
                i6 = i3 + 1;
            } else {
                i6 = i3;
            }
        }
        return bArr;
    }

    private Base64() {
    }
}
