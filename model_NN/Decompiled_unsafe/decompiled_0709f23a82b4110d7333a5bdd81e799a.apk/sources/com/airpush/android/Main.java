package com.airpush.android;

import android.app.Activity;
import android.os.Bundle;
import com.christmasgame.balloon2.R;

public class Main extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        new Airpush(this, "10256", "airpush", true, true, true);
        setContentView((int) R.layout.profile);
    }
}
