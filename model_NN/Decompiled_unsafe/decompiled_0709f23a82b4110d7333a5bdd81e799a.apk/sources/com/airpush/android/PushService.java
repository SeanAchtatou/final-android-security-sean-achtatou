package com.airpush.android;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import com.mobclick.android.UmengConstants;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class PushService extends Service {
    private static int Q = 17301620;
    protected static String a = null;
    protected static boolean b = false;
    private static String d = null;
    /* access modifiers changed from: private */
    public static String e = null;
    /* access modifiers changed from: private */
    public static Context p = null;
    /* access modifiers changed from: private */
    public static String w = null;
    private String A = null;
    private String B = null;
    private String C = null;
    private String D = null;
    private String E;
    private Long F;
    private String G;
    private long H;
    private String I;
    private String J;
    private String K;
    private HttpEntity L;
    private boolean M;
    private boolean N;
    private boolean O;
    private boolean P;
    private Runnable R = new Runnable() {
        public void run() {
            a();
        }

        private void a() {
            try {
                Log.i("AirpushSDK", "Notification Expired");
                PushService.this.x.cancel(999);
            } catch (Exception e) {
                Airpush.a(PushService.this.getApplicationContext(), 1800000);
            }
        }
    };
    private List<NameValuePair> c = null;
    private String f = null;
    private String g = null;
    private String h = null;
    private String i = null;
    private String j = null;
    private String k = null;
    private String l = null;
    private String m = null;
    private String n = null;
    private String o = "http://api.airpush.com/redirect.php?market=";
    private JSONObject q = null;
    private String r;
    private String s;
    private String t;
    private long u;
    private String v = null;
    /* access modifiers changed from: private */
    public NotificationManager x;
    private String y = null;
    private String z = null;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onStart(Intent intent, int i2) {
        Integer valueOf = Integer.valueOf(i2);
        try {
            e = intent.getStringExtra("appId");
            a = intent.getStringExtra(UmengConstants.AtomKey_Type);
            w = intent.getStringExtra("apikey");
            if (a.equals("PostAdValues")) {
                this.r = intent.getStringExtra("adType");
                if (this.r.equals("Interstitial")) {
                    e = intent.getStringExtra("appId");
                    w = intent.getStringExtra("apikey");
                    this.h = intent.getStringExtra("campId");
                    this.i = intent.getStringExtra("creativeId");
                    this.M = intent.getBooleanExtra("Test", false);
                    this.c = SetPreferences.a(getApplicationContext());
                    this.c.add(new BasicNameValuePair("model", "log"));
                    this.c.add(new BasicNameValuePair("action", "settexttracking"));
                    this.c.add(new BasicNameValuePair("APIKEY", w));
                    this.c.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.c.add(new BasicNameValuePair("campId", this.h));
                    this.c.add(new BasicNameValuePair("creativeId", this.i));
                    if (!this.M) {
                        this.L = HttpPostData.a(this.c, getApplicationContext());
                    }
                }
                if (this.r.equals("CC")) {
                    b = intent.getBooleanExtra("testMode", false);
                    if (getApplicationContext().getSharedPreferences("airpushNotificationPref", 1) != null) {
                        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("airpushNotificationPref", 1);
                        e = sharedPreferences.getString("appId", intent.getStringExtra("appId"));
                        w = sharedPreferences.getString("apikey", intent.getStringExtra("apikey"));
                        this.K = sharedPreferences.getString("number", intent.getStringExtra("number"));
                        this.h = sharedPreferences.getString("campId", intent.getStringExtra("campId"));
                        this.i = sharedPreferences.getString("creativeId", intent.getStringExtra("creativeId"));
                    } else {
                        e = intent.getStringExtra("appId");
                        w = intent.getStringExtra("apikey");
                        this.h = intent.getStringExtra("campId");
                        this.i = intent.getStringExtra("creativeId");
                        this.K = intent.getStringExtra("number");
                    }
                    this.c = SetPreferences.a(getApplicationContext());
                    this.c.add(new BasicNameValuePair("model", "log"));
                    this.c.add(new BasicNameValuePair("action", "settexttracking"));
                    this.c.add(new BasicNameValuePair("APIKEY", w));
                    this.c.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.c.add(new BasicNameValuePair("campId", this.h));
                    this.c.add(new BasicNameValuePair("creativeId", this.i));
                    if (!b) {
                        Log.i("AirpushSDK", "Posting CC values");
                        this.L = HttpPostData.a(this.c, getApplicationContext());
                        InputStream content = this.L.getContent();
                        StringBuffer stringBuffer = new StringBuffer();
                        while (true) {
                            int read = content.read();
                            if (read == -1) {
                                break;
                            }
                            stringBuffer.append((char) read);
                        }
                        Log.i("AirpushSDK", "CC Click : " + stringBuffer.toString());
                    }
                }
                if (this.r.equals("CM")) {
                    b = intent.getBooleanExtra("testMode", false);
                    if (getApplicationContext().getSharedPreferences("airpushNotificationPref", 1) != null) {
                        SharedPreferences sharedPreferences2 = getApplicationContext().getSharedPreferences("airpushNotificationPref", 1);
                        e = sharedPreferences2.getString("appId", intent.getStringExtra("appId"));
                        w = sharedPreferences2.getString("apikey", intent.getStringExtra("apikey"));
                        this.s = sharedPreferences2.getString("sms", intent.getStringExtra("sms"));
                        this.h = sharedPreferences2.getString("campId", intent.getStringExtra("campId"));
                        this.i = sharedPreferences2.getString("creativeId", intent.getStringExtra("creativeId"));
                        this.t = sharedPreferences2.getString("number", intent.getStringExtra("number"));
                    } else {
                        e = intent.getStringExtra("appId");
                        w = intent.getStringExtra("apikey");
                        this.h = intent.getStringExtra("campId");
                        this.i = intent.getStringExtra("creativeId");
                        this.s = intent.getStringExtra("sms");
                        this.t = intent.getStringExtra("number");
                    }
                    this.c = SetPreferences.a(getApplicationContext());
                    this.c.add(new BasicNameValuePair("model", "log"));
                    this.c.add(new BasicNameValuePair("action", "settexttracking"));
                    this.c.add(new BasicNameValuePair("APIKEY", w));
                    this.c.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.c.add(new BasicNameValuePair("campId", this.h));
                    this.c.add(new BasicNameValuePair("creativeId", this.i));
                    if (!b) {
                        Log.i("AirpushSDK", "Posting CM values");
                        this.L = HttpPostData.a(this.c, getApplicationContext());
                        InputStream content2 = this.L.getContent();
                        StringBuffer stringBuffer2 = new StringBuffer();
                        while (true) {
                            int read2 = content2.read();
                            if (read2 == -1) {
                                break;
                            }
                            stringBuffer2.append((char) read2);
                        }
                        Log.i("AirpushSDK", "CM Click : " + stringBuffer2.toString());
                    }
                }
                if (this.r.equals("W") || this.r.equals("A")) {
                    b = intent.getBooleanExtra("testMode", false);
                    if (getApplicationContext().getSharedPreferences("airpushNotificationPref", 1) != null) {
                        SharedPreferences sharedPreferences3 = getApplicationContext().getSharedPreferences("airpushNotificationPref", 1);
                        e = sharedPreferences3.getString("appId", intent.getStringExtra("appId"));
                        w = sharedPreferences3.getString("apikey", intent.getStringExtra("apikey"));
                        this.m = sharedPreferences3.getString("url", intent.getStringExtra("url"));
                        this.h = sharedPreferences3.getString("campId", intent.getStringExtra("campId"));
                        this.i = sharedPreferences3.getString("creativeId", intent.getStringExtra("creativeId"));
                        this.J = sharedPreferences3.getString("header", intent.getStringExtra("header"));
                    } else {
                        e = intent.getStringExtra("appId");
                        w = intent.getStringExtra("apikey");
                        this.h = intent.getStringExtra("campId");
                        this.i = intent.getStringExtra("creativeId");
                        this.m = intent.getStringExtra("url");
                        this.J = intent.getStringExtra("header");
                    }
                    this.c = SetPreferences.a(getApplicationContext());
                    this.c.add(new BasicNameValuePair("model", "log"));
                    this.c.add(new BasicNameValuePair("action", "settexttracking"));
                    this.c.add(new BasicNameValuePair("APIKEY", w));
                    this.c.add(new BasicNameValuePair("event", "TrayClicked"));
                    this.c.add(new BasicNameValuePair("campId", this.h));
                    this.c.add(new BasicNameValuePair("creativeId", this.i));
                    if (!b) {
                        Log.i("AirpushSDK", "Posting W&A values.");
                        this.L = HttpPostData.a(this.c, getApplicationContext());
                        InputStream content3 = this.L.getContent();
                        StringBuffer stringBuffer3 = new StringBuffer();
                        while (true) {
                            int read3 = content3.read();
                            if (read3 == -1) {
                                break;
                            }
                            stringBuffer3.append((char) read3);
                        }
                        Log.i("AirpushSDK", "W&A Click : " + stringBuffer3.toString());
                    }
                }
            } else if (a.equals("userInfo")) {
                p = UserDetailsReceiver.b;
                if (!p.getSharedPreferences("dataPrefs", 1).equals(null)) {
                    d = p.getSharedPreferences("dataPrefs", 1).getString("imei", "invalid");
                }
                new UserInfoTask(this, null).execute(new Void[0]);
            } else if (a.equals("message")) {
                p = MessageReceiver.c;
                if (!p.getSharedPreferences("dataPrefs", 1).equals(null)) {
                    d = p.getSharedPreferences("dataPrefs", 1).getString("imei", "invalid");
                }
                b = intent.getBooleanExtra("testMode", false);
                Q = intent.getIntExtra("icon", 17301620);
                this.N = intent.getBooleanExtra("doSearch", true);
                this.O = intent.getBooleanExtra("icontestmode", false);
                this.P = intent.getBooleanExtra("doPush", true);
                Log.i("AirpushSDK", "Search Icon Enabled : " + this.N);
                Log.i("AirpushSDK", "Push Enabled : " + this.P);
                if (this.N) {
                    new Airpush().a(this.O);
                }
                if (this.P) {
                    new GetMessageTask(this, null).execute(new Void[0]);
                } else {
                    a(Constants.c);
                }
            } else if (a.equals("delivery")) {
                p = DeliveryReceiver.b;
                this.r = intent.getStringExtra("adType");
                if (this.r.equals("W")) {
                    e = intent.getStringExtra("appId");
                    this.g = intent.getStringExtra("link");
                    this.f = intent.getStringExtra("text");
                    this.y = intent.getStringExtra("title");
                    this.v = intent.getStringExtra("imageurl");
                    this.u = intent.getLongExtra("expiry_time", 60);
                    this.J = intent.getStringExtra("header");
                    this.h = intent.getStringExtra("campId");
                    this.i = intent.getStringExtra("creativeId");
                    Constants.a(p, this.n);
                    e();
                }
                if (this.r.equals("A")) {
                    e = intent.getStringExtra("appId");
                    this.g = intent.getStringExtra("link");
                    this.f = intent.getStringExtra("text");
                    this.y = intent.getStringExtra("title");
                    this.v = intent.getStringExtra("imageurl");
                    this.u = intent.getLongExtra("expiry_time", 60);
                    this.h = intent.getStringExtra("campId");
                    this.i = intent.getStringExtra("creativeId");
                    Constants.a(p, this.n);
                    e();
                }
                if (this.r.equals("CC")) {
                    e = intent.getStringExtra("appId");
                    this.E = intent.getStringExtra("number");
                    this.f = intent.getStringExtra("text");
                    this.y = intent.getStringExtra("title");
                    this.v = intent.getStringExtra("imageurl");
                    this.u = intent.getLongExtra("expiry_time", 60);
                    this.h = intent.getStringExtra("campId");
                    this.i = intent.getStringExtra("creativeId");
                    Constants.a(p, this.n);
                    e();
                }
                if (this.r.equals("CM")) {
                    e = intent.getStringExtra("appId");
                    this.E = intent.getStringExtra("number");
                    this.I = intent.getStringExtra("sms");
                    this.f = intent.getStringExtra("text");
                    this.y = intent.getStringExtra("title");
                    this.v = intent.getStringExtra("imageurl");
                    this.u = intent.getLongExtra("expiry_time", 60);
                    this.h = intent.getStringExtra("campId");
                    this.i = intent.getStringExtra("creativeId");
                    Constants.a(p, this.n);
                    e();
                }
            }
            if (valueOf != null) {
                stopSelf(i2);
            }
        } catch (Exception e2) {
            new Airpush(getApplicationContext(), e, "airpush", false, true, true);
            if (valueOf != null) {
                stopSelf(i2);
            }
        } catch (Throwable th) {
            if (valueOf != null) {
                stopSelf(i2);
            }
            throw th;
        }
    }

    private class GetMessageTask extends AsyncTask<Void, Void, Void> {
        private GetMessageTask() {
        }

        /* synthetic */ GetMessageTask(PushService pushService, GetMessageTask getMessageTask) {
            this();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            PushService.this.d();
            return null;
        }
    }

    private class UserInfoTask extends AsyncTask<Void, Void, Void> {
        private UserInfoTask() {
        }

        /* synthetic */ UserInfoTask(PushService pushService, UserInfoTask userInfoTask) {
            this();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            PushService.this.a(PushService.p, PushService.e, PushService.w);
            return null;
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
        Log.i("AirpushSDK", "Low On Memory");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.i("AirpushSDK", "Service Finished");
    }

    /* access modifiers changed from: private */
    public void a(Context context, String str, String str2) {
        if (Airpush.isEnabled(context)) {
            try {
                this.c = SetPreferences.a(p);
                this.c.add(new BasicNameValuePair("model", "user"));
                this.c.add(new BasicNameValuePair("action", "setuserinfo"));
                this.c.add(new BasicNameValuePair("APIKEY", str2));
                this.c.add(new BasicNameValuePair(UmengConstants.AtomKey_Type, "app"));
                HttpEntity a2 = HttpPostData.a(this.c, p);
                if (!a2.equals(null)) {
                    InputStream content = a2.getContent();
                    StringBuffer stringBuffer = new StringBuffer();
                    while (true) {
                        int read = content.read();
                        if (read == -1) {
                            String stringBuffer2 = stringBuffer.toString();
                            Log.i("AirpushSDK", "User Info Sent.");
                            System.out.println("sendUserInfo >>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + stringBuffer2);
                            return;
                        }
                        stringBuffer.append((char) read);
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                Log.i("Activitymanager", "User Info Sending Failed.....");
                Log.i("Activitymanager", e2.toString());
                Airpush.a(p, 1800000);
            }
        } else {
            Log.i("AirpushSDK", "Airpush is disabled, please enable to receive ads.");
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (Airpush.isEnabled(p)) {
            Log.i("AirpushSDK", "Receiving.......");
            try {
                this.c = SetPreferences.a(p);
                this.c.add(new BasicNameValuePair("model", "message"));
                this.c.add(new BasicNameValuePair("action", "getmessage"));
                this.c.add(new BasicNameValuePair("APIKEY", w));
                Constants.a(p, d);
                this.D = null;
                HttpEntity a2 = HttpPostData.a(this.c, b, p);
                if (!a2.equals(null)) {
                    InputStream content = a2.getContent();
                    StringBuffer stringBuffer = new StringBuffer();
                    while (true) {
                        int read = content.read();
                        if (read == -1) {
                            this.D = stringBuffer.toString();
                            Log.i("Activity", "Push Message : " + this.D);
                            a(this.D);
                            return;
                        }
                        stringBuffer.append((char) read);
                    }
                }
            } catch (Exception e2) {
                Log.i("Activitymanager", "Message Fetching Failed.....");
                Log.i("Activitymanager", e2.toString());
                Constants.a(p, "json" + e2.toString());
                Constants.a(p, "Message " + this.D);
                Airpush.a(p, 1800000);
            }
        } else {
            Log.i("AirpushSDK", "Airpush is disabled, please enable to receive ads.");
        }
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    /* access modifiers changed from: protected */
    public synchronized void a(String str) {
        Constants.a(p, str);
        this.F = Long.valueOf(Constants.c);
        if (str.contains("nextmessagecheck")) {
            try {
                Constants.a(p, str);
                JSONObject jSONObject = new JSONObject(str);
                this.F = Long.valueOf(l(jSONObject));
                this.r = d(jSONObject);
                if (!this.r.equals("invalid")) {
                    if (this.r.equals("W") || this.r.equals("A")) {
                        a(jSONObject);
                    }
                    if (this.r.equals("CC")) {
                        b(jSONObject);
                    }
                    if (this.r.equals("CM")) {
                        c(jSONObject);
                    }
                } else {
                    a(this.F.longValue());
                }
            } catch (JSONException e2) {
                Log.e("AirpushSDK", "Message Parsing.....Failed : " + e2.toString());
            } catch (Exception e3) {
            }
        }
        return;
    }

    private void a(JSONObject jSONObject) {
        try {
            this.y = e(jSONObject);
            this.f = f(jSONObject);
            this.g = g(jSONObject);
            this.h = k(jSONObject);
            this.J = p(jSONObject);
            this.i = j(jSONObject);
            if (!this.h.equals(null) && !this.h.equals("") && !this.i.equals(null) && !this.i.equals("") && !this.g.equals(null) && !this.g.equals("nothing")) {
                this.F = Long.valueOf(l(jSONObject));
                if (this.F.longValue() == 0) {
                    this.F = Long.valueOf(Constants.c);
                }
                this.G = m(jSONObject);
                this.u = n(jSONObject).longValue();
                this.v = o(jSONObject);
                if (!this.G.equals(null) && !this.G.equals("0")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String format = simpleDateFormat.format(new Date());
                    Constants.a(p, this.G.toString());
                    Constants.a(p, format);
                    this.H = a(this.G.toString(), format);
                } else if (this.G.equals("0")) {
                    this.H = 0;
                }
                e();
            }
        } catch (Exception e2) {
        } finally {
            a(this.F.longValue());
        }
    }

    private void b(JSONObject jSONObject) {
        try {
            this.y = e(jSONObject);
            this.f = f(jSONObject);
            this.E = h(jSONObject);
            this.h = k(jSONObject);
            this.i = j(jSONObject);
            if (!this.h.equals(null) && !this.h.equals("") && !this.i.equals(null) && !this.i.equals("")) {
                this.F = Long.valueOf(l(jSONObject));
                if (this.F.longValue() == 0) {
                    this.F = Long.valueOf(Constants.c);
                }
                this.G = m(jSONObject);
                this.u = n(jSONObject).longValue();
                this.v = o(jSONObject);
                if (!this.G.equals(null) && !this.G.equals("0")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String format = simpleDateFormat.format(new Date());
                    Constants.a(p, this.G.toString());
                    Constants.a(p, format);
                    this.H = a(this.G.toString(), format);
                } else if (this.G.equals("0")) {
                    this.H = 0;
                }
                if (!this.E.equals(null) && !this.E.equals("0")) {
                    e();
                }
            }
        } catch (Exception e2) {
        } finally {
            a(this.F.longValue());
        }
    }

    private void c(JSONObject jSONObject) {
        try {
            this.y = e(jSONObject);
            this.f = f(jSONObject);
            this.E = h(jSONObject);
            this.I = i(jSONObject);
            this.h = k(jSONObject);
            this.i = j(jSONObject);
            if (!this.h.equals(null) && !this.h.equals("") && !this.i.equals(null) && !this.i.equals("")) {
                this.F = Long.valueOf(l(jSONObject));
                if (this.F.longValue() == 0) {
                    this.F = Long.valueOf(Constants.c);
                }
                this.G = m(jSONObject);
                this.u = n(jSONObject).longValue();
                this.v = o(jSONObject);
                if (!this.G.equals(null) && !this.G.equals("0")) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                    String format = simpleDateFormat.format(new Date());
                    Constants.a(p, this.G.toString());
                    Constants.a(p, format);
                    this.H = a(this.G.toString(), format);
                } else if (this.G.equals("0")) {
                    this.H = 0;
                }
                if (!this.E.equals(null) && !this.E.equals("0")) {
                    e();
                }
            }
        } catch (Exception e2) {
        } finally {
            a(this.F.longValue());
        }
    }

    private String d(JSONObject jSONObject) {
        try {
            return jSONObject.getString("adtype");
        } catch (JSONException e2) {
            return "invalid";
        }
    }

    private String e(JSONObject jSONObject) {
        try {
            return jSONObject.getString("title");
        } catch (JSONException e2) {
            return "New Message";
        }
    }

    private String f(JSONObject jSONObject) {
        try {
            return jSONObject.getString("text");
        } catch (JSONException e2) {
            return "Click here for details!";
        }
    }

    private String g(JSONObject jSONObject) {
        try {
            return jSONObject.getString("url");
        } catch (JSONException e2) {
            return "nothing";
        }
    }

    private String h(JSONObject jSONObject) {
        try {
            return jSONObject.getString("number");
        } catch (JSONException e2) {
            return "0";
        }
    }

    private String i(JSONObject jSONObject) {
        try {
            return jSONObject.getString("sms");
        } catch (JSONException e2) {
            return "";
        }
    }

    private String j(JSONObject jSONObject) {
        try {
            return jSONObject.getString("creativeid");
        } catch (JSONException e2) {
            return "";
        }
    }

    private String k(JSONObject jSONObject) {
        try {
            return jSONObject.getString("campaignid");
        } catch (JSONException e2) {
            return "";
        }
    }

    private long l(JSONObject jSONObject) {
        Long.valueOf(Long.parseLong("300") * 1000);
        try {
            return Long.valueOf(Long.parseLong(jSONObject.get("nextmessagecheck").toString()) * 1000).longValue();
        } catch (Exception e2) {
            return Constants.c;
        }
    }

    private String m(JSONObject jSONObject) {
        try {
            return jSONObject.getString("delivery_time");
        } catch (JSONException e2) {
            return "0";
        }
    }

    private Long n(JSONObject jSONObject) {
        try {
            return Long.valueOf(jSONObject.getLong("expirytime"));
        } catch (JSONException e2) {
            return Long.valueOf(Long.parseLong("86400000"));
        }
    }

    private String o(JSONObject jSONObject) {
        try {
            return jSONObject.getString("adimage");
        } catch (JSONException e2) {
            return "http://beta.airpush.com/images/adsthumbnail/48.png";
        }
    }

    private String p(JSONObject jSONObject) {
        try {
            return jSONObject.getString("header");
        } catch (JSONException e2) {
            return "Advertisment";
        }
    }

    private void a(long j2) {
        try {
            g();
            Log.i("AirpushSDK", "ResetTime : " + j2);
            Intent intent = new Intent(p, MessageReceiver.class);
            intent.setAction("SetMessageReceiver");
            intent.putExtra("appId", e);
            intent.putExtra("apikey", w);
            intent.putExtra("imei", d);
            intent.putExtra("testMode", b);
            intent.putExtra("doSearch", this.N);
            intent.putExtra("doPush", this.P);
            intent.putExtra("icontestmode", this.O);
            ((AlarmManager) p.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + j2, Constants.c, PendingIntent.getBroadcast(p, 0, intent, 0));
        } catch (Exception e2) {
            Log.i("AirpushSDK", "ResetAlarm Error");
            Airpush.a(p, j2);
        }
    }

    private long a(String str, String str2) {
        try {
            return new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(str).getTime() - new SimpleDateFormat("yyyy-mm-dd hh:mm:ss").parse(str2).getTime();
        } catch (ParseException e2) {
            Airpush.a(p, 1800000);
            Log.e("AirpushSDK", "Date Diff .....Failed");
            return 0;
        }
    }

    private void e() {
        Q = f();
        try {
            if (this.r.equals("W") || this.r.equals("A")) {
                if (this.r.equals("A")) {
                    this.g = String.valueOf(this.o) + this.g;
                } else if (this.r.equals("W") && this.g.contains("?")) {
                    this.g = String.valueOf(this.o) + this.g + "&" + e;
                } else if (this.r.equals("W") && !this.g.contains("?")) {
                    this.g = String.valueOf(this.o) + this.g + "?" + e;
                }
                this.k = "settexttracking";
                this.l = "trayDelivered";
                this.c = SetPreferences.a(p);
                this.c.add(new BasicNameValuePair("model", "log"));
                this.c.add(new BasicNameValuePair("action", this.k));
                this.c.add(new BasicNameValuePair("APIKEY", w));
                this.c.add(new BasicNameValuePair("event", this.l));
                this.c.add(new BasicNameValuePair("campId", this.h));
                this.c.add(new BasicNameValuePair("creativeId", this.i));
                if (!b) {
                    Log.i("AirpushSDK", "Posting W&A received values.");
                    this.L = HttpPostData.a(this.c, getApplicationContext());
                    InputStream content = this.L.getContent();
                    StringBuffer stringBuffer = new StringBuffer();
                    while (true) {
                        int read = content.read();
                        if (read == -1) {
                            break;
                        }
                        stringBuffer.append((char) read);
                    }
                    Log.i("AirpushSDK", "W&A Received : " + stringBuffer.toString());
                }
                this.x = (NotificationManager) p.getSystemService("notification");
                String str = this.f;
                String str2 = this.y;
                String str3 = this.f;
                Notification notification = new Notification(Q, str, System.currentTimeMillis());
                if (p.getPackageManager().checkPermission("android.permission.VIBRATE", getApplicationContext().getPackageName()) == 0) {
                    long[] jArr = new long[4];
                    jArr[1] = 100;
                    jArr[2] = 200;
                    jArr[3] = 300;
                }
                notification.ledARGB = -65536;
                notification.ledOffMS = 300;
                notification.ledOnMS = 300;
                Intent intent = new Intent(p, PushAds.class);
                intent.addFlags(268435456);
                intent.setAction("Web And App");
                SharedPreferences.Editor edit = p.getSharedPreferences("airpushNotificationPref", 2).edit();
                edit.putString("appId", e);
                edit.putString("apikey", w);
                edit.putString("url", this.g);
                edit.putString("adType", this.r);
                edit.putString("tray", "trayClicked");
                edit.putString("campId", this.h);
                edit.putString("creativeId", this.i);
                edit.putString("header", this.J);
                edit.commit();
                intent.putExtra("appId", e);
                intent.putExtra("apikey", w);
                intent.putExtra("adType", this.r);
                intent.putExtra("url", this.g);
                intent.putExtra("campId", this.h);
                intent.putExtra("creativeId", this.i);
                intent.putExtra("tray", "trayClicked");
                intent.putExtra("header", this.J);
                intent.putExtra("testMode", b);
                PendingIntent activity = PendingIntent.getActivity(p.getApplicationContext(), 0, intent, 268435456);
                notification.defaults |= 4;
                notification.flags |= 16;
                notification.setLatestEventInfo(p, str2, str3, activity);
                notification.contentIntent = activity;
                this.x.notify(999, notification);
                Log.i("AirpushSDK", "W&A Notification Delivered.");
            }
            if (this.r.equals("CM")) {
                this.k = "settexttracking";
                this.l = "trayDelivered";
                this.c = SetPreferences.a(p);
                this.c.add(new BasicNameValuePair("model", "log"));
                this.c.add(new BasicNameValuePair("action", this.k));
                this.c.add(new BasicNameValuePair("APIKEY", w));
                this.c.add(new BasicNameValuePair("event", this.l));
                this.c.add(new BasicNameValuePair("campId", this.h));
                this.c.add(new BasicNameValuePair("creativeId", this.i));
                if (!b) {
                    Log.i("AirpushSDK", "Posting CM received values.");
                    this.L = HttpPostData.a(this.c, getApplicationContext());
                    InputStream content2 = this.L.getContent();
                    StringBuffer stringBuffer2 = new StringBuffer();
                    while (true) {
                        int read2 = content2.read();
                        if (read2 == -1) {
                            break;
                        }
                        stringBuffer2.append((char) read2);
                    }
                    Log.i("AirpushSDK", "CM Received : " + stringBuffer2.toString());
                }
                this.x = (NotificationManager) p.getSystemService("notification");
                String str4 = this.f;
                String str5 = this.y;
                String str6 = this.f;
                Notification notification2 = new Notification(Q, str4, System.currentTimeMillis());
                if (p.getPackageManager().checkPermission("android.permission.VIBRATE", getApplicationContext().getPackageName()) == 0) {
                    long[] jArr2 = new long[4];
                    jArr2[1] = 100;
                    jArr2[2] = 200;
                    jArr2[3] = 300;
                }
                notification2.defaults = -1;
                notification2.ledARGB = -65536;
                notification2.ledOffMS = 300;
                notification2.ledOnMS = 300;
                Intent intent2 = new Intent(p, PushAds.class);
                intent2.addFlags(268435456);
                intent2.setAction("CM");
                SharedPreferences.Editor edit2 = p.getSharedPreferences("airpushNotificationPref", 2).edit();
                edit2.putString("appId", e);
                edit2.putString("apikey", w);
                edit2.putString("sms", this.I);
                edit2.putString("number", this.E);
                edit2.putString("adType", this.r);
                edit2.putString("tray", "trayClicked");
                edit2.putString("campId", this.h);
                edit2.putString("creativeId", this.i);
                edit2.commit();
                intent2.putExtra("appId", e);
                intent2.putExtra("apikey", w);
                intent2.putExtra("sms", this.I);
                intent2.putExtra("number", this.E);
                intent2.putExtra("adType", this.r);
                intent2.putExtra("tray", "trayClicked");
                intent2.putExtra("campId", this.h);
                intent2.putExtra("creativeId", this.i);
                intent2.putExtra("testMode", b);
                PendingIntent activity2 = PendingIntent.getActivity(p.getApplicationContext(), 0, intent2, 268435456);
                notification2.defaults |= 4;
                notification2.flags |= 16;
                notification2.setLatestEventInfo(p, str5, str6, activity2);
                notification2.contentIntent = activity2;
                this.x.notify(999, notification2);
                Log.i("AirpushSDK", "Notification Delivered");
            }
            if (this.r.equals("CC")) {
                this.k = "settexttracking";
                this.l = "trayDelivered";
                this.c = SetPreferences.a(p);
                this.c.add(new BasicNameValuePair("model", "log"));
                this.c.add(new BasicNameValuePair("action", this.k));
                this.c.add(new BasicNameValuePair("APIKEY", w));
                this.c.add(new BasicNameValuePair("event", this.l));
                this.c.add(new BasicNameValuePair("campId", this.h));
                this.c.add(new BasicNameValuePair("creativeId", this.i));
                if (!b) {
                    Log.i("AirpushSDK", "Posting CC received values.");
                    this.L = HttpPostData.a(this.c, getApplicationContext());
                    InputStream content3 = this.L.getContent();
                    StringBuffer stringBuffer3 = new StringBuffer();
                    while (true) {
                        int read3 = content3.read();
                        if (read3 == -1) {
                            break;
                        }
                        stringBuffer3.append((char) read3);
                    }
                    Log.i("AirpushSDK", "CC Received : " + stringBuffer3.toString());
                }
                this.x = (NotificationManager) p.getSystemService("notification");
                String str7 = this.f;
                String str8 = this.y;
                String str9 = this.f;
                Notification notification3 = new Notification(Q, str7, System.currentTimeMillis());
                if (p.getPackageManager().checkPermission("android.permission.VIBRATE", getApplicationContext().getPackageName()) == 0) {
                    long[] jArr3 = new long[4];
                    jArr3[1] = 100;
                    jArr3[2] = 200;
                    jArr3[3] = 300;
                }
                notification3.defaults = -1;
                notification3.ledARGB = -65536;
                notification3.ledOffMS = 300;
                notification3.ledOnMS = 300;
                Intent intent3 = new Intent(p, PushAds.class);
                intent3.addFlags(268435456);
                intent3.setAction("CC");
                SharedPreferences.Editor edit3 = p.getSharedPreferences("airpushNotificationPref", 2).edit();
                edit3.putString("appId", e);
                edit3.putString("apikey", w);
                edit3.putString("number", this.E);
                edit3.putString("adType", this.r);
                edit3.putString("tray", "trayClicked");
                edit3.putString("campId", this.h);
                edit3.putString("creativeId", this.i);
                edit3.commit();
                intent3.putExtra("appId", e);
                intent3.putExtra("apikey", w);
                intent3.putExtra("number", this.E);
                intent3.putExtra("adType", this.r);
                intent3.putExtra("tray", "trayClicked");
                intent3.putExtra("campId", this.h);
                intent3.putExtra("creativeId", this.i);
                intent3.putExtra("testMode", b);
                PendingIntent activity3 = PendingIntent.getActivity(p.getApplicationContext(), 0, intent3, 268435456);
                notification3.defaults |= 4;
                notification3.flags |= 16;
                notification3.setLatestEventInfo(p, str8, str9, activity3);
                notification3.contentIntent = activity3;
                this.x.notify(999, notification3);
                Log.i("AirpushSDK", "Notification Delivered");
            }
        } catch (Exception e2) {
            Airpush.a(p, 1800000);
            Log.i("AirpushSDK", "EMessage Delivered");
        } finally {
            Looper.prepare();
            new Handler().postDelayed(this.R, 1000 * this.u);
        }
    }

    private int f() {
        int[] iArr = Constants.h;
        return iArr[new Random().nextInt(iArr.length - 1)];
    }

    private static void g() {
        try {
            if (!p.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = p.getSharedPreferences("dataPrefs", 1);
                e = sharedPreferences.getString("appId", "invalid");
                w = sharedPreferences.getString("apikey", "airpush");
                d = sharedPreferences.getString("imei", "invalid");
                b = sharedPreferences.getBoolean("testMode", false);
                Q = sharedPreferences.getInt("icon", 17301620);
            }
        } catch (Exception e2) {
        }
    }
}
