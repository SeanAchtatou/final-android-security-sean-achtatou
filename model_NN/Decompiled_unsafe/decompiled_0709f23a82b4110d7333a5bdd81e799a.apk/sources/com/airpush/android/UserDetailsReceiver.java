package com.airpush.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import com.mobclick.android.UmengConstants;
import java.util.List;
import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class UserDetailsReceiver extends BroadcastReceiver {
    protected static Context b = null;
    private static String e = "invalid";
    List<NameValuePair> a = null;
    private String c = "Invalid";
    private String d = "airpush";
    private boolean f;
    private int g;
    private String h;
    private String i;
    private JSONObject j;

    public void onReceive(Context context, Intent intent) {
        b = context;
        if (SetPreferences.isEnabled(b)) {
            try {
                if (Constants.a(b)) {
                    if (intent.getAction().equals("SetUserInfo")) {
                        a();
                    }
                    Log.i("AirpushSDK", "Sending User Info....");
                    Constants.a(context, "airpushAppid " + this.c);
                    Intent intent2 = new Intent();
                    intent2.setAction("com.airpush.android.PushServiceStart" + this.c);
                    intent2.putExtra("appId", this.c);
                    intent2.putExtra(UmengConstants.AtomKey_Type, "userInfo");
                    intent2.putExtra("apikey", this.d);
                    if (!intent2.equals(null)) {
                        b.startService(intent2);
                        return;
                    }
                    a();
                    new Airpush(b, this.c, "airpush", false, true, true);
                    return;
                }
                Airpush.a(b, 1800000);
            } catch (Exception e2) {
                a();
                new Airpush(b, this.c, "airpush", false, true, true);
                Log.i("AirpushSDK", "Sending User Info failed");
            }
        } else {
            Log.i("AirpushSDK", "SDK is disabled, please enable to receive Ads !");
        }
    }

    private void a() {
        try {
            if (!b.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences sharedPreferences = b.getSharedPreferences("dataPrefs", 1);
                this.c = sharedPreferences.getString("appId", "invalid");
                this.d = sharedPreferences.getString("apikey", "airpush");
                e = sharedPreferences.getString("imei", "invalid");
                this.f = sharedPreferences.getBoolean("testMode", false);
                this.g = sharedPreferences.getInt("icon", 17301620);
                return;
            }
            this.i = b.getPackageName();
            this.h = HttpPostData.a("http://api.airpush.com/model/user/getappinfo.php?packageName=" + this.i, "default", "default", b);
            this.c = a(this.h);
            this.d = b(this.h);
        } catch (Exception e2) {
            this.i = b.getPackageName();
            this.h = HttpPostData.a("http://api.airpush.com/model/user/getappinfo.php?packageName=" + this.i, "default", "default", b);
            this.c = a(this.h);
            this.d = b(this.h);
            new Airpush(b, this.c, "airpush", false, true, true);
        }
    }

    private String a(String str) {
        try {
            this.j = new JSONObject(str);
            return this.j.getString("appid");
        } catch (JSONException e2) {
            return "invalid Id";
        }
    }

    private String b(String str) {
        try {
            this.j = new JSONObject(str);
            return this.j.getString("authkey");
        } catch (JSONException e2) {
            return "invalid key";
        }
    }
}
