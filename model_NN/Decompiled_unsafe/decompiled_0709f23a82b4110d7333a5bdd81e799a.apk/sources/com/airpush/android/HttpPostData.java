package com.airpush.android;

import android.content.Context;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.List;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HttpContext;

public final class HttpPostData {
    protected static long a = 1800000;
    private static String b;
    private static Context c;
    private static BasicHttpParams d;
    private static int e;
    private static int f;
    private static DefaultHttpClient g;
    private static HttpEntity h;
    private static BasicHttpResponse i;
    private static HttpPost j;

    private interface Prefs {
        public static final String IOSCHED_SYNC = "iosched_sync";
        public static final String LOCAL_VERSION = "local_version";
    }

    protected static HttpEntity a(List<NameValuePair> list, Context context) {
        if (Constants.a(context)) {
            c = context;
            try {
                j = new HttpPost("http://api.airpush.com/v2/api.php");
                j.setEntity(new UrlEncodedFormEntity(list));
                d = new BasicHttpParams();
                e = 3000;
                HttpConnectionParams.setConnectionTimeout(d, e);
                f = 3000;
                HttpConnectionParams.setSoTimeout(d, f);
                g = new DefaultHttpClient(d);
                g.addRequestInterceptor(new HttpRequestInterceptor() {
                    public void process(HttpRequest httpRequest, HttpContext httpContext) {
                        if (!httpRequest.containsHeader("Accept-Encoding")) {
                            httpRequest.addHeader("Accept-Encoding", "gzip");
                        }
                    }
                });
                g.addResponseInterceptor(new HttpResponseInterceptor() {
                    public void process(HttpResponse httpResponse, HttpContext httpContext) {
                        Header contentEncoding = httpResponse.getEntity().getContentEncoding();
                        if (contentEncoding != null) {
                            for (HeaderElement name : contentEncoding.getElements()) {
                                if (name.getName().equalsIgnoreCase("gzip")) {
                                    httpResponse.setEntity(new InflatingEntity(httpResponse.getEntity()));
                                    return;
                                }
                            }
                        }
                    }
                });
                i = g.execute(j);
                h = i.getEntity();
                return h;
            } catch (SocketTimeoutException e2) {
                Log.d("SocketTimeoutException Thrown", e2.toString());
                Airpush.a(c, 1800000);
                return null;
            } catch (ClientProtocolException e3) {
                Log.d("ClientProtocolException Thrown", e3.toString());
                Airpush.a(c, 1800000);
                return null;
            } catch (MalformedURLException e4) {
                Airpush.a(c, 1800000);
                Log.d("MalformedURLException Thrown", e4.toString());
                return null;
            } catch (IOException e5) {
                Airpush.a(c, 1800000);
                Log.d("IOException Thrown", e5.toString());
                return null;
            } catch (Exception e6) {
                Log.i("AirpushSDK", e6.toString());
                Airpush.a(c, 1800000);
                return null;
            }
        } else {
            Airpush.a(context, a);
            return null;
        }
    }

    protected static HttpEntity a(List<NameValuePair> list, boolean z, Context context) {
        if (Constants.a(context)) {
            c = context;
            if (z) {
                try {
                    b = "http://api.airpush.com/testmsg2.php";
                } catch (SocketTimeoutException e2) {
                    Log.d("SocketTimeoutException Thrown", e2.toString());
                    Airpush.a(c, 1800000);
                    return null;
                } catch (ClientProtocolException e3) {
                    Log.d("ClientProtocolException Thrown", e3.toString());
                    Airpush.a(c, 1800000);
                    return null;
                } catch (MalformedURLException e4) {
                    Airpush.a(c, 1800000);
                    Log.d("MalformedURLException Thrown", e4.toString());
                    return null;
                } catch (IOException e5) {
                    Airpush.a(c, 1800000);
                    Log.d("IOException Thrown", e5.toString());
                    return null;
                } catch (Exception e6) {
                    Airpush.a(c, 1800000);
                    return null;
                }
            } else {
                b = "http://api.airpush.com/v2/api.php";
            }
            j = new HttpPost(b);
            j.setEntity(new UrlEncodedFormEntity(list));
            d = new BasicHttpParams();
            e = 10000;
            HttpConnectionParams.setConnectionTimeout(d, e);
            f = 10000;
            HttpConnectionParams.setSoTimeout(d, f);
            g = new DefaultHttpClient(d);
            g.addRequestInterceptor(new HttpRequestInterceptor() {
                public void process(HttpRequest httpRequest, HttpContext httpContext) {
                    if (!httpRequest.containsHeader("Accept-Encoding")) {
                        httpRequest.addHeader("Accept-Encoding", "gzip");
                    }
                }
            });
            g.addResponseInterceptor(new HttpResponseInterceptor() {
                public void process(HttpResponse httpResponse, HttpContext httpContext) {
                    Header contentEncoding = httpResponse.getEntity().getContentEncoding();
                    if (contentEncoding != null) {
                        for (HeaderElement name : contentEncoding.getElements()) {
                            if (name.getName().equalsIgnoreCase("gzip")) {
                                httpResponse.setEntity(new InflatingEntity(httpResponse.getEntity()));
                                return;
                            }
                        }
                    }
                }
            });
            i = g.execute(j);
            h = i.getEntity();
            return h;
        }
        Airpush.a(context, a);
        return null;
    }

    protected static String a(String str, String str2, String str3, Context context) {
        if (Constants.a(context)) {
            c = context;
            try {
                if (Constants.a(context)) {
                    HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.setConnectTimeout(3000);
                    httpURLConnection.connect();
                    if (httpURLConnection.getResponseCode() == 200) {
                        StringBuffer stringBuffer = new StringBuffer();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                        while (true) {
                            String readLine = bufferedReader.readLine();
                            if (readLine == null) {
                                return stringBuffer.toString();
                            }
                            stringBuffer.append(readLine);
                        }
                    }
                }
            } catch (SocketTimeoutException e2) {
                Log.d("SocketTimeoutException Thrown", e2.toString());
                Airpush.a(c, 1800000);
            } catch (ClientProtocolException e3) {
                Log.d("ClientProtocolException Thrown", e3.toString());
                Airpush.a(c, 1800000);
            } catch (MalformedURLException e4) {
                e4.printStackTrace();
                Airpush.a(c, 1800000);
                Log.d("MalformedURLException Thrown", e4.toString());
            } catch (IOException e5) {
                e5.printStackTrace();
                Airpush.a(c, 1800000);
                Log.d("IOException Thrown", e5.toString());
            } catch (Exception e6) {
                Airpush.a(c, 1800000);
            }
            return "";
        }
        Airpush.a(context, a);
        return "";
    }

    private static class InflatingEntity extends HttpEntityWrapper {
        public InflatingEntity(HttpEntity httpEntity) {
            super(httpEntity);
        }

        public InputStream getContent() throws IOException {
            return new GZIPInputStream(this.wrappedEntity.getContent());
        }

        public long getContentLength() {
            return -1;
        }
    }
}
