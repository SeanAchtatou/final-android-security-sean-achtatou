package com.airpush.android;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.KeyEvent;
import android.webkit.WebView;

public class CustomWebView extends WebView {
    private ProgressDialog a = null;
    private Context b;

    public CustomWebView(Context context) {
        super(context);
        clearCache(true);
        clearFormData();
        clearHistory();
        getSettings().setJavaScriptEnabled(true);
        getSettings().setUserAgentString(null);
        requestFocus(130);
        this.a = new ProgressDialog(context);
        this.a.setMessage("Loading...");
        this.b = context;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || !canGoBack()) {
            return super.onKeyDown(i, keyEvent);
        }
        goBack();
        return true;
    }
}
