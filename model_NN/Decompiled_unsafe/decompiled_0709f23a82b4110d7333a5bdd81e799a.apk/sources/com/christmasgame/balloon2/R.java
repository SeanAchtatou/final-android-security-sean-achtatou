package com.christmasgame.balloon2;

public final class R {

    public static final class array {
        public static final int barscolors = 2131099651;
        public static final int ebucolors = 2131099652;
        public static final int livewallpaper_testpattern_names = 2131099648;
        public static final int livewallpaper_testpattern_prefix = 2131099649;
        public static final int smptecolors = 2131099650;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int rankingland = 2130837505;
    }

    public static final class id {
        public static final int Button01 = 2131230736;
        public static final int ButtonOk = 2131230726;
        public static final int EditTextNick = 2131230724;
        public static final int LinearLayout01 = 2131230729;
        public static final int LinearLayout02 = 2131230730;
        public static final int LinearLayout03 = 2131230722;
        public static final int LinearLayoutAd = 2131230737;
        public static final int LinearLayoutAd1 = 2131230727;
        public static final int LinearLayoutAd2 = 2131230728;
        public static final int LinearLayoutButtons = 2131230735;
        public static final int ScrollView01 = 2131230733;
        public static final int TableLayoutRankList = 2131230734;
        public static final int TextView03 = 2131230723;
        public static final int TextViewTitle = 2131230731;
        public static final int bestRecord = 2131230732;
        public static final int editProfile = 2131230739;
        public static final int featured_apps_webview = 2131230720;
        public static final int refresh = 2131230740;
        public static final int resultString = 2131230725;
        public static final int rootLayout = 2131230721;
        public static final int widgetBtn = 2131230738;
    }

    public static final class layout {
        public static final int featured = 2130903040;
        public static final int main = 2130903041;
        public static final int profile = 2130903042;
        public static final int ranking = 2130903043;
        public static final int widget = 2130903044;
    }

    public static final class menu {
        public static final int rankmenu = 2131165184;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int startapp_appid = 2131034120;
        public static final int startapp_devid = 2131034119;
        public static final int string_id_featured = 2131034118;
        public static final int string_id_feedback = 2131034115;
        public static final int string_id_quit = 2131034114;
        public static final int string_id_register = 2131034116;
        public static final int string_id_removead = 2131034117;
        public static final int string_id_submit = 2131034113;
    }

    public static final class xml {
        public static final int app_widget = 2130968576;
        public static final int livewallpaper = 2130968577;
    }
}
