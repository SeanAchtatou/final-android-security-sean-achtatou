package com.christmasgame.balloon2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.airpush.android.Airpush;

public class BootReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (Integer.parseInt(Build.VERSION.SDK) > 3) {
            new Airpush(context, "36986", "1330012471988484806", false, true, true);
        }
    }
}
