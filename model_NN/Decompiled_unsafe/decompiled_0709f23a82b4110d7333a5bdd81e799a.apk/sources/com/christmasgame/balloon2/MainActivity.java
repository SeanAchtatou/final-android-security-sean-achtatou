package com.christmasgame.balloon2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.Process;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.christmasgame.common.GamePlayer;
import com.christmasgame.common.GameView;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.mobclick.android.MobclickAgent;
import com.mobclix.android.sdk.MobclixAdView;
import com.mobclix.android.sdk.MobclixAdViewListener;
import com.mobclix.android.sdk.MobclixMMABannerXLAdView;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Locale;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class MainActivity extends Activity {
    public static final int MENU_ID_FEATURED = 7;
    public static final int MENU_ID_FEEDBACK = 2;
    public static final int MENU_ID_QUIT = 4;
    public static final int MENU_ID_REGISTER = 5;
    public static final int MENU_ID_SHOWPRO = 6;
    public static final int MENU_ID_SUBMIT = 3;
    public static String MYGAMEID = "balloongame2";
    public static String mFeaturedURL = "http://api.itunes-game.com/featured-app-list.php?gameid=";
    public static String mLeaderBoardServer = "http://api.itunes-game.com/aftdmedia-userapi.php";
    public static GamePlayer mPlayer = null;
    public static String mUserID;
    int A = 1;
    boolean B = false;
    Sensor C;
    SensorEventListener D;
    double E = 10.2d;
    double F = -10.2d;
    boolean G = false;
    PowerManager.WakeLock H;
    int I = 0;
    boolean J = false;
    private GameView K;
    /* access modifiers changed from: private */
    public int L;
    /* access modifiers changed from: private */
    public int M;
    /* access modifiers changed from: private */
    public FrameLayout N;
    private int O;
    /* access modifiers changed from: private */
    public AD_TYPE P = AD_TYPE.AD_TYPE_NONE;
    /* access modifiers changed from: private */
    public AD_TYPE Q = AD_TYPE.AD_TYPE_MOBCLIX;
    /* access modifiers changed from: private */
    public AdView R;
    private SensorManager S;
    private int T;
    private int U;
    /* access modifiers changed from: private */
    public final Handler V = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 65537:
                    Toast.makeText(MainActivity.this, "New highest score!You can submit score to leaderboard!", 1).show();
                    return;
                case 65538:
                    if (!MainActivity.this.y) {
                        return;
                    }
                    if (MainActivity.this.P == AD_TYPE.AD_TYPE_ADMOB) {
                        if (MainActivity.this.R != null) {
                            MainActivity.this.R.setVisibility(0);
                            try {
                                MainActivity.this.R.requestLayout();
                                return;
                            } catch (Exception e) {
                                return;
                            }
                        } else {
                            return;
                        }
                    } else if (MainActivity.this.P == AD_TYPE.AD_TYPE_MOBCLIX && MainActivity.this.b != null) {
                        MainActivity.this.b.setVisibility(0);
                        try {
                            MainActivity.this.b.requestLayout();
                            return;
                        } catch (Exception e2) {
                            return;
                        }
                    } else {
                        return;
                    }
                case 65539:
                    if (!MainActivity.this.y) {
                        return;
                    }
                    if (MainActivity.this.P == AD_TYPE.AD_TYPE_ADMOB) {
                        if (MainActivity.this.R != null) {
                            MainActivity.this.R.setVisibility(4);
                            return;
                        }
                        return;
                    } else if (MainActivity.this.P == AD_TYPE.AD_TYPE_MOBCLIX && MainActivity.this.b != null) {
                        MainActivity.this.b.setVisibility(4);
                        return;
                    } else {
                        return;
                    }
                case 65540:
                case 65544:
                case 65545:
                default:
                    return;
                case 65541:
                    try {
                        ArrayList arrayList = (ArrayList) message.obj;
                        String[] split = ((String) arrayList.get(0)).split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                        if (split.length == 2) {
                            int parseInt = Integer.parseInt(split[0]);
                            String str = split[1];
                            if (str == null || (parseInt & 2) == 0 || str.equals(MainActivity.this.getPackageName())) {
                                MainActivity.this.a(arrayList, parseInt);
                                return;
                            } else {
                                MainActivity.this.f(str);
                                return;
                            }
                        } else {
                            return;
                        }
                    } catch (Exception e3) {
                        return;
                    }
                case 65542:
                    try {
                        if (!(MainActivity.this.R == null || MainActivity.this.r() || MainActivity.this.Q == AD_TYPE.AD_TYPE_ADMOB)) {
                            MainActivity.this.N.removeView(MainActivity.this.R);
                            MainActivity.this.R = (AdView) null;
                        }
                        MainActivity.this.P = AD_TYPE.AD_TYPE_MOBCLIX;
                        if (MainActivity.this.B && MainActivity.this.b != null) {
                            MainActivity.this.b.setVisibility(4);
                        }
                        int measuredWidth = MainActivity.this.b.getMeasuredWidth();
                        int g = MainActivity.this.L - measuredWidth;
                        MainActivity.this.b.setLayoutParams(new FrameLayout.LayoutParams(measuredWidth, MainActivity.this.b.getMeasuredHeight(), 49));
                        return;
                    } catch (Exception e4) {
                        return;
                    }
                case 65543:
                    try {
                        if (MainActivity.this.b != null && (MainActivity.this.r() || MainActivity.this.Q == AD_TYPE.AD_TYPE_ADMOB)) {
                            MainActivity.this.N.removeView(MainActivity.this.b);
                            MainActivity.this.b = null;
                        }
                        MainActivity.this.P = AD_TYPE.AD_TYPE_ADMOB;
                        if (MainActivity.this.B && MainActivity.this.R != null) {
                            MainActivity.this.R.setVisibility(4);
                        }
                        int measuredWidth2 = MainActivity.this.R.getMeasuredWidth();
                        int g2 = MainActivity.this.L - measuredWidth2;
                        MainActivity.this.R.setLayoutParams(new FrameLayout.LayoutParams(measuredWidth2, MainActivity.this.R.getMeasuredHeight(), 49));
                        return;
                    } catch (Exception e5) {
                        return;
                    }
                case 65546:
                    MainActivity.this.s();
                    return;
                case 65547:
                    MainActivity.this.t();
                    return;
            }
        }
    };
    int a = 1592486423;
    public MobclixMMABannerXLAdView b;
    public String c = null;
    boolean d = false;
    ByteBuffer e = null;
    byte[] f = null;
    final String g = "localopentime";
    final String h = "localshowedpro";
    final String i = "localshowedfeature";
    final String j = "localshowednewgame_";
    final String k = "localshowedgotourl";
    final String l = "localshowedgotourlvalue";
    final String m = "localshowedratedialog";
    public final int n = 65537;
    public final int o = 65538;
    public final int p = 65539;
    public final int q = 65540;
    public final int r = 65541;
    public final int s = 65542;
    public final int t = 65543;
    public final int u = 65544;
    public final int v = 65545;
    public final int w = 65546;
    public final int x = 65547;
    boolean y = true;
    boolean z = false;

    enum AD_TYPE {
        AD_TYPE_NONE,
        AD_TYPE_ADMOB,
        AD_TYPE_MOBCLIX,
        AD_TYPE_PONTIFLEX
    }

    class dialogData {
        String a;
        String b;
        String c;
        String d;
        boolean e;
        Drawable f;

        dialogData() {
        }
    }

    private void p() {
        try {
            this.b = new MobclixMMABannerXLAdView(this);
            this.b.setBackgroundColor(16777215);
            this.b.a(new MobclixAdViewListener() {
                public String a() {
                    return null;
                }

                public boolean a(MobclixAdView mobclixAdView, int i) {
                    Log.d("Mobclix", "onOpenAllocationLoad");
                    return false;
                }

                public void a(MobclixAdView mobclixAdView, String str) {
                }

                public void a(MobclixAdView mobclixAdView) {
                    Log.d("Mobclix", "onAdClick:" + (MainActivity.this.R != null));
                }

                public void b(MobclixAdView mobclixAdView, int i) {
                    Log.d("Mobclix", "ADFail");
                }

                public void b(MobclixAdView mobclixAdView) {
                    Log.d("Mobclix", "onSuccessfulLoad");
                    MainActivity.this.V.sendEmptyMessage(65542);
                }

                public String b() {
                    return "";
                }
            });
            this.N.addView(this.b);
            this.b.d();
        } catch (Exception e2) {
        }
    }

    private void q() {
        if (r()) {
            this.R = new AdView(this, AdSize.IAB_BANNER, "a14eda642d0202e");
        } else {
            this.R = new AdView(this, AdSize.BANNER, "a14eda642d0202e");
        }
        this.N.addView(this.R);
        this.R.a(new AdRequest());
        this.R.a(new AdListener() {
            public void a(Ad ad) {
            }

            public void a(Ad ad, AdRequest.ErrorCode errorCode) {
            }

            public void b(Ad ad) {
            }

            public void c(Ad ad) {
            }

            public void d(Ad ad) {
                MainActivity.this.V.sendEmptyMessage(65543);
            }
        });
    }

    public void a() {
        this.V.sendEmptyMessage(65538);
        this.B = false;
    }

    public void b() {
        this.V.sendEmptyMessage(65539);
        this.B = true;
    }

    /* access modifiers changed from: private */
    public boolean r() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        if (mPlayer.d) {
            mPlayer.a(i2, i3, 34);
        }
    }

    public void c() {
        if (mPlayer.e && !this.G) {
            this.S.registerListener(this.D, this.C, 1);
            this.G = true;
        }
    }

    public void d() {
        if (this.G) {
            this.S.unregisterListener(this.D);
            this.G = false;
        }
    }

    /* renamed from: com.christmasgame.balloon2.MainActivity$4  reason: invalid class name */
    class AnonymousClass4 implements SensorEventListener {
        final /* synthetic */ MainActivity a;

        public void onSensorChanged(SensorEvent sensorEvent) {
            double d = (double) sensorEvent.values[0];
            double d2 = (double) sensorEvent.values[1];
            double d3 = (double) sensorEvent.values[2];
            if (this.a.A != 2) {
                d = d2;
            }
            if (d < 1.0d && d > -1.0d) {
                this.a.a(this.a.L / 2, this.a.M / 2);
            } else if (d <= 0.0d) {
                if (d < this.a.F) {
                    d = this.a.F;
                }
                this.a.a((int) ((((d / this.a.E) * ((double) this.a.L)) / 2.0d) + ((double) (this.a.L / 2))), this.a.M / 2);
            } else {
                if (d > this.a.E) {
                    d = this.a.E;
                }
                this.a.a((int) ((((d / this.a.E) * ((double) this.a.L)) / 2.0d) + ((double) (this.a.L / 2))), this.a.M / 2);
            }
        }

        public void onAccuracyChanged(Sensor sensor, int i) {
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        try {
            getWindow().addFlags(128);
            this.H = ((PowerManager) getSystemService("power")).newWakeLock(536870922, "GamePlayer");
            this.H.acquire();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        try {
            this.H.release();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        try {
            if (getSDKVersionNumber() >= 8) {
                Method declaredMethod = Class.forName("android.view.Display").getDeclaredMethod("getRotation", new Class[0]);
                declaredMethod.setAccessible(true);
                try {
                    int intValue = ((Integer) declaredMethod.invoke(getWindowManager().getDefaultDisplay(), new Object[0])).intValue();
                    if (getResources().getConfiguration().orientation == 2 && intValue == 0) {
                        this.A = 2;
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public void h() {
        SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), 0);
        try {
            this.I = sharedPreferences.getInt("localopentime", 0);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putInt("localopentime", this.I + 1);
            edit.commit();
        } catch (Exception e2) {
        }
        this.J = false;
        if (this.I >= 5 && e("com.android.vending") && !sharedPreferences.getBoolean("localshowedratedialog", false)) {
            this.J = true;
            this.V.sendEmptyMessageDelayed(65547, 500);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean i() {
        try {
            return getSharedPreferences(getPackageName(), 0).getBoolean("localshowedfeature", false);
        } catch (Exception e2) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        try {
            SharedPreferences.Editor edit = getSharedPreferences(getPackageName(), 0).edit();
            edit.putBoolean("localshowedfeature", z2);
            edit.commit();
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str) {
        if (str == null) {
            return false;
        }
        try {
            return getSharedPreferences(getPackageName(), 0).getBoolean("localshowednewgame_" + str, false);
        } catch (Exception e2) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, boolean z2) {
        if (str != null) {
            try {
                SharedPreferences.Editor edit = getSharedPreferences(getPackageName(), 0).edit();
                edit.putBoolean("localshowednewgame_" + str, z2);
                edit.commit();
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String j() {
        try {
            return getSharedPreferences(getPackageName(), 0).getString("localshowedgotourlvalue", null);
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (str != null) {
            try {
                SharedPreferences.Editor edit = getSharedPreferences(getPackageName(), 0).edit();
                edit.putString("localshowedgotourlvalue", str);
                edit.commit();
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c(String str) {
        if (str == null) {
            return true;
        }
        String j2 = j();
        if (j2 == null || !j2.equals(str)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        b(str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x00aa A[Catch:{ Exception -> 0x00fc }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bf A[Catch:{ Exception -> 0x00fc }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
            r9 = this;
            r6 = 0
            super.onCreate(r10)
            r0 = 2130903041(0x7f030001, float:1.7412889E38)
            r9.setContentView(r0)
            com.apperhand.device.android.AndroidSDKProvider.initSDK(r9)     // Catch:{ Exception -> 0x00fe }
        L_0x000d:
            android.util.DisplayMetrics r0 = new android.util.DisplayMetrics
            r0.<init>()
            android.content.res.Resources r0 = r9.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            int r1 = r0.widthPixels
            r9.L = r1
            int r0 = r0.heightPixels
            r9.M = r0
            r9.g()
            int r0 = r9.L
            int r1 = r9.M
            android.graphics.Bitmap$Config r2 = android.graphics.Bitmap.Config.RGB_565
            android.graphics.Bitmap r4 = android.graphics.Bitmap.createBitmap(r0, r1, r2)
            com.christmasgame.common.GameView r0 = new com.christmasgame.common.GameView
            int r2 = r9.L
            int r3 = r9.M
            r5 = 4
            r1 = r9
            r0.<init>(r1, r2, r3, r4, r5)
            r9.K = r0
            com.christmasgame.common.GamePlayer r0 = new com.christmasgame.common.GamePlayer
            com.christmasgame.common.GameView r1 = r9.K
            r0.<init>(r9, r1)
            com.christmasgame.balloon2.MainActivity.mPlayer = r0
            r9.O = r6
            r0 = 2131230721(0x7f080001, float:1.8077503E38)
            android.view.View r0 = r9.findViewById(r0)
            android.widget.FrameLayout r0 = (android.widget.FrameLayout) r0
            r9.N = r0
            android.widget.FrameLayout r0 = r9.N
            com.christmasgame.common.GameView r1 = r9.K
            r0.addView(r1)
            java.lang.String r0 = com.christmasgame.common.GamePlayer.getUniqueId()
            com.christmasgame.balloon2.MainActivity.mUserID = r0
            java.lang.String r0 = "GamePlayer"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "generate uuid:"
            r1.<init>(r2)
            java.lang.String r2 = com.christmasgame.balloon2.MainActivity.mUserID
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r0, r1)
            r9.v()
            java.lang.String r0 = r9.getPackageName()
            android.content.SharedPreferences r8 = r9.getSharedPreferences(r0, r6)
            java.lang.String r0 = "adflag:"
            r1 = 0
            int r0 = r8.getInt(r0, r1)     // Catch:{ Exception -> 0x00f1 }
            r1 = r0 & 1
            if (r1 == 0) goto L_0x008f
            com.christmasgame.balloon2.MainActivity$AD_TYPE r1 = com.christmasgame.balloon2.MainActivity.AD_TYPE.AD_TYPE_ADMOB     // Catch:{ Exception -> 0x0101 }
            r9.Q = r1     // Catch:{ Exception -> 0x0101 }
        L_0x008f:
            r1 = r0 & 2
            if (r1 == 0) goto L_0x0096
            r1 = 0
            r9.y = r1     // Catch:{ Exception -> 0x0101 }
        L_0x0096:
            r1 = r0 & 4
            if (r1 == 0) goto L_0x00ea
            boolean r1 = isUSLocale(r9)     // Catch:{ Exception -> 0x0101 }
            if (r1 == 0) goto L_0x00ea
            r1 = 1
            r9.z = r1     // Catch:{ Exception -> 0x0101 }
            r7 = r0
        L_0x00a4:
            boolean r0 = r9.r()     // Catch:{ Exception -> 0x00fc }
            if (r0 != 0) goto L_0x00b0
            com.christmasgame.balloon2.MainActivity$AD_TYPE r0 = r9.Q     // Catch:{ Exception -> 0x00fc }
            com.christmasgame.balloon2.MainActivity$AD_TYPE r1 = com.christmasgame.balloon2.MainActivity.AD_TYPE.AD_TYPE_ADMOB     // Catch:{ Exception -> 0x00fc }
            if (r0 != r1) goto L_0x00f5
        L_0x00b0:
            r9.p()     // Catch:{ Exception -> 0x00fc }
            r9.q()     // Catch:{ Exception -> 0x00fc }
        L_0x00b6:
            java.lang.String r0 = android.os.Build.VERSION.SDK     // Catch:{ Exception -> 0x00fc }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x00fc }
            r1 = 3
            if (r0 <= r1) goto L_0x00cf
            com.airpush.android.Airpush r0 = new com.airpush.android.Airpush     // Catch:{ Exception -> 0x00fc }
            android.content.Context r1 = r9.getApplicationContext()     // Catch:{ Exception -> 0x00fc }
            java.lang.String r2 = "36986"
            java.lang.String r3 = "1330012471988484806"
            r4 = 0
            r5 = 1
            r6 = 1
            r0.<init>(r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x00fc }
        L_0x00cf:
            r0 = r7 & 8
            if (r0 == 0) goto L_0x00e6
            java.lang.String r0 = "HadShowedFeatured"
            r1 = 0
            boolean r0 = r8.getBoolean(r0, r1)     // Catch:{ Exception -> 0x00fc }
            if (r0 != 0) goto L_0x00e6
            android.os.Handler r0 = r9.V     // Catch:{ Exception -> 0x00fc }
            r1 = 65546(0x1000a, float:9.185E-41)
            r2 = 3000(0xbb8, double:1.482E-320)
            r0.sendEmptyMessageDelayed(r1, r2)     // Catch:{ Exception -> 0x00fc }
        L_0x00e6:
            r9.h()
            return
        L_0x00ea:
            if (r0 == 0) goto L_0x0103
            r1 = 0
            r9.z = r1     // Catch:{ Exception -> 0x0101 }
            r7 = r0
            goto L_0x00a4
        L_0x00f1:
            r0 = move-exception
            r0 = r6
        L_0x00f3:
            r7 = r0
            goto L_0x00a4
        L_0x00f5:
            r9.q()     // Catch:{ Exception -> 0x00fc }
            r9.p()     // Catch:{ Exception -> 0x00fc }
            goto L_0x00b6
        L_0x00fc:
            r0 = move-exception
            goto L_0x00e6
        L_0x00fe:
            r0 = move-exception
            goto L_0x000d
        L_0x0101:
            r1 = move-exception
            goto L_0x00f3
        L_0x0103:
            r7 = r0
            goto L_0x00a4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.christmasgame.balloon2.MainActivity.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0081  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int k() {
        /*
            r7 = this;
            r2 = 0
            r1 = 0
            android.content.res.AssetManager r3 = r7.getAssets()
            r0 = r1
            java.lang.String[] r0 = (java.lang.String[]) r0
            java.lang.String r4 = "raw"
            java.lang.String[] r0 = r3.list(r4)     // Catch:{ IOException -> 0x0085 }
        L_0x000f:
            java.lang.String r4 = "Fils"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            int r6 = r0.length
            java.lang.String r6 = java.lang.String.valueOf(r6)
            r5.<init>(r6)
            java.lang.String r6 = "::"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.d(r4, r0)
            java.lang.String r0 = "raw/assets"
            java.io.InputStream r0 = r3.open(r0)     // Catch:{ IOException -> 0x0071 }
            r3 = r0
        L_0x0033:
            if (r3 == 0) goto L_0x007b
            int r0 = r3.available()     // Catch:{ Exception -> 0x0074 }
            java.nio.ByteBuffer r4 = java.nio.ByteBuffer.allocate(r0)     // Catch:{ Exception -> 0x0083 }
            r7.e = r4     // Catch:{ Exception -> 0x0083 }
            java.nio.ByteBuffer r4 = r7.e     // Catch:{ Exception -> 0x0083 }
            byte[] r4 = r4.array()     // Catch:{ Exception -> 0x0083 }
            r7.f = r4     // Catch:{ Exception -> 0x0083 }
            byte[] r4 = r7.f     // Catch:{ Exception -> 0x0083 }
            int r3 = r3.read(r4)     // Catch:{ Exception -> 0x0083 }
            if (r3 == r0) goto L_0x0069
            java.lang.String r4 = "READ"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0083 }
            java.lang.String r6 = "read:"
            r5.<init>(r6)     // Catch:{ Exception -> 0x0083 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Exception -> 0x0083 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0083 }
            android.util.Log.d(r4, r3)     // Catch:{ Exception -> 0x0083 }
            r3 = 0
            r7.e = r3     // Catch:{ Exception -> 0x0083 }
            r3 = 0
            r7.f = r3     // Catch:{ Exception -> 0x0083 }
        L_0x0069:
            byte[] r1 = r7.f
            if (r1 != 0) goto L_0x0081
            r7.finish()
        L_0x0070:
            return r2
        L_0x0071:
            r0 = move-exception
            r3 = r1
            goto L_0x0033
        L_0x0074:
            r0 = move-exception
            r0 = r2
        L_0x0076:
            r7.e = r1
            r7.f = r1
            goto L_0x0069
        L_0x007b:
            r7.e = r1
            r7.f = r1
            r0 = r2
            goto L_0x0069
        L_0x0081:
            r2 = r0
            goto L_0x0070
        L_0x0083:
            r3 = move-exception
            goto L_0x0076
        L_0x0085:
            r4 = move-exception
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.christmasgame.balloon2.MainActivity.k():int");
    }

    public void onStart() {
        int i2 = 0;
        super.onStart();
        if (this.O == 0) {
            if (this.f == null) {
                i2 = k();
            }
            this.O = mPlayer.a(this.e, i2);
        }
        if (this.e != null) {
            this.f = null;
            this.e = null;
        }
        if (this.O == 0) {
            finish();
            return;
        }
        try {
            mPlayer.c = getSharedPreferences(getPackageName(), 0).getBoolean("sound", true);
        } catch (Exception e2) {
        }
    }

    public void onResume() {
        super.onResume();
        mPlayer.c();
        MobclickAgent.onResume(this);
        this.d = false;
        try {
            e();
            setVolumeControlStream(3);
        } catch (Exception e2) {
        }
    }

    public void onPause() {
        super.onPause();
        mPlayer.b();
        MobclickAgent.onPause(this);
        this.d = true;
        try {
            f();
            setVolumeControlStream(2);
        } catch (Exception e2) {
        }
    }

    public void onStop() {
        super.onStop();
    }

    public void onRestart() {
        super.onRestart();
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            if (this.R != null) {
                this.R.a();
            }
        } catch (Exception e2) {
        }
    }

    private boolean e(String str) {
        try {
            getPackageManager().getPackageInfo(str, 0);
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    public static boolean isUSLocale(Context context) {
        if (context == null || context.getResources() == null || context.getResources().getConfiguration() == null || context.getResources().getConfiguration().locale == null || !context.getResources().getConfiguration().locale.getISO3Country().equals("USA")) {
            return false;
        }
        return true;
    }

    public String l() {
        return String.valueOf(this.a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.christmasgame.balloon2.MainActivity.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.christmasgame.balloon2.MainActivity.a(com.christmasgame.balloon2.MainActivity$dialogData, android.content.Intent):void
      com.christmasgame.balloon2.MainActivity.a(com.christmasgame.balloon2.MainActivity, com.christmasgame.balloon2.MainActivity$AD_TYPE):void
      com.christmasgame.balloon2.MainActivity.a(com.christmasgame.balloon2.MainActivity, com.google.ads.AdView):void
      com.christmasgame.balloon2.MainActivity.a(com.christmasgame.balloon2.MainActivity, java.lang.String):void
      com.christmasgame.balloon2.MainActivity.a(java.util.ArrayList<java.lang.String>, int):void
      com.christmasgame.balloon2.MainActivity.a(int, int):void
      com.christmasgame.balloon2.MainActivity.a(java.lang.String, boolean):void */
    /* access modifiers changed from: private */
    public void a(ArrayList<String> arrayList, int i2) {
        boolean z2;
        String str;
        int i3;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        String str9 = null;
        int i4 = 0;
        String str10 = null;
        Boolean bool = true;
        SharedPreferences.Editor edit = getSharedPreferences(getPackageName(), 0).edit();
        int i5 = 1;
        while (i5 < arrayList.size()) {
            String str11 = arrayList.get(i5);
            if (str11 != null) {
                if (str11.startsWith("adflag:")) {
                    try {
                        edit.putInt("adflag:", Integer.parseInt(str11.substring("adflag:".length())));
                        z2 = bool;
                        str = str10;
                        i3 = i4;
                        str2 = str9;
                        str3 = str8;
                        str4 = str7;
                        str5 = str6;
                    } catch (NumberFormatException e2) {
                        e2.printStackTrace();
                        z2 = bool;
                        str = str10;
                        i3 = i4;
                        str2 = str9;
                        str3 = str8;
                        str4 = str7;
                        str5 = str6;
                    }
                } else if (str11.startsWith("title:")) {
                    Boolean bool2 = bool;
                    str = str10;
                    i3 = i4;
                    str2 = str9;
                    str3 = str8;
                    str4 = str7;
                    str5 = str11.substring("title:".length());
                    z2 = bool2;
                } else if (str11.startsWith("desc:")) {
                    str5 = str6;
                    String str12 = str10;
                    i3 = i4;
                    str2 = str9;
                    str3 = str8;
                    str4 = str11.substring("desc:".length());
                    z2 = bool;
                    str = str12;
                } else if (str11.startsWith("id:")) {
                    str4 = str7;
                    str5 = str6;
                    int i6 = i4;
                    str2 = str9;
                    str3 = str11.substring("id:".length());
                    z2 = bool;
                    str = str10;
                    i3 = i6;
                } else if (str11.startsWith("timeout:")) {
                    try {
                        edit.putLong("timeout:", Long.parseLong(str11.substring("timeout:".length())));
                        z2 = bool;
                        str = str10;
                        i3 = i4;
                        str2 = str9;
                        str3 = str8;
                        str4 = str7;
                        str5 = str6;
                    } catch (NumberFormatException e3) {
                        e3.printStackTrace();
                        z2 = bool;
                        str = str10;
                        i3 = i4;
                        str2 = str9;
                        str3 = str8;
                        str4 = str7;
                        str5 = str6;
                    }
                } else if (str11.startsWith("when:")) {
                    str11.substring("when:".length());
                    z2 = bool;
                    str = str10;
                    i3 = i4;
                    str2 = str9;
                    str3 = str8;
                    str4 = str7;
                    str5 = str6;
                } else if (str11.startsWith("locale:")) {
                    str3 = str8;
                    str4 = str7;
                    str5 = str6;
                    String substring = str11.substring("locale:".length());
                    z2 = bool;
                    str = str10;
                    i3 = i4;
                    str2 = substring;
                } else if (str11.startsWith("versioncode:")) {
                    try {
                        str2 = str9;
                        str3 = str8;
                        str4 = str7;
                        str5 = str6;
                        String str13 = str10;
                        i3 = Integer.parseInt(str11.substring("versioncode:".length()));
                        z2 = bool;
                        str = str13;
                    } catch (NumberFormatException e4) {
                        e4.printStackTrace();
                        z2 = bool;
                        str = str10;
                        i3 = i4;
                        str2 = str9;
                        str3 = str8;
                        str4 = str7;
                        str5 = str6;
                    }
                } else if (str11.startsWith("url:")) {
                    i3 = i4;
                    str2 = str9;
                    str3 = str8;
                    str4 = str7;
                    str5 = str6;
                    String substring2 = str11.substring("url:".length());
                    z2 = bool;
                    str = substring2;
                } else if (str11.startsWith("cancelable:")) {
                    if ("false".equalsIgnoreCase(str11.substring("cancelable:".length()))) {
                        z2 = false;
                        str = str10;
                        i3 = i4;
                        str2 = str9;
                        str3 = str8;
                        str4 = str7;
                        str5 = str6;
                    }
                } else if (str11.startsWith("showafteropens:")) {
                    try {
                        edit.putInt("showafteropens:", Integer.parseInt(str11.substring("showafteropens:".length())));
                        z2 = bool;
                        str = str10;
                        i3 = i4;
                        str2 = str9;
                        str3 = str8;
                        str4 = str7;
                        str5 = str6;
                    } catch (NumberFormatException e5) {
                        e5.printStackTrace();
                        z2 = bool;
                        str = str10;
                        i3 = i4;
                        str2 = str9;
                        str3 = str8;
                        str4 = str7;
                        str5 = str6;
                    }
                } else if (str11.startsWith("icon:")) {
                    str11.substring("icon:".length());
                    z2 = bool;
                    str = str10;
                    i3 = i4;
                    str2 = str9;
                    str3 = str8;
                    str4 = str7;
                    str5 = str6;
                }
                i5++;
                str6 = str5;
                str7 = str4;
                str8 = str3;
                str9 = str2;
                i4 = i3;
                str10 = str;
                bool = z2;
            }
            z2 = bool;
            str = str10;
            i3 = i4;
            str2 = str9;
            str3 = str8;
            str4 = str7;
            str5 = str6;
            i5++;
            str6 = str5;
            str7 = str4;
            str8 = str3;
            str9 = str2;
            i4 = i3;
            str10 = str;
            bool = z2;
        }
        edit.commit();
        if (this.I >= 3) {
            String locale = Locale.getDefault().toString();
            if (str9 == null || str9.length() == 0 || str9.equals(locale)) {
                dialogData dialogdata = new dialogData();
                dialogdata.e = bool.booleanValue();
                dialogdata.b = str7;
                dialogdata.c = str8;
                dialogdata.d = str10;
                dialogdata.a = str6;
                if ((i2 & 4) == 0 || str8 == null || !e("com.android.vending")) {
                    if ((i2 & 8) == 0 || str10 == null || c(str10)) {
                        if ((i2 & 32) != 0) {
                            if (str8 != null && e("com.android.vending") && str10 != null && !a(str8) && !e(str8)) {
                                if (this.J) {
                                    edit.putLong("timeout:", 0);
                                    edit.commit();
                                    return;
                                }
                                a(dialogdata);
                                a(str8, true);
                            }
                        } else if ((i2 & 16) != 0 && e("com.android.vending") && !i()) {
                            if (this.J) {
                                edit.putLong("timeout:", 0);
                                edit.commit();
                                return;
                            }
                            s();
                            a(true);
                        }
                    } else if (this.J) {
                        edit.putLong("timeout:", 0);
                        edit.commit();
                    } else {
                        b(dialogdata);
                        d(str10);
                    }
                } else if (this.J) {
                    edit.putLong("timeout:", 0);
                    edit.commit();
                } else if (!str8.equals(getPackageName())) {
                    if (!e(str8) && !a(str8)) {
                        c(dialogdata);
                        a(str8, true);
                    }
                } else if (i4 > 0) {
                    try {
                        if (getPackageManager().getPackageInfo(getPackageName(), 0).versionCode < i4) {
                            f(getPackageName());
                        }
                    } catch (Exception e6) {
                    }
                }
            }
        }
    }

    private void a(dialogData dialogdata) {
        a(dialogdata, new Intent("android.intent.action.VIEW", Uri.parse(dialogdata.d)));
    }

    private void b(dialogData dialogdata) {
        if (dialogdata.d != null) {
            Intent intent = new Intent(this, AppsList.class);
            intent.putExtra("url", dialogdata.d);
            a(dialogdata, intent);
        }
    }

    private void c(dialogData dialogdata) {
        String str = dialogdata.c;
        if (str != null) {
            a(dialogdata, new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + str)));
        }
    }

    private void a(dialogData dialogdata, final Intent intent) {
        if (dialogdata.a != null && dialogdata.b != null && intent != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(dialogdata.a).setMessage(dialogdata.b).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    try {
                        MainActivity.this.startActivity(intent);
                    } catch (Exception e) {
                    }
                }
            });
            if (dialogdata.e) {
                builder.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
            }
            if (dialogdata.f != null) {
                builder.setIcon(dialogdata.f);
            }
            builder.show();
        }
    }

    private void b(boolean z2) {
        new AlertDialog.Builder(this).setTitle("Support Developer").setMessage("Do you want to buy the ad remover from our partner to support the developer?").setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    MainActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:com.gamevisa8.drunkpro0")));
                } catch (Exception e) {
                }
            }
        }).setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).show();
        if (z2) {
            try {
                SharedPreferences.Editor edit = getSharedPreferences(getPackageName(), 0).edit();
                edit.putBoolean("localshowedpro", true);
                edit.commit();
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void f(final String str) {
        new AlertDialog.Builder(this).setTitle("Update Available").setMessage("New version available,do you want to update?").setPositiveButton("Update", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                try {
                    MainActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + str)));
                } catch (Exception e) {
                }
            }
        }).setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void s() {
        new AlertDialog.Builder(this).setTitle("More Free Games").setMessage("Do you want to get more amazing free games?").setPositiveButton("Show Me", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                MainActivity.this.u();
            }
        }).setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).show();
    }

    /* access modifiers changed from: package-private */
    public String m() {
        return mPlayer.a();
    }

    /* access modifiers changed from: private */
    public void t() {
        new AlertDialog.Builder(this).setTitle("Do you like this game?").setMessage("If you like this game, Please rate us 5 star, thanks for your support!!").setPositiveButton("LOVE this game!", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                MainActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + MainActivity.this.m())));
            }
        }).setNegativeButton("Not so much fun", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).show();
        try {
            SharedPreferences.Editor edit = getSharedPreferences(getPackageName(), 0).edit();
            edit.putBoolean("localshowedratedialog", true);
            edit.commit();
        } catch (Exception e2) {
        }
    }

    public void n() {
        if (this.V != null) {
            this.V.sendEmptyMessage(65546);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.O != 0) {
            if (motionEvent.getAction() == 0) {
                this.T = (int) motionEvent.getX();
                this.U = (int) motionEvent.getY();
                if (this.T >= this.L) {
                    this.T = this.L - 1;
                }
                if (this.U >= this.M) {
                    this.U = this.M - 1;
                }
                mPlayer.a(this.T, this.U, 0);
                return true;
            } else if (motionEvent.getAction() == 1) {
                this.T = (int) motionEvent.getX();
                this.U = (int) motionEvent.getY();
                if (this.T >= this.L) {
                    this.T = this.L - 1;
                }
                if (this.U >= this.M) {
                    this.U = this.M - 1;
                }
                mPlayer.a(this.T, this.U, 1);
                return true;
            } else if (motionEvent.getAction() == 2) {
                this.T = (int) motionEvent.getX();
                this.U = (int) motionEvent.getY();
                if (this.T >= this.L) {
                    this.T = this.L - 1;
                }
                if (this.U >= this.M) {
                    this.U = this.M - 1;
                }
                mPlayer.a(this.T, this.U, 2);
                return true;
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 == 4) {
            AlertDialog create = new AlertDialog.Builder(this).setTitle("   quit game?  ").setPositiveButton("More Games", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    MainActivity.this.u();
                }
            }).setNeutralButton("Exit", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    Process.killProcess(Process.myPid());
                }
            }).setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            }).create();
            create.setOnDismissListener(new DialogInterface.OnDismissListener() {
                public void onDismiss(DialogInterface dialogInterface) {
                    if (MainActivity.mPlayer != null && !MainActivity.this.d) {
                        MainActivity.mPlayer.c();
                    }
                }
            });
            create.show();
            mPlayer.b();
            return false;
        } else if (i2 == 82) {
            return super.onKeyDown(i2, keyEvent);
        } else {
            return super.onKeyDown(i2, keyEvent);
        }
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        return super.onKeyUp(i2, keyEvent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 2, 0, getString(R.string.string_id_feedback));
        menu.add(0, 3, 1, getString(R.string.string_id_submit));
        menu.add(0, 4, 2, getString(R.string.string_id_quit));
        menu.add(0, 7, 3, getString(R.string.string_id_featured));
        return true;
    }

    public void o() {
        try {
            Intent intent = new Intent();
            String string = getSharedPreferences(getPackageName(), 0).getString("highScore", null);
            intent.setClass(this, Ranking.class);
            intent.putExtra("mGameID", MYGAMEID);
            intent.putExtra("mThisGameHighScore", string);
            startActivity(intent);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public String a(Context context) {
        if (context == null || context.getResources() == null || context.getResources().getConfiguration() == null || context.getResources().getConfiguration().locale == null) {
            return null;
        }
        return context.getResources().getConfiguration().locale.getISO3Country();
    }

    /* access modifiers changed from: private */
    public void u() {
        Intent intent = new Intent();
        intent.putExtra("url", String.valueOf(mFeaturedURL) + MYGAMEID + "&i=" + mUserID + "&model=" + Build.MODEL + "&l=" + a((Context) this));
        intent.setClass(this, AppsList.class);
        startActivity(intent);
        SharedPreferences.Editor edit = getSharedPreferences(getPackageName(), 0).edit();
        edit.putBoolean("HadShowedFeatured", true);
        edit.commit();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 2:
                try {
                    Intent intent = new Intent("android.intent.action.SENDTO");
                    intent.setData(Uri.parse("mailto:ogregames@hotmail.com"));
                    intent.putExtra("android.intent.extra.SUBJECT", String.valueOf(getString(R.string.app_name)) + "+" + getCurrentVersionName(this) + "+" + Build.MODEL + "+" + getSDKVersionNumber());
                    startActivity(intent);
                    break;
                } catch (Exception e2) {
                    break;
                }
            case 3:
                o();
                break;
            case 4:
                Process.killProcess(Process.myPid());
                break;
            case 6:
                b(false);
                break;
            case 7:
                u();
                break;
        }
        return false;
    }

    public static int getSDKVersionNumber() {
        try {
            return Integer.valueOf(Build.VERSION.SDK).intValue();
        } catch (NumberFormatException e2) {
            return 0;
        }
    }

    public static String getCurrentVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e2) {
            return "";
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    public void g(String str) {
        if (str != null) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://api.itunes-game.com/aftdmedia-game-api-v2-dyn.php?action=geturl&extra=aftdmedia&gameid=" + str).openConnection();
                httpURLConnection.setDoInput(true);
                httpURLConnection.setConnectTimeout(10000);
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setRequestProperty("accept", "*/*");
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), "utf-8"));
                try {
                    ArrayList arrayList = new ArrayList();
                    for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
                        arrayList.add(readLine);
                    }
                    if (arrayList.size() > 0) {
                        this.V.sendMessage(this.V.obtainMessage(65541, arrayList));
                    }
                    httpURLConnection.disconnect();
                } catch (Exception e2) {
                    e2.printStackTrace();
                    httpURLConnection.disconnect();
                } catch (Throwable th) {
                    httpURLConnection.disconnect();
                    throw th;
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    private void v() {
        try {
            SharedPreferences sharedPreferences = getSharedPreferences(getPackageName(), 0);
            long j2 = sharedPreferences.getLong("timeout:", 0);
            if (j2 > 0) {
                long j3 = sharedPreferences.getLong("LastCheckConfigTime", 0);
                if (j3 > 0 && j2 + j3 > System.currentTimeMillis()) {
                    return;
                }
            }
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putLong("LastCheckConfigTime", System.currentTimeMillis());
            edit.commit();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        new Thread(null, new Runnable() {
            public void run() {
                try {
                    MainActivity.this.g(MainActivity.MYGAMEID);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, "DoJob").start();
    }
}
