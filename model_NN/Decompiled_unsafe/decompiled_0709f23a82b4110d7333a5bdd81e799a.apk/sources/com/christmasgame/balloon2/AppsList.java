package com.christmasgame.balloon2;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class AppsList extends Activity {
    private String mFeaturedAppsURL;
    private WebView mFeaturedAppsWebView;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(2);
        setContentView((int) R.layout.featured);
        this.mFeaturedAppsURL = getIntent().getStringExtra("url");
        if (this.mFeaturedAppsURL == null) {
            finish();
        }
        this.mFeaturedAppsWebView = (WebView) findViewById(R.id.featured_apps_webview);
        this.mFeaturedAppsWebView.getSettings().setJavaScriptEnabled(true);
        this.mFeaturedAppsWebView.setScrollBarStyle(0);
        this.mFeaturedAppsWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                webView.loadUrl(str);
                return true;
            }

            public void onPageFinished(WebView webView, String str) {
                super.onPageFinished(webView, str);
            }
        });
        this.mFeaturedAppsWebView.setWebChromeClient(new WebChromeClient() {
            public void onReceivedTitle(WebView webView, String str) {
                AppsList.this.setTitle(str);
            }

            public void onProgressChanged(WebView webView, int i) {
                AppsList.this.getWindow().setFeatureInt(2, i * 100);
                super.onProgressChanged(webView, i);
            }
        });
        this.mFeaturedAppsWebView.addJavascriptInterface(new Object() {
            public void gotoMarketBySearch(String str) {
                AppsList.this.searchGoogleMarket("pname:" + str);
            }

            public void gotoMarket(String str) {
                AppsList.this.openGoogleMarketById(str);
            }

            public void gotoMarketByPub(String str) {
                AppsList.this.searchGoogleMarket("pub:%22" + str + "%22");
            }

            public void getURL(String str) {
                AppsList.this.openURL(str);
            }

            public boolean isAppInstalled(String str) {
                return AppsList.this.checkAppInstalled(str);
            }

            public int getSystemVersionCode() {
                return Integer.parseInt(Build.VERSION.SDK);
            }
        }, "android");
        this.mFeaturedAppsWebView.loadUrl(this.mFeaturedAppsURL);
    }

    /* access modifiers changed from: private */
    public void openURL(String str) {
        try {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void searchGoogleMarket(String str) {
        if (isGoogleMarketInstalled()) {
            openURL("market://search?q=" + str);
        } else {
            openURL("http://market.android.com/search?q=" + str);
        }
    }

    /* access modifiers changed from: private */
    public void openGoogleMarketById(String str) {
        if (isGoogleMarketInstalled()) {
            openURL("market://details?id=" + str);
        } else {
            openURL("http://market.android.com/details?id=" + str);
        }
    }

    /* access modifiers changed from: private */
    public boolean checkAppInstalled(String str) {
        try {
            getPackageManager().getPackageInfo(str, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    private boolean isGoogleMarketInstalled() {
        return checkAppInstalled("com.android.vending");
    }
}
