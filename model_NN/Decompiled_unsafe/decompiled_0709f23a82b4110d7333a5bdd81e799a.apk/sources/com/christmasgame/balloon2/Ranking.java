package com.christmasgame.balloon2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import com.christmasgame.common.GamePlayer;
import com.mobclick.android.MobclickAgent;
import com.mobclix.android.sdk.MobclixAdView;
import com.mobclix.android.sdk.MobclixAdViewListener;
import com.mobclix.android.sdk.MobclixMMABannerXLAdView;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.TimeZone;

public class Ranking extends Activity {
    boolean a = false;
    double b = 0.0d;
    public MobclixMMABannerXLAdView c;
    private int d = 1;
    private boolean e = false;
    /* access modifiers changed from: private */
    public TextView f;
    /* access modifiers changed from: private */
    public TextView g;
    private String h;
    private String i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public ProgressDialog l = null;
    private int m = 0;
    private Context n;
    /* access modifiers changed from: private */
    public LinearLayout o;
    private Double p = Double.valueOf(0.0d);
    /* access modifiers changed from: private */
    public Double q = Double.valueOf(0.0d);
    private String r;
    private GamePlayer s;
    private Menu t;
    /* access modifiers changed from: private */
    public RankPage u = null;
    private final Handler v = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 8:
                    try {
                        if (Ranking.this.o != null) {
                            Ranking.this.o.setVisibility(0);
                            return;
                        }
                        return;
                    } catch (Exception e) {
                        return;
                    }
                case 9:
                default:
                    return;
            }
        }
    };

    class RankItem {
        public long a;
        public String b;
        public String c;
        public String d;

        public RankItem(long j, String str, String str2, String str3) {
            this.a = j;
            this.b = str;
            this.c = str2;
            this.d = str3;
        }
    }

    class RankPage {
        ArrayList<RankItem> a;
        private long c = 0;
        private long d = 0;

        public RankPage(long j) {
            this.c = j;
            this.a = new ArrayList<>();
        }

        public void a(RankItem rankItem) {
            if (rankItem != null) {
                this.a.add(rankItem);
                this.d = (((long) this.a.size()) + this.c) - 1;
            }
        }

        public RankItem a(int i) {
            try {
                return this.a.get(i);
            } catch (Exception e) {
                return null;
            }
        }

        public int a() {
            return this.a.size();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        MobclickAgent.onError(this);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.ranking);
        this.n = this;
        Intent intent = getIntent();
        this.i = intent.getStringExtra("mThisGameHighScore");
        this.k = intent.getStringExtra("mGameID");
        try {
            if (this.i != null) {
                this.b = Double.parseDouble(this.i);
            }
        } catch (Exception e2) {
        }
        this.r = MainActivity.mLeaderBoardServer;
        this.s = MainActivity.mPlayer;
        this.h = MainActivity.mUserID;
        MyRankTask myRankTask = new MyRankTask(this, null);
        this.l = ProgressDialog.show(this.n, "Please Wait", "Be patient! Retrieving data ...");
        myRankTask.execute(new String[0][]);
        this.f = (TextView) findViewById(R.id.TextViewTitle);
        this.g = (TextView) findViewById(R.id.bestRecord);
        this.g.setText("");
        ((Button) findViewById(R.id.Button01)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    Intent intent = new Intent();
                    intent.setClass(Ranking.this, EditProfile.class);
                    intent.putExtra("nick", Ranking.this.j);
                    intent.putExtra("mGameID", Ranking.this.k);
                    Ranking.this.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        try {
            this.c = new MobclixMMABannerXLAdView(this);
            this.o = (LinearLayout) findViewById(R.id.LinearLayoutAd);
            this.c.a(new MobclixAdViewListener() {
                public String a() {
                    return "";
                }

                public boolean a(MobclixAdView mobclixAdView, int i) {
                    return true;
                }

                public void a(MobclixAdView mobclixAdView, String str) {
                }

                public void a(MobclixAdView mobclixAdView) {
                }

                public void b(MobclixAdView mobclixAdView, int i) {
                }

                public void b(MobclixAdView mobclixAdView) {
                }

                public String b() {
                    return "";
                }
            });
            this.o.addView(this.c);
        } catch (Exception e3) {
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.l != null) {
            this.l.cancel();
        }
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (this.l != null) {
            this.l.dismiss();
        }
        return super.onKeyUp(i2, keyEvent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.t = menu;
        getMenuInflater().inflate(R.menu.rankmenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.editProfile:
                Log.d("GameRank", "editProfile");
                try {
                    Intent intent = new Intent();
                    intent.setClass(this, EditProfile.class);
                    intent.putExtra("nick", this.j);
                    intent.putExtra("mGameID", this.k);
                    startActivity(intent);
                    break;
                } catch (Exception e2) {
                    e2.printStackTrace();
                    break;
                }
            case R.id.refresh:
                Log.d("GameRank", "refresh");
                MyRankTask myRankTask = new MyRankTask(this, null);
                this.l = ProgressDialog.show(this.n, "Please Wait", "Be patient! Retrieving data ...");
                myRankTask.execute(new String[0][]);
                break;
        }
        return false;
    }

    public void onResume() {
        super.onResume();
        Log.d("GameRank", "onResume");
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        Log.d("GameRank", "onPause");
        MobclickAgent.onPause(this);
    }

    public void onRestart() {
        super.onRestart();
        Log.d("GameRank", "onRestart");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d("GameRank", "onDestroy");
    }

    /* access modifiers changed from: package-private */
    public String a() {
        if (this.j == null || this.p == null) {
            return "";
        }
        return "     " + this.j + ",your best record is " + this.p;
    }

    private class MyRankTask extends AsyncTask<String[], Integer, Integer> {
        private MyRankTask() {
        }

        /* synthetic */ MyRankTask(Ranking ranking, MyRankTask myRankTask) {
            this();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Integer doInBackground(String[]... strArr) {
            Ranking.this.b();
            return 0;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Integer num) {
            Ranking.this.c();
            Ranking.this.f.setText("Leaderboard");
            Ranking.this.l.dismiss();
            Ranking.this.g.setText(Ranking.this.a());
            if (!Ranking.this.a && Ranking.this.u != null) {
                Toast.makeText(Ranking.this, "you need at least " + Ranking.this.q + " to show your socre on the leaderboard!", 1).show();
            }
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        String readLine;
        String[] split;
        if (this.s != null) {
            this.u = null;
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(a(Double.valueOf(this.b))).openConnection();
                httpURLConnection.setDoInput(true);
                httpURLConnection.setConnectTimeout(10000);
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setRequestProperty("accept", "*/*");
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                try {
                    String readLine2 = bufferedReader.readLine();
                    if (readLine2 != null) {
                        this.j = EditProfile.unescape(readLine2);
                        String readLine3 = bufferedReader.readLine();
                        if (readLine3 != null) {
                            this.p = Double.valueOf(Double.parseDouble(readLine3));
                            String readLine4 = bufferedReader.readLine();
                            if (readLine4 != null) {
                                int parseInt = Integer.parseInt(readLine4);
                                String readLine5 = bufferedReader.readLine();
                                if (readLine5 != null) {
                                    this.q = Double.valueOf(Double.parseDouble(readLine5));
                                    String readLine6 = bufferedReader.readLine();
                                    if (readLine6 != null && readLine6.equals("start") && (readLine = bufferedReader.readLine()) != null && (split = EditProfile.unescape(readLine).split("\t")) != null && split.length >= 4) {
                                        int parseInt2 = Integer.parseInt(split[0]);
                                        if (parseInt == 1) {
                                            this.a = true;
                                        }
                                        RankPage rankPage = new RankPage((long) parseInt2);
                                        rankPage.a(new RankItem((long) parseInt2, split[1], split[split.length - 2], split[split.length - 1]));
                                        while (true) {
                                            String readLine7 = bufferedReader.readLine();
                                            if (readLine7 == null) {
                                                break;
                                            }
                                            String[] split2 = EditProfile.unescape(readLine7).split("\t");
                                            if (split2 != null && split2.length == 4) {
                                                rankPage.a(new RankItem((long) Integer.parseInt(split2[0]), split2[1], split2[split2.length - 2], split2[split2.length - 1]));
                                            }
                                        }
                                        this.u = rankPage;
                                        httpURLConnection.disconnect();
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        int i2;
        try {
            if (this.u != null) {
                TableLayout tableLayout = (TableLayout) findViewById(R.id.TableLayoutRankList);
                tableLayout.removeAllViewsInLayout();
                tableLayout.setStretchAllColumns(true);
                TableRow tableRow = new TableRow(this);
                TextView textView = new TextView(this);
                textView.setTextSize((float) 15);
                textView.setTextColor(-16777216);
                textView.setText("   Rank");
                tableRow.addView(textView);
                TextView textView2 = new TextView(this);
                textView2.setTextSize((float) 15);
                textView2.setTextColor(-16777216);
                textView2.setText("Nick");
                tableRow.addView(textView2);
                TextView textView3 = new TextView(this);
                textView3.setTextSize((float) 15);
                textView3.setTextColor(-16777216);
                textView3.setText("Score");
                tableRow.addView(textView3);
                TextView textView4 = new TextView(this);
                textView4.setTextSize((float) 15);
                textView4.setTextColor(-16777216);
                textView4.setText("   Date");
                tableRow.addView(textView4);
                tableLayout.addView(tableRow, new TableLayout.LayoutParams(-2, -1));
                int i3 = 0;
                while (true) {
                    int i4 = i3;
                    if (i4 < this.u.a()) {
                        RankItem a2 = this.u.a(i4);
                        if (a2 != null) {
                            if (a2.b.equals(this.j)) {
                                i2 = -3407872;
                            } else {
                                i2 = -16777216;
                            }
                            TableRow tableRow2 = new TableRow(this);
                            TextView textView5 = new TextView(this);
                            textView5.setTextSize((float) 15);
                            textView5.setTextColor(i2);
                            textView5.setText("     " + a2.a);
                            tableRow2.addView(textView5);
                            TextView textView6 = new TextView(this);
                            textView6.setTextSize((float) 15);
                            textView6.setTextColor(i2);
                            textView6.setText(a2.b);
                            tableRow2.addView(textView6);
                            TextView textView7 = new TextView(this);
                            textView7.setTextSize((float) 15);
                            textView7.setTextColor(i2);
                            textView7.setText(a2.c);
                            tableRow2.addView(textView7);
                            TextView textView8 = new TextView(this);
                            textView8.setTextSize((float) 15);
                            textView8.setTextColor(i2);
                            textView8.setText(a2.d);
                            tableRow2.addView(textView8);
                            tableLayout.addView(tableRow2, new TableLayout.LayoutParams(-2, -1));
                        }
                        i3 = i4 + 1;
                    } else {
                        return;
                    }
                }
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public String a(Double d2) {
        String str = "a=getScore&i=" + this.h + "&gi=" + this.k + "&s=" + d2 + "&tz=" + (TimeZone.getDefault().getRawOffset() / 3600000) + "&e=";
        return String.valueOf(this.r) + "?" + str + this.s.a(str);
    }
}
