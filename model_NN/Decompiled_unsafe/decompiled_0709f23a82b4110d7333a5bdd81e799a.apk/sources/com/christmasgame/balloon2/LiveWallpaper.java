package com.christmasgame.balloon2;

import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.WindowManager;

public class LiveWallpaper extends WallpaperService {
    public static final String SHARED_PREFS_NAME = "livewallpapersettings";

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public WallpaperService.Engine onCreateEngine() {
        return new TestPatternEngine();
    }

    class TestPatternEngine extends WallpaperService.Engine implements SharedPreferences.OnSharedPreferenceChangeListener {
        GradientDrawable a;
        private final Handler c = new Handler();
        private float d = -1.0f;
        private float e = -1.0f;
        private final Paint f = new Paint();
        private final Runnable g = new Runnable() {
            public void run() {
                TestPatternEngine.this.a();
            }
        };
        private boolean h;
        private SharedPreferences i;
        private Rect j;
        private Rect[] k;
        private int[] l;
        private int m;
        private Rect n;
        private boolean o = false;
        private int p = 0;
        private boolean q = true;
        private String r = "smpte";

        TestPatternEngine() {
            super(LiveWallpaper.this);
            Paint paint = this.f;
            paint.setColor(-1);
            paint.setAntiAlias(true);
            paint.setStrokeWidth(2.0f);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setStyle(Paint.Style.STROKE);
            this.i = LiveWallpaper.this.getSharedPreferences(LiveWallpaper.SHARED_PREFS_NAME, 0);
            this.i.registerOnSharedPreferenceChangeListener(this);
            onSharedPreferenceChanged(this.i, null);
        }

        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
            this.r = sharedPreferences.getString("livewallpaper_testpattern", "smpte");
            this.q = sharedPreferences.getBoolean("livewallpaper_movement", true);
            c();
        }

        private void c() {
            this.l = LiveWallpaper.this.getResources().getIntArray(LiveWallpaper.this.getResources().getIdentifier(String.valueOf(this.r) + "colors", "array", LiveWallpaper.this.getPackageName()));
            this.m = this.l.length;
            this.k = new Rect[this.m];
            System.out.println("mRectCount " + this.m);
            b();
        }

        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);
            setTouchEventsEnabled(true);
        }

        public void onDestroy() {
            super.onDestroy();
            this.c.removeCallbacks(this.g);
        }

        public void onVisibilityChanged(boolean z) {
            this.h = z;
            if (z) {
                a();
            } else {
                this.c.removeCallbacks(this.g);
            }
        }

        public void onSurfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
            super.onSurfaceChanged(surfaceHolder, i2, i3, i4);
            b();
            a();
        }

        public void onSurfaceCreated(SurfaceHolder surfaceHolder) {
            super.onSurfaceCreated(surfaceHolder);
        }

        public void onSurfaceDestroyed(SurfaceHolder surfaceHolder) {
            super.onSurfaceDestroyed(surfaceHolder);
            this.h = false;
            this.c.removeCallbacks(this.g);
        }

        public void onOffsetsChanged(float f2, float f3, float f4, float f5, int i2, int i3) {
            a();
        }

        public void onTouchEvent(MotionEvent motionEvent) {
            if (motionEvent.getAction() == 2) {
                this.d = motionEvent.getX();
                this.e = motionEvent.getY();
            } else {
                this.d = -1.0f;
                this.e = -1.0f;
            }
            super.onTouchEvent(motionEvent);
        }

        /* access modifiers changed from: package-private */
        public void a() {
            SurfaceHolder surfaceHolder = getSurfaceHolder();
            Canvas canvas = null;
            try {
                canvas = surfaceHolder.lockCanvas();
                if (canvas != null) {
                    a(canvas);
                    b(canvas);
                }
                this.c.removeCallbacks(this.g);
                if (this.h) {
                    this.c.postDelayed(this.g, 40);
                }
            } finally {
                if (canvas != null) {
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(Canvas canvas) {
            int i2 = 0;
            canvas.save();
            canvas.drawColor(-16777216);
            Paint paint = new Paint();
            if (this.q) {
                this.p++;
                if (this.o) {
                    if (this.p > this.j.right) {
                        this.p = 0;
                    }
                    while (true) {
                        int i3 = i2;
                        if (i3 >= this.m) {
                            break;
                        }
                        paint.setColor(this.l[i3]);
                        int i4 = this.k[i3].right + this.p;
                        int i5 = this.k[i3].left + this.p;
                        if (i4 > this.j.right) {
                            canvas.drawRect((float) (i5 - this.j.right), (float) this.k[i3].top, (float) (i4 - this.j.right), (float) this.k[i3].bottom, paint);
                        }
                        if (i5 < this.j.right) {
                            canvas.drawRect((float) i5, (float) this.k[i3].top, (float) i4, (float) this.k[i3].bottom, paint);
                        }
                        i2 = i3 + 1;
                    }
                    if (this.r.compareToIgnoreCase("smpte") == 0) {
                        int i6 = this.n.right + this.p;
                        int i7 = this.n.left + this.p;
                        if (i6 > this.j.right) {
                            this.a.setBounds(i7 - this.j.right, this.n.top, i6 - this.j.right, this.n.bottom);
                            this.a.draw(canvas);
                        }
                        if (i7 < this.j.right) {
                            this.a.setBounds(i7, this.n.top, i6, this.n.bottom);
                            this.a.draw(canvas);
                        }
                    }
                } else {
                    if (this.p > this.j.bottom) {
                        this.p = 0;
                    }
                    while (true) {
                        int i8 = i2;
                        if (i8 >= this.m) {
                            break;
                        }
                        paint.setColor(this.l[i8]);
                        int i9 = this.k[i8].top + this.p;
                        int i10 = this.k[i8].bottom + this.p;
                        if (i10 > this.j.bottom) {
                            canvas.drawRect((float) this.k[i8].left, (float) (i9 - this.j.bottom), (float) this.k[i8].right, (float) (i10 - this.j.bottom), paint);
                        }
                        if (i9 < this.j.bottom) {
                            canvas.drawRect((float) this.k[i8].left, (float) i9, (float) this.k[i8].right, (float) i10, paint);
                        }
                        i2 = i8 + 1;
                    }
                    if (this.r.compareToIgnoreCase("smpte") == 0) {
                        int i11 = this.n.top + this.p;
                        int i12 = this.n.bottom + this.p;
                        if (i12 > this.j.bottom) {
                            this.a.setBounds(this.n.left, i11 - this.j.bottom, this.n.right, i12 - this.j.bottom);
                            this.a.draw(canvas);
                        }
                        if (i11 < this.j.bottom) {
                            this.a.setBounds(this.n.left, i11, this.n.right, i12);
                            this.a.draw(canvas);
                        }
                    }
                }
            } else {
                while (i2 < this.m) {
                    paint.setColor(this.l[i2]);
                    canvas.drawRect(this.k[i2], paint);
                    i2++;
                }
                if (this.r.compareToIgnoreCase("smpte") == 0) {
                    this.a.setBounds(this.n);
                    this.a.draw(canvas);
                }
            }
            canvas.restore();
        }

        /* access modifiers changed from: package-private */
        public void b(Canvas canvas) {
            if (this.d >= 0.0f && this.e >= 0.0f) {
                canvas.drawCircle(this.d, this.e, 80.0f, this.f);
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            Display defaultDisplay = ((WindowManager) LiveWallpaper.this.getSystemService("window")).getDefaultDisplay();
            defaultDisplay.getMetrics(displayMetrics);
            this.j = new Rect(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
            int orientation = defaultDisplay.getOrientation();
            if (orientation == 0 || orientation == 2) {
                this.o = false;
            } else {
                this.o = true;
            }
            System.out.println("mHorizontal " + this.o);
            System.out.println("mShape " + this.r);
            if (this.r.compareToIgnoreCase("smpte") == 0) {
                System.out.println("mShape == smpte");
                d();
            } else if (this.r.compareToIgnoreCase("bars") == 0) {
                System.out.println("mShape == bars");
                e();
            } else {
                System.out.println("mShape == ebu");
                f();
            }
        }

        private void d() {
            if (this.o) {
                int i2 = (this.j.bottom * 7) / 12;
                int i3 = (this.j.bottom * 3) / 4;
                int i4 = this.j.right / 8;
                int i5 = (this.j.right * 3) / 28;
                this.k[0] = new Rect(0, 0, i4, i2);
                for (int i6 = 1; i6 < 8; i6++) {
                    this.k[i6] = new Rect(this.k[i6 - 1].right, 0, this.k[i6 - 1].right + i5, i2);
                }
                this.k[8] = new Rect(this.k[7].right, 0, this.j.right, i2);
                for (int i7 = 0; i7 < 2; i7++) {
                    int i8 = (this.j.bottom * (i7 + 7)) / 12;
                    int i9 = (this.j.bottom * (i7 + 8)) / 12;
                    this.k[i7 + 9] = new Rect(0, i8, i4, i9);
                    this.k[i7 + 11] = new Rect(i4, i8, i5 + i4, i9);
                    this.k[i7 + 13] = new Rect((i5 * 7) + i4, i8, this.j.right, i9);
                }
                this.k[15] = new Rect(i5 + i4, i2, (i5 * 7) + i4, (this.j.bottom * 8) / 12);
                this.n = new Rect(this.k[15].left, this.k[15].bottom, this.k[15].right, (this.j.bottom * 9) / 12);
                this.a = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{-16448251, -131587});
                this.a.setBounds(this.n);
                this.k[16] = new Rect(0, i3, i4, this.j.right);
                this.k[17] = new Rect(this.k[16].right, i3, ((this.j.right * 9) / 56) + this.k[16].right, this.j.bottom);
                this.k[18] = new Rect(this.k[17].right, i3, ((this.j.right * 3) / 14) + this.k[17].right, this.j.bottom);
                this.k[19] = new Rect(this.k[18].right, i3, ((this.j.right * 45) / 448) + this.k[18].right, this.j.bottom);
                for (int i10 = 20; i10 < 25; i10++) {
                    this.k[i10] = new Rect(this.k[i10 - 1].right, i3, ((this.j.right * 15) / 448) + this.k[i10 - 1].right, this.j.right);
                }
                this.k[25] = new Rect(this.k[24].right, i3, i5 + this.k[24].right, this.j.bottom);
                this.k[26] = new Rect(this.k[25].right, i3, this.j.right, this.j.bottom);
                return;
            }
            int i11 = (this.j.right * 5) / 12;
            int i12 = this.j.right / 4;
            int i13 = this.j.bottom / 8;
            int i14 = (this.j.bottom * 3) / 28;
            this.k[0] = new Rect(i11, 0, this.j.bottom, i13);
            for (int i15 = 1; i15 < 8; i15++) {
                this.k[i15] = new Rect(i11, this.k[i15 - 1].bottom, this.j.right, this.k[i15 - 1].bottom + i14);
            }
            this.k[8] = new Rect(i11, this.k[7].bottom, this.j.right, this.j.bottom);
            for (int i16 = 0; i16 < 2; i16++) {
                int i17 = (this.j.right * (4 - i16)) / 12;
                int i18 = (this.j.right * (5 - i16)) / 12;
                this.k[i16 + 9] = new Rect(i17, 0, i18, i13);
                this.k[i16 + 11] = new Rect(i17, i13, i18, i14 + i13);
                this.k[i16 + 13] = new Rect(i17, (i14 * 7) + i13, i18, this.j.bottom);
            }
            this.k[15] = new Rect((this.j.right * 4) / 12, i14 + i13, (this.j.right * 5) / 12, (i14 * 7) + i13);
            this.n = new Rect((this.j.right * 3) / 12, this.k[15].top, this.k[15].left, this.k[15].bottom);
            this.a = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-16448251, -131587});
            this.a.setBounds(this.n);
            this.k[16] = new Rect(0, 0, i12, i13);
            this.k[17] = new Rect(0, this.k[16].bottom, i12, ((this.j.bottom * 9) / 56) + this.k[16].bottom);
            this.k[18] = new Rect(0, this.k[17].bottom, i12, ((this.j.bottom * 3) / 14) + this.k[17].bottom);
            this.k[19] = new Rect(0, this.k[18].bottom, i12, ((this.j.bottom * 45) / 448) + this.k[18].bottom);
            for (int i19 = 20; i19 < 25; i19++) {
                this.k[i19] = new Rect(0, this.k[i19 - 1].bottom, i12, ((this.j.bottom * 15) / 448) + this.k[i19 - 1].bottom);
            }
            this.k[25] = new Rect(0, this.k[24].bottom, i12, i14 + this.k[24].bottom);
            this.k[26] = new Rect(0, this.k[25].bottom, i12, this.j.bottom);
        }

        private void e() {
            if (this.o) {
                int i2 = this.j.right / 7;
                int i3 = (this.j.right * 5) / 28;
                int i4 = this.j.right / 21;
                int i5 = (this.j.bottom * 2) / 3;
                int i6 = this.j.bottom / 12;
                this.k[0] = new Rect(0, 0, i2, i5);
                for (int i7 = 1; i7 < 7; i7++) {
                    this.k[i7] = new Rect(this.k[i7 - 1].right, 0, this.k[i7 - 1].right + i2, i5);
                }
                this.k[7] = new Rect(0, this.k[0].bottom, i2, i6 + this.k[0].bottom);
                for (int i8 = 8; i8 < 14; i8++) {
                    this.k[i8] = new Rect(this.k[i8 - 1].right, this.k[7].top, this.k[i8 - 1].right + i2, this.k[7].bottom);
                }
                this.k[14] = new Rect(0, this.k[7].bottom, i3, this.j.bottom);
                for (int i9 = 15; i9 < 18; i9++) {
                    this.k[i9] = new Rect(this.k[i9 - 1].right, this.k[14].top, this.k[i9 - 1].right + i3, this.j.bottom);
                }
                this.k[18] = new Rect(this.k[17].right, this.k[17].top, this.k[17].right + i4, this.j.bottom);
                for (int i10 = 19; i10 < 21; i10++) {
                    this.k[i10] = new Rect(this.k[i10 - 1].right, this.k[14].top, this.k[i10 - 1].right + i4, this.j.bottom);
                }
                this.k[21] = new Rect(this.k[20].right, this.k[17].top, this.k[6].right, this.j.bottom);
                return;
            }
            int i11 = this.j.bottom / 7;
            int i12 = (this.j.bottom * 5) / 28;
            int i13 = this.j.bottom / 21;
            int i14 = this.j.right / 3;
            int i15 = this.j.right / 12;
            this.k[0] = new Rect(i14, 0, this.j.right, i11);
            for (int i16 = 1; i16 < 7; i16++) {
                this.k[i16] = new Rect(i14, this.k[i16 - 1].bottom, this.j.right, this.k[i16 - 1].bottom + i11);
            }
            this.k[7] = new Rect(i15 + this.k[0].left, 0, this.k[0].left, i11);
            for (int i17 = 8; i17 < 14; i17++) {
                this.k[i17] = new Rect(this.k[7].left, this.k[i17 - 1].bottom, this.k[7].right, this.k[i17 - 1].bottom + i11);
            }
            this.k[14] = new Rect(0, 0, this.k[7].right, i12);
            for (int i18 = 15; i18 < 18; i18++) {
                this.k[i18] = new Rect(0, this.k[i18 - 1].bottom, this.k[7].right, this.k[i18 - 1].bottom + i12);
            }
            this.k[18] = new Rect(0, this.k[17].bottom, this.k[7].right, this.k[17].bottom + i13);
            for (int i19 = 19; i19 < 21; i19++) {
                this.k[i19] = new Rect(0, this.k[i19 - 1].bottom, this.k[7].right, this.k[i19 - 1].bottom + i13);
            }
            this.k[21] = new Rect(0, this.k[20].bottom, this.k[7].right, this.j.bottom);
        }

        private void f() {
            int i2 = 1;
            if (this.o) {
                int i3 = this.j.right / 8;
                this.k[0] = new Rect(0, 0, i3, this.j.bottom);
                while (i2 < 8) {
                    this.k[i2] = new Rect(this.k[i2 - 1].right, 0, this.k[i2 - 1].right + i3, this.j.bottom);
                    i2++;
                }
                return;
            }
            int i4 = this.j.bottom / 8;
            this.k[0] = new Rect(0, 0, this.j.right, i4);
            while (i2 < 8) {
                this.k[i2] = new Rect(0, this.k[i2 - 1].bottom, this.j.right, this.k[i2 - 1].bottom + i4);
                i2++;
            }
        }
    }
}
