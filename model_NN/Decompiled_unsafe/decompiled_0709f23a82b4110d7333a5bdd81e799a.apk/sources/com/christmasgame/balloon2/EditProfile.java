package com.christmasgame.balloon2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.christmasgame.common.GamePlayer;
import com.mobclick.android.MobclickAgent;
import com.mobclix.android.sdk.MobclixAdView;
import com.mobclix.android.sdk.MobclixAdViewListener;
import com.mobclix.android.sdk.MobclixMMABannerXLAdView;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class EditProfile extends Activity {
    Context a;
    EditText b;
    TextView c;
    boolean d = false;
    MobclixMMABannerXLAdView e;
    private GamePlayer f;
    private String g;
    private String h;
    private String i;
    private String j;
    /* access modifiers changed from: private */
    public ProgressDialog k = null;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        MobclickAgent.onError(this);
        Intent intent = getIntent();
        this.i = intent.getStringExtra("nick");
        this.j = intent.getStringExtra("mGameID");
        this.g = MainActivity.mLeaderBoardServer;
        this.f = MainActivity.mPlayer;
        this.h = MainActivity.mUserID;
        if (this.f == null) {
            finish();
        }
        this.a = this;
        setContentView((int) R.layout.profile);
        ((Button) findViewById(R.id.ButtonOk)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                EditProfile.this.d = false;
                SetNickTask setNickTask = new SetNickTask(EditProfile.this, null);
                EditProfile.this.k = ProgressDialog.show(EditProfile.this.a, "Please Wait", "Be patient! Retrieving data ...");
                setNickTask.execute(new String[0][]);
            }
        });
        this.b = (EditText) findViewById(R.id.EditTextNick);
        this.c = (TextView) findViewById(R.id.resultString);
        this.b.setText(this.i);
        try {
            this.e = new MobclixMMABannerXLAdView(this);
            ((LinearLayout) findViewById(R.id.LinearLayoutAd2)).addView(this.e);
            this.e.a(new MobclixAdViewListener() {
                public String a() {
                    return "android";
                }

                public boolean a(MobclixAdView mobclixAdView, int i) {
                    return true;
                }

                public void a(MobclixAdView mobclixAdView, String str) {
                }

                public void a(MobclixAdView mobclixAdView) {
                }

                public void b(MobclixAdView mobclixAdView, int i) {
                    Log.d("profile", "ADFail");
                }

                public void b(MobclixAdView mobclixAdView) {
                    if (mobclixAdView != null) {
                        Log.d("profile", "ADSucess:" + mobclixAdView.getLeft());
                    }
                }

                public String b() {
                    return "";
                }
            });
        } catch (Exception e2) {
        }
    }

    private class SetNickTask extends AsyncTask<String[], Integer, Integer> {
        private SetNickTask() {
        }

        /* synthetic */ SetNickTask(EditProfile editProfile, SetNickTask setNickTask) {
            this();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Integer doInBackground(String[]... strArr) {
            try {
                EditProfile.this.b(EditProfile.this.b.getText().toString().trim());
            } catch (Exception e) {
            }
            return 0;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Integer num) {
            EditProfile.this.k.dismiss();
            if (!EditProfile.this.d) {
                EditProfile.this.c.setText(String.valueOf(EditProfile.this.b.getText().toString().trim()) + " is not available! try another one!");
                EditProfile.this.c.setTextColor(-65536);
                return;
            }
            EditProfile.this.c.setText("Congratulations, update successful!");
            EditProfile.this.c.setTextColor(-16711936);
        }
    }

    public static String escape(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.ensureCapacity(str.length() * 6);
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if (Character.isDigit(charAt) || Character.isLowerCase(charAt) || Character.isUpperCase(charAt)) {
                stringBuffer.append(charAt);
            } else if (charAt < 256) {
                stringBuffer.append("%");
                if (charAt < 16) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(Integer.toString(charAt, 16));
            } else {
                stringBuffer.append("%u");
                stringBuffer.append(Integer.toString(charAt, 16));
            }
        }
        return stringBuffer.toString();
    }

    public static String unescape(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.ensureCapacity(str.length());
        int i2 = 0;
        while (i2 < str.length()) {
            int indexOf = str.indexOf("%", i2);
            if (indexOf == i2) {
                if (str.charAt(indexOf + 1) == 'u') {
                    stringBuffer.append((char) Integer.parseInt(str.substring(indexOf + 2, indexOf + 6), 16));
                    i2 = indexOf + 6;
                } else {
                    stringBuffer.append((char) Integer.parseInt(str.substring(indexOf + 1, indexOf + 3), 16));
                    i2 = indexOf + 3;
                }
            } else if (indexOf == -1) {
                stringBuffer.append(str.substring(i2));
                i2 = str.length();
            } else {
                stringBuffer.append(str.substring(i2, indexOf));
                i2 = indexOf;
            }
        }
        return stringBuffer.toString();
    }

    public static String encode(String str) {
        try {
            char[] charArray = new String(str.getBytes("UTF8"), "ISO-8859-1").toCharArray();
            StringBuffer stringBuffer = new StringBuffer();
            for (int i2 = 0; i2 < charArray.length; i2++) {
                if ((charArray[i2] > 'z' || charArray[i2] < 'a') && !((charArray[i2] <= 'Z' && charArray[i2] >= 'A') || charArray[i2] == '-' || charArray[i2] == '_' || charArray[i2] == '.' || charArray[i2] == '!' || charArray[i2] == '~' || charArray[i2] == '*' || charArray[i2] == '\'' || charArray[i2] == '(' || charArray[i2] == ')' || charArray[i2] == ';' || charArray[i2] == '/' || charArray[i2] == '?' || charArray[i2] == ':' || charArray[i2] == '@' || charArray[i2] == '&' || charArray[i2] == '=' || charArray[i2] == '+' || charArray[i2] == '$' || charArray[i2] == ',' || charArray[i2] == '#')) {
                    stringBuffer.append("%");
                    stringBuffer.append(Integer.toHexString(charArray[i2]));
                } else {
                    stringBuffer.append(charArray[i2]);
                }
            }
            return stringBuffer.toString();
        } catch (Exception e2) {
            return "";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* access modifiers changed from: private */
    public void b(String str) {
        if (this.f != null && str != null) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(a(escape(str.replace('\\', ' ').replace(13, ' ').replace(10, ' ').replace(9, ' ')))).openConnection();
                httpURLConnection.setDoInput(true);
                httpURLConnection.setConnectTimeout(10000);
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setRequestProperty("accept", "*/*");
                httpURLConnection.getContentLength();
                httpURLConnection.getResponseCode();
                try {
                    String readLine = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream())).readLine();
                    if (readLine != null) {
                        if (readLine.equals("1")) {
                            this.d = true;
                        }
                        httpURLConnection.disconnect();
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        String str2 = "a=setNick&i=" + this.h + "&n=" + str + "&gi=" + this.j + "&e=";
        return String.valueOf(this.g) + "?" + str2 + this.f.a(str2);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }
}
