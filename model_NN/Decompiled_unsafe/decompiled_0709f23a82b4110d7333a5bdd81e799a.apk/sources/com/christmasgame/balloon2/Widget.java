package com.christmasgame.balloon2;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

public class Widget extends AppWidgetProvider {
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        try {
            a(context, appWidgetManager, iArr);
        } catch (Exception e) {
        }
    }

    static void a(Context context, AppWidgetManager appWidgetManager, int[] iArr) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), (int) R.layout.widget);
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.setClass(context, MainActivity.class);
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.addFlags(65536);
        remoteViews.setOnClickPendingIntent(R.id.widgetBtn, PendingIntent.getActivity(context, 0, intent, 65536));
        for (int updateAppWidget : iArr) {
            appWidgetManager.updateAppWidget(updateAppWidget, remoteViews);
        }
    }
}
