package com.christmasgame.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {
    static GameView a;
    public int b;
    public int c;
    public int d;
    public Bitmap e;
    private Context f;
    private boolean g = false;
    private SurfaceHolder h = getHolder();

    public GameView(Context context, int i, int i2, Bitmap bitmap, int i3) {
        super(context);
        a = this;
        this.f = context;
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.h.addCallback(this);
        this.e = bitmap;
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.d("GamePlayer", "surfaceCreated");
        surfaceHolder.setFormat(this.d);
        this.g = true;
    }

    public void a(Bitmap bitmap, int i, int i2) {
        this.b = i;
        this.c = i2;
        this.e.recycle();
        this.e = bitmap;
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        Log.d("GamePlayer", "surfaceChanged:" + i2 + " w:h " + i3);
        if (this.b == i2) {
        }
        GamePlayer.ChangeSurface(i2, i3);
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        Log.d("GamePlayer", "surfaceDestroyed");
        this.g = false;
    }

    public boolean a() {
        Canvas lockCanvas;
        if (this.f.getResources().getConfiguration().orientation != 2 || !this.g || (lockCanvas = this.h.lockCanvas()) == null) {
            return false;
        }
        lockCanvas.drawBitmap(this.e, 0.0f, 0.0f, (Paint) null);
        this.h.unlockCanvasAndPost(lockCanvas);
        return true;
    }
}
