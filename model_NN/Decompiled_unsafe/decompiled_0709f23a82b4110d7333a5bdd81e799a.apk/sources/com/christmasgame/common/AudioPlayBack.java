package com.christmasgame.common;

import android.media.AudioTrack;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class AudioPlayBack {
    public PcmPlayHandler a = null;
    /* access modifiers changed from: private */
    public AudioTrack b;
    private int c;
    /* access modifiers changed from: private */
    public GamePlayer d;
    private PcmPlayThread e = null;

    AudioPlayBack(GamePlayer gamePlayer) {
        this.d = gamePlayer;
        this.e = new PcmPlayThread(this, null);
        this.e.start();
    }

    private class PcmPlayThread extends Thread {
        private PcmPlayThread() {
        }

        /* synthetic */ PcmPlayThread(AudioPlayBack audioPlayBack, PcmPlayThread pcmPlayThread) {
            this();
        }

        public void run() {
            Looper.prepare();
            AudioPlayBack.this.a = new PcmPlayHandler();
            Looper.loop();
        }
    }

    private class PcmPlayHandler extends Handler {
        public PcmPlayHandler() {
            super(Looper.myLooper());
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    int i = message.arg2;
                    AudioPlayBack.this.b.write((short[]) message.obj, 0, i);
                    message.obj = null;
                    AudioPlayBack.this.d.onComplete();
                    return;
                case 2:
                    AudioTrack audioTrack = (AudioTrack) message.obj;
                    if (audioTrack != null) {
                        audioTrack.stop();
                        audioTrack.release();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public int a(int i, int i2, int i3, int i4) {
        int i5;
        int i6 = 3;
        if (i3 == 16) {
            i5 = 2;
        } else if (i3 != 8) {
            return 0;
        } else {
            i5 = 3;
        }
        if (i2 != 2) {
            if (i2 != 1) {
                return 0;
            }
            i6 = 2;
        }
        try {
            this.c = i2;
            this.b = new AudioTrack(3, i, i6, i5, AudioTrack.getMinBufferSize(i, i6, i5), 1);
            this.b.play();
            return 1;
        } catch (Exception e2) {
            return 0;
        }
    }

    public int a(short[] sArr, int i) {
        if (sArr == null || this.b == null) {
            return 0;
        }
        if (this.a == null) {
            return 1;
        }
        this.a.sendMessage(this.a.obtainMessage(1, 0, i, sArr));
        return 1;
    }

    public void a() {
        if (this.a != null) {
            this.a.removeMessages(1);
            this.a.sendMessage(this.a.obtainMessage(2, this.b));
        }
    }
}
