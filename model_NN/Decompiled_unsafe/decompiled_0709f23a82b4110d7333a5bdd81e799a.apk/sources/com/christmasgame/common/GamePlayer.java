package com.christmasgame.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.christmasgame.balloon2.MainActivity;
import java.nio.ByteBuffer;
import java.util.UUID;

public class GamePlayer {
    static GamePlayer a;
    public GamerHandler b = null;
    public boolean c;
    public boolean d = false;
    public boolean e = false;
    String f = null;
    private Context g;
    /* access modifiers changed from: private */
    public GameView h;
    private AudioPlayBack i;
    private GamerThread j;
    /* access modifiers changed from: private */
    public int k;

    /* access modifiers changed from: private */
    public native int ChangeSurface(Bitmap bitmap, int i2, int i3);

    /* access modifiers changed from: private */
    public native int GamePlayerControl(int i2, int i3, int i4);

    private native int HandleEvent(int i2, int i3, int i4, int i5, int i6, String str);

    /* access modifiers changed from: private */
    public native int HandleTouchEvent(int i2, int i3, int i4, int i5);

    private native int OpenGame(String str, String str2, ByteBuffer byteBuffer, int i2, int i3, int i4);

    public native int DestroyPlayer();

    public native long GetStrURL(String str);

    public native int MsgControl(String str, String str2, String str3);

    public native void onComplete();

    private static final class MouseEvent {
        int a;
        int b;
        int c;

        MouseEvent(int i, int i2, int i3) {
            this.a = i;
            this.b = i2;
            this.c = i3;
        }
    }

    public GamePlayer(Context context, GameView gameView) {
        a = this;
        this.i = new AudioPlayBack(this);
        this.g = context;
        this.h = gameView;
        this.k = 0;
        this.c = true;
        this.j = new GamerThread(this, null);
        this.j.start();
        this.f = this.g.getPackageName();
        while (this.b == null) {
            try {
                Thread.sleep(1);
            } catch (Exception e2) {
                return;
            }
        }
    }

    public String a() {
        return this.f;
    }

    public int a(ByteBuffer byteBuffer, int i2) {
        String path = this.g.getFilesDir().getPath();
        Log.d("GamePlayer", "dataPath:" + path);
        this.k = OpenGame(path, this.f, byteBuffer, i2, this.h.b, this.h.c);
        return this.k;
    }

    public void a(int i2, int i3, int i4) {
        this.b.sendMessage(this.b.obtainMessage(1, new MouseEvent(i2, i3, i4)));
    }

    public static void ChangeSurface(int i2, int i3) {
        if (a != null && a.b != null && i2 > 0 && i3 > 0) {
            a.b.sendMessage(a.b.obtainMessage(4, i2, i3));
        }
    }

    private class GamerHandler extends Handler {
        public GamerHandler() {
            super(Looper.myLooper());
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    MouseEvent mouseEvent = (MouseEvent) message.obj;
                    if (mouseEvent != null) {
                        int unused = GamePlayer.this.HandleTouchEvent(GamePlayer.this.k, mouseEvent.a, mouseEvent.b, mouseEvent.c);
                        return;
                    }
                    return;
                case 2:
                    int unused2 = GamePlayer.this.GamePlayerControl(message.arg1, 0, 0);
                    if (message.arg2 == 1) {
                        GamePlayer.this.b.sendMessage(GamePlayer.this.b.obtainMessage(2, 1, 1));
                        return;
                    } else if (message.arg2 == 2) {
                        removeMessages(2);
                        GamePlayer.this.b.sendMessage(GamePlayer.this.b.obtainMessage(2, 1, 1));
                        return;
                    } else {
                        removeMessages(2);
                        return;
                    }
                case 3:
                    if (message.arg1 == 0) {
                        Log.d("GamePlayer", "msg res:" + GamePlayer.this.MsgControl("ScoreObject", "SetScore", new StringBuilder(String.valueOf(message.arg2)).toString()));
                        return;
                    }
                    return;
                case 4:
                    int i = message.arg1;
                    int i2 = message.arg2;
                    Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.RGB_565);
                    if (GamePlayer.this.ChangeSurface(createBitmap, i, i2) > 0) {
                        GamePlayer.this.h.a(createBitmap, i, i2);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private class GamerThread extends Thread {
        private GamerThread() {
        }

        /* synthetic */ GamerThread(GamePlayer gamePlayer, GamerThread gamerThread) {
            this();
        }

        public void run() {
            Looper.prepare();
            GamePlayer.this.b = new GamerHandler();
            Looper.loop();
        }
    }

    public void b() {
        this.b.sendMessage(this.b.obtainMessage(2, 6, 0));
    }

    public void c() {
        this.b.sendMessage(this.b.obtainMessage(2, 7, 2));
    }

    public static Bitmap GetSurfaceBitmap() {
        if (a == null) {
            return null;
        }
        return a.h.e;
    }

    public static int OpenAudio(int i2, int i3, int i4, int i5) {
        if (a == null || !a.c) {
            return 0;
        }
        return a.i.a(i2, i3, i4, i5);
    }

    public static int PlayPCM(short[] sArr, int i2) {
        if (a == null || !a.c || a.i == null) {
            return 0;
        }
        return a.i.a(sArr, i2);
    }

    public static void DestroyAudio() {
        if (a != null && a.i != null) {
            a.i.a();
        }
    }

    public static void Update() {
        if (a != null && a.h != null) {
            a.h.a();
        }
    }

    public static String GetIMSI() {
        try {
            if (a == null) {
                return null;
            }
            return ((TelephonyManager) a.g.getSystemService("phone")).getSubscriberId();
        } catch (Exception e2) {
            return null;
        }
    }

    public static String GetIMEI() {
        try {
            if (a == null) {
                return null;
            }
            return ((TelephonyManager) a.g.getSystemService("phone")).getDeviceId();
        } catch (Exception e2) {
            return null;
        }
    }

    public static String getAndroidId() {
        try {
            if (a == null) {
                return null;
            }
            return Settings.Secure.getString(a.g.getContentResolver(), "android_id");
        } catch (Exception e2) {
            return null;
        }
    }

    public static String getUniqueId() {
        try {
            String str = GetIMEI();
            String str2 = Settings.Secure.getString(a.g.getContentResolver(), "android_id");
            return new UUID((((long) str2.hashCode()) << 32) | ((long) str2.hashCode()), ((long) str.hashCode()) | (((long) str.hashCode()) << 32)).toString();
        } catch (Exception e2) {
            return null;
        }
    }

    public static String getSimSerialNumber() {
        try {
            if (a == null) {
                return null;
            }
            return ((TelephonyManager) a.g.getSystemService("phone")).getSimSerialNumber();
        } catch (Exception e2) {
            return null;
        }
    }

    public static String getMACAddr() {
        try {
            if (a == null) {
                return null;
            }
            return ((WifiManager) a.g.getSystemService("wifi")).getConnectionInfo().getMacAddress();
        } catch (Exception e2) {
            return null;
        }
    }

    public static void testGetUserID() {
        Log.d("GamePlayer", "IMSI:" + GetIMSI());
        Log.d("GamePlayer", "IMEI:" + GetIMEI());
        Log.d("GamePlayer", "getAndroidId:" + getAndroidId());
        Log.d("GamePlayer", "getSimSerialNumber:" + getSimSerialNumber());
        Log.d("GamePlayer", "getMACAddr:" + getMACAddr());
    }

    public static String NativeGetStr(int i2, String str) {
        if (a == null) {
            return null;
        }
        try {
            SharedPreferences sharedPreferences = a.g.getSharedPreferences(a.g.getPackageName(), 0);
            if (i2 == 0) {
                return sharedPreferences.getString("highScore", null);
            }
            if (i2 == 1) {
                Log.d("GamePlayer", "saveScore:" + str);
                if (str != null) {
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("highScore", str);
                    edit.commit();
                }
                return null;
            } else if (i2 == 2) {
                ((MainActivity) a.g).a();
                return null;
            } else if (i2 == 3) {
                ((MainActivity) a.g).b();
                return null;
            } else {
                if (i2 == 4) {
                    a.e = true;
                    ((MainActivity) a.g).c();
                } else if (i2 == 5) {
                    a.e = false;
                    ((MainActivity) a.g).d();
                } else if (i2 == 6) {
                    a.d = true;
                } else if (i2 == 7) {
                    a.d = false;
                } else if (i2 == 8) {
                    ((MainActivity) a.g).n();
                } else if (i2 == 9) {
                    return ((MainActivity) a.g).l();
                } else {
                    if (i2 == 10) {
                        return a.g.getPackageName();
                    }
                    if (i2 == 11) {
                        return new StringBuilder().append(a.g.getPackageManager().getPackageInfo(a.g.getPackageName(), 64).signatures[0].hashCode()).toString();
                    }
                }
                return null;
            }
        } catch (Exception e2) {
            return null;
        }
    }

    public String a(String str) {
        return new StringBuilder(String.valueOf(GetStrURL(str))).toString();
    }

    static {
        System.loadLibrary("xmasgame7");
    }
}
