package com.mobclix.android.sdk;

import com.mobclix.android.sdk.MobclixUtility;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import org.codehaus.jackson.util.MinimalPrettyPrinter;
import org.json.JSONObject;

public class MobclixInstrumentation {
    static String a = "startup";
    static String b = "adview";
    static final String[] c = {a, b};
    private static String d = "MobclixInstrumentation";
    private static final MobclixInstrumentation i = new MobclixInstrumentation();
    private JSONObject e = new JSONObject();
    private HashMap<String, Long> f = new HashMap<>();
    private ArrayList<String> g = new ArrayList<>();
    private HashMap<String, Integer> h = new HashMap<>();

    private MobclixInstrumentation() {
    }

    static MobclixInstrumentation a() {
        return i;
    }

    /* access modifiers changed from: package-private */
    public void a(Object obj, String str, String str2) {
        if (obj != null && str != null && !str.equals("") && str2 != null && !str2.equals("")) {
            try {
                this.e.getJSONObject(str2).getJSONObject("data").put(str, obj);
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        return a(str, null);
    }

    /* access modifiers changed from: package-private */
    public String a(String str, String str2) {
        int i2;
        if (str == null || str.equals("")) {
            return null;
        }
        if (this.e.has(str)) {
            return str;
        }
        if (str2 == null || str2.equals("")) {
            str2 = str;
        }
        try {
            Mobclix instance = Mobclix.getInstance();
            try {
                i2 = this.h.get(str2).intValue();
            } catch (Exception e2) {
                i2 = 0;
            }
            if (instance.a(str2) == null) {
                return null;
            }
            int parseInt = Integer.parseInt(instance.a(str2));
            this.h.put(str2, Integer.valueOf(i2 + 1));
            if (parseInt < 0 || (parseInt != 0 && i2 % parseInt != 0)) {
                return null;
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("benchmarks", new JSONObject());
            jSONObject.put("data", new JSONObject());
            jSONObject.put("startDate", new SimpleDateFormat("yyyy-MM-dd'T'hh:mmZ").format(new Date(System.currentTimeMillis())));
            jSONObject.put("startDateNanoTime", System.nanoTime());
            this.e.put(str, jSONObject);
            return str;
        } catch (Exception e3) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b(String str) {
        String[] split = str.split("/");
        if (split.length != 1) {
            str = split[0];
        }
        return this.e.has(str);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0050  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean c(java.lang.String r7) {
        /*
            r6 = this;
            r1 = 0
            if (r7 == 0) goto L_0x000b
            java.lang.String r0 = ""
            boolean r0 = r7.equals(r0)
            if (r0 == 0) goto L_0x000d
        L_0x000b:
            r0 = r1
        L_0x000c:
            return r0
        L_0x000d:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = java.lang.String.valueOf(r7)
            r0.<init>(r2)
            java.lang.String r2 = "/"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = r0.toString()
            java.lang.String r0 = com.mobclix.android.sdk.MobclixInstrumentation.d
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Current benchmarks: "
            r3.<init>(r4)
            java.util.HashMap<java.lang.String, java.lang.Long> r4 = r6.f
            java.util.Set r4 = r4.keySet()
            java.lang.String r4 = r4.toString()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.v(r0, r3)
            java.util.HashMap<java.lang.String, java.lang.Long> r0 = r6.f
            java.util.Set r0 = r0.keySet()
            java.util.Iterator r3 = r0.iterator()
        L_0x0048:
            boolean r0 = r3.hasNext()
            if (r0 != 0) goto L_0x0050
            r0 = 1
            goto L_0x000c
        L_0x0050:
            java.lang.Object r0 = r3.next()
            java.lang.String r0 = (java.lang.String) r0
            int r4 = r0.length()
            int r5 = r2.length()
            if (r4 < r5) goto L_0x006e
            int r4 = r2.length()
            java.lang.String r4 = r0.substring(r1, r4)
            boolean r4 = r4.equals(r2)
            if (r4 != 0) goto L_0x0074
        L_0x006e:
            boolean r0 = r0.equals(r7)
            if (r0 == 0) goto L_0x0048
        L_0x0074:
            r0 = r1
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixInstrumentation.c(java.lang.String):boolean");
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        if (str != null && !str.equals("") && b(str)) {
            if (c(str)) {
                try {
                    Long valueOf = Long.valueOf(System.currentTimeMillis());
                    JSONObject jSONObject = this.e.getJSONObject(str);
                    this.e.remove(str);
                    jSONObject.put("endDate", new SimpleDateFormat("yyyy-MM-dd'T'hh:mmZ").format(new Date(valueOf.longValue())));
                    jSONObject.put("totalElapsedTime", ((double) (System.nanoTime() - jSONObject.getLong("startDateNanoTime"))) / 1.0E9d);
                    jSONObject.remove("startDateNanoTime");
                    Mobclix instance = Mobclix.getInstance();
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("app_id", instance.b());
                    jSONObject2.put("platform", instance.d());
                    jSONObject2.put("sdk_ver", instance.u());
                    jSONObject2.put("app_ver", instance.h());
                    jSONObject2.put("udid", instance.i());
                    jSONObject2.put("dev_model", instance.k());
                    jSONObject2.put("dev_vers", instance.g());
                    jSONObject2.put("hw_dev_model", instance.l());
                    jSONObject2.put("conn", instance.m());
                    jSONObject.put("environment", jSONObject2);
                    StringBuilder sb = new StringBuilder("cat=");
                    sb.append(str).append("&payload=");
                    sb.append(URLEncoder.encode(jSONObject.toString(), "UTF-8"));
                    new Thread(new MobclixUtility.POSTThread(instance.v(), sb.toString(), null, null)).run();
                } catch (Exception e2) {
                }
            } else if (!this.g.contains(str)) {
                this.g.add(str);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String b(String str, String str2) {
        if (str2 == null || str2.equals("") || str == null || str.equals("") || !b(str)) {
            return null;
        }
        String str3 = String.valueOf(str) + "/" + str2;
        this.f.put(str3, Long.valueOf(System.nanoTime()));
        return str3;
    }

    /* access modifiers changed from: package-private */
    public String e(String str) {
        if (str == null || str.equals("") || !b(str)) {
            return null;
        }
        try {
            String[] split = str.split("/");
            if (split.length == 1) {
                return null;
            }
            Long valueOf = Long.valueOf(System.nanoTime());
            String str2 = split[0];
            this.e.getJSONObject(str2).getJSONObject("benchmarks").put(str, ((double) (valueOf.longValue() - this.f.get(str).longValue())) / 1.0E9d);
            this.f.remove(str);
            if (this.g.contains(str2)) {
                d(str2);
            }
            if (split.length <= 1) {
                return null;
            }
            StringBuilder sb = new StringBuilder(split[0]);
            for (int i2 = 1; i2 < split.length - 1; i2++) {
                sb.append("/").append(split[i2]);
            }
            String replace = sb.toString().replace(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, "").replace("\\r", "").replace("\\n", "");
            if (replace.length() == 0) {
                return null;
            }
            if (replace.equals("/")) {
                return null;
            }
            return replace;
        } catch (Exception e2) {
            return null;
        }
    }
}
