package com.mobclix.android.sdk;

import com.mobclick.android.UmengConstants;
import java.util.HashMap;

public class MobclixAdUnitSettings {
    HashMap<String, HashMap<String, String>> a = new HashMap<>();
    private boolean b = true;
    private boolean c = false;
    private boolean d = true;
    private long e = UmengConstants.kContinueSessionMillis;
    private long f = 120000;
    private boolean g = false;
    private String h = "";

    MobclixAdUnitSettings() {
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.b = z;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        this.c = z;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void c(boolean z) {
        this.d = z;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void a(long j) {
        this.e = j;
    }

    /* access modifiers changed from: package-private */
    public long d() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void b(long j) {
        this.f = j;
    }

    /* access modifiers changed from: package-private */
    public long e() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.h = str;
    }

    /* access modifiers changed from: package-private */
    public String f() {
        return this.h;
    }
}
