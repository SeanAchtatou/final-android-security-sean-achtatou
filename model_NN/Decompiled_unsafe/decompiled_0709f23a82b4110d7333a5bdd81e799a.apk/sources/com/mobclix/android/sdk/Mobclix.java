package com.mobclix.android.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.google.ads.AdActivity;
import com.mobclick.android.UmengConstants;
import com.mobclix.android.sdk.MobclixAnalytics;
import com.mobclix.android.sdk.MobclixLocation;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.SoftReference;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TimerTask;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

public final class Mobclix {
    public static final int LOG_LEVEL_DEBUG = 1;
    public static final int LOG_LEVEL_ERROR = 8;
    public static final int LOG_LEVEL_FATAL = 16;
    public static final int LOG_LEVEL_INFO = 2;
    public static final int LOG_LEVEL_WARN = 4;
    public static final String MC_LIBRARY_VERSION = "3.1.3";
    static final String[] a = {"320x50", "300x250"};
    /* access modifiers changed from: private */
    public static final Mobclix aa = new Mobclix();
    static final String[] b = {"openmillennial"};
    static HashMap<String, MobclixAdUnitSettings> d = new HashMap<>();
    static HashMap<String, String> e = new HashMap<>();
    static boolean x = false;
    static boolean y = false;
    private MobclixInstrumentation A = MobclixInstrumentation.a();
    private String B = "android";
    private String C = "mcnative";
    private String D = "0";
    private String E = "null";
    private String F = "null";
    private String G = "null";
    private String H = "null";
    private String I = "null";
    private String J = "null";
    private Handler K;
    /* access modifiers changed from: private */
    public String L = "null";
    /* access modifiers changed from: private */
    public String M = "null";
    private String N = "null";
    private String O = "null";
    private String P = "null";
    private String Q = "null";
    private String R = "null";
    private int S = -1;
    /* access modifiers changed from: private */
    public int T = -1;
    private String U = "null";
    private String V = "null";
    private int W = 16;
    private boolean X = false;
    private Criteria Y;
    private Context Z;
    JSONObject c = new JSONObject();
    String f = "http://ads.mobclix.com/";
    String g = "http://data.mobclix.com/post/config";
    String h = "http://data.mobclix.com/post/sendData";
    String i = "http://vc.mobclix.com";
    String j = "http://data.mobclix.com/post/feedback";
    String k = "http://data.mobclix.com/post/debug";
    List<String> l = new ArrayList();
    int m = 120000;
    int n = 0;
    boolean o = false;
    String p = "null";
    MobclixLocation q = new MobclixLocation();
    HashMap<String, Boolean> r = new HashMap<>();
    String s = null;
    boolean t = false;
    SoftReference<MobclixWebView> u;
    SoftReference<MobclixWebView> v;
    SoftReference<View> w;
    private SharedPreferences z = null;

    /* access modifiers changed from: package-private */
    public Context a() {
        return this.Z;
    }

    public String b() {
        return this.U == null ? "null" : this.U;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.V == null ? "null" : this.V;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.B == null ? "null" : this.B;
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.C == null ? "null" : this.C;
    }

    /* access modifiers changed from: package-private */
    public String f() {
        return this.D == null ? "null" : this.D;
    }

    /* access modifiers changed from: package-private */
    public String g() {
        return this.E == null ? "null" : this.E;
    }

    /* access modifiers changed from: package-private */
    public String h() {
        return this.F == null ? "null" : this.F;
    }

    /* access modifiers changed from: package-private */
    public String i() {
        return this.p == null ? "null" : this.p;
    }

    /* access modifiers changed from: package-private */
    public String j() {
        return this.G == null ? "null" : this.G;
    }

    /* access modifiers changed from: package-private */
    public String k() {
        return this.H == null ? "null" : this.H;
    }

    /* access modifiers changed from: package-private */
    public String l() {
        return this.I == null ? "null" : this.I;
    }

    /* access modifiers changed from: package-private */
    public String m() {
        return this.J == null ? "null" : this.J;
    }

    /* access modifiers changed from: package-private */
    public String n() {
        return this.L == null ? "null" : this.L;
    }

    /* access modifiers changed from: package-private */
    public String o() {
        return this.M == null ? "null" : this.M;
    }

    /* access modifiers changed from: package-private */
    public String p() {
        if (n().equals("null") || o().equals("null")) {
            return "null";
        }
        return String.valueOf(n()) + "," + o();
    }

    /* access modifiers changed from: package-private */
    public String q() {
        return this.N == null ? "null" : this.N;
    }

    /* access modifiers changed from: package-private */
    public String r() {
        return this.O == null ? "null" : this.O;
    }

    /* access modifiers changed from: package-private */
    public String s() {
        return this.P == null ? "null" : this.P;
    }

    /* access modifiers changed from: package-private */
    public String t() {
        return this.Q == null ? "null" : this.Q;
    }

    /* access modifiers changed from: package-private */
    public String u() {
        return MC_LIBRARY_VERSION;
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        try {
            return e.get(str);
        } catch (Exception e2) {
            return "";
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b(String str) {
        try {
            return d.get(str).a();
        } catch (Exception e2) {
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public long c(String str) {
        try {
            return d.get(str).d();
        } catch (Exception e2) {
            return -1;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean d(String str) {
        try {
            return d.get(str).b();
        } catch (Exception e2) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public Long e(String str) {
        try {
            return Long.valueOf(d.get(str).e());
        } catch (Exception e2) {
            return 0L;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean f(String str) {
        return MobclixAdView.n.get(str).longValue() + getInstance().e(str).longValue() < System.currentTimeMillis();
    }

    /* access modifiers changed from: package-private */
    public boolean g(String str) {
        try {
            return d.get(str).c();
        } catch (Exception e2) {
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public String v() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public String w() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public String x() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public List<String> y() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public int z() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public String A() {
        if (this.R.equals("null") && j("UserAgent")) {
            this.R = i("UserAgent");
        }
        return this.R;
    }

    /* access modifiers changed from: package-private */
    public void h(String str) {
        this.R = str;
        a("UserAgent", str);
    }

    /* access modifiers changed from: package-private */
    public boolean B() {
        return this.S != -1;
    }

    public boolean C() {
        if (this.S != -1) {
            return this.S == 1;
        }
        try {
            Runtime.getRuntime().exec("su");
            this.S = 1;
        } catch (Exception e2) {
            this.S = 0;
        }
        if (this.S != 1) {
            return false;
        }
        return true;
    }

    static String i(String str) {
        try {
            return aa.z.getString(str, "");
        } catch (Exception e2) {
            return "";
        }
    }

    static boolean j(String str) {
        try {
            return aa.z.contains(str);
        } catch (Exception e2) {
            return false;
        }
    }

    static void a(String str, String str2) {
        try {
            SharedPreferences.Editor edit = aa.z.edit();
            edit.putString(str, str2);
            edit.commit();
        } catch (Exception e2) {
        }
    }

    static void a(Map<String, String> map) {
        try {
            SharedPreferences.Editor edit = aa.z.edit();
            for (Map.Entry next : map.entrySet()) {
                edit.putString((String) next.getKey(), (String) next.getValue());
            }
            edit.commit();
        } catch (Exception e2) {
        }
    }

    static void k(String str) {
        try {
            SharedPreferences.Editor edit = aa.z.edit();
            edit.remove(str);
            edit.commit();
        } catch (Exception e2) {
        }
    }

    static void a(List<String> list) {
        try {
            SharedPreferences.Editor edit = aa.z.edit();
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= list.size()) {
                    edit.commit();
                    return;
                } else {
                    edit.remove(list.get(i3));
                    i2 = i3 + 1;
                }
            }
        } catch (Exception e2) {
        }
    }

    private static String m(String str) {
        byte[] bArr = new byte[40];
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(str.getBytes(), 0, str.length());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                stringBuffer.append(Integer.toHexString(b2 & 255));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e2) {
            throw new RuntimeException(e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void D() {
        I();
        try {
            if (this.X) {
                this.K.sendEmptyMessage(0);
            }
        } catch (Exception e2) {
        }
        try {
            this.c.put("ts", System.currentTimeMillis());
            String p2 = p();
            if (!p2.equals("null")) {
                this.c.put("ll", p2);
            } else {
                this.c.remove("ll");
            }
            this.c.put("g", this.J);
        } catch (Exception e3) {
        }
    }

    /* access modifiers changed from: package-private */
    public void E() {
        this.q.a(this.Z, new MobclixLocation.LocationResult() {
            public void a(Location location) {
                try {
                    Mobclix.this.L = Double.toString(location.getLatitude());
                    Mobclix.this.M = Double.toString(location.getLongitude());
                } catch (Exception e) {
                }
            }
        });
    }

    private void I() {
        NetworkInfo activeNetworkInfo;
        try {
            String typeName = (!this.r.get("android.permission.ACCESS_NETWORK_STATE").booleanValue() || (activeNetworkInfo = ((ConnectivityManager) this.Z.getSystemService("connectivity")).getActiveNetworkInfo()) == null) ? AdActivity.URL_PARAM : activeNetworkInfo.getTypeName();
            if (typeName.equals("WI_FI") || typeName.equals("WIFI")) {
                this.J = "wifi";
            } else if (typeName.equals("MOBILE")) {
                this.J = Integer.toString(((TelephonyManager) this.Z.getSystemService("phone")).getNetworkType());
            } else {
                this.J = "null";
            }
            if (this.J == null) {
                this.J = "null";
            }
        } catch (Exception e2) {
            this.J = "null";
        }
    }

    private Mobclix() {
    }

    public static Mobclix getInstance() {
        return aa;
    }

    public static final synchronized void onCreateWithApplicationId(Activity activity, String str) {
        synchronized (Mobclix.class) {
            onCreateWithApplicationId(activity, str, true);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001c, code lost:
        if (r4.equals(com.mobclix.android.sdk.Mobclix.aa.U) == false) goto L_0x001e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final synchronized void onCreateWithApplicationId(android.app.Activity r3, java.lang.String r4, boolean r5) {
        /*
            java.lang.Class<com.mobclix.android.sdk.Mobclix> r1 = com.mobclix.android.sdk.Mobclix.class
            monitor-enter(r1)
            if (r3 != 0) goto L_0x0010
            android.content.res.Resources$NotFoundException r0 = new android.content.res.Resources$NotFoundException     // Catch:{ all -> 0x000d }
            java.lang.String r2 = "Activity not provided."
            r0.<init>(r2)     // Catch:{ all -> 0x000d }
            throw r0     // Catch:{ all -> 0x000d }
        L_0x000d:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0010:
            if (r4 == 0) goto L_0x0026
            if (r5 == 0) goto L_0x001e
            com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.aa     // Catch:{ all -> 0x000d }
            java.lang.String r0 = r0.U     // Catch:{ all -> 0x000d }
            boolean r0 = r4.equals(r0)     // Catch:{ all -> 0x000d }
            if (r0 != 0) goto L_0x0026
        L_0x001e:
            com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.aa     // Catch:{ all -> 0x000d }
            r2 = 0
            r0.n = r2     // Catch:{ all -> 0x000d }
            r0 = 0
            com.mobclix.android.sdk.Mobclix.x = r0     // Catch:{ all -> 0x000d }
        L_0x0026:
            com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.aa     // Catch:{ Exception -> 0x0054 }
            android.content.Context r2 = r3.getApplicationContext()     // Catch:{ Exception -> 0x0054 }
            r0.Z = r2     // Catch:{ Exception -> 0x0054 }
        L_0x002e:
            boolean r0 = com.mobclix.android.sdk.Mobclix.x     // Catch:{ all -> 0x000d }
            if (r0 != 0) goto L_0x004c
            boolean r0 = com.mobclix.android.sdk.Mobclix.y     // Catch:{ all -> 0x000d }
            if (r0 != 0) goto L_0x004c
            com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.aa     // Catch:{ MobclixPermissionException -> 0x004a, Exception -> 0x0052 }
            r0.U = r4     // Catch:{ MobclixPermissionException -> 0x004a, Exception -> 0x0052 }
            com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.aa     // Catch:{ MobclixPermissionException -> 0x004a, Exception -> 0x0052 }
            r0.J()     // Catch:{ MobclixPermissionException -> 0x004a, Exception -> 0x0052 }
            com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.aa     // Catch:{ MobclixPermissionException -> 0x004a, Exception -> 0x0052 }
            com.mobclix.android.sdk.Mobclix$2 r2 = new com.mobclix.android.sdk.Mobclix$2     // Catch:{ MobclixPermissionException -> 0x004a, Exception -> 0x0052 }
            r2.<init>()     // Catch:{ MobclixPermissionException -> 0x004a, Exception -> 0x0052 }
            r0.K = r2     // Catch:{ MobclixPermissionException -> 0x004a, Exception -> 0x0052 }
        L_0x0048:
            monitor-exit(r1)
            return
        L_0x004a:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x000d }
        L_0x004c:
            com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.aa     // Catch:{ all -> 0x000d }
            r0.G()     // Catch:{ all -> 0x000d }
            goto L_0x0048
        L_0x0052:
            r0 = move-exception
            goto L_0x0048
        L_0x0054:
            r0 = move-exception
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.Mobclix.onCreateWithApplicationId(android.app.Activity, java.lang.String, boolean):void");
    }

    public static final synchronized void onCreate(Activity activity) {
        synchronized (Mobclix.class) {
            onCreateWithApplicationId(activity, null);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:23|24|(3:26|27|28)|30|31) */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0080, code lost:
        if (r3.equals("") != false) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00c1, code lost:
        android.util.Log.e("mobclix-controller", "Application Key Started");
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00d2, code lost:
        throw new android.content.res.Resources.NotFoundException("com.mobclix.APPLICATION_ID not found in the Android Manifest xml.");
     */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x012b A[Catch:{ NullPointerException -> 0x00ca, NameNotFoundException -> 0x00c0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0169 A[Catch:{ NullPointerException -> 0x00ca, NameNotFoundException -> 0x00c0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void F() {
        /*
            r10 = this;
            r2 = 2
            r1 = 0
            r3 = 0
            r0 = 1
            monitor-enter(r10)
            boolean r4 = com.mobclix.android.sdk.Mobclix.x     // Catch:{ all -> 0x008a }
            if (r4 == 0) goto L_0x000b
        L_0x0009:
            monitor-exit(r10)
            return
        L_0x000b:
            r4 = 1
            com.mobclix.android.sdk.Mobclix.y = r4     // Catch:{ all -> 0x008a }
            android.content.Context r4 = r10.Z     // Catch:{ all -> 0x008a }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x008a }
            android.content.Context r6 = r10.Z     // Catch:{ all -> 0x008a }
            java.lang.String r6 = r6.getPackageName()     // Catch:{ all -> 0x008a }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ all -> 0x008a }
            r5.<init>(r6)     // Catch:{ all -> 0x008a }
            java.lang.String r6 = ".MCConfig"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x008a }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x008a }
            r6 = 0
            android.content.SharedPreferences r4 = r4.getSharedPreferences(r5, r6)     // Catch:{ all -> 0x008a }
            r10.z = r4     // Catch:{ all -> 0x008a }
            java.lang.String[] r4 = com.mobclix.android.sdk.MobclixInstrumentation.c     // Catch:{ all -> 0x008a }
            int r5 = r4.length     // Catch:{ all -> 0x008a }
        L_0x0033:
            if (r3 < r5) goto L_0x008d
            com.mobclix.android.sdk.MobclixInstrumentation r3 = r10.A     // Catch:{ all -> 0x008a }
            if (r3 != 0) goto L_0x003f
            com.mobclix.android.sdk.MobclixInstrumentation r3 = com.mobclix.android.sdk.MobclixInstrumentation.a()     // Catch:{ all -> 0x008a }
            r10.A = r3     // Catch:{ all -> 0x008a }
        L_0x003f:
            com.mobclix.android.sdk.MobclixInstrumentation r3 = r10.A     // Catch:{ all -> 0x008a }
            java.lang.String r4 = com.mobclix.android.sdk.MobclixInstrumentation.a     // Catch:{ all -> 0x008a }
            java.lang.String r3 = r3.a(r4)     // Catch:{ all -> 0x008a }
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r10.A     // Catch:{ all -> 0x008a }
            java.lang.String r5 = "init"
            java.lang.String r3 = r4.b(r3, r5)     // Catch:{ all -> 0x008a }
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r10.A     // Catch:{ all -> 0x008a }
            java.lang.String r5 = "environment"
            java.lang.String r5 = r4.b(r3, r5)     // Catch:{ all -> 0x008a }
            java.lang.String r3 = ""
            android.content.Context r4 = r10.Z     // Catch:{ Exception -> 0x03f0 }
            java.lang.String r3 = r4.getPackageName()     // Catch:{ Exception -> 0x03f0 }
        L_0x005f:
            android.content.Context r4 = r10.Z     // Catch:{ NameNotFoundException -> 0x00c0 }
            android.content.pm.PackageManager r4 = r4.getPackageManager()     // Catch:{ NameNotFoundException -> 0x00c0 }
            r6 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r3 = r4.getApplicationInfo(r3, r6)     // Catch:{ NameNotFoundException -> 0x00c0 }
            r4 = r3
        L_0x006c:
            java.lang.String r3 = r10.U     // Catch:{ all -> 0x008a }
            if (r3 != 0) goto L_0x00d3
            android.os.Bundle r3 = r4.metaData     // Catch:{ NullPointerException -> 0x00ca }
            java.lang.String r6 = "com.mobclix.APPLICATION_ID"
            java.lang.String r3 = r3.getString(r6)     // Catch:{ NullPointerException -> 0x00ca }
            if (r3 == 0) goto L_0x0082
            java.lang.String r6 = ""
            boolean r6 = r3.equals(r6)     // Catch:{ all -> 0x008a }
            if (r6 == 0) goto L_0x00d3
        L_0x0082:
            android.content.res.Resources$NotFoundException r0 = new android.content.res.Resources$NotFoundException     // Catch:{ all -> 0x008a }
            java.lang.String r1 = "com.mobclix.APPLICATION_ID not found in the Android Manifest xml."
            r0.<init>(r1)     // Catch:{ all -> 0x008a }
            throw r0     // Catch:{ all -> 0x008a }
        L_0x008a:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x008d:
            r6 = r4[r3]     // Catch:{ all -> 0x008a }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x008a }
            java.lang.String r8 = "debug_"
            r7.<init>(r8)     // Catch:{ all -> 0x008a }
            java.lang.StringBuilder r7 = r7.append(r6)     // Catch:{ all -> 0x008a }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x008a }
            boolean r7 = j(r7)     // Catch:{ all -> 0x008a }
            if (r7 == 0) goto L_0x00bc
            java.util.HashMap<java.lang.String, java.lang.String> r7 = com.mobclix.android.sdk.Mobclix.e     // Catch:{ all -> 0x008a }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x008a }
            java.lang.String r9 = "debug_"
            r8.<init>(r9)     // Catch:{ all -> 0x008a }
            java.lang.StringBuilder r8 = r8.append(r6)     // Catch:{ all -> 0x008a }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x008a }
            java.lang.String r8 = i(r8)     // Catch:{ all -> 0x008a }
            r7.put(r6, r8)     // Catch:{ all -> 0x008a }
        L_0x00bc:
            int r3 = r3 + 1
            goto L_0x0033
        L_0x00c0:
            r3 = move-exception
            java.lang.String r3 = "mobclix-controller"
            java.lang.String r4 = "Application Key Started"
            android.util.Log.e(r3, r4)     // Catch:{ all -> 0x008a }
            r4 = r1
            goto L_0x006c
        L_0x00ca:
            r0 = move-exception
            android.content.res.Resources$NotFoundException r0 = new android.content.res.Resources$NotFoundException     // Catch:{ all -> 0x008a }
            java.lang.String r1 = "com.mobclix.APPLICATION_ID not found in the Android Manifest xml."
            r0.<init>(r1)     // Catch:{ all -> 0x008a }
            throw r0     // Catch:{ all -> 0x008a }
        L_0x00d3:
            r10.U = r3     // Catch:{ all -> 0x008a }
            android.os.Bundle r3 = r4.metaData     // Catch:{ NullPointerException -> 0x0133 }
            java.lang.String r6 = "ADMOB_PUBLISHER_ID"
            java.lang.String r3 = r3.getString(r6)     // Catch:{ NullPointerException -> 0x0133 }
            r10.V = r3     // Catch:{ NullPointerException -> 0x0133 }
            java.lang.String r3 = r10.V     // Catch:{ NullPointerException -> 0x0133 }
            if (r3 != 0) goto L_0x00e7
            java.lang.String r3 = "null"
            r10.V = r3     // Catch:{ NullPointerException -> 0x0133 }
        L_0x00e7:
            android.os.Bundle r3 = r4.metaData     // Catch:{ Exception -> 0x0139 }
            java.lang.String r4 = "com.mobclix.LOG_LEVEL"
            java.lang.String r3 = r3.getString(r4)     // Catch:{ Exception -> 0x0139 }
            r4 = r3
        L_0x00f0:
            r3 = 16
            if (r4 == 0) goto L_0x03f3
            java.lang.String r6 = "debug"
            boolean r6 = r4.equalsIgnoreCase(r6)     // Catch:{ all -> 0x008a }
            if (r6 == 0) goto L_0x013c
        L_0x00fc:
            r10.W = r0     // Catch:{ all -> 0x008a }
            java.lang.String r0 = android.os.Build.VERSION.RELEASE     // Catch:{ all -> 0x008a }
            r10.E = r0     // Catch:{ all -> 0x008a }
            java.lang.String r0 = r10.E     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x0110
            java.lang.String r0 = r10.E     // Catch:{ all -> 0x008a }
            java.lang.String r2 = ""
            boolean r0 = r0.equals(r2)     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x0114
        L_0x0110:
            java.lang.String r0 = "null"
            r10.E = r0     // Catch:{ all -> 0x008a }
        L_0x0114:
            java.lang.String r0 = ""
            android.content.Context r2 = r10.Z     // Catch:{ all -> 0x008a }
            android.content.pm.PackageManager r3 = r2.getPackageManager()     // Catch:{ all -> 0x008a }
            android.content.Context r2 = r10.Z     // Catch:{ Exception -> 0x0166 }
            java.lang.String r0 = r2.getPackageName()     // Catch:{ Exception -> 0x0166 }
            r2 = r0
        L_0x0123:
            java.lang.String r0 = "android.permission.INTERNET"
            int r0 = r3.checkPermission(r0, r2)     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x0169
            com.mobclix.android.sdk.Mobclix$MobclixPermissionException r0 = new com.mobclix.android.sdk.Mobclix$MobclixPermissionException     // Catch:{ all -> 0x008a }
            java.lang.String r1 = "Missing required permission INTERNET."
            r0.<init>(r1)     // Catch:{ all -> 0x008a }
            throw r0     // Catch:{ all -> 0x008a }
        L_0x0133:
            r3 = move-exception
            java.lang.String r3 = "null"
            r10.V = r3     // Catch:{ all -> 0x008a }
            goto L_0x00e7
        L_0x0139:
            r3 = move-exception
            r4 = r1
            goto L_0x00f0
        L_0x013c:
            java.lang.String r0 = "info"
            boolean r0 = r4.equalsIgnoreCase(r0)     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x0146
            r0 = r2
            goto L_0x00fc
        L_0x0146:
            java.lang.String r0 = "warn"
            boolean r0 = r4.equalsIgnoreCase(r0)     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x0150
            r0 = 4
            goto L_0x00fc
        L_0x0150:
            java.lang.String r0 = "error"
            boolean r0 = r4.equalsIgnoreCase(r0)     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x015b
            r0 = 8
            goto L_0x00fc
        L_0x015b:
            java.lang.String r0 = "fatal"
            boolean r0 = r4.equalsIgnoreCase(r0)     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x03f3
            r0 = 16
            goto L_0x00fc
        L_0x0166:
            r2 = move-exception
            r2 = r0
            goto L_0x0123
        L_0x0169:
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r10.r     // Catch:{ all -> 0x008a }
            java.lang.String r4 = "android.permission.INTERNET"
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x008a }
            r0.put(r4, r6)     // Catch:{ all -> 0x008a }
            java.lang.String r0 = "android.permission.READ_PHONE_STATE"
            int r0 = r3.checkPermission(r0, r2)     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x0185
            com.mobclix.android.sdk.Mobclix$MobclixPermissionException r0 = new com.mobclix.android.sdk.Mobclix$MobclixPermissionException     // Catch:{ all -> 0x008a }
            java.lang.String r1 = "Missing required permission READ_PHONE_STATE."
            r0.<init>(r1)     // Catch:{ all -> 0x008a }
            throw r0     // Catch:{ all -> 0x008a }
        L_0x0185:
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r10.r     // Catch:{ all -> 0x008a }
            java.lang.String r4 = "android.permission.READ_PHONE_STATE"
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x008a }
            r0.put(r4, r6)     // Catch:{ all -> 0x008a }
            java.lang.String r0 = "android.permission.BATTERY_STATS"
            int r0 = r3.checkPermission(r0, r2)     // Catch:{ Exception -> 0x03ed }
            if (r0 != 0) goto L_0x01ae
            android.content.Context r0 = r10.Z     // Catch:{ Exception -> 0x03ed }
            android.content.Context r0 = r0.getApplicationContext()     // Catch:{ Exception -> 0x03ed }
            com.mobclix.android.sdk.Mobclix$3 r4 = new com.mobclix.android.sdk.Mobclix$3     // Catch:{ Exception -> 0x03ed }
            r4.<init>()     // Catch:{ Exception -> 0x03ed }
            android.content.IntentFilter r6 = new android.content.IntentFilter     // Catch:{ Exception -> 0x03ed }
            java.lang.String r7 = "android.intent.action.BATTERY_CHANGED"
            r6.<init>(r7)     // Catch:{ Exception -> 0x03ed }
            r0.registerReceiver(r4, r6)     // Catch:{ Exception -> 0x03ed }
        L_0x01ae:
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r10.r     // Catch:{ Exception -> 0x03ed }
            java.lang.String r4 = "android.permission.BATTERY_STATS"
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ Exception -> 0x03ed }
            r0.put(r4, r6)     // Catch:{ Exception -> 0x03ed }
        L_0x01ba:
            java.lang.String r0 = "android.permission.CAMERA"
            int r0 = r3.checkPermission(r0, r2)     // Catch:{ all -> 0x008a }
            if (r0 != 0) goto L_0x01ce
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r10.r     // Catch:{ all -> 0x008a }
            java.lang.String r4 = "android.permission.CAMERA"
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x008a }
            r0.put(r4, r6)     // Catch:{ all -> 0x008a }
        L_0x01ce:
            java.lang.String r0 = "android.permission.READ_CALENDAR"
            int r0 = r3.checkPermission(r0, r2)     // Catch:{ all -> 0x008a }
            if (r0 != 0) goto L_0x01e2
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r10.r     // Catch:{ all -> 0x008a }
            java.lang.String r4 = "android.permission.READ_CALENDAR"
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x008a }
            r0.put(r4, r6)     // Catch:{ all -> 0x008a }
        L_0x01e2:
            java.lang.String r0 = "android.permission.WRITE_CALENDAR"
            int r0 = r3.checkPermission(r0, r2)     // Catch:{ all -> 0x008a }
            if (r0 != 0) goto L_0x01f6
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r10.r     // Catch:{ all -> 0x008a }
            java.lang.String r4 = "android.permission.WRITE_CALENDAR"
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x008a }
            r0.put(r4, r6)     // Catch:{ all -> 0x008a }
        L_0x01f6:
            java.lang.String r0 = "android.permission.READ_CONTACTS"
            int r0 = r3.checkPermission(r0, r2)     // Catch:{ all -> 0x008a }
            if (r0 != 0) goto L_0x020a
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r10.r     // Catch:{ all -> 0x008a }
            java.lang.String r4 = "android.permission.READ_CONTACTS"
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x008a }
            r0.put(r4, r6)     // Catch:{ all -> 0x008a }
        L_0x020a:
            java.lang.String r0 = "android.permission.WRITE_CONTACTS"
            int r0 = r3.checkPermission(r0, r2)     // Catch:{ all -> 0x008a }
            if (r0 != 0) goto L_0x021e
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r10.r     // Catch:{ all -> 0x008a }
            java.lang.String r4 = "android.permission.WRITE_CONTACTS"
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x008a }
            r0.put(r4, r6)     // Catch:{ all -> 0x008a }
        L_0x021e:
            java.lang.String r0 = "android.permission.GET_ACCOUNTS"
            int r0 = r3.checkPermission(r0, r2)     // Catch:{ all -> 0x008a }
            if (r0 != 0) goto L_0x0232
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r10.r     // Catch:{ all -> 0x008a }
            java.lang.String r4 = "android.permission.GET_ACCOUNTS"
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x008a }
            r0.put(r4, r6)     // Catch:{ all -> 0x008a }
        L_0x0232:
            java.lang.String r0 = "android.permission.VIBRATE"
            int r0 = r3.checkPermission(r0, r2)     // Catch:{ all -> 0x008a }
            if (r0 != 0) goto L_0x0246
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r10.r     // Catch:{ all -> 0x008a }
            java.lang.String r4 = "android.permission.VIBRATE"
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x008a }
            r0.put(r4, r6)     // Catch:{ all -> 0x008a }
        L_0x0246:
            java.lang.String r0 = "android.permission.ACCESS_FINE_LOCATION"
            int r0 = r3.checkPermission(r0, r2)     // Catch:{ Exception -> 0x03bb }
            if (r0 != 0) goto L_0x0395
            android.location.Criteria r0 = new android.location.Criteria     // Catch:{ Exception -> 0x03bb }
            r0.<init>()     // Catch:{ Exception -> 0x03bb }
            r10.Y = r0     // Catch:{ Exception -> 0x03bb }
            android.location.Criteria r0 = r10.Y     // Catch:{ Exception -> 0x03bb }
            r4 = 1
            r0.setAccuracy(r4)     // Catch:{ Exception -> 0x03bb }
            r0 = 1
            r10.X = r0     // Catch:{ Exception -> 0x03bb }
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r10.r     // Catch:{ Exception -> 0x03bb }
            java.lang.String r4 = "android.permission.ACCESS_FINE_LOCATION"
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ Exception -> 0x03bb }
            r0.put(r4, r6)     // Catch:{ Exception -> 0x03bb }
        L_0x026a:
            java.lang.String r0 = "android.permission.ACCESS_NETWORK_STATE"
            int r0 = r3.checkPermission(r0, r2)     // Catch:{ Exception -> 0x03bb }
            if (r0 != 0) goto L_0x027e
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r10.r     // Catch:{ Exception -> 0x03bb }
            java.lang.String r4 = "android.permission.ACCESS_NETWORK_STATE"
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ Exception -> 0x03bb }
            r0.put(r4, r6)     // Catch:{ Exception -> 0x03bb }
        L_0x027e:
            android.content.Context r0 = r10.Z     // Catch:{ Exception -> 0x03c3 }
            java.lang.String r4 = "phone"
            java.lang.Object r0 = r0.getSystemService(r4)     // Catch:{ Exception -> 0x03c3 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Exception -> 0x03c3 }
        L_0x0288:
            r1 = 0
            android.content.pm.PackageInfo r1 = r3.getPackageInfo(r2, r1)     // Catch:{ Exception -> 0x03ea }
            java.lang.String r1 = r1.versionName     // Catch:{ Exception -> 0x03ea }
            r10.F = r1     // Catch:{ Exception -> 0x03ea }
        L_0x0291:
            java.lang.String r1 = r10.F     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x029f
            java.lang.String r1 = r10.F     // Catch:{ all -> 0x008a }
            java.lang.String r2 = ""
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x02a3
        L_0x029f:
            java.lang.String r1 = "null"
            r10.F = r1     // Catch:{ all -> 0x008a }
        L_0x02a3:
            android.content.Context r1 = r10.Z     // Catch:{ Exception -> 0x03c7 }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ Exception -> 0x03c7 }
            java.lang.String r2 = "android_id"
            java.lang.String r1 = android.provider.Settings.System.getString(r1, r2)     // Catch:{ Exception -> 0x03c7 }
            r10.G = r1     // Catch:{ Exception -> 0x03c7 }
        L_0x02b1:
            java.lang.String r1 = r10.G     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x02bf
            java.lang.String r1 = r10.G     // Catch:{ all -> 0x008a }
            java.lang.String r2 = ""
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x02c3
        L_0x02bf:
            java.lang.String r1 = "null"
            r10.G = r1     // Catch:{ all -> 0x008a }
        L_0x02c3:
            java.lang.String r1 = r0.getDeviceId()     // Catch:{ Exception -> 0x03e7 }
            r10.p = r1     // Catch:{ Exception -> 0x03e7 }
        L_0x02c9:
            java.lang.String r1 = r10.p     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x02d7
            java.lang.String r1 = r10.p     // Catch:{ all -> 0x008a }
            java.lang.String r2 = ""
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x02db
        L_0x02d7:
            java.lang.String r1 = r10.G     // Catch:{ all -> 0x008a }
            r10.p = r1     // Catch:{ all -> 0x008a }
        L_0x02db:
            java.lang.String r1 = android.os.Build.MODEL     // Catch:{ Exception -> 0x03e4 }
            r10.H = r1     // Catch:{ Exception -> 0x03e4 }
        L_0x02df:
            java.lang.String r1 = r10.H     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x02ed
            java.lang.String r1 = r10.H     // Catch:{ all -> 0x008a }
            java.lang.String r2 = ""
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x02f1
        L_0x02ed:
            java.lang.String r1 = "null"
            r10.H = r1     // Catch:{ all -> 0x008a }
        L_0x02f1:
            java.lang.String r1 = android.os.Build.DEVICE     // Catch:{ Exception -> 0x03e1 }
            r10.I = r1     // Catch:{ Exception -> 0x03e1 }
        L_0x02f5:
            java.lang.String r1 = r10.I     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x0303
            java.lang.String r1 = r10.I     // Catch:{ all -> 0x008a }
            java.lang.String r2 = ""
            boolean r1 = r1.equals(r2)     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x0307
        L_0x0303:
            java.lang.String r1 = "null"
            r10.I = r1     // Catch:{ all -> 0x008a }
        L_0x0307:
            java.lang.String r0 = r0.getNetworkOperator()     // Catch:{ Exception -> 0x03de }
            if (r0 == 0) goto L_0x031c
            r1 = 0
            r2 = 3
            java.lang.String r1 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x03de }
            r10.P = r1     // Catch:{ Exception -> 0x03de }
            r1 = 3
            java.lang.String r0 = r0.substring(r1)     // Catch:{ Exception -> 0x03de }
            r10.Q = r0     // Catch:{ Exception -> 0x03de }
        L_0x031c:
            java.lang.String r0 = r10.P     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x032a
            java.lang.String r0 = r10.P     // Catch:{ all -> 0x008a }
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x032e
        L_0x032a:
            java.lang.String r0 = "null"
            r10.P = r0     // Catch:{ all -> 0x008a }
        L_0x032e:
            java.lang.String r0 = r10.Q     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x033c
            java.lang.String r0 = r10.Q     // Catch:{ all -> 0x008a }
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x0340
        L_0x033c:
            java.lang.String r0 = "null"
            r10.Q = r0     // Catch:{ all -> 0x008a }
        L_0x0340:
            java.util.Locale r0 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x03db }
            java.lang.String r1 = r0.toString()     // Catch:{ Exception -> 0x03db }
            r10.N = r1     // Catch:{ Exception -> 0x03db }
            java.lang.String r0 = r0.getLanguage()     // Catch:{ Exception -> 0x03db }
            r10.O = r0     // Catch:{ Exception -> 0x03db }
        L_0x0350:
            java.lang.String r0 = r10.N     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x035e
            java.lang.String r0 = r10.N     // Catch:{ all -> 0x008a }
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x0362
        L_0x035e:
            java.lang.String r0 = "null"
            r10.N = r0     // Catch:{ all -> 0x008a }
        L_0x0362:
            java.lang.String r0 = r10.O     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x0370
            java.lang.String r0 = r10.O     // Catch:{ all -> 0x008a }
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r1)     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x0374
        L_0x0370:
            java.lang.String r0 = "null"
            r10.O = r0     // Catch:{ all -> 0x008a }
        L_0x0374:
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r10.A     // Catch:{ all -> 0x008a }
            java.lang.String r0 = r0.e(r5)     // Catch:{ all -> 0x008a }
            com.mobclix.android.sdk.MobclixInstrumentation r1 = r10.A     // Catch:{ all -> 0x008a }
            java.lang.String r2 = "session"
            java.lang.String r0 = r1.b(r0, r2)     // Catch:{ all -> 0x008a }
            r1 = 1
            com.mobclix.android.sdk.Mobclix.x = r1     // Catch:{ all -> 0x008a }
            r1 = 0
            com.mobclix.android.sdk.Mobclix.y = r1     // Catch:{ all -> 0x008a }
            com.mobclix.android.sdk.MobclixInstrumentation r1 = r10.A     // Catch:{ all -> 0x008a }
            java.lang.String r0 = r1.e(r0)     // Catch:{ all -> 0x008a }
            com.mobclix.android.sdk.MobclixInstrumentation r1 = r10.A     // Catch:{ all -> 0x008a }
            r1.e(r0)     // Catch:{ all -> 0x008a }
            goto L_0x0009
        L_0x0395:
            java.lang.String r0 = "android.permission.ACCESS_COARSE_LOCATION"
            int r0 = r3.checkPermission(r0, r2)     // Catch:{ Exception -> 0x03bb }
            if (r0 != 0) goto L_0x03be
            android.location.Criteria r0 = new android.location.Criteria     // Catch:{ Exception -> 0x03bb }
            r0.<init>()     // Catch:{ Exception -> 0x03bb }
            r10.Y = r0     // Catch:{ Exception -> 0x03bb }
            android.location.Criteria r0 = r10.Y     // Catch:{ Exception -> 0x03bb }
            r4 = 2
            r0.setAccuracy(r4)     // Catch:{ Exception -> 0x03bb }
            r0 = 1
            r10.X = r0     // Catch:{ Exception -> 0x03bb }
            java.util.HashMap<java.lang.String, java.lang.Boolean> r0 = r10.r     // Catch:{ Exception -> 0x03bb }
            java.lang.String r4 = "android.permission.ACCESS_COARSE_LOCATION"
            r6 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r6)     // Catch:{ Exception -> 0x03bb }
            r0.put(r4, r6)     // Catch:{ Exception -> 0x03bb }
            goto L_0x026a
        L_0x03bb:
            r0 = move-exception
            goto L_0x027e
        L_0x03be:
            r0 = 0
            r10.X = r0     // Catch:{ Exception -> 0x03bb }
            goto L_0x026a
        L_0x03c3:
            r0 = move-exception
            r0 = r1
            goto L_0x0288
        L_0x03c7:
            r1 = move-exception
            android.content.Context r1 = r10.Z     // Catch:{ Exception -> 0x03d8 }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ Exception -> 0x03d8 }
            java.lang.String r2 = "android_id"
            java.lang.String r1 = android.provider.Settings.System.getString(r1, r2)     // Catch:{ Exception -> 0x03d8 }
            r10.G = r1     // Catch:{ Exception -> 0x03d8 }
            goto L_0x02b1
        L_0x03d8:
            r1 = move-exception
            goto L_0x02b1
        L_0x03db:
            r0 = move-exception
            goto L_0x0350
        L_0x03de:
            r0 = move-exception
            goto L_0x031c
        L_0x03e1:
            r1 = move-exception
            goto L_0x02f5
        L_0x03e4:
            r1 = move-exception
            goto L_0x02df
        L_0x03e7:
            r1 = move-exception
            goto L_0x02c9
        L_0x03ea:
            r1 = move-exception
            goto L_0x0291
        L_0x03ed:
            r0 = move-exception
            goto L_0x01ba
        L_0x03f0:
            r4 = move-exception
            goto L_0x005f
        L_0x03f3:
            r0 = r3
            goto L_0x00fc
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.Mobclix.F():void");
    }

    public static final synchronized void onStop(Activity activity) {
        synchronized (Mobclix.class) {
            aa.G();
        }
    }

    public static final void logEvent(int i2, String str, String str2, String str3, boolean z2) {
        if (!x) {
            Log.v("mobclix-controller", "logEvent failed - You must initialize Mobclix by calling Mobclix.onCreate(this).");
        } else if (i2 >= aa.W) {
            String str4 = String.valueOf(str) + ", " + str2 + ": " + str3;
            switch (i2) {
                case 1:
                    Log.d("Mobclix", str4);
                    break;
                case 2:
                    Log.i("Mobclix", str4);
                    break;
                case 4:
                    Log.w("Mobclix", str4);
                    break;
                case 8:
                    Log.e("Mobclix", str4);
                    break;
                case 16:
                    Log.e("Mobclix", str4);
                    break;
            }
            try {
                JSONObject jSONObject = new JSONObject(aa.c, new String[]{"ts", "ll", "g", "id"});
                jSONObject.put("el", Integer.toString(i2));
                jSONObject.put("ep", URLEncoder.encode(str, "UTF-8"));
                jSONObject.put("en", URLEncoder.encode(str2, "UTF-8"));
                jSONObject.put("ed", URLEncoder.encode(str3, "UTF-8"));
                jSONObject.put("et", Long.toString(Thread.currentThread().getId()));
                jSONObject.put("es", z2 ? "1" : "0");
                new Thread(new MobclixAnalytics.LogEvent(jSONObject)).start();
            } catch (Exception e2) {
            }
        }
    }

    public static final void sync() {
        if (!x) {
            Log.v("mobclix-controller", "sync failed - You must initialize Mobclix by calling Mobclix.onCreate(this).");
        } else if (MobclixAnalytics.a() == MobclixAnalytics.a) {
            new Thread(new MobclixAnalytics.Sync()).start();
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void G() {
        /*
            r6 = this;
            monitor-enter(r6)
            r0 = 0
            java.lang.String r2 = "lastSessionEvent"
            boolean r2 = j(r2)     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
            if (r2 == 0) goto L_0x0015
            java.lang.String r2 = "lastSessionEvent"
            java.lang.String r2 = i(r2)     // Catch:{ Exception -> 0x0039, all -> 0x0036 }
            long r0 = java.lang.Long.parseLong(r2)     // Catch:{ Exception -> 0x0039, all -> 0x0036 }
        L_0x0015:
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
            int r4 = r6.m     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
            long r4 = (long) r4     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
            long r0 = r0 + r4
            int r0 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x0026
            r6.J()     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
        L_0x0024:
            monitor-exit(r6)
            return
        L_0x0026:
            java.lang.String r0 = "lastSessionEvent"
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
            java.lang.String r1 = java.lang.Long.toString(r1)     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
            a(r0, r1)     // Catch:{ Exception -> 0x0034, all -> 0x0036 }
            goto L_0x0024
        L_0x0034:
            r0 = move-exception
            goto L_0x0024
        L_0x0036:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0039:
            r2 = move-exception
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.Mobclix.G():void");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void J() {
        /*
            r10 = this;
            r2 = 1000(0x3e8, double:4.94E-321)
            r6 = 0
            monitor-enter(r10)
            java.lang.String r0 = "firstSessionEvent"
            boolean r0 = j(r0)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            if (r0 == 0) goto L_0x0119
            java.lang.String r0 = "firstSessionEvent"
            java.lang.String r0 = i(r0)     // Catch:{ Exception -> 0x0101, all -> 0x0109 }
            long r0 = java.lang.Long.parseLong(r0)     // Catch:{ Exception -> 0x0101, all -> 0x0109 }
            r8 = r0
        L_0x0018:
            java.lang.String r0 = "lastSessionEvent"
            boolean r0 = j(r0)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            if (r0 == 0) goto L_0x0116
            java.lang.String r0 = "lastSessionEvent"
            java.lang.String r0 = i(r0)     // Catch:{ Exception -> 0x0105, all -> 0x0109 }
            long r0 = java.lang.Long.parseLong(r0)     // Catch:{ Exception -> 0x0105, all -> 0x0109 }
            r4 = r0
        L_0x002b:
            long r0 = r4 - r8
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 == 0) goto L_0x0035
            int r4 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r4 != 0) goto L_0x0036
        L_0x0035:
            r0 = r2
        L_0x0036:
            java.lang.String r4 = "totalSessionTime"
            boolean r4 = j(r4)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            if (r4 == 0) goto L_0x0049
            java.lang.String r4 = "totalSessionTime"
            java.lang.String r4 = i(r4)     // Catch:{ Exception -> 0x0113, all -> 0x0109 }
            long r4 = java.lang.Long.parseLong(r4)     // Catch:{ Exception -> 0x0113, all -> 0x0109 }
            long r0 = r0 + r4
        L_0x0049:
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            r4.<init>()     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.String r5 = "totalSessionTime"
            java.lang.String r0 = java.lang.Long.toString(r0)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            r4.put(r5, r0)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            boolean r0 = r10.o     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            if (r0 == 0) goto L_0x0079
            r0 = 1
            java.lang.String r1 = "offlineSessions"
            boolean r1 = j(r1)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            if (r1 == 0) goto L_0x006f
            java.lang.String r1 = "offlineSessions"
            java.lang.String r1 = i(r1)     // Catch:{ Exception -> 0x0110, all -> 0x0109 }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x0110, all -> 0x0109 }
            int r0 = r0 + r1
        L_0x006f:
            java.lang.String r1 = "offlineSessions"
            long r5 = (long) r0     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.String r0 = java.lang.Long.toString(r5)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            r4.put(r1, r0)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
        L_0x0079:
            java.lang.String r0 = "firstSessionEvent"
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            long r1 = r5 - r2
            java.lang.String r1 = java.lang.Long.toString(r1)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            r4.put(r0, r1)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.String r0 = "lastSessionEvent"
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.String r1 = java.lang.Long.toString(r1)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            r4.put(r0, r1)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            a(r4)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            com.mobclix.android.sdk.MobclixInstrumentation r0 = r10.A     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.String r1 = com.mobclix.android.sdk.MobclixInstrumentation.a     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.String r0 = r0.a(r1)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            com.mobclix.android.sdk.MobclixInstrumentation r1 = r10.A     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.String r2 = "session"
            java.lang.String r0 = r1.b(r0, r2)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            com.mobclix.android.sdk.MobclixInstrumentation r1 = r10.A     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.String r2 = "init"
            java.lang.String r0 = r1.b(r0, r2)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.String r4 = r10.p     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.String r1 = m(r1)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            org.json.JSONObject r2 = r10.c     // Catch:{ Exception -> 0x010e, all -> 0x0109 }
            java.lang.String r3 = "id"
            java.lang.String r4 = "UTF-8"
            java.lang.String r1 = java.net.URLEncoder.encode(r1, r4)     // Catch:{ Exception -> 0x010e, all -> 0x0109 }
            r2.put(r3, r1)     // Catch:{ Exception -> 0x010e, all -> 0x0109 }
        L_0x00d8:
            com.mobclix.android.sdk.MobclixInstrumentation r1 = r10.A     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.String r0 = r1.e(r0)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            com.mobclix.android.sdk.MobclixInstrumentation r1 = r10.A     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.String r2 = "config"
            java.lang.String r0 = r1.b(r0, r2)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            r1 = 0
            r10.n = r1     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            com.mobclix.android.sdk.FetchRemoteConfig r1 = new com.mobclix.android.sdk.FetchRemoteConfig     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            r1.<init>()     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            r2 = 0
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            r1.execute(r2)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            com.mobclix.android.sdk.MobclixInstrumentation r1 = r10.A     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            java.lang.String r0 = r1.e(r0)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            com.mobclix.android.sdk.MobclixInstrumentation r1 = r10.A     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
            r1.e(r0)     // Catch:{ Exception -> 0x010c, all -> 0x0109 }
        L_0x00ff:
            monitor-exit(r10)
            return
        L_0x0101:
            r0 = move-exception
            r8 = r6
            goto L_0x0018
        L_0x0105:
            r0 = move-exception
            r4 = r6
            goto L_0x002b
        L_0x0109:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x010c:
            r0 = move-exception
            goto L_0x00ff
        L_0x010e:
            r1 = move-exception
            goto L_0x00d8
        L_0x0110:
            r1 = move-exception
            goto L_0x006f
        L_0x0113:
            r4 = move-exception
            goto L_0x0049
        L_0x0116:
            r4 = r6
            goto L_0x002b
        L_0x0119:
            r8 = r6
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.Mobclix.J():void");
    }

    class MobclixPermissionException extends RuntimeException {
        MobclixPermissionException(String str) {
            super(str);
        }
    }

    static String l(String str) {
        try {
            return CookieManager.getInstance().getCookie(str);
        } catch (Exception e2) {
            return "";
        }
    }

    static void a(CookieStore cookieStore, String str) {
        try {
            CookieManager instance = CookieManager.getInstance();
            List cookies = cookieStore.getCookies();
            if (!cookies.isEmpty()) {
                StringBuffer stringBuffer = new StringBuffer();
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= cookies.size()) {
                        CookieSyncManager.getInstance().sync();
                        CookieSyncManager.getInstance().stopSync();
                        return;
                    }
                    Cookie cookie = (Cookie) cookies.get(i3);
                    stringBuffer.append(cookie.getName()).append("=").append(cookie.getValue());
                    if (cookie.getExpiryDate() != null) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E, dd-MMM-yyyy HH:mm:ss z");
                        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
                        stringBuffer.append("; expires=").append(simpleDateFormat.format(cookie.getExpiryDate()));
                    }
                    if (cookie.getPath() != null) {
                        stringBuffer.append("; path=").append(cookie.getPath());
                    }
                    if (cookie.getDomain() != null) {
                        stringBuffer.append("; domain=").append(cookie.getDomain());
                    }
                    instance.setCookie(str, stringBuffer.toString());
                    i2 = i3 + 1;
                }
            }
        } catch (Exception e2) {
        }
    }

    static class MobclixHttpClient extends DefaultHttpClient {
        String a;
        HttpGet b = new HttpGet(this.a);

        public MobclixHttpClient(String str) {
            this.a = str;
            this.b.setHeader("Cookie", Mobclix.l(this.a));
            this.b.setHeader("User-Agent", Mobclix.aa.A());
        }

        public HttpResponse a() throws ClientProtocolException, IOException {
            try {
                HttpResponse execute = Mobclix.super.execute(this.b);
                Mobclix.a(getCookieStore(), this.a);
                return execute;
            } catch (Throwable th) {
                return null;
            }
        }
    }

    static class BitmapHandler extends Handler {
        protected Bitmap a = null;
        protected Object b = null;

        BitmapHandler() {
        }

        public void a(Bitmap bitmap) {
            this.a = bitmap;
        }

        public void handleMessage(Message message) {
            if (this.a != null) {
                this.a.recycle();
            }
        }
    }

    static class FetchImageThread implements Runnable {
        private BitmapHandler a;
        private String b;
        private Bitmap c;

        FetchImageThread(String str, BitmapHandler bitmapHandler) {
            this.b = str;
            this.a = bitmapHandler;
        }

        public void run() {
            try {
                HttpEntity entity = new MobclixHttpClient(this.b).a().getEntity();
                this.c = BitmapFactory.decodeStream(entity.getContent());
                entity.consumeContent();
                this.a.a(this.c);
            } catch (Throwable th) {
            }
            this.a.sendEmptyMessage(0);
        }
    }

    static class FetchResponseThread extends TimerTask implements Runnable {
        private Handler a;
        private String b;

        FetchResponseThread(String str, Handler handler) {
            this.b = str;
            this.a = handler;
        }

        public void run() {
            BufferedReader bufferedReader;
            Throwable th;
            int i = -503;
            Mobclix.aa.D();
            if (this.b.equals("")) {
                a(-503);
            }
            String str = "";
            BufferedReader bufferedReader2 = null;
            try {
                HttpResponse a2 = new MobclixHttpClient(this.b).a();
                HttpEntity entity = a2.getEntity();
                int statusCode = a2.getStatusLine().getStatusCode();
                if ((statusCode == 200 || statusCode == 251) && entity != null) {
                    bufferedReader = new BufferedReader(new InputStreamReader(entity.getContent()), 8000);
                    try {
                        for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
                            str = String.valueOf(str) + readLine;
                        }
                        entity.consumeContent();
                        if (!str.equals("")) {
                            Message message = new Message();
                            Bundle bundle = new Bundle();
                            bundle.putString(UmengConstants.AtomKey_Type, "success");
                            bundle.putString("response", str);
                            message.setData(bundle);
                            this.a.sendMessage(message);
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        try {
                            bufferedReader.close();
                        } catch (Exception e) {
                        }
                        throw th;
                    }
                } else {
                    switch (statusCode) {
                        case 251:
                            String value = a2.getFirstHeader("X-Mobclix-Suballocation").getValue();
                            if (value != null) {
                                i = Integer.parseInt(value);
                                break;
                            }
                            break;
                    }
                    a(i);
                    bufferedReader = null;
                }
                try {
                    bufferedReader.close();
                } catch (Exception e2) {
                }
            } catch (Throwable th3) {
                Throwable th4 = th3;
                bufferedReader = null;
                th = th4;
                bufferedReader.close();
                throw th;
            }
        }

        /* access modifiers changed from: package-private */
        public void a(String str) {
            this.b = str;
        }

        /* access modifiers changed from: protected */
        public void a(int i) {
            Message message = new Message();
            Bundle bundle = new Bundle();
            bundle.putString(UmengConstants.AtomKey_Type, "failure");
            bundle.putInt("errorCode", i);
            message.setData(bundle);
            this.a.sendMessage(message);
        }
    }

    static class ObjectOnClickListener implements DialogInterface.OnClickListener {
        Object b;
        Object c;
        Object d;

        public ObjectOnClickListener(Object obj) {
            this.b = obj;
        }

        public ObjectOnClickListener(Object obj, Object obj2) {
            this.b = obj;
            this.c = obj2;
        }

        public ObjectOnClickListener(Object obj, Object obj2, Object obj3) {
            this.b = obj;
            this.c = obj2;
            this.d = obj3;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
        }
    }
}
