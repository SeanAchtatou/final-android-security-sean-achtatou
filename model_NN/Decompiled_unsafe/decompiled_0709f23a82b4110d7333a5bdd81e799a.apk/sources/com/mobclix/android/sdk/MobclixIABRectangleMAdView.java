package com.mobclix.android.sdk;

import android.content.Context;
import android.util.AttributeSet;

public final class MobclixIABRectangleMAdView extends MobclixAdView {
    public MobclixIABRectangleMAdView(Context context) {
        super(context, "300x250");
    }

    public MobclixIABRectangleMAdView(Context context, AttributeSet attributeSet) {
        super(context, "300x250", attributeSet);
    }
}
