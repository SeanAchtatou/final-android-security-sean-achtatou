package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.ArcShape;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Browser;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import com.mobclix.android.sdk.Mobclix;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import org.json.JSONArray;
import org.json.JSONObject;

public class MobclixBrowserActivity extends Activity {
    FrameLayout a;
    ScreenReceiver b;
    boolean c = true;
    Uri d;
    private String e = "mobclix-browser";
    private String f = "";
    private float g = 1.0f;
    private int h;
    /* access modifiers changed from: private */
    public View i;
    /* access modifiers changed from: private */
    public ResourceResponseHandler j = new ResourceResponseHandler();
    /* access modifiers changed from: private */
    public LinkedList<Thread> k = new LinkedList<>();
    private Intent l = null;
    private final int m = 0;
    private final int n = 1;
    private final int o = 2;

    /* access modifiers changed from: private */
    public int a(int i2) {
        return (int) (this.g * ((float) i2));
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r7) {
        /*
            r6 = this;
            r5 = 2
            r0 = 0
            r1 = 1
            super.onCreate(r7)
            android.content.res.Resources r2 = r6.getResources()     // Catch:{ Throwable -> 0x00c2 }
            android.util.DisplayMetrics r2 = r2.getDisplayMetrics()     // Catch:{ Throwable -> 0x00c2 }
            float r2 = r2.density     // Catch:{ Throwable -> 0x00c2 }
            r6.g = r2     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r2 = r6.getIntent()     // Catch:{ Throwable -> 0x00c2 }
            android.os.Bundle r2 = r2.getExtras()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Throwable -> 0x00c2 }
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = ".data"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Throwable -> 0x00c2 }
            r6.f = r3     // Catch:{ Throwable -> 0x00c2 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Throwable -> 0x00c2 }
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = ".type"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = "video"
            boolean r3 = r3.equals(r4)     // Catch:{ Throwable -> 0x00c2 }
            if (r3 == 0) goto L_0x008e
            r0 = 0
            r6.h = r0     // Catch:{ Throwable -> 0x00c2 }
            r0 = 1
            r6.requestWindowFeature(r0)     // Catch:{ Throwable -> 0x00c2 }
            r0 = 0
            r6.setRequestedOrientation(r0)     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$MobclixVideoView r0 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixVideoView     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r1 = r6.f     // Catch:{ Throwable -> 0x00c2 }
            r0.<init>(r6, r6, r1)     // Catch:{ Throwable -> 0x00c2 }
            r6.i = r0     // Catch:{ Throwable -> 0x00c2 }
        L_0x006e:
            android.widget.FrameLayout r0 = new android.widget.FrameLayout     // Catch:{ Throwable -> 0x00c2 }
            r0.<init>(r6)     // Catch:{ Throwable -> 0x00c2 }
            r6.a = r0     // Catch:{ Throwable -> 0x00c2 }
            android.widget.FrameLayout r0 = r6.a     // Catch:{ Throwable -> 0x00c2 }
            android.widget.TableLayout$LayoutParams r1 = new android.widget.TableLayout$LayoutParams     // Catch:{ Throwable -> 0x00c2 }
            r2 = -1
            r3 = -1
            r1.<init>(r2, r3)     // Catch:{ Throwable -> 0x00c2 }
            r0.setLayoutParams(r1)     // Catch:{ Throwable -> 0x00c2 }
            android.widget.FrameLayout r0 = r6.a     // Catch:{ Throwable -> 0x00c2 }
            r6.setContentView(r0)     // Catch:{ Throwable -> 0x00c2 }
            android.widget.FrameLayout r0 = r6.a     // Catch:{ Throwable -> 0x00c2 }
            android.view.View r1 = r6.i     // Catch:{ Throwable -> 0x00c2 }
            r0.addView(r1)     // Catch:{ Throwable -> 0x00c2 }
        L_0x008d:
            return
        L_0x008e:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Throwable -> 0x00c2 }
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = ".type"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = "browser"
            boolean r3 = r3.equals(r4)     // Catch:{ Throwable -> 0x00c2 }
            if (r3 == 0) goto L_0x00c7
            r0 = 2
            r6.h = r0     // Catch:{ Throwable -> 0x00c2 }
            r0 = 2
            r6.requestWindowFeature(r0)     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser r0 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r1 = r6.f     // Catch:{ Throwable -> 0x00c2 }
            r0.<init>(r6, r1)     // Catch:{ Throwable -> 0x00c2 }
            r6.i = r0     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x006e
        L_0x00c2:
            r0 = move-exception
            r6.finish()
            goto L_0x008d
        L_0x00c7:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Throwable -> 0x00c2 }
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = ".type"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = "expander"
            boolean r3 = r3.equals(r4)     // Catch:{ Throwable -> 0x00c2 }
            if (r3 == 0) goto L_0x011f
            r0 = 3
            r6.h = r0     // Catch:{ Throwable -> 0x00c2 }
            r0 = 1
            r6.requestWindowFeature(r0)     // Catch:{ Throwable -> 0x00c2 }
            android.view.Window r0 = r6.getWindow()     // Catch:{ Throwable -> 0x00c2 }
            r1 = 1024(0x400, float:1.435E-42)
            r2 = 1024(0x400, float:1.435E-42)
            r0.setFlags(r1, r2)     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$MobclixExpander r0 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixExpander     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r1 = r6.f     // Catch:{ Throwable -> 0x00c2 }
            r0.<init>(r6, r6, r1)     // Catch:{ Throwable -> 0x00c2 }
            r6.i = r0     // Catch:{ Throwable -> 0x00c2 }
            android.content.IntentFilter r0 = new android.content.IntentFilter     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r1 = "android.intent.action.SCREEN_ON"
            r0.<init>(r1)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r1 = "android.intent.action.SCREEN_OFF"
            r0.addAction(r1)     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$ScreenReceiver r1 = new com.mobclix.android.sdk.MobclixBrowserActivity$ScreenReceiver     // Catch:{ Throwable -> 0x00c2 }
            r1.<init>()     // Catch:{ Throwable -> 0x00c2 }
            r6.b = r1     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$ScreenReceiver r1 = r6.b     // Catch:{ Throwable -> 0x00c2 }
            r6.registerReceiver(r1, r0)     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x006e
        L_0x011f:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Throwable -> 0x00c2 }
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = ".type"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = "fullscreen"
            boolean r3 = r3.equals(r4)     // Catch:{ Throwable -> 0x00c2 }
            if (r3 == 0) goto L_0x0176
            r0 = 9
            r6.h = r0     // Catch:{ Throwable -> 0x00c2 }
            r0 = 1
            r6.requestWindowFeature(r0)     // Catch:{ Throwable -> 0x00c2 }
            android.view.Window r0 = r6.getWindow()     // Catch:{ Throwable -> 0x00c2 }
            r1 = 1024(0x400, float:1.435E-42)
            r2 = 1024(0x400, float:1.435E-42)
            r0.setFlags(r1, r2)     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$MobclixFullScreenAd r0 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixFullScreenAd     // Catch:{ Throwable -> 0x00c2 }
            r0.<init>(r6)     // Catch:{ Throwable -> 0x00c2 }
            r6.i = r0     // Catch:{ Throwable -> 0x00c2 }
            android.content.IntentFilter r0 = new android.content.IntentFilter     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r1 = "android.intent.action.SCREEN_ON"
            r0.<init>(r1)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r1 = "android.intent.action.SCREEN_OFF"
            r0.addAction(r1)     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$ScreenReceiver r1 = new com.mobclix.android.sdk.MobclixBrowserActivity$ScreenReceiver     // Catch:{ Throwable -> 0x00c2 }
            r1.<init>()     // Catch:{ Throwable -> 0x00c2 }
            r6.b = r1     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$ScreenReceiver r1 = r6.b     // Catch:{ Throwable -> 0x00c2 }
            r6.registerReceiver(r1, r0)     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x006e
        L_0x0176:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Throwable -> 0x00c2 }
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = ".type"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = "camera"
            boolean r3 = r3.equals(r4)     // Catch:{ Throwable -> 0x00c2 }
            if (r3 == 0) goto L_0x01d7
            r0 = 4
            r6.h = r0     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r0 = "camera.jpg"
            android.content.ContentValues r1 = new android.content.ContentValues     // Catch:{ Throwable -> 0x00c2 }
            r1.<init>()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r2 = "title"
            r1.put(r2, r0)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r0 = "description"
            java.lang.String r2 = "Image capture by camera"
            r1.put(r0, r2)     // Catch:{ Throwable -> 0x00c2 }
            android.content.ContentResolver r0 = r6.getContentResolver()     // Catch:{ Exception -> 0x0354 }
            android.net.Uri r2 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI     // Catch:{ Exception -> 0x0354 }
            android.net.Uri r0 = r0.insert(r2, r1)     // Catch:{ Exception -> 0x0354 }
            r6.d = r0     // Catch:{ Exception -> 0x0354 }
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r1 = "android.media.action.IMAGE_CAPTURE"
            r0.<init>(r1)     // Catch:{ Throwable -> 0x00c2 }
            r6.l = r0     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r0 = r6.l     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r1 = "output"
            android.net.Uri r2 = r6.d     // Catch:{ Throwable -> 0x00c2 }
            r0.putExtra(r1, r2)     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r0 = r6.l     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r1 = "android.intent.extra.videoQuality"
            r2 = 0
            r0.putExtra(r1, r2)     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x008d
        L_0x01d7:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Throwable -> 0x00c2 }
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = ".type"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = "gallery"
            boolean r3 = r3.equals(r4)     // Catch:{ Throwable -> 0x00c2 }
            if (r3 == 0) goto L_0x0214
            r0 = 5
            r6.h = r0     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Throwable -> 0x00c2 }
            r0.<init>()     // Catch:{ Throwable -> 0x00c2 }
            r6.l = r0     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r0 = r6.l     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r1 = "image/*"
            r0.setType(r1)     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r0 = r6.l     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r1 = "android.intent.action.GET_CONTENT"
            r0.setAction(r1)     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x008d
        L_0x0214:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Throwable -> 0x00c2 }
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = ".type"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = "sendToServer"
            boolean r3 = r3.equals(r4)     // Catch:{ Throwable -> 0x00c2 }
            if (r3 == 0) goto L_0x0251
            r0 = 6
            r6.h = r0     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r0 = new android.content.Intent     // Catch:{ Throwable -> 0x00c2 }
            r0.<init>()     // Catch:{ Throwable -> 0x00c2 }
            r6.l = r0     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r0 = r6.l     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r1 = "image/*"
            r0.setType(r1)     // Catch:{ Throwable -> 0x00c2 }
            android.content.Intent r0 = r6.l     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r1 = "android.intent.action.GET_CONTENT"
            r0.setAction(r1)     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x008d
        L_0x0251:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Throwable -> 0x00c2 }
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = ".type"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = "contact"
            boolean r3 = r3.equals(r4)     // Catch:{ Throwable -> 0x00c2 }
            if (r3 == 0) goto L_0x0279
            r0 = 7
            r6.h = r0     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x008d
        L_0x0279:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Throwable -> 0x00c2 }
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = ".type"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = "addContact"
            boolean r3 = r3.equals(r4)     // Catch:{ Throwable -> 0x00c2 }
            if (r3 == 0) goto L_0x02fb
            r0 = 8
            r6.h = r0     // Catch:{ Throwable -> 0x00c2 }
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x02cc }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02cc }
            java.lang.String r3 = r6.getPackageName()     // Catch:{ Exception -> 0x02cc }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x02cc }
            r1.<init>(r3)     // Catch:{ Exception -> 0x02cc }
            java.lang.String r3 = ".data"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x02cc }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x02cc }
            java.lang.String r1 = r2.getString(r1)     // Catch:{ Exception -> 0x02cc }
            r0.<init>(r1)     // Catch:{ Exception -> 0x02cc }
            com.mobclix.android.sdk.MobclixContacts r1 = com.mobclix.android.sdk.MobclixContacts.getInstance()     // Catch:{ Exception -> 0x02cc }
            android.content.Intent r0 = r1.a(r0)     // Catch:{ Exception -> 0x02cc }
            r6.l = r0     // Catch:{ Exception -> 0x02cc }
            goto L_0x008d
        L_0x02cc:
            r0 = move-exception
            com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.getInstance()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.ref.SoftReference<com.mobclix.android.sdk.MobclixWebView> r0 = r0.v     // Catch:{ Throwable -> 0x00c2 }
            if (r0 == 0) goto L_0x02f6
            com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.getInstance()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.ref.SoftReference<com.mobclix.android.sdk.MobclixWebView> r0 = r0.v     // Catch:{ Throwable -> 0x00c2 }
            java.lang.Object r0 = r0.get()     // Catch:{ Throwable -> 0x00c2 }
            if (r0 == 0) goto L_0x02f6
            com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.getInstance()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.ref.SoftReference<com.mobclix.android.sdk.MobclixWebView> r0 = r0.v     // Catch:{ Throwable -> 0x00c2 }
            java.lang.Object r0 = r0.get()     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixWebView r0 = (com.mobclix.android.sdk.MobclixWebView) r0     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixJavascriptInterface r0 = r0.c()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r1 = "Error getting contact."
            r0.c(r1)     // Catch:{ Throwable -> 0x00c2 }
        L_0x02f6:
            r6.finish()     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x008d
        L_0x02fb:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Throwable -> 0x00c2 }
            r3.<init>(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r4 = ".type"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r2 = r2.getString(r3)     // Catch:{ Throwable -> 0x00c2 }
            java.lang.String r3 = "html5Video"
            boolean r2 = r2.equals(r3)     // Catch:{ Throwable -> 0x00c2 }
            if (r2 == 0) goto L_0x006e
            r2 = 10
            r6.h = r2     // Catch:{ Throwable -> 0x00c2 }
            android.content.res.Resources r2 = r6.getResources()     // Catch:{ Throwable -> 0x00c2 }
            android.content.res.Configuration r2 = r2.getConfiguration()     // Catch:{ Throwable -> 0x00c2 }
            int r2 = r2.orientation     // Catch:{ Throwable -> 0x00c2 }
            if (r2 != r5) goto L_0x0352
        L_0x032e:
            r6.setRequestedOrientation(r0)     // Catch:{ Throwable -> 0x00c2 }
            r0 = 1
            r6.requestWindowFeature(r0)     // Catch:{ Throwable -> 0x00c2 }
            android.view.Window r0 = r6.getWindow()     // Catch:{ Throwable -> 0x00c2 }
            r1 = 128(0x80, float:1.794E-43)
            r0.addFlags(r1)     // Catch:{ Throwable -> 0x00c2 }
            android.view.Window r0 = r6.getWindow()     // Catch:{ Throwable -> 0x00c2 }
            r1 = 1024(0x400, float:1.435E-42)
            r2 = 1024(0x400, float:1.435E-42)
            r0.setFlags(r1, r2)     // Catch:{ Throwable -> 0x00c2 }
            com.mobclix.android.sdk.MobclixBrowserActivity$MobclixHTML5Video r0 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixHTML5Video     // Catch:{ Throwable -> 0x00c2 }
            r0.<init>(r6)     // Catch:{ Throwable -> 0x00c2 }
            r6.i = r0     // Catch:{ Throwable -> 0x00c2 }
            goto L_0x006e
        L_0x0352:
            r0 = r1
            goto L_0x032e
        L_0x0354:
            r0 = move-exception
            goto L_0x008d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixBrowserActivity.onCreate(android.os.Bundle):void");
    }

    public void onStart() {
        super.onStart();
        try {
            switch (this.h) {
                default:
                    return;
                case 0:
                    if (!((MobclixVideoView) this.i).r) {
                        a();
                        ((MobclixVideoView) this.i).m = ProgressDialog.show(this, "", "Loading...", true, true);
                        ((MobclixVideoView) this.i).m.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            public void onCancel(DialogInterface dialogInterface) {
                                MobclixBrowserActivity.this.finish();
                            }
                        });
                        return;
                    }
                    ((MobclixVideoView) this.i).q.setVisibility(0);
                    ((MobclixVideoView) this.i).o.start();
                    return;
            }
        } catch (Exception e2) {
            finish();
        }
        finish();
    }

    public void onResume() {
        super.onResume();
        try {
            switch (this.h) {
                case 3:
                case 9:
                    ((MobclixFullScreen) this.i).k.c().d();
                    ((MobclixFullScreen) this.i).o = false;
                    if (!this.c) {
                        ((MobclixFullScreen) this.i).k.c().l();
                    }
                    this.c = false;
                    return;
                case 4:
                    if (Mobclix.getInstance().v == null || Mobclix.getInstance().v.get() != null) {
                        finish();
                        return;
                    } else if (this.l != null) {
                        startActivityForResult(this.l, this.h);
                        return;
                    } else {
                        finish();
                        return;
                    }
                case 5:
                    if (this.l != null) {
                        startActivityForResult(Intent.createChooser(this.l, "Select Picture"), this.h);
                        return;
                    } else {
                        finish();
                        return;
                    }
                case 6:
                    if (this.l != null) {
                        startActivityForResult(Intent.createChooser(this.l, "Select Picture"), this.h);
                        return;
                    } else {
                        finish();
                        return;
                    }
                case 7:
                    startActivityForResult(MobclixContacts.getInstance().a(), this.h);
                    return;
                case 8:
                    try {
                        startActivityForResult(this.l, this.h);
                        return;
                    } catch (Exception e2) {
                        if (Mobclix.getInstance().v == null || Mobclix.getInstance().v.get() != null) {
                            Mobclix.getInstance().v.get().c().c("Error getting contact.");
                        }
                        finish();
                        return;
                    }
                default:
                    return;
            }
        } catch (Exception e3) {
            finish();
        }
        finish();
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        try {
            if (Mobclix.getInstance().v == null || Mobclix.getInstance().v.get() == null) {
                finish();
            }
            MobclixWebView mobclixWebView = Mobclix.getInstance().v.get();
            if (i2 == 4) {
                if (i3 == -1) {
                    try {
                        Bitmap decodeFile = BitmapFactory.decodeFile(convertImageUriToFile(this.d, this).getAbsolutePath());
                        int i4 = mobclixWebView.c().h;
                        int i5 = mobclixWebView.c().g;
                        if (!(i4 == 0 || i5 == 0)) {
                            decodeFile = Bitmap.createScaledBitmap(decodeFile, mobclixWebView.c().g, mobclixWebView.c().h, true);
                        }
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        decodeFile.compress(Bitmap.CompressFormat.JPEG, 10, byteArrayOutputStream);
                        mobclixWebView.c().a(Base64.encodeBytes(byteArrayOutputStream.toByteArray()));
                        byteArrayOutputStream.close();
                        System.gc();
                    } catch (Exception e2) {
                        mobclixWebView.c().b(e2.toString());
                        mobclixWebView.c().b("Error processing photo.");
                    }
                } else if (i3 == 0) {
                    mobclixWebView.c().b("User canceled.");
                }
            } else if (i2 == 5) {
                if (i3 == -1) {
                    try {
                        Uri data = intent.getData();
                        String path = data.getPath();
                        String a2 = a(data);
                        if (a2 != null) {
                            path = a2;
                        }
                        File file = new File(path);
                        Bitmap decodeFile2 = BitmapFactory.decodeFile(file.getAbsolutePath());
                        int i6 = mobclixWebView.c().h;
                        int i7 = mobclixWebView.c().g;
                        if (!(i6 == 0 || i7 == 0)) {
                            decodeFile2 = Bitmap.createScaledBitmap(decodeFile2, mobclixWebView.c().g, mobclixWebView.c().h, true);
                        }
                        ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
                        decodeFile2.compress(Bitmap.CompressFormat.JPEG, 10, byteArrayOutputStream2);
                        mobclixWebView.c().a(Base64.encodeBytes(byteArrayOutputStream2.toByteArray()));
                        byteArrayOutputStream2.close();
                        System.gc();
                        file.delete();
                    } catch (Exception e3) {
                        mobclixWebView.c().b(e3.toString());
                        mobclixWebView.c().b("Error processing photo.");
                    }
                } else if (i3 == 0) {
                    mobclixWebView.c().b("User canceled.");
                }
            } else if (i2 == 6) {
                if (i3 == -1) {
                    try {
                        Uri data2 = intent.getData();
                        String path2 = data2.getPath();
                        String a3 = a(data2);
                        if (a3 != null) {
                            path2 = a3;
                        }
                        mobclixWebView.c().d(path2);
                    } catch (Exception e4) {
                        mobclixWebView.c().b(e4.toString());
                        mobclixWebView.c().b("Error processing photo.");
                    }
                } else if (i3 == 0) {
                    mobclixWebView.c().b("User canceled.");
                }
            } else if (i2 == 7) {
                if (i3 == -1) {
                    try {
                        mobclixWebView.c().a(intent.getData());
                    } catch (Exception e5) {
                        mobclixWebView.c().c("Error getting contact.");
                    }
                } else if (i3 == 0) {
                    mobclixWebView.c().c("User canceled.");
                }
            } else if (i2 == 8) {
                if (i3 == -1) {
                    try {
                        mobclixWebView.c().f();
                    } catch (Exception e6) {
                        mobclixWebView.c().c("Error getting contact.");
                    }
                } else if (i3 == 0) {
                    mobclixWebView.c().c("User canceled.");
                }
            }
            Mobclix.getInstance().v = null;
            finish();
        } catch (Exception e7) {
            finish();
        }
    }

    public String a(Uri uri) {
        Cursor managedQuery = managedQuery(uri, new String[]{"_data"}, null, null, null);
        if (managedQuery == null) {
            return null;
        }
        int columnIndexOrThrow = managedQuery.getColumnIndexOrThrow("_data");
        managedQuery.moveToFirst();
        return managedQuery.getString(columnIndexOrThrow);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0041  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.io.File convertImageUriToFile(android.net.Uri r7, android.app.Activity r8) {
        /*
            r6 = 0
            r0 = 3
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ all -> 0x003e }
            r0 = 0
            java.lang.String r1 = "_data"
            r2[r0] = r1     // Catch:{ all -> 0x003e }
            r0 = 1
            java.lang.String r1 = "_id"
            r2[r0] = r1     // Catch:{ all -> 0x003e }
            r0 = 2
            java.lang.String r1 = "orientation"
            r2[r0] = r1     // Catch:{ all -> 0x003e }
            r3 = 0
            r4 = 0
            r5 = 0
            r0 = r8
            r1 = r7
            android.database.Cursor r1 = r0.managedQuery(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x003e }
            java.lang.String r0 = "_data"
            int r2 = r1.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x0045 }
            boolean r0 = r1.moveToFirst()     // Catch:{ all -> 0x0045 }
            if (r0 == 0) goto L_0x0037
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x0045 }
            java.lang.String r2 = r1.getString(r2)     // Catch:{ all -> 0x0045 }
            r0.<init>(r2)     // Catch:{ all -> 0x0045 }
            if (r1 == 0) goto L_0x0036
            r1.close()
        L_0x0036:
            return r0
        L_0x0037:
            if (r1 == 0) goto L_0x003c
            r1.close()
        L_0x003c:
            r0 = r6
            goto L_0x0036
        L_0x003e:
            r0 = move-exception
        L_0x003f:
            if (r6 == 0) goto L_0x0044
            r6.close()
        L_0x0044:
            throw r0
        L_0x0045:
            r0 = move-exception
            r6 = r1
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixBrowserActivity.convertImageUriToFile(android.net.Uri, android.app.Activity):java.io.File");
    }

    public void onSaveInstanceState(Bundle bundle) {
        if (this.d != null) {
            bundle.putParcelable("imageUri", this.d);
        }
        super.onSaveInstanceState(bundle);
    }

    public void onRestoreInstanceState(Bundle bundle) {
        if (bundle.containsKey("imageUri")) {
            this.d = (Uri) bundle.getParcelable("imageUri");
        }
    }

    public void onStop() {
        super.onStop();
        try {
            switch (this.h) {
                case 3:
                    if (((MobclixFullScreen) this.i).o) {
                        ((MobclixFullScreen) this.i).k.c().k();
                        return;
                    } else if (((MobclixFullScreen) this.i).n) {
                        ((MobclixFullScreen) this.i).a(1);
                        ((MobclixFullScreen) this.i).k.c().k();
                        return;
                    } else {
                        return;
                    }
                case 9:
                    if (((MobclixFullScreen) this.i).o) {
                        ((MobclixFullScreen) this.i).k.c().k();
                        return;
                    } else if (((MobclixFullScreen) this.i).n) {
                        ((MobclixFullScreen) this.i).k.c().k();
                        return;
                    } else {
                        return;
                    }
                case 10:
                    try {
                        ((MobclixHTML5Video) this.i).f.stopPlayback();
                        ((MobclixHTML5Video) this.i).f = null;
                    } catch (Exception e2) {
                        try {
                            Log.v(this.e, e2.toString());
                        } catch (Exception e3) {
                        }
                    }
                    try {
                        ((MobclixHTML5Video) this.i).d.g.getClass().getMethod("onCustomViewHidden", new Class[0]).invoke(((MobclixHTML5Video) this.i).d.g, new Object[0]);
                        ((MobclixHTML5Video) this.i).d.g = null;
                    } catch (Exception e4) {
                        Log.v(this.e, e4.toString());
                    }
                    finish();
                    return;
                default:
                    return;
            }
        } catch (Exception e5) {
            finish();
        }
        finish();
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(this.b);
        } catch (Exception e2) {
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onPause() {
        super.onPause();
        try {
            switch (this.h) {
                case 0:
                    ((MobclixVideoView) this.i).m.dismiss();
                    return;
                case 1:
                case 2:
                default:
                    return;
                case 3:
                    ((MobclixExpander) this.i).k.c().c();
                    return;
            }
        } catch (Exception e2) {
            finish();
        }
        finish();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        try {
            switch (this.h) {
                case 0:
                    if (i2 == 4) {
                        finish();
                    }
                    return super.onKeyDown(i2, keyEvent);
                case 2:
                    if (i2 != 4 || !((MobclixBrowser) this.i).a().canGoBack()) {
                        return super.onKeyDown(i2, keyEvent);
                    }
                    ((MobclixBrowser) this.i).a().goBack();
                    return true;
                case 3:
                    if (i2 != 4) {
                        return super.onKeyDown(i2, keyEvent);
                    }
                    if (((MobclixExpander) this.i).k.canGoBack()) {
                        ((MobclixExpander) this.i).k.d();
                    }
                    ((MobclixExpander) this.i).a(500);
                    return true;
                case 9:
                    if (i2 != 4) {
                        return super.onKeyDown(i2, keyEvent);
                    }
                    if (!((MobclixFullScreenAd) this.i).k.canGoBack()) {
                        ((MobclixFullScreenAd) this.i).a(0);
                    } else if (((MobclixFullScreenAd) this.i).k.canGoBackOrForward(-2)) {
                        ((MobclixFullScreenAd) this.i).k.goBack();
                    } else {
                        ((MobclixFullScreenAd) this.i).k.d();
                        ((MobclixFullScreenAd) this.i).k.clearHistory();
                    }
                    return true;
                default:
                    return super.onKeyDown(i2, keyEvent);
            }
        } catch (Exception e2) {
            return super.onKeyDown(i2, keyEvent);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            switch (this.h) {
                case 2:
                case 9:
                    super.onCreateOptionsMenu(menu);
                    menu.add(0, 0, 0, "Bookmark").setIcon(17301555);
                    menu.add(0, 1, 0, "Forward").setIcon(17301565);
                    menu.add(0, 2, 0, "Close").setIcon(17301560);
                    return true;
                default:
                    return false;
            }
        } catch (Exception e2) {
            return false;
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        try {
            switch (this.h) {
                case 2:
                    switch (menuItem.getItemId()) {
                        case 0:
                            Browser.saveBookmark(this, ((MobclixBrowser) this.i).a().getTitle(), ((MobclixBrowser) this.i).a().getUrl());
                            return true;
                        case 1:
                            if (((MobclixBrowser) this.i).a().canGoForward()) {
                                ((MobclixBrowser) this.i).a().goForward();
                            }
                            return true;
                        case 2:
                            finish();
                            return true;
                        default:
                            return super.onContextItemSelected(menuItem);
                    }
                case 9:
                    switch (menuItem.getItemId()) {
                        case 0:
                            Browser.saveBookmark(this, ((MobclixFullScreen) this.i).k.getTitle(), ((MobclixFullScreen) this.i).k.getUrl());
                            return true;
                        case 1:
                            if (((MobclixFullScreen) this.i).k.canGoForward()) {
                                ((MobclixFullScreen) this.i).k.goForward();
                            }
                            return true;
                        case 2:
                            ((MobclixFullScreen) this.i).a(0);
                            return true;
                        default:
                            return super.onContextItemSelected(menuItem);
                    }
                default:
                    return false;
            }
        } catch (Exception e2) {
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixBrowserActivity.MobclixVideoView.a(com.mobclix.android.sdk.MobclixBrowserActivity$MobclixVideoView, boolean):void
     arg types: [com.mobclix.android.sdk.MobclixBrowserActivity$MobclixVideoView, int]
     candidates:
      com.mobclix.android.sdk.MobclixBrowserActivity.MobclixVideoView.a(com.mobclix.android.sdk.MobclixBrowserActivity$MobclixVideoView, android.app.ProgressDialog):void
      com.mobclix.android.sdk.MobclixBrowserActivity.MobclixVideoView.a(com.mobclix.android.sdk.MobclixBrowserActivity$MobclixVideoView, boolean):void */
    public void a() {
        try {
            if (!this.k.isEmpty()) {
                this.k.removeFirst().start();
                return;
            }
            switch (this.h) {
                case 0:
                    ((MobclixVideoView) this.i).r = true;
                    ((MobclixVideoView) this.i).m.dismiss();
                    ((MobclixVideoView) this.i).b();
                    ((MobclixVideoView) this.i).o.setVisibility(0);
                    ((MobclixVideoView) this.i).o.start();
                    return;
                default:
                    return;
            }
        } catch (Exception e2) {
            finish();
        }
    }

    class ResourceResponseHandler extends Handler {
        ResourceResponseHandler() {
        }

        public void handleMessage(Message message) {
            MobclixBrowserActivity.this.a();
        }
    }

    private class MobclixVideoView extends RelativeLayout implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
        /* access modifiers changed from: package-private */
        public final /* synthetic */ MobclixBrowserActivity a;
        private Activity b;
        private String c = "";
        private String d;
        private String e = "";
        private String f = "";
        private ArrayList<String> g = new ArrayList<>();
        private ArrayList<String> h = new ArrayList<>();
        /* access modifiers changed from: private */
        public ArrayList<Bitmap> i = new ArrayList<>();
        private ArrayList<String> j = new ArrayList<>();
        private LinearLayout k = null;
        private LinearLayout l = null;
        /* access modifiers changed from: private */
        public ProgressDialog m = null;
        /* access modifiers changed from: private */
        public ImageView n = null;
        /* access modifiers changed from: private */
        public VideoView o;
        private MediaController p;
        /* access modifiers changed from: private */
        public ImageView q;
        /* access modifiers changed from: private */
        public boolean r = false;
        /* access modifiers changed from: private */
        public boolean s = false;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        MobclixVideoView(MobclixBrowserActivity mobclixBrowserActivity, Activity activity, String str) {
            super(activity);
            this.a = mobclixBrowserActivity;
            this.b = activity;
            try {
                JSONObject jSONObject = new JSONObject(str);
                try {
                    this.d = jSONObject.getString("videoUrl");
                } catch (Exception e2) {
                }
                try {
                    this.c = jSONObject.getString("landingUrl");
                } catch (Exception e3) {
                }
                try {
                    this.e = jSONObject.getString("tagline");
                } catch (Exception e4) {
                }
                try {
                    this.f = jSONObject.getString("taglineImageUrl");
                } catch (Exception e5) {
                }
                try {
                    JSONArray jSONArray = jSONObject.getJSONArray("buttons");
                    for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                        this.h.add(jSONArray.getJSONObject(i2).getString("imageUrl"));
                        this.j.add(jSONArray.getJSONObject(i2).getString("url"));
                    }
                } catch (Exception e6) {
                }
                JSONArray jSONArray2 = jSONObject.getJSONArray("trackingUrls");
                for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                    this.g.add(jSONArray2.getString(i3));
                }
            } catch (Exception e7) {
            }
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            a();
        }

        public void onPrepared(MediaPlayer mediaPlayer) {
            this.s = true;
            this.q.setVisibility(8);
            this.m.dismiss();
        }

        public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
            this.m.dismiss();
            removeView(this.o);
            return true;
        }

        public void onCompletion(MediaPlayer mediaPlayer) {
            this.q.setVisibility(0);
        }

        public void a() {
            this.a.getWindow().setFlags(1024, 1024);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            layoutParams.addRule(13);
            this.o = new VideoView(this.b);
            this.o.setId(1337);
            this.o.setLayoutParams(layoutParams);
            this.p = new MediaController(this.b);
            this.p.setAnchorView(this.o);
            this.o.setVideoURI(Uri.parse(this.d));
            this.o.setMediaController(this.p);
            this.o.setOnPreparedListener(this);
            this.o.setOnErrorListener(this);
            this.o.setOnCompletionListener(this);
            this.o.setVisibility(4);
            addView(this.o);
            this.q = new ImageView(this.b);
            this.q.setLayoutParams(layoutParams);
            this.q.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (MobclixVideoView.this.s) {
                        MobclixVideoView.this.q.setVisibility(8);
                        if (!MobclixVideoView.this.o.isPlaying()) {
                            MobclixVideoView.this.o.start();
                        }
                    }
                }
            });
            addView(this.q);
            if ((!this.f.equals("null") && !this.f.equals("")) || !this.e.equals("")) {
                LinearLayout linearLayout = new LinearLayout(this.b);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams2.addRule(11);
                linearLayout.setLayoutParams(layoutParams2);
                linearLayout.setBackgroundColor(Color.parseColor("#CC666666"));
                linearLayout.setPadding(this.a.a(4), this.a.a(4), this.a.a(4), this.a.a(4));
                if (this.f.equals("null") || this.f.equals("")) {
                    TextView textView = new TextView(this.b);
                    textView.setText(this.e);
                    linearLayout.addView(textView);
                } else {
                    this.n = new ImageView(this.b);
                    linearLayout.addView(this.n);
                    d();
                }
                addView(linearLayout);
            }
            this.a.setContentView(this);
            Iterator<String> it = this.h.iterator();
            while (it.hasNext()) {
                a(it.next());
            }
            if (!this.c.equals("")) {
                c();
            }
            Iterator<String> it2 = this.g.iterator();
            while (it2.hasNext()) {
                b(it2.next());
            }
        }

        public void b() {
            int i2 = 0;
            if (this.i.size() != 0) {
                this.k = new LinearLayout(this.b);
                this.k.setOrientation(1);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
                layoutParams.addRule(12);
                this.k.setLayoutParams(layoutParams);
                this.k.setBackgroundColor(Color.parseColor("#CC666666"));
                ImageView imageView = new ImageView(this.b);
                imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                imageView.setBackgroundResource(17301524);
                this.k.addView(imageView);
                this.l = new LinearLayout(this.b);
                this.l.setPadding(0, this.a.a(4), 0, 0);
                while (true) {
                    int i3 = i2;
                    if (i3 >= this.i.size()) {
                        this.k.addView(this.l);
                        addView(this.k);
                        return;
                    }
                    ImageView imageView2 = new ImageView(this.b);
                    Bitmap bitmap = this.i.get(i3);
                    LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(this.a.a(bitmap.getWidth()), this.a.a(bitmap.getHeight()));
                    layoutParams2.weight = 1.0f;
                    imageView2.setLayoutParams(layoutParams2);
                    imageView2.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    imageView2.setImageBitmap(bitmap);
                    imageView2.setOnClickListener(new ButtonOnClickListener(this.b, this.j.get(i3)));
                    this.l.addView(imageView2);
                    i2 = i3 + 1;
                }
            }
        }

        public void c() {
            this.a.k.add(new Thread(new Mobclix.FetchImageThread(this.c, new Mobclix.BitmapHandler() {
                public void handleMessage(Message message) {
                    if (this.a != null) {
                        MobclixVideoView.this.q.setImageBitmap(this.a);
                    }
                    MobclixVideoView.this.a.j.sendEmptyMessage(0);
                }
            })));
        }

        public void d() {
            this.a.k.add(new Thread(new Mobclix.FetchImageThread(this.f, new Mobclix.BitmapHandler() {
                public void handleMessage(Message message) {
                    if (this.a != null) {
                        int width = this.a.getWidth();
                        int height = this.a.getHeight();
                        MobclixVideoView.this.n.setLayoutParams(new LinearLayout.LayoutParams(MobclixVideoView.this.a.a(width), MobclixVideoView.this.a.a(height)));
                        MobclixVideoView.this.n.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        MobclixVideoView.this.n.setImageBitmap(this.a);
                    }
                    MobclixVideoView.this.a.j.sendEmptyMessage(0);
                }
            })));
        }

        public void a(String str) {
            this.a.k.add(new Thread(new Mobclix.FetchImageThread(str, new Mobclix.BitmapHandler() {
                public void handleMessage(Message message) {
                    if (this.a != null) {
                        MobclixVideoView.this.i.add(this.a);
                    }
                    MobclixVideoView.this.a.j.sendEmptyMessage(0);
                }
            })));
        }

        public void b(String str) {
            this.a.k.add(new Thread(new Mobclix.FetchImageThread(str, new Mobclix.BitmapHandler() {
                public void handleMessage(Message message) {
                    MobclixVideoView.this.a.j.sendEmptyMessage(0);
                }
            })));
        }

        class ButtonOnClickListener implements View.OnClickListener {
            private Context b;
            private String c;

            public ButtonOnClickListener(Context context, String str) {
                this.b = context;
                this.c = str;
            }

            public void onClick(View view) {
                this.b.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.c)));
            }
        }
    }

    private class MobclixHTML5Video extends RelativeLayout implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
        private Activity b;
        private ProgressBar c = null;
        /* access modifiers changed from: private */
        public MobclixWebView d = null;
        private FrameLayout e = null;
        /* access modifiers changed from: private */
        public VideoView f = null;
        private MediaController g = null;

        MobclixHTML5Video(Activity activity) {
            super(activity);
            try {
                this.b = activity;
                setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
                setBackgroundColor(-16777216);
                this.c = new ProgressBar(this.b);
                this.d = Mobclix.getInstance().v.get();
                Mobclix.getInstance().v = null;
                this.e = (FrameLayout) Mobclix.getInstance().w.get();
                Mobclix.getInstance().w = null;
                if (this.e == null) {
                    activity.finish();
                }
                this.f = (VideoView) this.e.getFocusedChild();
                this.f.setOnPreparedListener(this);
                this.f.setOnCompletionListener(this);
                this.f.setOnErrorListener(this);
                this.e.removeView(this.f);
                this.g = new MediaController(this.b);
                this.g.setAnchorView(this.f);
                this.f.setMediaController(this.g);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                layoutParams.addRule(14);
                layoutParams.addRule(15);
                this.f.setLayoutParams(layoutParams);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams2.addRule(14);
                layoutParams2.addRule(15);
                this.c.setLayoutParams(layoutParams2);
                Mobclix.getInstance().w = null;
                addView(this.f);
                addView(this.c);
                this.f.start();
                this.f.seekTo(0);
            } catch (Exception e2) {
                activity.finish();
            }
        }

        public void onPrepared(MediaPlayer mediaPlayer) {
            this.c.setVisibility(8);
        }

        public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
            this.b.finish();
            return true;
        }

        public void onCompletion(MediaPlayer mediaPlayer) {
            this.b.finish();
        }
    }

    private class MobclixBrowser extends RelativeLayout {
        /* access modifiers changed from: private */
        public Activity b;
        private String c;
        private String d = "";
        private String e = "standard";
        private WebView f;

        /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
            r6.c = "http://www.mobclix.com";
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
            r6.c = "http://www.mobclix.com";
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x00fd, code lost:
            r6.e = "standard";
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00e9 A[ExcHandler: JSONException (e org.json.JSONException), Splitter:B:3:0x000f] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        MobclixBrowser(android.app.Activity r8, java.lang.String r9) {
            /*
                r6 = this;
                com.mobclix.android.sdk.MobclixBrowserActivity.this = r7
                r6.<init>(r8)
                java.lang.String r0 = ""
                r6.d = r0
                java.lang.String r0 = "standard"
                r6.e = r0
                r6.b = r8     // Catch:{ Exception -> 0x00f0 }
                org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ JSONException -> 0x00e9 }
                r0.<init>(r9)     // Catch:{ JSONException -> 0x00e9 }
                java.lang.String r1 = "url"
                java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x00e2, JSONException -> 0x00e9 }
                r6.c = r1     // Catch:{ Exception -> 0x00e2, JSONException -> 0x00e9 }
            L_0x001c:
                java.lang.String r1 = "cachedHTML"
                java.lang.String r1 = r0.getString(r1)     // Catch:{ Exception -> 0x00f5, JSONException -> 0x00e9 }
                r6.d = r1     // Catch:{ Exception -> 0x00f5, JSONException -> 0x00e9 }
            L_0x0024:
                java.lang.String r1 = "browserType"
                java.lang.String r0 = r0.getString(r1)     // Catch:{ Exception -> 0x00fc, JSONException -> 0x00e9 }
                r6.e = r0     // Catch:{ Exception -> 0x00fc, JSONException -> 0x00e9 }
            L_0x002c:
                android.webkit.WebView r0 = new android.webkit.WebView     // Catch:{ Exception -> 0x00f0 }
                r0.<init>(r8)     // Catch:{ Exception -> 0x00f0 }
                r6.f = r0     // Catch:{ Exception -> 0x00f0 }
                r0 = 1
                r7.setProgressBarVisibility(r0)     // Catch:{ Exception -> 0x00f0 }
                android.webkit.WebView r0 = r6.f     // Catch:{ Exception -> 0x00f0 }
                android.webkit.WebSettings r0 = r0.getSettings()     // Catch:{ Exception -> 0x00f0 }
                r1 = 1
                r0.setJavaScriptEnabled(r1)     // Catch:{ Exception -> 0x00f0 }
                android.webkit.WebView r0 = r6.f     // Catch:{ Exception -> 0x00f0 }
                com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser$1 r1 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser$1     // Catch:{ Exception -> 0x00f0 }
                r1.<init>()     // Catch:{ Exception -> 0x00f0 }
                r0.setWebViewClient(r1)     // Catch:{ Exception -> 0x00f0 }
                android.webkit.WebView r0 = r6.f     // Catch:{ Exception -> 0x00f0 }
                com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser$2 r1 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser$2     // Catch:{ Exception -> 0x00f0 }
                r1.<init>()     // Catch:{ Exception -> 0x00f0 }
                r0.setWebChromeClient(r1)     // Catch:{ Exception -> 0x00f0 }
                android.webkit.WebView r0 = r6.f     // Catch:{ Exception -> 0x00f0 }
                android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x00f0 }
                r2 = -1
                r3 = -1
                r1.<init>(r2, r3)     // Catch:{ Exception -> 0x00f0 }
                r0.setLayoutParams(r1)     // Catch:{ Exception -> 0x00f0 }
                java.lang.String r0 = r6.d     // Catch:{ Exception -> 0x00f0 }
                java.lang.String r1 = ""
                boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x00f0 }
                if (r0 == 0) goto L_0x0103
                android.webkit.WebView r0 = r6.f     // Catch:{ Exception -> 0x00f0 }
                java.lang.String r1 = r6.c     // Catch:{ Exception -> 0x00f0 }
                r0.loadUrl(r1)     // Catch:{ Exception -> 0x00f0 }
            L_0x0072:
                android.webkit.WebView r0 = r6.f     // Catch:{ Exception -> 0x00f0 }
                r6.addView(r0)     // Catch:{ Exception -> 0x00f0 }
                java.lang.String r0 = r6.e     // Catch:{ Exception -> 0x00f0 }
                java.lang.String r1 = "minimal"
                boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x00f0 }
                if (r0 == 0) goto L_0x00e1
                android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ Exception -> 0x00f0 }
                r1 = 30
                int r1 = r7.a(r1)     // Catch:{ Exception -> 0x00f0 }
                r2 = 30
                int r2 = r7.a(r2)     // Catch:{ Exception -> 0x00f0 }
                r0.<init>(r1, r2)     // Catch:{ Exception -> 0x00f0 }
                r1 = 5
                int r1 = r7.a(r1)     // Catch:{ Exception -> 0x00f0 }
                r2 = 5
                int r2 = r7.a(r2)     // Catch:{ Exception -> 0x00f0 }
                r3 = 0
                r4 = 0
                r0.setMargins(r1, r2, r3, r4)     // Catch:{ Exception -> 0x00f0 }
                android.widget.ImageView r1 = new android.widget.ImageView     // Catch:{ Exception -> 0x00f0 }
                r1.<init>(r7)     // Catch:{ Exception -> 0x00f0 }
                r1.setLayoutParams(r0)     // Catch:{ Exception -> 0x00f0 }
                r0 = 17301594(0x108005a, float:2.4979507E-38)
                r1.setImageResource(r0)     // Catch:{ Exception -> 0x00f0 }
                android.graphics.drawable.ShapeDrawable r0 = new android.graphics.drawable.ShapeDrawable     // Catch:{ Exception -> 0x00f0 }
                android.graphics.drawable.shapes.ArcShape r2 = new android.graphics.drawable.shapes.ArcShape     // Catch:{ Exception -> 0x00f0 }
                r3 = 0
                r4 = 1135869952(0x43b40000, float:360.0)
                r2.<init>(r3, r4)     // Catch:{ Exception -> 0x00f0 }
                r0.<init>(r2)     // Catch:{ Exception -> 0x00f0 }
                r2 = -7
                int r2 = r7.a(r2)     // Catch:{ Exception -> 0x00f0 }
                r3 = -7
                int r3 = r7.a(r3)     // Catch:{ Exception -> 0x00f0 }
                r4 = -7
                int r4 = r7.a(r4)     // Catch:{ Exception -> 0x00f0 }
                r5 = -7
                int r5 = r7.a(r5)     // Catch:{ Exception -> 0x00f0 }
                r0.setPadding(r2, r3, r4, r5)     // Catch:{ Exception -> 0x00f0 }
                r1.setBackgroundDrawable(r0)     // Catch:{ Exception -> 0x00f0 }
                com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser$3 r0 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixBrowser$3     // Catch:{ Exception -> 0x00f0 }
                r0.<init>()     // Catch:{ Exception -> 0x00f0 }
                r1.setOnClickListener(r0)     // Catch:{ Exception -> 0x00f0 }
                r6.addView(r1)     // Catch:{ Exception -> 0x00f0 }
            L_0x00e1:
                return
            L_0x00e2:
                r1 = move-exception
                java.lang.String r1 = "http://www.mobclix.com"
                r6.c = r1     // Catch:{ JSONException -> 0x00e9 }
                goto L_0x001c
            L_0x00e9:
                r0 = move-exception
                java.lang.String r0 = "http://www.mobclix.com"
                r6.c = r0     // Catch:{ Exception -> 0x00f0 }
                goto L_0x002c
            L_0x00f0:
                r0 = move-exception
                r8.finish()
                goto L_0x00e1
            L_0x00f5:
                r1 = move-exception
                java.lang.String r1 = ""
                r6.d = r1     // Catch:{ JSONException -> 0x00e9 }
                goto L_0x0024
            L_0x00fc:
                r0 = move-exception
                java.lang.String r0 = "standard"
                r6.e = r0     // Catch:{ JSONException -> 0x00e9 }
                goto L_0x002c
            L_0x0103:
                android.webkit.WebView r0 = r6.f     // Catch:{ Exception -> 0x00f0 }
                java.lang.String r1 = r6.c     // Catch:{ Exception -> 0x00f0 }
                java.lang.String r2 = r6.d     // Catch:{ Exception -> 0x00f0 }
                java.lang.String r3 = "text/html"
                java.lang.String r4 = "utf-8"
                r5 = 0
                r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00f0 }
                goto L_0x0072
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixBrowserActivity.MobclixBrowser.<init>(com.mobclix.android.sdk.MobclixBrowserActivity, android.app.Activity, java.lang.String):void");
        }

        /* access modifiers changed from: private */
        public WebView a() {
            return this.f;
        }
    }

    class MobclixFullScreen extends LinearLayout {
        Activity j;
        MobclixWebView k;
        ViewGroup l;
        int m;
        boolean n = true;
        boolean o = false;

        MobclixFullScreen(Activity activity) {
            super(activity);
            int i;
            try {
                this.j = activity;
                this.k = Mobclix.getInstance().u.get();
                this.k.c().b = this;
                this.m = ((Activity) this.k.a()).getWindow().getAttributes().flags;
                this.j.getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());
                if (this.j.getResources().getConfiguration().orientation == 2) {
                    i = 0;
                } else {
                    i = 1;
                }
                this.j.setRequestedOrientation(i);
                setBackgroundColor(Color.argb(128, 256, 256, 256));
                setOrientation(1);
                setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                this.l = (ViewGroup) this.k.getParent();
            } catch (Exception e) {
                activity.finish();
            }
        }

        public synchronized void a(int i) {
        }
    }

    class MobclixFullScreenAd extends MobclixFullScreen {
        RelativeLayout a;

        MobclixFullScreenAd(Activity activity) {
            super(activity);
            try {
                ((Activity) this.k.a()).getWindow().setFlags(1024, 1024);
                this.l.removeView(this.k);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
                this.a = new RelativeLayout(MobclixBrowserActivity.this);
                this.a.setLayoutParams(layoutParams);
                addView(this.a);
                this.k.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
                this.k.f.c = true;
                this.a.addView(this.k);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(MobclixBrowserActivity.this.a(46), MobclixBrowserActivity.this.a(46));
                RelativeLayout relativeLayout = new RelativeLayout(MobclixBrowserActivity.this);
                relativeLayout.setLayoutParams(layoutParams2);
                relativeLayout.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        MobclixFullScreenAd.this.a(0);
                    }
                });
                this.a.addView(relativeLayout);
                ImageView imageView = new ImageView(MobclixBrowserActivity.this);
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(MobclixBrowserActivity.this.a(30), MobclixBrowserActivity.this.a(30));
                layoutParams3.addRule(13);
                layoutParams3.setMargins(MobclixBrowserActivity.this.a(0), MobclixBrowserActivity.this.a(4), 0, 0);
                imageView.setLayoutParams(layoutParams3);
                if (MobclixAssets.a() != null) {
                    imageView.setImageBitmap(MobclixAssets.a());
                } else {
                    imageView.setImageResource(17301594);
                    ShapeDrawable shapeDrawable = new ShapeDrawable(new ArcShape(0.0f, 360.0f));
                    shapeDrawable.setPadding(MobclixBrowserActivity.this.a(-6), MobclixBrowserActivity.this.a(-6), MobclixBrowserActivity.this.a(-6), MobclixBrowserActivity.this.a(-6));
                    imageView.setBackgroundDrawable(shapeDrawable);
                }
                relativeLayout.addView(imageView);
            } catch (Exception e) {
                activity.finish();
            }
        }

        public synchronized void a(int i) {
            try {
                if (!(Mobclix.getInstance().u == null || Mobclix.getInstance().u.get() == null)) {
                    this.k.c().b = null;
                    Mobclix.getInstance().u = null;
                    this.n = false;
                    Window window = ((Activity) this.k.a()).getWindow();
                    WindowManager.LayoutParams attributes = window.getAttributes();
                    attributes.flags = this.m;
                    window.setAttributes(attributes);
                    MobclixBrowserActivity.this.unregisterReceiver(MobclixBrowserActivity.this.b);
                    removeAllViews();
                    this.k.c().i();
                    this.k.c().c();
                    if (this.k.b != null) {
                        this.k.b.c();
                        this.k.destroy();
                        this.k = null;
                    }
                    this.j.finish();
                }
            } catch (Exception e) {
                this.j.finish();
            }
            return;
        }
    }

    class MobclixExpander extends MobclixFullScreen implements Animation.AnimationListener {
        View a;
        View b;
        int c = 0;
        int d = 0;
        int e = 0;
        int f = 0;
        int g = 0;
        int h = 0;
        final /* synthetic */ MobclixBrowserActivity i;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x00fc, code lost:
            r1 = 0;
            r2 = 0;
            r3 = 0;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x00fb A[Catch:{ Exception -> 0x0109 }, ExcHandler: JSONException (e org.json.JSONException), Splitter:B:1:0x0014] */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x0115 A[ExcHandler: JSONException (e org.json.JSONException), PHI: r0 
          PHI: (r0v23 int) = (r0v0 int), (r0v24 int), (r0v24 int) binds: [B:22:0x0045, B:25:0x004b, B:26:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:22:0x0045] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        MobclixExpander(com.mobclix.android.sdk.MobclixBrowserActivity r10, android.app.Activity r11, java.lang.String r12) {
            /*
                r9 = this;
                r0 = 0
                r9.i = r10
                r9.<init>(r11)
                r9.c = r0
                r9.d = r0
                r9.e = r0
                r9.f = r0
                r9.g = r0
                r9.h = r0
                r7 = 500(0x1f4, float:7.0E-43)
                org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ JSONException -> 0x00fb }
                r4.<init>(r12)     // Catch:{ JSONException -> 0x00fb }
                java.lang.String r1 = "statusBarHeight"
                int r1 = r4.getInt(r1)     // Catch:{ Exception -> 0x0120, JSONException -> 0x00fb }
                r9.g = r1     // Catch:{ Exception -> 0x0120, JSONException -> 0x00fb }
            L_0x0021:
                java.lang.String r1 = "topMargin"
                int r1 = r4.getInt(r1)     // Catch:{ Exception -> 0x011d, JSONException -> 0x00fb }
                r9.c = r1     // Catch:{ Exception -> 0x011d, JSONException -> 0x00fb }
            L_0x0029:
                java.lang.String r1 = "leftMargin"
                int r1 = r4.getInt(r1)     // Catch:{ Exception -> 0x011a, JSONException -> 0x00fb }
                r9.d = r1     // Catch:{ Exception -> 0x011a, JSONException -> 0x00fb }
            L_0x0031:
                java.lang.String r1 = "fTopMargin"
                int r3 = r4.getInt(r1)     // Catch:{ Exception -> 0x00e9, JSONException -> 0x00fb }
            L_0x0037:
                java.lang.String r1 = "fLeftMargin"
                int r2 = r4.getInt(r1)     // Catch:{ Exception -> 0x00ed, JSONException -> 0x010e }
            L_0x003d:
                java.lang.String r1 = "fWidth"
                int r1 = r4.getInt(r1)     // Catch:{ Exception -> 0x00f1, JSONException -> 0x0112 }
            L_0x0043:
                java.lang.String r5 = "fHeight"
                int r0 = r4.getInt(r5)     // Catch:{ Exception -> 0x0117, JSONException -> 0x0115 }
            L_0x0049:
                java.lang.String r5 = "duration"
                int r7 = r4.getInt(r5)     // Catch:{ Exception -> 0x00f5, JSONException -> 0x0115 }
                r6 = r0
                r5 = r1
                r4 = r3
            L_0x0052:
                int r0 = r9.c     // Catch:{ Exception -> 0x0109 }
                int r1 = r9.g     // Catch:{ Exception -> 0x0109 }
                int r0 = r0 - r1
                r9.c = r0     // Catch:{ Exception -> 0x0109 }
                com.mobclix.android.sdk.MobclixBrowserActivity$MobclixExpander$1 r0 = new com.mobclix.android.sdk.MobclixBrowserActivity$MobclixExpander$1     // Catch:{ Exception -> 0x0109 }
                r0.<init>()     // Catch:{ Exception -> 0x0109 }
                r9.setOnClickListener(r0)     // Catch:{ Exception -> 0x0109 }
                android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x0109 }
                r1 = 1
                int r3 = r9.c     // Catch:{ Exception -> 0x0109 }
                r0.<init>(r1, r3)     // Catch:{ Exception -> 0x0109 }
                android.view.View r1 = new android.view.View     // Catch:{ Exception -> 0x0109 }
                android.app.Activity r3 = r9.j     // Catch:{ Exception -> 0x0109 }
                r1.<init>(r3)     // Catch:{ Exception -> 0x0109 }
                r9.a = r1     // Catch:{ Exception -> 0x0109 }
                android.view.View r1 = r9.a     // Catch:{ Exception -> 0x0109 }
                r1.setLayoutParams(r0)     // Catch:{ Exception -> 0x0109 }
                android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x0109 }
                r1 = -2
                r3 = -2
                r0.<init>(r1, r3)     // Catch:{ Exception -> 0x0109 }
                android.widget.LinearLayout r1 = new android.widget.LinearLayout     // Catch:{ Exception -> 0x0109 }
                android.app.Activity r3 = r9.j     // Catch:{ Exception -> 0x0109 }
                r1.<init>(r3)     // Catch:{ Exception -> 0x0109 }
                r1.setLayoutParams(r0)     // Catch:{ Exception -> 0x0109 }
                android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x0109 }
                int r3 = r9.d     // Catch:{ Exception -> 0x0109 }
                r8 = 1
                r0.<init>(r3, r8)     // Catch:{ Exception -> 0x0109 }
                android.view.View r3 = new android.view.View     // Catch:{ Exception -> 0x0109 }
                android.app.Activity r8 = r9.j     // Catch:{ Exception -> 0x0109 }
                r3.<init>(r8)     // Catch:{ Exception -> 0x0109 }
                r9.b = r3     // Catch:{ Exception -> 0x0109 }
                android.view.View r3 = r9.b     // Catch:{ Exception -> 0x0109 }
                r3.setLayoutParams(r0)     // Catch:{ Exception -> 0x0109 }
                com.mobclix.android.sdk.MobclixWebView r0 = r9.k     // Catch:{ Exception -> 0x0109 }
                android.view.ViewParent r0 = r0.getParent()     // Catch:{ Exception -> 0x0109 }
                android.view.ViewGroup r0 = (android.view.ViewGroup) r0     // Catch:{ Exception -> 0x0109 }
                r9.l = r0     // Catch:{ Exception -> 0x0109 }
                android.view.ViewGroup r0 = r9.l     // Catch:{ Exception -> 0x0109 }
                com.mobclix.android.sdk.MobclixWebView r3 = r9.k     // Catch:{ Exception -> 0x0109 }
                r0.removeView(r3)     // Catch:{ Exception -> 0x0109 }
                com.mobclix.android.sdk.MobclixWebView r0 = r9.k     // Catch:{ Exception -> 0x0109 }
                int r0 = r0.getWidth()     // Catch:{ Exception -> 0x0109 }
                r9.e = r0     // Catch:{ Exception -> 0x0109 }
                com.mobclix.android.sdk.MobclixWebView r0 = r9.k     // Catch:{ Exception -> 0x0109 }
                int r0 = r0.getHeight()     // Catch:{ Exception -> 0x0109 }
                r9.f = r0     // Catch:{ Exception -> 0x0109 }
                android.widget.LinearLayout$LayoutParams r0 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x0109 }
                int r3 = r9.e     // Catch:{ Exception -> 0x0109 }
                int r8 = r9.f     // Catch:{ Exception -> 0x0109 }
                r0.<init>(r3, r8)     // Catch:{ Exception -> 0x0109 }
                com.mobclix.android.sdk.MobclixWebView r3 = r9.k     // Catch:{ Exception -> 0x0109 }
                r3.setLayoutParams(r0)     // Catch:{ Exception -> 0x0109 }
                android.view.View r0 = r9.a     // Catch:{ Exception -> 0x0109 }
                r9.addView(r0)     // Catch:{ Exception -> 0x0109 }
                r9.addView(r1)     // Catch:{ Exception -> 0x0109 }
                android.view.View r0 = r9.b     // Catch:{ Exception -> 0x0109 }
                r1.addView(r0)     // Catch:{ Exception -> 0x0109 }
                com.mobclix.android.sdk.MobclixWebView r0 = r9.k     // Catch:{ Exception -> 0x0109 }
                r1.addView(r0)     // Catch:{ Exception -> 0x0109 }
                int r1 = r9.d     // Catch:{ Exception -> 0x0109 }
                int r3 = r9.c     // Catch:{ Exception -> 0x0109 }
                r0 = r9
                r8 = r9
                r0.a(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0109 }
            L_0x00e8:
                return
            L_0x00e9:
                r1 = move-exception
                r3 = r0
                goto L_0x0037
            L_0x00ed:
                r1 = move-exception
                r2 = r0
                goto L_0x003d
            L_0x00f1:
                r1 = move-exception
                r1 = r0
                goto L_0x0043
            L_0x00f5:
                r4 = move-exception
                r6 = r0
                r5 = r1
                r4 = r3
                goto L_0x0052
            L_0x00fb:
                r1 = move-exception
                r1 = r0
                r2 = r0
                r3 = r0
            L_0x00ff:
                android.app.Activity r4 = r9.j     // Catch:{ Exception -> 0x0109 }
                r4.finish()     // Catch:{ Exception -> 0x0109 }
                r6 = r0
                r5 = r1
                r4 = r3
                goto L_0x0052
            L_0x0109:
                r0 = move-exception
                r11.finish()
                goto L_0x00e8
            L_0x010e:
                r1 = move-exception
                r1 = r0
                r2 = r0
                goto L_0x00ff
            L_0x0112:
                r1 = move-exception
                r1 = r0
                goto L_0x00ff
            L_0x0115:
                r4 = move-exception
                goto L_0x00ff
            L_0x0117:
                r5 = move-exception
                goto L_0x0049
            L_0x011a:
                r1 = move-exception
                goto L_0x0031
            L_0x011d:
                r1 = move-exception
                goto L_0x0029
            L_0x0120:
                r1 = move-exception
                goto L_0x0021
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixBrowserActivity.MobclixExpander.<init>(com.mobclix.android.sdk.MobclixBrowserActivity, android.app.Activity, java.lang.String):void");
        }

        public synchronized void a(int i2) {
            try {
                if (!(Mobclix.getInstance().u == null || Mobclix.getInstance().u.get() == null)) {
                    this.k.c().b = null;
                    Mobclix.getInstance().u = null;
                    this.n = false;
                    Window window = ((Activity) this.k.a()).getWindow();
                    WindowManager.LayoutParams attributes = window.getAttributes();
                    attributes.flags = this.m;
                    window.setAttributes(attributes);
                    this.k.c().i();
                    a(this.d, this.c, this.e, this.f, i2, this);
                }
            } catch (Exception e2) {
                this.j.finish();
            }
            return;
        }

        public void a(int i2, int i3, int i4, int i5, int i6, Animation.AnimationListener animationListener) {
            a(this.b.getWidth(), i2, this.a.getHeight(), i3, i4, i5, i6, animationListener);
        }

        public void a(int i2, int i3, int i4, int i5, int i6, int i7, int i8, Animation.AnimationListener animationListener) {
            int i9;
            int i10;
            int i11;
            int i12;
            if (i8 < 0) {
                i9 = 0;
            } else {
                i9 = i8;
            }
            if (i9 > 3000) {
                i10 = 3000;
            } else {
                i10 = i9;
            }
            try {
                if (this.n) {
                    if (i4 < 0) {
                        i4 = 0;
                    }
                    if (i2 < 0) {
                        i2 = 0;
                    }
                    if (i5 < 0) {
                        i5 = 0;
                    }
                    if (i3 < 0) {
                        i3 = 0;
                    }
                    if (i6 < 0) {
                        i11 = 1;
                    } else {
                        i11 = i6;
                    }
                    if (i7 < 0) {
                        i12 = 1;
                    } else {
                        i12 = i7;
                    }
                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    this.i.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    i6 = displayMetrics.widthPixels;
                    i7 = displayMetrics.heightPixels;
                    if (i11 > i6) {
                        i3 = 0;
                    } else if (i11 + i3 > i6) {
                        i3 = i6 - i11;
                        i6 = i11;
                    } else {
                        i6 = i11;
                    }
                    if (i12 > i7) {
                        i5 = 0;
                    } else if (i12 + i5 > i7) {
                        i5 = i7 - i12;
                        i7 = i12;
                    } else {
                        i7 = i12;
                    }
                }
                ExpandAnimation expandAnimation = new ExpandAnimation(this.a, 1.0f, 1.0f, (float) i4, (float) i5);
                expandAnimation.setDuration((long) i10);
                ExpandAnimation expandAnimation2 = new ExpandAnimation(this.b, (float) i2, (float) i3, 1.0f, 1.0f);
                expandAnimation2.setDuration((long) i10);
                ExpandAnimation expandAnimation3 = new ExpandAnimation(this.k, (float) this.k.getWidth(), (float) i6, (float) this.k.getHeight(), (float) i7);
                expandAnimation3.setDuration((long) i10);
                expandAnimation3.setAnimationListener(this);
                this.a.startAnimation(expandAnimation);
                this.b.startAnimation(expandAnimation2);
                this.k.startAnimation(expandAnimation3);
            } catch (Exception e2) {
                this.j.finish();
            }
        }

        public void onAnimationEnd(Animation animation) {
            try {
                if (!this.n) {
                    this.k.f.c = false;
                    ((ViewGroup) this.k.getParent()).removeView(this.k);
                    this.l.addView(this.k);
                    this.i.unregisterReceiver(this.i.b);
                    this.j.finish();
                    return;
                }
                if (this.a.getHeight() <= this.g) {
                    ((Activity) this.k.a()).getWindow().setFlags(1024, 1024);
                }
                this.k.c().d();
                this.k.c().h();
            } catch (Exception e2) {
                this.j.finish();
            }
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    class ExpandAnimation extends Animation {
        View a;
        float b;
        float c;
        float d;
        float e;

        ExpandAnimation(View view, float f2, float f3, float f4, float f5) {
            this.a = view;
            this.b = f4;
            this.c = f5 - this.b;
            this.d = f2;
            this.e = f3 - this.d;
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float f2, Transformation transformation) {
            super.applyTransformation(f2, transformation);
            ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
            layoutParams.height = (int) (this.b + (this.c * f2));
            layoutParams.width = (int) (this.d + (this.e * f2));
            this.a.setLayoutParams(layoutParams);
        }
    }

    public class ScreenReceiver extends BroadcastReceiver {
        public boolean a = false;

        public ScreenReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            try {
                if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
                    ((MobclixFullScreen) MobclixBrowserActivity.this.i).k.c().k();
                } else {
                    intent.getAction().equals("android.intent.action.SCREEN_ON");
                }
            } catch (Exception e) {
            }
        }
    }
}
