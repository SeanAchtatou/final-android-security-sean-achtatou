package com.mobclix.android.sdk;

import android.content.Entity;
import android.database.Cursor;
import android.os.RemoteException;

public abstract class MobclixContactsCursorEntityIterator implements MobclixContactsEntityIterator {
    private final Cursor a;
    private boolean b = false;

    public abstract Entity a(Cursor cursor) throws RemoteException;

    public MobclixContactsCursorEntityIterator(Cursor cursor) {
        this.a = cursor;
        this.a.moveToFirst();
    }

    public final boolean hasNext() {
        if (!this.b) {
            return !this.a.isAfterLast();
        }
        throw new IllegalStateException("calling hasNext() when the iterator is closed");
    }

    /* renamed from: a */
    public Entity next() {
        if (this.b) {
            throw new IllegalStateException("calling next() when the iterator is closed");
        } else if (!hasNext()) {
            throw new IllegalStateException("you may only call next() if hasNext() is true");
        } else {
            try {
                return a(this.a);
            } catch (RemoteException e) {
                throw new RuntimeException("caught a remote exception, this process will die soon", e);
            }
        }
    }

    public void remove() {
        throw new UnsupportedOperationException("remove not supported by EntityIterators");
    }

    public final void b() {
        if (this.b) {
            throw new IllegalStateException("closing when already closed");
        }
        this.b = true;
        this.a.close();
    }
}
