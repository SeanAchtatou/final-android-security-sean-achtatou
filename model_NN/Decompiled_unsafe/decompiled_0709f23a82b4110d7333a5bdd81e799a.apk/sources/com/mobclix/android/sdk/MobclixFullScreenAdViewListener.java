package com.mobclix.android.sdk;

public interface MobclixFullScreenAdViewListener {
    public static final int ADSIZE_DISABLED = -999999;
    public static final int UNAVAILABLE = -503;
    public static final int UNKNOWN_ERROR = 0;

    String a();

    void a(MobclixFullScreenAdView mobclixFullScreenAdView);

    void a(MobclixFullScreenAdView mobclixFullScreenAdView, int i);

    String b();

    void b(MobclixFullScreenAdView mobclixFullScreenAdView);

    void c(MobclixFullScreenAdView mobclixFullScreenAdView);
}
