package com.mobclix.android.sdk;

import android.app.Activity;
import android.os.Environment;
import com.mobclix.android.sdk.MobclixFeedback;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

class MobclixUtility {
    private static String a = "MobclixUtility";

    MobclixUtility() {
    }

    public static boolean isSdPresent() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    static String a(String str) {
        return str.replace("'", "\\'").replace("\\r", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR).replace("\\n", MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
    }

    static class POSTThread implements Runnable {
        Mobclix a = Mobclix.getInstance();
        String b;
        String c;
        MobclixFeedback.Listener d;
        Activity e;

        POSTThread(String str, String str2, Activity activity, MobclixFeedback.Listener listener) {
            this.b = str;
            this.c = str2;
            this.e = activity;
            this.d = listener;
        }

        /* JADX WARNING: Removed duplicated region for block: B:23:0x0057 A[SYNTHETIC, Splitter:B:23:0x0057] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r4 = this;
                r2 = 0
                java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0069, all -> 0x0054 }
                java.lang.String r1 = r4.b     // Catch:{ Exception -> 0x0069, all -> 0x0054 }
                r0.<init>(r1)     // Catch:{ Exception -> 0x0069, all -> 0x0054 }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0069, all -> 0x0054 }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0069, all -> 0x0054 }
                java.lang.String r1 = "POST"
                r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x0069, all -> 0x0054 }
                r1 = 1
                r0.setDoInput(r1)     // Catch:{ Exception -> 0x0069, all -> 0x0054 }
                r1 = 1
                r0.setDoOutput(r1)     // Catch:{ Exception -> 0x0069, all -> 0x0054 }
                java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x0069, all -> 0x0054 }
                java.io.OutputStream r3 = r0.getOutputStream()     // Catch:{ Exception -> 0x0069, all -> 0x0054 }
                r1.<init>(r3)     // Catch:{ Exception -> 0x0069, all -> 0x0054 }
                java.lang.String r2 = r4.c     // Catch:{ Exception -> 0x0044, all -> 0x0062 }
                r1.writeBytes(r2)     // Catch:{ Exception -> 0x0044, all -> 0x0062 }
                int r2 = r0.getResponseCode()     // Catch:{ Exception -> 0x0044, all -> 0x0062 }
                r3 = 200(0xc8, float:2.8E-43)
                if (r2 != r3) goto L_0x0040
                r4.a()     // Catch:{ Exception -> 0x0044, all -> 0x0062 }
            L_0x0034:
                r0.disconnect()     // Catch:{ Exception -> 0x0044, all -> 0x0062 }
                if (r1 == 0) goto L_0x003f
                r1.flush()     // Catch:{ Exception -> 0x005e }
                r1.close()     // Catch:{ Exception -> 0x005e }
            L_0x003f:
                return
            L_0x0040:
                r4.b()     // Catch:{ Exception -> 0x0044, all -> 0x0062 }
                goto L_0x0034
            L_0x0044:
                r0 = move-exception
                r0 = r1
            L_0x0046:
                r4.b()     // Catch:{ all -> 0x0065 }
                if (r0 == 0) goto L_0x003f
                r0.flush()     // Catch:{ Exception -> 0x0052 }
                r0.close()     // Catch:{ Exception -> 0x0052 }
                goto L_0x003f
            L_0x0052:
                r0 = move-exception
                goto L_0x003f
            L_0x0054:
                r0 = move-exception
            L_0x0055:
                if (r2 == 0) goto L_0x005d
                r2.flush()     // Catch:{ Exception -> 0x0060 }
                r2.close()     // Catch:{ Exception -> 0x0060 }
            L_0x005d:
                throw r0
            L_0x005e:
                r0 = move-exception
                goto L_0x003f
            L_0x0060:
                r0 = move-exception
                goto L_0x003f
            L_0x0062:
                r0 = move-exception
                r2 = r1
                goto L_0x0055
            L_0x0065:
                r1 = move-exception
                r2 = r0
                r0 = r1
                goto L_0x0055
            L_0x0069:
                r0 = move-exception
                r0 = r2
                goto L_0x0046
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixUtility.POSTThread.run():void");
        }

        public void a() {
            if (this.d != null && this.e != null) {
                try {
                    this.e.runOnUiThread(new Runnable() {
                        public void run() {
                            POSTThread.this.d.a();
                        }
                    });
                } catch (Exception e2) {
                }
            }
        }

        public void b() {
            if (this.d != null && this.e != null) {
                try {
                    this.e.runOnUiThread(new Runnable() {
                        public void run() {
                            POSTThread.this.d.b();
                        }
                    });
                } catch (Exception e2) {
                }
            }
        }
    }
}
