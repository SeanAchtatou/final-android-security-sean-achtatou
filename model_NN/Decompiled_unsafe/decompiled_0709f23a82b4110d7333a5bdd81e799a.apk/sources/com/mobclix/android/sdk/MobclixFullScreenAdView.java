package com.mobclix.android.sdk;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.google.ads.AdActivity;
import com.mobclick.android.UmengConstants;
import com.mobclix.android.sdk.Mobclix;
import com.mobclix.android.sdk.MobclixCreative;
import java.lang.ref.SoftReference;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class MobclixFullScreenAdView {
    static int h = 0;
    /* access modifiers changed from: private */
    public String A;
    boolean a;
    boolean b;
    boolean c;
    Mobclix d;
    Activity e;
    final AdResponseHandler f;
    final RemoteConfigReadyHandler g;
    String i;
    int j;
    int k;
    HashSet<MobclixFullScreenAdViewListener> l;
    String m;
    MobclixWebView n;
    String o;
    String p;
    JSONArray q;
    int r;
    MobclixCreative.HTMLPagePool s;
    private String t;
    /* access modifiers changed from: private */
    public ArrayList<String> u;
    /* access modifiers changed from: private */
    public ArrayList<String> v;
    private boolean w;
    private boolean x;
    /* access modifiers changed from: private */
    public MobclixInstrumentation y;
    /* access modifiers changed from: private */
    public Thread z;

    /* access modifiers changed from: package-private */
    public Activity a() {
        return this.e;
    }

    public boolean b() {
        if (!this.a || this.n == null) {
            Log.e(this.t, "FullScreen Ad did not display, ad not yet loaded.");
            return false;
        } else if (this.b) {
            Log.e(this.t, "FullScreen Ad did not display, ad already displayed.");
            return false;
        } else {
            this.b = true;
            if (this.n.i) {
                this.n.d();
            }
            Mobclix.getInstance().u = new SoftReference<>(this.n);
            Intent intent = new Intent();
            String packageName = this.e.getPackageName();
            intent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "fullscreen");
            this.e.startActivity(intent);
            Iterator<MobclixFullScreenAdViewListener> it = this.l.iterator();
            while (it.hasNext()) {
                MobclixFullScreenAdViewListener next = it.next();
                if (next != null) {
                    next.b(this);
                }
            }
            f();
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        Iterator<MobclixFullScreenAdViewListener> it = this.l.iterator();
        while (it.hasNext()) {
            MobclixFullScreenAdViewListener next = it.next();
            if (next != null) {
                next.c(this);
            }
        }
        d();
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.n = null;
        this.a = false;
        this.c = false;
        this.u = null;
        this.v = null;
        this.w = false;
        this.x = false;
        this.b = false;
    }

    private class WaitForRemoteConfigThread implements Runnable {
        final /* synthetic */ MobclixFullScreenAdView a;

        public void run() {
            Long valueOf = Long.valueOf(System.currentTimeMillis());
            do {
                try {
                    if (this.a.d.z() == 1) {
                        break;
                    }
                } catch (Exception e) {
                    return;
                }
            } while (System.currentTimeMillis() - valueOf.longValue() < 10000);
            this.a.g.sendEmptyMessage(0);
        }
    }

    private class RemoteConfigReadyHandler extends Handler {
        final /* synthetic */ MobclixFullScreenAdView a;

        public void handleMessage(Message message) {
            this.a.u = new ArrayList();
            this.a.v = new ArrayList();
            this.a.e();
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (this.z != null) {
            this.z.interrupt();
            this.z = null;
        }
        String b2 = this.y.b(this.y.a(this.i, MobclixInstrumentation.b), "start_request");
        this.z = new Thread(new FetchAdResponseThread(this.f));
        this.z.start();
        this.y.e(b2);
    }

    /* access modifiers changed from: package-private */
    public void a(MobclixWebView mobclixWebView) {
        this.a = true;
        this.n = mobclixWebView;
        Iterator<MobclixFullScreenAdViewListener> it = this.l.iterator();
        while (it.hasNext()) {
            MobclixFullScreenAdViewListener next = it.next();
            if (next != null) {
                next.a(this);
            }
        }
        if (this.c) {
            b();
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        try {
            if (!this.w) {
                Iterator<String> it = this.u.iterator();
                while (it.hasNext()) {
                    new Thread(new Mobclix.FetchImageThread(it.next(), new Mobclix.BitmapHandler())).start();
                }
                this.w = true;
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        try {
            if (!this.x) {
                Iterator<String> it = this.v.iterator();
                while (it.hasNext()) {
                    new Thread(new Mobclix.FetchImageThread(it.next(), new Mobclix.BitmapHandler())).start();
                }
                this.x = true;
            }
        } catch (Exception e2) {
        }
    }

    private class FetchAdResponseThread extends Mobclix.FetchResponseThread {
        String a = "";

        FetchAdResponseThread(Handler handler) {
            super("", handler);
        }

        public void run() {
            a(a());
            super.run();
        }

        private String a() {
            String b2 = MobclixFullScreenAdView.this.y.b(MobclixFullScreenAdView.this.y.a(MobclixFullScreenAdView.this.i, MobclixInstrumentation.b), "build_request");
            StringBuffer stringBuffer = new StringBuffer();
            StringBuffer stringBuffer2 = new StringBuffer();
            String str = "";
            StringBuffer stringBuffer3 = new StringBuffer();
            try {
                b2 = MobclixFullScreenAdView.this.y.b(b2, "ad_feed_id_params");
                stringBuffer.append(MobclixFullScreenAdView.this.d.w());
                stringBuffer.append("?p=android");
                if (MobclixFullScreenAdView.this.A == null || MobclixFullScreenAdView.this.A.equals("")) {
                    if (MobclixFullScreenAdView.this.m.equals("")) {
                        stringBuffer.append("&i=").append(URLEncoder.encode(MobclixFullScreenAdView.this.d.b(), "UTF-8"));
                        stringBuffer.append("&s=").append(URLEncoder.encode("fullscreen", "UTF-8"));
                    } else {
                        stringBuffer.append("&a=").append(URLEncoder.encode(MobclixFullScreenAdView.this.m, "UTF-8"));
                    }
                    if (MobclixFullScreenAdView.this.o != null && !MobclixFullScreenAdView.this.o.equals("")) {
                        stringBuffer.append("&adurl=").append(URLEncoder.encode(MobclixFullScreenAdView.this.o, "UTF-8"));
                    }
                } else {
                    stringBuffer.append("&cr=").append(URLEncoder.encode(MobclixFullScreenAdView.this.A, "UTF-8"));
                }
                stringBuffer.append("&rt=").append(URLEncoder.encode(MobclixFullScreenAdView.this.d.e(), "UTF-8"));
                stringBuffer.append("&rtv=").append(URLEncoder.encode(MobclixFullScreenAdView.this.d.f(), "UTF-8"));
                String b3 = MobclixFullScreenAdView.this.y.b(MobclixFullScreenAdView.this.y.e(b2), "software_env");
                stringBuffer.append("&av=").append(URLEncoder.encode(MobclixFullScreenAdView.this.d.h(), "UTF-8"));
                stringBuffer.append("&u=").append(URLEncoder.encode(MobclixFullScreenAdView.this.d.i(), "UTF-8"));
                stringBuffer.append("&andid=").append(URLEncoder.encode(MobclixFullScreenAdView.this.d.j(), "UTF-8"));
                stringBuffer.append("&v=").append(URLEncoder.encode(MobclixFullScreenAdView.this.d.u(), "UTF-8"));
                stringBuffer.append("&ct=").append(URLEncoder.encode(MobclixFullScreenAdView.this.d.m()));
                String b4 = MobclixFullScreenAdView.this.y.b(MobclixFullScreenAdView.this.y.e(b3), "hardware_env");
                stringBuffer.append("&dm=").append(URLEncoder.encode(MobclixFullScreenAdView.this.d.k(), "UTF-8"));
                stringBuffer.append("&hwdm=").append(URLEncoder.encode(MobclixFullScreenAdView.this.d.l(), "UTF-8"));
                stringBuffer.append("&sv=").append(URLEncoder.encode(MobclixFullScreenAdView.this.d.g(), "UTF-8"));
                stringBuffer.append("&ua=").append(URLEncoder.encode(MobclixFullScreenAdView.this.d.A(), "UTF-8"));
                if (MobclixFullScreenAdView.this.d.B()) {
                    if (MobclixFullScreenAdView.this.d.C()) {
                        stringBuffer.append("&jb=1");
                    } else {
                        stringBuffer.append("&jb=0");
                    }
                }
                String b5 = MobclixFullScreenAdView.this.y.b(MobclixFullScreenAdView.this.y.e(MobclixFullScreenAdView.this.y.b(MobclixFullScreenAdView.this.y.e(b4), "ad_view_state_id_params")), "geo_lo");
                if (!MobclixFullScreenAdView.this.d.p().equals("null")) {
                    stringBuffer.append("&ll=").append(URLEncoder.encode(MobclixFullScreenAdView.this.d.p(), "UTF-8"));
                }
                stringBuffer.append("&l=").append(URLEncoder.encode(MobclixFullScreenAdView.this.d.q(), "UTF-8"));
                String b6 = MobclixFullScreenAdView.this.y.b(MobclixFullScreenAdView.this.y.e(b5), "keywords");
                try {
                    Iterator<MobclixFullScreenAdViewListener> it = MobclixFullScreenAdView.this.l.iterator();
                    while (it.hasNext()) {
                        MobclixFullScreenAdViewListener next = it.next();
                        if (next != null) {
                            str = next.a();
                        }
                        if (str == null) {
                            str = "";
                        }
                        if (!str.equals("")) {
                            if (stringBuffer2.length() == 0) {
                                stringBuffer2.append("&k=").append(URLEncoder.encode(str, "UTF-8"));
                            } else {
                                stringBuffer2.append("%2C").append(URLEncoder.encode(str, "UTF-8"));
                            }
                        }
                        String b7 = next.b();
                        if (b7 == null) {
                            b7 = "";
                        }
                        if (!b7.equals("")) {
                            if (stringBuffer3.length() == 0) {
                                stringBuffer3.append("&q=").append(URLEncoder.encode(b7, "UTF-8"));
                            } else {
                                stringBuffer3.append("%2B").append(URLEncoder.encode(b7, "UTF-8"));
                            }
                        }
                    }
                    if (stringBuffer2.length() > 0) {
                        stringBuffer.append(stringBuffer2);
                    }
                    String b8 = MobclixFullScreenAdView.this.y.b(MobclixFullScreenAdView.this.y.e(b6), "query");
                    if (stringBuffer3.length() > 0) {
                        stringBuffer.append(stringBuffer3);
                    }
                    String b9 = MobclixFullScreenAdView.this.y.b(MobclixFullScreenAdView.this.y.e(b8), "query");
                    if (!this.a.equals("")) {
                        stringBuffer.append(this.a);
                    }
                    this.a = "";
                    String e = MobclixFullScreenAdView.this.y.e(MobclixFullScreenAdView.this.y.e(b9));
                    MobclixFullScreenAdView.this.y.a(stringBuffer.toString(), "request_url", MobclixFullScreenAdView.this.i);
                    return stringBuffer.toString();
                } catch (Exception e2) {
                    b2 = b6;
                    MobclixFullScreenAdView.this.y.e(MobclixFullScreenAdView.this.y.e(b2));
                    MobclixFullScreenAdView.this.y.d(MobclixFullScreenAdView.this.i);
                    return "";
                }
            } catch (Exception e3) {
                MobclixFullScreenAdView.this.y.e(MobclixFullScreenAdView.this.y.e(b2));
                MobclixFullScreenAdView.this.y.d(MobclixFullScreenAdView.this.i);
                return "";
            }
        }
    }

    private class AdResponseHandler extends Handler {
        final /* synthetic */ MobclixFullScreenAdView a;

        public void handleMessage(Message message) {
            this.a.z = (Thread) null;
            String string = message.getData().getString(UmengConstants.AtomKey_Type);
            if (string.equals("success")) {
                String b = this.a.y.b(this.a.y.a(this.a.i, MobclixInstrumentation.b), "handle_response");
                try {
                    String b2 = this.a.y.b(b, "a_decode_json");
                    this.a.p = message.getData().getString("response");
                    this.a.y.a(this.a.p, "raw_json", this.a.i);
                    this.a.q = new JSONObject(this.a.p).getJSONArray("creatives");
                    if (this.a.q.length() >= 1) {
                        this.a.r = 0;
                        this.a.y.a(this.a.q.getJSONObject(this.a.r), "decoded_json", this.a.i);
                        JSONObject jSONObject = this.a.q.getJSONObject(this.a.r);
                        String b3 = this.a.y.b(b2, "b_build_models");
                        try {
                            JSONObject jSONObject2 = jSONObject.getJSONObject("eventUrls");
                            try {
                                JSONArray jSONArray = jSONObject2.getJSONArray("onShow");
                                for (int i = 0; i < jSONArray.length(); i++) {
                                    this.a.u.add(jSONArray.getString(i));
                                }
                            } catch (Exception e) {
                            }
                            JSONArray jSONArray2 = jSONObject2.getJSONArray("onTouch");
                            for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                                this.a.v.add(jSONArray2.getString(i2));
                            }
                        } catch (Exception e2) {
                        }
                        String b4 = this.a.y.b(this.a.y.b(this.a.y.e(b3), "c_build_creative"), "a_determine_type");
                        JSONObject jSONObject3 = jSONObject.getJSONObject("props");
                        String string2 = jSONObject.getString(UmengConstants.AtomKey_Type);
                        b = this.a.y.b(this.a.y.e(b4), "b_get_view");
                        if (string2.equals(AdActivity.HTML_PARAM)) {
                            this.a.s.a(this.a).a(jSONObject3.getString(AdActivity.HTML_PARAM));
                            b2 = this.a.y.e(this.a.y.e(b));
                        } else {
                            throw new Exception("Unsupported ad type");
                        }
                    }
                    this.a.y.e(b2);
                } catch (Exception e3) {
                    this.a.y.e(this.a.y.e(this.a.y.e(b)));
                    this.a.y.d(this.a.i);
                    Iterator<MobclixFullScreenAdViewListener> it = this.a.l.iterator();
                    while (it.hasNext()) {
                        MobclixFullScreenAdViewListener next = it.next();
                        if (next != null) {
                            next.a(this.a, 0);
                        }
                    }
                }
            } else if (string.equals("failure")) {
                int i3 = message.getData().getInt("errorCode");
                Iterator<MobclixFullScreenAdViewListener> it2 = this.a.l.iterator();
                while (it2.hasNext()) {
                    MobclixFullScreenAdViewListener next2 = it2.next();
                    if (next2 != null) {
                        next2.a(this.a, i3);
                    }
                }
            }
        }
    }
}
