package com.mobclix.android.sdk;

import android.app.Activity;
import java.util.HashMap;
import java.util.Map;

public class MobclixDemographics {
    public static final String AreaCode = "dac";
    public static final String Birthdate = "dbd";
    public static final String City = "dci";
    public static final String Country = "dco";
    public static final String DMACode = "ddc";
    public static final String DatingGender = "ddg";
    public static final String Education = "dec";
    public static final int EducationBachelorsDegree = 4;
    public static final int EducationDoctoralDegree = 6;
    public static final int EducationHighSchool = 1;
    public static final int EducationInCollege = 3;
    public static final int EducationMastersDegree = 5;
    public static final int EducationSomeCollege = 2;
    public static final int EducationUnknown = 0;
    public static final String Ethnicity = "den";
    public static final int EthnicityAsian = 2;
    public static final int EthnicityBlack = 3;
    public static final int EthnicityHispanic = 4;
    public static final int EthnicityMixed = 1;
    public static final int EthnicityNativeAmerican = 5;
    public static final int EthnicityUnknown = 0;
    public static final int EthnicityWhite = 6;
    public static final String Gender = "dg";
    public static final int GenderBoth = 3;
    public static final int GenderFemale = 2;
    public static final int GenderMale = 1;
    public static final int GenderUnknown = 0;
    public static final String Income = "dic";
    public static final int MaritalMarried = 3;
    public static final int MaritalSingleAvailable = 1;
    public static final int MaritalSingleUnavailable = 2;
    public static final String MaritalStatus = "dms";
    public static final int MaritalUnknown = 0;
    public static final String MetroCode = "dmc";
    public static final String PostalCode = "dpo";
    public static final String Region = "drg";
    public static final String Religion = "drl";
    public static final int ReligionBuddhism = 1;
    public static final int ReligionChristianity = 2;
    public static final int ReligionHinduism = 3;
    public static final int ReligionIslam = 4;
    public static final int ReligionJudaism = 5;
    public static final int ReligionOther = 7;
    public static final int ReligionUnaffiliated = 6;
    public static final int ReligionUnknown = 0;
    static Mobclix a = Mobclix.getInstance();
    static HashMap<String, String> b = null;
    private static String c = "mobclixDemographics";

    public static void sendDemographics(Activity activity, Map<String, Object> map) {
        sendDemographics(activity, map, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x019e  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x00a3 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void sendDemographics(android.app.Activity r9, java.util.Map<java.lang.String, java.lang.Object> r10, com.mobclix.android.sdk.MobclixFeedback.Listener r11) {
        /*
            r4 = 0
            r6 = 1
            if (r10 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            com.mobclix.android.sdk.Mobclix.onCreate(r9)
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            java.lang.String r0 = "dbd"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            r7.put(r0, r1)
            java.lang.String r0 = "dec"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            r7.put(r0, r1)
            java.lang.String r0 = "den"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            r7.put(r0, r1)
            java.lang.String r0 = "dg"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            r7.put(r0, r1)
            java.lang.String r0 = "ddg"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            r7.put(r0, r1)
            java.lang.String r0 = "dic"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            r7.put(r0, r1)
            java.lang.String r0 = "dms"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            r7.put(r0, r1)
            java.lang.String r0 = "drl"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            r7.put(r0, r1)
            java.lang.String r0 = "dac"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            r7.put(r0, r1)
            java.lang.String r0 = "dci"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            r7.put(r0, r1)
            java.lang.String r0 = "dco"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            r7.put(r0, r1)
            java.lang.String r0 = "ddc"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            r7.put(r0, r1)
            java.lang.String r0 = "dmc"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            r7.put(r0, r1)
            java.lang.String r0 = "dpo"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            r7.put(r0, r1)
            java.lang.String r0 = "drg"
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
            r7.put(r0, r1)
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            com.mobclix.android.sdk.MobclixDemographics.b = r0
            java.util.Set r0 = r10.keySet()
            java.util.Iterator r8 = r0.iterator()
        L_0x00a3:
            boolean r0 = r8.hasNext()
            if (r0 != 0) goto L_0x0186
            com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.MobclixDemographics.a
            java.lang.String r1 = r0.x()
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.String r0 = "p=android&t=demo"
            r2.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r0 = "&a="
            java.lang.StringBuffer r0 = r2.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.a     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r3 = r3.b()     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            r0.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r0 = "&v="
            java.lang.StringBuffer r0 = r2.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.a     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r3 = r3.h()     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            r0.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r0 = "&m="
            java.lang.StringBuffer r0 = r2.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.a     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r3 = r3.u()     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            r0.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r0 = "&d="
            java.lang.StringBuffer r0 = r2.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.a     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r3 = r3.i()     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            r0.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r0 = "&dt="
            java.lang.StringBuffer r0 = r2.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.a     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r3 = r3.k()     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            r0.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r0 = "&os="
            java.lang.StringBuffer r0 = r2.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.MobclixDemographics.a     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r3 = r3.g()     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            r0.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.MobclixDemographics.a     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r0 = r0.p()     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r3 = "null"
            boolean r3 = r0.equals(r3)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            if (r3 != 0) goto L_0x0163
            java.lang.String r3 = "UTF-8"
            java.lang.String r0 = java.net.URLEncoder.encode(r0, r3)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r3 = "&gps="
            java.lang.StringBuffer r3 = r2.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            r4 = 0
            r5 = 24
            int r6 = r0.length()     // Catch:{ UnsupportedEncodingException -> 0x02de }
            int r5 = java.lang.Math.min(r5, r6)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r0 = r0.substring(r4, r5)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            r3.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x02de }
        L_0x0163:
            java.util.HashMap<java.lang.String, java.lang.String> r0 = com.mobclix.android.sdk.MobclixDemographics.b     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.util.Set r0 = r0.keySet()     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ UnsupportedEncodingException -> 0x02de }
        L_0x016d:
            boolean r0 = r3.hasNext()     // Catch:{ UnsupportedEncodingException -> 0x02de }
            if (r0 != 0) goto L_0x02ac
            java.lang.Thread r0 = new java.lang.Thread
            com.mobclix.android.sdk.MobclixUtility$POSTThread r3 = new com.mobclix.android.sdk.MobclixUtility$POSTThread
            java.lang.String r2 = r2.toString()
            r3.<init>(r1, r2, r9, r11)
            r0.<init>(r3)
            r0.run()
            goto L_0x0004
        L_0x0186:
            java.lang.Object r0 = r8.next()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r5 = ""
            java.lang.Object r1 = r10.get(r0)
            if (r1 == 0) goto L_0x02e1
            java.lang.Object r2 = r7.get(r0)
            if (r2 != 0) goto L_0x01a5
            r1 = r5
            r2 = r4
        L_0x019c:
            if (r2 == 0) goto L_0x00a3
            java.util.HashMap<java.lang.String, java.lang.String> r2 = com.mobclix.android.sdk.MobclixDemographics.b
            r2.put(r0, r1)
            goto L_0x00a3
        L_0x01a5:
            java.lang.Class r2 = r1.getClass()
            java.lang.Class<java.util.Date> r3 = java.util.Date.class
            if (r2 != r3) goto L_0x01c4
            java.lang.String r2 = "dbd"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x02e1
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat
            java.lang.String r3 = "yyyyMMdd"
            r2.<init>(r3)
            java.util.Date r1 = (java.util.Date) r1
            java.lang.String r1 = r2.format(r1)
            r2 = r6
            goto L_0x019c
        L_0x01c4:
            java.lang.Class r2 = r1.getClass()
            java.lang.Class<java.lang.String> r3 = java.lang.String.class
            if (r2 != r3) goto L_0x021f
            java.lang.String r2 = "dci"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x020c
            java.lang.String r2 = "dco"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x020c
            java.lang.String r2 = "dpo"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x020c
            java.lang.String r2 = "drg"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x020c
            java.lang.String r2 = "dac"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x020c
            java.lang.String r2 = "ddc"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x020c
            java.lang.String r2 = "dic"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x020c
            java.lang.String r2 = "dmc"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x02e1
        L_0x020c:
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r1 = r1.trim()
            java.lang.String r2 = ""
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x021c
            r2 = r4
            goto L_0x019c
        L_0x021c:
            r2 = r6
            goto L_0x019c
        L_0x021f:
            java.lang.Class r2 = r1.getClass()
            java.lang.Class<java.lang.Integer> r3 = java.lang.Integer.class
            if (r2 != r3) goto L_0x02e1
            java.lang.String r2 = "dac"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x0247
            java.lang.String r2 = "ddc"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x0247
            java.lang.String r2 = "dic"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x0247
            java.lang.String r2 = "dmc"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x025f
        L_0x0247:
            java.lang.Integer r1 = (java.lang.Integer) r1
            java.lang.String r1 = r1.toString()
            java.lang.String r1 = r1.trim()
            java.lang.String r2 = ""
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x025c
            r2 = r4
            goto L_0x019c
        L_0x025c:
            r2 = r6
            goto L_0x019c
        L_0x025f:
            java.lang.String r2 = "dg"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x027b
            r2 = 2
            r3 = r2
        L_0x0269:
            r2 = r1
            java.lang.Integer r2 = (java.lang.Integer) r2
            int r2 = r2.intValue()
            if (r6 > r2) goto L_0x02e1
            if (r2 > r3) goto L_0x02e1
            java.lang.String r1 = r1.toString()
            r2 = r6
            goto L_0x019c
        L_0x027b:
            java.lang.String r2 = "ddg"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x028b
            java.lang.String r2 = "dms"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x028e
        L_0x028b:
            r2 = 3
            r3 = r2
            goto L_0x0269
        L_0x028e:
            java.lang.String r2 = "dec"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x029e
            java.lang.String r2 = "den"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x02a1
        L_0x029e:
            r2 = 6
            r3 = r2
            goto L_0x0269
        L_0x02a1:
            java.lang.String r2 = "drl"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x02e5
            r2 = 7
            r3 = r2
            goto L_0x0269
        L_0x02ac:
            java.lang.Object r0 = r3.next()     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r5 = "&"
            r4.<init>(r5)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r5 = "="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r4 = r4.toString()     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.StringBuffer r4 = r2.append(r4)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.util.HashMap<java.lang.String, java.lang.String> r5 = com.mobclix.android.sdk.MobclixDemographics.b     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.Object r0 = r5.get(r0)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ UnsupportedEncodingException -> 0x02de }
            java.lang.String r5 = "UTF-8"
            java.lang.String r0 = java.net.URLEncoder.encode(r0, r5)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            r4.append(r0)     // Catch:{ UnsupportedEncodingException -> 0x02de }
            goto L_0x016d
        L_0x02de:
            r0 = move-exception
            goto L_0x0004
        L_0x02e1:
            r1 = r5
            r2 = r4
            goto L_0x019c
        L_0x02e5:
            r3 = r4
            goto L_0x0269
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixDemographics.sendDemographics(android.app.Activity, java.util.Map, com.mobclix.android.sdk.MobclixFeedback$Listener):void");
    }
}
