package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.webkit.WebView;
import com.mobclix.android.sdk.Mobclix;
import com.mobclix.android.sdk.MobclixBrowserActivity;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.lang.ref.SoftReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class MobclixJavascriptInterface implements LocationListener {
    private static String k = "MobclixJavascriptInterface";
    private String A = "";
    private String B = "";
    private String C = "";
    private final int D = 1337;
    private final int E = 1338;
    private SensorManager F;
    private List<Integer> G = new ArrayList();
    private HashMap<Integer, SensorEventListener> H = new HashMap<>();
    private HashMap<Integer, Integer> I = new HashMap<>();
    private LocationManager J = null;
    private float K = 0.0f;
    private String L = "";
    private boolean M = false;
    private boolean N = false;
    private boolean O = false;
    private final int P = 75;
    private boolean Q = false;
    private String R = null;
    private String S = null;
    private String T = null;
    private String U = null;
    private String V = null;
    private String W = null;
    Activity a;
    MobclixBrowserActivity.MobclixFullScreen b;
    boolean c = false;
    boolean d = false;
    int e = 500;
    boolean f = false;
    int g;
    int h;
    String i;
    Thread j;
    /* access modifiers changed from: private */
    public MobclixWebView l;
    private Mobclix m = Mobclix.getInstance();
    private boolean n = true;
    private boolean o = false;
    private String p = null;
    /* access modifiers changed from: private */
    public HashMap<Integer, Integer> q = new HashMap<>();
    private final int r = 0;
    private final int s = 1;
    private final int t = 2;
    private final int u = 3;
    private final MobclixContacts v = MobclixContacts.getInstance();
    private Camera w = null;
    private String x = "null";
    private boolean y = false;
    /* access modifiers changed from: private */
    public String z = "";

    MobclixJavascriptInterface(MobclixWebView mobclixWebView, boolean z2) {
        this.l = mobclixWebView;
        this.a = (Activity) this.l.a();
        this.d = z2;
        try {
            this.o = this.l.a.a.b();
            this.n = this.l.a.a.a();
        } catch (Exception e2) {
        }
        if (z2) {
            this.n = true;
            this.o = false;
        }
        try {
            this.F = (SensorManager) this.a.getSystemService("sensor");
            for (Sensor type : this.F.getSensorList(-1)) {
                this.G.add(Integer.valueOf(type.getType()));
            }
            this.q.put(0, -1);
            this.q.put(1, -1);
            this.q.put(2, -1);
            this.q.put(3, -1);
            a();
        } catch (Exception e3) {
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a() {
        this.J = (LocationManager) this.a.getSystemService("location");
        PackageManager packageManager = this.l.getContext().getPackageManager();
        String packageName = this.l.getContext().getPackageName();
        if (packageManager.checkPermission("android.permission.ACCESS_FINE_LOCATION", packageName) != 0) {
            this.N = false;
        } else if (this.m.q.a("gps")) {
            this.N = this.J.isProviderEnabled("gps");
        } else {
            this.N = false;
        }
        if (packageManager.checkPermission("android.permission.ACCESS_COARSE_LOCATION", packageName) != 0) {
            this.O = false;
        } else if (this.m.q.a("network")) {
            this.O = this.J.isProviderEnabled("network");
        } else {
            this.O = false;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void b() {
        c();
        this.c = false;
        try {
            this.o = this.l.a.a.b();
            this.n = this.l.a.a.a();
        } catch (Exception e2) {
        }
        if (this.d) {
            this.n = true;
            this.o = false;
        }
        this.f = false;
        this.p = null;
        this.w = null;
        this.x = "null";
        this.y = false;
        this.q.put(0, -1);
        this.q.put(1, -1);
        this.q.put(2, -1);
        this.q.put(3, -1);
        this.i = null;
        this.j = null;
        this.z = "";
        this.A = "";
        this.B = "";
        this.C = "";
        this.H = new HashMap<>();
        this.I = new HashMap<>();
        this.J = null;
        this.K = 0.0f;
        this.L = "";
        this.M = false;
        try {
            a();
        } catch (Exception e3) {
            this.N = false;
            this.O = false;
        }
        this.Q = false;
        this.R = null;
        this.S = null;
        this.T = null;
        this.U = null;
        this.V = null;
        this.W = null;
        return;
    }

    /* access modifiers changed from: package-private */
    public synchronized void c() {
        if (!this.Q) {
            for (Map.Entry value : this.H.entrySet()) {
                try {
                    this.F.unregisterListener((SensorEventListener) value.getValue());
                } catch (Exception e2) {
                }
            }
            try {
                this.J.removeUpdates(this);
            } catch (Exception e3) {
            }
            try {
                a(true);
            } catch (Exception e4) {
            }
            this.Q = true;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void d() {
        if (this.Q) {
            for (Map.Entry next : this.H.entrySet()) {
                try {
                    this.F.unregisterListener((SensorEventListener) next.getValue());
                } catch (Exception e2) {
                }
                try {
                    if (((Integer) next.getKey()).intValue() == 1337) {
                        this.F.registerListener((SensorEventListener) next.getValue(), this.F.getDefaultSensor(1), this.I.get(next.getKey()).intValue());
                    } else {
                        this.F.registerListener((SensorEventListener) next.getValue(), this.F.getDefaultSensor(((Integer) next.getKey()).intValue()), this.I.get(next.getKey()).intValue());
                    }
                } catch (Exception e3) {
                }
            }
            try {
                if (this.M) {
                    if (this.N) {
                        this.J.requestLocationUpdates("gps", 0, this.K, this);
                    }
                    if (this.O) {
                        this.J.requestLocationUpdates("network", 0, this.K, this);
                    }
                }
            } catch (Exception e4) {
            }
            try {
                if (this.y) {
                    e();
                }
            } catch (Exception e5) {
            }
            this.Q = false;
        }
    }

    /* access modifiers changed from: package-private */
    public String a(String str, String str2, boolean z2) {
        StringBuilder sb = new StringBuilder("javascript:");
        sb.append(str);
        if (z2) {
            sb.append("(\"");
        } else {
            sb.append("(");
        }
        sb.append(str2);
        if (z2) {
            sb.append("\");");
        } else {
            sb.append(");");
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, DialogInterface.OnClickListener onClickListener) throws Exception {
        Context context;
        try {
            Context context2 = this.a;
            if (this.b != null) {
                context = this.b.getContext();
            } else {
                context = context2;
            }
            String str = "";
            if (i2 == 0) {
                str = "Do you want to allow this ad to access your calendar?";
            } else if (i2 == 1) {
                str = "Do you want to allow this ad to add a calendar event?";
            } else if (i2 == 2) {
                str = "Do you want to allow this ad to access your contacts?";
            } else if (i2 == 3) {
                str = "Do you want to allow this ad to add a contact?";
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(str).setCancelable(false).setPositiveButton("Yes", onClickListener).setNegativeButton("No", new Mobclix.ObjectOnClickListener(Integer.valueOf(i2)) {
                public void onClick(DialogInterface dialogInterface, int i) {
                    MobclixJavascriptInterface.this.q.put((Integer) this.b, 0);
                }
            });
            builder.create().show();
        } catch (Exception e2) {
            this.q.put(Integer.valueOf(i2), 0);
            throw new Exception(e2.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(String str, String str2) {
        StringBuilder sb = new StringBuilder("javascript: try { window.MOBCLIX.checkPermissionsForUserInteractionResponse(mAdViewPermissionsKey, \"");
        sb.append(str).append("\"); } catch(e) { ").append(str2).append("('User hasnt interacted with the ad yet.'); }");
        Log.v(k, sb.toString());
        try {
            this.l.loadUrl(sb.toString());
        } catch (Exception e2) {
        }
    }

    public void onLocationChanged(Location location) {
        Location location2;
        Location location3 = null;
        if (this.N) {
            location2 = this.J.getLastKnownLocation("gps");
        } else {
            location2 = null;
        }
        if (this.O) {
            location3 = this.J.getLastKnownLocation("network");
        }
        if (location2 == null || location3 == null) {
            if (location2 != null) {
                a(location2);
            } else if (location3 != null) {
                a(location3);
            }
        } else if (location2.getTime() > location3.getTime()) {
            a(location2);
        } else {
            a(location3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    public void a(Location location) {
        try {
            if (!this.L.equals("")) {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("altitude", location.getAltitude());
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("latitude", location.getLatitude());
                jSONObject2.put("longitude", location.getLongitude());
                jSONObject.put("coordinate", jSONObject2);
                jSONObject.put("course", (double) location.getBearing());
                jSONObject.put("speed", (double) location.getSpeed());
                jSONObject.put("timestamp", location.getTime());
                jSONObject.put("horizontalAccuracy", (double) location.getAccuracy());
                jSONObject.put("verticalAccuracy", (double) location.getAccuracy());
                this.l.loadUrl(a(this.L, "eval('(" + jSONObject.toString() + ")')", false));
            }
        } catch (JSONException e2) {
        }
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i2, Bundle bundle) {
    }

    class MobclixSensorEventListener implements SensorEventListener {
        WebView a;
        int b;
        String c;
        final /* synthetic */ MobclixJavascriptInterface d;

        public void onAccuracyChanged(Sensor sensor, int i) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
          com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
        public void onSensorChanged(SensorEvent sensorEvent) {
            try {
                if (sensorEvent.sensor.getType() == this.b) {
                    StringBuilder sb = new StringBuilder(new StringBuilder().append(sensorEvent.values[0]).toString());
                    if (sensorEvent.values.length > 1) {
                        sb.append(",");
                        sb.append(sensorEvent.values[1]);
                        if (sensorEvent.values.length > 2) {
                            sb.append(",");
                            sb.append(sensorEvent.values[2]);
                        }
                    }
                    this.a.loadUrl(this.d.a(this.c, sb.toString(), false));
                }
                if (this.b == 1338) {
                    this.a.loadUrl(this.d.a(this.c, new StringBuilder().append(sensorEvent.values[0]).toString(), false));
                }
            } catch (Exception e) {
            }
        }
    }

    class MobclixShakeEventListener implements SensorEventListener {
        WebView a;
        String b;
        final /* synthetic */ MobclixJavascriptInterface c;
        private double d;

        public void onAccuracyChanged(Sensor sensor, int i) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
          com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
        public void onSensorChanged(SensorEvent sensorEvent) {
            if (sensorEvent.sensor.getType() == 1) {
                double sqrt = Math.sqrt(0.0d + Math.pow((double) (sensorEvent.values[0] / 9.80665f), 2.0d) + Math.pow((double) (sensorEvent.values[1] / 9.80665f), 2.0d) + Math.pow((double) (sensorEvent.values[2] / 9.80665f), 2.0d));
                if (sqrt < 1.399999976158142d && this.d > 1.399999976158142d) {
                    this.a.loadUrl(this.c.a(this.b, "", false));
                }
                this.d = sqrt;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    public synchronized void a(String str) {
        if (this.z != null) {
            this.l.loadUrl(a(this.z, str, true));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    public synchronized void b(String str) {
        if (this.A != null) {
            this.l.loadUrl(a(this.A, str, true));
        }
    }

    /* renamed from: com.mobclix.android.sdk.MobclixJavascriptInterface$2  reason: invalid class name */
    class AnonymousClass2 extends Mobclix.ObjectOnClickListener {
        final /* synthetic */ MobclixJavascriptInterface a;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
          com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
        public void onClick(DialogInterface dialogInterface, int i) {
            if (this.b != null) {
                this.a.l.loadUrl(this.a.a((String) this.b, "0, '" + ((String) this.c) + "'", false));
            }
        }
    }

    /* renamed from: com.mobclix.android.sdk.MobclixJavascriptInterface$3  reason: invalid class name */
    class AnonymousClass3 extends Mobclix.ObjectOnClickListener {
        final /* synthetic */ MobclixJavascriptInterface a;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
          com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
        public void onClick(DialogInterface dialogInterface, int i) {
            if (this.b != null) {
                this.a.l.loadUrl(this.a.a((String) this.b, "1, '" + ((String) this.c) + "'", false));
            }
        }
    }

    /* renamed from: com.mobclix.android.sdk.MobclixJavascriptInterface$4  reason: invalid class name */
    class AnonymousClass4 extends Mobclix.ObjectOnClickListener {
        final /* synthetic */ MobclixJavascriptInterface a;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
          com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
        public void onClick(DialogInterface dialogInterface, int i) {
            if (this.b != null) {
                this.a.l.loadUrl(this.a.a((String) this.b, "2, '" + ((String) this.c) + "'", false));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void e() throws Exception {
        if (!this.m.r.containsKey("android.permission.CAMERA")) {
            throw new Exception("Application does not have the CAMERA permission.");
        }
        if (this.w != null) {
            this.w.release();
        }
        this.w = Camera.open();
        Camera.Parameters parameters = this.w.getParameters();
        if (this.x.equals("null")) {
            try {
                this.x = (String) Camera.Parameters.class.getMethod("getFlashMode", new Class[0]).invoke(parameters, new Object[0]);
            } catch (Exception e2) {
            }
        }
        try {
            Camera.Parameters.class.getMethod("setFlashMode", String.class).invoke(parameters, "torch");
        } catch (Exception e3) {
        }
        this.w.setParameters(parameters);
        this.y = true;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(boolean z2) throws Exception {
        if (!this.m.r.containsKey("android.permission.CAMERA")) {
            throw new Exception("Application does not have the CAMERA permission.");
        } else if (this.w == null) {
            throw new Exception("Flashlight isn't on.");
        } else {
            Camera.Parameters parameters = this.w.getParameters();
            String str = "auto";
            if (!this.x.equals("null")) {
                str = this.x;
            }
            try {
                Camera.Parameters.class.getMethod("setFlashMode", String.class).invoke(parameters, str);
            } catch (Exception e2) {
            }
            this.w.setParameters(parameters);
            this.w.release();
            this.w = null;
            if (!z2) {
                this.y = false;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    public synchronized void b(String str, String str2) {
        Uri parse;
        try {
            if (!this.l.d) {
                throw new Exception("Ad not yet displayed.");
            } else if (!this.n && this.o && !this.f) {
                StringBuilder sb = new StringBuilder("window.MOBCLIX.calendarGetCalendars('");
                sb.append(str).append("','").append(str2).append("');");
                a(sb.toString(), str2);
            } else if (!this.m.r.containsKey("android.permission.READ_CALENDAR")) {
                throw new Exception("Application does not have the READ_CALENDAR permission.");
            } else if (this.q.get(0).intValue() == 0) {
                throw new Exception("User declined access to calendar.");
            } else if (this.q.get(0).intValue() == -1) {
                a(0, new Mobclix.ObjectOnClickListener(str, str2) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MobclixJavascriptInterface.this.q.put(0, 1);
                        MobclixJavascriptInterface.this.b((String) this.b, (String) this.c);
                    }
                });
            } else {
                String[] strArr = {"_id", "name"};
                if (Integer.parseInt(Build.VERSION.SDK) >= 8) {
                    parse = Uri.parse("content://com.android.calendar/calendars");
                } else {
                    parse = Uri.parse("content://calendar/calendars");
                }
                Cursor managedQuery = this.a.managedQuery(parse, strArr, "selected=1", null, null);
                if (managedQuery.moveToFirst()) {
                    StringBuffer stringBuffer = new StringBuffer("['");
                    int columnIndex = managedQuery.getColumnIndex("name");
                    int i2 = 0;
                    while (true) {
                        if (i2 != 0) {
                            stringBuffer.append("','");
                        }
                        String string = managedQuery.getString(columnIndex);
                        if (string != null) {
                            string = MobclixUtility.a(string);
                        }
                        stringBuffer.append(string);
                        int i3 = i2 + 1;
                        if (!managedQuery.moveToNext()) {
                            break;
                        }
                        i2 = i3;
                    }
                    stringBuffer.append("']");
                    if (str != null) {
                        this.l.loadUrl(a(str, stringBuffer.toString(), false));
                    }
                }
            }
        } catch (Exception e2) {
            if (str2 != null) {
                this.l.loadUrl(a(str2, MobclixUtility.a(e2.toString()), true));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public synchronized void a(String str, String str2, String str3) {
        Exception exc;
        Uri parse;
        String string;
        Uri parse2;
        try {
            if (!this.l.d) {
                throw new Exception("Ad not yet displayed.");
            } else if (!this.n && this.o && !this.f) {
                StringBuilder sb = new StringBuilder("window.MOBCLIX.calendarAddEvent('");
                sb.append(str.replaceAll("\"", "\\\\\"")).append("','").append(str2).append("','").append(str3).append("');");
                a(sb.toString(), str3);
            } else if (this.q.get(1).intValue() == 0) {
                throw new Exception("User declined access to calendar.");
            } else if (this.q.get(1).intValue() == -1) {
                a(1, new Mobclix.ObjectOnClickListener(str, str2, str3) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MobclixJavascriptInterface.this.q.put(1, 1);
                        MobclixJavascriptInterface.this.a((String) this.b, (String) this.c, (String) this.d);
                    }
                });
            } else if (!this.m.r.containsKey("android.permission.WRITE_CALENDAR")) {
                throw new Exception("Application does not have the WRITE_CALENDAR permission.");
            } else {
                JSONObject jSONObject = new JSONObject(str);
                String[] strArr = {"_id", "name"};
                if (Integer.parseInt(Build.VERSION.SDK) >= 8) {
                    parse = Uri.parse("content://com.android.calendar/calendars");
                } else {
                    parse = Uri.parse("content://calendar/calendars");
                }
                Cursor managedQuery = this.a.managedQuery(parse, strArr, "selected=1", null, null);
                if (managedQuery.moveToFirst()) {
                    int columnIndex = managedQuery.getColumnIndex("name");
                    int columnIndex2 = managedQuery.getColumnIndex("_id");
                    while (true) {
                        String string2 = managedQuery.getString(columnIndex);
                        string = managedQuery.getString(columnIndex2);
                        if (jSONObject.has("calendar") && !string.equals(URLDecoder.decode(jSONObject.getString("calendar")))) {
                            if (!managedQuery.moveToNext()) {
                                string = null;
                                break;
                            }
                        } else {
                            Log.v(k, "Calendar found: " + string2 + ", " + string);
                        }
                    }
                    if (string == null) {
                        throw new Exception("Calendar not found.");
                    }
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("calendar_id", string);
                    if (jSONObject.has("hasAlarm") && jSONObject.getBoolean("hasAlarm")) {
                        contentValues.put("hasAlarm", (Integer) 1);
                    }
                    if (jSONObject.has("alarms") && jSONObject.getJSONArray("alarms").length() > 0) {
                        contentValues.put("hasAlarm", (Integer) 1);
                    }
                    if (jSONObject.has("allDay") && jSONObject.getBoolean("allDay")) {
                        contentValues.put("allDay", (Integer) 1);
                    }
                    if (!jSONObject.has("startDate")) {
                        throw new Exception("Start date not provided.");
                    } else if (!jSONObject.has("endDate")) {
                        throw new Exception("End date not provided.");
                    } else {
                        String string3 = jSONObject.getString("startDate");
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mmZ");
                        Date parse3 = simpleDateFormat.parse(string3);
                        contentValues.put("dtstart", Long.valueOf(parse3.getTime()));
                        contentValues.put("dtend", Long.valueOf(simpleDateFormat.parse(jSONObject.getString("endDate")).getTime()));
                        if (jSONObject.has("location")) {
                            contentValues.put("eventLocation", jSONObject.getString("location"));
                        }
                        if (jSONObject.has("description")) {
                            contentValues.put("description", jSONObject.getString("description"));
                        }
                        if (jSONObject.has("eventTitle")) {
                            contentValues.put("title", jSONObject.getString("eventTitle"));
                        }
                        if (jSONObject.has("status")) {
                            if (jSONObject.getString("status").equalsIgnoreCase("tentative")) {
                                contentValues.put("eventStatus", (Integer) 0);
                            }
                            if (jSONObject.getString("status").equalsIgnoreCase("confirmed")) {
                                contentValues.put("eventStatus", (Integer) 1);
                            }
                            if (jSONObject.getString("status").equalsIgnoreCase("canceled")) {
                                contentValues.put("eventStatus", (Integer) 2);
                            }
                        }
                        if (jSONObject.has("visibility")) {
                            if (jSONObject.getString("visibility").equalsIgnoreCase("default")) {
                                contentValues.put("visibility", (Integer) 0);
                            }
                            if (jSONObject.getString("visibility").equalsIgnoreCase("confidential")) {
                                contentValues.put("visibility", (Integer) 1);
                            }
                            if (jSONObject.getString("visibility").equalsIgnoreCase("private")) {
                                contentValues.put("visibility", (Integer) 2);
                            }
                            if (jSONObject.getString("visibility").equalsIgnoreCase("public")) {
                                contentValues.put("visibility", (Integer) 3);
                            }
                        }
                        if (jSONObject.has("transparency")) {
                            if (jSONObject.getString("transparency").equalsIgnoreCase("opaque")) {
                                contentValues.put("status", (Integer) 0);
                            }
                            if (jSONObject.getString("transparency").equalsIgnoreCase("transparent")) {
                                contentValues.put("status", (Integer) 1);
                            }
                        }
                        if (Integer.parseInt(Build.VERSION.SDK) >= 8) {
                            parse2 = Uri.parse("content://com.android.calendar/events");
                        } else {
                            parse2 = Uri.parse("content://calendar/events");
                        }
                        ContentResolver contentResolver = this.a.getContentResolver();
                        Uri insert = contentResolver.insert(parse2, contentValues);
                        if (insert != null && jSONObject.has("alarms")) {
                            JSONArray jSONArray = jSONObject.getJSONArray("alarms");
                            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                                Date parse4 = simpleDateFormat.parse(jSONArray.getString(i2));
                                long parseLong = Long.parseLong(insert.getLastPathSegment());
                                ContentValues contentValues2 = new ContentValues();
                                contentValues2.put("event_id", Long.valueOf(parseLong));
                                contentValues2.put("method", (Integer) 1);
                                contentValues2.put("minutes", Integer.valueOf((int) ((parse3.getTime() - parse4.getTime()) / 60000)));
                                if (Integer.parseInt(Build.VERSION.SDK) >= 8) {
                                    contentResolver.insert(Uri.parse("content://com.android.calendar/reminders"), contentValues2);
                                } else {
                                    contentResolver.insert(Uri.parse("content://calendar/reminders"), contentValues2);
                                }
                            }
                        }
                        if (str2 != null) {
                            this.l.loadUrl(a(str2, "", false));
                        }
                    }
                } else {
                    throw new Exception("No calendars found.");
                }
            }
        } catch (Exception e2) {
            exc = e2;
            Log.v(k, "ADD CALENDAR EXCEPTION 1: ", exc);
            JSONObject jSONObject2 = new JSONObject(str);
            if (this.q.get(1).intValue() == 0) {
                throw new Exception("User declined access to calendar.");
            } else if (!jSONObject2.has("startDate")) {
                throw new Exception("Start date not provided.");
            } else if (!jSONObject2.has("endDate")) {
                throw new Exception("End date not provided.");
            } else {
                String string4 = jSONObject2.getString("startDate");
                SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd'T'hh:mmZ");
                Date parse5 = simpleDateFormat2.parse(string4);
                Date parse6 = simpleDateFormat2.parse(jSONObject2.getString("endDate"));
                Intent intent = new Intent("android.intent.action.EDIT");
                intent.setType("vnd.android.cursor.item/event");
                if (jSONObject2.has("eventTitle")) {
                    intent.putExtra("title", jSONObject2.getString("eventTitle"));
                }
                if (jSONObject2.has("description")) {
                    intent.putExtra("description", jSONObject2.getString("description"));
                }
                if (jSONObject2.has("location")) {
                    intent.putExtra("eventLocation", jSONObject2.getString("location"));
                }
                intent.putExtra("beginTime", parse5.getTime());
                intent.putExtra("endTime", parse6.getTime());
                if (jSONObject2.has("allDay") && jSONObject2.getBoolean("allDay")) {
                    intent.putExtra("allDay", true);
                }
                this.l.b().startActivity(intent);
                if (str2 != null) {
                    this.l.loadUrl(a(str2, "", false));
                }
            }
        } catch (Exception e3) {
            Log.v(k, "ADD CALENDAR EXCEPTION 2: ", e3);
            if (str3 != null) {
                this.l.loadUrl(a(str3, MobclixUtility.a(exc.toString()), true));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    public synchronized void b(String str, String str2, String str3) {
        long j2;
        long j3;
        String str4;
        Uri parse;
        Uri.Builder buildUpon;
        try {
            if (!this.l.d) {
                throw new Exception("Ad not yet displayed.");
            } else if (!this.m.r.containsKey("android.permission.READ_CALENDAR")) {
                throw new Exception("Application does not have the READ_CALENDAR permission.");
            } else if (!this.n && this.o && !this.f) {
                StringBuilder sb = new StringBuilder("window.MOBCLIX.calendarQueryEvents('");
                sb.append(str.replaceAll("\"", "\\\\\"")).append("','").append(str2).append("','").append(str3).append("');");
                a(sb.toString(), str3);
            } else if (this.q.get(0).intValue() == 0) {
                throw new Exception("User declined access to calendar.");
            } else if (this.q.get(0).intValue() == -1) {
                a(0, new Mobclix.ObjectOnClickListener(str, str2, str3) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MobclixJavascriptInterface.this.q.put(0, 1);
                        MobclixJavascriptInterface.this.b((String) this.b, (String) this.c, (String) this.d);
                    }
                });
            } else {
                JSONObject jSONObject = new JSONObject(str);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mmZ");
                if (jSONObject.has("startDate")) {
                    j2 = simpleDateFormat.parse(jSONObject.getString("startDate")).getTime();
                } else {
                    j2 = 0;
                }
                if (jSONObject.has("endDate")) {
                    j3 = simpleDateFormat.parse(jSONObject.getString("endDate")).getTime();
                } else {
                    j3 = 0;
                }
                if (jSONObject.has("calendar")) {
                    str4 = jSONObject.getString("calendar");
                } else {
                    str4 = null;
                }
                if (j2 == 0 && j3 == 0 && str4 == null) {
                    throw new Exception("No query parameters supplied.");
                }
                HashMap hashMap = new HashMap();
                String[] strArr = {"_id", "name"};
                if (Integer.parseInt(Build.VERSION.SDK) >= 8) {
                    parse = Uri.parse("content://com.android.calendar/calendars");
                } else {
                    parse = Uri.parse("content://calendar/calendars");
                }
                Cursor managedQuery = this.a.managedQuery(parse, strArr, "selected=1", null, null);
                if (managedQuery.moveToFirst()) {
                    int columnIndex = managedQuery.getColumnIndex("name");
                    int columnIndex2 = managedQuery.getColumnIndex("_id");
                    do {
                        String string = managedQuery.getString(columnIndex);
                        if (string == null) {
                            string = "null";
                        }
                        String string2 = managedQuery.getString(columnIndex2);
                        if (str4 == null) {
                            hashMap.put(string2, string);
                        } else if (string.equals(str4)) {
                            hashMap.put(string2, string);
                        }
                    } while (managedQuery.moveToNext());
                    if (hashMap.isEmpty()) {
                        throw new Exception("Calendar not found.");
                    }
                    JSONArray jSONArray = new JSONArray();
                    for (String str5 : hashMap.keySet()) {
                        if (Integer.parseInt(Build.VERSION.SDK) >= 8) {
                            buildUpon = Uri.parse("content://com.android.calendar/instances/when").buildUpon();
                        } else {
                            buildUpon = Uri.parse("content://calendar/instances/when").buildUpon();
                        }
                        long time = new Date().getTime();
                        if (j2 != 0) {
                            ContentUris.appendId(buildUpon, j2);
                        } else {
                            ContentUris.appendId(buildUpon, time - 864000000000L);
                        }
                        if (j3 != 0) {
                            ContentUris.appendId(buildUpon, j3);
                        } else {
                            ContentUris.appendId(buildUpon, time + 864000000000L);
                        }
                        Cursor query = this.a.getContentResolver().query(buildUpon.build(), new String[]{"hasAlarm", "allDay", "begin", "end", "eventLocation", "description", "title", "eventStatus", "visibility", "transparency"}, "Calendars._id=" + str5, null, "startDay ASC, startMinute ASC");
                        while (query.moveToNext()) {
                            JSONObject jSONObject2 = new JSONObject();
                            jSONObject2.put("calendar", hashMap.get(str5));
                            if (query.getString(0) != null) {
                                jSONObject2.put("hasAlarm", Integer.parseInt(query.getString(0)));
                            }
                            if (query.getString(1) != null) {
                                jSONObject2.put("allDay", Integer.parseInt(query.getString(1)));
                            }
                            jSONObject2.put("startDate", simpleDateFormat.format(new Date(query.getLong(2))));
                            jSONObject2.put("endDate", simpleDateFormat.format(new Date(query.getLong(3))));
                            if (query.getString(4) != null) {
                                jSONObject2.put("location", query.getString(4));
                            }
                            if (query.getString(5) != null) {
                                jSONObject2.put("description", query.getString(5));
                            }
                            if (query.getString(6) != null) {
                                jSONObject2.put("eventTitle", query.getString(6));
                            }
                            jSONObject2.put("status", query.getInt(7));
                            jSONObject2.put("visibility", query.getInt(8));
                            jSONObject2.put("transparency", query.getInt(9));
                            jSONArray.put(jSONObject2);
                        }
                    }
                    if (str2 != null) {
                        this.l.loadUrl(a(str2, "eval('(" + MobclixUtility.a(jSONArray.toString()) + ")')", false));
                    }
                } else {
                    throw new Exception("No calendars found.");
                }
            }
        } catch (Exception e2) {
            if (str3 != null) {
                this.l.loadUrl(a(str3, MobclixUtility.a(e2.toString()), true));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    public synchronized void c(String str, String str2, String str3) {
        boolean z2 = true;
        synchronized (this) {
            try {
                if (!this.l.d) {
                    throw new Exception("Ad not yet displayed.");
                } else if (this.n || !this.o || this.f) {
                    int parseInt = Integer.parseInt(Build.VERSION.SDK);
                    if (this.m.r.containsKey("android.permission.WRITE_CONTACTS") || parseInt >= 5) {
                        if (this.m.r.containsKey("android.permission.READ_CONTACTS") && this.m.r.containsKey("android.permission.GET_ACCOUNTS")) {
                            z2 = false;
                        }
                        if (this.q.get(3).intValue() == 0) {
                            throw new Exception("User declined access to contacts.");
                        } else if (this.q.get(3).intValue() == -1) {
                            a(3, new Mobclix.ObjectOnClickListener(str, str2, str3) {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    MobclixJavascriptInterface.this.q.put(3, 1);
                                    MobclixJavascriptInterface.this.c((String) this.b, (String) this.c, (String) this.d);
                                }
                            });
                        } else {
                            Activity activity = this.a;
                            if (this.b != null) {
                                activity = (Activity) this.b.getContext();
                            }
                            JSONObject jSONObject = new JSONObject(str);
                            if (parseInt < 5) {
                                this.v.a(jSONObject, activity);
                            } else if (!z2) {
                                this.v.a(jSONObject, activity);
                            } else {
                                Intent intent = new Intent();
                                String packageName = this.a.getPackageName();
                                this.m.v = new SoftReference<>(this.l);
                                this.B = str2;
                                this.C = str3;
                                intent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "addContact").putExtra(String.valueOf(packageName) + ".data", str);
                                Activity activity2 = this.a;
                                if (this.b != null) {
                                    this.b.o = true;
                                }
                                activity2.startActivity(intent);
                            }
                            if (str2 != null) {
                                this.l.loadUrl(a(str2, "", false));
                            }
                        }
                    } else {
                        throw new Exception("Application does not have the WRITE_CONTACTS permission.");
                    }
                } else {
                    StringBuilder sb = new StringBuilder("window.MOBCLIX.contactsAddContact('");
                    sb.append(str.replaceAll("\"", "\\\\\"")).append("','").append(str2).append("','").append(str3).append("');");
                    a(sb.toString(), str3);
                }
            } catch (Exception e2) {
                if (str3 != null) {
                    this.l.loadUrl(a(str3, MobclixUtility.a(e2.toString()), true));
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    public synchronized void c(String str, String str2) {
        try {
            if (!this.l.d) {
                throw new Exception("Ad not yet displayed.");
            } else if (!this.n && this.o && !this.f) {
                StringBuilder sb = new StringBuilder("window.MOBCLIX.contactsGetContact('");
                sb.append(str).append("','").append(str2).append("');");
                a(sb.toString(), str2);
            } else if (!this.m.r.containsKey("android.permission.READ_CONTACTS")) {
                throw new Exception("Application does not have the READ_CONTACTS permission.");
            } else if (this.q.get(2).intValue() == 0) {
                throw new Exception("User declined access to contacts.");
            } else if (this.q.get(2).intValue() == -1) {
                a(2, new Mobclix.ObjectOnClickListener(str, str2) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MobclixJavascriptInterface.this.q.put(2, 1);
                        MobclixJavascriptInterface.this.c((String) this.b, (String) this.c);
                    }
                });
            } else {
                Intent intent = new Intent();
                String packageName = this.a.getPackageName();
                intent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "contact");
                this.m.v = new SoftReference<>(this.l);
                this.B = str;
                this.C = str2;
                Activity activity = this.a;
                if (this.b != null) {
                    this.b.o = true;
                }
                activity.startActivity(intent);
            }
        } catch (Exception e2) {
            if (str2 != null) {
                this.l.loadUrl(a(str2, MobclixUtility.a(e2.toString()), true));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    public synchronized void f() {
        if (this.B != null) {
            this.l.loadUrl(a(this.B, "", false));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    public synchronized void a(Uri uri) {
        Activity activity = this.a;
        if (this.b != null) {
            this.b.o = true;
        }
        JSONObject a2 = this.v.a(activity.getContentResolver(), uri);
        if (a2 == null && this.C != null) {
            this.l.loadUrl(a(this.C, "Error getting contact.", true));
        } else if (this.B != null) {
            this.l.loadUrl(a(this.B, "eval('(" + MobclixUtility.a(a2.toString()) + ")')", false));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    public synchronized void c(String str) {
        if (this.C != null) {
            this.l.loadUrl(a(this.C, str, true));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    public void d(String str) {
        if (this.j == null) {
            this.j = new Thread(new SendImageToServerThread(this.i, str));
            this.j.run();
        } else if (this.A != null) {
            this.l.loadUrl(a(this.A, "Image already being sent.", true));
        }
    }

    class SendImageToServerThread implements Runnable {
        private String b;
        private String c;

        SendImageToServerThread(String str, String str2) {
            this.c = str2;
            this.b = str;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
          com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
        public void run() {
            try {
                Bitmap decodeFile = BitmapFactory.decodeFile(new File(this.b).getAbsolutePath());
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                decodeFile.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.c).openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);
                httpURLConnection.connect();
                DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
                byteArrayOutputStream.writeTo(dataOutputStream);
                dataOutputStream.flush();
                dataOutputStream.close();
                byteArrayOutputStream.close();
                System.gc();
                if (MobclixJavascriptInterface.this.z != null) {
                    MobclixJavascriptInterface.this.l.loadUrl(MobclixJavascriptInterface.this.a(MobclixJavascriptInterface.this.z, "", false));
                }
            } catch (Exception e) {
                Exception exc = e;
                if (!(Mobclix.getInstance().v == null || Mobclix.getInstance().v.get() == null)) {
                    Mobclix.getInstance().v.get().c().b(exc.toString());
                    Mobclix.getInstance().v.get().c().b("Error processing photo.");
                }
            }
            MobclixJavascriptInterface.this.j = null;
        }
    }

    private class ExpanderRunnable implements Animation.AnimationListener, Runnable {
        int a;
        int b;
        int c;
        int d;
        int e;
        final /* synthetic */ MobclixJavascriptInterface f;

        public void run() {
            ((MobclixBrowserActivity.MobclixExpander) this.f.b).a(this.b, this.a, this.c, this.d, this.e, this);
        }

        public void onAnimationEnd(Animation animation) {
            this.f.h();
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    /* renamed from: com.mobclix.android.sdk.MobclixJavascriptInterface$10  reason: invalid class name */
    class AnonymousClass10 implements Runnable {
        final /* synthetic */ MobclixJavascriptInterface a;

        public void run() {
            this.a.b.a(this.a.e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    public synchronized void g() {
        if (this.R != null) {
            this.l.loadUrl(a(this.R, "", false));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    public synchronized void h() {
        if (this.S != null) {
            this.l.loadUrl(a(this.S, "", false));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    public synchronized void i() {
        if (this.T != null) {
            this.l.loadUrl(a(this.T, "", false));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    public synchronized void j() {
        if (this.U != null) {
            this.l.loadUrl(a(this.U, "", false));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    public synchronized void k() {
        if (this.V != null) {
            this.l.loadUrl(a(this.V, "", false));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, java.lang.String):void
      com.mobclix.android.sdk.MobclixJavascriptInterface.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    public synchronized void l() {
        if (this.W != null) {
            this.l.loadUrl(a(this.W, "", false));
        }
    }
}
