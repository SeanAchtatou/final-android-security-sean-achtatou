package com.mobclix.android.sdk;

public interface MobclixAdViewListener {
    public static final int ADSIZE_DISABLED = -999999;
    public static final int ADS_NOT_STARTED = -888888;
    public static final int ADS_SERVER_ERROR = -500;
    public static final int APP_NOT_IN_FOREGROUND = -777777;
    public static final int SUBALLOCATION_ADMOB = -750;
    public static final int SUBALLOCATION_GOOGLE = -10100;
    public static final int SUBALLOCATION_MILLENNIAL = -111111;
    public static final int SUBALLOCATION_OTHER = -1006;
    public static final int UNAVAILABLE = -503;
    public static final int UNKNOWN_ERROR = 0;

    String a();

    void a(MobclixAdView mobclixAdView);

    void a(MobclixAdView mobclixAdView, String str);

    boolean a(MobclixAdView mobclixAdView, int i);

    String b();

    void b(MobclixAdView mobclixAdView);

    void b(MobclixAdView mobclixAdView, int i);
}
