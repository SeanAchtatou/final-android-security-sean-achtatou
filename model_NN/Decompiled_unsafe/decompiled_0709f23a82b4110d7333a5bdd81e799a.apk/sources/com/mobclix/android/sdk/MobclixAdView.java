package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.widget.ViewFlipper;
import com.mobclick.android.UmengConstants;
import com.mobclix.android.sdk.Mobclix;
import com.mobclix.android.sdk.MobclixCreative;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;

public abstract class MobclixAdView extends ViewFlipper {
    static int e = 0;
    static HashMap<String, Long> n = new HashMap<>();
    MobclixCreative.HTMLPagePool A;
    String B = null;
    MobclixCreativeManager C = null;
    int D = 0;
    MobclixUtilityView E = null;
    private String F = "MobclixAdView";
    /* access modifiers changed from: private */
    public MobclixInstrumentation G;
    private Thread H;
    /* access modifiers changed from: private */
    public String I = "";
    private Timer J = null;
    private boolean K = false;
    /* access modifiers changed from: private */
    public long L = 0;
    Object a = new Object();
    final AdResponseHandler b = new AdResponseHandler(this, null);
    final RemoteConfigReadyHandler c = new RemoteConfigReadyHandler(this, null);
    boolean d = false;
    String f = null;
    boolean g = false;
    boolean h = false;
    int i = -1;
    int j = -1;
    boolean k = false;
    boolean l = false;
    long m = 0;
    String o = null;
    String p;
    int q = 0;
    float r;
    float s;
    float t = 1.0f;
    HashSet<MobclixAdViewListener> u = new HashSet<>();
    String v = "";
    String w = "";
    int x = 0;
    MobclixCreative y = null;
    MobclixCreative z = null;

    MobclixAdView(Context context, String str) {
        super(context);
        this.p = str;
        try {
            a((Activity) context);
        } catch (Mobclix.MobclixPermissionException e2) {
            throw e2;
        } catch (Exception e3) {
        }
    }

    MobclixAdView(Context context, String str, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.p = str;
        String attributeValue = attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "background");
        if (attributeValue != null) {
            this.q = Color.parseColor(attributeValue);
        }
        try {
            a((Activity) context);
        } catch (Mobclix.MobclixPermissionException e2) {
            throw e2;
        } catch (Exception e3) {
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 7) {
                try {
                    super.onDetachedFromWindow();
                    super.stopFlipping();
                } catch (IllegalArgumentException e2) {
                    Log.w(this.F, "Android project  issue 6191  workaround.");
                    super.stopFlipping();
                } catch (Throwable th) {
                    super.stopFlipping();
                    throw th;
                }
            } else {
                super.onDetachedFromWindow();
            }
        } catch (Exception e3) {
            super.onDetachedFromWindow();
        }
        int i2 = 0;
        while (getChildCount() > i2) {
            try {
                if (getChildAt(i2).getClass() == MobclixCreative.class) {
                    int i3 = 0;
                    while (i3 < getChildCount()) {
                        try {
                            try {
                                if (((MobclixCreative) getChildAt(i2)).getChildAt(i3).getClass() == MobclixCreative.OpenAllocationPage.class) {
                                    ((MobclixCreative.OpenAllocationPage) ((MobclixCreative) getChildAt(i2)).getChildAt(i3)).b();
                                }
                            } catch (Exception e4) {
                            }
                            i3++;
                        } catch (Exception e5) {
                        }
                    }
                    i2++;
                } else if (getChildAt(i2).getClass() == Class.forName("com.google.ads.AdView")) {
                    try {
                        Class.forName("com.google.ads.AdView").getMethod("destroy", new Class[0]).invoke(getChildAt(i2), new Object[0]);
                        getChildAt(i2).setOnClickListener(null);
                        removeViewAt(i2);
                    } catch (Throwable th2) {
                    }
                } else if (getChildAt(i2).getClass() == Class.forName("com.millennialmedia.android.MMAdView")) {
                    try {
                        Class.forName("com.millennialmedia.android.MMAdView").getMethod("halt", new Class[0]).invoke(getChildAt(i2), new Object[0]);
                        getChildAt(i2).setOnClickListener(null);
                        removeViewAt(i2);
                    } catch (Exception e6) {
                    }
                } else {
                    try {
                        removeViewAt(i2);
                    } catch (Exception e7) {
                    }
                }
            } catch (Exception e8) {
            }
        }
        this.k = true;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.k = false;
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        e();
        if (this.y != null) {
            this.y.d();
        }
        super.finalize();
    }

    public boolean a(MobclixAdViewListener mobclixAdViewListener) {
        if (mobclixAdViewListener == null) {
            return false;
        }
        return this.u.add(mobclixAdViewListener);
    }

    public void setLayoutParams(ViewGroup.LayoutParams layoutParams) {
        int parseInt = Integer.parseInt(this.p.split("x")[0]);
        int parseInt2 = Integer.parseInt(this.p.split("x")[1]);
        this.r = TypedValue.applyDimension(1, (float) parseInt, getResources().getDisplayMetrics());
        this.s = TypedValue.applyDimension(1, (float) parseInt2, getResources().getDisplayMetrics());
        this.t = getResources().getDisplayMetrics().density;
        layoutParams.width = (int) this.r;
        layoutParams.height = (int) this.s;
        super.setLayoutParams(layoutParams);
    }

    public void a(long j2) {
        e();
        this.L = j2;
        if (this.L >= 0) {
            if (this.L < 15000) {
                this.L = 15000;
            }
            try {
                this.J = new Timer();
                this.J.scheduleAtFixedRate(new FetchAdResponseThread(this.b), this.L, this.L);
            } catch (Exception e2) {
            }
        }
    }

    public void a(boolean z2) {
        this.i = z2 ? 1 : 0;
    }

    public void b(boolean z2) {
        this.j = z2 ? 1 : 0;
    }

    public boolean a() {
        return this.i == 1;
    }

    public boolean b() {
        return this.j == 1;
    }

    public void onWindowFocusChanged(boolean z2) {
        if (!z2) {
            try {
                e();
            } catch (Exception e2) {
            }
            try {
                Mobclix.getInstance().q.a();
            } catch (Exception e3) {
            }
            try {
                if (this.y != null) {
                    this.y.b();
                }
            } catch (Exception e4) {
            }
        } else if (this.y != null) {
            Mobclix.getInstance().G();
            if (!this.K) {
                c();
            }
            if (this.y != null) {
                this.y.c();
            }
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        synchronized (this) {
            if (this.J != null) {
                this.J.cancel();
                this.J.purge();
                this.J = null;
            }
        }
    }

    public void c() {
        if (Mobclix.getInstance().b(this.p)) {
            if (this.L != 0) {
                a(this.L);
            } else {
                a(Mobclix.getInstance().c(this.p));
            }
        }
        this.K = false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        com.mobclix.android.sdk.Mobclix.getInstance().h("Mozilla/5.0 (Linux; U; Android 1.1; en-us; dream) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0113, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0114, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0113 A[ExcHandler: MobclixPermissionException (r0v12 'e' com.mobclix.android.sdk.Mobclix$MobclixPermissionException A[CUSTOM_DECLARE]), Splitter:B:7:0x0045] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.app.Activity r10) {
        /*
            r9 = this;
            r8 = 0
            r7 = 1
            r1 = 0
            boolean r0 = r9.isInEditMode()
            if (r0 == 0) goto L_0x000a
        L_0x0009:
            return
        L_0x000a:
            java.lang.Object r2 = r9.a
            monitor-enter(r2)
            com.mobclix.android.sdk.MobclixInstrumentation r0 = com.mobclix.android.sdk.MobclixInstrumentation.a()     // Catch:{ all -> 0x0104 }
            r9.G = r0     // Catch:{ all -> 0x0104 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0104 }
            java.lang.String r3 = com.mobclix.android.sdk.MobclixInstrumentation.b     // Catch:{ all -> 0x0104 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ all -> 0x0104 }
            r0.<init>(r3)     // Catch:{ all -> 0x0104 }
            java.lang.String r3 = "_"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x0104 }
            java.lang.String r3 = r9.p     // Catch:{ all -> 0x0104 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x0104 }
            java.lang.String r3 = "_"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x0104 }
            int r3 = com.mobclix.android.sdk.MobclixAdView.e     // Catch:{ all -> 0x0104 }
            int r3 = r3 + 1
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x0104 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0104 }
            r9.f = r0     // Catch:{ all -> 0x0104 }
            int r0 = com.mobclix.android.sdk.MobclixAdView.e     // Catch:{ all -> 0x0104 }
            int r0 = r0 + 1
            com.mobclix.android.sdk.MobclixAdView.e = r0     // Catch:{ all -> 0x0104 }
            monitor-exit(r2)     // Catch:{ all -> 0x0104 }
            com.mobclix.android.sdk.Mobclix.onCreate(r10)     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.getInstance()     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            java.lang.String r0 = r0.A()     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            java.lang.String r2 = "null"
            boolean r0 = r0.equals(r2)     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            if (r0 == 0) goto L_0x0082
            android.webkit.WebView r0 = new android.webkit.WebView     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            android.content.Context r2 = r9.getContext()     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            r0.<init>(r2)     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            android.webkit.WebSettings r0 = r0.getSettings()     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            java.lang.Class r2 = r0.getClass()     // Catch:{ Exception -> 0x0107, MobclixPermissionException -> 0x0113 }
            java.lang.String r3 = "getUserAgentString"
            r4 = 0
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0107, MobclixPermissionException -> 0x0113 }
            java.lang.reflect.Method r2 = r2.getDeclaredMethod(r3, r4)     // Catch:{ Exception -> 0x0107, MobclixPermissionException -> 0x0113 }
            com.mobclix.android.sdk.Mobclix r3 = com.mobclix.android.sdk.Mobclix.getInstance()     // Catch:{ Exception -> 0x0107, MobclixPermissionException -> 0x0113 }
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0107, MobclixPermissionException -> 0x0113 }
            java.lang.Object r0 = r2.invoke(r0, r4)     // Catch:{ Exception -> 0x0107, MobclixPermissionException -> 0x0113 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0107, MobclixPermissionException -> 0x0113 }
            r3.h(r0)     // Catch:{ Exception -> 0x0107, MobclixPermissionException -> 0x0113 }
        L_0x0082:
            com.mobclix.android.sdk.MobclixCreative$HTMLPagePool r0 = new com.mobclix.android.sdk.MobclixCreative$HTMLPagePool     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            r0.<init>()     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            r9.A = r0     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            com.mobclix.android.sdk.MobclixAdView$MobclixUtilityView r0 = new com.mobclix.android.sdk.MobclixAdView$MobclixUtilityView     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            android.content.Context r2 = r9.getContext()     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            r0.<init>(r2)     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            r9.E = r0     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            com.mobclix.android.sdk.MobclixAdView$MobclixUtilityView r0 = r9.E     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            android.widget.FrameLayout$LayoutParams r2 = new android.widget.FrameLayout$LayoutParams     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            r3 = -1
            r4 = -1
            r2.<init>(r3, r4)     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            r0.setLayoutParams(r2)     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            com.mobclix.android.sdk.MobclixAdView$MobclixUtilityView r0 = r9.E     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            r2 = 0
            r0.setBackgroundColor(r2)     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
        L_0x00a6:
            java.lang.String[] r2 = com.mobclix.android.sdk.Mobclix.a
            int r3 = r2.length
            r0 = r1
        L_0x00aa:
            if (r0 < r3) goto L_0x0115
            r9.requestDisallowInterceptTouchEvent(r7)
            int r0 = r9.q
            r9.setBackgroundColor(r0)
            java.lang.Object r0 = r9.getTag()     // Catch:{ Exception -> 0x0130 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0130 }
            r9.I = r0     // Catch:{ Exception -> 0x0130 }
        L_0x00be:
            java.lang.String r0 = "com.admob.android.ads.AdManager"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x012e }
            java.lang.String r1 = "setTestDevices"
            r2 = 1
            java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ Exception -> 0x012e }
            r3 = 0
            java.lang.Class<java.lang.String[]> r4 = java.lang.String[].class
            r2[r3] = r4     // Catch:{ Exception -> 0x012e }
            java.lang.reflect.Method r1 = r0.getMethod(r1, r2)     // Catch:{ Exception -> 0x012e }
            java.lang.String r2 = "TEST_EMULATOR"
            java.lang.reflect.Field r0 = r0.getField(r2)     // Catch:{ Exception -> 0x012e }
            r2 = 0
            java.lang.Object r0 = r0.get(r2)     // Catch:{ Exception -> 0x012e }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x012e }
            r2 = 0
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x012e }
            r4 = 0
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ Exception -> 0x012e }
            r6 = 0
            r5[r6] = r0     // Catch:{ Exception -> 0x012e }
            r3[r4] = r5     // Catch:{ Exception -> 0x012e }
            r1.invoke(r2, r3)     // Catch:{ Exception -> 0x012e }
        L_0x00ef:
            com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.getInstance()
            if (r0 == 0) goto L_0x0009
            java.lang.Thread r0 = new java.lang.Thread
            com.mobclix.android.sdk.MobclixAdView$WaitForRemoteConfigThread r1 = new com.mobclix.android.sdk.MobclixAdView$WaitForRemoteConfigThread
            r1.<init>(r9, r8)
            r0.<init>(r1)
            r0.start()
            goto L_0x0009
        L_0x0104:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0104 }
            throw r0
        L_0x0107:
            r0 = move-exception
            com.mobclix.android.sdk.Mobclix r0 = com.mobclix.android.sdk.Mobclix.getInstance()     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            java.lang.String r2 = "Mozilla/5.0 (Linux; U; Android 1.1; en-us; dream) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2"
            r0.h(r2)     // Catch:{ MobclixPermissionException -> 0x0113, Exception -> 0x0132 }
            goto L_0x0082
        L_0x0113:
            r0 = move-exception
            throw r0
        L_0x0115:
            r1 = r2[r0]
            java.util.HashMap<java.lang.String, java.lang.Long> r4 = com.mobclix.android.sdk.MobclixAdView.n
            boolean r4 = r4.containsKey(r1)
            if (r4 != 0) goto L_0x012a
            java.util.HashMap<java.lang.String, java.lang.Long> r4 = com.mobclix.android.sdk.MobclixAdView.n
            r5 = 0
            java.lang.Long r5 = java.lang.Long.valueOf(r5)
            r4.put(r1, r5)
        L_0x012a:
            int r0 = r0 + 1
            goto L_0x00aa
        L_0x012e:
            r0 = move-exception
            goto L_0x00ef
        L_0x0130:
            r0 = move-exception
            goto L_0x00be
        L_0x0132:
            r0 = move-exception
            goto L_0x00a6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixAdView.a(android.app.Activity):void");
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        super.onSaveInstanceState();
        e();
        if (this.y == null) {
            return null;
        }
        this.y.d();
        Bundle bundle = new Bundle();
        bundle.putString("response", this.B);
        bundle.putInt("nCreative", this.C.a());
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        super.onRestoreInstanceState(View.BaseSavedState.EMPTY_STATE);
        try {
            this.B = ((Bundle) parcelable).getString("response");
            if (!this.B.equals("")) {
                try {
                    this.C = new MobclixCreativeManager(this.B, ((Bundle) parcelable).getInt("nCreative"));
                } catch (Exception e2) {
                }
                f();
                this.l = true;
            }
        } catch (Exception e3) {
        }
    }

    public void a(String str) {
        this.y = null;
        if (this.D > 3) {
            this.D = 0;
            return;
        }
        this.D++;
        this.m = 0;
        if (this.C == null || this.C.a == null) {
            b(str);
        } else if (!this.C.c()) {
            b(str);
        } else {
            try {
                this.y = new MobclixCreative(this, this.C.d(), false);
                if (!this.y.a()) {
                    a("");
                }
            } catch (Exception e2) {
                a("");
            }
        }
    }

    public void d() {
        b((String) null);
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (this.d && !this.k) {
            if (this.H != null) {
                this.H.interrupt();
                this.H = null;
            }
            String b2 = this.G.b(this.G.a(this.f, MobclixInstrumentation.b), "start_request");
            FetchAdResponseThread fetchAdResponseThread = new FetchAdResponseThread(this.b);
            if (str != null) {
                fetchAdResponseThread.b(str);
            }
            this.H = new Thread(fetchAdResponseThread);
            this.H.start();
            this.G.e(b2);
        }
    }

    private void f() {
        try {
            Mobclix.onCreate((Activity) getContext());
        } catch (Exception e2) {
        }
        try {
            if (this.H != null) {
                this.H.interrupt();
            }
            if (Mobclix.getInstance().b(this.p)) {
                this.d = true;
                this.y = new MobclixCreative(this, this.C.d(), true);
                a(Mobclix.getInstance().c(this.p));
                return;
            }
            Iterator<MobclixAdViewListener> it = this.u.iterator();
            while (it.hasNext()) {
                MobclixAdViewListener next = it.next();
                if (next != null) {
                    next.b(this, -999999);
                }
            }
        } catch (Exception e3) {
        }
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i2) {
        if (i2 == 0 && this.y != null && !this.y.c) {
            this.y.f();
        }
    }

    private class WaitForRemoteConfigThread implements Runnable {
        private WaitForRemoteConfigThread() {
        }

        /* synthetic */ WaitForRemoteConfigThread(MobclixAdView mobclixAdView, WaitForRemoteConfigThread waitForRemoteConfigThread) {
            this();
        }

        public void run() {
            Long valueOf = Long.valueOf(System.currentTimeMillis());
            do {
                try {
                    if (Mobclix.getInstance().z() == 1) {
                        break;
                    }
                } catch (Exception e) {
                    return;
                }
            } while (System.currentTimeMillis() - valueOf.longValue() < 10000);
            CookieManager.getInstance().setAcceptCookie(true);
            MobclixAdView.this.c.sendEmptyMessage(0);
        }
    }

    private class RemoteConfigReadyHandler extends Handler {
        private RemoteConfigReadyHandler() {
        }

        /* synthetic */ RemoteConfigReadyHandler(MobclixAdView mobclixAdView, RemoteConfigReadyHandler remoteConfigReadyHandler) {
            this();
        }

        public void handleMessage(Message message) {
            if (!MobclixAdView.this.l) {
                if (Mobclix.getInstance().b(MobclixAdView.this.p)) {
                    MobclixAdView.this.d = true;
                    if (MobclixAdView.this.i == -1) {
                        MobclixAdView.this.a(Mobclix.getInstance().d(MobclixAdView.this.p));
                    }
                    if (MobclixAdView.this.j == -1) {
                        MobclixAdView.this.b(Mobclix.getInstance().g(MobclixAdView.this.p));
                    }
                    if (MobclixAdView.this.L == 0 && Mobclix.getInstance().c(MobclixAdView.this.p) >= 15000) {
                        MobclixAdView.this.a(Mobclix.getInstance().c(MobclixAdView.this.p));
                    }
                    MobclixAdView.this.d();
                    return;
                }
                Iterator<MobclixAdViewListener> it = MobclixAdView.this.u.iterator();
                while (it.hasNext()) {
                    MobclixAdViewListener next = it.next();
                    if (next != null) {
                        next.b(MobclixAdView.this, -999999);
                    }
                }
            }
        }
    }

    private class FetchAdResponseThread extends Mobclix.FetchResponseThread {
        String a = "";

        FetchAdResponseThread(Handler handler) {
            super("", handler);
        }

        public void run() {
            if (MobclixAdView.this.k) {
                MobclixAdView.this.e();
                a((int) MobclixAdViewListener.APP_NOT_IN_FOREGROUND);
                return;
            }
            try {
                if (((KeyguardManager) MobclixAdView.this.getContext().getSystemService("keyguard")).inKeyguardRestrictedInputMode()) {
                    MobclixAdView.this.e();
                    a((int) MobclixAdViewListener.APP_NOT_IN_FOREGROUND);
                    return;
                }
            } catch (Exception e) {
            }
            try {
                if (!a()) {
                    MobclixAdView.this.e();
                    a((int) MobclixAdViewListener.APP_NOT_IN_FOREGROUND);
                    return;
                }
            } catch (Exception e2) {
            }
            try {
                if (MobclixAdView.this.E != null) {
                    if (!MobclixAdView.this.E.b() || !MobclixAdView.this.E.c()) {
                        MobclixAdView.this.E.a();
                    } else {
                        MobclixAdView.this.E.postInvalidate();
                        return;
                    }
                }
            } catch (Exception e3) {
            }
            if (System.currentTimeMillis() >= MobclixAdView.this.m + 5000) {
                MobclixAdView.this.m = System.currentTimeMillis();
                a(b());
                super.run();
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            try {
                List list = (List) ActivityManager.class.getMethod("getRunningAppProcesses", null).invoke((ActivityManager) MobclixAdView.this.getContext().getSystemService("activity"), null);
                if (list == null) {
                    return false;
                }
                String packageName = MobclixAdView.this.getContext().getPackageName();
                Class<?> cls = list.get(0).getClass();
                Field declaredField = cls.getDeclaredField("importance");
                Field declaredField2 = cls.getDeclaredField("processName");
                Field declaredField3 = cls.getDeclaredField("IMPORTANCE_FOREGROUND");
                for (Object next : list) {
                    int i = declaredField.getInt(next);
                    String str = (String) declaredField2.get(next);
                    if (i == declaredField3.getInt(next) && str.equals(packageName)) {
                        return true;
                    }
                }
                return false;
            } catch (Exception e) {
                return true;
            }
        }

        /* access modifiers changed from: package-private */
        public void b(String str) {
            if (str == null) {
                str = "";
            }
            this.a = str;
        }

        private String b() {
            String str;
            String b2 = MobclixAdView.this.G.b(MobclixAdView.this.G.a(MobclixAdView.this.f, MobclixInstrumentation.b), "build_request");
            StringBuffer stringBuffer = new StringBuffer();
            StringBuffer stringBuffer2 = new StringBuffer();
            String str2 = "";
            StringBuffer stringBuffer3 = new StringBuffer();
            try {
                Mobclix instance = Mobclix.getInstance();
                b2 = MobclixAdView.this.G.b(b2, "ad_feed_id_params");
                stringBuffer.append(instance.w());
                stringBuffer.append("?p=android");
                if (MobclixAdView.this.w == null || MobclixAdView.this.w.equals("")) {
                    if (MobclixAdView.this.v.equals("")) {
                        stringBuffer.append("&i=").append(URLEncoder.encode(instance.b(), "UTF-8"));
                        stringBuffer.append("&s=").append(URLEncoder.encode(MobclixAdView.this.p, "UTF-8"));
                    } else {
                        stringBuffer.append("&a=").append(URLEncoder.encode(MobclixAdView.this.v, "UTF-8"));
                    }
                    if (MobclixAdView.this.o != null && !MobclixAdView.this.o.equals("")) {
                        stringBuffer.append("&adurl=").append(URLEncoder.encode(MobclixAdView.this.o, "UTF-8"));
                    }
                } else {
                    stringBuffer.append("&cr=").append(URLEncoder.encode(MobclixAdView.this.w, "UTF-8"));
                }
                stringBuffer.append("&rt=").append(URLEncoder.encode(instance.e(), "UTF-8"));
                stringBuffer.append("&rtv=").append(URLEncoder.encode(instance.f(), "UTF-8"));
                String b3 = MobclixAdView.this.G.b(MobclixAdView.this.G.e(b2), "software_env");
                stringBuffer.append("&av=").append(URLEncoder.encode(instance.h(), "UTF-8"));
                stringBuffer.append("&u=").append(URLEncoder.encode(instance.i(), "UTF-8"));
                stringBuffer.append("&andid=").append(URLEncoder.encode(instance.j(), "UTF-8"));
                stringBuffer.append("&v=").append(URLEncoder.encode(instance.u()));
                stringBuffer.append("&ct=").append(URLEncoder.encode(instance.m()));
                String b4 = MobclixAdView.this.G.b(MobclixAdView.this.G.e(b3), "hardware_env");
                stringBuffer.append("&dm=").append(URLEncoder.encode(instance.k(), "UTF-8"));
                stringBuffer.append("&hwdm=").append(URLEncoder.encode(instance.l(), "UTF-8"));
                stringBuffer.append("&sv=").append(URLEncoder.encode(instance.g(), "UTF-8"));
                stringBuffer.append("&ua=").append(URLEncoder.encode(instance.A(), "UTF-8"));
                if (instance.B()) {
                    if (instance.C()) {
                        stringBuffer.append("&jb=1");
                    } else {
                        stringBuffer.append("&jb=0");
                    }
                }
                String b5 = MobclixAdView.this.G.b(MobclixAdView.this.G.e(b4), "ad_view_state_id_params");
                stringBuffer.append("&o=").append(MobclixAdView.this.x);
                MobclixAdView.this.x++;
                if (MobclixAdView.this.i != 1 || !Mobclix.getInstance().f(MobclixAdView.this.p)) {
                    stringBuffer.append("&ap=0");
                } else {
                    stringBuffer.append("&ap=1");
                }
                if (MobclixAdView.this.I != null && !MobclixAdView.this.I.equals("")) {
                    stringBuffer.append("&as=").append(URLEncoder.encode(MobclixAdView.this.I));
                }
                if (MobclixAdView.this.h) {
                    stringBuffer.append("&t=1");
                }
                String b6 = MobclixAdView.this.G.b(MobclixAdView.this.G.e(b5), "geo_lo");
                if (!instance.p().equals("null")) {
                    stringBuffer.append("&ll=").append(URLEncoder.encode(instance.p(), "UTF-8"));
                }
                stringBuffer.append("&l=").append(URLEncoder.encode(instance.q(), "UTF-8"));
                String e = MobclixAdView.this.G.e(b6);
                if (instance.s() != null && !instance.s().equals("null")) {
                    stringBuffer.append("&mcc=").append(URLEncoder.encode(instance.s(), "UTF-8"));
                }
                if (instance.t() != null && !instance.t().equals("null")) {
                    stringBuffer.append("&mnc=").append(URLEncoder.encode(instance.t(), "UTF-8"));
                }
                String b7 = MobclixAdView.this.G.b(e, "keywords");
                try {
                    Iterator<MobclixAdViewListener> it = MobclixAdView.this.u.iterator();
                    while (it.hasNext()) {
                        MobclixAdViewListener next = it.next();
                        if (next != null) {
                            str2 = next.a();
                        }
                        if (str2 == null) {
                            str2 = "";
                        }
                        if (!str2.equals("")) {
                            if (stringBuffer2.length() == 0) {
                                stringBuffer2.append("&k=").append(URLEncoder.encode(str2, "UTF-8"));
                            } else {
                                stringBuffer2.append("%2C").append(URLEncoder.encode(str2, "UTF-8"));
                            }
                        }
                        String b8 = next.b();
                        if (b8 == null) {
                            b8 = "";
                        }
                        if (!b8.equals("")) {
                            if (stringBuffer3.length() == 0) {
                                stringBuffer3.append("&q=").append(URLEncoder.encode(b8, "UTF-8"));
                            } else {
                                stringBuffer3.append("%2B").append(URLEncoder.encode(b8, "UTF-8"));
                            }
                        }
                    }
                    if (stringBuffer2.length() > 0) {
                        stringBuffer.append(stringBuffer2);
                    }
                    String b9 = MobclixAdView.this.G.b(MobclixAdView.this.G.e(b7), "query");
                    if (stringBuffer3.length() > 0) {
                        stringBuffer.append(stringBuffer3);
                    }
                    String b10 = MobclixAdView.this.G.b(MobclixAdView.this.G.e(b9), "additional_params");
                    if (!this.a.equals("")) {
                        stringBuffer.append(this.a);
                    }
                    this.a = "";
                    if (MobclixDemographics.b != null) {
                        if (MobclixDemographics.b.containsKey(MobclixDemographics.Birthdate)) {
                            stringBuffer.append("&d=").append(URLEncoder.encode(MobclixDemographics.b.get(MobclixDemographics.Birthdate), "UTF-8"));
                        }
                        if (MobclixDemographics.b.containsKey(MobclixDemographics.Education)) {
                            stringBuffer.append("&e=").append(URLEncoder.encode(MobclixDemographics.b.get(MobclixDemographics.Education), "UTF-8"));
                        }
                        if (MobclixDemographics.b.containsKey(MobclixDemographics.Ethnicity)) {
                            stringBuffer.append("&r=").append(URLEncoder.encode(MobclixDemographics.b.get(MobclixDemographics.Ethnicity), "UTF-8"));
                        }
                        if (MobclixDemographics.b.containsKey(MobclixDemographics.Gender)) {
                            stringBuffer.append("&g=").append(URLEncoder.encode(MobclixDemographics.b.get(MobclixDemographics.Gender), "UTF-8"));
                        }
                        if (MobclixDemographics.b.containsKey(MobclixDemographics.DatingGender)) {
                            stringBuffer.append("&dg=").append(URLEncoder.encode(MobclixDemographics.b.get(MobclixDemographics.DatingGender), "UTF-8"));
                        }
                        if (MobclixDemographics.b.containsKey(MobclixDemographics.Income)) {
                            stringBuffer.append("&m=").append(URLEncoder.encode(MobclixDemographics.b.get(MobclixDemographics.Income), "UTF-8"));
                        }
                        if (MobclixDemographics.b.containsKey(MobclixDemographics.MaritalStatus)) {
                            stringBuffer.append("&x=").append(URLEncoder.encode(MobclixDemographics.b.get(MobclixDemographics.MaritalStatus), "UTF-8"));
                        }
                        if (MobclixDemographics.b.containsKey(MobclixDemographics.Religion)) {
                            stringBuffer.append("&j=").append(URLEncoder.encode(MobclixDemographics.b.get(MobclixDemographics.Religion), "UTF-8"));
                        }
                        if (MobclixDemographics.b.containsKey(MobclixDemographics.AreaCode)) {
                            stringBuffer.append("&c=").append(URLEncoder.encode(MobclixDemographics.b.get(MobclixDemographics.AreaCode), "UTF-8"));
                        }
                        if (MobclixDemographics.b.containsKey(MobclixDemographics.City)) {
                            stringBuffer.append("&ci=").append(URLEncoder.encode(MobclixDemographics.b.get(MobclixDemographics.City), "UTF-8"));
                        }
                        if (MobclixDemographics.b.containsKey(MobclixDemographics.Country)) {
                            stringBuffer.append("&co=").append(URLEncoder.encode(MobclixDemographics.b.get(MobclixDemographics.Country), "UTF-8"));
                        }
                        if (MobclixDemographics.b.containsKey(MobclixDemographics.DMACode)) {
                            stringBuffer.append("&dc=").append(URLEncoder.encode(MobclixDemographics.b.get(MobclixDemographics.DMACode), "UTF-8"));
                        }
                        if (MobclixDemographics.b.containsKey(MobclixDemographics.PostalCode)) {
                            stringBuffer.append("&z=").append(URLEncoder.encode(MobclixDemographics.b.get(MobclixDemographics.PostalCode), "UTF-8"));
                        }
                        if (MobclixDemographics.b.containsKey(MobclixDemographics.Region)) {
                            stringBuffer.append("&re=").append(URLEncoder.encode(MobclixDemographics.b.get(MobclixDemographics.Region), "UTF-8"));
                        }
                    }
                    String e2 = MobclixAdView.this.G.e(MobclixAdView.this.G.e(b10));
                    MobclixAdView.this.G.a(stringBuffer.toString(), "request_url", MobclixAdView.this.f);
                    return stringBuffer.toString();
                } catch (Exception e3) {
                    b2 = str;
                    MobclixAdView.this.G.e(MobclixAdView.this.G.e(b2));
                    MobclixAdView.this.G.d(MobclixAdView.this.f);
                    return "";
                }
            } catch (Exception e4) {
                MobclixAdView.this.G.e(MobclixAdView.this.G.e(b2));
                MobclixAdView.this.G.d(MobclixAdView.this.f);
                return "";
            }
        }
    }

    private class AdResponseHandler extends Handler {
        private AdResponseHandler() {
        }

        /* synthetic */ AdResponseHandler(MobclixAdView mobclixAdView, AdResponseHandler adResponseHandler) {
            this();
        }

        public void handleMessage(Message message) {
            if (!MobclixAdView.this.k) {
                String string = message.getData().getString(UmengConstants.AtomKey_Type);
                if (string.equals("success")) {
                    String b = MobclixAdView.this.G.b(MobclixAdView.this.G.a(MobclixAdView.this.f, MobclixInstrumentation.b), "handle_response");
                    if (MobclixAdView.this.y != null) {
                        MobclixAdView.this.z = MobclixAdView.this.y;
                    }
                    try {
                        String b2 = MobclixAdView.this.G.b(b, "a_decode_json");
                        MobclixAdView.this.B = message.getData().getString("response");
                        MobclixAdView.this.G.a(MobclixAdView.this.B, "raw_json", MobclixAdView.this.f);
                        MobclixAdView.this.C = new MobclixCreativeManager(MobclixAdView.this.B, 0);
                        if (MobclixAdView.this.C.b() >= 1) {
                            MobclixAdView.this.G.a(MobclixAdView.this.C.d(), "decoded_json", MobclixAdView.this.f);
                            MobclixAdView.this.y = new MobclixCreative(MobclixAdView.this, MobclixAdView.this.C.d(), false);
                            if (!MobclixAdView.this.y.a()) {
                                MobclixAdView.this.a("");
                            }
                        }
                        MobclixAdView.this.G.e(b2);
                    } catch (Exception e) {
                        MobclixAdView.this.G.e(MobclixAdView.this.G.e(b));
                        MobclixAdView.this.G.d(MobclixAdView.this.f);
                        MobclixAdView.this.a("");
                    }
                } else if (string.equals("failure")) {
                    int i = message.getData().getInt("errorCode");
                    Iterator<MobclixAdViewListener> it = MobclixAdView.this.u.iterator();
                    while (it.hasNext()) {
                        MobclixAdViewListener next = it.next();
                        if (next != null) {
                            next.b(MobclixAdView.this, i);
                        }
                    }
                    MobclixAdView.this.m = 0;
                }
            }
        }
    }

    class MobclixUtilityView extends View {
        boolean a = false;
        boolean b = false;

        /* access modifiers changed from: package-private */
        public void a() {
            this.a = false;
            this.b = false;
        }

        /* access modifiers changed from: package-private */
        public boolean b() {
            return this.a;
        }

        /* access modifiers changed from: package-private */
        public boolean c() {
            return this.b;
        }

        public MobclixUtilityView(Context context) {
            super(context);
        }

        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            if (this.b) {
                Mobclix.getInstance().G();
                this.a = false;
                this.b = false;
            }
        }
    }
}
