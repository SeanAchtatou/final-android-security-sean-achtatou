package com.mobclix.android.sdk;

import android.app.Activity;
import com.mobclix.android.sdk.MobclixUtility;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class MobclixFeedback {
    static Mobclix a = Mobclix.getInstance();
    private static String b = "mobclixFeedback";

    public interface Listener {
        void a();

        void b();
    }

    public static class Ratings {
        int a = 0;
        int b = 0;
        int c = 0;
        int d = 0;
        int e = 0;
    }

    public static void sendComment(Activity activity, String str) {
        sendComment(activity, str, null);
    }

    public static void sendComment(Activity activity, String str, Listener listener) {
        if (str != null) {
            str.trim();
            if (str.length() != 0) {
                Mobclix.onCreate(activity);
                String x = a.x();
                StringBuffer stringBuffer = new StringBuffer();
                try {
                    stringBuffer.append("p=android&t=com");
                    stringBuffer.append("&a=").append(URLEncoder.encode(a.b(), "UTF-8"));
                    stringBuffer.append("&v=").append(URLEncoder.encode(a.h(), "UTF-8"));
                    stringBuffer.append("&m=").append(URLEncoder.encode(a.u(), "UTF-8"));
                    stringBuffer.append("&d=").append(URLEncoder.encode(a.i(), "UTF-8"));
                    stringBuffer.append("&dt=").append(URLEncoder.encode(a.k(), "UTF-8"));
                    stringBuffer.append("&os=").append(URLEncoder.encode(a.g(), "UTF-8"));
                    stringBuffer.append("&c=").append(URLEncoder.encode(str, "UTF-8"));
                    new Thread(new MobclixUtility.POSTThread(x, stringBuffer.toString(), activity, listener)).run();
                } catch (UnsupportedEncodingException e) {
                }
            }
        }
    }

    public static void sendRatings(Activity activity, Ratings ratings) {
        sendRatings(activity, ratings, null);
    }

    public static void sendRatings(Activity activity, Ratings ratings, Listener listener) {
        if (ratings != null) {
            Mobclix.onCreate(activity);
            String x = a.x();
            StringBuffer stringBuffer = new StringBuffer();
            try {
                stringBuffer.append("p=android&t=rat");
                stringBuffer.append("&a=").append(URLEncoder.encode(a.b(), "UTF-8"));
                stringBuffer.append("&v=").append(URLEncoder.encode(a.h(), "UTF-8"));
                stringBuffer.append("&m=").append(URLEncoder.encode(a.u(), "UTF-8"));
                stringBuffer.append("&d=").append(URLEncoder.encode(a.i(), "UTF-8"));
                stringBuffer.append("&dt=").append(URLEncoder.encode(a.k(), "UTF-8"));
                stringBuffer.append("&os=").append(URLEncoder.encode(a.g(), "UTF-8"));
                stringBuffer.append("&1=").append(ratings.a);
                stringBuffer.append("&2=").append(ratings.b);
                stringBuffer.append("&3=").append(ratings.c);
                stringBuffer.append("&4=").append(ratings.d);
                stringBuffer.append("&5=").append(ratings.e);
                new Thread(new MobclixUtility.POSTThread(x, stringBuffer.toString(), activity, listener)).run();
            } catch (UnsupportedEncodingException e) {
            }
        }
    }
}
