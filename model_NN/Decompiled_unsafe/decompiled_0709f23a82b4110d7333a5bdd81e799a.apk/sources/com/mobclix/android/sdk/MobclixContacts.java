package com.mobclix.android.sdk;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import org.json.JSONObject;

public abstract class MobclixContacts {
    private static MobclixContacts a;

    public abstract Intent a();

    public abstract Intent a(JSONObject jSONObject);

    public abstract JSONObject a(ContentResolver contentResolver, Uri uri);

    public abstract void a(JSONObject jSONObject, Activity activity) throws Exception;

    public static MobclixContacts getInstance() {
        String str;
        if (a == null) {
            if (Integer.parseInt(Build.VERSION.SDK) < 5) {
                str = "com.mobclix.android.sdk.MobclixContactsSdk3_4";
            } else {
                str = "com.mobclix.android.sdk.MobclixContactsSdk5";
            }
            try {
                a = (MobclixContacts) Class.forName(str).asSubclass(MobclixContacts.class).newInstance();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return a;
    }
}
