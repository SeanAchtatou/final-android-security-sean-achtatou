package com.mobclix.android.sdk;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.provider.Contacts;
import com.mobclick.android.UmengConstants;
import org.codehaus.jackson.util.MinimalPrettyPrinter;
import org.json.JSONArray;
import org.json.JSONObject;

public class MobclixContactsSdk3_4 extends MobclixContacts {
    public Intent a() {
        return new Intent("android.intent.action.PICK", Contacts.People.CONTENT_URI);
    }

    public Intent a(JSONObject jSONObject) {
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x00ab A[Catch:{ Exception -> 0x028c, all -> 0x0287 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00da A[Catch:{ Exception -> 0x0284, all -> 0x027f }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01d4 A[SYNTHETIC, Splitter:B:86:0x01d4] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x020e A[SYNTHETIC, Splitter:B:96:0x020e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.json.JSONObject a(android.content.ContentResolver r13, android.net.Uri r14) {
        /*
            r12 = this;
            r8 = 3
            r11 = 2
            r9 = 1
            r2 = 0
            org.json.JSONObject r6 = new org.json.JSONObject
            r6.<init>()
            r0 = r13
            r1 = r14
            r3 = r2
            r4 = r2
            r5 = r2
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5)
            boolean r0 = r7.moveToFirst()     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            if (r0 == 0) goto L_0x019f
            java.lang.String r0 = "name"
            int r0 = r7.getColumnIndexOrThrow(r0)     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            java.lang.String r0 = r7.getString(r0)     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            java.lang.String r1 = " "
            java.lang.String[] r4 = r0.split(r1)     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            int r0 = r4.length     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            if (r0 != r9) goto L_0x0169
            r0 = 0
            r0 = r4[r0]     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            r1 = r2
            r3 = r0
            r0 = r2
        L_0x0031:
            java.lang.String r4 = "_id"
            int r4 = r7.getColumnIndex(r4)     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            java.lang.String r9 = r7.getString(r4)     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            java.lang.String r4 = "firstName"
            r6.put(r4, r3)     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            java.lang.String r3 = "middleName"
            r6.put(r3, r1)     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            java.lang.String r1 = "lastName"
            r6.put(r1, r0)     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            java.lang.String r0 = "note"
            java.lang.String r1 = "notes"
            int r1 = r7.getColumnIndexOrThrow(r1)     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            java.lang.String r1 = r7.getString(r1)     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            r6.put(r0, r1)     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            r7.close()
            java.lang.String r0 = "organizations"
            android.net.Uri r1 = android.net.Uri.withAppendedPath(r14, r0)     // Catch:{ Exception -> 0x01ad, all -> 0x01b5 }
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "isprimary DESC"
            r0 = r13
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x01ad, all -> 0x01b5 }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x0294, all -> 0x028f }
            if (r1 == 0) goto L_0x0090
            java.lang.String r1 = "organization"
            java.lang.String r2 = "company"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0294, all -> 0x028f }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0294, all -> 0x028f }
            r6.put(r1, r2)     // Catch:{ Exception -> 0x0294, all -> 0x028f }
            java.lang.String r1 = "jobTitle"
            java.lang.String r2 = "title"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x0294, all -> 0x028f }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0294, all -> 0x028f }
            r6.put(r1, r2)     // Catch:{ Exception -> 0x0294, all -> 0x028f }
        L_0x0090:
            r0.close()
            r7 = r0
        L_0x0094:
            android.net.Uri r1 = android.provider.Contacts.ContactMethods.CONTENT_EMAIL_URI     // Catch:{ Exception -> 0x01ba, all -> 0x01c2 }
            r2 = 0
            java.lang.String r3 = "person = ?"
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01ba, all -> 0x01c2 }
            r0 = 0
            r4[r0] = r9     // Catch:{ Exception -> 0x01ba, all -> 0x01c2 }
            r5 = 0
            r0 = r13
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x01ba, all -> 0x01c2 }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x028c, all -> 0x0287 }
            if (r1 == 0) goto L_0x00ba
            java.lang.String r1 = "email"
            java.lang.String r2 = "data"
            int r2 = r0.getColumnIndexOrThrow(r2)     // Catch:{ Exception -> 0x028c, all -> 0x0287 }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x028c, all -> 0x0287 }
            r6.put(r1, r2)     // Catch:{ Exception -> 0x028c, all -> 0x0287 }
        L_0x00ba:
            r0.close()
            r7 = r0
        L_0x00be:
            java.lang.String r3 = "person = ? AND kind = ?"
            r0 = 2
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ Exception -> 0x01c7, all -> 0x01cf }
            r0 = 0
            r4[r0] = r9     // Catch:{ Exception -> 0x01c7, all -> 0x01cf }
            r0 = 1
            java.lang.String r1 = "vnd.android.cursor.item/jabber-im"
            r4[r0] = r1     // Catch:{ Exception -> 0x01c7, all -> 0x01cf }
            android.net.Uri r1 = android.provider.Contacts.ContactMethods.CONTENT_URI     // Catch:{ Exception -> 0x01c7, all -> 0x01cf }
            r2 = 0
            r5 = 0
            r0 = r13
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x01c7, all -> 0x01cf }
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x0284, all -> 0x027f }
            if (r1 == 0) goto L_0x00e9
            java.lang.String r1 = "IM"
            java.lang.String r2 = "data"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x0284, all -> 0x027f }
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Exception -> 0x0284, all -> 0x027f }
            r6.put(r1, r2)     // Catch:{ Exception -> 0x0284, all -> 0x027f }
        L_0x00e9:
            r0.close()
            r7 = r0
        L_0x00ed:
            java.lang.String r0 = "phones"
            android.net.Uri r1 = android.net.Uri.withAppendedPath(r14, r0)     // Catch:{ Exception -> 0x027c, all -> 0x0209 }
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "isprimary DESC"
            r0 = r13
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x027c, all -> 0x0209 }
            org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ Exception -> 0x0202, all -> 0x0278 }
            r2.<init>()     // Catch:{ Exception -> 0x0202, all -> 0x0278 }
        L_0x0102:
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x0202, all -> 0x0278 }
            if (r1 != 0) goto L_0x01d4
            java.lang.String r1 = "phoneNumbers"
            r6.put(r1, r2)     // Catch:{ Exception -> 0x0202, all -> 0x0278 }
            r0.close()
            r7 = r0
        L_0x0111:
            java.lang.String r3 = "person = ? AND kind = ?"
            r0 = 2
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ Exception -> 0x0275, all -> 0x026c }
            r0 = 0
            r4[r0] = r9     // Catch:{ Exception -> 0x0275, all -> 0x026c }
            r0 = 1
            r1 = 2
            java.lang.String r1 = java.lang.Integer.toString(r1)     // Catch:{ Exception -> 0x0275, all -> 0x026c }
            r4[r0] = r1     // Catch:{ Exception -> 0x0275, all -> 0x026c }
            android.net.Uri r1 = android.provider.Contacts.ContactMethods.CONTENT_URI     // Catch:{ Exception -> 0x0275, all -> 0x026c }
            r2 = 0
            r5 = 0
            r0 = r13
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0275, all -> 0x026c }
            org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            r2.<init>()     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
        L_0x012f:
            boolean r1 = r0.moveToNext()     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            if (r1 != 0) goto L_0x020e
            java.lang.String r1 = "addresses"
            r6.put(r1, r2)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "prefix"
            r2 = 0
            r6.put(r1, r2)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "suffix"
            r2 = 0
            r6.put(r1, r2)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "nickname"
            r2 = 0
            r6.put(r1, r2)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "department"
            r2 = 0
            r6.put(r1, r2)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "birthday"
            r2 = 0
            r6.put(r1, r2)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "website"
            r2 = 0
            r6.put(r1, r2)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "image"
            r2 = 0
            r6.put(r1, r2)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            r0.close()
        L_0x0167:
            r2 = r6
        L_0x0168:
            return r2
        L_0x0169:
            int r0 = r4.length     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            if (r0 != r11) goto L_0x0176
            r0 = 0
            r1 = r4[r0]     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            r0 = 1
            r0 = r4[r0]     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            r3 = r1
            r1 = r2
            goto L_0x0031
        L_0x0176:
            int r0 = r4.length     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            if (r0 < r8) goto L_0x0297
            r0 = 0
            r3 = r4[r0]     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            r0 = 1
            r1 = r4[r0]     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            r0 = 2
            r0 = r4[r0]     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            r5.<init>(r0)     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            r0 = r8
        L_0x0188:
            int r9 = r4.length     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            if (r0 < r9) goto L_0x0191
            java.lang.String r0 = r5.toString()     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            goto L_0x0031
        L_0x0191:
            java.lang.String r9 = " "
            java.lang.StringBuilder r9 = r5.append(r9)     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            r10 = r4[r0]     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            r9.append(r10)     // Catch:{ Exception -> 0x01a3, all -> 0x01a8 }
            int r0 = r0 + 1
            goto L_0x0188
        L_0x019f:
            r7.close()
            goto L_0x0168
        L_0x01a3:
            r0 = move-exception
            r7.close()
            goto L_0x0168
        L_0x01a8:
            r0 = move-exception
            r7.close()
            throw r0
        L_0x01ad:
            r0 = move-exception
            r0 = r7
        L_0x01af:
            r0.close()
            r7 = r0
            goto L_0x0094
        L_0x01b5:
            r0 = move-exception
        L_0x01b6:
            r7.close()
            throw r0
        L_0x01ba:
            r0 = move-exception
            r0 = r7
        L_0x01bc:
            r0.close()
            r7 = r0
            goto L_0x00be
        L_0x01c2:
            r0 = move-exception
        L_0x01c3:
            r7.close()
            throw r0
        L_0x01c7:
            r0 = move-exception
            r0 = r7
        L_0x01c9:
            r0.close()
            r7 = r0
            goto L_0x00ed
        L_0x01cf:
            r0 = move-exception
        L_0x01d0:
            r7.close()
            throw r0
        L_0x01d4:
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Exception -> 0x0202, all -> 0x0278 }
            r3.<init>()     // Catch:{ Exception -> 0x0202, all -> 0x0278 }
            java.lang.String r1 = "number"
            java.lang.String r4 = "number"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x0202, all -> 0x0278 }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ Exception -> 0x0202, all -> 0x0278 }
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0202, all -> 0x0278 }
            java.lang.String r1 = "home"
            java.lang.String r4 = "type"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x0202, all -> 0x0278 }
            int r4 = r0.getInt(r4)     // Catch:{ Exception -> 0x0202, all -> 0x0278 }
            if (r4 != r8) goto L_0x01f8
            java.lang.String r1 = "work"
        L_0x01f8:
            java.lang.String r4 = "label"
            r3.put(r4, r1)     // Catch:{ Exception -> 0x0202, all -> 0x0278 }
            r2.put(r3)     // Catch:{ Exception -> 0x0202, all -> 0x0278 }
            goto L_0x0102
        L_0x0202:
            r1 = move-exception
        L_0x0203:
            r0.close()
            r7 = r0
            goto L_0x0111
        L_0x0209:
            r0 = move-exception
        L_0x020a:
            r7.close()
            throw r0
        L_0x020e:
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            r3.<init>()     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "street"
            java.lang.String r4 = "data"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r4 = r0.getString(r4)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "home"
            java.lang.String r4 = "type"
            int r4 = r0.getColumnIndex(r4)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            int r4 = r0.getInt(r4)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            if (r4 != r11) goto L_0x0232
            java.lang.String r1 = "work"
        L_0x0232:
            java.lang.String r4 = "label"
            r3.put(r4, r1)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "city"
            r4 = 0
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "state"
            r4 = 0
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "postalCode"
            r4 = 0
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "country"
            r4 = 0
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "countryCode"
            r4 = 0
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "neighborhood"
            r4 = 0
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            java.lang.String r1 = "poBox"
            r4 = 0
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            r2.put(r3)     // Catch:{ Exception -> 0x0266, all -> 0x0271 }
            goto L_0x012f
        L_0x0266:
            r1 = move-exception
        L_0x0267:
            r0.close()
            goto L_0x0167
        L_0x026c:
            r0 = move-exception
        L_0x026d:
            r7.close()
            throw r0
        L_0x0271:
            r1 = move-exception
            r7 = r0
            r0 = r1
            goto L_0x026d
        L_0x0275:
            r0 = move-exception
            r0 = r7
            goto L_0x0267
        L_0x0278:
            r1 = move-exception
            r7 = r0
            r0 = r1
            goto L_0x020a
        L_0x027c:
            r0 = move-exception
            r0 = r7
            goto L_0x0203
        L_0x027f:
            r1 = move-exception
            r7 = r0
            r0 = r1
            goto L_0x01d0
        L_0x0284:
            r1 = move-exception
            goto L_0x01c9
        L_0x0287:
            r1 = move-exception
            r7 = r0
            r0 = r1
            goto L_0x01c3
        L_0x028c:
            r1 = move-exception
            goto L_0x01bc
        L_0x028f:
            r1 = move-exception
            r7 = r0
            r0 = r1
            goto L_0x01b6
        L_0x0294:
            r1 = move-exception
            goto L_0x01af
        L_0x0297:
            r0 = r2
            r1 = r2
            r3 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixContactsSdk3_4.a(android.content.ContentResolver, android.net.Uri):org.json.JSONObject");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void a(JSONObject jSONObject, Activity activity) throws Exception {
        String string;
        String string2;
        String string3;
        String string4;
        String string5;
        String string6;
        String string7;
        String string8;
        String string9;
        StringBuilder sb = new StringBuilder();
        if (jSONObject.has("firstName") && (string9 = jSONObject.getString("firstName")) != null) {
            sb.append(string9);
        }
        if (jSONObject.has("middleName") && (string8 = jSONObject.getString("middleName")) != null && !string8.equals("")) {
            if (sb.length() != 0) {
                sb.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            }
            sb.append(string8);
        }
        if (jSONObject.has("lastName") && (string7 = jSONObject.getString("lastName")) != null && !string7.equals("")) {
            if (sb.length() != 0) {
                sb.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            }
            sb.append(string7);
        }
        String sb2 = sb.toString();
        if (jSONObject.has("prefix") && jSONObject.getString("prefix") == null) {
        }
        if (jSONObject.has("suffix") && jSONObject.getString("suffix") == null) {
        }
        if (jSONObject.has("nickname") && jSONObject.getString("nickname") == null) {
        }
        String str = null;
        if (jSONObject.has("note") && (str = jSONObject.getString("note")) == null) {
            str = "";
        }
        if (sb2 == null || sb2.equals("")) {
            throw new Exception("Adding contact failed: No name provided.");
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", sb2);
        if (!str.equals("")) {
            contentValues.put("notes", str);
        }
        Uri createPersonInMyContactsGroup = Contacts.People.createPersonInMyContactsGroup(activity.getContentResolver(), contentValues);
        contentValues.clear();
        if (createPersonInMyContactsGroup == null) {
            throw new Exception("Error adding contact.");
        }
        String str2 = null;
        String str3 = null;
        if (jSONObject.has("organization") && (str2 = jSONObject.getString("organization")) == null) {
            str2 = "";
        }
        if (jSONObject.has("jobTitle") && (str3 = jSONObject.getString("jobTitle")) == null) {
            str3 = "";
        }
        if (jSONObject.has("department") && jSONObject.getString("department") == null) {
        }
        if (!str2.equals("")) {
            Uri withAppendedPath = Uri.withAppendedPath(createPersonInMyContactsGroup, "organizations");
            contentValues.put("company", str2);
            if (!str3.equals("")) {
                contentValues.put("title", str3);
            }
            contentValues.put(UmengConstants.AtomKey_Type, (Integer) 1);
            if (activity.getContentResolver().insert(withAppendedPath, contentValues) == null) {
                throw new Exception("Failed to insert organisation");
            }
            contentValues.clear();
        }
        String str4 = null;
        if (jSONObject.has("email") && (str4 = jSONObject.getString("email")) == null) {
            str4 = "";
        }
        if (!str4.equals("")) {
            Uri withAppendedPath2 = Uri.withAppendedPath(createPersonInMyContactsGroup, "contact_methods");
            contentValues.put("kind", (Integer) 1);
            contentValues.put(UmengConstants.AtomKey_Type, (Integer) 1);
            contentValues.put("data", str4);
            if (activity.getContentResolver().insert(withAppendedPath2, contentValues) == null) {
                throw new Exception("Failed to insert email");
            }
            contentValues.clear();
        }
        String str5 = null;
        if (jSONObject.has("im") && (str5 = jSONObject.getString("im")) == null) {
            str5 = "";
        }
        if (!str5.equals("")) {
            Uri withAppendedPath3 = Uri.withAppendedPath(createPersonInMyContactsGroup, "contact_methods");
            contentValues.put("kind", (Integer) 3);
            contentValues.put("data", str5);
            contentValues.put("aux_data", Contacts.ContactMethods.encodePredefinedImProtocol(5));
            contentValues.put(UmengConstants.AtomKey_Type, (Integer) 1);
            contentValues.put("data", str5);
            if (activity.getContentResolver().insert(withAppendedPath3, contentValues) == null) {
                throw new Exception("Failed to insert IM");
            }
            contentValues.clear();
        }
        JSONArray jSONArray = jSONObject.getJSONArray("addresses");
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            int i2 = 1;
            if (jSONObject2.has("label") && jSONObject2.getString("label").equalsIgnoreCase("work")) {
                i2 = 2;
            }
            StringBuilder sb3 = new StringBuilder();
            if (jSONObject2.has("street") && (string6 = jSONObject2.getString("street")) != null) {
                sb3.append(string6);
            }
            if (jSONObject2.has("poBox") && (string5 = jSONObject2.getString("poBox")) != null && !string5.equals("")) {
                if (sb3.length() != 0) {
                    sb3.append(", ");
                }
                sb3.append(string5);
            }
            if (jSONObject2.has("city") && (string4 = jSONObject2.getString("city")) != null && !string4.equals("")) {
                if (sb3.length() != 0) {
                    sb3.append(", ");
                }
                sb3.append(string4);
            }
            if (jSONObject2.has(UmengConstants.AtomKey_State) && (string3 = jSONObject2.getString(UmengConstants.AtomKey_State)) != null && !string3.equals("")) {
                if (sb3.length() != 0) {
                    sb3.append(", ");
                }
                sb3.append(string3);
            }
            if (jSONObject2.has("postalCode") && (string2 = jSONObject2.getString("postalCode")) != null && !string2.equals("")) {
                if (sb3.length() != 0) {
                    sb3.append(", ");
                }
                sb3.append(string2);
            }
            if (jSONObject2.has("country") && (string = jSONObject2.getString("country")) != null && !string.equals("")) {
                if (sb3.length() != 0) {
                    sb3.append(", ");
                }
                sb3.append(string);
            }
            if (sb3.length() > 0) {
                Uri withAppendedPath4 = Uri.withAppendedPath(createPersonInMyContactsGroup, "contact_methods");
                contentValues.put("kind", (Integer) 2);
                contentValues.put(UmengConstants.AtomKey_Type, Integer.valueOf(i2));
                contentValues.put("data", sb3.toString());
                if (activity.getContentResolver().insert(withAppendedPath4, contentValues) == null) {
                    throw new Exception("Failed to insert address");
                }
                contentValues.clear();
            }
        }
        JSONArray jSONArray2 = jSONObject.getJSONArray("phoneNumbers");
        for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
            JSONObject jSONObject3 = jSONArray2.getJSONObject(i3);
            int i4 = 1;
            String str6 = null;
            if (jSONObject3.has("label") && jSONObject3.getString("label").equalsIgnoreCase("work")) {
                i4 = 3;
            }
            if (jSONObject3.has("number") && (str6 = jSONObject3.getString("number")) == null) {
                str6 = "";
            }
            if (!str6.equals("")) {
                Uri withAppendedPath5 = Uri.withAppendedPath(createPersonInMyContactsGroup, "phones");
                contentValues.put("number", str6);
                contentValues.put(UmengConstants.AtomKey_Type, Integer.valueOf(i4));
                if (activity.getContentResolver().insert(withAppendedPath5, contentValues) == null) {
                    throw new Exception("Failed to insert mobile phone number");
                }
                contentValues.clear();
            }
        }
    }
}
