package com.mobclix.android.sdk;

import com.google.ads.AdActivity;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import org.json.JSONObject;

public class MobclixAnalytics {
    static int a = 0;
    static int b = 1;
    static int c = -1;
    /* access modifiers changed from: private */
    public static Mobclix d = Mobclix.getInstance();
    /* access modifiers changed from: private */
    public static String e = "mobclix";
    /* access modifiers changed from: private */
    public static String f = "analytics";
    private static int g = 100;
    /* access modifiers changed from: private */
    public static int h = 5;
    /* access modifiers changed from: private */
    public static File i = null;
    /* access modifiers changed from: private */
    public static boolean j = false;
    /* access modifiers changed from: private */
    public static boolean k = false;
    /* access modifiers changed from: private */
    public static int l = 0;
    /* access modifiers changed from: private */
    public static String m = null;
    /* access modifiers changed from: private */
    public static int n = 0;

    static int a() {
        return n;
    }

    static class LogEvent implements Runnable {
        JSONObject a;

        public LogEvent(JSONObject jSONObject) {
            this.a = jSONObject;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
        public void run() {
            while (true) {
                try {
                    if (!MobclixAnalytics.k && MobclixAnalytics.n == MobclixAnalytics.a) {
                        break;
                    }
                } catch (Exception e) {
                    MobclixAnalytics.k = false;
                    return;
                }
            }
            MobclixAnalytics.k = true;
            if (MobclixAnalytics.j || MobclixAnalytics.m()) {
                do {
                } while (!MobclixAnalytics.j);
                MobclixAnalytics.d.D();
                try {
                    FileOutputStream fileOutputStream = new FileOutputStream(MobclixAnalytics.i, true);
                    if (MobclixAnalytics.l > 1) {
                        fileOutputStream.write(",".getBytes());
                    }
                    fileOutputStream.write(this.a.toString().getBytes());
                    fileOutputStream.close();
                    MobclixAnalytics.l = MobclixAnalytics.l + 1;
                    if (MobclixAnalytics.l > MobclixAnalytics.h) {
                        MobclixAnalytics.j = false;
                        boolean unused = MobclixAnalytics.m();
                    }
                } catch (Exception e2) {
                }
                MobclixAnalytics.k = false;
                return;
            }
            MobclixAnalytics.k = false;
        }
    }

    /* access modifiers changed from: private */
    public static boolean m() {
        l = 1;
        d.D();
        try {
            JSONObject jSONObject = new JSONObject(d.c, new String[]{"ll", "g", "id"});
            jSONObject.put("a", URLEncoder.encode(d.b(), "UTF-8"));
            jSONObject.put("p", URLEncoder.encode(d.d(), "UTF-8"));
            jSONObject.put(AdActivity.TYPE_PARAM, URLEncoder.encode(d.u()));
            jSONObject.put("v", URLEncoder.encode(d.h(), "UTF-8"));
            jSONObject.put("d", URLEncoder.encode(d.i(), "UTF-8"));
            jSONObject.put("dm", URLEncoder.encode(d.k(), "UTF-8"));
            jSONObject.put("dv", URLEncoder.encode(d.g(), "UTF-8"));
            jSONObject.put("hwdm", URLEncoder.encode(d.l(), "UTF-8"));
            jSONObject.put(AdActivity.TYPE_PARAM, URLEncoder.encode(Mobclix.MC_LIBRARY_VERSION, "UTF-8"));
            jSONObject.put("lg", URLEncoder.encode(d.r(), "UTF-8"));
            jSONObject.put("lo", URLEncoder.encode(d.q(), "UTF-8"));
            File file = new File(String.valueOf(d.a().getDir(e, 0).getAbsolutePath()) + "/" + f);
            file.mkdir();
            try {
                if (file.listFiles().length >= g) {
                    return false;
                }
            } catch (Exception e2) {
            }
            i = new File(file.getAbsoluteFile() + "/" + System.currentTimeMillis() + ".log");
            i.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(i);
            fileOutputStream.write("[{\"hb\":".getBytes());
            fileOutputStream.write(jSONObject.toString().getBytes());
            fileOutputStream.write(",\"ev\":[".getBytes());
            fileOutputStream.close();
            j = true;
            return true;
        } catch (Exception e3) {
            return false;
        }
    }

    static class Sync implements Runnable {
        Sync() {
        }

        public synchronized void run() {
            int i = 0;
            synchronized (this) {
                do {
                } while (MobclixAnalytics.k);
                MobclixAnalytics.n = MobclixAnalytics.b;
                try {
                    File file = new File(String.valueOf(MobclixAnalytics.d.a().getDir(MobclixAnalytics.e, 0).getAbsolutePath()) + "/" + MobclixAnalytics.f);
                    file.mkdir();
                    File[] listFiles = file.listFiles();
                    while (true) {
                        int i2 = i;
                        if (i2 >= listFiles.length) {
                            break;
                        }
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append("p=android");
                        stringBuffer.append("&a=").append(URLEncoder.encode(MobclixAnalytics.d.b(), "UTF-8"));
                        stringBuffer.append("&m=").append(URLEncoder.encode(MobclixAnalytics.d.u()));
                        stringBuffer.append("&d=").append(URLEncoder.encode(MobclixAnalytics.d.i(), "UTF-8"));
                        stringBuffer.append("&v=").append(URLEncoder.encode(MobclixAnalytics.d.h(), "UTF-8"));
                        stringBuffer.append("&j=");
                        FileInputStream fileInputStream = new FileInputStream(listFiles[i2]);
                        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
                        DataInputStream dataInputStream = new DataInputStream(bufferedInputStream);
                        while (dataInputStream.available() != 0) {
                            stringBuffer.append(dataInputStream.readLine());
                        }
                        dataInputStream.close();
                        bufferedInputStream.close();
                        fileInputStream.close();
                        stringBuffer.append("]}]");
                        MobclixAnalytics.m = stringBuffer.toString();
                        try {
                            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(MobclixAnalytics.d.h).openConnection();
                            httpURLConnection.setDoOutput(true);
                            httpURLConnection.setDoInput(true);
                            httpURLConnection.setUseCaches(false);
                            httpURLConnection.setRequestMethod("POST");
                            PrintWriter printWriter = new PrintWriter(httpURLConnection.getOutputStream());
                            printWriter.print(MobclixAnalytics.m);
                            printWriter.flush();
                            printWriter.close();
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                            String str = null;
                            while (true) {
                                String readLine = bufferedReader.readLine();
                                if (readLine == null) {
                                    break;
                                } else if (str == null) {
                                    str = readLine;
                                }
                            }
                            if (!str.equals("1")) {
                                MobclixAnalytics.n = MobclixAnalytics.c;
                            }
                            bufferedReader.close();
                        } catch (Exception e) {
                            MobclixAnalytics.n = MobclixAnalytics.c;
                        }
                        if (MobclixAnalytics.n == MobclixAnalytics.c) {
                            MobclixAnalytics.n = MobclixAnalytics.a;
                            break;
                        } else {
                            listFiles[i2].delete();
                            i = i2 + 1;
                        }
                    }
                } catch (Exception e2) {
                }
            }
            MobclixAnalytics.m = (String) null;
            MobclixAnalytics.l = 0;
            MobclixAnalytics.j = false;
            MobclixAnalytics.n = MobclixAnalytics.a;
        }
    }
}
