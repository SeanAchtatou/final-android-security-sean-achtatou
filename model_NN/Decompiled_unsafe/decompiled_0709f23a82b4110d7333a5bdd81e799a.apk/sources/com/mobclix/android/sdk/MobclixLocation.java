package com.mobclix.android.sdk;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

class MobclixLocation {
    Timer a;
    LocationManager b;
    LocationResult c;
    boolean d = false;
    boolean e = false;
    LocationListener f = new LocationListener() {
        public void onLocationChanged(Location location) {
            try {
                if (MobclixLocation.this.a != null) {
                    MobclixLocation.this.a.cancel();
                    MobclixLocation.this.a.purge();
                    MobclixLocation.this.a = null;
                }
                MobclixLocation.this.c.a(location);
            } catch (Exception e) {
            }
            try {
                MobclixLocation.this.b.removeUpdates(this);
                MobclixLocation.this.b.removeUpdates(MobclixLocation.this.g);
            } catch (Exception e2) {
            }
        }

        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }
    };
    LocationListener g = new LocationListener() {
        public void onLocationChanged(Location location) {
            try {
                if (MobclixLocation.this.a != null) {
                    MobclixLocation.this.a.cancel();
                    MobclixLocation.this.a.purge();
                    MobclixLocation.this.a = null;
                }
                MobclixLocation.this.c.a(location);
            } catch (Exception e) {
            }
            try {
                MobclixLocation.this.b.removeUpdates(this);
                MobclixLocation.this.b.removeUpdates(MobclixLocation.this.f);
            } catch (Exception e2) {
            }
        }

        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }
    };

    public static abstract class LocationResult {
        public abstract void a(Location location);
    }

    MobclixLocation() {
    }

    public synchronized boolean a(Context context, LocationResult locationResult) {
        boolean z;
        if (this.a != null) {
            z = true;
        } else {
            try {
                this.c = locationResult;
                if (this.b == null) {
                    this.b = (LocationManager) context.getSystemService("location");
                }
                if (this.b == null) {
                    z = false;
                } else {
                    if (a("gps")) {
                        this.d = this.b.isProviderEnabled("gps");
                    } else {
                        this.d = false;
                    }
                    if (a("network")) {
                        this.e = this.b.isProviderEnabled("network");
                    } else {
                        this.e = false;
                    }
                    if (this.d || this.e) {
                        new Thread(new GetLastLocation()).run();
                        z = true;
                    } else {
                        z = false;
                    }
                }
            } catch (Exception e2) {
            }
        }
        return z;
    }

    public void a() {
        try {
            if (this.a != null) {
                this.a.cancel();
                this.a.purge();
                this.a = null;
            }
            if (this.b != null) {
                if (this.f != null) {
                    this.b.removeUpdates(this.f);
                }
                if (this.g != null) {
                    this.b.removeUpdates(this.g);
                }
            }
        } catch (Exception e2) {
        }
    }

    class GetLastLocation extends TimerTask {
        GetLastLocation() {
        }

        public void run() {
            Location location;
            Location location2 = null;
            try {
                if (MobclixLocation.this.d) {
                    location = MobclixLocation.this.b.getLastKnownLocation("gps");
                } else {
                    location = null;
                }
                if (MobclixLocation.this.e) {
                    location2 = MobclixLocation.this.b.getLastKnownLocation("network");
                }
                if (location == null || location2 == null) {
                    if (location != null) {
                        MobclixLocation.this.c.a(location);
                    } else if (location2 != null) {
                        MobclixLocation.this.c.a(location2);
                    } else {
                        MobclixLocation.this.c.a(null);
                    }
                } else if (location.getTime() > location2.getTime()) {
                    MobclixLocation.this.c.a(location);
                } else {
                    MobclixLocation.this.c.a(location2);
                }
            } catch (Exception e) {
            }
        }
    }

    public boolean a(String str) {
        try {
            List<String> allProviders = this.b.getAllProviders();
            int i = 0;
            while (i < allProviders.size()) {
                try {
                    if (str.equals(allProviders.get(i))) {
                        return true;
                    }
                    i++;
                } catch (Exception e2) {
                    return false;
                }
            }
            return false;
        } catch (Throwable th) {
            return false;
        }
    }
}
