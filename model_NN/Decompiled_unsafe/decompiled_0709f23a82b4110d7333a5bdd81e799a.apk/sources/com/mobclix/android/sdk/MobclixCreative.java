package com.mobclix.android.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Camera;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.google.ads.AdActivity;
import com.mobclick.android.UmengConstants;
import com.mobclix.android.sdk.Mobclix;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;
import java.util.Timer;
import java.util.TimerTask;
import org.codehaus.jackson.util.MinimalPrettyPrinter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class MobclixCreative extends ViewFlipper {
    /* access modifiers changed from: private */
    public static boolean r = false;
    MobclixAdView a;
    Action b;
    boolean c = false;
    final ResourceResponseHandler d = new ResourceResponseHandler();
    final PageCycleHandler e = new PageCycleHandler();
    private MobclixInstrumentation f = MobclixInstrumentation.a();
    private String g = "";
    private String h = "";
    private ArrayList<String> i = new ArrayList<>();
    private ArrayList<String> j = new ArrayList<>();
    /* access modifiers changed from: private */
    public Stack<Thread> k = new Stack<>();
    private boolean l = false;
    /* access modifiers changed from: private */
    public int m = 1;
    /* access modifiers changed from: private */
    public int n = 0;
    private String o = "none";
    /* access modifiers changed from: private */
    public boolean p = true;
    /* access modifiers changed from: private */
    public boolean q = false;
    private int s = 3000;
    /* access modifiers changed from: private */
    public Timer t = null;
    /* access modifiers changed from: private */
    public Thread u;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    MobclixCreative(MobclixAdView mobclixAdView, JSONObject jSONObject, boolean z) {
        super(mobclixAdView.getContext());
        int i2 = 0;
        this.a = mobclixAdView;
        String b2 = this.f.b(this.f.a(this.a.f, MobclixInstrumentation.b), "handle_response");
        requestDisallowInterceptTouchEvent(true);
        if (jSONObject == null) {
            addView(new CustomAdPage(this));
            this.m = 1;
            this.h = "customAd";
            this.l = true;
            return;
        }
        String b3 = this.f.b(b2, "b_build_models");
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("eventUrls");
            try {
                JSONArray jSONArray = jSONObject2.getJSONArray("onShow");
                for (int i3 = 0; i3 < jSONArray.length(); i3++) {
                    this.i.add(jSONArray.getString(i3));
                }
            } catch (Exception e2) {
            }
            JSONArray jSONArray2 = jSONObject2.getJSONArray("onTouch");
            for (int i4 = 0; i4 < jSONArray2.length(); i4++) {
                this.j.add(jSONArray2.getString(i4));
            }
        } catch (Exception e3) {
        }
        try {
            b2 = this.f.b(this.f.b(this.f.e(b3), "c_build_creative"), "a_determine_type");
            JSONObject jSONObject3 = jSONObject.getJSONObject("props");
            try {
                this.g = jSONObject.getString("creativeId");
            } catch (JSONException e4) {
            }
            this.h = jSONObject.getString(UmengConstants.AtomKey_Type);
            String e5 = this.f.e(b2);
            try {
                this.q = z;
                this.b = new Action(this, jSONObject.getJSONObject("action"), this);
            } catch (Exception e6) {
                this.b = new Action(this);
            }
            b2 = this.f.b(e5, "b_get_view");
            if (this.h.equals(AdActivity.HTML_PARAM)) {
                HTMLPage a2 = this.a.A.a(this);
                a2.a(jSONObject3.getString(AdActivity.HTML_PARAM));
                addView(a2);
                this.m = 1;
                this.l = true;
            } else if (this.h.equals("openallocation")) {
                addView(new OpenAllocationPage(jSONObject3, this));
                this.m = 1;
                this.l = true;
            } else {
                try {
                    this.o = jSONObject3.getString("transitionType");
                } catch (JSONException e7) {
                }
                a(this, this.o);
                try {
                    this.s = (int) (jSONObject3.getDouble("transitionTime") * 1000.0d);
                } catch (JSONException e8) {
                }
                if (this.s == 0) {
                    this.s = 3000;
                }
                try {
                    this.p = jSONObject3.getBoolean("loop");
                } catch (JSONException e9) {
                }
                if (this.h.equals("image")) {
                    JSONArray jSONArray3 = jSONObject3.getJSONArray("images");
                    this.m = jSONArray3.length();
                    while (i2 < jSONArray3.length()) {
                        addView(new ImagePage(jSONArray3.getString(i2), this));
                        i2++;
                    }
                } else if (this.h.equals("text")) {
                    JSONArray jSONArray4 = jSONObject3.getJSONArray("texts");
                    this.m = jSONArray4.length();
                    while (i2 < jSONArray4.length()) {
                        addView(new TextPage(jSONArray4.getJSONObject(i2), this));
                        i2++;
                    }
                }
                String b4 = this.f.b(this.f.e(b2), "f_load_ad_creative");
                e();
                String e10 = this.f.e(b4);
                this.l = true;
            }
        } catch (JSONException e11) {
            this.f.e(this.f.e(this.f.e(b2)));
            this.f.d(this.a.f);
        }
    }

    public boolean a() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            if (Integer.parseInt(Build.VERSION.SDK) >= 7) {
                try {
                    super.onDetachedFromWindow();
                    super.stopFlipping();
                } catch (IllegalArgumentException e2) {
                    Log.w("MobclixCreative", "Android project  issue 6191  workaround.");
                    super.stopFlipping();
                } catch (Throwable th) {
                    super.stopFlipping();
                    throw th;
                }
            } else {
                super.onDetachedFromWindow();
            }
        } catch (Exception e3) {
            super.onDetachedFromWindow();
        }
    }

    public void b() {
        synchronized (this) {
            if (this.t != null) {
                this.t.cancel();
                this.t.purge();
            }
        }
        try {
            if (getCurrentView().getClass() == HTMLPage.class) {
                MobclixJavascriptInterface c2 = ((HTMLPage) getCurrentView()).b.c();
                c2.c();
                if (!c2.c) {
                    c2.k();
                }
            }
        } catch (Exception e2) {
        }
    }

    public void c() {
        synchronized (this) {
            if (this.t != null) {
                this.t.cancel();
                this.t.purge();
                this.t = new Timer();
                this.t.scheduleAtFixedRate(new PageCycleThread(), (long) this.s, (long) this.s);
            }
        }
        try {
            if (getCurrentView().getClass() == HTMLPage.class) {
                MobclixJavascriptInterface c2 = ((HTMLPage) getCurrentView()).b.c();
                c2.d();
                if (!c2.c && c2.b == null) {
                    c2.l();
                }
                c2.c = false;
            }
        } catch (Exception e2) {
        }
    }

    public void d() {
        b();
        try {
            if (getCurrentView().getClass() == HTMLPage.class) {
                ((HTMLPage) getCurrentView()).b.c().j();
            }
        } catch (Exception e2) {
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        try {
            if (this.h.equals(AdActivity.HTML_PARAM) || motionEvent.getAction() != 0) {
                return false;
            }
            g();
            return this.b.c();
        } catch (Exception e2) {
            return false;
        }
    }

    public void e() {
        if (!this.k.isEmpty()) {
            this.k.pop().start();
            return;
        }
        this.a.D = 0;
        this.a.E.a = true;
        this.a.E.bringToFront();
        String b2 = this.f.b(this.f.a(this.a.f, MobclixInstrumentation.b), "handle_response");
        this.n = 0;
        String b3 = this.f.b(this.f.b(this.f.b(b2, "c_build_creative"), "b_get_view"), "c_deque_view");
        while (this.a.getChildCount() > 1) {
            if (this.a.getChildAt(0) == this.a.z) {
                try {
                    MobclixCreative mobclixCreative = (MobclixCreative) this.a.getChildAt(1);
                    for (int i2 = 0; i2 < mobclixCreative.getChildCount(); i2++) {
                        try {
                            ((Page) mobclixCreative.getChildAt(i2)).b();
                        } catch (Exception e2) {
                        }
                    }
                } catch (Exception e3) {
                }
                this.a.removeViewAt(1);
            } else {
                try {
                    MobclixCreative mobclixCreative2 = (MobclixCreative) this.a.getChildAt(0);
                    for (int i3 = 0; i3 < mobclixCreative2.getChildCount(); i3++) {
                        try {
                            ((Page) mobclixCreative2.getChildAt(i3)).b();
                        } catch (Exception e4) {
                        }
                    }
                } catch (Exception e5) {
                }
                this.a.removeViewAt(0);
            }
        }
        String b4 = this.f.b(this.f.e(this.f.e(b3)), "e_add_view");
        this.a.addView(this);
        String b5 = this.f.b(this.f.e(this.f.e(b4)), "d_bring_onscreen");
        if (this.a.z != null) {
            this.a.z.d();
            if (this.a.g) {
                a(this.a, "flipRight");
            }
        }
        this.a.showNext();
        String e6 = this.f.e(b5);
        if (this.m > 1) {
            b();
            this.t = new Timer();
            this.t.scheduleAtFixedRate(new PageCycleThread(), (long) this.s, (long) this.s);
        }
        String b6 = this.f.b(e6, "e_trigger_events");
        if (this.a.getVisibility() == 0 && !this.h.equals(AdActivity.HTML_PARAM)) {
            f();
        }
        String b7 = this.f.b(this.f.e(b6), "f_notify_delegates");
        Iterator<MobclixAdViewListener> it = this.a.u.iterator();
        while (it.hasNext()) {
            MobclixAdViewListener next = it.next();
            if (next != null) {
                next.b(this.a);
            }
        }
        this.a.m = 0;
        String b8 = this.f.b(this.f.e(b7), "h_handle_autoplay");
        if (this.b != null && this.b.a() && this.a.a() && !this.q && !r && Mobclix.getInstance().f(this.a.p)) {
            this.q = true;
            this.b.c();
            MobclixAdView.n.put(this.a.p, Long.valueOf(System.currentTimeMillis()));
        }
        this.f.e(this.f.e(b8));
        this.f.d(this.a.f);
    }

    /* access modifiers changed from: package-private */
    public void f() {
        try {
            Iterator<String> it = this.i.iterator();
            while (it.hasNext()) {
                new Thread(new Mobclix.FetchImageThread(it.next(), new Mobclix.BitmapHandler())).start();
            }
            this.c = true;
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        try {
            Iterator<String> it = this.j.iterator();
            while (it.hasNext()) {
                new Thread(new Mobclix.FetchImageThread(it.next(), new Mobclix.BitmapHandler())).start();
            }
        } catch (Exception e2) {
        }
    }

    public void a(ViewFlipper viewFlipper, String str) {
        Animation rotate3dAnimation;
        Animation rotate3dAnimation2;
        if (str != null) {
            if (str.equals("fade")) {
                rotate3dAnimation = new AlphaAnimation(1.0f, 0.0f);
                rotate3dAnimation2 = new AlphaAnimation(0.0f, 1.0f);
            } else if (str.equals("slideRight")) {
                rotate3dAnimation = new TranslateAnimation(2, 0.0f, 2, 1.0f, 2, 0.0f, 2, 0.0f);
                rotate3dAnimation2 = new TranslateAnimation(2, -1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
            } else if (str.equals("slideLeft")) {
                rotate3dAnimation = new TranslateAnimation(2, 0.0f, 2, -1.0f, 2, 0.0f, 2, 0.0f);
                rotate3dAnimation2 = new TranslateAnimation(2, 1.0f, 2, 0.0f, 2, 0.0f, 2, 0.0f);
            } else if (str.equals("slideUp")) {
                rotate3dAnimation = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 0.0f, 2, -1.0f);
                rotate3dAnimation2 = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 1.0f, 2, 0.0f);
            } else if (str.equals("slideDown")) {
                rotate3dAnimation = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 0.0f, 2, 1.0f);
                rotate3dAnimation2 = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, -1.0f, 2, 0.0f);
            } else if (str.equals("flipRight")) {
                rotate3dAnimation = new Rotate3dAnimation(0.0f, 90.0f, ((float) this.a.getWidth()) / 2.0f, ((float) this.a.getHeight()) / 2.0f, 0.0f, true);
                rotate3dAnimation2 = new Rotate3dAnimation(-90.0f, 0.0f, ((float) this.a.getWidth()) / 2.0f, ((float) this.a.getHeight()) / 2.0f, 0.0f, false);
                rotate3dAnimation2.setStartOffset(300);
            } else if (str.equals("flipLeft")) {
                rotate3dAnimation = new Rotate3dAnimation(0.0f, -90.0f, ((float) this.a.getWidth()) / 2.0f, ((float) this.a.getHeight()) / 2.0f, 0.0f, true);
                rotate3dAnimation2 = new Rotate3dAnimation(90.0f, 0.0f, ((float) this.a.getWidth()) / 2.0f, ((float) this.a.getHeight()) / 2.0f, 0.0f, false);
                rotate3dAnimation2.setStartOffset(300);
            } else {
                return;
            }
            rotate3dAnimation.setDuration(300);
            rotate3dAnimation2.setDuration(300);
            viewFlipper.setOutAnimation(rotate3dAnimation);
            viewFlipper.setInAnimation(rotate3dAnimation2);
        }
    }

    class ResourceResponseHandler extends Handler {
        ResourceResponseHandler() {
        }

        public void handleMessage(Message message) {
            try {
                MobclixCreative.this.e();
            } catch (Exception e) {
            }
        }
    }

    class PageCycleHandler extends Handler {
        PageCycleHandler() {
        }

        public void handleMessage(Message message) {
            int a2 = MobclixCreative.this.n + 1;
            if (a2 >= MobclixCreative.this.m) {
                if (!MobclixCreative.this.p) {
                    MobclixCreative.this.t.cancel();
                    return;
                }
                a2 = 0;
            }
            MobclixCreative.this.n = a2;
            MobclixCreative.this.showNext();
        }
    }

    class PageCycleThread extends TimerTask {
        PageCycleThread() {
        }

        public void run() {
            MobclixCreative.this.e.sendEmptyMessage(0);
        }
    }

    private class CustomAdThread implements Runnable {
        private String b;

        CustomAdThread(String str) {
            this.b = str;
        }

        public void run() {
            HttpURLConnection httpURLConnection;
            HttpURLConnection httpURLConnection2 = null;
            try {
                httpURLConnection = (HttpURLConnection) new URL(this.b).openConnection();
                try {
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.setRequestProperty("User-Agent", Mobclix.getInstance().A());
                    httpURLConnection.connect();
                    httpURLConnection.disconnect();
                } catch (Exception e) {
                    httpURLConnection.disconnect();
                } catch (Throwable th) {
                    Throwable th2 = th;
                    httpURLConnection2 = httpURLConnection;
                    th = th2;
                    httpURLConnection2.disconnect();
                    throw th;
                }
            } catch (Exception e2) {
                httpURLConnection = null;
                httpURLConnection.disconnect();
            } catch (Throwable th3) {
                th = th3;
                httpURLConnection2.disconnect();
                throw th;
            }
        }
    }

    static class Page extends RelativeLayout {
        protected HashMap<String, Integer> e = new HashMap<>();
        protected MobclixCreative f = null;

        Page(Context context) {
            super(context);
            this.e.put("center", 17);
            this.e.put("left", 19);
            this.e.put("right", 21);
        }

        Page(MobclixCreative mobclixCreative) {
            super(mobclixCreative.getContext());
            this.e.put("center", 17);
            this.e.put("left", 19);
            this.e.put("right", 21);
            this.f = mobclixCreative;
            try {
                ViewParent parent = this.f.a.E.getParent();
                if (parent != null) {
                    ((ViewGroup) parent).removeView(this.f.a.E);
                }
                addView(this.f.a.E);
            } catch (Exception e2) {
            }
            this.f.a.E.a = false;
            this.f.a.E.b = true;
        }

        public MobclixCreative e() {
            return this.f;
        }

        public int a(JSONObject jSONObject) {
            try {
                return Color.argb(jSONObject.getInt("a"), jSONObject.getInt("r"), jSONObject.getInt("g"), jSONObject.getInt("b"));
            } catch (JSONException e2) {
                return 0;
            }
        }

        /* access modifiers changed from: package-private */
        public int a(int i) {
            return (int) (this.f.a.t * ((float) i));
        }

        /* access modifiers changed from: package-private */
        public void b() {
        }
    }

    private static class TextPage extends Page {
        private String a = "null";
        private int b = -1;
        private String c = "null";
        private String d = "null";
        private String g = "center";
        private String h = "";
        private int i = -16776961;
        private String j = "center";
        private String k = "";
        private int l = -16776961;
        /* access modifiers changed from: private */
        public ImageView m;
        /* access modifiers changed from: private */
        public ImageView n;
        /* access modifiers changed from: private */
        public BitmapDrawable o = null;
        private TextView p;
        private TextView q;

        TextPage(JSONObject jSONObject, MobclixCreative mobclixCreative) {
            super(mobclixCreative);
            try {
                this.b = a(jSONObject.getJSONObject("bgColor"));
            } catch (JSONException e) {
            }
            try {
                this.a = jSONObject.getString("bgImg");
            } catch (JSONException e2) {
            }
            try {
                this.c = jSONObject.getString("leftIcon");
            } catch (JSONException e3) {
            }
            if (this.c.equals("")) {
                this.c = "null";
            }
            try {
                this.d = jSONObject.getString("rightIcon");
            } catch (JSONException e4) {
            }
            if (this.d.equals("")) {
                this.d = "null";
            }
            try {
                JSONObject jSONObject2 = jSONObject.getJSONObject("headerText");
                try {
                    this.g = jSONObject2.getString("alignment");
                } catch (JSONException e5) {
                }
                try {
                    this.h = jSONObject2.getString("text");
                } catch (JSONException e6) {
                }
                if (this.h.equals("null")) {
                    this.h = "";
                }
                this.i = a(jSONObject2.getJSONObject("color"));
            } catch (JSONException e7) {
            }
            try {
                JSONObject jSONObject3 = jSONObject.getJSONObject("bodyText");
                try {
                    this.j = jSONObject3.getString("alignment");
                } catch (JSONException e8) {
                }
                try {
                    this.k = jSONObject3.getString("text");
                } catch (JSONException e9) {
                }
                if (this.k.equals("null")) {
                    this.k = "";
                }
                this.l = a(jSONObject3.getJSONObject("color"));
            } catch (JSONException e10) {
            }
            a();
            c();
            if (!this.a.equals("null")) {
                d();
            }
        }

        public void a() {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(a(48), a(48));
            layoutParams2.addRule(15);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(a(48), a(48));
            layoutParams3.addRule(15);
            if (this.c.equals("null") && this.d.equals("null")) {
                layoutParams.setMargins(a(5), 0, a(5), 0);
            } else if (!this.c.equals("null") && this.d.equals("null")) {
                layoutParams.setMargins(a(60), 0, a(5), 0);
                layoutParams2.addRule(9);
                layoutParams2.setMargins(a(5), 0, 0, 0);
            } else if (!this.c.equals("null") || this.d.equals("null")) {
                layoutParams.setMargins(a(60), 0, a(60), 0);
                layoutParams2.addRule(9);
                layoutParams2.setMargins(a(5), 0, 0, 0);
                layoutParams3.addRule(11);
                layoutParams3.setMargins(0, 0, a(5), 0);
            } else {
                layoutParams.setMargins(a(5), 0, a(60), 0);
                layoutParams3.addRule(11);
                layoutParams3.setMargins(0, 0, a(5), 0);
            }
            LinearLayout linearLayout = new LinearLayout(this.f.a.getContext());
            linearLayout.setOrientation(1);
            linearLayout.setLayoutParams(layoutParams);
            linearLayout.setGravity(16);
            if (!this.h.equals("")) {
                this.p = new TextView(this.f.a.getContext());
                this.p.setGravity(((Integer) this.e.get(this.g)).intValue());
                this.p.setText(Html.fromHtml("<b>" + this.h + "</b>"));
                this.p.setTextColor(this.i);
                linearLayout.addView(this.p);
            }
            if (!this.k.equals("")) {
                this.q = new TextView(this.f.a.getContext());
                this.q.setGravity(((Integer) this.e.get(this.j)).intValue());
                this.q.setText(this.k);
                this.q.setTextColor(this.l);
                linearLayout.addView(this.q);
            }
            addView(linearLayout);
            if (!this.c.equals("null")) {
                this.m = new ImageView(this.f.a.getContext());
                this.m.setLayoutParams(layoutParams2);
                addView(this.m);
            }
            if (!this.d.equals("null")) {
                this.n = new ImageView(this.f.a.getContext());
                this.n.setLayoutParams(layoutParams3);
                addView(this.n);
            }
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            setBackgroundColor(this.b);
        }

        public void c() {
            if (!this.c.equals("null")) {
                this.f.k.push(new Thread(new Mobclix.FetchImageThread(this.c, new Mobclix.BitmapHandler() {
                    public void handleMessage(Message message) {
                        if (this.a != null) {
                            TextPage.this.m.setImageBitmap(this.a);
                        }
                        TextPage.this.e().d.sendEmptyMessage(0);
                    }
                })));
            }
            if (!this.d.equals("null")) {
                this.f.k.push(new Thread(new Mobclix.FetchImageThread(this.d, new Mobclix.BitmapHandler() {
                    public void handleMessage(Message message) {
                        if (this.a != null) {
                            TextPage.this.n.setImageBitmap(this.a);
                        }
                        TextPage.this.e().d.sendEmptyMessage(0);
                    }
                })));
            }
        }

        public void d() {
            this.f.k.push(new Thread(new Mobclix.FetchImageThread(this.a, new Mobclix.BitmapHandler() {
                public void handleMessage(Message message) {
                    if (this.a != null) {
                        TextPage.this.o = new BitmapDrawable(this.a);
                        TextPage.this.setBackgroundDrawable(TextPage.this.o);
                    }
                    TextPage.this.e().d.sendEmptyMessage(0);
                }
            })));
        }

        /* access modifiers changed from: package-private */
        public void b() {
            try {
                ((BitmapDrawable) this.m.getDrawable()).getBitmap().recycle();
            } catch (Exception e) {
            }
            try {
                ((BitmapDrawable) this.n.getDrawable()).getBitmap().recycle();
            } catch (Exception e2) {
            }
            try {
                this.o.getBitmap().recycle();
            } catch (Exception e3) {
            }
        }
    }

    private static class ImagePage extends Page {
        private String a;
        /* access modifiers changed from: private */
        public ImageView b;

        ImagePage(String str, MobclixCreative mobclixCreative) {
            super(mobclixCreative);
            this.a = str;
            a();
            c();
        }

        public void a() {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            layoutParams.addRule(15);
            this.b = new ImageView(this.f.a.getContext());
            this.b.setLayoutParams(layoutParams);
            addView(this.b);
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        }

        public void c() {
            this.f.k.push(new Thread(new Mobclix.FetchImageThread(this.a, new Mobclix.BitmapHandler() {
                public void handleMessage(Message message) {
                    if (this.a != null) {
                        ImagePage.this.b.setImageBitmap(this.a);
                    }
                    ImagePage.this.e().d.sendEmptyMessage(0);
                }
            })));
        }

        /* access modifiers changed from: package-private */
        public void b() {
            try {
                ((BitmapDrawable) this.b.getDrawable()).getBitmap().recycle();
            } catch (Exception e) {
            }
        }
    }

    static class HTMLPagePool {
        private ArrayList<HTMLPage> a = new ArrayList<>();

        /* access modifiers changed from: package-private */
        public synchronized HTMLPage a(MobclixFullScreenAdView mobclixFullScreenAdView) {
            HTMLPage hTMLPage;
            if (this.a.size() > 0) {
                hTMLPage = this.a.get(0);
                this.a.remove(0);
            } else {
                hTMLPage = new HTMLPage(mobclixFullScreenAdView);
            }
            return hTMLPage;
        }

        /* access modifiers changed from: package-private */
        public synchronized HTMLPage a(MobclixCreative mobclixCreative) {
            HTMLPage hTMLPage;
            if (this.a.size() > 0) {
                hTMLPage = this.a.get(0);
                hTMLPage.f = mobclixCreative;
                this.a.remove(0);
                try {
                    ViewParent parent = mobclixCreative.a.E.getParent();
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(mobclixCreative.a.E);
                    }
                    hTMLPage.addView(mobclixCreative.a.E);
                } catch (Exception e) {
                }
                mobclixCreative.a.E.a = false;
                mobclixCreative.a.E.b = true;
            } else {
                hTMLPage = new HTMLPage(mobclixCreative);
            }
            return hTMLPage;
        }

        /* access modifiers changed from: package-private */
        public void a(HTMLPage hTMLPage) {
            try {
                if (hTMLPage.getParent() != null) {
                    ((ViewGroup) hTMLPage.getParent()).removeView(hTMLPage);
                }
            } catch (Exception e) {
            }
            if (this.a.size() > 2) {
                try {
                    hTMLPage.b.destroy();
                    hTMLPage.b = null;
                } catch (Exception e2) {
                }
            } else {
                this.a.add(hTMLPage);
            }
        }
    }

    static class HTMLPage extends Page {
        private String a;
        /* access modifiers changed from: private */
        public MobclixWebView b;
        private MobclixJavascriptInterface c;
        /* access modifiers changed from: private */
        public MobclixFullScreenAdView d = null;

        HTMLPage(MobclixCreative mobclixCreative) {
            super(mobclixCreative);
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            requestDisallowInterceptTouchEvent(true);
            try {
                a();
            } catch (Exception e) {
            }
        }

        HTMLPage(MobclixFullScreenAdView mobclixFullScreenAdView) {
            super(mobclixFullScreenAdView.a());
            this.d = mobclixFullScreenAdView;
            requestDisallowInterceptTouchEvent(true);
            try {
                a();
            } catch (Exception e) {
            }
        }

        public void a() {
            RelativeLayout.LayoutParams layoutParams;
            if (this.d == null) {
                this.b = new MobclixWebView(this.f);
                this.c = new MobclixJavascriptInterface(this.b, false);
            } else {
                this.b = new MobclixWebView(this.d);
                this.c = new MobclixJavascriptInterface(this.b, true);
            }
            this.b.requestDisallowInterceptTouchEvent(true);
            if (this.d == null) {
                layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            } else {
                layoutParams = new RelativeLayout.LayoutParams(this.d.j, this.d.k);
            }
            this.b.setLayoutParams(layoutParams);
            this.b.setScrollBarStyle(33554432);
            this.b.addJavascriptInterface(this.c, "MOBCLIX");
            this.b.a(this.c);
            WebSettings settings = this.b.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setAllowFileAccess(true);
            settings.setPluginsEnabled(false);
            Class<?>[] classes = WebSettings.class.getClasses();
            if (classes != null) {
                Class<?> cls = null;
                for (int i = 0; i < classes.length; i++) {
                    if (classes[i].getName().equals("android.webkit.WebSettings$PluginState")) {
                        cls = classes[i];
                    }
                }
                if (cls != null) {
                    try {
                        WebSettings.class.getMethod("setPluginState", cls).invoke(settings, cls.getField("OFF").get(null));
                    } catch (Exception e) {
                    }
                }
            }
            this.b.setWebViewClient(new WebViewClient() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.mobclix.android.sdk.MobclixCreative.a(com.mobclix.android.sdk.MobclixCreative, boolean):void
                 arg types: [com.mobclix.android.sdk.MobclixCreative, int]
                 candidates:
                  com.mobclix.android.sdk.MobclixCreative.a(com.mobclix.android.sdk.MobclixCreative, int):void
                  com.mobclix.android.sdk.MobclixCreative.a(com.mobclix.android.sdk.MobclixCreative, java.lang.Thread):void
                  com.mobclix.android.sdk.MobclixCreative.a(android.widget.ViewFlipper, java.lang.String):void
                  com.mobclix.android.sdk.MobclixCreative.a(com.mobclix.android.sdk.MobclixCreative, boolean):void */
                public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                    try {
                        Uri parse = Uri.parse(str);
                        if (HTMLPage.this.b.c) {
                            if (!HTMLPage.this.b.d) {
                                return true;
                            }
                            if (!HTMLPage.this.b.e && HTMLPage.this.f != null) {
                                if (!HTMLPage.this.f.a.a() || !Mobclix.getInstance().f(HTMLPage.this.f.a.p)) {
                                    return true;
                                }
                                HTMLPage.this.f.q = true;
                                MobclixAdView.n.put(HTMLPage.this.f.a.p, Long.valueOf(System.currentTimeMillis()));
                            }
                        }
                        if (parse.getScheme().equals("tel") || parse.getScheme().equals("mailto")) {
                            if (HTMLPage.this.f != null) {
                                HTMLPage.this.f.b.a = "null";
                                HTMLPage.this.f.b.c();
                            }
                            if (HTMLPage.this.b.c().b != null) {
                                HTMLPage.this.b.c().b.o = true;
                            }
                            HTMLPage.this.getContext().startActivity(new Intent("android.intent.action.VIEW", parse));
                            return true;
                        } else if (parse.getScheme().equals("sms")) {
                            if (HTMLPage.this.f != null) {
                                HTMLPage.this.f.b.a = "null";
                                HTMLPage.this.f.b.c();
                            }
                            if (HTMLPage.this.b.c().b != null) {
                                HTMLPage.this.b.c().b.o = true;
                            }
                            String[] split = str.split(":");
                            String str2 = String.valueOf(split[0]) + "://";
                            for (int i = 1; i < split.length; i++) {
                                str2 = String.valueOf(str2) + split[i];
                            }
                            String queryParameter = Uri.parse(str2).getQueryParameter("body");
                            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str.split("\\?")[0]));
                            intent.putExtra("sms_body", queryParameter);
                            HTMLPage.this.getContext().startActivity(intent);
                            return true;
                        } else {
                            try {
                                if (parse.getHost().equals("market.android.com") || parse.getScheme().equals("market")) {
                                    if (HTMLPage.this.b.c().b != null) {
                                        HTMLPage.this.b.c().b.o = true;
                                    }
                                    HTMLPage.this.getContext().startActivity(new Intent("android.intent.action.VIEW", parse));
                                    if (HTMLPage.this.f == null) {
                                        return true;
                                    }
                                    HTMLPage.this.f.b.a = "null";
                                    HTMLPage.this.f.b.c();
                                    return true;
                                }
                            } catch (Exception e) {
                            }
                            String queryParameter2 = parse.getQueryParameter("shouldOpenInNewWindow");
                            if (queryParameter2 == null) {
                                queryParameter2 = "";
                            } else {
                                queryParameter2.toLowerCase();
                            }
                            if ((HTMLPage.this.b.c().b != null || queryParameter2.equals("no")) && !queryParameter2.equals("yes")) {
                                HTMLPage.this.b.loadUrl(str);
                                return true;
                            }
                            if (HTMLPage.this.b.c().b != null) {
                                HTMLPage.this.b.c().b.o = true;
                            }
                            HTMLPage.this.getContext().startActivity(new Intent("android.intent.action.VIEW", parse));
                            if (HTMLPage.this.f == null) {
                                return true;
                            }
                            HTMLPage.this.f.b.a = "null";
                            HTMLPage.this.f.b.c();
                            return true;
                        }
                    } catch (Exception e2) {
                        return false;
                    }
                }

                public void onPageFinished(WebView webView, String str) {
                    CookieSyncManager.getInstance().sync();
                    if (!HTMLPage.this.b.c && HTMLPage.this.f != null) {
                        HTMLPage.this.e().d.sendEmptyMessage(0);
                    }
                    if (!HTMLPage.this.b.c && HTMLPage.this.d != null) {
                        HTMLPage.this.d.a(HTMLPage.this.b);
                    }
                    HTMLPage.this.b.c = true;
                }

                public void onLoadResource(WebView webView, String str) {
                }
            });
            this.b.setWebChromeClient(new WebChromeClient() {
                public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
                    if (HTMLPage.this.b.d) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(HTMLPage.this.b.b());
                        builder.setMessage(str2).setCancelable(false);
                        builder.setPositiveButton(17039370, new Mobclix.ObjectOnClickListener(jsResult) {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ((JsResult) this.b).confirm();
                            }
                        });
                        builder.create().show();
                    }
                    return true;
                }

                public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
                    if (HTMLPage.this.b.d) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(HTMLPage.this.b.b());
                        builder.setMessage(str2).setCancelable(false);
                        builder.setPositiveButton(17039370, new Mobclix.ObjectOnClickListener(jsResult) {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ((JsResult) this.b).confirm();
                            }
                        });
                        builder.setNegativeButton(17039360, new Mobclix.ObjectOnClickListener(jsResult) {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ((JsResult) this.b).cancel();
                            }
                        });
                        builder.create().show();
                    }
                    return true;
                }

                public boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
                    if (HTMLPage.this.b.d) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(HTMLPage.this.b.b());
                        builder.setMessage(str2).setTitle("Confirm Navigation").setCancelable(false);
                        builder.setPositiveButton("Leave this Page", new Mobclix.ObjectOnClickListener(jsResult) {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ((JsResult) this.b).confirm();
                            }
                        });
                        builder.setNegativeButton("Stay on this Page", new Mobclix.ObjectOnClickListener(jsResult) {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ((JsResult) this.b).cancel();
                            }
                        });
                        builder.create().show();
                    }
                    return true;
                }

                public boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
                    if (HTMLPage.this.b.d) {
                        Context b = HTMLPage.this.b.b();
                        AlertDialog.Builder builder = new AlertDialog.Builder(b);
                        EditText editText = new EditText(b);
                        if (str3 != null) {
                            editText.setText(str3);
                        }
                        builder.setMessage(str2).setView(editText).setCancelable(false).setPositiveButton(17039370, new Mobclix.ObjectOnClickListener(jsPromptResult, editText) {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ((JsPromptResult) this.b).confirm(((EditText) this.c).getText().toString());
                            }
                        }).setNegativeButton(17039360, new Mobclix.ObjectOnClickListener(jsPromptResult) {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ((JsPromptResult) this.b).cancel();
                            }
                        });
                        builder.create().show();
                    }
                    return true;
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.mobclix.android.sdk.MobclixCreative.a(com.mobclix.android.sdk.MobclixCreative, boolean):void
                 arg types: [com.mobclix.android.sdk.MobclixCreative, int]
                 candidates:
                  com.mobclix.android.sdk.MobclixCreative.a(com.mobclix.android.sdk.MobclixCreative, int):void
                  com.mobclix.android.sdk.MobclixCreative.a(com.mobclix.android.sdk.MobclixCreative, java.lang.Thread):void
                  com.mobclix.android.sdk.MobclixCreative.a(android.widget.ViewFlipper, java.lang.String):void
                  com.mobclix.android.sdk.MobclixCreative.a(com.mobclix.android.sdk.MobclixCreative, boolean):void */
                public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
                    try {
                        HTMLPage.this.b.g = customViewCallback;
                        if (HTMLPage.this.b.d) {
                            HTMLPage.this.b.f.a("null", UmengConstants.Atom_State_Error);
                            if (HTMLPage.this.f != null) {
                                if (!HTMLPage.this.b.e && HTMLPage.this.f != null) {
                                    if (HTMLPage.this.f.a.a() && Mobclix.getInstance().f(HTMLPage.this.f.a.p)) {
                                        HTMLPage.this.f.q = true;
                                        MobclixAdView.n.put(HTMLPage.this.f.a.p, Long.valueOf(System.currentTimeMillis()));
                                    } else if (view instanceof FrameLayout) {
                                        HTMLPage.this.b.b(view, customViewCallback);
                                        return;
                                    } else {
                                        return;
                                    }
                                }
                                if (view instanceof FrameLayout) {
                                    HTMLPage.this.b.a(view, customViewCallback);
                                }
                            } else if (view instanceof FrameLayout) {
                                HTMLPage.this.b.a(view, customViewCallback);
                            }
                        } else if (view instanceof FrameLayout) {
                            HTMLPage.this.b.b(view, customViewCallback);
                        }
                    } catch (Exception e) {
                    }
                }
            });
            this.b.setDownloadListener(new DownloadListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.mobclix.android.sdk.MobclixCreative.a(com.mobclix.android.sdk.MobclixCreative, boolean):void
                 arg types: [com.mobclix.android.sdk.MobclixCreative, int]
                 candidates:
                  com.mobclix.android.sdk.MobclixCreative.a(com.mobclix.android.sdk.MobclixCreative, int):void
                  com.mobclix.android.sdk.MobclixCreative.a(com.mobclix.android.sdk.MobclixCreative, java.lang.Thread):void
                  com.mobclix.android.sdk.MobclixCreative.a(android.widget.ViewFlipper, java.lang.String):void
                  com.mobclix.android.sdk.MobclixCreative.a(com.mobclix.android.sdk.MobclixCreative, boolean):void */
                public void onDownloadStart(String str, String str2, String str3, String str4, long j) {
                    try {
                        if (HTMLPage.this.b.d) {
                            if (!HTMLPage.this.b.e && HTMLPage.this.f != null) {
                                if (HTMLPage.this.f.a.a() && Mobclix.getInstance().f(HTMLPage.this.f.a.p)) {
                                    HTMLPage.this.f.q = true;
                                    MobclixAdView.n.put(HTMLPage.this.f.a.p, Long.valueOf(System.currentTimeMillis()));
                                } else {
                                    return;
                                }
                            }
                            if (str4.equals("video/mp4") || str4.equals("video/3gp") || str4.equals("video/m4v") || str4.equals("video/quicktime")) {
                                Intent intent = new Intent("android.intent.action.VIEW");
                                intent.setDataAndType(Uri.parse(str), "video/*");
                                HTMLPage.this.f.getContext().startActivity(intent);
                            }
                        }
                    } catch (Exception e) {
                    }
                }
            });
            this.b.setFocusable(true);
            addView(this.b);
        }

        /* access modifiers changed from: package-private */
        public void a(String str) {
            this.a = str;
            this.b.a(this.a);
            this.b.d();
        }

        public void onDetachedFromWindow() {
            try {
                this.c.c();
            } catch (Exception e) {
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            try {
                this.c.c();
            } catch (Exception e) {
            }
            try {
                this.b.e();
                this.f.a.A.a(this);
            } catch (Exception e2) {
            }
        }
    }

    private static class CustomAdPage extends Page {
        private ImageView a;

        CustomAdPage(MobclixCreative mobclixCreative) {
            super(mobclixCreative);
            a();
        }

        public void a() {
            try {
                Bitmap decodeStream = BitmapFactory.decodeStream(this.f.a.getContext().openFileInput(String.valueOf(this.f.a.p) + "_mc_cached_custom_ad.png"));
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                layoutParams.addRule(15);
                this.a = new ImageView(this.f.a.getContext());
                this.a.setLayoutParams(layoutParams);
                this.a.setImageBitmap(decodeStream);
                addView(this.a);
                setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            } catch (Exception e) {
            }
            e().d.sendEmptyMessage(0);
        }

        /* access modifiers changed from: package-private */
        public void b() {
            try {
                ((BitmapDrawable) this.a.getDrawable()).getBitmap().recycle();
            } catch (Exception e) {
            }
        }
    }

    static class OpenAllocationPage extends Page {
        ViewGroup a;
        String b = "openadmob";
        String c = null;
        int d = 0;

        OpenAllocationPage(JSONObject jSONObject, MobclixCreative mobclixCreative) {
            super(mobclixCreative);
            int i;
            try {
                this.b = jSONObject.getString("network");
            } catch (Exception e) {
            }
            try {
                StringBuffer stringBuffer = new StringBuffer();
                JSONObject jSONObject2 = jSONObject.getJSONObject("params");
                Iterator<String> keys = jSONObject2.keys();
                while (keys.hasNext()) {
                    String obj = keys.next().toString();
                    String obj2 = jSONObject2.get(obj).toString();
                    stringBuffer.append("&").append(obj);
                    stringBuffer.append("=").append(obj2);
                }
                this.c = stringBuffer.toString();
            } catch (Exception e2) {
            }
            if (this.b.equals("openadmob")) {
                i = -750;
            } else if (this.b.equals("opengoogle")) {
                i = -10100;
            } else if (this.b.equals("openmillennial")) {
                i = -111111;
            } else {
                i = -1006;
            }
            Iterator<MobclixAdViewListener> it = this.f.a.u.iterator();
            boolean z = false;
            while (it.hasNext()) {
                MobclixAdViewListener next = it.next();
                if (next != null) {
                    z = z || next.a(this.f.a, i);
                }
            }
            if (z) {
                this.f.a.m = 0;
            } else if (i == -111111) {
                try {
                    a();
                } catch (Throwable th) {
                    this.f.a.a(this.c);
                }
            } else if (i == -750 || i == -10100) {
                try {
                    d();
                } catch (Throwable th2) {
                    this.f.a.a(this.c);
                }
            } else {
                this.f.a.m = 0;
            }
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* access modifiers changed from: package-private */
        public void a() throws Exception {
            Object obj;
            HashMap hashMap = Mobclix.d.get(this.f.a.p).a.get("openmillennial");
            Hashtable hashtable = new Hashtable();
            for (String str : hashMap.keySet()) {
                if (!str.equals("key1")) {
                    try {
                        String str2 = ((String) hashMap.get(str)).split("=")[0];
                        if (str2.length() != ((String) hashMap.get(str)).length()) {
                            hashtable.put(str2, ((String) hashMap.get(str)).substring(str2.length() + 1, ((String) hashMap.get(str)).length()));
                        }
                    } catch (Exception e) {
                    }
                }
            }
            hashtable.put("height", Integer.toString((int) this.f.a.s));
            hashtable.put("width", Integer.toString((int) this.f.a.r));
            try {
                if (MobclixDemographics.b != null) {
                    if (MobclixDemographics.b.containsKey(MobclixDemographics.Birthdate)) {
                        hashtable.put("age", Integer.toString((int) (((double) (System.currentTimeMillis() - new SimpleDateFormat("yyyymmdd").parse(MobclixDemographics.b.get(MobclixDemographics.Birthdate)).getTime())) / 3.15576E10d)));
                    }
                    if (MobclixDemographics.b.containsKey(MobclixDemographics.Education)) {
                        switch (Integer.parseInt(MobclixDemographics.b.get(MobclixDemographics.Education))) {
                            case 1:
                                hashtable.put("education", "highschool");
                                break;
                            case 2:
                                hashtable.put("education", "somecollege");
                                break;
                            case 4:
                                hashtable.put("education", "bachelor");
                                break;
                            case 5:
                                hashtable.put("education", "masters");
                                break;
                            case 6:
                                hashtable.put("education", "phd");
                                break;
                        }
                    }
                    if (MobclixDemographics.b.containsKey(MobclixDemographics.Ethnicity)) {
                        switch (Integer.parseInt(MobclixDemographics.b.get(MobclixDemographics.Ethnicity))) {
                            case 0:
                                hashtable.put("ethnicity", "other");
                                break;
                            case 2:
                                hashtable.put("ethnicity", "asian");
                                break;
                            case 3:
                                hashtable.put("ethnicity", "africanamerican");
                                break;
                            case 4:
                                hashtable.put("ethnicity", "hispanic");
                                break;
                            case 5:
                                hashtable.put("ethnicity", "nativeamerican");
                                break;
                            case 6:
                                hashtable.put("ethnicity", "white");
                                break;
                        }
                    }
                    if (MobclixDemographics.b.containsKey(MobclixDemographics.Gender)) {
                        switch (Integer.parseInt(MobclixDemographics.b.get(MobclixDemographics.Gender))) {
                            case 0:
                                hashtable.put(UmengConstants.AtomKey_Sex, "unknown");
                                break;
                            case 1:
                                hashtable.put(UmengConstants.AtomKey_Sex, "male");
                                break;
                            case 2:
                                hashtable.put(UmengConstants.AtomKey_Sex, "female");
                                break;
                        }
                    }
                    if (MobclixDemographics.b.containsKey(MobclixDemographics.Income)) {
                        hashtable.put("income", MobclixDemographics.b.get(MobclixDemographics.Income));
                    }
                    if (MobclixDemographics.b.containsKey(MobclixDemographics.MaritalStatus)) {
                        switch (Integer.parseInt(MobclixDemographics.b.get(MobclixDemographics.MaritalStatus))) {
                            case 1:
                                hashtable.put("marital", "single");
                                break;
                            case 3:
                                hashtable.put("marital", "relationship");
                                break;
                        }
                    }
                    if (MobclixDemographics.b.containsKey(MobclixDemographics.PostalCode)) {
                        hashtable.put("zip", MobclixDemographics.b.get(MobclixDemographics.PostalCode));
                    }
                }
            } catch (Exception e2) {
            }
            StringBuilder sb = new StringBuilder();
            Iterator<MobclixAdViewListener> it = this.f.a.u.iterator();
            while (it.hasNext()) {
                MobclixAdViewListener next = it.next();
                if (next != null) {
                    String a2 = next.a();
                    if (a2 == null) {
                        a2 = "";
                    }
                    if (!a2.equals("")) {
                        sb.append(a2).append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                    }
                }
            }
            if (sb.length() > 0) {
                hashtable.put("keywords", sb.toString());
            }
            Class<?> cls = Class.forName("com.millennialmedia.android.MMAdView");
            Constructor<?> constructor = cls.getConstructor(Activity.class, String.class, String.class, Integer.TYPE, Hashtable.class);
            if (this.f.a.p == "300x250") {
                obj = "MMBannerAdRectangle";
            } else {
                obj = "MMBannerAdTop";
            }
            this.a = (FrameLayout) constructor.newInstance((Activity) this.f.getContext(), hashMap.get("key1"), obj, -1, hashtable);
            Class<?> cls2 = Class.forName("com.millennialmedia.android.MMAdView$MMAdListener");
            MMInvocationHandler mMInvocationHandler = new MMInvocationHandler();
            Object cast = cls2.cast(Proxy.newProxyInstance(cls2.getClassLoader(), new Class[]{cls2}, mMInvocationHandler));
            cls.getMethod("setListener", cls2).invoke(this.a, cast);
            cls.getMethod("setId", Integer.TYPE).invoke(this.a, Integer.valueOf((int) (System.currentTimeMillis() % 1000000000)));
            this.a.setVisibility(4);
            this.f.a.addView(this.a);
            cls.getMethod("callForAd", new Class[0]).invoke(this.a, new Object[0]);
        }

        public class MMInvocationHandler implements InvocationHandler {
            public MMInvocationHandler() {
            }

            public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
                if (method.getName().equals("MMAdReturned")) {
                    ((Activity) OpenAllocationPage.this.f.a.getContext()).runOnUiThread(new Runnable() {
                        public void run() {
                            if (OpenAllocationPage.this.d == 1) {
                                try {
                                    ViewParent parent = OpenAllocationPage.this.a.getParent();
                                    if (parent != null) {
                                        ((ViewGroup) parent).removeView(OpenAllocationPage.this.a);
                                    }
                                } catch (Exception e) {
                                }
                                OpenAllocationPage.this.addView(OpenAllocationPage.this.a);
                                OpenAllocationPage.this.a.setVisibility(0);
                                OpenAllocationPage.this.e().d.sendEmptyMessage(0);
                            }
                            OpenAllocationPage.this.d++;
                        }
                    });
                    return null;
                } else if (method.getName().equals("MMAdFailed")) {
                    ((Activity) OpenAllocationPage.this.f.a.getContext()).runOnUiThread(new Runnable() {
                        public void run() {
                            try {
                                ViewParent parent = OpenAllocationPage.this.a.getParent();
                                if (parent != null) {
                                    ((ViewGroup) parent).removeView(OpenAllocationPage.this.a);
                                }
                            } catch (Exception e) {
                            }
                            if (OpenAllocationPage.this.c == null) {
                                OpenAllocationPage.this.c = "";
                            }
                            OpenAllocationPage.this.f.a.a(OpenAllocationPage.this.c);
                        }
                    });
                    return null;
                } else if (!method.getName().equals("MMAdClickedToNewBrowser") && !method.getName().equals("MMAdClickedToOverlay")) {
                    return null;
                } else {
                    Iterator<MobclixAdViewListener> it = OpenAllocationPage.this.f.a.u.iterator();
                    while (it.hasNext()) {
                        MobclixAdViewListener next = it.next();
                        if (next != null) {
                            next.a(OpenAllocationPage.this.f.a);
                        }
                    }
                    return null;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void c() throws Exception {
            Class<?> cls = Class.forName("com.admob.android.ads.AdView");
            this.a = (RelativeLayout) cls.getConstructor(Activity.class).newInstance((Activity) this.f.getContext());
            Class<?> cls2 = Class.forName("com.admob.android.ads.AdListener");
            AdMobInvocationHandler adMobInvocationHandler = new AdMobInvocationHandler();
            Object cast = cls2.cast(Proxy.newProxyInstance(cls2.getClassLoader(), new Class[]{cls2}, adMobInvocationHandler));
            cls.getMethod("setAdListener", cls2).invoke(this.a, cast);
            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            Iterator<MobclixAdViewListener> it = this.f.a.u.iterator();
            while (it.hasNext()) {
                MobclixAdViewListener next = it.next();
                if (next != null) {
                    String a2 = next.a();
                    if (a2 == null) {
                        a2 = "";
                    }
                    if (!a2.equals("")) {
                        sb.append(a2).append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                    }
                    String b2 = next.b();
                    if (b2 == null) {
                        b2 = "";
                    }
                    if (!b2.equals("")) {
                        sb2.append(b2).append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                    }
                }
            }
            if (sb.length() > 0) {
                cls.getMethod("setKeywords", String.class).invoke(this.a, sb.toString());
            }
            if (sb2.length() > 0) {
                cls.getMethod("setSearchQuery", String.class).invoke(this.a, sb2.toString());
            }
            this.a.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Iterator<MobclixAdViewListener> it = OpenAllocationPage.this.f.a.u.iterator();
                    while (it.hasNext()) {
                        MobclixAdViewListener next = it.next();
                        if (next != null) {
                            next.a(OpenAllocationPage.this.f.a);
                        }
                    }
                }
            });
            this.a.setVisibility(4);
            this.f.a.addView(this.a);
        }

        public class AdMobInvocationHandler implements InvocationHandler {
            public AdMobInvocationHandler() {
            }

            public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
                if (method.getName().equals("onReceiveAd")) {
                    try {
                        ViewParent parent = OpenAllocationPage.this.a.getParent();
                        if (parent != null) {
                            ((ViewGroup) parent).removeView(OpenAllocationPage.this.a);
                        }
                    } catch (Exception e) {
                    }
                    if (OpenAllocationPage.this.d != 0) {
                        return null;
                    }
                    OpenAllocationPage.this.d++;
                    OpenAllocationPage.this.addView(OpenAllocationPage.this.a);
                    OpenAllocationPage.this.a.setVisibility(0);
                    OpenAllocationPage.this.e().d.sendEmptyMessage(0);
                    return null;
                } else if (method.getName().equals("onPresentScreen")) {
                    Iterator<MobclixAdViewListener> it = OpenAllocationPage.this.f.a.u.iterator();
                    while (it.hasNext()) {
                        MobclixAdViewListener next = it.next();
                        if (next != null) {
                            next.a(OpenAllocationPage.this.f.a);
                        }
                    }
                    return null;
                } else if (method.getName().equals("onLeaveApplication")) {
                    return null;
                } else {
                    if (OpenAllocationPage.this.c == null) {
                        OpenAllocationPage.this.c = "";
                    }
                    OpenAllocationPage.this.f.a.a(OpenAllocationPage.this.c);
                    return null;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void d() throws Exception {
            Class<?> cls = Class.forName("com.google.ads.AdSize");
            Field[] fields = cls.getFields();
            Object obj = null;
            Object obj2 = null;
            for (int i = 0; i < fields.length; i++) {
                if (fields[i].getName().equals("BANNER")) {
                    obj2 = fields[i].get(null);
                }
                if (fields[i].getName().equals("IAB_MRECT")) {
                    obj = fields[i].get(null);
                }
            }
            Class<?> cls2 = Class.forName("com.google.ads.AdView");
            Constructor<?> constructor = cls2.getConstructor(Activity.class, cls, String.class);
            if (!this.f.a.p.equals("300x250")) {
                obj = obj2;
            }
            this.a = (RelativeLayout) constructor.newInstance((Activity) this.f.getContext(), obj, Mobclix.getInstance().c());
            Class<?> cls3 = Class.forName("com.google.ads.AdListener");
            cls2.getMethod("setAdListener", cls3).invoke(this.a, cls3.cast(Proxy.newProxyInstance(cls3.getClassLoader(), new Class[]{cls3}, new AdMobInvocationHandler())));
            Class<?> cls4 = Class.forName("com.google.ads.AdRequest");
            Object newInstance = cls4.getConstructor(new Class[0]).newInstance(new Object[0]);
            HashSet hashSet = new HashSet();
            Iterator<MobclixAdViewListener> it = this.f.a.u.iterator();
            while (it.hasNext()) {
                MobclixAdViewListener next = it.next();
                if (next != null) {
                    String a2 = next.a();
                    if (a2 == null) {
                        a2 = "";
                    }
                    if (!a2.equals("")) {
                        String[] split = a2.split(",\\s*");
                        for (String add : split) {
                            hashSet.add(add);
                        }
                    }
                }
            }
            if (hashSet.size() > 0) {
                cls4.getMethod("setKeywords", Set.class).invoke(newInstance, hashSet);
            }
            cls4.getMethod("setTesting", Boolean.TYPE).invoke(newInstance, true);
            cls2.getMethod("loadAd", cls4).invoke(this.a, newInstance);
            this.a.setVisibility(4);
            this.f.a.addView(this.a);
        }

        /* access modifiers changed from: package-private */
        public void b() {
            try {
                if (this.a.getClass().getName().equals("com.google.ads.AdView")) {
                    this.a.setOnClickListener(null);
                    Class.forName("com.google.ads.AdView").getMethod("destroy", new Class[0]).invoke(this.a, new Object[0]);
                    ((ViewGroup) this.a.getParent()).removeView(this.a);
                }
                if (this.a.getClass().getName().equals("com.millennialmedia.android.MMAdView")) {
                    this.a.setOnClickListener(null);
                    Class.forName("com.millennialmedia.android.MMAdView").getMethod("halt", new Class[0]).invoke(this.a, new Object[0]);
                    ((ViewGroup) this.a.getParent()).removeView(this.a);
                }
            } catch (Throwable th) {
            }
        }
    }

    class Action {
        String a = "null";
        String b = "";
        String c = "standard";
        boolean d = false;
        Bitmap e = null;
        String f = "";
        private JSONObject h;
        private ArrayList<String> i = new ArrayList<>();
        private ArrayList<String> j = new ArrayList<>();
        /* access modifiers changed from: private */
        public MobclixCreative k;

        Action(MobclixCreative mobclixCreative) {
            this.k = mobclixCreative;
        }

        Action(MobclixCreative mobclixCreative, JSONObject jSONObject, MobclixCreative mobclixCreative2) {
            MobclixCreative.this = mobclixCreative;
            try {
                this.h = jSONObject;
                this.k = mobclixCreative2;
                this.a = jSONObject.getString(UmengConstants.AtomKey_Type);
                if (jSONObject.has("autoplay")) {
                    this.d = jSONObject.getBoolean("autoplay");
                }
                try {
                    JSONObject jSONObject2 = jSONObject.getJSONObject("eventUrls");
                    JSONArray jSONArray = jSONObject2.getJSONArray("onShow");
                    for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                        this.i.add(jSONArray.getString(i2));
                    }
                    JSONArray jSONArray2 = jSONObject2.getJSONArray("onTouch");
                    for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                        this.j.add(jSONArray2.getString(i3));
                    }
                } catch (Exception e2) {
                }
                if (this.a.equals("url")) {
                    try {
                        this.b = jSONObject.getString("url");
                    } catch (JSONException e3) {
                    }
                    try {
                        this.c = jSONObject.getString("browserType");
                    } catch (JSONException e4) {
                    }
                    try {
                        if (jSONObject.getBoolean("preload")) {
                            b();
                        }
                    } catch (JSONException e5) {
                    }
                } else if (this.a.equals(AdActivity.HTML_PARAM)) {
                    try {
                        this.b = jSONObject.getString("baseUrl");
                    } catch (JSONException e6) {
                    }
                    try {
                        this.f = jSONObject.getString(AdActivity.HTML_PARAM);
                    } catch (JSONException e7) {
                    }
                    try {
                        this.c = jSONObject.getString("browserType");
                    } catch (JSONException e8) {
                    }
                }
            } catch (JSONException e9) {
            }
        }

        public boolean a() {
            return this.d;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.k.k.push(new Thread(new Mobclix.FetchResponseThread(this.b, new Handler() {
                public void handleMessage(Message message) {
                    String string = message.getData().getString(UmengConstants.AtomKey_Type);
                    if (string.equals("success")) {
                        Action.this.f = message.getData().getString("response");
                    } else if (string.equals("failure")) {
                        Action.this.f = "";
                    }
                    Action.this.k.d.sendEmptyMessage(0);
                }
            })));
        }

        public boolean c() {
            try {
                Iterator<String> it = this.i.iterator();
                while (it.hasNext()) {
                    new Thread(new Mobclix.FetchImageThread(it.next(), new Mobclix.BitmapHandler())).start();
                }
            } catch (Exception e2) {
            }
            Iterator<MobclixAdViewListener> it2 = MobclixCreative.this.a.u.iterator();
            while (it2.hasNext()) {
                MobclixAdViewListener next = it2.next();
                if (next != null) {
                    next.a(MobclixCreative.this.a);
                }
            }
            MobclixCreative.r = true;
            if (this.a.equals("url") || this.a.equals(AdActivity.HTML_PARAM)) {
                d();
            } else if (this.a.equals("video")) {
                Intent intent = new Intent();
                String packageName = MobclixCreative.this.a.getContext().getPackageName();
                intent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "video").putExtra(String.valueOf(packageName) + ".data", this.h.toString());
                MobclixCreative.this.getContext().startActivity(intent);
            }
            MobclixCreative.r = false;
            return true;
        }

        /* access modifiers changed from: package-private */
        public void d() {
            try {
                if (!this.b.equals("")) {
                    Uri parse = Uri.parse(this.b);
                    String[] split = this.b.split("mobclix://");
                    if (split.length <= 1) {
                        split = this.b.split("mobclix%3A%2F%2F");
                        if (split.length <= 1) {
                            for (int i2 = 0; i2 < Mobclix.getInstance().y().size(); i2++) {
                                if (parse.getHost().equals(Mobclix.getInstance().y().get(i2))) {
                                    MobclixCreative.this.getContext().startActivity(new Intent("android.intent.action.VIEW", parse));
                                    return;
                                }
                            }
                            if (!this.f.equals("")) {
                                Intent intent = new Intent();
                                String packageName = MobclixCreative.this.a.getContext().getPackageName();
                                try {
                                    this.h.put("cachedHTML", this.f);
                                } catch (JSONException e2) {
                                }
                                intent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "browser").putExtra(String.valueOf(packageName) + ".data", this.h.toString());
                                MobclixCreative.this.getContext().startActivity(intent);
                            } else if (this.c.equals("minimal")) {
                                Intent intent2 = new Intent();
                                String packageName2 = MobclixCreative.this.a.getContext().getPackageName();
                                intent2.setClassName(packageName2, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName2) + ".type", "browser").putExtra(String.valueOf(packageName2) + ".data", this.h.toString());
                                MobclixCreative.this.getContext().startActivity(intent2);
                                return;
                            } else {
                                MobclixCreative.this.getContext().startActivity(new Intent("android.intent.action.VIEW", parse));
                            }
                            CookieSyncManager.getInstance().sync();
                            return;
                        }
                    }
                    String str = split[1];
                    MobclixCreative.this.u = new Thread(new CustomAdThread(this.b));
                    MobclixCreative.this.u.start();
                    Iterator<MobclixAdViewListener> it = MobclixCreative.this.a.u.iterator();
                    while (it.hasNext()) {
                        MobclixAdViewListener next = it.next();
                        if (next != null) {
                            next.a(MobclixCreative.this.a, str);
                        }
                    }
                }
            } catch (Exception e3) {
            }
        }
    }

    private class Rotate3dAnimation extends Animation {
        private final float b;
        private final float c;
        private final float d;
        private final float e;
        private final float f;
        private final boolean g;
        private Camera h;

        public Rotate3dAnimation(float f2, float f3, float f4, float f5, float f6, boolean z) {
            this.b = f2;
            this.c = f3;
            this.d = f4;
            this.e = f5;
            this.f = f6;
            this.g = z;
        }

        public void initialize(int i, int i2, int i3, int i4) {
            super.initialize(i, i2, i3, i4);
            this.h = new Camera();
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float f2, Transformation transformation) {
            float f3 = this.b;
            float f4 = f3 + ((this.c - f3) * f2);
            float f5 = this.d;
            float f6 = this.e;
            Camera camera = this.h;
            Matrix matrix = transformation.getMatrix();
            camera.save();
            if (this.g) {
                camera.translate(0.0f, 0.0f, this.f * f2);
            } else {
                camera.translate(0.0f, 0.0f, this.f * (1.0f - f2));
            }
            camera.rotateY(f4);
            camera.getMatrix(matrix);
            camera.restore();
            matrix.preTranslate(-f5, -f6);
            matrix.postTranslate(f5, f6);
        }
    }
}
