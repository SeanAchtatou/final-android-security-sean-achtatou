package com.mobclix.android.sdk;

import android.content.Context;
import android.util.AttributeSet;

public final class MobclixMMABannerXLAdView extends MobclixAdView {
    public MobclixMMABannerXLAdView(Context context) {
        super(context, "320x50");
    }

    public MobclixMMABannerXLAdView(Context context, AttributeSet attributeSet) {
        super(context, "320x50", attributeSet);
    }
}
