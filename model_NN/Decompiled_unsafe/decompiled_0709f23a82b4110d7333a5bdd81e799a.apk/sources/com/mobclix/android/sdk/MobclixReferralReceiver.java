package com.mobclix.android.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class MobclixReferralReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String string = intent.getExtras().getString("referrer");
        if (string != null && !string.equals("")) {
            SharedPreferences.Editor edit = context.getSharedPreferences(String.valueOf(context.getPackageName()) + ".MCConfig", 0).edit();
            edit.putString("MCReferralData", string);
            edit.commit();
        }
        try {
            Class<?> cls = Class.forName("com.admob.android.ads.analytics.InstallReceiver");
            Object newInstance = cls.getConstructor(new Class[0]).newInstance(new Object[0]);
            cls.getMethod("onReceive", Context.class, Intent.class).invoke(newInstance, context, intent);
        } catch (Exception e) {
            try {
                Class<?> cls2 = Class.forName("com.google.android.apps.analytics.AnalyticsReceiver");
                Object newInstance2 = cls2.getConstructor(new Class[0]).newInstance(new Object[0]);
                cls2.getMethod("onReceive", Context.class, Intent.class).invoke(newInstance2, context, intent);
            } catch (Exception e2) {
            }
        }
    }
}
