package com.mobclix.android.sdk;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class MobclixCreativeManager {
    JSONArray a;
    int b = 0;

    MobclixCreativeManager(String str, int i) {
        this.b = i;
        try {
            this.a = new JSONObject(str).getJSONArray("creatives");
        } catch (JSONException e) {
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.a.length();
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        this.b++;
        if (this.b >= this.a.length()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public JSONObject d() {
        try {
            return this.a.getJSONObject(this.b);
        } catch (JSONException e) {
            return null;
        }
    }
}
