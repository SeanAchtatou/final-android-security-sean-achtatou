package com.mobclix.android.sdk;

import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Entity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import com.mobclick.android.UmengConstants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.codehaus.jackson.util.MinimalPrettyPrinter;
import org.json.JSONArray;
import org.json.JSONObject;

public class MobclixContactsSdk5 extends MobclixContacts {
    private static String a = "MobclixContactsSdk5";
    private final String b = "com.google";
    private String c;

    public Intent a() {
        return new Intent("android.intent.action.PICK", ContactsContract.Contacts.CONTENT_URI);
    }

    public Intent a(JSONObject jSONObject) {
        String str;
        Uri fromParts;
        int i;
        String str2;
        int i2;
        String string;
        String string2;
        String string3;
        String string4;
        String string5;
        String string6;
        String string7;
        try {
            HashMap<String, String> b2 = b(jSONObject);
            JSONArray jSONArray = jSONObject.getJSONArray("addresses");
            JSONArray jSONArray2 = jSONObject.getJSONArray("phoneNumbers");
            int i3 = 0;
            while (true) {
                if (i3 < jSONArray2.length()) {
                    JSONObject jSONObject2 = jSONArray2.getJSONObject(i3);
                    if (jSONObject2.has("number") && (str = jSONObject2.getString("number")) != null && !str.equals("")) {
                        break;
                    }
                    i3++;
                } else {
                    str = null;
                    break;
                }
            }
            if (str != null) {
                fromParts = Uri.fromParts("tel", str, null);
            } else if (b2.get("email") == null) {
                return null;
            } else {
                fromParts = Uri.fromParts("mailto", b2.get("email"), null);
            }
            Intent intent = new Intent("com.android.contacts.action.SHOW_OR_CREATE_CONTACT", fromParts);
            intent.putExtra("name", b2.get("displayName"));
            intent.putExtra("notes", b2.get("note"));
            intent.putExtra("company", b2.get("organization"));
            intent.putExtra("job_title", b2.get("jobTitle"));
            intent.putExtra("email", b2.get("email"));
            intent.putExtra("email_type", 1);
            intent.putExtra("im_handle", b2.get("im"));
            intent.putExtra("im_protocol", 5);
            if (jSONArray.length() > 0) {
                JSONObject jSONObject3 = jSONArray.getJSONObject(0);
                StringBuilder sb = new StringBuilder();
                if (!jSONObject3.has("label") || jSONObject3.getString("label") == null || !jSONObject3.getString("label").equalsIgnoreCase("work")) {
                    i2 = 1;
                } else {
                    i2 = 2;
                }
                if (jSONObject3.has("street") && (string7 = jSONObject3.getString("street")) != null) {
                    sb.append(string7);
                }
                if (jSONObject3.has("poBox") && (string6 = jSONObject3.getString("poBox")) != null && !string6.equals("")) {
                    if (sb.length() != 0) {
                        sb.append(", ");
                    }
                    sb.append(string6);
                }
                if (jSONObject3.has("neighborhood") && (string5 = jSONObject3.getString("neighborhood")) != null && !string5.equals("")) {
                    if (sb.length() != 0) {
                        sb.append(", ");
                    }
                    sb.append(string5);
                }
                if (jSONObject3.has("city") && (string4 = jSONObject3.getString("city")) != null && !string4.equals("")) {
                    if (sb.length() != 0) {
                        sb.append(", ");
                    }
                    sb.append(string4);
                }
                if (jSONObject3.has(UmengConstants.AtomKey_State) && (string3 = jSONObject3.getString(UmengConstants.AtomKey_State)) != null && !string3.equals("")) {
                    if (sb.length() != 0) {
                        sb.append(", ");
                    }
                    sb.append(string3);
                }
                if (jSONObject3.has("postalCode") && (string2 = jSONObject3.getString("postalCode")) != null && !string2.equals("")) {
                    if (sb.length() != 0) {
                        sb.append(", ");
                    }
                    sb.append(string2);
                }
                if (jSONObject3.has("country") && (string = jSONObject3.getString("country")) != null && !string.equals("")) {
                    if (sb.length() != 0) {
                        sb.append(", ");
                    }
                    sb.append(string);
                }
                if (sb.length() > 0) {
                    intent.putExtra("postal", sb.toString());
                    intent.putExtra("postal_type", i2);
                }
            }
            for (int i4 = 0; i4 < jSONArray2.length(); i4++) {
                JSONObject jSONObject4 = jSONArray2.getJSONObject(i4);
                if (!jSONObject4.has("label") || jSONObject4.getString("label") == null || !jSONObject4.getString("label").equalsIgnoreCase("work")) {
                    i = 1;
                } else {
                    i = 3;
                }
                if (jSONObject4.has("number")) {
                    str2 = jSONObject4.getString("number");
                    if (str2.equals("")) {
                        str2 = null;
                    }
                } else {
                    str2 = null;
                }
                if (i4 != 0) {
                    if (i4 != 1) {
                        if (i4 != 2) {
                            break;
                        }
                        intent.putExtra("tertiary_phone", str2);
                        intent.putExtra("tertiary_phone_type", i);
                    } else {
                        intent.putExtra("secondary_phone", str2);
                        intent.putExtra("secondary_phone_type", i);
                    }
                } else {
                    intent.putExtra("phone", str2);
                    intent.putExtra("phone_type", i);
                }
            }
            return intent;
        } catch (Exception e) {
            return null;
        }
    }

    private static Cursor b(ContentResolver contentResolver, Uri uri) {
        if (uri == null) {
            return null;
        }
        List<String> pathSegments = uri.getPathSegments();
        if (pathSegments.size() != 4) {
            return null;
        }
        long parseLong = Long.parseLong(pathSegments.get(3));
        String encode = Uri.encode(pathSegments.get(2));
        Cursor query = contentResolver.query(Uri.withAppendedPath(ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, parseLong), "data"), null, null, null, null);
        if (!query.moveToFirst()) {
            query.close();
            return null;
        } else if (query.getString(query.getColumnIndex("lookup")).equals(encode)) {
            return query;
        } else {
            query.close();
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:263:0x0514, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:266:0x051f, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:267:0x0520, code lost:
        r4 = r2;
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:326:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0094, code lost:
        if (r2.equals("") == false) goto L_0x04d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c1, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        r5.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c5, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00c7, code lost:
        r2 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0174, code lost:
        r3 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x020f, code lost:
        r2 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:270:0x0528 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:6:0x0028] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x020f A[ExcHandler: all (th java.lang.Throwable), Splitter:B:13:0x0053] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.json.JSONObject a(android.content.ContentResolver r22, android.net.Uri r23) {
        /*
            r21 = this;
            r2 = 0
            java.lang.String r3 = r23.getAuthority()
            java.lang.String r4 = "com.android.contacts"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x009d
        L_0x000d:
            org.json.JSONObject r8 = new org.json.JSONObject
            r8.<init>()
            org.json.JSONArray r14 = new org.json.JSONArray
            r14.<init>()
            org.json.JSONArray r15 = new org.json.JSONArray
            r15.<init>()
            java.lang.String r2 = "addresses"
            r8.put(r2, r14)     // Catch:{ Exception -> 0x0535 }
            java.lang.String r2 = "phoneNumbers"
            r8.put(r2, r15)     // Catch:{ Exception -> 0x0535 }
        L_0x0026:
            r2 = 0
            r10 = 0
            android.database.Cursor r2 = b(r22, r23)     // Catch:{ Exception -> 0x0528, all -> 0x051a }
            if (r2 != 0) goto L_0x0550
            android.net.Uri r23 = android.provider.ContactsContract.Contacts.getLookupUri(r22, r23)     // Catch:{ Exception -> 0x0528, all -> 0x051f }
            android.database.Cursor r9 = b(r22, r23)     // Catch:{ Exception -> 0x0528, all -> 0x051f }
        L_0x0036:
            long r11 = android.content.ContentUris.parseId(r23)     // Catch:{ Exception -> 0x052b, all -> 0x0524 }
            r9.close()     // Catch:{ Exception -> 0x052b, all -> 0x0524 }
            android.net.Uri r3 = android.provider.ContactsContract.RawContactsEntity.CONTENT_URI     // Catch:{ Exception -> 0x052b, all -> 0x0524 }
            r4 = 0
            java.lang.String r5 = "contact_id=?"
            r2 = 1
            java.lang.String[] r6 = new java.lang.String[r2]     // Catch:{ Exception -> 0x052b, all -> 0x0524 }
            r2 = 0
            java.lang.String r7 = java.lang.String.valueOf(r11)     // Catch:{ Exception -> 0x052b, all -> 0x0524 }
            r6[r2] = r7     // Catch:{ Exception -> 0x052b, all -> 0x0524 }
            r7 = 0
            r2 = r22
            android.database.Cursor r4 = r2.query(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x052b, all -> 0x0524 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            int r2 = r4.getCount()     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            r3.<init>(r2)     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            com.mobclix.android.sdk.MobclixContactsEntityIterator r5 = newEntityIterator(r4)     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
        L_0x0060:
            boolean r2 = r5.hasNext()     // Catch:{ all -> 0x00c1 }
            if (r2 != 0) goto L_0x00b7
            r5.b()     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            java.util.Iterator r16 = r3.iterator()     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            r3 = r10
        L_0x006e:
            boolean r2 = r16.hasNext()     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            if (r2 != 0) goto L_0x00cd
            r5 = 0
            r2 = 0
            java.lang.String r6 = "firstName"
            java.lang.String r5 = r8.getString(r6)     // Catch:{ Exception -> 0x0532, all -> 0x020f }
        L_0x007c:
            java.lang.String r6 = "lastName"
            java.lang.String r2 = r8.getString(r6)     // Catch:{ Exception -> 0x052f, all -> 0x020f }
        L_0x0082:
            if (r5 == 0) goto L_0x0096
            if (r2 == 0) goto L_0x0096
            java.lang.String r6 = ""
            boolean r5 = r5.equals(r6)     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            if (r5 != 0) goto L_0x0096
            java.lang.String r5 = ""
            boolean r2 = r2.equals(r5)     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            if (r2 == 0) goto L_0x04d9
        L_0x0096:
            if (r3 != 0) goto L_0x04ba
            r4.close()
            r2 = 0
        L_0x009c:
            return r2
        L_0x009d:
            java.lang.String r4 = "contacts"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x0553
            long r2 = android.content.ContentUris.parseId(r23)
            android.net.Uri r4 = android.provider.ContactsContract.RawContacts.CONTENT_URI
            android.net.Uri r2 = android.content.ContentUris.withAppendedId(r4, r2)
            r0 = r22
            android.net.Uri r23 = android.provider.ContactsContract.RawContacts.getContactLookupUri(r0, r2)
            goto L_0x000d
        L_0x00b7:
            java.lang.Object r2 = r5.next()     // Catch:{ all -> 0x00c1 }
            android.content.Entity r2 = (android.content.Entity) r2     // Catch:{ all -> 0x00c1 }
            r3.add(r2)     // Catch:{ all -> 0x00c1 }
            goto L_0x0060
        L_0x00c1:
            r2 = move-exception
            r5.b()     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            throw r2     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
        L_0x00c6:
            r2 = move-exception
            r2 = r4
        L_0x00c8:
            r2.close()
            r2 = 0
            goto L_0x009c
        L_0x00cd:
            java.lang.Object r2 = r16.next()     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            android.content.Entity r2 = (android.content.Entity) r2     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            android.content.ContentValues r5 = r2.getEntityValues()     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = "account_type"
            java.lang.String r6 = r5.getAsString(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r7 = "_id"
            java.lang.Long r5 = r5.getAsLong(r7)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            long r17 = r5.longValue()     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r5 = "ACCOUNT TYPE: "
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            r7.<init>(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = ": "
            java.lang.StringBuilder r6 = r7.append(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            r0 = r17
            java.lang.StringBuilder r6 = r6.append(r0)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.util.ArrayList r2 = r2.getSubValues()     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.util.Iterator r19 = r2.iterator()     // Catch:{ Exception -> 0x0173, all -> 0x020f }
        L_0x010d:
            boolean r2 = r19.hasNext()     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r2 == 0) goto L_0x006e
            java.lang.Object r2 = r19.next()     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.Entity$NamedContentValues r2 = (android.content.Entity.NamedContentValues) r2     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r5 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = "raw_contact_id"
            java.lang.Long r7 = java.lang.Long.valueOf(r17)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            r5.put(r6, r7)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = "mimetype"
            java.lang.String r5 = r5.getAsString(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 == 0) goto L_0x010d
            java.lang.String r6 = "vnd.android.cursor.item/phone_v2"
            boolean r6 = r6.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 == 0) goto L_0x0178
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "PHONE"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r5 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = "data1"
            java.lang.String r6 = r5.getAsString(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r5 = "home"
            android.content.ContentValues r2 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r7 = "data2"
            java.lang.Integer r2 = r2.getAsInteger(r7)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            r7 = 3
            if (r2 != r7) goto L_0x054d
            java.lang.String r2 = "work"
        L_0x0156:
            if (r6 == 0) goto L_0x010d
            java.lang.String r5 = ""
            boolean r5 = r6.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 != 0) goto L_0x010d
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            r5.<init>()     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r7 = "number"
            r5.put(r7, r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = "label"
            r5.put(r6, r2)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            r15.put(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            goto L_0x010d
        L_0x0173:
            r2 = move-exception
            r2 = r3
            r3 = r2
            goto L_0x006e
        L_0x0178:
            java.lang.String r6 = "vnd.android.cursor.item/name"
            boolean r6 = r6.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 == 0) goto L_0x0214
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "NAME"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r5 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = "data1"
            java.lang.String r5 = r5.getAsString(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 == 0) goto L_0x019a
            java.lang.String r6 = ""
            boolean r6 = r5.equals(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 != 0) goto L_0x019a
            r3 = r5
        L_0x019a:
            android.content.ContentValues r5 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = "data2"
            java.lang.String r5 = r5.getAsString(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r6 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r7 = "data3"
            java.lang.String r6 = r6.getAsString(r7)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r7 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r9 = "data5"
            java.lang.String r7 = r7.getAsString(r9)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r9 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r10 = "data4"
            java.lang.String r9 = r9.getAsString(r10)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r2 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r10 = "data6"
            java.lang.String r2 = r2.getAsString(r10)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 == 0) goto L_0x01d1
            java.lang.String r10 = ""
            boolean r10 = r5.equals(r10)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r10 != 0) goto L_0x01d1
            java.lang.String r10 = "firstName"
            r8.put(r10, r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
        L_0x01d1:
            if (r6 == 0) goto L_0x01e0
            java.lang.String r5 = ""
            boolean r5 = r6.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 != 0) goto L_0x01e0
            java.lang.String r5 = "lastName"
            r8.put(r5, r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
        L_0x01e0:
            if (r7 == 0) goto L_0x01ef
            java.lang.String r5 = ""
            boolean r5 = r7.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 != 0) goto L_0x01ef
            java.lang.String r5 = "middleName"
            r8.put(r5, r7)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
        L_0x01ef:
            if (r9 == 0) goto L_0x01fe
            java.lang.String r5 = ""
            boolean r5 = r9.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 != 0) goto L_0x01fe
            java.lang.String r5 = "prefix"
            r8.put(r5, r9)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
        L_0x01fe:
            if (r2 == 0) goto L_0x010d
            java.lang.String r5 = ""
            boolean r5 = r2.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 != 0) goto L_0x010d
            java.lang.String r5 = "suffix"
            r8.put(r5, r2)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            goto L_0x010d
        L_0x020f:
            r2 = move-exception
        L_0x0210:
            r4.close()
            throw r2
        L_0x0214:
            java.lang.String r6 = "vnd.android.cursor.item/nickname"
            boolean r6 = r6.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 == 0) goto L_0x023c
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "NICKNAME"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r2 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r5 = "data1"
            java.lang.String r2 = r2.getAsString(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r2 == 0) goto L_0x010d
            java.lang.String r5 = ""
            boolean r5 = r2.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 != 0) goto L_0x010d
            java.lang.String r5 = "nickname"
            r8.put(r5, r2)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            goto L_0x010d
        L_0x023c:
            java.lang.String r6 = "vnd.android.cursor.item/organization"
            boolean r6 = r6.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 == 0) goto L_0x0292
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "ORGANIZATION"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r5 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = "data1"
            java.lang.String r5 = r5.getAsString(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r6 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r7 = "data4"
            java.lang.String r6 = r6.getAsString(r7)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r2 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r7 = "data5"
            java.lang.String r2 = r2.getAsString(r7)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 == 0) goto L_0x0272
            java.lang.String r7 = ""
            boolean r7 = r5.equals(r7)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r7 != 0) goto L_0x0272
            java.lang.String r7 = "organization"
            r8.put(r7, r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
        L_0x0272:
            if (r6 == 0) goto L_0x0281
            java.lang.String r5 = ""
            boolean r5 = r6.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 != 0) goto L_0x0281
            java.lang.String r5 = "jobTitle"
            r8.put(r5, r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
        L_0x0281:
            if (r2 == 0) goto L_0x010d
            java.lang.String r5 = ""
            boolean r5 = r2.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 != 0) goto L_0x010d
            java.lang.String r5 = "department"
            r8.put(r5, r2)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            goto L_0x010d
        L_0x0292:
            java.lang.String r6 = "vnd.android.cursor.item/email_v2"
            boolean r6 = r6.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 == 0) goto L_0x02ba
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "EMAIL"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r2 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r5 = "data1"
            java.lang.String r2 = r2.getAsString(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r2 == 0) goto L_0x010d
            java.lang.String r5 = ""
            boolean r5 = r2.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 != 0) goto L_0x010d
            java.lang.String r5 = "email"
            r8.put(r5, r2)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            goto L_0x010d
        L_0x02ba:
            java.lang.String r6 = "vnd.android.cursor.item/note"
            boolean r6 = r6.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 == 0) goto L_0x02e2
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "NOTE"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r2 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r5 = "data1"
            java.lang.String r2 = r2.getAsString(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r2 == 0) goto L_0x010d
            java.lang.String r5 = ""
            boolean r5 = r2.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 != 0) goto L_0x010d
            java.lang.String r5 = "note"
            r8.put(r5, r2)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            goto L_0x010d
        L_0x02e2:
            java.lang.String r6 = "vnd.android.cursor.item/postal-address_v2"
            boolean r6 = r6.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 == 0) goto L_0x0446
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "POSTAL"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r5 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = "data1"
            java.lang.String r5 = r5.getAsString(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 == 0) goto L_0x0303
            java.lang.String r6 = ""
            boolean r6 = r5.equals(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 == 0) goto L_0x054a
        L_0x0303:
            r5 = 0
            r13 = r5
        L_0x0305:
            java.lang.String r5 = "home"
            android.content.ContentValues r6 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r7 = "data2"
            java.lang.Integer r6 = r6.getAsInteger(r7)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            int r6 = r6.intValue()     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            r7 = 2
            if (r6 != r7) goto L_0x0547
            java.lang.String r5 = "work"
            r12 = r5
        L_0x0319:
            android.content.ContentValues r5 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = "data4"
            java.lang.String r5 = r5.getAsString(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 == 0) goto L_0x032b
            java.lang.String r6 = ""
            boolean r6 = r5.equals(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 == 0) goto L_0x0544
        L_0x032b:
            r5 = 0
            r11 = r5
        L_0x032d:
            android.content.ContentValues r5 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = "data5"
            java.lang.String r5 = r5.getAsString(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 == 0) goto L_0x033f
            java.lang.String r6 = ""
            boolean r6 = r5.equals(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 == 0) goto L_0x0541
        L_0x033f:
            r5 = 0
            r10 = r5
        L_0x0341:
            android.content.ContentValues r5 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = "data6"
            java.lang.String r5 = r5.getAsString(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 == 0) goto L_0x0353
            java.lang.String r6 = ""
            boolean r6 = r5.equals(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 == 0) goto L_0x053e
        L_0x0353:
            r5 = 0
            r9 = r5
        L_0x0355:
            android.content.ContentValues r5 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = "data7"
            java.lang.String r5 = r5.getAsString(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 == 0) goto L_0x0367
            java.lang.String r6 = ""
            boolean r6 = r5.equals(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 == 0) goto L_0x053b
        L_0x0367:
            r5 = 0
            r7 = r5
        L_0x0369:
            android.content.ContentValues r5 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r6 = "data8"
            java.lang.String r5 = r5.getAsString(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 == 0) goto L_0x037b
            java.lang.String r6 = ""
            boolean r6 = r5.equals(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 == 0) goto L_0x0538
        L_0x037b:
            r5 = 0
            r6 = r5
        L_0x037d:
            android.content.ContentValues r5 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r20 = "data9"
            r0 = r20
            java.lang.String r5 = r5.getAsString(r0)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 == 0) goto L_0x0393
            java.lang.String r20 = ""
            r0 = r20
            boolean r20 = r5.equals(r0)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r20 == 0) goto L_0x0394
        L_0x0393:
            r5 = 0
        L_0x0394:
            android.content.ContentValues r2 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r20 = "data10"
            r0 = r20
            java.lang.String r2 = r2.getAsString(r0)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r2 == 0) goto L_0x03aa
            java.lang.String r20 = ""
            r0 = r20
            boolean r20 = r2.equals(r0)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r20 == 0) goto L_0x03ab
        L_0x03aa:
            r2 = 0
        L_0x03ab:
            if (r11 != 0) goto L_0x03ca
            if (r10 != 0) goto L_0x03ca
            if (r9 != 0) goto L_0x03ca
            if (r7 != 0) goto L_0x03ca
            if (r6 != 0) goto L_0x03ca
            if (r5 != 0) goto L_0x03ca
            if (r2 != 0) goto L_0x03ca
            if (r13 == 0) goto L_0x010d
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            r2.<init>()     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r5 = "street"
            r2.put(r5, r13)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            r14.put(r2)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            goto L_0x010d
        L_0x03ca:
            org.json.JSONObject r13 = new org.json.JSONObject     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            r13.<init>()     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r11 == 0) goto L_0x03e2
            java.lang.String r20 = ""
            r0 = r20
            boolean r20 = r11.equals(r0)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r20 != 0) goto L_0x03e2
            java.lang.String r20 = "street"
            r0 = r20
            r13.put(r0, r11)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
        L_0x03e2:
            if (r10 == 0) goto L_0x03f1
            java.lang.String r11 = ""
            boolean r11 = r10.equals(r11)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r11 != 0) goto L_0x03f1
            java.lang.String r11 = "poBox"
            r13.put(r11, r10)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
        L_0x03f1:
            if (r9 == 0) goto L_0x0400
            java.lang.String r10 = ""
            boolean r10 = r9.equals(r10)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r10 != 0) goto L_0x0400
            java.lang.String r10 = "neighborhood"
            r13.put(r10, r9)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
        L_0x0400:
            if (r7 == 0) goto L_0x040f
            java.lang.String r9 = ""
            boolean r9 = r7.equals(r9)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r9 != 0) goto L_0x040f
            java.lang.String r9 = "city"
            r13.put(r9, r7)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
        L_0x040f:
            if (r6 == 0) goto L_0x041e
            java.lang.String r7 = ""
            boolean r7 = r6.equals(r7)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r7 != 0) goto L_0x041e
            java.lang.String r7 = "state"
            r13.put(r7, r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
        L_0x041e:
            if (r5 == 0) goto L_0x042d
            java.lang.String r6 = ""
            boolean r6 = r5.equals(r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 != 0) goto L_0x042d
            java.lang.String r6 = "postalCode"
            r13.put(r6, r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
        L_0x042d:
            if (r2 == 0) goto L_0x043c
            java.lang.String r5 = ""
            boolean r5 = r2.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 != 0) goto L_0x043c
            java.lang.String r5 = "country"
            r13.put(r5, r2)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
        L_0x043c:
            java.lang.String r2 = "label"
            r13.put(r2, r12)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            r14.put(r13)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            goto L_0x010d
        L_0x0446:
            java.lang.String r6 = "vnd.android.cursor.item/im"
            boolean r6 = r6.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 == 0) goto L_0x046e
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "IM"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r2 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r5 = "data1"
            java.lang.String r2 = r2.getAsString(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r2 == 0) goto L_0x010d
            java.lang.String r5 = ""
            boolean r5 = r2.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 != 0) goto L_0x010d
            java.lang.String r5 = "IM"
            r8.put(r5, r2)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            goto L_0x010d
        L_0x046e:
            java.lang.String r6 = "vnd.android.cursor.item/website"
            boolean r6 = r6.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r6 == 0) goto L_0x0496
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "WEBSITE"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r2 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r5 = "data1"
            java.lang.String r2 = r2.getAsString(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r2 == 0) goto L_0x010d
            java.lang.String r5 = ""
            boolean r5 = r2.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 != 0) goto L_0x010d
            java.lang.String r5 = "website"
            r8.put(r5, r2)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            goto L_0x010d
        L_0x0496:
            java.lang.String r6 = "vnd.android.cursor.item/photo"
            boolean r5 = r6.equals(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r5 == 0) goto L_0x010d
            java.lang.String r5 = "loadContact: "
            java.lang.String r6 = "PHOTO"
            android.util.Log.v(r5, r6)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            android.content.ContentValues r2 = r2.values     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            java.lang.String r5 = "data15"
            byte[] r2 = r2.getAsByteArray(r5)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            if (r2 == 0) goto L_0x010d
            java.lang.String r5 = "image"
            java.lang.String r2 = com.mobclix.android.sdk.Base64.encodeBytes(r2)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            r8.put(r5, r2)     // Catch:{ Exception -> 0x0173, all -> 0x020f }
            goto L_0x010d
        L_0x04ba:
            java.lang.String r2 = " "
            java.lang.String[] r6 = r3.split(r2)     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            r5 = 0
            r2 = 0
            r3 = 0
            int r7 = r6.length     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            r9 = 1
            if (r7 != r9) goto L_0x04df
            r5 = 0
            r5 = r6[r5]     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
        L_0x04ca:
            java.lang.String r6 = "firstName"
            r8.put(r6, r5)     // Catch:{ Exception -> 0x0513, all -> 0x020f }
            java.lang.String r5 = "middleName"
            r8.put(r5, r2)     // Catch:{ Exception -> 0x0513, all -> 0x020f }
            java.lang.String r2 = "lastName"
            r8.put(r2, r3)     // Catch:{ Exception -> 0x0513, all -> 0x020f }
        L_0x04d9:
            r4.close()
            r2 = r8
            goto L_0x009c
        L_0x04df:
            int r7 = r6.length     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            r9 = 2
            if (r7 != r9) goto L_0x04ea
            r3 = 0
            r5 = r6[r3]     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            r3 = 1
            r3 = r6[r3]     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            goto L_0x04ca
        L_0x04ea:
            int r7 = r6.length     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            r9 = 3
            if (r7 < r9) goto L_0x04ca
            r2 = 0
            r5 = r6[r2]     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            r2 = 1
            r2 = r6[r2]     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            r3 = 2
            r3 = r6[r3]     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            r7.<init>(r3)     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            r3 = 3
        L_0x04fd:
            int r9 = r6.length     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            if (r3 < r9) goto L_0x0505
            java.lang.String r3 = r7.toString()     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            goto L_0x04ca
        L_0x0505:
            java.lang.String r9 = " "
            java.lang.StringBuilder r9 = r7.append(r9)     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            r10 = r6[r3]     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            r9.append(r10)     // Catch:{ Exception -> 0x00c6, all -> 0x020f }
            int r3 = r3 + 1
            goto L_0x04fd
        L_0x0513:
            r2 = move-exception
            r4.close()
            r2 = 0
            goto L_0x009c
        L_0x051a:
            r3 = move-exception
            r4 = r2
            r2 = r3
            goto L_0x0210
        L_0x051f:
            r3 = move-exception
            r4 = r2
            r2 = r3
            goto L_0x0210
        L_0x0524:
            r2 = move-exception
            r4 = r9
            goto L_0x0210
        L_0x0528:
            r3 = move-exception
            goto L_0x00c8
        L_0x052b:
            r2 = move-exception
            r2 = r9
            goto L_0x00c8
        L_0x052f:
            r6 = move-exception
            goto L_0x0082
        L_0x0532:
            r6 = move-exception
            goto L_0x007c
        L_0x0535:
            r2 = move-exception
            goto L_0x0026
        L_0x0538:
            r6 = r5
            goto L_0x037d
        L_0x053b:
            r7 = r5
            goto L_0x0369
        L_0x053e:
            r9 = r5
            goto L_0x0355
        L_0x0541:
            r10 = r5
            goto L_0x0341
        L_0x0544:
            r11 = r5
            goto L_0x032d
        L_0x0547:
            r12 = r5
            goto L_0x0319
        L_0x054a:
            r13 = r5
            goto L_0x0305
        L_0x054d:
            r2 = r5
            goto L_0x0156
        L_0x0550:
            r9 = r2
            goto L_0x0036
        L_0x0553:
            r23 = r2
            goto L_0x000d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixContactsSdk5.a(android.content.ContentResolver, android.net.Uri):org.json.JSONObject");
    }

    public void a(JSONObject jSONObject, Activity activity) throws Exception {
        this.c = AccountManager.get(activity).getAccountsByType("com.google")[0].name;
        ArrayList arrayList = new ArrayList();
        int size = arrayList.size();
        arrayList.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI).withValue("account_type", "com.google").withValue("account_name", this.c).build());
        Cursor managedQuery = activity.managedQuery(ContactsContract.Data.CONTENT_URI, new String[]{"data1", "group_sourceid"}, "mimetype='vnd.android.cursor.item/group_membership'", null, null);
        if (managedQuery.moveToFirst()) {
            arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/group_membership").withValue("group_sourceid", Integer.valueOf(managedQuery.getInt(1))).build());
        }
        HashMap<String, String> b2 = b(jSONObject);
        arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/name").withValue("data2", b2.get("firstName")).withValue("data5", b2.get("middleName")).withValue("data3", b2.get("lastName")).withValue("data4", b2.get("prefix")).withValue("data6", b2.get("suffix")).withValue("data1", b2.get("displayName")).build());
        arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/nickname").withValue("data1", b2.get("nickname")).withValue("data2", 1).build());
        arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/organization").withValue("data1", b2.get("organization")).withValue("data4", b2.get("jobTitle")).withValue("data5", b2.get("department")).build());
        if (b2.get("email") != null) {
            arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/email_v2").withValue("data1", b2.get("email")).withValue("data2", 3).build());
        }
        if (b2.get("note") != null) {
            arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/note").withValue("data1", b2.get("note")).build());
        }
        if (b2.get("website") != null) {
            arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/website").withValue("data1", b2.get("website")).withValue("data2", 1).build());
        }
        if (b2.get("birthday") != null) {
            Date parse = new SimpleDateFormat("yyyy-MM-dd'T'hh:mmZ").parse(b2.get("birthday"));
            arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/contact_event").withValue("data1", String.valueOf(parse.getMonth()) + "/" + parse.getDate() + "/" + parse.getYear()).withValue("data2", 3).build());
        }
        if (b2.get("im") != null) {
            arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/im").withValue("data1", b2.get("im")).withValue("data2", 1).withValue("data5", 5).build());
        }
        if (b2.get("image") != null) {
            arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/photo").withValue("data15", Base64.decode(b2.get("image"))).build());
        }
        JSONArray jSONArray = jSONObject.getJSONArray("addresses");
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject2 = jSONArray.getJSONObject(i);
            int i2 = 1;
            String str = null;
            String str2 = null;
            String str3 = null;
            String str4 = null;
            String str5 = null;
            String str6 = null;
            String str7 = null;
            if (jSONObject2.has("label") && jSONObject2.getString("label").equalsIgnoreCase("work")) {
                i2 = 2;
            }
            if (jSONObject2.has("street") && (str = jSONObject2.getString("street")) == null) {
                str = "";
            }
            if (jSONObject2.has("city") && (str2 = jSONObject2.getString("city")) == null) {
                str2 = "";
            }
            if (jSONObject2.has(UmengConstants.AtomKey_State) && (str3 = jSONObject2.getString(UmengConstants.AtomKey_State)) == null) {
                str3 = "";
            }
            if (jSONObject2.has("postalCode") && (str4 = jSONObject2.getString("postalCode")) == null) {
                str4 = "";
            }
            if (jSONObject2.has("country") && (str5 = jSONObject2.getString("country")) == null) {
                str5 = "";
            }
            if (jSONObject2.has("neighborhood") && (str6 = jSONObject2.getString("neighborhood")) == null) {
                str6 = "";
            }
            if (jSONObject2.has("poBox") && (str7 = jSONObject2.getString("poBox")) == null) {
                str7 = "";
            }
            arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/postal-address_v2").withValue("data2", Integer.valueOf(i2)).withValue("data4", str).withValue("data7", str2).withValue("data8", str3).withValue("data9", str4).withValue("data10", str5).withValue("data6", str6).withValue("data5", str7).build());
        }
        JSONArray jSONArray2 = jSONObject.getJSONArray("phoneNumbers");
        for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
            JSONObject jSONObject3 = jSONArray2.getJSONObject(i3);
            int i4 = 1;
            String str8 = null;
            if (jSONObject3.has("label") && jSONObject3.getString("label").equalsIgnoreCase("work")) {
                i4 = 3;
            }
            if (jSONObject3.has("number") && (str8 = jSONObject3.getString("number")) == null) {
                str8 = "";
            }
            arrayList.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference("raw_contact_id", size).withValue("mimetype", "vnd.android.cursor.item/phone_v2").withValue("data2", Integer.valueOf(i4)).withValue("data1", str8).build());
        }
        activity.getContentResolver().applyBatch("com.android.contacts", arrayList);
    }

    /* access modifiers changed from: package-private */
    public HashMap<String, String> b(JSONObject jSONObject) throws Exception {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        String str9;
        String str10;
        String str11;
        String str12;
        String str13;
        String str14;
        String str15;
        String str16 = null;
        HashMap<String, String> hashMap = new HashMap<>();
        StringBuilder sb = new StringBuilder();
        if (jSONObject.has("firstName")) {
            try {
                str = jSONObject.getString("firstName");
            } catch (Exception e) {
                str = null;
            }
            if (str.equals("")) {
                str = null;
            }
            if (str != null) {
                sb.append(str);
            }
        } else {
            str = null;
        }
        if (jSONObject.has("middleName")) {
            try {
                str2 = jSONObject.getString("middleName");
            } catch (Exception e2) {
                str2 = null;
            }
            if (str2.equals("")) {
                str2 = null;
            }
            if (str2 != null) {
                if (sb.length() != 0) {
                    sb.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                }
                sb.append(str2);
            }
        } else {
            str2 = null;
        }
        if (jSONObject.has("lastName")) {
            try {
                str3 = jSONObject.getString("lastName");
            } catch (Exception e3) {
                str3 = null;
            }
            if (str3.equals("")) {
                str3 = null;
            }
            if (str3 != null) {
                if (sb.length() != 0) {
                    sb.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
                }
                sb.append(str3);
            }
        } else {
            str3 = null;
        }
        String sb2 = sb.toString();
        if (jSONObject.has("prefix")) {
            try {
                str4 = jSONObject.getString("prefix");
            } catch (Exception e4) {
                str4 = null;
            }
            if (str4.equals("")) {
                str4 = null;
            }
        } else {
            str4 = null;
        }
        if (jSONObject.has("suffix")) {
            try {
                str5 = jSONObject.getString("suffix");
            } catch (Exception e5) {
                str5 = null;
            }
            if (str5.equals("")) {
                str5 = null;
            }
        } else {
            str5 = null;
        }
        if (jSONObject.has("nickname")) {
            try {
                str6 = jSONObject.getString("nickname");
            } catch (Exception e6) {
                str6 = null;
            }
            if (str6.equals("")) {
                str6 = null;
            }
        } else {
            str6 = null;
        }
        if (sb2 == null || sb2.equals("")) {
            throw new Exception("Adding contact failed: No name provided.");
        }
        hashMap.put("firstName", str);
        hashMap.put("lastName", str3);
        hashMap.put("middleName", str2);
        hashMap.put("prefix", str4);
        hashMap.put("suffix", str5);
        hashMap.put("displayName", sb2);
        hashMap.put("nickname", str6);
        if (jSONObject.has("organization")) {
            try {
                str7 = jSONObject.getString("organization");
            } catch (Exception e7) {
                str7 = null;
            }
            if (str7.equals("")) {
                str7 = null;
            }
        } else {
            str7 = null;
        }
        if (jSONObject.has("jobTitle")) {
            try {
                str8 = jSONObject.getString("jobTitle");
            } catch (Exception e8) {
                str8 = null;
            }
            if (str8.equals("")) {
                str8 = null;
            }
        } else {
            str8 = null;
        }
        if (jSONObject.has("department")) {
            try {
                str9 = jSONObject.getString("department");
            } catch (Exception e9) {
                str9 = null;
            }
            if (str9.equals("")) {
                str9 = null;
            }
        } else {
            str9 = null;
        }
        hashMap.put("organization", str7);
        hashMap.put("jobTitle", str8);
        hashMap.put("department", str9);
        if (jSONObject.has("email")) {
            try {
                str10 = jSONObject.getString("email");
            } catch (Exception e10) {
                str10 = null;
            }
            if (str10.equals("")) {
                str10 = null;
            }
        } else {
            str10 = null;
        }
        if (jSONObject.has("IM")) {
            try {
                str11 = jSONObject.getString("IM");
            } catch (Exception e11) {
                str11 = null;
            }
            if (str11.equals("")) {
                str11 = null;
            }
        } else {
            str11 = null;
        }
        if (jSONObject.has("website")) {
            try {
                str12 = jSONObject.getString("website");
            } catch (Exception e12) {
                str12 = null;
            }
            if (str12.equals("")) {
                str12 = null;
            }
        } else {
            str12 = null;
        }
        if (jSONObject.has("note")) {
            try {
                str13 = jSONObject.getString("note");
            } catch (Exception e13) {
                str13 = null;
            }
            if (str13.equals("")) {
                str13 = null;
            }
        } else {
            str13 = null;
        }
        if (jSONObject.has("birthday")) {
            try {
                str14 = jSONObject.getString("birthday");
            } catch (Exception e14) {
                str14 = null;
            }
            if (str14.equals("")) {
                str14 = null;
            }
        } else {
            str14 = null;
        }
        if (jSONObject.has("photo")) {
            try {
                str15 = jSONObject.getString("image");
            } catch (Exception e15) {
                str15 = null;
            }
            if (!str15.equals("")) {
                str16 = str15;
            }
        }
        hashMap.put("email", str10);
        hashMap.put("im", str11);
        hashMap.put("website", str12);
        hashMap.put("note", str13);
        hashMap.put("birthday", str14);
        hashMap.put("image", str16);
        return hashMap;
    }

    public static MobclixContactsEntityIterator newEntityIterator(Cursor cursor) {
        return new EntityIteratorImpl(cursor);
    }

    private static class EntityIteratorImpl extends MobclixContactsCursorEntityIterator {
        private static final String[] a = {"data1", "data2", "data3", "data4", "data5", "data6", "data7", "data8", "data9", "data10", "data11", "data12", "data13", "data14", "data15", "data_sync1", "data_sync2", "data_sync3", "data_sync4"};

        public EntityIteratorImpl(Cursor cursor) {
            super(cursor);
        }

        public Entity a(Cursor cursor) throws RemoteException {
            int columnIndexOrThrow = cursor.getColumnIndexOrThrow("_id");
            long j = cursor.getLong(columnIndexOrThrow);
            ContentValues contentValues = new ContentValues();
            MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, contentValues, "account_name");
            MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, contentValues, "account_type");
            MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, contentValues, "_id");
            MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, contentValues, "dirty");
            MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, contentValues, "version");
            MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, contentValues, "sourceid");
            MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, contentValues, "sync1");
            MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, contentValues, "sync2");
            MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, contentValues, "sync3");
            MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, contentValues, "sync4");
            MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, contentValues, "deleted");
            MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, contentValues, "contact_id");
            MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, contentValues, "starred");
            MobclixContactsSdk5.cursorIntToContentValuesIfPresent(cursor, contentValues, "is_restricted");
            Entity entity = new Entity(contentValues);
            while (j == cursor.getLong(columnIndexOrThrow)) {
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("_id", Long.valueOf(cursor.getLong(cursor.getColumnIndexOrThrow("data_id"))));
                MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, contentValues2, "res_package");
                MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, contentValues2, "mimetype");
                MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, contentValues2, "is_primary");
                MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, contentValues2, "is_super_primary");
                MobclixContactsSdk5.cursorLongToContentValuesIfPresent(cursor, contentValues2, "data_version");
                MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, contentValues2, "group_sourceid");
                MobclixContactsSdk5.cursorStringToContentValuesIfPresent(cursor, contentValues2, "data_version");
                for (String str : a) {
                    int columnIndexOrThrow2 = cursor.getColumnIndexOrThrow(str);
                    if (!cursor.isNull(columnIndexOrThrow2)) {
                        try {
                            contentValues2.put(str, cursor.getString(columnIndexOrThrow2));
                        } catch (SQLiteException e) {
                            contentValues2.put(str, cursor.getBlob(columnIndexOrThrow2));
                        }
                    }
                }
                entity.addSubValue(ContactsContract.Data.CONTENT_URI, contentValues2);
                if (!cursor.moveToNext()) {
                    break;
                }
            }
            return entity;
        }
    }

    public static void cursorStringToContentValuesIfPresent(Cursor cursor, ContentValues contentValues, String str) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow(str);
        if (!cursor.isNull(columnIndexOrThrow)) {
            contentValues.put(str, cursor.getString(columnIndexOrThrow));
        }
    }

    public static void cursorLongToContentValuesIfPresent(Cursor cursor, ContentValues contentValues, String str) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow(str);
        if (!cursor.isNull(columnIndexOrThrow)) {
            contentValues.put(str, Long.valueOf(cursor.getLong(columnIndexOrThrow)));
        }
    }

    public static void cursorIntToContentValuesIfPresent(Cursor cursor, ContentValues contentValues, String str) {
        int columnIndexOrThrow = cursor.getColumnIndexOrThrow(str);
        if (!cursor.isNull(columnIndexOrThrow)) {
            contentValues.put(str, Integer.valueOf(cursor.getInt(columnIndexOrThrow)));
        }
    }
}
