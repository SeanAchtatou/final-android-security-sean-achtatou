package com.mobclix.android.sdk;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.VideoView;
import java.lang.ref.SoftReference;

class MobclixWebView extends WebView implements View.OnTouchListener {
    MobclixCreative a = null;
    MobclixFullScreenAdView b = null;
    boolean c = false;
    boolean d = false;
    boolean e = false;
    MobclixJavascriptInterface f = null;
    Object g = null;
    VideoView h = null;
    boolean i = false;
    private String j = null;

    public MobclixWebView(MobclixCreative mobclixCreative) {
        super(mobclixCreative.a.getContext());
        this.a = mobclixCreative;
        setOnTouchListener(this);
    }

    public MobclixWebView(MobclixFullScreenAdView mobclixFullScreenAdView) {
        super(mobclixFullScreenAdView.a());
        this.b = mobclixFullScreenAdView;
        setOnTouchListener(this);
    }

    public Context a() {
        if (this.a != null) {
            return this.a.a.getContext();
        }
        if (this.b != null) {
            return this.b.a();
        }
        return getContext();
    }

    public Context b() {
        Context a2 = a();
        if (this.f == null || this.f.b == null) {
            return a2;
        }
        return this.f.b.getContext();
    }

    public void a(MobclixJavascriptInterface mobclixJavascriptInterface) {
        this.f = mobclixJavascriptInterface;
    }

    public MobclixJavascriptInterface c() {
        return this.f;
    }

    public void a(String str) {
        this.j = str;
    }

    public void d() {
        try {
            loadDataWithBaseURL(null, this.j, "text/html", "utf-8", null);
        } catch (Exception e2) {
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.d) {
            this.d = true;
            try {
                this.a.f();
            } catch (Exception e2) {
            }
            if (this.f != null) {
                this.f.g();
            }
        }
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        try {
            if (!this.e) {
                if (this.a != null) {
                    this.a.g();
                }
                if (this.b != null) {
                    this.b.g();
                }
            }
        } catch (Exception e2) {
        }
        this.e = true;
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(View view, Object obj) {
        Mobclix.getInstance().v = new SoftReference<>(this);
        Mobclix.getInstance().w = new SoftReference<>(view);
        Intent intent = new Intent();
        String packageName = getContext().getPackageName();
        intent.setClassName(packageName, MobclixBrowserActivity.class.getName()).putExtra(String.valueOf(packageName) + ".type", "html5Video");
        a().startActivity(intent);
        this.g = obj;
    }

    /* access modifiers changed from: package-private */
    public void b(View view, Object obj) {
        this.h = (VideoView) ((FrameLayout) view).getFocusedChild();
        if (Integer.parseInt(Build.VERSION.SDK) < 9) {
            this.h.stopPlayback();
            try {
                this.g.getClass().getMethod("onCustomViewHidden", new Class[0]).invoke(this.g, new Object[0]);
                this.g = null;
            } catch (Exception e2) {
            }
        } else {
            ((FrameLayout) view).removeView(this.h);
            this.h.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                    try {
                        MobclixWebView.this.h.stopPlayback();
                    } catch (Exception e) {
                    }
                    try {
                        MobclixWebView.this.g.getClass().getMethod("onCustomViewHidden", new Class[0]).invoke(MobclixWebView.this.g, new Object[0]);
                        MobclixWebView.this.g = null;
                    } catch (Exception e2) {
                        Log.v("MobclixWebView", e2.toString());
                    }
                    try {
                        ((ViewGroup) MobclixWebView.this.getParent()).removeView(MobclixWebView.this.h);
                        MobclixWebView.this.h = null;
                        return true;
                    } catch (Exception e3) {
                        return true;
                    }
                }
            });
            this.h.setVideoURI(Uri.parse("http://a.mobclix.com/fail"));
            this.h.setLayoutParams(new ViewGroup.LayoutParams(0, 0));
            ((WindowManager) a().getSystemService("window")).addView(this.h, new WindowManager.LayoutParams(1, 1));
            this.h.setMediaController(null);
            this.h.start();
            this.h.stopPlayback();
            this.i = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        scrollTo(0, 0);
        clearHistory();
        clearCache(false);
        this.f.b();
        this.c = false;
        this.d = false;
        this.e = false;
    }
}
