package com.mobclix.android.sdk;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import com.mobclix.android.sdk.Mobclix;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: MobclixConfig */
class FetchRemoteConfig extends AsyncTask<String, Integer, JSONArray> {
    private static String b = "MobclixConfig";
    private static boolean d = false;
    Mobclix a = Mobclix.getInstance();
    private MobclixInstrumentation c = MobclixInstrumentation.a();
    private String e;

    FetchRemoteConfig() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:137:0x04eb, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:?, code lost:
        android.util.Log.v(com.mobclix.android.sdk.FetchRemoteConfig.b, "ERROR: " + r1.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x051b, code lost:
        r1 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x0557, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x055d, code lost:
        r7.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x0584, code lost:
        r1 = r2;
        r2 = r4;
        r4 = r5;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x045f  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0475 A[LOOP:1: B:22:0x00dc->B:125:0x0475, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x0557 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:67:0x029c] */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x055d  */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x00e7 A[EDGE_INSN: B:194:0x00e7->B:25:0x00e7 ?: BREAK  , SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.json.JSONArray doInBackground(java.lang.String... r18) {
        /*
            r17 = this;
            boolean r1 = com.mobclix.android.sdk.FetchRemoteConfig.d
            if (r1 == 0) goto L_0x0006
            r1 = 0
        L_0x0005:
            return r1
        L_0x0006:
            r1 = 1
            com.mobclix.android.sdk.FetchRemoteConfig.d = r1
            r0 = r17
            com.mobclix.android.sdk.Mobclix r1 = r0.a
            r1.F()
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r1 = r0.c
            if (r1 != 0) goto L_0x001e
            com.mobclix.android.sdk.MobclixInstrumentation r1 = com.mobclix.android.sdk.MobclixInstrumentation.a()
            r0 = r17
            r0.c = r1
        L_0x001e:
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r1 = r0.c
            java.lang.String r2 = com.mobclix.android.sdk.MobclixInstrumentation.a
            java.lang.String r1 = r1.a(r2)
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r2 = r0.c
            java.lang.String r3 = "config"
            java.lang.String r1 = r2.b(r1, r3)
            r0 = r17
            com.mobclix.android.sdk.Mobclix r2 = r0.a     // Catch:{ Exception -> 0x0596 }
            android.content.Context r2 = r2.a()     // Catch:{ Exception -> 0x0596 }
            android.webkit.CookieSyncManager.createInstance(r2)     // Catch:{ Exception -> 0x0596 }
        L_0x003d:
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r2 = r0.c
            java.lang.String r3 = "update_session"
            java.lang.String r1 = r2.b(r1, r3)
            r0 = r17
            com.mobclix.android.sdk.Mobclix r2 = r0.a
            r2.D()
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r2 = r0.c
            java.lang.String r1 = r2.e(r1)
            r2 = 0
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r3 = r0.c
            java.lang.String r4 = "update"
            java.lang.String r1 = r3.b(r1, r4)
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r3 = r0.c
            java.lang.String r4 = "parse_cache"
            java.lang.String r1 = r3.b(r1, r4)
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r3 = r0.c
            java.lang.String r4 = "load_misc_settings"
            java.lang.String r1 = r3.b(r1, r4)
            java.lang.String r3 = "deviceId"
            boolean r3 = com.mobclix.android.sdk.Mobclix.j(r3)     // Catch:{ Exception -> 0x01c2 }
            if (r3 == 0) goto L_0x01ae
            java.lang.String r3 = "deviceId"
            java.lang.String r3 = com.mobclix.android.sdk.Mobclix.i(r3)     // Catch:{ Exception -> 0x01c2 }
            r0 = r17
            com.mobclix.android.sdk.Mobclix r4 = r0.a     // Catch:{ Exception -> 0x01c2 }
            java.lang.String r4 = r4.p     // Catch:{ Exception -> 0x01c2 }
            boolean r4 = r3.equals(r4)     // Catch:{ Exception -> 0x01c2 }
            if (r4 != 0) goto L_0x0095
            r0 = r17
            com.mobclix.android.sdk.Mobclix r4 = r0.a     // Catch:{ Exception -> 0x01c2 }
            r4.s = r3     // Catch:{ Exception -> 0x01c2 }
        L_0x0095:
            java.lang.String r3 = "passiveSessionTimeout"
            boolean r3 = com.mobclix.android.sdk.Mobclix.j(r3)
            if (r3 == 0) goto L_0x00ad
            r0 = r17
            com.mobclix.android.sdk.Mobclix r3 = r0.a
            java.lang.String r4 = "passiveSessionTimeout"
            java.lang.String r4 = com.mobclix.android.sdk.Mobclix.i(r4)
            int r4 = java.lang.Integer.parseInt(r4)
            r3.m = r4
        L_0x00ad:
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r3 = r0.c
            java.lang.String r1 = r3.e(r1)
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r3 = r0.c
            java.lang.String r4 = "load_adunits"
            java.lang.String r4 = r3.b(r1, r4)
            java.lang.String[] r5 = com.mobclix.android.sdk.Mobclix.a
            int r6 = r5.length
            r1 = 0
            r3 = r1
        L_0x00c4:
            if (r3 < r6) goto L_0x01c5
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r1 = r0.c
            java.lang.String r1 = r1.e(r4)
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r3 = r0.c
            java.lang.String r3 = r3.e(r1)
            r1 = 1
            r16 = r1
            r1 = r3
            r3 = r16
        L_0x00dc:
            r0 = r17
            com.mobclix.android.sdk.Mobclix r4 = r0.a
            int r4 = r4.n
            r5 = 1
            if (r4 != r5) goto L_0x022a
            r4 = r1
            r1 = r2
        L_0x00e7:
            r0 = r17
            com.mobclix.android.sdk.Mobclix r2 = r0.a
            int r2 = r2.n
            r3 = 1
            if (r2 == r3) goto L_0x0186
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r2 = r0.c
            java.lang.String r3 = "parse_cache"
            java.lang.String r2 = r2.b(r4, r3)
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r3 = r0.c
            java.lang.String r4 = "load_urls"
            java.lang.String r2 = r3.b(r2, r4)
            java.lang.String r3 = "ConfigServer"
            boolean r3 = com.mobclix.android.sdk.Mobclix.j(r3)
            if (r3 == 0) goto L_0x0118
            r0 = r17
            com.mobclix.android.sdk.Mobclix r3 = r0.a
            java.lang.String r4 = "ConfigServer"
            java.lang.String r4 = com.mobclix.android.sdk.Mobclix.i(r4)
            r3.g = r4
        L_0x0118:
            java.lang.String r3 = "AdServer"
            boolean r3 = com.mobclix.android.sdk.Mobclix.j(r3)
            if (r3 == 0) goto L_0x012c
            r0 = r17
            com.mobclix.android.sdk.Mobclix r3 = r0.a
            java.lang.String r4 = "AdServer"
            java.lang.String r4 = com.mobclix.android.sdk.Mobclix.i(r4)
            r3.f = r4
        L_0x012c:
            java.lang.String r3 = "AnalyticsServer"
            boolean r3 = com.mobclix.android.sdk.Mobclix.j(r3)
            if (r3 == 0) goto L_0x0140
            r0 = r17
            com.mobclix.android.sdk.Mobclix r3 = r0.a
            java.lang.String r4 = "AnalyticsServer"
            java.lang.String r4 = com.mobclix.android.sdk.Mobclix.i(r4)
            r3.h = r4
        L_0x0140:
            java.lang.String r3 = "VcServer"
            boolean r3 = com.mobclix.android.sdk.Mobclix.j(r3)
            if (r3 == 0) goto L_0x0154
            r0 = r17
            com.mobclix.android.sdk.Mobclix r3 = r0.a
            java.lang.String r4 = "VcServer"
            java.lang.String r4 = com.mobclix.android.sdk.Mobclix.i(r4)
            r3.i = r4
        L_0x0154:
            java.lang.String r3 = "FeedbackServer"
            boolean r3 = com.mobclix.android.sdk.Mobclix.j(r3)
            if (r3 == 0) goto L_0x0168
            r0 = r17
            com.mobclix.android.sdk.Mobclix r3 = r0.a
            java.lang.String r4 = "FeedbackServer"
            java.lang.String r4 = com.mobclix.android.sdk.Mobclix.i(r4)
            r3.j = r4
        L_0x0168:
            r0 = r17
            com.mobclix.android.sdk.Mobclix r3 = r0.a
            r4 = 1
            r3.n = r4
            r0 = r17
            com.mobclix.android.sdk.Mobclix r3 = r0.a
            r4 = 1
            r3.o = r4
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r3 = r0.c
            java.lang.String r2 = r3.e(r2)
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r3 = r0.c
            java.lang.String r4 = r3.e(r2)
        L_0x0186:
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r2 = r0.c
            java.lang.String r2 = r2.e(r4)
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r3 = r0.c
            java.lang.String r2 = r3.e(r2)
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r3 = r0.c
            r3.e(r2)
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r2 = r0.c
            java.lang.String r3 = com.mobclix.android.sdk.MobclixInstrumentation.a
            r2.d(r3)
            com.mobclix.android.sdk.Mobclix.sync()
            r2 = 0
            com.mobclix.android.sdk.FetchRemoteConfig.d = r2
            goto L_0x0005
        L_0x01ae:
            r0 = r17
            com.mobclix.android.sdk.Mobclix r3 = r0.a     // Catch:{ Exception -> 0x01c2 }
            r4 = 1
            r3.t = r4     // Catch:{ Exception -> 0x01c2 }
            java.lang.String r3 = "deviceId"
            r0 = r17
            com.mobclix.android.sdk.Mobclix r4 = r0.a     // Catch:{ Exception -> 0x01c2 }
            java.lang.String r4 = r4.p     // Catch:{ Exception -> 0x01c2 }
            com.mobclix.android.sdk.Mobclix.a(r3, r4)     // Catch:{ Exception -> 0x01c2 }
            goto L_0x0095
        L_0x01c2:
            r3 = move-exception
            goto L_0x0095
        L_0x01c5:
            r7 = r5[r3]
            java.util.HashMap<java.lang.String, com.mobclix.android.sdk.MobclixAdUnitSettings> r1 = com.mobclix.android.sdk.Mobclix.d
            com.mobclix.android.sdk.MobclixAdUnitSettings r8 = new com.mobclix.android.sdk.MobclixAdUnitSettings
            r8.<init>()
            r1.put(r7, r8)
            boolean r1 = com.mobclix.android.sdk.Mobclix.j(r7)
            if (r1 == 0) goto L_0x01e5
            java.lang.String r1 = com.mobclix.android.sdk.Mobclix.i(r7)
            org.json.JSONObject r8 = new org.json.JSONObject     // Catch:{ Exception -> 0x0225 }
            r8.<init>(r1)     // Catch:{ Exception -> 0x0225 }
            r0 = r17
            r0.a(r8)     // Catch:{ Exception -> 0x0225 }
        L_0x01e5:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r8 = java.lang.String.valueOf(r7)
            r1.<init>(r8)
            java.lang.String r8 = "CustomAdUrl"
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r1 = r1.toString()
            boolean r1 = com.mobclix.android.sdk.Mobclix.j(r1)
            if (r1 == 0) goto L_0x0220
            java.util.HashMap<java.lang.String, com.mobclix.android.sdk.MobclixAdUnitSettings> r1 = com.mobclix.android.sdk.Mobclix.d
            java.lang.Object r1 = r1.get(r7)
            com.mobclix.android.sdk.MobclixAdUnitSettings r1 = (com.mobclix.android.sdk.MobclixAdUnitSettings) r1
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            java.lang.String r7 = java.lang.String.valueOf(r7)
            r8.<init>(r7)
            java.lang.String r7 = "CustomAdUrl"
            java.lang.StringBuilder r7 = r8.append(r7)
            java.lang.String r7 = r7.toString()
            java.lang.String r7 = com.mobclix.android.sdk.Mobclix.i(r7)
            r1.a(r7)
        L_0x0220:
            int r1 = r3 + 1
            r3 = r1
            goto L_0x00c4
        L_0x0225:
            r1 = move-exception
            com.mobclix.android.sdk.Mobclix.k(r7)
            goto L_0x01e5
        L_0x022a:
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r0.c
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "attempt_"
            r5.<init>(r6)
            java.lang.StringBuilder r5 = r5.append(r3)
            java.lang.String r5 = r5.toString()
            java.lang.String r1 = r4.b(r1, r5)
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r0.c
            java.lang.String r5 = "build_request"
            java.lang.String r4 = r4.b(r1, r5)
            r1 = 1
            if (r3 != r1) goto L_0x047c
            r1 = 1
        L_0x024f:
            r0 = r17
            java.lang.String r1 = r0.a(r1)
            r0 = r17
            r0.e = r1
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r1 = r0.c
            java.lang.String r1 = r1.e(r4)
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r0.c
            java.lang.String r5 = "send_request"
            java.lang.String r4 = r4.b(r1, r5)
            java.lang.String r6 = ""
            r7 = 0
            r1 = 0
            com.mobclix.android.sdk.Mobclix$MobclixHttpClient r5 = new com.mobclix.android.sdk.Mobclix$MobclixHttpClient     // Catch:{ Exception -> 0x057b, all -> 0x0577 }
            r0 = r17
            java.lang.String r8 = r0.e     // Catch:{ Exception -> 0x057b, all -> 0x0577 }
            r5.<init>(r8)     // Catch:{ Exception -> 0x057b, all -> 0x0577 }
            org.apache.http.HttpResponse r5 = r5.a()     // Catch:{ Exception -> 0x057b, all -> 0x0577 }
            org.apache.http.HttpEntity r8 = r5.getEntity()     // Catch:{ Exception -> 0x057b, all -> 0x0577 }
            org.apache.http.StatusLine r5 = r5.getStatusLine()     // Catch:{ Exception -> 0x057b, all -> 0x0577 }
            int r5 = r5.getStatusCode()     // Catch:{ Exception -> 0x057b, all -> 0x0577 }
            r9 = 200(0xc8, float:2.8E-43)
            if (r5 != r9) goto L_0x052a
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x057b, all -> 0x0577 }
            java.io.InputStreamReader r9 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x057b, all -> 0x0577 }
            java.io.InputStream r10 = r8.getContent()     // Catch:{ Exception -> 0x057b, all -> 0x0577 }
            r9.<init>(r10)     // Catch:{ Exception -> 0x057b, all -> 0x0577 }
            r10 = 8000(0x1f40, float:1.121E-41)
            r5.<init>(r9, r10)     // Catch:{ Exception -> 0x057b, all -> 0x0577 }
            java.lang.String r1 = r5.readLine()     // Catch:{ Exception -> 0x0583, all -> 0x0557 }
        L_0x02a0:
            if (r1 != 0) goto L_0x047f
            r8.consumeContent()     // Catch:{ Exception -> 0x0583, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r1 = r0.c     // Catch:{ Exception -> 0x0583, all -> 0x0557 }
            java.lang.String r1 = r1.e(r4)     // Catch:{ Exception -> 0x0583, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r0.c     // Catch:{ Exception -> 0x0536, all -> 0x0557 }
            java.lang.String r8 = "handle_response"
            java.lang.String r1 = r4.b(r1, r8)     // Catch:{ Exception -> 0x0536, all -> 0x0557 }
            java.lang.String r4 = ""
            boolean r4 = r6.equals(r4)     // Catch:{ Exception -> 0x0536, all -> 0x0557 }
            if (r4 != 0) goto L_0x0599
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r0.c     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r8 = "decode_json"
            java.lang.String r1 = r4.b(r1, r8)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            org.json.JSONObject r8 = new org.json.JSONObject     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r8.<init>(r6)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r0.c     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r1 = r4.e(r1)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r0.c     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r9 = "save_json"
            java.lang.String r1 = r4.b(r1, r9)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r0.c     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r9 = "raw_config_json"
            java.lang.String r10 = com.mobclix.android.sdk.MobclixInstrumentation.a     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r4.a(r6, r9, r10)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r0.c     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r6 = "decoded_config_json"
            java.lang.String r9 = com.mobclix.android.sdk.MobclixInstrumentation.a     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r4.a(r8, r6, r9)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r0.c     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r1 = r4.e(r1)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r0.c     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r6 = "load_config"
            java.lang.String r1 = r4.b(r1, r6)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r4 = "urls"
            org.json.JSONObject r4 = r8.getJSONObject(r4)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.Mobclix r6 = r0.a     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r9 = "config"
            java.lang.String r9 = r4.getString(r9)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r6.g = r9     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.Mobclix r6 = r0.a     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r9 = "ads"
            java.lang.String r9 = r4.getString(r9)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r6.f = r9     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.Mobclix r6 = r0.a     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r9 = "analytics"
            java.lang.String r9 = r4.getString(r9)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r6.h = r9     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.Mobclix r6 = r0.a     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r9 = "vc"
            java.lang.String r9 = r4.getString(r9)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r6.i = r9     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.Mobclix r6 = r0.a     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r9 = "feedback"
            java.lang.String r9 = r4.getString(r9)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r6.j = r9     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.Mobclix r6 = r0.a     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r9 = "debug"
            java.lang.String r4 = r4.getString(r9)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r6.k = r4     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.Mobclix r4 = r0.a     // Catch:{ Exception -> 0x0593, all -> 0x0557 }
            java.lang.String r6 = "passive_session_timeout"
            int r6 = r8.getInt(r6)     // Catch:{ Exception -> 0x0593, all -> 0x0557 }
            int r6 = r6 * 1000
            r4.m = r6     // Catch:{ Exception -> 0x0593, all -> 0x0557 }
        L_0x0364:
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r0.c     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r6 = "set_default_values"
            java.lang.String r4 = r4.b(r1, r6)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.util.HashMap r6 = new java.util.HashMap     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            r6.<init>()     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r1 = "ConfigServer"
            r0 = r17
            com.mobclix.android.sdk.Mobclix r9 = r0.a     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r9 = r9.g     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            r6.put(r1, r9)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r1 = "AdServer"
            r0 = r17
            com.mobclix.android.sdk.Mobclix r9 = r0.a     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r9 = r9.f     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            r6.put(r1, r9)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r1 = "AnalyticsServer"
            r0 = r17
            com.mobclix.android.sdk.Mobclix r9 = r0.a     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r9 = r9.h     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            r6.put(r1, r9)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r1 = "VcServer"
            r0 = r17
            com.mobclix.android.sdk.Mobclix r9 = r0.a     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r9 = r9.i     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            r6.put(r1, r9)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r1 = "FeedbackServer"
            r0 = r17
            com.mobclix.android.sdk.Mobclix r9 = r0.a     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r9 = r9.j     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            r6.put(r1, r9)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r1 = "passiveSessionTimeout"
            r0 = r17
            com.mobclix.android.sdk.Mobclix r9 = r0.a     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            int r9 = r9.m     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r9 = java.lang.Integer.toString(r9)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            r6.put(r1, r9)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r1 = "ad_units"
            org.json.JSONArray r9 = r8.getJSONArray(r1)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            r1 = 0
        L_0x03c0:
            int r10 = r9.length()     // Catch:{ Exception -> 0x0590, all -> 0x0557 }
            if (r1 < r10) goto L_0x0496
        L_0x03c6:
            java.lang.String r1 = "debug_config"
            org.json.JSONObject r9 = r8.getJSONObject(r1)     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            r10.<init>()     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            java.lang.String[] r11 = com.mobclix.android.sdk.MobclixInstrumentation.c     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            int r12 = r11.length     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            r1 = 0
        L_0x03d5:
            if (r1 < r12) goto L_0x04b0
            com.mobclix.android.sdk.Mobclix.a(r10)     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            java.util.Iterator r10 = r9.keys()     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            java.util.HashMap r11 = new java.util.HashMap     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            r11.<init>()     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
        L_0x03e3:
            boolean r1 = r10.hasNext()     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            if (r1 != 0) goto L_0x04c8
            com.mobclix.android.sdk.Mobclix.a(r11)     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
        L_0x03ec:
            java.lang.String r1 = "app_alerts"
            org.json.JSONArray r2 = r8.getJSONArray(r1)     // Catch:{ Exception -> 0x058d, all -> 0x0557 }
        L_0x03f2:
            java.lang.String r1 = "native_urls"
            org.json.JSONArray r8 = r8.getJSONArray(r1)     // Catch:{ Exception -> 0x058a, all -> 0x0557 }
            r1 = 0
        L_0x03f9:
            int r9 = r8.length()     // Catch:{ Exception -> 0x058a, all -> 0x0557 }
            if (r1 < r9) goto L_0x0509
        L_0x03ff:
            r0 = r17
            com.mobclix.android.sdk.Mobclix r1 = r0.a     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r1 = r1.s     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            if (r1 == 0) goto L_0x0412
            java.lang.String r1 = "deviceId"
            r0 = r17
            com.mobclix.android.sdk.Mobclix r8 = r0.a     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r8 = r8.p     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            r6.put(r1, r8)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
        L_0x0412:
            java.lang.String r1 = "offlineSessions"
            java.lang.String r8 = "0"
            r6.put(r1, r8)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r1 = "totalSessionTime"
            java.lang.String r8 = "0"
            r6.put(r1, r8)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r1 = "totalIdleTime"
            java.lang.String r8 = "0"
            r6.put(r1, r8)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            com.mobclix.android.sdk.Mobclix.a(r6)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r1 = "MCReferralData"
            boolean r1 = com.mobclix.android.sdk.Mobclix.j(r1)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            if (r1 == 0) goto L_0x0437
            java.lang.String r1 = "MCReferralData"
            com.mobclix.android.sdk.Mobclix.k(r1)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
        L_0x0437:
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r1 = r0.c     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            java.lang.String r1 = r1.e(r4)     // Catch:{ Exception -> 0x051a, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r0.c     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            java.lang.String r1 = r4.e(r1)     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.Mobclix r4 = r0.a     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r6 = 0
            r4.o = r6     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r0 = r17
            com.mobclix.android.sdk.Mobclix r4 = r0.a     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r6 = 1
            r4.n = r6     // Catch:{ Exception -> 0x0588, all -> 0x0557 }
            r16 = r2
            r2 = r1
            r1 = r16
        L_0x045a:
            r5.close()     // Catch:{ Exception -> 0x056a }
        L_0x045d:
            if (r7 == 0) goto L_0x0462
            r7.disconnect()
        L_0x0462:
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r0.c
            java.lang.String r2 = r4.e(r2)
            r0 = r17
            com.mobclix.android.sdk.MobclixInstrumentation r4 = r0.c
            java.lang.String r4 = r4.e(r2)
            r2 = 1
            if (r3 > r2) goto L_0x00e7
            int r2 = r3 + 1
            r3 = r2
            r2 = r1
            r1 = r4
            goto L_0x00dc
        L_0x047c:
            r1 = 0
            goto L_0x024f
        L_0x047f:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0583, all -> 0x0557 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x0583, all -> 0x0557 }
            r9.<init>(r6)     // Catch:{ Exception -> 0x0583, all -> 0x0557 }
            java.lang.StringBuilder r1 = r9.append(r1)     // Catch:{ Exception -> 0x0583, all -> 0x0557 }
            java.lang.String r6 = r1.toString()     // Catch:{ Exception -> 0x0583, all -> 0x0557 }
            java.lang.String r1 = r5.readLine()     // Catch:{ Exception -> 0x0583, all -> 0x0557 }
            goto L_0x02a0
        L_0x0496:
            org.json.JSONObject r10 = r9.getJSONObject(r1)     // Catch:{ Exception -> 0x0590, all -> 0x0557 }
            java.lang.String r11 = "size"
            java.lang.String r11 = r10.getString(r11)     // Catch:{ Exception -> 0x0590, all -> 0x0557 }
            r0 = r17
            r0.a(r10)     // Catch:{ Exception -> 0x0590, all -> 0x0557 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x0590, all -> 0x0557 }
            r6.put(r11, r10)     // Catch:{ Exception -> 0x0590, all -> 0x0557 }
            int r1 = r1 + 1
            goto L_0x03c0
        L_0x04b0:
            r13 = r11[r1]     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            java.lang.String r15 = "debug_"
            r14.<init>(r15)     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            java.lang.StringBuilder r13 = r14.append(r13)     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            r10.add(r13)     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            int r1 = r1 + 1
            goto L_0x03d5
        L_0x04c8:
            java.lang.Object r1 = r10.next()     // Catch:{ Exception -> 0x04eb, all -> 0x0557 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x04eb, all -> 0x0557 }
            java.lang.String r12 = r9.getString(r1)     // Catch:{ Exception -> 0x04eb, all -> 0x0557 }
            java.util.HashMap<java.lang.String, java.lang.String> r13 = com.mobclix.android.sdk.Mobclix.e     // Catch:{ Exception -> 0x04eb, all -> 0x0557 }
            r13.put(r1, r12)     // Catch:{ Exception -> 0x04eb, all -> 0x0557 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04eb, all -> 0x0557 }
            java.lang.String r14 = "debug_"
            r13.<init>(r14)     // Catch:{ Exception -> 0x04eb, all -> 0x0557 }
            java.lang.StringBuilder r1 = r13.append(r1)     // Catch:{ Exception -> 0x04eb, all -> 0x0557 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x04eb, all -> 0x0557 }
            r11.put(r1, r12)     // Catch:{ Exception -> 0x04eb, all -> 0x0557 }
            goto L_0x03e3
        L_0x04eb:
            r1 = move-exception
            java.lang.String r12 = com.mobclix.android.sdk.FetchRemoteConfig.b     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            java.lang.String r14 = "ERROR: "
            r13.<init>(r14)     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            java.lang.StringBuilder r1 = r13.append(r1)     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            android.util.Log.v(r12, r1)     // Catch:{ Exception -> 0x0506, all -> 0x0557 }
            goto L_0x03e3
        L_0x0506:
            r1 = move-exception
            goto L_0x03ec
        L_0x0509:
            r0 = r17
            com.mobclix.android.sdk.Mobclix r9 = r0.a     // Catch:{ Exception -> 0x058a, all -> 0x0557 }
            java.util.List<java.lang.String> r9 = r9.l     // Catch:{ Exception -> 0x058a, all -> 0x0557 }
            java.lang.String r10 = r8.getString(r1)     // Catch:{ Exception -> 0x058a, all -> 0x0557 }
            r9.add(r10)     // Catch:{ Exception -> 0x058a, all -> 0x0557 }
            int r1 = r1 + 1
            goto L_0x03f9
        L_0x051a:
            r1 = move-exception
            r1 = r4
        L_0x051c:
            r0 = r17
            com.mobclix.android.sdk.Mobclix r4 = r0.a     // Catch:{ Exception -> 0x0536, all -> 0x0557 }
            r6 = -1
            r4.n = r6     // Catch:{ Exception -> 0x0536, all -> 0x0557 }
            r16 = r2
            r2 = r1
            r1 = r16
            goto L_0x045a
        L_0x052a:
            r0 = r17
            com.mobclix.android.sdk.Mobclix r5 = r0.a     // Catch:{ Exception -> 0x057b, all -> 0x0577 }
            r6 = -1
            r5.n = r6     // Catch:{ Exception -> 0x057b, all -> 0x0577 }
            r5 = r1
            r1 = r2
            r2 = r4
            goto L_0x045a
        L_0x0536:
            r4 = move-exception
            r4 = r5
            r16 = r2
            r2 = r1
            r1 = r16
        L_0x053d:
            r0 = r17
            com.mobclix.android.sdk.Mobclix r5 = r0.a     // Catch:{ all -> 0x0574 }
            r6 = -1
            r5.n = r6     // Catch:{ all -> 0x0574 }
            r4.close()     // Catch:{ Exception -> 0x054e }
        L_0x0547:
            if (r7 == 0) goto L_0x0462
            r7.disconnect()
            goto L_0x0462
        L_0x054e:
            r4 = move-exception
            r0 = r17
            com.mobclix.android.sdk.Mobclix r4 = r0.a
            r5 = -1
            r4.n = r5
            goto L_0x0547
        L_0x0557:
            r1 = move-exception
        L_0x0558:
            r5.close()     // Catch:{ Exception -> 0x0561 }
        L_0x055b:
            if (r7 == 0) goto L_0x0560
            r7.disconnect()
        L_0x0560:
            throw r1
        L_0x0561:
            r2 = move-exception
            r0 = r17
            com.mobclix.android.sdk.Mobclix r2 = r0.a
            r3 = -1
            r2.n = r3
            goto L_0x055b
        L_0x056a:
            r4 = move-exception
            r0 = r17
            com.mobclix.android.sdk.Mobclix r4 = r0.a
            r5 = -1
            r4.n = r5
            goto L_0x045d
        L_0x0574:
            r1 = move-exception
            r5 = r4
            goto L_0x0558
        L_0x0577:
            r2 = move-exception
            r5 = r1
            r1 = r2
            goto L_0x0558
        L_0x057b:
            r5 = move-exception
            r16 = r1
            r1 = r2
            r2 = r4
            r4 = r16
            goto L_0x053d
        L_0x0583:
            r1 = move-exception
            r1 = r2
            r2 = r4
            r4 = r5
            goto L_0x053d
        L_0x0588:
            r4 = move-exception
            goto L_0x051c
        L_0x058a:
            r1 = move-exception
            goto L_0x03ff
        L_0x058d:
            r1 = move-exception
            goto L_0x03f2
        L_0x0590:
            r1 = move-exception
            goto L_0x03c6
        L_0x0593:
            r4 = move-exception
            goto L_0x0364
        L_0x0596:
            r2 = move-exception
            goto L_0x003d
        L_0x0599:
            r16 = r2
            r2 = r1
            r1 = r16
            goto L_0x045a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.FetchRemoteConfig.doInBackground(java.lang.String[]):org.json.JSONArray");
    }

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject) throws JSONException {
        String string = jSONObject.getString("size");
        MobclixAdUnitSettings mobclixAdUnitSettings = Mobclix.d.get(string);
        mobclixAdUnitSettings.a(jSONObject.getBoolean("enabled"));
        if (jSONObject.getLong("refresh") == -1) {
            mobclixAdUnitSettings.a(-1);
        } else {
            mobclixAdUnitSettings.a(jSONObject.getLong("refresh") * 1000);
        }
        mobclixAdUnitSettings.b(jSONObject.getBoolean("autoplay"));
        if (jSONObject.getLong("autoplay_interval") == -1) {
            mobclixAdUnitSettings.b(-1);
        } else {
            mobclixAdUnitSettings.b(jSONObject.getLong("autoplay_interval") * 1000);
        }
        mobclixAdUnitSettings.c(jSONObject.getBoolean("rm_require_user"));
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("open_allocs");
            for (String str : Mobclix.b) {
                JSONObject jSONObject3 = jSONObject2.getJSONObject(str);
                if (jSONObject3 != null) {
                    Iterator<String> keys = jSONObject3.keys();
                    HashMap hashMap = new HashMap();
                    while (keys.hasNext()) {
                        try {
                            String next = keys.next();
                            String string2 = jSONObject3.getString(next);
                            if (string2 != null && !string2.equals("")) {
                                hashMap.put(next, string2);
                            }
                        } catch (Exception e2) {
                        }
                    }
                    mobclixAdUnitSettings.a.put(str, hashMap);
                } else {
                    mobclixAdUnitSettings.a.put(str, null);
                }
            }
        } catch (Exception e3) {
        }
        try {
            String string3 = jSONObject.getString("customAdUrl");
            if (string3.equals(mobclixAdUnitSettings.f())) {
                mobclixAdUnitSettings.a("");
                return;
            }
            Mobclix.k(String.valueOf(string) + "CustomAdUrl");
            mobclixAdUnitSettings.a(string3);
        } catch (Exception e4) {
            mobclixAdUnitSettings.a("");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(JSONArray jSONArray) {
        long j;
        int i;
        String str;
        if (jSONArray != null) {
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                try {
                    JSONObject jSONObject = jSONArray.getJSONObject(i2);
                    String string = jSONObject.getString("id");
                    if (string != null && !string.equals("")) {
                        int i3 = 0;
                        String i4 = Mobclix.i("MCAppAlert" + string);
                        if (!i4.equals("")) {
                            String[] split = i4.split(",");
                            try {
                                i3 = Integer.parseInt(split[0]);
                            } catch (Exception e2) {
                            }
                            try {
                                j = Long.parseLong(split[1]);
                                i = i3;
                            } catch (Exception e3) {
                                j = 0;
                                i = i3;
                            }
                        } else {
                            j = 0;
                            i = 0;
                        }
                        String string2 = jSONObject.getString("title");
                        if (string2 != null && !string2.equals("") && !string2.equals("null")) {
                            String str2 = null;
                            try {
                                str2 = jSONObject.getString("message");
                                if (str2 == null || str2.equals("") || str2.equals("null")) {
                                    str2 = null;
                                }
                            } catch (Exception e4) {
                            }
                            int i5 = 0;
                            try {
                                i5 = jSONObject.getInt("max_displays");
                            } catch (Exception e5) {
                            }
                            if (i5 == 0 || i < i5) {
                                long j2 = 0;
                                try {
                                    j2 = ((long) jSONObject.getInt("display_interval")) * 1000;
                                } catch (Exception e6) {
                                }
                                if (j2 != 0) {
                                    if (j2 + j >= System.currentTimeMillis()) {
                                    }
                                }
                                JSONArray jSONArray2 = jSONObject.getJSONArray("target_versions");
                                boolean z = false;
                                for (int i6 = 0; i6 < jSONArray2.length(); i6++) {
                                    try {
                                        String str3 = jSONArray2.getString(i6).split("\\*")[0];
                                        if (this.a.h().substring(0, str3.length()).equals(str3)) {
                                            z = true;
                                        }
                                    } catch (Exception e7) {
                                    }
                                }
                                if (z) {
                                    String str4 = null;
                                    try {
                                        str4 = jSONObject.getString("action_button");
                                        if (str4 == null || str4.equals("") || str4.equals("null")) {
                                            str4 = null;
                                        }
                                    } catch (Exception e8) {
                                    }
                                    try {
                                        String string3 = jSONObject.getString("action_url");
                                        if (string3 == null || string3.equals("") || string3.equals("null")) {
                                            str = null;
                                        } else {
                                            str = string3;
                                        }
                                    } catch (Exception e9) {
                                        str = null;
                                    }
                                    if (str == null || str4 != null) {
                                        String str5 = null;
                                        try {
                                            str5 = jSONObject.getString("dismiss_button");
                                            if (str5 == null || str5.equals("") || str5.equals("null")) {
                                                str5 = null;
                                            }
                                        } catch (Exception e10) {
                                        }
                                        if (str != null || str5 != null) {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(this.a.a());
                                            builder.setTitle(string2);
                                            builder.setCancelable(false);
                                            if (str2 != null) {
                                                builder.setMessage(str2);
                                            }
                                            if (str != null) {
                                                builder.setPositiveButton(str4, new Mobclix.ObjectOnClickListener(str) {
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        FetchRemoteConfig.this.a.a().startActivity(new Intent("android.intent.action.VIEW", Uri.parse((String) this.b)));
                                                    }
                                                });
                                            }
                                            if (str5 != null) {
                                                builder.setNegativeButton(str5, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        dialogInterface.cancel();
                                                    }
                                                });
                                            }
                                            builder.create().show();
                                            Mobclix.a("MCAppAlert" + string, String.valueOf(Integer.toString(i + 1)) + "," + Long.toString(System.currentTimeMillis()));
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e11) {
                }
            }
        }
    }

    private String a(boolean z) {
        String str = this.a.g;
        StringBuffer stringBuffer = new StringBuffer();
        try {
            if (Mobclix.j("ConfigServer") && z) {
                str = Mobclix.i("ConfigServer");
            }
            stringBuffer.append(str);
            stringBuffer.append("?p=android");
            stringBuffer.append("&rt=").append(URLEncoder.encode(this.a.e(), "UTF-8"));
            stringBuffer.append("&rtv=").append(URLEncoder.encode(this.a.f(), "UTF-8"));
            stringBuffer.append("&a=").append(URLEncoder.encode(this.a.b(), "UTF-8"));
            stringBuffer.append("&m=").append(URLEncoder.encode(this.a.u()));
            stringBuffer.append("&v=").append(URLEncoder.encode(this.a.h(), "UTF-8"));
            stringBuffer.append("&d=").append(URLEncoder.encode(this.a.i(), "UTF-8"));
            stringBuffer.append("&dm=").append(URLEncoder.encode(this.a.k(), "UTF-8"));
            stringBuffer.append("&dv=").append(URLEncoder.encode(this.a.g(), "UTF-8"));
            stringBuffer.append("&hwdm=").append(URLEncoder.encode(this.a.l(), "UTF-8"));
            stringBuffer.append("&g=").append(URLEncoder.encode(this.a.m(), "UTF-8"));
            if (!this.a.p().equals("null")) {
                stringBuffer.append("&ll=").append(URLEncoder.encode(this.a.p(), "UTF-8"));
            }
            if (Mobclix.j("offlineSessions")) {
                try {
                    stringBuffer.append("&off=").append(Mobclix.i("offlineSessions"));
                } catch (Exception e2) {
                }
            }
            if (Mobclix.j("totalSessionTime")) {
                try {
                    stringBuffer.append("&st=").append(Mobclix.i("totalSessionTime"));
                } catch (Exception e3) {
                }
            }
            stringBuffer.append("&it=0");
            if (this.a.s != null) {
                stringBuffer.append("&pd=").append(URLEncoder.encode(this.a.s, "UTF-8"));
            }
            stringBuffer.append("&mcc=").append(URLEncoder.encode(this.a.s(), "UTF-8"));
            stringBuffer.append("&mnc=").append(URLEncoder.encode(this.a.t(), "UTF-8"));
            if (this.a.t) {
                stringBuffer.append("&new=true");
            }
            try {
                if (Mobclix.j("MCReferralData")) {
                    String i = Mobclix.i("MCReferralData");
                    if (!i.equals("")) {
                        stringBuffer.append("&r=").append(i);
                    }
                }
            } catch (Exception e4) {
            }
            return stringBuffer.toString();
        } catch (Exception e5) {
            return "";
        }
    }
}
