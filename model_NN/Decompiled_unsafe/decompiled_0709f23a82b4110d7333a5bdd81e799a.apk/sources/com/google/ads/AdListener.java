package com.google.ads;

import com.google.ads.AdRequest;

public interface AdListener {
    void a(Ad ad);

    void a(Ad ad, AdRequest.ErrorCode errorCode);

    void b(Ad ad);

    void c(Ad ad);

    void d(Ad ad);
}
