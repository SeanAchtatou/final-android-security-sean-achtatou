package com.google.ads;

import android.content.Context;
import android.location.Location;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdRequest {
    public static final String LOGTAG = "Ads";
    public static final String TEST_EMULATOR = AdUtil.a("emulator");
    public static final String VERSION = "4.1.1";
    private Gender a = null;
    private String b = null;
    private Set<String> c = null;
    private Map<String, Object> d = null;
    private Location e = null;
    private boolean f = false;
    private boolean g = false;
    private Set<String> h = null;

    public enum ErrorCode {
        INVALID_REQUEST("Invalid Google Ad request."),
        NO_FILL("Ad request successful, but no ad returned due to lack of ad inventory."),
        NETWORK_ERROR("A network error occurred."),
        INTERNAL_ERROR("There was an internal error.");
        
        private String a;

        private ErrorCode(String str) {
            this.a = str;
        }

        public final String toString() {
            return this.a;
        }
    }

    public enum Gender {
        MALE(AdActivity.TYPE_PARAM),
        FEMALE("f");
        
        private String a;

        private Gender(String str) {
            this.a = str;
        }

        public final String toString() {
            return this.a;
        }
    }

    public Map<String, Object> a(Context context) {
        HashMap hashMap = new HashMap();
        if (this.c != null) {
            hashMap.put("kw", this.c);
        }
        if (this.a != null) {
            hashMap.put("cust_gender", this.a.toString());
        }
        if (this.b != null) {
            hashMap.put("cust_age", this.b);
        }
        if (this.e != null) {
            hashMap.put("uule", AdUtil.a(this.e));
        }
        if (this.f) {
            hashMap.put("testing", 1);
        }
        if (b(context)) {
            hashMap.put("adtest", "on");
        } else if (!this.g) {
            a.c("To get test ads on this device, call adRequest.addTestDevice(" + (AdUtil.c() ? "AdRequest.TEST_EMULATOR" : "\"" + AdUtil.a(context) + "\"") + ");");
            this.g = true;
        }
        if (this.d != null) {
            hashMap.put("extras", this.d);
        }
        return hashMap;
    }

    public void a(String str) {
        if (this.c == null) {
            this.c = new HashSet();
        }
        this.c.add(str);
    }

    public boolean b(Context context) {
        String a2;
        return (this.h == null || (a2 = AdUtil.a(context)) == null || !this.h.contains(a2)) ? false : true;
    }
}
