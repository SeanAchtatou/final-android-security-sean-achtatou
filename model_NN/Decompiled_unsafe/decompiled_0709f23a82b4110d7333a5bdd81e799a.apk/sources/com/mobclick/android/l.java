package com.mobclick.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.Deflater;
import javax.microedition.khronos.opengles.GL10;

public class l {
    static String a = "utf-8";
    public static int b;

    public static int a(Context context, String str, String str2) {
        try {
            Field field = Class.forName(String.valueOf(context.getPackageName()) + ".R$" + str).getField(str2);
            return Integer.parseInt(field.get(field.getName()).toString());
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static int a(Date date, Date date2) {
        if (!date.after(date2)) {
            Date date3 = date2;
            date2 = date;
            date = date3;
        }
        return (int) ((date.getTime() - date2.getTime()) / 1000);
    }

    public static String a() {
        String str;
        String str2 = null;
        try {
            FileReader fileReader = new FileReader("/proc/cpuinfo");
            try {
                BufferedReader bufferedReader = new BufferedReader(fileReader, 1024);
                str2 = bufferedReader.readLine();
                bufferedReader.close();
                fileReader.close();
                str = str2;
            } catch (IOException e) {
                Log.e(UmengConstants.LOG_TAG, "Could not read from file /proc/cpuinfo", e);
                str = str2;
            }
        } catch (FileNotFoundException e2) {
            Log.e(UmengConstants.LOG_TAG, "Could not open file /proc/cpuinfo", e2);
            str = str2;
        }
        if (str != null) {
            str = str.substring(str.indexOf(58) + 1);
        }
        return str.trim();
    }

    public static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                stringBuffer.append(Integer.toHexString(b2 & 255));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String a(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }

    public static void a(Context context) {
        Toast.makeText(context, context.getString(a(context, "string", "UMToast_IsUpdating")), 0).show();
    }

    public static void a(Context context, Date date) {
        SharedPreferences.Editor edit = context.getSharedPreferences("exchange_last_request_time", 0).edit();
        edit.putString("last_request_time", a(date));
        edit.commit();
    }

    public static boolean a(Context context, String str) {
        return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
    }

    public static String[] a(GL10 gl10) {
        try {
            return new String[]{gl10.glGetString(7936), gl10.glGetString(7937)};
        } catch (Exception e) {
            Log.e(UmengConstants.LOG_TAG, "Could not read gpu infor:", e);
            return new String[0];
        }
    }

    public static String b() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static String b(Context context) {
        String str;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                str = applicationInfo.metaData.getString("UMENG_APPKEY");
                if (str == null) {
                    Log.i(UmengConstants.LOG_TAG, "Could not read UMENG_APPKEY meta-data from AndroidManifest.xml.");
                    str = null;
                }
                return str.trim();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        str = null;
        return str.trim();
    }

    public static byte[] b(String str) {
        b = 0;
        Deflater deflater = new Deflater();
        deflater.setInput(str.getBytes(a));
        deflater.finish();
        byte[] bArr = new byte[8192];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (!deflater.finished()) {
            int deflate = deflater.deflate(bArr);
            b += deflate;
            byteArrayOutputStream.write(bArr, 0, deflate);
        }
        deflater.end();
        return byteArrayOutputStream.toByteArray();
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String c(android.content.Context r3) {
        /*
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r3.getSystemService(r0)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            if (r0 != 0) goto L_0x0011
            java.lang.String r1 = "MobclickAgent"
            java.lang.String r2 = "No IMEI."
            android.util.Log.w(r1, r2)
        L_0x0011:
            java.lang.String r1 = ""
            java.lang.String r2 = "android.permission.READ_PHONE_STATE"
            boolean r2 = a(r3, r2)     // Catch:{ Exception -> 0x003b }
            if (r2 == 0) goto L_0x003f
            java.lang.String r0 = r0.getDeviceId()     // Catch:{ Exception -> 0x003b }
        L_0x001f:
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x0041
            java.lang.String r0 = "MobclickAgent"
            java.lang.String r1 = "No IMEI."
            android.util.Log.w(r0, r1)
            java.lang.String r0 = i(r3)
            if (r0 != 0) goto L_0x0041
            java.lang.String r0 = "MobclickAgent"
            java.lang.String r1 = "Failed to take mac as IMEI."
            android.util.Log.w(r0, r1)
            r0 = 0
        L_0x003a:
            return r0
        L_0x003b:
            r0 = move-exception
            r0.printStackTrace()
        L_0x003f:
            r0 = r1
            goto L_0x001f
        L_0x0041:
            java.lang.String r0 = a(r0)
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclick.android.l.c(android.content.Context):java.lang.String");
    }

    public static Date c(String str) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
        } catch (Exception e) {
            return null;
        }
    }

    public static Date d(Context context) {
        return c(context.getSharedPreferences("exchange_last_request_time", 0).getString("last_request_time", "1900-01-01 00:00:00"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException} */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0192, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0193, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.json.JSONObject e(android.content.Context r6) {
        /*
            r2 = 0
            org.json.JSONObject r3 = new org.json.JSONObject
            r3.<init>()
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r6.getSystemService(r0)     // Catch:{ Exception -> 0x0192 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = c(r6)     // Catch:{ Exception -> 0x0192 }
            if (r1 == 0) goto L_0x001c
            java.lang.String r4 = ""
            boolean r4 = r1.equals(r4)     // Catch:{ Exception -> 0x0192 }
            if (r4 == 0) goto L_0x0025
        L_0x001c:
            java.lang.String r0 = "MobclickAgent"
            java.lang.String r1 = "No device id"
            android.util.Log.e(r0, r1)     // Catch:{ Exception -> 0x0192 }
            r0 = r2
        L_0x0024:
            return r0
        L_0x0025:
            java.lang.String r4 = "idmd5"
            r3.put(r4, r1)     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = "device_model"
            java.lang.String r4 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0192 }
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = b(r6)     // Catch:{ Exception -> 0x0192 }
            if (r1 != 0) goto L_0x0040
            java.lang.String r0 = "MobclickAgent"
            java.lang.String r1 = "No appkey"
            android.util.Log.e(r0, r1)     // Catch:{ Exception -> 0x0192 }
            r0 = r2
            goto L_0x0024
        L_0x0040:
            java.lang.String r4 = "appkey"
            r3.put(r4, r1)     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = g(r6)     // Catch:{ Exception -> 0x0192 }
            java.lang.String r4 = "channel"
            r3.put(r4, r1)     // Catch:{ Exception -> 0x0192 }
            android.content.pm.PackageManager r1 = r6.getPackageManager()     // Catch:{ NameNotFoundException -> 0x017e }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ NameNotFoundException -> 0x017e }
            r5 = 0
            android.content.pm.PackageInfo r1 = r1.getPackageInfo(r4, r5)     // Catch:{ NameNotFoundException -> 0x017e }
            java.lang.String r4 = r1.versionName     // Catch:{ NameNotFoundException -> 0x017e }
            int r1 = r1.versionCode     // Catch:{ NameNotFoundException -> 0x017e }
            java.lang.String r5 = "app_version"
            r3.put(r5, r4)     // Catch:{ NameNotFoundException -> 0x017e }
            java.lang.String r4 = "version_code"
            r3.put(r4, r1)     // Catch:{ NameNotFoundException -> 0x017e }
        L_0x0069:
            java.lang.String r1 = "sdk_type"
            java.lang.String r4 = "Android"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = "sdk_version"
            java.lang.String r4 = "3.0"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = "os"
            java.lang.String r4 = "Android"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = "os_version"
            java.lang.String r4 = android.os.Build.VERSION.RELEASE     // Catch:{ Exception -> 0x0192 }
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0192 }
            android.content.res.Configuration r1 = new android.content.res.Configuration     // Catch:{ Exception -> 0x0192 }
            r1.<init>()     // Catch:{ Exception -> 0x0192 }
            android.content.ContentResolver r4 = r6.getContentResolver()     // Catch:{ Exception -> 0x0192 }
            android.provider.Settings.System.getConfiguration(r4, r1)     // Catch:{ Exception -> 0x0192 }
            if (r1 == 0) goto L_0x01ab
            java.util.Locale r4 = r1.locale     // Catch:{ Exception -> 0x0192 }
            if (r4 == 0) goto L_0x01ab
            java.lang.String r4 = "country"
            java.util.Locale r5 = r1.locale     // Catch:{ Exception -> 0x0192 }
            java.lang.String r5 = r5.getCountry()     // Catch:{ Exception -> 0x0192 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0192 }
            java.lang.String r4 = "language"
            java.util.Locale r5 = r1.locale     // Catch:{ Exception -> 0x0192 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0192 }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x0192 }
            java.util.Locale r1 = r1.locale     // Catch:{ Exception -> 0x0192 }
            java.util.Calendar r1 = java.util.Calendar.getInstance(r1)     // Catch:{ Exception -> 0x0192 }
            if (r1 == 0) goto L_0x01a2
            java.util.TimeZone r1 = r1.getTimeZone()     // Catch:{ Exception -> 0x0192 }
            if (r1 == 0) goto L_0x0199
            java.lang.String r4 = "timezone"
            int r1 = r1.getRawOffset()     // Catch:{ Exception -> 0x0192 }
            r5 = 3600000(0x36ee80, float:5.044674E-39)
            int r1 = r1 / r5
            r3.put(r4, r1)     // Catch:{ Exception -> 0x0192 }
        L_0x00c8:
            android.util.DisplayMetrics r4 = new android.util.DisplayMetrics     // Catch:{ Exception -> 0x01c2 }
            r4.<init>()     // Catch:{ Exception -> 0x01c2 }
            java.lang.String r1 = "window"
            java.lang.Object r1 = r6.getSystemService(r1)     // Catch:{ Exception -> 0x01c2 }
            android.view.WindowManager r1 = (android.view.WindowManager) r1     // Catch:{ Exception -> 0x01c2 }
            android.view.Display r1 = r1.getDefaultDisplay()     // Catch:{ Exception -> 0x01c2 }
            r1.getMetrics(r4)     // Catch:{ Exception -> 0x01c2 }
            int r1 = r4.widthPixels     // Catch:{ Exception -> 0x01c2 }
            int r4 = r4.heightPixels     // Catch:{ Exception -> 0x01c2 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01c2 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x01c2 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x01c2 }
            r5.<init>(r4)     // Catch:{ Exception -> 0x01c2 }
            java.lang.String r4 = "*"
            java.lang.StringBuilder r4 = r5.append(r4)     // Catch:{ Exception -> 0x01c2 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x01c2 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x01c2 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01c2 }
            java.lang.String r4 = "resolution"
            r3.put(r4, r1)     // Catch:{ Exception -> 0x01c2 }
        L_0x0104:
            java.lang.String[] r1 = f(r6)     // Catch:{ Exception -> 0x01cf }
            java.lang.String r4 = "access"
            r5 = 0
            r5 = r1[r5]     // Catch:{ Exception -> 0x01cf }
            r3.put(r4, r5)     // Catch:{ Exception -> 0x01cf }
            r4 = 0
            r4 = r1[r4]     // Catch:{ Exception -> 0x01cf }
            java.lang.String r5 = "2G/3G"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x01cf }
            if (r4 == 0) goto L_0x0123
            java.lang.String r4 = "access_subtype"
            r5 = 1
            r1 = r1[r5]     // Catch:{ Exception -> 0x01cf }
            r3.put(r4, r1)     // Catch:{ Exception -> 0x01cf }
        L_0x0123:
            java.lang.String r1 = "carrier"
            java.lang.String r0 = r0.getNetworkOperatorName()     // Catch:{ Exception -> 0x01dc }
            r3.put(r1, r0)     // Catch:{ Exception -> 0x01dc }
        L_0x012c:
            boolean r0 = com.mobclick.android.UmengConstants.LOCATION_OPEN     // Catch:{ Exception -> 0x0192 }
            if (r0 == 0) goto L_0x0150
            android.location.Location r0 = j(r6)     // Catch:{ Exception -> 0x0192 }
            if (r0 == 0) goto L_0x01e9
            java.lang.String r1 = "lat"
            double r4 = r0.getLatitude()     // Catch:{ Exception -> 0x0192 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x0192 }
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = "lng"
            double r4 = r0.getLongitude()     // Catch:{ Exception -> 0x0192 }
            java.lang.String r0 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x0192 }
            r3.put(r1, r0)     // Catch:{ Exception -> 0x0192 }
        L_0x0150:
            java.lang.String r0 = a()     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = "cpu"
            r3.put(r1, r0)     // Catch:{ Exception -> 0x0192 }
            java.lang.String r0 = com.mobclick.android.MobclickAgent.GPU_VENDER     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x0192 }
            if (r0 != 0) goto L_0x016a
            java.lang.String r0 = "gpu_vender"
            java.lang.String r1 = com.mobclick.android.MobclickAgent.GPU_VENDER     // Catch:{ Exception -> 0x0192 }
            r3.put(r0, r1)     // Catch:{ Exception -> 0x0192 }
        L_0x016a:
            java.lang.String r0 = com.mobclick.android.MobclickAgent.GPU_RENDERER     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = ""
            boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x0192 }
            if (r0 != 0) goto L_0x017b
            java.lang.String r0 = "gpu_renderer"
            java.lang.String r1 = com.mobclick.android.MobclickAgent.GPU_RENDERER     // Catch:{ Exception -> 0x0192 }
            r3.put(r0, r1)     // Catch:{ Exception -> 0x0192 }
        L_0x017b:
            r0 = r3
            goto L_0x0024
        L_0x017e:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = "app_version"
            java.lang.String r4 = "unknown"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = "version_code"
            java.lang.String r4 = "unknown"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0192 }
            goto L_0x0069
        L_0x0192:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x0024
        L_0x0199:
            java.lang.String r1 = "timezone"
            r4 = 8
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0192 }
            goto L_0x00c8
        L_0x01a2:
            java.lang.String r1 = "timezone"
            r4 = 8
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0192 }
            goto L_0x00c8
        L_0x01ab:
            java.lang.String r1 = "country"
            java.lang.String r4 = "Unknown"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = "language"
            java.lang.String r4 = "Unknown"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = "timezone"
            r4 = 8
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0192 }
            goto L_0x00c8
        L_0x01c2:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = "resolution"
            java.lang.String r4 = "Unknown"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0192 }
            goto L_0x0104
        L_0x01cf:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ Exception -> 0x0192 }
            java.lang.String r1 = "access"
            java.lang.String r4 = "Unknown"
            r3.put(r1, r4)     // Catch:{ Exception -> 0x0192 }
            goto L_0x0123
        L_0x01dc:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x0192 }
            java.lang.String r0 = "carrier"
            java.lang.String r1 = "Unknown"
            r3.put(r0, r1)     // Catch:{ Exception -> 0x0192 }
            goto L_0x012c
        L_0x01e9:
            java.lang.String r0 = "lat"
            r4 = 0
            r3.put(r0, r4)     // Catch:{ Exception -> 0x0192 }
            java.lang.String r0 = "lng"
            r4 = 0
            r3.put(r0, r4)     // Catch:{ Exception -> 0x0192 }
            goto L_0x0150
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclick.android.l.e(android.content.Context):org.json.JSONObject");
    }

    public static String[] f(Context context) {
        String[] strArr = {"Unknown", "Unknown"};
        if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0) {
            strArr[0] = "Unknown";
            return strArr;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            strArr[0] = "Unknown";
            return strArr;
        } else if (connectivityManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
            strArr[0] = "Wi-Fi";
            return strArr;
        } else {
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
            if (networkInfo.getState() != NetworkInfo.State.CONNECTED) {
                return strArr;
            }
            strArr[0] = "2G/3G";
            strArr[1] = networkInfo.getSubtypeName();
            return strArr;
        }
    }

    public static String g(Context context) {
        Object obj;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (!(applicationInfo == null || applicationInfo.metaData == null || (obj = applicationInfo.metaData.get("UMENG_CHANNEL")) == null)) {
                String obj2 = obj.toString();
                if (obj2 != null) {
                    return obj2;
                }
                Log.i(UmengConstants.LOG_TAG, "Could not read UMENG_CHANNEL meta-data from AndroidManifest.xml.");
                return "Unknown";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Unknown";
    }

    public static boolean h(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        if (connectivityManager.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED) {
            return true;
        }
        return connectivityManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED;
    }

    private static String i(Context context) {
        try {
            return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
        } catch (Exception e) {
            Log.i(UmengConstants.LOG_TAG, "Could not read MAC, forget to include ACCESS_WIFI_STATE permission?", e);
            return null;
        }
    }

    private static Location j(Context context) {
        return new e(context).a();
    }
}
