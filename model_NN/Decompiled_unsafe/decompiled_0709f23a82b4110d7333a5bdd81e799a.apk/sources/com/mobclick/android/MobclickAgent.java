package com.mobclick.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.feedback.b.d;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.microedition.khronos.opengles.GL10;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.codehaus.jackson.util.MinimalPrettyPrinter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MobclickAgent {
    public static String GPU_RENDERER = "";
    public static String GPU_VENDER = "";
    /* access modifiers changed from: private */
    public static final MobclickAgent a = new MobclickAgent();
    private static int b = 1;
    private static UmengUpdateListener e = null;
    private static JSONObject f = null;
    public static boolean mUpdateOnlyWifi = true;
    public static boolean updateAutoPopup = true;
    private Context c;
    private final Handler d;

    private MobclickAgent() {
        HandlerThread handlerThread = new HandlerThread(UmengConstants.LOG_TAG);
        handlerThread.start();
        this.d = new Handler(handlerThread.getLooper());
    }

    private static String a(Context context) {
        String str;
        Exception e2;
        try {
            String packageName = context.getPackageName();
            ArrayList arrayList = new ArrayList();
            arrayList.add("logcat");
            arrayList.add("-d");
            arrayList.add("-v");
            arrayList.add("raw");
            arrayList.add("-s");
            arrayList.add("AndroidRuntime:E");
            arrayList.add("-p");
            arrayList.add(packageName);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec((String[]) arrayList.toArray(new String[arrayList.size()])).getInputStream()), 1024);
            boolean z = false;
            String readLine = bufferedReader.readLine();
            str = "";
            boolean z2 = false;
            while (readLine != null) {
                String str2 = readLine.indexOf("thread attach failed") < 0 ? String.valueOf(str) + readLine + 10 : str;
                if (!z2 && readLine.toLowerCase().indexOf("exception") >= 0) {
                    z2 = true;
                }
                z = (z || readLine.indexOf(packageName) < 0) ? z : true;
                readLine = bufferedReader.readLine();
                str = str2;
            }
            if (str.length() <= 0 || !z2 || !z) {
                str = "";
            }
            try {
                Runtime.getRuntime().exec("logcat -c");
            } catch (Exception e3) {
                try {
                    Log.e(UmengConstants.LOG_TAG, "Failed to clear log");
                    e3.printStackTrace();
                } catch (Exception e4) {
                    e2 = e4;
                }
            }
        } catch (Exception e5) {
            Exception exc = e5;
            str = "";
            e2 = exc;
            Log.e(UmengConstants.LOG_TAG, "Failed to catch error log");
            e2.printStackTrace();
            return str;
        }
        return str;
    }

    private String a(Context context, SharedPreferences sharedPreferences) {
        Long valueOf = Long.valueOf(System.currentTimeMillis());
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putLong("start_millis", valueOf.longValue());
        edit.putLong("end_millis", -1);
        edit.commit();
        return sharedPreferences.getString("session_id", null);
    }

    private String a(Context context, String str, SharedPreferences sharedPreferences) {
        c(context, sharedPreferences);
        long currentTimeMillis = System.currentTimeMillis();
        String str2 = String.valueOf(str) + String.valueOf(currentTimeMillis);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(UmengConstants.AtomKey_AppKey, str);
        edit.putString("session_id", str2);
        edit.putLong("start_millis", currentTimeMillis);
        edit.putLong("end_millis", -1);
        edit.putLong("duration", 0);
        edit.putString("activities", "");
        edit.commit();
        b(context, sharedPreferences);
        return str2;
    }

    private static String a(Context context, JSONObject jSONObject, String str, boolean z) {
        Log.i(UmengConstants.LOG_TAG, jSONObject.toString());
        HttpPost httpPost = new HttpPost(str);
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, d.b);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        try {
            String a2 = m.a(context);
            if (a2 != null) {
                Log.i("TAG", "Proxy IP:" + a2);
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", new HttpHost(a2, 80));
            }
            String jSONObject2 = jSONObject.toString();
            if (!UmengConstants.COMPRESS_DATA || z) {
                ArrayList arrayList = new ArrayList(1);
                arrayList.add(new BasicNameValuePair(UmengConstants.AtomKey_Content, jSONObject2));
                httpPost.setEntity(new UrlEncodedFormEntity(arrayList, "UTF-8"));
            } else {
                byte[] b2 = l.b("content=" + jSONObject2);
                httpPost.addHeader("Content-Encoding", "deflate");
                httpPost.setEntity(new InputStreamEntity(new ByteArrayInputStream(b2), (long) l.b));
            }
            SharedPreferences.Editor edit = g(context).edit();
            Date date = new Date();
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            long time = new Date().getTime() - date.getTime();
            if (execute.getStatusLine().getStatusCode() == 200) {
                Log.i(UmengConstants.LOG_TAG, "Sent message to " + str);
                edit.putLong("req_time", time);
                edit.commit();
                HttpEntity entity = execute.getEntity();
                if (entity != null) {
                    return a(entity.getContent());
                }
                return null;
            }
            edit.putLong("req_time", -1);
            return null;
        } catch (ClientProtocolException e2) {
            Log.i(UmengConstants.LOG_TAG, "ClientProtocolException,Failed to send message.", e2);
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            Log.i(UmengConstants.LOG_TAG, "IOException,Failed to send message.", e3);
            e3.printStackTrace();
            return null;
        }
    }

    private static String a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    try {
                        inputStream.close();
                        return sb.toString();
                    } catch (IOException e2) {
                        Log.e(UmengConstants.LOG_TAG, "Caught IOException in convertStreamToString()", e2);
                        e2.printStackTrace();
                        return null;
                    }
                } else {
                    sb.append(String.valueOf(readLine) + "\n");
                }
            } catch (IOException e3) {
                Log.e(UmengConstants.LOG_TAG, "Caught IOException in convertStreamToString()", e3);
                e3.printStackTrace();
                try {
                    inputStream.close();
                    return null;
                } catch (IOException e4) {
                    Log.e(UmengConstants.LOG_TAG, "Caught IOException in convertStreamToString()", e4);
                    e4.printStackTrace();
                    return null;
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                    throw th;
                } catch (IOException e5) {
                    Log.e(UmengConstants.LOG_TAG, "Caught IOException in convertStreamToString()", e5);
                    e5.printStackTrace();
                    return null;
                }
            }
        }
    }

    private static void a(Context context, int i) {
        if (i < 0 || i > 5) {
            Log.e(UmengConstants.LOG_TAG, "Illegal value of report policy");
            return;
        }
        SharedPreferences l = l(context);
        if (!l.contains("umeng_report_policy")) {
            synchronized (UmengConstants.saveOnlineConfigMutex) {
                l.edit().putInt("umeng_report_policy", i).commit();
            }
        }
    }

    private void a(Context context, SharedPreferences sharedPreferences, String str, String str2, int i) {
        String string = sharedPreferences.getString("session_id", "");
        String b2 = b();
        String str3 = b2.split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR)[0];
        String str4 = b2.split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR)[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(UmengConstants.AtomKey_Type, "event");
            jSONObject.put("session_id", string);
            jSONObject.put("date", str3);
            jSONObject.put("time", str4);
            jSONObject.put("tag", str);
            jSONObject.put("label", str2);
            jSONObject.put("acc", i);
            this.d.post(new k(this, context, jSONObject));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    private synchronized void a(Context context, String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(UmengConstants.AtomKey_Type, "update");
            jSONObject.put(UmengConstants.AtomKey_AppKey, str);
            int i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            String packageName = context.getPackageName();
            jSONObject.put("version_code", i);
            jSONObject.put("package", packageName);
            jSONObject.put(UmengConstants.AtomKey_SDK_Version, UmengConstants.SDK_VERSION);
            jSONObject.put("idmd5", l.c(context));
            jSONObject.put("channel", l.g(context));
            this.d.post(new k(this, context, jSONObject));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return;
    }

    /* access modifiers changed from: private */
    public synchronized void a(Context context, String str, String str2) {
        this.c = context;
        SharedPreferences i = i(context);
        if (i != null) {
            if (a(i)) {
                Log.i(UmengConstants.LOG_TAG, "Start new session: " + a(context, str, i));
            } else {
                Log.i(UmengConstants.LOG_TAG, "Extend current session: " + a(context, i));
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(Context context, String str, String str2, String str3, int i) {
        SharedPreferences i2 = i(context);
        if (i2 != null) {
            a(context, i2, str2, str3, i);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclick.android.MobclickAgent.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean):java.lang.String
     arg types: [android.content.Context, org.json.JSONObject, java.lang.String, int]
     candidates:
      com.mobclick.android.MobclickAgent.a(com.mobclick.android.MobclickAgent, android.content.Context, java.lang.String, java.lang.String):void
      com.mobclick.android.MobclickAgent.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public void a(Context context, JSONObject jSONObject) {
        if (a("update", context)) {
            String str = null;
            for (String a2 : UmengConstants.UPDATE_URL_LIST) {
                str = a(context, jSONObject, a2, true);
                Log.i(UmengConstants.LOG_TAG, "return message from " + str);
                if (str != null) {
                    break;
                }
            }
            if (str != null) {
                d(context, str);
            } else if (e != null) {
                e.a(UpdateStatus.Timeout);
            }
        } else if (e != null) {
            e.a(UpdateStatus.No);
        }
    }

    private boolean a(SharedPreferences sharedPreferences) {
        return UmengConstants.testMode || System.currentTimeMillis() - sharedPreferences.getLong("end_millis", -1) > UmengConstants.kContinueSessionMillis;
    }

    private static boolean a(String str, Context context) {
        if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) == 0 && !l.h(context)) {
            return false;
        }
        if (str == "update" || str == UmengConstants.FeedbackPreName || str == "online_config") {
            return true;
        }
        b = m(context);
        if (b == 3) {
            return str == "flush";
        }
        if (str == UmengConstants.Atom_State_Error) {
            return true;
        }
        if (b == 1 && str == "launch") {
            return true;
        }
        if (b == 2 && str == "terminate") {
            return true;
        }
        if (b == 0) {
            return true;
        }
        if (b == 4) {
            String string = h(context).getString(l.b(), "false");
            Log.i(UmengConstants.LOG_TAG, "Log has been sent today: " + string + ";type:" + str);
            return !string.equals("true") && str.equals("launch");
        } else if (b == 5) {
            return l.f(context)[0].equals("Wi-Fi");
        } else {
            return false;
        }
    }

    private static AlertDialog b(Context context, File file) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getString(l.a(context, "string", "UMUpdateTitle"))).setMessage(context.getString(l.a(context, "string", "UMDialog_InstallAPK"))).setCancelable(false).setPositiveButton(context.getString(l.a(context, "string", "UMUpdateNow")), new f(context, file)).setNegativeButton(context.getString(l.a(context, "string", "UMNotNow")), new g());
        AlertDialog create = builder.create();
        create.setCancelable(true);
        return create;
    }

    private static AlertDialog b(Context context, JSONObject jSONObject) {
        try {
            String string = jSONObject.has("version") ? jSONObject.getString("version") : "";
            String string2 = jSONObject.has("update_log") ? jSONObject.getString("update_log") : "";
            String string3 = jSONObject.has("path") ? jSONObject.getString("path") : "";
            String str = "";
            if (!l.f(context)[0].equals("Wi-Fi")) {
                str = String.valueOf(context.getString(l.a(context, "string", "UMGprsCondition"))) + "\n";
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(context.getString(l.a(context, "string", "UMUpdateTitle"))).setMessage(String.valueOf(str) + context.getString(l.a(context, "string", "UMNewVersion")) + string + "\n" + string2).setCancelable(false).setPositiveButton(context.getString(l.a(context, "string", "UMUpdateNow")), new h(context, string3, string)).setNegativeButton(context.getString(l.a(context, "string", "UMNotNow")), new i());
            AlertDialog create = builder.create();
            create.setCancelable(true);
            return create;
        } catch (Exception e2) {
            Log.e(UmengConstants.LOG_TAG, "Fail to create update dialog box.", e2);
            e2.printStackTrace();
            return null;
        }
    }

    private static String b() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    /* access modifiers changed from: private */
    public synchronized void b(Context context) {
        if (this.c != context) {
            Log.e(UmengConstants.LOG_TAG, "onPause() called without context from corresponding onResume()");
        } else {
            this.c = context;
            SharedPreferences i = i(context);
            if (i != null) {
                long j = i.getLong("start_millis", -1);
                if (j == -1) {
                    Log.e(UmengConstants.LOG_TAG, "onEndSession called before onStartSession");
                } else {
                    long currentTimeMillis = System.currentTimeMillis();
                    long j2 = currentTimeMillis - j;
                    long j3 = i.getLong("duration", 0);
                    SharedPreferences.Editor edit = i.edit();
                    if (UmengConstants.ACTIVITY_DURATION_OPEN) {
                        String string = i.getString("activities", "");
                        String name = context.getClass().getName();
                        if (!"".equals(string)) {
                            string = String.valueOf(string) + ";";
                        }
                        edit.remove("activities");
                        edit.putString("activities", String.valueOf(string) + "[" + name + "," + (j2 / 1000) + "]");
                    }
                    edit.putLong("start_millis", -1);
                    edit.putLong("end_millis", currentTimeMillis);
                    edit.putLong("duration", j2 + j3);
                    edit.commit();
                }
            }
        }
    }

    private void b(Context context, SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString("session_id", null);
        if (string == null) {
            Log.e(UmengConstants.LOG_TAG, "Missing session_id, ignore message");
            return;
        }
        String b2 = b();
        String str = b2.split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR)[0];
        String str2 = b2.split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR)[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(UmengConstants.AtomKey_Type, "launch");
            jSONObject.put("session_id", string);
            jSONObject.put("date", str);
            jSONObject.put("time", str2);
            this.d.post(new k(this, context, jSONObject));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b(Context context, String str) {
        String a2 = a(context);
        if (a2 != "" && a2.length() <= 10240) {
            c(context, a2);
        }
    }

    private synchronized void c(Context context) {
        d(context);
    }

    private void c(Context context, SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString("session_id", null);
        if (string == null) {
            Log.w(UmengConstants.LOG_TAG, "Missing session_id, ignore message");
            return;
        }
        Long valueOf = Long.valueOf(sharedPreferences.getLong("duration", -1));
        if (valueOf.longValue() <= 0) {
            valueOf = 0L;
        }
        String b2 = b();
        String str = b2.split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR)[0];
        String str2 = b2.split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR)[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(UmengConstants.AtomKey_Type, "terminate");
            jSONObject.put("session_id", string);
            jSONObject.put("date", str);
            jSONObject.put("time", str2);
            jSONObject.put("duration", String.valueOf(valueOf.longValue() / 1000));
            if (UmengConstants.ACTIVITY_DURATION_OPEN) {
                String[] split = sharedPreferences.getString("activities", "").split(";");
                JSONArray jSONArray = new JSONArray();
                for (String jSONArray2 : split) {
                    jSONArray.put(new JSONArray(jSONArray2));
                }
                jSONObject.put("activities", jSONArray);
            }
            this.d.post(new k(this, context, jSONObject));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public static void c(Context context, File file) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse("file://" + file.getAbsolutePath()), "application/vnd.android.package-archive");
        context.startActivity(intent);
    }

    private void c(Context context, String str) {
        String b2 = b();
        String str2 = b2.split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR)[0];
        String str3 = b2.split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR)[1];
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(UmengConstants.AtomKey_Type, UmengConstants.Atom_State_Error);
            jSONObject.put("context", str);
            jSONObject.put("date", str2);
            jSONObject.put("time", str3);
            this.d.post(new k(this, context, jSONObject));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclick.android.MobclickAgent.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean):java.lang.String
     arg types: [android.content.Context, org.json.JSONObject, java.lang.String, int]
     candidates:
      com.mobclick.android.MobclickAgent.a(com.mobclick.android.MobclickAgent, android.content.Context, java.lang.String, java.lang.String):void
      com.mobclick.android.MobclickAgent.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public void c(Context context, JSONObject jSONObject) {
        JSONObject jSONObject2;
        SharedPreferences g = g(context);
        JSONObject e2 = l.e(context);
        long j = g.getLong("req_time", 0);
        if (j != 0) {
            try {
                e2.put("req_time", j);
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
        }
        g.edit().putString("header", e2.toString()).commit();
        JSONObject e4 = e(context);
        JSONObject jSONObject3 = new JSONObject();
        try {
            String string = jSONObject.getString(UmengConstants.AtomKey_Type);
            if (string != null) {
                if (string != "flush") {
                    jSONObject.remove(UmengConstants.AtomKey_Type);
                    if (e4 == null) {
                        JSONObject jSONObject4 = new JSONObject();
                        JSONArray jSONArray = new JSONArray();
                        jSONArray.put(jSONObject);
                        jSONObject4.put(string, jSONArray);
                        jSONObject2 = jSONObject4;
                    } else if (e4.isNull(string)) {
                        JSONArray jSONArray2 = new JSONArray();
                        jSONArray2.put(jSONObject);
                        e4.put(string, jSONArray2);
                        jSONObject2 = e4;
                    } else {
                        e4.getJSONArray(string).put(jSONObject);
                        jSONObject2 = e4;
                    }
                } else {
                    jSONObject2 = e4;
                }
                if (jSONObject2 == null) {
                    Log.w(UmengConstants.LOG_TAG, "No cache message to flush");
                    return;
                }
                jSONObject3.put("header", e2);
                jSONObject3.put("body", jSONObject2);
                if (a(string, context)) {
                    String str = null;
                    for (String a2 : UmengConstants.APPLOG_URL_LIST) {
                        str = a(context, jSONObject3, a2, false);
                        Log.i(UmengConstants.LOG_TAG, "return message from " + str);
                        if (str != null) {
                            break;
                        }
                    }
                    if (str != null) {
                        Log.i(UmengConstants.LOG_TAG, "send message succeed, clear cache");
                        f(context);
                        if (b == 4) {
                            SharedPreferences.Editor edit = h(context).edit();
                            edit.putString(l.b(), "true");
                            edit.commit();
                            return;
                        }
                        return;
                    }
                }
                d(context, jSONObject2);
            }
        } catch (JSONException e5) {
            Log.e(UmengConstants.LOG_TAG, "Fail to construct json message.");
            e5.printStackTrace();
            f(context);
        }
    }

    private void d(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(UmengConstants.AtomKey_Type, "flush");
            this.d.post(new k(this, context, jSONObject));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    private void d(Context context, String str) {
        try {
            f = new JSONObject(str);
            if (f.getString("update").equals("Yes")) {
                if (e != null) {
                    e.a(UpdateStatus.Yes);
                }
                if (updateAutoPopup) {
                    showUpdateDialog(context);
                }
            } else if (e != null) {
                e.a(UpdateStatus.No);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private static void d(Context context, JSONObject jSONObject) {
        try {
            FileOutputStream openFileOutput = context.openFileOutput(k(context), 0);
            openFileOutput.write(jSONObject.toString().getBytes());
            openFileOutput.close();
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    private static File e(Context context, JSONObject jSONObject) {
        String absolutePath;
        try {
            String string = jSONObject.has("path") ? jSONObject.getString("path") : "";
            String string2 = jSONObject.has("version") ? jSONObject.getString("version") : "";
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(string).openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setConnectTimeout(5000);
            httpURLConnection.connect();
            int contentLength = httpURLConnection.getContentLength();
            httpURLConnection.disconnect();
            if (Environment.getExternalStorageState().equals("mounted")) {
                File externalStorageDirectory = Environment.getExternalStorageDirectory();
                absolutePath = String.valueOf(externalStorageDirectory.getParent()) + "/" + externalStorageDirectory.getName() + "/download";
            } else {
                absolutePath = context.getFilesDir().getAbsolutePath();
            }
            File file = new File(absolutePath, a.a(context.getPackageName(), string2, contentLength));
            if (!file.exists() || file.length() != ((long) contentLength)) {
                return null;
            }
            return file;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private static JSONObject e(Context context) {
        String str;
        try {
            FileInputStream openFileInput = context.openFileInput(k(context));
            String str2 = "";
            byte[] bArr = new byte[16384];
            while (true) {
                str = str2;
                int read = openFileInput.read(bArr);
                if (read == -1) {
                    break;
                }
                str2 = String.valueOf(str) + new String(bArr, 0, read);
            }
            if (str.length() == 0) {
                return null;
            }
            try {
                return new JSONObject(str);
            } catch (JSONException e2) {
                openFileInput.close();
                f(context);
                e2.printStackTrace();
                return null;
            }
        } catch (FileNotFoundException | IOException e3) {
            return null;
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void e(android.content.Context r6, java.lang.String r7) {
        /*
            android.content.SharedPreferences r1 = l(r6)
            java.lang.Object r2 = com.mobclick.android.UmengConstants.saveOnlineConfigMutex
            monitor-enter(r2)
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Exception -> 0x006f }
            r3.<init>(r7)     // Catch:{ Exception -> 0x006f }
            java.lang.String r0 = "last_config_time"
            boolean r0 = r3.has(r0)     // Catch:{ Exception -> 0x007c }
            if (r0 == 0) goto L_0x0027
            android.content.SharedPreferences$Editor r0 = r1.edit()     // Catch:{ Exception -> 0x007c }
            java.lang.String r4 = "umeng_last_config_time"
            java.lang.String r5 = "last_config_time"
            java.lang.String r5 = r3.getString(r5)     // Catch:{ Exception -> 0x007c }
            android.content.SharedPreferences$Editor r0 = r0.putString(r4, r5)     // Catch:{ Exception -> 0x007c }
            r0.commit()     // Catch:{ Exception -> 0x007c }
        L_0x0027:
            java.lang.String r0 = "report_policy"
            boolean r0 = r3.has(r0)     // Catch:{ Exception -> 0x0081 }
            if (r0 == 0) goto L_0x0042
            android.content.SharedPreferences$Editor r0 = r1.edit()     // Catch:{ Exception -> 0x0081 }
            java.lang.String r4 = "umeng_report_policy"
            java.lang.String r5 = "report_policy"
            int r5 = r3.getInt(r5)     // Catch:{ Exception -> 0x0081 }
            android.content.SharedPreferences$Editor r0 = r0.putInt(r4, r5)     // Catch:{ Exception -> 0x0081 }
            r0.commit()     // Catch:{ Exception -> 0x0081 }
        L_0x0042:
            java.lang.String r0 = "online_params"
            boolean r0 = r3.has(r0)     // Catch:{ Exception -> 0x0094 }
            if (r0 == 0) goto L_0x006d
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Exception -> 0x0094 }
            java.lang.String r0 = "online_params"
            java.lang.String r0 = r3.getString(r0)     // Catch:{ Exception -> 0x0094 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0094 }
            java.util.Iterator r3 = r4.keys()     // Catch:{ Exception -> 0x0094 }
            android.content.SharedPreferences$Editor r1 = r1.edit()     // Catch:{ Exception -> 0x0094 }
        L_0x005d:
            boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x0094 }
            if (r0 != 0) goto L_0x0086
            r1.commit()     // Catch:{ Exception -> 0x0094 }
            java.lang.String r0 = "MobclickAgent"
            java.lang.String r1 = "get online setting params"
            android.util.Log.i(r0, r1)     // Catch:{ Exception -> 0x0094 }
        L_0x006d:
            monitor-exit(r2)     // Catch:{ all -> 0x0079 }
        L_0x006e:
            return
        L_0x006f:
            r0 = move-exception
            java.lang.String r0 = "MobclickAgent"
            java.lang.String r1 = "not json string"
            android.util.Log.i(r0, r1)     // Catch:{ all -> 0x0079 }
            monitor-exit(r2)     // Catch:{ all -> 0x0079 }
            goto L_0x006e
        L_0x0079:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0079 }
            throw r0
        L_0x007c:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0079 }
            goto L_0x0027
        L_0x0081:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0079 }
            goto L_0x0042
        L_0x0086:
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x0094 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0094 }
            java.lang.String r5 = r4.getString(r0)     // Catch:{ Exception -> 0x0094 }
            r1.putString(r0, r5)     // Catch:{ Exception -> 0x0094 }
            goto L_0x005d
        L_0x0094:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0079 }
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobclick.android.MobclickAgent.e(android.content.Context, java.lang.String):void");
    }

    public static void enterPage(Context context, String str) {
        onEvent(context, "_PAGE_", str);
    }

    private static void f(Context context) {
        context.deleteFile(j(context));
        context.deleteFile(k(context));
    }

    private synchronized void f(Context context, String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(UmengConstants.AtomKey_Type, "online_config");
            jSONObject.put(UmengConstants.AtomKey_AppKey, str);
            int i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            String packageName = context.getPackageName();
            jSONObject.put("version_code", i);
            jSONObject.put("package", packageName);
            jSONObject.put(UmengConstants.AtomKey_SDK_Version, UmengConstants.SDK_VERSION);
            jSONObject.put("idmd5", l.c(context));
            jSONObject.put("channel", l.g(context));
            jSONObject.put("report_policy", m(context));
            jSONObject.put("last_config_time", n(context));
            this.d.post(new k(this, context, jSONObject));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mobclick.android.MobclickAgent.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean):java.lang.String
     arg types: [android.content.Context, org.json.JSONObject, java.lang.String, int]
     candidates:
      com.mobclick.android.MobclickAgent.a(com.mobclick.android.MobclickAgent, android.content.Context, java.lang.String, java.lang.String):void
      com.mobclick.android.MobclickAgent.a(android.content.Context, org.json.JSONObject, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public void f(Context context, JSONObject jSONObject) {
        if (a("online_config", context)) {
            String a2 = a(context, jSONObject, UmengConstants.CONFIG_URL, true);
            Log.i(UmengConstants.LOG_TAG, "return message from " + a2);
            if (a2 == null) {
                a2 = a(context, jSONObject, UmengConstants.CONFIG_URL_BACK, true);
            }
            if (a2 != null) {
                e(context, a2);
            }
        }
    }

    public static void flush(Context context) {
        if (context == null) {
            try {
                Log.e(UmengConstants.LOG_TAG, "unexpected null context");
            } catch (Exception e2) {
                Log.e(UmengConstants.LOG_TAG, "Exception occurred in Mobclick.flush(). ");
                e2.printStackTrace();
                return;
            }
        }
        a.c(context);
    }

    private static SharedPreferences g(Context context) {
        return context.getSharedPreferences("mobclick_agent_header_" + context.getPackageName(), 0);
    }

    public static String getConfigParams(Context context, String str) {
        return l(context).getString(str, "");
    }

    public static JSONObject getUpdateInfo() {
        return f;
    }

    private static SharedPreferences h(Context context) {
        return context.getSharedPreferences("mobclick_agent_update_" + context.getPackageName(), 0);
    }

    private static SharedPreferences i(Context context) {
        return context.getSharedPreferences("mobclick_agent_state_" + context.getPackageName(), 0);
    }

    private static String j(Context context) {
        return "mobclick_agent_header_" + context.getPackageName();
    }

    private static String k(Context context) {
        return "mobclick_agent_cached_" + context.getPackageName();
    }

    private static SharedPreferences l(Context context) {
        return context.getSharedPreferences("mobclick_agent_online_setting_" + context.getPackageName(), 0);
    }

    private static int m(Context context) {
        return l(context).getInt("umeng_report_policy", b);
    }

    private static String n(Context context) {
        return l(context).getString("umeng_last_config_time", "");
    }

    public static void onError(Context context) {
        try {
            String b2 = l.b(context);
            if (b2 == null || b2.length() == 0) {
                Log.e(UmengConstants.LOG_TAG, "unexpected empty appkey");
            } else if (context == null) {
                Log.e(UmengConstants.LOG_TAG, "unexpected null context");
            } else {
                new j(context, b2, 2).start();
            }
        } catch (Exception e2) {
            Log.e(UmengConstants.LOG_TAG, "Exception occurred in Mobclick.onError()");
            e2.printStackTrace();
        }
    }

    public static void onEvent(Context context, String str) {
        onEvent(context, str, 1);
    }

    public static void onEvent(Context context, String str, int i) {
        onEvent(context, str, str, i);
    }

    public static void onEvent(Context context, String str, String str2) {
        onEvent(context, str, str2, 1);
    }

    public static void onEvent(Context context, String str, String str2, int i) {
        try {
            String b2 = l.b(context);
            if (b2 == null || b2.length() == 0) {
                Log.e(UmengConstants.LOG_TAG, "unexpected empty appkey");
            } else if (context == null) {
                Log.e(UmengConstants.LOG_TAG, "unexpected null context");
            } else if (str == null || str == "") {
                Log.e(UmengConstants.LOG_TAG, "tag is null or empty");
            } else if (str2 == null || str2 == "") {
                Log.e(UmengConstants.LOG_TAG, "label is null or empty");
            } else if (i <= 0) {
                Log.e(UmengConstants.LOG_TAG, "Illegal value of acc");
            } else {
                new j(context, b2, str, str2, i, 3).start();
            }
        } catch (Exception e2) {
            Log.e(UmengConstants.LOG_TAG, "Exception occurred in Mobclick.onEvent(). ");
            e2.printStackTrace();
        }
    }

    public static void onPause(Context context) {
        if (context == null) {
            try {
                Log.e(UmengConstants.LOG_TAG, "unexpected null context");
            } catch (Exception e2) {
                Log.e(UmengConstants.LOG_TAG, "Exception occurred in Mobclick.onRause(). ");
                e2.printStackTrace();
            }
        } else {
            new j(context, 0).start();
        }
    }

    public static void onResume(Context context) {
        onResume(context, l.b(context), l.g(context));
    }

    public static void onResume(Context context, String str, String str2) {
        if (context == null) {
            try {
                Log.e(UmengConstants.LOG_TAG, "unexpected null context");
            } catch (Exception e2) {
                Log.e(UmengConstants.LOG_TAG, "Exception occurred in Mobclick.onResume(). ");
                e2.printStackTrace();
            }
        } else if (str == null || str.length() == 0) {
            Log.e(UmengConstants.LOG_TAG, "unexpected empty appkey");
        } else {
            new j(context, str, str2, 1).start();
        }
    }

    public static void openActivityDurationTrack(boolean z) {
        UmengConstants.ACTIVITY_DURATION_OPEN = z;
    }

    public static void setDefaultReportPolicy(Context context, int i) {
        if (i < 0 || i > 5) {
            Log.e(UmengConstants.LOG_TAG, "Illegal value of report policy");
            return;
        }
        b = i;
        a(context, i);
    }

    public static void setOpenGLContext(GL10 gl10) {
        if (gl10 != null) {
            String[] a2 = l.a(gl10);
            if (a2.length == 2) {
                GPU_VENDER = a2[0];
                GPU_RENDERER = a2[1];
            }
        }
    }

    public static void setUpdateListener(UmengUpdateListener umengUpdateListener) {
        e = umengUpdateListener;
    }

    public static void setUpdateOnlyWifi(boolean z) {
        mUpdateOnlyWifi = z;
    }

    public static void showUpdateDialog(Context context) {
        if (f != null && f.has("update") && f.optString("update").toLowerCase().equals("yes")) {
            File e2 = e(context, f);
            if (e2 == null || !UmengConstants.enableCacheInUpdate) {
                b(context, f).show();
            } else {
                b(context, e2).show();
            }
        }
    }

    public static void update(Context context) {
        try {
            if (a.b()) {
                l.a(context);
            } else if (!mUpdateOnlyWifi || l.f(context)[0].equals("Wi-Fi")) {
                if (context == null) {
                    if (e != null) {
                        e.a(UpdateStatus.No);
                    }
                    Log.e(UmengConstants.LOG_TAG, "unexpected null context");
                    return;
                }
                a.a(context, l.b(context));
            } else if (e != null) {
                e.a(UpdateStatus.NoneWifi);
            }
        } catch (Exception e2) {
            Log.e(UmengConstants.LOG_TAG, "Exception occurred in Mobclick.update(). " + e2.getMessage());
            e2.printStackTrace();
        }
    }

    public static void updateOnlineConfig(Context context) {
        if (context == null) {
            try {
                Log.e(UmengConstants.LOG_TAG, "unexpected null context");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else {
            a.f(context, l.b(context));
        }
    }
}
