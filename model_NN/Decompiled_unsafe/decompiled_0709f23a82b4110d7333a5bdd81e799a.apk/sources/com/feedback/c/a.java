package com.feedback.c;

import android.content.Context;
import java.util.concurrent.Callable;
import org.json.JSONObject;

public class a implements Callable {
    static String a = "MsgWorker";
    JSONObject b;
    Context c;

    public a(JSONObject jSONObject, Context context) {
        this.b = jSONObject;
        this.c = context;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0039  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Boolean call() {
        /*
            r6 = this;
            r1 = 0
            org.json.JSONObject r0 = r6.b
            java.lang.String r2 = "feedback_id"
            java.lang.String r0 = com.feedback.b.b.a(r0, r2)
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            java.lang.String r3 = "postFeedbackFinished"
            android.content.Intent r2 = r2.setAction(r3)
            java.lang.String r3 = "type"
            java.lang.String r4 = "user_reply"
            android.content.Intent r2 = r2.putExtra(r3, r4)
            java.lang.String r3 = "feedback_id"
            android.content.Intent r2 = r2.putExtra(r3, r0)
            org.json.JSONObject r0 = r6.b     // Catch:{ Exception -> 0x005f }
            java.lang.String r3 = "http://feedback.whalecloud.com/feedback/reply"
            java.lang.String r4 = "reply"
            java.lang.String r3 = com.feedback.b.d.a(r0, r3, r4)     // Catch:{ Exception -> 0x005f }
            if (r3 == 0) goto L_0x0063
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x005f }
            r0.<init>(r3)     // Catch:{ Exception -> 0x005f }
        L_0x0033:
            boolean r3 = com.feedback.b.b.b(r0)
            if (r3 == 0) goto L_0x006a
            org.json.JSONObject r3 = r6.b
            com.feedback.b.b.f(r3)
            org.json.JSONObject r3 = r6.b     // Catch:{ JSONException -> 0x0065 }
            java.lang.String r4 = "reply_id"
            java.lang.String r5 = "reply_id"
            java.lang.String r0 = r0.getString(r5)     // Catch:{ JSONException -> 0x0065 }
            com.feedback.b.b.a(r3, r4, r0)     // Catch:{ JSONException -> 0x0065 }
        L_0x004b:
            java.lang.String r0 = "PostFeedbackBroadcast"
            java.lang.String r3 = "succeed"
            r2.putExtra(r0, r3)
        L_0x0052:
            android.content.Context r0 = r6.c
            org.json.JSONObject r3 = r6.b
            com.feedback.b.c.a(r0, r3)
            android.content.Context r0 = r6.c
            r0.sendBroadcast(r2)
            return r1
        L_0x005f:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0063:
            r0 = r1
            goto L_0x0033
        L_0x0065:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004b
        L_0x006a:
            org.json.JSONObject r0 = r6.b
            com.feedback.b.b.d(r0)
            java.lang.String r0 = "PostFeedbackBroadcast"
            java.lang.String r3 = "fail"
            r2.putExtra(r0, r3)
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: com.feedback.c.a.call():java.lang.Boolean");
    }
}
