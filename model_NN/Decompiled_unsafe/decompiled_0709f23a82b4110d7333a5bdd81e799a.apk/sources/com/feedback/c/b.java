package com.feedback.c;

import android.content.Context;
import org.json.JSONObject;

public class b extends Thread {
    static String a = "PostFeedbackThread";
    JSONObject b;
    Context c;

    public b(JSONObject jSONObject, Context context) {
        com.feedback.b.b.c(jSONObject);
        this.b = jSONObject;
        this.c = context;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0047  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r5 = this;
            r1 = 0
            android.content.Intent r0 = new android.content.Intent
            r0.<init>()
            java.lang.String r2 = "postFeedbackFinished"
            android.content.Intent r2 = r0.setAction(r2)
            java.lang.String r0 = "new_feedback"
            org.json.JSONObject r3 = r5.b     // Catch:{ Exception -> 0x0072 }
            java.lang.String r4 = "type"
            java.lang.String r3 = r3.optString(r4)     // Catch:{ Exception -> 0x0072 }
            boolean r0 = r0.equals(r3)     // Catch:{ Exception -> 0x0072 }
            if (r0 == 0) goto L_0x0076
            org.json.JSONObject r0 = r5.b     // Catch:{ Exception -> 0x0072 }
            java.lang.String r3 = "http://feedback.whalecloud.com/feedback/feedbacks"
            java.lang.String r4 = "feedback"
            java.lang.String r3 = com.feedback.b.d.a(r0, r3, r4)     // Catch:{ Exception -> 0x0072 }
            java.lang.String r0 = "type"
            java.lang.String r4 = "feedback"
            r2.putExtra(r0, r4)     // Catch:{ Exception -> 0x0072 }
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x0072 }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0072 }
        L_0x0032:
            org.json.JSONObject r1 = r5.b
            java.lang.String r3 = "sequence_num"
            java.lang.String r1 = com.feedback.b.b.a(r1, r3)
            android.content.Context r3 = r5.c
            java.lang.String r4 = "temp"
            com.feedback.b.c.a(r3, r4, r1)
            boolean r3 = com.feedback.b.b.b(r0)
            if (r3 == 0) goto L_0x0078
            java.lang.String r1 = "feedback_id"
            java.lang.String r0 = com.feedback.b.b.a(r0, r1)
            org.json.JSONObject r1 = r5.b
            com.feedback.b.b.f(r1)
            org.json.JSONObject r1 = r5.b
            java.lang.String r3 = "feedback_id"
            com.feedback.b.b.a(r1, r3, r0)
            android.content.Context r1 = r5.c
            org.json.JSONObject r3 = r5.b
            com.feedback.b.c.a(r1, r3)
            java.lang.String r1 = "PostFeedbackBroadcast"
            java.lang.String r3 = "succeed"
            r2.putExtra(r1, r3)
            java.lang.String r1 = "feedback_id"
            r2.putExtra(r1, r0)
        L_0x006c:
            android.content.Context r0 = r5.c
            r0.sendBroadcast(r2)
            return
        L_0x0072:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0076:
            r0 = r1
            goto L_0x0032
        L_0x0078:
            org.json.JSONObject r0 = r5.b
            com.feedback.b.b.d(r0)
            android.content.Context r0 = r5.c
            org.json.JSONObject r3 = r5.b
            com.feedback.b.c.c(r0, r3)
            java.lang.String r0 = "sequence_num"
            r2.putExtra(r0, r1)
            java.lang.String r0 = "PostFeedbackBroadcast"
            java.lang.String r1 = "fail"
            r2.putExtra(r0, r1)
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.feedback.c.b.run():void");
    }
}
