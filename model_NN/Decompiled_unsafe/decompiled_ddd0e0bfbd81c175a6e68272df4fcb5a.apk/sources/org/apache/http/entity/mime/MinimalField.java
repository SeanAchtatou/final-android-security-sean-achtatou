package org.apache.http.entity.mime;

import org.apache.http.annotation.Immutable;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.ContentUtil;

@Immutable
public class MinimalField implements Field {
    private final String name;
    private ByteSequence raw = null;
    private final String value;

    MinimalField(String name2, String value2) {
        this.name = name2;
        this.value = value2;
    }

    public String getName() {
        return this.name;
    }

    public String getBody() {
        return this.value;
    }

    public ByteSequence getRaw() {
        if (this.raw == null) {
            Object[] objArr = {(String) MinimalField.class.getMethod("toString", new Class[0]).invoke(this, new Object[0])};
            this.raw = (ByteSequence) ContentUtil.class.getMethod("encode", String.class).invoke(null, objArr);
        }
        return this.raw;
    }

    public String toString() {
        StringBuilder buffer = new StringBuilder();
        Object[] objArr = {this.name};
        StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr);
        Object[] objArr2 = {": "};
        StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr2);
        Object[] objArr3 = {this.value};
        StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr3);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(buffer, new Object[0]);
    }
}
