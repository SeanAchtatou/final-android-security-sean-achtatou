package org.apache.http.entity.mime.content;

import java.util.Collections;
import java.util.Map;
import org.apache.http.annotation.NotThreadSafe;
import org.apache.james.mime4j.message.Entity;
import org.apache.james.mime4j.message.SingleBody;

@NotThreadSafe
public abstract class AbstractContentBody extends SingleBody implements ContentBody {
    private final String mediaType;
    private final String mimeType;
    private Entity parent = null;
    private final String subType;

    public AbstractContentBody(String mimeType2) {
        if (mimeType2 == null) {
            throw new IllegalArgumentException("MIME type may not be null");
        }
        this.mimeType = mimeType2;
        Class[] clsArr = {Integer.TYPE};
        int i = ((Integer) String.class.getMethod("indexOf", clsArr).invoke(mimeType2, new Integer(47))).intValue();
        if (i != -1) {
            Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
            this.mediaType = (String) String.class.getMethod("substring", clsArr2).invoke(mimeType2, new Integer(0), new Integer(i));
            Class[] clsArr3 = {Integer.TYPE};
            this.subType = (String) String.class.getMethod("substring", clsArr3).invoke(mimeType2, new Integer(i + 1));
            return;
        }
        this.mediaType = mimeType2;
        this.subType = null;
    }

    public Entity getParent() {
        return this.parent;
    }

    public void setParent(Entity parent2) {
        this.parent = parent2;
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public String getMediaType() {
        return this.mediaType;
    }

    public String getSubType() {
        return this.subType;
    }

    public Map<String, String> getContentTypeParameters() {
        return (Map) Collections.class.getMethod("emptyMap", new Class[0]).invoke(null, new Object[0]);
    }

    public void dispose() {
    }
}
