package org.apache.http.entity.mime;

import java.lang.reflect.Method;
import org.apache.http.annotation.NotThreadSafe;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.james.mime4j.descriptor.ContentDescriptor;
import org.apache.james.mime4j.message.Body;
import org.apache.james.mime4j.message.BodyPart;
import org.apache.james.mime4j.message.Header;
import org.apache.james.mime4j.parser.Field;

@NotThreadSafe
public class FormBodyPart extends BodyPart {
    private final String name;

    public FormBodyPart(String name2, ContentBody body) {
        if (name2 == null) {
            throw new IllegalArgumentException("Name may not be null");
        } else if (body == null) {
            throw new IllegalArgumentException("Body may not be null");
        } else {
            this.name = name2;
            Object[] objArr = {new Header()};
            FormBodyPart.class.getMethod("setHeader", Header.class).invoke(this, objArr);
            Object[] objArr2 = {body};
            FormBodyPart.class.getMethod("setBody", Body.class).invoke(this, objArr2);
            Object[] objArr3 = {body};
            FormBodyPart.class.getMethod("generateContentDisp", ContentBody.class).invoke(this, objArr3);
            Object[] objArr4 = {body};
            FormBodyPart.class.getMethod("generateContentType", ContentDescriptor.class).invoke(this, objArr4);
            Object[] objArr5 = {body};
            FormBodyPart.class.getMethod("generateTransferEncoding", ContentDescriptor.class).invoke(this, objArr5);
        }
    }

    public String getName() {
        return this.name;
    }

    /* access modifiers changed from: protected */
    public void generateContentDisp(ContentBody body) {
        StringBuilder buffer = new StringBuilder();
        Object[] objArr = {"form-data; name=\""};
        StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr);
        Object[] objArr2 = {(String) FormBodyPart.class.getMethod("getName", new Class[0]).invoke(this, new Object[0])};
        StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr2);
        Object[] objArr3 = {"\""};
        StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr3);
        if (((String) ContentBody.class.getMethod("getFilename", new Class[0]).invoke(body, new Object[0])) != null) {
            Object[] objArr4 = {"; filename=\""};
            StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr4);
            Object[] objArr5 = {(String) ContentBody.class.getMethod("getFilename", new Class[0]).invoke(body, new Object[0])};
            StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr5);
            Object[] objArr6 = {"\""};
            StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr6);
        }
        Object[] objArr7 = {"Content-Disposition", (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(buffer, new Object[0])};
        FormBodyPart.class.getMethod("addField", String.class, String.class).invoke(this, objArr7);
    }

    /* access modifiers changed from: protected */
    public void generateContentType(ContentDescriptor desc) {
        if (((String) ContentDescriptor.class.getMethod("getMimeType", new Class[0]).invoke(desc, new Object[0])) != null) {
            StringBuilder buffer = new StringBuilder();
            Object[] objArr = {(String) ContentDescriptor.class.getMethod("getMimeType", new Class[0]).invoke(desc, new Object[0])};
            StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr);
            if (((String) ContentDescriptor.class.getMethod("getCharset", new Class[0]).invoke(desc, new Object[0])) != null) {
                Object[] objArr2 = {"; charset="};
                StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr2);
                Object[] objArr3 = {(String) ContentDescriptor.class.getMethod("getCharset", new Class[0]).invoke(desc, new Object[0])};
                StringBuilder.class.getMethod("append", String.class).invoke(buffer, objArr3);
            }
            Object[] objArr4 = {"Content-Type", (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(buffer, new Object[0])};
            FormBodyPart.class.getMethod("addField", String.class, String.class).invoke(this, objArr4);
        }
    }

    /* access modifiers changed from: protected */
    public void generateTransferEncoding(ContentDescriptor desc) {
        if (((String) ContentDescriptor.class.getMethod("getTransferEncoding", new Class[0]).invoke(desc, new Object[0])) != null) {
            Object[] objArr = {"Content-Transfer-Encoding", (String) ContentDescriptor.class.getMethod("getTransferEncoding", new Class[0]).invoke(desc, new Object[0])};
            FormBodyPart.class.getMethod("addField", String.class, String.class).invoke(this, objArr);
        }
    }

    private void addField(String name2, String value) {
        Method method = FormBodyPart.class.getMethod("getHeader", new Class[0]);
        Method method2 = Header.class.getMethod("addField", Field.class);
        method2.invoke((Header) method.invoke(this, new Object[0]), new MinimalField(name2, value));
    }
}
