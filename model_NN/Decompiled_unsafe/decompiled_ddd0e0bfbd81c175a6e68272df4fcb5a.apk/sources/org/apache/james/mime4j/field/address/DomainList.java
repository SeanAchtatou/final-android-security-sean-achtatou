package org.apache.james.mime4j.field.address;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class DomainList extends AbstractList<String> implements Serializable {
    private static final long serialVersionUID = 1;
    private final List<String> domains;

    public DomainList(List<String> domains2, boolean dontCopy) {
        if (domains2 != null) {
            this.domains = !dontCopy ? new ArrayList<>(domains2) : domains2;
        } else {
            this.domains = (List) Collections.class.getMethod("emptyList", new Class[0]).invoke(null, new Object[0]);
        }
    }

    public int size() {
        return ((Integer) List.class.getMethod("size", new Class[0]).invoke(this.domains, new Object[0])).intValue();
    }

    public String get(int index) {
        List<String> list = this.domains;
        Class[] clsArr = {Integer.TYPE};
        return (String) List.class.getMethod("get", clsArr).invoke(list, new Integer(index));
    }

    public String toRouteString() {
        StringBuilder sb = new StringBuilder();
        Iterator i$ = (Iterator) List.class.getMethod("iterator", new Class[0]).invoke(this.domains, new Object[0]);
        while (true) {
            if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                String domain = (String) Iterator.class.getMethod("next", new Class[0]).invoke(i$, new Object[0]);
                if (((Integer) StringBuilder.class.getMethod("length", new Class[0]).invoke(sb, new Object[0])).intValue() > 0) {
                    Class[] clsArr = {Character.TYPE};
                    StringBuilder.class.getMethod("append", clsArr).invoke(sb, new Character(','));
                }
                Object[] objArr = {"@"};
                StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr);
                Object[] objArr2 = {domain};
                StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr2);
            } else {
                return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb, new Object[0]);
            }
        }
    }

    public String toString() {
        return (String) DomainList.class.getMethod("toRouteString", new Class[0]).invoke(this, new Object[0]);
    }
}
