package org.apache.james.mime4j.io;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import org.apache.james.mime4j.util.ByteArrayBuffer;

public class LineReaderInputStreamAdaptor extends LineReaderInputStream {
    private final LineReaderInputStream bis;
    private boolean eof;
    private final int maxLineLen;
    private boolean used;

    public LineReaderInputStreamAdaptor(InputStream is, int maxLineLen2) {
        super(is);
        this.used = false;
        this.eof = false;
        if (is instanceof LineReaderInputStream) {
            this.bis = (LineReaderInputStream) is;
        } else {
            this.bis = null;
        }
        this.maxLineLen = maxLineLen2;
    }

    public LineReaderInputStreamAdaptor(InputStream is) {
        this(is, -1);
    }

    public int read() throws IOException {
        int i = ((Integer) InputStream.class.getMethod("read", new Class[0]).invoke(this.in, new Object[0])).intValue();
        this.eof = i == -1;
        this.used = true;
        return i;
    }

    public int read(byte[] b, int off, int len) throws IOException {
        int i = ((Integer) InputStream.class.getMethod("read", byte[].class, Integer.TYPE, Integer.TYPE).invoke(this.in, b, new Integer(off), new Integer(len))).intValue();
        this.eof = i == -1;
        this.used = true;
        return i;
    }

    public int readLine(ByteArrayBuffer dst) throws IOException {
        int i;
        if (this.bis != null) {
            i = ((Integer) LineReaderInputStream.class.getMethod("readLine", ByteArrayBuffer.class).invoke(this.bis, dst)).intValue();
        } else {
            i = ((Integer) LineReaderInputStreamAdaptor.class.getMethod("doReadLine", ByteArrayBuffer.class).invoke(this, dst)).intValue();
        }
        this.eof = i == -1;
        this.used = true;
        return i;
    }

    private int doReadLine(ByteArrayBuffer dst) throws IOException {
        int ch;
        int total = 0;
        do {
            ch = ((Integer) InputStream.class.getMethod("read", new Class[0]).invoke(this.in, new Object[0])).intValue();
            if (ch == -1) {
                break;
            }
            Class[] clsArr = {Integer.TYPE};
            ByteArrayBuffer.class.getMethod("append", clsArr).invoke(dst, new Integer(ch));
            total++;
            if (this.maxLineLen > 0) {
                if (((Integer) ByteArrayBuffer.class.getMethod("length", new Class[0]).invoke(dst, new Object[0])).intValue() >= this.maxLineLen) {
                    throw new MaxLineLimitException("Maximum line length limit exceeded");
                }
            }
        } while (ch != 10);
        if (total == 0 && ch == -1) {
            return -1;
        }
        return total;
    }

    public boolean eof() {
        return this.eof;
    }

    public boolean isUsed() {
        return this.used;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        Class[] clsArr = {String.class};
        Object[] objArr = {"[LineReaderInputStreamAdaptor: "};
        Object[] objArr2 = {this.bis};
        Method method = StringBuilder.class.getMethod("append", Object.class);
        Object[] objArr3 = {"]"};
        Method method2 = StringBuilder.class.getMethod("append", String.class);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), objArr3), new Object[0]);
    }
}
