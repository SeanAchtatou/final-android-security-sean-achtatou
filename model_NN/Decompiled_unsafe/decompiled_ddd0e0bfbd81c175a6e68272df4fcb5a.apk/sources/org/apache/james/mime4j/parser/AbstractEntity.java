package org.apache.james.mime4j.parser;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.BitSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.descriptor.BodyDescriptor;
import org.apache.james.mime4j.descriptor.DefaultBodyDescriptor;
import org.apache.james.mime4j.descriptor.MaximalBodyDescriptor;
import org.apache.james.mime4j.descriptor.MutableBodyDescriptor;
import org.apache.james.mime4j.io.LineReaderInputStream;
import org.apache.james.mime4j.io.MaxHeaderLimitException;
import org.apache.james.mime4j.io.MaxLineLimitException;
import org.apache.james.mime4j.util.ByteArrayBuffer;

public abstract class AbstractEntity implements EntityStateMachine {
    private static final int T_IN_BODYPART = -2;
    private static final int T_IN_MESSAGE = -3;
    private static final BitSet fieldChars = new BitSet();
    protected final MutableBodyDescriptor body;
    protected final MimeEntityConfig config;
    private boolean endOfHeader = false;
    protected final int endState;
    private Field field;
    private int headerCount = 0;
    private int lineCount = 0;
    private final ByteArrayBuffer linebuf = new ByteArrayBuffer(64);
    protected final Log log;
    protected final BodyDescriptor parent;
    protected final int startState;
    protected int state;

    /* access modifiers changed from: protected */
    public abstract LineReaderInputStream getDataStream();

    /* access modifiers changed from: protected */
    public abstract int getLineNumber();

    static {
        for (int i = 33; i <= 57; i++) {
            BitSet bitSet = fieldChars;
            Class[] clsArr = {Integer.TYPE};
            BitSet.class.getMethod("set", clsArr).invoke(bitSet, new Integer(i));
        }
        for (int i2 = 59; i2 <= 126; i2++) {
            BitSet bitSet2 = fieldChars;
            Class[] clsArr2 = {Integer.TYPE};
            BitSet.class.getMethod("set", clsArr2).invoke(bitSet2, new Integer(i2));
        }
    }

    AbstractEntity(BodyDescriptor parent2, int startState2, int endState2, MimeEntityConfig config2) {
        Object[] objArr = {(Class) Object.class.getMethod("getClass", new Class[0]).invoke(this, new Object[0])};
        this.log = (Log) LogFactory.class.getMethod("getLog", Class.class).invoke(null, objArr);
        this.parent = parent2;
        this.state = startState2;
        this.startState = startState2;
        this.endState = endState2;
        this.config = config2;
        Object[] objArr2 = {parent2};
        this.body = (MutableBodyDescriptor) AbstractEntity.class.getMethod("newBodyDescriptor", BodyDescriptor.class).invoke(this, objArr2);
    }

    public int getState() {
        return this.state;
    }

    /* access modifiers changed from: protected */
    public MutableBodyDescriptor newBodyDescriptor(BodyDescriptor pParent) {
        if (((Boolean) MimeEntityConfig.class.getMethod("isMaximalBodyDescriptor", new Class[0]).invoke(this.config, new Object[0])).booleanValue()) {
            return new MaximalBodyDescriptor(pParent);
        }
        return new DefaultBodyDescriptor(pParent);
    }

    private ByteArrayBuffer fillFieldBuffer() throws IOException, MimeException {
        if (this.endOfHeader) {
            throw new IllegalStateException();
        }
        int maxLineLen = ((Integer) MimeEntityConfig.class.getMethod("getMaxLineLen", new Class[0]).invoke(this.config, new Object[0])).intValue();
        LineReaderInputStream instream = (LineReaderInputStream) AbstractEntity.class.getMethod("getDataStream", new Class[0]).invoke(this, new Object[0]);
        ByteArrayBuffer fieldbuf = new ByteArrayBuffer(64);
        while (true) {
            int len = ((Integer) ByteArrayBuffer.class.getMethod("length", new Class[0]).invoke(this.linebuf, new Object[0])).intValue();
            if (maxLineLen > 0) {
                if (((Integer) ByteArrayBuffer.class.getMethod("length", new Class[0]).invoke(fieldbuf, new Object[0])).intValue() + len >= maxLineLen) {
                    throw new MaxLineLimitException("Maximum line length limit exceeded");
                }
            }
            if (len > 0) {
                Method method = ByteArrayBuffer.class.getMethod("buffer", new Class[0]);
                ByteArrayBuffer.class.getMethod("append", byte[].class, Integer.TYPE, Integer.TYPE).invoke(fieldbuf, (byte[]) method.invoke(this.linebuf, new Object[0]), new Integer(0), new Integer(len));
            }
            ByteArrayBuffer.class.getMethod("clear", new Class[0]).invoke(this.linebuf, new Object[0]);
            if (((Integer) LineReaderInputStream.class.getMethod("readLine", ByteArrayBuffer.class).invoke(instream, this.linebuf)).intValue() == -1) {
                AbstractEntity.class.getMethod("monitor", Event.class).invoke(this, Event.HEADERS_PREMATURE_END);
                this.endOfHeader = true;
                break;
            }
            int len2 = ((Integer) ByteArrayBuffer.class.getMethod("length", new Class[0]).invoke(this.linebuf, new Object[0])).intValue();
            if (len2 > 0) {
                if (((Byte) ByteArrayBuffer.class.getMethod("byteAt", Integer.TYPE).invoke(this.linebuf, new Integer(len2 - 1))).byteValue() == 10) {
                    len2--;
                }
            }
            if (len2 > 0) {
                if (((Byte) ByteArrayBuffer.class.getMethod("byteAt", Integer.TYPE).invoke(this.linebuf, new Integer(len2 - 1))).byteValue() == 13) {
                    len2--;
                }
            }
            if (len2 == 0) {
                this.endOfHeader = true;
                break;
            }
            this.lineCount++;
            if (this.lineCount > 1) {
                int ch = ((Byte) ByteArrayBuffer.class.getMethod("byteAt", Integer.TYPE).invoke(this.linebuf, new Integer(0))).byteValue();
                if (!(ch == 32 || ch == 9)) {
                    break;
                }
            }
        }
        return fieldbuf;
    }

    /* access modifiers changed from: protected */
    public boolean parseField() throws MimeException, IOException {
        int maxHeaderLimit = ((Integer) MimeEntityConfig.class.getMethod("getMaxHeaderCount", new Class[0]).invoke(this.config, new Object[0])).intValue();
        while (!this.endOfHeader) {
            if (this.headerCount >= maxHeaderLimit) {
                throw new MaxHeaderLimitException("Maximum header limit exceeded");
            }
            ByteArrayBuffer fieldbuf = (ByteArrayBuffer) AbstractEntity.class.getMethod("fillFieldBuffer", new Class[0]).invoke(this, new Object[0]);
            this.headerCount++;
            int len = ((Integer) ByteArrayBuffer.class.getMethod("length", new Class[0]).invoke(fieldbuf, new Object[0])).intValue();
            if (len > 0) {
                if (((Byte) ByteArrayBuffer.class.getMethod("byteAt", Integer.TYPE).invoke(fieldbuf, new Integer(len - 1))).byteValue() == 10) {
                    len--;
                }
            }
            if (len > 0) {
                if (((Byte) ByteArrayBuffer.class.getMethod("byteAt", Integer.TYPE).invoke(fieldbuf, new Integer(len - 1))).byteValue() == 13) {
                    len--;
                }
            }
            ByteArrayBuffer.class.getMethod("setLength", Integer.TYPE).invoke(fieldbuf, new Integer(len));
            boolean valid = true;
            int pos = ((Integer) ByteArrayBuffer.class.getMethod("indexOf", Byte.TYPE).invoke(fieldbuf, new Byte((byte) 58))).intValue();
            if (pos > 0) {
                int i = 0;
                while (true) {
                    if (i >= pos) {
                        continue;
                        break;
                    }
                    BitSet bitSet = fieldChars;
                    Class[] clsArr = {Integer.TYPE};
                    Object[] objArr = {new Integer(i)};
                    if (!((Boolean) BitSet.class.getMethod("get", Integer.TYPE).invoke(bitSet, new Integer(((Byte) ByteArrayBuffer.class.getMethod("byteAt", clsArr).invoke(fieldbuf, objArr)).byteValue() & 255))).booleanValue()) {
                        AbstractEntity.class.getMethod("monitor", Event.class).invoke(this, Event.INALID_HEADER);
                        valid = false;
                        continue;
                        break;
                    }
                    i++;
                }
            } else {
                AbstractEntity.class.getMethod("monitor", Event.class).invoke(this, Event.INALID_HEADER);
                valid = false;
                continue;
            }
            if (valid) {
                this.field = new RawField(fieldbuf, pos);
                MutableBodyDescriptor.class.getMethod("addField", Field.class).invoke(this.body, this.field);
                return true;
            }
        }
        return false;
    }

    public BodyDescriptor getBodyDescriptor() {
        switch (((Integer) AbstractEntity.class.getMethod("getState", new Class[0]).invoke(this, new Object[0])).intValue()) {
            case -1:
            case 6:
            case 8:
            case 9:
            case 12:
                break;
            default:
                StringBuilder sb = new StringBuilder();
                Class[] clsArr = {String.class};
                Object[] objArr = {"Invalid state :"};
                int i = this.state;
                Class[] clsArr2 = {Integer.TYPE};
                Object[] objArr2 = {(String) AbstractEntity.class.getMethod("stateToString", clsArr2).invoke(null, new Integer(i))};
                Method method = StringBuilder.class.getMethod("append", String.class);
                throw new IllegalStateException((String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), new Object[0]));
        }
        return this.body;
    }

    public Field getField() {
        switch (((Integer) AbstractEntity.class.getMethod("getState", new Class[0]).invoke(this, new Object[0])).intValue()) {
            case 4:
                break;
            default:
                StringBuilder sb = new StringBuilder();
                Class[] clsArr = {String.class};
                Object[] objArr = {"Invalid state :"};
                int i = this.state;
                Class[] clsArr2 = {Integer.TYPE};
                Object[] objArr2 = {(String) AbstractEntity.class.getMethod("stateToString", clsArr2).invoke(null, new Integer(i))};
                Method method = StringBuilder.class.getMethod("append", String.class);
                throw new IllegalStateException((String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), new Object[0]));
        }
        return this.field;
    }

    /* access modifiers changed from: protected */
    public void monitor(Event event) throws MimeException, IOException {
        if (((Boolean) MimeEntityConfig.class.getMethod("isStrictParsing", new Class[0]).invoke(this.config, new Object[0])).booleanValue()) {
            throw new MimeParseEventException(event);
        }
        Object[] objArr = {event};
        AbstractEntity.class.getMethod("warn", Event.class).invoke(this, objArr);
    }

    /* access modifiers changed from: protected */
    public String message(Event event) {
        String message;
        if (event == null) {
            message = "Event is unexpectedly null.";
        } else {
            message = (String) Event.class.getMethod("toString", new Class[0]).invoke(event, new Object[0]);
        }
        int lineNumber = ((Integer) AbstractEntity.class.getMethod("getLineNumber", new Class[0]).invoke(this, new Object[0])).intValue();
        if (lineNumber <= 0) {
            return message;
        }
        StringBuilder sb = new StringBuilder();
        Class[] clsArr = {String.class};
        Object[] objArr = {"Line "};
        Class[] clsArr2 = {Integer.TYPE};
        Object[] objArr2 = {new Integer(lineNumber)};
        Method method = StringBuilder.class.getMethod("append", clsArr2);
        Object[] objArr3 = {": "};
        Method method2 = StringBuilder.class.getMethod("append", String.class);
        Object[] objArr4 = {message};
        Method method3 = StringBuilder.class.getMethod("append", String.class);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method3.invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), objArr3), objArr4), new Object[0]);
    }

    /* access modifiers changed from: protected */
    public void warn(Event event) {
        if (((Boolean) Log.class.getMethod("isWarnEnabled", new Class[0]).invoke(this.log, new Object[0])).booleanValue()) {
            Object[] objArr = {event};
            Object[] objArr2 = {(String) AbstractEntity.class.getMethod("message", Event.class).invoke(this, objArr)};
            Log.class.getMethod("warn", Object.class).invoke(this.log, objArr2);
        }
    }

    /* access modifiers changed from: protected */
    public void debug(Event event) {
        if (((Boolean) Log.class.getMethod("isDebugEnabled", new Class[0]).invoke(this.log, new Object[0])).booleanValue()) {
            Object[] objArr = {event};
            Object[] objArr2 = {(String) AbstractEntity.class.getMethod("message", Event.class).invoke(this, objArr)};
            Log.class.getMethod("debug", Object.class).invoke(this.log, objArr2);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        Class[] clsArr = {String.class};
        Object[] objArr = {((Class) Object.class.getMethod("getClass", new Class[0]).invoke(this, new Object[0])).getName()};
        Object[] objArr2 = {" ["};
        Method method = StringBuilder.class.getMethod("append", String.class);
        int i = this.state;
        Class[] clsArr2 = {Integer.TYPE};
        Object[] objArr3 = {(String) AbstractEntity.class.getMethod("stateToString", clsArr2).invoke(null, new Integer(i))};
        Method method2 = StringBuilder.class.getMethod("append", String.class);
        Object[] objArr4 = {"]["};
        Method method3 = StringBuilder.class.getMethod("append", String.class);
        Object[] objArr5 = {(String) MutableBodyDescriptor.class.getMethod("getMimeType", new Class[0]).invoke(this.body, new Object[0])};
        Method method4 = StringBuilder.class.getMethod("append", String.class);
        Object[] objArr6 = {"]["};
        Method method5 = StringBuilder.class.getMethod("append", String.class);
        Object[] objArr7 = {(String) MutableBodyDescriptor.class.getMethod("getBoundary", new Class[0]).invoke(this.body, new Object[0])};
        Method method6 = StringBuilder.class.getMethod("append", String.class);
        Object[] objArr8 = {"]"};
        Method method7 = StringBuilder.class.getMethod("append", String.class);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method7.invoke((StringBuilder) method6.invoke((StringBuilder) method5.invoke((StringBuilder) method4.invoke((StringBuilder) method3.invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), objArr3), objArr4), objArr5), objArr6), objArr7), objArr8), new Object[0]);
    }

    public static final String stateToString(int state2) {
        switch (state2) {
            case T_IN_MESSAGE /*-3*/:
                return "In message";
            case T_IN_BODYPART /*-2*/:
                return "Bodypart";
            case -1:
                return "End of stream";
            case 0:
                return "Start message";
            case 1:
                return "End message";
            case 2:
                return "Raw entity";
            case 3:
                return "Start header";
            case 4:
                return "Field";
            case 5:
                return "End header";
            case 6:
                return "Start multipart";
            case 7:
                return "End multipart";
            case 8:
                return "Preamble";
            case 9:
                return "Epilogue";
            case 10:
                return "Start bodypart";
            case 11:
                return "End bodypart";
            case 12:
                return "Body";
            default:
                return "Unknown";
        }
    }
}
