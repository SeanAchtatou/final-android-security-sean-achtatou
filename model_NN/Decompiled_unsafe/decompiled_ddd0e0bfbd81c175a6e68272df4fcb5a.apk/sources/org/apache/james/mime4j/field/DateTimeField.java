package org.apache.james.mime4j.field;

import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.Date;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.james.mime4j.field.datetime.DateTime;
import org.apache.james.mime4j.field.datetime.parser.DateTimeParser;
import org.apache.james.mime4j.field.datetime.parser.ParseException;
import org.apache.james.mime4j.field.datetime.parser.TokenMgrError;
import org.apache.james.mime4j.util.ByteSequence;

public class DateTimeField extends AbstractField {
    static final FieldParser PARSER = new FieldParser() {
        public ParsedField parse(String name, String body, ByteSequence raw) {
            return new DateTimeField(name, body, raw);
        }
    };
    private static Log log;
    private Date date;
    private ParseException parseException;
    private boolean parsed = false;

    static {
        Object[] objArr = {DateTimeField.class};
        log = (Log) LogFactory.class.getMethod("getLog", Class.class).invoke(null, objArr);
    }

    DateTimeField(String name, String body, ByteSequence raw) {
        super(name, body, raw);
    }

    public Date getDate() {
        if (!this.parsed) {
            DateTimeField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        return this.date;
    }

    public ParseException getParseException() {
        if (!this.parsed) {
            DateTimeField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        return this.parseException;
    }

    private void parse() {
        String body = (String) DateTimeField.class.getMethod("getBody", new Class[0]).invoke(this, new Object[0]);
        try {
            Method method = DateTimeParser.class.getMethod("parseAll", new Class[0]);
            this.date = (Date) DateTime.class.getMethod("getDate", new Class[0]).invoke((DateTime) method.invoke(new DateTimeParser(new StringReader(body)), new Object[0]), new Object[0]);
        } catch (ParseException e) {
            if (((Boolean) Log.class.getMethod("isDebugEnabled", new Class[0]).invoke(log, new Object[0])).booleanValue()) {
                Log log2 = log;
                Object[] objArr = {"Parsing value '"};
                Object[] objArr2 = {body};
                Method method2 = StringBuilder.class.getMethod("append", String.class);
                Object[] objArr3 = {"': "};
                Method method3 = StringBuilder.class.getMethod("append", String.class);
                Object[] objArr4 = {(String) ParseException.class.getMethod("getMessage", new Class[0]).invoke(e, new Object[0])};
                Method method4 = StringBuilder.class.getMethod("append", String.class);
                Method method5 = StringBuilder.class.getMethod("toString", new Class[0]);
                Object[] objArr5 = {(String) method5.invoke((StringBuilder) method4.invoke((StringBuilder) method3.invoke((StringBuilder) method2.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr), objArr2), objArr3), objArr4), new Object[0])};
                Log.class.getMethod("debug", Object.class).invoke(log2, objArr5);
            }
            this.parseException = e;
        } catch (TokenMgrError e2) {
            if (((Boolean) Log.class.getMethod("isDebugEnabled", new Class[0]).invoke(log, new Object[0])).booleanValue()) {
                Log log3 = log;
                Object[] objArr6 = {"Parsing value '"};
                Object[] objArr7 = {body};
                Method method6 = StringBuilder.class.getMethod("append", String.class);
                Object[] objArr8 = {"': "};
                Method method7 = StringBuilder.class.getMethod("append", String.class);
                Object[] objArr9 = {(String) TokenMgrError.class.getMethod("getMessage", new Class[0]).invoke(e2, new Object[0])};
                Method method8 = StringBuilder.class.getMethod("append", String.class);
                Method method9 = StringBuilder.class.getMethod("toString", new Class[0]);
                Object[] objArr10 = {(String) method9.invoke((StringBuilder) method8.invoke((StringBuilder) method7.invoke((StringBuilder) method6.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr6), objArr7), objArr8), objArr9), new Object[0])};
                Log.class.getMethod("debug", Object.class).invoke(log3, objArr10);
            }
            this.parseException = new ParseException((String) TokenMgrError.class.getMethod("getMessage", new Class[0]).invoke(e2, new Object[0]));
        }
        this.parsed = true;
    }
}
