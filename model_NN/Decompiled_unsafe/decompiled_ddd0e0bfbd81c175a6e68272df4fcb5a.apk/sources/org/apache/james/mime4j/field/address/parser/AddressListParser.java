package org.apache.james.mime4j.field.address.parser;

import java.io.InputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Vector;

public class AddressListParser implements AddressListParserConstants, AddressListParserTreeConstants {
    private static int[] jj_la1_0;
    private static int[] jj_la1_1;
    private final JJCalls[] jj_2_rtns;
    private int jj_endpos;
    private Vector<int[]> jj_expentries;
    private int[] jj_expentry;
    private int jj_gc;
    private int jj_gen;
    SimpleCharStream jj_input_stream;
    private int jj_kind;
    private int jj_la;
    private final int[] jj_la1;
    private Token jj_lastpos;
    private int[] jj_lasttokens;
    private final LookaheadSuccess jj_ls;
    public Token jj_nt;
    private int jj_ntk;
    private boolean jj_rescan;
    private Token jj_scanpos;
    private boolean jj_semLA;
    protected JJTAddressListParserState jjtree;
    public boolean lookingAhead;
    public Token token;
    public AddressListParserTokenManager token_source;

    public static void main(String[] args) throws ParseException {
        while (true) {
            try {
                AddressListParser parser = new AddressListParser(System.in);
                AddressListParser.class.getMethod("parseLine", new Class[0]).invoke(parser, new Object[0]);
                Method method = JJTAddressListParserState.class.getMethod("rootNode", new Class[0]);
                Method method2 = SimpleNode.class.getMethod("dump", String.class);
                method2.invoke((SimpleNode) ((Node) method.invoke(parser.jjtree, new Object[0])), "> ");
            } catch (Exception x) {
                Exception.class.getMethod("printStackTrace", new Class[0]).invoke(x, new Object[0]);
                return;
            }
        }
    }

    public ASTaddress_list parseAddressList() throws ParseException {
        try {
            AddressListParser.class.getMethod("parseAddressList0", new Class[0]).invoke(this, new Object[0]);
            return (ASTaddress_list) ((Node) JJTAddressListParserState.class.getMethod("rootNode", new Class[0]).invoke(this.jjtree, new Object[0]));
        } catch (TokenMgrError tme) {
            throw new ParseException((String) TokenMgrError.class.getMethod("getMessage", new Class[0]).invoke(tme, new Object[0]));
        }
    }

    public ASTaddress parseAddress() throws ParseException {
        try {
            AddressListParser.class.getMethod("parseAddress0", new Class[0]).invoke(this, new Object[0]);
            return (ASTaddress) ((Node) JJTAddressListParserState.class.getMethod("rootNode", new Class[0]).invoke(this.jjtree, new Object[0]));
        } catch (TokenMgrError tme) {
            throw new ParseException((String) TokenMgrError.class.getMethod("getMessage", new Class[0]).invoke(tme, new Object[0]));
        }
    }

    public ASTmailbox parseMailbox() throws ParseException {
        try {
            AddressListParser.class.getMethod("parseMailbox0", new Class[0]).invoke(this, new Object[0]);
            return (ASTmailbox) ((Node) JJTAddressListParserState.class.getMethod("rootNode", new Class[0]).invoke(this.jjtree, new Object[0]));
        } catch (TokenMgrError tme) {
            throw new ParseException((String) TokenMgrError.class.getMethod("getMessage", new Class[0]).invoke(tme, new Object[0]));
        }
    }

    /* access modifiers changed from: package-private */
    public void jjtreeOpenNodeScope(Node n) {
        Class[] clsArr = {Integer.TYPE};
        ((SimpleNode) n).firstToken = (Token) AddressListParser.class.getMethod("getToken", clsArr).invoke(this, new Integer(1));
    }

    /* access modifiers changed from: package-private */
    public void jjtreeCloseNodeScope(Node n) {
        Class[] clsArr = {Integer.TYPE};
        ((SimpleNode) n).lastToken = (Token) AddressListParser.class.getMethod("getToken", clsArr).invoke(this, new Integer(0));
    }

    public final void parseLine() throws ParseException {
        int i;
        AddressListParser.class.getMethod("address_list", new Class[0]).invoke(this, new Object[0]);
        if (this.jj_ntk == -1) {
            i = ((Integer) AddressListParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 1:
                Class[] clsArr = {Integer.TYPE};
                AddressListParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(1));
                break;
            default:
                this.jj_la1[0] = this.jj_gen;
                break;
        }
        Class[] clsArr2 = {Integer.TYPE};
        AddressListParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(2));
    }

    public final void parseAddressList0() throws ParseException {
        AddressListParser.class.getMethod("address_list", new Class[0]).invoke(this, new Object[0]);
        Class[] clsArr = {Integer.TYPE};
        AddressListParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(0));
    }

    public final void parseAddress0() throws ParseException {
        AddressListParser.class.getMethod("address", new Class[0]).invoke(this, new Object[0]);
        Class[] clsArr = {Integer.TYPE};
        AddressListParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(0));
    }

    public final void parseMailbox0() throws ParseException {
        AddressListParser.class.getMethod("mailbox", new Class[0]).invoke(this, new Object[0]);
        Class[] clsArr = {Integer.TYPE};
        AddressListParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(0));
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0139 A[SYNTHETIC, Splitter:B:29:0x0139] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x013d A[Catch:{ Throwable -> 0x00d5, all -> 0x00f9 }, FALL_THROUGH] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x007b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0061 A[Catch:{ Throwable -> 0x00d5, all -> 0x00f9 }] */
    public final void address_list() throws org.apache.james.mime4j.field.address.parser.ParseException {
        /*
            r12 = this;
            r7 = -1
            r6 = 1
            org.apache.james.mime4j.field.address.parser.ASTaddress_list r2 = new org.apache.james.mime4j.field.address.parser.ASTaddress_list
            r2.<init>(r6)
            r0 = 1
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r3 = r12.jjtree
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r11 = org.apache.james.mime4j.field.address.parser.Node.class
            r9[r8] = r11
            r10[r8] = r2
            java.lang.String r8 = "openNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r11 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r3, r10)
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r11 = org.apache.james.mime4j.field.address.parser.Node.class
            r9[r8] = r11
            r10[r8] = r2
            java.lang.String r8 = "jjtreeOpenNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r12, r10)
            int r3 = r12.jj_ntk     // Catch:{ Throwable -> 0x00d5 }
            if (r3 != r7) goto L_0x00c1
            r8 = 0
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x00d5 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x00d5 }
            java.lang.String r8 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ Throwable -> 0x00d5 }
            java.lang.Object r8 = r8.invoke(r12, r10)     // Catch:{ Throwable -> 0x00d5 }
            java.lang.Integer r8 = (java.lang.Integer) r8     // Catch:{ Throwable -> 0x00d5 }
            int r3 = r8.intValue()     // Catch:{ Throwable -> 0x00d5 }
        L_0x0053:
            switch(r3) {
                case 6: goto L_0x00c4;
                case 14: goto L_0x00c4;
                case 31: goto L_0x00c4;
                default: goto L_0x0056;
            }     // Catch:{ Throwable -> 0x00d5 }
        L_0x0056:
            int[] r3 = r12.jj_la1     // Catch:{ Throwable -> 0x00d5 }
            r4 = 1
            int r5 = r12.jj_gen     // Catch:{ Throwable -> 0x00d5 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x00d5 }
        L_0x005d:
            int r3 = r12.jj_ntk     // Catch:{ Throwable -> 0x00d5 }
            if (r3 != r7) goto L_0x0139
            r8 = 0
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x00d5 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x00d5 }
            java.lang.String r8 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ Throwable -> 0x00d5 }
            java.lang.Object r8 = r8.invoke(r12, r10)     // Catch:{ Throwable -> 0x00d5 }
            java.lang.Integer r8 = (java.lang.Integer) r8     // Catch:{ Throwable -> 0x00d5 }
            int r3 = r8.intValue()     // Catch:{ Throwable -> 0x00d5 }
        L_0x0078:
            switch(r3) {
                case 3: goto L_0x013d;
                default: goto L_0x007b;
            }     // Catch:{ Throwable -> 0x00d5 }
        L_0x007b:
            int[] r3 = r12.jj_la1     // Catch:{ Throwable -> 0x00d5 }
            r4 = 2
            int r5 = r12.jj_gen     // Catch:{ Throwable -> 0x00d5 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x00d5 }
            if (r0 == 0) goto L_0x00c0
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r3 = r12.jjtree
            r8 = 2
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r11 = org.apache.james.mime4j.field.address.parser.Node.class
            r9[r8] = r11
            r10[r8] = r2
            r8 = 1
            java.lang.Class r11 = java.lang.Boolean.TYPE
            r9[r8] = r11
            java.lang.Boolean r11 = new java.lang.Boolean
            r11.<init>(r6)
            r10[r8] = r11
            java.lang.String r8 = "closeNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r11 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r3, r10)
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r11 = org.apache.james.mime4j.field.address.parser.Node.class
            r9[r8] = r11
            r10[r8] = r2
            java.lang.String r8 = "jjtreeCloseNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r12, r10)
        L_0x00c0:
            return
        L_0x00c1:
            int r3 = r12.jj_ntk     // Catch:{ Throwable -> 0x00d5 }
            goto L_0x0053
        L_0x00c4:
            r8 = 0
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x00d5 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x00d5 }
            java.lang.String r8 = "address"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ Throwable -> 0x00d5 }
            r8.invoke(r12, r10)     // Catch:{ Throwable -> 0x00d5 }
            goto L_0x005d
        L_0x00d5:
            r1 = move-exception
            if (r0 == 0) goto L_0x0196
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r3 = r12.jjtree     // Catch:{ all -> 0x00f9 }
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ all -> 0x00f9 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ all -> 0x00f9 }
            r8 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r11 = org.apache.james.mime4j.field.address.parser.Node.class
            r9[r8] = r11     // Catch:{ all -> 0x00f9 }
            r10[r8] = r2     // Catch:{ all -> 0x00f9 }
            java.lang.String r8 = "clearNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r11 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ all -> 0x00f9 }
            r8.invoke(r3, r10)     // Catch:{ all -> 0x00f9 }
            r0 = 0
        L_0x00f2:
            boolean r3 = r1 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x00f9 }
            if (r3 == 0) goto L_0x01aa
            java.lang.RuntimeException r1 = (java.lang.RuntimeException) r1     // Catch:{ all -> 0x00f9 }
            throw r1     // Catch:{ all -> 0x00f9 }
        L_0x00f9:
            r3 = move-exception
            if (r0 == 0) goto L_0x0138
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r4 = r12.jjtree
            r8 = 2
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r11 = org.apache.james.mime4j.field.address.parser.Node.class
            r9[r8] = r11
            r10[r8] = r2
            r8 = 1
            java.lang.Class r11 = java.lang.Boolean.TYPE
            r9[r8] = r11
            java.lang.Boolean r11 = new java.lang.Boolean
            r11.<init>(r6)
            r10[r8] = r11
            java.lang.String r8 = "closeNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r11 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r4, r10)
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r11 = org.apache.james.mime4j.field.address.parser.Node.class
            r9[r8] = r11
            r10[r8] = r2
            java.lang.String r8 = "jjtreeCloseNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r12, r10)
        L_0x0138:
            throw r3
        L_0x0139:
            int r3 = r12.jj_ntk     // Catch:{ Throwable -> 0x00d5 }
            goto L_0x0078
        L_0x013d:
            r3 = 3
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x00d5 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x00d5 }
            r8 = 0
            java.lang.Class r11 = java.lang.Integer.TYPE     // Catch:{ Throwable -> 0x00d5 }
            r9[r8] = r11     // Catch:{ Throwable -> 0x00d5 }
            java.lang.Integer r11 = new java.lang.Integer     // Catch:{ Throwable -> 0x00d5 }
            r11.<init>(r3)     // Catch:{ Throwable -> 0x00d5 }
            r10[r8] = r11     // Catch:{ Throwable -> 0x00d5 }
            java.lang.String r8 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ Throwable -> 0x00d5 }
            r8.invoke(r12, r10)     // Catch:{ Throwable -> 0x00d5 }
            int r3 = r12.jj_ntk     // Catch:{ Throwable -> 0x00d5 }
            if (r3 != r7) goto L_0x0181
            r8 = 0
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x00d5 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x00d5 }
            java.lang.String r8 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ Throwable -> 0x00d5 }
            java.lang.Object r8 = r8.invoke(r12, r10)     // Catch:{ Throwable -> 0x00d5 }
            java.lang.Integer r8 = (java.lang.Integer) r8     // Catch:{ Throwable -> 0x00d5 }
            int r3 = r8.intValue()     // Catch:{ Throwable -> 0x00d5 }
        L_0x0175:
            switch(r3) {
                case 6: goto L_0x0184;
                case 14: goto L_0x0184;
                case 31: goto L_0x0184;
                default: goto L_0x0178;
            }     // Catch:{ Throwable -> 0x00d5 }
        L_0x0178:
            int[] r3 = r12.jj_la1     // Catch:{ Throwable -> 0x00d5 }
            r4 = 3
            int r5 = r12.jj_gen     // Catch:{ Throwable -> 0x00d5 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x00d5 }
            goto L_0x005d
        L_0x0181:
            int r3 = r12.jj_ntk     // Catch:{ Throwable -> 0x00d5 }
            goto L_0x0175
        L_0x0184:
            r8 = 0
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x00d5 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x00d5 }
            java.lang.String r8 = "address"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ Throwable -> 0x00d5 }
            r8.invoke(r12, r10)     // Catch:{ Throwable -> 0x00d5 }
            goto L_0x005d
        L_0x0196:
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r3 = r12.jjtree     // Catch:{ all -> 0x00f9 }
            r8 = 0
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ all -> 0x00f9 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ all -> 0x00f9 }
            java.lang.String r8 = "popNode"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r11 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ all -> 0x00f9 }
            r8.invoke(r3, r10)     // Catch:{ all -> 0x00f9 }
            goto L_0x00f2
        L_0x01aa:
            boolean r3 = r1 instanceof org.apache.james.mime4j.field.address.parser.ParseException     // Catch:{ all -> 0x00f9 }
            if (r3 == 0) goto L_0x01b1
            org.apache.james.mime4j.field.address.parser.ParseException r1 = (org.apache.james.mime4j.field.address.parser.ParseException) r1     // Catch:{ all -> 0x00f9 }
            throw r1     // Catch:{ all -> 0x00f9 }
        L_0x01b1:
            java.lang.Error r1 = (java.lang.Error) r1     // Catch:{ all -> 0x00f9 }
            throw r1     // Catch:{ all -> 0x00f9 }
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.address.parser.AddressListParser.address_list():void");
    }

    public final void address() throws ParseException {
        int i;
        int i2;
        ASTaddress jjtn000 = new ASTaddress(2);
        Object[] objArr = {jjtn000};
        JJTAddressListParserState.class.getMethod("openNodeScope", Node.class).invoke(this.jjtree, objArr);
        Object[] objArr2 = {jjtn000};
        AddressListParser.class.getMethod("jjtreeOpenNodeScope", Node.class).invoke(this, objArr2);
        try {
            Class[] clsArr = {Integer.TYPE};
            if (((Boolean) AddressListParser.class.getMethod("jj_2_1", clsArr).invoke(this, new Integer(Integer.MAX_VALUE))).booleanValue()) {
                AddressListParser.class.getMethod("addr_spec", new Class[0]).invoke(this, new Object[0]);
            } else {
                if (this.jj_ntk == -1) {
                    i = ((Integer) AddressListParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
                } else {
                    i = this.jj_ntk;
                }
                switch (i) {
                    case 6:
                        AddressListParser.class.getMethod("angle_addr", new Class[0]).invoke(this, new Object[0]);
                        break;
                    case 14:
                    case AddressListParserConstants.QUOTEDSTRING:
                        AddressListParser.class.getMethod("phrase", new Class[0]).invoke(this, new Object[0]);
                        if (this.jj_ntk == -1) {
                            i2 = ((Integer) AddressListParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
                        } else {
                            i2 = this.jj_ntk;
                        }
                        switch (i2) {
                            case 4:
                                AddressListParser.class.getMethod("group_body", new Class[0]).invoke(this, new Object[0]);
                                break;
                            case 5:
                            default:
                                this.jj_la1[4] = this.jj_gen;
                                Class[] clsArr2 = {Integer.TYPE};
                                AddressListParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(-1));
                                throw new ParseException();
                            case 6:
                                AddressListParser.class.getMethod("angle_addr", new Class[0]).invoke(this, new Object[0]);
                                break;
                        }
                    default:
                        this.jj_la1[5] = this.jj_gen;
                        Class[] clsArr3 = {Integer.TYPE};
                        AddressListParser.class.getMethod("jj_consume_token", clsArr3).invoke(this, new Integer(-1));
                        throw new ParseException();
                }
            }
            if (1 != 0) {
                JJTAddressListParserState jJTAddressListParserState = this.jjtree;
                Class[] clsArr4 = {Node.class, Boolean.TYPE};
                JJTAddressListParserState.class.getMethod("closeNodeScope", clsArr4).invoke(jJTAddressListParserState, jjtn000, new Boolean(true));
                Object[] objArr3 = {jjtn000};
                AddressListParser.class.getMethod("jjtreeCloseNodeScope", Node.class).invoke(this, objArr3);
            }
        } catch (Throwable th) {
            if (1 != 0) {
                JJTAddressListParserState jJTAddressListParserState2 = this.jjtree;
                Class[] clsArr5 = {Node.class, Boolean.TYPE};
                JJTAddressListParserState.class.getMethod("closeNodeScope", clsArr5).invoke(jJTAddressListParserState2, jjtn000, new Boolean(true));
                Object[] objArr4 = {jjtn000};
                AddressListParser.class.getMethod("jjtreeCloseNodeScope", Node.class).invoke(this, objArr4);
            }
            throw th;
        }
    }

    public final void mailbox() throws ParseException {
        int i;
        ASTmailbox jjtn000 = new ASTmailbox(3);
        Object[] objArr = {jjtn000};
        JJTAddressListParserState.class.getMethod("openNodeScope", Node.class).invoke(this.jjtree, objArr);
        Object[] objArr2 = {jjtn000};
        AddressListParser.class.getMethod("jjtreeOpenNodeScope", Node.class).invoke(this, objArr2);
        try {
            Class[] clsArr = {Integer.TYPE};
            if (((Boolean) AddressListParser.class.getMethod("jj_2_2", clsArr).invoke(this, new Integer(Integer.MAX_VALUE))).booleanValue()) {
                AddressListParser.class.getMethod("addr_spec", new Class[0]).invoke(this, new Object[0]);
            } else {
                if (this.jj_ntk == -1) {
                    i = ((Integer) AddressListParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
                } else {
                    i = this.jj_ntk;
                }
                switch (i) {
                    case 6:
                        AddressListParser.class.getMethod("angle_addr", new Class[0]).invoke(this, new Object[0]);
                        break;
                    case 14:
                    case AddressListParserConstants.QUOTEDSTRING:
                        AddressListParser.class.getMethod("name_addr", new Class[0]).invoke(this, new Object[0]);
                        break;
                    default:
                        this.jj_la1[6] = this.jj_gen;
                        Class[] clsArr2 = {Integer.TYPE};
                        AddressListParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(-1));
                        throw new ParseException();
                }
            }
            if (1 != 0) {
                JJTAddressListParserState jJTAddressListParserState = this.jjtree;
                Class[] clsArr3 = {Node.class, Boolean.TYPE};
                JJTAddressListParserState.class.getMethod("closeNodeScope", clsArr3).invoke(jJTAddressListParserState, jjtn000, new Boolean(true));
                Object[] objArr3 = {jjtn000};
                AddressListParser.class.getMethod("jjtreeCloseNodeScope", Node.class).invoke(this, objArr3);
            }
        } catch (Throwable th) {
            if (1 != 0) {
                JJTAddressListParserState jJTAddressListParserState2 = this.jjtree;
                Class[] clsArr4 = {Node.class, Boolean.TYPE};
                JJTAddressListParserState.class.getMethod("closeNodeScope", clsArr4).invoke(jJTAddressListParserState2, jjtn000, new Boolean(true));
                Object[] objArr4 = {jjtn000};
                AddressListParser.class.getMethod("jjtreeCloseNodeScope", Node.class).invoke(this, objArr4);
            }
            throw th;
        }
    }

    public final void name_addr() throws ParseException {
        ASTname_addr jjtn000 = new ASTname_addr(4);
        Object[] objArr = {jjtn000};
        JJTAddressListParserState.class.getMethod("openNodeScope", Node.class).invoke(this.jjtree, objArr);
        Object[] objArr2 = {jjtn000};
        AddressListParser.class.getMethod("jjtreeOpenNodeScope", Node.class).invoke(this, objArr2);
        try {
            AddressListParser.class.getMethod("phrase", new Class[0]).invoke(this, new Object[0]);
            AddressListParser.class.getMethod("angle_addr", new Class[0]).invoke(this, new Object[0]);
            if (1 != 0) {
                JJTAddressListParserState jJTAddressListParserState = this.jjtree;
                Class[] clsArr = {Node.class, Boolean.TYPE};
                JJTAddressListParserState.class.getMethod("closeNodeScope", clsArr).invoke(jJTAddressListParserState, jjtn000, new Boolean(true));
                Object[] objArr3 = {jjtn000};
                AddressListParser.class.getMethod("jjtreeCloseNodeScope", Node.class).invoke(this, objArr3);
            }
        } catch (Throwable th) {
            if (1 != 0) {
                JJTAddressListParserState jJTAddressListParserState2 = this.jjtree;
                Class[] clsArr2 = {Node.class, Boolean.TYPE};
                JJTAddressListParserState.class.getMethod("closeNodeScope", clsArr2).invoke(jJTAddressListParserState2, jjtn000, new Boolean(true));
                Object[] objArr4 = {jjtn000};
                AddressListParser.class.getMethod("jjtreeCloseNodeScope", Node.class).invoke(this, objArr4);
            }
            throw th;
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0177 A[SYNTHETIC, Splitter:B:29:0x0177] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x017b A[Catch:{ Throwable -> 0x0113, all -> 0x0137 }, FALL_THROUGH] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0099 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x007f A[Catch:{ Throwable -> 0x0113, all -> 0x0137 }] */
    public final void group_body() throws org.apache.james.mime4j.field.address.parser.ParseException {
        /*
            r12 = this;
            r3 = 5
            r7 = 1
            r6 = -1
            org.apache.james.mime4j.field.address.parser.ASTgroup_body r2 = new org.apache.james.mime4j.field.address.parser.ASTgroup_body
            r2.<init>(r3)
            r0 = 1
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r3 = r12.jjtree
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r11 = org.apache.james.mime4j.field.address.parser.Node.class
            r9[r8] = r11
            r10[r8] = r2
            java.lang.String r8 = "openNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r11 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r3, r10)
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r11 = org.apache.james.mime4j.field.address.parser.Node.class
            r9[r8] = r11
            r10[r8] = r2
            java.lang.String r8 = "jjtreeOpenNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r12, r10)
            r3 = 4
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x0113 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0113 }
            r8 = 0
            java.lang.Class r11 = java.lang.Integer.TYPE     // Catch:{ Throwable -> 0x0113 }
            r9[r8] = r11     // Catch:{ Throwable -> 0x0113 }
            java.lang.Integer r11 = new java.lang.Integer     // Catch:{ Throwable -> 0x0113 }
            r11.<init>(r3)     // Catch:{ Throwable -> 0x0113 }
            r10[r8] = r11     // Catch:{ Throwable -> 0x0113 }
            java.lang.String r8 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ Throwable -> 0x0113 }
            r8.invoke(r12, r10)     // Catch:{ Throwable -> 0x0113 }
            int r3 = r12.jj_ntk     // Catch:{ Throwable -> 0x0113 }
            if (r3 != r6) goto L_0x00fd
            r8 = 0
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x0113 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0113 }
            java.lang.String r8 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ Throwable -> 0x0113 }
            java.lang.Object r8 = r8.invoke(r12, r10)     // Catch:{ Throwable -> 0x0113 }
            java.lang.Integer r8 = (java.lang.Integer) r8     // Catch:{ Throwable -> 0x0113 }
            int r3 = r8.intValue()     // Catch:{ Throwable -> 0x0113 }
        L_0x0071:
            switch(r3) {
                case 6: goto L_0x0101;
                case 14: goto L_0x0101;
                case 31: goto L_0x0101;
                default: goto L_0x0074;
            }     // Catch:{ Throwable -> 0x0113 }
        L_0x0074:
            int[] r3 = r12.jj_la1     // Catch:{ Throwable -> 0x0113 }
            r4 = 7
            int r5 = r12.jj_gen     // Catch:{ Throwable -> 0x0113 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x0113 }
        L_0x007b:
            int r3 = r12.jj_ntk     // Catch:{ Throwable -> 0x0113 }
            if (r3 != r6) goto L_0x0177
            r8 = 0
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x0113 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0113 }
            java.lang.String r8 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ Throwable -> 0x0113 }
            java.lang.Object r8 = r8.invoke(r12, r10)     // Catch:{ Throwable -> 0x0113 }
            java.lang.Integer r8 = (java.lang.Integer) r8     // Catch:{ Throwable -> 0x0113 }
            int r3 = r8.intValue()     // Catch:{ Throwable -> 0x0113 }
        L_0x0096:
            switch(r3) {
                case 3: goto L_0x017b;
                default: goto L_0x0099;
            }     // Catch:{ Throwable -> 0x0113 }
        L_0x0099:
            int[] r3 = r12.jj_la1     // Catch:{ Throwable -> 0x0113 }
            r4 = 8
            int r5 = r12.jj_gen     // Catch:{ Throwable -> 0x0113 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x0113 }
            r3 = 5
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x0113 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0113 }
            r8 = 0
            java.lang.Class r11 = java.lang.Integer.TYPE     // Catch:{ Throwable -> 0x0113 }
            r9[r8] = r11     // Catch:{ Throwable -> 0x0113 }
            java.lang.Integer r11 = new java.lang.Integer     // Catch:{ Throwable -> 0x0113 }
            r11.<init>(r3)     // Catch:{ Throwable -> 0x0113 }
            r10[r8] = r11     // Catch:{ Throwable -> 0x0113 }
            java.lang.String r8 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ Throwable -> 0x0113 }
            r8.invoke(r12, r10)     // Catch:{ Throwable -> 0x0113 }
            if (r0 == 0) goto L_0x00fc
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r3 = r12.jjtree
            r8 = 2
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r11 = org.apache.james.mime4j.field.address.parser.Node.class
            r9[r8] = r11
            r10[r8] = r2
            r8 = 1
            java.lang.Class r11 = java.lang.Boolean.TYPE
            r9[r8] = r11
            java.lang.Boolean r11 = new java.lang.Boolean
            r11.<init>(r7)
            r10[r8] = r11
            java.lang.String r8 = "closeNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r11 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r3, r10)
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r11 = org.apache.james.mime4j.field.address.parser.Node.class
            r9[r8] = r11
            r10[r8] = r2
            java.lang.String r8 = "jjtreeCloseNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r12, r10)
        L_0x00fc:
            return
        L_0x00fd:
            int r3 = r12.jj_ntk     // Catch:{ Throwable -> 0x0113 }
            goto L_0x0071
        L_0x0101:
            r8 = 0
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x0113 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0113 }
            java.lang.String r8 = "mailbox"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ Throwable -> 0x0113 }
            r8.invoke(r12, r10)     // Catch:{ Throwable -> 0x0113 }
            goto L_0x007b
        L_0x0113:
            r1 = move-exception
            if (r0 == 0) goto L_0x01d5
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r3 = r12.jjtree     // Catch:{ all -> 0x0137 }
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ all -> 0x0137 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ all -> 0x0137 }
            r8 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r11 = org.apache.james.mime4j.field.address.parser.Node.class
            r9[r8] = r11     // Catch:{ all -> 0x0137 }
            r10[r8] = r2     // Catch:{ all -> 0x0137 }
            java.lang.String r8 = "clearNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r11 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ all -> 0x0137 }
            r8.invoke(r3, r10)     // Catch:{ all -> 0x0137 }
            r0 = 0
        L_0x0130:
            boolean r3 = r1 instanceof java.lang.RuntimeException     // Catch:{ all -> 0x0137 }
            if (r3 == 0) goto L_0x01e9
            java.lang.RuntimeException r1 = (java.lang.RuntimeException) r1     // Catch:{ all -> 0x0137 }
            throw r1     // Catch:{ all -> 0x0137 }
        L_0x0137:
            r3 = move-exception
            if (r0 == 0) goto L_0x0176
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r4 = r12.jjtree
            r8 = 2
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r11 = org.apache.james.mime4j.field.address.parser.Node.class
            r9[r8] = r11
            r10[r8] = r2
            r8 = 1
            java.lang.Class r11 = java.lang.Boolean.TYPE
            r9[r8] = r11
            java.lang.Boolean r11 = new java.lang.Boolean
            r11.<init>(r7)
            r10[r8] = r11
            java.lang.String r8 = "closeNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r11 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r4, r10)
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]
            java.lang.Object[] r10 = new java.lang.Object[r8]
            r8 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r11 = org.apache.james.mime4j.field.address.parser.Node.class
            r9[r8] = r11
            r10[r8] = r2
            java.lang.String r8 = "jjtreeCloseNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)
            r8.invoke(r12, r10)
        L_0x0176:
            throw r3
        L_0x0177:
            int r3 = r12.jj_ntk     // Catch:{ Throwable -> 0x0113 }
            goto L_0x0096
        L_0x017b:
            r3 = 3
            r8 = 1
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x0113 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0113 }
            r8 = 0
            java.lang.Class r11 = java.lang.Integer.TYPE     // Catch:{ Throwable -> 0x0113 }
            r9[r8] = r11     // Catch:{ Throwable -> 0x0113 }
            java.lang.Integer r11 = new java.lang.Integer     // Catch:{ Throwable -> 0x0113 }
            r11.<init>(r3)     // Catch:{ Throwable -> 0x0113 }
            r10[r8] = r11     // Catch:{ Throwable -> 0x0113 }
            java.lang.String r8 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ Throwable -> 0x0113 }
            r8.invoke(r12, r10)     // Catch:{ Throwable -> 0x0113 }
            int r3 = r12.jj_ntk     // Catch:{ Throwable -> 0x0113 }
            if (r3 != r6) goto L_0x01c0
            r8 = 0
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x0113 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0113 }
            java.lang.String r8 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ Throwable -> 0x0113 }
            java.lang.Object r8 = r8.invoke(r12, r10)     // Catch:{ Throwable -> 0x0113 }
            java.lang.Integer r8 = (java.lang.Integer) r8     // Catch:{ Throwable -> 0x0113 }
            int r3 = r8.intValue()     // Catch:{ Throwable -> 0x0113 }
        L_0x01b3:
            switch(r3) {
                case 6: goto L_0x01c3;
                case 14: goto L_0x01c3;
                case 31: goto L_0x01c3;
                default: goto L_0x01b6;
            }     // Catch:{ Throwable -> 0x0113 }
        L_0x01b6:
            int[] r3 = r12.jj_la1     // Catch:{ Throwable -> 0x0113 }
            r4 = 9
            int r5 = r12.jj_gen     // Catch:{ Throwable -> 0x0113 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x0113 }
            goto L_0x007b
        L_0x01c0:
            int r3 = r12.jj_ntk     // Catch:{ Throwable -> 0x0113 }
            goto L_0x01b3
        L_0x01c3:
            r8 = 0
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ Throwable -> 0x0113 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ Throwable -> 0x0113 }
            java.lang.String r8 = "mailbox"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r11 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ Throwable -> 0x0113 }
            r8.invoke(r12, r10)     // Catch:{ Throwable -> 0x0113 }
            goto L_0x007b
        L_0x01d5:
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r3 = r12.jjtree     // Catch:{ all -> 0x0137 }
            r8 = 0
            java.lang.Class[] r9 = new java.lang.Class[r8]     // Catch:{ all -> 0x0137 }
            java.lang.Object[] r10 = new java.lang.Object[r8]     // Catch:{ all -> 0x0137 }
            java.lang.String r8 = "popNode"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r11 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r8 = r11.getMethod(r8, r9)     // Catch:{ all -> 0x0137 }
            r8.invoke(r3, r10)     // Catch:{ all -> 0x0137 }
            goto L_0x0130
        L_0x01e9:
            boolean r3 = r1 instanceof org.apache.james.mime4j.field.address.parser.ParseException     // Catch:{ all -> 0x0137 }
            if (r3 == 0) goto L_0x01f0
            org.apache.james.mime4j.field.address.parser.ParseException r1 = (org.apache.james.mime4j.field.address.parser.ParseException) r1     // Catch:{ all -> 0x0137 }
            throw r1     // Catch:{ all -> 0x0137 }
        L_0x01f0:
            java.lang.Error r1 = (java.lang.Error) r1     // Catch:{ all -> 0x0137 }
            throw r1     // Catch:{ all -> 0x0137 }
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.address.parser.AddressListParser.group_body():void");
    }

    public final void angle_addr() throws ParseException {
        int i;
        ASTangle_addr jjtn000 = new ASTangle_addr(6);
        Object[] objArr = {jjtn000};
        JJTAddressListParserState.class.getMethod("openNodeScope", Node.class).invoke(this.jjtree, objArr);
        Object[] objArr2 = {jjtn000};
        AddressListParser.class.getMethod("jjtreeOpenNodeScope", Node.class).invoke(this, objArr2);
        try {
            Class[] clsArr = {Integer.TYPE};
            AddressListParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(6));
            if (this.jj_ntk == -1) {
                i = ((Integer) AddressListParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
            } else {
                i = this.jj_ntk;
            }
            switch (i) {
                case 8:
                    AddressListParser.class.getMethod("route", new Class[0]).invoke(this, new Object[0]);
                    break;
                default:
                    this.jj_la1[10] = this.jj_gen;
                    break;
            }
            AddressListParser.class.getMethod("addr_spec", new Class[0]).invoke(this, new Object[0]);
            Class[] clsArr2 = {Integer.TYPE};
            AddressListParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(7));
            if (1 != 0) {
                JJTAddressListParserState jJTAddressListParserState = this.jjtree;
                Class[] clsArr3 = {Node.class, Boolean.TYPE};
                JJTAddressListParserState.class.getMethod("closeNodeScope", clsArr3).invoke(jJTAddressListParserState, jjtn000, new Boolean(true));
                Object[] objArr3 = {jjtn000};
                AddressListParser.class.getMethod("jjtreeCloseNodeScope", Node.class).invoke(this, objArr3);
            }
        } catch (Throwable th) {
            if (1 != 0) {
                JJTAddressListParserState jJTAddressListParserState2 = this.jjtree;
                Class[] clsArr4 = {Node.class, Boolean.TYPE};
                JJTAddressListParserState.class.getMethod("closeNodeScope", clsArr4).invoke(jJTAddressListParserState2, jjtn000, new Boolean(true));
                Object[] objArr4 = {jjtn000};
                AddressListParser.class.getMethod("jjtreeCloseNodeScope", Node.class).invoke(this, objArr4);
            }
            throw th;
        }
    }

    public final void route() throws ParseException {
        int i;
        int i2;
        ASTroute jjtn000 = new ASTroute(7);
        Object[] objArr = {jjtn000};
        JJTAddressListParserState.class.getMethod("openNodeScope", Node.class).invoke(this.jjtree, objArr);
        Object[] objArr2 = {jjtn000};
        AddressListParser.class.getMethod("jjtreeOpenNodeScope", Node.class).invoke(this, objArr2);
        try {
            Class[] clsArr = {Integer.TYPE};
            AddressListParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(8));
            AddressListParser.class.getMethod("domain", new Class[0]).invoke(this, new Object[0]);
            while (true) {
                if (this.jj_ntk == -1) {
                    i = ((Integer) AddressListParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
                } else {
                    i = this.jj_ntk;
                }
                switch (i) {
                    case 3:
                    case 8:
                        break;
                    default:
                        this.jj_la1[11] = this.jj_gen;
                        Class[] clsArr2 = {Integer.TYPE};
                        AddressListParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(4));
                        if (1 != 0) {
                            JJTAddressListParserState jJTAddressListParserState = this.jjtree;
                            Class[] clsArr3 = {Node.class, Boolean.TYPE};
                            JJTAddressListParserState.class.getMethod("closeNodeScope", clsArr3).invoke(jJTAddressListParserState, jjtn000, new Boolean(true));
                            Object[] objArr3 = {jjtn000};
                            AddressListParser.class.getMethod("jjtreeCloseNodeScope", Node.class).invoke(this, objArr3);
                            return;
                        }
                        return;
                }
                while (true) {
                    if (this.jj_ntk == -1) {
                        i2 = ((Integer) AddressListParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
                    } else {
                        i2 = this.jj_ntk;
                    }
                    switch (i2) {
                        case 3:
                            Class[] clsArr4 = {Integer.TYPE};
                            AddressListParser.class.getMethod("jj_consume_token", clsArr4).invoke(this, new Integer(3));
                        default:
                            this.jj_la1[12] = this.jj_gen;
                            Class[] clsArr5 = {Integer.TYPE};
                            AddressListParser.class.getMethod("jj_consume_token", clsArr5).invoke(this, new Integer(8));
                            AddressListParser.class.getMethod("domain", new Class[0]).invoke(this, new Object[0]);
                    }
                }
            }
        } catch (Throwable th) {
            if (1 != 0) {
                JJTAddressListParserState jJTAddressListParserState2 = this.jjtree;
                Class[] clsArr6 = {Node.class, Boolean.TYPE};
                JJTAddressListParserState.class.getMethod("closeNodeScope", clsArr6).invoke(jJTAddressListParserState2, jjtn000, new Boolean(true));
                Object[] objArr4 = {jjtn000};
                AddressListParser.class.getMethod("jjtreeCloseNodeScope", Node.class).invoke(this, objArr4);
            }
            throw th;
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x00c3 A[SYNTHETIC, Splitter:B:12:0x00c3] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00c6 A[Catch:{ all -> 0x0083 }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00e8 A[Catch:{ all -> 0x0083 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0169 A[Catch:{ all -> 0x0083 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0058 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0102 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:4:0x003e A[Catch:{ all -> 0x0083 }] */
    public final void phrase() throws org.apache.james.mime4j.field.address.parser.ParseException {
        /*
            r10 = this;
            r5 = 1
            r3 = -1
            org.apache.james.mime4j.field.address.parser.ASTphrase r1 = new org.apache.james.mime4j.field.address.parser.ASTphrase
            r2 = 8
            r1.<init>(r2)
            r0 = 1
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r2 = r10.jjtree
            r6 = 1
            java.lang.Class[] r7 = new java.lang.Class[r6]
            java.lang.Object[] r8 = new java.lang.Object[r6]
            r6 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r9 = org.apache.james.mime4j.field.address.parser.Node.class
            r7[r6] = r9
            r8[r6] = r1
            java.lang.String r6 = "openNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r9 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r6 = r9.getMethod(r6, r7)
            r6.invoke(r2, r8)
            r6 = 1
            java.lang.Class[] r7 = new java.lang.Class[r6]
            java.lang.Object[] r8 = new java.lang.Object[r6]
            r6 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r9 = org.apache.james.mime4j.field.address.parser.Node.class
            r7[r6] = r9
            r8[r6] = r1
            java.lang.String r6 = "jjtreeOpenNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r9 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r6 = r9.getMethod(r6, r7)
            r6.invoke(r10, r8)
        L_0x003a:
            int r2 = r10.jj_ntk     // Catch:{ all -> 0x0083 }
            if (r2 != r3) goto L_0x00c3
            r6 = 0
            java.lang.Class[] r7 = new java.lang.Class[r6]     // Catch:{ all -> 0x0083 }
            java.lang.Object[] r8 = new java.lang.Object[r6]     // Catch:{ all -> 0x0083 }
            java.lang.String r6 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r9 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r6 = r9.getMethod(r6, r7)     // Catch:{ all -> 0x0083 }
            java.lang.Object r6 = r6.invoke(r10, r8)     // Catch:{ all -> 0x0083 }
            java.lang.Integer r6 = (java.lang.Integer) r6     // Catch:{ all -> 0x0083 }
            int r2 = r6.intValue()     // Catch:{ all -> 0x0083 }
        L_0x0055:
            switch(r2) {
                case 14: goto L_0x00c6;
                case 31: goto L_0x0149;
                default: goto L_0x0058;
            }     // Catch:{ all -> 0x0083 }
        L_0x0058:
            int[] r2 = r10.jj_la1     // Catch:{ all -> 0x0083 }
            r3 = 13
            int r4 = r10.jj_gen     // Catch:{ all -> 0x0083 }
            r2[r3] = r4     // Catch:{ all -> 0x0083 }
            r2 = -1
            r6 = 1
            java.lang.Class[] r7 = new java.lang.Class[r6]     // Catch:{ all -> 0x0083 }
            java.lang.Object[] r8 = new java.lang.Object[r6]     // Catch:{ all -> 0x0083 }
            r6 = 0
            java.lang.Class r9 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0083 }
            r7[r6] = r9     // Catch:{ all -> 0x0083 }
            java.lang.Integer r9 = new java.lang.Integer     // Catch:{ all -> 0x0083 }
            r9.<init>(r2)     // Catch:{ all -> 0x0083 }
            r8[r6] = r9     // Catch:{ all -> 0x0083 }
            java.lang.String r6 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r9 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r6 = r9.getMethod(r6, r7)     // Catch:{ all -> 0x0083 }
            r6.invoke(r10, r8)     // Catch:{ all -> 0x0083 }
            org.apache.james.mime4j.field.address.parser.ParseException r2 = new org.apache.james.mime4j.field.address.parser.ParseException     // Catch:{ all -> 0x0083 }
            r2.<init>()     // Catch:{ all -> 0x0083 }
            throw r2     // Catch:{ all -> 0x0083 }
        L_0x0083:
            r2 = move-exception
            if (r0 == 0) goto L_0x00c2
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r3 = r10.jjtree
            r6 = 2
            java.lang.Class[] r7 = new java.lang.Class[r6]
            java.lang.Object[] r8 = new java.lang.Object[r6]
            r6 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r9 = org.apache.james.mime4j.field.address.parser.Node.class
            r7[r6] = r9
            r8[r6] = r1
            r6 = 1
            java.lang.Class r9 = java.lang.Boolean.TYPE
            r7[r6] = r9
            java.lang.Boolean r9 = new java.lang.Boolean
            r9.<init>(r5)
            r8[r6] = r9
            java.lang.String r6 = "closeNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r9 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r6 = r9.getMethod(r6, r7)
            r6.invoke(r3, r8)
            r6 = 1
            java.lang.Class[] r7 = new java.lang.Class[r6]
            java.lang.Object[] r8 = new java.lang.Object[r6]
            r6 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r9 = org.apache.james.mime4j.field.address.parser.Node.class
            r7[r6] = r9
            r8[r6] = r1
            java.lang.String r6 = "jjtreeCloseNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r9 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r6 = r9.getMethod(r6, r7)
            r6.invoke(r10, r8)
        L_0x00c2:
            throw r2
        L_0x00c3:
            int r2 = r10.jj_ntk     // Catch:{ all -> 0x0083 }
            goto L_0x0055
        L_0x00c6:
            r2 = 14
            r6 = 1
            java.lang.Class[] r7 = new java.lang.Class[r6]     // Catch:{ all -> 0x0083 }
            java.lang.Object[] r8 = new java.lang.Object[r6]     // Catch:{ all -> 0x0083 }
            r6 = 0
            java.lang.Class r9 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0083 }
            r7[r6] = r9     // Catch:{ all -> 0x0083 }
            java.lang.Integer r9 = new java.lang.Integer     // Catch:{ all -> 0x0083 }
            r9.<init>(r2)     // Catch:{ all -> 0x0083 }
            r8[r6] = r9     // Catch:{ all -> 0x0083 }
            java.lang.String r6 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r9 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r6 = r9.getMethod(r6, r7)     // Catch:{ all -> 0x0083 }
            r6.invoke(r10, r8)     // Catch:{ all -> 0x0083 }
        L_0x00e4:
            int r2 = r10.jj_ntk     // Catch:{ all -> 0x0083 }
            if (r2 != r3) goto L_0x0169
            r6 = 0
            java.lang.Class[] r7 = new java.lang.Class[r6]     // Catch:{ all -> 0x0083 }
            java.lang.Object[] r8 = new java.lang.Object[r6]     // Catch:{ all -> 0x0083 }
            java.lang.String r6 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r9 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r6 = r9.getMethod(r6, r7)     // Catch:{ all -> 0x0083 }
            java.lang.Object r6 = r6.invoke(r10, r8)     // Catch:{ all -> 0x0083 }
            java.lang.Integer r6 = (java.lang.Integer) r6     // Catch:{ all -> 0x0083 }
            int r2 = r6.intValue()     // Catch:{ all -> 0x0083 }
        L_0x00ff:
            switch(r2) {
                case 14: goto L_0x003a;
                case 31: goto L_0x003a;
                default: goto L_0x0102;
            }     // Catch:{ all -> 0x0083 }
        L_0x0102:
            int[] r2 = r10.jj_la1     // Catch:{ all -> 0x0083 }
            r3 = 14
            int r4 = r10.jj_gen     // Catch:{ all -> 0x0083 }
            r2[r3] = r4     // Catch:{ all -> 0x0083 }
            if (r0 == 0) goto L_0x0148
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r2 = r10.jjtree
            r6 = 2
            java.lang.Class[] r7 = new java.lang.Class[r6]
            java.lang.Object[] r8 = new java.lang.Object[r6]
            r6 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r9 = org.apache.james.mime4j.field.address.parser.Node.class
            r7[r6] = r9
            r8[r6] = r1
            r6 = 1
            java.lang.Class r9 = java.lang.Boolean.TYPE
            r7[r6] = r9
            java.lang.Boolean r9 = new java.lang.Boolean
            r9.<init>(r5)
            r8[r6] = r9
            java.lang.String r6 = "closeNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r9 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r6 = r9.getMethod(r6, r7)
            r6.invoke(r2, r8)
            r6 = 1
            java.lang.Class[] r7 = new java.lang.Class[r6]
            java.lang.Object[] r8 = new java.lang.Object[r6]
            r6 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r9 = org.apache.james.mime4j.field.address.parser.Node.class
            r7[r6] = r9
            r8[r6] = r1
            java.lang.String r6 = "jjtreeCloseNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r9 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r6 = r9.getMethod(r6, r7)
            r6.invoke(r10, r8)
        L_0x0148:
            return
        L_0x0149:
            r2 = 31
            r6 = 1
            java.lang.Class[] r7 = new java.lang.Class[r6]     // Catch:{ all -> 0x0083 }
            java.lang.Object[] r8 = new java.lang.Object[r6]     // Catch:{ all -> 0x0083 }
            r6 = 0
            java.lang.Class r9 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0083 }
            r7[r6] = r9     // Catch:{ all -> 0x0083 }
            java.lang.Integer r9 = new java.lang.Integer     // Catch:{ all -> 0x0083 }
            r9.<init>(r2)     // Catch:{ all -> 0x0083 }
            r8[r6] = r9     // Catch:{ all -> 0x0083 }
            java.lang.String r6 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r9 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r6 = r9.getMethod(r6, r7)     // Catch:{ all -> 0x0083 }
            r6.invoke(r10, r8)     // Catch:{ all -> 0x0083 }
            goto L_0x00e4
        L_0x0169:
            int r2 = r10.jj_ntk     // Catch:{ all -> 0x0083 }
            goto L_0x00ff
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.address.parser.AddressListParser.phrase():void");
    }

    public final void addr_spec() throws ParseException {
        ASTaddr_spec jjtn000 = new ASTaddr_spec(9);
        Object[] objArr = {jjtn000};
        JJTAddressListParserState.class.getMethod("openNodeScope", Node.class).invoke(this.jjtree, objArr);
        Object[] objArr2 = {jjtn000};
        AddressListParser.class.getMethod("jjtreeOpenNodeScope", Node.class).invoke(this, objArr2);
        try {
            AddressListParser.class.getMethod("local_part", new Class[0]).invoke(this, new Object[0]);
            Class[] clsArr = {Integer.TYPE};
            AddressListParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(8));
            AddressListParser.class.getMethod("domain", new Class[0]).invoke(this, new Object[0]);
            if (1 != 0) {
                JJTAddressListParserState jJTAddressListParserState = this.jjtree;
                Class[] clsArr2 = {Node.class, Boolean.TYPE};
                JJTAddressListParserState.class.getMethod("closeNodeScope", clsArr2).invoke(jJTAddressListParserState, jjtn000, new Boolean(true));
                Object[] objArr3 = {jjtn000};
                AddressListParser.class.getMethod("jjtreeCloseNodeScope", Node.class).invoke(this, objArr3);
            }
        } catch (Throwable th) {
            if (1 != 0) {
                JJTAddressListParserState jJTAddressListParserState2 = this.jjtree;
                Class[] clsArr3 = {Node.class, Boolean.TYPE};
                JJTAddressListParserState.class.getMethod("closeNodeScope", clsArr3).invoke(jJTAddressListParserState2, jjtn000, new Boolean(true));
                Object[] objArr4 = {jjtn000};
                AddressListParser.class.getMethod("jjtreeCloseNodeScope", Node.class).invoke(this, objArr4);
            }
            throw th;
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00ed A[Catch:{ all -> 0x0085 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0171 A[Catch:{ all -> 0x0085 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0174 A[Catch:{ all -> 0x0085 }, FALL_THROUGH] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0107 A[SYNTHETIC] */
    public final void local_part() throws org.apache.james.mime4j.field.address.parser.ParseException {
        /*
            r13 = this;
            r8 = 1
            r7 = 31
            r6 = -1
            org.apache.james.mime4j.field.address.parser.ASTlocal_part r1 = new org.apache.james.mime4j.field.address.parser.ASTlocal_part
            r3 = 10
            r1.<init>(r3)
            r0 = 1
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r3 = r13.jjtree
            r9 = 1
            java.lang.Class[] r10 = new java.lang.Class[r9]
            java.lang.Object[] r11 = new java.lang.Object[r9]
            r9 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r12 = org.apache.james.mime4j.field.address.parser.Node.class
            r10[r9] = r12
            r11[r9] = r1
            java.lang.String r9 = "openNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r12 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)
            r9.invoke(r3, r11)
            r9 = 1
            java.lang.Class[] r10 = new java.lang.Class[r9]
            java.lang.Object[] r11 = new java.lang.Object[r9]
            r9 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r12 = org.apache.james.mime4j.field.address.parser.Node.class
            r10[r9] = r12
            r11[r9] = r1
            java.lang.String r9 = "jjtreeOpenNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r12 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)
            r9.invoke(r13, r11)
            int r3 = r13.jj_ntk     // Catch:{ all -> 0x0085 }
            if (r3 != r6) goto L_0x00c5
            r9 = 0
            java.lang.Class[] r10 = new java.lang.Class[r9]     // Catch:{ all -> 0x0085 }
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x0085 }
            java.lang.String r9 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r12 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)     // Catch:{ all -> 0x0085 }
            java.lang.Object r9 = r9.invoke(r13, r11)     // Catch:{ all -> 0x0085 }
            java.lang.Integer r9 = (java.lang.Integer) r9     // Catch:{ all -> 0x0085 }
            int r3 = r9.intValue()     // Catch:{ all -> 0x0085 }
        L_0x0057:
            switch(r3) {
                case 14: goto L_0x00c8;
                case 31: goto L_0x014e;
                default: goto L_0x005a;
            }     // Catch:{ all -> 0x0085 }
        L_0x005a:
            int[] r3 = r13.jj_la1     // Catch:{ all -> 0x0085 }
            r4 = 15
            int r5 = r13.jj_gen     // Catch:{ all -> 0x0085 }
            r3[r4] = r5     // Catch:{ all -> 0x0085 }
            r3 = -1
            r9 = 1
            java.lang.Class[] r10 = new java.lang.Class[r9]     // Catch:{ all -> 0x0085 }
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x0085 }
            r9 = 0
            java.lang.Class r12 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0085 }
            r10[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.Integer r12 = new java.lang.Integer     // Catch:{ all -> 0x0085 }
            r12.<init>(r3)     // Catch:{ all -> 0x0085 }
            r11[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.String r9 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r12 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)     // Catch:{ all -> 0x0085 }
            r9.invoke(r13, r11)     // Catch:{ all -> 0x0085 }
            org.apache.james.mime4j.field.address.parser.ParseException r3 = new org.apache.james.mime4j.field.address.parser.ParseException     // Catch:{ all -> 0x0085 }
            r3.<init>()     // Catch:{ all -> 0x0085 }
            throw r3     // Catch:{ all -> 0x0085 }
        L_0x0085:
            r3 = move-exception
            if (r0 == 0) goto L_0x00c4
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r4 = r13.jjtree
            r9 = 2
            java.lang.Class[] r10 = new java.lang.Class[r9]
            java.lang.Object[] r11 = new java.lang.Object[r9]
            r9 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r12 = org.apache.james.mime4j.field.address.parser.Node.class
            r10[r9] = r12
            r11[r9] = r1
            r9 = 1
            java.lang.Class r12 = java.lang.Boolean.TYPE
            r10[r9] = r12
            java.lang.Boolean r12 = new java.lang.Boolean
            r12.<init>(r8)
            r11[r9] = r12
            java.lang.String r9 = "closeNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r12 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)
            r9.invoke(r4, r11)
            r9 = 1
            java.lang.Class[] r10 = new java.lang.Class[r9]
            java.lang.Object[] r11 = new java.lang.Object[r9]
            r9 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r12 = org.apache.james.mime4j.field.address.parser.Node.class
            r10[r9] = r12
            r11[r9] = r1
            java.lang.String r9 = "jjtreeCloseNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r12 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)
            r9.invoke(r13, r11)
        L_0x00c4:
            throw r3
        L_0x00c5:
            int r3 = r13.jj_ntk     // Catch:{ all -> 0x0085 }
            goto L_0x0057
        L_0x00c8:
            r3 = 14
            r9 = 1
            java.lang.Class[] r10 = new java.lang.Class[r9]     // Catch:{ all -> 0x0085 }
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x0085 }
            r9 = 0
            java.lang.Class r12 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0085 }
            r10[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.Integer r12 = new java.lang.Integer     // Catch:{ all -> 0x0085 }
            r12.<init>(r3)     // Catch:{ all -> 0x0085 }
            r11[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.String r9 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r12 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)     // Catch:{ all -> 0x0085 }
            java.lang.Object r2 = r9.invoke(r13, r11)     // Catch:{ all -> 0x0085 }
            org.apache.james.mime4j.field.address.parser.Token r2 = (org.apache.james.mime4j.field.address.parser.Token) r2     // Catch:{ all -> 0x0085 }
        L_0x00e9:
            int r3 = r13.jj_ntk     // Catch:{ all -> 0x0085 }
            if (r3 != r6) goto L_0x0171
            r9 = 0
            java.lang.Class[] r10 = new java.lang.Class[r9]     // Catch:{ all -> 0x0085 }
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x0085 }
            java.lang.String r9 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r12 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)     // Catch:{ all -> 0x0085 }
            java.lang.Object r9 = r9.invoke(r13, r11)     // Catch:{ all -> 0x0085 }
            java.lang.Integer r9 = (java.lang.Integer) r9     // Catch:{ all -> 0x0085 }
            int r3 = r9.intValue()     // Catch:{ all -> 0x0085 }
        L_0x0104:
            switch(r3) {
                case 9: goto L_0x0174;
                case 14: goto L_0x0174;
                case 31: goto L_0x0174;
                default: goto L_0x0107;
            }     // Catch:{ all -> 0x0085 }
        L_0x0107:
            int[] r3 = r13.jj_la1     // Catch:{ all -> 0x0085 }
            r4 = 16
            int r5 = r13.jj_gen     // Catch:{ all -> 0x0085 }
            r3[r4] = r5     // Catch:{ all -> 0x0085 }
            if (r0 == 0) goto L_0x014d
            org.apache.james.mime4j.field.address.parser.JJTAddressListParserState r3 = r13.jjtree
            r9 = 2
            java.lang.Class[] r10 = new java.lang.Class[r9]
            java.lang.Object[] r11 = new java.lang.Object[r9]
            r9 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r12 = org.apache.james.mime4j.field.address.parser.Node.class
            r10[r9] = r12
            r11[r9] = r1
            r9 = 1
            java.lang.Class r12 = java.lang.Boolean.TYPE
            r10[r9] = r12
            java.lang.Boolean r12 = new java.lang.Boolean
            r12.<init>(r8)
            r11[r9] = r12
            java.lang.String r9 = "closeNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.JJTAddressListParserState> r12 = org.apache.james.mime4j.field.address.parser.JJTAddressListParserState.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)
            r9.invoke(r3, r11)
            r9 = 1
            java.lang.Class[] r10 = new java.lang.Class[r9]
            java.lang.Object[] r11 = new java.lang.Object[r9]
            r9 = 0
            java.lang.Class<org.apache.james.mime4j.field.address.parser.Node> r12 = org.apache.james.mime4j.field.address.parser.Node.class
            r10[r9] = r12
            r11[r9] = r1
            java.lang.String r9 = "jjtreeCloseNodeScope"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r12 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)
            r9.invoke(r13, r11)
        L_0x014d:
            return
        L_0x014e:
            r3 = 31
            r9 = 1
            java.lang.Class[] r10 = new java.lang.Class[r9]     // Catch:{ all -> 0x0085 }
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x0085 }
            r9 = 0
            java.lang.Class r12 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0085 }
            r10[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.Integer r12 = new java.lang.Integer     // Catch:{ all -> 0x0085 }
            r12.<init>(r3)     // Catch:{ all -> 0x0085 }
            r11[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.String r9 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r12 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)     // Catch:{ all -> 0x0085 }
            java.lang.Object r2 = r9.invoke(r13, r11)     // Catch:{ all -> 0x0085 }
            org.apache.james.mime4j.field.address.parser.Token r2 = (org.apache.james.mime4j.field.address.parser.Token) r2     // Catch:{ all -> 0x0085 }
            goto L_0x00e9
        L_0x0171:
            int r3 = r13.jj_ntk     // Catch:{ all -> 0x0085 }
            goto L_0x0104
        L_0x0174:
            int r3 = r13.jj_ntk     // Catch:{ all -> 0x0085 }
            if (r3 != r6) goto L_0x01ea
            r9 = 0
            java.lang.Class[] r10 = new java.lang.Class[r9]     // Catch:{ all -> 0x0085 }
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x0085 }
            java.lang.String r9 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r12 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)     // Catch:{ all -> 0x0085 }
            java.lang.Object r9 = r9.invoke(r13, r11)     // Catch:{ all -> 0x0085 }
            java.lang.Integer r9 = (java.lang.Integer) r9     // Catch:{ all -> 0x0085 }
            int r3 = r9.intValue()     // Catch:{ all -> 0x0085 }
        L_0x018f:
            switch(r3) {
                case 9: goto L_0x01ed;
                default: goto L_0x0192;
            }     // Catch:{ all -> 0x0085 }
        L_0x0192:
            int[] r3 = r13.jj_la1     // Catch:{ all -> 0x0085 }
            r4 = 17
            int r5 = r13.jj_gen     // Catch:{ all -> 0x0085 }
            r3[r4] = r5     // Catch:{ all -> 0x0085 }
        L_0x019a:
            int r3 = r2.kind     // Catch:{ all -> 0x0085 }
            if (r3 == r7) goto L_0x01e2
            java.lang.String r3 = r2.image     // Catch:{ all -> 0x0085 }
            java.lang.String r4 = r2.image     // Catch:{ all -> 0x0085 }
            r9 = 0
            java.lang.Class[] r10 = new java.lang.Class[r9]     // Catch:{ all -> 0x0085 }
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x0085 }
            java.lang.String r9 = "length"
            java.lang.Class<java.lang.String> r12 = java.lang.String.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)     // Catch:{ all -> 0x0085 }
            java.lang.Object r9 = r9.invoke(r4, r11)     // Catch:{ all -> 0x0085 }
            java.lang.Integer r9 = (java.lang.Integer) r9     // Catch:{ all -> 0x0085 }
            int r4 = r9.intValue()     // Catch:{ all -> 0x0085 }
            int r4 = r4 + -1
            r9 = 1
            java.lang.Class[] r10 = new java.lang.Class[r9]     // Catch:{ all -> 0x0085 }
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x0085 }
            r9 = 0
            java.lang.Class r12 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0085 }
            r10[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.Integer r12 = new java.lang.Integer     // Catch:{ all -> 0x0085 }
            r12.<init>(r4)     // Catch:{ all -> 0x0085 }
            r11[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.String r9 = "charAt"
            java.lang.Class<java.lang.String> r12 = java.lang.String.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)     // Catch:{ all -> 0x0085 }
            java.lang.Object r9 = r9.invoke(r3, r11)     // Catch:{ all -> 0x0085 }
            java.lang.Character r9 = (java.lang.Character) r9     // Catch:{ all -> 0x0085 }
            char r3 = r9.charValue()     // Catch:{ all -> 0x0085 }
            r4 = 46
            if (r3 == r4) goto L_0x020f
        L_0x01e2:
            org.apache.james.mime4j.field.address.parser.ParseException r3 = new org.apache.james.mime4j.field.address.parser.ParseException     // Catch:{ all -> 0x0085 }
            java.lang.String r4 = "Words in local part must be separated by '.'"
            r3.<init>(r4)     // Catch:{ all -> 0x0085 }
            throw r3     // Catch:{ all -> 0x0085 }
        L_0x01ea:
            int r3 = r13.jj_ntk     // Catch:{ all -> 0x0085 }
            goto L_0x018f
        L_0x01ed:
            r3 = 9
            r9 = 1
            java.lang.Class[] r10 = new java.lang.Class[r9]     // Catch:{ all -> 0x0085 }
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x0085 }
            r9 = 0
            java.lang.Class r12 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0085 }
            r10[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.Integer r12 = new java.lang.Integer     // Catch:{ all -> 0x0085 }
            r12.<init>(r3)     // Catch:{ all -> 0x0085 }
            r11[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.String r9 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r12 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)     // Catch:{ all -> 0x0085 }
            java.lang.Object r2 = r9.invoke(r13, r11)     // Catch:{ all -> 0x0085 }
            org.apache.james.mime4j.field.address.parser.Token r2 = (org.apache.james.mime4j.field.address.parser.Token) r2     // Catch:{ all -> 0x0085 }
            goto L_0x019a
        L_0x020f:
            int r3 = r13.jj_ntk     // Catch:{ all -> 0x0085 }
            if (r3 != r6) goto L_0x0258
            r9 = 0
            java.lang.Class[] r10 = new java.lang.Class[r9]     // Catch:{ all -> 0x0085 }
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x0085 }
            java.lang.String r9 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r12 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)     // Catch:{ all -> 0x0085 }
            java.lang.Object r9 = r9.invoke(r13, r11)     // Catch:{ all -> 0x0085 }
            java.lang.Integer r9 = (java.lang.Integer) r9     // Catch:{ all -> 0x0085 }
            int r3 = r9.intValue()     // Catch:{ all -> 0x0085 }
        L_0x022a:
            switch(r3) {
                case 14: goto L_0x025b;
                case 31: goto L_0x027e;
                default: goto L_0x022d;
            }     // Catch:{ all -> 0x0085 }
        L_0x022d:
            int[] r3 = r13.jj_la1     // Catch:{ all -> 0x0085 }
            r4 = 18
            int r5 = r13.jj_gen     // Catch:{ all -> 0x0085 }
            r3[r4] = r5     // Catch:{ all -> 0x0085 }
            r3 = -1
            r9 = 1
            java.lang.Class[] r10 = new java.lang.Class[r9]     // Catch:{ all -> 0x0085 }
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x0085 }
            r9 = 0
            java.lang.Class r12 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0085 }
            r10[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.Integer r12 = new java.lang.Integer     // Catch:{ all -> 0x0085 }
            r12.<init>(r3)     // Catch:{ all -> 0x0085 }
            r11[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.String r9 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r12 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)     // Catch:{ all -> 0x0085 }
            r9.invoke(r13, r11)     // Catch:{ all -> 0x0085 }
            org.apache.james.mime4j.field.address.parser.ParseException r3 = new org.apache.james.mime4j.field.address.parser.ParseException     // Catch:{ all -> 0x0085 }
            r3.<init>()     // Catch:{ all -> 0x0085 }
            throw r3     // Catch:{ all -> 0x0085 }
        L_0x0258:
            int r3 = r13.jj_ntk     // Catch:{ all -> 0x0085 }
            goto L_0x022a
        L_0x025b:
            r3 = 14
            r9 = 1
            java.lang.Class[] r10 = new java.lang.Class[r9]     // Catch:{ all -> 0x0085 }
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x0085 }
            r9 = 0
            java.lang.Class r12 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0085 }
            r10[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.Integer r12 = new java.lang.Integer     // Catch:{ all -> 0x0085 }
            r12.<init>(r3)     // Catch:{ all -> 0x0085 }
            r11[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.String r9 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r12 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)     // Catch:{ all -> 0x0085 }
            java.lang.Object r2 = r9.invoke(r13, r11)     // Catch:{ all -> 0x0085 }
            org.apache.james.mime4j.field.address.parser.Token r2 = (org.apache.james.mime4j.field.address.parser.Token) r2     // Catch:{ all -> 0x0085 }
            goto L_0x00e9
        L_0x027e:
            r3 = 31
            r9 = 1
            java.lang.Class[] r10 = new java.lang.Class[r9]     // Catch:{ all -> 0x0085 }
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x0085 }
            r9 = 0
            java.lang.Class r12 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0085 }
            r10[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.Integer r12 = new java.lang.Integer     // Catch:{ all -> 0x0085 }
            r12.<init>(r3)     // Catch:{ all -> 0x0085 }
            r11[r9] = r12     // Catch:{ all -> 0x0085 }
            java.lang.String r9 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.address.parser.AddressListParser> r12 = org.apache.james.mime4j.field.address.parser.AddressListParser.class
            java.lang.reflect.Method r9 = r12.getMethod(r9, r10)     // Catch:{ all -> 0x0085 }
            java.lang.Object r2 = r9.invoke(r13, r11)     // Catch:{ all -> 0x0085 }
            org.apache.james.mime4j.field.address.parser.Token r2 = (org.apache.james.mime4j.field.address.parser.Token) r2
            goto L_0x00e9
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.address.parser.AddressListParser.local_part():void");
    }

    public final void domain() throws ParseException {
        String str;
        String str2;
        int i;
        int i2;
        int i3;
        ASTdomain jjtn000 = new ASTdomain(11);
        Object[] objArr = {jjtn000};
        JJTAddressListParserState.class.getMethod("openNodeScope", Node.class).invoke(this.jjtree, objArr);
        Object[] objArr2 = {jjtn000};
        AddressListParser.class.getMethod("jjtreeOpenNodeScope", Node.class).invoke(this, objArr2);
        try {
            if (this.jj_ntk == -1) {
                i = ((Integer) AddressListParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
            } else {
                i = this.jj_ntk;
            }
            switch (i) {
                case 14:
                    Class[] clsArr = {Integer.TYPE};
                    Token t = (Token) AddressListParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(14));
                    while (true) {
                        if (this.jj_ntk == -1) {
                            i2 = ((Integer) AddressListParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
                        } else {
                            i2 = this.jj_ntk;
                        }
                        switch (i2) {
                            case 9:
                            case 14:
                                if (this.jj_ntk == -1) {
                                    i3 = ((Integer) AddressListParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
                                } else {
                                    i3 = this.jj_ntk;
                                }
                                switch (i3) {
                                    case 9:
                                        Class[] clsArr2 = {Integer.TYPE};
                                        t = (Token) AddressListParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(9));
                                        break;
                                    default:
                                        this.jj_la1[20] = this.jj_gen;
                                        break;
                                }
                                String str3 = t.image;
                                Method method = String.class.getMethod("length", new Class[0]);
                                Class[] clsArr3 = {Integer.TYPE};
                                if (((Character) String.class.getMethod("charAt", clsArr3).invoke(str3, new Integer(((Integer) method.invoke(t.image, new Object[0])).intValue() - 1))).charValue() != '.') {
                                    throw new ParseException("Atoms in domain names must be separated by '.'");
                                }
                                Class[] clsArr4 = {Integer.TYPE};
                                t = (Token) AddressListParser.class.getMethod("jj_consume_token", clsArr4).invoke(this, new Integer(14));
                            default:
                                this.jj_la1[19] = this.jj_gen;
                                break;
                        }
                    }
                case 18:
                    Class[] clsArr5 = {Integer.TYPE};
                    AddressListParser.class.getMethod("jj_consume_token", clsArr5).invoke(this, new Integer(18));
                    break;
                default:
                    this.jj_la1[21] = this.jj_gen;
                    Class[] clsArr6 = {Integer.TYPE};
                    AddressListParser.class.getMethod("jj_consume_token", clsArr6).invoke(this, new Integer(-1));
                    throw new ParseException();
            }
        } finally {
            if (1 != 0) {
                JJTAddressListParserState jJTAddressListParserState = this.jjtree;
                int i4 = 2;
                Class[] clsArr7 = new Class[i4];
                Object[] objArr3 = new Object[i4];
                char c = 0;
                clsArr7[c] = Node.class;
                objArr3[c] = jjtn000;
                char c2 = 1;
                clsArr7[c2] = Boolean.TYPE;
                objArr3[c2] = new Boolean(true);
                str = "closeNodeScope";
                JJTAddressListParserState.class.getMethod(str, clsArr7).invoke(jJTAddressListParserState, objArr3);
                int i5 = 1;
                Class[] clsArr8 = new Class[i5];
                Object[] objArr4 = new Object[i5];
                char c3 = 0;
                clsArr8[c3] = Node.class;
                objArr4[c3] = jjtn000;
                str2 = "jjtreeCloseNodeScope";
                AddressListParser.class.getMethod(str2, clsArr8).invoke(this, objArr4);
            }
        }
    }

    private final boolean jj_2_1(int xla) {
        String str;
        boolean z = true;
        this.jj_la = xla;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (((Boolean) AddressListParser.class.getMethod("jj_3_1", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
                z = false;
            }
        } catch (LookaheadSuccess e) {
        } finally {
            int i = 2;
            Class[] clsArr = new Class[i];
            Object[] objArr = new Object[i];
            char c = 0;
            clsArr[c] = Integer.TYPE;
            objArr[c] = new Integer(0);
            char c2 = 1;
            clsArr[c2] = Integer.TYPE;
            objArr[c2] = new Integer(xla);
            str = "jj_save";
            AddressListParser.class.getMethod(str, clsArr).invoke(this, objArr);
        }
        return z;
    }

    /* JADX INFO: finally extract failed */
    private final boolean jj_2_2(int xla) {
        this.jj_la = xla;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            boolean z = !((Boolean) AddressListParser.class.getMethod("jj_3_2", new Class[0]).invoke(this, new Object[0])).booleanValue();
            AddressListParser.class.getMethod("jj_save", Integer.TYPE, Integer.TYPE).invoke(this, new Integer(1), new Integer(xla));
            return z;
        } catch (LookaheadSuccess e) {
            AddressListParser.class.getMethod("jj_save", Integer.TYPE, Integer.TYPE).invoke(this, new Integer(1), new Integer(xla));
            return true;
        } catch (Throwable th) {
            AddressListParser.class.getMethod("jj_save", Integer.TYPE, Integer.TYPE).invoke(this, new Integer(1), new Integer(xla));
            throw th;
        }
    }

    private final boolean jj_3R_12() {
        Token xsp;
        Class[] clsArr = {Integer.TYPE};
        if (((Boolean) AddressListParser.class.getMethod("jj_scan_token", clsArr).invoke(this, new Integer(14))).booleanValue()) {
            return true;
        }
        do {
            xsp = this.jj_scanpos;
        } while (!((Boolean) AddressListParser.class.getMethod("jj_3R_13", new Class[0]).invoke(this, new Object[0])).booleanValue());
        this.jj_scanpos = xsp;
        return false;
    }

    private final boolean jj_3R_10() {
        Token xsp = this.jj_scanpos;
        if (((Boolean) AddressListParser.class.getMethod("jj_3R_12", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
            this.jj_scanpos = xsp;
            Class[] clsArr = {Integer.TYPE};
            if (((Boolean) AddressListParser.class.getMethod("jj_scan_token", clsArr).invoke(this, new Integer(18))).booleanValue()) {
                return true;
            }
        }
        return false;
    }

    private final boolean jj_3_2() {
        if (((Boolean) AddressListParser.class.getMethod("jj_3R_8", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
            return true;
        }
        return false;
    }

    private final boolean jj_3R_9() {
        Token xsp;
        Token xsp2 = this.jj_scanpos;
        Class[] clsArr = {Integer.TYPE};
        if (((Boolean) AddressListParser.class.getMethod("jj_scan_token", clsArr).invoke(this, new Integer(14))).booleanValue()) {
            this.jj_scanpos = xsp2;
            Class[] clsArr2 = {Integer.TYPE};
            if (((Boolean) AddressListParser.class.getMethod("jj_scan_token", clsArr2).invoke(this, new Integer(31))).booleanValue()) {
                return true;
            }
        }
        do {
            xsp = this.jj_scanpos;
        } while (!((Boolean) AddressListParser.class.getMethod("jj_3R_11", new Class[0]).invoke(this, new Object[0])).booleanValue());
        this.jj_scanpos = xsp;
        return false;
    }

    private final boolean jj_3R_11() {
        Token xsp = this.jj_scanpos;
        Class[] clsArr = {Integer.TYPE};
        if (((Boolean) AddressListParser.class.getMethod("jj_scan_token", clsArr).invoke(this, new Integer(9))).booleanValue()) {
            this.jj_scanpos = xsp;
        }
        Token xsp2 = this.jj_scanpos;
        Class[] clsArr2 = {Integer.TYPE};
        if (((Boolean) AddressListParser.class.getMethod("jj_scan_token", clsArr2).invoke(this, new Integer(14))).booleanValue()) {
            this.jj_scanpos = xsp2;
            Class[] clsArr3 = {Integer.TYPE};
            if (((Boolean) AddressListParser.class.getMethod("jj_scan_token", clsArr3).invoke(this, new Integer(31))).booleanValue()) {
                return true;
            }
        }
        return false;
    }

    private final boolean jj_3R_13() {
        Token xsp = this.jj_scanpos;
        Class[] clsArr = {Integer.TYPE};
        if (((Boolean) AddressListParser.class.getMethod("jj_scan_token", clsArr).invoke(this, new Integer(9))).booleanValue()) {
            this.jj_scanpos = xsp;
        }
        Class[] clsArr2 = {Integer.TYPE};
        if (((Boolean) AddressListParser.class.getMethod("jj_scan_token", clsArr2).invoke(this, new Integer(14))).booleanValue()) {
            return true;
        }
        return false;
    }

    private final boolean jj_3R_8() {
        if (((Boolean) AddressListParser.class.getMethod("jj_3R_9", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
            return true;
        }
        Class[] clsArr = {Integer.TYPE};
        if (((Boolean) AddressListParser.class.getMethod("jj_scan_token", clsArr).invoke(this, new Integer(8))).booleanValue()) {
            return true;
        }
        if (!((Boolean) AddressListParser.class.getMethod("jj_3R_10", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
            return false;
        }
        return true;
    }

    private final boolean jj_3_1() {
        if (((Boolean) AddressListParser.class.getMethod("jj_3R_8", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
            return true;
        }
        return false;
    }

    static {
        AddressListParser.class.getMethod("jj_la1_0", new Class[0]).invoke(null, new Object[0]);
        AddressListParser.class.getMethod("jj_la1_1", new Class[0]).invoke(null, new Object[0]);
    }

    private static void jj_la1_0() {
        jj_la1_0 = new int[]{2, -2147467200, 8, -2147467200, 80, -2147467200, -2147467200, -2147467200, 8, -2147467200, 256, 264, 8, -2147467264, -2147467264, -2147467264, -2147466752, 512, -2147467264, 16896, 512, 278528};
    }

    private static void jj_la1_1() {
        jj_la1_1 = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    }

    public AddressListParser(InputStream stream) {
        this(stream, null);
    }

    public AddressListParser(InputStream stream, String encoding) {
        this.jjtree = new JJTAddressListParserState();
        this.lookingAhead = false;
        this.jj_la1 = new int[22];
        this.jj_2_rtns = new JJCalls[2];
        this.jj_rescan = false;
        this.jj_gc = 0;
        this.jj_ls = new LookaheadSuccess();
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.jj_lasttokens = new int[100];
        try {
            this.jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1);
            this.token_source = new AddressListParserTokenManager(this.jj_input_stream);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 22; i++) {
                this.jj_la1[i] = -1;
            }
            for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
                this.jj_2_rtns[i2] = new JJCalls();
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public void ReInit(InputStream stream) {
        Object[] objArr = {stream, null};
        AddressListParser.class.getMethod("ReInit", InputStream.class, String.class).invoke(this, objArr);
    }

    public void ReInit(InputStream stream, String encoding) {
        try {
            SimpleCharStream simpleCharStream = this.jj_input_stream;
            Class[] clsArr = {InputStream.class, String.class, Integer.TYPE, Integer.TYPE};
            SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(simpleCharStream, stream, encoding, new Integer(1), new Integer(1));
            AddressListParserTokenManager addressListParserTokenManager = this.token_source;
            Object[] objArr = {this.jj_input_stream};
            AddressListParserTokenManager.class.getMethod("ReInit", SimpleCharStream.class).invoke(addressListParserTokenManager, objArr);
            this.token = new Token();
            this.jj_ntk = -1;
            JJTAddressListParserState.class.getMethod("reset", new Class[0]).invoke(this.jjtree, new Object[0]);
            this.jj_gen = 0;
            for (int i = 0; i < 22; i++) {
                this.jj_la1[i] = -1;
            }
            for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
                this.jj_2_rtns[i2] = new JJCalls();
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public AddressListParser(Reader stream) {
        this.jjtree = new JJTAddressListParserState();
        this.lookingAhead = false;
        this.jj_la1 = new int[22];
        this.jj_2_rtns = new JJCalls[2];
        this.jj_rescan = false;
        this.jj_gc = 0;
        this.jj_ls = new LookaheadSuccess();
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.jj_lasttokens = new int[100];
        this.jj_input_stream = new SimpleCharStream(stream, 1, 1);
        this.token_source = new AddressListParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 22; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    public void ReInit(Reader stream) {
        SimpleCharStream simpleCharStream = this.jj_input_stream;
        Class[] clsArr = {Reader.class, Integer.TYPE, Integer.TYPE};
        SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(simpleCharStream, stream, new Integer(1), new Integer(1));
        AddressListParserTokenManager addressListParserTokenManager = this.token_source;
        Object[] objArr = {this.jj_input_stream};
        AddressListParserTokenManager.class.getMethod("ReInit", SimpleCharStream.class).invoke(addressListParserTokenManager, objArr);
        this.token = new Token();
        this.jj_ntk = -1;
        JJTAddressListParserState.class.getMethod("reset", new Class[0]).invoke(this.jjtree, new Object[0]);
        this.jj_gen = 0;
        for (int i = 0; i < 22; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    public AddressListParser(AddressListParserTokenManager tm) {
        this.jjtree = new JJTAddressListParserState();
        this.lookingAhead = false;
        this.jj_la1 = new int[22];
        this.jj_2_rtns = new JJCalls[2];
        this.jj_rescan = false;
        this.jj_gc = 0;
        this.jj_ls = new LookaheadSuccess();
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.jj_lasttokens = new int[100];
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 22; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    public void ReInit(AddressListParserTokenManager tm) {
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        JJTAddressListParserState.class.getMethod("reset", new Class[0]).invoke(this.jjtree, new Object[0]);
        this.jj_gen = 0;
        for (int i = 0; i < 22; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    private final Token jj_consume_token(int kind) throws ParseException {
        Token oldToken = this.token;
        if (oldToken.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token token3 = (Token) AddressListParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token2.next = token3;
            this.token = token3;
        }
        this.jj_ntk = -1;
        if (this.token.kind == kind) {
            this.jj_gen++;
            int i = this.jj_gc + 1;
            this.jj_gc = i;
            if (i > 100) {
                this.jj_gc = 0;
                for (JJCalls c : this.jj_2_rtns) {
                    while (c != null) {
                        if (c.gen < this.jj_gen) {
                            c.first = null;
                        }
                        c = c.next;
                    }
                }
            }
            return this.token;
        }
        this.token = oldToken;
        this.jj_kind = kind;
        throw ((ParseException) AddressListParser.class.getMethod("generateParseException", new Class[0]).invoke(this, new Object[0]));
    }

    private static final class LookaheadSuccess extends Error {
        private LookaheadSuccess() {
        }
    }

    private final boolean jj_scan_token(int kind) {
        if (this.jj_scanpos == this.jj_lastpos) {
            this.jj_la--;
            if (this.jj_scanpos.next == null) {
                Token token2 = this.jj_scanpos;
                Token token3 = (Token) AddressListParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
                token2.next = token3;
                this.jj_scanpos = token3;
                this.jj_lastpos = token3;
            } else {
                Token token4 = this.jj_scanpos.next;
                this.jj_scanpos = token4;
                this.jj_lastpos = token4;
            }
        } else {
            this.jj_scanpos = this.jj_scanpos.next;
        }
        if (this.jj_rescan) {
            int i = 0;
            Token tok = this.token;
            while (tok != null && tok != this.jj_scanpos) {
                i++;
                tok = tok.next;
            }
            if (tok != null) {
                AddressListParser.class.getMethod("jj_add_error_token", Integer.TYPE, Integer.TYPE).invoke(this, new Integer(kind), new Integer(i));
            }
        }
        if (this.jj_scanpos.kind != kind) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_scanpos != this.jj_lastpos) {
            return false;
        }
        throw this.jj_ls;
    }

    public final Token getNextToken() {
        if (this.token.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token token3 = (Token) AddressListParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token2.next = token3;
            this.token = token3;
        }
        this.jj_ntk = -1;
        this.jj_gen++;
        return this.token;
    }

    public final Token getToken(int index) {
        Token t;
        int i = 0;
        Token t2 = this.lookingAhead ? this.jj_scanpos : this.token;
        while (i < index) {
            if (t2.next != null) {
                t = t2.next;
            } else {
                t = (Token) AddressListParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
                t2.next = t;
            }
            i++;
            t2 = t;
        }
        return t2;
    }

    private final int jj_ntk() {
        Token token2 = this.token.next;
        this.jj_nt = token2;
        if (token2 == null) {
            Token token3 = this.token;
            Token token4 = (Token) AddressListParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token3.next = token4;
            int i = token4.kind;
            this.jj_ntk = i;
            return i;
        }
        int i2 = this.jj_nt.kind;
        this.jj_ntk = i2;
        return i2;
    }

    private void jj_add_error_token(int kind, int pos) {
        if (pos < 100) {
            if (pos == this.jj_endpos + 1) {
                int[] iArr = this.jj_lasttokens;
                int i = this.jj_endpos;
                this.jj_endpos = i + 1;
                iArr[i] = kind;
            } else if (this.jj_endpos != 0) {
                this.jj_expentry = new int[this.jj_endpos];
                for (int i2 = 0; i2 < this.jj_endpos; i2++) {
                    this.jj_expentry[i2] = this.jj_lasttokens[i2];
                }
                boolean exists = false;
                Enumeration e = (Enumeration) Vector.class.getMethod("elements", new Class[0]).invoke(this.jj_expentries, new Object[0]);
                while (true) {
                    if (!((Boolean) Enumeration.class.getMethod("hasMoreElements", new Class[0]).invoke(e, new Object[0])).booleanValue()) {
                        break;
                    }
                    int[] oldentry = (int[]) Enumeration.class.getMethod("nextElement", new Class[0]).invoke(e, new Object[0]);
                    if (oldentry.length == this.jj_expentry.length) {
                        exists = true;
                        int i3 = 0;
                        while (true) {
                            if (i3 >= this.jj_expentry.length) {
                                break;
                            } else if (oldentry[i3] != this.jj_expentry[i3]) {
                                exists = false;
                                break;
                            } else {
                                i3++;
                            }
                        }
                        if (exists) {
                            break;
                        }
                    }
                }
                if (!exists) {
                    Vector<int[]> vector = this.jj_expentries;
                    Object[] objArr = {this.jj_expentry};
                    Vector.class.getMethod("addElement", Object.class).invoke(vector, objArr);
                }
                if (pos != 0) {
                    int[] iArr2 = this.jj_lasttokens;
                    this.jj_endpos = pos;
                    iArr2[pos - 1] = kind;
                }
            }
        }
    }

    public ParseException generateParseException() {
        Vector.class.getMethod("removeAllElements", new Class[0]).invoke(this.jj_expentries, new Object[0]);
        boolean[] la1tokens = new boolean[34];
        for (int i = 0; i < 34; i++) {
            la1tokens[i] = false;
        }
        if (this.jj_kind >= 0) {
            la1tokens[this.jj_kind] = true;
            this.jj_kind = -1;
        }
        for (int i2 = 0; i2 < 22; i2++) {
            if (this.jj_la1[i2] == this.jj_gen) {
                for (int j = 0; j < 32; j++) {
                    if ((jj_la1_0[i2] & (1 << j)) != 0) {
                        la1tokens[j] = true;
                    }
                    if ((jj_la1_1[i2] & (1 << j)) != 0) {
                        la1tokens[j + 32] = true;
                    }
                }
            }
        }
        for (int i3 = 0; i3 < 34; i3++) {
            if (la1tokens[i3]) {
                this.jj_expentry = new int[1];
                this.jj_expentry[0] = i3;
                Vector<int[]> vector = this.jj_expentries;
                Object[] objArr = {this.jj_expentry};
                Vector.class.getMethod("addElement", Object.class).invoke(vector, objArr);
            }
        }
        this.jj_endpos = 0;
        AddressListParser.class.getMethod("jj_rescan_token", new Class[0]).invoke(this, new Object[0]);
        Class[] clsArr = {Integer.TYPE, Integer.TYPE};
        AddressListParser.class.getMethod("jj_add_error_token", clsArr).invoke(this, new Integer(0), new Integer(0));
        int[][] exptokseq = new int[((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.jj_expentries, new Object[0])).intValue()][];
        int i4 = 0;
        while (true) {
            if (i4 >= ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.jj_expentries, new Object[0])).intValue()) {
                return new ParseException(this.token, exptokseq, tokenImage);
            }
            Vector<int[]> vector2 = this.jj_expentries;
            Class[] clsArr2 = {Integer.TYPE};
            exptokseq[i4] = (int[]) Vector.class.getMethod("elementAt", clsArr2).invoke(vector2, new Integer(i4));
            i4++;
        }
    }

    public final void enable_tracing() {
    }

    public final void disable_tracing() {
    }

    private final void jj_rescan_token() {
        this.jj_rescan = true;
        for (int i = 0; i < 2; i++) {
            try {
                JJCalls p = this.jj_2_rtns[i];
                do {
                    if (p.gen > this.jj_gen) {
                        this.jj_la = p.arg;
                        Token token2 = p.first;
                        this.jj_scanpos = token2;
                        this.jj_lastpos = token2;
                        switch (i) {
                            case 0:
                                ((Boolean) AddressListParser.class.getMethod("jj_3_1", new Class[0]).invoke(this, new Object[0])).booleanValue();
                                break;
                            case 1:
                                ((Boolean) AddressListParser.class.getMethod("jj_3_2", new Class[0]).invoke(this, new Object[0])).booleanValue();
                                break;
                        }
                    }
                    p = p.next;
                } while (p != null);
            } catch (LookaheadSuccess e) {
            }
        }
        this.jj_rescan = false;
    }

    private final void jj_save(int index, int xla) {
        JJCalls p = this.jj_2_rtns[index];
        while (true) {
            if (p.gen <= this.jj_gen) {
                break;
            } else if (p.next == null) {
                JJCalls p2 = new JJCalls();
                p.next = p2;
                p = p2;
                break;
            } else {
                p = p.next;
            }
        }
        p.gen = (this.jj_gen + xla) - this.jj_la;
        p.first = this.token;
        p.arg = xla;
    }

    static final class JJCalls {
        int arg;
        Token first;
        int gen;
        JJCalls next;

        JJCalls() {
        }
    }
}
