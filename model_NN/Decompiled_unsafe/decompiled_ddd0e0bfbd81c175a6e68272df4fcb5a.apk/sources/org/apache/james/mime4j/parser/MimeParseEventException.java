package org.apache.james.mime4j.parser;

import org.apache.james.mime4j.MimeException;

public class MimeParseEventException extends MimeException {
    private static final long serialVersionUID = 4632991604246852302L;
    private final Event event;

    public MimeParseEventException(Event event2) {
        super((String) Event.class.getMethod("toString", new Class[0]).invoke(event2, new Object[0]));
        this.event = event2;
    }

    public Event getEvent() {
        return this.event;
    }
}
