package org.apache.james.mime4j.message;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.james.mime4j.storage.DefaultStorageProvider;
import org.apache.james.mime4j.storage.MultiReferenceStorage;
import org.apache.james.mime4j.storage.Storage;
import org.apache.james.mime4j.storage.StorageProvider;
import org.apache.james.mime4j.util.CharsetUtil;

public class BodyFactory {
    private static final Charset FALLBACK_CHARSET = CharsetUtil.DEFAULT_CHARSET;
    private static Log log;
    private StorageProvider storageProvider;

    static {
        Object[] objArr = {BodyFactory.class};
        log = (Log) LogFactory.class.getMethod("getLog", Class.class).invoke(null, objArr);
    }

    public BodyFactory() {
        this.storageProvider = (StorageProvider) DefaultStorageProvider.class.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
    }

    public BodyFactory(StorageProvider storageProvider2) {
        if (storageProvider2 == null) {
            storageProvider2 = (StorageProvider) DefaultStorageProvider.class.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        }
        this.storageProvider = storageProvider2;
    }

    public StorageProvider getStorageProvider() {
        return this.storageProvider;
    }

    public BinaryBody binaryBody(InputStream is) throws IOException {
        if (is == null) {
            throw new IllegalArgumentException();
        }
        return new StorageBinaryBody(new MultiReferenceStorage((Storage) StorageProvider.class.getMethod("store", InputStream.class).invoke(this.storageProvider, is)));
    }

    public BinaryBody binaryBody(Storage storage) throws IOException {
        if (storage != null) {
            return new StorageBinaryBody(new MultiReferenceStorage(storage));
        }
        throw new IllegalArgumentException();
    }

    public TextBody textBody(InputStream is) throws IOException {
        if (is == null) {
            throw new IllegalArgumentException();
        }
        return new StorageTextBody(new MultiReferenceStorage((Storage) StorageProvider.class.getMethod("store", InputStream.class).invoke(this.storageProvider, is)), CharsetUtil.DEFAULT_CHARSET);
    }

    public TextBody textBody(InputStream is, String mimeCharset) throws IOException {
        if (is == null) {
            throw new IllegalArgumentException();
        } else if (mimeCharset == null) {
            throw new IllegalArgumentException();
        } else {
            Object[] objArr = {is};
            Class[] clsArr = {String.class, Boolean.TYPE};
            Object[] objArr2 = {mimeCharset, new Boolean(false)};
            return new StorageTextBody(new MultiReferenceStorage((Storage) StorageProvider.class.getMethod("store", InputStream.class).invoke(this.storageProvider, objArr)), (Charset) BodyFactory.class.getMethod("toJavaCharset", clsArr).invoke(null, objArr2));
        }
    }

    public TextBody textBody(Storage storage) throws IOException {
        if (storage != null) {
            return new StorageTextBody(new MultiReferenceStorage(storage), CharsetUtil.DEFAULT_CHARSET);
        }
        throw new IllegalArgumentException();
    }

    public TextBody textBody(Storage storage, String mimeCharset) throws IOException {
        if (storage == null) {
            throw new IllegalArgumentException();
        } else if (mimeCharset == null) {
            throw new IllegalArgumentException();
        } else {
            Class[] clsArr = {String.class, Boolean.TYPE};
            Object[] objArr = {mimeCharset, new Boolean(false)};
            return new StorageTextBody(new MultiReferenceStorage(storage), (Charset) BodyFactory.class.getMethod("toJavaCharset", clsArr).invoke(null, objArr));
        }
    }

    public TextBody textBody(String text) {
        if (text != null) {
            return new StringTextBody(text, CharsetUtil.DEFAULT_CHARSET);
        }
        throw new IllegalArgumentException();
    }

    public TextBody textBody(String text, String mimeCharset) {
        if (text == null) {
            throw new IllegalArgumentException();
        } else if (mimeCharset == null) {
            throw new IllegalArgumentException();
        } else {
            Class[] clsArr = {String.class, Boolean.TYPE};
            return new StringTextBody(text, (Charset) BodyFactory.class.getMethod("toJavaCharset", clsArr).invoke(null, mimeCharset, new Boolean(true)));
        }
    }

    private static Charset toJavaCharset(String mimeCharset, boolean forEncoding) {
        Object[] objArr = {mimeCharset};
        String charset = (String) CharsetUtil.class.getMethod("toJavaCharset", String.class).invoke(null, objArr);
        if (charset == null) {
            if (((Boolean) Log.class.getMethod("isWarnEnabled", new Class[0]).invoke(log, new Object[0])).booleanValue()) {
                Log log2 = log;
                Object[] objArr2 = {"MIME charset '"};
                Object[] objArr3 = {mimeCharset};
                Method method = StringBuilder.class.getMethod("append", String.class);
                Object[] objArr4 = {"' has no "};
                Method method2 = StringBuilder.class.getMethod("append", String.class);
                Object[] objArr5 = {"corresponding Java charset. Using "};
                Method method3 = StringBuilder.class.getMethod("append", String.class);
                Object[] objArr6 = {FALLBACK_CHARSET};
                Method method4 = StringBuilder.class.getMethod("append", Object.class);
                Object[] objArr7 = {" instead."};
                Method method5 = StringBuilder.class.getMethod("append", String.class);
                Method method6 = StringBuilder.class.getMethod("toString", new Class[0]);
                Object[] objArr8 = {(String) method6.invoke((StringBuilder) method5.invoke((StringBuilder) method4.invoke((StringBuilder) method3.invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr2), objArr3), objArr4), objArr5), objArr6), objArr7), new Object[0])};
                Log.class.getMethod("warn", Object.class).invoke(log2, objArr8);
            }
            return FALLBACK_CHARSET;
        }
        if (forEncoding) {
            if (!((Boolean) CharsetUtil.class.getMethod("isEncodingSupported", String.class).invoke(null, charset)).booleanValue()) {
                if (((Boolean) Log.class.getMethod("isWarnEnabled", new Class[0]).invoke(log, new Object[0])).booleanValue()) {
                    Log log3 = log;
                    Object[] objArr9 = {"MIME charset '"};
                    Object[] objArr10 = {mimeCharset};
                    Method method7 = StringBuilder.class.getMethod("append", String.class);
                    Object[] objArr11 = {"' does not support encoding. Using "};
                    Method method8 = StringBuilder.class.getMethod("append", String.class);
                    Object[] objArr12 = {FALLBACK_CHARSET};
                    Method method9 = StringBuilder.class.getMethod("append", Object.class);
                    Object[] objArr13 = {" instead."};
                    Method method10 = StringBuilder.class.getMethod("append", String.class);
                    Method method11 = StringBuilder.class.getMethod("toString", new Class[0]);
                    Object[] objArr14 = {(String) method11.invoke((StringBuilder) method10.invoke((StringBuilder) method9.invoke((StringBuilder) method8.invoke((StringBuilder) method7.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr9), objArr10), objArr11), objArr12), objArr13), new Object[0])};
                    Log.class.getMethod("warn", Object.class).invoke(log3, objArr14);
                }
                return FALLBACK_CHARSET;
            }
        }
        if (!forEncoding) {
            if (!((Boolean) CharsetUtil.class.getMethod("isDecodingSupported", String.class).invoke(null, charset)).booleanValue()) {
                if (((Boolean) Log.class.getMethod("isWarnEnabled", new Class[0]).invoke(log, new Object[0])).booleanValue()) {
                    Log log4 = log;
                    Object[] objArr15 = {"MIME charset '"};
                    Object[] objArr16 = {mimeCharset};
                    Method method12 = StringBuilder.class.getMethod("append", String.class);
                    Object[] objArr17 = {"' does not support decoding. Using "};
                    Method method13 = StringBuilder.class.getMethod("append", String.class);
                    Object[] objArr18 = {FALLBACK_CHARSET};
                    Method method14 = StringBuilder.class.getMethod("append", Object.class);
                    Object[] objArr19 = {" instead."};
                    Method method15 = StringBuilder.class.getMethod("append", String.class);
                    Method method16 = StringBuilder.class.getMethod("toString", new Class[0]);
                    Object[] objArr20 = {(String) method16.invoke((StringBuilder) method15.invoke((StringBuilder) method14.invoke((StringBuilder) method13.invoke((StringBuilder) method12.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr15), objArr16), objArr17), objArr18), objArr19), new Object[0])};
                    Log.class.getMethod("warn", Object.class).invoke(log4, objArr20);
                }
                return FALLBACK_CHARSET;
            }
        }
        return (Charset) Charset.class.getMethod("forName", String.class).invoke(null, charset);
    }
}
