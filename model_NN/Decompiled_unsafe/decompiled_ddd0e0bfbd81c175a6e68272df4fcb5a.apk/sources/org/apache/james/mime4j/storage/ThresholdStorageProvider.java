package org.apache.james.mime4j.storage;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.lang.reflect.Method;
import org.apache.james.mime4j.storage.MemoryStorageProvider;
import org.apache.james.mime4j.util.ByteArrayBuffer;

public class ThresholdStorageProvider extends AbstractStorageProvider {
    /* access modifiers changed from: private */
    public final StorageProvider backend;
    /* access modifiers changed from: private */
    public final int thresholdSize;

    public ThresholdStorageProvider(StorageProvider backend2) {
        this(backend2, 2048);
    }

    public ThresholdStorageProvider(StorageProvider backend2, int thresholdSize2) {
        if (backend2 == null) {
            throw new IllegalArgumentException();
        } else if (thresholdSize2 < 1) {
            throw new IllegalArgumentException();
        } else {
            this.backend = backend2;
            this.thresholdSize = thresholdSize2;
        }
    }

    public StorageOutputStream createStorageOutputStream() {
        return new ThresholdStorageOutputStream();
    }

    private final class ThresholdStorageOutputStream extends StorageOutputStream {
        private final ByteArrayBuffer head;
        private StorageOutputStream tail;

        public ThresholdStorageOutputStream() {
            Object[] objArr = {ThresholdStorageProvider.this};
            int intValue = ((Integer) ThresholdStorageProvider.class.getMethod("access$000", ThresholdStorageProvider.class).invoke(null, objArr)).intValue();
            Class[] clsArr = {Integer.TYPE, Integer.TYPE};
            this.head = new ByteArrayBuffer(((Integer) Math.class.getMethod("min", clsArr).invoke(null, new Integer(intValue), new Integer(1024))).intValue());
        }

        public void close() throws IOException {
            super.close();
            if (this.tail != null) {
                StorageOutputStream.class.getMethod("close", new Class[0]).invoke(this.tail, new Object[0]);
            }
        }

        /* access modifiers changed from: protected */
        public void write0(byte[] buffer, int offset, int length) throws IOException {
            Object[] objArr = {ThresholdStorageProvider.this};
            int remainingHeadSize = ((Integer) ThresholdStorageProvider.class.getMethod("access$000", ThresholdStorageProvider.class).invoke(null, objArr)).intValue() - ((Integer) ByteArrayBuffer.class.getMethod("length", new Class[0]).invoke(this.head, new Object[0])).intValue();
            if (remainingHeadSize > 0) {
                Class[] clsArr = {Integer.TYPE, Integer.TYPE};
                int n = ((Integer) Math.class.getMethod("min", clsArr).invoke(null, new Integer(remainingHeadSize), new Integer(length))).intValue();
                ByteArrayBuffer byteArrayBuffer = this.head;
                Class[] clsArr2 = {byte[].class, Integer.TYPE, Integer.TYPE};
                ByteArrayBuffer.class.getMethod("append", clsArr2).invoke(byteArrayBuffer, buffer, new Integer(offset), new Integer(n));
                offset += n;
                length -= n;
            }
            if (length > 0) {
                if (this.tail == null) {
                    Object[] objArr2 = {ThresholdStorageProvider.this};
                    Method method = StorageProvider.class.getMethod("createStorageOutputStream", new Class[0]);
                    this.tail = (StorageOutputStream) method.invoke((StorageProvider) ThresholdStorageProvider.class.getMethod("access$100", ThresholdStorageProvider.class).invoke(null, objArr2), new Object[0]);
                }
                StorageOutputStream storageOutputStream = this.tail;
                Class[] clsArr3 = {byte[].class, Integer.TYPE, Integer.TYPE};
                StorageOutputStream.class.getMethod("write", clsArr3).invoke(storageOutputStream, buffer, new Integer(offset), new Integer(length));
            }
        }

        /* access modifiers changed from: protected */
        public Storage toStorage0() throws IOException {
            if (this.tail == null) {
                return new MemoryStorageProvider.MemoryStorage((byte[]) ByteArrayBuffer.class.getMethod("buffer", new Class[0]).invoke(this.head, new Object[0]), ((Integer) ByteArrayBuffer.class.getMethod("length", new Class[0]).invoke(this.head, new Object[0])).intValue());
            }
            Method method = ByteArrayBuffer.class.getMethod("buffer", new Class[0]);
            return new ThresholdStorage((byte[]) method.invoke(this.head, new Object[0]), ((Integer) ByteArrayBuffer.class.getMethod("length", new Class[0]).invoke(this.head, new Object[0])).intValue(), (Storage) StorageOutputStream.class.getMethod("toStorage", new Class[0]).invoke(this.tail, new Object[0]));
        }
    }

    private static final class ThresholdStorage implements Storage {
        private byte[] head;
        private final int headLen;
        private Storage tail;

        public ThresholdStorage(byte[] head2, int headLen2, Storage tail2) {
            this.head = head2;
            this.headLen = headLen2;
            this.tail = tail2;
        }

        public void delete() {
            if (this.head != null) {
                this.head = null;
                Storage.class.getMethod("delete", new Class[0]).invoke(this.tail, new Object[0]);
                this.tail = null;
            }
        }

        public InputStream getInputStream() throws IOException {
            if (this.head == null) {
                throw new IllegalStateException("storage has been deleted");
            }
            return new SequenceInputStream(new ByteArrayInputStream(this.head, 0, this.headLen), (InputStream) Storage.class.getMethod("getInputStream", new Class[0]).invoke(this.tail, new Object[0]));
        }
    }
}
