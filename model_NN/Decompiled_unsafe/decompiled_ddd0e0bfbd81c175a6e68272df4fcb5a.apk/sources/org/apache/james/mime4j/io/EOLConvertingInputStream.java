package org.apache.james.mime4j.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

public class EOLConvertingInputStream extends InputStream {
    public static final int CONVERT_BOTH = 3;
    public static final int CONVERT_CR = 1;
    public static final int CONVERT_LF = 2;
    private int flags;
    private PushbackInputStream in;
    private int previous;

    public EOLConvertingInputStream(InputStream in2) {
        this(in2, 3);
    }

    public EOLConvertingInputStream(InputStream in2, int flags2) {
        this.in = null;
        this.previous = 0;
        this.flags = 3;
        this.in = new PushbackInputStream(in2, 2);
        this.flags = flags2;
    }

    public void close() throws IOException {
        PushbackInputStream.class.getMethod("close", new Class[0]).invoke(this.in, new Object[0]);
    }

    public int read() throws IOException {
        int b = ((Integer) PushbackInputStream.class.getMethod("read", new Class[0]).invoke(this.in, new Object[0])).intValue();
        if (b == -1) {
            return -1;
        }
        if ((this.flags & 1) != 0 && b == 13) {
            int c = ((Integer) PushbackInputStream.class.getMethod("read", new Class[0]).invoke(this.in, new Object[0])).intValue();
            if (c != -1) {
                PushbackInputStream pushbackInputStream = this.in;
                Class[] clsArr = {Integer.TYPE};
                PushbackInputStream.class.getMethod("unread", clsArr).invoke(pushbackInputStream, new Integer(c));
            }
            if (c != 10) {
                PushbackInputStream pushbackInputStream2 = this.in;
                Class[] clsArr2 = {Integer.TYPE};
                PushbackInputStream.class.getMethod("unread", clsArr2).invoke(pushbackInputStream2, new Integer(10));
            }
        } else if (!((this.flags & 2) == 0 || b != 10 || this.previous == 13)) {
            b = 13;
            PushbackInputStream pushbackInputStream3 = this.in;
            Class[] clsArr3 = {Integer.TYPE};
            PushbackInputStream.class.getMethod("unread", clsArr3).invoke(pushbackInputStream3, new Integer(10));
        }
        this.previous = b;
        return b;
    }
}
