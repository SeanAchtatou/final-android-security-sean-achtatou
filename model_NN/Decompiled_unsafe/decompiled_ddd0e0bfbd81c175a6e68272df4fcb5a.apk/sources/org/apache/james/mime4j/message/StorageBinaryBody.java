package org.apache.james.mime4j.message;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.james.mime4j.codec.CodecUtil;
import org.apache.james.mime4j.storage.MultiReferenceStorage;

class StorageBinaryBody extends BinaryBody {
    private MultiReferenceStorage storage;

    public StorageBinaryBody(MultiReferenceStorage storage2) {
        this.storage = storage2;
    }

    public InputStream getInputStream() throws IOException {
        return (InputStream) MultiReferenceStorage.class.getMethod("getInputStream", new Class[0]).invoke(this.storage, new Object[0]);
    }

    public void writeTo(OutputStream out) throws IOException {
        if (out == null) {
            throw new IllegalArgumentException();
        }
        InputStream in = (InputStream) MultiReferenceStorage.class.getMethod("getInputStream", new Class[0]).invoke(this.storage, new Object[0]);
        Object[] objArr = {in, out};
        CodecUtil.class.getMethod("copy", InputStream.class, OutputStream.class).invoke(null, objArr);
        InputStream.class.getMethod("close", new Class[0]).invoke(in, new Object[0]);
    }

    public StorageBinaryBody copy() {
        MultiReferenceStorage.class.getMethod("addReference", new Class[0]).invoke(this.storage, new Object[0]);
        return new StorageBinaryBody(this.storage);
    }

    public void dispose() {
        if (this.storage != null) {
            MultiReferenceStorage.class.getMethod("delete", new Class[0]).invoke(this.storage, new Object[0]);
            this.storage = null;
        }
    }
}
