package org.apache.james.mime4j.field.address;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.james.mime4j.codec.DecoderUtil;
import org.apache.james.mime4j.field.address.parser.ASTaddr_spec;
import org.apache.james.mime4j.field.address.parser.ASTaddress;
import org.apache.james.mime4j.field.address.parser.ASTaddress_list;
import org.apache.james.mime4j.field.address.parser.ASTangle_addr;
import org.apache.james.mime4j.field.address.parser.ASTdomain;
import org.apache.james.mime4j.field.address.parser.ASTgroup_body;
import org.apache.james.mime4j.field.address.parser.ASTlocal_part;
import org.apache.james.mime4j.field.address.parser.ASTmailbox;
import org.apache.james.mime4j.field.address.parser.ASTname_addr;
import org.apache.james.mime4j.field.address.parser.ASTphrase;
import org.apache.james.mime4j.field.address.parser.ASTroute;
import org.apache.james.mime4j.field.address.parser.Node;
import org.apache.james.mime4j.field.address.parser.SimpleNode;
import org.apache.james.mime4j.field.address.parser.Token;

class Builder {
    private static Builder singleton = new Builder();

    Builder() {
    }

    public static Builder getInstance() {
        return singleton;
    }

    public AddressList buildAddressList(ASTaddress_list node) {
        List<Address> list = new ArrayList<>();
        int i = 0;
        while (true) {
            if (i >= ((Integer) ASTaddress_list.class.getMethod("jjtGetNumChildren", new Class[0]).invoke(node, new Object[0])).intValue()) {
                return new AddressList(list, true);
            }
            Class[] clsArr = {Integer.TYPE};
            Object[] objArr = {(ASTaddress) ((Node) ASTaddress_list.class.getMethod("jjtGetChild", clsArr).invoke(node, new Integer(i)))};
            Object[] objArr2 = {(Address) Builder.class.getMethod("buildAddress", ASTaddress.class).invoke(this, objArr)};
            ((Boolean) List.class.getMethod("add", Object.class).invoke(list, objArr2)).booleanValue();
            i++;
        }
    }

    public Address buildAddress(ASTaddress node) {
        ChildNodeIterator it = new ChildNodeIterator(node);
        Node n = (Node) ChildNodeIterator.class.getMethod("next", new Class[0]).invoke(it, new Object[0]);
        if (n instanceof ASTaddr_spec) {
            return (Mailbox) Builder.class.getMethod("buildAddrSpec", ASTaddr_spec.class).invoke(this, (ASTaddr_spec) n);
        } else if (n instanceof ASTangle_addr) {
            return (Mailbox) Builder.class.getMethod("buildAngleAddr", ASTangle_addr.class).invoke(this, (ASTangle_addr) n);
        } else if (n instanceof ASTphrase) {
            Class[] clsArr = {SimpleNode.class, Boolean.TYPE};
            String name = (String) Builder.class.getMethod("buildString", clsArr).invoke(this, (ASTphrase) n, new Boolean(false));
            Node n2 = (Node) ChildNodeIterator.class.getMethod("next", new Class[0]).invoke(it, new Object[0]);
            if (n2 instanceof ASTgroup_body) {
                return new Group(name, (MailboxList) Builder.class.getMethod("buildGroupBody", ASTgroup_body.class).invoke(this, (ASTgroup_body) n2));
            } else if (n2 instanceof ASTangle_addr) {
                return new Mailbox((String) DecoderUtil.class.getMethod("decodeEncodedWords", String.class).invoke(null, name), (Mailbox) Builder.class.getMethod("buildAngleAddr", ASTangle_addr.class).invoke(this, (ASTangle_addr) n2));
            } else {
                throw new IllegalStateException();
            }
        } else {
            throw new IllegalStateException();
        }
    }

    private MailboxList buildGroupBody(ASTgroup_body node) {
        List<Mailbox> results = new ArrayList<>();
        ChildNodeIterator it = new ChildNodeIterator(node);
        while (true) {
            if (!((Boolean) ChildNodeIterator.class.getMethod("hasNext", new Class[0]).invoke(it, new Object[0])).booleanValue()) {
                return new MailboxList(results, true);
            }
            Node n = (Node) ChildNodeIterator.class.getMethod("next", new Class[0]).invoke(it, new Object[0]);
            if (n instanceof ASTmailbox) {
                Object[] objArr = {(ASTmailbox) n};
                Object[] objArr2 = {(Mailbox) Builder.class.getMethod("buildMailbox", ASTmailbox.class).invoke(this, objArr)};
                ((Boolean) List.class.getMethod("add", Object.class).invoke(results, objArr2)).booleanValue();
            } else {
                throw new IllegalStateException();
            }
        }
    }

    public Mailbox buildMailbox(ASTmailbox node) {
        Node n = (Node) ChildNodeIterator.class.getMethod("next", new Class[0]).invoke(new ChildNodeIterator(node), new Object[0]);
        if (n instanceof ASTaddr_spec) {
            return (Mailbox) Builder.class.getMethod("buildAddrSpec", ASTaddr_spec.class).invoke(this, (ASTaddr_spec) n);
        } else if (n instanceof ASTangle_addr) {
            return (Mailbox) Builder.class.getMethod("buildAngleAddr", ASTangle_addr.class).invoke(this, (ASTangle_addr) n);
        } else if (n instanceof ASTname_addr) {
            return (Mailbox) Builder.class.getMethod("buildNameAddr", ASTname_addr.class).invoke(this, (ASTname_addr) n);
        } else {
            throw new IllegalStateException();
        }
    }

    private Mailbox buildNameAddr(ASTname_addr node) {
        ChildNodeIterator it = new ChildNodeIterator(node);
        Node n = (Node) ChildNodeIterator.class.getMethod("next", new Class[0]).invoke(it, new Object[0]);
        if (n instanceof ASTphrase) {
            Class[] clsArr = {SimpleNode.class, Boolean.TYPE};
            String name = (String) Builder.class.getMethod("buildString", clsArr).invoke(this, (ASTphrase) n, new Boolean(false));
            Node n2 = (Node) ChildNodeIterator.class.getMethod("next", new Class[0]).invoke(it, new Object[0]);
            if (n2 instanceof ASTangle_addr) {
                return new Mailbox((String) DecoderUtil.class.getMethod("decodeEncodedWords", String.class).invoke(null, name), (Mailbox) Builder.class.getMethod("buildAngleAddr", ASTangle_addr.class).invoke(this, (ASTangle_addr) n2));
            }
            throw new IllegalStateException();
        }
        throw new IllegalStateException();
    }

    private Mailbox buildAngleAddr(ASTangle_addr node) {
        ChildNodeIterator it = new ChildNodeIterator(node);
        DomainList route = null;
        Node n = (Node) ChildNodeIterator.class.getMethod("next", new Class[0]).invoke(it, new Object[0]);
        if (n instanceof ASTroute) {
            Object[] objArr = {(ASTroute) n};
            route = (DomainList) Builder.class.getMethod("buildRoute", ASTroute.class).invoke(this, objArr);
            n = (Node) ChildNodeIterator.class.getMethod("next", new Class[0]).invoke(it, new Object[0]);
        } else if (!(n instanceof ASTaddr_spec)) {
            throw new IllegalStateException();
        }
        if (n instanceof ASTaddr_spec) {
            return (Mailbox) Builder.class.getMethod("buildAddrSpec", DomainList.class, ASTaddr_spec.class).invoke(this, route, (ASTaddr_spec) n);
        }
        throw new IllegalStateException();
    }

    private DomainList buildRoute(ASTroute node) {
        List<String> results = new ArrayList<>(((Integer) ASTroute.class.getMethod("jjtGetNumChildren", new Class[0]).invoke(node, new Object[0])).intValue());
        ChildNodeIterator it = new ChildNodeIterator(node);
        while (true) {
            if (!((Boolean) ChildNodeIterator.class.getMethod("hasNext", new Class[0]).invoke(it, new Object[0])).booleanValue()) {
                return new DomainList(results, true);
            }
            Node n = (Node) ChildNodeIterator.class.getMethod("next", new Class[0]).invoke(it, new Object[0]);
            if (n instanceof ASTdomain) {
                Class[] clsArr = {SimpleNode.class, Boolean.TYPE};
                Object[] objArr = {(String) Builder.class.getMethod("buildString", clsArr).invoke(this, (ASTdomain) n, new Boolean(true))};
                ((Boolean) List.class.getMethod("add", Object.class).invoke(results, objArr)).booleanValue();
            } else {
                throw new IllegalStateException();
            }
        }
    }

    private Mailbox buildAddrSpec(ASTaddr_spec node) {
        return (Mailbox) Builder.class.getMethod("buildAddrSpec", DomainList.class, ASTaddr_spec.class).invoke(this, null, node);
    }

    private Mailbox buildAddrSpec(DomainList route, ASTaddr_spec node) {
        ChildNodeIterator it = new ChildNodeIterator(node);
        Method method = ChildNodeIterator.class.getMethod("next", new Class[0]);
        Class[] clsArr = {SimpleNode.class, Boolean.TYPE};
        Object[] objArr = {(ASTlocal_part) ((Node) method.invoke(it, new Object[0])), new Boolean(true)};
        Method method2 = ChildNodeIterator.class.getMethod("next", new Class[0]);
        Class[] clsArr2 = {SimpleNode.class, Boolean.TYPE};
        return new Mailbox(route, (String) Builder.class.getMethod("buildString", clsArr).invoke(this, objArr), (String) Builder.class.getMethod("buildString", clsArr2).invoke(this, (ASTdomain) ((Node) method2.invoke(it, new Object[0])), new Boolean(true)));
    }

    private String buildString(SimpleNode node, boolean stripSpaces) {
        Token head = node.firstToken;
        Token tail = node.lastToken;
        StringBuilder out = new StringBuilder();
        while (head != tail) {
            Object[] objArr = {head.image};
            StringBuilder.class.getMethod("append", String.class).invoke(out, objArr);
            head = head.next;
            if (!stripSpaces) {
                Object[] objArr2 = {out, head.specialToken};
                Builder.class.getMethod("addSpecials", StringBuilder.class, Token.class).invoke(this, objArr2);
            }
        }
        Object[] objArr3 = {tail.image};
        StringBuilder.class.getMethod("append", String.class).invoke(out, objArr3);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(out, new Object[0]);
    }

    private void addSpecials(StringBuilder out, Token specialToken) {
        if (specialToken != null) {
            Object[] objArr = {out, specialToken.specialToken};
            Builder.class.getMethod("addSpecials", StringBuilder.class, Token.class).invoke(this, objArr);
            Object[] objArr2 = {specialToken.image};
            StringBuilder.class.getMethod("append", String.class).invoke(out, objArr2);
        }
    }

    private static class ChildNodeIterator implements Iterator<Node> {
        private int index = 0;
        private int len;
        private SimpleNode simpleNode;

        public ChildNodeIterator(SimpleNode simpleNode2) {
            this.simpleNode = simpleNode2;
            this.len = ((Integer) SimpleNode.class.getMethod("jjtGetNumChildren", new Class[0]).invoke(simpleNode2, new Object[0])).intValue();
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public boolean hasNext() {
            return this.index < this.len;
        }

        public Node next() {
            SimpleNode simpleNode2 = this.simpleNode;
            int i = this.index;
            this.index = i + 1;
            Class[] clsArr = {Integer.TYPE};
            return (Node) SimpleNode.class.getMethod("jjtGetChild", clsArr).invoke(simpleNode2, new Integer(i));
        }
    }
}
