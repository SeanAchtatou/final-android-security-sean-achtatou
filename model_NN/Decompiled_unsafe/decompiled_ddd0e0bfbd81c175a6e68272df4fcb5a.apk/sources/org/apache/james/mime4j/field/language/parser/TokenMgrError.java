package org.apache.james.mime4j.field.language.parser;

import java.lang.reflect.Method;

public class TokenMgrError extends Error {
    static final int INVALID_LEXICAL_STATE = 2;
    static final int LEXICAL_ERROR = 0;
    static final int LOOP_DETECTED = 3;
    static final int STATIC_LEXER_ERROR = 1;
    int errorCode;

    protected static final String addEscapes(String str) {
        StringBuffer retval = new StringBuffer();
        int i = 0;
        while (true) {
            if (i < ((Integer) String.class.getMethod("length", new Class[0]).invoke(str, new Object[0])).intValue()) {
                Class[] clsArr = {Integer.TYPE};
                switch (((Character) String.class.getMethod("charAt", clsArr).invoke(str, new Integer(i))).charValue()) {
                    case 0:
                        break;
                    case 8:
                        Object[] objArr = {"\\b"};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr);
                        break;
                    case 9:
                        Object[] objArr2 = {"\\t"};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr2);
                        break;
                    case 10:
                        Object[] objArr3 = {"\\n"};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr3);
                        break;
                    case 12:
                        Object[] objArr4 = {"\\f"};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr4);
                        break;
                    case 13:
                        Object[] objArr5 = {"\\r"};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr5);
                        break;
                    case '\"':
                        Object[] objArr6 = {"\\\""};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr6);
                        break;
                    case '\'':
                        Object[] objArr7 = {"\\'"};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr7);
                        break;
                    case '\\':
                        Object[] objArr8 = {"\\\\"};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr8);
                        break;
                    default:
                        Class[] clsArr2 = {Integer.TYPE};
                        char ch = ((Character) String.class.getMethod("charAt", clsArr2).invoke(str, new Integer(i))).charValue();
                        if (ch >= ' ' && ch <= '~') {
                            Class[] clsArr3 = {Character.TYPE};
                            StringBuffer.class.getMethod("append", clsArr3).invoke(retval, new Character(ch));
                            break;
                        } else {
                            Object[] objArr9 = {"0000"};
                            Class[] clsArr4 = {Integer.TYPE, Integer.TYPE};
                            Object[] objArr10 = {(String) Integer.class.getMethod("toString", clsArr4).invoke(null, new Integer(ch), new Integer(16))};
                            Method method = StringBuilder.class.getMethod("append", String.class);
                            Method method2 = StringBuilder.class.getMethod("toString", new Class[0]);
                            String s = (String) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr9), objArr10), new Object[0]);
                            Object[] objArr11 = {"\\u"};
                            Method method3 = String.class.getMethod("length", new Class[0]);
                            int intValue = ((Integer) String.class.getMethod("length", new Class[0]).invoke(s, new Object[0])).intValue();
                            Class[] clsArr5 = {Integer.TYPE, Integer.TYPE};
                            Object[] objArr12 = {(String) String.class.getMethod("substring", clsArr5).invoke(s, new Integer(((Integer) method3.invoke(s, new Object[0])).intValue() - 4), new Integer(intValue))};
                            Method method4 = StringBuilder.class.getMethod("append", String.class);
                            Method method5 = StringBuilder.class.getMethod("toString", new Class[0]);
                            Object[] objArr13 = {(String) method5.invoke((StringBuilder) method4.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr11), objArr12), new Object[0])};
                            StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr13);
                            break;
                        }
                        break;
                }
                i++;
            } else {
                return (String) StringBuffer.class.getMethod("toString", new Class[0]).invoke(retval, new Object[0]);
            }
        }
    }

    protected static String LexicalError(boolean EOFSeen, int lexState, int errorLine, int errorColumn, String errorAfter, char curChar) {
        String str;
        Object[] objArr = {"Lexical error at line "};
        Class[] clsArr = {Integer.TYPE};
        Object[] objArr2 = {new Integer(errorLine)};
        Method method = StringBuilder.class.getMethod("append", clsArr);
        Object[] objArr3 = {", column "};
        Method method2 = StringBuilder.class.getMethod("append", String.class);
        Class[] clsArr2 = {Integer.TYPE};
        Object[] objArr4 = {new Integer(errorColumn)};
        Method method3 = StringBuilder.class.getMethod("append", clsArr2);
        Object[] objArr5 = {".  Encountered: "};
        Method method4 = StringBuilder.class.getMethod("append", String.class);
        StringBuilder sb = (StringBuilder) method4.invoke((StringBuilder) method3.invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr), objArr2), objArr3), objArr4), objArr5);
        if (EOFSeen) {
            str = "<EOF> ";
        } else {
            Object[] objArr6 = {"\""};
            Class[] clsArr3 = {Character.TYPE};
            Object[] objArr7 = {(String) String.class.getMethod("valueOf", clsArr3).invoke(null, new Character(curChar))};
            Object[] objArr8 = {(String) TokenMgrError.class.getMethod("addEscapes", String.class).invoke(null, objArr7)};
            Method method5 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr9 = {"\""};
            Method method6 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr10 = {" ("};
            Method method7 = StringBuilder.class.getMethod("append", String.class);
            Class[] clsArr4 = {Integer.TYPE};
            Object[] objArr11 = {new Integer(curChar)};
            Method method8 = StringBuilder.class.getMethod("append", clsArr4);
            Object[] objArr12 = {"), "};
            Method method9 = StringBuilder.class.getMethod("append", String.class);
            Method method10 = StringBuilder.class.getMethod("toString", new Class[0]);
            str = (String) method10.invoke((StringBuilder) method9.invoke((StringBuilder) method8.invoke((StringBuilder) method7.invoke((StringBuilder) method6.invoke((StringBuilder) method5.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr6), objArr8), objArr9), objArr10), objArr11), objArr12), new Object[0]);
        }
        Class[] clsArr5 = {String.class};
        Object[] objArr13 = {str};
        Object[] objArr14 = {"after : \""};
        Method method11 = StringBuilder.class.getMethod("append", String.class);
        Object[] objArr15 = {errorAfter};
        Object[] objArr16 = {(String) TokenMgrError.class.getMethod("addEscapes", String.class).invoke(null, objArr15)};
        Method method12 = StringBuilder.class.getMethod("append", String.class);
        Object[] objArr17 = {"\""};
        Method method13 = StringBuilder.class.getMethod("append", String.class);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method13.invoke((StringBuilder) method12.invoke((StringBuilder) method11.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr5).invoke(sb, objArr13), objArr14), objArr16), objArr17), new Object[0]);
    }

    public String getMessage() {
        return super.getMessage();
    }

    public TokenMgrError() {
    }

    public TokenMgrError(String message, int reason) {
        super(message);
        this.errorCode = reason;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TokenMgrError(boolean r6, int r7, int r8, int r9, java.lang.String r10, char r11, int r12) {
        /*
            r5 = this;
            r1 = 6
            java.lang.Class[] r2 = new java.lang.Class[r1]
            java.lang.Object[] r3 = new java.lang.Object[r1]
            r1 = 0
            java.lang.Class r4 = java.lang.Boolean.TYPE
            r2[r1] = r4
            java.lang.Boolean r4 = new java.lang.Boolean
            r4.<init>(r6)
            r3[r1] = r4
            r1 = 1
            java.lang.Class r4 = java.lang.Integer.TYPE
            r2[r1] = r4
            java.lang.Integer r4 = new java.lang.Integer
            r4.<init>(r7)
            r3[r1] = r4
            r1 = 2
            java.lang.Class r4 = java.lang.Integer.TYPE
            r2[r1] = r4
            java.lang.Integer r4 = new java.lang.Integer
            r4.<init>(r8)
            r3[r1] = r4
            r1 = 3
            java.lang.Class r4 = java.lang.Integer.TYPE
            r2[r1] = r4
            java.lang.Integer r4 = new java.lang.Integer
            r4.<init>(r9)
            r3[r1] = r4
            r1 = 4
            java.lang.Class<java.lang.String> r4 = java.lang.String.class
            r2[r1] = r4
            r3[r1] = r10
            r1 = 5
            java.lang.Class r4 = java.lang.Character.TYPE
            r2[r1] = r4
            java.lang.Character r4 = new java.lang.Character
            r4.<init>(r11)
            r3[r1] = r4
            java.lang.String r1 = "LexicalError"
            java.lang.Class<org.apache.james.mime4j.field.language.parser.TokenMgrError> r4 = org.apache.james.mime4j.field.language.parser.TokenMgrError.class
            java.lang.reflect.Method r1 = r4.getMethod(r1, r2)
            r4 = 0
            java.lang.Object r0 = r1.invoke(r4, r3)
            java.lang.String r0 = (java.lang.String) r0
            r5.<init>(r0, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.language.parser.TokenMgrError.<init>(boolean, int, int, int, java.lang.String, char, int):void");
    }
}
