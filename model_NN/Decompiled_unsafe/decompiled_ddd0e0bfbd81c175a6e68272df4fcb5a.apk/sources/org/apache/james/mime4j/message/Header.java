package org.apache.james.mime4j.message;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.MimeIOException;
import org.apache.james.mime4j.parser.AbstractContentHandler;
import org.apache.james.mime4j.parser.ContentHandler;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.util.CharsetUtil;

public class Header implements Iterable<Field> {
    private Map<String, List<Field>> fieldMap = new HashMap();
    private List<Field> fields = new LinkedList();

    public Header() {
    }

    public Header(Header other) {
        Iterator i$ = (Iterator) List.class.getMethod("iterator", new Class[0]).invoke(other.fields, new Object[0]);
        while (true) {
            if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                Object[] objArr = {(Field) Iterator.class.getMethod("next", new Class[0]).invoke(i$, new Object[0])};
                Header.class.getMethod("addField", Field.class).invoke(this, objArr);
            } else {
                return;
            }
        }
    }

    public Header(InputStream is) throws IOException, MimeIOException {
        final MimeStreamParser parser = new MimeStreamParser();
        Object[] objArr = {new AbstractContentHandler() {
            public void endHeader() {
                MimeStreamParser.class.getMethod("stop", new Class[0]).invoke(parser, new Object[0]);
            }

            public void field(Field field) throws MimeException {
                Object[] objArr = {field};
                Header.class.getMethod("addField", Field.class).invoke(Header.this, objArr);
            }
        }};
        MimeStreamParser.class.getMethod("setContentHandler", ContentHandler.class).invoke(parser, objArr);
        try {
            Object[] objArr2 = {is};
            MimeStreamParser.class.getMethod("parse", InputStream.class).invoke(parser, objArr2);
        } catch (MimeException ex) {
            throw new MimeIOException(ex);
        }
    }

    public void addField(Field field) {
        Map<String, List<Field>> map = this.fieldMap;
        Method method = Field.class.getMethod("getName", new Class[0]);
        Object[] objArr = {(String) String.class.getMethod("toLowerCase", new Class[0]).invoke((String) method.invoke(field, new Object[0]), new Object[0])};
        List<Field> values = (List) Map.class.getMethod("get", Object.class).invoke(map, objArr);
        if (values == null) {
            values = new LinkedList<>();
            Map<String, List<Field>> map2 = this.fieldMap;
            Method method2 = Field.class.getMethod("getName", new Class[0]);
            Object[] objArr2 = {(String) String.class.getMethod("toLowerCase", new Class[0]).invoke((String) method2.invoke(field, new Object[0]), new Object[0]), values};
            Map.class.getMethod("put", Object.class, Object.class).invoke(map2, objArr2);
        }
        Object[] objArr3 = {field};
        ((Boolean) List.class.getMethod("add", Object.class).invoke(values, objArr3)).booleanValue();
        Object[] objArr4 = {field};
        ((Boolean) List.class.getMethod("add", Object.class).invoke(this.fields, objArr4)).booleanValue();
    }

    public List<Field> getFields() {
        Class[] clsArr = {List.class};
        return (List) Collections.class.getMethod("unmodifiableList", clsArr).invoke(null, this.fields);
    }

    public Field getField(String name) {
        Map<String, List<Field>> map = this.fieldMap;
        Object[] objArr = {(String) String.class.getMethod("toLowerCase", new Class[0]).invoke(name, new Object[0])};
        List<Field> l = (List) Map.class.getMethod("get", Object.class).invoke(map, objArr);
        if (l != null) {
            if (!((Boolean) List.class.getMethod("isEmpty", new Class[0]).invoke(l, new Object[0])).booleanValue()) {
                Class[] clsArr = {Integer.TYPE};
                return (Field) List.class.getMethod("get", clsArr).invoke(l, new Integer(0));
            }
        }
        return null;
    }

    public List<Field> getFields(String name) {
        Method method = String.class.getMethod("toLowerCase", new Class[0]);
        Object[] objArr = {(String) method.invoke(name, new Object[0])};
        List<Field> l = (List) Map.class.getMethod("get", Object.class).invoke(this.fieldMap, objArr);
        if (l != null) {
            if (!((Boolean) List.class.getMethod("isEmpty", new Class[0]).invoke(l, new Object[0])).booleanValue()) {
                return (List) Collections.class.getMethod("unmodifiableList", List.class).invoke(null, l);
            }
        }
        return (List) Collections.class.getMethod("emptyList", new Class[0]).invoke(null, new Object[0]);
    }

    public Iterator<Field> iterator() {
        Class[] clsArr = {List.class};
        Object[] objArr = {this.fields};
        return (Iterator) List.class.getMethod("iterator", new Class[0]).invoke((List) Collections.class.getMethod("unmodifiableList", clsArr).invoke(null, objArr), new Object[0]);
    }

    public int removeFields(String name) {
        Method method = String.class.getMethod("toLowerCase", new Class[0]);
        Object[] objArr = {(String) method.invoke(name, new Object[0])};
        List<Field> removed = (List) Map.class.getMethod("remove", Object.class).invoke(this.fieldMap, objArr);
        if (removed != null) {
            if (!((Boolean) List.class.getMethod("isEmpty", new Class[0]).invoke(removed, new Object[0])).booleanValue()) {
                Iterator<Field> iterator = (Iterator) List.class.getMethod("iterator", new Class[0]).invoke(this.fields, new Object[0]);
                while (true) {
                    if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(iterator, new Object[0])).booleanValue()) {
                        Method method2 = Iterator.class.getMethod("next", new Class[0]);
                        Method method3 = Field.class.getMethod("getName", new Class[0]);
                        Object[] objArr2 = {name};
                        if (((Boolean) String.class.getMethod("equalsIgnoreCase", String.class).invoke((String) method3.invoke((Field) method2.invoke(iterator, new Object[0]), new Object[0]), objArr2)).booleanValue()) {
                            Iterator.class.getMethod("remove", new Class[0]).invoke(iterator, new Object[0]);
                        }
                    } else {
                        return ((Integer) List.class.getMethod("size", new Class[0]).invoke(removed, new Object[0])).intValue();
                    }
                }
            }
        }
        return 0;
    }

    public void setField(Field field) {
        Method method = Field.class.getMethod("getName", new Class[0]);
        Method method2 = String.class.getMethod("toLowerCase", new Class[0]);
        Object[] objArr = {(String) method2.invoke((String) method.invoke(field, new Object[0]), new Object[0])};
        List<Field> l = (List) Map.class.getMethod("get", Object.class).invoke(this.fieldMap, objArr);
        if (l != null) {
            if (!((Boolean) List.class.getMethod("isEmpty", new Class[0]).invoke(l, new Object[0])).booleanValue()) {
                List.class.getMethod("clear", new Class[0]).invoke(l, new Object[0]);
                Object[] objArr2 = {field};
                ((Boolean) List.class.getMethod("add", Object.class).invoke(l, objArr2)).booleanValue();
                int firstOccurrence = -1;
                int index = 0;
                Iterator<Field> iterator = (Iterator) List.class.getMethod("iterator", new Class[0]).invoke(this.fields, new Object[0]);
                while (true) {
                    if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(iterator, new Object[0])).booleanValue()) {
                        Method method3 = Iterator.class.getMethod("next", new Class[0]);
                        Method method4 = Field.class.getMethod("getName", new Class[0]);
                        Object[] objArr3 = {(String) Field.class.getMethod("getName", new Class[0]).invoke(field, new Object[0])};
                        if (((Boolean) String.class.getMethod("equalsIgnoreCase", String.class).invoke((String) method4.invoke((Field) method3.invoke(iterator, new Object[0]), new Object[0]), objArr3)).booleanValue()) {
                            Iterator.class.getMethod("remove", new Class[0]).invoke(iterator, new Object[0]);
                            if (firstOccurrence == -1) {
                                firstOccurrence = index;
                            }
                        }
                        index++;
                    } else {
                        List<Field> list = this.fields;
                        Class[] clsArr = {Integer.TYPE, Object.class};
                        List.class.getMethod("add", clsArr).invoke(list, new Integer(firstOccurrence), field);
                        return;
                    }
                }
            }
        }
        Object[] objArr4 = {field};
        Header.class.getMethod("addField", Field.class).invoke(this, objArr4);
    }

    public String toString() {
        StringBuilder str = new StringBuilder(128);
        Iterator i$ = (Iterator) List.class.getMethod("iterator", new Class[0]).invoke(this.fields, new Object[0]);
        while (true) {
            if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                Method method = Iterator.class.getMethod("next", new Class[0]);
                Object[] objArr = {(String) Object.class.getMethod("toString", new Class[0]).invoke((Field) method.invoke(i$, new Object[0]), new Object[0])};
                StringBuilder.class.getMethod("append", String.class).invoke(str, objArr);
                Object[] objArr2 = {CharsetUtil.CRLF};
                StringBuilder.class.getMethod("append", String.class).invoke(str, objArr2);
            } else {
                return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(str, new Object[0]);
            }
        }
    }
}
