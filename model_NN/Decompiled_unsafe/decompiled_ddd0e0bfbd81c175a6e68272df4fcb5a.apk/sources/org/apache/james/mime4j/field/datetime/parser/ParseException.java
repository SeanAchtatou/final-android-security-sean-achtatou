package org.apache.james.mime4j.field.datetime.parser;

import java.lang.reflect.Method;

public class ParseException extends org.apache.james.mime4j.field.ParseException {
    private static final long serialVersionUID = 1;
    public Token currentToken;
    protected String eol;
    public int[][] expectedTokenSequences;
    protected boolean specialConstructor = false;
    public String[] tokenImage;

    public ParseException(Token currentTokenVal, int[][] expectedTokenSequencesVal, String[] tokenImageVal) {
        super("");
        Object[] objArr = {"line.separator", "\n"};
        this.eol = (String) System.class.getMethod("getProperty", String.class, String.class).invoke(null, objArr);
        this.currentToken = currentTokenVal;
        this.expectedTokenSequences = expectedTokenSequencesVal;
        this.tokenImage = tokenImageVal;
    }

    public ParseException() {
        super("Cannot parse field");
        Object[] objArr = {"line.separator", "\n"};
        this.eol = (String) System.class.getMethod("getProperty", String.class, String.class).invoke(null, objArr);
    }

    public ParseException(Throwable cause) {
        super(cause);
        Object[] objArr = {"line.separator", "\n"};
        this.eol = (String) System.class.getMethod("getProperty", String.class, String.class).invoke(null, objArr);
    }

    public ParseException(String message) {
        super(message);
        Object[] objArr = {"line.separator", "\n"};
        this.eol = (String) System.class.getMethod("getProperty", String.class, String.class).invoke(null, objArr);
    }

    public String getMessage() {
        String retval;
        if (!this.specialConstructor) {
            return super.getMessage();
        }
        StringBuffer expected = new StringBuffer();
        int maxSize = 0;
        for (int i = 0; i < this.expectedTokenSequences.length; i++) {
            if (maxSize < this.expectedTokenSequences[i].length) {
                maxSize = this.expectedTokenSequences[i].length;
            }
            for (int i2 : this.expectedTokenSequences[i]) {
                Object[] objArr = {this.tokenImage[i2]};
                Method method = StringBuffer.class.getMethod("append", String.class);
                method.invoke((StringBuffer) StringBuffer.class.getMethod("append", String.class).invoke(expected, objArr), " ");
            }
            if (this.expectedTokenSequences[i][this.expectedTokenSequences[i].length - 1] != 0) {
                StringBuffer.class.getMethod("append", String.class).invoke(expected, "...");
            }
            Object[] objArr2 = {this.eol};
            Method method2 = StringBuffer.class.getMethod("append", String.class);
            method2.invoke((StringBuffer) StringBuffer.class.getMethod("append", String.class).invoke(expected, objArr2), "    ");
        }
        String retval2 = "Encountered \"";
        Token tok = this.currentToken.next;
        int i3 = 0;
        while (true) {
            if (i3 >= maxSize) {
                break;
            }
            if (i3 != 0) {
                Object[] objArr3 = {" "};
                Method method3 = StringBuilder.class.getMethod("append", String.class);
                Method method4 = StringBuilder.class.getMethod("toString", new Class[0]);
                retval2 = (String) method4.invoke((StringBuilder) method3.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), retval2), objArr3), new Object[0]);
            }
            if (tok.kind == 0) {
                Object[] objArr4 = {this.tokenImage[0]};
                Method method5 = StringBuilder.class.getMethod("append", String.class);
                Method method6 = StringBuilder.class.getMethod("toString", new Class[0]);
                retval2 = (String) method6.invoke((StringBuilder) method5.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), retval2), objArr4), new Object[0]);
                break;
            }
            Object[] objArr5 = {(String) ParseException.class.getMethod("add_escapes", String.class).invoke(this, tok.image)};
            Method method7 = StringBuilder.class.getMethod("append", String.class);
            Method method8 = StringBuilder.class.getMethod("toString", new Class[0]);
            retval2 = (String) method8.invoke((StringBuilder) method7.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), retval2), objArr5), new Object[0]);
            tok = tok.next;
            i3++;
        }
        Object[] objArr6 = {"\" at line "};
        Method method9 = StringBuilder.class.getMethod("append", String.class);
        int i4 = this.currentToken.next.beginLine;
        Class[] clsArr = {Integer.TYPE};
        Object[] objArr7 = {new Integer(i4)};
        Method method10 = StringBuilder.class.getMethod("append", clsArr);
        Object[] objArr8 = {", column "};
        Method method11 = StringBuilder.class.getMethod("append", String.class);
        int i5 = this.currentToken.next.beginColumn;
        Class[] clsArr2 = {Integer.TYPE};
        Object[] objArr9 = {new Integer(i5)};
        Method method12 = StringBuilder.class.getMethod("append", clsArr2);
        Method method13 = StringBuilder.class.getMethod("toString", new Class[0]);
        Object[] objArr10 = {(String) method13.invoke((StringBuilder) method12.invoke((StringBuilder) method11.invoke((StringBuilder) method10.invoke((StringBuilder) method9.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), retval2), objArr6), objArr7), objArr8), objArr9), new Object[0])};
        Method method14 = StringBuilder.class.getMethod("append", String.class);
        Object[] objArr11 = {this.eol};
        Method method15 = StringBuilder.class.getMethod("append", String.class);
        Method method16 = StringBuilder.class.getMethod("toString", new Class[0]);
        String retval3 = (String) method16.invoke((StringBuilder) method15.invoke((StringBuilder) method14.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr10), "."), objArr11), new Object[0]);
        if (this.expectedTokenSequences.length == 1) {
            Object[] objArr12 = {"Was expecting:"};
            Method method17 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr13 = {this.eol};
            Method method18 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr14 = {"    "};
            Method method19 = StringBuilder.class.getMethod("append", String.class);
            Method method20 = StringBuilder.class.getMethod("toString", new Class[0]);
            retval = (String) method20.invoke((StringBuilder) method19.invoke((StringBuilder) method18.invoke((StringBuilder) method17.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), retval3), objArr12), objArr13), objArr14), new Object[0]);
        } else {
            Object[] objArr15 = {"Was expecting one of:"};
            Method method21 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr16 = {this.eol};
            Method method22 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr17 = {"    "};
            Method method23 = StringBuilder.class.getMethod("append", String.class);
            Method method24 = StringBuilder.class.getMethod("toString", new Class[0]);
            retval = (String) method24.invoke((StringBuilder) method23.invoke((StringBuilder) method22.invoke((StringBuilder) method21.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), retval3), objArr15), objArr16), objArr17), new Object[0]);
        }
        StringBuilder sb = new StringBuilder();
        Class[] clsArr3 = {String.class};
        Object[] objArr18 = {retval};
        Object[] objArr19 = {(String) StringBuffer.class.getMethod("toString", new Class[0]).invoke(expected, new Object[0])};
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr3).invoke(sb, objArr18), objArr19), new Object[0]);
    }

    /* access modifiers changed from: protected */
    public String add_escapes(String str) {
        StringBuffer retval = new StringBuffer();
        int i = 0;
        while (true) {
            if (i < ((Integer) String.class.getMethod("length", new Class[0]).invoke(str, new Object[0])).intValue()) {
                Class[] clsArr = {Integer.TYPE};
                switch (((Character) String.class.getMethod("charAt", clsArr).invoke(str, new Integer(i))).charValue()) {
                    case 0:
                        break;
                    case 8:
                        Object[] objArr = {"\\b"};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr);
                        break;
                    case 9:
                        Object[] objArr2 = {"\\t"};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr2);
                        break;
                    case 10:
                        Object[] objArr3 = {"\\n"};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr3);
                        break;
                    case 12:
                        Object[] objArr4 = {"\\f"};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr4);
                        break;
                    case 13:
                        Object[] objArr5 = {"\\r"};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr5);
                        break;
                    case '\"':
                        Object[] objArr6 = {"\\\""};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr6);
                        break;
                    case '\'':
                        Object[] objArr7 = {"\\'"};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr7);
                        break;
                    case '\\':
                        Object[] objArr8 = {"\\\\"};
                        StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr8);
                        break;
                    default:
                        Class[] clsArr2 = {Integer.TYPE};
                        char ch = ((Character) String.class.getMethod("charAt", clsArr2).invoke(str, new Integer(i))).charValue();
                        if (ch >= ' ' && ch <= '~') {
                            Class[] clsArr3 = {Character.TYPE};
                            StringBuffer.class.getMethod("append", clsArr3).invoke(retval, new Character(ch));
                            break;
                        } else {
                            Object[] objArr9 = {"0000"};
                            Class[] clsArr4 = {Integer.TYPE, Integer.TYPE};
                            Object[] objArr10 = {(String) Integer.class.getMethod("toString", clsArr4).invoke(null, new Integer(ch), new Integer(16))};
                            Method method = StringBuilder.class.getMethod("append", String.class);
                            Method method2 = StringBuilder.class.getMethod("toString", new Class[0]);
                            String s = (String) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr9), objArr10), new Object[0]);
                            Object[] objArr11 = {"\\u"};
                            Method method3 = String.class.getMethod("length", new Class[0]);
                            int intValue = ((Integer) String.class.getMethod("length", new Class[0]).invoke(s, new Object[0])).intValue();
                            Class[] clsArr5 = {Integer.TYPE, Integer.TYPE};
                            Object[] objArr12 = {(String) String.class.getMethod("substring", clsArr5).invoke(s, new Integer(((Integer) method3.invoke(s, new Object[0])).intValue() - 4), new Integer(intValue))};
                            Method method4 = StringBuilder.class.getMethod("append", String.class);
                            Method method5 = StringBuilder.class.getMethod("toString", new Class[0]);
                            Object[] objArr13 = {(String) method5.invoke((StringBuilder) method4.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr11), objArr12), new Object[0])};
                            StringBuffer.class.getMethod("append", String.class).invoke(retval, objArr13);
                            break;
                        }
                        break;
                }
                i++;
            } else {
                return (String) StringBuffer.class.getMethod("toString", new Class[0]).invoke(retval, new Object[0]);
            }
        }
    }
}
