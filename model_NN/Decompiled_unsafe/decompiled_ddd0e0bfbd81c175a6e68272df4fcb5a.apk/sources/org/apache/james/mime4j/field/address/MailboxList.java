package org.apache.james.mime4j.field.address;

import java.io.PrintStream;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MailboxList extends AbstractList<Mailbox> implements Serializable {
    private static final long serialVersionUID = 1;
    private final List<Mailbox> mailboxes;

    public MailboxList(List<Mailbox> mailboxes2, boolean dontCopy) {
        if (mailboxes2 != null) {
            this.mailboxes = !dontCopy ? new ArrayList<>(mailboxes2) : mailboxes2;
        } else {
            this.mailboxes = (List) Collections.class.getMethod("emptyList", new Class[0]).invoke(null, new Object[0]);
        }
    }

    public int size() {
        return ((Integer) List.class.getMethod("size", new Class[0]).invoke(this.mailboxes, new Object[0])).intValue();
    }

    public Mailbox get(int index) {
        List<Mailbox> list = this.mailboxes;
        Class[] clsArr = {Integer.TYPE};
        return (Mailbox) List.class.getMethod("get", clsArr).invoke(list, new Integer(index));
    }

    public void print() {
        int i = 0;
        while (true) {
            if (i < ((Integer) MailboxList.class.getMethod("size", new Class[0]).invoke(this, new Object[0])).intValue()) {
                Class[] clsArr = {Integer.TYPE};
                Object[] objArr = {new Integer(i)};
                PrintStream printStream = System.out;
                Object[] objArr2 = {(String) Mailbox.class.getMethod("toString", new Class[0]).invoke((Mailbox) MailboxList.class.getMethod("get", clsArr).invoke(this, objArr), new Object[0])};
                PrintStream.class.getMethod("println", String.class).invoke(printStream, objArr2);
                i++;
            } else {
                return;
            }
        }
    }
}
