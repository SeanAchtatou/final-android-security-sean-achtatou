package org.apache.james.mime4j.field.address;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Serializable;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.apache.james.mime4j.field.address.parser.ASTaddress_list;
import org.apache.james.mime4j.field.address.parser.AddressListParser;
import org.apache.james.mime4j.field.address.parser.ParseException;

public class AddressList extends AbstractList<Address> implements Serializable {
    private static final long serialVersionUID = 1;
    private final List<? extends Address> addresses;

    public AddressList(List<? extends Address> addresses2, boolean dontCopy) {
        if (addresses2 != null) {
            this.addresses = !dontCopy ? new ArrayList<>(addresses2) : addresses2;
        } else {
            this.addresses = (List) Collections.class.getMethod("emptyList", new Class[0]).invoke(null, new Object[0]);
        }
    }

    public int size() {
        return ((Integer) List.class.getMethod("size", new Class[0]).invoke(this.addresses, new Object[0])).intValue();
    }

    public Address get(int index) {
        List<? extends Address> list = this.addresses;
        Class[] clsArr = {Integer.TYPE};
        return (Address) List.class.getMethod("get", clsArr).invoke(list, new Integer(index));
    }

    public MailboxList flatten() {
        boolean groupDetected = false;
        Iterator i$ = (Iterator) List.class.getMethod("iterator", new Class[0]).invoke(this.addresses, new Object[0]);
        while (true) {
            if (!((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                break;
            }
            if (!(((Address) Iterator.class.getMethod("next", new Class[0]).invoke(i$, new Object[0])) instanceof Mailbox)) {
                groupDetected = true;
                break;
            }
        }
        if (!groupDetected) {
            return new MailboxList(this.addresses, true);
        }
        List<Mailbox> results = new ArrayList<>();
        Iterator i$2 = (Iterator) List.class.getMethod("iterator", new Class[0]).invoke(this.addresses, new Object[0]);
        while (true) {
            if (!((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$2, new Object[0])).booleanValue()) {
                return new MailboxList(results, false);
            }
            Method method = Iterator.class.getMethod("next", new Class[0]);
            Method method2 = Address.class.getMethod("addMailboxesTo", List.class);
            method2.invoke((Address) method.invoke(i$2, new Object[0]), results);
        }
    }

    public void print() {
        Iterator i$ = (Iterator) List.class.getMethod("iterator", new Class[0]).invoke(this.addresses, new Object[0]);
        while (true) {
            if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                Method method = Iterator.class.getMethod("next", new Class[0]);
                PrintStream printStream = System.out;
                Object[] objArr = {(String) Address.class.getMethod("toString", new Class[0]).invoke((Address) method.invoke(i$, new Object[0]), new Object[0])};
                PrintStream.class.getMethod("println", String.class).invoke(printStream, objArr);
            } else {
                return;
            }
        }
    }

    public static AddressList parse(String rawAddressList) throws ParseException {
        AddressListParser parser = new AddressListParser(new StringReader(rawAddressList));
        Method method = Builder.class.getMethod("getInstance", new Class[0]);
        Object[] objArr = {(ASTaddress_list) AddressListParser.class.getMethod("parseAddressList", new Class[0]).invoke(parser, new Object[0])};
        return (AddressList) Builder.class.getMethod("buildAddressList", ASTaddress_list.class).invoke((Builder) method.invoke(null, new Object[0]), objArr);
    }

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                Object[] objArr = {"> "};
                PrintStream.class.getMethod("print", String.class).invoke(System.out, objArr);
                String line = (String) BufferedReader.class.getMethod("readLine", new Class[0]).invoke(reader, new Object[0]);
                if (((Integer) String.class.getMethod("length", new Class[0]).invoke(line, new Object[0])).intValue() == 0) {
                    break;
                }
                Method method = String.class.getMethod("toLowerCase", new Class[0]);
                Object[] objArr2 = {"exit"};
                if (((Boolean) String.class.getMethod("equals", Object.class).invoke((String) method.invoke(line, new Object[0]), objArr2)).booleanValue()) {
                    break;
                }
                Method method2 = String.class.getMethod("toLowerCase", new Class[0]);
                Object[] objArr3 = {"quit"};
                if (((Boolean) String.class.getMethod("equals", Object.class).invoke((String) method2.invoke(line, new Object[0]), objArr3)).booleanValue()) {
                    break;
                }
                Object[] objArr4 = {line};
                Method method3 = AddressList.class.getMethod("print", new Class[0]);
                method3.invoke((AddressList) AddressList.class.getMethod("parse", String.class).invoke(null, objArr4), new Object[0]);
            } catch (Exception e) {
                Exception.class.getMethod("printStackTrace", new Class[0]).invoke(e, new Object[0]);
                Class[] clsArr = {Long.TYPE};
                Thread.class.getMethod("sleep", clsArr).invoke(null, new Long(300));
            }
        }
        Object[] objArr5 = {"Goodbye."};
        PrintStream.class.getMethod("println", String.class).invoke(System.out, objArr5);
    }
}
