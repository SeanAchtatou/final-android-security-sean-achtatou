package org.apache.james.mime4j.field.contenttype.parser;

import java.io.InputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ContentTypeParser implements ContentTypeParserConstants {
    private static int[] jj_la1_0;
    private Vector<int[]> jj_expentries;
    private int[] jj_expentry;
    private int jj_gen;
    SimpleCharStream jj_input_stream;
    private int jj_kind;
    private final int[] jj_la1;
    public Token jj_nt;
    private int jj_ntk;
    private List<String> paramNames;
    private List<String> paramValues;
    private String subtype;
    public Token token;
    public ContentTypeParserTokenManager token_source;
    private String type;

    public String getType() {
        return this.type;
    }

    public String getSubType() {
        return this.subtype;
    }

    public List<String> getParamNames() {
        return this.paramNames;
    }

    public List<String> getParamValues() {
        return this.paramValues;
    }

    public static void main(String[] args) throws ParseException {
        while (true) {
            try {
                ContentTypeParser.class.getMethod("parseLine", new Class[0]).invoke(new ContentTypeParser(System.in), new Object[0]);
            } catch (Exception x) {
                Exception.class.getMethod("printStackTrace", new Class[0]).invoke(x, new Object[0]);
                return;
            }
        }
    }

    public final void parseLine() throws ParseException {
        int i;
        ContentTypeParser.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        if (this.jj_ntk == -1) {
            i = ((Integer) ContentTypeParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 1:
                Class[] clsArr = {Integer.TYPE};
                ContentTypeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(1));
                break;
            default:
                this.jj_la1[0] = this.jj_gen;
                break;
        }
        Class[] clsArr2 = {Integer.TYPE};
        ContentTypeParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(2));
    }

    public final void parseAll() throws ParseException {
        ContentTypeParser.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        Class[] clsArr = {Integer.TYPE};
        ContentTypeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(0));
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x008f A[FALL_THROUGH, LOOP:0: B:1:0x0065->B:8:0x008f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0084 A[SYNTHETIC] */
    public final void parse() throws org.apache.james.mime4j.field.contenttype.parser.ParseException {
        /*
            r9 = this;
            r3 = 21
            r5 = 1
            java.lang.Class[] r6 = new java.lang.Class[r5]
            java.lang.Object[] r7 = new java.lang.Object[r5]
            r5 = 0
            java.lang.Class r8 = java.lang.Integer.TYPE
            r6[r5] = r8
            java.lang.Integer r8 = new java.lang.Integer
            r8.<init>(r3)
            r7[r5] = r8
            java.lang.String r5 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser> r8 = org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser.class
            java.lang.reflect.Method r5 = r8.getMethod(r5, r6)
            java.lang.Object r1 = r5.invoke(r9, r7)
            org.apache.james.mime4j.field.contenttype.parser.Token r1 = (org.apache.james.mime4j.field.contenttype.parser.Token) r1
            r2 = 3
            r5 = 1
            java.lang.Class[] r6 = new java.lang.Class[r5]
            java.lang.Object[] r7 = new java.lang.Object[r5]
            r5 = 0
            java.lang.Class r8 = java.lang.Integer.TYPE
            r6[r5] = r8
            java.lang.Integer r8 = new java.lang.Integer
            r8.<init>(r2)
            r7[r5] = r8
            java.lang.String r5 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser> r8 = org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser.class
            java.lang.reflect.Method r5 = r8.getMethod(r5, r6)
            r5.invoke(r9, r7)
            r5 = 1
            java.lang.Class[] r6 = new java.lang.Class[r5]
            java.lang.Object[] r7 = new java.lang.Object[r5]
            r5 = 0
            java.lang.Class r8 = java.lang.Integer.TYPE
            r6[r5] = r8
            java.lang.Integer r8 = new java.lang.Integer
            r8.<init>(r3)
            r7[r5] = r8
            java.lang.String r5 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser> r8 = org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser.class
            java.lang.reflect.Method r5 = r8.getMethod(r5, r6)
            java.lang.Object r0 = r5.invoke(r9, r7)
            org.apache.james.mime4j.field.contenttype.parser.Token r0 = (org.apache.james.mime4j.field.contenttype.parser.Token) r0
            java.lang.String r2 = r1.image
            r9.type = r2
            java.lang.String r2 = r0.image
            r9.subtype = r2
        L_0x0065:
            int r2 = r9.jj_ntk
            r3 = -1
            if (r2 != r3) goto L_0x008c
            r5 = 0
            java.lang.Class[] r6 = new java.lang.Class[r5]
            java.lang.Object[] r7 = new java.lang.Object[r5]
            java.lang.String r5 = "jj_ntk"
            java.lang.Class<org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser> r8 = org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser.class
            java.lang.reflect.Method r5 = r8.getMethod(r5, r6)
            java.lang.Object r5 = r5.invoke(r9, r7)
            java.lang.Integer r5 = (java.lang.Integer) r5
            int r2 = r5.intValue()
        L_0x0081:
            switch(r2) {
                case 4: goto L_0x008f;
                default: goto L_0x0084;
            }
        L_0x0084:
            int[] r2 = r9.jj_la1
            r3 = 1
            int r4 = r9.jj_gen
            r2[r3] = r4
            return
        L_0x008c:
            int r2 = r9.jj_ntk
            goto L_0x0081
        L_0x008f:
            r2 = 4
            r5 = 1
            java.lang.Class[] r6 = new java.lang.Class[r5]
            java.lang.Object[] r7 = new java.lang.Object[r5]
            r5 = 0
            java.lang.Class r8 = java.lang.Integer.TYPE
            r6[r5] = r8
            java.lang.Integer r8 = new java.lang.Integer
            r8.<init>(r2)
            r7[r5] = r8
            java.lang.String r5 = "jj_consume_token"
            java.lang.Class<org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser> r8 = org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser.class
            java.lang.reflect.Method r5 = r8.getMethod(r5, r6)
            r5.invoke(r9, r7)
            r5 = 0
            java.lang.Class[] r6 = new java.lang.Class[r5]
            java.lang.Object[] r7 = new java.lang.Object[r5]
            java.lang.String r5 = "parameter"
            java.lang.Class<org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser> r8 = org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser.class
            java.lang.reflect.Method r5 = r8.getMethod(r5, r6)
            r5.invoke(r9, r7)
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser.parse():void");
    }

    public final void parameter() throws ParseException {
        Class[] clsArr = {Integer.TYPE};
        Object[] objArr = {new Integer(21)};
        Class[] clsArr2 = {Integer.TYPE};
        ContentTypeParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(5));
        Method method = ContentTypeParser.class.getMethod("value", new Class[0]);
        List<String> list = this.paramNames;
        String str = ((Token) ContentTypeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, objArr)).image;
        Object[] objArr2 = {str};
        ((Boolean) List.class.getMethod("add", Object.class).invoke(list, objArr2)).booleanValue();
        Object[] objArr3 = {(String) method.invoke(this, new Object[0])};
        ((Boolean) List.class.getMethod("add", Object.class).invoke(this.paramValues, objArr3)).booleanValue();
    }

    public final String value() throws ParseException {
        int i;
        Token t;
        if (this.jj_ntk == -1) {
            i = ((Integer) ContentTypeParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 19:
                Class[] clsArr = {Integer.TYPE};
                t = (Token) ContentTypeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(19));
                break;
            case 20:
                Class[] clsArr2 = {Integer.TYPE};
                t = (Token) ContentTypeParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(20));
                break;
            case 21:
                Class[] clsArr3 = {Integer.TYPE};
                t = (Token) ContentTypeParser.class.getMethod("jj_consume_token", clsArr3).invoke(this, new Integer(21));
                break;
            default:
                this.jj_la1[2] = this.jj_gen;
                Class[] clsArr4 = {Integer.TYPE};
                ContentTypeParser.class.getMethod("jj_consume_token", clsArr4).invoke(this, new Integer(-1));
                throw new ParseException();
        }
        return t.image;
    }

    static {
        ContentTypeParser.class.getMethod("jj_la1_0", new Class[0]).invoke(null, new Object[0]);
    }

    private static void jj_la1_0() {
        jj_la1_0 = new int[]{2, 16, 3670016};
    }

    public ContentTypeParser(InputStream stream) {
        this(stream, null);
    }

    public ContentTypeParser(InputStream stream, String encoding) {
        this.paramNames = new ArrayList();
        this.paramValues = new ArrayList();
        this.jj_la1 = new int[3];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        try {
            this.jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1);
            this.token_source = new ContentTypeParserTokenManager(this.jj_input_stream);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 3; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public void ReInit(InputStream stream) {
        Object[] objArr = {stream, null};
        ContentTypeParser.class.getMethod("ReInit", InputStream.class, String.class).invoke(this, objArr);
    }

    public void ReInit(InputStream stream, String encoding) {
        try {
            SimpleCharStream simpleCharStream = this.jj_input_stream;
            Class[] clsArr = {InputStream.class, String.class, Integer.TYPE, Integer.TYPE};
            SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(simpleCharStream, stream, encoding, new Integer(1), new Integer(1));
            ContentTypeParserTokenManager contentTypeParserTokenManager = this.token_source;
            Object[] objArr = {this.jj_input_stream};
            ContentTypeParserTokenManager.class.getMethod("ReInit", SimpleCharStream.class).invoke(contentTypeParserTokenManager, objArr);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 3; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public ContentTypeParser(Reader stream) {
        this.paramNames = new ArrayList();
        this.paramValues = new ArrayList();
        this.jj_la1 = new int[3];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.jj_input_stream = new SimpleCharStream(stream, 1, 1);
        this.token_source = new ContentTypeParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public void ReInit(Reader stream) {
        SimpleCharStream simpleCharStream = this.jj_input_stream;
        Class[] clsArr = {Reader.class, Integer.TYPE, Integer.TYPE};
        SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(simpleCharStream, stream, new Integer(1), new Integer(1));
        ContentTypeParserTokenManager contentTypeParserTokenManager = this.token_source;
        Object[] objArr = {this.jj_input_stream};
        ContentTypeParserTokenManager.class.getMethod("ReInit", SimpleCharStream.class).invoke(contentTypeParserTokenManager, objArr);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public ContentTypeParser(ContentTypeParserTokenManager tm) {
        this.paramNames = new ArrayList();
        this.paramValues = new ArrayList();
        this.jj_la1 = new int[3];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public void ReInit(ContentTypeParserTokenManager tm) {
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    private final Token jj_consume_token(int kind) throws ParseException {
        Token oldToken = this.token;
        if (oldToken.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token token3 = (Token) ContentTypeParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token2.next = token3;
            this.token = token3;
        }
        this.jj_ntk = -1;
        if (this.token.kind == kind) {
            this.jj_gen++;
            return this.token;
        }
        this.token = oldToken;
        this.jj_kind = kind;
        throw ((ParseException) ContentTypeParser.class.getMethod("generateParseException", new Class[0]).invoke(this, new Object[0]));
    }

    public final Token getNextToken() {
        if (this.token.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token token3 = (Token) ContentTypeParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token2.next = token3;
            this.token = token3;
        }
        this.jj_ntk = -1;
        this.jj_gen++;
        return this.token;
    }

    public final Token getToken(int index) {
        Token t;
        int i = 0;
        Token t2 = this.token;
        while (i < index) {
            if (t2.next != null) {
                t = t2.next;
            } else {
                t = (Token) ContentTypeParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
                t2.next = t;
            }
            i++;
            t2 = t;
        }
        return t2;
    }

    private final int jj_ntk() {
        Token token2 = this.token.next;
        this.jj_nt = token2;
        if (token2 == null) {
            Token token3 = this.token;
            Token token4 = (Token) ContentTypeParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token3.next = token4;
            int i = token4.kind;
            this.jj_ntk = i;
            return i;
        }
        int i2 = this.jj_nt.kind;
        this.jj_ntk = i2;
        return i2;
    }

    public ParseException generateParseException() {
        Vector.class.getMethod("removeAllElements", new Class[0]).invoke(this.jj_expentries, new Object[0]);
        boolean[] la1tokens = new boolean[24];
        for (int i = 0; i < 24; i++) {
            la1tokens[i] = false;
        }
        if (this.jj_kind >= 0) {
            la1tokens[this.jj_kind] = true;
            this.jj_kind = -1;
        }
        for (int i2 = 0; i2 < 3; i2++) {
            if (this.jj_la1[i2] == this.jj_gen) {
                for (int j = 0; j < 32; j++) {
                    if ((jj_la1_0[i2] & (1 << j)) != 0) {
                        la1tokens[j] = true;
                    }
                }
            }
        }
        for (int i3 = 0; i3 < 24; i3++) {
            if (la1tokens[i3]) {
                this.jj_expentry = new int[1];
                this.jj_expentry[0] = i3;
                Vector<int[]> vector = this.jj_expentries;
                Object[] objArr = {this.jj_expentry};
                Vector.class.getMethod("addElement", Object.class).invoke(vector, objArr);
            }
        }
        int[][] exptokseq = new int[((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.jj_expentries, new Object[0])).intValue()][];
        int i4 = 0;
        while (true) {
            if (i4 >= ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.jj_expentries, new Object[0])).intValue()) {
                return new ParseException(this.token, exptokseq, tokenImage);
            }
            Vector<int[]> vector2 = this.jj_expentries;
            Class[] clsArr = {Integer.TYPE};
            exptokseq[i4] = (int[]) Vector.class.getMethod("elementAt", clsArr).invoke(vector2, new Integer(i4));
            i4++;
        }
    }

    public final void enable_tracing() {
    }

    public final void disable_tracing() {
    }
}
