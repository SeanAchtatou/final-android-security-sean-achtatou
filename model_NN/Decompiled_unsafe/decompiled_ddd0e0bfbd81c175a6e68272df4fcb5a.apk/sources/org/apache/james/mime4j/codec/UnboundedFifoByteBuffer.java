package org.apache.james.mime4j.codec;

import java.util.Iterator;
import java.util.NoSuchElementException;

class UnboundedFifoByteBuffer {
    protected byte[] buffer;
    protected int head;
    protected int tail;

    static /* synthetic */ int access$000(UnboundedFifoByteBuffer x0, int x1) {
        Class[] clsArr = {Integer.TYPE};
        return ((Integer) UnboundedFifoByteBuffer.class.getMethod("increment", clsArr).invoke(x0, new Integer(x1))).intValue();
    }

    static /* synthetic */ int access$100(UnboundedFifoByteBuffer x0, int x1) {
        Class[] clsArr = {Integer.TYPE};
        return ((Integer) UnboundedFifoByteBuffer.class.getMethod("decrement", clsArr).invoke(x0, new Integer(x1))).intValue();
    }

    public UnboundedFifoByteBuffer() {
        this(32);
    }

    public UnboundedFifoByteBuffer(int initialSize) {
        if (initialSize <= 0) {
            throw new IllegalArgumentException("The size must be greater than 0");
        }
        this.buffer = new byte[(initialSize + 1)];
        this.head = 0;
        this.tail = 0;
    }

    public int size() {
        if (this.tail < this.head) {
            return (this.buffer.length - this.head) + this.tail;
        }
        return this.tail - this.head;
    }

    public boolean isEmpty() {
        return ((Integer) UnboundedFifoByteBuffer.class.getMethod("size", new Class[0]).invoke(this, new Object[0])).intValue() == 0;
    }

    public boolean add(byte b) {
        if (((Integer) UnboundedFifoByteBuffer.class.getMethod("size", new Class[0]).invoke(this, new Object[0])).intValue() + 1 >= this.buffer.length) {
            byte[] tmp = new byte[(((this.buffer.length - 1) * 2) + 1)];
            int j = 0;
            int i = this.head;
            while (i != this.tail) {
                tmp[j] = this.buffer[i];
                this.buffer[i] = 0;
                j++;
                i++;
                if (i == this.buffer.length) {
                    i = 0;
                }
            }
            this.buffer = tmp;
            this.head = 0;
            this.tail = j;
        }
        this.buffer[this.tail] = b;
        this.tail++;
        if (this.tail < this.buffer.length) {
            return true;
        }
        this.tail = 0;
        return true;
    }

    public byte get() {
        if (!((Boolean) UnboundedFifoByteBuffer.class.getMethod("isEmpty", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
            return this.buffer[this.head];
        }
        throw new IllegalStateException("The buffer is already empty");
    }

    public byte remove() {
        if (((Boolean) UnboundedFifoByteBuffer.class.getMethod("isEmpty", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
            throw new IllegalStateException("The buffer is already empty");
        }
        byte element = this.buffer[this.head];
        this.head++;
        if (this.head >= this.buffer.length) {
            this.head = 0;
        }
        return element;
    }

    private int increment(int index) {
        int index2 = index + 1;
        if (index2 >= this.buffer.length) {
            return 0;
        }
        return index2;
    }

    private int decrement(int index) {
        int index2 = index - 1;
        if (index2 < 0) {
            return this.buffer.length - 1;
        }
        return index2;
    }

    public Iterator<Byte> iterator() {
        return new Iterator<Byte>() {
            private int index = UnboundedFifoByteBuffer.this.head;
            private int lastReturnedIndex = -1;

            public boolean hasNext() {
                return this.index != UnboundedFifoByteBuffer.this.tail;
            }

            public Byte next() {
                if (!((Boolean) AnonymousClass1.class.getMethod("hasNext", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
                    throw new NoSuchElementException();
                }
                this.lastReturnedIndex = this.index;
                UnboundedFifoByteBuffer unboundedFifoByteBuffer = UnboundedFifoByteBuffer.this;
                int i = this.index;
                Class[] clsArr = {UnboundedFifoByteBuffer.class, Integer.TYPE};
                this.index = ((Integer) UnboundedFifoByteBuffer.class.getMethod("access$000", clsArr).invoke(null, unboundedFifoByteBuffer, new Integer(i))).intValue();
                return new Byte(UnboundedFifoByteBuffer.this.buffer[this.lastReturnedIndex]);
            }

            public void remove() {
                if (this.lastReturnedIndex == -1) {
                    throw new IllegalStateException();
                } else if (this.lastReturnedIndex == UnboundedFifoByteBuffer.this.head) {
                    ((Byte) UnboundedFifoByteBuffer.class.getMethod("remove", new Class[0]).invoke(UnboundedFifoByteBuffer.this, new Object[0])).byteValue();
                    this.lastReturnedIndex = -1;
                } else {
                    int i = this.lastReturnedIndex + 1;
                    while (i != UnboundedFifoByteBuffer.this.tail) {
                        if (i >= UnboundedFifoByteBuffer.this.buffer.length) {
                            UnboundedFifoByteBuffer.this.buffer[i - 1] = UnboundedFifoByteBuffer.this.buffer[0];
                            i = 0;
                        } else {
                            UnboundedFifoByteBuffer.this.buffer[i - 1] = UnboundedFifoByteBuffer.this.buffer[i];
                            i++;
                        }
                    }
                    this.lastReturnedIndex = -1;
                    UnboundedFifoByteBuffer unboundedFifoByteBuffer = UnboundedFifoByteBuffer.this;
                    UnboundedFifoByteBuffer unboundedFifoByteBuffer2 = UnboundedFifoByteBuffer.this;
                    int i2 = UnboundedFifoByteBuffer.this.tail;
                    Class[] clsArr = {UnboundedFifoByteBuffer.class, Integer.TYPE};
                    unboundedFifoByteBuffer.tail = ((Integer) UnboundedFifoByteBuffer.class.getMethod("access$100", clsArr).invoke(null, unboundedFifoByteBuffer2, new Integer(i2))).intValue();
                    UnboundedFifoByteBuffer.this.buffer[UnboundedFifoByteBuffer.this.tail] = 0;
                    UnboundedFifoByteBuffer unboundedFifoByteBuffer3 = UnboundedFifoByteBuffer.this;
                    int i3 = this.index;
                    Class[] clsArr2 = {UnboundedFifoByteBuffer.class, Integer.TYPE};
                    this.index = ((Integer) UnboundedFifoByteBuffer.class.getMethod("access$100", clsArr2).invoke(null, unboundedFifoByteBuffer3, new Integer(i3))).intValue();
                }
            }
        };
    }
}
