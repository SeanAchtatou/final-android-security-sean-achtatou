package org.apache.james.mime4j.field.language.parser;

import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Method;
import org.apache.james.mime4j.field.datetime.parser.DateTimeParserConstants;

public class ContentLanguageParserTokenManager implements ContentLanguageParserConstants {
    static final long[] jjbitVec0 = {0, 0, -1, -1};
    public static final int[] jjnewLexState = {-1, -1, -1, -1, 1, 0, -1, 2, -1, -1, -1, -1, -1, 3, -1, -1, 0, -1, -1, -1, -1, -1, -1};
    static final int[] jjnextStates = new int[0];
    public static final String[] jjstrLiteralImages = {"", ",", "-", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ".", null, null};
    static final long[] jjtoMore = {65488};
    static final long[] jjtoSkip = {40};
    static final long[] jjtoSpecial = {8};
    static final long[] jjtoToken = {2031623};
    public static final String[] lexStateNames = {"DEFAULT", "INCOMMENT", "NESTED_COMMENT", "INQUOTEDSTRING"};
    int commentNest;
    protected char curChar;
    int curLexState;
    public PrintStream debugStream;
    int defaultLexState;
    StringBuffer image;
    protected SimpleCharStream input_stream;
    int jjimageLen;
    int jjmatchedKind;
    int jjmatchedPos;
    int jjnewStateCnt;
    int jjround;
    private final int[] jjrounds;
    private final int[] jjstateSet;
    int lengthOfMatch;

    public void setDebugStream(PrintStream ds) {
        this.debugStream = ds;
    }

    private final int jjStopStringLiteralDfa_0(int pos, long active0) {
        return -1;
    }

    private final int jjStartNfa_0(int pos, long active0) {
        Class[] clsArr = {Integer.TYPE, Long.TYPE};
        int intValue = ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjStopStringLiteralDfa_0", clsArr).invoke(this, new Integer(pos), new Long(active0))).intValue();
        Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
        return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjMoveNfa_0", clsArr2).invoke(this, new Integer(intValue), new Integer(pos + 1))).intValue();
    }

    private final int jjStopAtPos(int pos, int kind) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        return pos + 1;
    }

    private final int jjStartNfaWithStates_0(int pos, int kind, int state) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        try {
            this.curChar = ((Character) SimpleCharStream.class.getMethod("readChar", new Class[0]).invoke(this.input_stream, new Object[0])).charValue();
            Class[] clsArr = {Integer.TYPE, Integer.TYPE};
            return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjMoveNfa_0", clsArr).invoke(this, new Integer(state), new Integer(pos + 1))).intValue();
        } catch (IOException e) {
            return pos + 1;
        }
    }

    private final int jjMoveStringLiteralDfa0_0() {
        switch (this.curChar) {
            case '\"':
                Class[] clsArr = {Integer.TYPE, Integer.TYPE};
                return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjStopAtPos", clsArr).invoke(this, new Integer(0), new Integer(13))).intValue();
            case '(':
                Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjStopAtPos", clsArr2).invoke(this, new Integer(0), new Integer(4))).intValue();
            case ',':
                Class[] clsArr3 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjStopAtPos", clsArr3).invoke(this, new Integer(0), new Integer(1))).intValue();
            case '-':
                Class[] clsArr4 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjStopAtPos", clsArr4).invoke(this, new Integer(0), new Integer(2))).intValue();
            case DateTimeParserConstants.DIGITS:
                Class[] clsArr5 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjStopAtPos", clsArr5).invoke(this, new Integer(0), new Integer(20))).intValue();
            default:
                Class[] clsArr6 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjMoveNfa_0", clsArr6).invoke(this, new Integer(4), new Integer(0))).intValue();
        }
    }

    private final void jjCheckNAdd(int state) {
        if (this.jjrounds[state] != this.jjround) {
            int[] iArr = this.jjstateSet;
            int i = this.jjnewStateCnt;
            this.jjnewStateCnt = i + 1;
            iArr[i] = state;
            this.jjrounds[state] = this.jjround;
        }
    }

    private final void jjAddStates(int start, int end) {
        while (true) {
            int[] iArr = this.jjstateSet;
            int i = this.jjnewStateCnt;
            this.jjnewStateCnt = i + 1;
            iArr[i] = jjnextStates[start];
            int start2 = start + 1;
            if (start != end) {
                start = start2;
            } else {
                return;
            }
        }
    }

    private final void jjCheckNAddTwoStates(int state1, int state2) {
        Class[] clsArr = {Integer.TYPE};
        ContentLanguageParserTokenManager.class.getMethod("jjCheckNAdd", clsArr).invoke(this, new Integer(state1));
        Class[] clsArr2 = {Integer.TYPE};
        ContentLanguageParserTokenManager.class.getMethod("jjCheckNAdd", clsArr2).invoke(this, new Integer(state2));
    }

    private final void jjCheckNAddStates(int start, int end) {
        while (true) {
            int i = jjnextStates[start];
            Class[] clsArr = {Integer.TYPE};
            ContentLanguageParserTokenManager.class.getMethod("jjCheckNAdd", clsArr).invoke(this, new Integer(i));
            int start2 = start + 1;
            if (start != end) {
                start = start2;
            } else {
                return;
            }
        }
    }

    private final void jjCheckNAddStates(int start) {
        int i = jjnextStates[start];
        Class[] clsArr = {Integer.TYPE};
        ContentLanguageParserTokenManager.class.getMethod("jjCheckNAdd", clsArr).invoke(this, new Integer(i));
        int i2 = jjnextStates[start + 1];
        Class[] clsArr2 = {Integer.TYPE};
        ContentLanguageParserTokenManager.class.getMethod("jjCheckNAdd", clsArr2).invoke(this, new Integer(i2));
    }

    private final int jjMoveNfa_0(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 4;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            int i2 = this.jjround + 1;
            this.jjround = i2;
            if (i2 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long l = 1 << this.curChar;
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((4294977024L & l) != 0) {
                                kind = 3;
                                jjCheckNAdd(0);
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if ((287948901175001088L & l) == 0) {
                                continue;
                            } else {
                                if (kind > 17) {
                                    kind = 17;
                                }
                                jjCheckNAdd(1);
                                continue;
                            }
                        case 3:
                            if ((287948901175001088L & l) == 0) {
                                continue;
                            } else {
                                if (kind > 19) {
                                    kind = 19;
                                }
                                jjCheckNAdd(3);
                                continue;
                            }
                        case 4:
                            if ((287948901175001088L & l) != 0) {
                                if (kind > 19) {
                                    kind = 19;
                                }
                                jjCheckNAdd(3);
                            } else if ((4294977024L & l) != 0) {
                                if (kind > 3) {
                                    kind = 3;
                                }
                                jjCheckNAdd(0);
                            }
                            if ((287948901175001088L & l) == 0) {
                                continue;
                            } else {
                                if (kind > 17) {
                                    kind = 17;
                                }
                                jjCheckNAdd(1);
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else if (this.curChar < 128) {
                long l2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 2:
                            if ((576460743847706622L & l2) == 0) {
                                continue;
                            } else {
                                if (kind > 18) {
                                    kind = 18;
                                }
                                jjCheckNAdd(2);
                                continue;
                            }
                        case 3:
                            if ((576460743847706622L & l2) == 0) {
                                continue;
                            } else {
                                if (kind > 19) {
                                    kind = 19;
                                }
                                jjCheckNAdd(3);
                                continue;
                            }
                        case 4:
                            if ((576460743847706622L & l2) != 0) {
                                if (kind > 19) {
                                    kind = 19;
                                }
                                jjCheckNAdd(3);
                            }
                            if ((576460743847706622L & l2) == 0) {
                                continue;
                            } else {
                                if (kind > 18) {
                                    kind = 18;
                                }
                                jjCheckNAdd(2);
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else {
                int i3 = (this.curChar & 255) >> 6;
                long j = 1 << (this.curChar & '?');
                do {
                    i--;
                    int i4 = this.jjstateSet[i];
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            curPos++;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 4 - startsAt;
            if (i != startsAt) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return curPos;
        }
    }

    private final int jjStopStringLiteralDfa_1(int pos, long active0) {
        return -1;
    }

    private final int jjStartNfa_1(int pos, long active0) {
        Class[] clsArr = {Integer.TYPE, Long.TYPE};
        int intValue = ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjStopStringLiteralDfa_1", clsArr).invoke(this, new Integer(pos), new Long(active0))).intValue();
        Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
        return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjMoveNfa_1", clsArr2).invoke(this, new Integer(intValue), new Integer(pos + 1))).intValue();
    }

    private final int jjStartNfaWithStates_1(int pos, int kind, int state) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        try {
            this.curChar = ((Character) SimpleCharStream.class.getMethod("readChar", new Class[0]).invoke(this.input_stream, new Object[0])).charValue();
            Class[] clsArr = {Integer.TYPE, Integer.TYPE};
            return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjMoveNfa_1", clsArr).invoke(this, new Integer(state), new Integer(pos + 1))).intValue();
        } catch (IOException e) {
            return pos + 1;
        }
    }

    private final int jjMoveStringLiteralDfa0_1() {
        switch (this.curChar) {
            case '(':
                Class[] clsArr = {Integer.TYPE, Integer.TYPE};
                return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjStopAtPos", clsArr).invoke(this, new Integer(0), new Integer(7))).intValue();
            case ')':
                Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjStopAtPos", clsArr2).invoke(this, new Integer(0), new Integer(5))).intValue();
            default:
                Class[] clsArr3 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjMoveNfa_1", clsArr3).invoke(this, new Integer(0), new Integer(0))).intValue();
        }
    }

    private final int jjMoveNfa_1(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 3;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            int i2 = this.jjround + 1;
            this.jjround = i2;
            if (i2 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long j = 1 << this.curChar;
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if (kind > 8) {
                                kind = 8;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (kind > 6) {
                                kind = 6;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else if (this.curChar < 128) {
                long j2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if (kind > 8) {
                                kind = 8;
                            }
                            if (this.curChar == '\\') {
                                int[] iArr = this.jjstateSet;
                                int i3 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i3 + 1;
                                iArr[i3] = 1;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (kind > 6) {
                                kind = 6;
                                continue;
                            } else {
                                continue;
                            }
                        case 2:
                            if (kind > 8) {
                                kind = 8;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else {
                int i22 = (this.curChar & 255) >> 6;
                long l2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((jjbitVec0[i22] & l2) != 0 && kind > 8) {
                                kind = 8;
                                continue;
                            }
                        case 1:
                            if ((jjbitVec0[i22] & l2) != 0 && kind > 6) {
                                kind = 6;
                                continue;
                            }
                    }
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            curPos++;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 3 - startsAt;
            if (i != startsAt) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return curPos;
        }
    }

    private final int jjStopStringLiteralDfa_3(int pos, long active0) {
        return -1;
    }

    private final int jjStartNfa_3(int pos, long active0) {
        Class[] clsArr = {Integer.TYPE, Long.TYPE};
        int intValue = ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjStopStringLiteralDfa_3", clsArr).invoke(this, new Integer(pos), new Long(active0))).intValue();
        Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
        return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjMoveNfa_3", clsArr2).invoke(this, new Integer(intValue), new Integer(pos + 1))).intValue();
    }

    private final int jjStartNfaWithStates_3(int pos, int kind, int state) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        try {
            this.curChar = ((Character) SimpleCharStream.class.getMethod("readChar", new Class[0]).invoke(this.input_stream, new Object[0])).charValue();
            Class[] clsArr = {Integer.TYPE, Integer.TYPE};
            return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjMoveNfa_3", clsArr).invoke(this, new Integer(state), new Integer(pos + 1))).intValue();
        } catch (IOException e) {
            return pos + 1;
        }
    }

    private final int jjMoveStringLiteralDfa0_3() {
        switch (this.curChar) {
            case '\"':
                Class[] clsArr = {Integer.TYPE, Integer.TYPE};
                return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjStopAtPos", clsArr).invoke(this, new Integer(0), new Integer(16))).intValue();
            default:
                Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjMoveNfa_3", clsArr2).invoke(this, new Integer(0), new Integer(0))).intValue();
        }
    }

    private final int jjMoveNfa_3(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 3;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            int i2 = this.jjround + 1;
            this.jjround = i2;
            if (i2 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long l = 1 << this.curChar;
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                        case 2:
                            if ((-17179869185L & l) == 0) {
                                continue;
                            } else {
                                if (kind > 15) {
                                    kind = 15;
                                }
                                jjCheckNAdd(2);
                                continue;
                            }
                        case 1:
                            if (kind > 14) {
                                kind = 14;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else if (this.curChar < 128) {
                long l2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((-268435457 & l2) != 0) {
                                if (kind > 15) {
                                    kind = 15;
                                }
                                jjCheckNAdd(2);
                                continue;
                            } else if (this.curChar == '\\') {
                                int[] iArr = this.jjstateSet;
                                int i3 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i3 + 1;
                                iArr[i3] = 1;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (kind > 14) {
                                kind = 14;
                                continue;
                            } else {
                                continue;
                            }
                        case 2:
                            if ((-268435457 & l2) == 0) {
                                continue;
                            } else {
                                if (kind > 15) {
                                    kind = 15;
                                }
                                jjCheckNAdd(2);
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else {
                int i22 = (this.curChar & 255) >> 6;
                long l22 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                        case 2:
                            if ((jjbitVec0[i22] & l22) == 0) {
                                continue;
                            } else {
                                if (kind > 15) {
                                    kind = 15;
                                }
                                jjCheckNAdd(2);
                                continue;
                            }
                        case 1:
                            if ((jjbitVec0[i22] & l22) != 0 && kind > 14) {
                                kind = 14;
                                continue;
                            }
                    }
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            curPos++;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 3 - startsAt;
            if (i != startsAt) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return curPos;
        }
    }

    private final int jjStopStringLiteralDfa_2(int pos, long active0) {
        return -1;
    }

    private final int jjStartNfa_2(int pos, long active0) {
        Class[] clsArr = {Integer.TYPE, Long.TYPE};
        int intValue = ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjStopStringLiteralDfa_2", clsArr).invoke(this, new Integer(pos), new Long(active0))).intValue();
        Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
        return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjMoveNfa_2", clsArr2).invoke(this, new Integer(intValue), new Integer(pos + 1))).intValue();
    }

    private final int jjStartNfaWithStates_2(int pos, int kind, int state) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        try {
            this.curChar = ((Character) SimpleCharStream.class.getMethod("readChar", new Class[0]).invoke(this.input_stream, new Object[0])).charValue();
            Class[] clsArr = {Integer.TYPE, Integer.TYPE};
            return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjMoveNfa_2", clsArr).invoke(this, new Integer(state), new Integer(pos + 1))).intValue();
        } catch (IOException e) {
            return pos + 1;
        }
    }

    private final int jjMoveStringLiteralDfa0_2() {
        switch (this.curChar) {
            case '(':
                Class[] clsArr = {Integer.TYPE, Integer.TYPE};
                return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjStopAtPos", clsArr).invoke(this, new Integer(0), new Integer(10))).intValue();
            case ')':
                Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjStopAtPos", clsArr2).invoke(this, new Integer(0), new Integer(11))).intValue();
            default:
                Class[] clsArr3 = {Integer.TYPE, Integer.TYPE};
                return ((Integer) ContentLanguageParserTokenManager.class.getMethod("jjMoveNfa_2", clsArr3).invoke(this, new Integer(0), new Integer(0))).intValue();
        }
    }

    private final int jjMoveNfa_2(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 3;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            int i2 = this.jjround + 1;
            this.jjround = i2;
            if (i2 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long j = 1 << this.curChar;
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if (kind > 12) {
                                kind = 12;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (kind > 9) {
                                kind = 9;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else if (this.curChar < 128) {
                long j2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if (kind > 12) {
                                kind = 12;
                            }
                            if (this.curChar == '\\') {
                                int[] iArr = this.jjstateSet;
                                int i3 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i3 + 1;
                                iArr[i3] = 1;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if (kind > 9) {
                                kind = 9;
                                continue;
                            } else {
                                continue;
                            }
                        case 2:
                            if (kind > 12) {
                                kind = 12;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else {
                int i22 = (this.curChar & 255) >> 6;
                long l2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((jjbitVec0[i22] & l2) != 0 && kind > 12) {
                                kind = 12;
                                continue;
                            }
                        case 1:
                            if ((jjbitVec0[i22] & l2) != 0 && kind > 9) {
                                kind = 9;
                                continue;
                            }
                    }
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            curPos++;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 3 - startsAt;
            if (i != startsAt) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return curPos;
        }
    }

    public ContentLanguageParserTokenManager(SimpleCharStream stream) {
        this.debugStream = System.out;
        this.jjrounds = new int[4];
        this.jjstateSet = new int[8];
        this.curLexState = 0;
        this.defaultLexState = 0;
        this.input_stream = stream;
    }

    public ContentLanguageParserTokenManager(SimpleCharStream stream, int lexState) {
        this(stream);
        Class[] clsArr = {Integer.TYPE};
        ContentLanguageParserTokenManager.class.getMethod("SwitchTo", clsArr).invoke(this, new Integer(lexState));
    }

    public void ReInit(SimpleCharStream stream) {
        this.jjnewStateCnt = 0;
        this.jjmatchedPos = 0;
        this.curLexState = this.defaultLexState;
        this.input_stream = stream;
        ContentLanguageParserTokenManager.class.getMethod("ReInitRounds", new Class[0]).invoke(this, new Object[0]);
    }

    private final void ReInitRounds() {
        this.jjround = -2147483647;
        int i = 4;
        while (true) {
            int i2 = i;
            i = i2 - 1;
            if (i2 > 0) {
                this.jjrounds[i] = Integer.MIN_VALUE;
            } else {
                return;
            }
        }
    }

    public void ReInit(SimpleCharStream stream, int lexState) {
        Object[] objArr = {stream};
        ContentLanguageParserTokenManager.class.getMethod("ReInit", SimpleCharStream.class).invoke(this, objArr);
        Class[] clsArr = {Integer.TYPE};
        ContentLanguageParserTokenManager.class.getMethod("SwitchTo", clsArr).invoke(this, new Integer(lexState));
    }

    public void SwitchTo(int lexState) {
        if (lexState >= 4 || lexState < 0) {
            StringBuilder sb = new StringBuilder();
            Class[] clsArr = {String.class};
            Object[] objArr = {"Error: Ignoring invalid lexical state : "};
            Class[] clsArr2 = {Integer.TYPE};
            Object[] objArr2 = {new Integer(lexState)};
            Method method = StringBuilder.class.getMethod("append", clsArr2);
            Object[] objArr3 = {". State unchanged."};
            Method method2 = StringBuilder.class.getMethod("append", String.class);
            throw new TokenMgrError((String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), objArr3), new Object[0]), 2);
        }
        this.curLexState = lexState;
    }

    /* access modifiers changed from: protected */
    public Token jjFillToken() {
        int i = this.jjmatchedKind;
        Class[] clsArr = {Integer.TYPE};
        Token t = (Token) Token.class.getMethod("newToken", clsArr).invoke(null, new Integer(i));
        t.kind = this.jjmatchedKind;
        String im = jjstrLiteralImages[this.jjmatchedKind];
        if (im == null) {
            im = (String) SimpleCharStream.class.getMethod("GetImage", new Class[0]).invoke(this.input_stream, new Object[0]);
        }
        t.image = im;
        t.beginLine = ((Integer) SimpleCharStream.class.getMethod("getBeginLine", new Class[0]).invoke(this.input_stream, new Object[0])).intValue();
        t.beginColumn = ((Integer) SimpleCharStream.class.getMethod("getBeginColumn", new Class[0]).invoke(this.input_stream, new Object[0])).intValue();
        t.endLine = ((Integer) SimpleCharStream.class.getMethod("getEndLine", new Class[0]).invoke(this.input_stream, new Object[0])).intValue();
        t.endColumn = ((Integer) SimpleCharStream.class.getMethod("getEndColumn", new Class[0]).invoke(this.input_stream, new Object[0])).intValue();
        return t;
    }

    public Token getNextToken() {
        Token specialToken = null;
        int curPos = 0;
        while (true) {
            try {
                this.curChar = this.input_stream.BeginToken();
                this.image = null;
                this.jjimageLen = 0;
                while (true) {
                    switch (this.curLexState) {
                        case 0:
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            this.jjmatchedPos = 0;
                            curPos = jjMoveStringLiteralDfa0_0();
                            break;
                        case 1:
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            this.jjmatchedPos = 0;
                            curPos = jjMoveStringLiteralDfa0_1();
                            break;
                        case 2:
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            this.jjmatchedPos = 0;
                            curPos = jjMoveStringLiteralDfa0_2();
                            break;
                        case 3:
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            this.jjmatchedPos = 0;
                            curPos = jjMoveStringLiteralDfa0_3();
                            break;
                    }
                    if (this.jjmatchedKind != Integer.MAX_VALUE) {
                        if (this.jjmatchedPos + 1 < curPos) {
                            this.input_stream.backup((curPos - this.jjmatchedPos) - 1);
                        }
                        if ((jjtoToken[this.jjmatchedKind >> 6] & (1 << (this.jjmatchedKind & 63))) != 0) {
                            Token matchedToken = jjFillToken();
                            matchedToken.specialToken = specialToken;
                            TokenLexicalActions(matchedToken);
                            if (jjnewLexState[this.jjmatchedKind] != -1) {
                                this.curLexState = jjnewLexState[this.jjmatchedKind];
                            }
                            return matchedToken;
                        } else if ((jjtoSkip[this.jjmatchedKind >> 6] & (1 << (this.jjmatchedKind & 63))) != 0) {
                            if ((jjtoSpecial[this.jjmatchedKind >> 6] & (1 << (this.jjmatchedKind & 63))) != 0) {
                                Token matchedToken2 = jjFillToken();
                                if (specialToken == null) {
                                    specialToken = matchedToken2;
                                } else {
                                    matchedToken2.specialToken = specialToken;
                                    specialToken.next = matchedToken2;
                                    specialToken = matchedToken2;
                                }
                            }
                            if (jjnewLexState[this.jjmatchedKind] != -1) {
                                this.curLexState = jjnewLexState[this.jjmatchedKind];
                            }
                        } else {
                            MoreLexicalActions();
                            if (jjnewLexState[this.jjmatchedKind] != -1) {
                                this.curLexState = jjnewLexState[this.jjmatchedKind];
                            }
                            curPos = 0;
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            try {
                                this.curChar = this.input_stream.readChar();
                            } catch (IOException e) {
                            }
                        }
                    }
                }
            } catch (IOException e2) {
                this.jjmatchedKind = 0;
                Token matchedToken3 = jjFillToken();
                matchedToken3.specialToken = specialToken;
                return matchedToken3;
            }
        }
        int error_line = this.input_stream.getEndLine();
        int error_column = this.input_stream.getEndColumn();
        String error_after = null;
        boolean EOFSeen = false;
        try {
            this.input_stream.readChar();
            this.input_stream.backup(1);
        } catch (IOException e3) {
            EOFSeen = true;
            error_after = curPos <= 1 ? "" : this.input_stream.GetImage();
            if (this.curChar == 10 || this.curChar == 13) {
                error_line++;
                error_column = 0;
            } else {
                error_column++;
            }
        }
        if (!EOFSeen) {
            this.input_stream.backup(1);
            if (curPos <= 1) {
                error_after = "";
            } else {
                error_after = this.input_stream.GetImage();
            }
        }
        throw new TokenMgrError(EOFSeen, this.curLexState, error_line, error_column, error_after, this.curChar, 0);
    }

    /* access modifiers changed from: package-private */
    public void MoreLexicalActions() {
        int i = this.jjimageLen;
        int i2 = this.jjmatchedPos + 1;
        this.lengthOfMatch = i2;
        this.jjimageLen = i + i2;
        switch (this.jjmatchedKind) {
            case 6:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                StringBuffer stringBuffer = this.image;
                SimpleCharStream simpleCharStream = this.input_stream;
                int i3 = this.jjimageLen;
                StringBuffer.class.getMethod("append", char[].class).invoke(stringBuffer, (char[]) SimpleCharStream.class.getMethod("GetSuffix", Integer.TYPE).invoke(simpleCharStream, new Integer(i3)));
                this.jjimageLen = 0;
                StringBuffer stringBuffer2 = this.image;
                Method method = StringBuffer.class.getMethod("length", new Class[0]);
                StringBuffer.class.getMethod("deleteCharAt", Integer.TYPE).invoke(stringBuffer2, new Integer(((Integer) method.invoke(this.image, new Object[0])).intValue() - 2));
                return;
            case 7:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                StringBuffer stringBuffer3 = this.image;
                SimpleCharStream simpleCharStream2 = this.input_stream;
                int i4 = this.jjimageLen;
                StringBuffer.class.getMethod("append", char[].class).invoke(stringBuffer3, (char[]) SimpleCharStream.class.getMethod("GetSuffix", Integer.TYPE).invoke(simpleCharStream2, new Integer(i4)));
                this.jjimageLen = 0;
                this.commentNest = 1;
                return;
            case 8:
            case 12:
            default:
                return;
            case 9:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                StringBuffer stringBuffer4 = this.image;
                SimpleCharStream simpleCharStream3 = this.input_stream;
                int i5 = this.jjimageLen;
                StringBuffer.class.getMethod("append", char[].class).invoke(stringBuffer4, (char[]) SimpleCharStream.class.getMethod("GetSuffix", Integer.TYPE).invoke(simpleCharStream3, new Integer(i5)));
                this.jjimageLen = 0;
                StringBuffer stringBuffer5 = this.image;
                Method method2 = StringBuffer.class.getMethod("length", new Class[0]);
                StringBuffer.class.getMethod("deleteCharAt", Integer.TYPE).invoke(stringBuffer5, new Integer(((Integer) method2.invoke(this.image, new Object[0])).intValue() - 2));
                return;
            case 10:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                StringBuffer stringBuffer6 = this.image;
                SimpleCharStream simpleCharStream4 = this.input_stream;
                int i6 = this.jjimageLen;
                StringBuffer.class.getMethod("append", char[].class).invoke(stringBuffer6, (char[]) SimpleCharStream.class.getMethod("GetSuffix", Integer.TYPE).invoke(simpleCharStream4, new Integer(i6)));
                this.jjimageLen = 0;
                this.commentNest++;
                return;
            case 11:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                StringBuffer stringBuffer7 = this.image;
                SimpleCharStream simpleCharStream5 = this.input_stream;
                int i7 = this.jjimageLen;
                StringBuffer.class.getMethod("append", char[].class).invoke(stringBuffer7, (char[]) SimpleCharStream.class.getMethod("GetSuffix", Integer.TYPE).invoke(simpleCharStream5, new Integer(i7)));
                this.jjimageLen = 0;
                this.commentNest--;
                if (this.commentNest == 0) {
                    ContentLanguageParserTokenManager.class.getMethod("SwitchTo", Integer.TYPE).invoke(this, new Integer(1));
                    return;
                }
                return;
            case 13:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                StringBuffer stringBuffer8 = this.image;
                SimpleCharStream simpleCharStream6 = this.input_stream;
                int i8 = this.jjimageLen;
                StringBuffer.class.getMethod("append", char[].class).invoke(stringBuffer8, (char[]) SimpleCharStream.class.getMethod("GetSuffix", Integer.TYPE).invoke(simpleCharStream6, new Integer(i8)));
                this.jjimageLen = 0;
                StringBuffer stringBuffer9 = this.image;
                Method method3 = StringBuffer.class.getMethod("length", new Class[0]);
                StringBuffer.class.getMethod("deleteCharAt", Integer.TYPE).invoke(stringBuffer9, new Integer(((Integer) method3.invoke(this.image, new Object[0])).intValue() - 1));
                return;
            case 14:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                StringBuffer stringBuffer10 = this.image;
                SimpleCharStream simpleCharStream7 = this.input_stream;
                int i9 = this.jjimageLen;
                StringBuffer.class.getMethod("append", char[].class).invoke(stringBuffer10, (char[]) SimpleCharStream.class.getMethod("GetSuffix", Integer.TYPE).invoke(simpleCharStream7, new Integer(i9)));
                this.jjimageLen = 0;
                StringBuffer stringBuffer11 = this.image;
                Method method4 = StringBuffer.class.getMethod("length", new Class[0]);
                StringBuffer.class.getMethod("deleteCharAt", Integer.TYPE).invoke(stringBuffer11, new Integer(((Integer) method4.invoke(this.image, new Object[0])).intValue() - 2));
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void TokenLexicalActions(Token matchedToken) {
        switch (this.jjmatchedKind) {
            case 16:
                if (this.image == null) {
                    this.image = new StringBuffer();
                }
                StringBuffer stringBuffer = this.image;
                SimpleCharStream simpleCharStream = this.input_stream;
                int i = this.jjimageLen;
                int i2 = this.jjmatchedPos + 1;
                this.lengthOfMatch = i2;
                Class[] clsArr = {Integer.TYPE};
                Object[] objArr = {(char[]) SimpleCharStream.class.getMethod("GetSuffix", clsArr).invoke(simpleCharStream, new Integer(i + i2))};
                StringBuffer.class.getMethod("append", char[].class).invoke(stringBuffer, objArr);
                StringBuffer stringBuffer2 = this.image;
                Method method = StringBuffer.class.getMethod("length", new Class[0]);
                Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
                matchedToken.image = (String) StringBuffer.class.getMethod("substring", clsArr2).invoke(stringBuffer2, new Integer(0), new Integer(((Integer) method.invoke(this.image, new Object[0])).intValue() - 1));
                return;
            default:
                return;
        }
    }
}
