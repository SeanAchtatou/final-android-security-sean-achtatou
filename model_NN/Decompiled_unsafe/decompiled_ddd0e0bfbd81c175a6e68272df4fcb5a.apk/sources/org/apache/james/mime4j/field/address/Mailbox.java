package org.apache.james.mime4j.field.address;

import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import org.apache.james.mime4j.codec.EncoderUtil;
import org.apache.james.mime4j.field.address.parser.ASTmailbox;
import org.apache.james.mime4j.field.address.parser.AddressListParser;
import org.apache.james.mime4j.field.address.parser.ParseException;

public class Mailbox extends Address {
    private static final DomainList EMPTY_ROUTE_LIST = new DomainList((List) Collections.class.getMethod("emptyList", new Class[0]).invoke(null, new Object[0]), true);
    private static final long serialVersionUID = 1;
    private final String domain;
    private final String localPart;
    private final String name;
    private final DomainList route;

    public Mailbox(String localPart2, String domain2) {
        this(null, null, localPart2, domain2);
    }

    public Mailbox(DomainList route2, String localPart2, String domain2) {
        this(null, route2, localPart2, domain2);
    }

    public Mailbox(String name2, String localPart2, String domain2) {
        this(name2, null, localPart2, domain2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0064, code lost:
        if (((java.lang.Integer) java.lang.String.class.getMethod("length", new java.lang.Class[0]).invoke(r10, new java.lang.Object[0])).intValue() == 0) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x003e, code lost:
        if (((java.lang.Integer) java.lang.String.class.getMethod("length", new java.lang.Class[0]).invoke(r7, new java.lang.Object[0])).intValue() == 0) goto L_0x0040;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Mailbox(java.lang.String r7, org.apache.james.mime4j.field.address.DomainList r8, java.lang.String r9, java.lang.String r10) {
        /*
            r6 = this;
            r0 = 0
            r6.<init>()
            if (r9 == 0) goto L_0x001f
            r2 = 0
            java.lang.Class[] r3 = new java.lang.Class[r2]
            java.lang.Object[] r4 = new java.lang.Object[r2]
            java.lang.String r2 = "length"
            java.lang.Class<java.lang.String> r5 = java.lang.String.class
            java.lang.reflect.Method r2 = r5.getMethod(r2, r3)
            java.lang.Object r2 = r2.invoke(r9, r4)
            java.lang.Integer r2 = (java.lang.Integer) r2
            int r1 = r2.intValue()
            if (r1 != 0) goto L_0x0025
        L_0x001f:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            r0.<init>()
            throw r0
        L_0x0025:
            if (r7 == 0) goto L_0x0040
            r2 = 0
            java.lang.Class[] r3 = new java.lang.Class[r2]
            java.lang.Object[] r4 = new java.lang.Object[r2]
            java.lang.String r2 = "length"
            java.lang.Class<java.lang.String> r5 = java.lang.String.class
            java.lang.reflect.Method r2 = r5.getMethod(r2, r3)
            java.lang.Object r2 = r2.invoke(r7, r4)
            java.lang.Integer r2 = (java.lang.Integer) r2
            int r1 = r2.intValue()
            if (r1 != 0) goto L_0x0041
        L_0x0040:
            r7 = r0
        L_0x0041:
            r6.name = r7
            if (r8 != 0) goto L_0x0047
            org.apache.james.mime4j.field.address.DomainList r8 = org.apache.james.mime4j.field.address.Mailbox.EMPTY_ROUTE_LIST
        L_0x0047:
            r6.route = r8
            r6.localPart = r9
            if (r10 == 0) goto L_0x0066
            r2 = 0
            java.lang.Class[] r3 = new java.lang.Class[r2]
            java.lang.Object[] r4 = new java.lang.Object[r2]
            java.lang.String r2 = "length"
            java.lang.Class<java.lang.String> r5 = java.lang.String.class
            java.lang.reflect.Method r2 = r5.getMethod(r2, r3)
            java.lang.Object r2 = r2.invoke(r10, r4)
            java.lang.Integer r2 = (java.lang.Integer) r2
            int r1 = r2.intValue()
            if (r1 != 0) goto L_0x0067
        L_0x0066:
            r10 = r0
        L_0x0067:
            r6.domain = r10
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.address.Mailbox.<init>(java.lang.String, org.apache.james.mime4j.field.address.DomainList, java.lang.String, java.lang.String):void");
    }

    Mailbox(String name2, Mailbox baseMailbox) {
        this(name2, (DomainList) Mailbox.class.getMethod("getRoute", new Class[0]).invoke(baseMailbox, new Object[0]), (String) Mailbox.class.getMethod("getLocalPart", new Class[0]).invoke(baseMailbox, new Object[0]), (String) Mailbox.class.getMethod("getDomain", new Class[0]).invoke(baseMailbox, new Object[0]));
    }

    public static Mailbox parse(String rawMailboxString) {
        AddressListParser parser = new AddressListParser(new StringReader(rawMailboxString));
        try {
            Method method = Builder.class.getMethod("getInstance", new Class[0]);
            Object[] objArr = {(ASTmailbox) AddressListParser.class.getMethod("parseMailbox", new Class[0]).invoke(parser, new Object[0])};
            return (Mailbox) Builder.class.getMethod("buildMailbox", ASTmailbox.class).invoke((Builder) method.invoke(null, new Object[0]), objArr);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public String getName() {
        return this.name;
    }

    public DomainList getRoute() {
        return this.route;
    }

    public String getLocalPart() {
        return this.localPart;
    }

    public String getDomain() {
        return this.domain;
    }

    public String getAddress() {
        if (this.domain == null) {
            return this.localPart;
        }
        StringBuilder sb = new StringBuilder();
        Class[] clsArr = {String.class};
        Object[] objArr = {this.localPart};
        Class[] clsArr2 = {Character.TYPE};
        Object[] objArr2 = {new Character('@')};
        Method method = StringBuilder.class.getMethod("append", clsArr2);
        Object[] objArr3 = {this.domain};
        Method method2 = StringBuilder.class.getMethod("append", String.class);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), objArr3), new Object[0]);
    }

    public String getDisplayString(boolean includeRoute) {
        boolean z;
        boolean includeAngleBrackets;
        if (this.route != null) {
            z = true;
        } else {
            z = false;
        }
        boolean includeRoute2 = includeRoute & z;
        if (this.name != null || includeRoute2) {
            includeAngleBrackets = true;
        } else {
            includeAngleBrackets = false;
        }
        StringBuilder sb = new StringBuilder();
        if (this.name != null) {
            StringBuilder.class.getMethod("append", String.class).invoke(sb, this.name);
            StringBuilder.class.getMethod("append", Character.TYPE).invoke(sb, new Character(' '));
        }
        if (includeAngleBrackets) {
            StringBuilder.class.getMethod("append", Character.TYPE).invoke(sb, new Character('<'));
        }
        if (includeRoute2) {
            StringBuilder.class.getMethod("append", String.class).invoke(sb, (String) DomainList.class.getMethod("toRouteString", new Class[0]).invoke(this.route, new Object[0]));
            StringBuilder.class.getMethod("append", Character.TYPE).invoke(sb, new Character(':'));
        }
        StringBuilder.class.getMethod("append", String.class).invoke(sb, this.localPart);
        if (this.domain != null) {
            StringBuilder.class.getMethod("append", Character.TYPE).invoke(sb, new Character('@'));
            StringBuilder.class.getMethod("append", String.class).invoke(sb, this.domain);
        }
        if (includeAngleBrackets) {
            StringBuilder.class.getMethod("append", Character.TYPE).invoke(sb, new Character('>'));
        }
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb, new Object[0]);
    }

    public String getEncodedString() {
        StringBuilder sb = new StringBuilder();
        if (this.name != null) {
            Object[] objArr = {this.name};
            Object[] objArr2 = {(String) EncoderUtil.class.getMethod("encodeAddressDisplayName", String.class).invoke(null, objArr)};
            StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr2);
            Object[] objArr3 = {" <"};
            StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr3);
        }
        Object[] objArr4 = {this.localPart};
        Object[] objArr5 = {(String) EncoderUtil.class.getMethod("encodeAddressLocalPart", String.class).invoke(null, objArr4)};
        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr5);
        if (this.domain != null) {
            Class[] clsArr = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr).invoke(sb, new Character('@'));
            Object[] objArr6 = {this.domain};
            StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr6);
        }
        if (this.name != null) {
            Class[] clsArr2 = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr2).invoke(sb, new Character('>'));
        }
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb, new Object[0]);
    }

    public int hashCode() {
        Method method = Mailbox.class.getMethod("getCanonicalizedAddress", new Class[0]);
        return ((Integer) Object.class.getMethod("hashCode", new Class[0]).invoke(method.invoke(this, new Object[0]), new Object[0])).intValue();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Mailbox)) {
            return false;
        }
        Method method = Mailbox.class.getMethod("getCanonicalizedAddress", new Class[0]);
        Object[] objArr = {Mailbox.class.getMethod("getCanonicalizedAddress", new Class[0]).invoke((Mailbox) obj, new Object[0])};
        return ((Boolean) Object.class.getMethod("equals", Object.class).invoke(method.invoke(this, new Object[0]), objArr)).booleanValue();
    }

    /* access modifiers changed from: protected */
    public final void doAddMailboxesTo(List<Mailbox> results) {
        Object[] objArr = {this};
        ((Boolean) List.class.getMethod("add", Object.class).invoke(results, objArr)).booleanValue();
    }

    private Object getCanonicalizedAddress() {
        if (this.domain == null) {
            return this.localPart;
        }
        StringBuilder sb = new StringBuilder();
        Class[] clsArr = {String.class};
        Object[] objArr = {this.localPart};
        Class[] clsArr2 = {Character.TYPE};
        Object[] objArr2 = {new Character('@')};
        Method method = StringBuilder.class.getMethod("append", clsArr2);
        String str = this.domain;
        Object[] objArr3 = {Locale.US};
        Object[] objArr4 = {(String) String.class.getMethod("toLowerCase", Locale.class).invoke(str, objArr3)};
        Method method2 = StringBuilder.class.getMethod("append", String.class);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), objArr4), new Object[0]);
    }
}
