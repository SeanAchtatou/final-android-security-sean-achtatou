package org.apache.james.mime4j.field.address.parser;

import java.io.PrintStream;
import java.lang.reflect.Method;

public class SimpleNode extends BaseNode implements Node {
    protected Node[] children;
    protected int id;
    protected Node parent;
    protected AddressListParser parser;

    public SimpleNode(int i) {
        this.id = i;
    }

    public SimpleNode(AddressListParser p, int i) {
        this(i);
        this.parser = p;
    }

    public void jjtOpen() {
    }

    public void jjtClose() {
    }

    public void jjtSetParent(Node n) {
        this.parent = n;
    }

    public Node jjtGetParent() {
        return this.parent;
    }

    public void jjtAddChild(Node n, int i) {
        if (this.children == null) {
            this.children = new Node[(i + 1)];
        } else if (i >= this.children.length) {
            Node[] c = new Node[(i + 1)];
            Node[] nodeArr = this.children;
            int length = this.children.length;
            Class[] clsArr = {Object.class, Integer.TYPE, Object.class, Integer.TYPE, Integer.TYPE};
            System.class.getMethod("arraycopy", clsArr).invoke(null, nodeArr, new Integer(0), c, new Integer(0), new Integer(length));
            this.children = c;
        }
        this.children[i] = n;
    }

    public Node jjtGetChild(int i) {
        return this.children[i];
    }

    public int jjtGetNumChildren() {
        if (this.children == null) {
            return 0;
        }
        return this.children.length;
    }

    public Object jjtAccept(AddressListParserVisitor visitor, Object data) {
        return AddressListParserVisitor.class.getMethod("visit", SimpleNode.class, Object.class).invoke(visitor, this, data);
    }

    public Object childrenAccept(AddressListParserVisitor visitor, Object data) {
        if (this.children != null) {
            for (Node invoke : this.children) {
                Node.class.getMethod("jjtAccept", AddressListParserVisitor.class, Object.class).invoke(invoke, visitor, data);
            }
        }
        return data;
    }

    public String toString() {
        return AddressListParserTreeConstants.jjtNodeName[this.id];
    }

    public String toString(String prefix) {
        StringBuilder sb = new StringBuilder();
        Class[] clsArr = {String.class};
        Object[] objArr = {prefix};
        Object[] objArr2 = {(String) SimpleNode.class.getMethod("toString", new Class[0]).invoke(this, new Object[0])};
        Method method = StringBuilder.class.getMethod("append", String.class);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), new Object[0]);
    }

    public void dump(String prefix) {
        PrintStream.class.getMethod("println", String.class).invoke(System.out, (String) SimpleNode.class.getMethod("toString", String.class).invoke(this, prefix));
        if (this.children != null) {
            for (Node node : this.children) {
                SimpleNode n = (SimpleNode) node;
                if (n != null) {
                    Object[] objArr = {" "};
                    Method method = StringBuilder.class.getMethod("append", String.class);
                    Method method2 = StringBuilder.class.getMethod("toString", new Class[0]);
                    SimpleNode.class.getMethod("dump", String.class).invoke(n, (String) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), prefix), objArr), new Object[0]));
                }
            }
        }
    }
}
