package org.apache.james.mime4j.field.datetime;

import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DateTime {
    private final Date date;
    private final int day;
    private final int hour;
    private final int minute;
    private final int month;
    private final int second;
    private final int timeZone;
    private final int year;

    public DateTime(String yearString, int month2, int day2, int hour2, int minute2, int second2, int timeZone2) {
        this.year = convertToYear(yearString);
        this.date = convertToDate(this.year, month2, day2, hour2, minute2, second2, timeZone2);
        this.month = month2;
        this.day = day2;
        this.hour = hour2;
        this.minute = minute2;
        this.second = second2;
        this.timeZone = timeZone2;
    }

    private int convertToYear(String yearString) {
        Object[] objArr = {yearString};
        int year2 = ((Integer) Integer.class.getMethod("parseInt", String.class).invoke(null, objArr)).intValue();
        switch (((Integer) String.class.getMethod("length", new Class[0]).invoke(yearString, new Object[0])).intValue()) {
            case 1:
            case 2:
                if (year2 < 0 || year2 >= 50) {
                    return year2 + 1900;
                }
                return year2 + 2000;
            case 3:
                return year2 + 1900;
            default:
                return year2;
        }
    }

    public static Date convertToDate(int year2, int month2, int day2, int hour2, int minute2, int second2, int timeZone2) {
        Calendar c = new GregorianCalendar(TimeZone.getTimeZone("GMT+0"));
        c.set(year2, month2 - 1, day2, hour2, minute2, second2);
        c.set(14, 0);
        if (timeZone2 != Integer.MIN_VALUE) {
            c.add(12, (((timeZone2 / 100) * 60) + (timeZone2 % 100)) * -1);
        }
        return c.getTime();
    }

    public Date getDate() {
        return this.date;
    }

    public int getYear() {
        return this.year;
    }

    public int getMonth() {
        return this.month;
    }

    public int getDay() {
        return this.day;
    }

    public int getHour() {
        return this.hour;
    }

    public int getMinute() {
        return this.minute;
    }

    public int getSecond() {
        return this.second;
    }

    public int getTimeZone() {
        return this.timeZone;
    }

    public void print() {
        PrintStream printStream = System.out;
        Object[] objArr = {(String) DateTime.class.getMethod("toString", new Class[0]).invoke(this, new Object[0])};
        PrintStream.class.getMethod("println", String.class).invoke(printStream, objArr);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int intValue = ((Integer) DateTime.class.getMethod("getYear", new Class[0]).invoke(this, new Object[0])).intValue();
        Class[] clsArr = {Integer.TYPE};
        Object[] objArr = {new Integer(intValue)};
        Object[] objArr2 = {" "};
        Method method = StringBuilder.class.getMethod("append", String.class);
        int intValue2 = ((Integer) DateTime.class.getMethod("getMonth", new Class[0]).invoke(this, new Object[0])).intValue();
        Class[] clsArr2 = {Integer.TYPE};
        Object[] objArr3 = {new Integer(intValue2)};
        Method method2 = StringBuilder.class.getMethod("append", clsArr2);
        Object[] objArr4 = {" "};
        Method method3 = StringBuilder.class.getMethod("append", String.class);
        int intValue3 = ((Integer) DateTime.class.getMethod("getDay", new Class[0]).invoke(this, new Object[0])).intValue();
        Class[] clsArr3 = {Integer.TYPE};
        Object[] objArr5 = {new Integer(intValue3)};
        Method method4 = StringBuilder.class.getMethod("append", clsArr3);
        Object[] objArr6 = {"; "};
        Method method5 = StringBuilder.class.getMethod("append", String.class);
        int intValue4 = ((Integer) DateTime.class.getMethod("getHour", new Class[0]).invoke(this, new Object[0])).intValue();
        Class[] clsArr4 = {Integer.TYPE};
        Object[] objArr7 = {new Integer(intValue4)};
        Method method6 = StringBuilder.class.getMethod("append", clsArr4);
        Object[] objArr8 = {" "};
        Method method7 = StringBuilder.class.getMethod("append", String.class);
        int intValue5 = ((Integer) DateTime.class.getMethod("getMinute", new Class[0]).invoke(this, new Object[0])).intValue();
        Class[] clsArr5 = {Integer.TYPE};
        Object[] objArr9 = {new Integer(intValue5)};
        Method method8 = StringBuilder.class.getMethod("append", clsArr5);
        Object[] objArr10 = {" "};
        Method method9 = StringBuilder.class.getMethod("append", String.class);
        int intValue6 = ((Integer) DateTime.class.getMethod("getSecond", new Class[0]).invoke(this, new Object[0])).intValue();
        Class[] clsArr6 = {Integer.TYPE};
        Object[] objArr11 = {new Integer(intValue6)};
        Method method10 = StringBuilder.class.getMethod("append", clsArr6);
        Object[] objArr12 = {" "};
        Method method11 = StringBuilder.class.getMethod("append", String.class);
        int intValue7 = ((Integer) DateTime.class.getMethod("getTimeZone", new Class[0]).invoke(this, new Object[0])).intValue();
        Class[] clsArr7 = {Integer.TYPE};
        Object[] objArr13 = {new Integer(intValue7)};
        Method method12 = StringBuilder.class.getMethod("append", clsArr7);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method12.invoke((StringBuilder) method11.invoke((StringBuilder) method10.invoke((StringBuilder) method9.invoke((StringBuilder) method8.invoke((StringBuilder) method7.invoke((StringBuilder) method6.invoke((StringBuilder) method5.invoke((StringBuilder) method4.invoke((StringBuilder) method3.invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), objArr3), objArr4), objArr5), objArr6), objArr7), objArr8), objArr9), objArr10), objArr11), objArr12), objArr13), new Object[0]);
    }

    public int hashCode() {
        int intValue;
        if (this.date == null) {
            intValue = 0;
        } else {
            intValue = ((Integer) Date.class.getMethod("hashCode", new Class[0]).invoke(this.date, new Object[0])).intValue();
        }
        return ((((((((((((((intValue + 31) * 31) + this.day) * 31) + this.hour) * 31) + this.minute) * 31) + this.month) * 31) + this.second) * 31) + this.timeZone) * 31) + this.year;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (((Class) Object.class.getMethod("getClass", new Class[0]).invoke(this, new Object[0])) != ((Class) Object.class.getMethod("getClass", new Class[0]).invoke(obj, new Object[0]))) {
            return false;
        }
        DateTime other = (DateTime) obj;
        if (this.date != null) {
            Class[] clsArr = {Object.class};
            if (!((Boolean) Date.class.getMethod("equals", clsArr).invoke(this.date, other.date)).booleanValue()) {
                return false;
            }
        } else if (other.date != null) {
            return false;
        }
        if (this.day != other.day) {
            return false;
        }
        if (this.hour != other.hour) {
            return false;
        }
        if (this.minute != other.minute) {
            return false;
        }
        if (this.month != other.month) {
            return false;
        }
        if (this.second != other.second) {
            return false;
        }
        if (this.timeZone != other.timeZone) {
            return false;
        }
        if (this.year != other.year) {
            return false;
        }
        return true;
    }
}
