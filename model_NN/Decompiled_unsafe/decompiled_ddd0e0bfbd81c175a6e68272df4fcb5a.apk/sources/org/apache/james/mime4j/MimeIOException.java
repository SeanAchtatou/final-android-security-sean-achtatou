package org.apache.james.mime4j;

import java.io.IOException;

public class MimeIOException extends IOException {
    private static final long serialVersionUID = 5393613459533735409L;

    public MimeIOException(String message) {
        this(new MimeException(message));
    }

    public MimeIOException(MimeException cause) {
        super((String) MimeException.class.getMethod("getMessage", new Class[0]).invoke(cause, new Object[0]));
        Object[] objArr = {cause};
        MimeIOException.class.getMethod("initCause", Throwable.class).invoke(this, objArr);
    }
}
