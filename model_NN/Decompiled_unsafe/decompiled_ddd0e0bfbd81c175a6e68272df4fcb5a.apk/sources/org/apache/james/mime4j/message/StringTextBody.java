package org.apache.james.mime4j.message;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.nio.charset.Charset;
import org.apache.james.mime4j.util.CharsetUtil;

class StringTextBody extends TextBody {
    private final Charset charset;
    private final String text;

    public StringTextBody(String text2, Charset charset2) {
        this.text = text2;
        this.charset = charset2;
    }

    public String getMimeCharset() {
        Class[] clsArr = {String.class};
        return (String) CharsetUtil.class.getMethod("toMimeCharset", clsArr).invoke(null, (String) Charset.class.getMethod("name", new Class[0]).invoke(this.charset, new Object[0]));
    }

    public Reader getReader() throws IOException {
        return new StringReader(this.text);
    }

    public void writeTo(OutputStream out) throws IOException {
        if (out == null) {
            throw new IllegalArgumentException();
        }
        Reader reader = new StringReader(this.text);
        Writer writer = new OutputStreamWriter(out, this.charset);
        char[] buffer = new char[1024];
        while (true) {
            Object[] objArr = {buffer};
            int nChars = ((Integer) Reader.class.getMethod("read", char[].class).invoke(reader, objArr)).intValue();
            if (nChars == -1) {
                Reader.class.getMethod("close", new Class[0]).invoke(reader, new Object[0]);
                Writer.class.getMethod("flush", new Class[0]).invoke(writer, new Object[0]);
                return;
            }
            Class[] clsArr = {char[].class, Integer.TYPE, Integer.TYPE};
            Writer.class.getMethod("write", clsArr).invoke(writer, buffer, new Integer(0), new Integer(nChars));
        }
    }

    public StringTextBody copy() {
        return new StringTextBody(this.text, this.charset);
    }
}
