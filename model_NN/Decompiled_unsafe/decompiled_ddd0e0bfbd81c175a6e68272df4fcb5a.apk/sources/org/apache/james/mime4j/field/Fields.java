package org.apache.james.mime4j.field;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.james.mime4j.codec.EncoderUtil;
import org.apache.james.mime4j.field.address.Address;
import org.apache.james.mime4j.field.address.Mailbox;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.ContentUtil;
import org.apache.james.mime4j.util.MimeUtil;

public class Fields {
    private static final Pattern FIELD_NAME_PATTERN;

    static {
        Object[] objArr = {"[\\x21-\\x39\\x3b-\\x7e]+"};
        FIELD_NAME_PATTERN = (Pattern) Pattern.class.getMethod("compile", String.class).invoke(null, objArr);
    }

    private Fields() {
    }

    public static ContentTypeField contentType(String contentType) {
        Class[] clsArr = {FieldParser.class, String.class, String.class};
        return (ContentTypeField) ((Field) Fields.class.getMethod("parse", clsArr).invoke(null, ContentTypeField.PARSER, "Content-Type", contentType));
    }

    public static ContentTypeField contentType(String mimeType, Map<String, String> parameters) {
        if (!((Boolean) Fields.class.getMethod("isValidMimeType", String.class).invoke(null, mimeType)).booleanValue()) {
            throw new IllegalArgumentException();
        }
        if (parameters != null) {
            if (!((Boolean) Map.class.getMethod("isEmpty", new Class[0]).invoke(parameters, new Object[0])).booleanValue()) {
                StringBuilder sb = new StringBuilder(mimeType);
                Method method = Map.class.getMethod("entrySet", new Class[0]);
                Iterator i$ = (Iterator) Set.class.getMethod("iterator", new Class[0]).invoke((Set) method.invoke(parameters, new Object[0]), new Object[0]);
                while (true) {
                    if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                        Map.Entry<String, String> entry = (Map.Entry) Iterator.class.getMethod("next", new Class[0]).invoke(i$, new Object[0]);
                        Object[] objArr = {"; "};
                        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr);
                        Object[] objArr2 = {(String) Map.Entry.class.getMethod("getKey", new Class[0]).invoke(entry, new Object[0]), (String) Map.Entry.class.getMethod("getValue", new Class[0]).invoke(entry, new Object[0])};
                        Object[] objArr3 = {(String) EncoderUtil.class.getMethod("encodeHeaderParameter", String.class, String.class).invoke(null, objArr2)};
                        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr3);
                    } else {
                        Class[] clsArr = {String.class};
                        return (ContentTypeField) Fields.class.getMethod("contentType", clsArr).invoke(null, (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb, new Object[0]));
                    }
                }
            }
        }
        Class[] clsArr2 = {FieldParser.class, String.class, String.class};
        return (ContentTypeField) ((Field) Fields.class.getMethod("parse", clsArr2).invoke(null, ContentTypeField.PARSER, "Content-Type", mimeType));
    }

    public static ContentTransferEncodingField contentTransferEncoding(String contentTransferEncoding) {
        Class[] clsArr = {FieldParser.class, String.class, String.class};
        return (ContentTransferEncodingField) ((Field) Fields.class.getMethod("parse", clsArr).invoke(null, ContentTransferEncodingField.PARSER, "Content-Transfer-Encoding", contentTransferEncoding));
    }

    public static ContentDispositionField contentDisposition(String contentDisposition) {
        Class[] clsArr = {FieldParser.class, String.class, String.class};
        return (ContentDispositionField) ((Field) Fields.class.getMethod("parse", clsArr).invoke(null, ContentDispositionField.PARSER, "Content-Disposition", contentDisposition));
    }

    public static ContentDispositionField contentDisposition(String dispositionType, Map<String, String> parameters) {
        if (!((Boolean) Fields.class.getMethod("isValidDispositionType", String.class).invoke(null, dispositionType)).booleanValue()) {
            throw new IllegalArgumentException();
        }
        if (parameters != null) {
            if (!((Boolean) Map.class.getMethod("isEmpty", new Class[0]).invoke(parameters, new Object[0])).booleanValue()) {
                StringBuilder sb = new StringBuilder(dispositionType);
                Method method = Map.class.getMethod("entrySet", new Class[0]);
                Iterator i$ = (Iterator) Set.class.getMethod("iterator", new Class[0]).invoke((Set) method.invoke(parameters, new Object[0]), new Object[0]);
                while (true) {
                    if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                        Map.Entry<String, String> entry = (Map.Entry) Iterator.class.getMethod("next", new Class[0]).invoke(i$, new Object[0]);
                        Object[] objArr = {"; "};
                        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr);
                        Object[] objArr2 = {(String) Map.Entry.class.getMethod("getKey", new Class[0]).invoke(entry, new Object[0]), (String) Map.Entry.class.getMethod("getValue", new Class[0]).invoke(entry, new Object[0])};
                        Object[] objArr3 = {(String) EncoderUtil.class.getMethod("encodeHeaderParameter", String.class, String.class).invoke(null, objArr2)};
                        StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr3);
                    } else {
                        Class[] clsArr = {String.class};
                        return (ContentDispositionField) Fields.class.getMethod("contentDisposition", clsArr).invoke(null, (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb, new Object[0]));
                    }
                }
            }
        }
        Class[] clsArr2 = {FieldParser.class, String.class, String.class};
        return (ContentDispositionField) ((Field) Fields.class.getMethod("parse", clsArr2).invoke(null, ContentDispositionField.PARSER, "Content-Disposition", dispositionType));
    }

    public static ContentDispositionField contentDisposition(String dispositionType, String filename) {
        Class[] clsArr = {String.class, String.class, Long.TYPE, Date.class, Date.class, Date.class};
        return (ContentDispositionField) Fields.class.getMethod("contentDisposition", clsArr).invoke(null, dispositionType, filename, new Long(-1), null, null, null);
    }

    public static ContentDispositionField contentDisposition(String dispositionType, String filename, long size) {
        Class[] clsArr = {String.class, String.class, Long.TYPE, Date.class, Date.class, Date.class};
        return (ContentDispositionField) Fields.class.getMethod("contentDisposition", clsArr).invoke(null, dispositionType, filename, new Long(size), null, null, null);
    }

    public static ContentDispositionField contentDisposition(String dispositionType, String filename, long size, Date creationDate, Date modificationDate, Date readDate) {
        Map<String, String> parameters = new HashMap<>();
        if (filename != null) {
            Object[] objArr = {"filename", filename};
            Map.class.getMethod("put", Object.class, Object.class).invoke(parameters, objArr);
        }
        if (size >= 0) {
            Class[] clsArr = {Long.TYPE};
            Object[] objArr2 = {"size", (String) Long.class.getMethod("toString", clsArr).invoke(null, new Long(size))};
            Map.class.getMethod("put", Object.class, Object.class).invoke(parameters, objArr2);
        }
        if (creationDate != null) {
            Object[] objArr3 = {creationDate, null};
            Object[] objArr4 = {"creation-date", (String) MimeUtil.class.getMethod("formatDate", Date.class, TimeZone.class).invoke(null, objArr3)};
            Map.class.getMethod("put", Object.class, Object.class).invoke(parameters, objArr4);
        }
        if (modificationDate != null) {
            Object[] objArr5 = {modificationDate, null};
            Object[] objArr6 = {"modification-date", (String) MimeUtil.class.getMethod("formatDate", Date.class, TimeZone.class).invoke(null, objArr5)};
            Map.class.getMethod("put", Object.class, Object.class).invoke(parameters, objArr6);
        }
        if (readDate != null) {
            Object[] objArr7 = {readDate, null};
            Object[] objArr8 = {"read-date", (String) MimeUtil.class.getMethod("formatDate", Date.class, TimeZone.class).invoke(null, objArr7)};
            Map.class.getMethod("put", Object.class, Object.class).invoke(parameters, objArr8);
        }
        return (ContentDispositionField) Fields.class.getMethod("contentDisposition", String.class, Map.class).invoke(null, dispositionType, parameters);
    }

    public static DateTimeField date(Date date) {
        Class[] clsArr = {String.class, Date.class, TimeZone.class};
        return (DateTimeField) Fields.class.getMethod("date0", clsArr).invoke(null, FieldName.DATE, date, null);
    }

    public static DateTimeField date(String fieldName, Date date) {
        Object[] objArr = {fieldName};
        Fields.class.getMethod("checkValidFieldName", String.class).invoke(null, objArr);
        return (DateTimeField) Fields.class.getMethod("date0", String.class, Date.class, TimeZone.class).invoke(null, fieldName, date, null);
    }

    public static DateTimeField date(String fieldName, Date date, TimeZone zone) {
        Object[] objArr = {fieldName};
        Fields.class.getMethod("checkValidFieldName", String.class).invoke(null, objArr);
        return (DateTimeField) Fields.class.getMethod("date0", String.class, Date.class, TimeZone.class).invoke(null, fieldName, date, zone);
    }

    public static Field messageId(String hostname) {
        Object[] objArr = {hostname};
        FieldParser fieldParser = UnstructuredField.PARSER;
        Class[] clsArr = {FieldParser.class, String.class, String.class};
        return (Field) Fields.class.getMethod("parse", clsArr).invoke(null, fieldParser, FieldName.MESSAGE_ID, (String) MimeUtil.class.getMethod("createUniqueMessageId", String.class).invoke(null, objArr));
    }

    public static UnstructuredField subject(String subject) {
        int usedCharacters = ((Integer) String.class.getMethod("length", new Class[0]).invoke(FieldName.SUBJECT, new Object[0])).intValue() + 2;
        EncoderUtil.Usage usage = EncoderUtil.Usage.TEXT_TOKEN;
        Class[] clsArr = {String.class, EncoderUtil.Usage.class, Integer.TYPE};
        Object[] objArr = {subject, usage, new Integer(usedCharacters)};
        Class[] clsArr2 = {FieldParser.class, String.class, String.class};
        return (UnstructuredField) ((Field) Fields.class.getMethod("parse", clsArr2).invoke(null, UnstructuredField.PARSER, FieldName.SUBJECT, (String) EncoderUtil.class.getMethod("encodeIfNecessary", clsArr).invoke(null, objArr)));
    }

    public static MailboxField sender(Mailbox mailbox) {
        Class[] clsArr = {String.class, Mailbox.class};
        return (MailboxField) Fields.class.getMethod("mailbox0", clsArr).invoke(null, FieldName.SENDER, mailbox);
    }

    public static MailboxListField from(Mailbox mailbox) {
        Object[] objArr = {mailbox};
        Class[] clsArr = {String.class, Iterable.class};
        return (MailboxListField) Fields.class.getMethod("mailboxList0", clsArr).invoke(null, FieldName.FROM, (Set) Collections.class.getMethod("singleton", Object.class).invoke(null, objArr));
    }

    public static MailboxListField from(Mailbox... mailboxes) {
        Object[] objArr = {mailboxes};
        Class[] clsArr = {String.class, Iterable.class};
        return (MailboxListField) Fields.class.getMethod("mailboxList0", clsArr).invoke(null, FieldName.FROM, (List) Arrays.class.getMethod("asList", Object[].class).invoke(null, objArr));
    }

    public static MailboxListField from(Iterable<Mailbox> mailboxes) {
        Class[] clsArr = {String.class, Iterable.class};
        return (MailboxListField) Fields.class.getMethod("mailboxList0", clsArr).invoke(null, FieldName.FROM, mailboxes);
    }

    public static AddressListField to(Address address) {
        Object[] objArr = {address};
        Class[] clsArr = {String.class, Iterable.class};
        return (AddressListField) Fields.class.getMethod("addressList0", clsArr).invoke(null, FieldName.TO, (Set) Collections.class.getMethod("singleton", Object.class).invoke(null, objArr));
    }

    public static AddressListField to(Address... addresses) {
        Object[] objArr = {addresses};
        Class[] clsArr = {String.class, Iterable.class};
        return (AddressListField) Fields.class.getMethod("addressList0", clsArr).invoke(null, FieldName.TO, (List) Arrays.class.getMethod("asList", Object[].class).invoke(null, objArr));
    }

    public static AddressListField to(Iterable<Address> addresses) {
        Class[] clsArr = {String.class, Iterable.class};
        return (AddressListField) Fields.class.getMethod("addressList0", clsArr).invoke(null, FieldName.TO, addresses);
    }

    public static AddressListField cc(Address address) {
        Object[] objArr = {address};
        Class[] clsArr = {String.class, Iterable.class};
        return (AddressListField) Fields.class.getMethod("addressList0", clsArr).invoke(null, FieldName.CC, (Set) Collections.class.getMethod("singleton", Object.class).invoke(null, objArr));
    }

    public static AddressListField cc(Address... addresses) {
        Object[] objArr = {addresses};
        Class[] clsArr = {String.class, Iterable.class};
        return (AddressListField) Fields.class.getMethod("addressList0", clsArr).invoke(null, FieldName.CC, (List) Arrays.class.getMethod("asList", Object[].class).invoke(null, objArr));
    }

    public static AddressListField cc(Iterable<Address> addresses) {
        Class[] clsArr = {String.class, Iterable.class};
        return (AddressListField) Fields.class.getMethod("addressList0", clsArr).invoke(null, FieldName.CC, addresses);
    }

    public static AddressListField bcc(Address address) {
        Object[] objArr = {address};
        Class[] clsArr = {String.class, Iterable.class};
        return (AddressListField) Fields.class.getMethod("addressList0", clsArr).invoke(null, FieldName.BCC, (Set) Collections.class.getMethod("singleton", Object.class).invoke(null, objArr));
    }

    public static AddressListField bcc(Address... addresses) {
        Object[] objArr = {addresses};
        Class[] clsArr = {String.class, Iterable.class};
        return (AddressListField) Fields.class.getMethod("addressList0", clsArr).invoke(null, FieldName.BCC, (List) Arrays.class.getMethod("asList", Object[].class).invoke(null, objArr));
    }

    public static AddressListField bcc(Iterable<Address> addresses) {
        Class[] clsArr = {String.class, Iterable.class};
        return (AddressListField) Fields.class.getMethod("addressList0", clsArr).invoke(null, FieldName.BCC, addresses);
    }

    public static AddressListField replyTo(Address address) {
        Object[] objArr = {address};
        Class[] clsArr = {String.class, Iterable.class};
        return (AddressListField) Fields.class.getMethod("addressList0", clsArr).invoke(null, FieldName.REPLY_TO, (Set) Collections.class.getMethod("singleton", Object.class).invoke(null, objArr));
    }

    public static AddressListField replyTo(Address... addresses) {
        Object[] objArr = {addresses};
        Class[] clsArr = {String.class, Iterable.class};
        return (AddressListField) Fields.class.getMethod("addressList0", clsArr).invoke(null, FieldName.REPLY_TO, (List) Arrays.class.getMethod("asList", Object[].class).invoke(null, objArr));
    }

    public static AddressListField replyTo(Iterable<Address> addresses) {
        Class[] clsArr = {String.class, Iterable.class};
        return (AddressListField) Fields.class.getMethod("addressList0", clsArr).invoke(null, FieldName.REPLY_TO, addresses);
    }

    public static MailboxField mailbox(String fieldName, Mailbox mailbox) {
        Object[] objArr = {fieldName};
        Fields.class.getMethod("checkValidFieldName", String.class).invoke(null, objArr);
        return (MailboxField) Fields.class.getMethod("mailbox0", String.class, Mailbox.class).invoke(null, fieldName, mailbox);
    }

    public static MailboxListField mailboxList(String fieldName, Iterable<Mailbox> mailboxes) {
        Object[] objArr = {fieldName};
        Fields.class.getMethod("checkValidFieldName", String.class).invoke(null, objArr);
        return (MailboxListField) Fields.class.getMethod("mailboxList0", String.class, Iterable.class).invoke(null, fieldName, mailboxes);
    }

    public static AddressListField addressList(String fieldName, Iterable<Address> addresses) {
        Object[] objArr = {fieldName};
        Fields.class.getMethod("checkValidFieldName", String.class).invoke(null, objArr);
        return (AddressListField) Fields.class.getMethod("addressList0", String.class, Iterable.class).invoke(null, fieldName, addresses);
    }

    private static DateTimeField date0(String fieldName, Date date, TimeZone zone) {
        Object[] objArr = {date, zone};
        FieldParser fieldParser = DateTimeField.PARSER;
        Class[] clsArr = {FieldParser.class, String.class, String.class};
        return (DateTimeField) ((Field) Fields.class.getMethod("parse", clsArr).invoke(null, fieldParser, fieldName, (String) MimeUtil.class.getMethod("formatDate", Date.class, TimeZone.class).invoke(null, objArr)));
    }

    private static MailboxField mailbox0(String fieldName, Mailbox mailbox) {
        Object[] objArr = {mailbox};
        Object[] objArr2 = {(Set) Collections.class.getMethod("singleton", Object.class).invoke(null, objArr)};
        FieldParser fieldParser = MailboxField.PARSER;
        Class[] clsArr = {FieldParser.class, String.class, String.class};
        return (MailboxField) ((Field) Fields.class.getMethod("parse", clsArr).invoke(null, fieldParser, fieldName, (String) Fields.class.getMethod("encodeAddresses", Iterable.class).invoke(null, objArr2)));
    }

    private static MailboxListField mailboxList0(String fieldName, Iterable<Mailbox> mailboxes) {
        Object[] objArr = {mailboxes};
        FieldParser fieldParser = MailboxListField.PARSER;
        Class[] clsArr = {FieldParser.class, String.class, String.class};
        return (MailboxListField) ((Field) Fields.class.getMethod("parse", clsArr).invoke(null, fieldParser, fieldName, (String) Fields.class.getMethod("encodeAddresses", Iterable.class).invoke(null, objArr)));
    }

    private static AddressListField addressList0(String fieldName, Iterable<Address> addresses) {
        Object[] objArr = {addresses};
        FieldParser fieldParser = AddressListField.PARSER;
        Class[] clsArr = {FieldParser.class, String.class, String.class};
        return (AddressListField) ((Field) Fields.class.getMethod("parse", clsArr).invoke(null, fieldParser, fieldName, (String) Fields.class.getMethod("encodeAddresses", Iterable.class).invoke(null, objArr)));
    }

    private static void checkValidFieldName(String fieldName) {
        Pattern pattern = FIELD_NAME_PATTERN;
        Class[] clsArr = {CharSequence.class};
        Object[] objArr = {fieldName};
        if (!((Boolean) Matcher.class.getMethod("matches", new Class[0]).invoke((Matcher) Pattern.class.getMethod("matcher", clsArr).invoke(pattern, objArr), new Object[0])).booleanValue()) {
            throw new IllegalArgumentException("Invalid field name");
        }
    }

    private static boolean isValidMimeType(String mimeType) {
        if (mimeType == null) {
            return false;
        }
        Class[] clsArr = {Integer.TYPE};
        int idx = ((Integer) String.class.getMethod("indexOf", clsArr).invoke(mimeType, new Integer(47))).intValue();
        if (idx == -1) {
            return false;
        }
        Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
        Object[] objArr = {new Integer(0), new Integer(idx)};
        Class[] clsArr3 = {Integer.TYPE};
        String subType = (String) String.class.getMethod("substring", clsArr3).invoke(mimeType, new Integer(idx + 1));
        Class[] clsArr4 = {String.class};
        if (!((Boolean) EncoderUtil.class.getMethod("isToken", clsArr4).invoke(null, (String) String.class.getMethod("substring", clsArr2).invoke(mimeType, objArr))).booleanValue()) {
            return false;
        }
        if (((Boolean) EncoderUtil.class.getMethod("isToken", String.class).invoke(null, subType)).booleanValue()) {
            return true;
        }
        return false;
    }

    private static boolean isValidDispositionType(String dispositionType) {
        if (dispositionType == null) {
            return false;
        }
        return ((Boolean) EncoderUtil.class.getMethod("isToken", String.class).invoke(null, dispositionType)).booleanValue();
    }

    private static <F extends Field> F parse(FieldParser parser, String fieldName, String fieldBody) {
        Object[] objArr = {fieldName};
        Object[] objArr2 = {": "};
        Method method = StringBuilder.class.getMethod("append", String.class);
        Object[] objArr3 = {fieldBody};
        Method method2 = StringBuilder.class.getMethod("append", String.class);
        Method method3 = StringBuilder.class.getMethod("toString", new Class[0]);
        Class[] clsArr = {String.class, Integer.TYPE};
        Object[] objArr4 = {(String) method3.invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr), objArr2), objArr3), new Object[0]), new Integer(0)};
        Object[] objArr5 = {(String) MimeUtil.class.getMethod("fold", clsArr).invoke(null, objArr4)};
        Class[] clsArr2 = {String.class, String.class, ByteSequence.class};
        return (ParsedField) FieldParser.class.getMethod("parse", clsArr2).invoke(parser, fieldName, fieldBody, (ByteSequence) ContentUtil.class.getMethod("encode", String.class).invoke(null, objArr5));
    }

    private static String encodeAddresses(Iterable<? extends Address> addresses) {
        StringBuilder sb = new StringBuilder();
        Iterator i$ = (Iterator) Iterable.class.getMethod("iterator", new Class[0]).invoke(addresses, new Object[0]);
        while (true) {
            if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                Address address = (Address) Iterator.class.getMethod("next", new Class[0]).invoke(i$, new Object[0]);
                if (((Integer) StringBuilder.class.getMethod("length", new Class[0]).invoke(sb, new Object[0])).intValue() > 0) {
                    Object[] objArr = {", "};
                    StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr);
                }
                Object[] objArr2 = {(String) Address.class.getMethod("getEncodedString", new Class[0]).invoke(address, new Object[0])};
                StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr2);
            } else {
                return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb, new Object[0]);
            }
        }
    }
}
