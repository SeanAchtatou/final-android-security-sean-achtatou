package org.apache.james.mime4j.message;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.james.mime4j.field.ContentDispositionField;
import org.apache.james.mime4j.field.ContentTransferEncodingField;
import org.apache.james.mime4j.field.ContentTypeField;
import org.apache.james.mime4j.field.Fields;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.util.MimeUtil;

public abstract class Entity implements Disposable {
    private Body body = null;
    private Header header = null;
    private Entity parent = null;

    protected Entity() {
    }

    protected Entity(Entity other) {
        if (other.header != null) {
            this.header = new Header(other.header);
        }
        if (other.body != null) {
            Object[] objArr = {other.body};
            Object[] objArr2 = {(Body) BodyCopier.class.getMethod("copy", Body.class).invoke(null, objArr)};
            Entity.class.getMethod("setBody", Body.class).invoke(this, objArr2);
        }
    }

    public Entity getParent() {
        return this.parent;
    }

    public void setParent(Entity parent2) {
        this.parent = parent2;
    }

    public Header getHeader() {
        return this.header;
    }

    public void setHeader(Header header2) {
        this.header = header2;
    }

    public Body getBody() {
        return this.body;
    }

    public void setBody(Body body2) {
        if (this.body != null) {
            throw new IllegalStateException("body already set");
        }
        this.body = body2;
        Object[] objArr = {this};
        Body.class.getMethod("setParent", Entity.class).invoke(body2, objArr);
    }

    public Body removeBody() {
        if (this.body == null) {
            return null;
        }
        Body body2 = this.body;
        this.body = null;
        Object[] objArr = {null};
        Body.class.getMethod("setParent", Entity.class).invoke(body2, objArr);
        return body2;
    }

    public void setMessage(Message message) {
        Object[] objArr = {message, ContentTypeField.TYPE_MESSAGE_RFC822, null};
        Entity.class.getMethod("setBody", Body.class, String.class, Map.class).invoke(this, objArr);
    }

    public void setMultipart(Multipart multipart) {
        Object[] objArr = {ContentTypeField.TYPE_MULTIPART_PREFIX};
        Object[] objArr2 = {(String) Multipart.class.getMethod("getSubType", new Class[0]).invoke(multipart, new Object[0])};
        Method method = StringBuilder.class.getMethod("append", String.class);
        Method method2 = StringBuilder.class.getMethod("toString", new Class[0]);
        Object[] objArr3 = {ContentTypeField.PARAM_BOUNDARY, (String) MimeUtil.class.getMethod("createUniqueBoundary", new Class[0]).invoke(null, new Object[0])};
        Object[] objArr4 = {multipart, (String) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr), objArr2), new Object[0]), (Map) Collections.class.getMethod("singletonMap", Object.class, Object.class).invoke(null, objArr3)};
        Entity.class.getMethod("setBody", Body.class, String.class, Map.class).invoke(this, objArr4);
    }

    public void setMultipart(Multipart multipart, Map<String, String> parameters) {
        Object[] objArr = {ContentTypeField.TYPE_MULTIPART_PREFIX};
        Object[] objArr2 = {(String) Multipart.class.getMethod("getSubType", new Class[0]).invoke(multipart, new Object[0])};
        Method method = StringBuilder.class.getMethod("append", String.class);
        Method method2 = StringBuilder.class.getMethod("toString", new Class[0]);
        String mimeType = (String) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr), objArr2), new Object[0]);
        Class[] clsArr = {Object.class};
        if (!((Boolean) Map.class.getMethod("containsKey", clsArr).invoke(parameters, ContentTypeField.PARAM_BOUNDARY)).booleanValue()) {
            Map<String, String> parameters2 = new HashMap<>(parameters);
            Object[] objArr3 = {ContentTypeField.PARAM_BOUNDARY, (String) MimeUtil.class.getMethod("createUniqueBoundary", new Class[0]).invoke(null, new Object[0])};
            Map.class.getMethod("put", Object.class, Object.class).invoke(parameters2, objArr3);
            parameters = parameters2;
        }
        Object[] objArr4 = {multipart, mimeType, parameters};
        Entity.class.getMethod("setBody", Body.class, String.class, Map.class).invoke(this, objArr4);
    }

    public void setText(TextBody textBody) {
        Object[] objArr = {textBody, "plain"};
        Entity.class.getMethod("setText", TextBody.class, String.class).invoke(this, objArr);
    }

    public void setText(TextBody textBody, String subtype) {
        Object[] objArr = {"text/"};
        Object[] objArr2 = {subtype};
        Method method = StringBuilder.class.getMethod("append", String.class);
        Method method2 = StringBuilder.class.getMethod("toString", new Class[0]);
        String mimeType = (String) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr), objArr2), new Object[0]);
        Map<String, String> parameters = null;
        String mimeCharset = (String) TextBody.class.getMethod("getMimeCharset", new Class[0]).invoke(textBody, new Object[0]);
        if (mimeCharset != null) {
            if (!((Boolean) String.class.getMethod("equalsIgnoreCase", String.class).invoke(mimeCharset, "us-ascii")).booleanValue()) {
                Object[] objArr3 = {ContentTypeField.PARAM_CHARSET, mimeCharset};
                parameters = (Map) Collections.class.getMethod("singletonMap", Object.class, Object.class).invoke(null, objArr3);
            }
        }
        Object[] objArr4 = {textBody, mimeType, parameters};
        Entity.class.getMethod("setBody", Body.class, String.class, Map.class).invoke(this, objArr4);
    }

    public void setBody(Body body2, String mimeType) {
        Object[] objArr = {body2, mimeType, null};
        Entity.class.getMethod("setBody", Body.class, String.class, Map.class).invoke(this, objArr);
    }

    public void setBody(Body body2, String mimeType, Map<String, String> parameters) {
        Object[] objArr = {body2};
        Entity.class.getMethod("setBody", Body.class).invoke(this, objArr);
        Method method = Entity.class.getMethod("obtainHeader", new Class[0]);
        Object[] objArr2 = {mimeType, parameters};
        Object[] objArr3 = {(ContentTypeField) Fields.class.getMethod("contentType", String.class, Map.class).invoke(null, objArr2)};
        Header.class.getMethod("setField", Field.class).invoke((Header) method.invoke(this, new Object[0]), objArr3);
    }

    public String getMimeType() {
        ContentTypeField parent2;
        Method method = Entity.class.getMethod("getHeader", new Class[0]);
        Method method2 = Header.class.getMethod("getField", String.class);
        ContentTypeField child = (ContentTypeField) ((Field) method2.invoke((Header) method.invoke(this, new Object[0]), "Content-Type"));
        if (((Entity) Entity.class.getMethod("getParent", new Class[0]).invoke(this, new Object[0])) != null) {
            Method method3 = Entity.class.getMethod("getParent", new Class[0]);
            Method method4 = Entity.class.getMethod("getHeader", new Class[0]);
            Method method5 = Header.class.getMethod("getField", String.class);
            parent2 = (ContentTypeField) ((Field) method5.invoke((Header) method4.invoke((Entity) method3.invoke(this, new Object[0]), new Object[0]), "Content-Type"));
        } else {
            parent2 = null;
        }
        return (String) ContentTypeField.class.getMethod("getMimeType", ContentTypeField.class, ContentTypeField.class).invoke(null, child, parent2);
    }

    public String getCharset() {
        Method method = Entity.class.getMethod("getHeader", new Class[0]);
        Method method2 = Header.class.getMethod("getField", String.class);
        Class[] clsArr = {ContentTypeField.class};
        return (String) ContentTypeField.class.getMethod("getCharset", clsArr).invoke(null, (ContentTypeField) ((Field) method2.invoke((Header) method.invoke(this, new Object[0]), "Content-Type")));
    }

    public String getContentTransferEncoding() {
        Method method = Entity.class.getMethod("getHeader", new Class[0]);
        Method method2 = Header.class.getMethod("getField", String.class);
        Class[] clsArr = {ContentTransferEncodingField.class};
        return (String) ContentTransferEncodingField.class.getMethod("getEncoding", clsArr).invoke(null, (ContentTransferEncodingField) ((Field) method2.invoke((Header) method.invoke(this, new Object[0]), "Content-Transfer-Encoding")));
    }

    public void setContentTransferEncoding(String contentTransferEncoding) {
        Method method = Entity.class.getMethod("obtainHeader", new Class[0]);
        Object[] objArr = {contentTransferEncoding};
        Object[] objArr2 = {(ContentTransferEncodingField) Fields.class.getMethod("contentTransferEncoding", String.class).invoke(null, objArr)};
        Header.class.getMethod("setField", Field.class).invoke((Header) method.invoke(this, new Object[0]), objArr2);
    }

    public String getDispositionType() {
        Object[] objArr = {"Content-Disposition"};
        ContentDispositionField field = (ContentDispositionField) ((Field) Entity.class.getMethod("obtainField", String.class).invoke(this, objArr));
        if (field == null) {
            return null;
        }
        return (String) ContentDispositionField.class.getMethod("getDispositionType", new Class[0]).invoke(field, new Object[0]);
    }

    public void setContentDisposition(String dispositionType) {
        Method method = Entity.class.getMethod("obtainHeader", new Class[0]);
        Class[] clsArr = {String.class, String.class, Long.TYPE, Date.class, Date.class, Date.class};
        Object[] objArr = {(ContentDispositionField) Fields.class.getMethod("contentDisposition", clsArr).invoke(null, dispositionType, null, new Long(-1), null, null, null)};
        Header.class.getMethod("setField", Field.class).invoke((Header) method.invoke(this, new Object[0]), objArr);
    }

    public void setContentDisposition(String dispositionType, String filename) {
        Method method = Entity.class.getMethod("obtainHeader", new Class[0]);
        Class[] clsArr = {String.class, String.class, Long.TYPE, Date.class, Date.class, Date.class};
        Object[] objArr = {(ContentDispositionField) Fields.class.getMethod("contentDisposition", clsArr).invoke(null, dispositionType, filename, new Long(-1), null, null, null)};
        Header.class.getMethod("setField", Field.class).invoke((Header) method.invoke(this, new Object[0]), objArr);
    }

    public void setContentDisposition(String dispositionType, String filename, long size) {
        obtainHeader().setField(Fields.contentDisposition(dispositionType, filename, size, null, null, null));
    }

    public void setContentDisposition(String dispositionType, String filename, long size, Date creationDate, Date modificationDate, Date readDate) {
        Method method = Entity.class.getMethod("obtainHeader", new Class[0]);
        Class[] clsArr = {String.class, String.class, Long.TYPE, Date.class, Date.class, Date.class};
        Object[] objArr = {(ContentDispositionField) Fields.class.getMethod("contentDisposition", clsArr).invoke(null, dispositionType, filename, new Long(size), creationDate, modificationDate, readDate)};
        Header.class.getMethod("setField", Field.class).invoke((Header) method.invoke(this, new Object[0]), objArr);
    }

    public String getFilename() {
        Object[] objArr = {"Content-Disposition"};
        ContentDispositionField field = (ContentDispositionField) ((Field) Entity.class.getMethod("obtainField", String.class).invoke(this, objArr));
        if (field == null) {
            return null;
        }
        return (String) ContentDispositionField.class.getMethod("getFilename", new Class[0]).invoke(field, new Object[0]);
    }

    public void setFilename(String filename) {
        Header header2 = obtainHeader();
        ContentDispositionField field = (ContentDispositionField) header2.getField("Content-Disposition");
        if (field != null) {
            String dispositionType = field.getDispositionType();
            Map<String, String> parameters = new HashMap<>(field.getParameters());
            if (filename == null) {
                parameters.remove("filename");
            } else {
                parameters.put("filename", filename);
            }
            header2.setField(Fields.contentDisposition(dispositionType, parameters));
        } else if (filename != null) {
            header2.setField(Fields.contentDisposition(ContentDispositionField.DISPOSITION_TYPE_ATTACHMENT, filename, -1, null, null, null));
        }
    }

    public boolean isMimeType(String type) {
        Method method = Entity.class.getMethod("getMimeType", new Class[0]);
        Object[] objArr = {type};
        return ((Boolean) String.class.getMethod("equalsIgnoreCase", String.class).invoke((String) method.invoke(this, new Object[0]), objArr)).booleanValue();
    }

    public boolean isMultipart() {
        Method method = Entity.class.getMethod("getHeader", new Class[0]);
        Method method2 = Header.class.getMethod("getField", String.class);
        ContentTypeField f = (ContentTypeField) ((Field) method2.invoke((Header) method.invoke(this, new Object[0]), "Content-Type"));
        if (f != null) {
            if (((String) ContentTypeField.class.getMethod("getBoundary", new Class[0]).invoke(f, new Object[0])) != null) {
                Method method3 = Entity.class.getMethod("getMimeType", new Class[0]);
                Object[] objArr = {ContentTypeField.TYPE_MULTIPART_PREFIX};
                if (((Boolean) String.class.getMethod("startsWith", String.class).invoke((String) method3.invoke(this, new Object[0]), objArr)).booleanValue()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void dispose() {
        if (this.body != null) {
            Body.class.getMethod("dispose", new Class[0]).invoke(this.body, new Object[0]);
        }
    }

    /* access modifiers changed from: package-private */
    public Header obtainHeader() {
        if (this.header == null) {
            this.header = new Header();
        }
        return this.header;
    }

    /* access modifiers changed from: package-private */
    public <F extends Field> F obtainField(String fieldName) {
        Header header2 = (Header) Entity.class.getMethod("getHeader", new Class[0]).invoke(this, new Object[0]);
        if (header2 == null) {
            return null;
        }
        return (Field) Header.class.getMethod("getField", String.class).invoke(header2, fieldName);
    }
}
