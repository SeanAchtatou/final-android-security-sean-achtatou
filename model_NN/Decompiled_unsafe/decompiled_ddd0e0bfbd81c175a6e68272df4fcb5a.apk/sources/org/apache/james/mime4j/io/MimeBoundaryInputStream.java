package org.apache.james.mime4j.io;

import java.io.IOException;
import java.lang.reflect.Method;
import org.apache.james.mime4j.util.ByteArrayBuffer;

public class MimeBoundaryInputStream extends LineReaderInputStream {
    private boolean atBoundary;
    private final byte[] boundary;
    private int boundaryLen;
    private BufferedLineReaderInputStream buffer;
    private boolean completed;
    private boolean eof;
    private boolean lastPart;
    private int limit;

    public MimeBoundaryInputStream(BufferedLineReaderInputStream inbuffer, String boundary2) throws IOException {
        super(inbuffer);
        if (((Integer) BufferedLineReaderInputStream.class.getMethod("capacity", new Class[0]).invoke(inbuffer, new Object[0])).intValue() <= ((Integer) String.class.getMethod("length", new Class[0]).invoke(boundary2, new Object[0])).intValue()) {
            throw new IllegalArgumentException("Boundary is too long");
        }
        this.buffer = inbuffer;
        this.eof = false;
        this.limit = -1;
        this.atBoundary = false;
        this.boundaryLen = 0;
        this.lastPart = false;
        this.completed = false;
        this.boundary = new byte[(((Integer) String.class.getMethod("length", new Class[0]).invoke(boundary2, new Object[0])).intValue() + 2)];
        this.boundary[0] = 45;
        this.boundary[1] = 45;
        int i = 0;
        while (true) {
            if (i < ((Integer) String.class.getMethod("length", new Class[0]).invoke(boundary2, new Object[0])).intValue()) {
                Class[] clsArr = {Integer.TYPE};
                byte ch = (byte) ((Character) String.class.getMethod("charAt", clsArr).invoke(boundary2, new Integer(i))).charValue();
                if (ch != 13 && ch != 10) {
                    this.boundary[i + 2] = ch;
                    i++;
                }
            } else {
                ((Integer) MimeBoundaryInputStream.class.getMethod("fillBuffer", new Class[0]).invoke(this, new Object[0])).intValue();
                return;
            }
        }
        throw new IllegalArgumentException("Boundary may not contain CR or LF");
    }

    public void close() throws IOException {
    }

    public boolean markSupported() {
        return false;
    }

    public int read() throws IOException {
        if (this.completed) {
            return -1;
        }
        if (((Boolean) MimeBoundaryInputStream.class.getMethod("endOfStream", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
            if (!((Boolean) MimeBoundaryInputStream.class.getMethod("hasData", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
                MimeBoundaryInputStream.class.getMethod("skipBoundary", new Class[0]).invoke(this, new Object[0]);
                return -1;
            }
        }
        while (true) {
            if (((Boolean) MimeBoundaryInputStream.class.getMethod("hasData", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
                return ((Integer) BufferedLineReaderInputStream.class.getMethod("read", new Class[0]).invoke(this.buffer, new Object[0])).intValue();
            }
            if (((Boolean) MimeBoundaryInputStream.class.getMethod("endOfStream", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
                MimeBoundaryInputStream.class.getMethod("skipBoundary", new Class[0]).invoke(this, new Object[0]);
                return -1;
            }
            ((Integer) MimeBoundaryInputStream.class.getMethod("fillBuffer", new Class[0]).invoke(this, new Object[0])).intValue();
        }
    }

    public int read(byte[] b, int off, int len) throws IOException {
        if (this.completed) {
            return -1;
        }
        if (((Boolean) MimeBoundaryInputStream.class.getMethod("endOfStream", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
            if (!((Boolean) MimeBoundaryInputStream.class.getMethod("hasData", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
                MimeBoundaryInputStream.class.getMethod("skipBoundary", new Class[0]).invoke(this, new Object[0]);
                return -1;
            }
        }
        ((Integer) MimeBoundaryInputStream.class.getMethod("fillBuffer", new Class[0]).invoke(this, new Object[0])).intValue();
        if (!((Boolean) MimeBoundaryInputStream.class.getMethod("hasData", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
            Class[] clsArr = {byte[].class, Integer.TYPE, Integer.TYPE};
            return ((Integer) MimeBoundaryInputStream.class.getMethod("read", clsArr).invoke(this, b, new Integer(off), new Integer(len))).intValue();
        }
        int i = this.limit;
        Method method = BufferedLineReaderInputStream.class.getMethod("pos", new Class[0]);
        Class[] clsArr2 = {Integer.TYPE, Integer.TYPE};
        int chunk = ((Integer) Math.class.getMethod("min", clsArr2).invoke(null, new Integer(len), new Integer(i - ((Integer) method.invoke(this.buffer, new Object[0])).intValue()))).intValue();
        BufferedLineReaderInputStream bufferedLineReaderInputStream = this.buffer;
        Class[] clsArr3 = {byte[].class, Integer.TYPE, Integer.TYPE};
        return ((Integer) BufferedLineReaderInputStream.class.getMethod("read", clsArr3).invoke(bufferedLineReaderInputStream, b, new Integer(off), new Integer(chunk))).intValue();
    }

    public int readLine(ByteArrayBuffer dst) throws IOException {
        int chunk;
        if (dst == null) {
            throw new IllegalArgumentException("Destination buffer may not be null");
        } else if (this.completed) {
            return -1;
        } else {
            if (((Boolean) MimeBoundaryInputStream.class.getMethod("endOfStream", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
                if (!((Boolean) MimeBoundaryInputStream.class.getMethod("hasData", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
                    MimeBoundaryInputStream.class.getMethod("skipBoundary", new Class[0]).invoke(this, new Object[0]);
                    return -1;
                }
            }
            int total = 0;
            boolean found = false;
            int bytesRead = 0;
            while (true) {
                if (found) {
                    break;
                }
                if (!((Boolean) MimeBoundaryInputStream.class.getMethod("hasData", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
                    bytesRead = ((Integer) MimeBoundaryInputStream.class.getMethod("fillBuffer", new Class[0]).invoke(this, new Object[0])).intValue();
                    if (!((Boolean) MimeBoundaryInputStream.class.getMethod("hasData", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
                        if (((Boolean) MimeBoundaryInputStream.class.getMethod("endOfStream", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
                            MimeBoundaryInputStream.class.getMethod("skipBoundary", new Class[0]).invoke(this, new Object[0]);
                            bytesRead = -1;
                            break;
                        }
                    }
                }
                int len = this.limit - ((Integer) BufferedLineReaderInputStream.class.getMethod("pos", new Class[0]).invoke(this.buffer, new Object[0])).intValue();
                BufferedLineReaderInputStream bufferedLineReaderInputStream = this.buffer;
                int intValue = ((Integer) BufferedLineReaderInputStream.class.getMethod("pos", new Class[0]).invoke(this.buffer, new Object[0])).intValue();
                Class[] clsArr = {Byte.TYPE, Integer.TYPE, Integer.TYPE};
                int i = ((Integer) BufferedLineReaderInputStream.class.getMethod("indexOf", clsArr).invoke(bufferedLineReaderInputStream, new Byte((byte) 10), new Integer(intValue), new Integer(len))).intValue();
                if (i != -1) {
                    found = true;
                    chunk = (i + 1) - ((Integer) BufferedLineReaderInputStream.class.getMethod("pos", new Class[0]).invoke(this.buffer, new Object[0])).intValue();
                } else {
                    chunk = len;
                }
                if (chunk > 0) {
                    Method method = BufferedLineReaderInputStream.class.getMethod("buf", new Class[0]);
                    int intValue2 = ((Integer) BufferedLineReaderInputStream.class.getMethod("pos", new Class[0]).invoke(this.buffer, new Object[0])).intValue();
                    Class[] clsArr2 = {byte[].class, Integer.TYPE, Integer.TYPE};
                    ByteArrayBuffer.class.getMethod("append", clsArr2).invoke(dst, (byte[]) method.invoke(this.buffer, new Object[0]), new Integer(intValue2), new Integer(chunk));
                    BufferedLineReaderInputStream bufferedLineReaderInputStream2 = this.buffer;
                    Class[] clsArr3 = {Integer.TYPE};
                    ((Integer) BufferedLineReaderInputStream.class.getMethod("skip", clsArr3).invoke(bufferedLineReaderInputStream2, new Integer(chunk))).intValue();
                    total += chunk;
                }
            }
            if (total == 0 && bytesRead == -1) {
                return -1;
            }
            return total;
        }
    }

    private boolean endOfStream() {
        return this.eof || this.atBoundary;
    }

    private boolean hasData() {
        if (this.limit > ((Integer) BufferedLineReaderInputStream.class.getMethod("pos", new Class[0]).invoke(this.buffer, new Object[0])).intValue()) {
            if (this.limit <= ((Integer) BufferedLineReaderInputStream.class.getMethod("limit", new Class[0]).invoke(this.buffer, new Object[0])).intValue()) {
                return true;
            }
        }
        return false;
    }

    private int fillBuffer() throws IOException {
        int bytesRead;
        if (this.eof) {
            return -1;
        }
        if (!((Boolean) MimeBoundaryInputStream.class.getMethod("hasData", new Class[0]).invoke(this, new Object[0])).booleanValue()) {
            bytesRead = ((Integer) BufferedLineReaderInputStream.class.getMethod("fillBuffer", new Class[0]).invoke(this.buffer, new Object[0])).intValue();
        } else {
            bytesRead = 0;
        }
        this.eof = bytesRead == -1;
        int i = ((Integer) BufferedLineReaderInputStream.class.getMethod("indexOf", byte[].class).invoke(this.buffer, this.boundary)).intValue();
        while (i > 0) {
            if (((Byte) BufferedLineReaderInputStream.class.getMethod("charAt", Integer.TYPE).invoke(this.buffer, new Integer(i - 1))).byteValue() == 10) {
                break;
            }
            int i2 = i + this.boundary.length;
            BufferedLineReaderInputStream bufferedLineReaderInputStream = this.buffer;
            byte[] bArr = this.boundary;
            Method method = BufferedLineReaderInputStream.class.getMethod("limit", new Class[0]);
            i = ((Integer) BufferedLineReaderInputStream.class.getMethod("indexOf", byte[].class, Integer.TYPE, Integer.TYPE).invoke(bufferedLineReaderInputStream, bArr, new Integer(i2), new Integer(((Integer) method.invoke(this.buffer, new Object[0])).intValue() - i2))).intValue();
        }
        if (i != -1) {
            this.limit = i;
            this.atBoundary = true;
            MimeBoundaryInputStream.class.getMethod("calculateBoundaryLen", new Class[0]).invoke(this, new Object[0]);
            return bytesRead;
        } else if (this.eof) {
            this.limit = ((Integer) BufferedLineReaderInputStream.class.getMethod("limit", new Class[0]).invoke(this.buffer, new Object[0])).intValue();
            return bytesRead;
        } else {
            this.limit = ((Integer) BufferedLineReaderInputStream.class.getMethod("limit", new Class[0]).invoke(this.buffer, new Object[0])).intValue() - (this.boundary.length + 1);
            return bytesRead;
        }
    }

    private void calculateBoundaryLen() throws IOException {
        this.boundaryLen = this.boundary.length;
        int len = this.limit - ((Integer) BufferedLineReaderInputStream.class.getMethod("pos", new Class[0]).invoke(this.buffer, new Object[0])).intValue();
        if (len > 0) {
            if (((Byte) BufferedLineReaderInputStream.class.getMethod("charAt", Integer.TYPE).invoke(this.buffer, new Integer(this.limit - 1))).byteValue() == 10) {
                this.boundaryLen++;
                this.limit--;
            }
        }
        if (len > 1) {
            if (((Byte) BufferedLineReaderInputStream.class.getMethod("charAt", Integer.TYPE).invoke(this.buffer, new Integer(this.limit - 1))).byteValue() == 13) {
                this.boundaryLen++;
                this.limit--;
            }
        }
    }

    private void skipBoundary() throws IOException {
        if (!this.completed) {
            this.completed = true;
            BufferedLineReaderInputStream bufferedLineReaderInputStream = this.buffer;
            int i = this.boundaryLen;
            Class[] clsArr = {Integer.TYPE};
            ((Integer) BufferedLineReaderInputStream.class.getMethod("skip", clsArr).invoke(bufferedLineReaderInputStream, new Integer(i))).intValue();
            boolean checkForLastPart = true;
            while (true) {
                if (((Integer) BufferedLineReaderInputStream.class.getMethod("length", new Class[0]).invoke(this.buffer, new Object[0])).intValue() > 1) {
                    BufferedLineReaderInputStream bufferedLineReaderInputStream2 = this.buffer;
                    int intValue = ((Integer) BufferedLineReaderInputStream.class.getMethod("pos", new Class[0]).invoke(this.buffer, new Object[0])).intValue();
                    Class[] clsArr2 = {Integer.TYPE};
                    int ch1 = ((Byte) BufferedLineReaderInputStream.class.getMethod("charAt", clsArr2).invoke(bufferedLineReaderInputStream2, new Integer(intValue))).byteValue();
                    BufferedLineReaderInputStream bufferedLineReaderInputStream3 = this.buffer;
                    Method method = BufferedLineReaderInputStream.class.getMethod("pos", new Class[0]);
                    Class[] clsArr3 = {Integer.TYPE};
                    int ch2 = ((Byte) BufferedLineReaderInputStream.class.getMethod("charAt", clsArr3).invoke(bufferedLineReaderInputStream3, new Integer(((Integer) method.invoke(this.buffer, new Object[0])).intValue() + 1))).byteValue();
                    if (checkForLastPart && ch1 == 45 && ch2 == 45) {
                        this.lastPart = true;
                        BufferedLineReaderInputStream bufferedLineReaderInputStream4 = this.buffer;
                        Class[] clsArr4 = {Integer.TYPE};
                        ((Integer) BufferedLineReaderInputStream.class.getMethod("skip", clsArr4).invoke(bufferedLineReaderInputStream4, new Integer(2))).intValue();
                        checkForLastPart = false;
                    } else if (ch1 == 13 && ch2 == 10) {
                        BufferedLineReaderInputStream bufferedLineReaderInputStream5 = this.buffer;
                        Class[] clsArr5 = {Integer.TYPE};
                        ((Integer) BufferedLineReaderInputStream.class.getMethod("skip", clsArr5).invoke(bufferedLineReaderInputStream5, new Integer(2))).intValue();
                        return;
                    } else if (ch1 == 10) {
                        BufferedLineReaderInputStream bufferedLineReaderInputStream6 = this.buffer;
                        Class[] clsArr6 = {Integer.TYPE};
                        ((Integer) BufferedLineReaderInputStream.class.getMethod("skip", clsArr6).invoke(bufferedLineReaderInputStream6, new Integer(1))).intValue();
                        return;
                    } else {
                        BufferedLineReaderInputStream bufferedLineReaderInputStream7 = this.buffer;
                        Class[] clsArr7 = {Integer.TYPE};
                        ((Integer) BufferedLineReaderInputStream.class.getMethod("skip", clsArr7).invoke(bufferedLineReaderInputStream7, new Integer(1))).intValue();
                    }
                } else if (!this.eof) {
                    ((Integer) MimeBoundaryInputStream.class.getMethod("fillBuffer", new Class[0]).invoke(this, new Object[0])).intValue();
                } else {
                    return;
                }
            }
        }
    }

    public boolean isLastPart() {
        return this.lastPart;
    }

    public boolean eof() {
        if (this.eof) {
            if (!((Boolean) BufferedLineReaderInputStream.class.getMethod("hasBufferedData", new Class[0]).invoke(this.buffer, new Object[0])).booleanValue()) {
                return true;
            }
        }
        return false;
    }

    public String toString() {
        StringBuilder buffer2 = new StringBuilder("MimeBoundaryInputStream, boundary ");
        byte[] arr$ = this.boundary;
        int len$ = arr$.length;
        for (int i$ = 0; i$ < len$; i$++) {
            Class[] clsArr = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr).invoke(buffer2, new Character((char) arr$[i$]));
        }
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(buffer2, new Object[0]);
    }
}
