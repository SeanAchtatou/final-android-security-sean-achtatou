package org.apache.james.mime4j.parser;

import java.lang.reflect.Method;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.ContentUtil;

class RawField implements Field {
    private String body;
    private int colonIdx;
    private String name;
    private final ByteSequence raw;

    public RawField(ByteSequence raw2, int colonIdx2) {
        this.raw = raw2;
        this.colonIdx = colonIdx2;
    }

    public String getName() {
        if (this.name == null) {
            this.name = (String) RawField.class.getMethod("parseName", new Class[0]).invoke(this, new Object[0]);
        }
        return this.name;
    }

    public String getBody() {
        if (this.body == null) {
            this.body = (String) RawField.class.getMethod("parseBody", new Class[0]).invoke(this, new Object[0]);
        }
        return this.body;
    }

    public ByteSequence getRaw() {
        return this.raw;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        Class[] clsArr = {String.class};
        Object[] objArr = {(String) RawField.class.getMethod("getName", new Class[0]).invoke(this, new Object[0])};
        Class[] clsArr2 = {Character.TYPE};
        Object[] objArr2 = {new Character(':')};
        Method method = StringBuilder.class.getMethod("append", clsArr2);
        Object[] objArr3 = {(String) RawField.class.getMethod("getBody", new Class[0]).invoke(this, new Object[0])};
        Method method2 = StringBuilder.class.getMethod("append", String.class);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), objArr3), new Object[0]);
    }

    private String parseName() {
        ByteSequence byteSequence = this.raw;
        int i = this.colonIdx;
        Class[] clsArr = {ByteSequence.class, Integer.TYPE, Integer.TYPE};
        return (String) ContentUtil.class.getMethod("decode", clsArr).invoke(null, byteSequence, new Integer(0), new Integer(i));
    }

    private String parseBody() {
        int offset = this.colonIdx + 1;
        int length = ((Integer) ByteSequence.class.getMethod("length", new Class[0]).invoke(this.raw, new Object[0])).intValue() - offset;
        ByteSequence byteSequence = this.raw;
        Class[] clsArr = {ByteSequence.class, Integer.TYPE, Integer.TYPE};
        return (String) ContentUtil.class.getMethod("decode", clsArr).invoke(null, byteSequence, new Integer(offset), new Integer(length));
    }
}
