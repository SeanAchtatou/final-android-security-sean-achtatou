package org.apache.james.mime4j.field;

import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.james.mime4j.field.contentdisposition.parser.ContentDispositionParser;
import org.apache.james.mime4j.field.datetime.DateTime;
import org.apache.james.mime4j.field.datetime.parser.DateTimeParser;
import org.apache.james.mime4j.field.datetime.parser.TokenMgrError;
import org.apache.james.mime4j.util.ByteSequence;

public class ContentDispositionField extends AbstractField {
    public static final String DISPOSITION_TYPE_ATTACHMENT = "attachment";
    public static final String DISPOSITION_TYPE_INLINE = "inline";
    public static final String PARAM_CREATION_DATE = "creation-date";
    public static final String PARAM_FILENAME = "filename";
    public static final String PARAM_MODIFICATION_DATE = "modification-date";
    public static final String PARAM_READ_DATE = "read-date";
    public static final String PARAM_SIZE = "size";
    static final FieldParser PARSER = new FieldParser() {
        public ParsedField parse(String name, String body, ByteSequence raw) {
            return new ContentDispositionField(name, body, raw);
        }
    };
    private static Log log;
    private Date creationDate;
    private boolean creationDateParsed;
    private String dispositionType = "";
    private Date modificationDate;
    private boolean modificationDateParsed;
    private Map<String, String> parameters = new HashMap();
    private ParseException parseException;
    private boolean parsed = false;
    private Date readDate;
    private boolean readDateParsed;

    static {
        Object[] objArr = {ContentDispositionField.class};
        log = (Log) LogFactory.class.getMethod("getLog", Class.class).invoke(null, objArr);
    }

    ContentDispositionField(String name, String body, ByteSequence raw) {
        super(name, body, raw);
    }

    public ParseException getParseException() {
        if (!this.parsed) {
            ContentDispositionField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        return this.parseException;
    }

    public String getDispositionType() {
        if (!this.parsed) {
            ContentDispositionField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        return this.dispositionType;
    }

    public String getParameter(String name) {
        if (!this.parsed) {
            ContentDispositionField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        Class[] clsArr = {Object.class};
        return (String) Map.class.getMethod("get", clsArr).invoke(this.parameters, (String) String.class.getMethod("toLowerCase", new Class[0]).invoke(name, new Object[0]));
    }

    public Map<String, String> getParameters() {
        if (!this.parsed) {
            ContentDispositionField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        Class[] clsArr = {Map.class};
        return (Map) Collections.class.getMethod("unmodifiableMap", clsArr).invoke(null, this.parameters);
    }

    public boolean isDispositionType(String dispositionType2) {
        if (!this.parsed) {
            ContentDispositionField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        return ((Boolean) String.class.getMethod("equalsIgnoreCase", String.class).invoke(this.dispositionType, dispositionType2)).booleanValue();
    }

    public boolean isInline() {
        if (!this.parsed) {
            ContentDispositionField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        Class[] clsArr = {Object.class};
        return ((Boolean) String.class.getMethod("equals", clsArr).invoke(this.dispositionType, DISPOSITION_TYPE_INLINE)).booleanValue();
    }

    public boolean isAttachment() {
        if (!this.parsed) {
            ContentDispositionField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        Class[] clsArr = {Object.class};
        return ((Boolean) String.class.getMethod("equals", clsArr).invoke(this.dispositionType, DISPOSITION_TYPE_ATTACHMENT)).booleanValue();
    }

    public String getFilename() {
        return (String) ContentDispositionField.class.getMethod("getParameter", String.class).invoke(this, "filename");
    }

    public Date getCreationDate() {
        if (!this.creationDateParsed) {
            Object[] objArr = {"creation-date"};
            this.creationDate = (Date) ContentDispositionField.class.getMethod("parseDate", String.class).invoke(this, objArr);
            this.creationDateParsed = true;
        }
        return this.creationDate;
    }

    public Date getModificationDate() {
        if (!this.modificationDateParsed) {
            Object[] objArr = {"modification-date"};
            this.modificationDate = (Date) ContentDispositionField.class.getMethod("parseDate", String.class).invoke(this, objArr);
            this.modificationDateParsed = true;
        }
        return this.modificationDate;
    }

    public Date getReadDate() {
        if (!this.readDateParsed) {
            Object[] objArr = {"read-date"};
            this.readDate = (Date) ContentDispositionField.class.getMethod("parseDate", String.class).invoke(this, objArr);
            this.readDateParsed = true;
        }
        return this.readDate;
    }

    public long getSize() {
        String value = (String) ContentDispositionField.class.getMethod("getParameter", String.class).invoke(this, "size");
        if (value == null) {
            return -1;
        }
        try {
            long size = ((Long) Long.class.getMethod("parseLong", String.class).invoke(null, value)).longValue();
            if (size < 0) {
                size = -1;
            }
            return size;
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    private Date parseDate(String paramName) {
        Object[] objArr = {paramName};
        String value = (String) ContentDispositionField.class.getMethod("getParameter", String.class).invoke(this, objArr);
        if (value == null) {
            if (!((Boolean) Log.class.getMethod("isDebugEnabled", new Class[0]).invoke(log, new Object[0])).booleanValue()) {
                return null;
            }
            Log log2 = log;
            Object[] objArr2 = {"Parsing "};
            Object[] objArr3 = {paramName};
            Method method = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr4 = {" null"};
            Method method2 = StringBuilder.class.getMethod("append", String.class);
            Method method3 = StringBuilder.class.getMethod("toString", new Class[0]);
            Object[] objArr5 = {(String) method3.invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr2), objArr3), objArr4), new Object[0])};
            Log.class.getMethod("debug", Object.class).invoke(log2, objArr5);
            return null;
        }
        try {
            Method method4 = DateTimeParser.class.getMethod("parseAll", new Class[0]);
            return (Date) DateTime.class.getMethod("getDate", new Class[0]).invoke((DateTime) method4.invoke(new DateTimeParser(new StringReader(value)), new Object[0]), new Object[0]);
        } catch (ParseException e) {
            if (!((Boolean) Log.class.getMethod("isDebugEnabled", new Class[0]).invoke(log, new Object[0])).booleanValue()) {
                return null;
            }
            Log log3 = log;
            Object[] objArr6 = {"Parsing "};
            Object[] objArr7 = {paramName};
            Method method5 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr8 = {" '"};
            Method method6 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr9 = {value};
            Method method7 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr10 = {"': "};
            Method method8 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr11 = {(String) ParseException.class.getMethod("getMessage", new Class[0]).invoke(e, new Object[0])};
            Method method9 = StringBuilder.class.getMethod("append", String.class);
            Method method10 = StringBuilder.class.getMethod("toString", new Class[0]);
            Object[] objArr12 = {(String) method10.invoke((StringBuilder) method9.invoke((StringBuilder) method8.invoke((StringBuilder) method7.invoke((StringBuilder) method6.invoke((StringBuilder) method5.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr6), objArr7), objArr8), objArr9), objArr10), objArr11), new Object[0])};
            Log.class.getMethod("debug", Object.class).invoke(log3, objArr12);
            return null;
        } catch (TokenMgrError e2) {
            if (!((Boolean) Log.class.getMethod("isDebugEnabled", new Class[0]).invoke(log, new Object[0])).booleanValue()) {
                return null;
            }
            Log log4 = log;
            Object[] objArr13 = {"Parsing "};
            Object[] objArr14 = {paramName};
            Method method11 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr15 = {" '"};
            Method method12 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr16 = {value};
            Method method13 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr17 = {"': "};
            Method method14 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr18 = {(String) TokenMgrError.class.getMethod("getMessage", new Class[0]).invoke(e2, new Object[0])};
            Method method15 = StringBuilder.class.getMethod("append", String.class);
            Method method16 = StringBuilder.class.getMethod("toString", new Class[0]);
            Object[] objArr19 = {(String) method16.invoke((StringBuilder) method15.invoke((StringBuilder) method14.invoke((StringBuilder) method13.invoke((StringBuilder) method12.invoke((StringBuilder) method11.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr13), objArr14), objArr15), objArr16), objArr17), objArr18), new Object[0])};
            Log.class.getMethod("debug", Object.class).invoke(log4, objArr19);
            return null;
        }
    }

    private void parse() {
        String body = getBody();
        ContentDispositionParser parser = new ContentDispositionParser(new StringReader(body));
        try {
            parser.parseAll();
        } catch (ParseException e) {
            if (log.isDebugEnabled()) {
                log.debug("Parsing value '" + body + "': " + e.getMessage());
            }
            this.parseException = e;
        } catch (org.apache.james.mime4j.field.contentdisposition.parser.TokenMgrError e2) {
            if (log.isDebugEnabled()) {
                log.debug("Parsing value '" + body + "': " + e2.getMessage());
            }
            this.parseException = new ParseException(e2.getMessage());
        }
        String dispositionType2 = parser.getDispositionType();
        if (dispositionType2 != null) {
            this.dispositionType = dispositionType2.toLowerCase(Locale.US);
            List<String> paramNames = parser.getParamNames();
            List<String> paramValues = parser.getParamValues();
            if (!(paramNames == null || paramValues == null)) {
                int len = Math.min(paramNames.size(), paramValues.size());
                for (int i = 0; i < len; i++) {
                    this.parameters.put(paramNames.get(i).toLowerCase(Locale.US), paramValues.get(i));
                }
            }
        }
        this.parsed = true;
    }
}
