package org.apache.james.mime4j.field;

import org.apache.james.mime4j.codec.DecoderUtil;
import org.apache.james.mime4j.util.ByteSequence;

public class UnstructuredField extends AbstractField {
    static final FieldParser PARSER = new FieldParser() {
        public ParsedField parse(String name, String body, ByteSequence raw) {
            return new UnstructuredField(name, body, raw);
        }
    };
    private boolean parsed = false;
    private String value;

    UnstructuredField(String name, String body, ByteSequence raw) {
        super(name, body, raw);
    }

    public String getValue() {
        if (!this.parsed) {
            UnstructuredField.class.getMethod("parse", new Class[0]).invoke(this, new Object[0]);
        }
        return this.value;
    }

    private void parse() {
        Object[] objArr = {(String) UnstructuredField.class.getMethod("getBody", new Class[0]).invoke(this, new Object[0])};
        this.value = (String) DecoderUtil.class.getMethod("decodeEncodedWords", String.class).invoke(null, objArr);
        this.parsed = true;
    }
}
