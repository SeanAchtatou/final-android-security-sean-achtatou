package org.apache.james.mime4j.parser;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.descriptor.BodyDescriptor;

public class MimeStreamParser {
    private boolean contentDecoding;
    private ContentHandler handler;
    private final MimeTokenStream mimeTokenStream;

    public MimeStreamParser(MimeEntityConfig config) {
        MimeEntityConfig localConfig;
        this.handler = null;
        if (config != null) {
            localConfig = (MimeEntityConfig) MimeEntityConfig.class.getMethod("clone", new Class[0]).invoke(config, new Object[0]);
        } else {
            localConfig = new MimeEntityConfig();
        }
        this.mimeTokenStream = new MimeTokenStream(localConfig);
        this.contentDecoding = false;
    }

    public MimeStreamParser() {
        this(null);
    }

    public boolean isContentDecoding() {
        return this.contentDecoding;
    }

    public void setContentDecoding(boolean b) {
        this.contentDecoding = b;
    }

    public void parse(InputStream is) throws MimeException, IOException {
        InputStream bodyContent;
        Object[] objArr = {is};
        MimeTokenStream.class.getMethod("parse", InputStream.class).invoke(this.mimeTokenStream, objArr);
        while (true) {
            int state = ((Integer) MimeTokenStream.class.getMethod("getState", new Class[0]).invoke(this.mimeTokenStream, new Object[0])).intValue();
            switch (state) {
                case -1:
                    return;
                case 0:
                    ContentHandler.class.getMethod("startMessage", new Class[0]).invoke(this.handler, new Object[0]);
                    break;
                case 1:
                    ContentHandler.class.getMethod("endMessage", new Class[0]).invoke(this.handler, new Object[0]);
                    break;
                case 2:
                    ContentHandler contentHandler = this.handler;
                    Object[] objArr2 = {(InputStream) MimeTokenStream.class.getMethod("getInputStream", new Class[0]).invoke(this.mimeTokenStream, new Object[0])};
                    ContentHandler.class.getMethod("raw", InputStream.class).invoke(contentHandler, objArr2);
                    break;
                case 3:
                    ContentHandler.class.getMethod("startHeader", new Class[0]).invoke(this.handler, new Object[0]);
                    break;
                case 4:
                    ContentHandler contentHandler2 = this.handler;
                    Object[] objArr3 = {(Field) MimeTokenStream.class.getMethod("getField", new Class[0]).invoke(this.mimeTokenStream, new Object[0])};
                    ContentHandler.class.getMethod("field", Field.class).invoke(contentHandler2, objArr3);
                    break;
                case 5:
                    ContentHandler.class.getMethod("endHeader", new Class[0]).invoke(this.handler, new Object[0]);
                    break;
                case 6:
                    ContentHandler contentHandler3 = this.handler;
                    Object[] objArr4 = {(BodyDescriptor) MimeTokenStream.class.getMethod("getBodyDescriptor", new Class[0]).invoke(this.mimeTokenStream, new Object[0])};
                    ContentHandler.class.getMethod("startMultipart", BodyDescriptor.class).invoke(contentHandler3, objArr4);
                    break;
                case 7:
                    ContentHandler.class.getMethod("endMultipart", new Class[0]).invoke(this.handler, new Object[0]);
                    break;
                case 8:
                    ContentHandler contentHandler4 = this.handler;
                    Object[] objArr5 = {(InputStream) MimeTokenStream.class.getMethod("getInputStream", new Class[0]).invoke(this.mimeTokenStream, new Object[0])};
                    ContentHandler.class.getMethod("preamble", InputStream.class).invoke(contentHandler4, objArr5);
                    break;
                case 9:
                    ContentHandler contentHandler5 = this.handler;
                    Object[] objArr6 = {(InputStream) MimeTokenStream.class.getMethod("getInputStream", new Class[0]).invoke(this.mimeTokenStream, new Object[0])};
                    ContentHandler.class.getMethod("epilogue", InputStream.class).invoke(contentHandler5, objArr6);
                    break;
                case 10:
                    ContentHandler.class.getMethod("startBodyPart", new Class[0]).invoke(this.handler, new Object[0]);
                    break;
                case 11:
                    ContentHandler.class.getMethod("endBodyPart", new Class[0]).invoke(this.handler, new Object[0]);
                    break;
                case 12:
                    BodyDescriptor desc = (BodyDescriptor) MimeTokenStream.class.getMethod("getBodyDescriptor", new Class[0]).invoke(this.mimeTokenStream, new Object[0]);
                    if (this.contentDecoding) {
                        bodyContent = (InputStream) MimeTokenStream.class.getMethod("getDecodedInputStream", new Class[0]).invoke(this.mimeTokenStream, new Object[0]);
                    } else {
                        bodyContent = (InputStream) MimeTokenStream.class.getMethod("getInputStream", new Class[0]).invoke(this.mimeTokenStream, new Object[0]);
                    }
                    Object[] objArr7 = {desc, bodyContent};
                    ContentHandler.class.getMethod("body", BodyDescriptor.class, InputStream.class).invoke(this.handler, objArr7);
                    break;
                default:
                    StringBuilder sb = new StringBuilder();
                    Class[] clsArr = {String.class};
                    Object[] objArr8 = {"Invalid state: "};
                    Class[] clsArr2 = {Integer.TYPE};
                    Object[] objArr9 = {new Integer(state)};
                    Method method = StringBuilder.class.getMethod("append", clsArr2);
                    throw new IllegalStateException((String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr8), objArr9), new Object[0]));
            }
            ((Integer) MimeTokenStream.class.getMethod("next", new Class[0]).invoke(this.mimeTokenStream, new Object[0])).intValue();
        }
    }

    public boolean isRaw() {
        return ((Boolean) MimeTokenStream.class.getMethod("isRaw", new Class[0]).invoke(this.mimeTokenStream, new Object[0])).booleanValue();
    }

    public void setRaw(boolean raw) {
        MimeTokenStream mimeTokenStream2 = this.mimeTokenStream;
        Class[] clsArr = {Integer.TYPE};
        MimeTokenStream.class.getMethod("setRecursionMode", clsArr).invoke(mimeTokenStream2, new Integer(2));
    }

    public void stop() {
        MimeTokenStream.class.getMethod("stop", new Class[0]).invoke(this.mimeTokenStream, new Object[0]);
    }

    public void setContentHandler(ContentHandler h) {
        this.handler = h;
    }
}
