package org.apache.james.mime4j.message;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Stack;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.codec.Base64InputStream;
import org.apache.james.mime4j.codec.QuotedPrintableInputStream;
import org.apache.james.mime4j.descriptor.BodyDescriptor;
import org.apache.james.mime4j.field.AbstractField;
import org.apache.james.mime4j.field.ParsedField;
import org.apache.james.mime4j.parser.ContentHandler;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.storage.StorageProvider;
import org.apache.james.mime4j.util.ByteArrayBuffer;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.MimeUtil;

public class MessageBuilder implements ContentHandler {
    private final BodyFactory bodyFactory;
    private final Entity entity;
    private Stack<Object> stack = new Stack<>();

    public MessageBuilder(Entity entity2) {
        this.entity = entity2;
        this.bodyFactory = new BodyFactory();
    }

    public MessageBuilder(Entity entity2, StorageProvider storageProvider) {
        this.entity = entity2;
        this.bodyFactory = new BodyFactory(storageProvider);
    }

    private void expect(Class<?> c) {
        if (!c.isInstance(Stack.class.getMethod("peek", new Class[0]).invoke(this.stack, new Object[0]))) {
            StringBuilder sb = new StringBuilder();
            Class[] clsArr = {String.class};
            Object[] objArr = {"Internal stack error: Expected '"};
            Object[] objArr2 = {c.getName()};
            Method method = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr3 = {"' found '"};
            Method method2 = StringBuilder.class.getMethod("append", String.class);
            Method method3 = Stack.class.getMethod("peek", new Class[0]);
            Object[] objArr4 = {((Class) Object.class.getMethod("getClass", new Class[0]).invoke(method3.invoke(this.stack, new Object[0]), new Object[0])).getName()};
            Method method4 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr5 = {"'"};
            Method method5 = StringBuilder.class.getMethod("append", String.class);
            throw new IllegalStateException((String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method5.invoke((StringBuilder) method4.invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr), objArr2), objArr3), objArr4), objArr5), new Object[0]));
        }
    }

    public void startMessage() throws MimeException {
        if (((Boolean) Stack.class.getMethod("isEmpty", new Class[0]).invoke(this.stack, new Object[0])).booleanValue()) {
            Stack<Object> stack2 = this.stack;
            Object[] objArr = {this.entity};
            Stack.class.getMethod("push", Object.class).invoke(stack2, objArr);
            return;
        }
        Object[] objArr2 = {Entity.class};
        MessageBuilder.class.getMethod("expect", Class.class).invoke(this, objArr2);
        Message m = new Message();
        Method method = Stack.class.getMethod("peek", new Class[0]);
        Method method2 = Entity.class.getMethod("setBody", Body.class);
        method2.invoke((Entity) method.invoke(this.stack, new Object[0]), m);
        Object[] objArr3 = {m};
        Stack.class.getMethod("push", Object.class).invoke(this.stack, objArr3);
    }

    public void endMessage() throws MimeException {
        Object[] objArr = {Message.class};
        MessageBuilder.class.getMethod("expect", Class.class).invoke(this, objArr);
        Stack.class.getMethod("pop", new Class[0]).invoke(this.stack, new Object[0]);
    }

    public void startHeader() throws MimeException {
        Stack<Object> stack2 = this.stack;
        Object[] objArr = {new Header()};
        Stack.class.getMethod("push", Object.class).invoke(stack2, objArr);
    }

    public void field(Field field) throws MimeException {
        Object[] objArr = {Header.class};
        MessageBuilder.class.getMethod("expect", Class.class).invoke(this, objArr);
        Object[] objArr2 = {(ByteSequence) Field.class.getMethod("getRaw", new Class[0]).invoke(field, new Object[0])};
        Method method = Stack.class.getMethod("peek", new Class[0]);
        Object[] objArr3 = {(ParsedField) AbstractField.class.getMethod("parse", ByteSequence.class).invoke(null, objArr2)};
        Header.class.getMethod("addField", Field.class).invoke((Header) method.invoke(this.stack, new Object[0]), objArr3);
    }

    public void endHeader() throws MimeException {
        Object[] objArr = {Header.class};
        MessageBuilder.class.getMethod("expect", Class.class).invoke(this, objArr);
        Method method = Stack.class.getMethod("pop", new Class[0]);
        Object[] objArr2 = {Entity.class};
        MessageBuilder.class.getMethod("expect", Class.class).invoke(this, objArr2);
        Method method2 = Stack.class.getMethod("peek", new Class[0]);
        Object[] objArr3 = {(Header) method.invoke(this.stack, new Object[0])};
        Entity.class.getMethod("setHeader", Header.class).invoke((Entity) method2.invoke(this.stack, new Object[0]), objArr3);
    }

    public void startMultipart(BodyDescriptor bd) throws MimeException {
        Object[] objArr = {Entity.class};
        MessageBuilder.class.getMethod("expect", Class.class).invoke(this, objArr);
        Method method = Stack.class.getMethod("peek", new Class[0]);
        Multipart multiPart = new Multipart((String) BodyDescriptor.class.getMethod("getSubType", new Class[0]).invoke(bd, new Object[0]));
        Method method2 = Entity.class.getMethod("setBody", Body.class);
        method2.invoke((Entity) method.invoke(this.stack, new Object[0]), multiPart);
        Object[] objArr2 = {multiPart};
        Stack.class.getMethod("push", Object.class).invoke(this.stack, objArr2);
    }

    public void body(BodyDescriptor bd, InputStream is) throws MimeException, IOException {
        InputStream decodedStream;
        Body body;
        Object[] objArr = {Entity.class};
        MessageBuilder.class.getMethod("expect", Class.class).invoke(this, objArr);
        String enc = (String) BodyDescriptor.class.getMethod("getTransferEncoding", new Class[0]).invoke(bd, new Object[0]);
        if (((Boolean) String.class.getMethod("equals", Object.class).invoke(MimeUtil.ENC_BASE64, enc)).booleanValue()) {
            decodedStream = new Base64InputStream(is);
        } else {
            if (((Boolean) String.class.getMethod("equals", Object.class).invoke(MimeUtil.ENC_QUOTED_PRINTABLE, enc)).booleanValue()) {
                decodedStream = new QuotedPrintableInputStream(is);
            } else {
                decodedStream = is;
            }
        }
        Method method = BodyDescriptor.class.getMethod("getMimeType", new Class[0]);
        Object[] objArr2 = {"text/"};
        if (((Boolean) String.class.getMethod("startsWith", String.class).invoke((String) method.invoke(bd, new Object[0]), objArr2)).booleanValue()) {
            BodyFactory bodyFactory2 = this.bodyFactory;
            Object[] objArr3 = {decodedStream, (String) BodyDescriptor.class.getMethod("getCharset", new Class[0]).invoke(bd, new Object[0])};
            body = (TextBody) BodyFactory.class.getMethod("textBody", InputStream.class, String.class).invoke(bodyFactory2, objArr3);
        } else {
            Object[] objArr4 = {decodedStream};
            body = (BinaryBody) BodyFactory.class.getMethod("binaryBody", InputStream.class).invoke(this.bodyFactory, objArr4);
        }
        Method method2 = Stack.class.getMethod("peek", new Class[0]);
        Method method3 = Entity.class.getMethod("setBody", Body.class);
        method3.invoke((Entity) method2.invoke(this.stack, new Object[0]), body);
    }

    public void endMultipart() throws MimeException {
        Stack.class.getMethod("pop", new Class[0]).invoke(this.stack, new Object[0]);
    }

    public void startBodyPart() throws MimeException {
        Object[] objArr = {Multipart.class};
        MessageBuilder.class.getMethod("expect", Class.class).invoke(this, objArr);
        BodyPart bodyPart = new BodyPart();
        Method method = Stack.class.getMethod("peek", new Class[0]);
        Method method2 = Multipart.class.getMethod("addBodyPart", BodyPart.class);
        method2.invoke((Multipart) method.invoke(this.stack, new Object[0]), bodyPart);
        Object[] objArr2 = {bodyPart};
        Stack.class.getMethod("push", Object.class).invoke(this.stack, objArr2);
    }

    public void endBodyPart() throws MimeException {
        Object[] objArr = {BodyPart.class};
        MessageBuilder.class.getMethod("expect", Class.class).invoke(this, objArr);
        Stack.class.getMethod("pop", new Class[0]).invoke(this.stack, new Object[0]);
    }

    public void epilogue(InputStream is) throws MimeException, IOException {
        Object[] objArr = {Multipart.class};
        MessageBuilder.class.getMethod("expect", Class.class).invoke(this, objArr);
        Object[] objArr2 = {is};
        Method method = Stack.class.getMethod("peek", new Class[0]);
        Object[] objArr3 = {(ByteSequence) MessageBuilder.class.getMethod("loadStream", InputStream.class).invoke(null, objArr2)};
        Multipart.class.getMethod("setEpilogueRaw", ByteSequence.class).invoke((Multipart) method.invoke(this.stack, new Object[0]), objArr3);
    }

    public void preamble(InputStream is) throws MimeException, IOException {
        Object[] objArr = {Multipart.class};
        MessageBuilder.class.getMethod("expect", Class.class).invoke(this, objArr);
        Object[] objArr2 = {is};
        Method method = Stack.class.getMethod("peek", new Class[0]);
        Object[] objArr3 = {(ByteSequence) MessageBuilder.class.getMethod("loadStream", InputStream.class).invoke(null, objArr2)};
        Multipart.class.getMethod("setPreambleRaw", ByteSequence.class).invoke((Multipart) method.invoke(this.stack, new Object[0]), objArr3);
    }

    public void raw(InputStream is) throws MimeException, IOException {
        throw new UnsupportedOperationException("Not supported");
    }

    private static ByteSequence loadStream(InputStream in) throws IOException {
        ByteArrayBuffer bab = new ByteArrayBuffer(64);
        while (true) {
            int b = ((Integer) InputStream.class.getMethod("read", new Class[0]).invoke(in, new Object[0])).intValue();
            if (b == -1) {
                return bab;
            }
            Class[] clsArr = {Integer.TYPE};
            ByteArrayBuffer.class.getMethod("append", clsArr).invoke(bab, new Integer(b));
        }
    }
}
