package org.apache.james.mime4j.codec;

import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.BitSet;
import java.util.Locale;
import org.apache.james.mime4j.util.CharsetUtil;

public class EncoderUtil {
    private static final BitSet ATEXT_CHARS;
    private static final char BASE64_PAD = '=';
    private static final byte[] BASE64_TABLE = Base64OutputStream.BASE64_TABLE;
    private static final int ENCODED_WORD_MAX_LENGTH = 75;
    private static final String ENC_WORD_PREFIX = "=?";
    private static final String ENC_WORD_SUFFIX = "?=";
    private static final int MAX_USED_CHARACTERS = 50;
    private static final BitSet Q_REGULAR_CHARS;
    private static final BitSet Q_RESTRICTED_CHARS;
    private static final BitSet TOKEN_CHARS;

    public enum Encoding {
        B,
        Q
    }

    public enum Usage {
        TEXT_TOKEN,
        WORD_ENTITY
    }

    static {
        Object[] objArr = {"=_?"};
        Q_REGULAR_CHARS = (BitSet) EncoderUtil.class.getMethod("initChars", String.class).invoke(null, objArr);
        Object[] objArr2 = {"=_?\"#$%&'(),.:;<>@[\\]^`{|}~"};
        Q_RESTRICTED_CHARS = (BitSet) EncoderUtil.class.getMethod("initChars", String.class).invoke(null, objArr2);
        Object[] objArr3 = {"()<>@,;:\\\"/[]?="};
        TOKEN_CHARS = (BitSet) EncoderUtil.class.getMethod("initChars", String.class).invoke(null, objArr3);
        Object[] objArr4 = {"()<>@.,;:\\\"[]"};
        ATEXT_CHARS = (BitSet) EncoderUtil.class.getMethod("initChars", String.class).invoke(null, objArr4);
    }

    private static BitSet initChars(String specials) {
        BitSet bs = new BitSet(128);
        for (char ch = '!'; ch < 127; ch = (char) (ch + 1)) {
            Class[] clsArr = {Integer.TYPE};
            if (((Integer) String.class.getMethod("indexOf", clsArr).invoke(specials, new Integer(ch))).intValue() == -1) {
                Class[] clsArr2 = {Integer.TYPE};
                BitSet.class.getMethod("set", clsArr2).invoke(bs, new Integer(ch));
            }
        }
        return bs;
    }

    private EncoderUtil() {
    }

    public static String encodeAddressDisplayName(String displayName) {
        if (((Boolean) EncoderUtil.class.getMethod("isAtomPhrase", String.class).invoke(null, displayName)).booleanValue()) {
            return displayName;
        }
        Class[] clsArr = {String.class, Integer.TYPE};
        if (((Boolean) EncoderUtil.class.getMethod("hasToBeEncoded", clsArr).invoke(null, displayName, new Integer(0))).booleanValue()) {
            Class[] clsArr2 = {String.class, Usage.class};
            return (String) EncoderUtil.class.getMethod("encodeEncodedWord", clsArr2).invoke(null, displayName, Usage.WORD_ENTITY);
        }
        return (String) EncoderUtil.class.getMethod("quote", String.class).invoke(null, displayName);
    }

    public static String encodeAddressLocalPart(String localPart) {
        if (((Boolean) EncoderUtil.class.getMethod("isDotAtomText", String.class).invoke(null, localPart)).booleanValue()) {
            return localPart;
        }
        return (String) EncoderUtil.class.getMethod("quote", String.class).invoke(null, localPart);
    }

    public static String encodeHeaderParameter(String name, String value) {
        Object[] objArr = {Locale.US};
        String name2 = (String) String.class.getMethod("toLowerCase", Locale.class).invoke(name, objArr);
        if (((Boolean) EncoderUtil.class.getMethod("isToken", String.class).invoke(null, value)).booleanValue()) {
            StringBuilder sb = new StringBuilder();
            Class[] clsArr = {String.class};
            Object[] objArr2 = {name2};
            Object[] objArr3 = {"="};
            Method method = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr4 = {value};
            Method method2 = StringBuilder.class.getMethod("append", String.class);
            return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr2), objArr3), objArr4), new Object[0]);
        }
        StringBuilder sb2 = new StringBuilder();
        Class[] clsArr2 = {String.class};
        Object[] objArr5 = {name2};
        Object[] objArr6 = {"="};
        Method method3 = StringBuilder.class.getMethod("append", String.class);
        Object[] objArr7 = {value};
        Object[] objArr8 = {(String) EncoderUtil.class.getMethod("quote", String.class).invoke(null, objArr7)};
        Method method4 = StringBuilder.class.getMethod("append", String.class);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method4.invoke((StringBuilder) method3.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr2).invoke(sb2, objArr5), objArr6), objArr8), new Object[0]);
    }

    public static String encodeIfNecessary(String text, Usage usage, int usedCharacters) {
        Class[] clsArr = {String.class, Integer.TYPE};
        if (!((Boolean) EncoderUtil.class.getMethod("hasToBeEncoded", clsArr).invoke(null, text, new Integer(usedCharacters))).booleanValue()) {
            return text;
        }
        Class[] clsArr2 = {String.class, Usage.class, Integer.TYPE};
        return (String) EncoderUtil.class.getMethod("encodeEncodedWord", clsArr2).invoke(null, text, usage, new Integer(usedCharacters));
    }

    public static boolean hasToBeEncoded(String text, int usedCharacters) {
        if (text == null) {
            throw new IllegalArgumentException();
        } else if (usedCharacters < 0 || usedCharacters > MAX_USED_CHARACTERS) {
            throw new IllegalArgumentException();
        } else {
            int nonWhiteSpaceCount = usedCharacters;
            int idx = 0;
            while (true) {
                if (idx >= ((Integer) String.class.getMethod("length", new Class[0]).invoke(text, new Object[0])).intValue()) {
                    return false;
                }
                Class[] clsArr = {Integer.TYPE};
                char ch = ((Character) String.class.getMethod("charAt", clsArr).invoke(text, new Integer(idx))).charValue();
                if (ch == 9 || ch == ' ') {
                    nonWhiteSpaceCount = 0;
                } else {
                    nonWhiteSpaceCount++;
                    if (nonWhiteSpaceCount > 77 || ch < ' ' || ch >= 127) {
                        return true;
                    }
                }
                idx++;
            }
        }
    }

    public static String encodeEncodedWord(String text, Usage usage) {
        Class[] clsArr = {String.class, Usage.class, Integer.TYPE, Charset.class, Encoding.class};
        return (String) EncoderUtil.class.getMethod("encodeEncodedWord", clsArr).invoke(null, text, usage, new Integer(0), null, null);
    }

    public static String encodeEncodedWord(String text, Usage usage, int usedCharacters) {
        Class[] clsArr = {String.class, Usage.class, Integer.TYPE, Charset.class, Encoding.class};
        return (String) EncoderUtil.class.getMethod("encodeEncodedWord", clsArr).invoke(null, text, usage, new Integer(usedCharacters), null, null);
    }

    public static String encodeEncodedWord(String text, Usage usage, int usedCharacters, Charset charset, Encoding encoding) {
        if (text == null) {
            throw new IllegalArgumentException();
        } else if (usedCharacters < 0 || usedCharacters > MAX_USED_CHARACTERS) {
            throw new IllegalArgumentException();
        } else {
            if (charset == null) {
                Object[] objArr = {text};
                charset = (Charset) EncoderUtil.class.getMethod("determineCharset", String.class).invoke(null, objArr);
            }
            Object[] objArr2 = {(String) Charset.class.getMethod("name", new Class[0]).invoke(charset, new Object[0])};
            String mimeCharset = (String) CharsetUtil.class.getMethod("toMimeCharset", String.class).invoke(null, objArr2);
            if (mimeCharset == null) {
                throw new IllegalArgumentException("Unsupported charset");
            }
            Object[] objArr3 = {text, charset};
            byte[] bytes = (byte[]) EncoderUtil.class.getMethod("encode", String.class, Charset.class).invoke(null, objArr3);
            if (encoding == null) {
                Object[] objArr4 = {bytes, usage};
                encoding = (Encoding) EncoderUtil.class.getMethod("determineEncoding", byte[].class, Usage.class).invoke(null, objArr4);
            }
            if (encoding == Encoding.B) {
                Object[] objArr5 = {ENC_WORD_PREFIX};
                Object[] objArr6 = {mimeCharset};
                Method method = StringBuilder.class.getMethod("append", String.class);
                Object[] objArr7 = {"?B?"};
                Method method2 = StringBuilder.class.getMethod("append", String.class);
                Method method3 = StringBuilder.class.getMethod("toString", new Class[0]);
                Class[] clsArr = {String.class, String.class, Integer.TYPE, Charset.class, byte[].class};
                return (String) EncoderUtil.class.getMethod("encodeB", clsArr).invoke(null, (String) method3.invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr5), objArr6), objArr7), new Object[0]), text, new Integer(usedCharacters), charset, bytes);
            }
            Object[] objArr8 = {ENC_WORD_PREFIX};
            Object[] objArr9 = {mimeCharset};
            Method method4 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr10 = {"?Q?"};
            Method method5 = StringBuilder.class.getMethod("append", String.class);
            Method method6 = StringBuilder.class.getMethod("toString", new Class[0]);
            Class[] clsArr2 = {String.class, String.class, Usage.class, Integer.TYPE, Charset.class, byte[].class};
            return (String) EncoderUtil.class.getMethod("encodeQ", clsArr2).invoke(null, (String) method6.invoke((StringBuilder) method5.invoke((StringBuilder) method4.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr8), objArr9), objArr10), new Object[0]), text, usage, new Integer(usedCharacters), charset, bytes);
        }
    }

    public static String encodeB(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        int idx = 0;
        int end = bytes.length;
        while (idx < end - 2) {
            int data = ((bytes[idx] & 255) << 16) | ((bytes[idx + 1] & 255) << 8) | (bytes[idx + 2] & 255);
            Class[] clsArr = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr).invoke(sb, new Character((char) BASE64_TABLE[(data >> 18) & 63]));
            Class[] clsArr2 = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr2).invoke(sb, new Character((char) BASE64_TABLE[(data >> 12) & 63]));
            Class[] clsArr3 = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr3).invoke(sb, new Character((char) BASE64_TABLE[(data >> 6) & 63]));
            Class[] clsArr4 = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr4).invoke(sb, new Character((char) BASE64_TABLE[data & 63]));
            idx += 3;
        }
        if (idx == end - 2) {
            int data2 = ((bytes[idx] & 255) << 16) | ((bytes[idx + 1] & 255) << 8);
            Class[] clsArr5 = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr5).invoke(sb, new Character((char) BASE64_TABLE[(data2 >> 18) & 63]));
            Class[] clsArr6 = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr6).invoke(sb, new Character((char) BASE64_TABLE[(data2 >> 12) & 63]));
            Class[] clsArr7 = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr7).invoke(sb, new Character((char) BASE64_TABLE[(data2 >> 6) & 63]));
            Class[] clsArr8 = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr8).invoke(sb, new Character(BASE64_PAD));
        } else if (idx == end - 1) {
            int data3 = (bytes[idx] & 255) << 16;
            Class[] clsArr9 = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr9).invoke(sb, new Character((char) BASE64_TABLE[(data3 >> 18) & 63]));
            Class[] clsArr10 = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr10).invoke(sb, new Character((char) BASE64_TABLE[(data3 >> 12) & 63]));
            Class[] clsArr11 = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr11).invoke(sb, new Character(BASE64_PAD));
            Class[] clsArr12 = {Character.TYPE};
            StringBuilder.class.getMethod("append", clsArr12).invoke(sb, new Character(BASE64_PAD));
        }
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb, new Object[0]);
    }

    public static String encodeQ(byte[] bytes, Usage usage) {
        BitSet qChars = usage == Usage.TEXT_TOKEN ? Q_REGULAR_CHARS : Q_RESTRICTED_CHARS;
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            int v = b & 255;
            if (v == 32) {
                StringBuilder.class.getMethod("append", Character.TYPE).invoke(sb, new Character('_'));
            } else {
                if (!((Boolean) BitSet.class.getMethod("get", Integer.TYPE).invoke(qChars, new Integer(v))).booleanValue()) {
                    StringBuilder.class.getMethod("append", Character.TYPE).invoke(sb, new Character(BASE64_PAD));
                    StringBuilder.class.getMethod("append", Character.TYPE).invoke(sb, new Character(((Character) EncoderUtil.class.getMethod("hexDigit", Integer.TYPE).invoke(null, new Integer(v >>> 4))).charValue()));
                    StringBuilder.class.getMethod("append", Character.TYPE).invoke(sb, new Character(((Character) EncoderUtil.class.getMethod("hexDigit", Integer.TYPE).invoke(null, new Integer(v & 15))).charValue()));
                } else {
                    StringBuilder.class.getMethod("append", Character.TYPE).invoke(sb, new Character((char) v));
                }
            }
        }
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb, new Object[0]);
    }

    public static boolean isToken(String str) {
        int length = ((Integer) String.class.getMethod("length", new Class[0]).invoke(str, new Object[0])).intValue();
        if (length == 0) {
            return false;
        }
        for (int idx = 0; idx < length; idx++) {
            Class[] clsArr = {Integer.TYPE};
            char ch = ((Character) String.class.getMethod("charAt", clsArr).invoke(str, new Integer(idx))).charValue();
            BitSet bitSet = TOKEN_CHARS;
            Class[] clsArr2 = {Integer.TYPE};
            if (!((Boolean) BitSet.class.getMethod("get", clsArr2).invoke(bitSet, new Integer(ch))).booleanValue()) {
                return false;
            }
        }
        return true;
    }

    private static boolean isAtomPhrase(String str) {
        boolean containsAText = false;
        int length = ((Integer) String.class.getMethod("length", new Class[0]).invoke(str, new Object[0])).intValue();
        for (int idx = 0; idx < length; idx++) {
            Class[] clsArr = {Integer.TYPE};
            char ch = ((Character) String.class.getMethod("charAt", clsArr).invoke(str, new Integer(idx))).charValue();
            BitSet bitSet = ATEXT_CHARS;
            Class[] clsArr2 = {Integer.TYPE};
            if (((Boolean) BitSet.class.getMethod("get", clsArr2).invoke(bitSet, new Integer(ch))).booleanValue()) {
                containsAText = true;
            } else {
                Class[] clsArr3 = {Character.TYPE};
                if (!((Boolean) CharsetUtil.class.getMethod("isWhitespace", clsArr3).invoke(null, new Character(ch))).booleanValue()) {
                    return false;
                }
            }
        }
        return containsAText;
    }

    private static boolean isDotAtomText(String str) {
        char prev = '.';
        int length = ((Integer) String.class.getMethod("length", new Class[0]).invoke(str, new Object[0])).intValue();
        if (length == 0) {
            return false;
        }
        for (int idx = 0; idx < length; idx++) {
            Class[] clsArr = {Integer.TYPE};
            char ch = ((Character) String.class.getMethod("charAt", clsArr).invoke(str, new Integer(idx))).charValue();
            if (ch != '.') {
                BitSet bitSet = ATEXT_CHARS;
                Class[] clsArr2 = {Integer.TYPE};
                if (!((Boolean) BitSet.class.getMethod("get", clsArr2).invoke(bitSet, new Integer(ch))).booleanValue()) {
                    return false;
                }
            } else if (prev == '.' || idx == length - 1) {
                return false;
            }
            prev = ch;
        }
        return true;
    }

    private static String quote(String str) {
        Object[] objArr = {"[\\\\\"]", "\\\\$0"};
        StringBuilder sb = new StringBuilder();
        Class[] clsArr = {String.class};
        Object[] objArr2 = {"\""};
        Object[] objArr3 = {(String) String.class.getMethod("replaceAll", String.class, String.class).invoke(str, objArr)};
        Method method = StringBuilder.class.getMethod("append", String.class);
        Object[] objArr4 = {"\""};
        Method method2 = StringBuilder.class.getMethod("append", String.class);
        return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr2), objArr3), objArr4), new Object[0]);
    }

    private static String encodeB(String prefix, String text, int usedCharacters, Charset charset, byte[] bytes) {
        if (prefix.length() + bEncodedLength(bytes) + ENC_WORD_SUFFIX.length() <= 75 - usedCharacters) {
            return prefix + encodeB(bytes) + ENC_WORD_SUFFIX;
        }
        String part1 = text.substring(0, text.length() / 2);
        String word1 = encodeB(prefix, part1, usedCharacters, charset, encode(part1, charset));
        String part2 = text.substring(text.length() / 2);
        return word1 + " " + encodeB(prefix, part2, 0, charset, encode(part2, charset));
    }

    private static int bEncodedLength(byte[] bytes) {
        return ((bytes.length + 2) / 3) * 4;
    }

    private static String encodeQ(String prefix, String text, Usage usage, int usedCharacters, Charset charset, byte[] bytes) {
        if (prefix.length() + qEncodedLength(bytes, usage) + ENC_WORD_SUFFIX.length() <= 75 - usedCharacters) {
            return prefix + encodeQ(bytes, usage) + ENC_WORD_SUFFIX;
        }
        String part1 = text.substring(0, text.length() / 2);
        String word1 = encodeQ(prefix, part1, usage, usedCharacters, charset, encode(part1, charset));
        String part2 = text.substring(text.length() / 2);
        return word1 + " " + encodeQ(prefix, part2, usage, 0, charset, encode(part2, charset));
    }

    private static int qEncodedLength(byte[] bytes, Usage usage) {
        BitSet qChars = usage == Usage.TEXT_TOKEN ? Q_REGULAR_CHARS : Q_RESTRICTED_CHARS;
        int count = 0;
        for (byte b : bytes) {
            int v = b & 255;
            if (v == 32) {
                count++;
            } else {
                if (!((Boolean) BitSet.class.getMethod("get", Integer.TYPE).invoke(qChars, new Integer(v))).booleanValue()) {
                    count += 3;
                } else {
                    count++;
                }
            }
        }
        return count;
    }

    private static byte[] encode(String text, Charset charset) {
        Object[] objArr = {text};
        ByteBuffer buffer = (ByteBuffer) Charset.class.getMethod("encode", String.class).invoke(charset, objArr);
        byte[] bytes = new byte[((Integer) ByteBuffer.class.getMethod("limit", new Class[0]).invoke(buffer, new Object[0])).intValue()];
        Object[] objArr2 = {bytes};
        ByteBuffer.class.getMethod("get", byte[].class).invoke(buffer, objArr2);
        return bytes;
    }

    private static Charset determineCharset(String text) {
        boolean ascii = true;
        int len = ((Integer) String.class.getMethod("length", new Class[0]).invoke(text, new Object[0])).intValue();
        for (int index = 0; index < len; index++) {
            char ch = ((Character) String.class.getMethod("charAt", Integer.TYPE).invoke(text, new Integer(index))).charValue();
            if (ch > 255) {
                return CharsetUtil.UTF_8;
            }
            if (ch > 127) {
                ascii = false;
            }
        }
        return ascii ? CharsetUtil.US_ASCII : CharsetUtil.ISO_8859_1;
    }

    private static Encoding determineEncoding(byte[] bytes, Usage usage) {
        if (bytes.length == 0) {
            return Encoding.Q;
        }
        BitSet qChars = usage == Usage.TEXT_TOKEN ? Q_REGULAR_CHARS : Q_RESTRICTED_CHARS;
        int qEncoded = 0;
        for (byte b : bytes) {
            int v = b & 255;
            if (v != 32) {
                if (!((Boolean) BitSet.class.getMethod("get", Integer.TYPE).invoke(qChars, new Integer(v))).booleanValue()) {
                    qEncoded++;
                }
            }
        }
        return (qEncoded * 100) / bytes.length > 30 ? Encoding.B : Encoding.Q;
    }

    private static char hexDigit(int i) {
        return i < 10 ? (char) (i + 48) : (char) ((i - 10) + 65);
    }
}
