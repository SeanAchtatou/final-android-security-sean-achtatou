package org.apache.james.mime4j.codec;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.james.mime4j.util.CharsetUtil;

public class DecoderUtil {
    private static Log log;

    static {
        Object[] objArr = {DecoderUtil.class};
        log = (Log) LogFactory.class.getMethod("getLog", Class.class).invoke(null, objArr);
    }

    public static byte[] decodeBaseQuotedPrintable(String s) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            Object[] objArr = {"US-ASCII"};
            QuotedPrintableInputStream is = new QuotedPrintableInputStream(new ByteArrayInputStream((byte[]) String.class.getMethod("getBytes", String.class).invoke(s, objArr)));
            while (true) {
                int b = ((Integer) QuotedPrintableInputStream.class.getMethod("read", new Class[0]).invoke(is, new Object[0])).intValue();
                if (b == -1) {
                    break;
                }
                Class[] clsArr = {Integer.TYPE};
                ByteArrayOutputStream.class.getMethod("write", clsArr).invoke(baos, new Integer(b));
            }
        } catch (IOException e) {
            Object[] objArr2 = {e};
            Log.class.getMethod("error", Object.class).invoke(log, objArr2);
        }
        return (byte[]) ByteArrayOutputStream.class.getMethod("toByteArray", new Class[0]).invoke(baos, new Object[0]);
    }

    public static byte[] decodeBase64(String s) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            Object[] objArr = {"US-ASCII"};
            Base64InputStream is = new Base64InputStream(new ByteArrayInputStream((byte[]) String.class.getMethod("getBytes", String.class).invoke(s, objArr)));
            while (true) {
                int b = ((Integer) Base64InputStream.class.getMethod("read", new Class[0]).invoke(is, new Object[0])).intValue();
                if (b == -1) {
                    break;
                }
                Class[] clsArr = {Integer.TYPE};
                ByteArrayOutputStream.class.getMethod("write", clsArr).invoke(baos, new Integer(b));
            }
        } catch (IOException e) {
            Object[] objArr2 = {e};
            Log.class.getMethod("error", Object.class).invoke(log, objArr2);
        }
        return (byte[]) ByteArrayOutputStream.class.getMethod("toByteArray", new Class[0]).invoke(baos, new Object[0]);
    }

    public static String decodeB(String encodedWord, String charset) throws UnsupportedEncodingException {
        return new String((byte[]) DecoderUtil.class.getMethod("decodeBase64", String.class).invoke(null, encodedWord), charset);
    }

    public static String decodeQ(String encodedWord, String charset) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder(128);
        int i = 0;
        while (true) {
            if (i < ((Integer) String.class.getMethod("length", new Class[0]).invoke(encodedWord, new Object[0])).intValue()) {
                Class[] clsArr = {Integer.TYPE};
                char c = ((Character) String.class.getMethod("charAt", clsArr).invoke(encodedWord, new Integer(i))).charValue();
                if (c == '_') {
                    Object[] objArr = {"=20"};
                    StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr);
                } else {
                    Class[] clsArr2 = {Character.TYPE};
                    StringBuilder.class.getMethod("append", clsArr2).invoke(sb, new Character(c));
                }
                i++;
            } else {
                Class[] clsArr3 = {String.class};
                return new String((byte[]) DecoderUtil.class.getMethod("decodeBaseQuotedPrintable", clsArr3).invoke(null, (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb, new Object[0])), charset);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0197, code lost:
        if (((java.lang.Boolean) org.apache.james.mime4j.util.CharsetUtil.class.getMethod("isWhitespace", java.lang.String.class).invoke(null, r6)).booleanValue() == false) goto L_0x0199;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String decodeEncodedWords(java.lang.String r14) {
        /*
            r7 = -1
            r3 = 0
            r4 = 0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
        L_0x0008:
            java.lang.String r8 = "=?"
            r10 = 2
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r8
            r10 = 1
            java.lang.Class r13 = java.lang.Integer.TYPE
            r11[r10] = r13
            java.lang.Integer r13 = new java.lang.Integer
            r13.<init>(r3)
            r12[r10] = r13
            java.lang.String r10 = "indexOf"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r10 = r10.invoke(r14, r12)
            java.lang.Integer r10 = (java.lang.Integer) r10
            int r0 = r10.intValue()
            if (r0 != r7) goto L_0x003c
            r2 = r7
        L_0x0037:
            if (r2 != r7) goto L_0x00b5
            if (r3 != 0) goto L_0x006b
        L_0x003b:
            return r14
        L_0x003c:
            java.lang.String r8 = "?="
            int r9 = r0 + 2
            r10 = 2
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r8
            r10 = 1
            java.lang.Class r13 = java.lang.Integer.TYPE
            r11[r10] = r13
            java.lang.Integer r13 = new java.lang.Integer
            r13.<init>(r9)
            r12[r10] = r13
            java.lang.String r10 = "indexOf"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r10 = r10.invoke(r14, r12)
            java.lang.Integer r10 = (java.lang.Integer) r10
            int r2 = r10.intValue()
            goto L_0x0037
        L_0x006b:
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class r13 = java.lang.Integer.TYPE
            r11[r10] = r13
            java.lang.Integer r13 = new java.lang.Integer
            r13.<init>(r3)
            r12[r10] = r13
            java.lang.String r10 = "substring"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r7 = r10.invoke(r14, r12)
            java.lang.String r7 = (java.lang.String) r7
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r7
            java.lang.String r10 = "append"
            java.lang.Class<java.lang.StringBuilder> r13 = java.lang.StringBuilder.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            r10.invoke(r5, r12)
            r10 = 0
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            java.lang.String r10 = "toString"
            java.lang.Class<java.lang.StringBuilder> r13 = java.lang.StringBuilder.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r14 = r10.invoke(r5, r12)
            java.lang.String r14 = (java.lang.String) r14
            goto L_0x003b
        L_0x00b5:
            int r2 = r2 + 2
            r10 = 2
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class r13 = java.lang.Integer.TYPE
            r11[r10] = r13
            java.lang.Integer r13 = new java.lang.Integer
            r13.<init>(r3)
            r12[r10] = r13
            r10 = 1
            java.lang.Class r13 = java.lang.Integer.TYPE
            r11[r10] = r13
            java.lang.Integer r13 = new java.lang.Integer
            r13.<init>(r0)
            r12[r10] = r13
            java.lang.String r10 = "substring"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r6 = r10.invoke(r14, r12)
            java.lang.String r6 = (java.lang.String) r6
            r10 = 3
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r14
            r10 = 1
            java.lang.Class r13 = java.lang.Integer.TYPE
            r11[r10] = r13
            java.lang.Integer r13 = new java.lang.Integer
            r13.<init>(r0)
            r12[r10] = r13
            r10 = 2
            java.lang.Class r13 = java.lang.Integer.TYPE
            r11[r10] = r13
            java.lang.Integer r13 = new java.lang.Integer
            r13.<init>(r2)
            r12[r10] = r13
            java.lang.String r10 = "decodeEncodedWord"
            java.lang.Class<org.apache.james.mime4j.codec.DecoderUtil> r13 = org.apache.james.mime4j.codec.DecoderUtil.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            r13 = 0
            java.lang.Object r1 = r10.invoke(r13, r12)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 != 0) goto L_0x0176
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r6
            java.lang.String r10 = "append"
            java.lang.Class<java.lang.StringBuilder> r13 = java.lang.StringBuilder.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            r10.invoke(r5, r12)
            r10 = 2
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class r13 = java.lang.Integer.TYPE
            r11[r10] = r13
            java.lang.Integer r13 = new java.lang.Integer
            r13.<init>(r0)
            r12[r10] = r13
            r10 = 1
            java.lang.Class r13 = java.lang.Integer.TYPE
            r11[r10] = r13
            java.lang.Integer r13 = new java.lang.Integer
            r13.<init>(r2)
            r12[r10] = r13
            java.lang.String r10 = "substring"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r8 = r10.invoke(r14, r12)
            java.lang.String r8 = (java.lang.String) r8
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r8
            java.lang.String r10 = "append"
            java.lang.Class<java.lang.StringBuilder> r13 = java.lang.StringBuilder.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            r10.invoke(r5, r12)
        L_0x0170:
            r3 = r2
            if (r1 == 0) goto L_0x01c8
            r4 = 1
        L_0x0174:
            goto L_0x0008
        L_0x0176:
            if (r4 == 0) goto L_0x0199
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r6
            java.lang.String r10 = "isWhitespace"
            java.lang.Class<org.apache.james.mime4j.util.CharsetUtil> r13 = org.apache.james.mime4j.util.CharsetUtil.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            r13 = 0
            java.lang.Object r10 = r10.invoke(r13, r12)
            java.lang.Boolean r10 = (java.lang.Boolean) r10
            boolean r8 = r10.booleanValue()
            if (r8 != 0) goto L_0x01b0
        L_0x0199:
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r6
            java.lang.String r10 = "append"
            java.lang.Class<java.lang.StringBuilder> r13 = java.lang.StringBuilder.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            r10.invoke(r5, r12)
        L_0x01b0:
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r1
            java.lang.String r10 = "append"
            java.lang.Class<java.lang.StringBuilder> r13 = java.lang.StringBuilder.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            r10.invoke(r5, r12)
            goto L_0x0170
        L_0x01c8:
            r4 = 0
            goto L_0x0174
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.codec.DecoderUtil.decodeEncodedWords(java.lang.String):java.lang.String");
    }

    private static String decodeEncodedWord(String body, int begin, int end) {
        int qm2;
        int qm1 = body.indexOf(63, begin + 2);
        if (qm1 == end - 2 || (qm2 = body.indexOf(63, qm1 + 1)) == end - 2) {
            return null;
        }
        String mimeCharset = body.substring(begin + 2, qm1);
        String encoding = body.substring(qm1 + 1, qm2);
        String encodedText = body.substring(qm2 + 1, end - 2);
        String charset = CharsetUtil.toJavaCharset(mimeCharset);
        if (charset == null) {
            if (!log.isWarnEnabled()) {
                return null;
            }
            log.warn("MIME charset '" + mimeCharset + "' in encoded word '" + body.substring(begin, end) + "' doesn't have a " + "corresponding Java charset");
            return null;
        } else if (!CharsetUtil.isDecodingSupported(charset)) {
            if (!log.isWarnEnabled()) {
                return null;
            }
            log.warn("Current JDK doesn't support decoding of charset '" + charset + "' (MIME charset '" + mimeCharset + "' in encoded word '" + body.substring(begin, end) + "')");
            return null;
        } else if (encodedText.length() != 0) {
            try {
                if (encoding.equalsIgnoreCase("Q")) {
                    return decodeQ(encodedText, charset);
                }
                if (encoding.equalsIgnoreCase("B")) {
                    return decodeB(encodedText, charset);
                }
                if (!log.isWarnEnabled()) {
                    return null;
                }
                log.warn("Warning: Unknown encoding in encoded word '" + body.substring(begin, end) + "'");
                return null;
            } catch (UnsupportedEncodingException e) {
                if (!log.isWarnEnabled()) {
                    return null;
                }
                log.warn("Unsupported encoding in encoded word '" + body.substring(begin, end) + "'", e);
                return null;
            } catch (RuntimeException e2) {
                if (!log.isWarnEnabled()) {
                    return null;
                }
                log.warn("Could not decode encoded word '" + body.substring(begin, end) + "'", e2);
                return null;
            }
        } else if (!log.isWarnEnabled()) {
            return null;
        } else {
            log.warn("Missing encoded text in encoded word: '" + body.substring(begin, end) + "'");
            return null;
        }
    }
}
