package org.apache.james.mime4j.message;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.ContentUtil;

public class Multipart implements Body {
    private List<BodyPart> bodyParts;
    private ByteSequence epilogue;
    private transient String epilogueStrCache;
    private Entity parent;
    private ByteSequence preamble;
    private transient String preambleStrCache;
    private String subType;

    public Multipart(String subType2) {
        this.bodyParts = new LinkedList();
        this.parent = null;
        this.preamble = ByteSequence.EMPTY;
        this.preambleStrCache = "";
        this.epilogue = ByteSequence.EMPTY;
        this.epilogueStrCache = "";
        this.subType = subType2;
    }

    public Multipart(Multipart other) {
        this.bodyParts = new LinkedList();
        this.parent = null;
        this.preamble = other.preamble;
        this.preambleStrCache = other.preambleStrCache;
        this.epilogue = other.epilogue;
        this.epilogueStrCache = other.epilogueStrCache;
        Iterator i$ = (Iterator) List.class.getMethod("iterator", new Class[0]).invoke(other.bodyParts, new Object[0]);
        while (true) {
            if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                Object[] objArr = {new BodyPart((BodyPart) Iterator.class.getMethod("next", new Class[0]).invoke(i$, new Object[0]))};
                Multipart.class.getMethod("addBodyPart", BodyPart.class).invoke(this, objArr);
            } else {
                this.subType = other.subType;
                return;
            }
        }
    }

    public String getSubType() {
        return this.subType;
    }

    public void setSubType(String subType2) {
        this.subType = subType2;
    }

    public Entity getParent() {
        return this.parent;
    }

    public void setParent(Entity parent2) {
        this.parent = parent2;
        Iterator i$ = (Iterator) List.class.getMethod("iterator", new Class[0]).invoke(this.bodyParts, new Object[0]);
        while (true) {
            if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                Method method = Iterator.class.getMethod("next", new Class[0]);
                Method method2 = BodyPart.class.getMethod("setParent", Entity.class);
                method2.invoke((BodyPart) method.invoke(i$, new Object[0]), parent2);
            } else {
                return;
            }
        }
    }

    public int getCount() {
        return ((Integer) List.class.getMethod("size", new Class[0]).invoke(this.bodyParts, new Object[0])).intValue();
    }

    public List<BodyPart> getBodyParts() {
        Class[] clsArr = {List.class};
        return (List) Collections.class.getMethod("unmodifiableList", clsArr).invoke(null, this.bodyParts);
    }

    public void setBodyParts(List<BodyPart> bodyParts2) {
        this.bodyParts = bodyParts2;
        Iterator i$ = (Iterator) List.class.getMethod("iterator", new Class[0]).invoke(bodyParts2, new Object[0]);
        while (true) {
            if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                Method method = Iterator.class.getMethod("next", new Class[0]);
                Method method2 = BodyPart.class.getMethod("setParent", Entity.class);
                method2.invoke((BodyPart) method.invoke(i$, new Object[0]), this.parent);
            } else {
                return;
            }
        }
    }

    public void addBodyPart(BodyPart bodyPart) {
        if (bodyPart == null) {
            throw new IllegalArgumentException();
        }
        Object[] objArr = {bodyPart};
        ((Boolean) List.class.getMethod("add", Object.class).invoke(this.bodyParts, objArr)).booleanValue();
        Object[] objArr2 = {this.parent};
        BodyPart.class.getMethod("setParent", Entity.class).invoke(bodyPart, objArr2);
    }

    public void addBodyPart(BodyPart bodyPart, int index) {
        if (bodyPart == null) {
            throw new IllegalArgumentException();
        }
        List<BodyPart> list = this.bodyParts;
        Class[] clsArr = {Integer.TYPE, Object.class};
        List.class.getMethod("add", clsArr).invoke(list, new Integer(index), bodyPart);
        Object[] objArr = {this.parent};
        BodyPart.class.getMethod("setParent", Entity.class).invoke(bodyPart, objArr);
    }

    public BodyPart removeBodyPart(int index) {
        List<BodyPart> list = this.bodyParts;
        Class[] clsArr = {Integer.TYPE};
        BodyPart bodyPart = (BodyPart) List.class.getMethod("remove", clsArr).invoke(list, new Integer(index));
        Object[] objArr = {null};
        BodyPart.class.getMethod("setParent", Entity.class).invoke(bodyPart, objArr);
        return bodyPart;
    }

    public BodyPart replaceBodyPart(BodyPart bodyPart, int index) {
        if (bodyPart == null) {
            throw new IllegalArgumentException();
        }
        List<BodyPart> list = this.bodyParts;
        Class[] clsArr = {Integer.TYPE, Object.class};
        BodyPart replacedBodyPart = (BodyPart) List.class.getMethod("set", clsArr).invoke(list, new Integer(index), bodyPart);
        if (bodyPart == replacedBodyPart) {
            throw new IllegalArgumentException("Cannot replace body part with itself");
        }
        Object[] objArr = {this.parent};
        BodyPart.class.getMethod("setParent", Entity.class).invoke(bodyPart, objArr);
        Object[] objArr2 = {null};
        BodyPart.class.getMethod("setParent", Entity.class).invoke(replacedBodyPart, objArr2);
        return replacedBodyPart;
    }

    /* access modifiers changed from: package-private */
    public ByteSequence getPreambleRaw() {
        return this.preamble;
    }

    /* access modifiers changed from: package-private */
    public void setPreambleRaw(ByteSequence preamble2) {
        this.preamble = preamble2;
        this.preambleStrCache = null;
    }

    public String getPreamble() {
        if (this.preambleStrCache == null) {
            Object[] objArr = {this.preamble};
            this.preambleStrCache = (String) ContentUtil.class.getMethod("decode", ByteSequence.class).invoke(null, objArr);
        }
        return this.preambleStrCache;
    }

    public void setPreamble(String preamble2) {
        Object[] objArr = {preamble2};
        this.preamble = (ByteSequence) ContentUtil.class.getMethod("encode", String.class).invoke(null, objArr);
        this.preambleStrCache = preamble2;
    }

    /* access modifiers changed from: package-private */
    public ByteSequence getEpilogueRaw() {
        return this.epilogue;
    }

    /* access modifiers changed from: package-private */
    public void setEpilogueRaw(ByteSequence epilogue2) {
        this.epilogue = epilogue2;
        this.epilogueStrCache = null;
    }

    public String getEpilogue() {
        if (this.epilogueStrCache == null) {
            Object[] objArr = {this.epilogue};
            this.epilogueStrCache = (String) ContentUtil.class.getMethod("decode", ByteSequence.class).invoke(null, objArr);
        }
        return this.epilogueStrCache;
    }

    public void setEpilogue(String epilogue2) {
        Object[] objArr = {epilogue2};
        this.epilogue = (ByteSequence) ContentUtil.class.getMethod("encode", String.class).invoke(null, objArr);
        this.epilogueStrCache = epilogue2;
    }

    public void dispose() {
        Iterator i$ = (Iterator) List.class.getMethod("iterator", new Class[0]).invoke(this.bodyParts, new Object[0]);
        while (true) {
            if (((Boolean) Iterator.class.getMethod("hasNext", new Class[0]).invoke(i$, new Object[0])).booleanValue()) {
                Method method = Iterator.class.getMethod("next", new Class[0]);
                BodyPart.class.getMethod("dispose", new Class[0]).invoke((BodyPart) method.invoke(i$, new Object[0]), new Object[0]);
            } else {
                return;
            }
        }
    }
}
