package org.apache.james.mime4j.descriptor;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.james.mime4j.field.ContentTypeField;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.util.MimeUtil;

public class DefaultBodyDescriptor implements MutableBodyDescriptor {
    private static final String DEFAULT_MEDIA_TYPE = "text";
    private static final String DEFAULT_MIME_TYPE = "text/plain";
    private static final String DEFAULT_SUB_TYPE = "plain";
    private static final String EMAIL_MESSAGE_MIME_TYPE = "message/rfc822";
    private static final String MEDIA_TYPE_MESSAGE = "message";
    private static final String MEDIA_TYPE_TEXT = "text";
    private static final String SUB_TYPE_EMAIL = "rfc822";
    private static final String US_ASCII = "us-ascii";
    private static Log log;
    private String boundary;
    private String charset;
    private long contentLength;
    private boolean contentTransferEncSet;
    private boolean contentTypeSet;
    private String mediaType;
    private String mimeType;
    private Map<String, String> parameters;
    private String subType;
    private String transferEncoding;

    static {
        Object[] objArr = {DefaultBodyDescriptor.class};
        log = (Log) LogFactory.class.getMethod("getLog", Class.class).invoke(null, objArr);
    }

    public DefaultBodyDescriptor() {
        this(null);
    }

    public DefaultBodyDescriptor(BodyDescriptor parent) {
        this.mediaType = "text";
        this.subType = DEFAULT_SUB_TYPE;
        this.mimeType = "text/plain";
        this.boundary = null;
        this.charset = US_ASCII;
        this.transferEncoding = MimeUtil.ENC_7BIT;
        this.parameters = new HashMap();
        this.contentLength = -1;
        if (parent != null) {
            Class[] clsArr = {String.class, String.class};
            if (((Boolean) MimeUtil.class.getMethod("isSameMimeType", clsArr).invoke(null, ContentTypeField.TYPE_MULTIPART_DIGEST, (String) BodyDescriptor.class.getMethod("getMimeType", new Class[0]).invoke(parent, new Object[0]))).booleanValue()) {
                this.mimeType = "message/rfc822";
                this.subType = SUB_TYPE_EMAIL;
                this.mediaType = MEDIA_TYPE_MESSAGE;
                return;
            }
        }
        this.mimeType = "text/plain";
        this.subType = DEFAULT_SUB_TYPE;
        this.mediaType = "text";
    }

    public void addField(Field field) {
        Method method = Field.class.getMethod("getName", new Class[0]);
        String value = (String) Field.class.getMethod("getBody", new Class[0]).invoke(field, new Object[0]);
        Method method2 = String.class.getMethod("trim", new Class[0]);
        String name = (String) String.class.getMethod("toLowerCase", new Class[0]).invoke((String) method2.invoke((String) method.invoke(field, new Object[0]), new Object[0]), new Object[0]);
        if (!((Boolean) String.class.getMethod("equals", Object.class).invoke(name, "content-transfer-encoding")).booleanValue() || this.contentTransferEncSet) {
            if (!((Boolean) String.class.getMethod("equals", Object.class).invoke(name, "content-length")).booleanValue() || this.contentLength != -1) {
                if (((Boolean) String.class.getMethod("equals", Object.class).invoke(name, "content-type")).booleanValue() && !this.contentTypeSet) {
                    Object[] objArr = {value};
                    DefaultBodyDescriptor.class.getMethod("parseContentType", String.class).invoke(this, objArr);
                    return;
                }
                return;
            }
            try {
                Object[] objArr2 = {(String) String.class.getMethod("trim", new Class[0]).invoke(value, new Object[0])};
                this.contentLength = ((Long) Long.class.getMethod("parseLong", String.class).invoke(null, objArr2)).longValue();
            } catch (NumberFormatException e) {
                Log log2 = log;
                Object[] objArr3 = {"Invalid content-length: "};
                Object[] objArr4 = {value};
                Method method3 = StringBuilder.class.getMethod("append", String.class);
                Method method4 = StringBuilder.class.getMethod("toString", new Class[0]);
                Object[] objArr5 = {(String) method4.invoke((StringBuilder) method3.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder(), objArr3), objArr4), new Object[0])};
                Log.class.getMethod("error", Object.class).invoke(log2, objArr5);
            }
        } else {
            this.contentTransferEncSet = true;
            Method method5 = String.class.getMethod("trim", new Class[0]);
            String value2 = (String) String.class.getMethod("toLowerCase", new Class[0]).invoke((String) method5.invoke(value, new Object[0]), new Object[0]);
            if (((Integer) String.class.getMethod("length", new Class[0]).invoke(value2, new Object[0])).intValue() > 0) {
                this.transferEncoding = value2;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0206, code lost:
        if (((java.lang.Boolean) java.lang.String.class.getMethod("startsWith", r11).invoke(r3, org.apache.james.mime4j.field.ContentTypeField.TYPE_MULTIPART_PREFIX)).booleanValue() == false) goto L_0x0208;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void parseContentType(java.lang.String r15) {
        /*
            r14 = this;
            r8 = 1
            r14.contentTypeSet = r8
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r15
            java.lang.String r10 = "getHeaderParams"
            java.lang.Class<org.apache.james.mime4j.util.MimeUtil> r13 = org.apache.james.mime4j.util.MimeUtil.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            r13 = 0
            java.lang.Object r4 = r10.invoke(r13, r12)
            java.util.Map r4 = (java.util.Map) r4
            java.lang.String r8 = ""
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.Object> r13 = java.lang.Object.class
            r11[r10] = r13
            r12[r10] = r8
            java.lang.String r10 = "get"
            java.lang.Class<java.util.Map> r13 = java.util.Map.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r3 = r10.invoke(r4, r12)
            java.lang.Object r3 = (java.lang.Object) r3
            java.lang.String r3 = (java.lang.String) r3
            r6 = 0
            r5 = 0
            if (r3 == 0) goto L_0x01a2
            r10 = 0
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            java.lang.String r10 = "toLowerCase"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r8 = r10.invoke(r3, r12)
            java.lang.String r8 = (java.lang.String) r8
            r10 = 0
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            java.lang.String r10 = "trim"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r3 = r10.invoke(r8, r12)
            java.lang.String r3 = (java.lang.String) r3
            r8 = 47
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class r13 = java.lang.Integer.TYPE
            r11[r10] = r13
            java.lang.Integer r13 = new java.lang.Integer
            r13.<init>(r8)
            r12[r10] = r13
            java.lang.String r10 = "indexOf"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r10 = r10.invoke(r3, r12)
            java.lang.Integer r10 = (java.lang.Integer) r10
            int r2 = r10.intValue()
            r7 = 0
            r8 = -1
            if (r2 == r8) goto L_0x019d
            r8 = 0
            r10 = 2
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class r13 = java.lang.Integer.TYPE
            r11[r10] = r13
            java.lang.Integer r13 = new java.lang.Integer
            r13.<init>(r8)
            r12[r10] = r13
            r10 = 1
            java.lang.Class r13 = java.lang.Integer.TYPE
            r11[r10] = r13
            java.lang.Integer r13 = new java.lang.Integer
            r13.<init>(r2)
            r12[r10] = r13
            java.lang.String r10 = "substring"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r8 = r10.invoke(r3, r12)
            java.lang.String r8 = (java.lang.String) r8
            r10 = 0
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            java.lang.String r10 = "trim"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r6 = r10.invoke(r8, r12)
            java.lang.String r6 = (java.lang.String) r6
            int r8 = r2 + 1
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class r13 = java.lang.Integer.TYPE
            r11[r10] = r13
            java.lang.Integer r13 = new java.lang.Integer
            r13.<init>(r8)
            r12[r10] = r13
            java.lang.String r10 = "substring"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r8 = r10.invoke(r3, r12)
            java.lang.String r8 = (java.lang.String) r8
            r10 = 0
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            java.lang.String r10 = "trim"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r5 = r10.invoke(r8, r12)
            java.lang.String r5 = (java.lang.String) r5
            r10 = 0
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            java.lang.String r10 = "length"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r10 = r10.invoke(r6, r12)
            java.lang.Integer r10 = (java.lang.Integer) r10
            int r8 = r10.intValue()
            if (r8 <= 0) goto L_0x019d
            r10 = 0
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            java.lang.String r10 = "length"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r10 = r10.invoke(r5, r12)
            java.lang.Integer r10 = (java.lang.Integer) r10
            int r8 = r10.intValue()
            if (r8 <= 0) goto L_0x019d
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r6
            java.lang.String r10 = "append"
            java.lang.Class<java.lang.StringBuilder> r13 = java.lang.StringBuilder.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r8 = r10.invoke(r8, r12)
            java.lang.StringBuilder r8 = (java.lang.StringBuilder) r8
            java.lang.String r9 = "/"
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r9
            java.lang.String r10 = "append"
            java.lang.Class<java.lang.StringBuilder> r13 = java.lang.StringBuilder.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r8 = r10.invoke(r8, r12)
            java.lang.StringBuilder r8 = (java.lang.StringBuilder) r8
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r5
            java.lang.String r10 = "append"
            java.lang.Class<java.lang.StringBuilder> r13 = java.lang.StringBuilder.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r8 = r10.invoke(r8, r12)
            java.lang.StringBuilder r8 = (java.lang.StringBuilder) r8
            r10 = 0
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            java.lang.String r10 = "toString"
            java.lang.Class<java.lang.StringBuilder> r13 = java.lang.StringBuilder.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r3 = r10.invoke(r8, r12)
            java.lang.String r3 = (java.lang.String) r3
            r7 = 1
        L_0x019d:
            if (r7 != 0) goto L_0x01a2
            r3 = 0
            r6 = 0
            r5 = 0
        L_0x01a2:
            java.lang.String r8 = "boundary"
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.Object> r13 = java.lang.Object.class
            r11[r10] = r13
            r12[r10] = r8
            java.lang.String r10 = "get"
            java.lang.Class<java.util.Map> r13 = java.util.Map.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r0 = r10.invoke(r4, r12)
            java.lang.Object r0 = (java.lang.Object) r0
            java.lang.String r0 = (java.lang.String) r0
            if (r3 == 0) goto L_0x020e
            java.lang.String r8 = "multipart/"
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r8
            java.lang.String r10 = "startsWith"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r10 = r10.invoke(r3, r12)
            java.lang.Boolean r10 = (java.lang.Boolean) r10
            boolean r8 = r10.booleanValue()
            if (r8 == 0) goto L_0x01e6
            if (r0 != 0) goto L_0x0208
        L_0x01e6:
            java.lang.String r8 = "multipart/"
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r8
            java.lang.String r10 = "startsWith"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r10 = r10.invoke(r3, r12)
            java.lang.Boolean r10 = (java.lang.Boolean) r10
            boolean r8 = r10.booleanValue()
            if (r8 != 0) goto L_0x020e
        L_0x0208:
            r14.mimeType = r3
            r14.subType = r5
            r14.mediaType = r6
        L_0x020e:
            java.lang.String r8 = r14.mimeType
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            r11[r10] = r13
            r12[r10] = r8
            java.lang.String r10 = "isMultipart"
            java.lang.Class<org.apache.james.mime4j.util.MimeUtil> r13 = org.apache.james.mime4j.util.MimeUtil.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            r13 = 0
            java.lang.Object r10 = r10.invoke(r13, r12)
            java.lang.Boolean r10 = (java.lang.Boolean) r10
            boolean r8 = r10.booleanValue()
            if (r8 == 0) goto L_0x0233
            r14.boundary = r0
        L_0x0233:
            java.lang.String r8 = "charset"
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.Object> r13 = java.lang.Object.class
            r11[r10] = r13
            r12[r10] = r8
            java.lang.String r10 = "get"
            java.lang.Class<java.util.Map> r13 = java.util.Map.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r1 = r10.invoke(r4, r12)
            java.lang.Object r1 = (java.lang.Object) r1
            java.lang.String r1 = (java.lang.String) r1
            r8 = 0
            r14.charset = r8
            if (r1 == 0) goto L_0x0297
            r10 = 0
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            java.lang.String r10 = "trim"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r1 = r10.invoke(r1, r12)
            java.lang.String r1 = (java.lang.String) r1
            r10 = 0
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            java.lang.String r10 = "length"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r10 = r10.invoke(r1, r12)
            java.lang.Integer r10 = (java.lang.Integer) r10
            int r8 = r10.intValue()
            if (r8 <= 0) goto L_0x0297
            r10 = 0
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            java.lang.String r10 = "toLowerCase"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r8 = r10.invoke(r1, r12)
            java.lang.String r8 = (java.lang.String) r8
            r14.charset = r8
        L_0x0297:
            java.lang.String r8 = r14.charset
            if (r8 != 0) goto L_0x02c3
            java.lang.String r8 = "text"
            java.lang.String r9 = r14.mediaType
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.Object> r13 = java.lang.Object.class
            r11[r10] = r13
            r12[r10] = r9
            java.lang.String r10 = "equals"
            java.lang.Class<java.lang.String> r13 = java.lang.String.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            java.lang.Object r10 = r10.invoke(r8, r12)
            java.lang.Boolean r10 = (java.lang.Boolean) r10
            boolean r8 = r10.booleanValue()
            if (r8 == 0) goto L_0x02c3
            java.lang.String r8 = "us-ascii"
            r14.charset = r8
        L_0x02c3:
            java.util.Map<java.lang.String, java.lang.String> r8 = r14.parameters
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.util.Map> r13 = java.util.Map.class
            r11[r10] = r13
            r12[r10] = r4
            java.lang.String r10 = "putAll"
            java.lang.Class<java.util.Map> r13 = java.util.Map.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            r10.invoke(r8, r12)
            java.util.Map<java.lang.String, java.lang.String> r8 = r14.parameters
            java.lang.String r9 = ""
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.Object> r13 = java.lang.Object.class
            r11[r10] = r13
            r12[r10] = r9
            java.lang.String r10 = "remove"
            java.lang.Class<java.util.Map> r13 = java.util.Map.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            r10.invoke(r8, r12)
            java.util.Map<java.lang.String, java.lang.String> r8 = r14.parameters
            java.lang.String r9 = "boundary"
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.Object> r13 = java.lang.Object.class
            r11[r10] = r13
            r12[r10] = r9
            java.lang.String r10 = "remove"
            java.lang.Class<java.util.Map> r13 = java.util.Map.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            r10.invoke(r8, r12)
            java.util.Map<java.lang.String, java.lang.String> r8 = r14.parameters
            java.lang.String r9 = "charset"
            r10 = 1
            java.lang.Class[] r11 = new java.lang.Class[r10]
            java.lang.Object[] r12 = new java.lang.Object[r10]
            r10 = 0
            java.lang.Class<java.lang.Object> r13 = java.lang.Object.class
            r11[r10] = r13
            r12[r10] = r9
            java.lang.String r10 = "remove"
            java.lang.Class<java.util.Map> r13 = java.util.Map.class
            java.lang.reflect.Method r10 = r13.getMethod(r10, r11)
            r10.invoke(r8, r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.descriptor.DefaultBodyDescriptor.parseContentType(java.lang.String):void");
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public String getBoundary() {
        return this.boundary;
    }

    public String getCharset() {
        return this.charset;
    }

    public Map<String, String> getContentTypeParameters() {
        return this.parameters;
    }

    public String getTransferEncoding() {
        return this.transferEncoding;
    }

    public String toString() {
        return this.mimeType;
    }

    public long getContentLength() {
        return this.contentLength;
    }

    public String getMediaType() {
        return this.mediaType;
    }

    public String getSubType() {
        return this.subType;
    }
}
