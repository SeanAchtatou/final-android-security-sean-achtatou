package org.apache.james.mime4j.field.datetime.parser;

import java.io.InputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.Vector;
import org.apache.james.mime4j.field.address.parser.AddressListParserConstants;
import org.apache.james.mime4j.field.contenttype.parser.ContentTypeParserConstants;
import org.apache.james.mime4j.field.datetime.DateTime;
import org.apache.james.mime4j.field.structured.parser.StructuredFieldParserConstants;

public class DateTimeParser implements DateTimeParserConstants {
    private static final boolean ignoreMilitaryZoneOffset = true;
    private static int[] jj_la1_0;
    private static int[] jj_la1_1;
    private Vector<int[]> jj_expentries;
    private int[] jj_expentry;
    private int jj_gen;
    SimpleCharStream jj_input_stream;
    private int jj_kind;
    private final int[] jj_la1;
    public Token jj_nt;
    private int jj_ntk;
    public Token token;
    public DateTimeParserTokenManager token_source;

    public static void main(String[] args) throws ParseException {
        while (true) {
            try {
                DateTimeParser.class.getMethod("parseLine", new Class[0]).invoke(new DateTimeParser(System.in), new Object[0]);
            } catch (Exception x) {
                Exception.class.getMethod("printStackTrace", new Class[0]).invoke(x, new Object[0]);
                return;
            }
        }
    }

    private static int parseDigits(Token token2) {
        String str = token2.image;
        Class[] clsArr = {String.class, Integer.TYPE};
        return ((Integer) Integer.class.getMethod("parseInt", clsArr).invoke(null, str, new Integer(10))).intValue();
    }

    private static int getMilitaryZoneOffset(char c) {
        return 0;
    }

    private static class Time {
        private int hour;
        private int minute;
        private int second;
        private int zone;

        public Time(int hour2, int minute2, int second2, int zone2) {
            this.hour = hour2;
            this.minute = minute2;
            this.second = second2;
            this.zone = zone2;
        }

        public int getHour() {
            return this.hour;
        }

        public int getMinute() {
            return this.minute;
        }

        public int getSecond() {
            return this.second;
        }

        public int getZone() {
            return this.zone;
        }
    }

    private static class Date {
        private int day;
        private int month;
        private String year;

        public Date(String year2, int month2, int day2) {
            this.year = year2;
            this.month = month2;
            this.day = day2;
        }

        public String getYear() {
            return this.year;
        }

        public int getMonth() {
            return this.month;
        }

        public int getDay() {
            return this.day;
        }
    }

    public final DateTime parseLine() throws ParseException {
        int i;
        DateTime dt = (DateTime) DateTimeParser.class.getMethod("date_time", new Class[0]).invoke(this, new Object[0]);
        if (this.jj_ntk == -1) {
            i = ((Integer) DateTimeParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 1:
                Class[] clsArr = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(1));
                break;
            default:
                this.jj_la1[0] = this.jj_gen;
                break;
        }
        Class[] clsArr2 = {Integer.TYPE};
        DateTimeParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(2));
        return dt;
    }

    public final DateTime parseAll() throws ParseException {
        DateTime dt = (DateTime) DateTimeParser.class.getMethod("date_time", new Class[0]).invoke(this, new Object[0]);
        Class[] clsArr = {Integer.TYPE};
        DateTimeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(0));
        return dt;
    }

    public final DateTime date_time() throws ParseException {
        int i;
        if (this.jj_ntk == -1) {
            i = ((Integer) DateTimeParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                DateTimeParser.class.getMethod("day_of_week", new Class[0]).invoke(this, new Object[0]);
                Class[] clsArr = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(3));
                break;
            default:
                this.jj_la1[1] = this.jj_gen;
                break;
        }
        Date d = (Date) DateTimeParser.class.getMethod("date", new Class[0]).invoke(this, new Object[0]);
        Time t = (Time) DateTimeParser.class.getMethod("time", new Class[0]).invoke(this, new Object[0]);
        Method method = Date.class.getMethod("getYear", new Class[0]);
        return new DateTime((String) method.invoke(d, new Object[0]), ((Integer) Date.class.getMethod("getMonth", new Class[0]).invoke(d, new Object[0])).intValue(), ((Integer) Date.class.getMethod("getDay", new Class[0]).invoke(d, new Object[0])).intValue(), ((Integer) Time.class.getMethod("getHour", new Class[0]).invoke(t, new Object[0])).intValue(), ((Integer) Time.class.getMethod("getMinute", new Class[0]).invoke(t, new Object[0])).intValue(), ((Integer) Time.class.getMethod("getSecond", new Class[0]).invoke(t, new Object[0])).intValue(), ((Integer) Time.class.getMethod("getZone", new Class[0]).invoke(t, new Object[0])).intValue());
    }

    public final String day_of_week() throws ParseException {
        int i;
        if (this.jj_ntk == -1) {
            i = ((Integer) DateTimeParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 4:
                Class[] clsArr = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(4));
                break;
            case 5:
                Class[] clsArr2 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(5));
                break;
            case 6:
                Class[] clsArr3 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr3).invoke(this, new Integer(6));
                break;
            case 7:
                Class[] clsArr4 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr4).invoke(this, new Integer(7));
                break;
            case 8:
                Class[] clsArr5 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr5).invoke(this, new Integer(8));
                break;
            case 9:
                Class[] clsArr6 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr6).invoke(this, new Integer(9));
                break;
            case 10:
                Class[] clsArr7 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr7).invoke(this, new Integer(10));
                break;
            default:
                this.jj_la1[2] = this.jj_gen;
                Class[] clsArr8 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr8).invoke(this, new Integer(-1));
                throw new ParseException();
        }
        return this.token.image;
    }

    public final Date date() throws ParseException {
        int d = ((Integer) DateTimeParser.class.getMethod("day", new Class[0]).invoke(this, new Object[0])).intValue();
        return new Date((String) DateTimeParser.class.getMethod("year", new Class[0]).invoke(this, new Object[0]), ((Integer) DateTimeParser.class.getMethod("month", new Class[0]).invoke(this, new Object[0])).intValue(), d);
    }

    public final int day() throws ParseException {
        Class[] clsArr = {Integer.TYPE};
        Class[] clsArr2 = {Token.class};
        return ((Integer) DateTimeParser.class.getMethod("parseDigits", clsArr2).invoke(null, (Token) DateTimeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(46)))).intValue();
    }

    public final int month() throws ParseException {
        int i;
        if (this.jj_ntk == -1) {
            i = ((Integer) DateTimeParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 11:
                Class[] clsArr = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(11));
                return 1;
            case 12:
                Class[] clsArr2 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(12));
                return 2;
            case 13:
                Class[] clsArr3 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr3).invoke(this, new Integer(13));
                return 3;
            case 14:
                Class[] clsArr4 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr4).invoke(this, new Integer(14));
                return 4;
            case StructuredFieldParserConstants.CONTENT /*15*/:
                Class[] clsArr5 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr5).invoke(this, new Integer(15));
                return 5;
            case 16:
                Class[] clsArr6 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr6).invoke(this, new Integer(16));
                return 6;
            case 17:
                Class[] clsArr7 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr7).invoke(this, new Integer(17));
                return 7;
            case 18:
                Class[] clsArr8 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr8).invoke(this, new Integer(18));
                return 8;
            case 19:
                Class[] clsArr9 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr9).invoke(this, new Integer(19));
                return 9;
            case 20:
                Class[] clsArr10 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr10).invoke(this, new Integer(20));
                return 10;
            case 21:
                Class[] clsArr11 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr11).invoke(this, new Integer(21));
                return 11;
            case 22:
                Class[] clsArr12 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr12).invoke(this, new Integer(22));
                return 12;
            default:
                this.jj_la1[3] = this.jj_gen;
                Class[] clsArr13 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr13).invoke(this, new Integer(-1));
                throw new ParseException();
        }
    }

    public final String year() throws ParseException {
        Class[] clsArr = {Integer.TYPE};
        return ((Token) DateTimeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(46))).image;
    }

    public final Time time() throws ParseException {
        int i;
        int s = 0;
        int h = ((Integer) DateTimeParser.class.getMethod("hour", new Class[0]).invoke(this, new Object[0])).intValue();
        Class[] clsArr = {Integer.TYPE};
        DateTimeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(23));
        int m = ((Integer) DateTimeParser.class.getMethod("minute", new Class[0]).invoke(this, new Object[0])).intValue();
        if (this.jj_ntk == -1) {
            i = ((Integer) DateTimeParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case ContentTypeParserConstants.ANY:
                Class[] clsArr2 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(23));
                s = ((Integer) DateTimeParser.class.getMethod("second", new Class[0]).invoke(this, new Object[0])).intValue();
                break;
            default:
                this.jj_la1[4] = this.jj_gen;
                break;
        }
        return new Time(h, m, s, ((Integer) DateTimeParser.class.getMethod("zone", new Class[0]).invoke(this, new Object[0])).intValue());
    }

    public final int hour() throws ParseException {
        Class[] clsArr = {Integer.TYPE};
        Class[] clsArr2 = {Token.class};
        return ((Integer) DateTimeParser.class.getMethod("parseDigits", clsArr2).invoke(null, (Token) DateTimeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(46)))).intValue();
    }

    public final int minute() throws ParseException {
        Class[] clsArr = {Integer.TYPE};
        Class[] clsArr2 = {Token.class};
        return ((Integer) DateTimeParser.class.getMethod("parseDigits", clsArr2).invoke(null, (Token) DateTimeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(46)))).intValue();
    }

    public final int second() throws ParseException {
        Class[] clsArr = {Integer.TYPE};
        Class[] clsArr2 = {Token.class};
        return ((Integer) DateTimeParser.class.getMethod("parseDigits", clsArr2).invoke(null, (Token) DateTimeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(46)))).intValue();
    }

    public final int zone() throws ParseException {
        int i;
        int i2 = -1;
        if (this.jj_ntk == -1) {
            i = ((Integer) DateTimeParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case DateTimeParserConstants.OFFSETDIR /*24*/:
                Class[] clsArr = {Integer.TYPE};
                Object[] objArr = {new Integer(24)};
                Class[] clsArr2 = {Integer.TYPE};
                Object[] objArr2 = {(Token) DateTimeParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(46))};
                int intValue = ((Integer) DateTimeParser.class.getMethod("parseDigits", Token.class).invoke(null, objArr2)).intValue();
                if (!((Boolean) String.class.getMethod("equals", Object.class).invoke(((Token) DateTimeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, objArr)).image, "-")).booleanValue()) {
                    i2 = 1;
                }
                return intValue * i2;
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case AddressListParserConstants.QUOTEDSTRING:
            case 32:
            case AddressListParserConstants.ANY:
            case 34:
            case DateTimeParserConstants.MILITARY_ZONE /*35*/:
                return ((Integer) DateTimeParser.class.getMethod("obs_zone", new Class[0]).invoke(this, new Object[0])).intValue();
            default:
                this.jj_la1[5] = this.jj_gen;
                Class[] clsArr3 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr3).invoke(this, new Integer(-1));
                throw new ParseException();
        }
    }

    public final int obs_zone() throws ParseException {
        int i;
        int z;
        if (this.jj_ntk == -1) {
            i = ((Integer) DateTimeParser.class.getMethod("jj_ntk", new Class[0]).invoke(this, new Object[0])).intValue();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 25:
                Class[] clsArr = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr).invoke(this, new Integer(25));
                z = 0;
                break;
            case 26:
                Class[] clsArr2 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr2).invoke(this, new Integer(26));
                z = 0;
                break;
            case 27:
                Class[] clsArr3 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr3).invoke(this, new Integer(27));
                z = -5;
                break;
            case 28:
                Class[] clsArr4 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr4).invoke(this, new Integer(28));
                z = -4;
                break;
            case 29:
                Class[] clsArr5 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr5).invoke(this, new Integer(29));
                z = -6;
                break;
            case 30:
                Class[] clsArr6 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr6).invoke(this, new Integer(30));
                z = -5;
                break;
            case AddressListParserConstants.QUOTEDSTRING:
                Class[] clsArr7 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr7).invoke(this, new Integer(31));
                z = -7;
                break;
            case 32:
                Class[] clsArr8 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr8).invoke(this, new Integer(32));
                z = -6;
                break;
            case AddressListParserConstants.ANY:
                Class[] clsArr9 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr9).invoke(this, new Integer(33));
                z = -8;
                break;
            case 34:
                Class[] clsArr10 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr10).invoke(this, new Integer(34));
                z = -7;
                break;
            case DateTimeParserConstants.MILITARY_ZONE /*35*/:
                Class[] clsArr11 = {Integer.TYPE};
                String str = ((Token) DateTimeParser.class.getMethod("jj_consume_token", clsArr11).invoke(this, new Integer(35))).image;
                Class[] clsArr12 = {Integer.TYPE};
                char charValue = ((Character) String.class.getMethod("charAt", clsArr12).invoke(str, new Integer(0))).charValue();
                Class[] clsArr13 = {Character.TYPE};
                z = ((Integer) DateTimeParser.class.getMethod("getMilitaryZoneOffset", clsArr13).invoke(null, new Character(charValue))).intValue();
                break;
            default:
                this.jj_la1[6] = this.jj_gen;
                Class[] clsArr14 = {Integer.TYPE};
                DateTimeParser.class.getMethod("jj_consume_token", clsArr14).invoke(this, new Integer(-1));
                throw new ParseException();
        }
        return z * 100;
    }

    static {
        DateTimeParser.class.getMethod("jj_la1_0", new Class[0]).invoke(null, new Object[0]);
        DateTimeParser.class.getMethod("jj_la1_1", new Class[0]).invoke(null, new Object[0]);
    }

    private static void jj_la1_0() {
        jj_la1_0 = new int[]{2, 2032, 2032, 8386560, 8388608, -16777216, -33554432};
    }

    private static void jj_la1_1() {
        jj_la1_1 = new int[]{0, 0, 0, 0, 0, 15, 15};
    }

    public DateTimeParser(InputStream stream) {
        this(stream, null);
    }

    public DateTimeParser(InputStream stream, String encoding) {
        this.jj_la1 = new int[7];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        try {
            this.jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1);
            this.token_source = new DateTimeParserTokenManager(this.jj_input_stream);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 7; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public void ReInit(InputStream stream) {
        Object[] objArr = {stream, null};
        DateTimeParser.class.getMethod("ReInit", InputStream.class, String.class).invoke(this, objArr);
    }

    public void ReInit(InputStream stream, String encoding) {
        try {
            SimpleCharStream simpleCharStream = this.jj_input_stream;
            Class[] clsArr = {InputStream.class, String.class, Integer.TYPE, Integer.TYPE};
            SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(simpleCharStream, stream, encoding, new Integer(1), new Integer(1));
            DateTimeParserTokenManager dateTimeParserTokenManager = this.token_source;
            Object[] objArr = {this.jj_input_stream};
            DateTimeParserTokenManager.class.getMethod("ReInit", SimpleCharStream.class).invoke(dateTimeParserTokenManager, objArr);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 7; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public DateTimeParser(Reader stream) {
        this.jj_la1 = new int[7];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.jj_input_stream = new SimpleCharStream(stream, 1, 1);
        this.token_source = new DateTimeParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 7; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public void ReInit(Reader stream) {
        SimpleCharStream simpleCharStream = this.jj_input_stream;
        Class[] clsArr = {Reader.class, Integer.TYPE, Integer.TYPE};
        SimpleCharStream.class.getMethod("ReInit", clsArr).invoke(simpleCharStream, stream, new Integer(1), new Integer(1));
        DateTimeParserTokenManager dateTimeParserTokenManager = this.token_source;
        Object[] objArr = {this.jj_input_stream};
        DateTimeParserTokenManager.class.getMethod("ReInit", SimpleCharStream.class).invoke(dateTimeParserTokenManager, objArr);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 7; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public DateTimeParser(DateTimeParserTokenManager tm) {
        this.jj_la1 = new int[7];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 7; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public void ReInit(DateTimeParserTokenManager tm) {
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 7; i++) {
            this.jj_la1[i] = -1;
        }
    }

    private final Token jj_consume_token(int kind) throws ParseException {
        Token oldToken = this.token;
        if (oldToken.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token token3 = (Token) DateTimeParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token2.next = token3;
            this.token = token3;
        }
        this.jj_ntk = -1;
        if (this.token.kind == kind) {
            this.jj_gen++;
            return this.token;
        }
        this.token = oldToken;
        this.jj_kind = kind;
        throw ((ParseException) DateTimeParser.class.getMethod("generateParseException", new Class[0]).invoke(this, new Object[0]));
    }

    public final Token getNextToken() {
        if (this.token.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token token3 = (Token) DateTimeParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token2.next = token3;
            this.token = token3;
        }
        this.jj_ntk = -1;
        this.jj_gen++;
        return this.token;
    }

    public final Token getToken(int index) {
        Token t;
        int i = 0;
        Token t2 = this.token;
        while (i < index) {
            if (t2.next != null) {
                t = t2.next;
            } else {
                t = (Token) DateTimeParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
                t2.next = t;
            }
            i++;
            t2 = t;
        }
        return t2;
    }

    private final int jj_ntk() {
        Token token2 = this.token.next;
        this.jj_nt = token2;
        if (token2 == null) {
            Token token3 = this.token;
            Token token4 = (Token) DateTimeParserTokenManager.class.getMethod("getNextToken", new Class[0]).invoke(this.token_source, new Object[0]);
            token3.next = token4;
            int i = token4.kind;
            this.jj_ntk = i;
            return i;
        }
        int i2 = this.jj_nt.kind;
        this.jj_ntk = i2;
        return i2;
    }

    public ParseException generateParseException() {
        Vector.class.getMethod("removeAllElements", new Class[0]).invoke(this.jj_expentries, new Object[0]);
        boolean[] la1tokens = new boolean[49];
        for (int i = 0; i < 49; i++) {
            la1tokens[i] = false;
        }
        if (this.jj_kind >= 0) {
            la1tokens[this.jj_kind] = true;
            this.jj_kind = -1;
        }
        for (int i2 = 0; i2 < 7; i2++) {
            if (this.jj_la1[i2] == this.jj_gen) {
                for (int j = 0; j < 32; j++) {
                    if ((jj_la1_0[i2] & (1 << j)) != 0) {
                        la1tokens[j] = true;
                    }
                    if ((jj_la1_1[i2] & (1 << j)) != 0) {
                        la1tokens[j + 32] = true;
                    }
                }
            }
        }
        for (int i3 = 0; i3 < 49; i3++) {
            if (la1tokens[i3]) {
                this.jj_expentry = new int[1];
                this.jj_expentry[0] = i3;
                Vector<int[]> vector = this.jj_expentries;
                Object[] objArr = {this.jj_expentry};
                Vector.class.getMethod("addElement", Object.class).invoke(vector, objArr);
            }
        }
        int[][] exptokseq = new int[((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.jj_expentries, new Object[0])).intValue()][];
        int i4 = 0;
        while (true) {
            if (i4 >= ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.jj_expentries, new Object[0])).intValue()) {
                return new ParseException(this.token, exptokseq, tokenImage);
            }
            Vector<int[]> vector2 = this.jj_expentries;
            Class[] clsArr = {Integer.TYPE};
            exptokseq[i4] = (int[]) Vector.class.getMethod("elementAt", clsArr).invoke(vector2, new Integer(i4));
            i4++;
        }
    }

    public final void enable_tracing() {
    }

    public final void disable_tracing() {
    }
}
