package org.apache.james.mime4j.parser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.LinkedList;
import org.apache.james.mime4j.codec.Base64InputStream;
import org.apache.james.mime4j.codec.QuotedPrintableInputStream;
import org.apache.james.mime4j.descriptor.BodyDescriptor;
import org.apache.james.mime4j.io.BufferedLineReaderInputStream;
import org.apache.james.mime4j.io.LineNumberInputStream;
import org.apache.james.mime4j.io.LineNumberSource;
import org.apache.james.mime4j.util.CharsetUtil;
import org.apache.james.mime4j.util.MimeUtil;

public class MimeTokenStream implements EntityStates, RecursionMode {
    private final MimeEntityConfig config;
    private EntityStateMachine currentStateMachine;
    private final LinkedList<EntityStateMachine> entities;
    private BufferedLineReaderInputStream inbuffer;
    private int recursionMode;
    private int state;

    public static final MimeTokenStream createMaximalDescriptorStream() {
        MimeEntityConfig config2 = new MimeEntityConfig();
        Class[] clsArr = {Boolean.TYPE};
        MimeEntityConfig.class.getMethod("setMaximalBodyDescriptor", clsArr).invoke(config2, new Boolean(true));
        return new MimeTokenStream(config2);
    }

    public static final MimeTokenStream createStrictValidationStream() {
        MimeEntityConfig config2 = new MimeEntityConfig();
        Class[] clsArr = {Boolean.TYPE};
        MimeEntityConfig.class.getMethod("setStrictParsing", clsArr).invoke(config2, new Boolean(true));
        return new MimeTokenStream(config2);
    }

    public MimeTokenStream() {
        this(new MimeEntityConfig());
    }

    protected MimeTokenStream(MimeEntityConfig config2) {
        this.entities = new LinkedList<>();
        this.state = -1;
        this.recursionMode = 0;
        this.config = config2;
    }

    public void parse(InputStream stream) {
        Object[] objArr = {stream, null};
        MimeTokenStream.class.getMethod("doParse", InputStream.class, String.class).invoke(this, objArr);
    }

    public void parseHeadless(InputStream stream, String contentType) {
        if (contentType == null) {
            throw new IllegalArgumentException("Content type may not be null");
        }
        Object[] objArr = {stream, contentType};
        MimeTokenStream.class.getMethod("doParse", InputStream.class, String.class).invoke(this, objArr);
    }

    private void doParse(InputStream stream, String contentType) {
        LinkedList.class.getMethod("clear", new Class[0]).invoke(this.entities, new Object[0]);
        LineNumberSource lineSource = null;
        if (((Boolean) MimeEntityConfig.class.getMethod("isCountLineNumbers", new Class[0]).invoke(this.config, new Object[0])).booleanValue()) {
            LineNumberSource lineInput = new LineNumberInputStream(stream);
            lineSource = lineInput;
            stream = lineInput;
        }
        this.inbuffer = new BufferedLineReaderInputStream(stream, 4096, ((Integer) MimeEntityConfig.class.getMethod("getMaxLineLen", new Class[0]).invoke(this.config, new Object[0])).intValue());
        switch (this.recursionMode) {
            case 0:
            case 1:
            case 3:
                MimeEntity mimeentity = new MimeEntity(lineSource, this.inbuffer, null, 0, 1, this.config);
                int i = this.recursionMode;
                Class[] clsArr = {Integer.TYPE};
                MimeEntity.class.getMethod("setRecursionMode", clsArr).invoke(mimeentity, new Integer(i));
                if (contentType != null) {
                    Object[] objArr = {contentType};
                    MimeEntity.class.getMethod("skipHeader", String.class).invoke(mimeentity, objArr);
                }
                this.currentStateMachine = mimeentity;
                break;
            case 2:
                this.currentStateMachine = new RawEntity(this.inbuffer);
                break;
        }
        LinkedList<EntityStateMachine> linkedList = this.entities;
        Object[] objArr2 = {this.currentStateMachine};
        ((Boolean) LinkedList.class.getMethod("add", Object.class).invoke(linkedList, objArr2)).booleanValue();
        this.state = ((Integer) EntityStateMachine.class.getMethod("getState", new Class[0]).invoke(this.currentStateMachine, new Object[0])).intValue();
    }

    public boolean isRaw() {
        return this.recursionMode == 2;
    }

    public int getRecursionMode() {
        return this.recursionMode;
    }

    public void setRecursionMode(int mode) {
        this.recursionMode = mode;
        if (this.currentStateMachine != null) {
            EntityStateMachine entityStateMachine = this.currentStateMachine;
            Class[] clsArr = {Integer.TYPE};
            EntityStateMachine.class.getMethod("setRecursionMode", clsArr).invoke(entityStateMachine, new Integer(mode));
        }
    }

    public void stop() {
        BufferedLineReaderInputStream.class.getMethod("truncate", new Class[0]).invoke(this.inbuffer, new Object[0]);
    }

    public int getState() {
        return this.state;
    }

    public InputStream getInputStream() {
        return (InputStream) EntityStateMachine.class.getMethod("getContentStream", new Class[0]).invoke(this.currentStateMachine, new Object[0]);
    }

    public InputStream getDecodedInputStream() {
        Method method = MimeTokenStream.class.getMethod("getBodyDescriptor", new Class[0]);
        String transferEncoding = (String) BodyDescriptor.class.getMethod("getTransferEncoding", new Class[0]).invoke((BodyDescriptor) method.invoke(this, new Object[0]), new Object[0]);
        InputStream dataStream = (InputStream) EntityStateMachine.class.getMethod("getContentStream", new Class[0]).invoke(this.currentStateMachine, new Object[0]);
        if (((Boolean) MimeUtil.class.getMethod("isBase64Encoding", String.class).invoke(null, transferEncoding)).booleanValue()) {
            return new Base64InputStream(dataStream);
        }
        if (((Boolean) MimeUtil.class.getMethod("isQuotedPrintableEncoded", String.class).invoke(null, transferEncoding)).booleanValue()) {
            return new QuotedPrintableInputStream(dataStream);
        }
        return dataStream;
    }

    public Reader getReader() {
        Charset charset;
        Method method = MimeTokenStream.class.getMethod("getBodyDescriptor", new Class[0]);
        String mimeCharset = (String) BodyDescriptor.class.getMethod("getCharset", new Class[0]).invoke((BodyDescriptor) method.invoke(this, new Object[0]), new Object[0]);
        if (mimeCharset != null) {
            if (!((Boolean) String.class.getMethod("equals", Object.class).invoke("", mimeCharset)).booleanValue()) {
                Object[] objArr = {mimeCharset};
                charset = (Charset) Charset.class.getMethod("forName", String.class).invoke(null, objArr);
                return new InputStreamReader((InputStream) MimeTokenStream.class.getMethod("getDecodedInputStream", new Class[0]).invoke(this, new Object[0]), charset);
            }
        }
        charset = CharsetUtil.US_ASCII;
        return new InputStreamReader((InputStream) MimeTokenStream.class.getMethod("getDecodedInputStream", new Class[0]).invoke(this, new Object[0]), charset);
    }

    public BodyDescriptor getBodyDescriptor() {
        return (BodyDescriptor) EntityStateMachine.class.getMethod("getBodyDescriptor", new Class[0]).invoke(this.currentStateMachine, new Object[0]);
    }

    public Field getField() {
        return (Field) EntityStateMachine.class.getMethod("getField", new Class[0]).invoke(this.currentStateMachine, new Object[0]);
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 124 */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x009f, code lost:
        r8.currentStateMachine = (org.apache.james.mime4j.parser.EntityStateMachine) java.util.LinkedList.class.getMethod("getLast", new java.lang.Class[0]).invoke(r8.entities, new java.lang.Object[0]);
        r1 = r8.currentStateMachine;
        r2 = r8.recursionMode;
        r5 = new java.lang.Class[]{java.lang.Integer.TYPE};
        org.apache.james.mime4j.parser.EntityStateMachine.class.getMethod("setRecursionMode", r5).invoke(r1, new java.lang.Integer(r2));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int next() throws java.io.IOException, org.apache.james.mime4j.MimeException {
        /*
            r8 = this;
            r3 = -1
            int r1 = r8.state
            if (r1 == r3) goto L_0x0009
            org.apache.james.mime4j.parser.EntityStateMachine r1 = r8.currentStateMachine
            if (r1 != 0) goto L_0x0041
        L_0x0009:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "No more tokens are available."
            r1.<init>(r2)
            throw r1
        L_0x0011:
            java.util.LinkedList<org.apache.james.mime4j.parser.EntityStateMachine> r1 = r8.entities
            r4 = 0
            java.lang.Class[] r5 = new java.lang.Class[r4]
            java.lang.Object[] r6 = new java.lang.Object[r4]
            java.lang.String r4 = "removeLast"
            java.lang.Class<java.util.LinkedList> r7 = java.util.LinkedList.class
            java.lang.reflect.Method r4 = r7.getMethod(r4, r5)
            r4.invoke(r1, r6)
            java.util.LinkedList<org.apache.james.mime4j.parser.EntityStateMachine> r1 = r8.entities
            r4 = 0
            java.lang.Class[] r5 = new java.lang.Class[r4]
            java.lang.Object[] r6 = new java.lang.Object[r4]
            java.lang.String r4 = "isEmpty"
            java.lang.Class<java.util.LinkedList> r7 = java.util.LinkedList.class
            java.lang.reflect.Method r4 = r7.getMethod(r4, r5)
            java.lang.Object r4 = r4.invoke(r1, r6)
            java.lang.Boolean r4 = (java.lang.Boolean) r4
            boolean r1 = r4.booleanValue()
            if (r1 == 0) goto L_0x009f
            r1 = 0
            r8.currentStateMachine = r1
        L_0x0041:
            org.apache.james.mime4j.parser.EntityStateMachine r1 = r8.currentStateMachine
            if (r1 == 0) goto L_0x00da
            org.apache.james.mime4j.parser.EntityStateMachine r1 = r8.currentStateMachine
            r4 = 0
            java.lang.Class[] r5 = new java.lang.Class[r4]
            java.lang.Object[] r6 = new java.lang.Object[r4]
            java.lang.String r4 = "advance"
            java.lang.Class<org.apache.james.mime4j.parser.EntityStateMachine> r7 = org.apache.james.mime4j.parser.EntityStateMachine.class
            java.lang.reflect.Method r4 = r7.getMethod(r4, r5)
            java.lang.Object r0 = r4.invoke(r1, r6)
            org.apache.james.mime4j.parser.EntityStateMachine r0 = (org.apache.james.mime4j.parser.EntityStateMachine) r0
            if (r0 == 0) goto L_0x007d
            java.util.LinkedList<org.apache.james.mime4j.parser.EntityStateMachine> r1 = r8.entities
            r4 = 1
            java.lang.Class[] r5 = new java.lang.Class[r4]
            java.lang.Object[] r6 = new java.lang.Object[r4]
            r4 = 0
            java.lang.Class<java.lang.Object> r7 = java.lang.Object.class
            r5[r4] = r7
            r6[r4] = r0
            java.lang.String r4 = "add"
            java.lang.Class<java.util.LinkedList> r7 = java.util.LinkedList.class
            java.lang.reflect.Method r4 = r7.getMethod(r4, r5)
            java.lang.Object r4 = r4.invoke(r1, r6)
            java.lang.Boolean r4 = (java.lang.Boolean) r4
            r4.booleanValue()
            r8.currentStateMachine = r0
        L_0x007d:
            org.apache.james.mime4j.parser.EntityStateMachine r1 = r8.currentStateMachine
            r4 = 0
            java.lang.Class[] r5 = new java.lang.Class[r4]
            java.lang.Object[] r6 = new java.lang.Object[r4]
            java.lang.String r4 = "getState"
            java.lang.Class<org.apache.james.mime4j.parser.EntityStateMachine> r7 = org.apache.james.mime4j.parser.EntityStateMachine.class
            java.lang.reflect.Method r4 = r7.getMethod(r4, r5)
            java.lang.Object r4 = r4.invoke(r1, r6)
            java.lang.Integer r4 = (java.lang.Integer) r4
            int r1 = r4.intValue()
            r8.state = r1
            int r1 = r8.state
            if (r1 == r3) goto L_0x0011
            int r1 = r8.state
        L_0x009e:
            return r1
        L_0x009f:
            java.util.LinkedList<org.apache.james.mime4j.parser.EntityStateMachine> r1 = r8.entities
            r4 = 0
            java.lang.Class[] r5 = new java.lang.Class[r4]
            java.lang.Object[] r6 = new java.lang.Object[r4]
            java.lang.String r4 = "getLast"
            java.lang.Class<java.util.LinkedList> r7 = java.util.LinkedList.class
            java.lang.reflect.Method r4 = r7.getMethod(r4, r5)
            java.lang.Object r1 = r4.invoke(r1, r6)
            java.lang.Object r1 = (java.lang.Object) r1
            org.apache.james.mime4j.parser.EntityStateMachine r1 = (org.apache.james.mime4j.parser.EntityStateMachine) r1
            r8.currentStateMachine = r1
            org.apache.james.mime4j.parser.EntityStateMachine r1 = r8.currentStateMachine
            int r2 = r8.recursionMode
            r4 = 1
            java.lang.Class[] r5 = new java.lang.Class[r4]
            java.lang.Object[] r6 = new java.lang.Object[r4]
            r4 = 0
            java.lang.Class r7 = java.lang.Integer.TYPE
            r5[r4] = r7
            java.lang.Integer r7 = new java.lang.Integer
            r7.<init>(r2)
            r6[r4] = r7
            java.lang.String r4 = "setRecursionMode"
            java.lang.Class<org.apache.james.mime4j.parser.EntityStateMachine> r7 = org.apache.james.mime4j.parser.EntityStateMachine.class
            java.lang.reflect.Method r4 = r7.getMethod(r4, r5)
            r4.invoke(r1, r6)
            goto L_0x0041
        L_0x00da:
            r8.state = r3
            int r1 = r8.state
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.parser.MimeTokenStream.next():int");
    }

    public static final String stateToString(int state2) {
        Class[] clsArr = {Integer.TYPE};
        return (String) AbstractEntity.class.getMethod("stateToString", clsArr).invoke(null, new Integer(state2));
    }
}
