package com.admob.android.ads;

import android.util.Log;
import java.lang.ref.WeakReference;
import org.json.JSONArray;

final class cf implements Runnable {
    private WeakReference a;
    private JSONArray b;

    public cf(d dVar, JSONArray jSONArray) {
        this.a = new WeakReference(dVar);
        this.b = jSONArray;
    }

    public final void run() {
        try {
            d dVar = (d) this.a.get();
            if (dVar != null) {
                d.a(dVar, this.b);
            }
        } catch (Exception e) {
            if (bh.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "exception caught in Ad$ViewAdd.run(), " + e.getMessage());
                e.printStackTrace();
            }
            d dVar2 = (d) this.a.get();
            if (dVar2 != null) {
                dVar2.o();
            }
        }
    }
}
