package com.admob.android.ads;

import android.view.animation.Animation;

final class v implements Animation.AnimationListener {
    private /* synthetic */ f a;
    private /* synthetic */ AdView b;
    private /* synthetic */ f c;

    v(f fVar, AdView adView, f fVar2) {
        this.a = fVar;
        this.b = adView;
        this.c = fVar2;
    }

    public final void onAnimationEnd(Animation animation) {
        if (this.a != null) {
            this.b.removeView(this.a);
        }
        f unused = this.b.b = this.c;
        if (this.a != null) {
            this.a.d();
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
