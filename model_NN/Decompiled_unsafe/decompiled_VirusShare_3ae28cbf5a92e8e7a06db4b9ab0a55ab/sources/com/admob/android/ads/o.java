package com.admob.android.ads;

import android.os.Build;
import android.util.Log;
import java.net.URL;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public abstract class o implements n {
    private static Executor m = null;
    private static String n;
    protected String a;
    protected int b;
    protected Exception c = null;
    protected Map d;
    protected int e;
    protected int f;
    protected String g;
    protected b h;
    protected URL i;
    protected byte[] j;
    protected boolean k;
    protected String l;
    private String o;
    private Object p;

    protected o(String str, String str2, b bVar, int i2, String str3) {
        this.o = str;
        this.g = str2;
        this.h = bVar;
        this.b = i2;
        this.d = null;
        this.k = true;
        this.e = 0;
        this.f = 3;
        if (str3 != null) {
            this.l = str3;
            this.a = "application/x-www-form-urlencoded";
            return;
        }
        this.l = null;
        this.a = null;
    }

    public static String i() {
        if (n == null) {
            StringBuffer stringBuffer = new StringBuffer();
            String str = Build.VERSION.RELEASE;
            if (str.length() > 0) {
                stringBuffer.append(str);
            } else {
                stringBuffer.append("1.0");
            }
            stringBuffer.append("; ");
            Locale locale = Locale.getDefault();
            String language = locale.getLanguage();
            if (language != null) {
                stringBuffer.append(language.toLowerCase());
                String country = locale.getCountry();
                if (country != null) {
                    stringBuffer.append("-");
                    stringBuffer.append(country.toLowerCase());
                }
            } else {
                stringBuffer.append("en");
            }
            String str2 = Build.MODEL;
            if (str2.length() > 0) {
                stringBuffer.append("; ");
                stringBuffer.append(str2);
            }
            String str3 = Build.ID;
            if (str3.length() > 0) {
                stringBuffer.append(" Build/");
                stringBuffer.append(str3);
            }
            n = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2 (AdMob-ANDROID-%s)", stringBuffer, "20101109");
            if (bh.a("AdMobSDK", 3)) {
                Log.d("AdMobSDK", "Phone's user-agent is:  " + n);
            }
        }
        return n;
    }

    public final void a(b bVar) {
        this.h = bVar;
    }

    public final void a(Object obj) {
        this.p = obj;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final byte[] c() {
        return this.j;
    }

    public final String d() {
        return this.o;
    }

    public final URL e() {
        return this.i;
    }

    public final void f() {
        if (m == null) {
            m = Executors.newCachedThreadPool();
        }
        m.execute(this);
    }

    public final void g() {
        this.f = 1;
    }

    public final Object h() {
        return this.p;
    }
}
