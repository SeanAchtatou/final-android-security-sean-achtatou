package com.admob.android.ads;

import android.util.Log;

public final class bh implements h {
    private br a;

    public static boolean a(String str, int i) {
        return (i >= 5) || Log.isLoggable(str, i);
    }

    public final void a() {
        if (this.a != null) {
            this.a.c();
        }
    }

    public final void a(d dVar) {
        if (this.a != null) {
            this.a.g = dVar;
            this.a.a();
        }
    }
}
