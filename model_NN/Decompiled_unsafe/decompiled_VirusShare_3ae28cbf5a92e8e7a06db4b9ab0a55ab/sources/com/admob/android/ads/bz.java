package com.admob.android.ads;

import java.lang.ref.WeakReference;

final class bz implements Runnable {
    private WeakReference a;

    public bz(bd bdVar) {
        this.a = new WeakReference(bdVar);
    }

    public final void run() {
        bd bdVar = (bd) this.a.get();
        if (bdVar != null && bdVar.e != null) {
            bdVar.e.setVisibility(0);
            bdVar.e.requestLayout();
            bdVar.e.requestFocus();
            bdVar.e.start();
        }
    }
}
