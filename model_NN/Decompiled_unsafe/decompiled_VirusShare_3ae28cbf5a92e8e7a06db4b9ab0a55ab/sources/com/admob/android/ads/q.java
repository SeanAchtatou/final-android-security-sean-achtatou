package com.admob.android.ads;

import android.os.Bundle;

public final class q implements i {
    public String a;
    public boolean b;

    public q() {
        this.a = null;
        this.b = false;
    }

    public q(String str, boolean z) {
        this.a = str;
        this.b = z;
    }

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("u", this.a);
        bundle.putBoolean("p", this.b);
        return bundle;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof q)) {
            return false;
        }
        q qVar = (q) obj;
        return !(this.a == null && qVar.a != null) && !(this.a != null && !this.a.equals(qVar.a)) && !(this.b != qVar.b);
    }

    public final int hashCode() {
        return this.a != null ? this.a.hashCode() : super.hashCode();
    }
}
