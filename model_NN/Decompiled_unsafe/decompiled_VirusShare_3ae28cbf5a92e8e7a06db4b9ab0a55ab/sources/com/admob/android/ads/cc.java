package com.admob.android.ads;

import android.view.View;
import java.lang.ref.WeakReference;
import java.util.Map;

public final class cc implements View.OnClickListener {
    private WeakReference a;
    private boolean b;

    public cc(bd bdVar, boolean z) {
        this.a = new WeakReference(bdVar);
        this.b = z;
    }

    public final void onClick(View view) {
        bd bdVar = (bd) this.a.get();
        if (bdVar != null) {
            if (this.b) {
                bdVar.f.a("skip", (Map) null);
            }
            bdVar.c();
        }
    }
}
