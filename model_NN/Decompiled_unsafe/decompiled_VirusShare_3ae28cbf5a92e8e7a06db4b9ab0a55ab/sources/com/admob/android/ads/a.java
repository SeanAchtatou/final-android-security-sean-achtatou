package com.admob.android.ads;

import org.json.JSONObject;

public final class a {
    private static boolean a = false;

    public static n a(String str, String str2, b bVar) {
        n a2 = a(str, null, str2, bVar, 5000, null);
        if (a2 != null) {
            a2.g();
        }
        return a2;
    }

    public static n a(String str, String str2, String str3) {
        return a(str, str2, str3, null);
    }

    public static n a(String str, String str2, String str3, b bVar) {
        return a(str, str2, str3, bVar, 5000, null);
    }

    public static n a(String str, String str2, String str3, b bVar, int i, String str4) {
        return new c(str, str2, str3, bVar, i, str4);
    }

    public static n a(String str, String str2, String str3, JSONObject jSONObject, b bVar) {
        n a2 = a(str, str2, str3, bVar, 5000, jSONObject == null ? null : jSONObject.toString());
        a2.a("application/json");
        return a2;
    }
}
