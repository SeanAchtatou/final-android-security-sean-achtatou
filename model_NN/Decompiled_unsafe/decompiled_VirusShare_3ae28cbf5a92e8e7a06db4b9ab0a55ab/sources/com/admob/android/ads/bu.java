package com.admob.android.ads;

import android.util.Log;
import android.webkit.WebView;
import com.admob.android.ads.a.a;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

public final class bu extends ba {
    boolean b = false;
    boolean c = false;
    d d;
    private Timer e;
    private TimerTask f;

    public bu(a aVar, d dVar) {
        super(aVar);
        this.d = dVar;
    }

    public final void onPageFinished(WebView webView, String str) {
        a aVar = (a) this.a.get();
        if (aVar != null) {
            if (str != null && str.equals(aVar.c)) {
                this.c = true;
                super.onPageFinished(webView, str);
                if (aVar instanceof r) {
                    ((r) aVar).a_();
                }
                if (bh.a("AdMobSDK", 3)) {
                    Log.d("AdMobSDK", "startResponseTimer()");
                }
                this.f = new bs(this);
                this.e = new Timer();
                this.e.schedule(this.f, 10000);
            } else if (bh.a("AdMobSDK", 4)) {
                Log.i("AdMobSDK", "Unexpected page loaded, urlThatFinished: " + str);
            }
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        Hashtable a;
        String str2;
        Hashtable a2;
        String str3;
        if (bh.a("AdMobSDK", 2)) {
            Log.v("AdMobSDK", "shouldOverrideUrlLoading, url: " + str);
        }
        try {
            URI uri = new URI(str);
            if ("admob".equals(uri.getScheme())) {
                String host = uri.getHost();
                if ("ready".equals(host)) {
                    if (this.b) {
                        return true;
                    }
                    this.b = true;
                    if (bh.a("AdMobSDK", 3)) {
                        Log.d("AdMobSDK", "cancelResponseTimer()");
                    }
                    if (this.e != null) {
                        this.e.cancel();
                    }
                    String query = uri.getQuery();
                    if (query == null || (a2 = a(query)) == null || (str3 = (String) a2.get("success")) == null || !"true".equalsIgnoreCase(str3)) {
                        if (this.d != null) {
                            this.d.a(false);
                        }
                        return true;
                    }
                    if (this.d != null) {
                        this.d.a(true);
                    }
                    return true;
                } else if ("movie".equals(host)) {
                    String query2 = uri.getQuery();
                    if (query2 != null && (a = a(query2)) != null && (str2 = (String) a.get("action")) != null && !"play".equalsIgnoreCase(str2) && !"pause".equalsIgnoreCase(str2) && !"stop".equalsIgnoreCase(str2) && !"remove".equalsIgnoreCase(str2) && !"replay".equalsIgnoreCase(str2) && bh.a("AdMobSDK", 5)) {
                        Log.w("AdMobSDK", "Unknown actionString, admob://movie?action=" + str2);
                    }
                    return true;
                }
            }
        } catch (URISyntaxException e2) {
            Log.w("AdMobSDK", "Bad link URL in AdMob web view.", e2);
        }
        return super.shouldOverrideUrlLoading(webView, str);
    }
}
