package com.admob.android.ads;

import android.util.Log;
import android.view.animation.DecelerateInterpolator;
import java.lang.ref.WeakReference;

final class t implements Runnable {
    private WeakReference a;
    private WeakReference b;

    public t(f fVar, AdView adView) {
        this.b = new WeakReference(fVar);
        this.a = new WeakReference(adView);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.ap.<init>(float, float, float, float, float, boolean):void
     arg types: [int, int, float, float, float, int]
     candidates:
      com.admob.android.ads.ap.<init>(float[], float[], float, float, float, boolean):void
      com.admob.android.ads.ap.<init>(float, float, float, float, float, boolean):void */
    public final void run() {
        try {
            AdView adView = (AdView) this.a.get();
            f fVar = (f) this.b.get();
            if (adView != null && fVar != null) {
                f a2 = adView.b;
                if (a2 != null) {
                    a2.setVisibility(8);
                }
                fVar.setVisibility(0);
                ap apVar = new ap(90.0f, 0.0f, ((float) adView.getWidth()) / 2.0f, ((float) adView.getHeight()) / 2.0f, -0.4f * ((float) adView.getWidth()), false);
                apVar.setDuration(700);
                apVar.setFillAfter(true);
                apVar.setInterpolator(new DecelerateInterpolator());
                apVar.setAnimationListener(new v(a2, adView, fVar));
                adView.startAnimation(apVar);
            }
        } catch (Exception e) {
            if (bh.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "exception caught in SwapViews.run(), " + e.getMessage());
            }
        }
    }
}
