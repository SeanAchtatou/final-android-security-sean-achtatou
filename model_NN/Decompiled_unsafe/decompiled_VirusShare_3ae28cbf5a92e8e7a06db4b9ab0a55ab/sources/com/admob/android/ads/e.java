package com.admob.android.ads;

import android.util.Log;

final class e implements b {
    e() {
    }

    public final void a(n nVar) {
        if (bh.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Click processed at " + nVar.e());
        }
    }

    public final void a(n nVar, Exception exc) {
        if (bh.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Click processing failed at " + nVar.e(), exc);
        }
    }
}
