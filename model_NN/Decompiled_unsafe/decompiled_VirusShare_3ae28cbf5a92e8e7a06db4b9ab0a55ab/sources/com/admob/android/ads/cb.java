package com.admob.android.ads;

import android.view.MotionEvent;
import android.view.View;
import java.lang.ref.WeakReference;

public final class cb implements View.OnTouchListener {
    private WeakReference a;

    public cb(bd bdVar) {
        this.a = new WeakReference(bdVar);
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        bd bdVar = (bd) this.a.get();
        if (bdVar == null) {
            return false;
        }
        bdVar.b(false);
        bd.a(bdVar, motionEvent);
        return false;
    }
}
