package com.admob.android.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.admob.android.ads.a.a;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;

public class ba extends WebViewClient {
    protected WeakReference a;
    private WeakReference b;
    private az c;
    private Map d;

    public ba(a aVar) {
        this(aVar, null);
    }

    public ba(a aVar, WeakReference weakReference) {
        this.a = new WeakReference(aVar);
        this.b = weakReference;
        weakReference.get();
        this.c = new az();
        this.d = null;
        aVar.addJavascriptInterface(this.c, "JsProxy");
    }

    public static Hashtable a(String str) {
        Hashtable hashtable = null;
        if (str != null) {
            hashtable = new Hashtable();
            StringTokenizer stringTokenizer = new StringTokenizer(str, "&");
            while (stringTokenizer.hasMoreTokens()) {
                String nextToken = stringTokenizer.nextToken();
                int indexOf = nextToken.indexOf(61);
                if (indexOf != -1) {
                    String substring = nextToken.substring(0, indexOf);
                    String substring2 = nextToken.substring(indexOf + 1);
                    if (!(substring == null || substring2 == null)) {
                        hashtable.put(substring, substring2);
                    }
                }
            }
        }
        return hashtable;
    }

    public void onPageFinished(WebView webView, String str) {
        a aVar = (a) this.a.get();
        if (aVar != null) {
            if (str != null && str.equals(aVar.c)) {
                if (this.d == null) {
                    this.d = new HashMap();
                    Context context = aVar.getContext();
                    this.d.put("sdkVersion", "20101109-ANDROID-3312276cc1406347");
                    this.d.put("ua", o.i());
                    this.d.put("portrait", ak.i(context));
                    this.d.put("width", String.valueOf(aVar.getWidth()));
                    this.d.put("height", String.valueOf(aVar.getHeight()));
                    this.d.put("isu", ak.g(context));
                }
                aVar.a("onEvent", "loaded", this.d);
            } else if (bh.a("AdMobSDK", 4)) {
                Log.i("AdMobSDK", "Unexpected page loaded, urlThatFinished: " + str);
            }
        }
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        Hashtable a2;
        String str2;
        Hashtable a3;
        String str3;
        Hashtable a4;
        String str4;
        if (bh.a("AdMobSDK", 2)) {
            Log.v("AdMobSDK", "shouldOverrideUrlLoading, url: " + str);
        }
        a aVar = (a) this.a.get();
        if (aVar == null) {
            return false;
        }
        Context context = aVar.getContext();
        try {
            URI uri = new URI(str);
            if ("admob".equals(uri.getScheme())) {
                String host = uri.getHost();
                if ("launch".equals(host)) {
                    String query = uri.getQuery();
                    if (!(query == null || (a4 = a(query)) == null || (str4 = (String) a4.get("url")) == null)) {
                        if (!(context instanceof Activity)) {
                            context = (Context) this.b.get();
                        }
                        if (context != null) {
                            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str4)));
                        }
                        return true;
                    }
                } else if ("open".equals(host)) {
                    String query2 = uri.getQuery();
                    if (!(query2 == null || (a3 = a(query2)) == null || (str3 = (String) a3.get("vars")) == null)) {
                        aVar.loadUrl("javascript: JsProxy.setDataAndOpen(" + str3 + ")");
                        return true;
                    }
                } else if ("closecanvas".equals(host)) {
                    if (webView == aVar) {
                        aVar.a();
                        return true;
                    }
                } else if ("log".equals(host)) {
                    String query3 = uri.getQuery();
                    if (!(query3 == null || (a2 = a(query3)) == null || (str2 = (String) a2.get("string")) == null)) {
                        if (bh.a("AdMobSDK", 3)) {
                            Log.d("AdMobSDK", "<AdMob:WebView>: " + str2);
                        }
                        return true;
                    }
                } else {
                    if (bh.a("AdMobSDK", 3)) {
                        Log.d("AdMobSDK", "Received message from JS but didn't know how to handle: " + str);
                    }
                    return true;
                }
            }
        } catch (URISyntaxException e) {
            Log.w("AdMobSDK", "Bad link URL in AdMob web view.", e);
        }
        return false;
    }
}
