package com.admob.android.ads;

import java.lang.ref.WeakReference;

final class bi implements Runnable {
    private WeakReference a;

    public bi(br brVar) {
        this.a = new WeakReference(brVar);
    }

    public final void run() {
        br brVar = (br) this.a.get();
        if (brVar != null) {
            brVar.d();
        }
    }
}
