package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import java.lang.ref.WeakReference;

final class ab extends Thread {
    private WeakReference a;

    public ab(AdView adView) {
        this.a = new WeakReference(adView);
    }

    public final void run() {
        AdView adView = (AdView) this.a.get();
        if (adView != null) {
            try {
                Context context = adView.getContext();
                if (k.a(AdView.c(adView), context, adView.i, adView.j, adView.a(), adView.b(), adView.c(), new f(context, adView), (int) (((float) adView.getMeasuredWidth()) / f.c()), adView.d(), null, adView.e()) == null) {
                    AdView.f(adView);
                }
            } catch (Exception e) {
                if (bh.a("AdMobSDK", 6)) {
                    Log.e("AdMobSDK", "Unhandled exception requesting a fresh ad.", e);
                }
                AdView.f(adView);
            } finally {
                boolean unused = adView.n = false;
                adView.a(true);
            }
        }
    }
}
