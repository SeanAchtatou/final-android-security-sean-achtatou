package com.admob.android.ads;

import java.lang.ref.WeakReference;

final class ca implements Runnable {
    private WeakReference a;

    public ca(bd bdVar) {
        this.a = new WeakReference(bdVar);
    }

    public final void run() {
        bd bdVar = (bd) this.a.get();
        if (bdVar != null && bdVar.e() && bdVar.g == 2 && bdVar.k != null) {
            bdVar.k.a();
        }
    }
}
