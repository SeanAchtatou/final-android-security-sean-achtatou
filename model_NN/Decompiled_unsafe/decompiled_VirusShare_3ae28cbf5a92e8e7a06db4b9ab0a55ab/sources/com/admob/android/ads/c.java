package com.admob.android.ads;

import android.util.Log;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

final class c extends o {
    private HttpURLConnection m;
    private URL n;

    public c(String str, String str2, String str3, b bVar, int i, String str4) {
        super(str2, str3, bVar, i, str4);
        try {
            this.n = new URL(str);
            this.i = this.n;
        } catch (MalformedURLException e) {
            this.n = null;
            this.c = e;
        }
        this.m = null;
        this.e = 0;
    }

    private void j() {
        if (this.m != null) {
            this.m.disconnect();
            this.m = null;
        }
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 217 */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0116 A[Catch:{ all -> 0x0245 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0145 A[Catch:{ all -> 0x0245 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0150 A[SYNTHETIC, Splitter:B:54:0x0150] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0239 A[SYNTHETIC, Splitter:B:97:0x0239] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a() {
        /*
            r12 = this;
            r10 = 2
            r9 = 1
            r8 = 0
            java.lang.String r7 = "AdMobSDK"
            java.net.URL r0 = r12.n
            if (r0 != 0) goto L_0x0257
            com.admob.android.ads.b r0 = r12.h
            if (r0 == 0) goto L_0x0019
            com.admob.android.ads.b r0 = r12.h
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r2 = "url was null"
            r1.<init>(r2)
            r0.a(r12, r1)
        L_0x0019:
            r0 = r8
        L_0x001a:
            if (r0 != 0) goto L_0x0027
            com.admob.android.ads.b r1 = r12.h
            if (r1 == 0) goto L_0x0027
            com.admob.android.ads.b r1 = r12.h
            java.lang.Exception r2 = r12.c
            r1.a(r12, r2)
        L_0x0027:
            return r0
        L_0x0028:
            r3 = 302(0x12e, float:4.23E-43)
            if (r1 != r3) goto L_0x024f
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r3 = "Location"
            java.lang.String r1 = r1.getHeaderField(r3)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r3 = "AdMobSDK"
            r4 = 3
            boolean r3 = com.admob.android.ads.bh.a(r3, r4)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            if (r3 == 0) goto L_0x0055
            java.lang.String r3 = "AdMobSDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r4.<init>()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r5 = "Got redirectUrl: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            android.util.Log.d(r3, r4)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
        L_0x0055:
            java.net.URL r3 = new java.net.URL     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r12.n = r3     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r12.j()
        L_0x005f:
            int r0 = r12.e
            int r1 = r12.f
            if (r0 >= r1) goto L_0x0254
            if (r2 != 0) goto L_0x0254
            java.lang.String r0 = "AdMobSDK"
            boolean r0 = com.admob.android.ads.bh.a(r7, r10)
            if (r0 == 0) goto L_0x0095
            java.lang.String r0 = "AdMobSDK"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "attempt "
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = r12.e
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = " to connect to url "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.net.URL r1 = r12.n
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            android.util.Log.v(r7, r0)
        L_0x0095:
            r3 = 0
            r12.j()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.URL r0 = r12.n     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r12.m = r0     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r0 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r1 = 1
            r0.setUseCaches(r1)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r0 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r1 = 1
            r0.setInstanceFollowRedirects(r1)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r0 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            if (r0 == 0) goto L_0x0251
            java.net.HttpURLConnection r0 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r1 = "User-Agent"
            java.lang.String r4 = i()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r0.setRequestProperty(r1, r4)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r0 = r12.g     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            if (r0 == 0) goto L_0x00cb
            java.net.HttpURLConnection r0 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r1 = "X-ADMOB-ISU"
            java.lang.String r4 = r12.g     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r0.setRequestProperty(r1, r4)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
        L_0x00cb:
            java.net.HttpURLConnection r0 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            int r1 = r12.b     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r0 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            int r1 = r12.b     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r0 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r1 = 0
            r0.setUseCaches(r1)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.util.Map r0 = r12.d     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            if (r0 == 0) goto L_0x0160
            java.util.Map r0 = r12.d     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.util.Set r0 = r0.keySet()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
        L_0x00ed:
            boolean r0 = r4.hasNext()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            if (r0 == 0) goto L_0x0160
            java.lang.Object r0 = r4.next()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            if (r0 == 0) goto L_0x00ed
            java.util.Map r1 = r12.d     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.Object r1 = r1.get(r0)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            if (r1 == 0) goto L_0x00ed
            java.net.HttpURLConnection r5 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r5.addRequestProperty(r0, r1)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            goto L_0x00ed
        L_0x010b:
            r0 = move-exception
            r1 = r3
        L_0x010d:
            java.lang.String r2 = "AdMobSDK"
            r3 = 3
            boolean r2 = com.admob.android.ads.bh.a(r2, r3)     // Catch:{ all -> 0x0245 }
            if (r2 == 0) goto L_0x013c
            java.lang.String r2 = "AdMobSDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0245 }
            r3.<init>()     // Catch:{ all -> 0x0245 }
            java.lang.String r4 = "connection attempt "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0245 }
            int r4 = r12.e     // Catch:{ all -> 0x0245 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0245 }
            java.lang.String r4 = " failed, url "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0245 }
            java.net.URL r4 = r12.n     // Catch:{ all -> 0x0245 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0245 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0245 }
            android.util.Log.d(r2, r3)     // Catch:{ all -> 0x0245 }
        L_0x013c:
            java.lang.String r2 = "AdMobSDK"
            r3 = 2
            boolean r2 = com.admob.android.ads.bh.a(r2, r3)     // Catch:{ all -> 0x0245 }
            if (r2 == 0) goto L_0x014c
            java.lang.String r2 = "AdMobSDK"
            java.lang.String r3 = "exception: "
            android.util.Log.v(r2, r3, r0)     // Catch:{ all -> 0x0245 }
        L_0x014c:
            r12.c = r0     // Catch:{ all -> 0x0245 }
            if (r1 == 0) goto L_0x0153
            r1.close()     // Catch:{ Exception -> 0x0240 }
        L_0x0153:
            r12.j()
            r0 = r8
        L_0x0157:
            int r1 = r12.e
            int r1 = r1 + 1
            r12.e = r1
            r2 = r0
            goto L_0x005f
        L_0x0160:
            java.lang.String r0 = r12.l     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            if (r0 == 0) goto L_0x0215
            java.net.HttpURLConnection r0 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r1 = "POST"
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r0 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r1 = 1
            r0.setDoOutput(r1)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r0 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r1 = "Content-Type"
            java.lang.String r4 = r12.a     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r0.setRequestProperty(r1, r4)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r0 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r1 = "Content-Length"
            java.lang.String r4 = r12.l     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            int r4 = r4.length()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r0.setRequestProperty(r1, r4)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.net.HttpURLConnection r0 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.io.OutputStream r0 = r0.getOutputStream()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.io.BufferedWriter r1 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.io.OutputStreamWriter r4 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r0 = 4096(0x1000, float:5.74E-42)
            r1.<init>(r4, r0)     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            java.lang.String r0 = r12.l     // Catch:{ Exception -> 0x024c }
            r1.write(r0)     // Catch:{ Exception -> 0x024c }
            r1.close()     // Catch:{ Exception -> 0x024c }
            r0 = 0
        L_0x01a6:
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            int r1 = r1.getResponseCode()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r3 = "AdMobSDK"
            r4 = 2
            boolean r3 = com.admob.android.ads.bh.a(r3, r4)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            if (r3 == 0) goto L_0x01d7
            java.net.HttpURLConnection r3 = r12.m     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r4 = "X-AdMob-AdSrc"
            java.lang.String r3 = r3.getHeaderField(r4)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            if (r3 == 0) goto L_0x01d7
            java.lang.String r4 = "AdMobSDK"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r5.<init>()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r6 = "Ad response came from server "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            android.util.Log.v(r4, r3)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
        L_0x01d7:
            r3 = 200(0xc8, float:2.8E-43)
            if (r1 < r3) goto L_0x0028
            r3 = 300(0x12c, float:4.2E-43)
            if (r1 >= r3) goto L_0x0028
            java.net.HttpURLConnection r1 = r12.m     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.net.URL r1 = r1.getURL()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r12.i = r1     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            boolean r1 = r12.k     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            if (r1 == 0) goto L_0x0222
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.net.HttpURLConnection r2 = r12.m     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.io.InputStream r2 = r2.getInputStream()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r3 = 4096(0x1000, float:5.74E-42)
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r2 = 4096(0x1000, float:5.74E-42)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r4 = 4096(0x1000, float:5.74E-42)
            r3.<init>(r4)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
        L_0x0203:
            int r4 = r1.read(r2)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r5 = -1
            if (r4 == r5) goto L_0x021c
            r5 = 0
            r3.write(r2, r5, r4)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            goto L_0x0203
        L_0x020f:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x010d
        L_0x0215:
            java.net.HttpURLConnection r0 = r12.m     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r0.connect()     // Catch:{ Exception -> 0x010b, all -> 0x0235 }
            r0 = r3
            goto L_0x01a6
        L_0x021c:
            byte[] r1 = r3.toByteArray()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r12.j = r1     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
        L_0x0222:
            com.admob.android.ads.b r1 = r12.h     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            if (r1 == 0) goto L_0x022b
            com.admob.android.ads.b r1 = r12.h     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r1.a(r12)     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
        L_0x022b:
            r1 = r9
        L_0x022c:
            r12.j()     // Catch:{ Exception -> 0x020f, all -> 0x0247 }
            r12.j()
            r0 = r1
            goto L_0x0157
        L_0x0235:
            r0 = move-exception
            r1 = r3
        L_0x0237:
            if (r1 == 0) goto L_0x023c
            r1.close()     // Catch:{ Exception -> 0x0243 }
        L_0x023c:
            r12.j()
            throw r0
        L_0x0240:
            r0 = move-exception
            goto L_0x0153
        L_0x0243:
            r1 = move-exception
            goto L_0x023c
        L_0x0245:
            r0 = move-exception
            goto L_0x0237
        L_0x0247:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x0237
        L_0x024c:
            r0 = move-exception
            goto L_0x010d
        L_0x024f:
            r1 = r2
            goto L_0x022c
        L_0x0251:
            r0 = r3
            r1 = r2
            goto L_0x022c
        L_0x0254:
            r0 = r2
            goto L_0x001a
        L_0x0257:
            r2 = r8
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.c.a():boolean");
    }

    public final void b() {
        j();
        this.h = null;
    }

    public final void run() {
        try {
            a();
        } catch (Exception e) {
            if (bh.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "exception caught in AdMobURLConnector.run(), " + e.getMessage());
            }
            if (this.h != null) {
                this.h.a(this, this.c);
            }
        }
    }
}
