package com.admob.android.ads;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class ac implements i {
    public ci a = ci.CLICK_TO_BROWSER;
    public String b = "";
    public Vector c = new Vector();
    public String d = null;
    public be e = be.ANY;
    public boolean f = false;
    public Point g = new Point(4, 4);
    public y h = null;
    public String i = null;
    public String j = null;
    public Bundle k = new Bundle();
    public boolean l = false;
    private boolean m = false;
    private Point n = new Point(0, 0);
    private String o = null;

    private static Point a(int[] iArr) {
        if (iArr == null || iArr.length == 2) {
            return null;
        }
        return new Point(iArr[0], iArr[1]);
    }

    public static boolean a(byte b2) {
        return b2 == 1;
    }

    private static int[] a(Point point) {
        if (point == null) {
            return null;
        }
        return new int[]{point.x, point.y};
    }

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("a", this.a.toString());
        bundle.putString("t", this.b);
        bundle.putParcelableArrayList("c", ad.a(this.c));
        bundle.putString("u", this.d);
        bundle.putInt("or", this.e.ordinal());
        bundle.putByte("tr", this.m ? (byte) 1 : 0);
        bundle.putByte("sc", this.f ? (byte) 1 : 0);
        bundle.putIntArray("cbo", a(this.g));
        bundle.putIntArray("cs", a(this.n));
        bundle.putBundle("mi", ad.a(this.h));
        bundle.putString("su", this.i);
        bundle.putString("si", this.j);
        bundle.putString("json", this.o);
        bundle.putBundle("$", this.k);
        bundle.putByte("int", this.l ? (byte) 1 : 0);
        return bundle;
    }

    public final void a(String str, boolean z) {
        if (str != null && !"".equals(str)) {
            this.c.add(new q(str, z));
        }
    }

    public final void a(JSONObject jSONObject, ai aiVar, String str) {
        this.a = ci.a(jSONObject.optString("a"));
        a(jSONObject.optString("au"), true);
        a(jSONObject.optString("tu"), false);
        JSONObject optJSONObject = jSONObject.optJSONObject("stats");
        if (optJSONObject != null) {
            this.i = optJSONObject.optString("url");
            this.j = optJSONObject.optString("id");
        }
        String optString = jSONObject.optString("or");
        if (optString != null && !optString.equals("")) {
            if ("l".equals(optString)) {
                this.e = be.LANDSCAPE;
            } else {
                this.e = be.PORTRAIT;
            }
        }
        this.m = jSONObject.opt("t") != null;
        this.b = jSONObject.optString("title");
        if (this.a == ci.CLICK_TO_INTERACTIVE_VIDEO) {
            this.h = new y();
            JSONObject optJSONObject2 = jSONObject.optJSONObject("$");
            if (aiVar != null) {
                try {
                    aiVar.a(optJSONObject2, str);
                } catch (JSONException e2) {
                }
            }
            this.h.a = jSONObject.optString("u");
            this.h.b = jSONObject.optString("title");
            this.h.c = jSONObject.optInt("mc", 2);
            this.h.d = jSONObject.optInt("msm", 0);
            this.h.e = jSONObject.optString("stats");
            this.h.f = jSONObject.optString("splash");
            this.h.g = jSONObject.optDouble("splash_duration", 1.5d);
            this.h.h = jSONObject.optString("skip_down");
            this.h.i = jSONObject.optString("skip_up");
            this.h.j = jSONObject.optBoolean("no_splash_skip");
            this.h.k = jSONObject.optString("replay_down");
            this.h.l = jSONObject.optString("replay_up");
            JSONArray optJSONArray = jSONObject.optJSONArray("buttons");
            if (optJSONArray != null) {
                int length = optJSONArray.length();
                for (int i2 = 0; i2 < length; i2++) {
                    JSONObject optJSONObject3 = optJSONArray.optJSONObject(i2);
                    aa aaVar = new aa();
                    aaVar.a = optJSONObject3.optString("$");
                    aaVar.b = optJSONObject3.optString("h");
                    aaVar.c = optJSONObject3.optString("x");
                    aaVar.e = optJSONObject3.optString("analytics_page_name");
                    aaVar.d.a(optJSONObject3.optJSONObject("o"), aiVar, str);
                    aaVar.f = optJSONObject3.optJSONObject("o").toString();
                    this.h.m.add(aaVar);
                }
            }
        }
        this.f = jSONObject.optInt("sc", 0) != 0;
        JSONArray optJSONArray2 = jSONObject.optJSONArray("co");
        if (optJSONArray2 != null && optJSONArray2.length() >= 2) {
            this.g = new Point(optJSONArray2.optInt(0), optJSONArray2.optInt(1));
        }
        this.o = jSONObject.toString();
    }

    public final boolean a(Bundle bundle) {
        if (bundle == null) {
            return false;
        }
        this.a = ci.a(bundle.getString("a"));
        this.b = bundle.getString("t");
        this.c = new Vector();
        ArrayList parcelableArrayList = bundle.getParcelableArrayList("c");
        if (parcelableArrayList != null) {
            Iterator it = parcelableArrayList.iterator();
            while (it.hasNext()) {
                Bundle bundle2 = (Bundle) it.next();
                if (bundle2 != null) {
                    q qVar = new q();
                    qVar.a = bundle2.getString("u");
                    qVar.b = bundle2.getBoolean("p", false);
                    this.c.add(qVar);
                }
            }
        }
        this.d = bundle.getString("u");
        this.e = be.a(bundle.getInt("or"));
        this.m = a(bundle.getByte("tr"));
        this.f = a(bundle.getByte("sc"));
        this.g = a(bundle.getIntArray("cbo"));
        if (this.g == null) {
            this.g = new Point(4, 4);
        }
        this.n = a(bundle.getIntArray("cs"));
        y yVar = new y();
        if (yVar.a(bundle.getBundle("mi"))) {
            this.h = yVar;
        } else {
            this.h = null;
        }
        this.i = bundle.getString("su");
        this.j = bundle.getString("si");
        this.o = bundle.getString("json");
        this.k = bundle.getBundle("$");
        this.l = a(bundle.getByte("int"));
        return true;
    }

    public final Hashtable b() {
        Set<String> keySet = this.k.keySet();
        Hashtable hashtable = new Hashtable();
        for (String next : keySet) {
            Parcelable parcelable = this.k.getParcelable(next);
            if (parcelable instanceof Bitmap) {
                hashtable.put(next, (Bitmap) parcelable);
            }
        }
        return hashtable;
    }
}
