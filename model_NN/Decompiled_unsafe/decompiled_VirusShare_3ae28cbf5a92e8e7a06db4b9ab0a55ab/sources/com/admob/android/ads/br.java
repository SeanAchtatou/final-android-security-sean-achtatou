package com.admob.android.ads;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.util.Timer;

public final class br {
    private static Handler a = null;
    private static Timer b = null;
    private static bg c = null;
    private bv d;
    private WeakReference e;
    private boolean f;
    /* access modifiers changed from: private */
    public d g;
    private String h;
    private String i;
    private bh j;
    private long k;

    /* access modifiers changed from: package-private */
    public final void a() {
        if (a != null) {
            a.post(new bj(this));
        }
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        if (b != null) {
            b.cancel();
            b = null;
        }
        if (this.k != -1 && bh.a("AdMobSDK", 2)) {
            Log.v("AdMobSDK", "total request time: " + (SystemClock.uptimeMillis() - this.k));
        }
        this.f = true;
        c = null;
        if (((bp) this.e.get()) != null) {
        }
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        if (a != null) {
            a.post(new bi(this));
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        c = null;
        if (((bp) this.e.get()) != null) {
        }
    }

    /* access modifiers changed from: package-private */
    public final bv e() {
        return this.d;
    }

    public final String f() {
        return this.i;
    }

    public final String g() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public final bh h() {
        return this.j;
    }
}
