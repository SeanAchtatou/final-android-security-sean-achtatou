package com.admob.android.ads;

import android.os.Bundle;

public final class aa implements i {
    public String a;
    public String b;
    public String c;
    public ac d = new ac();
    public String e;
    public String f;

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("ad", this.a);
        bundle.putString("au", this.b);
        bundle.putString("t", this.c);
        bundle.putBundle("oi", ad.a(this.d));
        bundle.putString("ap", this.e);
        bundle.putString("json", this.f);
        return bundle;
    }
}
