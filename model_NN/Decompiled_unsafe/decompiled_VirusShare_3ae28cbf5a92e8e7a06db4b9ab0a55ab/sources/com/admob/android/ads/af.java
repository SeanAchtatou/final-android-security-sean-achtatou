package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

public final class af implements b {
    private static af a = null;
    private static Context b = null;
    private static Thread c = null;
    private static String d = null;
    private Properties e = null;
    private Context f;

    private af(Context context) {
        this.f = context;
        d = a();
        if (a != null) {
            a.e = null;
        }
        if (!b() && c == null) {
            Thread thread = new Thread(a.a("http://mm.admob.com/static/android/i18n/20101109" + "/" + d + ".properties", ak.g(this.f), this));
            c = thread;
            thread.start();
        }
    }

    private static File a(Context context, String str) {
        File file = new File(context.getCacheDir(), "admob_cache");
        if (!file.exists()) {
            file.mkdir();
        }
        File file2 = new File(file, "20101109");
        if (!file2.exists()) {
            file2.mkdir();
        }
        return new File(file2, str + ".properties");
    }

    public static String a() {
        if (d == null) {
            String language = Locale.getDefault().getLanguage();
            d = language;
            if (language == null) {
                d = "en";
            }
        }
        return d;
    }

    public static String a(String str) {
        a(b);
        af afVar = a;
        afVar.b();
        if (afVar.e == null) {
            return str;
        }
        String property = afVar.e.getProperty(str);
        return (property == null || property.equals("")) ? str : property;
    }

    public static void a(Context context) {
        if (b == null && context != null) {
            b = context.getApplicationContext();
        }
        if (a == null) {
            a = new af(b);
        }
    }

    private boolean b() {
        if (this.e == null) {
            try {
                Properties properties = new Properties();
                File a2 = a(this.f, d);
                if (a2.exists()) {
                    properties.load(new FileInputStream(a2));
                    this.e = properties;
                }
            } catch (IOException e2) {
                this.e = null;
            }
        }
        return this.e != null;
    }

    public final void a(n nVar) {
        try {
            byte[] c2 = nVar.c();
            if (c2 != null) {
                FileOutputStream fileOutputStream = new FileOutputStream(a(this.f, d));
                fileOutputStream.write(c2);
                fileOutputStream.close();
            }
        } catch (Exception e2) {
            if (bh.a("AdMobSDK", 3)) {
                Log.d("AdMobSDK", "Could not store localized strings to cache file.");
            }
        }
    }

    public final void a(n nVar, Exception exc) {
        if (bh.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Could not get localized strings from the AdMob servers.");
        }
    }
}
