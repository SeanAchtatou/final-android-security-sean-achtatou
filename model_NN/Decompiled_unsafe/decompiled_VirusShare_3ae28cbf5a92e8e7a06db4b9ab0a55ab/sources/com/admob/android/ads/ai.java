package com.admob.android.ads;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import org.json.JSONObject;

public final class ai implements b {
    public Hashtable a = new Hashtable();
    public HashSet b = new HashSet();
    public ag c = null;
    public WeakReference d;
    private bq e;

    public ai(bq bqVar) {
        this.e = bqVar;
        this.d = null;
    }

    private void a(String str, String str2, String str3, boolean z) {
        n a2 = a.a(str, str2, str3, this);
        if (z) {
            a2.a((Object) true);
        }
        this.b.add(a2);
    }

    public final void a(n nVar) {
        String d2 = nVar.d();
        byte[] c2 = nVar.c();
        if (c2 != null) {
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeByteArray(c2, 0, c2.length);
            } catch (Throwable th) {
                if (bh.a("AdMobSDK", 6)) {
                    Log.e("AdMobSDK", "couldn't create a Bitmap", th);
                }
            }
            if (bitmap != null) {
                Object h = nVar.h();
                if ((h instanceof Boolean) && ((Boolean) h).booleanValue()) {
                    this.c.a(d2, bitmap);
                }
                this.a.put(d2, bitmap);
                if (this.b != null) {
                    synchronized (this.b) {
                        this.b.remove(nVar);
                    }
                    if (a() && this.e != null) {
                        this.e.j();
                        return;
                    }
                    return;
                }
                return;
            }
            if (bh.a("AdMobSDK", 3)) {
                Log.d("AdMobSDK", "Failed reading asset(" + d2 + ") as a bitmap.");
            }
            c();
            return;
        }
        if (bh.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Failed reading asset(" + d2 + ") for ad");
        }
        c();
    }

    public final void a(n nVar, Exception exc) {
        String str;
        String str2;
        String str3;
        String str4;
        if (exc != null) {
            if (bh.a("AdMobSDK", 3)) {
                if (nVar != null) {
                    String d2 = nVar.d();
                    URL e2 = nVar.e();
                    if (e2 != null) {
                        String url = e2.toString();
                        str4 = d2;
                        str3 = url;
                    } else {
                        str4 = d2;
                        str3 = null;
                    }
                } else {
                    str3 = null;
                    str4 = null;
                }
                Log.d("AdMobSDK", "Failed downloading assets for ad: " + str4 + " " + str3, exc);
            }
        } else if (bh.a("AdMobSDK", 3)) {
            if (nVar != null) {
                String d3 = nVar.d();
                URL e3 = nVar.e();
                if (e3 != null) {
                    String url2 = e3.toString();
                    str2 = d3;
                    str = url2;
                } else {
                    str2 = d3;
                    str = null;
                }
            } else {
                str = null;
                str2 = null;
            }
            Log.d("AdMobSDK", "Failed downloading assets for ad: " + str2 + " " + str);
        }
        c();
    }

    public final void a(JSONObject jSONObject, String str) {
        if (this.b != null) {
            synchronized (this.b) {
                if (jSONObject != null) {
                    Iterator<String> keys = jSONObject.keys();
                    while (keys.hasNext()) {
                        String next = keys.next();
                        JSONObject jSONObject2 = jSONObject.getJSONObject(next);
                        String string = jSONObject2.getString("u");
                        if (!(jSONObject2.optInt("c", 0) == 1) || this.c == null) {
                            a(string, next, str, false);
                        } else {
                            Bitmap a2 = this.c.a(next);
                            if (a2 != null) {
                                this.a.put(next, a2);
                            } else {
                                a(string, next, str, true);
                            }
                        }
                    }
                }
            }
        }
    }

    public final boolean a() {
        return this.b == null || this.b.size() == 0;
    }

    public final void b() {
        if (this.b != null) {
            synchronized (this.b) {
                Iterator it = this.b.iterator();
                while (it.hasNext()) {
                    ((n) it.next()).f();
                }
            }
        }
    }

    public final void c() {
        if (this.b != null) {
            synchronized (this.b) {
                Iterator it = this.b.iterator();
                while (it.hasNext()) {
                    ((n) it.next()).b();
                }
                this.b.clear();
                this.b = null;
            }
        }
        d();
        if (this.e != null) {
            this.e.k();
        }
    }

    public final void d() {
        if (this.a != null) {
            this.a.clear();
            this.a = null;
        }
    }
}
