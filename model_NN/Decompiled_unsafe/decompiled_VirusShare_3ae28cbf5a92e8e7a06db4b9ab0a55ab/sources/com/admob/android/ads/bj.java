package com.admob.android.ads;

import java.lang.ref.WeakReference;

final class bj implements Runnable {
    private WeakReference a;

    public bj(br brVar) {
        this.a = new WeakReference(brVar);
    }

    public final void run() {
        br brVar = (br) this.a.get();
        if (brVar != null) {
            brVar.b();
        }
    }
}
