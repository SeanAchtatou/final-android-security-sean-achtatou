package com.admob.android.ads;

public enum ch {
    VIEW("view"),
    INTERSTITIAL("full_screen"),
    BAR("bar");
    
    private String d;

    private ch(String str) {
        this.d = str;
    }

    public final String toString() {
        return this.d;
    }
}
