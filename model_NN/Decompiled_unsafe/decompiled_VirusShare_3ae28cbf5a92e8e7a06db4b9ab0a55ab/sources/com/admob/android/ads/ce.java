package com.admob.android.ads;

import android.view.View;
import java.lang.ref.WeakReference;
import java.util.Map;

public final class ce implements View.OnClickListener {
    private WeakReference a;

    public ce(bd bdVar) {
        this.a = new WeakReference(bdVar);
    }

    public final void onClick(View view) {
        bd bdVar = (bd) this.a.get();
        if (bdVar != null) {
            bdVar.f.a("replay", (Map) null);
            if (bdVar.d != null) {
                bd.b(bdVar.d);
            }
            bdVar.b(false);
            bdVar.h = true;
            bdVar.a(bdVar.getContext());
        }
    }
}
