package com.admob.android.ads;

import android.util.Log;
import java.lang.ref.WeakReference;

final class w implements Runnable {
    private WeakReference a;
    private WeakReference b;
    private int c;
    private boolean d;

    public w(AdView adView, f fVar, int i, boolean z) {
        this.a = new WeakReference(adView);
        this.b = new WeakReference(fVar);
        this.c = i;
        this.d = z;
    }

    public final void run() {
        try {
            AdView adView = (AdView) this.a.get();
            f fVar = (f) this.b.get();
            if (adView != null && fVar != null) {
                adView.addView(fVar);
                AdView.j(adView);
                if (this.c != 0) {
                    f unused = adView.b = fVar;
                } else if (this.d) {
                    adView.a(fVar);
                } else {
                    AdView.b(adView, fVar);
                }
            }
        } catch (Exception e) {
            if (bh.a("AdMobSDK", 6)) {
                Log.e("AdMobSDK", "Unhandled exception placing AdContainer into AdView.", e);
            }
        }
    }
}
