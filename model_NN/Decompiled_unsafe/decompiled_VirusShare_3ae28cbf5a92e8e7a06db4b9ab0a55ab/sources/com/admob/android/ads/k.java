package com.admob.android.ads;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

final class k {
    private static String a = "http://r.admob.com/ad_source.php";
    private static int b;
    private static long c;
    private static String d = null;
    private static boolean e = false;
    private static boolean f = false;

    k() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:66:0x01a5  */
    /* JADX WARNING: Removed duplicated region for block: B:75:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.admob.android.ads.d a(com.admob.android.ads.h r14, android.content.Context r15, java.lang.String r16, java.lang.String r17, int r18, int r19, int r20, com.admob.android.ads.f r21, int r22, com.admob.android.ads.ch r23, com.admob.android.ads.bv r24, com.admob.android.ads.u r25) {
        /*
            java.lang.String r3 = "android.permission.INTERNET"
            int r3 = r15.checkCallingOrSelfPermission(r3)
            r4 = -1
            if (r3 != r4) goto L_0x000e
            java.lang.String r3 = "Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />"
            com.admob.android.ads.ak.a(r3)
        L_0x000e:
            boolean r3 = com.admob.android.ads.k.f
            if (r3 != 0) goto L_0x004c
            r3 = 1
            com.admob.android.ads.k.f = r3
            r3 = 1
            android.content.pm.PackageManager r4 = r15.getPackageManager()
            android.content.Intent r5 = new android.content.Intent
            java.lang.Class<com.admob.android.ads.AdMobActivity> r6 = com.admob.android.ads.AdMobActivity.class
            r5.<init>(r15, r6)
            r6 = 65536(0x10000, float:9.18355E-41)
            android.content.pm.ResolveInfo r4 = r4.resolveActivity(r5, r6)
            if (r4 == 0) goto L_0x0039
            android.content.pm.ActivityInfo r5 = r4.activityInfo
            if (r5 == 0) goto L_0x0039
            java.lang.String r5 = "com.admob.android.ads.AdMobActivity"
            android.content.pm.ActivityInfo r6 = r4.activityInfo
            java.lang.String r6 = r6.name
            boolean r5 = r5.equals(r6)
            if (r5 != 0) goto L_0x0062
        L_0x0039:
            java.lang.String r3 = "AdMobSDK"
            r4 = 6
            boolean r3 = com.admob.android.ads.bh.a(r3, r4)
            if (r3 == 0) goto L_0x0049
            java.lang.String r3 = "AdMobSDK"
            java.lang.String r4 = "could not find com.admob.android.ads.AdMobActivity, please make sure it is registered in AndroidManifest.xml"
            android.util.Log.e(r3, r4)
        L_0x0049:
            r3 = 0
        L_0x004a:
            com.admob.android.ads.k.e = r3
        L_0x004c:
            boolean r3 = com.admob.android.ads.k.e
            if (r3 != 0) goto L_0x00c8
            java.lang.String r14 = "AdMobSDK"
            r15 = 6
            boolean r14 = com.admob.android.ads.bh.a(r14, r15)
            if (r14 == 0) goto L_0x0060
            java.lang.String r14 = "AdMobSDK"
            java.lang.String r15 = "com.admob.android.ads.AdMobActivity must be registered in your AndroidManifest.xml file."
            android.util.Log.e(r14, r15)
        L_0x0060:
            r14 = 0
        L_0x0061:
            return r14
        L_0x0062:
            android.content.pm.ActivityInfo r5 = r4.activityInfo
            int r5 = r5.theme
            r6 = 16973831(0x1030007, float:2.406092E-38)
            if (r5 == r6) goto L_0x007c
            java.lang.String r3 = "AdMobSDK"
            r5 = 6
            boolean r3 = com.admob.android.ads.bh.a(r3, r5)
            if (r3 == 0) goto L_0x007b
            java.lang.String r3 = "AdMobSDK"
            java.lang.String r5 = "The activity Theme for com.admob.android.ads.AdMobActivity is not @android:style/Theme.NoTitleBar.Fullscreen, please change in AndroidManifest.xml"
            android.util.Log.e(r3, r5)
        L_0x007b:
            r3 = 0
        L_0x007c:
            android.content.pm.ActivityInfo r5 = r4.activityInfo
            int r5 = r5.configChanges
            r5 = r5 & 128(0x80, float:1.794E-43)
            if (r5 != 0) goto L_0x0095
            java.lang.String r3 = "AdMobSDK"
            r5 = 6
            boolean r3 = com.admob.android.ads.bh.a(r3, r5)
            if (r3 == 0) goto L_0x0094
            java.lang.String r3 = "AdMobSDK"
            java.lang.String r5 = "The android:configChanges value of the com.admob.android.ads.AdMobActivity must include orientation"
            android.util.Log.e(r3, r5)
        L_0x0094:
            r3 = 0
        L_0x0095:
            android.content.pm.ActivityInfo r5 = r4.activityInfo
            int r5 = r5.configChanges
            r5 = r5 & 16
            if (r5 != 0) goto L_0x00ae
            java.lang.String r3 = "AdMobSDK"
            r5 = 6
            boolean r3 = com.admob.android.ads.bh.a(r3, r5)
            if (r3 == 0) goto L_0x00ad
            java.lang.String r3 = "AdMobSDK"
            java.lang.String r5 = "The android:configChanges value of the com.admob.android.ads.AdMobActivity must include keyboard"
            android.util.Log.e(r3, r5)
        L_0x00ad:
            r3 = 0
        L_0x00ae:
            android.content.pm.ActivityInfo r4 = r4.activityInfo
            int r4 = r4.configChanges
            r4 = r4 & 32
            if (r4 != 0) goto L_0x004a
            java.lang.String r3 = "AdMobSDK"
            r4 = 6
            boolean r3 = com.admob.android.ads.bh.a(r3, r4)
            if (r3 == 0) goto L_0x00c6
            java.lang.String r3 = "AdMobSDK"
            java.lang.String r4 = "The android:configChanges value of the com.admob.android.ads.AdMobActivity must include keyboardHidden"
            android.util.Log.e(r3, r4)
        L_0x00c6:
            r3 = 0
            goto L_0x004a
        L_0x00c8:
            com.admob.android.ads.af.a(r15)
            r11 = 0
            long r12 = android.os.SystemClock.uptimeMillis()
            r3 = r15
            r4 = r16
            r5 = r17
            r6 = r22
            r7 = r23
            r8 = r24
            r9 = r25
            java.lang.String r8 = a(r3, r4, r5, r6, r7, r8, r9)
            java.lang.String r3 = com.admob.android.ads.k.a
            r4 = 0
            java.lang.String r5 = com.admob.android.ads.ak.g(r15)
            r6 = 0
            r7 = 3000(0xbb8, float:4.204E-42)
            com.admob.android.ads.n r16 = com.admob.android.ads.a.a(r3, r4, r5, r6, r7, r8)
            java.lang.String r17 = "AdMobSDK"
            r22 = 3
            r0 = r17
            r1 = r22
            boolean r17 = com.admob.android.ads.bh.a(r0, r1)
            if (r17 == 0) goto L_0x0120
            java.lang.String r17 = "AdMobSDK"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder
            r22.<init>()
            java.lang.String r24 = "Requesting an ad with POST params:  "
            r0 = r22
            r1 = r24
            java.lang.StringBuilder r22 = r0.append(r1)
            r0 = r22
            r1 = r8
            java.lang.StringBuilder r22 = r0.append(r1)
            java.lang.String r22 = r22.toString()
            r0 = r17
            r1 = r22
            android.util.Log.d(r0, r1)
        L_0x0120:
            r17 = 0
            boolean r22 = r16.a()
            if (r22 == 0) goto L_0x0208
            byte[] r16 = r16.c()
            java.lang.String r17 = new java.lang.String
            r0 = r17
            r1 = r16
            r0.<init>(r1)
            r16 = r17
        L_0x0137:
            if (r22 == 0) goto L_0x0206
            java.lang.String r17 = "AdMobSDK"
            r22 = 3
            r0 = r17
            r1 = r22
            boolean r17 = com.admob.android.ads.bh.a(r0, r1)
            if (r17 == 0) goto L_0x0152
            java.lang.String r17 = "AdMobSDK"
            java.lang.String r22 = "Ad response: "
            r0 = r17
            r1 = r22
            android.util.Log.d(r0, r1)
        L_0x0152:
            java.lang.String r17 = ""
            boolean r17 = r16.equals(r17)
            if (r17 != 0) goto L_0x0206
            org.json.JSONTokener r17 = new org.json.JSONTokener
            r0 = r17
            r1 = r16
            r0.<init>(r1)
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x01d2 }
            r0 = r5
            r1 = r17
            r0.<init>(r1)     // Catch:{ JSONException -> 0x01d2 }
            java.lang.String r17 = "AdMobSDK"
            r22 = 3
            r0 = r17
            r1 = r22
            boolean r17 = com.admob.android.ads.bh.a(r0, r1)     // Catch:{ JSONException -> 0x01d2 }
            if (r17 == 0) goto L_0x018b
            java.lang.String r17 = "AdMobSDK"
            r22 = 4
            r0 = r5
            r1 = r22
            java.lang.String r22 = r0.toString(r1)     // Catch:{ JSONException -> 0x01d2 }
            r0 = r17
            r1 = r22
            android.util.Log.d(r0, r1)     // Catch:{ JSONException -> 0x01d2 }
        L_0x018b:
            r3 = r14
            r4 = r15
            r6 = r18
            r7 = r19
            r8 = r20
            r9 = r21
            r10 = r23
            com.admob.android.ads.d r14 = com.admob.android.ads.d.a(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ JSONException -> 0x01d2 }
        L_0x019b:
            java.lang.String r15 = "AdMobSDK"
            r16 = 4
            boolean r15 = com.admob.android.ads.bh.a(r15, r16)
            if (r15 == 0) goto L_0x0061
            long r15 = android.os.SystemClock.uptimeMillis()
            long r15 = r15 - r12
            if (r14 != 0) goto L_0x0061
            java.lang.String r17 = "AdMobSDK"
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            r18.<init>()
            java.lang.String r19 = "No fill.  Server replied that no ads are available ("
            java.lang.StringBuilder r18 = r18.append(r19)
            r0 = r18
            r1 = r15
            java.lang.StringBuilder r15 = r0.append(r1)
            java.lang.String r16 = "ms)"
            java.lang.StringBuilder r15 = r15.append(r16)
            java.lang.String r15 = r15.toString()
            r0 = r17
            r1 = r15
            android.util.Log.i(r0, r1)
            goto L_0x0061
        L_0x01d2:
            r14 = move-exception
            java.lang.String r15 = "AdMobSDK"
            r17 = 5
            r0 = r15
            r1 = r17
            boolean r15 = com.admob.android.ads.bh.a(r0, r1)
            if (r15 == 0) goto L_0x0206
            java.lang.String r15 = "AdMobSDK"
            java.lang.StringBuilder r17 = new java.lang.StringBuilder
            r17.<init>()
            java.lang.String r18 = "Problem decoding ad response.  Cannot display ad: \""
            java.lang.StringBuilder r17 = r17.append(r18)
            r0 = r17
            r1 = r16
            java.lang.StringBuilder r16 = r0.append(r1)
            java.lang.String r17 = "\""
            java.lang.StringBuilder r16 = r16.append(r17)
            java.lang.String r16 = r16.toString()
            r0 = r15
            r1 = r16
            r2 = r14
            android.util.Log.w(r0, r1, r2)
        L_0x0206:
            r14 = r11
            goto L_0x019b
        L_0x0208:
            r16 = r17
            goto L_0x0137
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.k.a(com.admob.android.ads.h, android.content.Context, java.lang.String, java.lang.String, int, int, int, com.admob.android.ads.f, int, com.admob.android.ads.ch, com.admob.android.ads.bv, com.admob.android.ads.u):com.admob.android.ads.d");
    }

    static d a(h hVar, Context context, String str, String str2, bv bvVar) {
        return a(hVar, context, str, str2, -1, -1, -1, null, -1, ch.INTERSTITIAL, bvVar, null);
    }

    static String a(Context context) {
        return a(context, null, null, 0, null, null, null);
    }

    private static String a(Context context, String str, String str2, int i, ch chVar, bv bvVar, u uVar) {
        if (bh.a("AdMobSDK", 3)) {
            Log.d("AdMobSDK", "Ad request:");
        }
        ak.a(context);
        StringBuilder sb = new StringBuilder();
        long currentTimeMillis = System.currentTimeMillis();
        sb.append("z").append("=").append(currentTimeMillis / 1000).append(".").append(currentTimeMillis % 1000);
        ch chVar2 = chVar == null ? ch.BAR : chVar;
        a(sb, "ad_type", chVar2.toString());
        switch (bx.a[chVar2.ordinal()]) {
            case 1:
                if (bvVar != null) {
                    a(sb, "event", String.valueOf(bvVar.ordinal()));
                    break;
                }
                break;
            case 2:
                if (uVar != null) {
                    a(sb, "dim", uVar.toString());
                    break;
                }
                break;
        }
        a(sb, "rt", "0");
        String str3 = null;
        if (chVar == ch.INTERSTITIAL) {
            str3 = ak.e(context);
        }
        if (str3 == null) {
            str3 = ak.d(context);
        }
        if (str3 == null) {
            throw new IllegalStateException("Publisher ID is not set!  To serve ads you must set your publisher ID assigned from www.admob.com.  Either add it to AndroidManifest.xml under the <application> tag or call AdManager.setPublisherId().");
        }
        a(sb, "s", str3);
        a(sb, "l", af.a());
        a(sb, "f", "jsonp");
        a(sb, "client_sdk", "1");
        a(sb, "ex", "1");
        a(sb, "v", "20101109-ANDROID-3312276cc1406347");
        a(sb, "isu", ak.g(context));
        a(sb, "so", ak.i(context));
        if (i > 0) {
            a(sb, "screen_width", String.valueOf(i));
        }
        a(sb, "d[coord]", ak.h(context));
        a(sb, "d[coord_timestamp]", ak.c());
        a(sb, "d[pc]", ak.d());
        a(sb, "d[dob]", ak.e());
        a(sb, "d[gender]", ak.f());
        a(sb, "k", str);
        a(sb, "search", str2);
        a(sb, "density", String.valueOf(f.c()));
        if (ak.f(context)) {
            if (bh.a("AdMobSDK", 4)) {
                Log.i("AdMobSDK", "Making ad request in test mode");
            }
            a(sb, "m", "test");
            String a2 = ak.a();
            if (chVar == ch.INTERSTITIAL && ci.CLICK_TO_BROWSER.toString().equals(a2)) {
                a2 = "video_int";
            }
            a(sb, "test_action", a2);
        }
        if (d == null) {
            StringBuilder sb2 = new StringBuilder();
            PackageManager packageManager = context.getPackageManager();
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("geo:0,0?q=donuts")), 65536);
            if (queryIntentActivities == null || queryIntentActivities.size() == 0) {
                sb2.append("m");
            }
            List<ResolveInfo> queryIntentActivities2 = packageManager.queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:com.admob")), 65536);
            if (queryIntentActivities2 == null || queryIntentActivities2.size() == 0) {
                if (sb2.length() > 0) {
                    sb2.append(",");
                }
                sb2.append("a");
            }
            List<ResolveInfo> queryIntentActivities3 = packageManager.queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse("tel://6509313940")), 65536);
            if (queryIntentActivities3 == null || queryIntentActivities3.size() == 0) {
                if (sb2.length() > 0) {
                    sb2.append(",");
                }
                sb2.append("t");
            }
            d = sb2.toString();
        }
        String str4 = d;
        if (str4 != null && str4.length() > 0) {
            a(sb, "ic", str4);
        }
        a(sb, "audio", String.valueOf(ak.a(new ah(context)).ordinal()));
        int i2 = b + 1;
        b = i2;
        if (i2 == 1) {
            c = System.currentTimeMillis();
            a(sb, "pub_data[identifier]", ak.b(context));
            a(sb, "pub_data[version]", String.valueOf(ak.c(context)));
        } else {
            a(sb, "stats[reqs]", String.valueOf(b));
            a(sb, "stats[time]", String.valueOf((System.currentTimeMillis() - c) / 1000));
        }
        return sb.toString();
    }

    private static void a(StringBuilder sb, String str, String str2) {
        if (str2 != null && str2.length() > 0) {
            try {
                sb.append("&").append(URLEncoder.encode(str, "UTF-8")).append("=").append(URLEncoder.encode(str2, "UTF-8"));
                if (bh.a("AdMobSDK", 3)) {
                    Log.d("AdMobSDK", "    " + str + ": " + str2);
                }
            } catch (UnsupportedEncodingException e2) {
                if (bh.a("AdMobSDK", 6)) {
                    Log.e("AdMobSDK", "UTF-8 encoding is not supported on this device.  Ad requests are impossible.", e2);
                }
            }
        }
    }
}
