package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import java.lang.ref.WeakReference;

final class bg extends Thread {
    private br a;
    private WeakReference b;
    private boolean c;

    public final void run() {
        Context context = (Context) this.b.get();
        if (context != null) {
            try {
                d a2 = k.a(this.a.h(), context, this.a.f(), this.a.g(), this.a.e());
                if (!this.c && a2 == null) {
                    this.a.c();
                }
            } catch (Exception e) {
                if (bh.a("AdMobSDK", 6)) {
                    Log.e("AdMobSDK", "Unhandled exception requesting a fresh ad.", e);
                }
                if (!this.c) {
                    this.a.c();
                }
            }
        } else if (!this.c) {
            this.a.c();
        }
    }
}
