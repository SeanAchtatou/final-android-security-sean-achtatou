package com.admob.android.ads;

import java.lang.ref.WeakReference;

final class ck implements Runnable {
    private WeakReference a;

    public ck(bd bdVar) {
        this.a = new WeakReference(bdVar);
    }

    public final void run() {
        bd bdVar = (bd) this.a.get();
        if (bdVar != null) {
            bdVar.g();
            bdVar.d();
        }
    }
}
