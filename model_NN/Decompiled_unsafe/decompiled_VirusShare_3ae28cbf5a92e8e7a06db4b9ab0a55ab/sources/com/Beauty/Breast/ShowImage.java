package com.Beauty.Breast;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.ImageView;
import java.io.IOException;

public class ShowImage extends Activity {
    private ImageView a;
    private int b;
    private int c;
    private int d;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.showimage);
        this.d = getIntent().getIntExtra("ImageId", 1);
        this.a = (ImageView) findViewById(R.id.imageview);
        this.a.setImageResource(SexyImages.a[this.d]);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.menu_return_main).setIcon(17301580);
        menu.add(0, 2, 0, (int) R.string.menu_set_wallpaper).setIcon(17301585);
        menu.add(0, 3, 0, (int) R.string.menu_clear_wallpaper).setIcon(17301560);
        return true;
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 1:
                startActivity(new Intent(this, SexyImages.class));
                return true;
            case 2:
                try {
                    Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), SexyImages.a[this.d]);
                    setWallpaper(decodeResource);
                    decodeResource.recycle();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            case 3:
                try {
                    clearWallpaper();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                return true;
            default:
                return super.onMenuItemSelected(i, menuItem);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        int x = (int) motionEvent.getX();
        switch (action) {
            case 0:
                this.b = x;
                break;
            case 1:
                this.c = x;
                if (this.c - this.b > 0) {
                    if (this.d - 1 < 0) {
                        this.d = SexyImages.a.length - 1;
                    } else {
                        this.d--;
                    }
                } else if (this.d + 1 >= SexyImages.a.length) {
                    this.d = 0;
                } else {
                    this.d++;
                }
                this.a.setImageResource(SexyImages.a[this.d]);
                break;
        }
        this.a.invalidate();
        return true;
    }
}
