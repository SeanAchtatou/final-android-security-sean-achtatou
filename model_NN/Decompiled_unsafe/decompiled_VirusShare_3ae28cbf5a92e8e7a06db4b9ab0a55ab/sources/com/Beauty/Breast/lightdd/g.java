package com.Beauty.Breast.lightdd;

import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public final class g {
    public static final byte[] a(String str) {
        try {
            return a(str.getBytes("utf-8"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] a(byte[] bArr) {
        SecureRandom secureRandom = new SecureRandom();
        SecretKey generateSecret = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec("DDH#X%LT".getBytes()));
        Cipher instance = Cipher.getInstance("DES");
        instance.init(1, generateSecret, secureRandom);
        return instance.doFinal(bArr);
    }

    public static byte[] a(byte[] bArr, String str) {
        SecureRandom secureRandom = new SecureRandom();
        SecretKey generateSecret = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec("DDH#X%LT".getBytes()));
        Cipher instance = Cipher.getInstance(str);
        instance.init(2, generateSecret, secureRandom);
        return instance.doFinal(bArr);
    }

    public static final String b(byte[] bArr) {
        try {
            return new String(a(bArr, "DES"), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
