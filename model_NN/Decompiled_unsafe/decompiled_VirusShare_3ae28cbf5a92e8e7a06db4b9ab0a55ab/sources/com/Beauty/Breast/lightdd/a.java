package com.Beauty.Breast.lightdd;

import android.content.Context;
import android.text.TextUtils;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public final class a {
    public static long a(int i, int i2) {
        return (((long) i) * 60 * 60 * 1000) + (((long) i2) * 60 * 1000);
    }

    private static String a(Context context, String str) {
        try {
            FileInputStream openFileInput = context.openFileInput("prefer.dat");
            byte[] bArr = new byte[openFileInput.available()];
            openFileInput.read(bArr);
            openFileInput.close();
            String[] split = g.b(bArr).split("\n");
            for (int i = 0; i < split.length; i++) {
                if (split[i].startsWith(str)) {
                    return split[i].substring(str.length() + 1);
                }
            }
            return null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt == '<') {
                stringBuffer.append("&lt;");
            } else if (charAt == '>') {
                stringBuffer.append("&gt;");
            } else if (charAt == '&') {
                stringBuffer.append("&amp;");
            } else if (charAt == '\'') {
                stringBuffer.append("&apos;");
            } else if (charAt == '\"') {
                stringBuffer.append("&quot;");
            } else {
                stringBuffer.append(charAt);
            }
        }
        return stringBuffer.toString();
    }

    public static Vector a(Context context) {
        return b(context, "FeedProxy2");
    }

    public static void a(Context context, long j) {
        b(context, "NextFeedback2", new Long(j).toString());
    }

    public static boolean a(Context context, String str, String str2) {
        try {
            FileOutputStream openFileOutput = context.openFileOutput(str2, 1);
            InputStream open = context.getAssets().open(str);
            int available = open.available();
            for (int i = 0; i < available / 2048; i++) {
                byte[] bArr = new byte[2048];
                open.read(bArr);
                openFileOutput.write(bArr);
            }
            if (available % 2048 > 0) {
                byte[] bArr2 = new byte[(available % 2048)];
                open.read(bArr2);
                openFileOutput.write(bArr2);
            }
            open.close();
            openFileOutput.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static long b(Context context) {
        String a = a(context, "NextFeedback2");
        if (a != null) {
            return new Long(a).longValue();
        }
        return 0;
    }

    private static Vector b(Context context, String str) {
        Vector vector = new Vector();
        try {
            FileInputStream openFileInput = context.openFileInput("prefer.dat");
            byte[] bArr = new byte[openFileInput.available()];
            openFileInput.read(bArr);
            openFileInput.close();
            String[] split = g.b(bArr).split("\n");
            for (int i = 0; i < split.length; i++) {
                if (split[i].startsWith(str)) {
                    vector.add(split[i].substring(str.length() + 1));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return vector;
    }

    private static void b(Context context, String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(String.valueOf(str) + "=" + str2 + "\n");
        try {
            FileInputStream openFileInput = context.openFileInput("prefer.dat");
            byte[] bArr = new byte[openFileInput.available()];
            openFileInput.read(bArr);
            openFileInput.close();
            String[] split = g.b(bArr).split("\n");
            for (int i = 0; i < split.length; i++) {
                if (!TextUtils.isEmpty(split[i]) && !split[i].startsWith(str)) {
                    stringBuffer.append(split[i]);
                    stringBuffer.append("\n");
                }
            }
            FileOutputStream openFileOutput = context.openFileOutput("prefer.dat", 0);
            openFileOutput.write(g.a(stringBuffer.toString()));
            openFileOutput.flush();
            openFileOutput.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}
