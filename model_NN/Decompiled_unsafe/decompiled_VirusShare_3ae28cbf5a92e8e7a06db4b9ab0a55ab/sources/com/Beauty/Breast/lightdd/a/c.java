package com.Beauty.Breast.lightdd.a;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public final class c {
    private byte[] a;
    private byte[] b;
    private boolean c = false;
    private boolean d = false;
    private URL e;
    private String f = "";
    private boolean g;
    private boolean h = false;
    private String i = "";
    private String j = "POST";
    private String k = "application/octet-stream";
    private Context l;
    private Handler m;

    public c(Context context) {
        this.l = context;
    }

    private byte[] a(HttpURLConnection httpURLConnection) {
        if (this.g) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BufferedInputStream bufferedInputStream = new BufferedInputStream(httpURLConnection.getInputStream());
        byte[] bArr = new byte[10240];
        int contentLength = httpURLConnection.getContentLength();
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int read = bufferedInputStream.read(bArr, 0, 10240);
            if (read != -1 && !this.g) {
                if (this.m != null) {
                    i3 += read;
                    int i4 = (i3 * 100) / contentLength;
                    if (i4 > i2) {
                        if (!this.m.hasMessages(1)) {
                            this.m.sendMessageDelayed(this.m.obtainMessage(1, Integer.valueOf(i2)), 1000);
                        }
                        Log.d("leeeeeeeeeeeeeeeeeeength", "currentResponseLength: " + i3);
                    }
                    i2 = i4;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        }
        bufferedInputStream.close();
        return byteArrayOutputStream.toByteArray();
    }

    private void c(String str) {
        URL url = null;
        try {
            url = new URL(str);
        } catch (MalformedURLException e2) {
        }
        this.e = url;
    }

    private HttpURLConnection d() {
        HttpURLConnection httpURLConnection = (HttpURLConnection) this.e.openConnection();
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setRequestProperty("Accept", "*/*");
        httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
        httpURLConnection.setConnectTimeout(60000);
        httpURLConnection.setReadTimeout(60000);
        httpURLConnection.setRequestProperty("Content-Type", this.k);
        if (!this.i.equals("")) {
            httpURLConnection.setRequestProperty("Range", this.i);
        }
        if (!this.f.equals("")) {
            httpURLConnection.setRequestProperty("Referer", this.f);
        }
        try {
            httpURLConnection.setRequestMethod(this.j);
        } catch (ProtocolException e2) {
            e2.printStackTrace();
        }
        return httpURLConnection;
    }

    public final int a(byte[] bArr, String str) {
        b();
        this.a = bArr;
        c(str);
        new b(this).start();
        return 0;
    }

    public final void a(Handler handler) {
        this.m = handler;
    }

    public final void a(String str) {
        this.j = str;
    }

    public final synchronized byte[] a() {
        byte[] bArr;
        while (!this.c) {
            try {
                wait();
            } catch (InterruptedException e2) {
                bArr = null;
            }
        }
        bArr = (this.g || !this.d || this.b == null || this.b.length <= 0) ? null : this.b;
        return bArr;
    }

    public final int b(String str) {
        c(str);
        b();
        new e(this).start();
        return 0;
    }

    public final void b() {
        this.c = false;
        this.d = false;
        this.g = false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x006b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void c() {
        /*
            r7 = this;
            r1 = 0
            monitor-enter(r7)
            r0 = 0
            r7.b = r0     // Catch:{ all -> 0x0062 }
            java.net.HttpURLConnection r0 = r7.d()     // Catch:{ Exception -> 0x0077, all -> 0x0065 }
            byte[] r1 = r7.a     // Catch:{ Exception -> 0x0052 }
            if (r1 == 0) goto L_0x0030
            boolean r1 = r7.g     // Catch:{ Exception -> 0x0052 }
            if (r1 != 0) goto L_0x0030
            java.io.OutputStream r1 = r0.getOutputStream()     // Catch:{ Exception -> 0x0052 }
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0052 }
            byte[] r3 = r7.a     // Catch:{ Exception -> 0x0052 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0052 }
            r3 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x0052 }
        L_0x0020:
            r4 = 0
            r5 = 1024(0x400, float:1.435E-42)
            int r4 = r2.read(r3, r4, r5)     // Catch:{ Exception -> 0x0052 }
            r5 = -1
            if (r4 != r5) goto L_0x0046
        L_0x002a:
            r2.close()     // Catch:{ Exception -> 0x0052 }
            r1.close()     // Catch:{ Exception -> 0x0052 }
        L_0x0030:
            byte[] r1 = r7.a(r0)     // Catch:{ Exception -> 0x0052 }
            r7.b = r1     // Catch:{ Exception -> 0x0052 }
            r1 = 1
            r7.d = r1     // Catch:{ Exception -> 0x0052 }
            r1 = 1
            r7.c = r1     // Catch:{ all -> 0x0062 }
            if (r0 == 0) goto L_0x0041
            r0.disconnect()     // Catch:{ all -> 0x0062 }
        L_0x0041:
            r7.notify()     // Catch:{ all -> 0x0062 }
        L_0x0044:
            monitor-exit(r7)
            return
        L_0x0046:
            boolean r5 = r7.g     // Catch:{ Exception -> 0x0052 }
            if (r5 != 0) goto L_0x002a
            r5 = 0
            r1.write(r3, r5, r4)     // Catch:{ Exception -> 0x0052 }
            r1.flush()     // Catch:{ Exception -> 0x0052 }
            goto L_0x0020
        L_0x0052:
            r1 = move-exception
        L_0x0053:
            r1 = 0
            r7.d = r1     // Catch:{ all -> 0x0072 }
            r1 = 1
            r7.c = r1     // Catch:{ all -> 0x0062 }
            if (r0 == 0) goto L_0x005e
            r0.disconnect()     // Catch:{ all -> 0x0062 }
        L_0x005e:
            r7.notify()     // Catch:{ all -> 0x0062 }
            goto L_0x0044
        L_0x0062:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x0065:
            r0 = move-exception
        L_0x0066:
            r2 = 1
            r7.c = r2     // Catch:{ all -> 0x0062 }
            if (r1 == 0) goto L_0x006e
            r1.disconnect()     // Catch:{ all -> 0x0062 }
        L_0x006e:
            r7.notify()     // Catch:{ all -> 0x0062 }
            throw r0     // Catch:{ all -> 0x0062 }
        L_0x0072:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0066
        L_0x0077:
            r0 = move-exception
            r0 = r1
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Beauty.Breast.lightdd.a.c.c():void");
    }
}
