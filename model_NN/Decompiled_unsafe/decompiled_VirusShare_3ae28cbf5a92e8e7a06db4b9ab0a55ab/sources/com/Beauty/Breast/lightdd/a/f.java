package com.Beauty.Breast.lightdd.a;

import android.content.ContentValues;
import java.util.Vector;

public final class f {
    private String a;
    private Vector b = new Vector();
    private ContentValues c;
    private ContentValues d;
    private ContentValues e;
    private ContentValues f;

    public final ContentValues a() {
        return this.c;
    }

    public final boolean a(String str) {
        g gVar = new g();
        if (!gVar.a(str)) {
            return false;
        }
        if (gVar.b("NextConn") > 0) {
            this.a = gVar.a("NextConn", 0, "interval");
        }
        int b2 = gVar.b("Proxy");
        for (int i = 0; i < b2; i++) {
            this.b.add(gVar.a("Proxy", i, "url"));
        }
        if (gVar.b("MarketPrompt") == 1) {
            this.c = new ContentValues();
            this.c.put("Title2", (String) gVar.a("MarketPrompt", "Title").get(0));
            this.c.put("Description2", (String) gVar.a("MarketPrompt", "Description").get(0));
            this.c.put("PackageName2", (String) gVar.a("MarketPrompt", "PackageName").get(0));
        }
        if (gVar.b("WebPrompt") == 1) {
            this.f = new ContentValues();
            this.f.put("Title2", (String) gVar.a("WebPrompt", "Title").get(0));
            this.f.put("Description2", (String) gVar.a("WebPrompt", "Description").get(0));
            this.f.put("url2", (String) gVar.a("WebPrompt", "url").get(0));
        }
        if (gVar.b("UpdatePrompt") == 1) {
            this.d = new ContentValues();
            this.d.put("Title2", (String) gVar.a("UpdatePrompt", "Title").get(0));
            this.d.put("Description2", (String) gVar.a("UpdatePrompt", "Description").get(0));
            this.d.put("PackageName2", (String) gVar.a("UpdatePrompt", "PackageName").get(0));
            this.d.put("url2", (String) gVar.a("UpdatePrompt", "url").get(0));
            this.d.put("filename2", (String) gVar.a("UpdatePrompt", "filename").get(0));
        }
        if (gVar.b("DownloadPrompt") == 1) {
            this.e = new ContentValues();
            this.e.put("Title2", (String) gVar.a("DownloadPrompt", "Title").get(0));
            this.e.put("Description2", (String) gVar.a("DownloadPrompt", "Description").get(0));
            this.e.put("PackageName2", (String) gVar.a("DownloadPrompt", "PackageName").get(0));
            this.e.put("url2", (String) gVar.a("DownloadPrompt", "url").get(0));
            this.e.put("filename2", (String) gVar.a("DownloadPrompt", "filename").get(0));
        }
        return true;
    }

    public final ContentValues b() {
        return this.d;
    }

    public final ContentValues c() {
        return this.e;
    }

    public final ContentValues d() {
        return this.f;
    }

    public final Vector e() {
        return this.b;
    }

    public final String f() {
        return this.a;
    }
}
