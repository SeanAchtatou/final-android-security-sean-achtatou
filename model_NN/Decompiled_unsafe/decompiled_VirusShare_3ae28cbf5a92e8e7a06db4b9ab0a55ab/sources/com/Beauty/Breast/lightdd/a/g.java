package com.Beauty.Breast.lightdd.a;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public final class g {
    private InputStream a;
    private Document b;
    private Element c;

    public final String a(String str, int i, String str2) {
        NodeList elementsByTagName;
        if (!(this.c == null || (elementsByTagName = this.c.getElementsByTagName(str)) == null || elementsByTagName.getLength() <= i)) {
            Node item = elementsByTagName.item(i);
            if (item.getAttributes().getNamedItem(str2) != null) {
                return item.getAttributes().getNamedItem(str2).getNodeValue();
            }
        }
        return null;
    }

    public final Vector a(String str, String str2) {
        NodeList elementsByTagName;
        NodeList childNodes;
        Node firstChild;
        Vector vector = new Vector();
        if (!(this.c == null || (elementsByTagName = this.c.getElementsByTagName(str)) == null || elementsByTagName.getLength() <= 0 || (childNodes = elementsByTagName.item(0).getChildNodes()) == null)) {
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node item = childNodes.item(i);
                if (item.getNodeName().equals(str2) && (firstChild = item.getFirstChild()) != null) {
                    vector.add(firstChild.getNodeValue());
                }
            }
        }
        return vector;
    }

    public final boolean a(String str) {
        DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
        this.a = new ByteArrayInputStream(str.getBytes());
        try {
            this.b = newInstance.newDocumentBuilder().parse(this.a);
            this.c = this.b.getDocumentElement();
            return true;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            return false;
        } catch (SAXException e2) {
            e2.printStackTrace();
            return false;
        } catch (IOException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public final int b(String str) {
        NodeList elementsByTagName;
        if (this.c == null || (elementsByTagName = this.c.getElementsByTagName(str)) == null) {
            return 0;
        }
        return elementsByTagName.getLength();
    }
}
