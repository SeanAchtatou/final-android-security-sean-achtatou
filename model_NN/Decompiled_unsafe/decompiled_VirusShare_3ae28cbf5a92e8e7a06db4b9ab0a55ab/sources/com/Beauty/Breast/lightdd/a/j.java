package com.Beauty.Breast.lightdd.a;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.Beauty.Breast.lightdd.a;
import com.Beauty.Breast.lightdd.g;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import junit.framework.TestCase;

public final class j extends TestCase {
    private StringBuffer a;
    private ContentValues b;
    private Context c;
    private int d = 2;

    public j(ContentValues contentValues, Context context) {
        this.b = contentValues;
        this.a = new StringBuffer();
        this.c = context;
    }

    private void a(String str) {
        b(str);
        List<PackageInfo> installedPackages = this.c.getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < installedPackages.size(); i++) {
            PackageInfo packageInfo = installedPackages.get(i);
            Vector vector = new Vector();
            vector.add(new a(this, "name", a.a(packageInfo.applicationInfo.loadLabel(this.c.getPackageManager()).toString())));
            vector.add(new a(this, "package", packageInfo.packageName));
            vector.add(new a(this, "ver", new StringBuilder(String.valueOf(packageInfo.versionCode)).toString()));
            a("Product", vector);
        }
        c(str);
    }

    private void a(String str, String str2) {
        b(str);
        this.a.append(str2);
        c(str);
    }

    private void a(String str, Vector vector) {
        this.a.append("<" + str + " ");
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= vector.size()) {
                this.a.append("/>");
                return;
            } else {
                this.a.append(String.valueOf(((a) vector.get(i2)).a) + "=" + "\"" + ((a) vector.get(i2)).b + "\" ");
                i = i2 + 1;
            }
        }
    }

    private String b() {
        d("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        d("<Request>\n");
        switch (this.d) {
            case 2:
                a("Protocol", "2.0");
                a("Command", String.valueOf(this.d));
                b("MobileInfo");
                a("Model", Build.DEVICE);
                a("Language", Locale.getDefault().getLanguage());
                a("Country", Locale.getDefault().getCountry());
                TelephonyManager telephonyManager = (TelephonyManager) this.c.getSystemService("phone");
                a("IMEI", telephonyManager.getDeviceId() == null ? "" : telephonyManager.getDeviceId());
                TelephonyManager telephonyManager2 = (TelephonyManager) this.c.getSystemService("phone");
                a("IMSI", telephonyManager2.getSubscriberId() == null ? "" : telephonyManager2.getSubscriberId());
                c("MobileInfo");
                b("ClientInfo");
                a("PlatformID", "5");
                a("OSVersion", new StringBuilder().append(Build.VERSION.SDK_INT).toString());
                try {
                    a("Edition", new StringBuilder().append(this.c.getPackageManager().getPackageInfo(this.c.getPackageName(), 0).versionCode).toString());
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                a("ProductID", "50103");
                a("SubCoopID", "800");
                a("PackageName", this.c.getPackageName());
                c("ClientInfo");
                a("InstalledProductInfo");
                break;
        }
        d("</Request>");
        return this.a.toString();
    }

    private void b(String str) {
        this.a.append("<" + str + ">");
    }

    private void c(String str) {
        this.a.append("</" + str + ">");
    }

    private void d(String str) {
        this.a.append(str);
    }

    public final byte[] a() {
        try {
            return g.a(b().getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
