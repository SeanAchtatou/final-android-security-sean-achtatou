package com.Beauty.Breast.lightdd;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import com.Beauty.Breast.R;
import java.io.File;
import java.util.Locale;

final class f extends Handler {
    private /* synthetic */ CoreService a;

    f(CoreService coreService) {
        this.a = coreService;
    }

    public final void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                this.a.e.setTextViewText(R.id.downloadTaskName, this.a.f);
                this.a.e.setProgressBar(R.id.downloadProgressBar, 100, 0, false);
                this.a.e.setTextViewText(R.id.downloadProgressText, "0%");
                this.a.b.notify(17301633, this.a.c);
                return;
            case 1:
                int intValue = ((Integer) message.obj).intValue();
                this.a.e.setProgressBar(R.id.downloadProgressBar, 100, intValue, false);
                this.a.e.setTextViewText(R.id.downloadTaskName, this.a.f);
                this.a.e.setTextViewText(R.id.downloadProgressText, String.valueOf(intValue) + "%");
                this.a.b.notify(17301633, this.a.c);
                return;
            case 2:
                if (((Boolean) message.obj).booleanValue()) {
                    this.a.b.cancel(17301633);
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.fromFile(new File(this.a.h.getFilesDir() + "/" + this.a.f)), "application/vnd.android.package-archive");
                    intent.setFlags(268435456);
                    this.a.startActivity(intent);
                    return;
                }
                this.a.b.cancel(17301633);
                if (Locale.getDefault().getLanguage().equals("zh")) {
                    CoreService.a(this.a, this.a.f, "下载失败", this.a.g, this.a.f);
                    return;
                } else {
                    CoreService.a(this.a, this.a.f, "Download unsuccessful", this.a.g, this.a.f);
                    return;
                }
            default:
                return;
        }
    }
}
