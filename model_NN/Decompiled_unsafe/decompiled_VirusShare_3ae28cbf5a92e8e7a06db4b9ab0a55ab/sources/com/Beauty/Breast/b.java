package com.Beauty.Breast;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public final class b extends BaseAdapter {
    private Activity a;
    private /* synthetic */ SexyImages b;

    public b(SexyImages sexyImages, Activity activity) {
        this.b = sexyImages;
        this.a = activity;
    }

    public final int getCount() {
        return SexyImages.a.length;
    }

    public final Object getItem(int i) {
        return Integer.valueOf(SexyImages.a[i]);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        ImageView imageView = new ImageView(this.a);
        imageView.setImageResource(SexyImages.b[i]);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setLayoutParams(new AbsListView.LayoutParams(136, 88));
        return imageView;
    }
}
