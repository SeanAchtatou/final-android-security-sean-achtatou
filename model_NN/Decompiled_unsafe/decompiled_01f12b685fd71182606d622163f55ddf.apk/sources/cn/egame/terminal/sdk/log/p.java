package cn.egame.terminal.sdk.log;

import java.util.ArrayList;
import java.util.LinkedList;

public final class p extends ThreadGroup {
    private static p c = null;
    private boolean a = false;
    private LinkedList b = new LinkedList();
    private ArrayList d = new ArrayList();

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private p(int i) {
        super("TubeThreadPool");
        setDaemon(true);
        for (int i2 = 0; i2 < i; i2++) {
            q qVar = new q(this, i2);
            this.d.add(qVar);
            qVar.start();
        }
        w.a("TubeThreadPool", "TubeThreadPool is working!");
    }

    public static p a(int i) {
        return new p(i);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r0 = (java.lang.Runnable) r1.b.removeFirst();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.Runnable b(int r2) {
        /*
            r1 = this;
            monitor-enter(r1)
        L_0x0001:
            java.util.LinkedList r0 = r1.b     // Catch:{ all -> 0x0014 }
            int r0 = r0.size()     // Catch:{ all -> 0x0014 }
            if (r0 != 0) goto L_0x0017
            boolean r0 = r1.a     // Catch:{ all -> 0x0014 }
            if (r0 == 0) goto L_0x0010
            r0 = 0
        L_0x000e:
            monitor-exit(r1)
            return r0
        L_0x0010:
            r1.wait()     // Catch:{ all -> 0x0014 }
            goto L_0x0001
        L_0x0014:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0017:
            java.util.LinkedList r0 = r1.b     // Catch:{ all -> 0x0014 }
            java.lang.Object r0 = r0.removeFirst()     // Catch:{ all -> 0x0014 }
            java.lang.Runnable r0 = (java.lang.Runnable) r0     // Catch:{ all -> 0x0014 }
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.egame.terminal.sdk.log.p.b(int):java.lang.Runnable");
    }
}
