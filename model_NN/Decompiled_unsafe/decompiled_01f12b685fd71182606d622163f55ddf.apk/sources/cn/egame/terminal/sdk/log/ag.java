package cn.egame.terminal.sdk.log;

import com.datalab.tools.Constant;
import java.util.HashMap;

public class ag {
    public static boolean a = true;
    private static b b = null;
    private static HashMap c = new HashMap();
    private static m d = new o().c(0).b(0).a((int) Constant.DEFAULT_LIMIT).a();
    private static m e = new o().c(1).b(0).a((int) Constant.DEFAULT_LIMIT).a();

    public static b a() {
        if (b == null) {
            b = new b();
            b.a(new l().a(true).a(d).a(c).a());
        }
        return b;
    }

    public static String a(String str, m mVar) {
        return a ? a().a(str, mVar) : a.a().a(str, mVar);
    }

    public static String b(String str, m mVar) {
        return a(str, mVar);
    }
}
