package cn.egame.terminal.sdk.log;

import java.io.PrintWriter;
import java.io.StringWriter;

class ae extends Thread {
    final /* synthetic */ Throwable a;
    final /* synthetic */ ad b;

    ae(ad adVar, Throwable th) {
        this.b = adVar;
        this.a = th;
    }

    public void run() {
        StringBuilder sb = new StringBuilder();
        sb.append(ad.a);
        sb.append(ap.a() + "\n");
        sb.append(ah.f(this.b.d));
        sb.append("\n");
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        this.a.printStackTrace(printWriter);
        printWriter.close();
        sb.append(stringWriter.toString());
        aj.b(this.b.d, sb.toString());
    }
}
