package cn.egame.terminal.sdk;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import cn.egame.terminal.sdk.jni.EgamePayProtocol;
import java.util.HashMap;

public class EgameCoreActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        params.put("bundle", savedInstanceState);
        if (EgamePayProtocol.initCore(this) == null) {
            finish();
        } else {
            callCore("onCreate", params);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        boolean result;
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        params.put("event", event);
        Object object = callCore("dispatchKeyEvent", params);
        return (object == null || !(result = ((Boolean) object).booleanValue())) ? super.dispatchKeyEvent(event) : result;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean result;
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        params.put("keyCode", Integer.valueOf(keyCode));
        params.put("event", event);
        Object object = callCore("onKeyDown", params);
        return (object == null || !(result = ((Boolean) object).booleanValue())) ? super.onKeyDown(keyCode, event) : result;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        callCore("onPause", params);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        callCore("onResume", params);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        callCore("onDestroy", params);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        params.put("configuration", newConfig);
        callCore("onConfigurationChanged", params);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        params.put("request", Integer.valueOf(requestCode));
        params.put("result", Integer.valueOf(resultCode));
        params.put("data", data);
        callCore("onActivityResult", params);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private Object callCore(String mn, HashMap<String, Object> params) {
        return EgamePayProtocol.callCore(null, mn, params);
    }
}
