package cn.egame.terminal.sdk.log;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.datalab.BuildConfig;
import java.io.File;
import java.io.RandomAccessFile;
import java.util.Map;

public class ah {
    private static String a = "";
    private static String b = "";
    private static String c = "";
    private static String d = "";
    private static String e = "";
    private static String f = "";
    private static String g = "";
    private static String h = BuildConfig.channel;
    private static String i = "";
    private static String j = "";
    private static String k = "";
    private static String l = "";
    private static String m = "";
    private static int n = -1;
    private static String o = "";
    private static String p = "";
    private static String q = "";
    private static PhoneStateListener r = null;
    /* access modifiers changed from: private */
    public static SignalStrength s = null;

    public static String a() {
        return i;
    }

    public static String a(Context context) {
        if (TextUtils.isEmpty(a)) {
            a = al.a(context);
        }
        return a;
    }

    public static String a(Context context, am amVar) {
        return a(context, amVar, true);
    }

    private static String a(Context context, am amVar, boolean z) {
        Map a2;
        aq aqVar = new aq(z);
        aqVar.a("version", h);
        if (amVar == null || TextUtils.isEmpty(amVar.d)) {
            aqVar.a("app_key", a(context));
        } else {
            aqVar.a("app_key", amVar.d);
        }
        if (amVar == null || TextUtils.isEmpty(amVar.c)) {
            aqVar.a("from", c(context));
        } else {
            aqVar.a("from", amVar.c);
        }
        if (amVar == null || TextUtils.isEmpty(amVar.e)) {
            aqVar.a("channel_id", b(context));
        } else {
            aqVar.a("channel_id", amVar.e);
        }
        aqVar.a("imsi", e);
        aqVar.a("mac", q);
        aqVar.a("msisdn", f);
        aqVar.a("user_id", g);
        aqVar.a("cp_uid", d(context));
        aqVar.a("network", as.a(context));
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        aqVar.a("network_int", activeNetworkInfo == null ? "-100" : activeNetworkInfo.getSubtype() + "");
        aqVar.a("network_class", activeNetworkInfo == null ? "-1" : activeNetworkInfo.getType() + "");
        aqVar.a("meid", i);
        aqVar.a("model", j);
        aqVar.a("build_id", k);
        aqVar.a("screen_px", l);
        aqVar.a("agency", m);
        aqVar.a("timestamp", System.currentTimeMillis());
        aqVar.a("api_level", n);
        aqVar.a("cpu_abi", o);
        aqVar.a("app_ver", p);
        if (s != null) {
            if (s.isGsm()) {
                aqVar.a("signal_gsm", ar.a(s));
            } else {
                aqVar.a("signal_cdma", ar.b(s));
            }
        }
        if (!(amVar == null || (a2 = amVar.a()) == null)) {
            for (String str : a2.keySet()) {
                aqVar.a(str, (String) a2.get(str));
            }
        }
        return aqVar.toString();
    }

    public static void a(Context context, String str) {
        try {
            if (!TextUtils.isEmpty(str) && !"null".equalsIgnoreCase(str)) {
                if (str.startsWith("egame_")) {
                    a = str.replaceFirst("egame_", "");
                    al.a(context, a);
                } else if (str.startsWith("key_")) {
                    String replaceFirst = str.replaceFirst("key_", "");
                    if (replaceFirst.length() < 12) {
                        w.c("egame_log", "AppKey获取异常,请检查'AndroidManifest.xml'中的参数配置. @Egame");
                    }
                    a = replaceFirst;
                    al.a(context, a);
                } else {
                    w.c("egame_log", "AppKey获取异常,请检查'AndroidManifest.xml'中的参数配置. @Egame");
                }
            }
        } catch (NullPointerException e2) {
        }
    }

    public static void a(String str) {
        g = str;
    }

    public static String b(Context context) {
        if (TextUtils.isEmpty(b)) {
            b = al.b(context);
        }
        return b;
    }

    public static String b(Context context, am amVar) {
        return a(context, amVar, false);
    }

    private static String b(String str) {
        String str2 = "";
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(new File(TextUtils.isEmpty(str) ? "/sys/class/net/wlan0/address" : str), "r");
            String readLine = randomAccessFile.readLine();
            if (!TextUtils.isEmpty(readLine)) {
                String replace = readLine.replace(":", "");
                if (replace.length() == 12) {
                    str2 = "eth" + replace;
                }
            }
            randomAccessFile.close();
        } catch (Exception e2) {
            w.a("EGAME_LOG", "fetch mac address error.");
        }
        return (!TextUtils.isEmpty(str2) || !TextUtils.isEmpty(str)) ? str2 : b("/sys/class/net/eth0/address");
    }

    public static void b(Context context, String str) {
        if (!TextUtils.isEmpty(str) && !"null".equalsIgnoreCase(str)) {
            if (str.startsWith("cid_")) {
                str = str.replaceFirst("cid_", "");
            }
            b = str;
            al.b(context, b);
        }
    }

    public static String c(Context context) {
        if (TextUtils.isEmpty(c)) {
            c = al.c(context);
        }
        return c;
    }

    public static void c(Context context, String str) {
        if (!TextUtils.isEmpty(str) && !"null".equalsIgnoreCase(str)) {
            c = str;
            al.c(context, str);
        }
    }

    public static String d(Context context) {
        if (TextUtils.isEmpty(d)) {
            d = al.d(context);
        }
        return d;
    }

    public static void d(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            d = str;
            al.d(context, str);
        }
    }

    public static void e(Context context) {
        if (ap.a(context, "android.permission.READ_PHONE_STATE")) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            e = telephonyManager.getSubscriberId();
            i = telephonyManager.getDeviceId();
            if (r == null) {
                r = new ai();
                telephonyManager.listen(r, 256);
            }
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        l = displayMetrics.widthPixels + "*" + displayMetrics.heightPixels;
        j = Build.MODEL;
        k = Build.DISPLAY;
        int a2 = at.a(e).a();
        if (a2 != at.NOT_DEFINE.a()) {
            m = String.valueOf(a2);
        }
        n = Build.VERSION.SDK_INT;
        o = Build.CPU_ABI;
        try {
            p = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            w.a(e2.getLocalizedMessage());
        }
        q = b((String) null);
        a(al.j(context));
    }

    public static String f(Context context) {
        return b(context, (am) null);
    }
}
