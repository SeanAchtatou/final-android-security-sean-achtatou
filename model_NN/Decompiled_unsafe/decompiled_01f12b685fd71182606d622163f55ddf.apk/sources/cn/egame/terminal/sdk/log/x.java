package cn.egame.terminal.sdk.log;

import android.text.TextUtils;
import java.net.URLEncoder;

public class x {
    private StringBuilder a;
    private boolean b;

    public x() {
        this(true);
    }

    public x(boolean z) {
        this.a = new StringBuilder();
        this.b = true;
        this.b = z;
    }

    public x a(String str, int i) {
        return a(str, String.valueOf(i));
    }

    public x a(String str, long j) {
        return j == -1 ? this : a(str, String.valueOf(j));
    }

    public x a(String str, String str2) {
        return (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2) || "null".equalsIgnoreCase(str2) || "-1".equals(str2)) ? this : b(str, str2);
    }

    public x b(String str, String str2) {
        if (this.a.length() > 0) {
            this.a.append("&");
        }
        if (this.b) {
            this.a.append(URLEncoder.encode(str) + "=" + URLEncoder.encode(str2));
        } else {
            this.a.append(str + "=" + str2);
        }
        return this;
    }

    public String toString() {
        return this.a.toString();
    }
}
