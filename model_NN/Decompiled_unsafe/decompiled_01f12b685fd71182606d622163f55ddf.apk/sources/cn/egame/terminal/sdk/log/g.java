package cn.egame.terminal.sdk.log;

import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;

public class g {
    private HttpClient a = null;
    private HttpEntity b = null;
    private HttpRequestBase c = null;

    public g(HttpClient httpClient, HttpRequestBase httpRequestBase, HttpEntity httpEntity) {
        this.a = httpClient;
        this.c = httpRequestBase;
        this.b = httpEntity;
    }

    public String a() {
        if (this.b != null) {
            try {
                String a2 = e.b(this.b);
                w.a("TUBE", "Result: \n" + a2);
                return a2;
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        return null;
    }

    public void b() {
        if (this.c != null && !this.c.isAborted()) {
            this.c.abort();
        }
        if (this.a != null) {
            this.a.getConnectionManager().shutdown();
        }
    }
}
