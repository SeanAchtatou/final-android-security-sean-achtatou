package cn.egame.terminal.sdk.jni;

public class ProtocolMessage {
    public int arg1;
    public int arg2;
    public Object obj = null;

    public ProtocolMessage(Object obj2, int arg12, int arg22) {
        this.obj = obj2;
        this.arg1 = arg12;
        this.arg2 = arg22;
    }
}
