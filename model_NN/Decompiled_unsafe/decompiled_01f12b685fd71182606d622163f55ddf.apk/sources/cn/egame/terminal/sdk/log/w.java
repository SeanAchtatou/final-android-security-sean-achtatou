package cn.egame.terminal.sdk.log;

import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;

public class w {
    public static boolean a = false;
    private static Handler b = null;
    private static int c = -1;

    private static void a(int i, String str, String str2) {
        String str3;
        if (b != null) {
            int i2 = -1;
            switch (i) {
                case 2:
                    i2 = -7829368;
                    str3 = "V";
                    break;
                case 3:
                    i2 = -65281;
                    str3 = "D";
                    break;
                case 4:
                    i2 = -16744704;
                    str3 = "I";
                    break;
                case 5:
                    i2 = -33024;
                    str3 = "W";
                    break;
                case 6:
                    i2 = -65536;
                    str3 = "E";
                    break;
                default:
                    str3 = "";
                    break;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str3);
            sb.append(":");
            sb.append(str);
            sb.append("->");
            sb.append(str2);
            sb.append("\n");
            SpannableString spannableString = new SpannableString(sb.toString());
            spannableString.setSpan(new ForegroundColorSpan(i2), 0, sb.length(), 33);
            Message message = new Message();
            message.what = c;
            message.obj = spannableString;
            message.arg1 = i;
            b.sendMessage(message);
        }
    }

    public static void a(String str) {
        b("LAZY", str);
    }

    public static void a(String str, String str2) {
        if (str != null && str2 != null) {
            if (a) {
                Log.d(str, str2);
            }
            a(3, str, str2);
        }
    }

    public static void b(String str, String str2) {
        if (str != null && str2 != null) {
            if (a) {
                Log.i(str, str2);
            }
            a(4, str, str2);
        }
    }

    public static void c(String str, String str2) {
        if (str != null && str2 != null) {
            Log.e(str, str2);
            a(6, str, str2);
        }
    }
}
