package cn.egame.terminal.sdk.log;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class as {
    public static String a(Context context) {
        return context != null ? av.a(((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo()).a() : av.UNKNOW_TYPE.a();
    }

    public static boolean b(Context context) {
        NetworkInfo networkInfo;
        if (context == null || (networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1)) == null) {
            return false;
        }
        return networkInfo.isAvailable();
    }

    public static boolean c(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return false;
        }
        return activeNetworkInfo != null && activeNetworkInfo.isAvailable();
    }
}
