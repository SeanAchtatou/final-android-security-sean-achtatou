package cn.egame.terminal.sdk.log;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class an extends am {
    public String f;

    public Map a() {
        if (TextUtils.isEmpty(this.f)) {
            return null;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("event_key", this.f);
        if (this.b != null) {
            hashMap.put("event_value", new JSONObject(this.b).toString());
        }
        hashMap.put("session_id", this.a);
        return hashMap;
    }
}
