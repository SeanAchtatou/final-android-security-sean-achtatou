package cn.egame.terminal.sdk.log;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import com.datalab.tools.Constant;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import org.apache.http.entity.AbstractHttpEntity;
import org.json.JSONException;
import org.json.JSONObject;
import org.objectweb.asm.Opcodes;

public class BaseLog {
    public static final int POLICY_OFF_LINE = 0;
    public static final int POLICY_ON_LINE = 1;
    protected static Handler a = null;
    private static String b = "log.vcgame.cn";
    private static String c = null;

    protected BaseLog(Context context) {
        HandlerThread handlerThread = new HandlerThread("EgameLog");
        handlerThread.start();
        a = new Handler(handlerThread.getLooper());
        a.post(new y(this, context));
    }

    private static String a() {
        return "http://" + b;
    }

    protected static void a(Context context) {
        Bundle bundle;
        try {
            bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
        } catch (PackageManager.NameNotFoundException e) {
            w.a("EGAME_LOG", e.getLocalizedMessage());
            bundle = null;
        }
        if (bundle != null) {
            ah.a(context, String.valueOf(bundle.get("egame_app_key")));
            ah.b(context, String.valueOf(bundle.get("egame_channel_id")));
            ah.c(context, String.valueOf(bundle.get("egame_sdk_from")));
        }
        ah.e(context);
        c = al.g(context);
        if (ap.a(context)) {
            c(context);
        }
    }

    protected static void a(Context context, am amVar) {
        a(context, ah.a(context, amVar));
    }

    protected static void a(Context context, String str) {
        if (al.f(context) != 1 || !as.c(context)) {
            d(context, str);
        } else {
            c(context, str);
        }
    }

    private static String b() {
        return a() + "/api/v2/egame/log.json";
    }

    protected static void b(Context context) {
        aj.a(context, new z(context));
        if (ap.a(context, "android.permission.WRITE_EXTERNAL_STORAGE") && ap.b(context)) {
            try {
                if (d(context)) {
                    aj.b();
                    al.c(context, System.currentTimeMillis());
                }
            } catch (IOException e) {
                w.a("EGAME_LOG", e.getLocalizedMessage());
            }
        }
    }

    private static String c() {
        return a() + "/api/v2/egame/log_error.json";
    }

    private static void c(Context context) {
        String a2 = ag.a(d() + "app_key=" + ah.a(context), null);
        if (!TextUtils.isEmpty(a2)) {
            try {
                JSONObject jSONObject = new JSONObject(a2);
                if (jSONObject.getInt("code") == 0) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject("ext");
                    if (jSONObject2.getInt("log_switch_status") == 1) {
                        al.a(context, 1);
                    } else {
                        al.a(context, 0);
                    }
                    String optString = jSONObject2.optString("log_url");
                    if (!TextUtils.isEmpty(optString)) {
                        al.f(context, optString);
                        c = optString;
                    }
                    al.b(context, System.currentTimeMillis());
                }
            } catch (JSONException e) {
                w.a("EGAME_LOG", "json error.");
                al.a(context, 1);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void c(Context context, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("req_log", str);
        if (TextUtils.isEmpty(ag.a(!TextUtils.isEmpty(c) ? c : b(), new o().c(1).a(hashMap).a((int) Constant.DEFAULT_LIMIT).b(2).a()))) {
            d(context, str);
        }
    }

    private static String d() {
        return a() + "/api/v2/egame/log/config.json?";
    }

    private static void d(Context context, String str) {
        aj.a(context, str);
    }

    private static boolean d(Context context) {
        AbstractHttpEntity a2;
        InputStream a3 = aj.a();
        u uVar = new u();
        byte[] bArr = new byte[Opcodes.ACC_ANNOTATION];
        int length = bArr.length / 2;
        int i = 0;
        int i2 = 0;
        while (i2 != -1) {
            i2 = a3.read(bArr, i, bArr.length - i);
            if (i2 != -1) {
                i += i2;
            }
            if (i2 == -1 || i >= length) {
                uVar.a(bArr, 0, i);
                i = 0;
            }
        }
        a3.close();
        synchronized (uVar) {
            byte[] bArr2 = new byte[uVar.b()];
            int i3 = 0;
            while (true) {
                v a4 = uVar.a();
                if (a4 == null) {
                    break;
                }
                if (a4.b != 0) {
                    System.arraycopy(a4.a, 0, bArr2, i3, a4.b);
                    i3 += a4.b;
                }
                a4.a();
            }
            a2 = ap.a(bArr2);
        }
        if (a2 == null) {
            return false;
        }
        a2.setContentType("text/plain");
        String a5 = ah.a(context, (am) null);
        HashMap hashMap = new HashMap();
        hashMap.put("req_log", a5);
        return !TextUtils.isEmpty(ag.b(c(), new o().c(1).a(hashMap).a(Constant.DEFAULT_LIMIT).a(a2).b(0).a()));
    }

    public static void setHostUrl(String str) {
        if (TextUtils.isEmpty(str)) {
            str = "log.vcgame.cn";
        }
        b = str;
    }
}
