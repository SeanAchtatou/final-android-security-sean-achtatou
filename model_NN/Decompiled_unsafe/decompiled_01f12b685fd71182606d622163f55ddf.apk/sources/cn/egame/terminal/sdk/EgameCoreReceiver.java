package cn.egame.terminal.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import com.tendcloud.tenddata.dc;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;

public class EgameCoreReceiver extends BroadcastReceiver {
    public static final String ACTION_RECEIVER_CMD = "cn.egame.terminal.sdk.RECEIVER_CMD";
    public static final String DATE_FILE = "data.dat";
    public static final String EXTRA_RECEIVER_CMD = "receiver_intent";
    public static final int PUBLIC_PSH_SDK_VERSION = 121;
    public static final long PUSK_USER_ACTION_DEFAULT_INTERVAL_TIME = 7200000;
    public static final String TAG = "cca";
    public static final String TOKEN_DATA_NAME = "EGAME_ABC_SHARE";

    public void onReceive(Context context, Intent intent) {
        long currentTime = System.currentTimeMillis();
        long lastActionTime = getLastUserActionTime(context);
        long intervalTime = getSdkConfigIntervalTime(context);
        if (currentTime - lastActionTime >= intervalTime || isPackageAction(intent)) {
            setLastUserActionTime(context, currentTime);
            Intent i = new Intent(context, EgameCoreService.class);
            i.setAction(ACTION_RECEIVER_CMD);
            i.putExtra(EXTRA_RECEIVER_CMD, intent);
            context.startService(i);
            return;
        }
        Log.d(TAG, "limit interval=" + intervalTime);
    }

    private boolean isPackageAction(Intent intent) {
        if (intent == null) {
            return false;
        }
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            return false;
        }
        if (action.equals("android.intent.action.PACKAGE_ADDED") || action.equals(dc.K)) {
            return true;
        }
        return false;
    }

    private long getLastUserActionTime(Context context) {
        return context.getSharedPreferences(TOKEN_DATA_NAME, 0).getLong("user_action_time", 0);
    }

    private void setLastUserActionTime(Context context, long time) {
        context.getSharedPreferences(TOKEN_DATA_NAME, 0).edit().putLong("user_action_time", time).commit();
    }

    private long getSdkConfigIntervalTime(Context context) {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return PUSK_USER_ACTION_DEFAULT_INTERVAL_TIME;
        }
        File file = new File(Environment.getExternalStorageDirectory(), DATE_FILE);
        if (!file.exists()) {
            return PUSK_USER_ACTION_DEFAULT_INTERVAL_TIME;
        }
        try {
            DataInputStream dis = new DataInputStream(new FileInputStream(file));
            try {
                long time = (long) dis.readInt();
                dis.close();
                return time * 60 * 60 * 1000;
            } catch (Exception e) {
                e = e;
                e.printStackTrace();
                return PUSK_USER_ACTION_DEFAULT_INTERVAL_TIME;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return PUSK_USER_ACTION_DEFAULT_INTERVAL_TIME;
        }
    }
}
