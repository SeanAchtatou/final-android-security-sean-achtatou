package cn.egame.terminal.sdk.log;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.LinkedList;
import java.util.ListIterator;

public class u {
    /* access modifiers changed from: private */
    public static final LinkedList a = new LinkedList();
    /* access modifiers changed from: private */
    public static final ReferenceQueue b = new ReferenceQueue();
    private LinkedList c = new LinkedList();

    private v a(int i) {
        v vVar;
        if (i < 8192) {
            i = 8192;
        }
        synchronized (a) {
            e();
            if (a.isEmpty() || (vVar = (v) ((SoftReference) a.removeFirst()).get()) == null) {
                vVar = new v(i);
            }
        }
        return vVar;
    }

    private void e() {
        while (true) {
            SoftReference softReference = (SoftReference) b.poll();
            if (softReference != null) {
                a.remove(softReference);
            } else {
                return;
            }
        }
    }

    public synchronized v a() {
        return this.c.isEmpty() ? null : (v) this.c.removeFirst();
    }

    public synchronized void a(byte[] bArr, int i, int i2) {
        v vVar;
        while (i2 > 0) {
            if (this.c.isEmpty()) {
                vVar = a(i2);
                this.c.addLast(vVar);
            } else {
                vVar = (v) this.c.getLast();
                if (vVar.b == vVar.a.length) {
                    vVar = a(i2);
                    this.c.addLast(vVar);
                }
            }
            int min = Math.min(i2, vVar.a.length - vVar.b);
            System.arraycopy(bArr, i, vVar.a, vVar.b, min);
            vVar.b += min;
            i2 -= min;
            i += min;
        }
    }

    public synchronized int b() {
        int i;
        int i2 = 0;
        synchronized (this) {
            ListIterator listIterator = this.c.listIterator(0);
            while (true) {
                i = i2;
                if (listIterator.hasNext()) {
                    i2 = ((v) listIterator.next()).b + i;
                }
            }
        }
        return i;
    }
}
