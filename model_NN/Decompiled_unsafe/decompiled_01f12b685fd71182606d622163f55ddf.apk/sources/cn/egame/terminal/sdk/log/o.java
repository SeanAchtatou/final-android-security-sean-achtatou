package cn.egame.terminal.sdk.log;

import com.wali.gamecenter.report.io.HttpConnectionManager;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;

public class o {
    private int a = HttpConnectionManager.WIFI_WAIT_TIMEOUT;
    private int b = HttpConnectionManager.WIFI_WAIT_TIMEOUT;
    private int c = 4;
    private Map d = null;
    private List e = null;
    private HttpEntity f = null;
    private boolean g = false;
    private HttpHost h = null;
    private String i = null;
    private int j = 0;

    public m a() {
        m mVar = new m();
        mVar.b = this.b;
        mVar.a = this.a;
        mVar.c = this.c;
        mVar.d = this.d;
        mVar.e = this.e;
        mVar.h = this.h;
        mVar.i = this.i;
        mVar.f = this.f;
        mVar.g = this.g;
        if (this.f != null) {
            mVar.j = 1;
        } else {
            mVar.j = this.j;
        }
        return mVar;
    }

    public o a(int i2) {
        this.b = i2;
        return this;
    }

    public o a(Map map) {
        this.d = map;
        return this;
    }

    public o a(HttpEntity httpEntity) {
        this.f = httpEntity;
        return this;
    }

    public o b(int i2) {
        this.c = i2;
        return this;
    }

    public o c(int i2) {
        if (i2 == 1) {
            this.j = 1;
        }
        return this;
    }
}
