package cn.egame.terminal.sdk.log;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

public class EgameAgent extends BaseLog {
    private static EgameAgent b = null;
    private static long c = 30000;
    private static String d = "";
    private static long e = 0;
    private static long f = 0;

    private EgameAgent(Context context) {
        super(context);
    }

    private static void a(Context context, String str, Map map, String str2, String str3, String str4, long j, long j2) {
        long j3 = 0;
        al.a(context, j2);
        long j4 = j2 - j;
        if (j4 > 0) {
            j3 = j4;
        }
        ao aoVar = new ao();
        if (TextUtils.isEmpty(str)) {
            aoVar.f = context.getClass().getName();
        } else {
            aoVar.f = str;
        }
        aoVar.a = d;
        aoVar.g = j;
        aoVar.h = j2;
        aoVar.i = j3;
        aoVar.b = map;
        aoVar.c = str2;
        aoVar.d = str3;
        aoVar.e = str4;
        a(context, aoVar);
    }

    private static void b(Context context, an anVar) {
        c(context);
        a.post(new ac(context, anVar));
    }

    /* access modifiers changed from: private */
    public static void b(Context context, String str, Map map, String str2, String str3, String str4) {
        f = System.currentTimeMillis();
        a(context, str, map, str2, str3, str4, e, f);
    }

    protected static EgameAgent c(Context context) {
        if (b == null) {
            synchronized (EgameAgent.class) {
                if (b == null) {
                    b = new EgameAgent(context);
                }
            }
        }
        return b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.egame.terminal.sdk.log.BaseLog.a(android.content.Context, cn.egame.terminal.sdk.log.am):void
     arg types: [android.content.Context, cn.egame.terminal.sdk.log.an]
     candidates:
      cn.egame.terminal.sdk.log.EgameAgent.a(android.content.Context, cn.egame.terminal.sdk.log.an):void
      cn.egame.terminal.sdk.log.BaseLog.a(android.content.Context, java.lang.String):void
      cn.egame.terminal.sdk.log.BaseLog.a(android.content.Context, cn.egame.terminal.sdk.log.am):void */
    /* access modifiers changed from: private */
    public static void c(Context context, an anVar) {
        a(context, (am) anVar);
    }

    /* access modifiers changed from: private */
    public static void e(Context context) {
        if (f(context) && as.c(context)) {
            b(context);
        }
        e = System.currentTimeMillis();
    }

    private static boolean f(Context context) {
        if (System.currentTimeMillis() - al.e(context) <= c && !TextUtils.isEmpty(d)) {
            return false;
        }
        g(context);
        return true;
    }

    private static String g(Context context) {
        String a = ah.a(context);
        if (TextUtils.isEmpty(a)) {
            a = "EgameApp";
        }
        long currentTimeMillis = System.currentTimeMillis();
        d = au.a((a + currentTimeMillis) + ah.a());
        al.e(context, d);
        al.a(context, currentTimeMillis);
        return d;
    }

    public static void init(Context context, String str) {
        ah.d(context, str);
        HashMap hashMap = new HashMap();
        hashMap.put("__cp_uid__", str);
        onEvent(context, "__egame_start_event__", hashMap);
    }

    public static void onEvent(Context context, String str) {
        onEvent(context, str, null);
    }

    public static void onEvent(Context context, String str, Map map) {
        onEvent(context, str, map, null, null, null);
    }

    public static void onEvent(Context context, String str, Map map, String str2, String str3, String str4) {
        an anVar = new an();
        anVar.f = str;
        anVar.b = map;
        anVar.a = d;
        anVar.c = str2;
        anVar.d = str3;
        anVar.e = str4;
        b(context, anVar);
    }

    public static void onPause(Context context) {
        onPause(context, null, null, null, null, null);
    }

    public static void onPause(Context context, String str) {
        onPause(context, str, null, null, null, null);
    }

    public static void onPause(Context context, String str, Map map) {
        onPause(context, str, map, null, null, null);
    }

    public static void onPause(Context context, String str, Map map, String str2, String str3, String str4) {
        c(context);
        a.post(new ab(context, str, map, str2, str3, str4));
    }

    public static void onPause(Context context, Map map) {
        onPause(context, null, map, null, null, null);
    }

    public static void onPause(Context context, Map map, String str, String str2, String str3) {
        onPause(context, null, map, str, str2, str3);
    }

    public static void onResume(Context context) {
        c(context);
        a.post(new aa(context));
    }

    public static void registerExceptionLog(Application application) {
        c((Context) application);
        ad.a(application, null);
    }

    public static void registerExceptionLog(Application application, EgameCrashListener egameCrashListener) {
        c((Context) application);
        ad.a(application, egameCrashListener);
    }

    public static void setContinueSessionMillis(long j) {
        c = j;
    }
}
