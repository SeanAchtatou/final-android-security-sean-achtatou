package cn.egame.terminal.sdk.jni;

import android.content.Context;
import cn.egame.terminal.paysdk.EgamePayListener;
import java.util.Map;

public class EgamePayProtocol {
    public static native Object call(String str, String str2, Map<?, ?> map);

    public static native Object call(String str, Map<?, ?> map);

    public static native Object callCore(String str, String str2, Map<?, ?> map);

    public static native Object callCore(String str, Map<?, ?> map);

    public static native ProtocolMessage initCore(Context context);

    public static native ProtocolMessage initPay(Context context);

    public static native void pay(Context context, Map<?, ?> map, EgamePayListener egamePayListener);

    static {
        System.loadLibrary("egamepay_dr2");
    }
}
