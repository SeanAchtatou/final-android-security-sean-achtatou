package cn.egame.terminal.sdk.log;

import android.content.Context;
import android.content.SharedPreferences;

public class al {
    public static String a(Context context) {
        return k(context).getString("app_key", "");
    }

    public static void a(Context context, int i) {
        SharedPreferences.Editor edit = k(context).edit();
        edit.putInt("record_policy", i);
        edit.commit();
    }

    public static void a(Context context, long j) {
        SharedPreferences.Editor edit = k(context).edit();
        edit.putLong("session_time", j);
        edit.commit();
    }

    public static void a(Context context, String str) {
        SharedPreferences.Editor edit = k(context).edit();
        edit.putString("app_key", str);
        edit.commit();
    }

    public static String b(Context context) {
        return k(context).getString("channel_id", "");
    }

    public static void b(Context context, long j) {
        SharedPreferences.Editor edit = k(context).edit();
        edit.putLong("update_cfg_time", j);
        edit.commit();
    }

    public static void b(Context context, String str) {
        SharedPreferences.Editor edit = k(context).edit();
        edit.putString("channel_id", str);
        edit.commit();
    }

    public static String c(Context context) {
        return k(context).getString("from", "game");
    }

    public static void c(Context context, long j) {
        SharedPreferences.Editor edit = k(context).edit();
        edit.putLong("upload_errorlog_time", j);
        edit.commit();
    }

    public static void c(Context context, String str) {
        SharedPreferences.Editor edit = k(context).edit();
        edit.putString("from", str);
        edit.commit();
    }

    public static String d(Context context) {
        return k(context).getString("cp_uid", "");
    }

    public static void d(Context context, String str) {
        SharedPreferences.Editor edit = k(context).edit();
        edit.putString("cp_uid", str);
        edit.commit();
    }

    public static long e(Context context) {
        return k(context).getLong("session_time", 0);
    }

    public static void e(Context context, String str) {
        SharedPreferences.Editor edit = k(context).edit();
        edit.putString("session_id", str);
        edit.commit();
    }

    public static int f(Context context) {
        return k(context).getInt("record_policy", 1);
    }

    public static void f(Context context, String str) {
        SharedPreferences.Editor edit = k(context).edit();
        edit.putString("log_url", str);
        edit.commit();
    }

    public static String g(Context context) {
        return k(context).getString("log_url", "");
    }

    public static long h(Context context) {
        return k(context).getLong("update_cfg_time", 0);
    }

    public static long i(Context context) {
        return k(context).getLong("upload_errorlog_time", 0);
    }

    public static String j(Context context) {
        return context.getSharedPreferences("cn_egame_openapi_opt", 0).getString("uid", "");
    }

    private static SharedPreferences k(Context context) {
        return context.getSharedPreferences("cn_egame_sdk_log", 0);
    }
}
