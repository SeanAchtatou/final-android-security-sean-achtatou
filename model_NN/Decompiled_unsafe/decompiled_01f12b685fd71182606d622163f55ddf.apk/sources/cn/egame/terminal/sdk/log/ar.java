package cn.egame.terminal.sdk.log;

import android.telephony.SignalStrength;

public class ar {
    public static int a(SignalStrength signalStrength) {
        int i = 2;
        int gsmSignalStrength = signalStrength.getGsmSignalStrength();
        if (gsmSignalStrength <= 2 || gsmSignalStrength == 99) {
            i = 0;
        } else if (gsmSignalStrength >= 12) {
            i = 4;
        } else if (gsmSignalStrength >= 8) {
            i = 3;
        } else if (gsmSignalStrength < 5) {
            i = 1;
        }
        w.a("SignalUtils", "getGsmLevel=" + i);
        return i;
    }

    public static int b(SignalStrength signalStrength) {
        int i = 4;
        int cdmaDbm = signalStrength.getCdmaDbm();
        int cdmaEcio = signalStrength.getCdmaEcio();
        int i2 = cdmaDbm >= -75 ? 4 : cdmaDbm >= -85 ? 3 : cdmaDbm >= -95 ? 2 : cdmaDbm >= -100 ? 1 : 0;
        if (cdmaEcio < -90) {
            i = cdmaEcio >= -110 ? 3 : cdmaEcio >= -130 ? 2 : cdmaEcio >= -150 ? 1 : 0;
        }
        if (i2 >= i) {
            i2 = i;
        }
        w.a("SignalUtils", "getCdmaLevel=" + i2 + ",cdmaDbm=" + cdmaDbm + ",cdmaEcio=" + cdmaEcio);
        return i2;
    }
}
