package cn.egame.terminal.sdk.log;

import java.net.Socket;
import java.security.KeyStore;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.apache.http.conn.ssl.SSLSocketFactory;

public class h extends SSLSocketFactory {
    SSLContext a = SSLContext.getInstance("TLS");

    public h(KeyStore keyStore) {
        super(keyStore);
        i iVar = new i(this);
        this.a.init(null, new TrustManager[]{iVar}, null);
    }

    public Socket createSocket() {
        return this.a.getSocketFactory().createSocket();
    }

    public Socket createSocket(Socket socket, String str, int i, boolean z) {
        return this.a.getSocketFactory().createSocket(socket, str, i, z);
    }
}
