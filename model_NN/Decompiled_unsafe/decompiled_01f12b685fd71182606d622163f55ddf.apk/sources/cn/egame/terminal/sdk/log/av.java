package cn.egame.terminal.sdk.log;

import android.net.NetworkInfo;

public enum av {
    SUB_TYPE1(1, "cu2g", "联通2G"),
    SUB_TYPE2(2, "cm2g", "移动2G"),
    SUB_TYPE3(3, "cu3g", "联通3G"),
    SUB_TYPE4(4, "ct2g", "电信2G"),
    SUB_TYPE5(5, "ct3g", "电信3G"),
    SUB_TYPE6(6, "ct3g", "电信3G"),
    SUB_TYPE8(8, "cu3g", "联通3G"),
    SUB_TYPE9(9, "HSUPA", "HSUPA"),
    SUB_TYPE10(10, "HSPA", "HSPA"),
    SUB_TYPE11(11, "IDEN", "IDEN"),
    SUB_TYPE12(12, "ct3g", "电信3G"),
    SUB_TYPE13(13, "cu3g", "联通3G"),
    SUB_TYPE15(15, "HSPAP", "HSPAP"),
    UNKNOW_SUB_TYPE(-2, "unknow", "未知类型"),
    UNKNOW_TYPE(-1, "unknow", "未知类型"),
    WIFI_TYPE(0, "wifi", "wifi网络");
    
    private int q;
    private String r;
    private String s;

    private av(int i, String str, String str2) {
        this.q = i;
        this.r = str;
        this.s = str2;
    }

    public static av a(NetworkInfo networkInfo) {
        if (networkInfo == null) {
            return UNKNOW_TYPE;
        }
        if (networkInfo.getType() == 1) {
            return WIFI_TYPE;
        }
        if (networkInfo.getType() != 0) {
            return UNKNOW_TYPE;
        }
        switch (networkInfo.getSubtype()) {
            case 1:
                return SUB_TYPE1;
            case 2:
                return SUB_TYPE2;
            case 3:
                return SUB_TYPE3;
            case 4:
                return SUB_TYPE4;
            case 5:
                return SUB_TYPE5;
            case 6:
                return SUB_TYPE6;
            case 7:
            case 14:
            default:
                return UNKNOW_SUB_TYPE;
            case 8:
                return SUB_TYPE8;
            case 9:
                return SUB_TYPE9;
            case 10:
                return SUB_TYPE10;
            case 11:
                return SUB_TYPE11;
            case 12:
                return SUB_TYPE12;
            case 13:
                return SUB_TYPE13;
            case 15:
                return SUB_TYPE15;
        }
    }

    public String a() {
        return this.r;
    }
}
