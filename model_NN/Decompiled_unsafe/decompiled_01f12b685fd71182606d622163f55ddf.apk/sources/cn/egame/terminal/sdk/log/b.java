package cn.egame.terminal.sdk.log;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import java.net.MalformedURLException;
import java.net.URL;

public class b {
    private p a = null;
    private j b = j.a();

    private String a(Looper looper, String str, m mVar, s sVar) {
        Handler handler = (looper == null || sVar == null) ? null : new Handler(looper);
        try {
            g a2 = e.a(str, this.b, mVar);
            String a3 = a2.a();
            a2.b();
            if (!TextUtils.isEmpty(a3)) {
                if (handler != null) {
                    try {
                        handler.post(new c(this, sVar, sVar.a(a3)));
                    } catch (Exception e) {
                        a(handler, sVar, new r(e, 2));
                    }
                }
                return a3;
            } else if (sVar == null) {
                return null;
            } else {
                a(handler, sVar, new r("The result is null or empty."));
                return null;
            }
        } catch (r e2) {
            if (sVar == null) {
                return null;
            }
            a(handler, sVar, e2);
            return null;
        }
    }

    private void a(Handler handler, t tVar, r rVar) {
        if (handler != null && tVar != null) {
            handler.post(new d(this, tVar, rVar));
        }
    }

    public String a(String str, m mVar) {
        return b(str, mVar);
    }

    public void a(j jVar) {
        this.b = jVar;
        if (this.a == null && this.b.a > 0) {
            this.a = p.a(this.b.a);
        }
    }

    public String b(String str, m mVar) {
        try {
            new URL(str);
            return a(null, str, mVar, null);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("The url can not be parsed. Please check it again.");
        }
    }
}
