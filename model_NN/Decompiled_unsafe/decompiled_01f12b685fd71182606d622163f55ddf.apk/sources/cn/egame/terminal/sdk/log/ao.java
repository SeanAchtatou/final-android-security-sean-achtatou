package cn.egame.terminal.sdk.log;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class ao extends am {
    public String f;
    public long g = 0;
    public long h = 0;
    public long i = 0;

    public Map a() {
        if (TextUtils.isEmpty(this.f) || TextUtils.isEmpty(this.a)) {
            w.a("wei.han", "activityName:" + this.f + "seessionId:" + this.a);
        }
        HashMap hashMap = new HashMap();
        hashMap.put("page_name", this.f);
        hashMap.put("session_id", this.a);
        hashMap.put("page_start", this.g + "");
        hashMap.put("page_end", this.h + "");
        hashMap.put("page_duration", this.i + "");
        if (this.b != null) {
            hashMap.put("page_value", new JSONObject(this.b).toString());
        }
        return hashMap;
    }
}
