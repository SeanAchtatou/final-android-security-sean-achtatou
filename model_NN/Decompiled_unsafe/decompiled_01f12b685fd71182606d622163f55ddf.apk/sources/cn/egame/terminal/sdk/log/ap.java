package cn.egame.terminal.sdk.log;

import android.content.Context;
import android.os.Build;
import android.os.Process;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.zip.GZIPOutputStream;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.ByteArrayEntity;

public class ap {
    public static String a() {
        return TimeZone.getDefault().getID() + " " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
    }

    public static AbstractHttpEntity a(byte[] bArr) {
        if (((long) bArr.length) < 256) {
            return new ByteArrayEntity(bArr);
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
        gZIPOutputStream.write(bArr);
        gZIPOutputStream.close();
        ByteArrayEntity byteArrayEntity = new ByteArrayEntity(byteArrayOutputStream.toByteArray());
        byteArrayEntity.setContentEncoding("gzip");
        return byteArrayEntity;
    }

    public static boolean a(Context context) {
        long h = al.h(context);
        if (h == 0) {
            return true;
        }
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        int i = instance.get(6);
        int i2 = instance.get(1);
        instance.setTimeInMillis(h);
        return i - instance.get(6) >= 3 || i2 - instance.get(1) > 1;
    }

    public static boolean a(Context context, String str) {
        return Build.VERSION.SDK_INT < 23 || context.checkPermission(str, Process.myPid(), Process.myUid()) == 0;
    }

    public static boolean b(Context context) {
        if (!as.b(context)) {
            return false;
        }
        long i = al.i(context);
        if (i == 0) {
            return true;
        }
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(System.currentTimeMillis());
        int i2 = instance.get(6);
        int i3 = instance.get(1);
        instance.setTimeInMillis(i);
        return i2 - instance.get(6) >= 3 || i3 - instance.get(1) > 1;
    }
}
