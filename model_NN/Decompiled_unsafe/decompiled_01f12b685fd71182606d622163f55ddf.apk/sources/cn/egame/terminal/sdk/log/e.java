package cn.egame.terminal.sdk.log;

import com.badlogic.gdx.net.HttpRequestHeader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.objectweb.asm.Opcodes;

public class e {
    private static Hashtable a = new Hashtable();

    public static g a(String str, j jVar, m mVar) {
        if (jVar == null) {
            throw new r("The cfg is null.");
        }
        if (mVar == null) {
            mVar = jVar.d;
        }
        if (mVar.d == null) {
            mVar.d = jVar.e;
        } else {
            mVar.d.putAll(jVar.e);
        }
        return a(str, mVar, (LinkedList) jVar.c.get(mVar.i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.egame.terminal.sdk.log.e.a(org.apache.http.client.HttpClient, org.apache.http.client.methods.HttpRequestBase, int, int, java.util.List, java.util.Map):void
     arg types: [org.apache.http.client.HttpClient, org.apache.http.client.methods.HttpGet, int, int, java.util.List, java.util.Map]
     candidates:
      cn.egame.terminal.sdk.log.e.a(org.apache.http.client.HttpClient, org.apache.http.client.methods.HttpRequestBase, java.lang.String, java.util.LinkedList, int, int):org.apache.http.HttpEntity
      cn.egame.terminal.sdk.log.e.a(org.apache.http.client.HttpClient, org.apache.http.client.methods.HttpRequestBase, int, int, java.util.List, java.util.Map):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: cn.egame.terminal.sdk.log.e.a(org.apache.http.client.HttpClient, org.apache.http.client.methods.HttpRequestBase, java.lang.String, java.util.LinkedList, int, int):org.apache.http.HttpEntity
     arg types: [org.apache.http.client.HttpClient, org.apache.http.client.methods.HttpGet, java.lang.String, java.util.LinkedList, int, int]
     candidates:
      cn.egame.terminal.sdk.log.e.a(org.apache.http.client.HttpClient, org.apache.http.client.methods.HttpRequestBase, int, int, java.util.List, java.util.Map):void
      cn.egame.terminal.sdk.log.e.a(org.apache.http.client.HttpClient, org.apache.http.client.methods.HttpRequestBase, java.lang.String, java.util.LinkedList, int, int):org.apache.http.HttpEntity */
    private static g a(String str, m mVar, LinkedList linkedList) {
        HttpGet httpGet;
        int i = 1;
        int i2 = mVar.j;
        int i3 = mVar.a;
        int i4 = mVar.b;
        int i5 = mVar.c;
        List list = mVar.e;
        Map map = mVar.d;
        HttpEntity httpEntity = mVar.f;
        String str2 = mVar.i;
        HttpHost httpHost = mVar.h;
        HttpClient b = str.startsWith("https") ? b() : new DefaultHttpClient();
        if (httpHost != null) {
            b.getParams().setParameter("http.route.default-proxy", httpHost);
        }
        if (i2 == 0) {
            httpGet = new HttpGet(a(str, str2, linkedList));
        } else {
            HttpGet httpPost = new HttpPost(a(str, str2, linkedList));
            if (httpEntity != null) {
                httpPost.setEntity(httpEntity);
            } else {
                i = i5;
            }
            i5 = i;
            httpGet = httpPost;
        }
        a(b, (HttpRequestBase) httpGet, i3, i4, list, map);
        try {
            return new g(b, httpGet, a(b, (HttpRequestBase) httpGet, str2, linkedList, i5, 0));
        } catch (r e) {
            if (!httpGet.isAborted()) {
                httpGet.abort();
            }
            b.getConnectionManager().shutdown();
            throw e;
        } catch (Exception e2) {
            if (!httpGet.isAborted()) {
                httpGet.abort();
            }
            b.getConnectionManager().shutdown();
            throw new r(e2.getMessage(), 1);
        }
    }

    private static URI a(String str, String str2, LinkedList linkedList) {
        URI create = URI.create(str);
        if (linkedList == null || linkedList.isEmpty() || !a.containsKey(str2)) {
            return create;
        }
        String authority = create.getAuthority();
        Integer num = (Integer) a.get(str2);
        return (num == null || linkedList.size() <= num.intValue()) ? create : URI.create(str.replaceFirst(authority, URI.create((String) linkedList.get(num.intValue())).getAuthority()));
    }

    private static HttpEntity a(HttpClient httpClient, HttpRequestBase httpRequestBase, String str, LinkedList linkedList, int i, int i2) {
        String str2 = "";
        if (w.a) {
            Header[] allHeaders = httpRequestBase.getAllHeaders();
            StringBuilder sb = new StringBuilder();
            for (Header header : allHeaders) {
                sb.append(header.getName() + ":" + header.getValue() + ";");
            }
            str2 = sb.toString();
        }
        long j = 0;
        long currentTimeMillis = System.currentTimeMillis();
        int i3 = 0;
        while (i3 <= i) {
            if (linkedList != null && !linkedList.isEmpty() && i3 > 0) {
                a(httpRequestBase, str, linkedList);
            }
            try {
                HttpResponse execute = httpClient.execute(httpRequestBase);
                long currentTimeMillis2 = j + (System.currentTimeMillis() - currentTimeMillis);
                int statusCode = execute.getStatusLine().getStatusCode();
                switch (statusCode) {
                    case 200:
                        if (w.a) {
                            w.a("TUBE", "Request: " + httpRequestBase.getURI().toString() + "\n----Headers: " + str2 + "\n----Response time: " + currentTimeMillis2 + "ms.");
                        }
                        return execute.getEntity();
                    case 301:
                    case 302:
                        if (i2 >= 4) {
                            throw new r("We got 302 redirect code too many times. Stop trying.", 3);
                        }
                        Header firstHeader = execute.getFirstHeader("location");
                        if (firstHeader != null) {
                            String replaceAll = firstHeader.getValue().replaceAll(" ", "").replaceAll(String.valueOf(9), "");
                            httpRequestBase.setURI(URI.create(replaceAll));
                            w.a("TUBE", "RedirectUrl-->" + replaceAll);
                            return a(httpClient, httpRequestBase, str, linkedList, i, i2 + 1);
                        }
                        throw new r("We got 302 redirect code, but no location.", 3);
                    default:
                        if (w.a) {
                            w.a("TUBE", "Request: " + httpRequestBase.getURI().toString() + "\n----Headers: " + str2 + "\n----Response time: " + currentTimeMillis2 + "ms.");
                        }
                        throw new r("HttpStatus is not OK. -> " + statusCode, 3);
                }
            } catch (IOException e) {
                j += System.currentTimeMillis() - currentTimeMillis;
                if (i3 != i) {
                    a();
                }
                i3++;
            } catch (Exception e2) {
                throw new r("The exception is unknow: " + e2.getMessage(), 1);
            }
        }
        if (w.a) {
            w.a("TUBE", "Request: " + httpRequestBase.getURI().toString() + "\n----Headers: " + str2 + "\n----Response time: " + j + "ms.");
        }
        throw new r("All connections is failed. Please check the network.", 1);
    }

    private static void a() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
        }
    }

    private static void a(HttpClient httpClient, HttpRequestBase httpRequestBase, int i, int i2, List list, Map map) {
        if (httpRequestBase instanceof HttpPost) {
            ((DefaultHttpClient) httpClient).setRedirectHandler(new f());
        }
        HttpParams params = httpClient.getParams();
        if (i > 0) {
            params.setIntParameter("http.socket.timeout", i);
        }
        if (i2 > 0) {
            params.setIntParameter("http.connection.timeout", i2);
        }
        httpRequestBase.setHeader(HttpRequestHeader.AcceptEncoding, "gzip");
        if (list != null) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                httpRequestBase.setHeader((Header) it.next());
            }
        }
        if (map != null) {
            for (String str : map.keySet()) {
                httpRequestBase.setHeader(str, (String) map.get(str));
            }
        }
    }

    private static void a(HttpRequestBase httpRequestBase, String str, LinkedList linkedList) {
        if (!a.containsKey(str)) {
            a.put(str, 0);
        }
        int intValue = ((Integer) a.get(str)).intValue() + 1;
        if (intValue >= linkedList.size()) {
            intValue = 0;
        }
        a.put(str, Integer.valueOf(intValue));
        String host = URI.create((String) linkedList.get(intValue)).getHost();
        URI uri = httpRequestBase.getURI();
        httpRequestBase.setURI(URI.create(uri.toString().replaceFirst(uri.getHost(), host)));
    }

    /* access modifiers changed from: private */
    public static String b(HttpEntity httpEntity) {
        String str;
        if (httpEntity == null) {
            throw new IOException("Entity is null!");
        }
        Header contentEncoding = httpEntity.getContentEncoding();
        boolean z = contentEncoding != null && contentEncoding.getValue().equals("gzip");
        if (!httpEntity.isChunked() && !z) {
            return EntityUtils.toString(httpEntity);
        }
        u uVar = new u();
        InputStream content = httpEntity.getContent();
        InputStream gZIPInputStream = z ? new GZIPInputStream(content) : content;
        byte[] bArr = new byte[Opcodes.ACC_ANNOTATION];
        int length = bArr.length / 2;
        int i = 0;
        int i2 = 0;
        while (i != -1) {
            i = gZIPInputStream.read(bArr, i2, bArr.length - i2);
            if (i != -1) {
                i2 += i;
            }
            if (i == -1 || i2 >= length) {
                uVar.a(bArr, 0, i2);
                i2 = 0;
            }
        }
        if (content != null) {
            content.close();
        }
        if (gZIPInputStream != null) {
            gZIPInputStream.close();
        }
        synchronized (uVar) {
            byte[] bArr2 = new byte[uVar.b()];
            int i3 = 0;
            while (true) {
                v a2 = uVar.a();
                if (a2 == null) {
                    str = new String(bArr2);
                } else {
                    if (a2.b != 0) {
                        System.arraycopy(a2.a, 0, bArr2, i3, a2.b);
                        i3 += a2.b;
                    }
                    a2.a();
                }
            }
        }
        return str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static org.apache.http.client.HttpClient b() {
        /*
            r2 = 0
            java.lang.String r0 = java.security.KeyStore.getDefaultType()     // Catch:{ Exception -> 0x0020 }
            java.security.KeyStore r0 = java.security.KeyStore.getInstance(r0)     // Catch:{ Exception -> 0x0020 }
            r1 = 0
            r3 = 0
            r0.load(r1, r3)     // Catch:{ Exception -> 0x0020 }
            cn.egame.terminal.sdk.log.h r1 = new cn.egame.terminal.sdk.log.h     // Catch:{ Exception -> 0x0020 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0020 }
            org.apache.http.conn.ssl.X509HostnameVerifier r0 = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER     // Catch:{ Exception -> 0x006b }
            r1.setHostnameVerifier(r0)     // Catch:{ Exception -> 0x006b }
        L_0x0018:
            if (r1 != 0) goto L_0x002c
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient
            r0.<init>()
        L_0x001f:
            return r0
        L_0x0020:
            r0 = move-exception
            r1 = r2
        L_0x0022:
            java.lang.String r2 = "TUBE"
            java.lang.String r0 = r0.getLocalizedMessage()
            cn.egame.terminal.sdk.log.w.a(r2, r0)
            goto L_0x0018
        L_0x002c:
            org.apache.http.params.BasicHttpParams r2 = new org.apache.http.params.BasicHttpParams
            r2.<init>()
            org.apache.http.HttpVersion r0 = org.apache.http.HttpVersion.HTTP_1_1
            org.apache.http.params.HttpProtocolParams.setVersion(r2, r0)
            java.lang.String r0 = "ISO-8859-1"
            org.apache.http.params.HttpProtocolParams.setContentCharset(r2, r0)
            r0 = 1
            org.apache.http.params.HttpProtocolParams.setUseExpectContinue(r2, r0)
            org.apache.http.conn.scheme.SchemeRegistry r0 = new org.apache.http.conn.scheme.SchemeRegistry
            r0.<init>()
            org.apache.http.conn.scheme.Scheme r3 = new org.apache.http.conn.scheme.Scheme
            java.lang.String r4 = "http"
            org.apache.http.conn.scheme.PlainSocketFactory r5 = org.apache.http.conn.scheme.PlainSocketFactory.getSocketFactory()
            r6 = 80
            r3.<init>(r4, r5, r6)
            r0.register(r3)
            org.apache.http.conn.scheme.Scheme r3 = new org.apache.http.conn.scheme.Scheme
            java.lang.String r4 = "https"
            r5 = 443(0x1bb, float:6.21E-43)
            r3.<init>(r4, r1, r5)
            r0.register(r3)
            org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager r1 = new org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager
            r1.<init>(r2, r0)
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient
            r0.<init>(r1, r2)
            goto L_0x001f
        L_0x006b:
            r0 = move-exception
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.egame.terminal.sdk.log.e.b():org.apache.http.client.HttpClient");
    }
}
