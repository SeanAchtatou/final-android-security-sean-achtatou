package cn.egame.terminal.sdk.log;

import android.content.Context;
import android.os.Process;
import java.lang.Thread;

public class ad implements Thread.UncaughtExceptionHandler {
    /* access modifiers changed from: private */
    public static String a = "==EGAME==LOG==\n";
    private static ad b = null;
    private Thread.UncaughtExceptionHandler c = null;
    /* access modifiers changed from: private */
    public Context d = null;
    private EgameCrashListener e = null;

    private ad(Context context, EgameCrashListener egameCrashListener) {
        this.d = context;
        this.e = egameCrashListener;
        this.c = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public static ad a(Context context, EgameCrashListener egameCrashListener) {
        if (b == null) {
            b = new ad(context, egameCrashListener);
        }
        return b;
    }

    private boolean a(Throwable th) {
        if (th == null) {
            return false;
        }
        new ae(this, th).start();
        if (this.e != null) {
            this.e.onHandlerException(th);
        }
        return true;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        if (a(th) || this.c == null) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e2) {
            }
            Process.killProcess(Process.myPid());
            System.exit(0);
            return;
        }
        this.c.uncaughtException(thread, th);
    }
}
