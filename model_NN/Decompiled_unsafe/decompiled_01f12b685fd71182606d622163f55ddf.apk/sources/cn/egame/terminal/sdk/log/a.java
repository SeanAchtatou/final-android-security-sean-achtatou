package cn.egame.terminal.sdk.log;

public class a {
    private static volatile a a;
    private b b = new b();

    private a() {
    }

    public static a a() {
        if (a == null) {
            synchronized (a.class) {
                if (a == null) {
                    a = new a();
                }
            }
        }
        return a;
    }

    public String a(String str, m mVar) {
        return this.b.b(str, mVar);
    }
}
