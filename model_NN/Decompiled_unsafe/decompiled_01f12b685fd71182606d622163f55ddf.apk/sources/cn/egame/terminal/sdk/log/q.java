package cn.egame.terminal.sdk.log;

class q extends Thread {
    final /* synthetic */ p a;
    private int b;
    private Runnable c = null;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q(p pVar, int i) {
        super(pVar, "TubeThread" + i);
        this.a = pVar;
        this.b = i;
        w.a("TubeThreadPool", "---creat WorkThread id=" + i);
    }

    public void run() {
        while (!isInterrupted()) {
            try {
                this.c = this.a.b(this.b);
            } catch (InterruptedException e) {
                w.a("TubeThreadPool", e.getLocalizedMessage());
            }
            if (this.c != null) {
                try {
                    this.c.run();
                } catch (Throwable th) {
                    w.a("TubeThreadPool", th.getLocalizedMessage());
                }
                this.c = null;
            } else {
                return;
            }
        }
    }
}
