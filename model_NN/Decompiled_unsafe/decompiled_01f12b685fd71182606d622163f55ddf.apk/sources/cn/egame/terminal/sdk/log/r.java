package cn.egame.terminal.sdk.log;

public class r extends Exception {
    private int a;

    public r(String str) {
        this(str, -1);
    }

    public r(String str, int i) {
        super(str);
        this.a = -1;
        this.a = i;
    }

    public r(Throwable th, int i) {
        super(th);
        this.a = -1;
        this.a = i;
    }
}
