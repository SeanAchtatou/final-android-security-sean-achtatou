package cn.egame.terminal.sdk.log;

public interface EgameCrashListener {
    void onHandlerException(Throwable th);
}
