package cn.egame.terminal.sdk.log;

import android.content.Context;
import java.util.Map;

final class ab implements Runnable {
    final /* synthetic */ Context a;
    final /* synthetic */ String b;
    final /* synthetic */ Map c;
    final /* synthetic */ String d;
    final /* synthetic */ String e;
    final /* synthetic */ String f;

    ab(Context context, String str, Map map, String str2, String str3, String str4) {
        this.a = context;
        this.b = str;
        this.c = map;
        this.d = str2;
        this.e = str3;
        this.f = str4;
    }

    public void run() {
        EgameAgent.b(this.a, this.b, this.c, this.d, this.e, this.f);
    }
}
