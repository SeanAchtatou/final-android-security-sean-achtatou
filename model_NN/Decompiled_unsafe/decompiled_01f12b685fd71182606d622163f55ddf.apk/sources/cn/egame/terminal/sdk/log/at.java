package cn.egame.terminal.sdk.log;

import android.text.TextUtils;

public enum at {
    CHINA_TELECOM(1, "中国电信", new String[]{"46003", "46005", "46011"}),
    CHINA_MOBILE(2, "中国移动", new String[]{"46020", "46000", "46002", "46007"}),
    CHINA_UNICOM(3, "中国联通", new String[]{"46001", "46006"}),
    NOT_DEFINE(4, "未定义", new String[0]);
    
    private int e;
    private String f;
    private String[] g;

    private at(int i, String str, String[] strArr) {
        this.e = i;
        this.f = str;
        this.g = new String[strArr.length];
        System.arraycopy(strArr, 0, this.g, 0, strArr.length);
    }

    public static at a(String str) {
        if (TextUtils.isEmpty(str)) {
            return NOT_DEFINE;
        }
        for (String str2 : CHINA_TELECOM.g) {
            if (str2.equals(str) || str.startsWith(str2)) {
                return CHINA_TELECOM;
            }
        }
        for (String str3 : CHINA_MOBILE.g) {
            if (str3.equals(str) || str.startsWith(str3)) {
                return CHINA_MOBILE;
            }
        }
        for (String str4 : CHINA_UNICOM.g) {
            if (str4.equals(str) || str.startsWith(str4)) {
                return CHINA_UNICOM;
            }
        }
        return NOT_DEFINE;
    }

    public int a() {
        return this.e;
    }
}
