package cn.egame.terminal.sdk.log;

import java.lang.ref.SoftReference;

public class v {
    public byte[] a;
    public int b = 0;

    public v(int i) {
        this.a = new byte[i];
    }

    public void a() {
        this.b = 0;
        synchronized (u.a) {
            u.a.offer(new SoftReference(this, u.b));
            u.a.notifyAll();
        }
    }
}
