package cn.egame.terminal.sdk;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import android.os.Process;
import android.util.Log;
import cn.egame.terminal.sdk.jni.EgamePayProtocol;
import java.util.HashMap;

public class EgameCoreService extends Service {
    public static final String ACTION_SERVICE_CMD = "cn.egame.terminal.sdk.SERVICE_CMD";

    public IBinder onBind(Intent intent) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("service", this);
        params.put("intent", intent);
        Object object = EgamePayProtocol.callCore("onBind", params);
        if (object != null) {
            return (IBinder) object;
        }
        return null;
    }

    public void onRebind(Intent intent) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("service", this);
        params.put("intent", intent);
        EgamePayProtocol.callCore("onRebind", params);
    }

    public boolean onUnbind(Intent intent) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("service", this);
        params.put("intent", intent);
        return ((Boolean) EgamePayProtocol.callCore("onUnbind", params)).booleanValue();
    }

    public void onCreate() {
        if (EgamePayProtocol.initCore(this) == null) {
            Log.e("ECS", "ecs init failed.");
            stopSelf();
            Process.killProcess(Process.myPid());
            return;
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put("service", this);
        EgamePayProtocol.callCore("onCreate", params);
    }

    public void onDestroy() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("service", this);
        EgamePayProtocol.callCore("onDestroy", params);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        HashMap<String, Object> params = new HashMap<>();
        params.put("service", this);
        params.put("configuration", newConfig);
        EgamePayProtocol.callCore("onConfigurationChanged", params);
    }

    public void onLowMemory() {
        super.onLowMemory();
        HashMap<String, Object> params = new HashMap<>();
        params.put("service", this);
        EgamePayProtocol.callCore("onLowMemory", params);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("service", this);
        params.put("intent", intent);
        params.put("flags", Integer.valueOf(flags));
        params.put("startId", Integer.valueOf(startId));
        Object object = EgamePayProtocol.callCore("onStartCommand", params);
        if (object != null) {
            return ((Integer) object).intValue();
        }
        return super.onStartCommand(intent, flags, startId);
    }
}
