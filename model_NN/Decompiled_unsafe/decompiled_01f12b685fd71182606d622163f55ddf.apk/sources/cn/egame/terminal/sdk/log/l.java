package cn.egame.terminal.sdk.log;

import java.util.HashMap;
import java.util.Map;

public class l {
    private int a;
    private boolean b;
    private Map c;
    private HashMap d;
    private m e;

    public l() {
        this.a = 0;
        this.b = false;
        this.c = null;
        this.d = new HashMap();
        this.e = null;
        this.c = new HashMap();
    }

    public j a() {
        j jVar = new j();
        if (this.e == null) {
            this.e = new o().a();
        }
        jVar.a = this.a;
        jVar.c = this.c;
        jVar.b = this.b;
        jVar.e = this.d;
        jVar.d = this.e;
        return jVar;
    }

    public l a(m mVar) {
        this.e = mVar;
        return this;
    }

    public l a(HashMap hashMap) {
        this.d.putAll(hashMap);
        return this;
    }

    public l a(boolean z) {
        this.b = z;
        return this;
    }
}
