package cn.egame.terminal.sdk.log;

import android.content.Context;
import android.text.TextUtils;
import com.badlogic.gdx.graphics.GL20;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

public class aj {
    public static InputStream a() {
        return new FileInputStream(new File(af.a + File.separator + "egame_lcpf.log"));
    }

    /* JADX WARNING: Removed duplicated region for block: B:105:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:107:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0118 A[SYNTHETIC, Splitter:B:66:0x0118] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0135 A[SYNTHETIC, Splitter:B:75:0x0135] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0149 A[SYNTHETIC, Splitter:B:81:0x0149] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r8, cn.egame.terminal.sdk.log.ak r9) {
        /*
            r1 = 0
            if (r9 != 0) goto L_0x0004
        L_0x0003:
            return
        L_0x0004:
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.io.File r3 = r8.getFilesDir()
            java.lang.String r3 = r3.getAbsolutePath()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = java.io.File.separator
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "egame_log_cached_file.log"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.<init>(r2)
            boolean r2 = r0.exists()
            if (r2 == 0) goto L_0x0003
            r2 = 0
            java.lang.String r3 = "egame_log_cached_file.log"
            java.io.FileInputStream r3 = r8.openFileInput(r3)     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            if (r3 != 0) goto L_0x004a
            if (r1 == 0) goto L_0x0003
            r2.close()     // Catch:{ IOException -> 0x003f }
            goto L_0x0003
        L_0x003f:
            r0 = move-exception
            java.lang.String r1 = "EGAME_LOG"
            java.lang.String r0 = r0.getLocalizedMessage()
            cn.egame.terminal.sdk.log.w.a(r1, r0)
            goto L_0x0003
        L_0x004a:
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            r4.<init>()     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            r5 = 8192(0x2000, float:1.14794E-41)
            byte[] r5 = new byte[r5]     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
        L_0x0053:
            int r6 = r3.read(r5)     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            r7 = -1
            if (r6 == r7) goto L_0x007a
            r7 = 0
            r4.write(r5, r7, r6)     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            goto L_0x0053
        L_0x005f:
            r0 = move-exception
        L_0x0060:
            java.lang.String r2 = "EGAME_LOG"
            java.lang.String r0 = r0.getLocalizedMessage()     // Catch:{ all -> 0x0146 }
            cn.egame.terminal.sdk.log.w.a(r2, r0)     // Catch:{ all -> 0x0146 }
            if (r1 == 0) goto L_0x0003
            r1.close()     // Catch:{ IOException -> 0x006f }
            goto L_0x0003
        L_0x006f:
            r0 = move-exception
            java.lang.String r1 = "EGAME_LOG"
            java.lang.String r0 = r0.getLocalizedMessage()
            cn.egame.terminal.sdk.log.w.a(r1, r0)
            goto L_0x0003
        L_0x007a:
            r3.close()     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            byte[] r3 = r4.toByteArray()     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            if (r3 != 0) goto L_0x0099
            r4.close()     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            if (r1 == 0) goto L_0x0003
            r2.close()     // Catch:{ IOException -> 0x008d }
            goto L_0x0003
        L_0x008d:
            r0 = move-exception
            java.lang.String r1 = "EGAME_LOG"
            java.lang.String r0 = r0.getLocalizedMessage()
            cn.egame.terminal.sdk.log.w.a(r1, r0)
            goto L_0x0003
        L_0x0099:
            r4.close()     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            r3.<init>(r2)     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            if (r2 != 0) goto L_0x00c0
            if (r2 == 0) goto L_0x0003
            r2.close()     // Catch:{ IOException -> 0x00b4 }
            goto L_0x0003
        L_0x00b4:
            r0 = move-exception
            java.lang.String r1 = "EGAME_LOG"
            java.lang.String r0 = r0.getLocalizedMessage()
            cn.egame.terminal.sdk.log.w.a(r1, r0)
            goto L_0x0003
        L_0x00c0:
            java.lang.String r3 = r2.readLine()     // Catch:{ FileNotFoundException -> 0x00ca, IOException -> 0x015e, NullPointerException -> 0x015b, all -> 0x0158 }
            if (r3 == 0) goto L_0x00cd
            r9.a(r3)     // Catch:{ FileNotFoundException -> 0x00ca, IOException -> 0x015e, NullPointerException -> 0x015b, all -> 0x0158 }
            goto L_0x00c0
        L_0x00ca:
            r0 = move-exception
            r1 = r2
            goto L_0x0060
        L_0x00cd:
            r2.close()     // Catch:{ FileNotFoundException -> 0x00ca, IOException -> 0x015e, NullPointerException -> 0x015b, all -> 0x0158 }
            r2 = 0
            boolean r3 = r0.delete()     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            if (r3 != 0) goto L_0x00f9
            java.io.File r3 = new java.io.File     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            r4.<init>()     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            java.lang.String r5 = r0.getAbsolutePath()     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            java.lang.String r5 = "_tmp"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            java.lang.String r4 = r4.toString()     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            r3.<init>(r4)     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            r0.renameTo(r3)     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
            r0.delete()     // Catch:{ FileNotFoundException -> 0x005f, IOException -> 0x010c, NullPointerException -> 0x0129 }
        L_0x00f9:
            if (r1 == 0) goto L_0x0003
            r2.close()     // Catch:{ IOException -> 0x0100 }
            goto L_0x0003
        L_0x0100:
            r0 = move-exception
            java.lang.String r1 = "EGAME_LOG"
            java.lang.String r0 = r0.getLocalizedMessage()
            cn.egame.terminal.sdk.log.w.a(r1, r0)
            goto L_0x0003
        L_0x010c:
            r0 = move-exception
        L_0x010d:
            java.lang.String r2 = "EGAME_LOG"
            java.lang.String r0 = r0.getLocalizedMessage()     // Catch:{ all -> 0x0146 }
            cn.egame.terminal.sdk.log.w.a(r2, r0)     // Catch:{ all -> 0x0146 }
            if (r1 == 0) goto L_0x0003
            r1.close()     // Catch:{ IOException -> 0x011d }
            goto L_0x0003
        L_0x011d:
            r0 = move-exception
            java.lang.String r1 = "EGAME_LOG"
            java.lang.String r0 = r0.getLocalizedMessage()
            cn.egame.terminal.sdk.log.w.a(r1, r0)
            goto L_0x0003
        L_0x0129:
            r0 = move-exception
        L_0x012a:
            java.lang.String r2 = "EGAME_LOG"
            java.lang.String r0 = r0.getLocalizedMessage()     // Catch:{ all -> 0x0146 }
            cn.egame.terminal.sdk.log.w.a(r2, r0)     // Catch:{ all -> 0x0146 }
            if (r1 == 0) goto L_0x0003
            r1.close()     // Catch:{ IOException -> 0x013a }
            goto L_0x0003
        L_0x013a:
            r0 = move-exception
            java.lang.String r1 = "EGAME_LOG"
            java.lang.String r0 = r0.getLocalizedMessage()
            cn.egame.terminal.sdk.log.w.a(r1, r0)
            goto L_0x0003
        L_0x0146:
            r0 = move-exception
        L_0x0147:
            if (r1 == 0) goto L_0x014c
            r1.close()     // Catch:{ IOException -> 0x014d }
        L_0x014c:
            throw r0
        L_0x014d:
            r1 = move-exception
            java.lang.String r2 = "EGAME_LOG"
            java.lang.String r1 = r1.getLocalizedMessage()
            cn.egame.terminal.sdk.log.w.a(r2, r1)
            goto L_0x014c
        L_0x0158:
            r0 = move-exception
            r1 = r2
            goto L_0x0147
        L_0x015b:
            r0 = move-exception
            r1 = r2
            goto L_0x012a
        L_0x015e:
            r0 = move-exception
            r1 = r2
            goto L_0x010d
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.egame.terminal.sdk.log.aj.a(android.content.Context, cn.egame.terminal.sdk.log.ak):void");
    }

    public static void a(Context context, String str) {
        File file = new File(context.getFilesDir().getAbsolutePath() + File.separator + "egame_log_cached_file.log");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                w.a("EGAME_LOG", e.getLocalizedMessage());
            }
        }
        try {
            FileOutputStream openFileOutput = context.openFileOutput("egame_log_cached_file.log", GL20.GL_COVERAGE_BUFFER_BIT_NV);
            a(openFileOutput, str);
            openFileOutput.close();
        } catch (FileNotFoundException e2) {
            w.a("EGAME_LOG", e2.getLocalizedMessage());
        } catch (IOException e3) {
            w.a("EGAME_LOG", e3.getLocalizedMessage());
        }
    }

    private static void a(FileOutputStream fileOutputStream, String str) {
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream, Charset.forName("UTF-8")));
        bufferedWriter.write(str);
        bufferedWriter.newLine();
        bufferedWriter.close();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public static void b(Context context, String str) {
        if (ap.a(context, "android.permission.WRITE_EXTERNAL_STORAGE") && !TextUtils.isEmpty(str)) {
            File file = new File(af.a);
            if (!file.exists()) {
                file.mkdirs();
            } else if (!file.isDirectory()) {
                file.delete();
                file.mkdirs();
            }
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(new File(af.a + File.separator + "egame_lcpf.log"), true);
                a(fileOutputStream, str);
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                w.a("EGAME_LOG", e.getLocalizedMessage());
            } catch (IOException e2) {
                w.a("EGAME_LOG", e2.getLocalizedMessage());
            }
        }
    }

    public static boolean b() {
        return new File(af.a + File.separator + "egame_lcpf.log").delete();
    }
}
