package cn.egame.terminal.paysdk;

import java.util.Map;

public interface EgamePayListener {
    void payCancel(Map<String, String> map);

    void payFailed(Map<String, String> map, int i);

    void paySuccess(Map<String, String> map);
}
