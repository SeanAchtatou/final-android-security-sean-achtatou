package cn.egame.terminal.paysdk;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import cn.egame.terminal.sdk.jni.EgamePayProtocol;
import java.util.HashMap;

public class EgamePayActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        params.put("bundle", savedInstanceState);
        EgamePayProtocol.call("onCreate", params);
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        params.put("event", event);
        boolean result = ((Boolean) EgamePayProtocol.call("dispatchKeyEvent", params)).booleanValue();
        return result ? result : super.dispatchKeyEvent(event);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        params.put("keyCode", Integer.valueOf(keyCode));
        params.put("event", event);
        boolean result = ((Boolean) EgamePayProtocol.call("onKeyDown", params)).booleanValue();
        return result ? result : super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        EgamePayProtocol.call("onPause", params);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        EgamePayProtocol.call("onResume", params);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        EgamePayProtocol.call("onDestroy", params);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        params.put("configuration", newConfig);
        EgamePayProtocol.call("onConfigurationChanged", params);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        params.put("request", Integer.valueOf(requestCode));
        params.put("result", Integer.valueOf(resultCode));
        params.put("data", data);
        EgamePayProtocol.call("onActivityResult", params);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public static void onRequestPermissionsResult(Activity activity, int requestCode, String[] permissions, int[] grantResults) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", activity);
        params.put("request", Integer.valueOf(requestCode));
        params.put("permissions", permissions);
        params.put("grantResults", grantResults);
        EgamePayProtocol.call("onRequestPermissionsResult", params);
    }

    @TargetApi(23)
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("activity", this);
        params.put("request", Integer.valueOf(requestCode));
        params.put("permissions", permissions);
        params.put("grantResults", grantResults);
        EgamePayProtocol.call("onRequestPermissionsResult", params);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
