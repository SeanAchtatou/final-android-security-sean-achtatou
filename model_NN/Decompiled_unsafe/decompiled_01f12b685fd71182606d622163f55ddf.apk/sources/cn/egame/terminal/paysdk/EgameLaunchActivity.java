package cn.egame.terminal.paysdk;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;

public class EgameLaunchActivity extends Activity {
    private static final int PIC_SHOW_WIDTH_PADING = 40;
    private ImageView logoView;
    private Bitmap mBitmap;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFFFFF")));
        if (initView()) {
            this.logoView.postDelayed(new Runnable() {
                public void run() {
                    EgameLaunchActivity.this.startGame();
                }
            }, 3000);
        } else {
            startGame();
        }
    }

    /* access modifiers changed from: private */
    public void startGame() {
        try {
            String gameLaunchActivity = getPackageManager().getApplicationInfo(getPackageName(), 128).metaData.getString("EGAME_LAUNCH_ACTIVITY");
            Intent intent = new Intent();
            intent.setClassName(this, gameLaunchActivity);
            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(this, "启动游戏失败，检查游戏启动类配置", 1).show();
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mBitmap != null) {
            this.mBitmap.recycle();
        }
    }

    private boolean initView() {
        this.mBitmap = getFitBitmap();
        if (this.mBitmap != null) {
            this.logoView = new ImageView(this);
            this.logoView.setScaleType(ImageView.ScaleType.FIT_XY);
            this.logoView.setImageBitmap(this.mBitmap);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(-2, -2);
            params.gravity = 17;
            FrameLayout main = new FrameLayout(this);
            main.addView(this.logoView, params);
            setContentView(main);
        }
        return this.mBitmap != null;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (this.mBitmap != null) {
            this.mBitmap.recycle();
        }
        this.mBitmap = getFitBitmap();
        if (this.mBitmap != null && this.logoView != null) {
            this.logoView.setImageBitmap(this.mBitmap);
        }
    }

    private Bitmap getFitBitmap() {
        Bitmap mBitmap2 = getBitmap("egame_sdk_egame_logo.png");
        if (mBitmap2 != null) {
            return getScaleBitmap(mBitmap2, (double) getScale(mBitmap2));
        }
        return mBitmap2;
    }

    private float getScale(Bitmap mBitmap2) {
        int height = getScreenHeight(this);
        int screenW = getScreenWidth(this);
        int orientation = getResources().getConfiguration().orientation;
        int bitmapH = mBitmap2.getHeight();
        int width = mBitmap2.getWidth();
        if (orientation != 2) {
            return ((float) (((double) screenW) * 0.57d)) / ((float) width);
        }
        float scale = ((float) (((double) height) * 0.33d)) / ((float) bitmapH);
        if (((int) (((float) width) * scale)) > screenW - 40) {
            return ((float) (screenW - 40)) / ((float) width);
        }
        return scale;
    }

    public static int getScreenWidth(Activity context) {
        return context.getWindowManager().getDefaultDisplay().getWidth();
    }

    public static int getScreenHeight(Activity context) {
        return context.getWindowManager().getDefaultDisplay().getHeight();
    }

    public Bitmap getScaleBitmap(Bitmap bitmap, double scale) {
        if (scale == 1.0d) {
            return bitmap;
        }
        return getScaleBitmap(bitmap, ((double) bitmap.getWidth()) * scale, ((double) bitmap.getHeight()) * scale);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public Bitmap getScaleBitmap(Bitmap bitmap, double newWidth, double newHeight) {
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        double scale = Math.min(newWidth / ((double) width), newHeight / ((double) height));
        Matrix matrix = new Matrix();
        matrix.postScale((float) scale, (float) scale);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        if (bitmap == null || bitmap.isRecycled()) {
            return createBitmap;
        }
        bitmap.recycle();
        return createBitmap;
    }

    private Bitmap getBitmap(String srcFileName) {
        InputStream is = null;
        Bitmap bitmap = null;
        try {
            is = getAssets().open(srcFileName);
            bitmap = BitmapFactory.decodeStream(is);
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                }
            }
        } catch (Exception e2) {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e3) {
                }
            }
        } catch (Throwable th) {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e4) {
                }
            }
            throw th;
        }
        return bitmap;
    }
}
