package cn.egame.terminal.paysdk;

public class FailedCode {
    public static final int ERROR_CODE_DOING = -2;
    public static final int ERROR_CODE_FILE_COPY_FAILED = -13;
    public static final int ERROR_CODE_MD5_FAILED = -12;
    public static final int ERROR_CODE_NEW_INSTANCE_FAILED = -14;
    public static final int ERROR_CODE_NORMAL = -1;
    public static final int ERROR_CODE_NO_INPUT = -10;
    public static final int ERROR_CODE_NO_OUTPUT = -11;
    public static final int ERROR_CODE_SDK_VERIFY = -16;
    public static final int ERROR_CODE_SECURITY = -15;
    public static final int REASON_CODE_ACTIVITY_IS_NULL = -100;
    public static final int REASON_CODE_APPKEY_INVALID = -103;
    public static final int REASON_CODE_CHANNELID_INVALID = -203;
    public static final int REASON_CODE_CPPARAM_INVALID = -205;
    public static final int REASON_CODE_FEEINFO_IS_NULL = -101;
    public static final int REASON_CODE_GET_SERIAL_NUMBER_ERROR = -301;
    public static final int REASON_CODE_GET_SERIAL_NUMBER_NET_ERROR = -300;
    public static final int REASON_CODE_IMIS_ERROR = -104;
    public static final int REASON_CODE_INIT_FAILED = -200;
    public static final int REASON_CODE_LISTENER_IS_NULL = -201;
    public static final int REASON_CODE_NOT_FEE_CHANNEL = -108;
    public static final int REASON_CODE_OTHER_PAY_ERROR = -209;
    public static final int REASON_CODE_PACKAGENAME_ERROR = -102;
    public static final int REASON_CODE_PAY_CALL_FAST_ERROR = -206;
    public static final int REASON_CODE_PAY_SMS_SEND_ERROR = -207;
    public static final int REASON_CODE_SDK_ERROR = -107;
    public static final int REASON_CODE_SERVICE_CODE_INVALID = -204;
    public static final int REASON_CODE_TOOLSALIAS_IS_NULL = -105;
    public static final int REASON_CODE_TOOLSCODE_INVALID = -202;
    public static final int REASON_CODE_TOOLSPRICE_IS_NULL = -106;
    public static final int REASON_CODE_USERID_IS_NULL_ERROR = -208;
}
