package cn.egame.terminal.paysdk.codec;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 {
    private static char[] hexDigits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static String getIsMD5String(InputStream in) throws IOException {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            DigestInputStream digestInputStream = new DigestInputStream(in, md);
            byte[] buf = new byte[16384];
            while (digestInputStream.read(buf) > 0) {
                md = digestInputStream.getMessageDigest();
            }
            return bufferToHex(md.digest());
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    public static String getFileMD5String(String filePath) throws IOException {
        return getFileMD5String(new File(filePath));
    }

    public static String getFileMD5String(File file) throws IOException {
        return getIsMD5String(new FileInputStream(file));
    }

    public static String getMD5String(String s) {
        return getMD5String(s.getBytes());
    }

    public static String getMD5String(byte[] bytes) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(bytes);
            return bufferToHex(md.digest());
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    public static String getMD5(File file) {
        try {
            return getMD5(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    public static String getMD5(InputStream is) {
        try {
            return getMD5(null, null, is);
        } catch (IOException | NoSuchAlgorithmException e) {
            return null;
        }
    }

    public static String getMD5(MessageDigest md, byte[] buffer, InputStream is) throws NoSuchAlgorithmException, IOException {
        if (md == null) {
            md = MessageDigest.getInstance("MD5");
        }
        if (buffer == null) {
            buffer = new byte[16384];
        }
        while (true) {
            int num = is.read(buffer);
            if (num <= 0) {
                return bufferToHex(md.digest());
            }
            md.update(buffer, 0, num);
        }
    }

    private static String bufferToHex(byte[] bytes) {
        return bufferToHex(bytes, 0, bytes.length);
    }

    private static String bufferToHex(byte[] bytes, int m, int n) {
        StringBuffer stringbuffer = new StringBuffer(n * 2);
        int k = m + n;
        for (int l = m; l < k; l++) {
            appendHexPair(bytes[l], stringbuffer);
        }
        return stringbuffer.toString();
    }

    private static void appendHexPair(byte bt, StringBuffer stringbuffer) {
        char c0 = hexDigits[(bt & 240) >> 4];
        char c1 = hexDigits[bt & 15];
        stringbuffer.append(c0);
        stringbuffer.append(c1);
    }
}
