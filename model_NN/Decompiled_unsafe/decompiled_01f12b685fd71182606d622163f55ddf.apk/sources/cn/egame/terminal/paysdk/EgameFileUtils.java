package cn.egame.terminal.paysdk;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class EgameFileUtils {
    public static final String PREFIX_NAME = "egame";
    public static final String PUSH_ROOT_DIR = (Environment.getExternalStorageDirectory() + File.separator + PREFIX_NAME + File.separator + "psh1x");
    public static final String ROOT_DIR = (Environment.getExternalStorageDirectory() + File.separator + PREFIX_NAME + File.separator + "pay4x");
    protected static Object sCB = null;
    protected static Object sCC = null;
    public static Object sObjOne = null;
    public static Object sObjTwo = null;

    public static boolean streamCopy(InputStream is, OutputStream os) {
        if (is == null || os == null) {
            return false;
        }
        try {
            byte[] buf = new byte[16384];
            while (true) {
                int len = is.read(buf);
                if (len != -1) {
                    os.write(buf, 0, len);
                } else {
                    os.flush();
                    os.close();
                    is.close();
                    return true;
                }
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static InputStream getInputStream(String filePath) {
        try {
            return new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    public static OutputStream getOutputStream(String filePath) {
        try {
            return new FileOutputStream(filePath);
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    public static String findFile(String dirPath, final String start, final String end) {
        File[] list;
        if (TextUtils.isEmpty(dirPath)) {
            return null;
        }
        File dir = new File(dirPath);
        if (!dir.isDirectory() || !dir.exists() || (list = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String filename) {
                return (TextUtils.isEmpty(start) && TextUtils.isEmpty(end)) || (filename.toLowerCase().startsWith(start) && TextUtils.isEmpty(end)) || ((TextUtils.isEmpty(start) && filename.toLowerCase().endsWith(end)) || (filename.toLowerCase().startsWith(start) && filename.toLowerCase().endsWith(end)));
            }
        })) == null || list.length == 0) {
            return null;
        }
        List<File> files = Arrays.asList(list);
        Collections.sort(files, new Comparator<File>() {
            public int compare(File o1, File o2) {
                if (o1.isDirectory() && o2.isFile()) {
                    return -1;
                }
                if (!o1.isFile() || !o2.isDirectory()) {
                    return o2.getName().compareTo(o1.getName());
                }
                return 1;
            }
        });
        return ((File) files.get(0)).getName();
    }

    public static boolean deleteFile(File file) {
        if (file == null) {
            return false;
        }
        return file.delete();
    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String file : children) {
                if (!deleteDir(new File(dir, file))) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    public static String findAssetsFile(Context context, String condition) {
        String[] nameList = null;
        try {
            nameList = context.getAssets().list(PREFIX_NAME);
        } catch (IOException e) {
        }
        if (nameList == null || nameList.length == 0) {
            return null;
        }
        List<String> names = new ArrayList<>();
        for (String name : nameList) {
            if (!TextUtils.isEmpty(name) && name.toLowerCase().contains(condition.toLowerCase())) {
                names.add(name);
            }
        }
        if (names.size() <= 0) {
            return null;
        }
        Collections.sort(names, new Comparator<String>() {
            public int compare(String lhs, String rhs) {
                return EgameFileUtils.compareFileName(rhs, lhs);
            }
        });
        return (String) names.get(0);
    }

    public static int compareFileName(String lhs, String rhs) {
        return lhs.compareTo(rhs);
    }

    public static void unZipFile(InputStream zipIs, String outDir) throws IOException {
        ZipInputStream zin = new ZipInputStream(zipIs);
        while (true) {
            ZipEntry entry = zin.getNextEntry();
            if (entry != null) {
                String entryName = entry.getName();
                String path = outDir + "/" + entryName;
                if (entry.isDirectory()) {
                    File decompressDirFile = new File(path);
                    if (!decompressDirFile.exists()) {
                        decompressDirFile.mkdirs();
                    }
                } else {
                    File fileDirFile = new File(path.substring(0, path.lastIndexOf("/")));
                    if (!fileDirFile.exists()) {
                        fileDirFile.mkdirs();
                    }
                    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outDir + "/" + entryName));
                    BufferedInputStream bi = new BufferedInputStream(zin);
                    byte[] readContent = new byte[1024];
                    for (int readCount = bi.read(readContent); readCount != -1; readCount = bi.read(readContent)) {
                        bos.write(readContent, 0, readCount);
                    }
                    bos.close();
                }
            } else {
                zin.close();
                return;
            }
        }
    }

    public static List<File> listFiles(File dir) {
        ArrayList<File> list = new ArrayList<>();
        if (dir.isDirectory()) {
            for (File file : dir.listFiles()) {
                if (file.isFile()) {
                    list.add(file);
                } else {
                    list.addAll(listFiles(file));
                }
            }
        }
        return list;
    }

    protected static Object load(Context context, Object... params) throws Exception {
        Object obj = Context.class.getMethod((String) params[0], new Class[0]).invoke(context, new Object[0]);
        Object obj2 = Class.forName((String) params[1]).getConstructor(params[2].getClass(), params[3].getClass(), params[4].getClass(), obj.getClass().getSuperclass()).newInstance(params[2], params[3], params[4], obj);
        obj2.getClass().getMethod((String) params[5], params[5].getClass()).invoke(obj2, params[6]);
        return obj2;
    }
}
