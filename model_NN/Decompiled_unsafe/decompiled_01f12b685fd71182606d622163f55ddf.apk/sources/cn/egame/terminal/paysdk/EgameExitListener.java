package cn.egame.terminal.paysdk;

public interface EgameExitListener {
    void cancel();

    void exit();
}
