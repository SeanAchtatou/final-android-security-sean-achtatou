package cn.egame.terminal.paysdk;

import android.app.Activity;
import android.os.Looper;
import cn.egame.terminal.sdk.jni.EgamePayProtocol;
import cn.egame.terminal.sdk.jni.ProtocolMessage;
import java.util.HashMap;
import java.util.Map;

public class EgamePay {
    public static final String PAY_PARAMS_KEY_CP_INFO_MD5 = "cp_info_md5";
    public static final String PAY_PARAMS_KEY_CP_PARAMS = "cpParams";
    public static final String PAY_PARAMS_KEY_MONTHLY_EVENT_KEY = "monthlyEvent";
    public static final String PAY_PARAMS_KEY_MONTHLY_QUERY_EVENT = "query";
    public static final String PAY_PARAMS_KEY_MONTHLY_UNSUBSCRIBE_EVENT = "cancel";
    public static final String PAY_PARAMS_KEY_PRIORITY = "priority";
    public static final String PAY_PARAMS_KEY_TOOLS_ALIAS = "toolsAlias";
    public static final String PAY_PARAMS_KEY_TOOLS_NAME = "toolsName";
    public static final String PAY_PARAMS_KEY_TOOLS_PRICE = "toolsPrice";
    public static final String PAY_PARAMS_KEY_USERID = "userId";
    public static final String PAY_PARAMS_KEY_USE_SMSPAY = "useSmsPay";
    public static final String PYA_PARAMS_KEY_OTHER_PAY_NOTIFY_URL = "otherNotifyUrl";
    public static int sInitStatus = -2;

    public static void init(final Activity activity) {
        new Thread("ipayt") {
            public void run() {
                Looper.prepare();
                long currentTimeMillis = System.currentTimeMillis();
                ProtocolMessage result = EgamePayProtocol.initPay(activity);
                long currentTimeMillis2 = System.currentTimeMillis();
                if (result != null) {
                    EgamePay.sInitStatus = result.arg1;
                }
            }
        }.start();
    }

    public static void pay(Activity activity, Map<String, String> payParams, EgamePayListener listener) {
        if (sInitStatus != 0) {
            listener.payFailed(payParams, sInitStatus);
        } else {
            EgamePayProtocol.pay(activity, payParams, listener);
        }
    }

    public static void queryUserMonthlyOrderStatus(Activity activity, Map<String, String> params, EgameQueryOrderInterface listener) {
        if (sInitStatus != 0) {
            listener.callBack(params, sInitStatus);
            return;
        }
        Map<String, Object> param = new HashMap<>(params);
        param.put("activity", activity);
        param.put("listener", listener);
        param.put(PAY_PARAMS_KEY_CP_PARAMS, params);
        EgamePayProtocol.call(null, "queryMonthlyOrderStatus", param);
    }

    public static void exit(Activity activity, EgameExitListener listener) {
        Map<String, Object> params = new HashMap<>();
        params.put("activity", activity);
        params.put("listener", listener);
        EgamePayProtocol.call(null, "exit", params);
    }

    public static void moreGame(Activity activity) {
        Map<String, Object> params = new HashMap<>();
        params.put("activity", activity);
        EgamePayProtocol.call(null, "moreGame", params);
    }
}
