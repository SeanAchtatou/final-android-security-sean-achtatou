package org.objectweb.asm;

import com.sg.pak.PAK_ASSETS;

class MethodWriter extends MethodVisitor {
    private int A;
    private Handler B;
    private Handler C;
    private int D;
    private ByteVector E;
    private int F;
    private ByteVector G;
    private int H;
    private ByteVector I;
    private Attribute J;
    private boolean K;
    private int L;
    private final int M;
    private Label N;
    private Label O;
    private Label P;
    private int Q;
    private int R;
    private int S;
    private int T;
    final ClassWriter b;
    private int c;
    private final int d;
    private final int e;
    private final String f;
    String g;
    int h;
    int i;
    int j;
    int[] k;
    private ByteVector l;
    private AnnotationWriter m;
    private AnnotationWriter n;
    private AnnotationWriter[] o;
    private AnnotationWriter[] p;
    private Attribute q;
    private ByteVector r = new ByteVector();
    private int s;
    private int t;
    private int u;
    private ByteVector v;
    private int w;
    private int[] x;
    private int[] z;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    MethodWriter(ClassWriter classWriter, int i2, String str, String str2, String str3, String[] strArr, boolean z2, boolean z3) {
        super(Opcodes.ASM4);
        int i3 = 0;
        if (classWriter.D == null) {
            classWriter.D = this;
        } else {
            classWriter.E.mv = this;
        }
        classWriter.E = this;
        this.b = classWriter;
        this.c = i2;
        if ("<init>".equals(str)) {
            this.c |= 524288;
        }
        this.d = classWriter.newUTF8(str);
        this.e = classWriter.newUTF8(str2);
        this.f = str2;
        this.g = str3;
        if (strArr != null && strArr.length > 0) {
            this.j = strArr.length;
            this.k = new int[this.j];
            for (int i4 = 0; i4 < this.j; i4++) {
                this.k[i4] = classWriter.newClass(strArr[i4]);
            }
        }
        this.M = !z3 ? z2 ? 1 : 2 : i3;
        if (z2 || z3) {
            int argumentsAndReturnSizes = Type.getArgumentsAndReturnSizes(this.f) >> 2;
            argumentsAndReturnSizes = (i2 & 8) != 0 ? argumentsAndReturnSizes - 1 : argumentsAndReturnSizes;
            this.t = argumentsAndReturnSizes;
            this.T = argumentsAndReturnSizes;
            this.N = new Label();
            this.N.a |= 8;
            visitLabel(this.N);
        }
    }

    private int a(int i2, int i3, int i4) {
        int i5 = i3 + 3 + i4;
        if (this.z == null || this.z.length < i5) {
            this.z = new int[i5];
        }
        this.z[0] = i2;
        this.z[1] = i3;
        this.z[2] = i4;
        return 3;
    }

    static int a(byte[] bArr, int i2) {
        return ((bArr[i2] & 255) << 24) | ((bArr[i2 + 1] & 255) << 16) | ((bArr[i2 + 2] & 255) << 8) | (bArr[i2 + 3] & 255);
    }

    static int a(int[] iArr, int[] iArr2, int i2, int i3) {
        int i4 = i3 - i2;
        for (int i5 = 0; i5 < iArr.length; i5++) {
            if (i2 < iArr[i5] && iArr[i5] <= i3) {
                i4 += iArr2[i5];
            } else if (i3 < iArr[i5] && iArr[i5] <= i2) {
                i4 -= iArr2[i5];
            }
        }
        return i4;
    }

    private void a(int i2, int i3) {
        while (i2 < i3) {
            int i4 = this.z[i2];
            int i5 = -268435456 & i4;
            if (i5 == 0) {
                int i6 = i4 & 1048575;
                switch (i4 & 267386880) {
                    case 24117248:
                        this.v.putByte(7).putShort(this.b.newClass(this.b.H[i6].g));
                        continue;
                    case 25165824:
                        this.v.putByte(8).putShort(this.b.H[i6].c);
                        continue;
                    default:
                        this.v.putByte(i6);
                        continue;
                }
            } else {
                StringBuffer stringBuffer = new StringBuffer();
                int i7 = i5 >> 28;
                while (true) {
                    int i8 = i7 - 1;
                    if (i7 > 0) {
                        stringBuffer.append('[');
                        i7 = i8;
                    } else {
                        if ((i4 & 267386880) != 24117248) {
                            switch (i4 & 15) {
                                case 1:
                                    stringBuffer.append('I');
                                    break;
                                case 2:
                                    stringBuffer.append('F');
                                    break;
                                case 3:
                                    stringBuffer.append('D');
                                    break;
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                default:
                                    stringBuffer.append('J');
                                    break;
                                case 9:
                                    stringBuffer.append('Z');
                                    break;
                                case 10:
                                    stringBuffer.append('B');
                                    break;
                                case 11:
                                    stringBuffer.append('C');
                                    break;
                                case 12:
                                    stringBuffer.append('S');
                                    break;
                            }
                        } else {
                            stringBuffer.append('L');
                            stringBuffer.append(this.b.H[i4 & 1048575].g);
                            stringBuffer.append(';');
                        }
                        this.v.putByte(7).putShort(this.b.newClass(stringBuffer.toString()));
                    }
                }
            }
            i2++;
        }
    }

    private void a(int i2, Label label) {
        Edge edge = new Edge();
        edge.a = i2;
        edge.b = label;
        edge.c = this.P.j;
        this.P.j = edge;
    }

    private void a(Object obj) {
        if (obj instanceof String) {
            this.v.putByte(7).putShort(this.b.newClass((String) obj));
        } else if (obj instanceof Integer) {
            this.v.putByte(((Integer) obj).intValue());
        } else {
            this.v.putByte(8).putShort(((Label) obj).c);
        }
    }

    private void a(Label label, Label[] labelArr) {
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(171, 0, (ClassWriter) null, (Item) null);
                a(0, label);
                label.a().a |= 16;
                for (int i2 = 0; i2 < labelArr.length; i2++) {
                    a(0, labelArr[i2]);
                    labelArr[i2].a().a |= 16;
                }
            } else {
                this.Q--;
                a(this.Q, label);
                for (Label a : labelArr) {
                    a(this.Q, a);
                }
            }
            e();
        }
    }

    static void a(byte[] bArr, int i2, int i3) {
        bArr[i2] = (byte) (i3 >>> 8);
        bArr[i2 + 1] = (byte) i3;
    }

    static void a(int[] iArr, int[] iArr2, Label label) {
        if ((label.a & 4) == 0) {
            label.c = a(iArr, iArr2, 0, label.c);
            label.a |= 4;
        }
    }

    static short b(byte[] bArr, int i2) {
        return (short) (((bArr[i2] & 255) << 8) | (bArr[i2 + 1] & 255));
    }

    private void b() {
        if (this.x != null) {
            if (this.v == null) {
                this.v = new ByteVector();
            }
            c();
            this.u++;
        }
        this.x = this.z;
        this.z = null;
    }

    private void b(Frame frame) {
        int[] iArr = frame.c;
        int[] iArr2 = frame.d;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i2 < iArr.length) {
            int i5 = iArr[i2];
            if (i5 == 16777216) {
                i4++;
            } else {
                i3 += i4 + 1;
                i4 = 0;
            }
            if (i5 == 16777220 || i5 == 16777219) {
                i2++;
            }
            i2++;
        }
        int i6 = 0;
        int i7 = 0;
        while (i6 < iArr2.length) {
            int i8 = iArr2[i6];
            i7++;
            if (i8 == 16777220 || i8 == 16777219) {
                i6++;
            }
            i6++;
        }
        int i9 = i3;
        int a = a(frame.b.c, i3, i7);
        int i10 = 0;
        while (i9 > 0) {
            int i11 = iArr[i10];
            int i12 = a + 1;
            this.z[a] = i11;
            if (i11 == 16777220 || i11 == 16777219) {
                i10++;
            }
            i10++;
            i9--;
            a = i12;
        }
        int i13 = a;
        int i14 = 0;
        while (i14 < iArr2.length) {
            int i15 = iArr2[i14];
            int i16 = i13 + 1;
            this.z[i13] = i15;
            if (i15 == 16777220 || i15 == 16777219) {
                i14++;
            }
            i14++;
            i13 = i16;
        }
        b();
    }

    static int c(byte[] bArr, int i2) {
        return ((bArr[i2] & 255) << 8) | (bArr[i2 + 1] & 255);
    }

    private void c() {
        int i2;
        char c2;
        int i3;
        char c3 = '@';
        int i4 = 0;
        int i5 = this.z[1];
        int i6 = this.z[2];
        if ((this.b.b & 65535) < 50) {
            this.v.putShort(this.z[0]).putShort(i5);
            a(3, i5 + 3);
            this.v.putShort(i6);
            a(i5 + 3, i5 + 3 + i6);
            return;
        }
        int i7 = this.x[1];
        int i8 = this.u == 0 ? this.z[0] : (this.z[0] - this.x[0]) - 1;
        if (i6 == 0) {
            i2 = i5 - i7;
            switch (i2) {
                case -3:
                case -2:
                case -1:
                    c2 = 248;
                    i7 = i5;
                    break;
                case 0:
                    if (i8 >= 64) {
                        c2 = 251;
                        break;
                    } else {
                        c2 = 0;
                        break;
                    }
                case 1:
                case 2:
                case 3:
                    c2 = 252;
                    break;
                default:
                    c2 = 255;
                    break;
            }
            i3 = i7;
        } else if (i5 == i7 && i6 == 1) {
            if (i8 >= 63) {
                c3 = 247;
            }
            i2 = 0;
            i3 = i7;
        } else {
            i2 = 0;
            c2 = 255;
            i3 = i7;
        }
        if (c2 != 255) {
            int i9 = 3;
            while (true) {
                if (i4 < i3) {
                    if (this.z[i9] != this.x[i9]) {
                        c2 = 255;
                    } else {
                        i9++;
                        i4++;
                    }
                }
            }
        }
        switch (c2) {
            case 0:
                this.v.putByte(i8);
                return;
            case '@':
                this.v.putByte(i8 + 64);
                a(i5 + 3, i5 + 4);
                return;
            case 247:
                this.v.putByte(247).putShort(i8);
                a(i5 + 3, i5 + 4);
                return;
            case 248:
                this.v.putByte(i2 + 251).putShort(i8);
                return;
            case 251:
                this.v.putByte(251).putShort(i8);
                return;
            case 252:
                this.v.putByte(i2 + 251).putShort(i8);
                a(i3 + 3, i5 + 3);
                return;
            default:
                this.v.putByte(255).putShort(i8).putShort(i5);
                a(3, i5 + 3);
                this.v.putShort(i6);
                a(i5 + 3, i5 + 3 + i6);
                return;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v49 */
    /* JADX WARN: Type inference failed for: r0v52, types: [int] */
    /* JADX WARN: Type inference failed for: r0v53, types: [int] */
    /* JADX WARN: Type inference failed for: r0v54, types: [int] */
    /* JADX WARN: Type inference failed for: r0v55, types: [int] */
    /* JADX WARN: Type inference failed for: r0v56, types: [int] */
    /* JADX WARN: Type inference failed for: r0v57, types: [int] */
    /* JADX WARN: Type inference failed for: r1v85, types: [int] */
    /* JADX WARN: Type inference failed for: r1v88, types: [int] */
    /* JADX WARN: Type inference failed for: r0v58, types: [int] */
    /* JADX WARN: Type inference failed for: r0v59, types: [int] */
    /* JADX WARN: Type inference failed for: r4v82, types: [int] */
    /* JADX WARN: Type inference failed for: r4v85, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d() {
        /*
            r13 = this;
            org.objectweb.asm.ByteVector r0 = r13.r
            byte[] r7 = r0.a
            r0 = 0
            int[] r3 = new int[r0]
            r0 = 0
            int[] r2 = new int[r0]
            org.objectweb.asm.ByteVector r0 = r13.r
            int r0 = r0.b
            boolean[] r8 = new boolean[r0]
            r0 = 3
        L_0x0011:
            r1 = 3
            if (r0 != r1) goto L_0x0015
            r0 = 2
        L_0x0015:
            r1 = 0
            r12 = r1
            r1 = r0
            r0 = r12
        L_0x0019:
            int r4 = r7.length
            if (r0 >= r4) goto L_0x010c
            byte r4 = r7[r0]
            r5 = r4 & 255(0xff, float:3.57E-43)
            r6 = 0
            byte[] r4 = org.objectweb.asm.ClassWriter.a
            byte r4 = r4[r5]
            switch(r4) {
                case 0: goto L_0x004e;
                case 1: goto L_0x0100;
                case 2: goto L_0x0104;
                case 3: goto L_0x0100;
                case 4: goto L_0x004e;
                case 5: goto L_0x0104;
                case 6: goto L_0x0104;
                case 7: goto L_0x0108;
                case 8: goto L_0x0108;
                case 9: goto L_0x0051;
                case 10: goto L_0x0092;
                case 11: goto L_0x0100;
                case 12: goto L_0x0104;
                case 13: goto L_0x0104;
                case 14: goto L_0x0095;
                case 15: goto L_0x00c6;
                case 16: goto L_0x0028;
                case 17: goto L_0x00ee;
                default: goto L_0x0028;
            }
        L_0x0028:
            int r0 = r0 + 4
        L_0x002a:
            if (r6 == 0) goto L_0x0019
            int r4 = r3.length
            int r4 = r4 + 1
            int[] r5 = new int[r4]
            int r4 = r2.length
            int r4 = r4 + 1
            int[] r4 = new int[r4]
            r9 = 0
            r10 = 0
            int r11 = r3.length
            java.lang.System.arraycopy(r3, r9, r5, r10, r11)
            r9 = 0
            r10 = 0
            int r11 = r2.length
            java.lang.System.arraycopy(r2, r9, r4, r10, r11)
            int r3 = r3.length
            r5[r3] = r0
            int r2 = r2.length
            r4[r2] = r6
            if (r6 <= 0) goto L_0x0379
            r1 = 3
            r2 = r4
            r3 = r5
            goto L_0x0019
        L_0x004e:
            int r0 = r0 + 1
            goto L_0x002a
        L_0x0051:
            r4 = 201(0xc9, float:2.82E-43)
            if (r5 <= r4) goto L_0x0088
            r4 = 218(0xda, float:3.05E-43)
            if (r5 >= r4) goto L_0x0085
            int r4 = r5 + -49
        L_0x005b:
            int r5 = r0 + 1
            int r5 = c(r7, r5)
            int r5 = r5 + r0
            r12 = r5
            r5 = r4
            r4 = r12
        L_0x0065:
            int r4 = a(r3, r2, r0, r4)
            r9 = -32768(0xffffffffffff8000, float:NaN)
            if (r4 < r9) goto L_0x0071
            r9 = 32767(0x7fff, float:4.5916E-41)
            if (r4 <= r9) goto L_0x037d
        L_0x0071:
            boolean r4 = r8[r0]
            if (r4 != 0) goto L_0x037d
            r4 = 167(0xa7, float:2.34E-43)
            if (r5 == r4) goto L_0x007d
            r4 = 168(0xa8, float:2.35E-43)
            if (r5 != r4) goto L_0x0090
        L_0x007d:
            r4 = 2
        L_0x007e:
            r5 = 1
            r8[r0] = r5
        L_0x0081:
            int r0 = r0 + 3
            r6 = r4
            goto L_0x002a
        L_0x0085:
            int r4 = r5 + -20
            goto L_0x005b
        L_0x0088:
            int r4 = r0 + 1
            short r4 = b(r7, r4)
            int r4 = r4 + r0
            goto L_0x0065
        L_0x0090:
            r4 = 5
            goto L_0x007e
        L_0x0092:
            int r0 = r0 + 5
            goto L_0x002a
        L_0x0095:
            r4 = 1
            if (r1 != r4) goto L_0x00bc
            r4 = 0
            int r4 = a(r3, r2, r4, r0)
            r4 = r4 & 3
            int r6 = -r4
        L_0x00a0:
            int r4 = r0 + 4
            r0 = r0 & 3
            int r0 = r4 - r0
            int r4 = r0 + 8
            int r4 = a(r7, r4)
            int r5 = r0 + 4
            int r5 = a(r7, r5)
            int r4 = r4 - r5
            int r4 = r4 + 1
            int r4 = r4 * 4
            int r4 = r4 + 12
            int r0 = r0 + r4
            goto L_0x002a
        L_0x00bc:
            boolean r4 = r8[r0]
            if (r4 != 0) goto L_0x00a0
            r6 = r0 & 3
            r4 = 1
            r8[r0] = r4
            goto L_0x00a0
        L_0x00c6:
            r4 = 1
            if (r1 != r4) goto L_0x00e4
            r4 = 0
            int r4 = a(r3, r2, r4, r0)
            r4 = r4 & 3
            int r6 = -r4
        L_0x00d1:
            int r4 = r0 + 4
            r0 = r0 & 3
            int r0 = r4 - r0
            int r4 = r0 + 4
            int r4 = a(r7, r4)
            int r4 = r4 * 8
            int r4 = r4 + 8
            int r0 = r0 + r4
            goto L_0x002a
        L_0x00e4:
            boolean r4 = r8[r0]
            if (r4 != 0) goto L_0x00d1
            r6 = r0 & 3
            r4 = 1
            r8[r0] = r4
            goto L_0x00d1
        L_0x00ee:
            int r4 = r0 + 1
            byte r4 = r7[r4]
            r4 = r4 & 255(0xff, float:3.57E-43)
            r5 = 132(0x84, float:1.85E-43)
            if (r4 != r5) goto L_0x00fc
            int r0 = r0 + 6
            goto L_0x002a
        L_0x00fc:
            int r0 = r0 + 4
            goto L_0x002a
        L_0x0100:
            int r0 = r0 + 2
            goto L_0x002a
        L_0x0104:
            int r0 = r0 + 3
            goto L_0x002a
        L_0x0108:
            int r0 = r0 + 5
            goto L_0x002a
        L_0x010c:
            r0 = 3
            if (r1 >= r0) goto L_0x0111
            int r1 = r1 + -1
        L_0x0111:
            if (r1 != 0) goto L_0x0376
            org.objectweb.asm.ByteVector r6 = new org.objectweb.asm.ByteVector
            org.objectweb.asm.ByteVector r0 = r13.r
            int r0 = r0.b
            r6.<init>(r0)
            r0 = 0
        L_0x011d:
            org.objectweb.asm.ByteVector r1 = r13.r
            int r1 = r1.b
            if (r0 >= r1) goto L_0x0291
            byte r1 = r7[r0]
            r4 = r1 & 255(0xff, float:3.57E-43)
            byte[] r1 = org.objectweb.asm.ClassWriter.a
            byte r1 = r1[r4]
            switch(r1) {
                case 0: goto L_0x0135;
                case 1: goto L_0x0279;
                case 2: goto L_0x0281;
                case 3: goto L_0x0279;
                case 4: goto L_0x0135;
                case 5: goto L_0x0281;
                case 6: goto L_0x0281;
                case 7: goto L_0x0289;
                case 8: goto L_0x0289;
                case 9: goto L_0x013b;
                case 10: goto L_0x01a1;
                case 11: goto L_0x0279;
                case 12: goto L_0x0281;
                case 13: goto L_0x0281;
                case 14: goto L_0x01b6;
                case 15: goto L_0x020f;
                case 16: goto L_0x012e;
                case 17: goto L_0x025f;
                default: goto L_0x012e;
            }
        L_0x012e:
            r1 = 4
            r6.putByteArray(r7, r0, r1)
            int r0 = r0 + 4
            goto L_0x011d
        L_0x0135:
            r6.putByte(r4)
            int r0 = r0 + 1
            goto L_0x011d
        L_0x013b:
            r1 = 201(0xc9, float:2.82E-43)
            if (r4 <= r1) goto L_0x016a
            r1 = 218(0xda, float:3.05E-43)
            if (r4 >= r1) goto L_0x0167
            int r1 = r4 + -49
        L_0x0145:
            int r4 = r0 + 1
            int r4 = c(r7, r4)
            int r4 = r4 + r0
            r12 = r4
            r4 = r1
            r1 = r12
        L_0x014f:
            int r5 = a(r3, r2, r0, r1)
            boolean r1 = r8[r0]
            if (r1 == 0) goto L_0x019a
            r1 = 167(0xa7, float:2.34E-43)
            if (r4 != r1) goto L_0x0172
            r1 = 200(0xc8, float:2.8E-43)
            r6.putByte(r1)
            r1 = r5
        L_0x0161:
            r6.putInt(r1)
        L_0x0164:
            int r0 = r0 + 3
            goto L_0x011d
        L_0x0167:
            int r1 = r4 + -20
            goto L_0x0145
        L_0x016a:
            int r1 = r0 + 1
            short r1 = b(r7, r1)
            int r1 = r1 + r0
            goto L_0x014f
        L_0x0172:
            r1 = 168(0xa8, float:2.35E-43)
            if (r4 != r1) goto L_0x017d
            r1 = 201(0xc9, float:2.82E-43)
            r6.putByte(r1)
            r1 = r5
            goto L_0x0161
        L_0x017d:
            r1 = 166(0xa6, float:2.33E-43)
            if (r4 > r1) goto L_0x0197
            int r1 = r4 + 1
            r1 = r1 ^ 1
            int r1 = r1 + -1
        L_0x0187:
            r6.putByte(r1)
            r1 = 8
            r6.putShort(r1)
            r1 = 200(0xc8, float:2.8E-43)
            r6.putByte(r1)
            int r1 = r5 + -3
            goto L_0x0161
        L_0x0197:
            r1 = r4 ^ 1
            goto L_0x0187
        L_0x019a:
            r6.putByte(r4)
            r6.putShort(r5)
            goto L_0x0164
        L_0x01a1:
            int r1 = r0 + 1
            int r1 = a(r7, r1)
            int r1 = r1 + r0
            int r1 = a(r3, r2, r0, r1)
            r6.putByte(r4)
            r6.putInt(r1)
            int r0 = r0 + 5
            goto L_0x011d
        L_0x01b6:
            int r1 = r0 + 4
            r4 = r0 & 3
            int r1 = r1 - r4
            r4 = 170(0xaa, float:2.38E-43)
            r6.putByte(r4)
            r4 = 0
            r5 = 0
            int r9 = r6.b
            int r9 = r9 % 4
            int r9 = 4 - r9
            int r9 = r9 % 4
            r6.putByteArray(r4, r5, r9)
            int r4 = a(r7, r1)
            int r4 = r4 + r0
            int r1 = r1 + 4
            int r4 = a(r3, r2, r0, r4)
            r6.putInt(r4)
            int r4 = a(r7, r1)
            int r5 = r1 + 4
            r6.putInt(r4)
            int r1 = a(r7, r5)
            int r1 = r1 - r4
            int r1 = r1 + 1
            int r4 = r5 + 4
            int r5 = r4 + -4
            int r5 = a(r7, r5)
            r6.putInt(r5)
            r12 = r1
            r1 = r4
            r4 = r12
        L_0x01f9:
            if (r4 <= 0) goto L_0x0373
            int r5 = a(r7, r1)
            int r9 = r0 + r5
            int r5 = r1 + 4
            int r1 = a(r3, r2, r0, r9)
            r6.putInt(r1)
            int r1 = r4 + -1
            r4 = r1
            r1 = r5
            goto L_0x01f9
        L_0x020f:
            int r1 = r0 + 4
            r4 = r0 & 3
            int r1 = r1 - r4
            r4 = 171(0xab, float:2.4E-43)
            r6.putByte(r4)
            r4 = 0
            r5 = 0
            int r9 = r6.b
            int r9 = r9 % 4
            int r9 = 4 - r9
            int r9 = r9 % 4
            r6.putByteArray(r4, r5, r9)
            int r4 = a(r7, r1)
            int r4 = r4 + r0
            int r5 = r1 + 4
            int r1 = a(r3, r2, r0, r4)
            r6.putInt(r1)
            int r1 = a(r7, r5)
            int r4 = r5 + 4
            r6.putInt(r1)
            r12 = r1
            r1 = r4
            r4 = r12
        L_0x0240:
            if (r4 <= 0) goto L_0x0373
            int r5 = a(r7, r1)
            r6.putInt(r5)
            int r1 = r1 + 4
            int r5 = a(r7, r1)
            int r9 = r0 + r5
            int r5 = r1 + 4
            int r1 = a(r3, r2, r0, r9)
            r6.putInt(r1)
            int r1 = r4 + -1
            r4 = r1
            r1 = r5
            goto L_0x0240
        L_0x025f:
            int r1 = r0 + 1
            byte r1 = r7[r1]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r4 = 132(0x84, float:1.85E-43)
            if (r1 != r4) goto L_0x0271
            r1 = 6
            r6.putByteArray(r7, r0, r1)
            int r0 = r0 + 6
            goto L_0x011d
        L_0x0271:
            r1 = 4
            r6.putByteArray(r7, r0, r1)
            int r0 = r0 + 4
            goto L_0x011d
        L_0x0279:
            r1 = 2
            r6.putByteArray(r7, r0, r1)
            int r0 = r0 + 2
            goto L_0x011d
        L_0x0281:
            r1 = 3
            r6.putByteArray(r7, r0, r1)
            int r0 = r0 + 3
            goto L_0x011d
        L_0x0289:
            r1 = 5
            r6.putByteArray(r7, r0, r1)
            int r0 = r0 + 5
            goto L_0x011d
        L_0x0291:
            int r0 = r13.u
            if (r0 <= 0) goto L_0x02e4
            int r0 = r13.M
            if (r0 != 0) goto L_0x02df
            r0 = 0
            r13.u = r0
            r0 = 0
            r13.v = r0
            r0 = 0
            r13.x = r0
            r0 = 0
            r13.z = r0
            org.objectweb.asm.Frame r0 = new org.objectweb.asm.Frame
            r0.<init>()
            org.objectweb.asm.Label r1 = r13.N
            r0.b = r1
            java.lang.String r1 = r13.f
            org.objectweb.asm.Type[] r1 = org.objectweb.asm.Type.getArgumentTypes(r1)
            org.objectweb.asm.ClassWriter r4 = r13.b
            int r5 = r13.c
            int r7 = r13.t
            r0.a(r4, r5, r1, r7)
            r13.b(r0)
            org.objectweb.asm.Label r0 = r13.N
        L_0x02c2:
            if (r0 == 0) goto L_0x02e4
            int r1 = r0.c
            int r1 = r1 + -3
            int r4 = r0.a
            r4 = r4 & 32
            if (r4 != 0) goto L_0x02d4
            if (r1 < 0) goto L_0x02dc
            boolean r1 = r8[r1]
            if (r1 == 0) goto L_0x02dc
        L_0x02d4:
            a(r3, r2, r0)
            org.objectweb.asm.Frame r1 = r0.h
            r13.b(r1)
        L_0x02dc:
            org.objectweb.asm.Label r0 = r0.i
            goto L_0x02c2
        L_0x02df:
            org.objectweb.asm.ClassWriter r0 = r13.b
            r1 = 1
            r0.L = r1
        L_0x02e4:
            org.objectweb.asm.Handler r0 = r13.B
        L_0x02e6:
            if (r0 == 0) goto L_0x02fa
            org.objectweb.asm.Label r1 = r0.a
            a(r3, r2, r1)
            org.objectweb.asm.Label r1 = r0.b
            a(r3, r2, r1)
            org.objectweb.asm.Label r1 = r0.c
            a(r3, r2, r1)
            org.objectweb.asm.Handler r0 = r0.f
            goto L_0x02e6
        L_0x02fa:
            r0 = 0
            r4 = r0
        L_0x02fc:
            r0 = 2
            if (r4 >= r0) goto L_0x0336
            if (r4 != 0) goto L_0x032e
            org.objectweb.asm.ByteVector r0 = r13.E
            r1 = r0
        L_0x0304:
            if (r1 == 0) goto L_0x0332
            byte[] r5 = r1.a
            r0 = 0
        L_0x0309:
            int r7 = r1.b
            if (r0 >= r7) goto L_0x0332
            int r7 = c(r5, r0)
            r8 = 0
            int r8 = a(r3, r2, r8, r7)
            a(r5, r0, r8)
            int r9 = r0 + 2
            int r9 = c(r5, r9)
            int r7 = r7 + r9
            r9 = 0
            int r7 = a(r3, r2, r9, r7)
            int r7 = r7 - r8
            int r8 = r0 + 2
            a(r5, r8, r7)
            int r0 = r0 + 10
            goto L_0x0309
        L_0x032e:
            org.objectweb.asm.ByteVector r0 = r13.G
            r1 = r0
            goto L_0x0304
        L_0x0332:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x02fc
        L_0x0336:
            org.objectweb.asm.ByteVector r0 = r13.I
            if (r0 == 0) goto L_0x0354
            org.objectweb.asm.ByteVector r0 = r13.I
            byte[] r1 = r0.a
            r0 = 0
        L_0x033f:
            org.objectweb.asm.ByteVector r4 = r13.I
            int r4 = r4.b
            if (r0 >= r4) goto L_0x0354
            r4 = 0
            int r5 = c(r1, r0)
            int r4 = a(r3, r2, r4, r5)
            a(r1, r0, r4)
            int r0 = r0 + 4
            goto L_0x033f
        L_0x0354:
            org.objectweb.asm.Attribute r0 = r13.J
            r1 = r0
        L_0x0357:
            if (r1 == 0) goto L_0x0370
            org.objectweb.asm.Label[] r4 = r1.getLabels()
            if (r4 == 0) goto L_0x036c
            int r0 = r4.length
            int r0 = r0 + -1
        L_0x0362:
            if (r0 < 0) goto L_0x036c
            r5 = r4[r0]
            a(r3, r2, r5)
            int r0 = r0 + -1
            goto L_0x0362
        L_0x036c:
            org.objectweb.asm.Attribute r0 = r1.a
            r1 = r0
            goto L_0x0357
        L_0x0370:
            r13.r = r6
            return
        L_0x0373:
            r0 = r1
            goto L_0x011d
        L_0x0376:
            r0 = r1
            goto L_0x0011
        L_0x0379:
            r2 = r4
            r3 = r5
            goto L_0x0019
        L_0x037d:
            r4 = r6
            goto L_0x0081
        */
        throw new UnsupportedOperationException("Method not decompiled: org.objectweb.asm.MethodWriter.d():void");
    }

    private void e() {
        if (this.M == 0) {
            Label label = new Label();
            label.h = new Frame();
            label.h.b = label;
            label.a(this, this.r.b, this.r.a);
            this.O.i = label;
            this.O = label;
        } else {
            this.P.g = this.R;
        }
        this.P = null;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private void f() {
        int i2;
        int a = a(0, this.f.length() + 1, 0);
        if ((this.c & 8) != 0) {
            i2 = a;
        } else if ((this.c & 524288) == 0) {
            i2 = a + 1;
            this.z[a] = this.b.c(this.b.I) | 24117248;
        } else {
            i2 = a + 1;
            this.z[a] = 6;
        }
        int i3 = i2;
        int i4 = 1;
        while (true) {
            int i5 = i4 + 1;
            switch (this.f.charAt(i4)) {
                case 'B':
                case 'C':
                case 'I':
                case 'S':
                case 'Z':
                    this.z[i3] = 1;
                    i3++;
                    i4 = i5;
                    break;
                case 'D':
                    this.z[i3] = 3;
                    i3++;
                    i4 = i5;
                    break;
                case 'F':
                    this.z[i3] = 2;
                    i3++;
                    i4 = i5;
                    break;
                case 'J':
                    this.z[i3] = 4;
                    i3++;
                    i4 = i5;
                    break;
                case 'L':
                    while (this.f.charAt(i5) != ';') {
                        i5++;
                    }
                    int i6 = i4 + 1;
                    i4 = i5 + 1;
                    this.z[i3] = this.b.c(this.f.substring(i6, i5)) | 24117248;
                    i3++;
                    break;
                case '[':
                    while (this.f.charAt(i5) == '[') {
                        i5++;
                    }
                    if (this.f.charAt(i5) == 'L') {
                        while (true) {
                            i5++;
                            if (this.f.charAt(i5) != ';') {
                            }
                        }
                    }
                    int i7 = i5 + 1;
                    this.z[i3] = this.b.c(this.f.substring(i4, i7)) | 24117248;
                    i4 = i7;
                    i3++;
                    break;
                default:
                    this.z[1] = i3 - 3;
                    b();
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final int a() {
        int i2;
        if (this.h != 0) {
            return this.i + 6;
        }
        if (this.K) {
            d();
        }
        int i3 = 8;
        if (this.r.b > 0) {
            if (this.r.b > 65536) {
                throw new RuntimeException("Method code too large!");
            }
            this.b.newUTF8("Code");
            int i4 = this.r.b + 18 + (this.A * 8) + 8;
            if (this.E != null) {
                this.b.newUTF8("LocalVariableTable");
                i4 += this.E.b + 8;
            }
            if (this.G != null) {
                this.b.newUTF8("LocalVariableTypeTable");
                i4 += this.G.b + 8;
            }
            if (this.I != null) {
                this.b.newUTF8("LineNumberTable");
                i4 += this.I.b + 8;
            }
            if (this.v != null) {
                this.b.newUTF8((this.b.b & 65535) >= 50 ? "StackMapTable" : "StackMap");
                i3 = i4 + this.v.b + 8;
            } else {
                i3 = i4;
            }
            if (this.J != null) {
                i3 += this.J.a(this.b, this.r.a, this.r.b, this.s, this.t);
            }
        }
        if (this.j > 0) {
            this.b.newUTF8("Exceptions");
            i3 += (this.j * 2) + 8;
        }
        if ((this.c & 4096) != 0 && ((this.b.b & 65535) < 49 || (this.c & Opcodes.ASM4) != 0)) {
            this.b.newUTF8("Synthetic");
            i3 += 6;
        }
        if ((this.c & Opcodes.ACC_DEPRECATED) != 0) {
            this.b.newUTF8("Deprecated");
            i3 += 6;
        }
        if (this.g != null) {
            this.b.newUTF8("Signature");
            this.b.newUTF8(this.g);
            i3 += 8;
        }
        if (this.l != null) {
            this.b.newUTF8("AnnotationDefault");
            i3 += this.l.b + 6;
        }
        if (this.m != null) {
            this.b.newUTF8("RuntimeVisibleAnnotations");
            i3 += this.m.a() + 8;
        }
        if (this.n != null) {
            this.b.newUTF8("RuntimeInvisibleAnnotations");
            i3 += this.n.a() + 8;
        }
        if (this.o != null) {
            this.b.newUTF8("RuntimeVisibleParameterAnnotations");
            i2 = i3 + ((this.o.length - this.S) * 2) + 7;
            int length = this.o.length;
            while (true) {
                length--;
                if (length < this.S) {
                    break;
                }
                i2 += this.o[length] == null ? 0 : this.o[length].a();
            }
        } else {
            i2 = i3;
        }
        if (this.p != null) {
            this.b.newUTF8("RuntimeInvisibleParameterAnnotations");
            int length2 = i2 + ((this.p.length - this.S) * 2) + 7;
            int length3 = this.p.length;
            while (true) {
                length3--;
                if (length3 < this.S) {
                    break;
                }
                length2 = i2 + (this.p[length3] == null ? 0 : this.p[length3].a());
            }
        }
        int i5 = i2;
        return this.q != null ? i5 + this.q.a(this.b, null, 0, -1, -1) : i5;
    }

    /* access modifiers changed from: package-private */
    public final void a(ByteVector byteVector) {
        boolean z2 = true;
        byteVector.putShort(((917504 | ((this.c & Opcodes.ASM4) / 64)) ^ -1) & this.c).putShort(this.d).putShort(this.e);
        if (this.h != 0) {
            byteVector.putByteArray(this.b.M.b, this.h, this.i);
            return;
        }
        int i2 = this.r.b > 0 ? 1 : 0;
        if (this.j > 0) {
            i2++;
        }
        if ((this.c & 4096) != 0 && ((this.b.b & 65535) < 49 || (this.c & Opcodes.ASM4) != 0)) {
            i2++;
        }
        if ((this.c & Opcodes.ACC_DEPRECATED) != 0) {
            i2++;
        }
        if (this.g != null) {
            i2++;
        }
        if (this.l != null) {
            i2++;
        }
        if (this.m != null) {
            i2++;
        }
        if (this.n != null) {
            i2++;
        }
        if (this.o != null) {
            i2++;
        }
        if (this.p != null) {
            i2++;
        }
        if (this.q != null) {
            i2 += this.q.a();
        }
        byteVector.putShort(i2);
        if (this.r.b > 0) {
            int i3 = this.r.b + 12 + (this.A * 8);
            if (this.E != null) {
                i3 += this.E.b + 8;
            }
            if (this.G != null) {
                i3 += this.G.b + 8;
            }
            if (this.I != null) {
                i3 += this.I.b + 8;
            }
            int i4 = this.v != null ? i3 + this.v.b + 8 : i3;
            if (this.J != null) {
                i4 += this.J.a(this.b, this.r.a, this.r.b, this.s, this.t);
            }
            byteVector.putShort(this.b.newUTF8("Code")).putInt(i4);
            byteVector.putShort(this.s).putShort(this.t);
            byteVector.putInt(this.r.b).putByteArray(this.r.a, 0, this.r.b);
            byteVector.putShort(this.A);
            if (this.A > 0) {
                for (Handler handler = this.B; handler != null; handler = handler.f) {
                    byteVector.putShort(handler.a.c).putShort(handler.b.c).putShort(handler.c.c).putShort(handler.e);
                }
            }
            int i5 = this.E != null ? 1 : 0;
            if (this.G != null) {
                i5++;
            }
            if (this.I != null) {
                i5++;
            }
            if (this.v != null) {
                i5++;
            }
            if (this.J != null) {
                i5 += this.J.a();
            }
            byteVector.putShort(i5);
            if (this.E != null) {
                byteVector.putShort(this.b.newUTF8("LocalVariableTable"));
                byteVector.putInt(this.E.b + 2).putShort(this.D);
                byteVector.putByteArray(this.E.a, 0, this.E.b);
            }
            if (this.G != null) {
                byteVector.putShort(this.b.newUTF8("LocalVariableTypeTable"));
                byteVector.putInt(this.G.b + 2).putShort(this.F);
                byteVector.putByteArray(this.G.a, 0, this.G.b);
            }
            if (this.I != null) {
                byteVector.putShort(this.b.newUTF8("LineNumberTable"));
                byteVector.putInt(this.I.b + 2).putShort(this.H);
                byteVector.putByteArray(this.I.a, 0, this.I.b);
            }
            if (this.v != null) {
                if ((this.b.b & 65535) < 50) {
                    z2 = false;
                }
                byteVector.putShort(this.b.newUTF8(z2 ? "StackMapTable" : "StackMap"));
                byteVector.putInt(this.v.b + 2).putShort(this.u);
                byteVector.putByteArray(this.v.a, 0, this.v.b);
            }
            if (this.J != null) {
                this.J.a(this.b, this.r.a, this.r.b, this.t, this.s, byteVector);
            }
        }
        if (this.j > 0) {
            byteVector.putShort(this.b.newUTF8("Exceptions")).putInt((this.j * 2) + 2);
            byteVector.putShort(this.j);
            for (int i6 = 0; i6 < this.j; i6++) {
                byteVector.putShort(this.k[i6]);
            }
        }
        if ((this.c & 4096) != 0 && ((this.b.b & 65535) < 49 || (this.c & Opcodes.ASM4) != 0)) {
            byteVector.putShort(this.b.newUTF8("Synthetic")).putInt(0);
        }
        if ((this.c & Opcodes.ACC_DEPRECATED) != 0) {
            byteVector.putShort(this.b.newUTF8("Deprecated")).putInt(0);
        }
        if (this.g != null) {
            byteVector.putShort(this.b.newUTF8("Signature")).putInt(2).putShort(this.b.newUTF8(this.g));
        }
        if (this.l != null) {
            byteVector.putShort(this.b.newUTF8("AnnotationDefault"));
            byteVector.putInt(this.l.b);
            byteVector.putByteArray(this.l.a, 0, this.l.b);
        }
        if (this.m != null) {
            byteVector.putShort(this.b.newUTF8("RuntimeVisibleAnnotations"));
            this.m.a(byteVector);
        }
        if (this.n != null) {
            byteVector.putShort(this.b.newUTF8("RuntimeInvisibleAnnotations"));
            this.n.a(byteVector);
        }
        if (this.o != null) {
            byteVector.putShort(this.b.newUTF8("RuntimeVisibleParameterAnnotations"));
            AnnotationWriter.a(this.o, this.S, byteVector);
        }
        if (this.p != null) {
            byteVector.putShort(this.b.newUTF8("RuntimeInvisibleParameterAnnotations"));
            AnnotationWriter.a(this.p, this.S, byteVector);
        }
        if (this.q != null) {
            this.q.a(this.b, null, 0, -1, -1, byteVector);
        }
    }

    public AnnotationVisitor visitAnnotation(String str, boolean z2) {
        ByteVector byteVector = new ByteVector();
        byteVector.putShort(this.b.newUTF8(str)).putShort(0);
        AnnotationWriter annotationWriter = new AnnotationWriter(this.b, true, byteVector, byteVector, 2);
        if (z2) {
            annotationWriter.g = this.m;
            this.m = annotationWriter;
        } else {
            annotationWriter.g = this.n;
            this.n = annotationWriter;
        }
        return annotationWriter;
    }

    public AnnotationVisitor visitAnnotationDefault() {
        this.l = new ByteVector();
        return new AnnotationWriter(this.b, false, this.l, null, 0);
    }

    public void visitAttribute(Attribute attribute) {
        if (attribute.isCodeAttribute()) {
            attribute.a = this.J;
            this.J = attribute;
            return;
        }
        attribute.a = this.q;
        this.q = attribute;
    }

    public void visitCode() {
    }

    public void visitEnd() {
    }

    public void visitFieldInsn(int i2, String str, String str2, String str3) {
        int i3;
        int i4 = 1;
        int i5 = -2;
        Item a = this.b.a(str, str2, str3);
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(i2, 0, this.b, a);
            } else {
                char charAt = str3.charAt(0);
                switch (i2) {
                    case 178:
                        int i6 = this.Q;
                        if (charAt == 'D' || charAt == 'J') {
                            i4 = 2;
                        }
                        i3 = i4 + i6;
                        break;
                    case 179:
                        i3 = ((charAt == 'D' || charAt == 'J') ? -2 : -1) + this.Q;
                        break;
                    case 180:
                        int i7 = this.Q;
                        if (!(charAt == 'D' || charAt == 'J')) {
                            i4 = 0;
                        }
                        i3 = i4 + i7;
                        break;
                    default:
                        int i8 = this.Q;
                        if (charAt == 'D' || charAt == 'J') {
                            i5 = -3;
                        }
                        i3 = i8 + i5;
                        break;
                }
                if (i3 > this.R) {
                    this.R = i3;
                }
                this.Q = i3;
            }
        }
        this.r.b(i2, a.a);
    }

    public void visitFrame(int i2, int i3, Object[] objArr, int i4, Object[] objArr2) {
        int i5;
        int i6;
        int i7;
        int i8 = 0;
        if (this.M != 0) {
            if (i2 == -1) {
                if (this.x == null) {
                    f();
                }
                this.T = i3;
                int a = a(this.r.b, i3, i4);
                int i9 = 0;
                while (i9 < i3) {
                    if (objArr[i9] instanceof String) {
                        i7 = a + 1;
                        this.z[a] = this.b.c((String) objArr[i9]) | 24117248;
                    } else if (objArr[i9] instanceof Integer) {
                        i7 = a + 1;
                        this.z[a] = ((Integer) objArr[i9]).intValue();
                    } else {
                        i7 = a + 1;
                        this.z[a] = this.b.a("", ((Label) objArr[i9]).c) | 25165824;
                    }
                    i9++;
                    a = i7;
                }
                while (i8 < i4) {
                    if (objArr2[i8] instanceof String) {
                        i6 = a + 1;
                        this.z[a] = this.b.c((String) objArr2[i8]) | 24117248;
                    } else if (objArr2[i8] instanceof Integer) {
                        i6 = a + 1;
                        this.z[a] = ((Integer) objArr2[i8]).intValue();
                    } else {
                        i6 = a + 1;
                        this.z[a] = this.b.a("", ((Label) objArr2[i8]).c) | 25165824;
                    }
                    i8++;
                    a = i6;
                }
                b();
            } else {
                if (this.v == null) {
                    this.v = new ByteVector();
                    i5 = this.r.b;
                } else {
                    i5 = (this.r.b - this.w) - 1;
                    if (i5 < 0) {
                        if (i2 != 3) {
                            throw new IllegalStateException();
                        }
                        return;
                    }
                }
                switch (i2) {
                    case 0:
                        this.T = i3;
                        this.v.putByte(255).putShort(i5).putShort(i3);
                        for (int i10 = 0; i10 < i3; i10++) {
                            a(objArr[i10]);
                        }
                        this.v.putShort(i4);
                        while (i8 < i4) {
                            a(objArr2[i8]);
                            i8++;
                        }
                        break;
                    case 1:
                        this.T += i3;
                        this.v.putByte(i3 + 251).putShort(i5);
                        for (int i11 = 0; i11 < i3; i11++) {
                            a(objArr[i11]);
                        }
                        break;
                    case 2:
                        this.T -= i3;
                        this.v.putByte(251 - i3).putShort(i5);
                        break;
                    case 3:
                        if (i5 >= 64) {
                            this.v.putByte(251).putShort(i5);
                            break;
                        } else {
                            this.v.putByte(i5);
                            break;
                        }
                    case 4:
                        if (i5 < 64) {
                            this.v.putByte(i5 + 64);
                        } else {
                            this.v.putByte(247).putShort(i5);
                        }
                        a(objArr2[0]);
                        break;
                }
                this.w = this.r.b;
                this.u++;
            }
            this.s = Math.max(this.s, i4);
            this.t = Math.max(this.t, this.T);
        }
    }

    public void visitIincInsn(int i2, int i3) {
        int i4;
        if (this.P != null && this.M == 0) {
            this.P.h.a(132, i2, (ClassWriter) null, (Item) null);
        }
        if (this.M != 2 && (i4 = i2 + 1) > this.t) {
            this.t = i4;
        }
        if (i2 > 255 || i3 > 127 || i3 < -128) {
            this.r.putByte(PAK_ASSETS.IMG_DZ_PINGGUOZHADAN).b(132, i2).putShort(i3);
        } else {
            this.r.putByte(132).a(i2, i3);
        }
    }

    public void visitInsn(int i2) {
        this.r.putByte(i2);
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(i2, 0, (ClassWriter) null, (Item) null);
            } else {
                int i3 = this.Q + Frame.a[i2];
                if (i3 > this.R) {
                    this.R = i3;
                }
                this.Q = i3;
            }
            if ((i2 >= 172 && i2 <= 177) || i2 == 191) {
                e();
            }
        }
    }

    public void visitIntInsn(int i2, int i3) {
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(i2, i3, (ClassWriter) null, (Item) null);
            } else if (i2 != 188) {
                int i4 = this.Q + 1;
                if (i4 > this.R) {
                    this.R = i4;
                }
                this.Q = i4;
            }
        }
        if (i2 == 17) {
            this.r.b(i2, i3);
        } else {
            this.r.a(i2, i3);
        }
    }

    public void visitInvokeDynamicInsn(String str, String str2, Handle handle, Object... objArr) {
        Item a = this.b.a(str, str2, handle, objArr);
        int i2 = a.c;
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(186, 0, this.b, a);
            } else {
                if (i2 == 0) {
                    i2 = Type.getArgumentsAndReturnSizes(str2);
                    a.c = i2;
                }
                int i3 = (i2 & 3) + (this.Q - (i2 >> 2)) + 1;
                if (i3 > this.R) {
                    this.R = i3;
                }
                this.Q = i3;
            }
        }
        this.r.b(186, a.a);
        this.r.putShort(0);
    }

    public void visitJumpInsn(int i2, Label label) {
        Label label2 = null;
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(i2, 0, (ClassWriter) null, (Item) null);
                label.a().a |= 16;
                a(0, label);
                if (i2 != 167) {
                    label2 = new Label();
                }
            } else if (i2 == 168) {
                if ((label.a & 512) == 0) {
                    label.a |= 512;
                    this.L++;
                }
                this.P.a |= 128;
                a(this.Q + 1, label);
                label2 = new Label();
            } else {
                this.Q += Frame.a[i2];
                a(this.Q, label);
            }
        }
        if ((label.a & 2) == 0 || label.c - this.r.b >= -32768) {
            this.r.putByte(i2);
            label.a(this, this.r, this.r.b - 1, false);
        } else {
            if (i2 == 167) {
                this.r.putByte(200);
            } else if (i2 == 168) {
                this.r.putByte(201);
            } else {
                if (label2 != null) {
                    label2.a |= 16;
                }
                this.r.putByte(i2 <= 166 ? ((i2 + 1) ^ 1) - 1 : i2 ^ 1);
                this.r.putShort(8);
                this.r.putByte(200);
            }
            label.a(this, this.r, this.r.b - 1, true);
        }
        if (this.P != null) {
            if (label2 != null) {
                visitLabel(label2);
            }
            if (i2 == 167) {
                e();
            }
        }
    }

    public void visitLabel(Label label) {
        this.K |= label.a(this, this.r.b, this.r.a);
        if ((label.a & 1) == 0) {
            if (this.M == 0) {
                if (this.P != null) {
                    if (label.c == this.P.c) {
                        this.P.a |= label.a & 16;
                        label.h = this.P.h;
                        return;
                    }
                    a(0, label);
                }
                this.P = label;
                if (label.h == null) {
                    label.h = new Frame();
                    label.h.b = label;
                }
                if (this.O != null) {
                    if (label.c == this.O.c) {
                        this.O.a |= label.a & 16;
                        label.h = this.O.h;
                        this.P = this.O;
                        return;
                    }
                    this.O.i = label;
                }
                this.O = label;
            } else if (this.M == 1) {
                if (this.P != null) {
                    this.P.g = this.R;
                    a(this.Q, label);
                }
                this.P = label;
                this.Q = 0;
                this.R = 0;
                if (this.O != null) {
                    this.O.i = label;
                }
                this.O = label;
            }
        }
    }

    public void visitLdcInsn(Object obj) {
        Item a = this.b.a(obj);
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(18, 0, this.b, a);
            } else {
                int i2 = (a.b == 5 || a.b == 6) ? this.Q + 2 : this.Q + 1;
                if (i2 > this.R) {
                    this.R = i2;
                }
                this.Q = i2;
            }
        }
        int i3 = a.a;
        if (a.b == 5 || a.b == 6) {
            this.r.b(20, i3);
        } else if (i3 >= 256) {
            this.r.b(19, i3);
        } else {
            this.r.a(18, i3);
        }
    }

    public void visitLineNumber(int i2, Label label) {
        if (this.I == null) {
            this.I = new ByteVector();
        }
        this.H++;
        this.I.putShort(label.c);
        this.I.putShort(i2);
    }

    public void visitLocalVariable(String str, String str2, String str3, Label label, Label label2, int i2) {
        int i3 = 2;
        if (str3 != null) {
            if (this.G == null) {
                this.G = new ByteVector();
            }
            this.F++;
            this.G.putShort(label.c).putShort(label2.c - label.c).putShort(this.b.newUTF8(str)).putShort(this.b.newUTF8(str3)).putShort(i2);
        }
        if (this.E == null) {
            this.E = new ByteVector();
        }
        this.D++;
        this.E.putShort(label.c).putShort(label2.c - label.c).putShort(this.b.newUTF8(str)).putShort(this.b.newUTF8(str2)).putShort(i2);
        if (this.M != 2) {
            char charAt = str2.charAt(0);
            if (!(charAt == 'J' || charAt == 'D')) {
                i3 = 1;
            }
            int i4 = i3 + i2;
            if (i4 > this.t) {
                this.t = i4;
            }
        }
    }

    public void visitLookupSwitchInsn(Label label, int[] iArr, Label[] labelArr) {
        int i2 = this.r.b;
        this.r.putByte(171);
        this.r.putByteArray(null, 0, (4 - (this.r.b % 4)) % 4);
        label.a(this, this.r, i2, true);
        this.r.putInt(labelArr.length);
        for (int i3 = 0; i3 < labelArr.length; i3++) {
            this.r.putInt(iArr[i3]);
            labelArr[i3].a(this, this.r, i2, true);
        }
        a(label, labelArr);
    }

    public void visitMaxs(int i2, int i3) {
        Label label;
        if (this.M == 0) {
            for (Handler handler = this.B; handler != null; handler = handler.f) {
                Label a = handler.a.a();
                Label a2 = handler.c.a();
                Label a3 = handler.b.a();
                int c2 = 24117248 | this.b.c(handler.d == null ? "java/lang/Throwable" : handler.d);
                a2.a |= 16;
                for (Label label2 = a; label2 != a3; label2 = label2.i) {
                    Edge edge = new Edge();
                    edge.a = c2;
                    edge.b = a2;
                    edge.c = label2.j;
                    label2.j = edge;
                }
            }
            Frame frame = this.N.h;
            frame.a(this.b, this.c, Type.getArgumentTypes(this.f), this.t);
            b(frame);
            Label label3 = this.N;
            int i4 = 0;
            while (label3 != null) {
                Label label4 = label3.k;
                label3.k = null;
                Frame frame2 = label3.h;
                if ((label3.a & 16) != 0) {
                    label3.a |= 32;
                }
                label3.a |= 64;
                int length = frame2.d.length + label3.g;
                if (length <= i4) {
                    length = i4;
                }
                Edge edge2 = label3.j;
                while (edge2 != null) {
                    Label a4 = edge2.b.a();
                    if (!frame2.a(this.b, a4.h, edge2.a) || a4.k != null) {
                        a4 = label4;
                    } else {
                        a4.k = label4;
                    }
                    edge2 = edge2.c;
                    label4 = a4;
                }
                label3 = label4;
                i4 = length;
            }
            int i5 = i4;
            for (Label label5 = this.N; label5 != null; label5 = label5.i) {
                Frame frame3 = label5.h;
                if ((label5.a & 32) != 0) {
                    b(frame3);
                }
                if ((label5.a & 64) == 0) {
                    Label label6 = label5.i;
                    int i6 = label5.c;
                    int i7 = (label6 == null ? this.r.b : label6.c) - 1;
                    if (i7 >= i6) {
                        i5 = Math.max(i5, 1);
                        for (int i8 = i6; i8 < i7; i8++) {
                            this.r.a[i8] = 0;
                        }
                        this.r.a[i7] = -65;
                        this.z[a(i6, 0, 1)] = this.b.c("java/lang/Throwable") | 24117248;
                        b();
                        this.B = Handler.a(this.B, label5, label6);
                    }
                }
            }
            this.A = 0;
            for (Handler handler2 = this.B; handler2 != null; handler2 = handler2.f) {
                this.A++;
            }
            this.s = i5;
        } else if (this.M == 1) {
            for (Handler handler3 = this.B; handler3 != null; handler3 = handler3.f) {
                Label label7 = handler3.c;
                Label label8 = handler3.b;
                for (Label label9 = handler3.a; label9 != label8; label9 = label9.i) {
                    Edge edge3 = new Edge();
                    edge3.a = Integer.MAX_VALUE;
                    edge3.b = label7;
                    if ((label9.a & 128) == 0) {
                        edge3.c = label9.j;
                        label9.j = edge3;
                    } else {
                        edge3.c = label9.j.c.c;
                        label9.j.c.c = edge3;
                    }
                }
            }
            if (this.L > 0) {
                this.N.b(null, 1, this.L);
                int i9 = 0;
                for (Label label10 = this.N; label10 != null; label10 = label10.i) {
                    if ((label10.a & 128) != 0) {
                        Label label11 = label10.j.c.b;
                        if ((label11.a & 1024) == 0) {
                            i9++;
                            label11.b(null, ((((long) i9) / 32) << 32) | (1 << (i9 % 32)), this.L);
                        }
                    }
                }
                for (Label label12 = this.N; label12 != null; label12 = label12.i) {
                    if ((label12.a & 128) != 0) {
                        for (Label label13 = this.N; label13 != null; label13 = label13.i) {
                            label13.a &= -2049;
                        }
                        label12.j.c.b.b(label12, 0, this.L);
                    }
                }
            }
            Label label14 = this.N;
            int i10 = 0;
            while (label14 != null) {
                Label label15 = label14.k;
                int i11 = label14.f;
                int i12 = label14.g + i11;
                if (i12 <= i10) {
                    i12 = i10;
                }
                Edge edge4 = label14.j;
                Edge edge5 = (label14.a & 128) != 0 ? edge4.c : edge4;
                while (edge5 != null) {
                    Label label16 = edge5.b;
                    if ((label16.a & 8) == 0) {
                        label16.f = edge5.a == Integer.MAX_VALUE ? 1 : edge5.a + i11;
                        label16.a |= 8;
                        label16.k = label15;
                        label = label16;
                    } else {
                        label = label15;
                    }
                    edge5 = edge5.c;
                    label15 = label;
                }
                label14 = label15;
                i10 = i12;
            }
            this.s = Math.max(i2, i10);
        } else {
            this.s = i2;
            this.t = i3;
        }
    }

    public void visitMethodInsn(int i2, String str, String str2, String str3) {
        int i3;
        int i4;
        boolean z2 = i2 == 185;
        Item a = this.b.a(str, str2, str3, z2);
        int i5 = a.c;
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(i2, 0, this.b, a);
            } else {
                if (i5 == 0) {
                    i4 = Type.getArgumentsAndReturnSizes(str3);
                    a.c = i4;
                } else {
                    i4 = i5;
                }
                int i6 = i2 == 184 ? (this.Q - (i4 >> 2)) + (i4 & 3) + 1 : (this.Q - (i4 >> 2)) + (i4 & 3);
                if (i6 > this.R) {
                    this.R = i6;
                }
                this.Q = i6;
                i5 = i4;
            }
        }
        if (z2) {
            if (i5 == 0) {
                i3 = Type.getArgumentsAndReturnSizes(str3);
                a.c = i3;
            } else {
                i3 = i5;
            }
            this.r.b(185, a.a).a(i3 >> 2, 0);
            return;
        }
        this.r.b(i2, a.a);
    }

    public void visitMultiANewArrayInsn(String str, int i2) {
        Item a = this.b.a(str);
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(197, i2, this.b, a);
            } else {
                this.Q += 1 - i2;
            }
        }
        this.r.b(197, a.a).putByte(i2);
    }

    public AnnotationVisitor visitParameterAnnotation(int i2, String str, boolean z2) {
        ByteVector byteVector = new ByteVector();
        if ("Ljava/lang/Synthetic;".equals(str)) {
            this.S = Math.max(this.S, i2 + 1);
            return new AnnotationWriter(this.b, false, byteVector, null, 0);
        }
        byteVector.putShort(this.b.newUTF8(str)).putShort(0);
        AnnotationWriter annotationWriter = new AnnotationWriter(this.b, true, byteVector, byteVector, 2);
        if (z2) {
            if (this.o == null) {
                this.o = new AnnotationWriter[Type.getArgumentTypes(this.f).length];
            }
            annotationWriter.g = this.o[i2];
            this.o[i2] = annotationWriter;
            return annotationWriter;
        }
        if (this.p == null) {
            this.p = new AnnotationWriter[Type.getArgumentTypes(this.f).length];
        }
        annotationWriter.g = this.p[i2];
        this.p[i2] = annotationWriter;
        return annotationWriter;
    }

    public void visitTableSwitchInsn(int i2, int i3, Label label, Label... labelArr) {
        int i4 = this.r.b;
        this.r.putByte(170);
        this.r.putByteArray(null, 0, (4 - (this.r.b % 4)) % 4);
        label.a(this, this.r, i4, true);
        this.r.putInt(i2).putInt(i3);
        for (Label a : labelArr) {
            a.a(this, this.r, i4, true);
        }
        a(label, labelArr);
    }

    public void visitTryCatchBlock(Label label, Label label2, Label label3, String str) {
        this.A++;
        Handler handler = new Handler();
        handler.a = label;
        handler.b = label2;
        handler.c = label3;
        handler.d = str;
        handler.e = str != null ? this.b.newClass(str) : 0;
        if (this.C == null) {
            this.B = handler;
        } else {
            this.C.f = handler;
        }
        this.C = handler;
    }

    public void visitTypeInsn(int i2, String str) {
        Item a = this.b.a(str);
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(i2, this.r.b, this.b, a);
            } else if (i2 == 187) {
                int i3 = this.Q + 1;
                if (i3 > this.R) {
                    this.R = i3;
                }
                this.Q = i3;
            }
        }
        this.r.b(i2, a.a);
    }

    public void visitVarInsn(int i2, int i3) {
        if (this.P != null) {
            if (this.M == 0) {
                this.P.h.a(i2, i3, (ClassWriter) null, (Item) null);
            } else if (i2 == 169) {
                this.P.a |= 256;
                this.P.f = this.Q;
                e();
            } else {
                int i4 = this.Q + Frame.a[i2];
                if (i4 > this.R) {
                    this.R = i4;
                }
                this.Q = i4;
            }
        }
        if (this.M != 2) {
            int i5 = (i2 == 22 || i2 == 24 || i2 == 55 || i2 == 57) ? i3 + 2 : i3 + 1;
            if (i5 > this.t) {
                this.t = i5;
            }
        }
        if (i3 < 4 && i2 != 169) {
            this.r.putByte(i2 < 54 ? ((i2 - 21) << 2) + 26 + i3 : ((i2 - 54) << 2) + 59 + i3);
        } else if (i3 >= 256) {
            this.r.putByte(PAK_ASSETS.IMG_DZ_PINGGUOZHADAN).b(i2, i3);
        } else {
            this.r.a(i2, i3);
        }
        if (i2 >= 54 && this.M == 0 && this.A > 0) {
            visitLabel(new Label());
        }
    }
}
