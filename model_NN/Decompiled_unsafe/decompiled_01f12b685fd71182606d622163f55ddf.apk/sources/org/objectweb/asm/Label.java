package org.objectweb.asm;

import com.sg.GameSprites.GameSpriteType;

public class Label {
    int a;
    int b;
    int c;
    private int d;
    private int[] e;
    int f;
    int g;
    Frame h;
    Label i;
    public Object info;
    Edge j;
    Label k;

    private void a(int i2, int i3) {
        if (this.e == null) {
            this.e = new int[6];
        }
        if (this.d >= this.e.length) {
            int[] iArr = new int[(this.e.length + 6)];
            System.arraycopy(this.e, 0, iArr, 0, this.e.length);
            this.e = iArr;
        }
        int[] iArr2 = this.e;
        int i4 = this.d;
        this.d = i4 + 1;
        iArr2[i4] = i2;
        int[] iArr3 = this.e;
        int i5 = this.d;
        this.d = i5 + 1;
        iArr3[i5] = i3;
    }

    /* access modifiers changed from: package-private */
    public Label a() {
        return this.h == null ? this : this.h.b;
    }

    /* access modifiers changed from: package-private */
    public void a(long j2, int i2) {
        if ((this.a & 1024) == 0) {
            this.a |= 1024;
            this.e = new int[(((i2 - 1) / 32) + 1)];
        }
        int[] iArr = this.e;
        int i3 = (int) (j2 >>> 32);
        iArr[i3] = iArr[i3] | ((int) j2);
    }

    /* access modifiers changed from: package-private */
    public void a(MethodWriter methodWriter, ByteVector byteVector, int i2, boolean z) {
        if ((this.a & 2) == 0) {
            if (z) {
                a(-1 - i2, byteVector.b);
                byteVector.putInt(-1);
                return;
            }
            a(i2, byteVector.b);
            byteVector.putShort(-1);
        } else if (z) {
            byteVector.putInt(this.c - i2);
        } else {
            byteVector.putShort(this.c - i2);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(long j2) {
        return ((this.a & 1024) == 0 || (this.e[(int) (j2 >>> 32)] & ((int) j2)) == 0) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Label label) {
        if ((this.a & 1024) == 0 || (label.a & 1024) == 0) {
            return false;
        }
        for (int i2 = 0; i2 < this.e.length; i2++) {
            if ((this.e[i2] & label.e[i2]) != 0) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a(MethodWriter methodWriter, int i2, byte[] bArr) {
        int i3 = 0;
        this.a |= 2;
        this.c = i2;
        boolean z = false;
        while (i3 < this.d) {
            int i4 = i3 + 1;
            int i5 = this.e[i3];
            i3 = i4 + 1;
            int i6 = this.e[i4];
            if (i5 >= 0) {
                int i7 = i2 - i5;
                if (i7 < -32768 || i7 > 32767) {
                    byte b2 = bArr[i6 - 1] & 255;
                    if (b2 <= 168) {
                        bArr[i6 - 1] = (byte) (b2 + GameSpriteType.f0TYPE_BOSS_);
                    } else {
                        bArr[i6 - 1] = (byte) (b2 + 20);
                    }
                    z = true;
                }
                bArr[i6] = (byte) (i7 >>> 8);
                bArr[i6 + 1] = (byte) i7;
            } else {
                int i8 = i5 + i2 + 1;
                int i9 = i6 + 1;
                bArr[i6] = (byte) (i8 >>> 24);
                int i10 = i9 + 1;
                bArr[i9] = (byte) (i8 >>> 16);
                bArr[i10] = (byte) (i8 >>> 8);
                bArr[i10 + 1] = (byte) i8;
            }
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void b(Label label, long j2, int i2) {
        while (r4 != null) {
            Label label2 = r4.k;
            r4.k = null;
            if (label != null) {
                if ((r4.a & Opcodes.ACC_STRICT) != 0) {
                    r4 = label2;
                } else {
                    r4.a |= Opcodes.ACC_STRICT;
                    if ((r4.a & 256) != 0 && !r4.a(label)) {
                        Edge edge = new Edge();
                        edge.a = r4.f;
                        edge.b = label.j.b;
                        edge.c = r4.j;
                        r4.j = edge;
                    }
                }
            } else if (r4.a(j2)) {
                r4 = label2;
            } else {
                r4.a(j2, i2);
            }
            Label label3 = label2;
            for (Edge edge2 = r4.j; edge2 != null; edge2 = edge2.c) {
                if (((r4.a & 128) == 0 || edge2 != r4.j.c) && edge2.b.k == null) {
                    edge2.b.k = label3;
                    label3 = edge2.b;
                }
            }
            r4 = label3;
        }
    }

    public int getOffset() {
        if ((this.a & 2) != 0) {
            return this.c;
        }
        throw new IllegalStateException("Label offset position has not been resolved yet");
    }

    public String toString() {
        return new StringBuffer().append("L").append(System.identityHashCode(this)).toString();
    }
}
