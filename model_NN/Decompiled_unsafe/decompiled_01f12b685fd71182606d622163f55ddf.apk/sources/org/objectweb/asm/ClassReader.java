package org.objectweb.asm;

import com.sg.GameSprites.GameSpriteType;
import java.io.IOException;
import java.io.InputStream;

public class ClassReader {
    public static final int EXPAND_FRAMES = 8;
    public static final int SKIP_CODE = 1;
    public static final int SKIP_DEBUG = 2;
    public static final int SKIP_FRAMES = 4;
    private final int[] a;
    public final byte[] b;
    private final String[] c;
    private final int d;
    public final int header;

    public ClassReader(InputStream inputStream) throws IOException {
        this(a(inputStream, false));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public ClassReader(String str) throws IOException {
        this(a(ClassLoader.getSystemResourceAsStream(new StringBuffer().append(str.replace('.', '/')).append(".class").toString()), true));
    }

    public ClassReader(byte[] bArr) {
        this(bArr, 0, bArr.length);
    }

    public ClassReader(byte[] bArr, int i, int i2) {
        int i3;
        this.b = bArr;
        if (readShort(i + 6) > 51) {
            throw new IllegalArgumentException();
        }
        this.a = new int[readUnsignedShort(i + 8)];
        int length = this.a.length;
        this.c = new String[length];
        int i4 = 0;
        int i5 = 1;
        int i6 = i + 10;
        while (i5 < length) {
            this.a[i5] = i6 + 1;
            switch (bArr[i6]) {
                case 1:
                    i3 = readUnsignedShort(i6 + 1) + 3;
                    if (i3 <= i4) {
                        break;
                    } else {
                        i4 = i3;
                        break;
                    }
                case 2:
                case 7:
                case 8:
                case 13:
                case 14:
                case 16:
                case 17:
                default:
                    i3 = 3;
                    break;
                case 3:
                case 4:
                case 9:
                case 10:
                case 11:
                case 12:
                case 18:
                    i3 = 5;
                    break;
                case 5:
                case 6:
                    i3 = 9;
                    i5++;
                    break;
                case 15:
                    i3 = 4;
                    break;
            }
            i5++;
            i6 = i3 + i6;
        }
        this.d = i4;
        this.header = i6;
    }

    private int a() {
        int readUnsignedShort = (readUnsignedShort(this.header + 6) * 2) + this.header + 8;
        for (int readUnsignedShort2 = readUnsignedShort(readUnsignedShort); readUnsignedShort2 > 0; readUnsignedShort2--) {
            for (int readUnsignedShort3 = readUnsignedShort(readUnsignedShort + 8); readUnsignedShort3 > 0; readUnsignedShort3--) {
                readUnsignedShort += readInt(readUnsignedShort + 12) + 6;
            }
            readUnsignedShort += 8;
        }
        int i = readUnsignedShort + 2;
        for (int readUnsignedShort4 = readUnsignedShort(i); readUnsignedShort4 > 0; readUnsignedShort4--) {
            for (int readUnsignedShort5 = readUnsignedShort(i + 8); readUnsignedShort5 > 0; readUnsignedShort5--) {
                i += readInt(i + 12) + 6;
            }
            i += 8;
        }
        return i + 2;
    }

    private int a(int i, boolean z, boolean z2, Label[] labelArr, Context context) {
        int i2;
        int i3;
        int i4;
        char[] cArr = context.c;
        if (z) {
            i3 = i + 1;
            i2 = this.b[i] & 255;
        } else {
            context.h = -1;
            i2 = 255;
            i3 = i;
        }
        context.k = 0;
        if (i2 < 64) {
            context.i = 3;
            context.m = 0;
        } else if (i2 < 128) {
            i2 -= 64;
            i4 = a(context.n, 0, i4, cArr, labelArr);
            context.i = 4;
            context.m = 1;
        } else {
            int readUnsignedShort = readUnsignedShort(i4);
            i4 += 2;
            if (i2 == 247) {
                i4 = a(context.n, 0, i4, cArr, labelArr);
                context.i = 4;
                context.m = 1;
                i2 = readUnsignedShort;
            } else if (i2 >= 248 && i2 < 251) {
                context.i = 2;
                context.k = 251 - i2;
                context.j -= context.k;
                context.m = 0;
                i2 = readUnsignedShort;
            } else if (i2 == 251) {
                context.i = 3;
                context.m = 0;
                i2 = readUnsignedShort;
            } else if (i2 < 255) {
                int i5 = i2 - 251;
                int i6 = z2 ? context.j : 0;
                while (i5 > 0) {
                    i4 = a(context.l, i6, i4, cArr, labelArr);
                    i5--;
                    i6++;
                }
                context.i = 1;
                context.k = i2 - 251;
                context.j += context.k;
                context.m = 0;
                i2 = readUnsignedShort;
            } else {
                context.i = 0;
                int readUnsignedShort2 = readUnsignedShort(i4);
                int i7 = i4 + 2;
                context.k = readUnsignedShort2;
                context.j = readUnsignedShort2;
                int i8 = 0;
                for (int i9 = readUnsignedShort2; i9 > 0; i9--) {
                    i7 = a(context.l, i8, i7, cArr, labelArr);
                    i8++;
                }
                int readUnsignedShort3 = readUnsignedShort(i7);
                i4 = i7 + 2;
                context.m = readUnsignedShort3;
                int i10 = 0;
                for (int i11 = readUnsignedShort3; i11 > 0; i11--) {
                    i4 = a(context.n, i10, i4, cArr, labelArr);
                    i10++;
                }
                i2 = readUnsignedShort;
            }
        }
        context.h += i2 + 1;
        readLabel(context.h, labelArr);
        return i4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int
     arg types: [int, char[], int, ?[OBJECT, ARRAY]]
     candidates:
      org.objectweb.asm.ClassReader.a(int, char[], java.lang.String, org.objectweb.asm.AnnotationVisitor):int
      org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int
     arg types: [int, char[], int, org.objectweb.asm.AnnotationVisitor]
     candidates:
      org.objectweb.asm.ClassReader.a(int, char[], java.lang.String, org.objectweb.asm.AnnotationVisitor):int
      org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int */
    private int a(int i, char[] cArr, String str, AnnotationVisitor annotationVisitor) {
        int i2 = 0;
        if (annotationVisitor == null) {
            switch (this.b[i] & 255) {
                case 64:
                    return a(i + 3, cArr, true, (AnnotationVisitor) null);
                case 91:
                    return a(i + 1, cArr, false, (AnnotationVisitor) null);
                case 101:
                    return i + 5;
                default:
                    return i + 3;
            }
        } else {
            int i3 = i + 1;
            switch (this.b[i] & 255) {
                case 64:
                    return a(i3 + 2, cArr, true, annotationVisitor.visitAnnotation(str, readUTF8(i3, cArr)));
                case 66:
                    annotationVisitor.visit(str, new Byte((byte) readInt(this.a[readUnsignedShort(i3)])));
                    return i3 + 2;
                case 67:
                    annotationVisitor.visit(str, new Character((char) readInt(this.a[readUnsignedShort(i3)])));
                    return i3 + 2;
                case 68:
                case 70:
                case 73:
                case 74:
                    annotationVisitor.visit(str, readConst(readUnsignedShort(i3), cArr));
                    return i3 + 2;
                case 83:
                    annotationVisitor.visit(str, new Short((short) readInt(this.a[readUnsignedShort(i3)])));
                    return i3 + 2;
                case 90:
                    annotationVisitor.visit(str, readInt(this.a[readUnsignedShort(i3)]) == 0 ? Boolean.FALSE : Boolean.TRUE);
                    return i3 + 2;
                case 91:
                    int readUnsignedShort = readUnsignedShort(i3);
                    int i4 = i3 + 2;
                    if (readUnsignedShort == 0) {
                        return a(i4 - 2, cArr, false, annotationVisitor.visitArray(str));
                    }
                    int i5 = i4 + 1;
                    switch (this.b[i4] & 255) {
                        case 66:
                            byte[] bArr = new byte[readUnsignedShort];
                            while (i2 < readUnsignedShort) {
                                bArr[i2] = (byte) readInt(this.a[readUnsignedShort(i5)]);
                                i5 += 3;
                                i2++;
                            }
                            annotationVisitor.visit(str, bArr);
                            return i5 - 1;
                        case 67:
                            char[] cArr2 = new char[readUnsignedShort];
                            while (i2 < readUnsignedShort) {
                                cArr2[i2] = (char) readInt(this.a[readUnsignedShort(i5)]);
                                i5 += 3;
                                i2++;
                            }
                            annotationVisitor.visit(str, cArr2);
                            return i5 - 1;
                        case 68:
                            double[] dArr = new double[readUnsignedShort];
                            while (i2 < readUnsignedShort) {
                                dArr[i2] = Double.longBitsToDouble(readLong(this.a[readUnsignedShort(i5)]));
                                i5 += 3;
                                i2++;
                            }
                            annotationVisitor.visit(str, dArr);
                            return i5 - 1;
                        case 70:
                            float[] fArr = new float[readUnsignedShort];
                            while (i2 < readUnsignedShort) {
                                fArr[i2] = Float.intBitsToFloat(readInt(this.a[readUnsignedShort(i5)]));
                                i5 += 3;
                                i2++;
                            }
                            annotationVisitor.visit(str, fArr);
                            return i5 - 1;
                        case 73:
                            int[] iArr = new int[readUnsignedShort];
                            while (i2 < readUnsignedShort) {
                                iArr[i2] = readInt(this.a[readUnsignedShort(i5)]);
                                i5 += 3;
                                i2++;
                            }
                            annotationVisitor.visit(str, iArr);
                            return i5 - 1;
                        case 74:
                            long[] jArr = new long[readUnsignedShort];
                            while (i2 < readUnsignedShort) {
                                jArr[i2] = readLong(this.a[readUnsignedShort(i5)]);
                                i5 += 3;
                                i2++;
                            }
                            annotationVisitor.visit(str, jArr);
                            return i5 - 1;
                        case 83:
                            short[] sArr = new short[readUnsignedShort];
                            while (i2 < readUnsignedShort) {
                                sArr[i2] = (short) readInt(this.a[readUnsignedShort(i5)]);
                                i5 += 3;
                                i2++;
                            }
                            annotationVisitor.visit(str, sArr);
                            return i5 - 1;
                        case 90:
                            boolean[] zArr = new boolean[readUnsignedShort];
                            int i6 = i5;
                            for (int i7 = 0; i7 < readUnsignedShort; i7++) {
                                zArr[i7] = readInt(this.a[readUnsignedShort(i6)]) != 0;
                                i6 += 3;
                            }
                            annotationVisitor.visit(str, zArr);
                            return i6 - 1;
                        default:
                            return a(i5 - 3, cArr, false, annotationVisitor.visitArray(str));
                    }
                case 99:
                    annotationVisitor.visit(str, Type.getType(readUTF8(i3, cArr)));
                    return i3 + 2;
                case 101:
                    annotationVisitor.visitEnum(str, readUTF8(i3, cArr), readUTF8(i3 + 2, cArr));
                    return i3 + 4;
                case 115:
                    annotationVisitor.visit(str, readUTF8(i3, cArr));
                    return i3 + 2;
                default:
                    return i3;
            }
        }
    }

    private int a(int i, char[] cArr, boolean z, AnnotationVisitor annotationVisitor) {
        int i2;
        int readUnsignedShort = readUnsignedShort(i);
        int i3 = i + 2;
        if (z) {
            int i4 = readUnsignedShort;
            i2 = i3;
            int i5 = i4;
            while (i5 > 0) {
                i5--;
                i2 = a(i2 + 2, cArr, readUTF8(i2, cArr), annotationVisitor);
            }
        } else {
            int i6 = readUnsignedShort;
            int i7 = i3;
            int i8 = i6;
            while (i8 > 0) {
                i8--;
                i7 = a(i2, cArr, (String) null, annotationVisitor);
            }
        }
        if (annotationVisitor != null) {
            annotationVisitor.visitEnd();
        }
        return i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int
     arg types: [int, char[], int, org.objectweb.asm.AnnotationVisitor]
     candidates:
      org.objectweb.asm.ClassReader.a(int, char[], java.lang.String, org.objectweb.asm.AnnotationVisitor):int
      org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int */
    private int a(ClassVisitor classVisitor, Context context, int i) {
        Attribute a2;
        int i2;
        int i3;
        int i4;
        char[] cArr = context.c;
        int readUnsignedShort = readUnsignedShort(i);
        String readUTF8 = readUTF8(i + 2, cArr);
        String readUTF82 = readUTF8(i + 4, cArr);
        int i5 = i + 6;
        String str = null;
        int i6 = 0;
        int i7 = 0;
        Object obj = null;
        Attribute attribute = null;
        int readUnsignedShort2 = readUnsignedShort(i5);
        int i8 = i5;
        while (readUnsignedShort2 > 0) {
            String readUTF83 = readUTF8(i8 + 2, cArr);
            if ("ConstantValue".equals(readUTF83)) {
                int readUnsignedShort3 = readUnsignedShort(i8 + 8);
                obj = readUnsignedShort3 == 0 ? null : readConst(readUnsignedShort3, cArr);
                a2 = attribute;
                i2 = i7;
                i3 = i6;
                i4 = readUnsignedShort;
            } else if ("Signature".equals(readUTF83)) {
                str = readUTF8(i8 + 8, cArr);
                a2 = attribute;
                i2 = i7;
                i3 = i6;
                i4 = readUnsignedShort;
            } else if ("Deprecated".equals(readUTF83)) {
                i2 = i7;
                i3 = i6;
                i4 = 131072 | readUnsignedShort;
                a2 = attribute;
            } else if ("Synthetic".equals(readUTF83)) {
                i2 = i7;
                i3 = i6;
                i4 = 266240 | readUnsignedShort;
                a2 = attribute;
            } else if ("RuntimeVisibleAnnotations".equals(readUTF83)) {
                i2 = i7;
                i3 = i8 + 8;
                i4 = readUnsignedShort;
                a2 = attribute;
            } else if ("RuntimeInvisibleAnnotations".equals(readUTF83)) {
                i2 = i8 + 8;
                i3 = i6;
                i4 = readUnsignedShort;
                a2 = attribute;
            } else {
                a2 = a(context.a, readUTF83, i8 + 8, readInt(i8 + 4), cArr, -1, null);
                if (a2 != null) {
                    a2.a = attribute;
                    i2 = i7;
                    i3 = i6;
                    i4 = readUnsignedShort;
                } else {
                    a2 = attribute;
                    i2 = i7;
                    i3 = i6;
                    i4 = readUnsignedShort;
                }
            }
            readUnsignedShort2--;
            attribute = a2;
            i7 = i2;
            i6 = i3;
            readUnsignedShort = i4;
            i8 += readInt(i8 + 4) + 6;
        }
        int i9 = i8 + 2;
        FieldVisitor visitField = classVisitor.visitField(readUnsignedShort, readUTF8, readUTF82, str, obj);
        if (visitField != null) {
            if (i6 != 0) {
                int i10 = i6 + 2;
                for (int readUnsignedShort4 = readUnsignedShort(i6); readUnsignedShort4 > 0; readUnsignedShort4--) {
                    i10 = a(i10 + 2, cArr, true, visitField.visitAnnotation(readUTF8(i10, cArr), true));
                }
            }
            if (i7 != 0) {
                int i11 = i7 + 2;
                for (int readUnsignedShort5 = readUnsignedShort(i7); readUnsignedShort5 > 0; readUnsignedShort5--) {
                    i11 = a(i11 + 2, cArr, true, visitField.visitAnnotation(readUTF8(i11, cArr), false));
                }
            }
            while (attribute != null) {
                Attribute attribute2 = attribute.a;
                attribute.a = null;
                visitField.visitAttribute(attribute);
                attribute = attribute2;
            }
            visitField.visitEnd();
        }
        return i9;
    }

    private int a(Object[] objArr, int i, int i2, char[] cArr, Label[] labelArr) {
        int i3 = i2 + 1;
        switch (this.b[i2] & 255) {
            case 0:
                objArr[i] = Opcodes.TOP;
                return i3;
            case 1:
                objArr[i] = Opcodes.INTEGER;
                return i3;
            case 2:
                objArr[i] = Opcodes.FLOAT;
                return i3;
            case 3:
                objArr[i] = Opcodes.DOUBLE;
                return i3;
            case 4:
                objArr[i] = Opcodes.LONG;
                return i3;
            case 5:
                objArr[i] = Opcodes.NULL;
                return i3;
            case 6:
                objArr[i] = Opcodes.UNINITIALIZED_THIS;
                return i3;
            case 7:
                objArr[i] = readClass(i3, cArr);
                return i3 + 2;
            default:
                objArr[i] = readLabel(readUnsignedShort(i3), labelArr);
                return i3 + 2;
        }
    }

    private String a(int i, int i2, char[] cArr) {
        int i3;
        int i4 = i + i2;
        byte[] bArr = this.b;
        char c2 = 0;
        char c3 = 0;
        int i5 = 0;
        while (i < i4) {
            int i6 = i + 1;
            byte b2 = bArr[i];
            switch (c3) {
                case 0:
                    byte b3 = b2 & 255;
                    if (b3 >= 128) {
                        if (b3 < 224 && b3 > 191) {
                            c2 = (char) (b3 & 31);
                            c3 = 1;
                            i3 = i5;
                            break;
                        } else {
                            c2 = (char) (b3 & 15);
                            c3 = 2;
                            i3 = i5;
                            break;
                        }
                    } else {
                        i3 = i5 + 1;
                        cArr[i5] = (char) b3;
                        break;
                    }
                case 1:
                    cArr[i5] = (char) ((b2 & GameSpriteType.f29TYPE_ENEMY_) | (c2 << 6));
                    i3 = i5 + 1;
                    c3 = 0;
                    break;
                case 2:
                    c2 = (char) ((c2 << 6) | (b2 & GameSpriteType.f29TYPE_ENEMY_));
                    c3 = 1;
                    i3 = i5;
                    break;
                default:
                    i3 = i5;
                    break;
            }
            i5 = i3;
            i = i6;
        }
        return new String(cArr, 0, i5);
    }

    private Attribute a(Attribute[] attributeArr, String str, int i, int i2, char[] cArr, int i3, Label[] labelArr) {
        for (int i4 = 0; i4 < attributeArr.length; i4++) {
            if (attributeArr[i4].type.equals(str)) {
                return attributeArr[i4].read(this, i, i2, cArr, i3, labelArr);
            }
        }
        return new Attribute(str).read(this, i, i2, null, -1, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int
     arg types: [int, char[], int, org.objectweb.asm.AnnotationVisitor]
     candidates:
      org.objectweb.asm.ClassReader.a(int, char[], java.lang.String, org.objectweb.asm.AnnotationVisitor):int
      org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int */
    private void a(int i, String str, char[] cArr, boolean z, MethodVisitor methodVisitor) {
        int i2 = i + 1;
        byte b2 = this.b[i] & 255;
        int length = Type.getArgumentTypes(str).length - b2;
        int i3 = 0;
        while (i3 < length) {
            AnnotationVisitor visitParameterAnnotation = methodVisitor.visitParameterAnnotation(i3, "Ljava/lang/Synthetic;", false);
            if (visitParameterAnnotation != null) {
                visitParameterAnnotation.visitEnd();
            }
            i3++;
        }
        while (true) {
            int i4 = i3;
            if (i4 < b2 + length) {
                i2 += 2;
                for (int readUnsignedShort = readUnsignedShort(i2); readUnsignedShort > 0; readUnsignedShort--) {
                    i2 = a(i2 + 2, cArr, true, methodVisitor.visitParameterAnnotation(i4, readUTF8(i2, cArr), z));
                }
                i3 = i4 + 1;
            } else {
                return;
            }
        }
    }

    private void a(ClassWriter classWriter, Item[] itemArr, char[] cArr) {
        boolean z;
        int a2 = a();
        int readUnsignedShort = readUnsignedShort(a2);
        int i = a2;
        while (true) {
            if (readUnsignedShort <= 0) {
                z = false;
                break;
            } else if ("BootstrapMethods".equals(readUTF8(i + 2, cArr))) {
                z = true;
                break;
            } else {
                readUnsignedShort--;
                i = readInt(i + 4) + 6 + i;
            }
        }
        if (z) {
            int readUnsignedShort2 = readUnsignedShort(i + 8);
            int i2 = i + 10;
            for (int i3 = 0; i3 < readUnsignedShort2; i3++) {
                int i4 = (i2 - i) - 10;
                int hashCode = readConst(readUnsignedShort(i2), cArr).hashCode();
                for (int readUnsignedShort3 = readUnsignedShort(i2 + 2); readUnsignedShort3 > 0; readUnsignedShort3--) {
                    hashCode ^= readConst(readUnsignedShort(i2 + 4), cArr).hashCode();
                    i2 += 2;
                }
                i2 += 4;
                Item item = new Item(i3);
                item.a(i4, hashCode & Integer.MAX_VALUE);
                int length = item.j % itemArr.length;
                item.k = itemArr[length];
                itemArr[length] = item;
            }
            int readInt = readInt(i + 4);
            ByteVector byteVector = new ByteVector(readInt + 62);
            byteVector.putByteArray(this.b, i + 10, readInt - 2);
            classWriter.z = readUnsignedShort2;
            classWriter.A = byteVector;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private void a(Context context) {
        int i;
        int i2 = 1;
        String str = context.g;
        Object[] objArr = context.l;
        int i3 = 0;
        if ((context.e & 8) == 0) {
            if ("<init>".equals(context.f)) {
                objArr[0] = Opcodes.UNINITIALIZED_THIS;
                i3 = 1;
            } else {
                objArr[0] = readClass(this.header + 2, context.c);
                i3 = 1;
            }
        }
        while (true) {
            int i4 = i2 + 1;
            switch (str.charAt(i2)) {
                case 'B':
                case 'C':
                case 'I':
                case 'S':
                case 'Z':
                    objArr[i] = Opcodes.INTEGER;
                    i++;
                    i2 = i4;
                    break;
                case 'D':
                    objArr[i] = Opcodes.DOUBLE;
                    i++;
                    i2 = i4;
                    break;
                case 'F':
                    objArr[i] = Opcodes.FLOAT;
                    i++;
                    i2 = i4;
                    break;
                case 'J':
                    objArr[i] = Opcodes.LONG;
                    i++;
                    i2 = i4;
                    break;
                case 'L':
                    while (str.charAt(i4) != ';') {
                        i4++;
                    }
                    int i5 = i2 + 1;
                    i2 = i4 + 1;
                    objArr[i] = str.substring(i5, i4);
                    i++;
                    break;
                case '[':
                    while (str.charAt(i4) == '[') {
                        i4++;
                    }
                    if (str.charAt(i4) == 'L') {
                        while (true) {
                            i4++;
                            if (str.charAt(i4) != ';') {
                            }
                        }
                    }
                    int i6 = i4 + 1;
                    objArr[i] = str.substring(i2, i6);
                    i2 = i6;
                    i++;
                    break;
                default:
                    context.j = i;
                    return;
            }
        }
    }

    private void a(MethodVisitor methodVisitor, Context context, int i) {
        Context context2;
        int i2;
        int readUnsignedShort;
        int i3;
        int i4;
        int i5;
        boolean z;
        int i6;
        int i7;
        Attribute attribute;
        int i8;
        byte[] bArr = this.b;
        char[] cArr = context.c;
        int readUnsignedShort2 = readUnsignedShort(i);
        int readUnsignedShort3 = readUnsignedShort(i + 2);
        int readInt = readInt(i + 4);
        int i9 = i + 8;
        int i10 = i9 + readInt;
        Label[] labelArr = new Label[(readInt + 2)];
        readLabel(readInt + 1, labelArr);
        int i11 = i9;
        while (i11 < i10) {
            int i12 = i11 - i9;
            switch (ClassWriter.a[bArr[i11] & 255]) {
                case 0:
                case 4:
                    i8 = i11 + 1;
                    break;
                case 1:
                case 3:
                case 11:
                    i8 = i11 + 2;
                    break;
                case 2:
                case 5:
                case 6:
                case 12:
                case 13:
                    i8 = i11 + 3;
                    break;
                case 7:
                case 8:
                    i8 = i11 + 5;
                    break;
                case 9:
                    readLabel(readShort(i11 + 1) + i12, labelArr);
                    i8 = i11 + 3;
                    break;
                case 10:
                    readLabel(readInt(i11 + 1) + i12, labelArr);
                    i8 = i11 + 5;
                    break;
                case 14:
                    int i13 = (i11 + 4) - (i12 & 3);
                    readLabel(readInt(i13) + i12, labelArr);
                    for (int readInt2 = (readInt(i13 + 8) - readInt(i13 + 4)) + 1; readInt2 > 0; readInt2--) {
                        readLabel(readInt(i13 + 12) + i12, labelArr);
                        i13 += 4;
                    }
                    i8 = i13 + 12;
                    break;
                case 15:
                    int i14 = (i11 + 4) - (i12 & 3);
                    readLabel(readInt(i14) + i12, labelArr);
                    for (int readInt3 = readInt(i14 + 4); readInt3 > 0; readInt3--) {
                        readLabel(readInt(i14 + 12) + i12, labelArr);
                        i14 += 8;
                    }
                    i8 = i14 + 8;
                    break;
                case 16:
                default:
                    i8 = i11 + 4;
                    break;
                case 17:
                    if ((bArr[i11 + 1] & 255) != 132) {
                        i8 = i11 + 4;
                        break;
                    } else {
                        i8 = i11 + 6;
                        break;
                    }
            }
            i11 = i8;
        }
        for (int readUnsignedShort4 = readUnsignedShort(i11); readUnsignedShort4 > 0; readUnsignedShort4--) {
            methodVisitor.visitTryCatchBlock(readLabel(readUnsignedShort(i11 + 2), labelArr), readLabel(readUnsignedShort(i11 + 4), labelArr), readLabel(readUnsignedShort(i11 + 6), labelArr), readUTF8(this.a[readUnsignedShort(i11 + 8)], cArr));
            i11 += 8;
        }
        int i15 = i11 + 2;
        int i16 = 0;
        int i17 = 0;
        boolean z2 = true;
        boolean z3 = (context.b & 8) != 0;
        int i18 = 0;
        int i19 = 0;
        int i20 = 0;
        Attribute attribute2 = null;
        int readUnsignedShort5 = readUnsignedShort(i15);
        while (readUnsignedShort5 > 0) {
            String readUTF8 = readUTF8(i15 + 2, cArr);
            if ("LocalVariableTable".equals(readUTF8)) {
                if ((context.b & 2) == 0) {
                    int i21 = i15 + 8;
                    int i22 = i15;
                    for (int readUnsignedShort6 = readUnsignedShort(i15 + 8); readUnsignedShort6 > 0; readUnsignedShort6--) {
                        int readUnsignedShort7 = readUnsignedShort(i22 + 10);
                        if (labelArr[readUnsignedShort7] == null) {
                            readLabel(readUnsignedShort7, labelArr).a |= 1;
                        }
                        int readUnsignedShort8 = readUnsignedShort7 + readUnsignedShort(i22 + 12);
                        if (labelArr[readUnsignedShort8] == null) {
                            readLabel(readUnsignedShort8, labelArr).a |= 1;
                        }
                        i22 += 10;
                    }
                    i4 = i19;
                    i5 = i18;
                    z = z2;
                    i6 = i17;
                    i7 = i21;
                    i3 = i20;
                }
                i3 = i20;
                i4 = i19;
                i5 = i18;
                z = z2;
                i6 = i17;
                i7 = i16;
            } else if ("LocalVariableTypeTable".equals(readUTF8)) {
                i4 = i19;
                i5 = i18;
                z = z2;
                i6 = i15 + 8;
                i7 = i16;
                i3 = i20;
            } else if ("LineNumberTable".equals(readUTF8)) {
                if ((context.b & 2) == 0) {
                    int i23 = i15;
                    for (int readUnsignedShort9 = readUnsignedShort(i15 + 8); readUnsignedShort9 > 0; readUnsignedShort9--) {
                        int readUnsignedShort10 = readUnsignedShort(i23 + 10);
                        if (labelArr[readUnsignedShort10] == null) {
                            readLabel(readUnsignedShort10, labelArr).a |= 1;
                        }
                        labelArr[readUnsignedShort10].b = readUnsignedShort(i23 + 12);
                        i23 += 4;
                    }
                    i3 = i20;
                    i4 = i19;
                    i5 = i18;
                    z = z2;
                    i6 = i17;
                    i7 = i16;
                }
                i3 = i20;
                i4 = i19;
                i5 = i18;
                z = z2;
                i6 = i17;
                i7 = i16;
            } else if ("StackMapTable".equals(readUTF8)) {
                if ((context.b & 4) == 0) {
                    i5 = i15 + 10;
                    i4 = readInt(i15 + 4);
                    i3 = readUnsignedShort(i15 + 8);
                    z = z2;
                    i6 = i17;
                    i7 = i16;
                }
                i3 = i20;
                i4 = i19;
                i5 = i18;
                z = z2;
                i6 = i17;
                i7 = i16;
            } else if ("StackMap".equals(readUTF8)) {
                if ((context.b & 4) == 0) {
                    z = false;
                    i5 = i15 + 10;
                    i4 = readInt(i15 + 4);
                    i3 = readUnsignedShort(i15 + 8);
                    i6 = i17;
                    i7 = i16;
                }
                i3 = i20;
                i4 = i19;
                i5 = i18;
                z = z2;
                i6 = i17;
                i7 = i16;
            } else {
                int i24 = 0;
                Attribute attribute3 = attribute2;
                while (i24 < context.a.length) {
                    if (!context.a[i24].type.equals(readUTF8) || (attribute = context.a[i24].read(this, i15 + 8, readInt(i15 + 4), cArr, i9 - 8, labelArr)) == null) {
                        attribute = attribute3;
                    } else {
                        attribute.a = attribute3;
                    }
                    i24++;
                    attribute3 = attribute;
                }
                attribute2 = attribute3;
                i3 = i20;
                i4 = i19;
                i5 = i18;
                z = z2;
                i7 = i16;
                i6 = i17;
            }
            i15 += readInt(i15 + 4) + 6;
            readUnsignedShort5--;
            i19 = i4;
            i18 = i5;
            z2 = z;
            i17 = i6;
            i16 = i7;
            i20 = i3;
        }
        int i25 = i15 + 2;
        if (i18 != 0) {
            context.h = -1;
            context.i = 0;
            context.j = 0;
            context.k = 0;
            context.m = 0;
            context.l = new Object[readUnsignedShort3];
            context.n = new Object[readUnsignedShort2];
            if (z3) {
                a(context);
            }
            for (int i26 = i18; i26 < (i18 + i19) - 2; i26++) {
                if (bArr[i26] == 8 && (readUnsignedShort = readUnsignedShort(i26 + 1)) >= 0 && readUnsignedShort < readInt && (bArr[i9 + readUnsignedShort] & 255) == 187) {
                    readLabel(readUnsignedShort, labelArr);
                }
            }
            context2 = context;
        } else {
            context2 = null;
        }
        int i27 = i20;
        int i28 = i18;
        int i29 = i9;
        while (i29 < i10) {
            int i30 = i29 - i9;
            Label label = labelArr[i30];
            if (label != null) {
                methodVisitor.visitLabel(label);
                if ((context.b & 2) == 0 && label.b > 0) {
                    methodVisitor.visitLineNumber(label.b, label);
                }
            }
            Context context3 = context2;
            int i31 = i27;
            int i32 = i28;
            while (context3 != null && (context3.h == i30 || context3.h == -1)) {
                if (context3.h != -1) {
                    if (!z2 || z3) {
                        methodVisitor.visitFrame(-1, context3.j, context3.l, context3.m, context3.n);
                    } else {
                        methodVisitor.visitFrame(context3.i, context3.k, context3.l, context3.m, context3.n);
                    }
                }
                if (i31 > 0) {
                    i31--;
                    i32 = a(i32, z2, z3, labelArr, context3);
                } else {
                    context3 = null;
                }
            }
            byte b2 = bArr[i29] & 255;
            switch (ClassWriter.a[b2]) {
                case 0:
                    methodVisitor.visitInsn(b2);
                    i2 = i29 + 1;
                    break;
                case 1:
                    methodVisitor.visitIntInsn(b2, bArr[i29 + 1]);
                    i2 = i29 + 2;
                    break;
                case 2:
                    methodVisitor.visitIntInsn(b2, readShort(i29 + 1));
                    i2 = i29 + 3;
                    break;
                case 3:
                    methodVisitor.visitVarInsn(b2, bArr[i29 + 1] & 255);
                    i2 = i29 + 2;
                    break;
                case 4:
                    if (b2 > 54) {
                        int i33 = b2 - 59;
                        methodVisitor.visitVarInsn((i33 >> 2) + 54, i33 & 3);
                    } else {
                        int i34 = b2 - 26;
                        methodVisitor.visitVarInsn((i34 >> 2) + 21, i34 & 3);
                    }
                    i2 = i29 + 1;
                    break;
                case 5:
                    methodVisitor.visitTypeInsn(b2, readClass(i29 + 1, cArr));
                    i2 = i29 + 3;
                    break;
                case 6:
                case 7:
                    int i35 = this.a[readUnsignedShort(i29 + 1)];
                    String readClass = readClass(i35, cArr);
                    int i36 = this.a[readUnsignedShort(i35 + 2)];
                    String readUTF82 = readUTF8(i36, cArr);
                    String readUTF83 = readUTF8(i36 + 2, cArr);
                    if (b2 < 182) {
                        methodVisitor.visitFieldInsn(b2, readClass, readUTF82, readUTF83);
                    } else {
                        methodVisitor.visitMethodInsn(b2, readClass, readUTF82, readUTF83);
                    }
                    if (b2 != 185) {
                        i2 = i29 + 3;
                        break;
                    } else {
                        i2 = i29 + 5;
                        break;
                    }
                case 8:
                    int i37 = this.a[readUnsignedShort(i29 + 1)];
                    int i38 = context.d[readUnsignedShort(i37)];
                    Handle handle = (Handle) readConst(readUnsignedShort(i38), cArr);
                    int readUnsignedShort11 = readUnsignedShort(i38 + 2);
                    Object[] objArr = new Object[readUnsignedShort11];
                    int i39 = i38 + 4;
                    for (int i40 = 0; i40 < readUnsignedShort11; i40++) {
                        objArr[i40] = readConst(readUnsignedShort(i39), cArr);
                        i39 += 2;
                    }
                    int i41 = this.a[readUnsignedShort(i37 + 2)];
                    methodVisitor.visitInvokeDynamicInsn(readUTF8(i41, cArr), readUTF8(i41 + 2, cArr), handle, objArr);
                    i2 = i29 + 5;
                    break;
                case 9:
                    methodVisitor.visitJumpInsn(b2, labelArr[readShort(i29 + 1) + i30]);
                    i2 = i29 + 3;
                    break;
                case 10:
                    methodVisitor.visitJumpInsn(b2 - 33, labelArr[readInt(i29 + 1) + i30]);
                    i2 = i29 + 5;
                    break;
                case 11:
                    methodVisitor.visitLdcInsn(readConst(bArr[i29 + 1] & 255, cArr));
                    i2 = i29 + 2;
                    break;
                case 12:
                    methodVisitor.visitLdcInsn(readConst(readUnsignedShort(i29 + 1), cArr));
                    i2 = i29 + 3;
                    break;
                case 13:
                    methodVisitor.visitIincInsn(bArr[i29 + 1] & 255, bArr[i29 + 2]);
                    i2 = i29 + 3;
                    break;
                case 14:
                    int i42 = (i29 + 4) - (i30 & 3);
                    int readInt4 = i30 + readInt(i42);
                    int readInt5 = readInt(i42 + 4);
                    int readInt6 = readInt(i42 + 8);
                    Label[] labelArr2 = new Label[((readInt6 - readInt5) + 1)];
                    i2 = i42 + 12;
                    for (int i43 = 0; i43 < labelArr2.length; i43++) {
                        labelArr2[i43] = labelArr[readInt(i2) + i30];
                        i2 += 4;
                    }
                    methodVisitor.visitTableSwitchInsn(readInt5, readInt6, labelArr[readInt4], labelArr2);
                    break;
                case 15:
                    int i44 = (i29 + 4) - (i30 & 3);
                    int readInt7 = i30 + readInt(i44);
                    int readInt8 = readInt(i44 + 4);
                    int[] iArr = new int[readInt8];
                    Label[] labelArr3 = new Label[readInt8];
                    int i45 = i44 + 8;
                    for (int i46 = 0; i46 < readInt8; i46++) {
                        iArr[i46] = readInt(i2);
                        labelArr3[i46] = labelArr[readInt(i2 + 4) + i30];
                        i45 = i2 + 8;
                    }
                    methodVisitor.visitLookupSwitchInsn(labelArr[readInt7], iArr, labelArr3);
                    break;
                case 16:
                default:
                    methodVisitor.visitMultiANewArrayInsn(readClass(i29 + 1, cArr), bArr[i29 + 3] & 255);
                    i2 = i29 + 4;
                    break;
                case 17:
                    byte b3 = bArr[i29 + 1] & 255;
                    if (b3 != 132) {
                        methodVisitor.visitVarInsn(b3, readUnsignedShort(i29 + 2));
                        i2 = i29 + 4;
                        break;
                    } else {
                        methodVisitor.visitIincInsn(readUnsignedShort(i29 + 2), readShort(i29 + 4));
                        i2 = i29 + 6;
                        break;
                    }
            }
            context2 = context3;
            i28 = i32;
            i29 = i2;
            i27 = i31;
        }
        if (labelArr[readInt] != null) {
            methodVisitor.visitLabel(labelArr[readInt]);
        }
        if ((context.b & 2) == 0 && i16 != 0) {
            int[] iArr2 = null;
            if (i17 != 0) {
                int i47 = i17 + 2;
                iArr2 = new int[(readUnsignedShort(i17) * 3)];
                int length = iArr2.length;
                while (length > 0) {
                    int i48 = length - 1;
                    iArr2[i48] = i47 + 6;
                    int i49 = i48 - 1;
                    iArr2[i49] = readUnsignedShort(i47 + 8);
                    length = i49 - 1;
                    iArr2[length] = readUnsignedShort(i47);
                    i47 += 10;
                }
            }
            int i50 = i16 + 2;
            for (int readUnsignedShort12 = readUnsignedShort(i16); readUnsignedShort12 > 0; readUnsignedShort12--) {
                int readUnsignedShort13 = readUnsignedShort(i50);
                int readUnsignedShort14 = readUnsignedShort(i50 + 2);
                int readUnsignedShort15 = readUnsignedShort(i50 + 8);
                String str = null;
                if (iArr2 != null) {
                    int i51 = 0;
                    while (true) {
                        if (i51 < iArr2.length) {
                            if (iArr2[i51] == readUnsignedShort13 && iArr2[i51 + 1] == readUnsignedShort15) {
                                str = readUTF8(iArr2[i51 + 2], cArr);
                            } else {
                                i51 += 3;
                            }
                        }
                    }
                }
                methodVisitor.visitLocalVariable(readUTF8(i50 + 4, cArr), readUTF8(i50 + 6, cArr), str, labelArr[readUnsignedShort13], labelArr[readUnsignedShort13 + readUnsignedShort14], readUnsignedShort15);
                i50 += 10;
            }
        }
        while (attribute2 != null) {
            Attribute attribute4 = attribute2.a;
            attribute2.a = null;
            methodVisitor.visitAttribute(attribute2);
            attribute2 = attribute4;
        }
        methodVisitor.visitMaxs(readUnsignedShort2, readUnsignedShort3);
    }

    private static byte[] a(InputStream inputStream, boolean z) throws IOException {
        byte[] bArr;
        int i = 0;
        if (inputStream == null) {
            throw new IOException("Class not found");
        }
        try {
            byte[] bArr2 = new byte[inputStream.available()];
            while (true) {
                int i2 = i;
                int read = inputStream.read(bArr2, i2, bArr2.length - i2);
                if (read == -1) {
                    if (i2 < bArr2.length) {
                        bArr = new byte[i2];
                        System.arraycopy(bArr2, 0, bArr, 0, i2);
                    } else {
                        bArr = bArr2;
                    }
                    if (!z) {
                        return bArr;
                    }
                    inputStream.close();
                    return bArr;
                }
                int i3 = i2 + read;
                if (i3 == bArr2.length) {
                    int read2 = inputStream.read();
                    if (read2 < 0) {
                        return bArr2;
                    }
                    byte[] bArr3 = new byte[(bArr2.length + 1000)];
                    System.arraycopy(bArr2, 0, bArr3, 0, i3);
                    i = i3 + 1;
                    bArr3[i3] = (byte) read2;
                    bArr2 = bArr3;
                } else {
                    i = i3;
                }
            }
        } finally {
            if (z) {
                inputStream.close();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int
     arg types: [int, char[], int, org.objectweb.asm.AnnotationVisitor]
     candidates:
      org.objectweb.asm.ClassReader.a(int, char[], java.lang.String, org.objectweb.asm.AnnotationVisitor):int
      org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.objectweb.asm.ClassReader.a(int, java.lang.String, char[], boolean, org.objectweb.asm.MethodVisitor):void
     arg types: [int, java.lang.String, char[], int, org.objectweb.asm.MethodVisitor]
     candidates:
      org.objectweb.asm.ClassReader.a(int, boolean, boolean, org.objectweb.asm.Label[], org.objectweb.asm.Context):int
      org.objectweb.asm.ClassReader.a(java.lang.Object[], int, int, char[], org.objectweb.asm.Label[]):int
      org.objectweb.asm.ClassReader.a(int, java.lang.String, char[], boolean, org.objectweb.asm.MethodVisitor):void */
    private int b(ClassVisitor classVisitor, Context context, int i) {
        Attribute attribute;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        char[] cArr = context.c;
        int readUnsignedShort = readUnsignedShort(i);
        String readUTF8 = readUTF8(i + 2, cArr);
        String readUTF82 = readUTF8(i + 4, cArr);
        int i10 = i + 6;
        int i11 = 0;
        int i12 = 0;
        String[] strArr = null;
        String str = null;
        int i13 = 0;
        int i14 = 0;
        int i15 = 0;
        int i16 = 0;
        int i17 = 0;
        Attribute attribute2 = null;
        int readUnsignedShort2 = readUnsignedShort(i10);
        int i18 = i10;
        while (readUnsignedShort2 > 0) {
            String readUTF83 = readUTF8(i18 + 2, cArr);
            if ("Code".equals(readUTF83)) {
                if ((context.b & 1) == 0) {
                    i2 = i17;
                    i3 = i16;
                    i4 = i15;
                    i5 = i14;
                    i6 = i13;
                    i7 = i12;
                    i9 = readUnsignedShort;
                    attribute = attribute2;
                    i8 = i18 + 8;
                }
                attribute = attribute2;
                i2 = i17;
                i3 = i16;
                i4 = i15;
                i5 = i14;
                i6 = i13;
                i7 = i12;
                i8 = i11;
                i9 = readUnsignedShort;
            } else if ("Exceptions".equals(readUTF83)) {
                strArr = new String[readUnsignedShort(i18 + 8)];
                int i19 = i18 + 10;
                int i20 = 0;
                while (i20 < strArr.length) {
                    strArr[i20] = readClass(i19, cArr);
                    i20++;
                    i19 += 2;
                }
                i2 = i17;
                i3 = i16;
                i4 = i15;
                i5 = i14;
                i6 = i13;
                i7 = i19;
                attribute = attribute2;
                i9 = readUnsignedShort;
                i8 = i11;
            } else if ("Signature".equals(readUTF83)) {
                str = readUTF8(i18 + 8, cArr);
                attribute = attribute2;
                i2 = i17;
                i3 = i16;
                i4 = i15;
                i5 = i14;
                i6 = i13;
                i7 = i12;
                i8 = i11;
                i9 = readUnsignedShort;
            } else if ("Deprecated".equals(readUTF83)) {
                i2 = i17;
                i3 = i16;
                i4 = i15;
                i5 = i14;
                i6 = i13;
                i7 = i12;
                i9 = 131072 | readUnsignedShort;
                attribute = attribute2;
                i8 = i11;
            } else if ("RuntimeVisibleAnnotations".equals(readUTF83)) {
                i2 = i17;
                i3 = i16;
                i4 = i15;
                i5 = i14;
                i6 = i18 + 8;
                i7 = i12;
                attribute = attribute2;
                i9 = readUnsignedShort;
                i8 = i11;
            } else if ("AnnotationDefault".equals(readUTF83)) {
                i2 = i17;
                i3 = i16;
                i4 = i18 + 8;
                i5 = i14;
                i6 = i13;
                i7 = i12;
                attribute = attribute2;
                i9 = readUnsignedShort;
                i8 = i11;
            } else if ("Synthetic".equals(readUTF83)) {
                i2 = i17;
                i3 = i16;
                i4 = i15;
                i5 = i14;
                i6 = i13;
                i7 = i12;
                i9 = 266240 | readUnsignedShort;
                attribute = attribute2;
                i8 = i11;
            } else if ("RuntimeInvisibleAnnotations".equals(readUTF83)) {
                i2 = i17;
                i3 = i16;
                i4 = i15;
                i5 = i18 + 8;
                i6 = i13;
                i7 = i12;
                attribute = attribute2;
                i9 = readUnsignedShort;
                i8 = i11;
            } else if ("RuntimeVisibleParameterAnnotations".equals(readUTF83)) {
                i2 = i17;
                i3 = i18 + 8;
                i4 = i15;
                i5 = i14;
                i6 = i13;
                i7 = i12;
                attribute = attribute2;
                i9 = readUnsignedShort;
                i8 = i11;
            } else if ("RuntimeInvisibleParameterAnnotations".equals(readUTF83)) {
                i2 = i18 + 8;
                i3 = i16;
                i4 = i15;
                i5 = i14;
                i6 = i13;
                i7 = i12;
                i9 = readUnsignedShort;
                attribute = attribute2;
                i8 = i11;
            } else {
                attribute = a(context.a, readUTF83, i18 + 8, readInt(i18 + 4), cArr, -1, null);
                if (attribute != null) {
                    attribute.a = attribute2;
                    i2 = i17;
                    i3 = i16;
                    i4 = i15;
                    i5 = i14;
                    i6 = i13;
                    i7 = i12;
                    i8 = i11;
                    i9 = readUnsignedShort;
                }
                attribute = attribute2;
                i2 = i17;
                i3 = i16;
                i4 = i15;
                i5 = i14;
                i6 = i13;
                i7 = i12;
                i8 = i11;
                i9 = readUnsignedShort;
            }
            readUnsignedShort2--;
            i14 = i5;
            i13 = i6;
            i12 = i7;
            i11 = i8;
            readUnsignedShort = i9;
            i18 += readInt(i18 + 4) + 6;
            i16 = i3;
            i15 = i4;
            attribute2 = attribute;
            i17 = i2;
        }
        int i21 = i18 + 2;
        MethodVisitor visitMethod = classVisitor.visitMethod(readUnsignedShort, readUTF8, readUTF82, str, strArr);
        if (visitMethod == null) {
            return i21;
        }
        if (visitMethod instanceof MethodWriter) {
            MethodWriter methodWriter = (MethodWriter) visitMethod;
            if (methodWriter.b.M == this && str == methodWriter.g) {
                boolean z = false;
                if (strArr == null) {
                    z = methodWriter.j == 0;
                } else if (strArr.length == methodWriter.j) {
                    z = true;
                    int length = strArr.length - 1;
                    while (true) {
                        if (length < 0) {
                            break;
                        }
                        i12 -= 2;
                        if (methodWriter.k[length] != readUnsignedShort(i12)) {
                            z = false;
                            break;
                        }
                        length--;
                    }
                }
                if (z) {
                    methodWriter.h = i10;
                    methodWriter.i = i21 - i10;
                    return i21;
                }
            }
        }
        if (i15 != 0) {
            AnnotationVisitor visitAnnotationDefault = visitMethod.visitAnnotationDefault();
            a(i15, cArr, (String) null, visitAnnotationDefault);
            if (visitAnnotationDefault != null) {
                visitAnnotationDefault.visitEnd();
            }
        }
        if (i13 != 0) {
            int i22 = i13 + 2;
            for (int readUnsignedShort3 = readUnsignedShort(i13); readUnsignedShort3 > 0; readUnsignedShort3--) {
                i22 = a(i22 + 2, cArr, true, visitMethod.visitAnnotation(readUTF8(i22, cArr), true));
            }
        }
        if (i14 != 0) {
            int i23 = i14 + 2;
            for (int readUnsignedShort4 = readUnsignedShort(i14); readUnsignedShort4 > 0; readUnsignedShort4--) {
                i23 = a(i23 + 2, cArr, true, visitMethod.visitAnnotation(readUTF8(i23, cArr), false));
            }
        }
        if (i16 != 0) {
            a(i16, readUTF82, cArr, true, visitMethod);
        }
        if (i17 != 0) {
            a(i17, readUTF82, cArr, false, visitMethod);
        }
        while (attribute2 != null) {
            Attribute attribute3 = attribute2.a;
            attribute2.a = null;
            visitMethod.visitAttribute(attribute2);
            attribute2 = attribute3;
        }
        if (i11 != 0) {
            context.e = readUnsignedShort;
            context.f = readUTF8;
            context.g = readUTF82;
            visitMethod.visitCode();
            a(visitMethod, context, i11);
        }
        visitMethod.visitEnd();
        return i21;
    }

    /* access modifiers changed from: package-private */
    public void a(ClassWriter classWriter) {
        int i;
        char[] cArr = new char[this.d];
        int length = this.a.length;
        Item[] itemArr = new Item[length];
        int i2 = 1;
        while (i2 < length) {
            int i3 = this.a[i2];
            byte b2 = this.b[i3 - 1];
            Item item = new Item(i2);
            switch (b2) {
                case 1:
                    String str = this.c[i2];
                    if (str == null) {
                        int i4 = this.a[i2];
                        String[] strArr = this.c;
                        str = a(i4 + 2, readUnsignedShort(i4), cArr);
                        strArr[i2] = str;
                    }
                    item.a(b2, str, null, null);
                    i = i2;
                    break;
                case 2:
                case 7:
                case 8:
                case 13:
                case 14:
                case 16:
                case 17:
                default:
                    item.a(b2, readUTF8(i3, cArr), null, null);
                    i = i2;
                    break;
                case 3:
                    item.a(readInt(i3));
                    i = i2;
                    break;
                case 4:
                    item.a(Float.intBitsToFloat(readInt(i3)));
                    i = i2;
                    break;
                case 5:
                    item.a(readLong(i3));
                    i = i2 + 1;
                    break;
                case 6:
                    item.a(Double.longBitsToDouble(readLong(i3)));
                    i = i2 + 1;
                    break;
                case 9:
                case 10:
                case 11:
                    int i5 = this.a[readUnsignedShort(i3 + 2)];
                    item.a(b2, readClass(i3, cArr), readUTF8(i5, cArr), readUTF8(i5 + 2, cArr));
                    i = i2;
                    break;
                case 12:
                    item.a(b2, readUTF8(i3, cArr), readUTF8(i3 + 2, cArr), null);
                    i = i2;
                    break;
                case 15:
                    int i6 = this.a[readUnsignedShort(i3 + 1)];
                    int i7 = this.a[readUnsignedShort(i6 + 2)];
                    item.a(readByte(i3) + 20, readClass(i6, cArr), readUTF8(i7, cArr), readUTF8(i7 + 2, cArr));
                    i = i2;
                    break;
                case 18:
                    if (classWriter.A == null) {
                        a(classWriter, itemArr, cArr);
                    }
                    int i8 = this.a[readUnsignedShort(i3 + 2)];
                    item.a(readUTF8(i8, cArr), readUTF8(i8 + 2, cArr), readUnsignedShort(i3));
                    i = i2;
                    break;
            }
            int length2 = item.j % itemArr.length;
            item.k = itemArr[length2];
            itemArr[length2] = item;
            i2 = i + 1;
        }
        int i9 = this.a[1] - 1;
        classWriter.d.putByteArray(this.b, i9, this.header - i9);
        classWriter.e = itemArr;
        classWriter.f = (int) (0.75d * ((double) length));
        classWriter.c = length;
    }

    public void accept(ClassVisitor classVisitor, int i) {
        accept(classVisitor, new Attribute[0], i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int
     arg types: [int, char[], int, org.objectweb.asm.AnnotationVisitor]
     candidates:
      org.objectweb.asm.ClassReader.a(int, char[], java.lang.String, org.objectweb.asm.AnnotationVisitor):int
      org.objectweb.asm.ClassReader.a(int, char[], boolean, org.objectweb.asm.AnnotationVisitor):int */
    public void accept(ClassVisitor classVisitor, Attribute[] attributeArr, int i) {
        Attribute a2;
        int i2;
        int i3;
        int i4;
        String str;
        String str2;
        String str3;
        String str4;
        int i5;
        String str5;
        String str6;
        String str7;
        String str8;
        int i6 = this.header;
        char[] cArr = new char[this.d];
        Context context = new Context();
        context.a = attributeArr;
        context.b = i;
        context.c = cArr;
        int readUnsignedShort = readUnsignedShort(i6);
        String readClass = readClass(i6 + 2, cArr);
        String readClass2 = readClass(i6 + 4, cArr);
        String[] strArr = new String[readUnsignedShort(i6 + 6)];
        int i7 = i6 + 8;
        for (int i8 = 0; i8 < strArr.length; i8++) {
            strArr[i8] = readClass(i7, cArr);
            i7 += 2;
        }
        String str9 = null;
        String str10 = null;
        String str11 = null;
        String str12 = null;
        String str13 = null;
        String str14 = null;
        int i9 = 0;
        int i10 = 0;
        int i11 = 0;
        Attribute attribute = null;
        int a3 = a();
        int readUnsignedShort2 = readUnsignedShort(a3);
        int i12 = a3;
        while (readUnsignedShort2 > 0) {
            String readUTF8 = readUTF8(i12 + 2, cArr);
            if ("SourceFile".equals(readUTF8)) {
                i2 = i11;
                i3 = i10;
                i4 = i9;
                str = str14;
                str2 = str13;
                str3 = str12;
                i5 = readUnsignedShort;
                str5 = str11;
                a2 = attribute;
                str4 = str9;
                str6 = readUTF8(i12 + 8, cArr);
            } else if ("InnerClasses".equals(readUTF8)) {
                i2 = i12 + 8;
                i3 = i10;
                i4 = i9;
                str = str14;
                str2 = str13;
                str3 = str12;
                i5 = readUnsignedShort;
                str5 = str11;
                a2 = attribute;
                str4 = str9;
                str6 = str10;
            } else if ("EnclosingMethod".equals(readUTF8)) {
                String readClass3 = readClass(i12 + 8, cArr);
                int readUnsignedShort3 = readUnsignedShort(i12 + 10);
                if (readUnsignedShort3 != 0) {
                    str8 = readUTF8(this.a[readUnsignedShort3], cArr);
                    str7 = readUTF8(this.a[readUnsignedShort3] + 2, cArr);
                } else {
                    str7 = str14;
                    str8 = str13;
                }
                i4 = i9;
                str = str7;
                str2 = str8;
                str3 = readClass3;
                i2 = i11;
                i3 = i10;
                a2 = attribute;
                str4 = str9;
                i5 = readUnsignedShort;
                str5 = str11;
                str6 = str10;
            } else if ("Signature".equals(readUTF8)) {
                a2 = attribute;
                i2 = i11;
                i3 = i10;
                i4 = i9;
                str = str14;
                str2 = str13;
                str3 = str12;
                str4 = readUTF8(i12 + 8, cArr);
                i5 = readUnsignedShort;
                str5 = str11;
                str6 = str10;
            } else if ("RuntimeVisibleAnnotations".equals(readUTF8)) {
                i2 = i11;
                i3 = i10;
                i4 = i12 + 8;
                str = str14;
                str2 = str13;
                str3 = str12;
                a2 = attribute;
                i5 = readUnsignedShort;
                str4 = str9;
                str5 = str11;
                str6 = str10;
            } else if ("Deprecated".equals(readUTF8)) {
                a2 = attribute;
                i2 = i11;
                i3 = i10;
                i4 = i9;
                str = str14;
                str2 = str13;
                str3 = str12;
                str4 = str9;
                i5 = readUnsignedShort | Opcodes.ACC_DEPRECATED;
                str5 = str11;
                str6 = str10;
            } else if ("Synthetic".equals(readUTF8)) {
                a2 = attribute;
                i2 = i11;
                i3 = i10;
                i4 = i9;
                str = str14;
                str2 = str13;
                str3 = str12;
                str4 = str9;
                i5 = readUnsignedShort | 266240;
                str5 = str11;
                str6 = str10;
            } else if ("SourceDebugExtension".equals(readUTF8)) {
                int readInt = readInt(i12 + 4);
                i2 = i11;
                i3 = i10;
                i4 = i9;
                str = str14;
                str2 = str13;
                str3 = str12;
                i5 = readUnsignedShort;
                str5 = a(i12 + 8, readInt, new char[readInt]);
                a2 = attribute;
                str4 = str9;
                str6 = str10;
            } else if ("RuntimeInvisibleAnnotations".equals(readUTF8)) {
                i2 = i11;
                i3 = i12 + 8;
                i4 = i9;
                str = str14;
                str2 = str13;
                str3 = str12;
                a2 = attribute;
                i5 = readUnsignedShort;
                str4 = str9;
                str5 = str11;
                str6 = str10;
            } else if ("BootstrapMethods".equals(readUTF8)) {
                int[] iArr = new int[readUnsignedShort(i12 + 8)];
                int i13 = i12 + 10;
                for (int i14 = 0; i14 < iArr.length; i14++) {
                    iArr[i14] = i13;
                    i13 += (readUnsignedShort(i13 + 2) + 2) << 1;
                }
                context.d = iArr;
                a2 = attribute;
                i2 = i11;
                i3 = i10;
                i4 = i9;
                str = str14;
                str2 = str13;
                str3 = str12;
                str4 = str9;
                i5 = readUnsignedShort;
                str5 = str11;
                str6 = str10;
            } else {
                a2 = a(attributeArr, readUTF8, i12 + 8, readInt(i12 + 4), cArr, -1, null);
                if (a2 != null) {
                    a2.a = attribute;
                    i2 = i11;
                    i3 = i10;
                    i4 = i9;
                    str = str14;
                    str2 = str13;
                    str3 = str12;
                    str4 = str9;
                    str6 = str10;
                    i5 = readUnsignedShort;
                    str5 = str11;
                } else {
                    a2 = attribute;
                    i2 = i11;
                    i3 = i10;
                    i4 = i9;
                    str = str14;
                    str2 = str13;
                    str3 = str12;
                    str4 = str9;
                    i5 = readUnsignedShort;
                    str5 = str11;
                    str6 = str10;
                }
            }
            readUnsignedShort2--;
            str14 = str;
            str13 = str2;
            str12 = str3;
            str11 = str5;
            str10 = str6;
            i12 += readInt(i12 + 4) + 6;
            i10 = i3;
            i9 = i4;
            str9 = str4;
            readUnsignedShort = i5;
            attribute = a2;
            i11 = i2;
        }
        classVisitor.visit(readInt(this.a[1] - 7), readUnsignedShort, readClass, str9, readClass2, strArr);
        if ((i & 2) == 0 && !(str10 == null && str11 == null)) {
            classVisitor.visitSource(str10, str11);
        }
        if (str12 != null) {
            classVisitor.visitOuterClass(str12, str13, str14);
        }
        if (i9 != 0) {
            int i15 = i9 + 2;
            for (int readUnsignedShort4 = readUnsignedShort(i9); readUnsignedShort4 > 0; readUnsignedShort4--) {
                i15 = a(i15 + 2, cArr, true, classVisitor.visitAnnotation(readUTF8(i15, cArr), true));
            }
        }
        if (i10 != 0) {
            int i16 = i10 + 2;
            for (int readUnsignedShort5 = readUnsignedShort(i10); readUnsignedShort5 > 0; readUnsignedShort5--) {
                i16 = a(i16 + 2, cArr, true, classVisitor.visitAnnotation(readUTF8(i16, cArr), false));
            }
        }
        while (attribute != null) {
            Attribute attribute2 = attribute.a;
            attribute.a = null;
            classVisitor.visitAttribute(attribute);
            attribute = attribute2;
        }
        if (i11 != 0) {
            int i17 = i11 + 2;
            for (int readUnsignedShort6 = readUnsignedShort(i11); readUnsignedShort6 > 0; readUnsignedShort6--) {
                classVisitor.visitInnerClass(readClass(i17, cArr), readClass(i17 + 2, cArr), readUTF8(i17 + 4, cArr), readUnsignedShort(i17 + 6));
                i17 += 8;
            }
        }
        int length = (strArr.length * 2) + this.header + 10;
        for (int readUnsignedShort7 = readUnsignedShort(length - 2); readUnsignedShort7 > 0; readUnsignedShort7--) {
            length = a(classVisitor, context, length);
        }
        int i18 = length + 2;
        for (int readUnsignedShort8 = readUnsignedShort(i18 - 2); readUnsignedShort8 > 0; readUnsignedShort8--) {
            i18 = b(classVisitor, context, i18);
        }
        classVisitor.visitEnd();
    }

    public int getAccess() {
        return readUnsignedShort(this.header);
    }

    public String getClassName() {
        return readClass(this.header + 2, new char[this.d]);
    }

    public String[] getInterfaces() {
        int i = this.header + 6;
        int readUnsignedShort = readUnsignedShort(i);
        String[] strArr = new String[readUnsignedShort];
        if (readUnsignedShort > 0) {
            char[] cArr = new char[this.d];
            for (int i2 = 0; i2 < readUnsignedShort; i2++) {
                i += 2;
                strArr[i2] = readClass(i, cArr);
            }
        }
        return strArr;
    }

    public int getItem(int i) {
        return this.a[i];
    }

    public int getItemCount() {
        return this.a.length;
    }

    public int getMaxStringLength() {
        return this.d;
    }

    public String getSuperName() {
        return readClass(this.header + 4, new char[this.d]);
    }

    public int readByte(int i) {
        return this.b[i] & 255;
    }

    public String readClass(int i, char[] cArr) {
        return readUTF8(this.a[readUnsignedShort(i)], cArr);
    }

    public Object readConst(int i, char[] cArr) {
        int i2 = this.a[i];
        switch (this.b[i2 - 1]) {
            case 3:
                return new Integer(readInt(i2));
            case 4:
                return new Float(Float.intBitsToFloat(readInt(i2)));
            case 5:
                return new Long(readLong(i2));
            case 6:
                return new Double(Double.longBitsToDouble(readLong(i2)));
            case 7:
                return Type.getObjectType(readUTF8(i2, cArr));
            case 8:
                return readUTF8(i2, cArr);
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            default:
                int readByte = readByte(i2);
                int[] iArr = this.a;
                int i3 = iArr[readUnsignedShort(i2 + 1)];
                String readClass = readClass(i3, cArr);
                int i4 = iArr[readUnsignedShort(i3 + 2)];
                return new Handle(readByte, readClass, readUTF8(i4, cArr), readUTF8(i4 + 2, cArr));
            case 16:
                return Type.getMethodType(readUTF8(i2, cArr));
        }
    }

    public int readInt(int i) {
        byte[] bArr = this.b;
        return (bArr[i + 3] & 255) | ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << 16) | ((bArr[i + 2] & 255) << 8);
    }

    /* access modifiers changed from: protected */
    public Label readLabel(int i, Label[] labelArr) {
        if (labelArr[i] == null) {
            labelArr[i] = new Label();
        }
        return labelArr[i];
    }

    public long readLong(int i) {
        return (((long) readInt(i)) << 32) | (((long) readInt(i + 4)) & 4294967295L);
    }

    public short readShort(int i) {
        byte[] bArr = this.b;
        return (short) ((bArr[i + 1] & 255) | ((bArr[i] & 255) << 8));
    }

    public String readUTF8(int i, char[] cArr) {
        int readUnsignedShort = readUnsignedShort(i);
        if (i == 0 || readUnsignedShort == 0) {
            return null;
        }
        String str = this.c[readUnsignedShort];
        if (str != null) {
            return str;
        }
        int i2 = this.a[readUnsignedShort];
        String[] strArr = this.c;
        String a2 = a(i2 + 2, readUnsignedShort(i2), cArr);
        strArr[readUnsignedShort] = a2;
        return a2;
    }

    public int readUnsignedShort(int i) {
        byte[] bArr = this.b;
        return (bArr[i + 1] & 255) | ((bArr[i] & 255) << 8);
    }
}
