package org.objectweb.asm;

import com.sg.pak.PAK_ASSETS;

public class ClassWriter extends ClassVisitor {
    public static final int COMPUTE_FRAMES = 2;
    public static final int COMPUTE_MAXS = 1;
    static final byte[] a;
    ByteVector A;
    FieldWriter B;
    FieldWriter C;
    MethodWriter D;
    MethodWriter E;
    private short G;
    Item[] H;
    String I;
    private boolean J;
    private boolean K;
    boolean L;
    ClassReader M;
    int b;
    int c;
    final ByteVector d;
    Item[] e;
    int f;
    final Item g;
    final Item h;
    final Item i;
    final Item j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;
    private int[] p;
    private int q;
    private ByteVector r;
    private int s;
    private int t;
    private AnnotationWriter u;
    private AnnotationWriter v;
    private Attribute w;
    private int x;
    private ByteVector y;
    int z;

    static {
        byte[] bArr = new byte[PAK_ASSETS.IMG_JUXINGNENGLIANGTA];
        for (int i2 = 0; i2 < bArr.length; i2++) {
            bArr[i2] = (byte) ("AAAAAAAAAAAAAAAABCLMMDDDDDEEEEEEEEEEEEEEEEEEEEAAAAAAAADDDDDEEEEEEEEEEEEEEEEEEEEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANAAAAAAAAAAAAAAAAAAAAJJJJJJJJJJJJJJJJDOPAAAAAAGGGGGGGHIFBFAAFFAARQJJKKJJJJJJJJJJJJJJJJJJ".charAt(i2) - 'A');
        }
        a = bArr;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ClassWriter(int i2) {
        super(Opcodes.ASM4);
        boolean z2 = true;
        this.c = 1;
        this.d = new ByteVector();
        this.e = new Item[256];
        this.f = (int) (0.75d * ((double) this.e.length));
        this.g = new Item();
        this.h = new Item();
        this.i = new Item();
        this.j = new Item();
        this.K = (i2 & 1) != 0;
        this.J = (i2 & 2) == 0 ? false : z2;
    }

    public ClassWriter(ClassReader classReader, int i2) {
        this(i2);
        classReader.a(this);
        this.M = classReader;
    }

    private Item a(Item item) {
        Item item2 = this.e[item.j % this.e.length];
        while (item2 != null && (item2.b != item.b || !item.a(item2))) {
            item2 = item2.k;
        }
        return item2;
    }

    private void a(int i2, int i3, int i4) {
        this.d.b(i2, i3).putShort(i4);
    }

    private Item b(String str) {
        this.h.a(8, str, null, null);
        Item a2 = a(this.h);
        if (a2 != null) {
            return a2;
        }
        this.d.b(8, newUTF8(str));
        int i2 = this.c;
        this.c = i2 + 1;
        Item item = new Item(i2, this.h);
        b(item);
        return item;
    }

    private void b(int i2, int i3, int i4) {
        this.d.a(i2, i3).putShort(i4);
    }

    private void b(Item item) {
        if (this.c + this.G > this.f) {
            int length = this.e.length;
            int i2 = (length * 2) + 1;
            Item[] itemArr = new Item[i2];
            for (int i3 = length - 1; i3 >= 0; i3--) {
                Item item2 = this.e[i3];
                while (item2 != null) {
                    int length2 = item2.j % itemArr.length;
                    Item item3 = item2.k;
                    item2.k = itemArr[length2];
                    itemArr[length2] = item2;
                    item2 = item3;
                }
            }
            this.e = itemArr;
            this.f = (int) (((double) i2) * 0.75d);
        }
        int length3 = item.j % this.e.length;
        item.k = this.e[length3];
        this.e[length3] = item;
    }

    private Item c(Item item) {
        this.G = (short) (this.G + 1);
        Item item2 = new Item(this.G, this.g);
        b(item2);
        if (this.H == null) {
            this.H = new Item[16];
        }
        if (this.G == this.H.length) {
            Item[] itemArr = new Item[(this.H.length * 2)];
            System.arraycopy(this.H, 0, itemArr, 0, this.H.length);
            this.H = itemArr;
        }
        this.H[this.G] = item2;
        return item2;
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, int i3) {
        this.h.b = 32;
        this.h.d = ((long) i2) | (((long) i3) << 32);
        this.h.j = Integer.MAX_VALUE & (i2 + 32 + i3);
        Item a2 = a(this.h);
        if (a2 == null) {
            String str = this.H[i2].g;
            String str2 = this.H[i3].g;
            this.h.c = c(getCommonSuperClass(str, str2));
            a2 = new Item(0, this.h);
            b(a2);
        }
        return a2.c;
    }

    /* access modifiers changed from: package-private */
    public int a(String str, int i2) {
        this.g.b = 31;
        this.g.c = i2;
        this.g.g = str;
        this.g.j = Integer.MAX_VALUE & (str.hashCode() + 31 + i2);
        Item a2 = a(this.g);
        if (a2 == null) {
            a2 = c(this.g);
        }
        return a2.a;
    }

    /* access modifiers changed from: package-private */
    public Item a(double d2) {
        this.g.a(d2);
        Item a2 = a(this.g);
        if (a2 != null) {
            return a2;
        }
        this.d.putByte(6).putLong(this.g.d);
        Item item = new Item(this.c, this.g);
        this.c += 2;
        b(item);
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(float f2) {
        this.g.a(f2);
        Item a2 = a(this.g);
        if (a2 != null) {
            return a2;
        }
        this.d.putByte(4).putInt(this.g.c);
        int i2 = this.c;
        this.c = i2 + 1;
        Item item = new Item(i2, this.g);
        b(item);
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(int i2) {
        this.g.a(i2);
        Item a2 = a(this.g);
        if (a2 != null) {
            return a2;
        }
        this.d.putByte(3).putInt(i2);
        int i3 = this.c;
        this.c = i3 + 1;
        Item item = new Item(i3, this.g);
        b(item);
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(int i2, String str, String str2, String str3) {
        this.j.a(i2 + 20, str, str2, str3);
        Item a2 = a(this.j);
        if (a2 != null) {
            return a2;
        }
        if (i2 <= 4) {
            b(15, i2, newField(str, str2, str3));
        } else {
            b(15, i2, newMethod(str, str2, str3, i2 == 9));
        }
        int i3 = this.c;
        this.c = i3 + 1;
        Item item = new Item(i3, this.j);
        b(item);
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(long j2) {
        this.g.a(j2);
        Item a2 = a(this.g);
        if (a2 != null) {
            return a2;
        }
        this.d.putByte(5).putLong(j2);
        Item item = new Item(this.c, this.g);
        this.c += 2;
        b(item);
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(Object obj) {
        if (obj instanceof Integer) {
            return a(((Integer) obj).intValue());
        }
        if (obj instanceof Byte) {
            return a(((Byte) obj).intValue());
        }
        if (obj instanceof Character) {
            return a((int) ((Character) obj).charValue());
        }
        if (obj instanceof Short) {
            return a(((Short) obj).intValue());
        }
        if (obj instanceof Boolean) {
            return a(((Boolean) obj).booleanValue() ? 1 : 0);
        } else if (obj instanceof Float) {
            return a(((Float) obj).floatValue());
        } else {
            if (obj instanceof Long) {
                return a(((Long) obj).longValue());
            }
            if (obj instanceof Double) {
                return a(((Double) obj).doubleValue());
            }
            if (obj instanceof String) {
                return b((String) obj);
            }
            if (obj instanceof Type) {
                Type type = (Type) obj;
                int sort = type.getSort();
                return sort == 10 ? a(type.getInternalName()) : sort == 11 ? m0c(type.getDescriptor()) : a(type.getDescriptor());
            } else if (obj instanceof Handle) {
                Handle handle = (Handle) obj;
                return a(handle.a, handle.b, handle.c, handle.d);
            } else {
                throw new IllegalArgumentException(new StringBuffer().append("value ").append(obj).toString());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Item a(String str) {
        this.h.a(7, str, null, null);
        Item a2 = a(this.h);
        if (a2 != null) {
            return a2;
        }
        this.d.b(7, newUTF8(str));
        int i2 = this.c;
        this.c = i2 + 1;
        Item item = new Item(i2, this.h);
        b(item);
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(String str, String str2) {
        this.h.a(12, str, str2, null);
        Item a2 = a(this.h);
        if (a2 != null) {
            return a2;
        }
        a(12, newUTF8(str), newUTF8(str2));
        int i2 = this.c;
        this.c = i2 + 1;
        Item item = new Item(i2, this.h);
        b(item);
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(String str, String str2, String str3) {
        this.i.a(9, str, str2, str3);
        Item a2 = a(this.i);
        if (a2 != null) {
            return a2;
        }
        a(9, newClass(str), newNameType(str2, str3));
        int i2 = this.c;
        this.c = i2 + 1;
        Item item = new Item(i2, this.i);
        b(item);
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(String str, String str2, String str3, boolean z2) {
        int i2 = z2 ? 11 : 10;
        this.i.a(i2, str, str2, str3);
        Item a2 = a(this.i);
        if (a2 != null) {
            return a2;
        }
        a(i2, newClass(str), newNameType(str2, str3));
        int i3 = this.c;
        this.c = i3 + 1;
        Item item = new Item(i3, this.i);
        b(item);
        return item;
    }

    /* access modifiers changed from: package-private */
    public Item a(String str, String str2, Handle handle, Object... objArr) {
        int i2;
        ByteVector byteVector = this.A;
        if (byteVector == null) {
            byteVector = new ByteVector();
            this.A = byteVector;
        }
        int i3 = byteVector.b;
        int hashCode = handle.hashCode();
        byteVector.putShort(newHandle(handle.a, handle.b, handle.c, handle.d));
        byteVector.putShort(r5);
        int i4 = hashCode;
        for (Object obj : objArr) {
            i4 ^= obj.hashCode();
            byteVector.putShort(newConst(obj));
        }
        byte[] bArr = byteVector.a;
        int i5 = (r5 + 2) << 1;
        int i6 = i4 & Integer.MAX_VALUE;
        Item item = this.e[i6 % this.e.length];
        loop1:
        while (item != null) {
            if (item.b == 33 && item.j == i6) {
                int i7 = item.c;
                int i8 = 0;
                while (i8 < i5) {
                    if (bArr[i3 + i8] != bArr[i7 + i8]) {
                        item = item.k;
                    } else {
                        i8++;
                    }
                }
                break loop1;
            }
            item = item.k;
        }
        if (item != null) {
            int i9 = item.a;
            byteVector.b = i3;
            i2 = i9;
        } else {
            i2 = this.z;
            this.z = i2 + 1;
            Item item2 = new Item(i2);
            item2.a(i3, i6);
            b(item2);
        }
        this.i.a(str, str2, i2);
        Item a2 = a(this.i);
        if (a2 != null) {
            return a2;
        }
        a(18, i2, newNameType(str, str2));
        int i10 = this.c;
        this.c = i10 + 1;
        Item item3 = new Item(i10, this.i);
        b(item3);
        return item3;
    }

    /* access modifiers changed from: package-private */
    public int c(String str) {
        this.g.a(30, str, null, null);
        Item a2 = a(this.g);
        if (a2 == null) {
            a2 = c(this.g);
        }
        return a2.a;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c  reason: collision with other method in class */
    public Item m0c(String str) {
        this.h.a(16, str, null, null);
        Item a2 = a(this.h);
        if (a2 != null) {
            return a2;
        }
        this.d.b(16, newUTF8(str));
        int i2 = this.c;
        this.c = i2 + 1;
        Item item = new Item(i2, this.h);
        b(item);
        return item;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* access modifiers changed from: protected */
    public String getCommonSuperClass(String str, String str2) {
        ClassLoader classLoader = getClass().getClassLoader();
        try {
            Class<?> cls = Class.forName(str.replace('/', '.'), false, classLoader);
            Class<?> cls2 = Class.forName(str2.replace('/', '.'), false, classLoader);
            if (cls.isAssignableFrom(cls2)) {
                return str;
            }
            if (cls2.isAssignableFrom(cls)) {
                return str2;
            }
            if (cls.isInterface() || cls2.isInterface()) {
                return "java/lang/Object";
            }
            do {
                cls = cls.getSuperclass();
            } while (!cls.isAssignableFrom(cls2));
            return cls.getName().replace('.', '/');
        } catch (Exception e2) {
            throw new RuntimeException(e2.toString());
        }
    }

    public int newClass(String str) {
        return a(str).a;
    }

    public int newConst(Object obj) {
        return a(obj).a;
    }

    public int newField(String str, String str2, String str3) {
        return a(str, str2, str3).a;
    }

    public int newHandle(int i2, String str, String str2, String str3) {
        return a(i2, str, str2, str3).a;
    }

    public int newInvokeDynamic(String str, String str2, Handle handle, Object... objArr) {
        return a(str, str2, handle, objArr).a;
    }

    public int newMethod(String str, String str2, String str3, boolean z2) {
        return a(str, str2, str3, z2).a;
    }

    public int newMethodType(String str) {
        return m0c(str).a;
    }

    public int newNameType(String str, String str2) {
        return a(str, str2).a;
    }

    public int newUTF8(String str) {
        this.g.a(1, str, null, null);
        Item a2 = a(this.g);
        if (a2 == null) {
            this.d.putByte(1).putUTF8(str);
            int i2 = this.c;
            this.c = i2 + 1;
            a2 = new Item(i2, this.g);
            b(a2);
        }
        return a2.a;
    }

    public byte[] toByteArray() {
        int i2;
        int i3;
        if (this.c > 65535) {
            throw new RuntimeException("Class file too large!");
        }
        int i4 = (this.o * 2) + 24;
        FieldWriter fieldWriter = this.B;
        int i5 = 0;
        while (fieldWriter != null) {
            i4 += fieldWriter.a();
            fieldWriter = (FieldWriter) fieldWriter.fv;
            i5++;
        }
        MethodWriter methodWriter = this.D;
        int i6 = 0;
        while (methodWriter != null) {
            i4 += methodWriter.a();
            methodWriter = (MethodWriter) methodWriter.mv;
            i6++;
        }
        if (this.A != null) {
            i2 = 1;
            i4 += this.A.b + 8;
            newUTF8("BootstrapMethods");
        } else {
            i2 = 0;
        }
        if (this.m != 0) {
            i2++;
            i4 += 8;
            newUTF8("Signature");
        }
        if (this.q != 0) {
            i2++;
            i4 += 8;
            newUTF8("SourceFile");
        }
        if (this.r != null) {
            i2++;
            i4 += this.r.b + 4;
            newUTF8("SourceDebugExtension");
        }
        if (this.s != 0) {
            i2++;
            i4 += 10;
            newUTF8("EnclosingMethod");
        }
        if ((this.k & Opcodes.ACC_DEPRECATED) != 0) {
            i2++;
            i4 += 6;
            newUTF8("Deprecated");
        }
        if ((this.k & 4096) != 0 && ((this.b & 65535) < 49 || (this.k & Opcodes.ASM4) != 0)) {
            i2++;
            i4 += 6;
            newUTF8("Synthetic");
        }
        if (this.y != null) {
            i2++;
            i4 += this.y.b + 8;
            newUTF8("InnerClasses");
        }
        if (this.u != null) {
            i2++;
            i4 += this.u.a() + 8;
            newUTF8("RuntimeVisibleAnnotations");
        }
        if (this.v != null) {
            i2++;
            i4 += this.v.a() + 8;
            newUTF8("RuntimeInvisibleAnnotations");
        }
        int i7 = i4;
        if (this.w != null) {
            int a2 = i2 + this.w.a();
            i7 += this.w.a(this, null, 0, -1, -1);
            i3 = a2;
        } else {
            i3 = i2;
        }
        ByteVector byteVector = new ByteVector(this.d.b + i7);
        byteVector.putInt(-889275714).putInt(this.b);
        byteVector.putShort(this.c).putByteArray(this.d.a, 0, this.d.b);
        byteVector.putShort(((393216 | ((this.k & Opcodes.ASM4) / 64)) ^ -1) & this.k).putShort(this.l).putShort(this.n);
        byteVector.putShort(this.o);
        for (int i8 = 0; i8 < this.o; i8++) {
            byteVector.putShort(this.p[i8]);
        }
        byteVector.putShort(i5);
        for (FieldWriter fieldWriter2 = this.B; fieldWriter2 != null; fieldWriter2 = (FieldWriter) fieldWriter2.fv) {
            fieldWriter2.a(byteVector);
        }
        byteVector.putShort(i6);
        for (MethodWriter methodWriter2 = this.D; methodWriter2 != null; methodWriter2 = (MethodWriter) methodWriter2.mv) {
            methodWriter2.a(byteVector);
        }
        byteVector.putShort(i3);
        if (this.A != null) {
            byteVector.putShort(newUTF8("BootstrapMethods"));
            byteVector.putInt(this.A.b + 2).putShort(this.z);
            byteVector.putByteArray(this.A.a, 0, this.A.b);
        }
        if (this.m != 0) {
            byteVector.putShort(newUTF8("Signature")).putInt(2).putShort(this.m);
        }
        if (this.q != 0) {
            byteVector.putShort(newUTF8("SourceFile")).putInt(2).putShort(this.q);
        }
        if (this.r != null) {
            int i9 = this.r.b - 2;
            byteVector.putShort(newUTF8("SourceDebugExtension")).putInt(i9);
            byteVector.putByteArray(this.r.a, 2, i9);
        }
        if (this.s != 0) {
            byteVector.putShort(newUTF8("EnclosingMethod")).putInt(4);
            byteVector.putShort(this.s).putShort(this.t);
        }
        if ((this.k & Opcodes.ACC_DEPRECATED) != 0) {
            byteVector.putShort(newUTF8("Deprecated")).putInt(0);
        }
        if ((this.k & 4096) != 0 && ((this.b & 65535) < 49 || (this.k & Opcodes.ASM4) != 0)) {
            byteVector.putShort(newUTF8("Synthetic")).putInt(0);
        }
        if (this.y != null) {
            byteVector.putShort(newUTF8("InnerClasses"));
            byteVector.putInt(this.y.b + 2).putShort(this.x);
            byteVector.putByteArray(this.y.a, 0, this.y.b);
        }
        if (this.u != null) {
            byteVector.putShort(newUTF8("RuntimeVisibleAnnotations"));
            this.u.a(byteVector);
        }
        if (this.v != null) {
            byteVector.putShort(newUTF8("RuntimeInvisibleAnnotations"));
            this.v.a(byteVector);
        }
        if (this.w != null) {
            this.w.a(this, null, 0, -1, -1, byteVector);
        }
        if (!this.L) {
            return byteVector.a;
        }
        this.u = null;
        this.v = null;
        this.w = null;
        this.x = 0;
        this.y = null;
        this.z = 0;
        this.A = null;
        this.B = null;
        this.C = null;
        this.D = null;
        this.E = null;
        this.K = false;
        this.J = true;
        this.L = false;
        new ClassReader(byteVector.a).accept(this, 4);
        return toByteArray();
    }

    public final void visit(int i2, int i3, String str, String str2, String str3, String[] strArr) {
        this.b = i2;
        this.k = i3;
        this.l = newClass(str);
        this.I = str;
        if (str2 != null) {
            this.m = newUTF8(str2);
        }
        this.n = str3 == null ? 0 : newClass(str3);
        if (strArr != null && strArr.length > 0) {
            this.o = strArr.length;
            this.p = new int[this.o];
            for (int i4 = 0; i4 < this.o; i4++) {
                this.p[i4] = newClass(strArr[i4]);
            }
        }
    }

    public final AnnotationVisitor visitAnnotation(String str, boolean z2) {
        ByteVector byteVector = new ByteVector();
        byteVector.putShort(newUTF8(str)).putShort(0);
        AnnotationWriter annotationWriter = new AnnotationWriter(this, true, byteVector, byteVector, 2);
        if (z2) {
            annotationWriter.g = this.u;
            this.u = annotationWriter;
        } else {
            annotationWriter.g = this.v;
            this.v = annotationWriter;
        }
        return annotationWriter;
    }

    public final void visitAttribute(Attribute attribute) {
        attribute.a = this.w;
        this.w = attribute;
    }

    public final void visitEnd() {
    }

    public final FieldVisitor visitField(int i2, String str, String str2, String str3, Object obj) {
        return new FieldWriter(this, i2, str, str2, str3, obj);
    }

    public final void visitInnerClass(String str, String str2, String str3, int i2) {
        int i3 = 0;
        if (this.y == null) {
            this.y = new ByteVector();
        }
        this.x++;
        this.y.putShort(str == null ? 0 : newClass(str));
        this.y.putShort(str2 == null ? 0 : newClass(str2));
        ByteVector byteVector = this.y;
        if (str3 != null) {
            i3 = newUTF8(str3);
        }
        byteVector.putShort(i3);
        this.y.putShort(i2);
    }

    public final MethodVisitor visitMethod(int i2, String str, String str2, String str3, String[] strArr) {
        return new MethodWriter(this, i2, str, str2, str3, strArr, this.K, this.J);
    }

    public final void visitOuterClass(String str, String str2, String str3) {
        this.s = newClass(str);
        if (str2 != null && str3 != null) {
            this.t = newNameType(str2, str3);
        }
    }

    public final void visitSource(String str, String str2) {
        if (str != null) {
            this.q = newUTF8(str);
        }
        if (str2 != null) {
            this.r = new ByteVector().putUTF8(str2);
        }
    }
}
