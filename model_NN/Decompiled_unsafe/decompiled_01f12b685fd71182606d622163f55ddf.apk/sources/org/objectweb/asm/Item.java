package org.objectweb.asm;

final class Item {
    int a;
    int b;
    int c;
    long d;
    String g;
    String h;
    String i;
    int j;
    Item k;

    Item() {
    }

    Item(int i2) {
        this.a = i2;
    }

    Item(int i2, Item item) {
        this.a = i2;
        this.b = item.b;
        this.c = item.c;
        this.d = item.d;
        this.g = item.g;
        this.h = item.h;
        this.i = item.i;
        this.j = item.j;
    }

    /* access modifiers changed from: package-private */
    public void a(double d2) {
        this.b = 6;
        this.d = Double.doubleToRawLongBits(d2);
        this.j = Integer.MAX_VALUE & (this.b + ((int) d2));
    }

    /* access modifiers changed from: package-private */
    public void a(float f) {
        this.b = 4;
        this.c = Float.floatToRawIntBits(f);
        this.j = Integer.MAX_VALUE & (this.b + ((int) f));
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.b = 3;
        this.c = i2;
        this.j = Integer.MAX_VALUE & (this.b + i2);
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3) {
        this.b = 33;
        this.c = i2;
        this.j = i3;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, String str, String str2, String str3) {
        this.b = i2;
        this.g = str;
        this.h = str2;
        this.i = str3;
        switch (i2) {
            case 1:
            case 7:
            case 8:
            case 16:
            case 30:
                this.j = (str.hashCode() + i2) & Integer.MAX_VALUE;
                return;
            case 12:
                this.j = ((str.hashCode() * str2.hashCode()) + i2) & Integer.MAX_VALUE;
                return;
            default:
                this.j = ((str.hashCode() * str2.hashCode() * str3.hashCode()) + i2) & Integer.MAX_VALUE;
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(long j2) {
        this.b = 5;
        this.d = j2;
        this.j = Integer.MAX_VALUE & (this.b + ((int) j2));
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2, int i2) {
        this.b = 18;
        this.d = (long) i2;
        this.g = str;
        this.h = str2;
        this.j = Integer.MAX_VALUE & ((this.g.hashCode() * i2 * this.h.hashCode()) + 18);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Item item) {
        switch (this.b) {
            case 1:
            case 7:
            case 8:
            case 16:
            case 30:
                return item.g.equals(this.g);
            case 3:
            case 4:
                return item.c == this.c;
            case 5:
            case 6:
            case 32:
                return item.d == this.d;
            case 12:
                return item.g.equals(this.g) && item.h.equals(this.h);
            case 18:
                return item.d == this.d && item.g.equals(this.g) && item.h.equals(this.h);
            case 31:
                return item.c == this.c && item.g.equals(this.g);
            default:
                return item.g.equals(this.g) && item.h.equals(this.h) && item.i.equals(this.i);
        }
    }
}
