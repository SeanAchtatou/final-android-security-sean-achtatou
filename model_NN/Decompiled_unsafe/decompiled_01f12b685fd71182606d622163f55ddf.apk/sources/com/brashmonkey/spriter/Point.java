package com.brashmonkey.spriter;

import com.kbz.esotericsoftware.spine.Animation;

public class Point {
    public float x;
    public float y;

    public Point() {
        this(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    }

    public Point(Point point) {
        this(point.x, point.y);
    }

    public Point(float x2, float y2) {
        set(x2, y2);
    }

    public Point set(float x2, float y2) {
        this.x = x2;
        this.y = y2;
        return this;
    }

    public Point translate(float x2, float y2) {
        return set(this.x + x2, this.y + y2);
    }

    public Point scale(float x2, float y2) {
        return set(this.x * x2, this.y * y2);
    }

    public Point set(Point point) {
        return set(point.x, point.y);
    }

    public Point translate(Point amount) {
        return translate(amount.x, amount.y);
    }

    public Point scale(Point amount) {
        return scale(amount.x, amount.y);
    }

    public Point rotate(float degrees) {
        if (!(this.x == Animation.CurveTimeline.LINEAR && this.y == Animation.CurveTimeline.LINEAR)) {
            float cos = Calculator.cosDeg(degrees);
            float sin = Calculator.sinDeg(degrees);
            this.x = (this.x * cos) - (this.y * sin);
            this.y = (this.x * sin) + (this.y * cos);
        }
        return this;
    }

    public Point copy() {
        return new Point(this.x, this.y);
    }

    public String toString() {
        return "[" + this.x + "," + this.y + "]";
    }
}
