package com.brashmonkey.spriter;

import com.kbz.esotericsoftware.spine.Animation;

public class Curve {
    public final Constraints constraints;
    private float lastCubicSolution;
    public Curve subCurve;
    private Type type;

    public enum Type {
        Instant,
        Linear,
        Quadratic,
        Cubic,
        Quartic,
        Quintic,
        Bezier
    }

    public static Type getType(String name) {
        if (name.equals("instant")) {
            return Type.Instant;
        }
        if (name.equals("quadratic")) {
            return Type.Quadratic;
        }
        if (name.equals("cubic")) {
            return Type.Cubic;
        }
        if (name.equals("quartic")) {
            return Type.Quartic;
        }
        if (name.equals("quintic")) {
            return Type.Quintic;
        }
        if (name.equals("bezier")) {
            return Type.Bezier;
        }
        return Type.Linear;
    }

    public Curve() {
        this(Type.Linear);
    }

    public Curve(Type type2) {
        this(type2, null);
    }

    public Curve(Type type2, Curve subCurve2) {
        this.constraints = new Constraints(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        this.lastCubicSolution = Animation.CurveTimeline.LINEAR;
        setType(type2);
        this.subCurve = subCurve2;
    }

    public void setType(Type type2) {
        if (type2 == null) {
            throw new SpriterException("The type of a curve cannot be null!");
        }
        this.type = type2;
    }

    public Type getType() {
        return this.type;
    }

    public float tween(float a, float b, float t) {
        float t2 = tweenSub(Animation.CurveTimeline.LINEAR, 1.0f, t);
        switch (this.type) {
            case Instant:
                return a;
            case Linear:
                return Interpolator.linear(a, b, t2);
            case Quadratic:
                return Interpolator.quadratic(a, Interpolator.linear(a, b, this.constraints.c1), b, t2);
            case Cubic:
                return Interpolator.cubic(a, Interpolator.linear(a, b, this.constraints.c1), Interpolator.linear(a, b, this.constraints.c2), b, t2);
            case Quartic:
                return Interpolator.quartic(a, Interpolator.linear(a, b, this.constraints.c1), Interpolator.linear(a, b, this.constraints.c2), Interpolator.linear(a, b, this.constraints.c3), b, t2);
            case Quintic:
                return Interpolator.quintic(a, Interpolator.linear(a, b, this.constraints.c1), Interpolator.linear(a, b, this.constraints.c2), Interpolator.linear(a, b, this.constraints.c3), Interpolator.linear(a, b, this.constraints.c4), b, t2);
            case Bezier:
                Float cubicSolution = Calculator.solveCubic(((this.constraints.c1 - this.constraints.c3) * 3.0f) + 1.0f, (this.constraints.c3 - (2.0f * this.constraints.c1)) * 3.0f, this.constraints.c1 * 3.0f, -t2);
                if (cubicSolution == null) {
                    cubicSolution = Float.valueOf(this.lastCubicSolution);
                } else {
                    this.lastCubicSolution = cubicSolution.floatValue();
                }
                return Interpolator.linear(a, b, Interpolator.bezier(cubicSolution.floatValue(), Animation.CurveTimeline.LINEAR, this.constraints.c2, this.constraints.c4, 1.0f));
            default:
                return Interpolator.linear(a, b, t2);
        }
    }

    public void tweenPoint(Point a, Point b, float t, Point target) {
        target.set(tween(a.x, b.x, t), tween(a.y, b.y, t));
    }

    private float tweenSub(float a, float b, float t) {
        if (this.subCurve != null) {
            return this.subCurve.tween(a, b, t);
        }
        return t;
    }

    public float tweenAngle(float a, float b, float t, int spin) {
        if (spin > 0) {
            if (b - a < Animation.CurveTimeline.LINEAR) {
                b += 360.0f;
            }
        } else if (spin >= 0) {
            return a;
        } else {
            if (b - a > Animation.CurveTimeline.LINEAR) {
                b -= 360.0f;
            }
        }
        return tween(a, b, t);
    }

    public float tweenAngle(float a, float b, float t) {
        float t2 = tweenSub(Animation.CurveTimeline.LINEAR, 1.0f, t);
        switch (this.type) {
            case Instant:
                return a;
            case Linear:
                return Interpolator.linearAngle(a, b, t2);
            case Quadratic:
                return Interpolator.quadraticAngle(a, Interpolator.linearAngle(a, b, this.constraints.c1), b, t2);
            case Cubic:
                return Interpolator.cubicAngle(a, Interpolator.linearAngle(a, b, this.constraints.c1), Interpolator.linearAngle(a, b, this.constraints.c2), b, t2);
            case Quartic:
                return Interpolator.quarticAngle(a, Interpolator.linearAngle(a, b, this.constraints.c1), Interpolator.linearAngle(a, b, this.constraints.c2), Interpolator.linearAngle(a, b, this.constraints.c3), b, t2);
            case Quintic:
                return Interpolator.quinticAngle(a, Interpolator.linearAngle(a, b, this.constraints.c1), Interpolator.linearAngle(a, b, this.constraints.c2), Interpolator.linearAngle(a, b, this.constraints.c3), Interpolator.linearAngle(a, b, this.constraints.c4), b, t2);
            case Bezier:
                Float cubicSolution = Calculator.solveCubic(((this.constraints.c1 - this.constraints.c3) * 3.0f) + 1.0f, (this.constraints.c3 - (2.0f * this.constraints.c1)) * 3.0f, this.constraints.c1 * 3.0f, -t2);
                if (cubicSolution == null) {
                    cubicSolution = Float.valueOf(this.lastCubicSolution);
                } else {
                    this.lastCubicSolution = cubicSolution.floatValue();
                }
                return Interpolator.linearAngle(a, b, Interpolator.bezier(cubicSolution.floatValue(), Animation.CurveTimeline.LINEAR, this.constraints.c2, this.constraints.c4, 1.0f));
            default:
                return Interpolator.linearAngle(a, b, t2);
        }
    }

    public String toString() {
        return getClass().getSimpleName() + "|[" + this.type + ":" + this.constraints + ", subCurve: " + this.subCurve + "]";
    }

    public static class Constraints {
        public float c1;
        public float c2;
        public float c3;
        public float c4;

        public Constraints(float c12, float c22, float c32, float c42) {
            set(c12, c22, c32, c42);
        }

        public void set(float c12, float c22, float c32, float c42) {
            this.c1 = c12;
            this.c2 = c22;
            this.c3 = c32;
            this.c4 = c42;
        }

        public String toString() {
            return getClass().getSimpleName() + "| [c1:" + this.c1 + ", c2:" + this.c2 + ", c3:" + this.c3 + ", c4:" + this.c4 + "]";
        }
    }
}
