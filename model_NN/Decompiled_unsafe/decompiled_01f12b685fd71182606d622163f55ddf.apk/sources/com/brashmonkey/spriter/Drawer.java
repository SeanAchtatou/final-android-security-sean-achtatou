package com.brashmonkey.spriter;

import com.brashmonkey.spriter.Entity;
import com.brashmonkey.spriter.Timeline;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;

public abstract class Drawer<R> {
    protected Loader<R> loader;
    public float pointRadius = 5.0f;

    public abstract void circle(float f, float f2, float f3);

    public abstract void draw(Timeline.Key.Object object);

    public abstract void line(float f, float f2, float f3, float f4);

    public abstract void rectangle(float f, float f2, float f3, float f4);

    public abstract void setColor(float f, float f2, float f3, float f4);

    public Drawer(Loader<R> loader2) {
        this.loader = loader2;
    }

    public void setLoader(Loader<R> loader2) {
        if (loader2 == null) {
            throw new SpriterException("The loader instance can not be null!");
        }
        this.loader = loader2;
    }

    public void drawBones(Player player) {
        setColor(1.0f, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f);
        Iterator<Timeline.Key.Bone> it = player.boneIterator();
        while (it.hasNext()) {
            Timeline.Key.Bone bone = it.next();
            if (player.getKeyFor(bone).active) {
                drawBone(bone, player.getObjectInfoFor(bone).size);
            }
        }
    }

    public void drawBone(Timeline.Key.Bone bone, Dimension size) {
        float halfHeight = size.height / 2.0f;
        float xx = bone.position.x + (((float) Math.cos(Math.toRadians((double) bone.angle))) * size.height);
        float yy = bone.position.y + (((float) Math.sin(Math.toRadians((double) bone.angle))) * size.height);
        float x2 = ((float) Math.cos(Math.toRadians((double) (bone.angle + 90.0f)))) * halfHeight * bone.scale.y;
        float y2 = ((float) Math.sin(Math.toRadians((double) (bone.angle + 90.0f)))) * halfHeight * bone.scale.y;
        float targetX = bone.position.x + (((float) Math.cos(Math.toRadians((double) bone.angle))) * size.width * bone.scale.x);
        float targetY = bone.position.y + (((float) Math.sin(Math.toRadians((double) bone.angle))) * size.width * bone.scale.x);
        float upperPointX = xx + x2;
        float upperPointY = yy + y2;
        line(bone.position.x, bone.position.y, upperPointX, upperPointY);
        line(upperPointX, upperPointY, targetX, targetY);
        float lowerPointX = xx - x2;
        float lowerPointY = yy - y2;
        line(bone.position.x, bone.position.y, lowerPointX, lowerPointY);
        line(lowerPointX, lowerPointY, targetX, targetY);
        line(bone.position.x, bone.position.y, targetX, targetY);
    }

    public void drawBoxes(Player player) {
        setColor(Animation.CurveTimeline.LINEAR, 1.0f, Animation.CurveTimeline.LINEAR, 1.0f);
        drawBoneBoxes(player);
        drawObjectBoxes(player);
        drawPoints(player);
    }

    public void drawBoneBoxes(Player player) {
        drawBoneBoxes(player, player.boneIterator());
    }

    public void drawBoneBoxes(Player player, Iterator<Timeline.Key.Bone> it) {
        while (it.hasNext()) {
            drawBox(player.getBox(it.next()));
        }
    }

    public void drawObjectBoxes(Player player) {
        drawObjectBoxes(player, player.objectIterator());
    }

    public void drawObjectBoxes(Player player, Iterator<Timeline.Key.Object> it) {
        while (it.hasNext()) {
            drawBox(player.getBox(it.next()));
        }
    }

    public void drawPoints(Player player) {
        drawPoints(player, player.objectIterator());
    }

    public void drawPoints(Player player, Iterator<Timeline.Key.Object> it) {
        while (it.hasNext()) {
            Timeline.Key.Object point = it.next();
            if (player.getObjectInfoFor(point).type == Entity.ObjectType.Point) {
                float x = point.position.x + ((float) (Math.cos(Math.toRadians((double) point.angle)) * ((double) this.pointRadius)));
                float y = point.position.y + ((float) (Math.sin(Math.toRadians((double) point.angle)) * ((double) this.pointRadius)));
                circle(point.position.x, point.position.y, this.pointRadius);
                line(point.position.x, point.position.y, x, y);
            }
        }
    }

    public void draw(Player player) {
        draw(player, player.characterMaps);
    }

    public void draw(Player player, Entity.CharacterMap[] maps) {
        draw(player.objectIterator(), maps);
    }

    public void draw(Iterator<Timeline.Key.Object> it, Entity.CharacterMap[] maps) {
        while (it.hasNext()) {
            Timeline.Key.Object object = it.next();
            if (object.ref.hasFile()) {
                if (maps != null) {
                    for (Entity.CharacterMap map : maps) {
                        if (map != null) {
                            object.ref.set(map.get(object.ref));
                        }
                    }
                }
                draw(object);
            }
        }
    }

    public void drawBox(Box box) {
        line(box.points[0].x, box.points[0].y, box.points[1].x, box.points[1].y);
        line(box.points[1].x, box.points[1].y, box.points[3].x, box.points[3].y);
        line(box.points[3].x, box.points[3].y, box.points[2].x, box.points[2].y);
        line(box.points[2].x, box.points[2].y, box.points[0].x, box.points[0].y);
    }

    public void drawRectangle(Rectangle rect) {
        rectangle(rect.left, rect.bottom, rect.size.width, rect.size.height);
    }
}
