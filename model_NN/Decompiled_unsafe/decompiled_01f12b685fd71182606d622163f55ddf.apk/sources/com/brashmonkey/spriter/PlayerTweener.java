package com.brashmonkey.spriter;

public class PlayerTweener extends Player {
    private TweenedAnimation anim;
    public String baseBoneName;
    private Player player1;
    private Player player2;
    public boolean updatePlayers;

    public PlayerTweener(Player player12, Player player22) {
        super(player12.getEntity());
        this.updatePlayers = true;
        this.baseBoneName = null;
        setPlayers(player12, player22);
    }

    public PlayerTweener(Entity entity) {
        this(new Player(entity), new Player(entity));
    }

    public void update() {
        if (this.updatePlayers) {
            this.player1.update();
            this.player2.update();
        }
        this.anim.setAnimations(this.player1.animation, this.player2.animation);
        super.update();
        if (this.baseBoneName != null) {
            int index = this.anim.onFirstMainLine() ? this.player1.getBoneIndex(this.baseBoneName) : this.player2.getBoneIndex(this.baseBoneName);
            if (index == -1) {
                throw new SpriterException("A bone with name \"" + this.baseBoneName + "\" does no exist!");
            }
            this.anim.base = this.anim.getCurrentKey().getBoneRef(index);
            super.update();
        }
    }

    public void setPlayers(Player player12, Player player22) {
        if (player12.entity != player22.entity) {
            throw new SpriterException("player1 and player2 have to hold the same entity!");
        }
        this.player1 = player12;
        this.player2 = player22;
        if (player12.entity != this.entity) {
            this.anim = new TweenedAnimation(player12.getEntity());
            this.anim.setAnimations(player12.animation, player22.animation);
            super.setEntity(player12.getEntity());
            super.setAnimation(this.anim);
        }
    }

    public Player getFirstPlayer() {
        return this.player1;
    }

    public Player getSecondPlayer() {
        return this.player2;
    }

    public void setWeight(float weight) {
        this.anim.weight = weight;
    }

    public float getWeight() {
        return this.anim.weight;
    }

    public void setBaseAnimation(Animation anim2) {
        this.anim.baseAnimation = anim2;
    }

    public void setBaseAnimation(int index) {
        setBaseAnimation(this.entity.getAnimation(index));
    }

    public void setBaseAnimation(String name) {
        setBaseAnimation(this.entity.getAnimation(name));
    }

    public Animation getBaseAnimation() {
        return this.anim.baseAnimation;
    }

    public void setAnimation(Animation anim2) {
    }

    public void setEntity(Entity entity) {
    }
}
