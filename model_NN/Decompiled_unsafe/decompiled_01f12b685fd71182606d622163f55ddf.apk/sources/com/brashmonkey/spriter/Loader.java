package com.brashmonkey.spriter;

import java.io.File;
import java.util.HashMap;

public abstract class Loader<R> {
    protected Data data;
    private boolean disposed;
    protected final HashMap<FileReference, R> resources;
    protected String root = "";

    /* access modifiers changed from: protected */
    public abstract R loadResource(FileReference fileReference);

    public Loader(Data data2) {
        this.data = data2;
        this.resources = new HashMap<>(100);
    }

    /* access modifiers changed from: protected */
    public void finishLoading() {
    }

    /* access modifiers changed from: protected */
    public void beginLoading() {
    }

    public void load(String root2) {
        this.root = root2;
        beginLoading();
        for (Folder folder : this.data.folders) {
            for (File file : folder.files) {
                FileReference ref = new FileReference(folder.id, file.id);
                this.resources.put(ref, loadResource(ref));
            }
        }
        this.disposed = false;
        finishLoading();
    }

    public void load(File file) {
        load(file.getParent());
    }

    public R get(FileReference ref) {
        return this.resources.get(ref);
    }

    public void dispose() {
        this.resources.clear();
        this.data = null;
        this.root = "";
        this.disposed = true;
    }

    public boolean isDisposed() {
        return this.disposed;
    }
}
