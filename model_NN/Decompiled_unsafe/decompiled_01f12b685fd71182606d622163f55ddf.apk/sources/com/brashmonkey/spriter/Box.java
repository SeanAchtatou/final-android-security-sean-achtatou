package com.brashmonkey.spriter;

import com.brashmonkey.spriter.Entity;
import com.brashmonkey.spriter.Timeline;
import com.kbz.esotericsoftware.spine.Animation;

public class Box {
    public final Point[] points = new Point[4];
    private Rectangle rect;

    public Box() {
        for (int i = 0; i < 4; i++) {
            this.points[i] = new Point(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        }
        this.rect = new Rectangle(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    }

    public void calcFor(Timeline.Key.Bone boneOrObject, Entity.ObjectInfo info) {
        float width = info.size.width * boneOrObject.scale.x;
        float height = info.size.height * boneOrObject.scale.y;
        float pivotX = width * boneOrObject.pivot.x;
        float pivotY = height * boneOrObject.pivot.y;
        this.points[0].set(-pivotX, -pivotY);
        this.points[1].set(width - pivotX, -pivotY);
        this.points[2].set(-pivotX, height - pivotY);
        this.points[3].set(width - pivotX, height - pivotY);
        for (int i = 0; i < 4; i++) {
            this.points[i].rotate(boneOrObject.angle);
        }
        for (int i2 = 0; i2 < 4; i2++) {
            this.points[i2].translate(boneOrObject.position);
        }
    }

    public boolean collides(Timeline.Key.Bone boneOrObject, Entity.ObjectInfo info, float x, float y) {
        float width = info.size.width * boneOrObject.scale.x;
        float height = info.size.height * boneOrObject.scale.y;
        float pivotX = width * boneOrObject.pivot.x;
        float pivotY = height * boneOrObject.pivot.y;
        Point point = new Point(x - boneOrObject.position.x, y - boneOrObject.position.y);
        point.rotate(-boneOrObject.angle);
        return point.x >= (-pivotX) && point.x <= width - pivotX && point.y >= (-pivotY) && point.y <= height - pivotY;
    }

    public boolean isInside(Rectangle rect2) {
        boolean inside = false;
        for (Point p : this.points) {
            inside |= rect2.isInside(p);
        }
        return inside;
    }

    public Rectangle getBoundingRect() {
        this.rect.set(this.points[0].x, this.points[0].y, this.points[0].x, this.points[0].y);
        this.rect.left = Math.min(Math.min(Math.min(Math.min(this.points[0].x, this.points[1].x), this.points[2].x), this.points[3].x), this.rect.left);
        this.rect.right = Math.max(Math.max(Math.max(Math.max(this.points[0].x, this.points[1].x), this.points[2].x), this.points[3].x), this.rect.right);
        this.rect.top = Math.max(Math.max(Math.max(Math.max(this.points[0].y, this.points[1].y), this.points[2].y), this.points[3].y), this.rect.top);
        this.rect.bottom = Math.min(Math.min(Math.min(Math.min(this.points[0].y, this.points[1].y), this.points[2].y), this.points[3].y), this.rect.bottom);
        return this.rect;
    }
}
