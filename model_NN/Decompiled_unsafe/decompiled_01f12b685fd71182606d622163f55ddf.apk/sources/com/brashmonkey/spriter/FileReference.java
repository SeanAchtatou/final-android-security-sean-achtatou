package com.brashmonkey.spriter;

import com.datalab.tools.Constant;

public class FileReference {
    public int file;
    public int folder;

    public FileReference(int folder2, int file2) {
        set(folder2, file2);
    }

    public int hashCode() {
        return (this.folder * Constant.DEFAULT_LIMIT) + this.file;
    }

    public boolean equals(Object ref) {
        if (!(ref instanceof FileReference)) {
            return false;
        }
        if (this.file == ((FileReference) ref).file && this.folder == ((FileReference) ref).folder) {
            return true;
        }
        return false;
    }

    public void set(int folder2, int file2) {
        this.folder = folder2;
        this.file = file2;
    }

    public void set(FileReference ref) {
        set(ref.folder, ref.file);
    }

    public boolean hasFile() {
        return this.file != -1;
    }

    public boolean hasFolder() {
        return this.folder != -1;
    }

    public String toString() {
        return "[folder: " + this.folder + ", file: " + this.file + "]";
    }
}
