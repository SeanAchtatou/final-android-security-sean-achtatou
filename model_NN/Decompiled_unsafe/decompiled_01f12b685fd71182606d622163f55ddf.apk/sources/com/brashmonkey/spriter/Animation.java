package com.brashmonkey.spriter;

import com.brashmonkey.spriter.Mainline;
import com.brashmonkey.spriter.Timeline;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.HashMap;

public class Animation {
    Mainline.Key currentKey;
    public final int id;
    public final int length;
    public final boolean looping;
    public final Mainline mainline;
    public final String name;
    private final HashMap<String, Timeline> nameToTimeline;
    private boolean prepared;
    private int timelinePointer = 0;
    private final Timeline[] timelines;
    Timeline.Key[] tweenedKeys;
    Timeline.Key[] unmappedTweenedKeys;

    public Animation(Mainline mainline2, int id2, String name2, int length2, boolean looping2, int timelines2) {
        this.mainline = mainline2;
        this.id = id2;
        this.name = name2;
        this.length = length2;
        this.looping = looping2;
        this.timelines = new Timeline[timelines2];
        this.prepared = false;
        this.nameToTimeline = new HashMap<>();
    }

    public Timeline getTimeline(int index) {
        return this.timelines[index];
    }

    public Timeline getTimeline(String name2) {
        return this.nameToTimeline.get(name2);
    }

    /* access modifiers changed from: package-private */
    public void addTimeline(Timeline timeline) {
        Timeline[] timelineArr = this.timelines;
        int i = this.timelinePointer;
        this.timelinePointer = i + 1;
        timelineArr[i] = timeline;
        this.nameToTimeline.put(timeline.name, timeline);
    }

    public int timelines() {
        return this.timelines.length;
    }

    public String toString() {
        String toReturn = (((getClass().getSimpleName() + "|[id: " + this.id + ", " + this.name + ", duration: " + this.length + ", is looping: " + this.looping) + "Mainline:\n") + this.mainline) + "Timelines\n";
        Timeline[] arr$ = this.timelines;
        for (int i$ = 0; i$ < arr$.length; i$++) {
            toReturn = toReturn + arr$[i$];
        }
        return toReturn + "]";
    }

    /* JADX INFO: Multiple debug info for r0v1 com.brashmonkey.spriter.Mainline$Key$BoneRef[]: [D('arr$' com.brashmonkey.spriter.Mainline$Key$BoneRef[]), D('arr$' com.brashmonkey.spriter.Timeline$Key[])] */
    /* JADX INFO: Multiple debug info for r0v2 com.brashmonkey.spriter.Mainline$Key$ObjectRef[]: [D('arr$' com.brashmonkey.spriter.Mainline$Key$ObjectRef[]), D('arr$' com.brashmonkey.spriter.Mainline$Key$BoneRef[])] */
    public void update(int time, Timeline.Key.Bone root) {
        if (!this.prepared) {
            throw new SpriterException("This animation is not ready yet to animate itself. Please call prepare()!");
        } else if (root == null) {
            throw new SpriterException("The root can not be null! Set a root bone to apply this animation relative to the root bone.");
        } else {
            this.currentKey = this.mainline.getKeyBeforeTime(time);
            for (Timeline.Key timelineKey : this.unmappedTweenedKeys) {
                timelineKey.active = false;
            }
            for (Mainline.Key.BoneRef ref : this.currentKey.boneRefs) {
                update(ref, root, time);
            }
            for (Mainline.Key.ObjectRef ref2 : this.currentKey.objectRefs) {
                update(ref2, root, time);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void update(Mainline.Key.BoneRef ref, Timeline.Key.Bone root, int time) {
        float t;
        boolean isObject = ref instanceof Mainline.Key.ObjectRef;
        Timeline timeline = getTimeline(ref.timeline);
        Timeline.Key key = timeline.getKey(ref.key);
        Timeline.Key nextKey = timeline.getKey((ref.key + 1) % timeline.keys.length);
        int currentTime = key.time;
        int nextTime = nextKey.time;
        if (nextTime < currentTime) {
            if (!this.looping) {
                nextKey = key;
            } else {
                nextTime = this.length;
            }
        }
        float t2 = ((float) (time - currentTime)) / ((float) (nextTime - currentTime));
        if (Float.isNaN(t2) || Float.isInfinite(t2)) {
            t2 = 1.0f;
        }
        if (this.currentKey.time > currentTime) {
            float tMid = ((float) (this.currentKey.time - currentTime)) / ((float) (nextTime - currentTime));
            if (Float.isNaN(tMid) || Float.isInfinite(tMid)) {
                tMid = Animation.CurveTimeline.LINEAR;
            }
            float t3 = ((float) (time - this.currentKey.time)) / ((float) (nextTime - this.currentKey.time));
            if (Float.isNaN(t3) || Float.isInfinite(t3)) {
                t3 = 1.0f;
            }
            t = this.currentKey.curve.tween(tMid, 1.0f, t3);
        } else {
            t = this.currentKey.curve.tween(Animation.CurveTimeline.LINEAR, 1.0f, t2);
        }
        Timeline.Key.Bone bone1 = key.object();
        Timeline.Key.Bone bone2 = nextKey.object();
        Timeline.Key.Bone tweenTarget = this.tweenedKeys[ref.timeline].object();
        if (isObject) {
            tweenObject((Timeline.Key.Object) bone1, (Timeline.Key.Object) bone2, (Timeline.Key.Object) tweenTarget, t, key.curve, key.spin);
        } else {
            tweenBone(bone1, bone2, tweenTarget, t, key.curve, key.spin);
        }
        this.unmappedTweenedKeys[ref.timeline].active = true;
        int i = ref.timeline;
        if (ref.parent != null) {
            root = this.unmappedTweenedKeys[ref.parent.timeline].object();
        }
        unmapTimelineObject(i, isObject, root);
    }

    /* access modifiers changed from: package-private */
    public void unmapTimelineObject(int timeline, boolean isObject, Timeline.Key.Bone root) {
        Timeline.Key.Bone tweenTarget = this.tweenedKeys[timeline].object();
        Timeline.Key.Object object = this.unmappedTweenedKeys[timeline].object();
        if (isObject) {
            object.set((Timeline.Key.Object) tweenTarget);
        } else {
            object.set(tweenTarget);
        }
        object.unmap(root);
    }

    /* access modifiers changed from: protected */
    public void tweenBone(Timeline.Key.Bone bone1, Timeline.Key.Bone bone2, Timeline.Key.Bone target, float t, Curve curve, int spin) {
        target.angle = curve.tweenAngle(bone1.angle, bone2.angle, t, spin);
        curve.tweenPoint(bone1.position, bone2.position, t, target.position);
        curve.tweenPoint(bone1.scale, bone2.scale, t, target.scale);
        curve.tweenPoint(bone1.pivot, bone2.pivot, t, target.pivot);
    }

    /* access modifiers changed from: protected */
    public void tweenObject(Timeline.Key.Object object1, Timeline.Key.Object object2, Timeline.Key.Object target, float t, Curve curve, int spin) {
        tweenBone(object1, object2, target, t, curve, spin);
        target.alpha = curve.tweenAngle(object1.alpha, object2.alpha, t);
        target.ref.set(object1.ref);
    }

    /* access modifiers changed from: package-private */
    public Timeline getSimilarTimeline(Timeline t) {
        Timeline found = getTimeline(t.name);
        if (found != null || t.id >= timelines()) {
            return found;
        }
        return getTimeline(t.id);
    }

    public void prepare() {
        if (!this.prepared) {
            this.tweenedKeys = new Timeline.Key[this.timelines.length];
            this.unmappedTweenedKeys = new Timeline.Key[this.timelines.length];
            for (int i = 0; i < this.tweenedKeys.length; i++) {
                this.tweenedKeys[i] = new Timeline.Key(i);
                this.unmappedTweenedKeys[i] = new Timeline.Key(i);
                this.tweenedKeys[i].setObject(new Timeline.Key.Object(new Point(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR)));
                this.unmappedTweenedKeys[i].setObject(new Timeline.Key.Object(new Point(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR)));
            }
            if (this.mainline.keys.length > 0) {
                this.currentKey = this.mainline.getKey(0);
            }
            this.prepared = true;
        }
    }
}
