package com.brashmonkey.spriter;

import com.brashmonkey.spriter.Entity;
import com.kbz.esotericsoftware.spine.Animation;

public class Timeline {
    public final int id;
    private int keyPointer = 0;
    public final Key[] keys;
    public final String name;
    public final Entity.ObjectInfo objectInfo;

    Timeline(int id2, String name2, Entity.ObjectInfo objectInfo2, int keys2) {
        this.id = id2;
        this.name = name2;
        this.objectInfo = objectInfo2;
        this.keys = new Key[keys2];
    }

    /* access modifiers changed from: package-private */
    public void addKey(Key key) {
        Key[] keyArr = this.keys;
        int i = this.keyPointer;
        this.keyPointer = i + 1;
        keyArr[i] = key;
    }

    public Key getKey(int index) {
        return this.keys[index];
    }

    public String toString() {
        String toReturn = getClass().getSimpleName() + "|[id:" + this.id + ", name: " + this.name + ", object_info: " + this.objectInfo;
        Key[] arr$ = this.keys;
        for (int i$ = 0; i$ < arr$.length; i$++) {
            toReturn = toReturn + "\n" + arr$[i$];
        }
        return toReturn + "]";
    }

    public static class Key {
        public boolean active;
        public final Curve curve;
        public final int id;
        private Object object;
        public final int spin;
        public int time;

        public Key(int id2, int time2, int spin2, Curve curve2) {
            this.id = id2;
            this.time = time2;
            this.spin = spin2;
            this.curve = curve2;
        }

        public Key(int id2, int time2, int spin2) {
            this(id2, time2, 1, new Curve());
        }

        public Key(int id2, int time2) {
            this(id2, time2, 1);
        }

        public Key(int id2) {
            this(id2, 0);
        }

        public void setObject(Object object2) {
            if (object2 == null) {
                throw new IllegalArgumentException("object can not be null!");
            }
            this.object = object2;
        }

        public Object object() {
            return this.object;
        }

        public String toString() {
            return getClass().getSimpleName() + "|[id: " + this.id + ", time: " + this.time + ", spin: " + this.spin + "\ncurve: " + this.curve + "\nobject:" + this.object + "]";
        }

        public static class Bone {
            public float angle;
            public final Point pivot;
            public final Point position;
            public final Point scale;

            public Bone(Point position2, Point scale2, Point pivot2, float angle2) {
                this.position = new Point(position2);
                this.scale = new Point(scale2);
                this.angle = angle2;
                this.pivot = new Point(pivot2);
            }

            public Bone(Bone bone) {
                this(bone.position, bone.scale, bone.pivot, bone.angle);
            }

            public Bone(Point position2) {
                this(position2, new Point(1.0f, 1.0f), new Point(Animation.CurveTimeline.LINEAR, 1.0f), Animation.CurveTimeline.LINEAR);
            }

            public Bone() {
                this(new Point());
            }

            public boolean isBone() {
                return !(this instanceof Object);
            }

            public void set(Bone bone) {
                set(bone.position, bone.angle, bone.scale, bone.pivot);
            }

            public void set(float x, float y, float angle2, float scaleX, float scaleY, float pivotX, float pivotY) {
                this.angle = angle2;
                this.position.set(x, y);
                this.scale.set(scaleX, scaleY);
                this.pivot.set(pivotX, pivotY);
            }

            public void set(Point position2, float angle2, Point scale2, Point pivot2) {
                set(position2.x, position2.y, angle2, scale2.x, scale2.y, pivot2.x, pivot2.y);
            }

            public void unmap(Bone parent) {
                this.angle *= Math.signum(parent.scale.x) * Math.signum(parent.scale.y);
                this.angle += parent.angle;
                this.scale.scale(parent.scale);
                this.position.scale(parent.scale);
                this.position.rotate(parent.angle);
                this.position.translate(parent.position);
            }

            public void map(Bone parent) {
                this.position.translate(-parent.position.x, -parent.position.y);
                this.position.rotate(-parent.angle);
                this.position.scale(1.0f / parent.scale.x, 1.0f / parent.scale.y);
                this.scale.scale(1.0f / parent.scale.x, 1.0f / parent.scale.y);
                this.angle -= parent.angle;
                this.angle *= Math.signum(parent.scale.x) * Math.signum(parent.scale.y);
            }

            public String toString() {
                return getClass().getSimpleName() + "|position: " + this.position + ", scale: " + this.scale + ", angle: " + this.angle;
            }
        }

        public static class Object extends Bone {
            public float alpha;
            public final FileReference ref;

            public Object(Point position, Point scale, Point pivot, float angle, float alpha2, FileReference ref2) {
                super(position, scale, pivot, angle);
                this.alpha = alpha2;
                this.ref = ref2;
            }

            public Object(Point position) {
                this(position, new Point(1.0f, 1.0f), new Point(Animation.CurveTimeline.LINEAR, 1.0f), Animation.CurveTimeline.LINEAR, 1.0f, new FileReference(-1, -1));
            }

            public Object(Object object) {
                this(object.position.copy(), object.scale.copy(), object.pivot.copy(), object.angle, object.alpha, object.ref);
            }

            public Object() {
                this(new Point());
            }

            public void set(Object object) {
                set(object.position, object.angle, object.scale, object.pivot, object.alpha, object.ref);
            }

            public void set(float x, float y, float angle, float scaleX, float scaleY, float pivotX, float pivotY, float alpha2, int folder, int file) {
                super.set(x, y, angle, scaleX, scaleY, pivotX, pivotY);
                this.alpha = alpha2;
                this.ref.folder = folder;
                this.ref.file = file;
            }

            public void set(Point position, float angle, Point scale, Point pivot, float alpha2, FileReference fileRef) {
                set(position.x, position.y, angle, scale.x, scale.y, pivot.x, pivot.y, alpha2, fileRef.folder, fileRef.file);
            }

            public String toString() {
                return super.toString() + ", pivot: " + this.pivot + ", alpha: " + this.alpha + ", reference: " + this.ref;
            }
        }
    }
}
