package com.brashmonkey.spriter;

public class IKObject extends Point {
    int chainLength;
    int iterations;

    public IKObject(float x, float y, int length, int iterations2) {
        super(x, y);
        setLength(length);
        setIterations(iterations2);
    }

    public IKObject setLength(int chainLength2) {
        if (chainLength2 < 0) {
            throw new SpriterException("The chain has to be at least 0!");
        }
        this.chainLength = chainLength2;
        return this;
    }

    public IKObject setIterations(int iterations2) {
        if (iterations2 < 0) {
            throw new SpriterException("The number of iterations has to be at least 1!");
        }
        this.iterations = iterations2;
        return this;
    }

    public int getChainLength() {
        return this.chainLength;
    }

    public int getIterations() {
        return this.iterations;
    }
}
