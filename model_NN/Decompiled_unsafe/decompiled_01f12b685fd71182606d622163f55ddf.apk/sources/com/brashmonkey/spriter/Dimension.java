package com.brashmonkey.spriter;

public class Dimension {
    public float height;
    public float width;

    public Dimension(float width2, float height2) {
        set(width2, height2);
    }

    public Dimension(Dimension size) {
        set(size);
    }

    public void set(float width2, float height2) {
        this.width = width2;
        this.height = height2;
    }

    public void set(Dimension size) {
        set(size.width, size.height);
    }

    public String toString() {
        return "[" + this.width + "x" + this.height + "]";
    }
}
