package com.brashmonkey.spriter;

import com.kbz.esotericsoftware.spine.Animation;

public class Rectangle {
    public float bottom;
    public float left;
    public float right;
    public final Dimension size;
    public float top;

    public Rectangle(float left2, float top2, float right2, float bottom2) {
        set(left2, top2, right2, bottom2);
        this.size = new Dimension(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        calculateSize();
    }

    public Rectangle(Rectangle rect) {
        this(rect.left, rect.top, rect.right, rect.bottom);
    }

    public boolean isInside(float x, float y) {
        return x >= this.left && x <= this.right && y <= this.top && y >= this.bottom;
    }

    public boolean isInside(Point point) {
        return isInside(point.x, point.y);
    }

    public void calculateSize() {
        this.size.set(this.right - this.left, this.top - this.bottom);
    }

    public void set(Rectangle rect) {
        if (rect != null) {
            this.bottom = rect.bottom;
            this.left = rect.left;
            this.right = rect.right;
            this.top = rect.top;
            calculateSize();
        }
    }

    public void set(float left2, float top2, float right2, float bottom2) {
        this.left = left2;
        this.top = top2;
        this.right = right2;
        this.bottom = bottom2;
    }

    public static boolean areIntersecting(Rectangle rect1, Rectangle rect2) {
        return rect1.isInside(rect2.left, rect2.top) || rect1.isInside(rect2.right, rect2.top) || rect1.isInside(rect2.left, rect2.bottom) || rect1.isInside(rect2.right, rect2.bottom);
    }

    public static void setBiggerRectangle(Rectangle rect1, Rectangle rect2, Rectangle target) {
        target.left = Math.min(rect1.left, rect2.left);
        target.bottom = Math.min(rect1.bottom, rect2.bottom);
        target.right = Math.max(rect1.right, rect2.right);
        target.top = Math.max(rect1.top, rect2.top);
    }
}
