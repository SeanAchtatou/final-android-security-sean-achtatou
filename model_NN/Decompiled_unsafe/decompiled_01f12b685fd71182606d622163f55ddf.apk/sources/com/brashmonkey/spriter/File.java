package com.brashmonkey.spriter;

public class File {
    public final int id;
    public final String name;
    public final Point pivot;
    public final Dimension size;

    File(int id2, String name2, Dimension size2, Point pivot2) {
        this.id = id2;
        this.name = name2;
        this.size = size2;
        this.pivot = pivot2;
    }

    public boolean isSprite() {
        return (this.pivot == null || this.size == null) ? false : true;
    }

    public String toString() {
        return getClass().getSimpleName() + "|[id: " + this.id + ", name: " + this.name + ", size: " + this.size + ", pivot: " + this.pivot;
    }
}
