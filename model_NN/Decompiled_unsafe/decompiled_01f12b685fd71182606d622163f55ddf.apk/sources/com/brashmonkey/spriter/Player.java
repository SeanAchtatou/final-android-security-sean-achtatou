package com.brashmonkey.spriter;

import com.brashmonkey.spriter.Entity;
import com.brashmonkey.spriter.Mainline;
import com.brashmonkey.spriter.Timeline;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Player {
    private float angle;
    Animation animation;
    public final List<Attachment> attachments = new ArrayList();
    private BoneIterator boneIterator = new BoneIterator();
    public Entity.CharacterMap[] characterMaps;
    public boolean copyObjects = true;
    private Mainline.Key currentKey;
    private boolean dirty = true;
    protected Entity entity;
    private List<PlayerListener> listeners = new ArrayList();
    private final HashMap<Timeline.Key.Object, Timeline.Key> objToTimeline = new HashMap<>();
    private ObjectIterator objectIterator = new ObjectIterator();
    private final Point pivot = new Point(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    private final Point position = new Point(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    public final Box prevBBox = new Box();
    private Mainline.Key prevKey;
    private Rectangle rect = new Rectangle(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    Timeline.Key.Bone root = new Timeline.Key.Bone(new Point(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR));
    public int speed = 15;
    private Timeline.Key[] tempTweenedKeys;
    private Timeline.Key[] tempUnmappedTweenedKeys;
    int time;
    Timeline.Key[] tweenedKeys;
    Timeline.Key[] unmappedTweenedKeys;

    public interface PlayerListener {
        void animationChanged(Animation animation, Animation animation2);

        void animationFinished(Animation animation);

        void mainlineKeyChanged(Mainline.Key key, Mainline.Key key2);

        void postProcess(Player player);

        void preProcess(Player player);
    }

    public Player(Entity entity2) {
        setEntity(entity2);
    }

    public void update() {
        for (PlayerListener listener : this.listeners) {
            listener.preProcess(this);
        }
        if (this.dirty) {
            updateRoot();
        }
        this.animation.update(this.time, this.root);
        this.currentKey = this.animation.currentKey;
        if (this.prevKey != this.currentKey) {
            for (PlayerListener listener2 : this.listeners) {
                listener2.mainlineKeyChanged(this.prevKey, this.currentKey);
            }
            this.prevKey = this.currentKey;
        }
        if (this.copyObjects) {
            this.tweenedKeys = this.tempTweenedKeys;
            this.unmappedTweenedKeys = this.tempUnmappedTweenedKeys;
            copyObjects();
        } else {
            this.tweenedKeys = this.animation.tweenedKeys;
            this.unmappedTweenedKeys = this.animation.unmappedTweenedKeys;
        }
        for (Attachment attach : this.attachments) {
            attach.update();
        }
        for (PlayerListener listener3 : this.listeners) {
            listener3.postProcess(this);
        }
        increaseTime();
    }

    private void copyObjects() {
        for (int i = 0; i < this.animation.tweenedKeys.length; i++) {
            this.tweenedKeys[i].active = this.animation.tweenedKeys[i].active;
            this.unmappedTweenedKeys[i].active = this.animation.unmappedTweenedKeys[i].active;
            this.tweenedKeys[i].object().set(this.animation.tweenedKeys[i].object());
            this.unmappedTweenedKeys[i].object().set(this.animation.unmappedTweenedKeys[i].object());
        }
    }

    private void increaseTime() {
        this.time += this.speed;
        if (this.time > this.animation.length) {
            this.time -= this.animation.length;
            for (PlayerListener listener : this.listeners) {
                listener.animationFinished(this.animation);
            }
        }
        if (this.time < 0) {
            for (PlayerListener listener2 : this.listeners) {
                listener2.animationFinished(this.animation);
            }
            this.time += this.animation.length;
        }
    }

    private void updateRoot() {
        this.root.angle = this.angle;
        this.root.position.set(this.pivot);
        this.root.position.rotate(this.angle);
        this.root.position.translate(this.position);
        this.dirty = false;
    }

    public Timeline.Key.Bone getBone(int index) {
        return this.unmappedTweenedKeys[getCurrentKey().getBoneRef(index).timeline].object();
    }

    public Timeline.Key.Object getObject(int index) {
        return this.unmappedTweenedKeys[getCurrentKey().getObjectRef(index).timeline].object();
    }

    public int getBoneIndex(String name) {
        for (Mainline.Key.BoneRef ref : getCurrentKey().boneRefs) {
            if (this.animation.getTimeline(ref.timeline).name.equals(name)) {
                return ref.id;
            }
        }
        return -1;
    }

    public Timeline.Key.Bone getBone(String name) {
        return this.unmappedTweenedKeys[this.animation.getTimeline(name).id].object();
    }

    public Mainline.Key.BoneRef getBoneRef(Timeline.Key.Bone bone) {
        return getCurrentKey().getBoneRefTimeline(this.objToTimeline.get(bone).id);
    }

    public int getObjectIndex(String name) {
        for (Mainline.Key.ObjectRef ref : getCurrentKey().objectRefs) {
            if (this.animation.getTimeline(ref.timeline).name.equals(name)) {
                return ref.id;
            }
        }
        return -1;
    }

    public Timeline.Key.Object getObject(String name) {
        return this.unmappedTweenedKeys[this.animation.getTimeline(name).id].object();
    }

    public Mainline.Key.ObjectRef getObjectRef(Timeline.Key.Object object) {
        return getCurrentKey().getObjectRefTimeline(this.objToTimeline.get(object).id);
    }

    public String getNameFor(Timeline.Key.Bone boneOrObject) {
        return this.animation.getTimeline(this.objToTimeline.get(boneOrObject).id).name;
    }

    public Entity.ObjectInfo getObjectInfoFor(Timeline.Key.Bone boneOrObject) {
        return this.animation.getTimeline(this.objToTimeline.get(boneOrObject).id).objectInfo;
    }

    public Timeline.Key getKeyFor(Timeline.Key.Bone boneOrObject) {
        return this.objToTimeline.get(boneOrObject);
    }

    public Box getBox(Timeline.Key.Bone boneOrObject) {
        this.prevBBox.calcFor(boneOrObject, getObjectInfoFor(boneOrObject));
        return this.prevBBox;
    }

    public boolean collidesFor(Timeline.Key.Bone boneOrObject, float x, float y) {
        Entity.ObjectInfo info = getObjectInfoFor(boneOrObject);
        this.prevBBox.calcFor(boneOrObject, info);
        return this.prevBBox.collides(boneOrObject, info, x, y);
    }

    public boolean collidesFor(Timeline.Key.Bone boneOrObject, Point point) {
        return collidesFor(boneOrObject, point.x, point.y);
    }

    public boolean collidesFor(Timeline.Key.Bone boneOrObject, Rectangle area) {
        this.prevBBox.calcFor(boneOrObject, getObjectInfoFor(boneOrObject));
        return this.prevBBox.isInside(area);
    }

    public void setBone(String name, float x, float y, float angle2, float scaleX, float scaleY) {
        int index = getBoneIndex(name);
        if (index == -1) {
            throw new SpriterException("No bone found of name \"" + name + "\"");
        }
        Mainline.Key.BoneRef ref = getCurrentKey().getBoneRef(index);
        getBone(index).set(x, y, angle2, scaleX, scaleY, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        unmapObjects(ref);
    }

    public void setBone(String name, Point position2, float angle2, Point scale) {
        setBone(name, position2.x, position2.y, angle2, scale.x, scale.y);
    }

    public void setBone(String name, float x, float y, float angle2) {
        Timeline.Key.Bone b = getBone(name);
        setBone(name, x, y, angle2, b.scale.x, b.scale.y);
    }

    public void setBone(String name, Point position2, float angle2) {
        Timeline.Key.Bone b = getBone(name);
        setBone(name, position2.x, position2.y, angle2, b.scale.x, b.scale.y);
    }

    public void setBone(String name, float x, float y) {
        setBone(name, x, y, getBone(name).angle);
    }

    public void setBone(String name, Point position2) {
        setBone(name, position2.x, position2.y);
    }

    public void setBone(String name, float angle2) {
        Timeline.Key.Bone b = getBone(name);
        setBone(name, b.position.x, b.position.y, angle2);
    }

    public void setBone(String name, Timeline.Key.Bone bone) {
        setBone(name, bone.position, bone.angle, bone.scale);
    }

    public void setObject(String name, float x, float y, float angle2, float scaleX, float scaleY, float pivotX, float pivotY, float alpha, int folder, int file) {
        int index = getObjectIndex(name);
        if (index == -1) {
            throw new SpriterException("No object found for name \"" + name + "\"");
        }
        Mainline.Key.ObjectRef ref = getCurrentKey().getObjectRef(index);
        getObject(index).set(x, y, angle2, scaleX, scaleY, pivotX, pivotY, alpha, folder, file);
        unmapObjects(ref);
    }

    public void setObject(String name, Point position2, float angle2, Point scale, Point pivot2, float alpha, FileReference ref) {
        setObject(name, position2.x, position2.y, angle2, scale.x, scale.y, pivot2.x, pivot2.y, alpha, ref.folder, ref.file);
    }

    public void setObject(String name, float x, float y, float angle2, float scaleX, float scaleY) {
        Timeline.Key.Object b = getObject(name);
        setObject(name, x, y, angle2, scaleX, scaleY, b.pivot.x, b.pivot.y, b.alpha, b.ref.folder, b.ref.file);
    }

    public void setObject(String name, float x, float y, float angle2) {
        Timeline.Key.Object b = getObject(name);
        setObject(name, x, y, angle2, b.scale.x, b.scale.y);
    }

    public void setObject(String name, Point position2, float angle2) {
        Timeline.Key.Object b = getObject(name);
        setObject(name, position2.x, position2.y, angle2, b.scale.x, b.scale.y);
    }

    public void setObject(String name, float x, float y) {
        setObject(name, x, y, getObject(name).angle);
    }

    public void setObject(String name, Point position2) {
        setObject(name, position2.x, position2.y);
    }

    public void setObject(String name, float angle2) {
        Timeline.Key.Object b = getObject(name);
        setObject(name, b.position.x, b.position.y, angle2);
    }

    public void setObject(String name, float alpha, int folder, int file) {
        Timeline.Key.Object b = getObject(name);
        setObject(name, b.position.x, b.position.y, b.angle, b.scale.x, b.scale.y, b.pivot.x, b.pivot.y, alpha, folder, file);
    }

    public void setObject(String name, Timeline.Key.Object object) {
        setObject(name, object.position, object.angle, object.scale, object.pivot, object.alpha, object.ref);
    }

    public void unmapObjects(Mainline.Key.BoneRef base) {
        for (int i = (base == null ? -1 : base.id - 1) + 1; i < getCurrentKey().boneRefs.length; i++) {
            Mainline.Key.BoneRef ref = getCurrentKey().getBoneRef(i);
            if (ref.parent == base || base == null) {
                Timeline.Key.Bone parent = ref.parent == null ? this.root : this.unmappedTweenedKeys[ref.parent.timeline].object();
                this.unmappedTweenedKeys[ref.timeline].object().set(this.tweenedKeys[ref.timeline].object());
                this.unmappedTweenedKeys[ref.timeline].object().unmap(parent);
                unmapObjects(ref);
            }
        }
        for (Mainline.Key.ObjectRef ref2 : getCurrentKey().objectRefs) {
            if (ref2.parent == base || base == null) {
                Timeline.Key.Bone parent2 = ref2.parent == null ? this.root : this.unmappedTweenedKeys[ref2.parent.timeline].object();
                this.unmappedTweenedKeys[ref2.timeline].object().set(this.tweenedKeys[ref2.timeline].object());
                this.unmappedTweenedKeys[ref2.timeline].object().unmap(parent2);
            }
        }
    }

    public void setEntity(Entity entity2) {
        if (entity2 == null) {
            throw new SpriterException("entity can not be null!");
        }
        this.entity = entity2;
        int maxAnims = entity2.getAnimationWithMostTimelines().timelines();
        this.tweenedKeys = new Timeline.Key[maxAnims];
        this.unmappedTweenedKeys = new Timeline.Key[maxAnims];
        for (int i = 0; i < maxAnims; i++) {
            Timeline.Key key = new Timeline.Key(i);
            Timeline.Key keyU = new Timeline.Key(i);
            key.setObject(new Timeline.Key.Object(new Point(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR)));
            keyU.setObject(new Timeline.Key.Object(new Point(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR)));
            this.tweenedKeys[i] = key;
            this.unmappedTweenedKeys[i] = keyU;
            this.objToTimeline.put(keyU.object(), keyU);
        }
        this.tempTweenedKeys = this.tweenedKeys;
        this.tempUnmappedTweenedKeys = this.unmappedTweenedKeys;
        setAnimation(entity2.getAnimation(0));
    }

    public Entity getEntity() {
        return this.entity;
    }

    public void setAnimation(Animation animation2) {
        Animation prevAnim = this.animation;
        if (animation2 != this.animation) {
            if (animation2 == null) {
                throw new SpriterException("animation can not be null!");
            } else if (this.entity.containsAnimation(animation2) || animation2.id == -1) {
                if (animation2 != this.animation) {
                    this.time = 0;
                }
                this.animation = animation2;
                int tempTime = this.time;
                this.time = 0;
                update();
                this.time = tempTime;
                for (PlayerListener listener : this.listeners) {
                    listener.animationChanged(prevAnim, animation2);
                }
            } else {
                throw new SpriterException("animation has to be in the same entity as the current set one!");
            }
        }
    }

    public void setAnimation(String name) {
        setAnimation(this.entity.getAnimation(name));
    }

    public void setAnimation(int index) {
        setAnimation(this.entity.getAnimation(index));
    }

    public Animation getAnimation() {
        return this.animation;
    }

    public Rectangle getBoundingRectangle(Mainline.Key.BoneRef root2) {
        Timeline.Key.Bone boneRoot = root2 == null ? this.root : this.unmappedTweenedKeys[root2.timeline].object();
        this.rect.set(boneRoot.position.x, boneRoot.position.y, boneRoot.position.x, boneRoot.position.y);
        calcBoundingRectangle(root2);
        this.rect.calculateSize();
        return this.rect;
    }

    public Rectangle getBoudingRectangle(Timeline.Key.Bone root2) {
        return getBoundingRectangle(root2 == null ? null : getBoneRef(root2));
    }

    /* JADX INFO: Multiple debug info for r0v1 com.brashmonkey.spriter.Mainline$Key$ObjectRef[]: [D('arr$' com.brashmonkey.spriter.Mainline$Key$ObjectRef[]), D('arr$' com.brashmonkey.spriter.Mainline$Key$BoneRef[])] */
    private void calcBoundingRectangle(Mainline.Key.BoneRef root2) {
        for (Mainline.Key.BoneRef ref : getCurrentKey().boneRefs) {
            if (ref.parent == root2 || root2 == null) {
                this.prevBBox.calcFor(this.unmappedTweenedKeys[ref.timeline].object(), this.animation.getTimeline(ref.timeline).objectInfo);
                Rectangle.setBiggerRectangle(this.rect, this.prevBBox.getBoundingRect(), this.rect);
                calcBoundingRectangle(ref);
            }
        }
        for (Mainline.Key.ObjectRef ref2 : getCurrentKey().objectRefs) {
            if (ref2.parent == root2) {
                this.prevBBox.calcFor(this.unmappedTweenedKeys[ref2.timeline].object(), this.animation.getTimeline(ref2.timeline).objectInfo);
                Rectangle.setBiggerRectangle(this.rect, this.prevBBox.getBoundingRect(), this.rect);
            }
        }
    }

    public Mainline.Key getCurrentKey() {
        return this.currentKey;
    }

    public int getTime() {
        return this.time;
    }

    public Player setTime(int time2) {
        this.time = time2;
        int prevSpeed = this.speed;
        this.speed = 0;
        increaseTime();
        this.speed = prevSpeed;
        return this;
    }

    public Player setScale(float scale) {
        this.root.scale.set(((float) flippedX()) * scale, ((float) flippedY()) * scale);
        return this;
    }

    public Player scale(float scale) {
        this.root.scale.scale(scale, scale);
        return this;
    }

    public float getScale() {
        return this.root.scale.x;
    }

    public Player flip(boolean x, boolean y) {
        if (x) {
            flipX();
        }
        if (y) {
            flipY();
        }
        return this;
    }

    public Player flipX() {
        this.root.scale.x *= -1.0f;
        return this;
    }

    public Player flipY() {
        this.root.scale.y *= -1.0f;
        return this;
    }

    public int flippedX() {
        return (int) Math.signum(this.root.scale.x);
    }

    public int flippedY() {
        return (int) Math.signum(this.root.scale.y);
    }

    public Player setPosition(float x, float y) {
        this.dirty = true;
        this.position.set(x, y);
        return this;
    }

    public Player setPosition(Point position2) {
        return setPosition(position2.x, position2.y);
    }

    public Player translatePosition(float x, float y) {
        return setPosition(this.position.x + x, this.position.y + y);
    }

    public Player translate(Point amount) {
        return translatePosition(amount.x, amount.y);
    }

    public float getX() {
        return this.position.x;
    }

    public float getY() {
        return this.position.y;
    }

    public Player setAngle(float angle2) {
        this.dirty = true;
        this.angle = angle2;
        return this;
    }

    public Player rotate(float angle2) {
        return setAngle(this.angle + angle2);
    }

    public float getAngle() {
        return this.angle;
    }

    public Player setPivot(float x, float y) {
        this.dirty = true;
        this.pivot.set(x, y);
        return this;
    }

    public Player setPivot(Point pivot2) {
        return setPivot(pivot2.x, pivot2.y);
    }

    public Player translatePivot(float x, float y) {
        return setPivot(this.pivot.x + x, this.pivot.y + y);
    }

    public Player translatePivot(Point amount) {
        return translatePivot(amount.x, amount.y);
    }

    public float getPivotX() {
        return this.pivot.x;
    }

    public float getPivotY() {
        return this.pivot.y;
    }

    public void addListener(PlayerListener listener) {
        this.listeners.add(listener);
    }

    public void removeListener(PlayerListener listener) {
        this.listeners.remove(listener);
    }

    public Iterator<Timeline.Key.Bone> boneIterator() {
        return boneIterator(getCurrentKey().boneRefs[0]);
    }

    public Iterator<Timeline.Key.Bone> boneIterator(Mainline.Key.BoneRef start) {
        this.boneIterator.index = start.id;
        return this.boneIterator;
    }

    public Iterator<Timeline.Key.Object> objectIterator() {
        return objectIterator(getCurrentKey().objectRefs[0]);
    }

    public Iterator<Timeline.Key.Object> objectIterator(Mainline.Key.ObjectRef start) {
        this.objectIterator.index = start.id;
        return this.objectIterator;
    }

    class ObjectIterator implements Iterator<Timeline.Key.Object> {
        int index = 0;

        ObjectIterator() {
        }

        public boolean hasNext() {
            return this.index < Player.this.getCurrentKey().objectRefs.length;
        }

        public Timeline.Key.Object next() {
            Timeline.Key[] keyArr = Player.this.unmappedTweenedKeys;
            Mainline.Key.ObjectRef[] objectRefArr = Player.this.getCurrentKey().objectRefs;
            int i = this.index;
            this.index = i + 1;
            return keyArr[objectRefArr[i].timeline].object();
        }

        public void remove() {
            throw new SpriterException("remove() is not supported by this iterator!");
        }
    }

    class BoneIterator implements Iterator<Timeline.Key.Bone> {
        int index = 0;

        BoneIterator() {
        }

        public boolean hasNext() {
            return this.index < Player.this.getCurrentKey().boneRefs.length;
        }

        public Timeline.Key.Bone next() {
            Timeline.Key[] keyArr = Player.this.unmappedTweenedKeys;
            Mainline.Key.BoneRef[] boneRefArr = Player.this.getCurrentKey().boneRefs;
            int i = this.index;
            this.index = i + 1;
            return keyArr[boneRefArr[i].timeline].object();
        }

        public void remove() {
            throw new SpriterException("remove() is not supported by this iterator!");
        }
    }

    public static abstract class Attachment extends Timeline.Key.Bone {
        private float angleTemp;
        private Timeline.Key.Bone parent;
        private final Point positionTemp = new Point();
        private final Point scaleTemp = new Point();

        /* access modifiers changed from: protected */
        public abstract void setAngle(float f);

        /* access modifiers changed from: protected */
        public abstract void setPosition(float f, float f2);

        /* access modifiers changed from: protected */
        public abstract void setScale(float f, float f2);

        public Attachment(Timeline.Key.Bone parent2) {
            setParent(parent2);
        }

        public void setParent(Timeline.Key.Bone parent2) {
            if (parent2 == null) {
                throw new SpriterException("The parent cannot be null!");
            }
            this.parent = parent2;
        }

        public Timeline.Key.Bone getParent() {
            return this.parent;
        }

        /* access modifiers changed from: package-private */
        public final void update() {
            this.positionTemp.set(this.position);
            this.scaleTemp.set(this.scale);
            this.angleTemp = this.angle;
            super.unmap(this.parent);
            setPosition(this.position.x, this.position.y);
            setScale(this.scale.x, this.scale.y);
            setAngle(this.angle);
            this.position.set(this.positionTemp);
            this.scale.set(this.scaleTemp);
            this.angle = this.angleTemp;
        }
    }
}
