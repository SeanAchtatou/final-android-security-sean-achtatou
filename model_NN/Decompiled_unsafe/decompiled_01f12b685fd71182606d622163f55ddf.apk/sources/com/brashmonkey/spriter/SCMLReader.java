package com.brashmonkey.spriter;

import com.brashmonkey.spriter.Entity;
import com.brashmonkey.spriter.Mainline;
import com.brashmonkey.spriter.Timeline;
import com.brashmonkey.spriter.XmlReader;
import com.kbz.esotericsoftware.spine.Animation;
import com.tendcloud.tenddata.dc;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class SCMLReader {
    protected Data data;

    public SCMLReader(InputStream stream) {
        this.data = load(stream);
    }

    public SCMLReader(String xml) {
        this.data = load(xml);
    }

    /* access modifiers changed from: protected */
    public Data load(String xml) {
        XmlReader.Element root = new XmlReader().parse(xml);
        ArrayList<XmlReader.Element> folders = root.getChildrenByName("folder");
        ArrayList<XmlReader.Element> entities = root.getChildrenByName("entity");
        this.data = new Data(root.get("scml_version"), root.get("generator"), root.get("generator_version"), folders.size(), entities.size());
        loadFolders(folders);
        loadEntities(entities);
        return this.data;
    }

    /* access modifiers changed from: protected */
    public Data load(InputStream stream) {
        try {
            XmlReader.Element root = new XmlReader().parse(stream);
            ArrayList<XmlReader.Element> folders = root.getChildrenByName("folder");
            ArrayList<XmlReader.Element> entities = root.getChildrenByName("entity");
            this.data = new Data(root.get("scml_version"), root.get("generator"), root.get("generator_version"), folders.size(), entities.size());
            loadFolders(folders);
            loadEntities(entities);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this.data;
    }

    /* access modifiers changed from: protected */
    public void loadFolders(ArrayList<XmlReader.Element> folders) {
        for (int i = 0; i < folders.size(); i++) {
            XmlReader.Element repo = folders.get(i);
            ArrayList<XmlReader.Element> files = repo.getChildrenByName("file");
            Folder folder = new Folder(repo.getInt(dc.V), repo.get("name", "no_name_" + i), files.size());
            loadFiles(files, folder);
            this.data.addFolder(folder);
        }
    }

    /* access modifiers changed from: protected */
    public void loadFiles(ArrayList<XmlReader.Element> files, Folder folder) {
        for (int j = 0; j < files.size(); j++) {
            XmlReader.Element f = files.get(j);
            folder.addFile(new File(f.getInt(dc.V), f.get("name"), new Dimension((float) f.getInt("width", 0), (float) f.getInt("height", 0)), new Point(f.getFloat("pivot_x", Animation.CurveTimeline.LINEAR), f.getFloat("pivot_y", 1.0f))));
        }
    }

    /* access modifiers changed from: protected */
    public void loadEntities(ArrayList<XmlReader.Element> entities) {
        for (int i = 0; i < entities.size(); i++) {
            XmlReader.Element e = entities.get(i);
            ArrayList<XmlReader.Element> infos = e.getChildrenByName("obj_info");
            ArrayList<XmlReader.Element> charMaps = e.getChildrenByName("character_map");
            ArrayList<XmlReader.Element> animations = e.getChildrenByName("animation");
            Entity entity = new Entity(e.getInt(dc.V), e.get("name"), animations.size(), charMaps.size(), infos.size());
            this.data.addEntity(entity);
            loadObjectInfos(infos, entity);
            loadCharacterMaps(charMaps, entity);
            loadAnimations(animations, entity);
        }
    }

    /* access modifiers changed from: protected */
    public void loadObjectInfos(ArrayList<XmlReader.Element> infos, Entity entity) {
        for (int i = 0; i < infos.size(); i++) {
            XmlReader.Element info = infos.get(i);
            Entity.ObjectInfo objInfo = new Entity.ObjectInfo(info.get("name", "info" + i), Entity.ObjectType.getObjectInfoFor(info.get("type", "")), new Dimension(info.getFloat("w", Animation.CurveTimeline.LINEAR), info.getFloat("h", Animation.CurveTimeline.LINEAR)));
            entity.addInfo(objInfo);
            XmlReader.Element frames = info.getChildByName("frames");
            if (frames != null) {
                Iterator i$ = frames.getChildrenByName("i").iterator();
                while (i$.hasNext()) {
                    XmlReader.Element index = i$.next();
                    objInfo.frames.add(new FileReference(index.getInt("folder", 0), index.getInt("file", 0)));
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void loadCharacterMaps(ArrayList<XmlReader.Element> maps, Entity entity) {
        for (int i = 0; i < maps.size(); i++) {
            XmlReader.Element map = maps.get(i);
            Entity.CharacterMap charMap = new Entity.CharacterMap(map.getInt(dc.V), map.getAttribute("name", "charMap" + i));
            entity.addCharacterMap(charMap);
            Iterator i$ = map.getChildrenByName("map").iterator();
            while (i$.hasNext()) {
                XmlReader.Element mapping = i$.next();
                int folder = mapping.getInt("folder");
                int file = mapping.getInt("file");
                charMap.put(new FileReference(folder, file), new FileReference(mapping.getInt("target_folder", folder), mapping.getInt("target_file", file)));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void loadAnimations(ArrayList<XmlReader.Element> animations, Entity entity) {
        for (int i = 0; i < animations.size(); i++) {
            XmlReader.Element a = animations.get(i);
            ArrayList<XmlReader.Element> timelines = a.getChildrenByName("timeline");
            ArrayList<XmlReader.Element> mainlineKeys = a.getChildByName("mainline").getChildrenByName("key");
            Animation animation = new Animation(new Mainline(mainlineKeys.size()), a.getInt(dc.V), a.get("name"), a.getInt("length"), a.getBoolean("looping", true), timelines.size());
            entity.addAnimation(animation);
            loadMainlineKeys(mainlineKeys, animation.mainline);
            loadTimelines(timelines, animation, entity);
            animation.prepare();
        }
    }

    /* access modifiers changed from: protected */
    public void loadMainlineKeys(ArrayList<XmlReader.Element> keys, Mainline main) {
        for (int i = 0; i < main.keys.length; i++) {
            XmlReader.Element k = keys.get(i);
            ArrayList<XmlReader.Element> objectRefs = k.getChildrenByName("object_ref");
            ArrayList<XmlReader.Element> boneRefs = k.getChildrenByName("bone_ref");
            Curve curve = new Curve();
            curve.setType(Curve.getType(k.get("curve_type", "linear")));
            curve.constraints.set(k.getFloat("c1", Animation.CurveTimeline.LINEAR), k.getFloat("c2", Animation.CurveTimeline.LINEAR), k.getFloat("c3", Animation.CurveTimeline.LINEAR), k.getFloat("c4", Animation.CurveTimeline.LINEAR));
            Mainline.Key key = new Mainline.Key(k.getInt(dc.V), k.getInt("time", 0), curve, boneRefs.size(), objectRefs.size());
            main.addKey(key);
            loadRefs(objectRefs, boneRefs, key);
        }
    }

    /* access modifiers changed from: protected */
    public void loadRefs(ArrayList<XmlReader.Element> objectRefs, ArrayList<XmlReader.Element> boneRefs, Mainline.Key key) {
        Iterator i$ = boneRefs.iterator();
        while (i$.hasNext()) {
            XmlReader.Element e = i$.next();
            key.addBoneRef(new Mainline.Key.BoneRef(e.getInt(dc.V), e.getInt("timeline"), e.getInt("key"), key.getBoneRef(e.getInt("parent", -1))));
        }
        Iterator i$2 = objectRefs.iterator();
        while (i$2.hasNext()) {
            XmlReader.Element o = i$2.next();
            key.addObjectRef(new Mainline.Key.ObjectRef(o.getInt(dc.V), o.getInt("timeline"), o.getInt("key"), key.getBoneRef(o.getInt("parent", -1)), o.getInt("z_index", 0)));
        }
        Arrays.sort(key.objectRefs);
    }

    /* access modifiers changed from: protected */
    public void loadTimelines(ArrayList<XmlReader.Element> timelines, Animation animation, Entity entity) {
        for (int i = 0; i < timelines.size(); i++) {
            XmlReader.Element t = timelines.get(i);
            ArrayList<XmlReader.Element> keys = timelines.get(i).getChildrenByName("key");
            String name = t.get("name");
            Entity.ObjectType type = Entity.ObjectType.getObjectInfoFor(t.get("object_type", "sprite"));
            Entity.ObjectInfo info = entity.getInfo(name);
            if (info == null) {
                info = new Entity.ObjectInfo(name, type, new Dimension(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR));
            }
            Timeline timeline = new Timeline(t.getInt(dc.V), name, info, keys.size());
            animation.addTimeline(timeline);
            loadTimelineKeys(keys, timeline);
        }
    }

    /* access modifiers changed from: protected */
    public void loadTimelineKeys(ArrayList<XmlReader.Element> keys, Timeline timeline) {
        Timeline.Key.Object object;
        for (int i = 0; i < keys.size(); i++) {
            XmlReader.Element k = keys.get(i);
            Curve curve = new Curve();
            curve.setType(Curve.getType(k.get("curve_type", "linear")));
            curve.constraints.set(k.getFloat("c1", Animation.CurveTimeline.LINEAR), k.getFloat("c2", Animation.CurveTimeline.LINEAR), k.getFloat("c3", Animation.CurveTimeline.LINEAR), k.getFloat("c4", Animation.CurveTimeline.LINEAR));
            Timeline.Key key = new Timeline.Key(k.getInt(dc.V), k.getInt("time", 0), k.getInt("spin", 1), curve);
            XmlReader.Element obj = k.getChildByName("bone");
            if (obj == null) {
                obj = k.getChildByName("object");
            }
            Point position = new Point(obj.getFloat("x", Animation.CurveTimeline.LINEAR), obj.getFloat("y", Animation.CurveTimeline.LINEAR));
            Point scale = new Point(obj.getFloat("scale_x", 1.0f), obj.getFloat("scale_y", 1.0f));
            Point pivot = new Point(obj.getFloat("pivot_x", Animation.CurveTimeline.LINEAR), obj.getFloat("pivot_y", timeline.objectInfo.type == Entity.ObjectType.Bone ? 0.5f : 1.0f));
            float angle = obj.getFloat("angle", Animation.CurveTimeline.LINEAR);
            float alpha = 1.0f;
            int folder = -1;
            int file = -1;
            if (obj.getName().equals("object") && timeline.objectInfo.type == Entity.ObjectType.Sprite) {
                alpha = obj.getFloat("a", 1.0f);
                folder = obj.getInt("folder", -1);
                file = obj.getInt("file", -1);
                File f = this.data.getFolder(folder).getFile(file);
                pivot = new Point(obj.getFloat("pivot_x", f.pivot.x), obj.getFloat("pivot_y", f.pivot.y));
                timeline.objectInfo.size.set(f.size);
            }
            if (obj.getName().equals("bone")) {
                object = new Timeline.Key.Object(position, scale, pivot, angle, alpha, new FileReference(folder, file));
            } else {
                object = new Timeline.Key.Object(position, scale, pivot, angle, alpha, new FileReference(folder, file));
            }
            key.setObject(object);
            timeline.addKey(key);
        }
    }

    public Data getData() {
        return this.data;
    }
}
