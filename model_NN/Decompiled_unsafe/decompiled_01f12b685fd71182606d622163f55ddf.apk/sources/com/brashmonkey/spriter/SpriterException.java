package com.brashmonkey.spriter;

public class SpriterException extends RuntimeException {
    private static final long serialVersionUID = 1;

    public SpriterException(String message) {
        super(message);
    }
}
