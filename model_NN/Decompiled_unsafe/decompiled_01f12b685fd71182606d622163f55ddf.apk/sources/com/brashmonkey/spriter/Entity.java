package com.brashmonkey.spriter;

import com.kbz.esotericsoftware.spine.Animation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Entity {
    private int animationPointer = 0;
    private final Animation[] animations;
    private int charMapPointer = 0;
    private final CharacterMap[] characterMaps;
    public final int id;
    public final String name;
    private final HashMap<String, Animation> namedAnimations;
    private int objInfoPointer = 0;
    private final ObjectInfo[] objectInfos;

    Entity(int id2, String name2, int animations2, int characterMaps2, int objectInfos2) {
        this.id = id2;
        this.name = name2;
        this.animations = new Animation[animations2];
        this.characterMaps = new CharacterMap[characterMaps2];
        this.objectInfos = new ObjectInfo[objectInfos2];
        this.namedAnimations = new HashMap<>();
    }

    /* access modifiers changed from: package-private */
    public void addAnimation(Animation anim) {
        Animation[] animationArr = this.animations;
        int i = this.animationPointer;
        this.animationPointer = i + 1;
        animationArr[i] = anim;
        this.namedAnimations.put(anim.name, anim);
    }

    public Animation getAnimation(int index) {
        return this.animations[index];
    }

    public Animation getAnimation(String name2) {
        return this.namedAnimations.get(name2);
    }

    public int animations() {
        return this.animations.length;
    }

    public boolean containsAnimation(Animation anim) {
        for (Animation a : this.animations) {
            if (a == anim) {
                return true;
            }
        }
        return false;
    }

    public Animation getAnimationWithMostTimelines() {
        Animation maxAnim = getAnimation(0);
        for (Animation anim : this.animations) {
            if (maxAnim.timelines() < anim.timelines()) {
                maxAnim = anim;
            }
        }
        return maxAnim;
    }

    public CharacterMap getCharacterMap(String name2) {
        for (CharacterMap map : this.characterMaps) {
            if (map.name.equals(name2)) {
                return map;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void addCharacterMap(CharacterMap map) {
        CharacterMap[] characterMapArr = this.characterMaps;
        int i = this.charMapPointer;
        this.charMapPointer = i + 1;
        characterMapArr[i] = map;
    }

    /* access modifiers changed from: package-private */
    public void addInfo(ObjectInfo info) {
        ObjectInfo[] objectInfoArr = this.objectInfos;
        int i = this.objInfoPointer;
        this.objInfoPointer = i + 1;
        objectInfoArr[i] = info;
    }

    public ObjectInfo getInfo(int index) {
        return this.objectInfos[index];
    }

    public ObjectInfo getInfo(String name2) {
        for (ObjectInfo info : this.objectInfos) {
            if (info.name.equals(name2)) {
                return info;
            }
        }
        return null;
    }

    public ObjectInfo getInfo(String name2, ObjectType type) {
        ObjectInfo info = getInfo(name2);
        if (info == null || info.type != type) {
            return null;
        }
        return info;
    }

    public enum ObjectType {
        Sprite,
        Bone,
        Box,
        Point,
        Skin;

        public static ObjectType getObjectInfoFor(String name) {
            if (name.equals("bone")) {
                return Bone;
            }
            if (name.equals("skin")) {
                return Skin;
            }
            if (name.equals("box")) {
                return Box;
            }
            if (name.equals("point")) {
                return Point;
            }
            return Sprite;
        }
    }

    public static class ObjectInfo {
        public final List<FileReference> frames;
        public final String name;
        public final Dimension size;
        public final ObjectType type;

        ObjectInfo(String name2, ObjectType type2, Dimension size2, List<FileReference> frames2) {
            this.type = type2;
            this.frames = frames2;
            this.name = name2;
            this.size = size2;
        }

        ObjectInfo(String name2, ObjectType type2, Dimension size2) {
            this(name2, type2, size2, new ArrayList());
        }

        ObjectInfo(String name2, ObjectType type2, List<FileReference> frames2) {
            this(name2, type2, new Dimension(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR), frames2);
        }

        public String toString() {
            return this.name + ": " + this.type + ", size: " + this.size + "|frames:\n" + this.frames;
        }
    }

    public static class CharacterMap extends HashMap<FileReference, FileReference> {
        private static final long serialVersionUID = 6062776450159802283L;
        public final int id;
        public final String name;

        public CharacterMap(int id2, String name2) {
            this.id = id2;
            this.name = name2;
        }

        public FileReference get(FileReference key) {
            return !super.containsKey(key) ? key : (FileReference) super.get((Object) key);
        }
    }

    /* JADX INFO: Multiple debug info for r1v1 com.brashmonkey.spriter.Entity$CharacterMap[]: [D('arr$' com.brashmonkey.spriter.Entity$ObjectInfo[]), D('arr$' com.brashmonkey.spriter.Entity$CharacterMap[])] */
    /* JADX INFO: Multiple debug info for r1v2 com.brashmonkey.spriter.Animation[]: [D('arr$' com.brashmonkey.spriter.Entity$CharacterMap[]), D('arr$' com.brashmonkey.spriter.Animation[])] */
    public String toString() {
        String toReturn = (getClass().getSimpleName() + "|[id: " + this.id + ", name: " + this.name + "]") + "Object infos:\n";
        ObjectInfo[] arr$ = this.objectInfos;
        for (int i$ = 0; i$ < arr$.length; i$++) {
            toReturn = toReturn + "\n" + arr$[i$];
        }
        String toReturn2 = toReturn + "Character maps:\n";
        CharacterMap[] arr$2 = this.characterMaps;
        for (int i$2 = 0; i$2 < arr$2.length; i$2++) {
            toReturn2 = toReturn2 + "\n" + arr$2[i$2];
        }
        String toReturn3 = toReturn2 + "Animations:\n";
        Animation[] arr$3 = this.animations;
        for (int i$3 = 0; i$3 < arr$3.length; i$3++) {
            toReturn3 = toReturn3 + "\n" + arr$3[i$3];
        }
        return toReturn3 + "]";
    }
}
