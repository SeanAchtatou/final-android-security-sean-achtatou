package com.brashmonkey.spriter;

public class Folder {
    private int filePointer = 0;
    final File[] files;
    public final int id;
    public final String name;

    Folder(int id2, String name2, int files2) {
        this.id = id2;
        this.name = name2;
        this.files = new File[files2];
    }

    /* access modifiers changed from: package-private */
    public void addFile(File file) {
        File[] fileArr = this.files;
        int i = this.filePointer;
        this.filePointer = i + 1;
        fileArr[i] = file;
    }

    public File getFile(int index) {
        return this.files[index];
    }

    public File getFile(String name2) {
        int index = getFileIndex(name2);
        if (index >= 0) {
            return getFile(index);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public int getFileIndex(String name2) {
        for (File file : this.files) {
            if (file.name.equals(name2)) {
                return file.id;
            }
        }
        return -1;
    }

    public String toString() {
        String toReturn = getClass().getSimpleName() + "|[id: " + this.id + ", name: " + this.name;
        File[] arr$ = this.files;
        for (int i$ = 0; i$ < arr$.length; i$++) {
            toReturn = toReturn + "\n" + arr$[i$];
        }
        return toReturn + "]";
    }
}
