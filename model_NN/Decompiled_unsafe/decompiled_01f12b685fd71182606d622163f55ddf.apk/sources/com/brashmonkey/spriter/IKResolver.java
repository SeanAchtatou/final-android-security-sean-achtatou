package com.brashmonkey.spriter;

import com.brashmonkey.spriter.Mainline;
import com.brashmonkey.spriter.Timeline;
import java.util.HashMap;
import java.util.Map;

public abstract class IKResolver {
    protected HashMap<IKObject, Mainline.Key.BoneRef> ikMap = new HashMap<>();
    protected Player player;
    protected float tolerance = 5.0f;

    /* access modifiers changed from: protected */
    public abstract void resolve(float f, float f2, int i, Mainline.Key.BoneRef boneRef);

    public IKResolver(Player player2) {
        setPlayer(player2);
    }

    public void setPlayer(Player player2) {
        if (player2 == null) {
            throw new SpriterException("player cannot be null!");
        }
        this.player = player2;
    }

    public Player getPlayer() {
        return this.player;
    }

    public void resolve() {
        for (Map.Entry<IKObject, Mainline.Key.BoneRef> entry : this.ikMap.entrySet()) {
            for (int j = 0; j < ((IKObject) entry.getKey()).iterations; j++) {
                resolve(((IKObject) entry.getKey()).x, ((IKObject) entry.getKey()).y, ((IKObject) entry.getKey()).chainLength, (Mainline.Key.BoneRef) entry.getValue());
            }
        }
    }

    public void mapIKObject(IKObject ikObject, Mainline.Key.BoneRef boneRef) {
        this.ikMap.put(ikObject, boneRef);
    }

    public void mapIKObject(IKObject ikObject, Timeline.Key.Bone bone) {
        this.ikMap.put(ikObject, this.player.getBoneRef(bone));
    }

    public void unmapIKObject(IKObject ikObject) {
        this.ikMap.remove(ikObject);
    }

    public float getTolerance() {
        return this.tolerance;
    }

    public void setTolerance(float tolerance2) {
        this.tolerance = tolerance2;
    }
}
