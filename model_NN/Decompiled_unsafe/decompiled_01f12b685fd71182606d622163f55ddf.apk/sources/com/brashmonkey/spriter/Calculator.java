package com.brashmonkey.spriter;

import com.kbz.esotericsoftware.spine.Animation;

public class Calculator {
    public static final float PI = 3.1415927f;
    private static final int SIN_BITS = 14;
    private static final int SIN_COUNT = 16384;
    private static final int SIN_MASK = 16383;
    private static final float degFull = 360.0f;
    public static final float degRad = 0.017453292f;
    private static final float degToIndex = 45.511112f;
    public static final float degreesToRadians = 0.017453292f;
    public static final float radDeg = 57.295776f;
    private static final float radFull = 6.2831855f;
    private static final float radToIndex = 2607.5945f;
    public static final float radiansToDegrees = 57.295776f;

    public static float angleDifference(float a, float b) {
        return ((((a - b) % degFull) + 540.0f) % degFull) - 180.0f;
    }

    public static float angleBetween(float x1, float y1, float x2, float y2) {
        return (float) Math.toDegrees(Math.atan2((double) (y2 - y1), (double) (x2 - x1)));
    }

    public static float distanceBetween(float x1, float y1, float x2, float y2) {
        float xDiff = x2 - x1;
        float yDiff = y2 - y1;
        return sqrt((xDiff * xDiff) + (yDiff * yDiff));
    }

    public static Float solveCubic(float a, float b, float c, float d) {
        if (a == Animation.CurveTimeline.LINEAR) {
            return solveQuadratic(b, c, d);
        }
        if (d == Animation.CurveTimeline.LINEAR) {
            return Float.valueOf((float) Animation.CurveTimeline.LINEAR);
        }
        float b2 = b / a;
        float c2 = c / a;
        float squaredB = squared(b2);
        float q = ((3.0f * c2) - squaredB) / 9.0f;
        float r = ((-27.0f * (d / a)) + (((9.0f * c2) - (2.0f * squaredB)) * b2)) / 54.0f;
        float disc = cubed(q) + squared(r);
        float term1 = b2 / 3.0f;
        if (disc > Animation.CurveTimeline.LINEAR) {
            float s = r + sqrt(disc);
            float s2 = s < Animation.CurveTimeline.LINEAR ? -cubicRoot(-s) : cubicRoot(s);
            float t = r - sqrt(disc);
            float result = (-term1) + s2 + (t < Animation.CurveTimeline.LINEAR ? -cubicRoot(-t) : cubicRoot(t));
            if (result >= Animation.CurveTimeline.LINEAR && result <= 1.0f) {
                return Float.valueOf(result);
            }
        } else if (disc == Animation.CurveTimeline.LINEAR) {
            float r13 = r < Animation.CurveTimeline.LINEAR ? -cubicRoot(-r) : cubicRoot(r);
            float result2 = (-term1) + (2.0f * r13);
            if (result2 >= Animation.CurveTimeline.LINEAR && result2 <= 1.0f) {
                return Float.valueOf(result2);
            }
            float result3 = -(r13 + term1);
            if (result3 >= Animation.CurveTimeline.LINEAR && result3 <= 1.0f) {
                return Float.valueOf(result3);
            }
        } else {
            float q2 = -q;
            float dum1 = acos(r / sqrt((q2 * q2) * q2));
            float r132 = 2.0f * sqrt(q2);
            float result4 = (-term1) + (cos(dum1 / 3.0f) * r132);
            if (result4 >= Animation.CurveTimeline.LINEAR && result4 <= 1.0f) {
                return Float.valueOf(result4);
            }
            float result5 = (-term1) + (cos((6.2831855f + dum1) / 3.0f) * r132);
            if (result5 >= Animation.CurveTimeline.LINEAR && result5 <= 1.0f) {
                return Float.valueOf(result5);
            }
            float result6 = (-term1) + (cos((12.566371f + dum1) / 3.0f) * r132);
            if (result6 >= Animation.CurveTimeline.LINEAR && result6 <= 1.0f) {
                return Float.valueOf(result6);
            }
        }
        return null;
    }

    public static Float solveQuadratic(float a, float b, float c) {
        float squaredB = squared(b);
        float twoA = 2.0f * a;
        float fourAC = 4.0f * a * c;
        float result = ((-b) + sqrt(squaredB - fourAC)) / twoA;
        if (result >= Animation.CurveTimeline.LINEAR && result <= 1.0f) {
            return Float.valueOf(result);
        }
        float result2 = ((-b) - sqrt(squaredB - fourAC)) / twoA;
        if (result2 < Animation.CurveTimeline.LINEAR || result2 > 1.0f) {
            return null;
        }
        return Float.valueOf(result2);
    }

    public static float squared(float f) {
        return f * f;
    }

    public static float cubed(float f) {
        return f * f * f;
    }

    public static float cubicRoot(float f) {
        return (float) Math.pow((double) f, 0.3333333432674408d);
    }

    public static float sqrt(float x) {
        return (float) Math.sqrt((double) x);
    }

    public static float acos(float x) {
        return (float) Math.acos((double) x);
    }

    private static class Sin {
        static final float[] table = new float[16384];

        private Sin() {
        }

        static {
            for (int i = 0; i < 16384; i++) {
                table[i] = (float) Math.sin((double) (((((float) i) + 0.5f) / 16384.0f) * 6.2831855f));
            }
            for (int i2 = 0; i2 < 360; i2 += 90) {
                table[((int) (((float) i2) * Calculator.degToIndex)) & Calculator.SIN_MASK] = (float) Math.sin((double) (((float) i2) * 0.017453292f));
            }
        }
    }

    public static final float sin(float radians) {
        return Sin.table[((int) (radToIndex * radians)) & SIN_MASK];
    }

    public static final float cos(float radians) {
        return Sin.table[((int) ((1.5707964f + radians) * radToIndex)) & SIN_MASK];
    }

    public static final float sinDeg(float degrees) {
        return Sin.table[((int) (degToIndex * degrees)) & SIN_MASK];
    }

    public static final float cosDeg(float degrees) {
        return Sin.table[((int) ((90.0f + degrees) * degToIndex)) & SIN_MASK];
    }
}
