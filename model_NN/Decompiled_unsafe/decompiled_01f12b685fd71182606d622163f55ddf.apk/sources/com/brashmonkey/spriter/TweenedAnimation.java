package com.brashmonkey.spriter;

import com.brashmonkey.spriter.Mainline;
import com.brashmonkey.spriter.Timeline;

public class TweenedAnimation extends Animation {
    private Animation anim1;
    private Animation anim2;
    Mainline.Key.BoneRef base = null;
    public Animation baseAnimation;
    public final Curve curve;
    public final Entity entity;
    public float spriteThreshold = 0.5f;
    public boolean tweenSprites = false;
    public float weight = 0.5f;

    public TweenedAnimation(Entity entity2) {
        super(new Mainline(0), -1, "__interpolatedAnimation__", 0, true, entity2.getAnimationWithMostTimelines().timelines());
        this.entity = entity2;
        this.curve = new Curve();
        setUpTimelines();
    }

    public Mainline.Key getCurrentKey() {
        return this.currentKey;
    }

    /* JADX INFO: Multiple debug info for r1v2 com.brashmonkey.spriter.Mainline$Key$BoneRef[]: [D('arr$' com.brashmonkey.spriter.Mainline$Key$BoneRef[]), D('arr$' com.brashmonkey.spriter.Timeline$Key[])] */
    public void update(int time, Timeline.Key.Bone root) {
        this.currentKey = onFirstMainLine() ? this.anim1.currentKey : this.anim2.currentKey;
        for (Timeline.Key timelineKey : this.unmappedTweenedKeys) {
            timelineKey.active = false;
        }
        if (this.base != null) {
            Animation currentAnim = onFirstMainLine() ? this.anim1 : this.anim2;
            Animation baseAnim = this.baseAnimation == null ? onFirstMainLine() ? this.anim1 : this.anim2 : this.baseAnimation;
            for (Mainline.Key.BoneRef ref : this.currentKey.boneRefs) {
                Timeline timeline = baseAnim.getSimilarTimeline(currentAnim.getTimeline(ref.timeline));
                if (timeline != null) {
                    Timeline.Key key = baseAnim.tweenedKeys[timeline.id];
                    Timeline.Key mappedKey = baseAnim.unmappedTweenedKeys[timeline.id];
                    this.tweenedKeys[ref.timeline].active = key.active;
                    this.tweenedKeys[ref.timeline].object().set(key.object());
                    this.unmappedTweenedKeys[ref.timeline].active = mappedKey.active;
                    unmapTimelineObject(ref.timeline, false, ref.parent != null ? this.unmappedTweenedKeys[ref.parent.timeline].object() : root);
                }
            }
        }
        tweenBoneRefs(this.base, root);
        for (Mainline.Key.ObjectRef ref2 : this.currentKey.objectRefs) {
            update(ref2, root, 0);
        }
    }

    private void tweenBoneRefs(Mainline.Key.BoneRef base2, Timeline.Key.Bone root) {
        int startIndex = base2 == null ? -1 : base2.id - 1;
        int length = this.currentKey.boneRefs.length;
        for (int i = startIndex + 1; i < length; i++) {
            Mainline.Key.BoneRef ref = this.currentKey.boneRefs[i];
            if (base2 == ref || ref.parent == base2) {
                update(ref, root, 0);
            }
            if (base2 == ref.parent) {
                tweenBoneRefs(ref, root);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void update(Mainline.Key.BoneRef ref, Timeline.Key.Bone root, int time) {
        boolean isObject = ref instanceof Mainline.Key.ObjectRef;
        Timeline.Key.Bone bone1 = null;
        Timeline.Key.Bone bone2 = null;
        Timeline.Key.Bone tweenTarget = null;
        Timeline t1 = onFirstMainLine() ? this.anim1.getTimeline(ref.timeline) : this.anim1.getSimilarTimeline(this.anim2.getTimeline(ref.timeline));
        Timeline t2 = onFirstMainLine() ? this.anim2.getSimilarTimeline(t1) : this.anim2.getTimeline(ref.timeline);
        Timeline targetTimeline = super.getTimeline(onFirstMainLine() ? t1.id : t2.id);
        if (t1 != null) {
            bone1 = this.anim1.tweenedKeys[t1.id].object();
        }
        if (t2 != null) {
            bone2 = this.anim2.tweenedKeys[t2.id].object();
        }
        if (targetTimeline != null) {
            tweenTarget = this.tweenedKeys[targetTimeline.id].object();
        }
        if (isObject && (t2 == null || !this.tweenSprites)) {
            if (!onFirstMainLine()) {
                bone1 = bone2;
            } else {
                bone2 = bone1;
            }
        }
        if (!(bone2 == null || tweenTarget == null || bone1 == null)) {
            if (isObject) {
                tweenObject((Timeline.Key.Object) bone1, (Timeline.Key.Object) bone2, (Timeline.Key.Object) tweenTarget, this.weight, this.curve);
            } else {
                tweenBone(bone1, bone2, tweenTarget, this.weight, this.curve);
            }
            this.unmappedTweenedKeys[targetTimeline.id].active = true;
        }
        if (this.unmappedTweenedKeys[ref.timeline].active) {
            int i = targetTimeline.id;
            if (ref.parent != null) {
                root = this.unmappedTweenedKeys[ref.parent.timeline].object();
            }
            unmapTimelineObject(i, isObject, root);
        }
    }

    private void tweenBone(Timeline.Key.Bone bone1, Timeline.Key.Bone bone2, Timeline.Key.Bone target, float t, Curve curve2) {
        target.angle = curve2.tweenAngle(bone1.angle, bone2.angle, t);
        curve2.tweenPoint(bone1.position, bone2.position, t, target.position);
        curve2.tweenPoint(bone1.scale, bone2.scale, t, target.scale);
        curve2.tweenPoint(bone1.pivot, bone2.pivot, t, target.pivot);
    }

    private void tweenObject(Timeline.Key.Object object1, Timeline.Key.Object object2, Timeline.Key.Object target, float t, Curve curve2) {
        tweenBone(object1, object2, target, t, curve2);
        target.alpha = curve2.tweenAngle(object1.alpha, object2.alpha, t);
        target.ref.set(object1.ref);
    }

    public boolean onFirstMainLine() {
        return this.weight < this.spriteThreshold;
    }

    private void setUpTimelines() {
        Animation maxAnim = this.entity.getAnimationWithMostTimelines();
        int max = maxAnim.timelines();
        for (int i = 0; i < max; i++) {
            addTimeline(new Timeline(i, maxAnim.getTimeline(i).name, maxAnim.getTimeline(i).objectInfo, 1));
        }
        prepare();
    }

    public void setAnimations(Animation animation1, Animation animation2) {
        boolean areInterpolated = (animation1 instanceof TweenedAnimation) || (animation2 instanceof TweenedAnimation);
        if (animation1 != this.anim1 || animation2 != this.anim2) {
            if ((!this.entity.containsAnimation(animation1) || !this.entity.containsAnimation(animation2)) && !areInterpolated) {
                throw new SpriterException("Both animations have to be part of the same entity!");
            }
            this.anim1 = animation1;
            this.anim2 = animation2;
        }
    }

    public Animation getFirstAnimation() {
        return this.anim1;
    }

    public Animation getSecondAnimation() {
        return this.anim2;
    }
}
