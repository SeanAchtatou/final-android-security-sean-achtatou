package com.brashmonkey.spriter;

import com.brashmonkey.spriter.Mainline;
import com.brashmonkey.spriter.Timeline;

public class CCDResolver extends IKResolver {
    public CCDResolver(Player player) {
        super(player);
    }

    public void resolve(float x, float y, int chainLength, Mainline.Key.BoneRef effectorRef) {
        Timeline.Key.Bone parent;
        Timeline timeline = this.player.animation.getTimeline(effectorRef.timeline);
        Timeline.Key key = this.player.tweenedKeys[effectorRef.timeline];
        Timeline.Key unmappedKey = this.player.unmappedTweenedKeys[effectorRef.timeline];
        Timeline.Key.Bone effector = key.object();
        Timeline.Key.Bone unmappedffector = unmappedKey.object();
        float width = (timeline.objectInfo != null ? timeline.objectInfo.size.width : 200.0f) * unmappedffector.scale.x;
        float xx = unmappedffector.position.x + (((float) Math.cos(Math.toRadians((double) unmappedffector.angle))) * width);
        float yy = unmappedffector.position.y + (((float) Math.sin(Math.toRadians((double) unmappedffector.angle))) * width);
        if (Calculator.distanceBetween(xx, yy, x, y) > this.tolerance) {
            effector.angle = Calculator.angleBetween(unmappedffector.position.x, unmappedffector.position.y, x, y);
            if (Math.signum(this.player.root.scale.x) == -1.0f) {
                effector.angle += 180.0f;
            }
            Mainline.Key.BoneRef parentRef = effectorRef.parent;
            Timeline.Key.Bone parent2 = null;
            Timeline.Key.Bone unmappedParent = null;
            if (parentRef != null) {
                parent2 = this.player.tweenedKeys[parentRef.timeline].object();
                unmappedParent = this.player.unmappedTweenedKeys[parentRef.timeline].object();
                effector.angle -= unmappedParent.angle;
            }
            this.player.unmapObjects(null);
            for (int i = 0; i < chainLength && parentRef != null && Calculator.distanceBetween(xx, yy, x, y) > this.tolerance; i++) {
                parent.angle += Calculator.angleDifference(Calculator.angleBetween(unmappedParent.position.x, unmappedParent.position.y, x, y), Calculator.angleBetween(unmappedParent.position.x, unmappedParent.position.y, xx, yy));
                parentRef = parentRef.parent;
                if (parentRef == null || i >= chainLength - 1) {
                    parent = null;
                } else {
                    parent = this.player.tweenedKeys[parentRef.timeline].object();
                    unmappedParent = this.player.unmappedTweenedKeys[parentRef.timeline].object();
                    parent.angle -= unmappedParent.angle;
                }
                this.player.unmapObjects(null);
                xx = unmappedffector.position.x + (((float) Math.cos(Math.toRadians((double) unmappedffector.angle))) * width);
                yy = unmappedffector.position.y + (((float) Math.sin(Math.toRadians((double) unmappedffector.angle))) * width);
            }
        }
    }
}
