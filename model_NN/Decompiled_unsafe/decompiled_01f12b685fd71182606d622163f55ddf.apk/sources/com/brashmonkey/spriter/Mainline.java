package com.brashmonkey.spriter;

public class Mainline {
    private int keyPointer = 0;
    final Key[] keys;

    public Mainline(int keys2) {
        this.keys = new Key[keys2];
    }

    public String toString() {
        String toReturn = getClass().getSimpleName() + "|";
        Key[] arr$ = this.keys;
        for (int i$ = 0; i$ < arr$.length; i$++) {
            toReturn = toReturn + "\n" + arr$[i$];
        }
        return toReturn + "]";
    }

    public void addKey(Key key) {
        Key[] keyArr = this.keys;
        int i = this.keyPointer;
        this.keyPointer = i + 1;
        keyArr[i] = key;
    }

    public Key getKey(int index) {
        return this.keys[index];
    }

    public Key getKeyBeforeTime(int time) {
        Key found = this.keys[0];
        for (Key key : this.keys) {
            if (key.time > time) {
                break;
            }
            found = key;
        }
        return found;
    }

    public static class Key {
        private int bonePointer = 0;
        final BoneRef[] boneRefs;
        public final Curve curve;
        public final int id;
        private int objectPointer = 0;
        final ObjectRef[] objectRefs;
        public final int time;

        public Key(int id2, int time2, Curve curve2, int boneRefs2, int objectRefs2) {
            this.id = id2;
            this.time = time2;
            this.curve = curve2;
            this.boneRefs = new BoneRef[boneRefs2];
            this.objectRefs = new ObjectRef[objectRefs2];
        }

        public void addBoneRef(BoneRef ref) {
            BoneRef[] boneRefArr = this.boneRefs;
            int i = this.bonePointer;
            this.bonePointer = i + 1;
            boneRefArr[i] = ref;
        }

        public void addObjectRef(ObjectRef ref) {
            ObjectRef[] objectRefArr = this.objectRefs;
            int i = this.objectPointer;
            this.objectPointer = i + 1;
            objectRefArr[i] = ref;
        }

        public BoneRef getBoneRef(int index) {
            if (index < 0 || index >= this.boneRefs.length) {
                return null;
            }
            return this.boneRefs[index];
        }

        public ObjectRef getObjectRef(int index) {
            if (index < 0 || index >= this.objectRefs.length) {
                return null;
            }
            return this.objectRefs[index];
        }

        public BoneRef getBoneRef(BoneRef ref) {
            return getBoneRefTimeline(ref.timeline);
        }

        public BoneRef getBoneRefTimeline(int timeline) {
            for (BoneRef boneRef : this.boneRefs) {
                if (boneRef.timeline == timeline) {
                    return boneRef;
                }
            }
            return null;
        }

        public ObjectRef getObjectRef(ObjectRef ref) {
            return getObjectRefTimeline(ref.timeline);
        }

        public ObjectRef getObjectRefTimeline(int timeline) {
            for (ObjectRef objRef : this.objectRefs) {
                if (objRef.timeline == timeline) {
                    return objRef;
                }
            }
            return null;
        }

        /* JADX INFO: Multiple debug info for r0v1 com.brashmonkey.spriter.Mainline$Key$ObjectRef[]: [D('arr$' com.brashmonkey.spriter.Mainline$Key$ObjectRef[]), D('arr$' com.brashmonkey.spriter.Mainline$Key$BoneRef[])] */
        public String toString() {
            String toReturn = getClass().getSimpleName() + "|[id:" + this.id + ", time: " + this.time + ", curve: [" + this.curve + "]";
            BoneRef[] arr$ = this.boneRefs;
            for (int i$ = 0; i$ < arr$.length; i$++) {
                toReturn = toReturn + "\n" + arr$[i$];
            }
            ObjectRef[] arr$2 = this.objectRefs;
            for (int i$2 = 0; i$2 < arr$2.length; i$2++) {
                toReturn = toReturn + "\n" + arr$2[i$2];
            }
            return toReturn + "]";
        }

        public static class BoneRef {
            public final int id;
            public final int key;
            public final BoneRef parent;
            public final int timeline;

            public BoneRef(int id2, int timeline2, int key2, BoneRef parent2) {
                this.id = id2;
                this.timeline = timeline2;
                this.key = key2;
                this.parent = parent2;
            }

            public String toString() {
                return getClass().getSimpleName() + "|id: " + this.id + ", parent:" + (this.parent != null ? this.parent.id : -1) + ", timeline: " + this.timeline + ", key: " + this.key;
            }
        }

        public static class ObjectRef extends BoneRef implements Comparable<ObjectRef> {
            public final int zIndex;

            public ObjectRef(int id, int timeline, int key, BoneRef parent, int zIndex2) {
                super(id, timeline, key, parent);
                this.zIndex = zIndex2;
            }

            public String toString() {
                return super.toString() + ", z_index: " + this.zIndex;
            }

            public int compareTo(ObjectRef o) {
                return (int) Math.signum((float) (this.zIndex - o.zIndex));
            }
        }
    }
}
