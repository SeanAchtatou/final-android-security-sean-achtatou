package com.brashmonkey.spriter;

import java.util.ArrayList;

public class Data {
    final Entity[] entities;
    private int entityPointer = 0;
    private int folderPointer = 0;
    final Folder[] folders;
    public final String generator;
    public final String generatorVersion;
    public final String scmlVersion;

    Data(String scmlVersion2, String generator2, String generatorVersion2, int folders2, int entities2) {
        this.scmlVersion = scmlVersion2;
        this.generator = generator2;
        this.generatorVersion = generatorVersion2;
        this.folders = new Folder[folders2];
        this.entities = new Entity[entities2];
    }

    /* access modifiers changed from: package-private */
    public void addFolder(Folder folder) {
        Folder[] folderArr = this.folders;
        int i = this.folderPointer;
        this.folderPointer = i + 1;
        folderArr[i] = folder;
    }

    /* access modifiers changed from: package-private */
    public void addEntity(Entity entity) {
        Entity[] entityArr = this.entities;
        int i = this.entityPointer;
        this.entityPointer = i + 1;
        entityArr[i] = entity;
    }

    public Folder getFolder(String name) {
        int index = getFolderIndex(name);
        if (index >= 0) {
            return getFolder(index);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public int getFolderIndex(String name) {
        for (Folder folder : this.folders) {
            if (folder.name.equals(name)) {
                return folder.id;
            }
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public Folder getFolder(int index) {
        return this.folders[index];
    }

    public Entity getEntity(int index) {
        return this.entities[index];
    }

    public Entity getEntity(String name) {
        int index = getEntityIndex(name);
        if (index >= 0) {
            return getEntity(index);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public int getEntityIndex(String name) {
        for (Entity entity : this.entities) {
            if (entity.name.equals(name)) {
                return entity.id;
            }
        }
        return -1;
    }

    public File getFile(Folder folder, int file) {
        return folder.getFile(file);
    }

    public File getFile(int folder, int file) {
        return getFile(getFolder(folder), file);
    }

    public File getFile(FileReference ref) {
        return getFile(ref.folder, ref.file);
    }

    /* JADX INFO: Multiple debug info for r0v1 com.brashmonkey.spriter.Entity[]: [D('arr$' com.brashmonkey.spriter.Folder[]), D('arr$' com.brashmonkey.spriter.Entity[])] */
    public String toString() {
        String toReturn = getClass().getSimpleName() + "|[Version: " + this.scmlVersion + ", Generator: " + this.generator + " (" + this.generatorVersion + ")]";
        Folder[] arr$ = this.folders;
        for (int i$ = 0; i$ < arr$.length; i$++) {
            toReturn = toReturn + "\n" + arr$[i$];
        }
        Entity[] arr$2 = this.entities;
        for (int i$2 = 0; i$2 < arr$2.length; i$2++) {
            toReturn = toReturn + "\n" + arr$2[i$2];
        }
        return toReturn + "]";
    }

    public ArrayList<String> getEntities() {
        ArrayList<String> names = new ArrayList<>(this.entities.length);
        for (Entity entity : this.entities) {
            names.add(entity.name);
        }
        return names;
    }
}
