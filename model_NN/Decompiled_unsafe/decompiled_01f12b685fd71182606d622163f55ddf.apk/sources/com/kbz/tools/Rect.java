package com.kbz.tools;

public class Rect {
    public static final float ANGLE_270 = 4.712389f;
    public static final float ANGLE_90 = 1.5707964f;
    public static final int BASELINE = 64;
    public static final int BOTTOM = 32;
    public static final int DOTTED = 1;
    public static final int HCENTER = 1;
    public static final int LEFT = 4;
    public static final int RIGHT = 8;
    public static final byte[][] ROTATE_TABLE = {new byte[]{6, 5, 2, 1}, new byte[]{7, 4, 3, 0}, new byte[]{4, 7, 0, 3}, new byte[]{5, 6, 1, 2}, new byte[]{1, 2, 5, 6}, new byte[]{0, 3, 4, 7}, new byte[]{3, 0, 7, 4}, new byte[]{2, 1, 6, 5}};
    public static final int SOLID = 0;
    public static final int TOP = 16;
    public static final byte[][] TRANSFORM_TABLE = {new byte[]{0, 1, 2, 3, 4, 5, 6, 7}, new byte[]{1, 0, 3, 2, 5, 4, 7, 6}, new byte[]{2, 3, 0, 1, 6, 7, 4, 5}, new byte[]{3, 2, 1, 0, 7, 6, 5, 4}, new byte[]{4, 6, 5, 7, 0, 2, 1, 3}, new byte[]{5, 7, 4, 6, 1, 3, 0, 2}, new byte[]{6, 4, 7, 5, 2, 0, 3, 1}, new byte[]{7, 5, 6, 4, 3, 1, 2, 0}};
    public static final int TRANS_INVERTED_AXES = 4;
    public static final byte TRANS_MIRROR = 2;
    public static final byte TRANS_MIRROR_ROT180 = 1;
    public static final byte TRANS_MIRROR_ROT270 = 4;
    public static final byte TRANS_MIRROR_ROT90 = 7;
    public static final byte TRANS_NONE = 0;
    public static final byte TRANS_ROT180 = 3;
    public static final byte TRANS_ROT270 = 6;
    public static final byte TRANS_ROT90 = 5;
    public static final int TRANS_X_FLIP = 2;
    public static final int TRANS_Y_FLIP = 1;
    public static final int VCENTER = 2;
    public int h;
    public int transform;
    public int w;
    public int x;
    public int y;

    public Rect() {
        this.x = 0;
        this.y = 0;
        this.w = 0;
        this.h = 0;
    }

    public Rect(Rect rect) {
        setRect(rect);
    }

    public Rect(int left, int top, int right, int bottom) {
        setRect(left, top, right, bottom);
    }

    public void setRect(Rect rect) {
        if (rect != null) {
            this.x = rect.x;
            this.y = rect.y;
            this.w = rect.w;
            this.h = rect.h;
            return;
        }
        this.x = 0;
        this.y = 0;
        this.w = 0;
        this.h = 0;
    }

    public void setRect(int left, int top, int right, int bottom) {
        this.x = left;
        this.y = top;
        this.w = right;
        this.h = bottom;
    }

    public int getWidth() {
        if (this.x < this.w) {
            return this.w - this.x;
        }
        return this.x - this.w;
    }

    public int getHeight() {
        if (this.y < this.h) {
            return this.h - this.y;
        }
        return this.y - this.h;
    }

    public boolean equals(Rect rect) {
        if (rect != null && this.x == rect.x && this.y == rect.y && this.w == rect.w && this.h == rect.h) {
            return true;
        }
        return false;
    }

    public boolean isEmpty() {
        if (this.x == this.w || this.y == this.h) {
            return true;
        }
        return false;
    }

    public void translate(float x2, float y2) {
        this.x = (int) (((float) this.x) + x2);
        this.y = (int) (((float) this.y) + y2);
        this.w = (int) (((float) this.w) + x2);
        this.h = (int) (((float) this.h) + y2);
    }

    public void transform(int transform2) {
        if (transform2 != 0) {
            if ((transform2 & 2) != 0) {
                this.x = -this.x;
                this.w = -this.w;
            }
            if ((transform2 & 1) != 0) {
                this.y = -this.y;
                this.h = -this.h;
            }
            if ((transform2 & 4) != 0) {
                int temp = this.x;
                this.x = this.y;
                this.y = temp;
                int temp2 = this.w;
                this.w = this.h;
                this.h = temp2;
            }
        }
    }

    public void normalize() {
        if (this.x > this.w) {
            int temp = this.x;
            this.x = this.w;
            this.w = temp;
        }
        if (this.y > this.h) {
            int temp2 = this.y;
            this.y = this.h;
            this.h = temp2;
        }
    }
}
