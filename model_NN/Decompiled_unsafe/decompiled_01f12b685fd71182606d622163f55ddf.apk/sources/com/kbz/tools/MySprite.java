package com.kbz.tools;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class MySprite {
    private int OriginalH;
    private int OriginalW;
    private Sprite sprite;

    public MySprite(TextureAtlas.AtlasRegion atlasRegion) {
        this.sprite = new Sprite(atlasRegion);
        this.OriginalW = atlasRegion.getRegionWidth();
        this.OriginalH = atlasRegion.getRegionHeight();
    }

    public MySprite(Texture texture, int w, int h) {
        this.sprite = new Sprite(texture);
        this.OriginalW = w;
        this.OriginalH = h;
    }

    public MySprite(MySprite sprite2, int clipX, int clipY, int clipW, int clipH) {
        this.sprite = new Sprite(sprite2.getSprite(), clipX, clipY, clipW, clipH);
    }

    public MySprite(Texture texture) {
        this.sprite = new Sprite(texture);
        this.OriginalW = texture.getWidth();
        this.OriginalH = texture.getHeight();
    }

    public Sprite getSprite() {
        return this.sprite;
    }

    public int getOriginalWidth() {
        return this.OriginalW;
    }

    public int getOriginalHeight() {
        return this.OriginalH;
    }

    public float getWidth() {
        return this.sprite.getWidth();
    }

    public float getHeight() {
        return this.sprite.getHeight();
    }

    public TextureRegion getTextureRegion() {
        TextureRegion region = new TextureRegion(this.sprite.getTexture());
        region.flip(false, true);
        return region;
    }
}
