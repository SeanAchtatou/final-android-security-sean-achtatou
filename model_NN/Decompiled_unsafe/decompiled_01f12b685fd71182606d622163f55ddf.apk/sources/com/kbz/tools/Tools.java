package com.kbz.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.kbz.AssetManger.GAssetsManager;
import com.kbz.spine.MySpineData;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.util.GameStage;
import java.io.InputStream;
import org.objectweb.asm.Opcodes;

public class Tools implements GameConstant {
    private static final byte[] HARD_JPG = {1, 9, 8, 5, 0, 9, 1, 6};
    private static final byte[] HARD_PNG = {-119, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82};
    protected static final int[] N_2 = {4, 8, 16, 32, 64, 128, 256, 512, 1024, Opcodes.ACC_STRICT};
    private static final byte[] TAIL_PNG = {0, 0, 0, 0, 73, 69, 78, 68, -82, 66, 96, -126};
    public static boolean isDebugMode;
    public static float lastsetOffX;
    public static float lastsetOffY;
    public static String packPath = "imagePack/imageAll/";
    public static float setOffX;
    public static float setOffY;

    public static void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + ",");
            }
            System.out.println();
        }
    }

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ",");
        }
        System.out.println();
    }

    public static void printArray(String name, int[][] array) {
        System.out.println(name + ":");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + ",");
            }
            System.out.println();
        }
    }

    public static void initTools(boolean isDebugMode2) {
        setDebugMode(isDebugMode2);
    }

    public static void setOffXY(float x, float y) {
        GameStage.getCamera().translate(x - setOffX, y - setOffY);
        setOffX = x;
        setOffY = y;
    }

    public static TextureRegion getImage(int imgIndex) {
        return loadMyAtlsRegion(imgIndex);
    }

    public static void loadParticleEffect(int particalIndex) {
        GAssetsManager.loadParticleEffect(particalIndex);
    }

    public static void loadParticleEffect(String particalIndex) {
        GAssetsManager.loadParticleEffect(particalIndex);
    }

    protected static void setDebugMode(boolean isDebug) {
        isDebugMode = isDebug;
        MySpineData.isDebug = isDebug;
    }

    public static int getImageId(String imageName) {
        for (int i = 0; i < imageNameStr.length; i++) {
            if (imageNameStr[i].equals(imageName)) {
                return i;
            }
        }
        return -1;
    }

    public static String getPackPath() {
        return packPath;
    }

    public static String getImagePath(int id) {
        if (getPackName(id) == null) {
            return "imageOther/" + PAK_ASSETS.imageNameStr[id];
        }
        if (isDebugMode) {
            return "imageAll/" + getPackName(id) + "/" + PAK_ASSETS.imageNameStr[id];
        }
        return "imagePack/imageAll/" + PAK_ASSETS.packNameStr[id][2] + ".png";
    }

    public static void loadTextureRegion(int id) {
        GAssetsManager.loadTexture(getImagePath(id));
    }

    public static void loadTextureAtlas(int id) {
        GAssetsManager.loadTextureAtlas(packPath + PAK_ASSETS.packNameStr[id][2] + ".atlas");
    }

    public static void unLoadAtlas(String fullName) {
        GAssetsManager.unload(fullName);
    }

    public static void unLoadAllAtlas() {
        for (int i = 0; i < PAK_ASSETS.packNameStr.length; i++) {
            TextureAtlas textureAtlas = (TextureAtlas) GAssetsManager.getRes("imagePack/imageAll/" + PAK_ASSETS.packNameStr[i][2] + ".atlas", TextureAtlas.class);
            if (textureAtlas != null) {
                GAssetsManager.assetManager.unload(textureAtlas);
            }
        }
    }

    public static void unLoadAllParticle() {
        if (!isDebugMode) {
            for (int i = 0; i < PAK_ASSETS.DATAPARTICAL_NAME.length; i++) {
                GAssetsManager.unloadParticleEffect(i);
            }
        }
    }

    public static void loadPackTextureRegion(int id) {
        if (isDebugMode) {
            for (int i = Integer.parseInt(PAK_ASSETS.packNameStr[id][0]); i < Integer.parseInt(PAK_ASSETS.packNameStr[id][1]); i++) {
                loadTextureRegion(i);
            }
            return;
        }
        GAssetsManager.loadTexture(packPath + PAK_ASSETS.packNameStr[id][2] + ".png");
    }

    public static void loadPackTextureRegion_jpg(int id) {
        GAssetsManager.loadTexture(PAK_ASSETS.packNameStr[id][2] + ".jpg");
    }

    protected static TextureRegion loadMyAtlsRegion(int id) {
        if (isDebugMode) {
            return GAssetsManager.getTextureRegion(id);
        }
        if (getPackName(id) != null) {
            return GAssetsManager.getAtlasRegion2(id);
        }
        return GAssetsManager.getTextureRegion(id);
    }

    public static String getPackName(int id) {
        for (int i = 0; i < PAK_ASSETS.packNameStr.length; i++) {
            if (id >= Integer.parseInt(PAK_ASSETS.packNameStr[i][0]) && id < Integer.parseInt(PAK_ASSETS.packNameStr[i][1])) {
                return PAK_ASSETS.packNameStr[i][2];
            }
        }
        return null;
    }

    public static final void removeAllTextures() {
        for (int i = 0; i < packNameStr.length; i++) {
            GAssetsManager.unloadPackImage(i);
        }
        System.gc();
    }

    public static final void removeTextures(int imgIndex) {
        GAssetsManager.unloadPackImage(imgIndex);
    }

    public static final void removeAllPartical() {
        for (int i = 0; i < DATAPARTICAL_NAME.length / 2; i++) {
            GAssetsManager.unloadParticleEffect(i);
        }
        System.gc();
    }

    private static Pixmap loadMyPixmap(String path) {
        System.out.println("读取图片路径:" + path);
        byte[] bitmap = readFile(path);
        try {
            return new Pixmap(bitmap, 0, bitmap.length);
        } catch (Exception e) {
            System.err.println("loadMyPixmap读取“" + path + "”时错误");
            return null;
        }
    }

    private static byte[] readFile(String path) {
        byte[] tempImg;
        byte[] data = null;
        try {
            InputStream is = Gdx.files.internal(path).read();
            data = new byte[is.available()];
            is.read(data);
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        boolean isJpg = true;
        int i = 0;
        while (true) {
            if (i >= HARD_JPG.length) {
                break;
            } else if (HARD_JPG[i] != data[i]) {
                isJpg = false;
                break;
            } else {
                i++;
            }
        }
        if (isJpg) {
            tempImg = new byte[(data.length - HARD_JPG.length)];
            System.arraycopy(data, HARD_JPG.length, tempImg, 0, tempImg.length);
            for (int i2 = 0; i2 < 16; i2++) {
                tempImg[i2] = (byte) (tempImg[i2] ^ "haopu_game".hashCode());
            }
        } else {
            int len = data.length;
            tempImg = new byte[(HARD_PNG.length + len + TAIL_PNG.length)];
            System.arraycopy(HARD_PNG, 0, tempImg, 0, HARD_PNG.length);
            System.arraycopy(data, 0, tempImg, HARD_PNG.length, len);
            System.arraycopy(TAIL_PNG, 0, tempImg, HARD_PNG.length + len, TAIL_PNG.length);
            for (int i3 = HARD_PNG.length; i3 < HARD_PNG.length + 16; i3++) {
                tempImg[i3] = (byte) (tempImg[i3] ^ "haopu_game".hashCode());
            }
        }
        return tempImg;
    }
}
