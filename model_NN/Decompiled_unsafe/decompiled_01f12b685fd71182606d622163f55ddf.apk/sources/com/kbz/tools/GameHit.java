package com.kbz.tools;

import com.kbz.ActorsExtra.ActorInterface;

public class GameHit {
    public static boolean hit(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2) {
        return x1 <= x2 + w2 && x2 <= x1 + w1 && y1 <= y2 + h2 && y2 <= y1 + h1;
    }

    public static boolean hit(int x1, int y1, int x2, int y2, int w2, int h2) {
        return x1 <= x2 + w2 && x2 <= x1 + 1 && y1 <= y2 + h2 && y2 <= y1 + 1;
    }

    public static boolean hit(int x1, int y1, int[] rect) {
        int x2 = rect[0];
        int y2 = rect[1];
        int w2 = rect[2];
        int h2 = rect[3];
        if (x1 > x2 + w2 || x2 > x1 + 1 || y1 > y2 + h2 || y2 > y1 + 1) {
            return false;
        }
        return true;
    }

    public static boolean hit(ActorInterface actor1, ActorInterface actor2) {
        float x1 = actor1.getX();
        float y1 = actor1.getY();
        float w1 = actor1.getWidth();
        float h1 = actor1.getHeight();
        float x2 = actor2.getX();
        float y2 = actor2.getY();
        return x1 <= x2 + actor2.getWidth() && x2 <= x1 + w1 && y1 <= y2 + actor2.getHeight() && y2 <= y1 + h1;
    }
}
