package com.kbz.tools;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.kbz.Actors.ActorShapeSprite;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.util.GameLayer;
import com.sg.util.GameStage;

public class PolygonHit implements GameConstant {
    public static final int TYPE_BC = 3;
    public static final int TYPE_BL = 7;
    public static final int TYPE_BR = 5;
    public static final int TYPE_CENTER = 0;
    public static final int TYPE_LC = 2;
    public static final int TYPE_RC = 4;
    public static final int TYPE_TC = 8;
    public static final int TYPE_TL = 1;
    public static final int TYPE_TR = 6;
    private static ActorShapeSprite actorShapeSprite0 = new ActorShapeSprite();
    private static ActorShapeSprite actorShapeSprite1 = new ActorShapeSprite();
    private static boolean isAdd = true;
    public static boolean isShowRect = true;
    private static Polygon polygon0 = new Polygon();
    private static Polygon polygon1 = new Polygon();
    private static float[] polygonFloat0 = new float[8];
    private static float[] polygonFloat1 = new float[8];

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void
     arg types: [int, float[]]
     candidates:
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, com.kbz.tools.Rect):void
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void */
    public static boolean isHitPolygons(Polygon polygon02, float x, float y, float w, float h) {
        polygonFloat1[0] = 0.0f;
        polygonFloat1[1] = 0.0f;
        polygonFloat1[2] = Animation.CurveTimeline.LINEAR + w;
        polygonFloat1[3] = 0.0f;
        polygonFloat1[4] = Animation.CurveTimeline.LINEAR + w;
        polygonFloat1[5] = Animation.CurveTimeline.LINEAR + h;
        polygonFloat1[6] = 0.0f;
        polygonFloat1[7] = Animation.CurveTimeline.LINEAR + h;
        polygon1.setVertices(polygonFloat1);
        polygon1.setPosition(x, y);
        if (isShowRect) {
            actorShapeSprite0.createMyRect(false, polygon02.getTransformedVertices());
            actorShapeSprite1.createMyRect(false, polygon1.getTransformedVertices());
        }
        if (isAdd) {
            isAdd = false;
            GameStage.addActorToMyStage(GameLayer.top, actorShapeSprite1);
            GameStage.addActorToMyStage(GameLayer.top, actorShapeSprite0);
        }
        if (Intersector.overlapConvexPolygons(polygon02, polygon1)) {
            return true;
        }
        return false;
    }

    public static void setShowRect(boolean isShow) {
        isShowRect = isShow;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void
     arg types: [int, float[]]
     candidates:
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, com.kbz.tools.Rect):void
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void */
    public static boolean isHitPolygons(Polygon polygon02, Polygon polygon12) {
        if (polygon02 == null || polygon12 == null) {
            return false;
        }
        if (isShowRect) {
            actorShapeSprite0.createMyRect(false, polygon02.getTransformedVertices());
            actorShapeSprite1.createMyRect(false, polygon12.getTransformedVertices());
        }
        if (isAdd) {
            float[] temp = polygon02.getTransformedVertices();
            for (int i = 0; i < temp.length; i++) {
                System.out.println(i + ":" + temp[i]);
            }
            isAdd = false;
            GameStage.addActor(actorShapeSprite0, 11, GameLayer.max);
            GameStage.addActor(actorShapeSprite1, 11, GameLayer.max);
        }
        if (Intersector.overlapConvexPolygons(polygon02, polygon12)) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void
     arg types: [int, float[]]
     candidates:
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, com.kbz.tools.Rect):void
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void */
    public static boolean isHitPolygons(float x, float y, float w, float h, int OriginType, float angle, float x2, float y2, float w2, float h2) {
        polygonFloat0[0] = 0.0f;
        polygonFloat0[1] = 0.0f;
        polygonFloat0[2] = Animation.CurveTimeline.LINEAR + w;
        polygonFloat0[3] = 0.0f;
        polygonFloat0[4] = Animation.CurveTimeline.LINEAR + w;
        polygonFloat0[5] = Animation.CurveTimeline.LINEAR + h;
        polygonFloat0[6] = 0.0f;
        polygonFloat0[7] = Animation.CurveTimeline.LINEAR + h;
        polygon0.setVertices(polygonFloat0);
        polygon0.setPosition(x, y);
        switch (OriginType) {
            case 0:
                polygon0.setOrigin(w / 2.0f, h / 2.0f);
                break;
            case 1:
                polygon0.setOrigin(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                break;
            case 2:
                polygon0.setOrigin(Animation.CurveTimeline.LINEAR, h / 2.0f);
                break;
            case 3:
                polygon0.setOrigin(w / 2.0f, h);
                break;
            case 4:
                polygon0.setOrigin(w, h / 2.0f);
                break;
            case 5:
                polygon0.setOrigin(w, h);
                break;
            case 6:
                polygon0.setOrigin(w, Animation.CurveTimeline.LINEAR);
                break;
            case 7:
                polygon0.setOrigin(Animation.CurveTimeline.LINEAR, h);
                break;
            case 8:
                polygon0.setOrigin(w / 2.0f, Animation.CurveTimeline.LINEAR);
                break;
        }
        polygon0.setRotation(angle);
        polygonFloat1[0] = 0.0f;
        polygonFloat1[1] = 0.0f;
        polygonFloat1[2] = Animation.CurveTimeline.LINEAR + w;
        polygonFloat1[3] = 0.0f;
        polygonFloat1[4] = Animation.CurveTimeline.LINEAR + w;
        polygonFloat1[5] = Animation.CurveTimeline.LINEAR + h;
        polygonFloat1[6] = 0.0f;
        polygonFloat1[7] = Animation.CurveTimeline.LINEAR + h;
        polygon1.setVertices(polygonFloat1);
        polygon1.setPosition(x2, y2);
        if (isAdd) {
            isAdd = false;
            actorShapeSprite0.createMyRect(false, polygon0.getTransformedVertices());
            actorShapeSprite1.createMyRect(false, polygon1.getTransformedVertices());
            GameStage.addActor(actorShapeSprite0, 11, GameLayer.max);
            GameStage.addActor(actorShapeSprite1, 11, GameLayer.max);
        }
        if (Intersector.overlapConvexPolygons(polygon0, polygon1)) {
            return true;
        }
        return false;
    }
}
