package com.kbz.tools;

import com.badlogic.gdx.Gdx;
import com.kbz.esotericsoftware.spine.Animation;

public final class GameFunction {
    public static final int getPoint(int[][] pointPos, int x, int y) {
        for (int i = 0; i < pointPos.length; i++) {
            if (((float) x) >= ((float) pointPos[i][0]) * 1.0f && ((float) x) < (((float) pointPos[i][0]) * 1.0f) + (((float) pointPos[i][2]) * 1.0f) && ((float) y) >= ((float) pointPos[i][1]) * 1.0f && ((float) y) < (((float) pointPos[i][1]) * 1.0f) + (((float) pointPos[i][3]) * 1.0f)) {
                return i;
            }
        }
        return -1;
    }

    public static final void drawCleanScrean() {
        Gdx.gl.glClearColor(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        Gdx.gl.glClear(16384);
    }
}
