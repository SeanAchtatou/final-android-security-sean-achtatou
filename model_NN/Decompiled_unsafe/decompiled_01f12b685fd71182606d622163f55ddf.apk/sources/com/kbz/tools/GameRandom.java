package com.kbz.tools;

import java.util.Random;

public class GameRandom {
    static Random rnd = new Random();

    public static final boolean isSuccess(int rate) {
        if (rate > 99) {
            rate = 99;
        }
        if (rate < 1) {
            rate = 1;
        }
        if ((rnd.nextInt() >>> 1) % 100 < rate) {
            return true;
        }
        return false;
    }

    public static final int result(int r) {
        return (rnd.nextInt() >>> 1) % r;
    }

    public static final int result(int min, int max) {
        if (min == max) {
            return min;
        }
        if (min > max) {
            int temp = max;
            max = min;
            min = temp;
        }
        return min + ((rnd.nextInt() >>> 1) % (max - min));
    }

    public static final float result(float min, float max) {
        if (min == max) {
            return min;
        }
        if (min > max) {
            float temp = max;
            max = min;
            min = temp;
        }
        return min + (((float) (rnd.nextInt() >>> 1)) % (max - min));
    }

    public static final int[] restlt_2(int len, int[] array) {
        int[] r = new int[len];
        for (int i = 0; i < len; i++) {
            int index = result(i, array.length);
            r[i] = array[i];
            array[i] = array[index];
            array[index] = r[i];
            r[i] = array[i];
        }
        return r;
    }

    public static final int restlt_3(int[] array) {
        return array[(rnd.nextInt() >>> 1) % array.length];
    }

    public static final int restlt_3(short[] array) {
        return array[(rnd.nextInt() >>> 1) % array.length];
    }

    public static final int restlt_3(byte[] array) {
        return array[(rnd.nextInt() >>> 1) % array.length];
    }

    public static final int[] restlt_4(int[] array) {
        int len = array.length;
        int[] r = new int[len];
        for (int i = 0; i < len; i++) {
            int index = result(i, array.length);
            r[i] = array[i];
            array[i] = array[index];
            array[index] = r[i];
            r[i] = array[i];
        }
        return r;
    }
}
