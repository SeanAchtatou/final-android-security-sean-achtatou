package com.kbz.tools;

import com.kbz.esotericsoftware.spine.Animation;

public class SlidingOperation {
    public static final int MOD_X = 0;
    public static final int MOD_Y = 1;
    public static final byte MST_BACK = 4;
    public static final byte MST_FASTMOVE = 3;
    public static final byte MST_MOVE = 2;
    public static final byte MST_SLOWMOVE = 1;
    public static final byte MST_STOP = 0;
    float lastx;
    private int mod;
    float moveDistance;
    int moveIndex;
    public int moveIndexLen;
    byte moveState;
    float speed;
    public float sx;
    public float sy;
    float touchDownX;

    public SlidingOperation(int mod2) {
        this.mod = mod2;
    }

    public void setMoveIndexLen(int moveIndexLen2) {
        this.moveIndexLen = moveIndexLen2;
    }

    public void setMoveState(byte moveState2) {
        this.moveState = moveState2;
    }

    public static SlidingOperation getSo(int mod2) {
        return new SlidingOperation(mod2);
    }

    public void run() {
        float tempx;
        switch (this.moveState) {
            case 0:
            case 1:
            case 3:
            default:
                return;
            case 2:
                float tempx1 = Math.abs(this.sx - this.lastx);
                if (tempx1 > this.moveDistance) {
                    tempx = tempx1 % this.moveDistance;
                } else {
                    tempx = tempx1;
                }
                if (tempx < this.moveDistance * 0.3f) {
                    float n = ((float) ((int) (tempx1 / this.moveDistance))) * this.moveDistance;
                    if (this.sx - this.lastx > Animation.CurveTimeline.LINEAR) {
                        this.sx -= this.speed;
                        if (this.sx < this.lastx + n) {
                            this.sx = this.lastx + n;
                            setMoveState((byte) 0);
                        }
                    } else if (this.sx - this.lastx < Animation.CurveTimeline.LINEAR) {
                        this.sx += this.speed;
                        if (this.sx > this.lastx - n) {
                            this.sx = this.lastx - n;
                            setMoveState((byte) 0);
                        }
                    }
                } else if (tempx >= this.moveDistance * 0.3f) {
                    float n2 = ((float) (((int) (tempx1 / this.moveDistance)) + 1)) * this.moveDistance;
                    if (this.sx - this.lastx > Animation.CurveTimeline.LINEAR) {
                        this.sx += this.speed;
                        if (this.sx > this.lastx + n2) {
                            this.sx = this.lastx + n2;
                            if (this.moveIndexLen > 0) {
                                this.moveIndex--;
                            }
                            setMoveState((byte) 0);
                        }
                    } else if (this.sx - this.lastx < Animation.CurveTimeline.LINEAR) {
                        this.sx -= this.speed;
                        if (this.sx < this.lastx - n2) {
                            this.sx = this.lastx - n2;
                            if (this.moveIndex < this.moveIndexLen) {
                                this.moveIndex++;
                            }
                            setMoveState((byte) 0);
                        }
                    }
                }
                runSpeed();
                return;
            case 4:
                if (this.sx > Animation.CurveTimeline.LINEAR) {
                    this.sx -= this.speed;
                    if (this.sx < Animation.CurveTimeline.LINEAR) {
                        this.sx = Animation.CurveTimeline.LINEAR;
                        setMoveState((byte) 0);
                    }
                }
                if (this.sx < ((float) (-this.moveIndexLen)) * this.moveDistance) {
                    this.sx += this.speed;
                    if (this.sx > ((float) (-this.moveIndexLen)) * this.moveDistance) {
                        this.sx = ((float) (-this.moveIndexLen)) * this.moveDistance;
                        setMoveState((byte) 0);
                    }
                }
                runSpeed();
                return;
        }
    }

    public void setMoveDistance(float moveDistance2) {
        this.moveDistance = moveDistance2;
    }

    public void setSpeed(float speed2) {
        this.speed = speed2 / 10.0f;
    }

    public void runSpeed() {
        if (this.speed > 10.0f) {
            this.speed -= 1.0f;
        }
    }

    public void onTouch(int type, float x, float y) {
        switch (this.mod) {
            case 0:
                touchMoveX(type, x, y);
                return;
            case 1:
                touchMoveY(type, x, y);
                return;
            default:
                return;
        }
    }

    private void touchMoveY(int type, float x, float y) {
    }

    private void touchMoveX(int type, float x, float y) {
    }

    public int getIndex() {
        int index = (int) ((-this.sx) / this.moveDistance);
        if (((double) (((-this.sx) / this.moveDistance) - ((float) index))) > 0.5d) {
            return index + 1;
        }
        return index;
    }

    public boolean isStop() {
        return this.moveState == 0;
    }
}
