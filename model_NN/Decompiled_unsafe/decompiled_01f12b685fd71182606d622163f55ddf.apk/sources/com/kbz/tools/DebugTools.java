package com.kbz.tools;

public class DebugTools {
    private static boolean isDebug = false;
    private static long tmpBeginTime;

    public static void timeBegin() {
        tmpBeginTime = System.currentTimeMillis();
    }

    public static void timeEnd() {
        long duration = System.currentTimeMillis() - tmpBeginTime;
        System.out.println("---持续时间---" + duration + "ms=" + (((float) duration) / 1000.0f) + "s");
        tmpBeginTime = 0;
    }

    public static boolean isDebug() {
        return isDebug;
    }
}
