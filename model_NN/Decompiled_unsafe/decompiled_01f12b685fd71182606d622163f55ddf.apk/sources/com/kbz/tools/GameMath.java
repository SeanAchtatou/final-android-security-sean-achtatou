package com.kbz.tools;

import com.kbz.ActorsExtra.ActorInterface;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;

public class GameMath {
    public static final float getR(ActorInterface sprite, ActorInterface conSprite) {
        float x1 = sprite.getX();
        float y1 = sprite.getY();
        return calcNormalAngle(((float) (sprite.w / 2)) + x1, ((float) (sprite.h / 2)) + y1, conSprite.getX(), conSprite.getY());
    }

    public static float getRad(float px1, float py1, float px2, float py2) {
        float x = px2 - px1;
        float rad = (float) Math.acos((double) (x / ((float) Math.sqrt(Math.pow((double) x, 2.0d) + Math.pow((double) (py1 - py2), 2.0d)))));
        if (py2 < py1) {
            return -rad;
        }
        return rad;
    }

    public static int getAngel(int x1, int y1, int x2, int y2) {
        return getA(y1 - y2, -(x1 - x2));
    }

    static int getA(int x, int y) {
        if (x != 0) {
            int angele = (int) ((Math.atan2((double) x, (double) y) * 180.0d) / 3.141592653589793d);
            if (x > 0 && y >= 0) {
                return angele;
            }
            if (x > 0 && y < 0) {
                return angele;
            }
            if (x >= 0 || y < 0) {
                return angele + PAK_ASSETS.IMG_XINJILU;
            }
            return angele + PAK_ASSETS.IMG_XINJILU;
        } else if (y > 0) {
            return 0;
        } else {
            return 180;
        }
    }

    public static final float calcNormalAngle(float x, float y, float tx, float ty) {
        float tempAngle = (float) ((Math.atan((double) (Math.abs(x - tx) / Math.abs(y - ty))) * 180.0d) / 3.141592653589793d);
        if (x > tx) {
            if (y > ty) {
                return 90.0f + tempAngle;
            }
            return 270.0f - tempAngle;
        } else if (x < tx) {
            if (y > ty) {
                return 90.0f - tempAngle;
            }
            return 270.0f + tempAngle;
        } else if (x == tx) {
            if (y < ty) {
                return 270.0f;
            }
            return 90.0f;
        } else if (y != ty || x < tx) {
            return Animation.CurveTimeline.LINEAR;
        } else {
            return 180.0f;
        }
    }
}
