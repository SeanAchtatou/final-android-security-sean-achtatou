package com.kbz.tools;

import com.badlogic.gdx.Gdx;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;

public class LoadTxt implements GameConstant {
    int pageIndex = 0;
    String[][] strskkk;

    public static String loadText(int txtIndex) {
        return Gdx.files.internal(PAK_ASSETS.DATATXT_PATH + DATATXT_NAME[txtIndex]).readString("utf-8");
    }

    public void test(String str) {
        String[] tmp = str.split("%翻页%");
        this.strskkk = new String[tmp.length][];
        System.err.println("   翻页:" + this.strskkk.length);
        for (int i = 0; i < tmp.length; i++) {
            this.strskkk[i] = tmp[i].split("%分割线%");
        }
    }

    private static String getTextSampling(String text) {
        if (text == null) {
            return null;
        }
        StringBuffer buff = new StringBuffer();
        buff.append(text.charAt(0));
        for (int i = 1; i < text.length(); i++) {
            char c = text.charAt(i);
            if (buff.indexOf("" + c) == -1) {
                buff.append(c);
            }
        }
        return buff.toString();
    }
}
