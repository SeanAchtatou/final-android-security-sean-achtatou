package com.kbz.tools;

import com.badlogic.gdx.Gdx;
import com.sg.pak.PAK_ASSETS;
import java.io.InputStream;

public class GameDataInputStream implements PAK_ASSETS {
    public static InputStream loadFile(String file) {
        return Gdx.files.internal(file).read();
    }
}
