package com.kbz.tools;

import com.badlogic.gdx.math.Bezier;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;

public class CHBezierToAction extends TemporalAction {
    private Bezier<Vector2> bezier;
    private Vector2 lastPos;
    private Vector2 out = new Vector2();
    private boolean rotate;
    protected float x;
    protected float y;

    public void setBezier(Bezier<Vector2> bezierParam) {
        this.bezier = bezierParam;
    }

    public boolean isRotate() {
        return this.rotate;
    }

    public void setRotate(boolean rotate2) {
        this.rotate = rotate2;
    }

    /* access modifiers changed from: protected */
    public void begin() {
        this.x = this.actor.getX();
        this.y = this.actor.getY();
        this.bezier.points.insert(0, new Vector2(this.x, this.y));
        this.lastPos = (Vector2) this.bezier.points.get(0);
    }

    /* access modifiers changed from: protected */
    public void end() {
        this.lastPos = null;
    }

    public void reset() {
        super.reset();
        this.lastPos = null;
        this.bezier = null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.math.Bezier.valueAt(com.badlogic.gdx.math.Vector, float):T
     arg types: [com.badlogic.gdx.math.Vector2, float]
     candidates:
      com.badlogic.gdx.math.Bezier.valueAt(java.lang.Object, float):java.lang.Object
      com.badlogic.gdx.math.Path.valueAt(java.lang.Object, float):T
      com.badlogic.gdx.math.Bezier.valueAt(com.badlogic.gdx.math.Vector, float):T */
    /* access modifiers changed from: protected */
    public void update(float percent) {
        this.bezier.valueAt((Vector) this.out, percent);
        this.target.setPosition(this.out.x, this.out.y);
        Vector2 copyOut = this.out.cpy();
        if (this.rotate) {
            this.target.setRotation(this.out.sub(this.lastPos).angle());
        }
        this.lastPos = copyOut;
    }

    public static CHBezierToAction obtain(Bezier<Vector2> bezierParam, float duration) {
        Pool<CHBezierToAction> pool = Pools.get(CHBezierToAction.class);
        CHBezierToAction action = (CHBezierToAction) pool.obtain();
        action.setDuration(duration);
        action.setBezier(bezierParam);
        action.setPool(pool);
        return action;
    }

    public static CHBezierToAction action(Bezier<Vector2> bezierParam, float duration, Interpolation interpolation) {
        CHBezierToAction action = obtain(bezierParam, duration);
        action.setInterpolation(interpolation);
        return action;
    }

    public static CHBezierToAction obtain(float[][] posXY, float duration) {
        Pool<CHBezierToAction> pool = Pools.get(CHBezierToAction.class);
        CHBezierToAction action = (CHBezierToAction) pool.obtain();
        action.setDuration(duration);
        Vector2[] points = new Vector2[posXY.length];
        for (int i = 0; i < points.length; i++) {
            points[i] = new Vector2(posXY[i][0], posXY[i][1]);
        }
        action.setBezier(new Bezier<>(points));
        action.setPool(pool);
        return action;
    }
}
