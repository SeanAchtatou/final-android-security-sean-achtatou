package com.kbz.Sound;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.LongMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.kbz.AssetManger.GAssetsManager;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.util.GameStage;
import java.util.Iterator;

public class GSound implements GameConstant {
    /* access modifiers changed from: private */
    public static Array<String> curSound = new Array<>();
    public static boolean isp = true;
    private static LongMap<String> loopSound = new LongMap<>();
    private static Music music;
    private static ObjectMap<String, Music> musicPool = new ObjectMap<>();
    private static boolean silence_music;
    private static boolean silence_sound;
    private static ObjectMap<String, Sound> soundPool = new ObjectMap<>();
    private static float volume_music = 1.0f;
    private static float volume_sound = 1.0f;

    static {
        GameStage.registerUpdateService("soundPlay", new GameStage.GUpdateService() {
            public boolean update(float delta) {
                for (int i = 0; i < GSound.curSound.size; i++) {
                    GSound.playSimpleSound((String) GSound.curSound.get(i));
                    GSound.curSound.clear();
                }
                return false;
            }
        });
    }

    public static void initMusic(int musicId) {
        stopMusic();
        String musicName = MUSIC_NAME[musicId];
        if (!musicPool.containsKey(musicName)) {
            GAssetsManager.loadSound(getMusicPath(musicId));
            music = GAssetsManager.getMusic(musicId);
            musicPool.put(musicName, music);
        }
        music = musicPool.get(musicName);
    }

    private static String getMusicPath(int musicIndex) {
        return PAK_ASSETS.MUSIC_PATH + MUSIC_NAME[musicIndex];
    }

    private static String getSoundPath(int musicIndex) {
        return PAK_ASSETS.SOUND_PATH + SOUND_NAME[musicIndex];
    }

    public static int getSoundIndex(String soundName) {
        for (int i = 0; i < SOUND_NAME.length; i++) {
            if (SOUND_NAME[i].equals(soundName)) {
                return i;
            }
        }
        return 0;
    }

    public static void playMusic() {
        if (music != null && !silence_music && !music.isPlaying()) {
            music.setVolume(volume_music);
            music.setLooping(true);
            music.play();
        }
    }

    public static void pauseMusic() {
        if (music != null) {
            music.pause();
        }
    }

    public static boolean isMusicPlaying() {
        return music != null && music.isPlaying();
    }

    public static void stopMusic() {
        if (music != null) {
            music.stop();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public static float setMusicVolume(float volume) {
        volume_music = Math.max((float) Animation.CurveTimeline.LINEAR, volume);
        volume_music = Math.min(volume_music, 1.0f);
        if (music != null) {
            music.setVolume(volume_music);
        }
        return volume_music;
    }

    public float getMusicVolume() {
        return volume_music;
    }

    public static void setMusicSilence(boolean isSilence) {
        silence_music = isSilence;
        if (music == null) {
            return;
        }
        if (silence_music) {
            music.stop();
        } else {
            playMusic();
        }
    }

    public static void initSound(int soundId) {
        String soundName = SOUND_NAME[soundId];
        if (!soundPool.containsKey(soundName)) {
            GAssetsManager.loadSound(getSoundPath(soundId));
            soundPool.put(soundName, GAssetsManager.getSound(soundId));
        }
        soundPool.put(soundName, GAssetsManager.getSound(soundId));
    }

    public static void clearSound(String soundName) {
        if (soundPool.containsKey(soundName)) {
            soundPool.remove(soundName);
        }
    }

    public static Sound getSound(int soundId) {
        String soundName = SOUND_NAME[soundId];
        if (!soundPool.containsKey(soundName)) {
            initSound(soundId);
        }
        return soundPool.get(soundName);
    }

    public static void playSound(int nameID) {
        if (!isp) {
            String soundName = PAK_ASSETS.SOUND_NAME[nameID];
            if (!curSound.contains(soundName, true)) {
                curSound.add(soundName);
            }
        }
    }

    public static long playSimpleSound(String soundName) {
        if (silence_sound) {
            return -1;
        }
        return getSound(getSoundIndex(soundName)).play(volume_sound);
    }

    public static long playLoopSound(String soundName) {
        if (silence_sound) {
            return -1;
        }
        Sound sound = getSound(getSoundIndex(soundName));
        long soundID = sound.loop(volume_sound);
        loopSound.put(soundID, soundName);
        sound.play();
        return soundID;
    }

    public static void stopSound() {
        ObjectMap.Values<Sound> it = soundPool.values().iterator();
        while (it.hasNext()) {
            ((Sound) it.next()).stop();
        }
    }

    public static void playLoopSound() {
        Iterator<String> it = loopSound.values().iterator();
        while (it.hasNext()) {
            playLoopSound(it.next());
        }
    }

    public static void resumeSound() {
        ObjectMap.Values<Sound> it = soundPool.values().iterator();
        while (it.hasNext()) {
            ((Sound) it.next()).resume();
        }
    }

    public static void resumeSound(int soundName) {
        getSound(soundName).resume();
    }

    public static void pauseSound() {
        ObjectMap.Values<Sound> it = soundPool.values().iterator();
        while (it.hasNext()) {
            ((Sound) it.next()).pause();
        }
    }

    public static void pauseSound(int soundName) {
        getSound(soundName).pause();
    }

    public static void stopSound(int soundName) {
        getSound(soundName).stop();
    }

    public static void stopSound(int soundName, long soundID) {
        getSound(soundName).stop(soundID);
        loopSound.remove(soundID);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public static float setSoundVolume(float volume) {
        volume_sound = Math.max((float) Animation.CurveTimeline.LINEAR, volume);
        volume_sound = Math.min(volume_sound, 1.0f);
        setLoopSoundVolume(volume_sound);
        return volume_music;
    }

    public float getSoundVolume() {
        return volume_sound;
    }

    public static void setSoundSilence(boolean isSilence) {
        silence_sound = isSilence;
        if (silence_music) {
            stopSound();
        }
    }

    public static void pause() {
        if (music != null) {
            music.pause();
        }
        pauseSound();
    }

    private static void setLoopSoundVolume(float volume) {
        Iterator<String> it = loopSound.values().iterator();
        while (it.hasNext()) {
            String value = it.next();
            ((Sound) GAssetsManager.getRes(value)).setVolume(loopSound.findKey(value, true, -1), volume);
        }
    }

    public static void resume() {
        playMusic();
        resumeSound();
    }

    public static void clear() {
        stopMusic();
        stopSound();
        ObjectMap.Values<Music> musics = musicPool.values();
        ObjectMap.Values<Music> it = musics.iterator();
        while (it.hasNext()) {
            Music music2 = (Music) it.next();
            GAssetsManager.unload(musics);
        }
        ObjectMap.Values<Sound> it2 = soundPool.values().iterator();
        while (it2.hasNext()) {
            GAssetsManager.unload((Sound) it2.next());
        }
        musicPool.clear();
        soundPool.clear();
        loopSound.clear();
    }
}
