package com.kbz.Sound;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.kbz.AssetManger.GAssetsManager;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.util.GameStage;
import java.util.ArrayList;

public class GameSound implements GameConstant {
    /* access modifiers changed from: private */
    public static ArrayList<Integer> curSound = new ArrayList<>();
    public static boolean isMusicOpen = true;
    public static boolean isSoundOpen = true;
    public static Music[] music;
    public static Sound[] sound;
    public static float volume_music = 1.0f;
    public static float volume_sound = 1.0f;

    static {
        GameStage.registerUpdateService("soundPlay", new GameStage.GUpdateService() {
            public boolean update(float delta) {
                for (int i = 0; i < GameSound.curSound.size(); i++) {
                    GameSound.playS(((Integer) GameSound.curSound.get(i)).intValue());
                }
                GameSound.curSound.clear();
                return false;
            }
        });
    }

    public static void loadAllMusic(String[] strMusic) {
        if (music == null) {
            music = new Music[strMusic.length];
        }
        for (int i = 0; i < strMusic.length; i++) {
            GAssetsManager.loadMusic(getMusicPath(i));
        }
    }

    private static String getMusicPath(int musicIndex) {
        return PAK_ASSETS.MUSIC_PATH + MUSIC_NAME[musicIndex];
    }

    public static void playMusic(int index) {
        if (isMusicOpen) {
            if (music == null) {
                music = new Music[MUSIC_NAME.length];
            }
            if (music[index] == null || !music[index].isPlaying()) {
                stopAllMusic();
                if (music[index] == null) {
                    music[index] = GAssetsManager.getMusic(index);
                }
                music[index].setVolume(volume_music);
                music[index].setLooping(true);
                music[index].play();
            }
        }
    }

    public static void playMusicOnce(int index) {
        if (isMusicOpen) {
            if (music == null) {
                music = new Music[MUSIC_NAME.length];
            }
            if (music[index] == null || !music[index].isPlaying()) {
                stopAllMusic();
                if (music[index] == null) {
                    music[index] = GAssetsManager.getMusic(index);
                }
                music[index].setVolume(volume_music);
                music[index].setLooping(false);
                music[index].play();
            }
        }
    }

    public static void stopAllMusic() {
        for (int i = 0; i < music.length; i++) {
            if (music[i] != null) {
                music[i].stop();
            }
        }
    }

    public static void loadAllSound(String[] strSound) {
        sound = new Sound[strSound.length];
        for (int i = 0; i < strSound.length; i++) {
            GAssetsManager.loadSound(getSoundPath(i));
        }
    }

    private static String getSoundPath(int musicIndex) {
        return PAK_ASSETS.SOUND_PATH + SOUND_NAME[musicIndex];
    }

    public static void playS(int index) {
        if (isSoundOpen) {
            if (sound == null) {
                sound = new Sound[SOUND_NAME.length];
            }
            if (sound[index] == null) {
                sound[index] = GAssetsManager.getSound(index);
            }
            sound[index].setVolume(sound[index].play(), volume_sound);
        }
    }

    public static void playSound(int index) {
        if (!curSound.contains(Integer.valueOf(index))) {
            curSound.add(Integer.valueOf(index));
        }
    }

    public static void playSoundLoop(int index) {
        if (isSoundOpen) {
            if (sound[index] == null) {
                sound[index] = GAssetsManager.getSound(index);
            }
            sound[index].setVolume(sound[index].loop(), volume_sound);
        }
    }

    public static void stopAllSound() {
        for (int i = 0; i < sound.length; i++) {
            if (sound[i] != null) {
                sound[i].stop();
            }
        }
    }

    public void closeAllSoundAndMusic() {
        stopAllMusic();
        stopAllSound();
    }
}
