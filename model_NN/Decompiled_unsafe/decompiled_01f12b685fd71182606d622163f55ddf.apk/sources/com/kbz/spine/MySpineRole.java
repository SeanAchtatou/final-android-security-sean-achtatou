package com.kbz.spine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Array;
import com.kbz.ActorsExtra.ActorInterface;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.esotericsoftware.spine.AnimationState;
import com.kbz.esotericsoftware.spine.AnimationStateData;
import com.kbz.esotericsoftware.spine.Skeleton;
import com.kbz.esotericsoftware.spine.SkeletonRenderer;
import com.kbz.esotericsoftware.spine.Slot;
import com.kbz.esotericsoftware.spine.attachments.Attachment;
import com.kbz.tools.Tools;
import com.sg.pak.PAK_ASSETS;
import java.util.HashMap;

public class MySpineRole extends ActorInterface {
    private static HashMap<Integer, HashMap<Float, MySpineData>> arraySpineRole = new HashMap<>();
    private static final float checkModifiedInterval = 0.25f;
    private static final float reloadDelay = 1.0f;
    private float aniSpeed = 1.0f;
    private String animationName = null;
    private boolean isFlipX;
    private boolean isFlipY;
    private boolean isLoop;
    private boolean isPause = false;
    private boolean isPremultipliedAlpha = false;
    private String lastAnimationName = null;
    private float mix = 0.3f;
    private MySpineData mySpineData;
    private int nowSpineId;
    private SkeletonRenderer<Batch> renderer;
    public Skeleton skeleton;
    public int spineId;
    public String[] spineName;
    public AnimationState state;

    public int getSpineId() {
        return this.spineId;
    }

    public MySpineData getMySpineData() {
        return this.mySpineData;
    }

    public void init(String[] name) {
        this.spineName = name;
    }

    public void createSpineRole(int spineID, float scaleF) {
        this.renderer = new SkeletonRenderer<>();
        this.mySpineData = getMySpineData(spineID, scaleF);
        createSpineData();
        this.spineId = spineID;
        this.nowSpineId = spineID;
    }

    public void setSkin(String skinName) {
        this.skeleton.setSkin(skinName);
    }

    public void changeSkin(String slotName, String attachmentName, String skinName) {
        Attachment att = getMySpineData().skeletonData.findSkin(skinName).getAttachment(this.skeleton.findSlotIndex(slotName), attachmentName);
        this.skeleton.findSlot(slotName).setAttachment(att);
        this.skeleton.getSkin().addAttachment(this.skeleton.findSlotIndex(slotName), attachmentName, att);
    }

    private void createSpineData() {
        this.skeleton = new Skeleton(this.mySpineData.skeletonData);
        this.skeleton.setToSetupPose();
        this.skeleton = new Skeleton(this.skeleton);
        this.skeleton.updateWorldTransform();
        this.state = new AnimationState(new AnimationStateData(this.mySpineData.skeletonData));
    }

    private final MySpineData getMySpineData(int id, float scaleF) {
        HashMap<Float, MySpineData> hashMap = arraySpineRole.get(Integer.valueOf(id));
        if (hashMap == null) {
            HashMap<Float, MySpineData> tempMap = new HashMap<>();
            arraySpineRole.put(Integer.valueOf(id), tempMap);
            hashMap = tempMap;
        }
        this.mySpineData = (MySpineData) hashMap.get(Float.valueOf(scaleF));
        if (this.mySpineData == null) {
            this.mySpineData = new MySpineData();
            this.mySpineData.scaleF = scaleF;
            this.mySpineData.loadSkeleton(Gdx.files.internal(PAK_ASSETS.SPINE_PATH + Tools.SPINE_NAME[id] + ".json"), false);
            hashMap.put(Float.valueOf(scaleF), this.mySpineData);
        }
        return this.mySpineData;
    }

    public void setAnimation(String aniName, boolean isLoop2) {
        this.skeleton.setToSetupPose();
        this.animationName = aniName;
        this.isLoop = isLoop2;
        this.state.setAnimation(0, this.animationName, isLoop2);
    }

    public void setScaleF(float scaleF) {
        this.mySpineData = getMySpineData(this.nowSpineId, scaleF);
        createSpineData();
        setAnimation(this.animationName, this.isLoop);
    }

    public void setAniSpeed(float aniSpeed2) {
        this.aniSpeed = aniSpeed2;
    }

    public void setPause(boolean isPause2) {
        this.isPause = isPause2;
    }

    public boolean isPause() {
        return this.isPause;
    }

    public void setMix(float mix2) {
        this.mix = mix2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void updata() {
        float delta = Gdx.graphics.getDeltaTime();
        if (this.skeleton != null) {
            if (this.mySpineData.reloadTimer <= Animation.CurveTimeline.LINEAR) {
                this.mySpineData.lastModifiedCheck -= delta;
                if (this.mySpineData.lastModifiedCheck < Animation.CurveTimeline.LINEAR) {
                    this.mySpineData.lastModifiedCheck = checkModifiedInterval;
                    long time = this.mySpineData.skeletonFile.lastModified();
                    if (!(time == 0 || this.mySpineData.lastModified == time)) {
                        this.mySpineData.reloadTimer = 1.0f;
                    }
                }
            } else {
                this.mySpineData.reloadTimer -= delta;
                if (this.mySpineData.reloadTimer <= Animation.CurveTimeline.LINEAR) {
                    this.mySpineData.loadSkeleton(this.mySpineData.skeletonFile, true);
                }
            }
            this.renderer.setPremultipliedAlpha(this.isPremultipliedAlpha);
            float delta2 = Math.min(delta, 0.032f) * this.aniSpeed;
            this.skeleton.update(delta2);
            if (!this.isPause) {
                this.state.update(delta2);
                this.state.apply(this.skeleton);
            }
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        this.skeleton.setPosition(getX(), getY());
        this.skeleton.setFlipX(this.isFlipX);
        this.skeleton.setFlipY(!this.isFlipY);
        this.skeleton.updateWorldTransform();
        this.renderer.draw(batch, this.skeleton);
        batch.setBlendFunction(770, 771);
    }

    public float getPlayTime() {
        return this.state.playTime;
    }

    public void setFilpX(boolean isFlipX2) {
        this.isFlipX = isFlipX2;
    }

    public void setFilpY(boolean isFlipY2) {
        this.isFlipY = isFlipY2;
    }

    public void setSkeleton(int spineID, String aniName, boolean isLoop2) {
        this.nowSpineId = spineID;
        this.mySpineData = getMySpineData(spineID, this.mySpineData.scaleF);
        createSpineData();
        setAnimation(aniName, isLoop2);
    }

    public void setAnnimationName(String name) {
        this.animationName = name;
    }

    public String getAnnimationName() {
        return this.animationName;
    }

    public boolean isEnd() {
        return this.state.isEnd();
    }

    public void addListener(AnimationState.AnimationStateListener listener) {
        if (this.state != null) {
            this.state.addListener(listener);
        }
    }

    public void removeListener(AnimationState.AnimationStateListener listener) {
        if (this.state != null) {
            this.state.removeListener(listener);
        }
    }

    public Array<Slot> getAllBouns() {
        return this.skeleton.getSlots();
    }

    public Slot findSlot(String str) {
        return this.skeleton.findSlot(str);
    }

    public void setColor(Color color) {
        for (int i = 0; i < getAllBouns().size; i++) {
            getAllBouns().get(i).getColor().set(color);
        }
    }
}
