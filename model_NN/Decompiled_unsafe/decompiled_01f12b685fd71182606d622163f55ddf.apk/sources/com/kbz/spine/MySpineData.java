package com.kbz.spine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.kbz.esotericsoftware.spine.Skeleton;
import com.kbz.esotericsoftware.spine.SkeletonBinary;
import com.kbz.esotericsoftware.spine.SkeletonData;
import com.kbz.esotericsoftware.spine.SkeletonJson;

public class MySpineData {
    private static final float checkModifiedInterval = 0.25f;
    public static boolean isDebug;
    public long lastModified;
    public float lastModifiedCheck;
    public float reloadTimer;
    public float scaleF;
    public Skeleton skeleton;
    public SkeletonData skeletonData;
    public FileHandle skeletonFile;

    public void loadSkeleton(FileHandle skeletonFile2, boolean reload) {
        TextureAtlas.TextureAtlasData data;
        if (skeletonFile2 != null) {
            String atlasFileName = skeletonFile2.nameWithoutExtension();
            if (atlasFileName.endsWith(".json")) {
                atlasFileName = new FileHandle(atlasFileName).nameWithoutExtension();
            }
            FileHandle atlasFile = skeletonFile2.sibling(atlasFileName + ".atlas");
            if (!atlasFile.exists()) {
                atlasFile = skeletonFile2.sibling(atlasFileName + ".atlas.txt");
            }
            if (!atlasFile.exists()) {
                data = null;
            } else {
                data = new TextureAtlas.TextureAtlasData(atlasFile, atlasFile.parent(), false);
            }
            TextureAtlas atlas = new TextureAtlas(data, null) {
                public TextureAtlas.AtlasRegion findRegion(String name) {
                    TextureAtlas.AtlasRegion region = super.findRegion(name);
                    if (region != null) {
                    }
                    return region;
                }
            };
            try {
                String extension = skeletonFile2.extension();
                if (extension.equalsIgnoreCase("json") || extension.equalsIgnoreCase("txt")) {
                    SkeletonJson json = new SkeletonJson(atlas);
                    json.setScale(this.scaleF);
                    this.skeletonData = json.readSkeletonData(skeletonFile2);
                } else {
                    SkeletonBinary binary = new SkeletonBinary(atlas);
                    binary.setScale(this.scaleF);
                    this.skeletonData = binary.readSkeletonData(skeletonFile2);
                }
                this.skeleton = new Skeleton(this.skeletonData);
                this.skeleton.setToSetupPose();
                this.skeleton = new Skeleton(this.skeleton);
                this.skeleton.updateWorldTransform();
                this.skeletonFile = skeletonFile2;
                Preferences prefs = Gdx.app.getPreferences("spine-skeletontest");
                prefs.putString("lastFile", skeletonFile2.path());
                prefs.flush();
                this.lastModified = skeletonFile2.lastModified();
                this.lastModifiedCheck = checkModifiedInterval;
            } catch (Exception ex) {
                ex.printStackTrace();
                this.lastModifiedCheck = 5.0f;
            }
        }
    }
}
