package com.kbz.spine;

import com.sg.GameSprites.GameSpriteType;
import java.lang.reflect.Array;

public class MySpine extends MySpineRole implements GameSpriteType {
    public final void initMotion(String[][] spineMotion) {
        int len = spineMotion.length;
        this.motion = (short[][]) Array.newInstance(Short.TYPE, len, 2);
        this.spineName = new String[len];
        for (int i = 0; i < len; i++) {
            this.motion[i][0] = (short) Integer.parseInt(spineMotion[i][0]);
            this.motion[i][1] = (short) Integer.parseInt(spineMotion[i][1]);
            this.spineName[i] = spineMotion[i][2];
        }
    }

    public void statusToAnimation() {
        int temp = -1;
        int i = 0;
        while (true) {
            if (i >= this.motion.length) {
                break;
            } else if (this.motion[i][0] == this.curStatus) {
                temp = i;
                break;
            } else {
                i++;
            }
        }
        if (this.motion[temp][0] != this.motion[temp][1]) {
            setStatus(this.motion[temp][1]);
        }
    }

    public void changeAnimation(int curStatus) {
        int temp = 0;
        int i = 0;
        while (true) {
            if (i >= this.motion.length) {
                break;
            } else if (this.motion[i][0] == curStatus) {
                setAnnimationName(this.spineName[i]);
                temp = i;
                break;
            } else {
                i++;
            }
        }
        if (this.motion[temp][0] != this.motion[temp][1]) {
            this.isRepeat = false;
        } else {
            this.isRepeat = true;
        }
        setAnimation(getAnnimationName(), this.isRepeat);
    }

    public void setSkeleon(int spineID, int stauts) {
        changeAnimation(stauts);
        setSkeleton(spineID, getAnnimationName(), this.isRepeat);
    }

    public void setSkeleonEffect(int spineID, int stauts) {
        setSkeleton(spineID, getAnnimationName(), this.isRepeat);
    }

    public void setStatus(int st) {
        if (this.curStatus != st) {
            this.curStatus = st;
            this.index = 0;
            changeAnimation(this.curStatus);
            updata();
        }
    }

    public void setSpine() {
        setSkeleon(this.spineId, 2);
    }
}
