package com.kbz.ParticleOld;

import com.badlogic.gdx.graphics.g2d.Sprite;

public interface ParticleEditorFlip {
    void flip(Sprite sprite);
}
