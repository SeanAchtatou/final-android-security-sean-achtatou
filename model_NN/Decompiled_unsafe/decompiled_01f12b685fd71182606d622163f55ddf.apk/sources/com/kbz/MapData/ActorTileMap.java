package com.kbz.MapData;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.kbz.esotericsoftware.spine.Animation;

public class ActorTileMap extends Actor {
    protected static final int FLAG_FLIP_DIAGONALLY = 536870912;
    protected static final int FLAG_FLIP_HORIZONTALLY = Integer.MIN_VALUE;
    protected static final int FLAG_FLIP_VERTICALLY = 1073741824;
    protected static final int MASK_CLEAR = -536870912;
    int anchor;
    protected TextureRegion img;
    boolean isFlipX;
    boolean isFlipY;

    public ActorTileMap(TextureRegion img2, int x, int y, Group group) {
        this.img = img2;
        setSize((float) img2.getRegionWidth(), (float) img2.getRegionHeight());
        setPosition((float) x, (float) y);
        group.addActor(this);
    }

    public ActorTileMap(TextureRegion imgRegion, MapProperties object, Group group) {
        int id = ((Integer) object.get("gid")).intValue();
        int intValue = ((Integer) object.get("gid")).intValue() & 536870911;
        int x = ((Float) object.get("x")).intValue();
        int y = ((Float) object.get("y")).intValue();
        float width = (float) ((Float) object.get("width")).intValue();
        float height = (float) ((Float) object.get("height")).intValue();
        boolean flipHorizontally = (FLAG_FLIP_HORIZONTALLY & id) != 0;
        boolean flipVertically = (FLAG_FLIP_VERTICALLY & id) != 0;
        float rota = object.containsKey("rotation") ? (float) ((Float) object.get("rotation")).intValue() : Animation.CurveTimeline.LINEAR;
        this.img = imgRegion;
        setSize((float) this.img.getRegionWidth(), (float) this.img.getRegionHeight());
        setPosition((float) x, (float) (y - this.img.getRegionHeight()));
        float scanfX = width / ((float) this.img.getRegionWidth());
        float scanfY = height / ((float) this.img.getRegionHeight());
        setOrigin(Animation.CurveTimeline.LINEAR, (float) this.img.getRegionHeight());
        if (scanfX == Animation.CurveTimeline.LINEAR || scanfY == Animation.CurveTimeline.LINEAR) {
            setScale(1.0f, 1.0f);
        } else {
            setScale(scanfX, scanfY);
        }
        setRotation(rota);
        setFlip(flipHorizontally, !flipVertically);
        group.addActor(this);
    }

    public void setFlip(boolean isFlipX2, boolean isFlipY2) {
        this.isFlipX = isFlipX2;
        this.isFlipY = isFlipY2;
    }

    public void setFlipX(boolean isFlipX2) {
        this.isFlipX = isFlipX2;
    }

    public void setFlipY(boolean isFlipY2) {
        this.isFlipY = isFlipY2;
    }

    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        this.img.flip(this.isFlipX, this.isFlipY);
        batch.draw(this.img, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        this.img.flip(this.isFlipX, this.isFlipY);
        setColor(color);
    }

    public void clean() {
        this.img.getTexture().dispose();
    }
}
