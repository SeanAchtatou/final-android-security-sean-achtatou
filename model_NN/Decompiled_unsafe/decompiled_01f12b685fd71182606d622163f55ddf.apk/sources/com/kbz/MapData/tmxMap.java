package com.kbz.MapData;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.utils.Array;
import com.sg.map.GameMapCollision;
import java.util.HashMap;
import java.util.Iterator;

public class tmxMap extends MapTileLayer {
    private static int HeightNum = 11;
    private static int WidthNum = 8;
    private static HashMap<Integer, String> firstIdHashMap = new HashMap<>();
    private static int mapHight;
    private static int mapWidth;
    public static int tileHight = 70;
    public static int tileWidth = 70;
    public Array<GameMapCollision> collisionsArray = new Array<>();

    public void initTmxMap(String tmxName) {
        initTiledMap(tmxName);
        initFirstImage();
        initData();
        clearMonsterXY();
        addMapLayerAll();
    }

    public static int getImageIndex(int firstId) {
        String name = firstIdHashMap.get(Integer.valueOf(firstId)).concat(".png");
        for (int i = 0; i < imageNameStr.length; i++) {
            if (imageNameStr[i].equals(name)) {
                return i;
            }
        }
        String name2 = firstIdHashMap.get(Integer.valueOf(firstId)).concat(".jpg");
        for (int i2 = 0; i2 < imageNameStr.length; i2++) {
            if (imageNameStr[i2].equals(name2)) {
                return i2;
            }
        }
        System.err.println("GameTMxMap  getImageIndex  err");
        return 0;
    }

    public static int getMapHight() {
        return tileHight * HeightNum;
    }

    public static int getMapWidth() {
        return tileWidth * WidthNum;
    }

    public static int getTileWidth() {
        return tileWidth;
    }

    public static int getTileHight() {
        return tileHight;
    }

    public static int getWidthNum() {
        return WidthNum;
    }

    public static int getHightNum() {
        return HeightNum;
    }

    private void initProp() {
        TiledMapTileLayer propLayer = (TiledMapTileLayer) getLayer("layer1");
        WidthNum = propLayer.getWidth();
        HeightNum = propLayer.getHeight();
        tileWidth = (int) propLayer.getTileWidth();
        tileHight = (int) propLayer.getTileHeight();
        mapWidth = WidthNum * tileWidth;
        mapHight = HeightNum * tileHight;
    }

    private void initFirstImage() {
        Iterator<TiledMapTileSet> iterato = getTiledMap().getTileSets().iterator();
        while (iterato.hasNext()) {
            TiledMapTileSet object = iterato.next();
            firstIdHashMap.put((Integer) object.getProperties().get("firstgid"), object.getName());
        }
    }

    private void initCollision() {
        Iterator<MapObject> iterato = getLayer("collision").getObjects().iterator();
        while (iterato.hasNext()) {
            this.collisionsArray.add(new GameMapCollision(iterato.next().getProperties()));
        }
    }

    public void clean() {
        getTiledMap().dispose();
    }
}
