package com.kbz.MapData;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

public class MapTileLayer extends tmxMapLoad implements GameConstant {
    public static int[][] blockData = ((int[][]) Array.newInstance(Integer.TYPE, 88, 2));
    public static Vector<BuildingData> buildingData = new Vector<>();
    static float homeX;
    static float homeY;
    static int i = 0;
    static Vector<int[]> monsterXY = new Vector<>();
    public static int[][] phyData = ((int[][]) Array.newInstance(Integer.TYPE, 88, 2));
    public static int[][] roadData = ((int[][]) Array.newInstance(Integer.TYPE, 88, 2));
    public static int tileHight = 70;
    public static int tileWidth = 70;
    protected Vector<Cloud> cloudData = new Vector<>();
    Group group;
    float[] polyline;

    /* access modifiers changed from: protected */
    public void setGroup(Group group2) {
        this.group = group2;
    }

    public void initTiledMap(String tmxName) {
        TmxMapLoader.Parameters parameters = new TmxMapLoader.Parameters();
        parameters.flipY = false;
        this.tiledMap = load(PAK_ASSETS.TMX_PATH + tmxName, parameters);
    }

    /* access modifiers changed from: protected */
    public void addMapLayerAll() {
        Iterator<MapLayer> it = this.tiledMap.getLayers().iterator();
        while (it.hasNext()) {
            MapLayer layer = it.next();
            if (layer instanceof TiledMapTileLayer) {
                initMapLayer(layer.getName());
            } else if ("bg".equals(layer.getName())) {
                addCloud();
            } else if ("building".equals(layer.getName())) {
                addBuilding();
            } else if ("obj".equals(layer.getName())) {
                getPolylineMapObject("obj");
            }
        }
    }

    public float[] getPolyline() {
        return this.polyline;
    }

    public static float getHomeX() {
        return homeX;
    }

    public static float getHomeY() {
        return homeY;
    }

    public static Vector<int[]> getMonsterXY() {
        return monsterXY;
    }

    public static void clearMonsterXY() {
        monsterXY.clear();
    }

    public void getPolylineMapObject(String objname) {
        Iterator<MapObject> iterato = getLayer(objname).getObjects().iterator();
        while (iterato.hasNext()) {
            MapObject object = iterato.next();
            if (object instanceof PolylineMapObject) {
                this.polyline = ((PolylineMapObject) object).getPolyline().getTransformedVertices();
            } else if ("MonsterBornPoint".equals(object.getName())) {
                homeX = (float) ((Float) object.getProperties().get("x")).intValue();
                homeY = (float) ((Float) object.getProperties().get("y")).intValue();
            } else if ("MonsterCreater".equals(object.getName())) {
                monsterXY.add(new int[]{((Float) object.getProperties().get("x")).intValue(), ((Float) object.getProperties().get("y")).intValue()});
                i++;
            }
        }
    }

    public class Cloud {
        String type;
        int x;
        int y;

        public Cloud() {
        }

        public int getX() {
            return this.x;
        }

        public void setX(int x2) {
            this.x = x2;
        }

        public int getY() {
            return this.y;
        }

        public void setY(int y2) {
            this.y = y2;
        }

        public String getType() {
            return this.type;
        }

        public void setType(String type2) {
            this.type = type2;
        }
    }

    private void addCloud() {
        this.cloudData.clear();
        Iterator<MapObject> iterato = getLayer("bg").getObjects().iterator();
        while (iterato.hasNext()) {
            MapProperties object = iterato.next().getProperties();
            int x = (int) getCollsionFloat(object, "x");
            Cloud cloud = new Cloud();
            cloud.setX(x);
            cloud.setY((int) getCollsionFloat(object, "y"));
            cloud.setType((String) object.get("type"));
            this.cloudData.add(cloud);
        }
    }

    public class BuildingData {
        String buildCreate = new String();
        int flip;
        int level;
        String name;
        Map<String, String> tower = new HashMap();
        String type;
        int x;
        int y;

        public BuildingData() {
        }

        public String getBuildCreate() {
            return this.buildCreate;
        }

        public void setBuildCreate(String buildCreate2) {
            this.buildCreate = buildCreate2;
        }

        public int getLevel() {
            return this.level;
        }

        public void setLevel(int level2) {
            this.level = level2;
        }

        public boolean isFlip() {
            return this.flip == 1;
        }

        public void setFlip(int flip2) {
            this.flip = flip2;
        }

        public Map<String, String> getTower() {
            return this.tower;
        }

        public void setTower(Map<String, String> tower2) {
            this.tower = tower2;
        }

        public String getName() {
            return this.name;
        }

        public void setName(String name2) {
            this.name = name2;
        }

        public int getX() {
            return this.x;
        }

        public void setX(int x2) {
            this.x = x2;
        }

        public int getY() {
            return this.y;
        }

        public void setY(int y2) {
            this.y = y2;
        }

        public String getType() {
            return this.type;
        }

        public void setType(String type2) {
            this.type = type2;
        }
    }

    private void addBuilding() {
        buildingData.clear();
        Iterator<MapObject> iterato = getLayer("building").getObjects().iterator();
        while (iterato.hasNext()) {
            MapObject m = iterato.next();
            MapProperties object = m.getProperties();
            String name = m.getName();
            int x = (int) getCollsionFloat(object, "x");
            BuildingData building = new BuildingData();
            building.setName(name);
            building.setX(x);
            building.setY((int) getCollsionFloat(object, "y"));
            building.setType((String) object.get("type"));
            loadProperties(object, "Tower", building);
            loadProperties(object, "Tower1", building);
            loadProperties(object, "Tower2", building);
            loadProperties(object, "Tower3", building);
            loadProperties(object, "Tower4", building);
            building.setBuildCreate((String) object.get("BuildCreate"));
            int flip = getCollsionStr(object, "Flip");
            int level = getCollsionStr(object, GameConstant.LEVEL);
            building.setFlip(flip);
            building.setLevel(level);
            buildingData.add(building);
        }
    }

    /* access modifiers changed from: package-private */
    public void loadProperties(MapProperties object, String p, BuildingData building) {
        String towerString = getCollsionStr_2(object, p);
        if (!"null".equals(towerString)) {
            building.tower.put(p, towerString);
        }
    }

    /* access modifiers changed from: package-private */
    public float getCollsionFloat(MapProperties object, String properties) {
        Float kbz = (Float) object.get(properties);
        if (kbz == null) {
            return -1.0f;
        }
        return kbz.floatValue();
    }

    private int getCollsionStr(MapProperties object, String properties) {
        String string = (String) object.get(properties);
        if (string == null) {
            return -1;
        }
        return Integer.parseInt(string);
    }

    private String getCollsionStr_2(MapProperties object, String properties) {
        String string = (String) object.get(properties);
        if (string == null) {
            return "null";
        }
        return string;
    }

    /* access modifiers changed from: protected */
    public void addObj(String objname) {
        Iterator<MapObject> iterato = getLayer(objname).getObjects().iterator();
        while (iterato.hasNext()) {
            MapObject object = iterato.next();
            MapProperties properties = object.getProperties();
            TextureRegion img = ((TextureMapObject) object).getTextureRegion();
            if (img == null) {
                System.err.println("null");
            }
            new ActorTileMap(img, properties, this.group);
        }
    }

    /* access modifiers changed from: package-private */
    public void initData() {
        for (int i2 = 0; i2 < phyData.length; i2++) {
            phyData[i2][0] = -1;
            phyData[i2][1] = -1;
        }
        for (int i3 = 0; i3 < blockData.length; i3++) {
            blockData[i3][0] = -1;
            blockData[i3][1] = -1;
        }
    }

    private void initMapLayer(String layerName) {
        TiledMapTileLayer propLayer = (TiledMapTileLayer) getLayer(layerName);
        int WidthNum = propLayer.getWidth();
        int HeightNum = propLayer.getHeight();
        int tileWidth2 = (int) propLayer.getTileWidth();
        int tileHight2 = (int) propLayer.getTileHeight();
        int i2 = WidthNum * tileWidth2;
        int i3 = HeightNum * tileHight2;
        for (int i4 = 0; i4 < WidthNum * HeightNum; i4++) {
            TiledMapTileLayer.Cell cell = propLayer.getCell(i4 % WidthNum, i4 / WidthNum);
            if (propLayer.getCell(i4 % WidthNum, i4 / WidthNum) == null) {
                setData(layerName, i4, -1, -1);
            } else {
                TextureRegion region = cell.getTile().getTextureRegion();
                int x = (i4 % WidthNum) * tileWidth2;
                int y = (i4 / WidthNum) * tileHight2;
                setData(layerName, i4, x, y);
                if (!"phy".equals(layerName) && !"block".equals(layerName)) {
                    ActorTileMap actor = new ActorTileMap(region, x, y, this.group);
                    actor.setFlip(cell.getFlipHorizontally(), !cell.getFlipVertically());
                    actor.setRotation((float) (360 - (cell.getRotation() * 90)));
                    if (cell.getRotation() != 0) {
                        actor.setPosition((float) x, (float) (y + tileHight2));
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setData(String layerName, int i2, int x, int y) {
        if ("phy".equals(layerName)) {
            setPhyData(i2, x, y);
        } else if ("block".equals(layerName)) {
            setBlockData(i2, x, y);
        } else if ("road".equals(layerName)) {
            setRoadData(i2, x, y);
        }
    }

    /* access modifiers changed from: package-private */
    public void setPhyData(int i2, int x, int y) {
        phyData[i2][0] = x;
        phyData[i2][1] = y;
    }

    /* access modifiers changed from: protected */
    public int[][] getPhyData() {
        return phyData;
    }

    /* access modifiers changed from: package-private */
    public void setBlockData(int i2, int x, int y) {
        blockData[i2][0] = x;
        blockData[i2][1] = y;
    }

    /* access modifiers changed from: package-private */
    public void setRoadData(int i2, int x, int y) {
        roadData[i2][0] = x;
        roadData[i2][1] = y;
    }

    /* access modifiers changed from: protected */
    public int[][] getBlockData() {
        return blockData;
    }
}
