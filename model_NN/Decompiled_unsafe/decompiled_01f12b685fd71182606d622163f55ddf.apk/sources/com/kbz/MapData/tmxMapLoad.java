package com.kbz.MapData;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;

class tmxMapLoad extends TmxMapLoader implements GameConstant {
    TiledMap tiledMap;

    tmxMapLoad() {
    }

    /* access modifiers changed from: protected */
    public TiledMap getTiledMap() {
        return this.tiledMap;
    }

    /* access modifiers changed from: protected */
    public MapLayer getLayer(String layerName) {
        return getTiledMap().getLayers().get(layerName);
    }

    /* access modifiers changed from: protected */
    public void initTiledMap(int tmxIndex) {
        TmxMapLoader.Parameters parameters = new TmxMapLoader.Parameters();
        parameters.flipY = false;
        this.tiledMap = load(PAK_ASSETS.TMX_PATH + TMX_NAME[tmxIndex], parameters);
    }
}
