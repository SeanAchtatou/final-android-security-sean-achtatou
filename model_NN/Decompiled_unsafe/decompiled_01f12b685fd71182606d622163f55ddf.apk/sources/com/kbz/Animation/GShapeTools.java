package com.kbz.Animation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.kbz.esotericsoftware.spine.Animation;

public final class GShapeTools {
    private static ShapeRenderer sRender = new ShapeRenderer(1000);

    public static ShapeRenderer getShapeRenderer() {
        return sRender;
    }

    private static void begin(Batch batch, ShapeRenderer.ShapeType shapeType, Color color) {
        batch.end();
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(770, 771);
        sRender.setProjectionMatrix(batch.getProjectionMatrix());
        sRender.setTransformMatrix(batch.getTransformMatrix());
        sRender.setColor(color);
        sRender.begin(shapeType);
    }

    private static void end(Batch batch) {
        sRender.end();
        batch.begin();
    }

    public static void drawBox(Batch batch, Color color, float x, float y, float z, float width, float height, float depth) {
        begin(batch, ShapeRenderer.ShapeType.Line, color);
        sRender.box(x, y, z, width, height, depth);
        end(batch);
    }

    public static void drawPoint(Batch batch, Color color, float x, float y) {
        begin(batch, ShapeRenderer.ShapeType.Point, color);
        sRender.point(x, y, Animation.CurveTimeline.LINEAR);
        end(batch);
    }

    public static void drawLine(Batch batch, Color color, float x1, float y1, float x2, float y2) {
        begin(batch, ShapeRenderer.ShapeType.Line, color);
        sRender.line(x1, y1, x2, y2);
        end(batch);
    }

    public static void drawPolygon(Batch batch, Color color, Polygon polygon) {
        begin(batch, ShapeRenderer.ShapeType.Line, color);
        sRender.polygon(polygon.getTransformedVertices());
        end(batch);
    }

    public static float[] getTransformedVertices(float[] localVertices, float x, float y, float originX, float originY, float scaleX, float scaleY, float rotation) {
        float[] worldVertices = new float[localVertices.length];
        float positionX = x;
        float positionY = y;
        boolean scale = (scaleX == 1.0f && scaleY == 1.0f) ? false : true;
        float cos = MathUtils.cosDeg(rotation);
        float sin = MathUtils.sinDeg(rotation);
        int n = localVertices.length;
        for (int i = 0; i < n; i += 2) {
            float x2 = localVertices[i] - originX;
            float y2 = localVertices[i + 1] - originY;
            if (scale) {
                x2 *= scaleX;
                y2 *= scaleY;
            }
            if (rotation != Animation.CurveTimeline.LINEAR) {
                float oldX = x2;
                x2 = (cos * x2) - (sin * y2);
                y2 = (sin * oldX) + (cos * y2);
            }
            worldVertices[i] = positionX + x2 + originX;
            worldVertices[i + 1] = positionY + y2 + originY;
        }
        return worldVertices;
    }

    public static void drawPolygon(Batch batch, Color color, Polygon polygon, float x, float y, float originX, float originY, float scaleX, float scaleY, float rotation) {
        begin(batch, ShapeRenderer.ShapeType.Line, color);
        sRender.polygon(getTransformedVertices(polygon.getVertices(), x, y, originX, originY, scaleX, scaleY, rotation));
        end(batch);
    }

    public static void drawPolygon(Batch batch, Color color, float[] vertices) {
        begin(batch, ShapeRenderer.ShapeType.Line, color);
        sRender.polygon(vertices);
        end(batch);
    }

    public static void drawARC(Batch batch, Color color, float x, float y, float radius, float start, float angle, boolean isFill) {
        begin(batch, isFill ? ShapeRenderer.ShapeType.Filled : ShapeRenderer.ShapeType.Line, color);
        sRender.arc(x, y, radius, start, angle);
        end(batch);
    }

    public static void drawTriangle(Batch batch, Color color, float x1, float y1, float x2, float y2, float x3, float y3, boolean isFill) {
        begin(batch, isFill ? ShapeRenderer.ShapeType.Filled : ShapeRenderer.ShapeType.Line, color);
        sRender.triangle(x1, y1, x2, y2, x3, y3);
        end(batch);
    }

    public static void drawCurve(Batch batch, Color color, float x1, float y1, float cx1, float cy1, float cx2, float cy2, float x2, float y2) {
        begin(batch, ShapeRenderer.ShapeType.Line, color);
        sRender.curve(x1, y1, cx1, cy1, cx2, cy2, x2, y2, 1);
        end(batch);
    }

    public static void drawRectangle(Batch batch, Color color, float x, float y, float w, float h, boolean isFill) {
        begin(batch, isFill ? ShapeRenderer.ShapeType.Filled : ShapeRenderer.ShapeType.Line, color);
        sRender.rect(x, y, w, h);
        end(batch);
    }

    public static void drawRectangle(Batch batch, Color color, float x, float y, float width, float height, float originX, float originY, float rotation, boolean isFill) {
        begin(batch, isFill ? ShapeRenderer.ShapeType.Filled : ShapeRenderer.ShapeType.Line, color);
        sRender.rect(x, y, width, height, originX, originY, 1.0f, 1.0f, rotation);
        end(batch);
    }

    public static void drawEllipse(Batch batch, Color color, float x, float y, float w, float h, boolean isFill) {
        begin(batch, isFill ? ShapeRenderer.ShapeType.Filled : ShapeRenderer.ShapeType.Line, color);
        sRender.ellipse(x, y, w, h);
        end(batch);
    }

    public static void drawX(Batch batch, Color color, float x, float y, float radius) {
        begin(batch, ShapeRenderer.ShapeType.Line, color);
        sRender.x(x, y, radius);
        end(batch);
    }

    public static void drawCircle(Batch batch, Color color, float x, float y, float radius, boolean isFill) {
        begin(batch, isFill ? ShapeRenderer.ShapeType.Filled : ShapeRenderer.ShapeType.Line, color);
        sRender.circle(x, y, radius);
        end(batch);
    }
}
