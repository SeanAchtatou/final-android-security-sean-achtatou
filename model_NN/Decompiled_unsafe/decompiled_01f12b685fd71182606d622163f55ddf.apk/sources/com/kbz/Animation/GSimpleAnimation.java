package com.kbz.Animation;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;
import com.kbz.Animation.GAnimationManager;
import com.kbz.AssetManger.GAssetsManager;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.PAK_ASSETS;
import com.sg.util.GameStage;

public class GSimpleAnimation extends Actor implements Pool.Poolable {
    /* access modifiers changed from: private */
    public static final ObjectMap<String, TextureAtlas> atlasBuff = new ObjectMap<>();
    public static boolean isDebug = false;
    private static final GAnimationManager.GElementData newElement = new GAnimationManager.GElementData(null);
    private static final GAnimationManager.GElementData pElement = new GAnimationManager.GElementData(null);
    private static TextureAtlas.AtlasRegion regionBuff;
    /* access modifiers changed from: private */
    public String animationPack;
    private GAnimationManager.GAnimationCompleteListener completeListener;
    private short curFrameIndex = 1;
    private Element[][] frameDat;
    private GAnimationManager.GFrameData[] frames;
    private short index = 0;
    private boolean isAutoNextFrame = true;
    public boolean isFastMode = false;
    private byte playMode = 2;
    private GAnimationPlayer player;
    /* access modifiers changed from: private */
    public GAnimationManager.GAnimationResListener resListener;
    private byte transMode;

    private class Element {
        GAnimationManager.GElementData data;
        TextureAtlas.AtlasRegion region;
        Element[] sub;

        public boolean isAnimation() {
            return this.sub != null;
        }

        public Element(GAnimationManager.GElementData data2, boolean isSubData) {
            if (isSubData) {
                this.data = new GAnimationManager.GElementData(data2);
            } else {
                this.data = data2;
            }
            switch (data2.res_type) {
                case 0:
                    String res_dir = data2.res_dir;
                    String res_name = data2.res_name;
                    res_dir = GSimpleAnimation.this.resListener != null ? GSimpleAnimation.this.resListener.changeResDir(GSimpleAnimation.this.animationPack, res_dir) : res_dir;
                    TextureAtlas atlas = (TextureAtlas) GSimpleAnimation.atlasBuff.get(res_dir);
                    if (atlas == null) {
                        atlas = GAssetsManager.getTextureAtlas(PAK_ASSETS.ANIMATION_PATH + res_dir + ".pack");
                        GSimpleAnimation.atlasBuff.put(res_dir, atlas);
                        Array<String> resTable = GAnimationManager.getResTable(GSimpleAnimation.this.animationPack);
                        if (resTable.contains(res_dir, false)) {
                            resTable.add(res_dir);
                        }
                    }
                    this.region = atlas.findRegion(res_name);
                    return;
                default:
                    GAnimationManager.GAnimationSample sample = GAnimationManager.getSample(GAnimationManager.createAnimationKey(data2.res_dir, data2.res_name));
                    GAnimationManager.GElementData[] subElements = sample.elementDatas[GAnimationPlayer.getRealKeyFrame(sample.frameDatas, data2.curFrame)];
                    this.sub = new Element[subElements.length];
                    for (int i = 0; i < subElements.length; i++) {
                        GAnimationManager.GElementData subData = subElements[i];
                        if (GSimpleAnimation.this.isFastMode) {
                            subData = GSimpleAnimation.this.getElement(data2, subData);
                        }
                        this.sub[i] = new Element(subData, GSimpleAnimation.this.isFastMode);
                    }
                    return;
            }
        }
    }

    public void reset() {
        this.playMode = 2;
        this.transMode = 0;
        this.curFrameIndex = 1;
        this.index = 0;
        this.isAutoNextFrame = true;
        this.player = null;
        this.completeListener = null;
        this.resListener = null;
        regionBuff = null;
        this.frames = null;
        this.animationPack = null;
        setColor(Color.WHITE);
        setBounds(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
        setName(null);
    }

    public String getAnimationPack() {
        return this.animationPack;
    }

    public void stopAnimation(int toIndex) {
        this.isAutoNextFrame = false;
        this.curFrameIndex = (short) (toIndex + 1);
    }

    public void resumeAnimation(int fromIndex) {
        this.isAutoNextFrame = true;
        this.curFrameIndex = (short) (fromIndex + 1);
    }

    public int getAnimationFrameIndex() {
        return this.curFrameIndex - 1;
    }

    public void flip(boolean x, boolean y) {
        this.transMode = (byte) (x ? 1 : 0);
        if (y) {
            this.transMode = (byte) (this.transMode ^ 2);
        }
    }

    public void setCompleteListener(GAnimationManager.GAnimationCompleteListener listener) {
        this.completeListener = listener;
    }

    public void setResListener(GAnimationManager.GAnimationResListener resListener2) {
        this.resListener = resListener2;
    }

    public GSimpleAnimation() {
    }

    public GSimpleAnimation(String animationPack2, String animationName) {
        changeAnimation(animationPack2, animationName, this.playMode, this.resListener, this.completeListener, this.isFastMode);
    }

    public GSimpleAnimation(String animationPack2, String animationName, boolean isFastMode2) {
        changeAnimation(animationPack2, animationName, this.playMode, this.resListener, this.completeListener, isFastMode2);
    }

    public GSimpleAnimation(String animationPack2, String animationName, byte playMode2, boolean isFastMode2) {
        changeAnimation(animationPack2, animationName, playMode2, this.resListener, this.completeListener, isFastMode2);
    }

    public GSimpleAnimation(String animationPack2, String animationName, byte playMode2, GAnimationManager.GAnimationResListener resListener2, GAnimationManager.GAnimationCompleteListener completeListener2, boolean isFastMode2) {
        changeAnimation(animationPack2, animationName, playMode2, resListener2, completeListener2, isFastMode2);
    }

    public void changeAnimation(String animationName) {
        changeAnimation(this.animationPack, animationName, this.playMode, this.resListener, this.completeListener, this.isFastMode);
    }

    public void changeAnimation(String animationName, byte playMode2) {
        changeAnimation(this.animationPack, animationName, playMode2, this.resListener, this.completeListener, this.isFastMode);
    }

    public void changeAnimation(String animationPack2, String animationName) {
        changeAnimation(animationPack2, animationName, this.playMode, this.resListener, this.completeListener, this.isFastMode);
    }

    public void changeAnimation(String animationPack2, String animationName, boolean isFastMode2) {
        changeAnimation(animationPack2, animationName, this.playMode, this.resListener, this.completeListener, isFastMode2);
    }

    public void changeAnimation(String animationPack2, String animationName, byte playMode2, boolean isFastMode2) {
        changeAnimation(animationPack2, animationName, playMode2, this.resListener, this.completeListener, isFastMode2);
    }

    /* access modifiers changed from: protected */
    public void changeAnimation(String animationPack2, String animationName, byte playMode2, GAnimationManager.GAnimationResListener resListener2, GAnimationManager.GAnimationCompleteListener completeListener2, boolean isFastMode2) {
        this.isFastMode = isFastMode2;
        GAnimationManager.GAnimationSample sample = GAnimationManager.getSample(animationPack2, animationName);
        if (sample == null) {
            System.err.println("can't find animation [" + animationPack2 + " " + animationName + "]\r\n begin");
            String[] animationList = GAnimationManager.getAnimationList();
            int length = animationList.length;
            for (int i = 0; i < length; i++) {
                System.err.println(" __________ " + animationList[i]);
            }
            System.err.println(" __________ end");
        }
        this.playMode = playMode2;
        this.completeListener = completeListener2;
        this.animationPack = animationPack2;
        this.resListener = resListener2;
        GAnimationManager.GElementData[][] data = sample.elementDatas;
        this.frameDat = new Element[data.length][];
        for (int i2 = 0; i2 < data.length; i2++) {
            this.frameDat[i2] = new Element[data[i2].length];
            for (int j = 0; j < data[i2].length; j++) {
                this.frameDat[i2][j] = new Element(data[i2][j], false);
            }
        }
        this.frames = sample.frameDatas;
        atlasBuff.clear();
        this.player = new GAnimationPlayer(GameStage.getSleepTime(), this.frames, playMode2);
        this.player.getKeyFrameIndex(Animation.CurveTimeline.LINEAR);
        setOrigin(sample.originX, sample.originY);
        setSize(sample.width, sample.height);
        setName(sample.name);
        runInitScript(sample.initScript);
        this.curFrameIndex = this.player.getKeyFrameIndex(Animation.CurveTimeline.LINEAR);
        this.index = (short) (Math.abs((int) this.curFrameIndex) - 1);
    }

    public void setPlayerTime(float time) {
        this.player.setFrameDuration(time);
    }

    public float getPlayerTime() {
        return this.player.getFrameDuration();
    }

    public void runInitScript(String animationName) {
        runInitScript(GAnimationManager.getSample(this.animationPack, animationName).initScript);
    }

    private void runInitScript(String[][] initScript) {
        GAnimationManager.GAnimationScriptParser parser = GAnimationManager.getAnimationInitParser();
        if (parser != null && initScript != null && initScript.length != 0) {
            parser.parse(this, initScript);
        }
    }

    private void runFrameScript(int index2) {
        GAnimationManager.GAnimationScriptParser parser = GAnimationManager.getFrameParser();
        if (parser != null && index2 >= 0 && this.frames[index2].script != null && this.frames[index2].script.length != 0) {
            parser.parse(this, this.frames[index2].script);
        }
    }

    private GAnimationManager.GElementData getActorElement() {
        pElement.x = getX();
        pElement.y = getY();
        pElement.w = getWidth();
        pElement.h = getHeight();
        pElement.originX = getOriginX();
        pElement.originY = getOriginY();
        pElement.rotation = getRotation();
        pElement.scaleX = getScaleX();
        pElement.scaleY = getScaleY();
        pElement.transMode = this.transMode;
        pElement.argb = -1;
        return pElement;
    }

    /* access modifiers changed from: private */
    public GAnimationManager.GElementData getElement(GAnimationManager.GElementData pElement2, GAnimationManager.GElementData element) {
        float x;
        float y;
        float pOX = pElement2.originX;
        float pOY = pElement2.originY;
        float x2 = element.x;
        float y2 = element.y;
        float rotation = element.rotation;
        float scaleX = element.scaleX;
        float scaleY = element.scaleY;
        float localX = -(((-x2) - element.originX) + pOX);
        float localY = -(((-y2) - element.originY) + pOY);
        float worldOriginX = x2 - localX;
        float worldOriginY = y2 - localY;
        float pSX = pElement2.scaleX;
        float pSY = pElement2.scaleY;
        if (!(pSX == 1.0f && pSY == 1.0f)) {
            localX *= pSX;
            localY *= pSY;
            scaleX *= pSX;
            scaleY *= pSY;
        }
        float pR = pElement2.rotation;
        if (pR != Animation.CurveTimeline.LINEAR) {
            float cos = MathUtils.cosDeg(pR);
            float sin = MathUtils.sinDeg(pR);
            x = ((localX * cos) - (localY * sin)) + worldOriginX;
            y = (localY * cos) + (localX * sin) + worldOriginY;
            rotation += pR;
        } else {
            x = localX + worldOriginX;
            y = localY + worldOriginY;
        }
        float ox = element.originX;
        float oy = element.originY;
        float pW = pElement2.w;
        float pH = pElement2.h;
        GAnimationManager.GElementData gElementData = newElement;
        float w = element.w;
        gElementData.w = w;
        GAnimationManager.GElementData gElementData2 = newElement;
        float h = element.h;
        gElementData2.h = h;
        byte transMode2 = element.transMode;
        GAnimationManager.GElementData gElementData3 = newElement;
        byte newValue = (byte) (pElement2.transMode ^ transMode2);
        gElementData3.transMode = newValue;
        switch (transMode2 ^ newValue) {
            case 1:
                rotation = 360.0f - rotation;
                x = (pW - x) - w;
                ox = w - ox;
                break;
            case 2:
                rotation = 360.0f - rotation;
                y = (pH - y) - h;
                oy = h - oy;
                break;
            case 3:
                x = (pW - x) - w;
                y = (pH - y) - h;
                ox = w - ox;
                oy = h - oy;
                break;
        }
        newElement.res_dir = element.res_dir;
        newElement.res_name = element.res_name;
        newElement.res_type = element.res_type;
        newElement.scaleX = scaleX;
        newElement.scaleY = scaleY;
        newElement.originX = ox;
        newElement.originY = oy;
        newElement.x = pElement2.x + x;
        newElement.y = pElement2.y + y;
        newElement.rotation = rotation;
        newElement.argb = element.argb;
        return newElement;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.Batch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.TextureAtlas$AtlasRegion, float, float, float, float, float, float, float, float, float, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.Batch.draw(com.badlogic.gdx.graphics.Texture, float, float, float, float, int, int, int, int, boolean, boolean):void
      com.badlogic.gdx.graphics.g2d.Batch.draw(com.badlogic.gdx.graphics.g2d.TextureRegion, float, float, float, float, float, float, float, float, float, boolean):void */
    private void drawElement(Batch batch, GAnimationManager.GElementData element, TextureAtlas.AtlasRegion region) {
        String str = element.res_dir;
        String str2 = element.res_name;
        float x = element.x - getOriginX();
        float y = element.y - getOriginY();
        float ox = element.originX;
        float oy = element.originY;
        float scaleX = element.scaleX;
        float scaleY = element.scaleY;
        float rotation = element.rotation;
        float w = element.w;
        float h = element.h;
        int argb = element.argb;
        Color oldColor = batch.getColor();
        boolean isChangeColor = argb != -1;
        if (isChangeColor) {
            batch.setColor(oldColor.r, oldColor.g, oldColor.b, (((float) ((-16777216 & argb) >>> 24)) / 255.0f) * oldColor.a);
        }
        getRegion(region, element.transMode);
        if (scaleX == 1.0f && scaleY == 1.0f && rotation == Animation.CurveTimeline.LINEAR && !regionBuff.rotate) {
            batch.draw(regionBuff, x, y);
        } else {
            if (scaleX * scaleY < Animation.CurveTimeline.LINEAR) {
                rotation = 360.0f - rotation;
            }
            if (regionBuff.rotate) {
                batch.draw((TextureRegion) regionBuff, x, y, ox, oy, w, h, scaleX, scaleY, rotation, false);
            } else {
                batch.draw(regionBuff, x, y, ox, oy, w, h, scaleX, scaleY, rotation);
            }
        }
        if (isChangeColor) {
            batch.setColor(oldColor);
        }
    }

    private void drawFrame(Batch batch, Element[] elements) {
        for (int i = 0; i < elements.length; i++) {
            if (elements[i].isAnimation()) {
                int argb = elements[i].data.argb;
                Color oldColor = batch.getColor();
                boolean isChangeColor = argb != -1;
                if (isChangeColor) {
                    batch.setColor(oldColor.r, oldColor.g, oldColor.b, (((float) ((-16777216 & argb) >>> 24)) / 255.0f) * oldColor.a);
                }
                Element[] sub = elements[i].sub;
                for (int j = 0; j < sub.length; j++) {
                    GAnimationManager.GElementData subData = sub[j].data;
                    if (!this.isFastMode) {
                        subData = getElement(elements[i].data, subData);
                    }
                    drawElement(batch, getElement(getActorElement(), subData), sub[j].region);
                }
                if (isChangeColor) {
                    batch.setColor(oldColor);
                }
            } else {
                drawElement(batch, getElement(getActorElement(), elements[i].data), elements[i].region);
            }
        }
    }

    private void setRegion(TextureAtlas.AtlasRegion reg) {
        regionBuff.setRegion(reg);
        regionBuff.index = reg.index;
        regionBuff.name = reg.name;
        regionBuff.offsetX = reg.offsetX;
        regionBuff.offsetY = reg.offsetY;
        regionBuff.packedWidth = reg.packedWidth;
        regionBuff.packedHeight = reg.packedHeight;
        regionBuff.originalWidth = reg.originalWidth;
        regionBuff.originalHeight = reg.originalHeight;
        regionBuff.rotate = reg.rotate;
        regionBuff.splits = reg.splits;
    }

    private TextureAtlas.AtlasRegion getRegion(TextureAtlas.AtlasRegion reg, int transMode2) {
        if (regionBuff == null) {
            regionBuff = new TextureAtlas.AtlasRegion(reg);
        } else {
            setRegion(reg);
        }
        switch (transMode2) {
            case 1:
                regionBuff.flip(true, false);
                break;
            case 2:
                regionBuff.flip(false, true);
                break;
            case 3:
                regionBuff.flip(true, true);
                break;
        }
        return regionBuff;
    }

    public void act(float delta) {
        super.act(delta);
        if (this.player != null) {
            if (this.completeListener != null && this.player.isFinished()) {
                this.completeListener.complete(this);
                if (this.player == null) {
                    return;
                }
            }
            if (this.isAutoNextFrame) {
                this.curFrameIndex = this.player.getKeyFrameIndex(delta);
            } else {
                this.curFrameIndex = (short) this.player.getFrameIndex(this.curFrameIndex);
            }
            this.index = (short) (Math.abs((int) this.curFrameIndex) - 1);
            if (this.player.updateFrame()) {
                runFrameScript(this.index);
            }
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        if (this.player != null) {
            Color color = getColor();
            batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
            if (this.curFrameIndex >= 0) {
                drawFrame(batch, this.frameDat[this.index]);
            }
            if (isDebug) {
                float x = getX() - (getOriginX() * getScaleX());
                float y = getY() - (getOriginY() * getScaleY());
                float w = getWidth() * getScaleX();
                float h = getHeight() * getScaleY();
                float ox = getOriginX() * getScaleX();
                float oy = getOriginY() * getScaleY();
                float rotation = getRotation();
                if (this.transMode == 1 || this.transMode == 2) {
                    rotation = 360.0f - rotation;
                }
                GShapeTools.drawRectangle(batch, Color.GREEN, x, y, w, h, ox, oy, rotation, false);
            }
        }
    }

    public Actor hit(float x, float y, boolean touchable) {
        return super.hit(x + getOriginX(), y + getOriginY(), touchable);
    }
}
