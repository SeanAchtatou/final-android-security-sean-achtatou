package com.kbz.Animation;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectMap;
import com.kbz.AssetManger.GAssetsManager;
import com.kbz.AssetManger.GRes;
import com.sg.pak.PAK_ASSETS;
import java.util.Iterator;

public final class GAnimationManager {
    public static final int RES_TYEP_GIF = 1;
    public static final int RES_TYEP_IMAGE = 0;
    public static final int RES_TYEP_MGROUP = 2;
    private static ObjectMap<String, Array<String>> animationResTable = new ObjectMap<>();
    private static GAnimationScriptParser frameParser;
    private static GAnimationScriptParser initParser;
    private static ObjectMap<String, GAnimationSample> sampleTable = new ObjectMap<>();

    public interface GAnimationCompleteListener {
        void complete(Actor actor);
    }

    public interface GAnimationResListener {
        String changeResDir(String str, String str2);
    }

    public interface GAnimationScriptParser {
        void parse(Actor actor, String[][] strArr);
    }

    public static Array<String> getResTable(String animationPack) {
        return animationResTable.get(animationPack);
    }

    private GAnimationManager() {
    }

    public static void setAnimationInitParser(GAnimationScriptParser initParser2) {
        initParser = initParser2;
    }

    public static GAnimationScriptParser getAnimationInitParser() {
        return initParser;
    }

    public static void setFrameParser(GAnimationScriptParser frameParser2) {
        frameParser = frameParser2;
    }

    public static GAnimationScriptParser getFrameParser() {
        return frameParser;
    }

    public static String[] getAnimationList() {
        return (String[]) sampleTable.keys().toArray().toArray(String.class);
    }

    public static Array<String> getAnimationPackList(String animationPack) {
        Array<String> array = new Array<>();
        ObjectMap.Values<GAnimationSample> it = sampleTable.values().iterator();
        while (it.hasNext()) {
            GAnimationSample sample = (GAnimationSample) it.next();
            if (sample.packageName.equals(animationPack)) {
                array.add(sample.packageName);
            }
        }
        return array;
    }

    public static void clear() {
        sampleTable.clear();
    }

    public static void unload(int animation) {
        String animationPack = PAK_ASSETS.ANIMATION_NAME[animation];
        ObjectMap.Keys<String> it = sampleTable.keys().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            if (key.startsWith(animationPack)) {
                sampleTable.remove(key);
            }
        }
        Iterator<String> it2 = getResTable(animationPack).iterator();
        while (it2.hasNext()) {
            String resDir = it2.next();
            GAssetsManager.unloadTextureAtlas(PAK_ASSETS.ANIMATION_PATH + resDir + ".pack");
            System.out.println("animation unload texture _____________ " + resDir + ".pack");
        }
    }

    public static boolean isExist(String animationPack) {
        ObjectMap.Values<GAnimationSample> it = sampleTable.values().iterator();
        while (it.hasNext()) {
            if (((GAnimationSample) it.next()).packageName.equals(animationPack)) {
                return true;
            }
        }
        return false;
    }

    private static String[][] toOrders(String[] script) {
        if (script == null || script.length == 0) {
            return null;
        }
        Array<String[]> buff = new Array<>();
        for (int i = 0; i < script.length; i++) {
            String[] order = GTools.splitString(script[i].substring(1, script[i].length()).trim(), "");
            if (order != null) {
                buff.add(order);
            }
        }
        return (String[][]) buff.toArray(String[].class);
    }

    public static void load(String animationPack) {
        String animationPack2 = animationPack + ".json";
        if (!isExist(animationPack2)) {
            JsonValue groups = new JsonReader().parse(GRes.openFileHandle(PAK_ASSETS.ANIMATION_PATH + animationPack2));
            int groupSize = groups.size;
            int i = 0;
            while (i < groupSize) {
                JsonValue groupData = groups.get(i);
                String animationName = groupData.getString("name");
                if (!isAnimationSampleExist(animationName)) {
                    float originX = groupData.getFloat("originX");
                    float originY = groupData.getFloat("originY");
                    float width = groupData.getFloat("w");
                    float height = groupData.getFloat("h");
                    String[] initScript = GTools.jsonValueToStringArray(groupData.get("script"));
                    JsonValue frames = groupData.get("frames");
                    int frameSize = frames.size;
                    GFrameData[] frameDatas = new GFrameData[frameSize];
                    GElementData[][] data = new GElementData[frameSize][];
                    for (int j = 0; j < frameSize; j++) {
                        JsonValue frameData = frames.get(j);
                        float delay = frameData.getFloat("delay");
                        JsonValue elements = frameData.get("elements");
                        data[j] = new GElementData[elements.size];
                        for (int k = 0; k < elements.size; k++) {
                            data[j][k] = parseElementData(animationPack2, elements.get(k));
                        }
                        frameDatas[j] = new GFrameData((int) delay, toOrders(GTools.jsonValueToStringArray(frameData.get("script"))));
                    }
                    sampleTable.put(createAnimationKey(animationPack2, animationName), new GAnimationSample(animationPack2, animationName, originX, originY, width, height, toOrders(initScript), frameDatas, data));
                    animationResTable.put(animationPack2, new Array());
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    protected static String createAnimationKey(String animationPack, String animationName) {
        return animationPack + "@" + animationName;
    }

    protected static GAnimationSample getSampleAnimation(String animationPath) {
        GAnimationSample sample = getSample(animationPath);
        if (sample == null) {
            return null;
        }
        return sample;
    }

    protected static GAnimationSample getSample(String animationPath) {
        return sampleTable.get(animationPath);
    }

    protected static GAnimationSample getSample(String animationPack, String animationName) {
        return getSample(createAnimationKey(animationPack, animationName));
    }

    public static boolean isAnimationSampleExist(String animationName) {
        return sampleTable.containsKey(animationName);
    }

    private static GElementData parseElementData(String animationPack, JsonValue elementData) {
        GElementData data = new GElementData(null);
        data.res_dir = elementData.getString("res_dir");
        data.res_name = elementData.getString("res_name");
        data.res_type = (byte) elementData.getInt("res_type");
        if (data.res_type == 2) {
            data.res_dir = animationPack;
        }
        data.name = elementData.getString("name");
        data.curFrame = (short) elementData.getInt("curFrame");
        data.x = elementData.getFloat("x");
        data.y = elementData.getFloat("y");
        data.w = elementData.getFloat("w");
        data.h = elementData.getFloat("h");
        data.originX = elementData.getFloat("originX");
        data.originY = elementData.getFloat("originY");
        data.scaleX = elementData.getFloat("scaleX");
        data.scaleY = elementData.getFloat("scaleY");
        data.rotation = elementData.getFloat("rotation");
        data.transMode = (byte) elementData.getInt("transMode");
        data.argb = elementData.getInt("argb");
        data.script = GTools.jsonValueToStringArray(elementData.get("script"));
        data.changeData();
        return data;
    }

    static class GElementData {
        public int argb;
        public short curFrame;
        public float h;
        public String name;
        public float originX;
        public float originY;
        public String res_dir;
        public String res_name;
        public byte res_type;
        public float rotation;
        public float scaleX;
        public float scaleY;
        public String[] script;
        public byte transMode;
        public float w;
        public float x;
        public float y;

        public void changeData() {
            boolean flipX;
            boolean flipY;
            if (this.transMode != 0) {
                if ((this.transMode & 1) != 0) {
                    flipX = true;
                } else {
                    flipX = false;
                }
                if ((this.transMode & 2) != 0) {
                    flipY = true;
                } else {
                    flipY = false;
                }
                if (flipX) {
                    this.rotation = 360.0f - this.rotation;
                    this.originX = this.w - this.originX;
                }
                if (flipY) {
                    this.rotation = 360.0f - this.rotation;
                    this.originY = this.h - this.originY;
                }
            }
        }

        public GElementData(GElementData data) {
            if (data != null) {
                this.res_dir = data.res_dir;
                this.res_name = data.res_name;
                this.res_type = data.res_type;
                this.curFrame = data.curFrame;
                this.transMode = data.transMode;
                this.script = data.script;
                this.name = data.name;
                this.originX = data.originX;
                this.originY = data.originY;
                this.x = data.x;
                this.y = data.y;
                this.w = data.w;
                this.h = data.h;
                this.scaleX = data.scaleX;
                this.scaleY = data.scaleY;
                this.rotation = data.rotation;
                this.argb = data.argb;
            }
        }
    }

    static class GFrameData {
        public int delay;
        public String[][] script;

        public GFrameData(int delay2, String[][] script2) {
            this.delay = delay2;
            this.script = script2;
        }
    }

    static class GAnimationSample {
        GElementData[][] elementDatas;
        GFrameData[] frameDatas;
        float height;
        String[][] initScript;
        String name;
        float originX;
        float originY;
        String packageName;
        float width;

        public GAnimationSample(String packageName2, String name2, float originX2, float originY2, float width2, float height2, String[][] initScript2, GFrameData[] frameDatas2, GElementData[][] elementDatas2) {
            this.packageName = packageName2;
            this.name = name2;
            this.originX = originX2;
            this.originY = originY2;
            this.width = width2;
            this.height = height2;
            this.initScript = initScript2;
            this.frameDatas = frameDatas2;
            this.elementDatas = elementDatas2;
        }
    }
}
