package com.kbz.Animation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonValue;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.util.GameStage;
import java.util.Random;

public class GTools {
    public static final byte CENTER = 4;
    public static final byte CENTER_BOTTOM = 5;
    public static final byte CENTER_TOP = 6;
    public static final byte LEFT_BOTTOM = 1;
    public static final byte LEFT_TOP = 0;
    public static final byte RIGHT_BOTTOM = 3;
    public static final byte RIGHT_TOP = 2;
    static Random rnd = new Random();

    public static float[] toScreenPoint(Actor actor) {
        float[] point = {actor.getX(), actor.getY()};
        for (Actor parent = actor.getParent(); parent != null; parent = parent.getParent()) {
            if (parent instanceof Group) {
                point[0] = point[0] + parent.getX();
                point[1] = point[1] + parent.getY();
            }
        }
        return point;
    }

    public static float getX(float x, float w, int anchor) {
        switch (anchor) {
            case 2:
            case 3:
                return x - w;
            case 4:
            case 5:
            case 6:
                return x - (w / 2.0f);
            default:
                return x;
        }
    }

    public static float getY(float y, float h, int anchor) {
        switch (anchor) {
            case 1:
            case 3:
            case 5:
                return y - h;
            case 2:
            default:
                return y;
            case 4:
                return y - (h / 2.0f);
        }
    }

    public static final boolean isDraw(float dx, float dy, float dw, float dh, int anchor) {
        float w = GameStage.getStageWidth();
        float h = GameStage.getStageHeight();
        switch (anchor) {
            case 0:
                if ((dx <= (-dw) || dx >= w) && dy > (-dh) && dy < h) {
                    return false;
                }
                break;
            case 1:
                if ((dx <= (-dw) || dx >= w) && dy > Animation.CurveTimeline.LINEAR && dy < h + dh) {
                    return false;
                }
                break;
        }
        return true;
    }

    public static float[] getVertices(float[] vertices, float x, float y, float width, float height, float originX, float originY, float rotation, float scaleX, float scaleY) {
        float localX = -originX;
        float localY = -originY;
        float localX2 = localX + width;
        float localY2 = localY + height;
        float worldOriginX = x - localX;
        float worldOriginY = y - localY;
        if (!(scaleX == 1.0f && scaleY == 1.0f)) {
            localX *= scaleX;
            localY *= scaleY;
            localX2 *= scaleX;
            localY2 *= scaleY;
        }
        if (rotation != Animation.CurveTimeline.LINEAR) {
            float cos = MathUtils.cosDeg(rotation);
            float sin = MathUtils.sinDeg(rotation);
            float localXCos = localX * cos;
            float localXSin = localX * sin;
            float localY2Cos = localY2 * cos;
            float localY2Sin = localY2 * sin;
            float x1 = (localXCos - (localY * sin)) + worldOriginX;
            float y1 = (localY * cos) + localXSin + worldOriginY;
            vertices[0] = x1;
            vertices[1] = y1;
            float x2 = (localXCos - localY2Sin) + worldOriginX;
            float y2 = localY2Cos + localXSin + worldOriginY;
            vertices[2] = x2;
            vertices[3] = y2;
            float x3 = ((localX2 * cos) - localY2Sin) + worldOriginX;
            float y3 = localY2Cos + (localX2 * sin) + worldOriginY;
            vertices[4] = x3;
            vertices[5] = y3;
            vertices[6] = (x1 + x3) - x2;
            vertices[7] = y3 - (y2 - y1);
        } else {
            float x12 = localX + worldOriginX;
            float y12 = localY + worldOriginY;
            float x22 = localX2 + worldOriginX;
            float y22 = localY2 + worldOriginY;
            vertices[0] = x12;
            vertices[1] = y12;
            vertices[2] = x12;
            vertices[3] = y22;
            vertices[4] = x22;
            vertices[5] = y22;
            vertices[6] = x22;
            vertices[7] = y12;
        }
        return vertices;
    }

    public static String[] jsonValueToStringArray(JsonValue jsonValue) {
        if (jsonValue == null) {
            return null;
        }
        String[] result = new String[jsonValue.size];
        for (int i = 0; i < result.length; i++) {
            result[i] = jsonValue.getString(i);
        }
        return result;
    }

    public static String[] splitString(String src, String key) {
        if (src == null) {
            return null;
        }
        Array<String> buff = new Array<>();
        int w = 0;
        boolean end = false;
        while (!end) {
            int pos = src.indexOf(key, w);
            if (pos == -1) {
                pos = src.length();
                end = true;
            }
            int endIndex = pos;
            if (pos > 0 && src.charAt(pos - 1) == 13) {
                endIndex = pos - 1;
            }
            String s = src.substring(w, endIndex).trim();
            if (!s.equals("")) {
                buff.add(s);
            }
            w = pos + key.length();
        }
        return (String[]) buff.toArray(String.class);
    }

    public static Preferences getPreferences(String name) {
        return Gdx.app.getPreferences(name);
    }

    public static boolean hit(float x1, float y1, float w1, float h1, float x2, float y2, float w2, float h2) {
        return x1 <= x2 + w2 && x2 <= x1 + w1 && y1 <= y2 + h2 && y2 <= y1 + h1;
    }

    public static boolean hit2(float x1, float y1, float w1, float h1, float x2, float y2, float w2, float h2) {
        return x1 < x2 + w2 && x2 < x1 + w1 && y1 < y2 + h2 && y2 < y1 + h1;
    }

    public static int getRandom(int n) {
        if (n == 0) {
            return 0;
        }
        return Math.abs(rnd.nextInt()) % (n + 1);
    }

    public static int getRandom(int n, int m) {
        if (n == m) {
            return n;
        }
        if (n > m) {
            int temp = n;
            n = m;
            m = temp;
        }
        return n + (Math.abs(rnd.nextInt()) % ((m - n) + 1));
    }

    public static float getLineAngle(float sx, float sy, float ex, float ey) {
        float targetDeg = (float) (Math.acos((double) ((ex - sx) / ((float) Math.sqrt((double) (((sx - ex) * (sx - ex)) + ((sy - ey) * (sy - ey))))))) * 57.2957763671875d);
        if (ey > sy) {
            targetDeg = 360.0f - targetDeg;
        }
        return toAngle((double) (360.0f - targetDeg));
    }

    public static float toAngle(double a) {
        return (float) ((a + 360.0d) % 360.0d);
    }
}
