package com.kbz.Animation;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.ShortArray;
import com.kbz.Animation.GAnimationManager;
import com.kbz.esotericsoftware.spine.Animation;

public class GAnimationPlayer {
    public static final byte LOOP = 2;
    public static final byte LOOP_PINGPONG = 4;
    public static final byte LOOP_RANDOM = 5;
    public static final byte LOOP_REVERSED = 3;
    public static final byte NORMAL = 0;
    public static final byte REVERSED = 1;
    private float animationDuration;
    private float frameDuration;
    private short[] frameSequence;
    private short frameSequenceIndex;
    private boolean isNext;
    private short lastFrameSequenceIndex;
    private byte playMode;
    private float runTimeCount;

    public GAnimationPlayer(float frameDuration2, GAnimationManager.GFrameData[] frames) {
        this(frameDuration2, frames, (byte) 0);
    }

    public GAnimationPlayer(float frameDuration2, GAnimationManager.GFrameData[] frames, byte playMode2) {
        this.playMode = 0;
        this.lastFrameSequenceIndex = -1;
        this.isNext = false;
        this.frameDuration = frameDuration2;
        this.playMode = playMode2;
        this.frameSequence = initFrameSequence(frames);
        this.animationDuration = ((float) this.frameSequence.length) * frameDuration2;
    }

    public float getFrameDuration() {
        return this.frameDuration;
    }

    public void setFrameDuration(float frameDuration2) {
        this.frameDuration = frameDuration2;
    }

    public int frameSequenceLen() {
        return this.frameSequence.length;
    }

    public GAnimationPlayer(float frameDuration2, TextureRegion[] regions, int delay) {
        this(frameDuration2, regions, delay, (byte) 0);
    }

    public GAnimationPlayer(float frameDuration2, TextureRegion[] regions, int delay, byte playMode2) {
        this.playMode = 0;
        this.lastFrameSequenceIndex = -1;
        this.isNext = false;
        this.frameDuration = frameDuration2;
        this.playMode = playMode2;
        this.frameSequence = initFrameSequence(regions, delay);
        this.animationDuration = ((float) this.frameSequence.length) * frameDuration2;
    }

    public GAnimationPlayer(float frameDuration2, int[] delays) {
        this(frameDuration2, delays, (byte) 0);
    }

    public GAnimationPlayer(float frameDuration2, int[] delays, byte playMode2) {
        this.playMode = 0;
        this.lastFrameSequenceIndex = -1;
        this.isNext = false;
        this.frameDuration = frameDuration2;
        this.playMode = playMode2;
        this.frameSequence = initFrameSequence(delays);
        this.animationDuration = ((float) this.frameSequence.length) * frameDuration2;
    }

    public static int getRealKeyFrame(GAnimationManager.GFrameData[] frames, int curFrame) {
        int index = 0;
        for (int i = 0; i < frames.length; i++) {
            int delay = frames[i].delay;
            int j = 0;
            while (j < Math.abs(delay)) {
                int index2 = index + 1;
                if (index == curFrame) {
                    return i;
                }
                j++;
                index = index2;
            }
        }
        return -1;
    }

    private static short[] initFrameSequence(TextureRegion[] regions, int delay) {
        ShortArray sequenceBuff = new ShortArray();
        for (int i = 0; i < regions.length; i++) {
            for (int j = 0; j < delay; j++) {
                sequenceBuff.add(i + 1);
            }
        }
        return sequenceBuff.toArray();
    }

    private static short[] initFrameSequence(GAnimationManager.GFrameData[] frames) {
        ShortArray sequenceBuff = new ShortArray();
        for (int i = 0; i < frames.length; i++) {
            int delay = frames[i].delay;
            for (int j = 0; j < Math.abs(delay); j++) {
                int temp = i + 1;
                if (delay <= 0) {
                    temp = -temp;
                }
                sequenceBuff.add(temp);
            }
        }
        return sequenceBuff.toArray();
    }

    private static short[] initFrameSequence(int[] delays) {
        ShortArray sequenceBuff = new ShortArray();
        for (int i = 0; i < delays.length; i++) {
            int delay = delays[i];
            for (int j = 0; j < Math.abs(delay); j++) {
                int temp = i + 1;
                if (delay <= 0) {
                    temp = -temp;
                }
                sequenceBuff.add(temp);
            }
        }
        return sequenceBuff.toArray();
    }

    public void reset() {
        this.runTimeCount = Animation.CurveTimeline.LINEAR;
        this.lastFrameSequenceIndex = -1;
    }

    public boolean updateFrame() {
        if (this.lastFrameSequenceIndex == this.frameSequenceIndex) {
            return false;
        }
        this.lastFrameSequenceIndex = this.frameSequenceIndex;
        return true;
    }

    public boolean isFinished() {
        switch (this.playMode) {
            case 1:
            case 3:
                if (this.frameSequenceIndex != 0) {
                    return false;
                }
                return true;
            case 2:
            default:
                if (this.frameSequence.length - 1 == this.frameSequenceIndex) {
                    return true;
                }
                return false;
            case 4:
                if (this.frameSequenceIndex != 1 || this.lastFrameSequenceIndex <= this.frameSequenceIndex) {
                    return false;
                }
                return true;
            case 5:
                return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(int, int):int}
     arg types: [int, short]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int} */
    public short getKeyFrameIndex(float delta) {
        short length = (short) this.frameSequence.length;
        this.frameSequenceIndex = (short) ((int) (this.runTimeCount / this.frameDuration));
        this.runTimeCount += delta;
        if (length == 1 || this.frameSequenceIndex < 0) {
            return 1;
        }
        switch (this.playMode) {
            case 0:
                this.frameSequenceIndex = (short) Math.min(length - 1, (int) this.frameSequenceIndex);
                break;
            case 1:
                this.frameSequenceIndex = (short) Math.max((length - this.frameSequenceIndex) - 1, 0);
                break;
            case 2:
                this.frameSequenceIndex = (short) (this.frameSequenceIndex % length);
                break;
            case 3:
                this.frameSequenceIndex = (short) (this.frameSequenceIndex % length);
                this.frameSequenceIndex = (short) ((length - this.frameSequenceIndex) - 1);
                break;
            case 4:
                this.frameSequenceIndex = (short) (this.frameSequenceIndex % ((length * 2) - 2));
                if (this.frameSequenceIndex >= length) {
                    this.frameSequenceIndex = (short) ((length - 2) - (this.frameSequenceIndex - length));
                    break;
                }
                break;
            case 5:
                this.frameSequenceIndex = (short) MathUtils.random(length - 1);
                break;
            default:
                this.frameSequenceIndex = (short) Math.min(length - 1, (int) this.frameSequenceIndex);
                break;
        }
        return this.frameSequence[this.frameSequenceIndex];
    }

    public void next() {
        this.isNext = true;
    }

    public int getFrameIndex(int curIndex) {
        if (!this.isNext) {
            return curIndex;
        }
        this.isNext = false;
        int curIndex2 = curIndex + 1;
        if (curIndex2 <= this.frameSequence.length) {
            return curIndex2;
        }
        switch (this.playMode) {
            case 0:
                return this.frameSequence.length;
            case 1:
            default:
                return curIndex2;
            case 2:
                return 1;
        }
    }
}
