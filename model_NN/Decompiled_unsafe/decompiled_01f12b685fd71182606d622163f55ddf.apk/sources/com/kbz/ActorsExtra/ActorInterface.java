package com.kbz.ActorsExtra;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kbz.Actors.ActorShapeSprite;
import com.kbz.tools.PolygonHit;
import com.sg.util.GameLayer;
import com.sg.util.GameStage;

public class ActorInterface extends Actor {
    public static int id;
    public static int maxId = 0;
    public static float scaleF = 1.0f;
    public int OriginType;
    private ActorShapeSprite actorShapeSprite;
    public int anchor;
    public int attack;
    public int attackArea = 40;
    public int[] attackBox = null;
    float cdTime;
    public int[] coxBox = null;
    public int curIndex;
    public int curStatus;
    public short[][] data;
    public int defend;
    int dir;
    public int double_attack;
    private int drawLayer;
    public int exp;
    public int exp_up;
    int faceDir;
    public int h;
    public int[] hitArray;
    public int hitTh;
    public int hitTw;
    public int hitTx;
    public int hitTy;
    int hp;
    int hp_max;
    public short[][] imgData;
    public int imgIndex = 0;
    public int index;
    public int injureTime;
    public boolean isLeft;
    public boolean isRepeat;
    public int lastStatus;
    int level = 1;
    int lucky;
    public short[][] motion;
    public int mp;
    public int mp_max;
    public int nextStatus;
    public Polygon polygon = new Polygon();
    private float[] polygonFloat0 = new float[8];
    public float rotaAngle;
    float shotFrequency;
    public float speedX;
    public float speedY;
    public String[] spineName;
    public int startPosX;
    public int startPosY;
    public int type;
    public int w;

    public float getCdTime() {
        return this.cdTime;
    }

    public void setCdTime(float obj) {
        this.cdTime = obj;
    }

    public float getShotFrequency() {
        return this.shotFrequency;
    }

    public void setShotFrequency(float obj) {
        this.shotFrequency = obj;
    }

    public float getStatus() {
        return (float) this.curStatus;
    }

    public void setStatus(int st) {
        if (this.curStatus != st) {
            this.curStatus = st;
            this.index = 0;
        }
    }

    public void setDir(int obj) {
        this.dir = obj;
    }

    public int getDir() {
        return this.dir;
    }

    public void setFaceDir(int obj) {
        this.faceDir = obj;
    }

    public int getFaceDir() {
        return this.faceDir;
    }

    public int getLevel() {
        return this.level;
    }

    public void setLevel(int level2) {
        this.level = level2;
    }

    public void setHp(int obj) {
        this.hp = obj;
    }

    public int getHp() {
        return this.hp;
    }

    public void setHp_max(int obj) {
        this.hp_max = obj;
    }

    public int getHp_max() {
        return this.hp_max;
    }

    public void setMp(int obj) {
        this.mp = obj;
    }

    public int getMp() {
        return this.mp;
    }

    public void setMp_max(int obj) {
        this.mp_max = obj;
    }

    public int getMp_max() {
        return this.mp_max;
    }

    public void setAttack(int obj) {
        this.attack = obj;
    }

    public int getAttack() {
        return this.attack;
    }

    public void setDefend(int obj) {
        this.defend = obj;
    }

    public int getDefend() {
        return this.defend;
    }

    public void setLucky(int obj) {
        this.lucky = obj;
    }

    public int getLucky() {
        return this.lucky;
    }

    public void setDouble_attack(int obj) {
        this.double_attack = obj;
    }

    public int getDouble_attack() {
        return this.double_attack;
    }

    public void setInjureTime(int obj) {
        this.double_attack = obj;
    }

    public int getInjureTime() {
        return this.double_attack;
    }

    public int getTpye() {
        return this.type;
    }

    public void setTpye(int obj) {
        this.type = obj;
    }

    public int getExp() {
        return this.exp;
    }

    public void setExp(int obj) {
        this.exp = obj;
    }

    public int getExp_up() {
        return this.exp_up;
    }

    public void setExp_up(int obj) {
        this.exp_up = obj;
    }

    public int getDrawLayer() {
        return this.drawLayer;
    }

    public void setDrawLayer(int obj) {
        this.drawLayer = obj;
    }

    public int getAnchor() {
        return this.anchor;
    }

    public void setAnchor(int obj) {
        this.anchor = obj;
    }

    public static void setId() {
        id = maxId;
        maxId++;
    }

    public static int getId() {
        return id;
    }

    public void run() {
    }

    public void setType(int type2) {
        this.index = 0;
        this.type = type2;
    }

    public int getStatusNum(int curS, short[][] data2) {
        if (data2 == null) {
            return -1;
        }
        for (int i = 0; i < data2.length; i++) {
            if (data2[i][0] == curS) {
                return i;
            }
        }
        return -1;
    }

    public int getStatusNum_dir(int curS, int dir2, short[][] data2) {
        for (int i = 0; i < data2.length; i++) {
            if (data2[i][0] == curS && data2[i][1] == dir2) {
                return i;
            }
        }
        return -1;
    }

    /* JADX WARN: Type inference failed for: r12v0, types: [short[]] */
    /* JADX WARN: Type inference failed for: r5v0, types: [short, int] */
    /* JADX WARN: Type inference failed for: r3v6, types: [short, int] */
    /* JADX WARN: Type inference failed for: r6v3, types: [short, int] */
    /* JADX WARN: Type inference failed for: r5v4, types: [short] */
    /* JADX WARN: Type inference failed for: r3v15, types: [short, int] */
    /* JADX WARN: Type inference failed for: r5v8, types: [short, int] */
    /* JADX WARN: Type inference failed for: r3v21, types: [short, int] */
    /* JADX WARN: Type inference failed for: r5v12, types: [short, int] */
    /* JADX WARN: Type inference failed for: r3v27, types: [short, int] */
    /* JADX WARN: Type inference failed for: r5v16, types: [short, int] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int[] hitArea(short[] r12, int r13, boolean r14, boolean r15) {
        /*
            r11 = this;
            r10 = 1073741824(0x40000000, float:2.0)
            r9 = 3
            r8 = 2
            r7 = 1065353216(0x3f800000, float:1.0)
            r4 = 0
            r0 = 0
            if (r14 == 0) goto L_0x000b
            r0 = 4
        L_0x000b:
            r3 = 4
            int[] r1 = new int[r3]
            int r3 = r13 * 8
            int r3 = r3 + r0
            short r5 = r12[r3]
            if (r15 == 0) goto L_0x00a7
            r3 = r4
        L_0x0016:
            int r3 = r3 + r5
            r1[r4] = r3
            r3 = 1
            int r5 = r13 * 8
            int r5 = r5 + r0
            int r5 = r5 + 3
            short r5 = r12[r5]
            r1[r3] = r5
            int r3 = r13 * 8
            int r3 = r3 + r0
            int r3 = r3 + 0
            short r3 = r12[r3]
            int r5 = r13 * 8
            int r5 = r5 + r0
            int r5 = r5 + 2
            short r5 = r12[r5]
            int r3 = r3 - r5
            int r3 = java.lang.Math.abs(r3)
            r1[r8] = r3
            int r3 = r13 * 8
            int r3 = r3 + r0
            int r3 = r3 + 1
            short r3 = r12[r3]
            int r5 = r13 * 8
            int r5 = r5 + r0
            int r5 = r5 + 3
            short r5 = r12[r5]
            int r3 = r3 - r5
            int r3 = java.lang.Math.abs(r3)
            r1[r9] = r3
            int r3 = r13 * 8
            int r3 = r3 + 0
            int r3 = r3 + 0
            short r3 = r12[r3]
            int r5 = r13 * 8
            int r5 = r5 + 0
            int r5 = r5 + 2
            short r5 = r12[r5]
            int r3 = r3 - r5
            int r2 = java.lang.Math.abs(r3)
            r3 = r1[r4]
            float r3 = (float) r3
            float r5 = com.kbz.ActorsExtra.ActorInterface.scaleF
            float r5 = r5 - r7
            float r6 = (float) r2
            float r5 = r5 * r6
            float r5 = r5 / r10
            float r3 = r3 - r5
            int r3 = (int) r3
            r1[r4] = r3
            if (r15 != 0) goto L_0x0084
            r3 = r1[r4]
            int r3 = -r3
            r1[r4] = r3
            r3 = r1[r4]
            float r3 = (float) r3
            float r5 = com.kbz.ActorsExtra.ActorInterface.scaleF
            float r5 = r5 - r7
            r6 = r1[r8]
            float r6 = (float) r6
            float r5 = r5 * r6
            float r3 = r3 - r5
            int r3 = (int) r3
            r1[r4] = r3
        L_0x0084:
            r3 = 1
            r4 = r1[r3]
            float r4 = (float) r4
            float r5 = com.kbz.ActorsExtra.ActorInterface.scaleF
            float r5 = r5 - r7
            r6 = r1[r9]
            float r6 = (float) r6
            float r5 = r5 * r6
            float r5 = r5 / r10
            float r4 = r4 + r5
            int r4 = (int) r4
            r1[r3] = r4
            r3 = r1[r8]
            float r3 = (float) r3
            float r4 = com.kbz.ActorsExtra.ActorInterface.scaleF
            float r3 = r3 * r4
            int r3 = (int) r3
            r1[r8] = r3
            r3 = r1[r9]
            float r3 = (float) r3
            float r4 = com.kbz.ActorsExtra.ActorInterface.scaleF
            float r3 = r3 * r4
            int r3 = (int) r3
            r1[r9] = r3
            return r1
        L_0x00a7:
            int r3 = r13 * 8
            int r3 = r3 + r0
            int r3 = r3 + 0
            short r3 = r12[r3]
            int r6 = r13 * 8
            int r6 = r6 + r0
            int r6 = r6 + 2
            short r6 = r12[r6]
            int r3 = r3 - r6
            int r3 = java.lang.Math.abs(r3)
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kbz.ActorsExtra.ActorInterface.hitArea(short[], int, boolean, boolean):int[]");
    }

    public void getBox() {
        boolean z = true;
        try {
            if (this.data != null) {
                this.attackBox = hitArea(this.data[1], this.curIndex, true, !this.isLeft);
                short[] sArr = this.data[1];
                int i = this.curIndex;
                if (this.isLeft) {
                    z = false;
                }
                this.coxBox = hitArea(sArr, i, false, z);
                return;
            }
            System.out.println("data null");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ActorShapeSprite getActorShapeSprite() {
        return this.actorShapeSprite;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void
     arg types: [int, float[]]
     candidates:
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, com.kbz.tools.Rect):void
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void */
    public void initHitPolygon(int tx, int ty, int tw, int th, float OriginX, float OriginY) {
        this.hitTx = tx;
        this.hitTy = ty;
        this.hitTw = tw;
        this.hitTh = th;
        this.polygonFloat0[0] = 0.0f;
        this.polygonFloat0[1] = 0.0f;
        this.polygonFloat0[2] = (float) tw;
        this.polygonFloat0[3] = 0.0f;
        this.polygonFloat0[4] = (float) (tw + 0);
        this.polygonFloat0[5] = (float) (th + 0);
        this.polygonFloat0[6] = 0.0f;
        this.polygonFloat0[7] = (float) th;
        this.polygon.setVertices(this.polygonFloat0);
        this.polygon.setPosition(getX() + ((float) tx), getY() + ((float) ty));
        this.polygon.setOrigin(OriginX, OriginY);
        this.polygon.setRotation(360.0f - this.rotaAngle);
        if (PolygonHit.isShowRect) {
            this.actorShapeSprite = new ActorShapeSprite();
            this.actorShapeSprite.createMyRect(false, this.polygon.getTransformedVertices());
            GameStage.addActorToMyStage(GameLayer.max, this.actorShapeSprite);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void
     arg types: [int, float[]]
     candidates:
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, com.kbz.tools.Rect):void
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void */
    public void initHitPolygon(int tx, int ty, int tw, int th) {
        this.hitTx = tx;
        this.hitTy = ty;
        this.hitTw = tw;
        this.hitTh = th;
        this.polygonFloat0[0] = 0.0f;
        this.polygonFloat0[1] = 0.0f;
        this.polygonFloat0[2] = (float) tw;
        this.polygonFloat0[3] = 0.0f;
        this.polygonFloat0[4] = (float) (tw + 0);
        this.polygonFloat0[5] = (float) (th + 0);
        this.polygonFloat0[6] = 0.0f;
        this.polygonFloat0[7] = (float) th;
        this.polygon.setVertices(this.polygonFloat0);
        this.polygon.setPosition(getX() + ((float) tx), getY() + ((float) ty));
        this.polygon.setOrigin((float) (tw / 2), (float) (th / 2));
        this.polygon.setRotation(360.0f - this.rotaAngle);
        if (PolygonHit.isShowRect) {
            this.actorShapeSprite = new ActorShapeSprite();
            this.actorShapeSprite.createMyRect(false, this.polygon.getTransformedVertices());
            GameStage.addActorToMyStage(GameLayer.max, this.actorShapeSprite);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void
     arg types: [int, float[]]
     candidates:
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, com.kbz.tools.Rect):void
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void */
    public void initHitPolygon(int[] hitArray2, float OriginX, float OriginY) {
        int tx = hitArray2[0];
        int ty = hitArray2[1];
        int tw = hitArray2[2];
        int th = hitArray2[3];
        this.hitTx = tx;
        this.hitTy = ty;
        this.hitTw = tw;
        this.hitTh = th;
        this.polygonFloat0[0] = 0.0f;
        this.polygonFloat0[1] = 0.0f;
        this.polygonFloat0[2] = (float) tw;
        this.polygonFloat0[3] = 0.0f;
        this.polygonFloat0[4] = (float) (tw + 0);
        this.polygonFloat0[5] = (float) (th + 0);
        this.polygonFloat0[6] = 0.0f;
        this.polygonFloat0[7] = (float) th;
        this.polygon.setVertices(this.polygonFloat0);
        this.polygon.setPosition(getX() + ((float) tx), getY() + ((float) ty));
        this.polygon.setOrigin(OriginX, OriginY);
        this.polygon.setRotation(360.0f - this.rotaAngle);
        if (PolygonHit.isShowRect) {
            this.actorShapeSprite = new ActorShapeSprite();
            this.actorShapeSprite.createMyRect(false, this.polygon.getTransformedVertices());
            GameStage.addActorToMyStage(GameLayer.max, this.actorShapeSprite);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void
     arg types: [int, float[]]
     candidates:
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, com.kbz.tools.Rect):void
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void */
    public void initHitPolygon(int[] hitArray2) {
        int tx = hitArray2[0];
        int ty = hitArray2[1];
        int tw = hitArray2[2];
        int th = hitArray2[3];
        this.hitTx = tx;
        this.hitTy = ty;
        this.hitTw = tw;
        this.hitTh = th;
        this.polygonFloat0[0] = 0.0f;
        this.polygonFloat0[1] = 0.0f;
        this.polygonFloat0[2] = (float) tw;
        this.polygonFloat0[3] = 0.0f;
        this.polygonFloat0[4] = (float) (tw + 0);
        this.polygonFloat0[5] = (float) (th + 0);
        this.polygonFloat0[6] = 0.0f;
        this.polygonFloat0[7] = (float) th;
        this.polygon.setVertices(this.polygonFloat0);
        this.polygon.setPosition(getX() + ((float) tx), getY() + ((float) ty));
        this.polygon.setOrigin((float) (tw / 2), (float) (th / 2));
        this.polygon.setRotation(360.0f - this.rotaAngle);
        if (PolygonHit.isShowRect) {
            this.actorShapeSprite = new ActorShapeSprite();
            this.actorShapeSprite.createMyRect(false, this.polygon.getTransformedVertices());
            GameStage.addActorToMyStage(GameLayer.max, this.actorShapeSprite);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void
     arg types: [int, float[]]
     candidates:
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, com.kbz.tools.Rect):void
      com.kbz.Actors.ActorShapeSprite.createMyRect(boolean, float[]):void */
    public void updataHitPolygon() {
        if (this.polygon != null) {
            this.polygon.setPosition(getX() + ((float) this.hitTx), getY() + ((float) this.hitTy));
            this.polygon.setRotation(360.0f - this.rotaAngle);
            if (PolygonHit.isShowRect) {
                this.actorShapeSprite.createMyRect(false, this.polygon.getTransformedVertices());
            }
        }
    }
}
