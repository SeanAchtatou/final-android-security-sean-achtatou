package com.kbz.ActorsExtra;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.kbz.tools.Tools;
import com.sg.pak.GameConstant;
import com.sg.util.GameLayer;
import com.sg.util.GameStage;

public class ActorImageExtra extends ActorInterface implements GameConstant {
    int anchor;
    float clipH;
    float clipW;
    protected TextureRegion img;
    int imgIndex;
    boolean isFlipX;
    boolean isFlipY;

    public ActorImageExtra(int imgID) {
        this.imgIndex = imgID;
        this.img = Tools.getImage(imgID);
        setSize((float) this.img.getRegionWidth(), (float) this.img.getRegionHeight());
        this.anchor = 12;
    }

    public ActorImageExtra(int imgID, int x, int y) {
        this.imgIndex = imgID;
        this.img = Tools.getImage(imgID);
        setSize((float) this.img.getRegionWidth(), (float) this.img.getRegionHeight());
        this.anchor = 12;
        setPosition((float) x, (float) y);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, int):void
     arg types: [int, int, int, int, int, int]
     candidates:
      com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
      com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, int):void */
    public ActorImageExtra(int imgID, int x, int y, int biglayer) {
        init(imgID, x, y, false, 12, biglayer);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, int):void
     arg types: [int, int, int, int, int, int]
     candidates:
      com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
      com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, int):void */
    public ActorImageExtra(int imgID, int x, int y, int biglayer, int anthor) {
        init(imgID, x, y, false, anthor, biglayer);
    }

    public void init(int imgID, int x, int y, boolean isMirror, int anthor, int lay) {
        this.imgIndex = imgID;
        this.img = Tools.getImage(imgID);
        setXY((float) x, (float) y, anthor);
        if (isMirror) {
            setFlipX(true);
        }
        GameStage.addActor(this, lay);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
     arg types: [int, int, int, int, int, com.sg.util.GameLayer]
     candidates:
      com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, int):void
      com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, com.sg.util.GameLayer):void */
    public ActorImageExtra(int imgID, int x, int y, GameLayer layer) {
        init(imgID, x, y, false, 12, layer);
    }

    public void init(int imgID, int x, int y, boolean isMirror, int anthor, GameLayer layer) {
        this.imgIndex = imgID;
        this.img = Tools.getImage(imgID);
        setXY((float) x, (float) y, anthor);
        if (isMirror) {
            setFlipX(true);
        }
        GameStage.addActor(this, layer);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
     arg types: [int, int, int, int, int, com.badlogic.gdx.scenes.scene2d.Group]
     candidates:
      com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, int):void
      com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
      com.kbz.ActorsExtra.ActorImageExtra.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void */
    public ActorImageExtra(int imgID, int x, int y, Group group) {
        init(imgID, x, y, false, 12, group);
    }

    public ActorImageExtra(int imgID, int x, int y, boolean isMirror, Group group) {
        init(imgID, x, y, isMirror, 12, group);
    }

    public void init(int imgID, int x, int y, boolean isMirror, int anthor, Group group) {
        this.img = Tools.getImage(imgID);
        setXY((float) x, (float) y, anthor);
        if (isMirror) {
            setFlipX(true);
        }
        group.addActor(this);
    }

    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        batch.draw(this.img, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        setColor(color);
    }

    public void setFlipX(boolean flipx) {
        this.isFlipX = flipx;
    }

    public void setFlipY(boolean flipy) {
        this.isFlipY = flipy;
    }

    private void setXY(float x2, float y2, int anchor2) {
        setSize((float) this.img.getRegionWidth(), (float) this.img.getRegionHeight());
        this.anchor = anchor2;
        setPosition(x2, y2, anchor2);
        setOrigin(getWidth() / 2.0f, getHeight() / 2.0f);
    }

    public void clean() {
        GameStage.removeActor(this);
    }
}
