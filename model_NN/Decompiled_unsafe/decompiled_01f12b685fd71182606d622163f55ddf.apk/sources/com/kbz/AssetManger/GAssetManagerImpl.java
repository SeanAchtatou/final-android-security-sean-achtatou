package com.kbz.AssetManger;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.BitmapFontLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.kbz.ParticleOld.ParticleEffect;
import com.kbz.ParticleOld.ParticleEffectLoader;
import com.kbz.ParticleOld.ParticleEmitter;
import com.kbz.tools.Tools;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import java.util.Iterator;

public class GAssetManagerImpl implements GameConstant {
    private static boolean isFinished;
    public static Array<String> tempResLog = new Array<>();
    private AssetManager assetManager;

    public interface GDataAssetLoad {
        Object load(String str, FileHandle fileHandle);
    }

    public GAssetManagerImpl(FileHandleResolver fileHandleResolver) {
        this.assetManager = new AssetManager(fileHandleResolver);
        this.assetManager.setLoader(Object.class, new GDataLoader(fileHandleResolver));
        this.assetManager.setLoader(ParticleEffect.class, new ParticleEffectLoader(fileHandleResolver));
    }

    public AssetManager getAssetManager() {
        return this.assetManager;
    }

    public int getReferenceCount(String fullName) {
        return this.assetManager.getReferenceCount(fullName);
    }

    public <T> int getReferenceCount(Object obj) {
        return this.assetManager.getReferenceCount(getAssetKey(obj));
    }

    public void paintAssetReferenceList() {
        Array<String> assetNames = this.assetManager.getAssetNames();
    }

    private static class CustomDataParameter extends AssetLoaderParameters<Object> {
        GDataAssetLoad dataLoad;

        private CustomDataParameter() {
        }
    }

    public static class GDataLoader extends AsynchronousAssetLoader<Object, CustomDataParameter> {
        private Object dat;

        public GDataLoader(FileHandleResolver resolver) {
            super(resolver);
        }

        public void loadAsync(AssetManager manager, String fileName, FileHandle file, CustomDataParameter parameter) {
            this.dat = parameter.dataLoad.load(fileName, file);
        }

        public Object loadSync(AssetManager manager, String fileName, FileHandle file, CustomDataParameter parameter) {
            return this.dat;
        }

        public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, CustomDataParameter parameter) {
            return null;
        }
    }

    public String loadCustomData(String fullName, GDataAssetLoad dataLoad) {
        CustomDataParameter param = new CustomDataParameter();
        param.dataLoad = dataLoad;
        load(fullName, Object.class, param);
        return fullName;
    }

    public Object getCustomData(String fullName, GDataAssetLoad dataLoad) {
        Object dat = getRes(fullName, Object.class);
        if (dat == null) {
            if (dataLoad == null) {
                System.out.println("GDataAssetLoad Is Null : " + fullName);
                return null;
            }
            loadCustomData(fullName, dataLoad);
            finishLoading();
            dat = getRes(fullName, Object.class);
            if (dat == null) {
                System.out.println("can't load getGameData) : " + fullName);
                return null;
            }
            addToLog(fullName + "---------BitmapFont.class");
        }
        return dat;
    }

    private void load(String fullName, Class type, AssetLoaderParameters param) {
        this.assetManager.load(fullName, type, param);
    }

    public String loadParticleEffectAsTextureAtlas(String particleName, String atlasName) {
        String path = GRes.getParticlePath(particleName);
        ParticleEffectLoader.ParticleEffectParameter param = new ParticleEffectLoader.ParticleEffectParameter();
        param.atlasFile = GRes.getTextureAtlasPath(atlasName);
        load(path, ParticleEffect.class, param);
        return path;
    }

    public String loadParticleEffectAsImageDir(String particleName, FileHandle imagesDir) {
        String path = GRes.getParticlePath(particleName);
        ParticleEffectLoader.ParticleEffectParameter param = new ParticleEffectLoader.ParticleEffectParameter();
        param.imagesDir = imagesDir;
        load(path, ParticleEffect.class, param);
        return path;
    }

    public String loadParticleEffectAsTextureAtlas(String particleName) {
        String path = GRes.getParticlePath(particleName);
        ParticleEffectLoader.ParticleEffectParameter param = new ParticleEffectLoader.ParticleEffectParameter();
        param.atlasFile = path.substring(0, path.lastIndexOf(".")) + ".pack";
        load(path, ParticleEffect.class, param);
        return path;
    }

    public void loadParticleEffect(String path) {
        load(path, ParticleEffect.class, null);
    }

    public void loadParticleEffect(int particleIndex) {
        load(PAK_ASSETS.DATAPARTICAL_PATH + DATAPARTICAL_NAME[particleIndex], ParticleEffect.class, null);
    }

    public void loadParticleEffect2(String particleName) {
        load(PAK_ASSETS.DATAPARTICAL_PATH + particleName, ParticleEffect.class, null);
    }

    public String loadPixmap(String textureName) {
        String fullName = GRes.getTexturePath(textureName);
        load(fullName, Pixmap.class, null);
        return fullName;
    }

    public String loadTexture(String fullName) {
        TextureLoader.TextureParameter param = new TextureLoader.TextureParameter();
        param.minFilter = GRes.minFilter;
        param.magFilter = GRes.magFilter;
        load(fullName, Texture.class, param);
        return fullName;
    }

    public String loadParticleTexture(String textureName) {
        String fullName = GRes.getParticlePath(textureName);
        TextureLoader.TextureParameter param = new TextureLoader.TextureParameter();
        param.minFilter = GRes.minFilter;
        param.magFilter = GRes.magFilter;
        load(fullName, Texture.class, param);
        return fullName;
    }

    public String loadTextureAtlas(String fullName) {
        TextureAtlasLoader.TextureAtlasParameter param = new TextureAtlasLoader.TextureAtlasParameter();
        param.flip = true;
        load(fullName, TextureAtlas.class, param);
        return fullName;
    }

    public String loadTextureAtlas(String fullName, boolean flip) {
        load(fullName, TextureAtlas.class, new TextureAtlasLoader.TextureAtlasParameter(flip));
        return fullName;
    }

    public String loadBitmapFont(String fontName) {
        String fullName = GRes.getFontPath(fontName);
        BitmapFontLoader.BitmapFontParameter param = new BitmapFontLoader.BitmapFontParameter();
        param.flip = true;
        param.minFilter = GRes.minFilter;
        param.magFilter = GRes.magFilter;
        load(fullName, BitmapFont.class, param);
        return fullName;
    }

    public String loadSound(String fullName) {
        load(fullName, Sound.class, null);
        return fullName;
    }

    public boolean isLoaded(String resName) {
        return this.assetManager.isLoaded(resName);
    }

    public String loadMusic(String fullName) {
        load(fullName, Music.class, null);
        return fullName;
    }

    public void unload(String fullName) {
        this.assetManager.unload(fullName);
    }

    public void unloadParticleEffect(String particleName) {
        unload(GRes.getParticlePath(particleName));
    }

    public void unloadTextureAtlas(String packName) {
        unload(GRes.getTextureAtlasPath(packName));
    }

    public void unloadPixmap(String textureName) {
        unload(GRes.getTexturePath(textureName));
    }

    public void unloadTexture(String textureName) {
        unload(GRes.getTexturePath(textureName));
    }

    public void unloadFont(String fontName) {
        unload(GRes.getFontPath(fontName));
    }

    public void unloadSound(String soundName) {
        unload(GRes.getSoundPath(soundName));
    }

    public <T> void unload(Object obj) {
        String fullName = getAssetKey(obj);
        if (fullName != null) {
            unload(fullName);
        }
    }

    public void clear() {
        this.assetManager.clear();
        isFinished = false;
    }

    public void finishLoading() {
        this.assetManager.finishLoading();
        isFinished = true;
    }

    public void update() {
        isFinished = this.assetManager.update();
        this.assetManager.getProgress();
    }

    public static boolean isFinished() {
        return isFinished;
    }

    public <T> String getAssetKey(T resource) {
        return this.assetManager.getAssetFileName(resource);
    }

    public Array<String> getAssetNameList() {
        return this.assetManager.getAssetNames();
    }

    public <T> T getRes(String fullName, Class<T> type) {
        if (!this.assetManager.isLoaded(fullName, type)) {
            return null;
        }
        return this.assetManager.get(fullName, type);
    }

    public <T> T getRes(String fullName) {
        return this.assetManager.get(fullName);
    }

    public static Array<String> getTempResLoadLog() {
        return tempResLog;
    }

    public static void clearTempResLog() {
        tempResLog.clear();
    }

    public static void addToLog(String log) {
        tempResLog.add(log);
    }

    public TextureRegion getTextureRegion(int imgIndex) {
        String fullName = Tools.getImagePath(imgIndex);
        Texture texture = (Texture) getRes(fullName, Texture.class);
        if (texture == null) {
            loadTexture(fullName);
            finishLoading();
            texture = (Texture) getRes(fullName, Texture.class);
            if (texture == null) {
                System.out.println("can't load (getTexture) : " + fullName);
                return null;
            }
            addToLog(fullName + "---------Texture.class");
        }
        TextureRegion region = new TextureRegion(texture);
        region.flip(false, true);
        return region;
    }

    public TextureRegion getTextureRegion(String fullName) {
        Texture texture = (Texture) getRes(fullName, Texture.class);
        if (texture == null) {
            loadTexture(fullName);
            finishLoading();
            texture = (Texture) getRes(fullName, Texture.class);
            if (texture == null) {
                System.out.println("can't load (getTexture) : " + fullName);
                return null;
            }
            addToLog(fullName + "---------Texture.class");
        }
        TextureRegion region = new TextureRegion(texture);
        region.flip(false, true);
        return region;
    }

    public Pixmap getPixmap(String textureName) {
        String fullName = GRes.getTexturePath(textureName);
        Pixmap pixmap = (Pixmap) getRes(fullName, Pixmap.class);
        if (pixmap == null) {
            loadPixmap(textureName);
            finishLoading();
            pixmap = (Pixmap) getRes(fullName, Pixmap.class);
            if (pixmap == null) {
                System.out.println("can't load (getPixmap) : " + fullName);
                return null;
            }
            addToLog(fullName + "---------Pixmap.class");
        }
        return pixmap;
    }

    public TextureAtlas getTextureAtlas(String fullName) {
        TextureAtlas atlas = (TextureAtlas) getRes(fullName, TextureAtlas.class);
        if (atlas != null) {
            return atlas;
        }
        loadTextureAtlas(fullName);
        finishLoading();
        TextureAtlas atlas2 = (TextureAtlas) getRes(fullName, TextureAtlas.class);
        GRes.initTextureAtlas(atlas2);
        addToLog(fullName + "---------TextureAtlas.class");
        return atlas2;
    }

    public BitmapFont getBitmapFont(String fontName) {
        String fullName = GRes.getFontPath(fontName);
        BitmapFont font = (BitmapFont) getRes(fullName, BitmapFont.class);
        if (font == null) {
            loadBitmapFont(fontName);
            finishLoading();
            font = (BitmapFont) getRes(fullName, BitmapFont.class);
            if (font == null) {
                System.out.println("can't load (getBitmapFont) : " + fullName);
                return null;
            }
            addToLog(fullName + "---------BitmapFont.class");
        }
        return font;
    }

    public TextureAtlas.AtlasRegion getAtlasRegion_kbz(String fullName, String regionName) {
        TextureAtlas atlas = getTextureAtlas(fullName);
        if (atlas != null) {
            return atlas.findRegion(regionName);
        }
        System.out.println("can't load (getTextureRegionFromAtlas) : " + fullName);
        return null;
    }

    public Sound getSound(int soundName) {
        String fullName = PAK_ASSETS.SOUND_PATH + SOUND_NAME[soundName];
        Sound sound = (Sound) getRes(fullName, Sound.class);
        if (sound == null) {
            loadSound(fullName);
            finishLoading();
            sound = (Sound) getRes(fullName, Sound.class);
            if (sound == null) {
                System.out.println("can't load (getSound) : " + fullName);
                return null;
            }
            addToLog(fullName + "---------Sound.class");
        }
        return sound;
    }

    public Music getMusic(int musicNameIndex) {
        String fullName = PAK_ASSETS.MUSIC_PATH + MUSIC_NAME[musicNameIndex];
        Music music = (Music) getRes(fullName, Music.class);
        if (music == null) {
            loadMusic(fullName);
            finishLoading();
            music = (Music) getRes(fullName, Music.class);
            if (music == null) {
                System.out.println("can't load (getMusic) : " + fullName);
                return null;
            }
            addToLog(fullName + "---------Music.class");
        }
        return music;
    }

    public ParticleEffect getParticleEffect(int particleIndex) {
        String fullName = PAK_ASSETS.DATAPARTICAL_PATH + DATAPARTICAL_NAME[particleIndex];
        ParticleEffect effect = (ParticleEffect) getRes(fullName, ParticleEffect.class);
        if (effect == null) {
            loadParticleEffect(fullName);
            finishLoading();
            effect = (ParticleEffect) getRes(fullName, ParticleEffect.class);
            if (effect == null) {
                return null;
            }
            addToLog(fullName + "---------ParticleEffect.class");
        }
        return effect;
    }

    public ParticleEffect getParticleEffect(String particleName) {
        String fullName = PAK_ASSETS.DATAPARTICAL_PATH + particleName;
        ParticleEffect effect = (ParticleEffect) getRes(fullName, ParticleEffect.class);
        if (effect == null) {
            loadParticleEffect(fullName);
            finishLoading();
            effect = (ParticleEffect) getRes(fullName, ParticleEffect.class);
            if (effect == null) {
                return null;
            }
            addToLog(fullName + "---------ParticleEffect.class");
        }
        return effect;
    }

    public static void initParticle(ParticleEffect effect) {
        if (effect == null) {
            System.err.println("GAssetManagerImpl initParticle null");
        }
        Iterator<ParticleEmitter> it = effect.getEmitters().iterator();
        while (it.hasNext()) {
            Sprite sprite = it.next().getSprite();
            GRes.setTextureFilter(sprite.getTexture());
            if (!sprite.isFlipY()) {
                sprite.flip(false, true);
            }
        }
    }

    public float getProgress() {
        return this.assetManager.getProgress() * 100.0f;
    }

    public <T> boolean containsAsset(T asset) {
        return this.assetManager.containsAsset(asset);
    }

    public void dispose() {
        this.assetManager.dispose();
    }
}
