package com.kbz.AssetManger;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.kbz.AssetManger.GAssetManagerImpl;
import com.kbz.ParticleOld.ParticleEffect;
import com.kbz.tools.Tools;
import com.sg.pak.PAK_ASSETS;

public class GAssetsManager {
    public static GAssetManagerImpl assetManager = new GAssetManagerImpl(new InternalFileHandleResolver());

    public static AssetManager getAssetManager() {
        return assetManager.getAssetManager();
    }

    public static int getReferenceCount(String fullName) {
        return assetManager.getReferenceCount(fullName);
    }

    public static <T> int getReferenceCount(T resource) {
        return assetManager.getReferenceCount(getAssetKey(resource));
    }

    public static void paintAssetReferenceList() {
        assetManager.paintAssetReferenceList();
    }

    public static String loadCustomData(String fullName, GAssetManagerImpl.GDataAssetLoad dataLoad) {
        return assetManager.loadCustomData(fullName, dataLoad);
    }

    public static Object getCustomData(String fullName, GAssetManagerImpl.GDataAssetLoad dataLoad) {
        return assetManager.getCustomData(fullName, dataLoad);
    }

    public static String loadParticleEffectAsTextureAtlas(String particleName, String atlasName) {
        return assetManager.loadParticleEffectAsTextureAtlas(particleName, atlasName);
    }

    public static String loadParticleEffectAsImageDir(String particleName, FileHandle imagesDir) {
        return assetManager.loadParticleEffectAsImageDir(particleName, imagesDir);
    }

    public static String loadParticleEffectAsTextureAtlas(String particleName) {
        return assetManager.loadParticleEffectAsTextureAtlas(particleName);
    }

    public static void loadParticleEffect(int particleName) {
        assetManager.loadParticleEffect(particleName);
    }

    public static void loadParticleEffect(String particleName) {
        assetManager.loadParticleEffect2(particleName);
    }

    public static String loadPixmap(String textureName) {
        return assetManager.loadPixmap(textureName);
    }

    public static String loadTexture(String textureName) {
        return assetManager.loadTexture(textureName);
    }

    public static String loadParticleTexture(String textureName) {
        return assetManager.loadParticleTexture(textureName);
    }

    public static String loadTextureAtlas(String packName) {
        return assetManager.loadTextureAtlas(packName);
    }

    public static String loadTextureAtlas(String packName, boolean flip) {
        return assetManager.loadTextureAtlas(packName, flip);
    }

    public static String loadBitmapFont(String fontName) {
        return assetManager.loadBitmapFont(fontName);
    }

    public static String loadSound(String soundName) {
        return assetManager.loadSound(soundName);
    }

    public static boolean isLoaded(String resName) {
        return assetManager.isLoaded(resName);
    }

    public static String loadMusic(String musicName) {
        return assetManager.loadMusic(musicName);
    }

    public static void unload(String fullName) {
        assetManager.unload(fullName);
    }

    public static void unloadPackImage(int id) {
        if (Tools.isDebugMode) {
            for (int i = Integer.parseInt(PAK_ASSETS.packNameStr[id][0]); i < Integer.parseInt(PAK_ASSETS.packNameStr[id][1]); i++) {
                String fullName = "imageAll/" + PAK_ASSETS.imageNameStr[i];
                Texture texture = (Texture) getRes(fullName, Texture.class);
                if (texture != null) {
                    System.err.println("unloadPackImage :" + fullName);
                    assetManager.unload(texture);
                }
            }
            return;
        }
        String fullName2 = Tools.packPath + PAK_ASSETS.packNameStr[id][2] + ".png";
        Texture texture2 = (Texture) getRes(fullName2, Texture.class);
        if (texture2 != null) {
            System.err.println("unloadPackImage :" + fullName2);
            assetManager.unload(texture2);
        }
    }

    public static void unloadParticleEffect(int particleName) {
        String fullName = PAK_ASSETS.DATAPARTICAL_PATH + PAK_ASSETS.DATAPARTICAL_NAME[particleName];
        ParticleEffect particleEffect = (ParticleEffect) getRes(fullName);
        if (particleEffect != null) {
            System.err.println("unload particleEffect :" + fullName);
            assetManager.unload(particleEffect);
        }
    }

    public static void unloadTextureAtlas(String packName) {
        unload(GRes.getTextureAtlasPath(packName));
    }

    public static void unloadPixmap(String textureName) {
        unload(GRes.getTexturePath(textureName));
    }

    public static void unloadTexture(String textureName) {
        unload(GRes.getTexturePath(textureName));
    }

    public static void unloadFont(String fontName) {
        unload(GRes.getFontPath(fontName));
    }

    public static void unloadSound(String soundName) {
        unload(GRes.getSoundPath(soundName));
    }

    public static <T> void unload(Object obj) {
        String fullName = getAssetKey(obj);
        if (fullName != null) {
            unload(fullName);
        }
    }

    public static void clear() {
        assetManager.clear();
    }

    public static void finishLoading() {
        assetManager.finishLoading();
    }

    public static void update() {
        assetManager.update();
    }

    public static boolean isFinished() {
        GAssetManagerImpl gAssetManagerImpl = assetManager;
        return GAssetManagerImpl.isFinished();
    }

    public static <T> String getAssetKey(T resource) {
        return assetManager.getAssetKey(resource);
    }

    public static Array<String> getAssetNameList() {
        return assetManager.getAssetNameList();
    }

    public static <T> T getRes(String fullName, Class<T> type) {
        return assetManager.getRes(fullName, type);
    }

    public static <T> T getRes(String fullName) {
        return assetManager.getRes(fullName);
    }

    public static Array<String> getTempResLoadLog() {
        GAssetManagerImpl gAssetManagerImpl = assetManager;
        return GAssetManagerImpl.getTempResLoadLog();
    }

    public static void clearTempResLog() {
        GAssetManagerImpl gAssetManagerImpl = assetManager;
        GAssetManagerImpl.clearTempResLog();
    }

    public static void addToLog(String log) {
        GAssetManagerImpl gAssetManagerImpl = assetManager;
        GAssetManagerImpl.addToLog(log);
    }

    public static Pixmap getPixmap(String textureName) {
        return assetManager.getPixmap(textureName);
    }

    public static TextureAtlas getTextureAtlas(String packName) {
        return assetManager.getTextureAtlas(packName);
    }

    public static BitmapFont getBitmapFont(String fontName) {
        return assetManager.getBitmapFont(fontName);
    }

    public static ParticleEffect getParticleEffect(int particleName) {
        return assetManager.getParticleEffect(particleName);
    }

    public static ParticleEffect getParticleEffect(String particleName) {
        return assetManager.getParticleEffect(particleName);
    }

    public static void initParticle(ParticleEffect effect) {
        GAssetManagerImpl gAssetManagerImpl = assetManager;
        GAssetManagerImpl.initParticle(effect);
    }

    public static float getProgress() {
        return assetManager.getProgress();
    }

    public static <T> boolean containsAsset(T asset) {
        return assetManager.containsAsset(asset);
    }

    public static void dispose() {
        assetManager.dispose();
    }

    public static Sound getSound(int soundName) {
        return assetManager.getSound(soundName);
    }

    public static Music getMusic(int musicName) {
        return assetManager.getMusic(musicName);
    }

    public static TextureRegion getTextureRegion(int textureName) {
        return assetManager.getTextureRegion(textureName);
    }

    public static TextureRegion getTextureRegion(String textureName) {
        return assetManager.getTextureRegion("imageAll/" + textureName);
    }

    public static TextureAtlas.AtlasRegion getAtlasRegion2(int textureName) {
        return assetManager.getAtlasRegion_kbz(Tools.getPackPath() + Tools.getPackName(textureName) + ".atlas", PAK_ASSETS.imageNameStr[textureName].substring(0, PAK_ASSETS.imageNameStr[textureName].length() - 4));
    }
}
