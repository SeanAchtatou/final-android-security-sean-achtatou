package com.kbz.AssetManger;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ObjectSet;
import com.badlogic.gdx.utils.StreamUtils;
import com.kbz.tools.Tools;
import com.sg.pak.GameConstant;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FilenameFilter;

public abstract class GRes implements GameConstant {
    private static BitmapFont defaultFont = null;
    public static Texture.TextureFilter magFilter = Texture.TextureFilter.Linear;
    public static Texture.TextureFilter minFilter = Texture.TextureFilter.Linear;

    public static void setTextureFilter(Texture texture) {
        texture.setFilter(minFilter, magFilter);
    }

    public static String getTexturePath(String fileName) {
        return fileName;
    }

    public static String getShaderPath(String shaderName) {
        return shaderName;
    }

    public static String getTextureAtlasPath(String packName) {
        return packName;
    }

    public static String getFontPath(String fontName) {
        return fontName;
    }

    public static String getSoundPath(String soundName) {
        return soundName;
    }

    public static String getMusicPath(String musicName) {
        return musicName;
    }

    public static String getParticlePath(String particleName) {
        return particleName;
    }

    public static String getAnimationPath(String animationeName) {
        return animationeName;
    }

    public static String getActionPath(String actionScriptName) {
        return actionScriptName;
    }

    public static String getDataPath(String dataName) {
        return dataName;
    }

    public static String getScriptPath(String fileName) {
        return fileName;
    }

    public static String getStringPath(String fileName) {
        return fileName;
    }

    public static String getTaskPath(String fileName) {
        return fileName;
    }

    public static FileHandle openFileHandle(String path) {
        return Gdx.files.internal(path);
    }

    public static DataInputStream openInputStream(String path) {
        return new DataInputStream(openFileHandle(path).read());
    }

    public static Pixmap loadPixmap(int textureName) {
        return new Pixmap(openFileHandle(Tools.getImagePath(textureName)));
    }

    public static TextureRegion[] loadTextureRegions(String path, String[] filter) {
        File file = new File(path);
        FilenameFilter filenameFilter = null;
        if (filter != null) {
            filenameFilter = new FilenameFilter() {
                public boolean accept(File file, String s) {
                    return false;
                }
            };
        }
        System.out.println(file.list(filenameFilter));
        return null;
    }

    public static Texture loadTexture(int textureName) {
        Texture texture = new Texture(openFileHandle(Tools.getImagePath(textureName)));
        setTextureFilter(texture);
        return texture;
    }

    public static TextureRegion loadTextureRegion(int textureName) {
        TextureRegion textureRegion = new TextureRegion(loadTexture(textureName));
        textureRegion.flip(false, true);
        setTextureFilter(textureRegion.getTexture());
        return textureRegion;
    }

    public static TextureRegion loadTextureRegion(int textureName, int x, int y, int w, int h) {
        TextureRegion textureRegion = new TextureRegion(loadTexture(textureName), x, y, w, h);
        textureRegion.flip(false, true);
        setTextureFilter(textureRegion.getTexture());
        return textureRegion;
    }

    public static TextureRegion[][] textureSplit(Texture texture, int tileWidth, int tileHeight) {
        setTextureFilter(texture);
        TextureRegion[][] regions = TextureRegion.split(texture, tileWidth, tileHeight);
        for (int i = 0; i < regions.length; i++) {
            for (TextureRegion flip : regions[i]) {
                flip.flip(false, true);
            }
        }
        return regions;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.TextureAtlas.<init>(com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.TextureAtlas.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle):void
      com.badlogic.gdx.graphics.g2d.TextureAtlas.<init>(com.badlogic.gdx.graphics.g2d.TextureAtlas$TextureAtlasData, java.lang.Object):void
      com.badlogic.gdx.graphics.g2d.TextureAtlas.<init>(com.badlogic.gdx.files.FileHandle, boolean):void */
    public static TextureAtlas loadTextureAtlas(int packName) {
        TextureAtlas atlas = new TextureAtlas(openFileHandle(Tools.getImagePath(packName)), true);
        initTextureAtlas(atlas);
        return atlas;
    }

    public static BitmapFont loadDefaultFont() {
        if (defaultFont == null) {
            defaultFont = new BitmapFont(true);
            setTextureFilter(defaultFont.getRegion().getTexture());
        }
        return defaultFont;
    }

    public static void initTextureAtlas(TextureAtlas atlas) {
        ObjectSet.ObjectSetIterator<Texture> it = atlas.getTextures().iterator();
        while (it.hasNext()) {
            setTextureFilter((Texture) it.next());
        }
    }

    public static TextureRegion createRegionFromTextureRegion(TextureRegion region, int x, int y, int w, int h) {
        TextureRegion newRegion = new TextureRegion(region.getTexture(), region.getRegionX() + x, (region.getRegionY() - region.getRegionHeight()) + y, w, h);
        newRegion.flip(false, true);
        return newRegion;
    }

    public static TextureAtlas.AtlasRegion createRegionFromAtlasRegion(TextureAtlas.AtlasRegion region, int x, int y, int w, int h) {
        int oh;
        int width;
        int height;
        if (region.rotate) {
            oh = region.getRegionWidth();
        } else {
            oh = region.getRegionHeight();
        }
        int ox = region.getRegionX();
        int oy = region.getRegionY() - oh;
        if (region.rotate) {
            width = h;
        } else {
            width = w;
        }
        if (region.rotate) {
            height = w;
        } else {
            height = h;
        }
        TextureAtlas.AtlasRegion newRegion = new TextureAtlas.AtlasRegion(region.getTexture(), ox + x, oy + y, width, height);
        newRegion.rotate = region.rotate;
        newRegion.flip(false, true);
        return newRegion;
    }

    public static String readTextFile(String fullName) {
        return readTextFile(openFileHandle(fullName));
    }

    public static String readTextFile(FileHandle fileHandle) {
        StringBuffer buff = new StringBuffer();
        BufferedReader br = null;
        try {
            BufferedReader br2 = new BufferedReader(fileHandle.reader("UTF-8"));
            while (true) {
                try {
                    String str = br2.readLine();
                    if (str != null) {
                        buff.append(str);
                    } else {
                        StreamUtils.closeQuietly(br2);
                        return buff.toString();
                    }
                } catch (Exception e) {
                    e = e;
                    br = br2;
                    try {
                        System.out.println("readTextFile Error : " + fileHandle);
                        e.printStackTrace();
                        StreamUtils.closeQuietly(br);
                        return null;
                    } catch (Throwable th) {
                        th = th;
                        StreamUtils.closeQuietly(br);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    br = br2;
                    StreamUtils.closeQuietly(br);
                    throw th;
                }
            }
        } catch (Exception e2) {
            e = e2;
            System.out.println("readTextFile Error : " + fileHandle);
            e.printStackTrace();
            StreamUtils.closeQuietly(br);
            return null;
        }
    }
}
