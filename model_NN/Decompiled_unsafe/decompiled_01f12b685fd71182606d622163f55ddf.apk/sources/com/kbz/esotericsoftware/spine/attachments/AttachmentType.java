package com.kbz.esotericsoftware.spine.attachments;

public enum AttachmentType {
    region,
    boundingbox,
    mesh,
    skinnedmesh;
    
    public static AttachmentType[] values = values();
}
