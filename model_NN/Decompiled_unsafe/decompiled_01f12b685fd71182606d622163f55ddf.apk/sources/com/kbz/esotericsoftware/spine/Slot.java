package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.FloatArray;
import com.kbz.esotericsoftware.spine.attachments.Attachment;

public class Slot {
    Attachment attachment;
    private float attachmentTime;
    private FloatArray attachmentVertices = new FloatArray();
    final Bone bone;
    final Color color;
    final SlotData data;

    Slot(SlotData data2) {
        this.data = data2;
        this.bone = null;
        this.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    }

    public Slot(SlotData data2, Bone bone2) {
        if (data2 == null) {
            throw new IllegalArgumentException("data cannot be null.");
        } else if (bone2 == null) {
            throw new IllegalArgumentException("bone cannot be null.");
        } else {
            this.data = data2;
            this.bone = bone2;
            this.color = new Color();
            setToSetupPose();
        }
    }

    public Slot(Slot slot, Bone bone2) {
        if (slot == null) {
            throw new IllegalArgumentException("slot cannot be null.");
        } else if (bone2 == null) {
            throw new IllegalArgumentException("bone cannot be null.");
        } else {
            this.data = slot.data;
            this.bone = bone2;
            this.color = new Color(slot.color);
            this.attachment = slot.attachment;
            this.attachmentTime = slot.attachmentTime;
        }
    }

    public SlotData getData() {
        return this.data;
    }

    public Bone getBone() {
        return this.bone;
    }

    public Skeleton getSkeleton() {
        return this.bone.skeleton;
    }

    public Color getColor() {
        return this.color;
    }

    public Attachment getAttachment() {
        return this.attachment;
    }

    public void setAttachment(Attachment attachment2) {
        if (this.attachment != attachment2) {
            this.attachment = attachment2;
            this.attachmentTime = this.bone.skeleton.time;
            this.attachmentVertices.clear();
        }
    }

    public void setAttachmentTime(float time) {
        this.attachmentTime = this.bone.skeleton.time - time;
    }

    public float getAttachmentTime() {
        return this.bone.skeleton.time - this.attachmentTime;
    }

    public void setAttachmentVertices(FloatArray attachmentVertices2) {
        this.attachmentVertices = attachmentVertices2;
    }

    public FloatArray getAttachmentVertices() {
        return this.attachmentVertices;
    }

    /* access modifiers changed from: package-private */
    public void setToSetupPose(int slotIndex) {
        this.color.set(this.data.color);
        if (this.data.attachmentName == null) {
            setAttachment(null);
            return;
        }
        this.attachment = null;
        setAttachment(this.bone.skeleton.getAttachment(slotIndex, this.data.attachmentName));
    }

    public void setToSetupPose() {
        setToSetupPose(this.bone.skeleton.data.slots.indexOf(this.data, true));
    }

    public String toString() {
        return this.data.name;
    }
}
