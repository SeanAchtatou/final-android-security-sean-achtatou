package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.utils.Array;
import java.util.Iterator;

public class SkeletonData {
    final Array<Animation> animations = new Array<>();
    final Array<BoneData> bones = new Array<>();
    Skin defaultSkin;
    final Array<EventData> events = new Array<>();
    String hash;
    float height;
    final Array<IkConstraintData> ikConstraints = new Array<>();
    String imagesPath;
    String name;
    final Array<Skin> skins = new Array<>();
    final Array<SlotData> slots = new Array<>();
    final Array<TransformConstraintData> transformConstraints = new Array<>();
    String version;
    float width;

    public Array<BoneData> getBones() {
        return this.bones;
    }

    public BoneData findBone(String boneName) {
        if (boneName == null) {
            throw new IllegalArgumentException("boneName cannot be null.");
        }
        Array<BoneData> bones2 = this.bones;
        int n = bones2.size;
        for (int i = 0; i < n; i++) {
            BoneData bone = bones2.get(i);
            if (bone.name.equals(boneName)) {
                return bone;
            }
        }
        return null;
    }

    public int findBoneIndex(String boneName) {
        if (boneName == null) {
            throw new IllegalArgumentException("boneName cannot be null.");
        }
        Array<BoneData> bones2 = this.bones;
        int n = bones2.size;
        for (int i = 0; i < n; i++) {
            if (bones2.get(i).name.equals(boneName)) {
                return i;
            }
        }
        return -1;
    }

    public Array<SlotData> getSlots() {
        return this.slots;
    }

    public SlotData findSlot(String slotName) {
        if (slotName == null) {
            throw new IllegalArgumentException("slotName cannot be null.");
        }
        Array<SlotData> slots2 = this.slots;
        int n = slots2.size;
        for (int i = 0; i < n; i++) {
            SlotData slot = slots2.get(i);
            if (slot.name.equals(slotName)) {
                return slot;
            }
        }
        return null;
    }

    public int findSlotIndex(String slotName) {
        if (slotName == null) {
            throw new IllegalArgumentException("slotName cannot be null.");
        }
        Array<SlotData> slots2 = this.slots;
        int n = slots2.size;
        for (int i = 0; i < n; i++) {
            if (slots2.get(i).name.equals(slotName)) {
                return i;
            }
        }
        return -1;
    }

    public Skin getDefaultSkin() {
        return this.defaultSkin;
    }

    public void setDefaultSkin(Skin defaultSkin2) {
        this.defaultSkin = defaultSkin2;
    }

    public Skin findSkin(String skinName) {
        if (skinName == null) {
            throw new IllegalArgumentException("skinName cannot be null.");
        }
        Iterator<Skin> it = this.skins.iterator();
        while (it.hasNext()) {
            Skin skin = it.next();
            if (skin.name.equals(skinName)) {
                return skin;
            }
        }
        return null;
    }

    public Array<Skin> getSkins() {
        return this.skins;
    }

    public EventData findEvent(String eventDataName) {
        if (eventDataName == null) {
            throw new IllegalArgumentException("eventDataName cannot be null.");
        }
        Iterator<EventData> it = this.events.iterator();
        while (it.hasNext()) {
            EventData eventData = it.next();
            if (eventData.name.equals(eventDataName)) {
                return eventData;
            }
        }
        return null;
    }

    public Array<EventData> getEvents() {
        return this.events;
    }

    public Array<Animation> getAnimations() {
        return this.animations;
    }

    public Animation findAnimation(String animationName) {
        if (animationName == null) {
            throw new IllegalArgumentException("animationName cannot be null.");
        }
        Array<Animation> animations2 = this.animations;
        int n = animations2.size;
        for (int i = 0; i < n; i++) {
            Animation animation = animations2.get(i);
            if (animation.name.equals(animationName)) {
                return animation;
            }
        }
        return null;
    }

    public Array<IkConstraintData> getIkConstraints() {
        return this.ikConstraints;
    }

    public IkConstraintData findIkConstraint(String constraintName) {
        if (constraintName == null) {
            throw new IllegalArgumentException("constraintName cannot be null.");
        }
        Array<IkConstraintData> ikConstraints2 = this.ikConstraints;
        int n = ikConstraints2.size;
        for (int i = 0; i < n; i++) {
            IkConstraintData constraint = ikConstraints2.get(i);
            if (constraint.name.equals(constraintName)) {
                return constraint;
            }
        }
        return null;
    }

    public Array<TransformConstraintData> getTransformConstraints() {
        return this.transformConstraints;
    }

    public TransformConstraintData findTransformConstraint(String constraintName) {
        if (constraintName == null) {
            throw new IllegalArgumentException("constraintName cannot be null.");
        }
        Array<TransformConstraintData> transformConstraints2 = this.transformConstraints;
        int n = transformConstraints2.size;
        for (int i = 0; i < n; i++) {
            TransformConstraintData constraint = transformConstraints2.get(i);
            if (constraint.name.equals(constraintName)) {
                return constraint;
            }
        }
        return null;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public float getWidth() {
        return this.width;
    }

    public void setWidth(float width2) {
        this.width = width2;
    }

    public float getHeight() {
        return this.height;
    }

    public void setHeight(float height2) {
        this.height = height2;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public String getHash() {
        return this.hash;
    }

    public void setHash(String hash2) {
        this.hash = hash2;
    }

    public String getImagesPath() {
        return this.imagesPath;
    }

    public void setImagesPath(String imagesPath2) {
        this.imagesPath = imagesPath2;
    }

    public String toString() {
        return this.name != null ? this.name : super.toString();
    }
}
