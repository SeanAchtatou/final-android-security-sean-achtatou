package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.SerializationException;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.esotericsoftware.spine.attachments.AtlasAttachmentLoader;
import com.kbz.esotericsoftware.spine.attachments.Attachment;
import com.kbz.esotericsoftware.spine.attachments.AttachmentLoader;
import com.kbz.esotericsoftware.spine.attachments.AttachmentType;
import com.kbz.esotericsoftware.spine.attachments.BoundingBoxAttachment;
import com.kbz.esotericsoftware.spine.attachments.MeshAttachment;
import com.kbz.esotericsoftware.spine.attachments.RegionAttachment;
import com.kbz.esotericsoftware.spine.attachments.SkinnedMeshAttachment;

public class SkeletonJson {
    private final AttachmentLoader attachmentLoader;
    private float scale = 1.0f;

    public SkeletonJson(TextureAtlas atlas) {
        this.attachmentLoader = new AtlasAttachmentLoader(atlas);
    }

    public SkeletonJson(AttachmentLoader attachmentLoader2) {
        this.attachmentLoader = attachmentLoader2;
    }

    public float getScale() {
        return this.scale;
    }

    public void setScale(float scale2) {
        this.scale = scale2;
    }

    public SkeletonData readSkeletonData(FileHandle file) {
        if (file == null) {
            throw new IllegalArgumentException("file cannot be null.");
        }
        float scale2 = this.scale;
        SkeletonData skeletonData = new SkeletonData();
        skeletonData.name = file.nameWithoutExtension();
        JsonValue root = new JsonReader().parse(file);
        JsonValue skeletonMap = root.get("skeleton");
        if (skeletonMap != null) {
            skeletonData.hash = skeletonMap.getString("hash", null);
            skeletonData.version = skeletonMap.getString("spine", null);
            skeletonData.width = skeletonMap.getFloat("width", Animation.CurveTimeline.LINEAR);
            skeletonData.height = skeletonMap.getFloat("height", Animation.CurveTimeline.LINEAR);
            skeletonData.imagesPath = skeletonMap.getString("images", null);
        }
        JsonValue boneMap = root.getChild("bones");
        while (boneMap != null) {
            BoneData parent = null;
            String parentName = boneMap.getString("parent", null);
            if (parentName == null || (parent = skeletonData.findBone(parentName)) != null) {
                BoneData boneData = new BoneData(boneMap.getString("name"), parent);
                boneData.length = boneMap.getFloat("length", Animation.CurveTimeline.LINEAR) * scale2;
                boneData.x = boneMap.getFloat("x", Animation.CurveTimeline.LINEAR) * scale2;
                boneData.y = boneMap.getFloat("y", Animation.CurveTimeline.LINEAR) * scale2;
                boneData.rotation = boneMap.getFloat("rotation", Animation.CurveTimeline.LINEAR);
                boneData.scaleX = boneMap.getFloat("scaleX", 1.0f);
                boneData.scaleY = boneMap.getFloat("scaleY", 1.0f);
                boneData.inheritScale = boneMap.getBoolean("inheritScale", true);
                boneData.inheritRotation = boneMap.getBoolean("inheritRotation", true);
                String color = boneMap.getString("color", null);
                if (color != null) {
                    boneData.getColor().set(Color.valueOf(color));
                }
                skeletonData.bones.add(boneData);
                boneMap = boneMap.next;
            } else {
                throw new SerializationException("Parent bone not found: " + parentName);
            }
        }
        for (JsonValue ikMap = root.getChild("ik"); ikMap != null; ikMap = ikMap.next) {
            IkConstraintData ikConstraintData = new IkConstraintData(ikMap.getString("name"));
            for (JsonValue boneMap2 = ikMap.getChild("bones"); boneMap2 != null; boneMap2 = boneMap2.next) {
                String boneName = boneMap2.asString();
                BoneData bone = skeletonData.findBone(boneName);
                if (bone == null) {
                    throw new SerializationException("IK bone not found: " + boneName);
                }
                ikConstraintData.bones.add(bone);
            }
            String targetName = ikMap.getString("target");
            ikConstraintData.target = skeletonData.findBone(targetName);
            if (ikConstraintData.target == null) {
                throw new SerializationException("Target bone not found: " + targetName);
            }
            ikConstraintData.bendDirection = ikMap.getBoolean("bendPositive", true) ? 1 : -1;
            ikConstraintData.mix = ikMap.getFloat("mix", 1.0f);
            skeletonData.ikConstraints.add(ikConstraintData);
        }
        for (JsonValue slotMap = root.getChild("slots"); slotMap != null; slotMap = slotMap.next) {
            String slotName = slotMap.getString("name");
            String boneName2 = slotMap.getString("bone");
            BoneData boneData2 = skeletonData.findBone(boneName2);
            if (boneData2 == null) {
                throw new SerializationException("Slot bone not found: " + boneName2);
            }
            SlotData slotData = new SlotData(slotName, boneData2);
            String color2 = slotMap.getString("color", null);
            if (color2 != null) {
                slotData.getColor().set(Color.valueOf(color2));
            }
            slotData.attachmentName = slotMap.getString("attachment", null);
            slotData.blendMode = BlendMode.valueOf(slotMap.getString("blend", BlendMode.normal.name()));
            skeletonData.slots.add(slotData);
        }
        for (JsonValue skinMap = root.getChild("skins"); skinMap != null; skinMap = skinMap.next) {
            Skin skin = new Skin(skinMap.name);
            for (JsonValue slotEntry = skinMap.child; slotEntry != null; slotEntry = slotEntry.next) {
                int slotIndex = skeletonData.findSlotIndex(slotEntry.name);
                if (slotIndex == -1) {
                    throw new SerializationException("Slot not found: " + slotEntry.name);
                }
                for (JsonValue entry = slotEntry.child; entry != null; entry = entry.next) {
                    Attachment attachment = readAttachment(skin, entry.name, entry);
                    if (attachment != null) {
                        skin.addAttachment(slotIndex, entry.name, attachment);
                    }
                }
            }
            skeletonData.skins.add(skin);
            if (skin.name.equals("default")) {
                skeletonData.defaultSkin = skin;
            }
        }
        for (JsonValue eventMap = root.getChild(fo.a); eventMap != null; eventMap = eventMap.next) {
            EventData eventData = new EventData(eventMap.name);
            eventData.intValue = eventMap.getInt("int", 0);
            eventData.floatValue = eventMap.getFloat("float", Animation.CurveTimeline.LINEAR);
            eventData.stringValue = eventMap.getString("string", null);
            skeletonData.events.add(eventData);
        }
        for (JsonValue animationMap = root.getChild("animations"); animationMap != null; animationMap = animationMap.next) {
            readAnimation(animationMap.name, animationMap, skeletonData);
        }
        skeletonData.bones.shrink();
        skeletonData.slots.shrink();
        skeletonData.skins.shrink();
        skeletonData.events.shrink();
        skeletonData.animations.shrink();
        skeletonData.ikConstraints.shrink();
        return skeletonData;
    }

    private Attachment readAttachment(Skin skin, String name, JsonValue map) {
        float scale2 = this.scale;
        String name2 = map.getString("name", name);
        String path = map.getString("path", name2);
        switch (AttachmentType.valueOf(map.getString("type", AttachmentType.region.name()))) {
            case region:
                RegionAttachment region = this.attachmentLoader.newRegionAttachment(skin, name2, path);
                if (region == null) {
                    return null;
                }
                region.setPath(path);
                region.setX(map.getFloat("x", Animation.CurveTimeline.LINEAR) * scale2);
                region.setY(map.getFloat("y", Animation.CurveTimeline.LINEAR) * scale2);
                region.setScaleX(map.getFloat("scaleX", 1.0f));
                region.setScaleY(map.getFloat("scaleY", 1.0f));
                region.setRotation(map.getFloat("rotation", Animation.CurveTimeline.LINEAR));
                region.setWidth(map.getFloat("width") * scale2);
                region.setHeight(map.getFloat("height") * scale2);
                String color = map.getString("color", null);
                if (color != null) {
                    region.getColor().set(Color.valueOf(color));
                }
                region.updateOffset();
                return region;
            case boundingbox:
                BoundingBoxAttachment box = this.attachmentLoader.newBoundingBoxAttachment(skin, name2);
                if (box == null) {
                    return null;
                }
                float[] vertices = map.require("vertices").asFloatArray();
                if (scale2 != 1.0f) {
                    int n = vertices.length;
                    for (int i = 0; i < n; i++) {
                        vertices[i] = vertices[i] * scale2;
                    }
                }
                box.setVertices(vertices);
                return box;
            case mesh:
                MeshAttachment mesh = this.attachmentLoader.newMeshAttachment(skin, name2, path);
                if (mesh == null) {
                    return null;
                }
                mesh.setPath(path);
                float[] vertices2 = map.require("vertices").asFloatArray();
                if (scale2 != 1.0f) {
                    int n2 = vertices2.length;
                    for (int i2 = 0; i2 < n2; i2++) {
                        vertices2[i2] = vertices2[i2] * scale2;
                    }
                }
                mesh.setVertices(vertices2);
                mesh.setTriangles(map.require("triangles").asShortArray());
                mesh.setRegionUVs(map.require("uvs").asFloatArray());
                mesh.updateUVs();
                String color2 = map.getString("color", null);
                if (color2 != null) {
                    mesh.getColor().set(Color.valueOf(color2));
                }
                if (map.has("hull")) {
                    mesh.setHullLength(map.require("hull").asInt() * 2);
                }
                if (map.has("edges")) {
                    mesh.setEdges(map.require("edges").asIntArray());
                }
                mesh.setWidth(map.getFloat("width", Animation.CurveTimeline.LINEAR) * scale2);
                mesh.setHeight(map.getFloat("height", Animation.CurveTimeline.LINEAR) * scale2);
                return mesh;
            case skinnedmesh:
                SkinnedMeshAttachment mesh2 = this.attachmentLoader.newSkinnedMeshAttachment(skin, name2, path);
                if (mesh2 == null) {
                    return null;
                }
                mesh2.setPath(path);
                float[] uvs = map.require("uvs").asFloatArray();
                float[] vertices3 = map.require("vertices").asFloatArray();
                FloatArray weights = new FloatArray(uvs.length * 3 * 3);
                IntArray bones = new IntArray(uvs.length * 3);
                int n3 = vertices3.length;
                int i3 = 0;
                while (i3 < n3) {
                    int i4 = i3 + 1;
                    int boneCount = (int) vertices3[i3];
                    bones.add(boneCount);
                    int nn = i4 + (boneCount * 4);
                    while (i4 < nn) {
                        bones.add((int) vertices3[i4]);
                        weights.add(vertices3[i4 + 1] * scale2);
                        weights.add(vertices3[i4 + 2] * scale2);
                        weights.add(vertices3[i4 + 3]);
                        i4 += 4;
                    }
                    i3 = i4;
                }
                mesh2.setBones(bones.toArray());
                mesh2.setWeights(weights.toArray());
                mesh2.setTriangles(map.require("triangles").asShortArray());
                mesh2.setRegionUVs(uvs);
                mesh2.updateUVs();
                String color3 = map.getString("color", null);
                if (color3 != null) {
                    mesh2.getColor().set(Color.valueOf(color3));
                }
                if (map.has("hull")) {
                    mesh2.setHullLength(map.require("hull").asInt() * 2);
                }
                if (map.has("edges")) {
                    mesh2.setEdges(map.require("edges").asIntArray());
                }
                mesh2.setWidth(map.getFloat("width", Animation.CurveTimeline.LINEAR) * scale2);
                mesh2.setHeight(map.getFloat("height", Animation.CurveTimeline.LINEAR) * scale2);
                return mesh2;
            default:
                return null;
        }
    }

    private void readAnimation(String name, JsonValue map, SkeletonData skeletonData) {
        int unchangedIndex;
        int unchangedIndex2;
        int originalIndex;
        int vertexCount;
        float[] vertices;
        Animation.TranslateTimeline timeline;
        float duration;
        float scale2 = this.scale;
        Array<Animation.Timeline> timelines = new Array<>();
        float duration2 = Animation.CurveTimeline.LINEAR;
        for (JsonValue slotMap = map.getChild("slots"); slotMap != null; slotMap = slotMap.next) {
            int slotIndex = skeletonData.findSlotIndex(slotMap.name);
            if (slotIndex == -1) {
                throw new SerializationException("Slot not found: " + slotMap.name);
            }
            for (JsonValue timelineMap = slotMap.child; timelineMap != null; timelineMap = timelineMap.next) {
                String timelineName = timelineMap.name;
                if (timelineName.equals("color")) {
                    Animation.ColorTimeline timeline2 = new Animation.ColorTimeline(timelineMap.size);
                    timeline2.slotIndex = slotIndex;
                    int frameIndex = 0;
                    for (JsonValue valueMap = timelineMap.child; valueMap != null; valueMap = valueMap.next) {
                        Color color = Color.valueOf(valueMap.getString("color"));
                        timeline2.setFrame(frameIndex, valueMap.getFloat("time"), color.r, color.g, color.b, color.a);
                        readCurve(timeline2, frameIndex, valueMap);
                        frameIndex++;
                    }
                    timelines.add(timeline2);
                    duration2 = Math.max(duration2, timeline2.getFrames()[(timeline2.getFrameCount() * 5) - 5]);
                } else if (timelineName.equals("attachment")) {
                    Animation.AttachmentTimeline timeline3 = new Animation.AttachmentTimeline(timelineMap.size);
                    timeline3.slotIndex = slotIndex;
                    JsonValue valueMap2 = timelineMap.child;
                    int frameIndex2 = 0;
                    while (valueMap2 != null) {
                        timeline3.setFrame(frameIndex2, valueMap2.getFloat("time"), valueMap2.getString("name"));
                        valueMap2 = valueMap2.next;
                        frameIndex2++;
                    }
                    timelines.add(timeline3);
                    duration2 = Math.max(duration2, timeline3.getFrames()[timeline3.getFrameCount() - 1]);
                } else {
                    throw new RuntimeException("Invalid timeline type for a slot: " + timelineName + " (" + slotMap.name + ")");
                }
            }
        }
        for (JsonValue boneMap = map.getChild("bones"); boneMap != null; boneMap = boneMap.next) {
            int boneIndex = skeletonData.findBoneIndex(boneMap.name);
            if (boneIndex == -1) {
                throw new SerializationException("Bone not found: " + boneMap.name);
            }
            for (JsonValue timelineMap2 = boneMap.child; timelineMap2 != null; timelineMap2 = timelineMap2.next) {
                String timelineName2 = timelineMap2.name;
                if (timelineName2.equals("rotate")) {
                    Animation.RotateTimeline timeline4 = new Animation.RotateTimeline(timelineMap2.size);
                    timeline4.boneIndex = boneIndex;
                    int frameIndex3 = 0;
                    for (JsonValue valueMap3 = timelineMap2.child; valueMap3 != null; valueMap3 = valueMap3.next) {
                        timeline4.setFrame(frameIndex3, valueMap3.getFloat("time"), valueMap3.getFloat("angle"));
                        readCurve(timeline4, frameIndex3, valueMap3);
                        frameIndex3++;
                    }
                    timelines.add(timeline4);
                    duration = Math.max(duration2, timeline4.getFrames()[(timeline4.getFrameCount() * 2) - 2]);
                } else if (timelineName2.equals("translate") || timelineName2.equals("scale")) {
                    float timelineScale = 1.0f;
                    if (timelineName2.equals("scale")) {
                        timeline = new Animation.ScaleTimeline(timelineMap2.size);
                    } else {
                        timeline = new Animation.TranslateTimeline(timelineMap2.size);
                        timelineScale = scale2;
                    }
                    timeline.boneIndex = boneIndex;
                    int frameIndex4 = 0;
                    for (JsonValue valueMap4 = timelineMap2.child; valueMap4 != null; valueMap4 = valueMap4.next) {
                        timeline.setFrame(frameIndex4, valueMap4.getFloat("time"), valueMap4.getFloat("x", Animation.CurveTimeline.LINEAR) * timelineScale, valueMap4.getFloat("y", Animation.CurveTimeline.LINEAR) * timelineScale);
                        readCurve(timeline, frameIndex4, valueMap4);
                        frameIndex4++;
                    }
                    timelines.add(timeline);
                    duration = Math.max(duration2, timeline.getFrames()[(timeline.getFrameCount() * 3) - 3]);
                } else {
                    throw new RuntimeException("Invalid timeline type for a bone: " + timelineName2 + " (" + boneMap.name + ")");
                }
            }
        }
        for (JsonValue ikMap = map.getChild("ik"); ikMap != null; ikMap = ikMap.next) {
            IkConstraintData ikConstraint = skeletonData.findIkConstraint(ikMap.name);
            Animation.IkConstraintTimeline timeline5 = new Animation.IkConstraintTimeline(ikMap.size);
            timeline5.ikConstraintIndex = skeletonData.getIkConstraints().indexOf(ikConstraint, true);
            int frameIndex5 = 0;
            for (JsonValue valueMap5 = ikMap.child; valueMap5 != null; valueMap5 = valueMap5.next) {
                timeline5.setFrame(frameIndex5, valueMap5.getFloat("time"), valueMap5.getFloat("mix"), valueMap5.getBoolean("bendPositive") ? 1 : -1);
                readCurve(timeline5, frameIndex5, valueMap5);
                frameIndex5++;
            }
            timelines.add(timeline5);
            duration2 = Math.max(duration2, timeline5.getFrames()[(timeline5.getFrameCount() * 3) - 3]);
        }
        for (JsonValue ffdMap = map.getChild("ffd"); ffdMap != null; ffdMap = ffdMap.next) {
            Skin skin = skeletonData.findSkin(ffdMap.name);
            if (skin == null) {
                throw new SerializationException("Skin not found: " + ffdMap.name);
            }
            for (JsonValue slotMap2 = ffdMap.child; slotMap2 != null; slotMap2 = slotMap2.next) {
                int slotIndex2 = skeletonData.findSlotIndex(slotMap2.name);
                if (slotIndex2 == -1) {
                    throw new SerializationException("Slot not found: " + slotMap2.name);
                }
                for (JsonValue meshMap = slotMap2.child; meshMap != null; meshMap = meshMap.next) {
                    Animation.FfdTimeline timeline6 = new Animation.FfdTimeline(meshMap.size);
                    Attachment attachment = skin.getAttachment(slotIndex2, meshMap.name);
                    if (attachment == null) {
                        throw new SerializationException("FFD attachment not found: " + meshMap.name);
                    }
                    timeline6.slotIndex = slotIndex2;
                    timeline6.attachment = attachment;
                    if (attachment instanceof MeshAttachment) {
                        vertexCount = ((MeshAttachment) attachment).getVertices().length;
                    } else {
                        vertexCount = (((SkinnedMeshAttachment) attachment).getWeights().length / 3) * 2;
                    }
                    int frameIndex6 = 0;
                    for (JsonValue valueMap6 = meshMap.child; valueMap6 != null; valueMap6 = valueMap6.next) {
                        JsonValue verticesValue = valueMap6.get("vertices");
                        if (verticesValue == null) {
                            vertices = attachment instanceof MeshAttachment ? ((MeshAttachment) attachment).getVertices() : new float[vertexCount];
                        } else {
                            vertices = new float[vertexCount];
                            int start = valueMap6.getInt("offset", 0);
                            System.arraycopy(verticesValue.asFloatArray(), 0, vertices, start, verticesValue.size);
                            if (scale2 != 1.0f) {
                                int i = start;
                                int n = i + verticesValue.size;
                                while (i < n) {
                                    vertices[i] = vertices[i] * scale2;
                                    i++;
                                }
                            }
                            if (attachment instanceof MeshAttachment) {
                                float[] meshVertices = ((MeshAttachment) attachment).getVertices();
                                for (int i2 = 0; i2 < vertexCount; i2++) {
                                    vertices[i2] = vertices[i2] + meshVertices[i2];
                                }
                            }
                        }
                        timeline6.setFrame(frameIndex6, valueMap6.getFloat("time"), vertices);
                        readCurve(timeline6, frameIndex6, valueMap6);
                        frameIndex6++;
                    }
                    timelines.add(timeline6);
                    duration2 = Math.max(duration2, timeline6.getFrames()[timeline6.getFrameCount() - 1]);
                }
            }
        }
        JsonValue drawOrdersMap = map.get("drawOrder");
        if (drawOrdersMap == null) {
            drawOrdersMap = map.get("draworder");
        }
        if (drawOrdersMap != null) {
            Animation.DrawOrderTimeline timeline7 = new Animation.DrawOrderTimeline(drawOrdersMap.size);
            int slotCount = skeletonData.slots.size;
            JsonValue drawOrderMap = drawOrdersMap.child;
            int frameIndex7 = 0;
            while (drawOrderMap != null) {
                int[] drawOrder = null;
                JsonValue offsets = drawOrderMap.get("offsets");
                if (offsets != null) {
                    drawOrder = new int[slotCount];
                    for (int i3 = slotCount - 1; i3 >= 0; i3--) {
                        drawOrder[i3] = -1;
                    }
                    int[] unchanged = new int[(slotCount - offsets.size)];
                    int originalIndex2 = 0;
                    int unchangedIndex3 = 0;
                    JsonValue offsetMap = offsets.child;
                    while (offsetMap != null) {
                        int slotIndex3 = skeletonData.findSlotIndex(offsetMap.getString("slot"));
                        if (slotIndex3 == -1) {
                            throw new SerializationException("Slot not found: " + offsetMap.getString("slot"));
                        }
                        while (true) {
                            unchangedIndex2 = unchangedIndex3;
                            originalIndex = originalIndex2;
                            if (originalIndex == slotIndex3) {
                                break;
                            }
                            unchangedIndex3 = unchangedIndex2 + 1;
                            originalIndex2 = originalIndex + 1;
                            unchanged[unchangedIndex2] = originalIndex;
                        }
                        originalIndex2 = originalIndex + 1;
                        drawOrder[offsetMap.getInt("offset") + originalIndex] = originalIndex;
                        offsetMap = offsetMap.next;
                        unchangedIndex3 = unchangedIndex2;
                    }
                    while (true) {
                        unchangedIndex = unchangedIndex3;
                        int originalIndex3 = originalIndex2;
                        if (originalIndex3 >= slotCount) {
                            break;
                        }
                        unchangedIndex3 = unchangedIndex + 1;
                        originalIndex2 = originalIndex3 + 1;
                        unchanged[unchangedIndex] = originalIndex3;
                    }
                    int unchangedIndex4 = unchangedIndex;
                    for (int i4 = slotCount - 1; i4 >= 0; i4--) {
                        if (drawOrder[i4] == -1) {
                            unchangedIndex4--;
                            drawOrder[i4] = unchanged[unchangedIndex4];
                        }
                    }
                }
                timeline7.setFrame(frameIndex7, drawOrderMap.getFloat("time"), drawOrder);
                drawOrderMap = drawOrderMap.next;
                frameIndex7++;
            }
            timelines.add(timeline7);
            duration2 = Math.max(duration2, timeline7.getFrames()[timeline7.getFrameCount() - 1]);
        }
        JsonValue eventsMap = map.get(fo.a);
        if (eventsMap != null) {
            Animation.EventTimeline timeline8 = new Animation.EventTimeline(eventsMap.size);
            JsonValue eventMap = eventsMap.child;
            int frameIndex8 = 0;
            while (eventMap != null) {
                EventData eventData = skeletonData.findEvent(eventMap.getString("name"));
                if (eventData == null) {
                    throw new SerializationException("Event not found: " + eventMap.getString("name"));
                }
                Event event = new Event(eventMap.getFloat("time"), eventData);
                event.intValue = eventMap.getInt("int", eventData.getInt());
                event.floatValue = eventMap.getFloat("float", eventData.getFloat());
                event.stringValue = eventMap.getString("string", eventData.getString());
                timeline8.setFrame(frameIndex8, event);
                eventMap = eventMap.next;
                frameIndex8++;
            }
            timelines.add(timeline8);
            duration2 = Math.max(duration2, timeline8.getFrames()[timeline8.getFrameCount() - 1]);
        }
        timelines.shrink();
        skeletonData.animations.add(new Animation(name, timelines, duration2));
    }

    /* access modifiers changed from: package-private */
    public void readCurve(Animation.CurveTimeline timeline, int frameIndex, JsonValue valueMap) {
        JsonValue curve = valueMap.get("curve");
        if (curve != null) {
            if (curve.isString() && curve.asString().equals("stepped")) {
                timeline.setStepped(frameIndex);
            } else if (curve.isArray()) {
                timeline.setCurve(frameIndex, curve.getFloat(0), curve.getFloat(1), curve.getFloat(2), curve.getFloat(3));
            }
        }
    }
}
