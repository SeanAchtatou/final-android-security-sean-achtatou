package com.kbz.esotericsoftware.spine;

public interface Updatable {
    void update();
}
