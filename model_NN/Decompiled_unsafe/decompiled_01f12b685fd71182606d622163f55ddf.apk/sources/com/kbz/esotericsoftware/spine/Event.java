package com.kbz.esotericsoftware.spine;

public class Event {
    private final EventData data;
    float floatValue;
    int intValue;
    String stringValue;
    final float time;

    public Event(float time2, EventData data2) {
        this.time = time2;
        this.data = data2;
    }

    public int getInt() {
        return this.intValue;
    }

    public void setInt(int intValue2) {
        this.intValue = intValue2;
    }

    public float getFloat() {
        return this.floatValue;
    }

    public void setFloat(float floatValue2) {
        this.floatValue = floatValue2;
    }

    public String getString() {
        return this.stringValue;
    }

    public void setString(String stringValue2) {
        this.stringValue = stringValue2;
    }

    public float getTime() {
        return this.time;
    }

    public EventData getData() {
        return this.data;
    }

    public String toString() {
        return this.data.name;
    }
}
