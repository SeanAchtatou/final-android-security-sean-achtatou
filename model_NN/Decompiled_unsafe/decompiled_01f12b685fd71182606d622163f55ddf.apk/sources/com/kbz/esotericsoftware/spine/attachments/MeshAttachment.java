package com.kbz.esotericsoftware.spine.attachments;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.NumberUtils;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.esotericsoftware.spine.Bone;
import com.kbz.esotericsoftware.spine.Skeleton;
import com.kbz.esotericsoftware.spine.Slot;

public class MeshAttachment extends Attachment {
    private final Color color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    private int[] edges;
    private float height;
    private int hullLength;
    private String path;
    private TextureRegion region;
    private float[] regionUVs;
    private short[] triangles;
    private float[] vertices;
    private float width;
    private float[] worldVertices;

    public MeshAttachment(String name) {
        super(name);
    }

    public void setRegion(TextureRegion region2) {
        if (region2 == null) {
            throw new IllegalArgumentException("region cannot be null.");
        }
        this.region = region2;
    }

    public TextureRegion getRegion() {
        if (this.region != null) {
            return this.region;
        }
        throw new IllegalStateException("Region has not been set: " + this);
    }

    public void updateUVs() {
        float u;
        float v;
        float width2;
        float height2;
        int verticesLength = this.vertices.length;
        int worldVerticesLength = (verticesLength / 2) * 5;
        if (this.worldVertices == null || this.worldVertices.length != worldVerticesLength) {
            this.worldVertices = new float[worldVerticesLength];
        }
        if (this.region == null) {
            v = Animation.CurveTimeline.LINEAR;
            u = 0.0f;
            height2 = 1.0f;
            width2 = 1.0f;
        } else {
            u = this.region.getU();
            v = this.region.getV();
            width2 = this.region.getU2() - u;
            height2 = this.region.getV2() - v;
        }
        float[] regionUVs2 = this.regionUVs;
        if (!(this.region instanceof TextureAtlas.AtlasRegion) || !((TextureAtlas.AtlasRegion) this.region).rotate) {
            int i = 0;
            int w = 3;
            while (i < verticesLength) {
                this.worldVertices[w] = (regionUVs2[i] * width2) + u;
                this.worldVertices[w + 1] = (regionUVs2[i + 1] * height2) + v;
                i += 2;
                w += 5;
            }
            return;
        }
        int i2 = 0;
        int w2 = 3;
        while (i2 < verticesLength) {
            this.worldVertices[w2] = (regionUVs2[i2 + 1] * width2) + u;
            this.worldVertices[w2 + 1] = (v + height2) - (regionUVs2[i2] * height2);
            i2 += 2;
            w2 += 5;
        }
    }

    public float[] updateWorldVertices(Slot slot, boolean premultipliedAlpha) {
        Skeleton skeleton = slot.getSkeleton();
        Color skeletonColor = skeleton.getColor();
        Color slotColor = slot.getColor();
        Color meshColor = this.color;
        float a = skeletonColor.a * slotColor.a * meshColor.a * 255.0f;
        float multiplier = premultipliedAlpha ? a : 255.0f;
        float color2 = NumberUtils.intToFloatColor((((int) a) << 24) | (((int) (((skeletonColor.b * slotColor.b) * meshColor.b) * multiplier)) << 16) | (((int) (((skeletonColor.g * slotColor.g) * meshColor.g) * multiplier)) << 8) | ((int) (skeletonColor.r * slotColor.r * meshColor.r * multiplier)));
        float[] worldVertices2 = this.worldVertices;
        FloatArray slotVertices = slot.getAttachmentVertices();
        float[] vertices2 = this.vertices;
        if (slotVertices.size == vertices2.length) {
            vertices2 = slotVertices.items;
        }
        Bone bone = slot.getBone();
        float x = skeleton.getX() + bone.getWorldX();
        float y = skeleton.getY() + bone.getWorldY();
        float m00 = bone.getA();
        float m01 = bone.getB();
        float m10 = bone.getC();
        float m11 = bone.getD();
        int v = 0;
        int n = worldVertices2.length;
        for (int w = 0; w < n; w += 5) {
            float vx = vertices2[v];
            float vy = vertices2[v + 1];
            worldVertices2[w] = (vx * m00) + (vy * m01) + x;
            worldVertices2[w + 1] = (vx * m10) + (vy * m11) + y;
            worldVertices2[w + 2] = color2;
            v += 2;
        }
        return worldVertices2;
    }

    public float[] getWorldVertices() {
        return this.worldVertices;
    }

    public float[] getVertices() {
        return this.vertices;
    }

    public void setVertices(float[] vertices2) {
        this.vertices = vertices2;
    }

    public short[] getTriangles() {
        return this.triangles;
    }

    public void setTriangles(short[] triangles2) {
        this.triangles = triangles2;
    }

    public float[] getRegionUVs() {
        return this.regionUVs;
    }

    public void setRegionUVs(float[] regionUVs2) {
        this.regionUVs = regionUVs2;
    }

    public Color getColor() {
        return this.color;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path2) {
        this.path = path2;
    }

    public int getHullLength() {
        return this.hullLength;
    }

    public void setHullLength(int hullLength2) {
        this.hullLength = hullLength2;
    }

    public int[] getEdges() {
        return this.edges;
    }

    public void setEdges(int[] edges2) {
        this.edges = edges2;
    }

    public float getWidth() {
        return this.width;
    }

    public void setWidth(float width2) {
        this.width = width2;
    }

    public float getHeight() {
        return this.height;
    }

    public void setHeight(float height2) {
        this.height = height2;
    }
}
