package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.esotericsoftware.spine.attachments.Attachment;
import com.kbz.esotericsoftware.spine.attachments.MeshAttachment;
import com.kbz.esotericsoftware.spine.attachments.RegionAttachment;
import com.kbz.esotericsoftware.spine.attachments.SkeletonAttachment;
import com.kbz.esotericsoftware.spine.attachments.SkinnedMeshAttachment;

public class SkeletonMeshRenderer extends SkeletonRenderer<PolygonSpriteBatch> {
    private static final short[] quadTriangles = {0, 1, 2, 2, 3, 0};

    public void draw(PolygonSpriteBatch batch, Skeleton skeleton) {
        boolean premultipliedAlpha = this.premultipliedAlpha;
        BlendMode blendMode = null;
        float[] vertices = null;
        short[] triangles = null;
        Array<Slot> drawOrder = skeleton.drawOrder;
        int n = drawOrder.size;
        for (int i = 0; i < n; i++) {
            Slot slot = drawOrder.get(i);
            Attachment attachment = slot.attachment;
            Texture texture = null;
            if (attachment instanceof RegionAttachment) {
                RegionAttachment region = (RegionAttachment) attachment;
                vertices = region.updateWorldVertices(slot, premultipliedAlpha);
                triangles = quadTriangles;
                texture = region.getRegion().getTexture();
            } else if (attachment instanceof MeshAttachment) {
                MeshAttachment mesh = (MeshAttachment) attachment;
                vertices = mesh.updateWorldVertices(slot, premultipliedAlpha);
                triangles = mesh.getTriangles();
                texture = mesh.getRegion().getTexture();
            } else if (attachment instanceof SkinnedMeshAttachment) {
                SkinnedMeshAttachment mesh2 = (SkinnedMeshAttachment) attachment;
                vertices = mesh2.updateWorldVertices(slot, premultipliedAlpha);
                triangles = mesh2.getTriangles();
                texture = mesh2.getRegion().getTexture();
            } else if (attachment instanceof SkeletonAttachment) {
                Skeleton attachmentSkeleton = ((SkeletonAttachment) attachment).getSkeleton();
                if (attachmentSkeleton != null) {
                    Bone bone = slot.getBone();
                    Bone rootBone = attachmentSkeleton.getRootBone();
                    float oldScaleX = rootBone.getScaleX();
                    float oldScaleY = rootBone.getScaleY();
                    float oldRotation = rootBone.getRotation();
                    attachmentSkeleton.setPosition(skeleton.getX() + bone.getWorldX(), skeleton.getY() + bone.getWorldY());
                    rootBone.setRotation(bone.getWorldRotationX() + oldRotation);
                    attachmentSkeleton.updateWorldTransform();
                    draw(batch, attachmentSkeleton);
                    attachmentSkeleton.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
                    rootBone.setScaleX(oldScaleX);
                    rootBone.setScaleY(oldScaleY);
                    rootBone.setRotation(oldRotation);
                }
            }
            if (texture != null) {
                BlendMode slotBlendMode = slot.data.getBlendMode();
                if (slotBlendMode != blendMode) {
                    blendMode = slotBlendMode;
                    batch.setBlendFunction(blendMode.getSource(premultipliedAlpha), blendMode.getDest());
                }
                batch.draw(texture, vertices, 0, vertices.length, triangles, 0, triangles.length);
            }
        }
    }
}
