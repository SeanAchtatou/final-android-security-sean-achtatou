package com.kbz.esotericsoftware.spine.attachments;

public abstract class Attachment {
    final String name;

    public Attachment(String name2) {
        if (name2 == null) {
            throw new IllegalArgumentException("name cannot be null.");
        }
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        return getName();
    }
}
