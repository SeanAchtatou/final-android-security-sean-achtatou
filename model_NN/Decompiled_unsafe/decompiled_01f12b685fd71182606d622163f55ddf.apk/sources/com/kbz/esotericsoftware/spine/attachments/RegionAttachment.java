package com.kbz.esotericsoftware.spine.attachments;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.NumberUtils;
import com.kbz.esotericsoftware.spine.Bone;
import com.kbz.esotericsoftware.spine.Skeleton;
import com.kbz.esotericsoftware.spine.Slot;

public class RegionAttachment extends Attachment {
    public static final int BLX = 0;
    public static final int BLY = 1;
    public static final int BRX = 6;
    public static final int BRY = 7;
    public static final int ULX = 2;
    public static final int ULY = 3;
    public static final int URX = 4;
    public static final int URY = 5;
    private final Color color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    private float height;
    private final float[] offset = new float[8];
    private String path;
    private TextureRegion region;
    private float rotation;
    private float scaleX = 1.0f;
    private float scaleY = 1.0f;
    private final float[] vertices = new float[20];
    private float width;
    private float x;
    private float y;

    public RegionAttachment(String name) {
        super(name);
    }

    public void updateOffset() {
        float width2 = getWidth();
        float height2 = getHeight();
        float localX2 = width2 / 2.0f;
        float localY2 = height2 / 2.0f;
        float localX = -localX2;
        float localY = -localY2;
        if (this.region instanceof TextureAtlas.AtlasRegion) {
            TextureAtlas.AtlasRegion region2 = (TextureAtlas.AtlasRegion) this.region;
            if (region2.rotate) {
                localX += (region2.offsetX / ((float) region2.originalWidth)) * width2;
                localY += (region2.offsetY / ((float) region2.originalHeight)) * height2;
                localX2 -= (((((float) region2.originalWidth) - region2.offsetX) - ((float) region2.packedHeight)) / ((float) region2.originalWidth)) * width2;
                localY2 -= (((((float) region2.originalHeight) - region2.offsetY) - ((float) region2.packedWidth)) / ((float) region2.originalHeight)) * height2;
            } else {
                localX += (region2.offsetX / ((float) region2.originalWidth)) * width2;
                localY += (region2.offsetY / ((float) region2.originalHeight)) * height2;
                localX2 -= (((((float) region2.originalWidth) - region2.offsetX) - ((float) region2.packedWidth)) / ((float) region2.originalWidth)) * width2;
                localY2 -= (((((float) region2.originalHeight) - region2.offsetY) - ((float) region2.packedHeight)) / ((float) region2.originalHeight)) * height2;
            }
        }
        float scaleX2 = getScaleX();
        float scaleY2 = getScaleY();
        float localX3 = localX * scaleX2;
        float localY3 = localY * scaleY2;
        float localX22 = localX2 * scaleX2;
        float localY22 = localY2 * scaleY2;
        float rotation2 = getRotation();
        float cos = MathUtils.cosDeg(rotation2);
        float sin = MathUtils.sinDeg(rotation2);
        float x2 = getX();
        float y2 = getY();
        float localXCos = (localX3 * cos) + x2;
        float localXSin = localX3 * sin;
        float localYCos = (localY3 * cos) + y2;
        float localYSin = localY3 * sin;
        float localX2Cos = (localX22 * cos) + x2;
        float localX2Sin = localX22 * sin;
        float localY2Cos = (localY22 * cos) + y2;
        float localY2Sin = localY22 * sin;
        float[] offset2 = this.offset;
        offset2[0] = localXCos - localYSin;
        offset2[1] = localYCos + localXSin;
        offset2[2] = localXCos - localY2Sin;
        offset2[3] = localY2Cos + localXSin;
        offset2[4] = localX2Cos - localY2Sin;
        offset2[5] = localY2Cos + localX2Sin;
        offset2[6] = localX2Cos - localYSin;
        offset2[7] = localYCos + localX2Sin;
    }

    public void setRegion(TextureRegion region2) {
        if (region2 == null) {
            throw new IllegalArgumentException("region cannot be null.");
        }
        this.region = region2;
        float[] vertices2 = this.vertices;
        if (!(region2 instanceof TextureAtlas.AtlasRegion) || !((TextureAtlas.AtlasRegion) region2).rotate) {
            vertices2[8] = region2.getU();
            vertices2[9] = region2.getV2();
            vertices2[13] = region2.getU();
            vertices2[14] = region2.getV();
            vertices2[18] = region2.getU2();
            vertices2[19] = region2.getV();
            vertices2[3] = region2.getU2();
            vertices2[4] = region2.getV2();
            return;
        }
        vertices2[13] = region2.getU();
        vertices2[14] = region2.getV2();
        vertices2[18] = region2.getU();
        vertices2[19] = region2.getV();
        vertices2[3] = region2.getU2();
        vertices2[4] = region2.getV();
        vertices2[8] = region2.getU2();
        vertices2[9] = region2.getV2();
    }

    public TextureRegion getRegion() {
        if (this.region != null) {
            return this.region;
        }
        throw new IllegalStateException("Region has not been set: " + this);
    }

    public float[] updateWorldVertices(Slot slot, boolean premultipliedAlpha) {
        Skeleton skeleton = slot.getSkeleton();
        Color skeletonColor = skeleton.getColor();
        Color slotColor = slot.getColor();
        Color regionColor = this.color;
        float a = skeletonColor.a * slotColor.a * regionColor.a * 255.0f;
        float multiplier = premultipliedAlpha ? a : 255.0f;
        float color2 = NumberUtils.intToFloatColor((((int) a) << 24) | (((int) (((skeletonColor.b * slotColor.b) * regionColor.b) * multiplier)) << 16) | (((int) (((skeletonColor.g * slotColor.g) * regionColor.g) * multiplier)) << 8) | ((int) (skeletonColor.r * slotColor.r * regionColor.r * multiplier)));
        float[] vertices2 = this.vertices;
        float[] offset2 = this.offset;
        Bone bone = slot.getBone();
        float x2 = skeleton.getX() + bone.getWorldX();
        float y2 = skeleton.getY() + bone.getWorldY();
        float m00 = bone.getA();
        float m01 = bone.getB();
        float m10 = bone.getC();
        float m11 = bone.getD();
        float offsetX = offset2[6];
        float offsetY = offset2[7];
        vertices2[0] = (offsetX * m00) + (offsetY * m01) + x2;
        vertices2[1] = (offsetX * m10) + (offsetY * m11) + y2;
        vertices2[2] = color2;
        float offsetX2 = offset2[0];
        float offsetY2 = offset2[1];
        vertices2[5] = (offsetX2 * m00) + (offsetY2 * m01) + x2;
        vertices2[6] = (offsetX2 * m10) + (offsetY2 * m11) + y2;
        vertices2[7] = color2;
        float offsetX3 = offset2[2];
        float offsetY3 = offset2[3];
        vertices2[10] = (offsetX3 * m00) + (offsetY3 * m01) + x2;
        vertices2[11] = (offsetX3 * m10) + (offsetY3 * m11) + y2;
        vertices2[12] = color2;
        float offsetX4 = offset2[4];
        float offsetY4 = offset2[5];
        vertices2[15] = (offsetX4 * m00) + (offsetY4 * m01) + x2;
        vertices2[16] = (offsetX4 * m10) + (offsetY4 * m11) + y2;
        vertices2[17] = color2;
        return vertices2;
    }

    public float[] getWorldVertices() {
        return this.vertices;
    }

    public float[] getOffset() {
        return this.offset;
    }

    public float getX() {
        return this.x;
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public float getY() {
        return this.y;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public float getScaleX() {
        return this.scaleX;
    }

    public void setScaleX(float scaleX2) {
        this.scaleX = scaleX2;
    }

    public float getScaleY() {
        return this.scaleY;
    }

    public void setScaleY(float scaleY2) {
        this.scaleY = scaleY2;
    }

    public float getRotation() {
        return this.rotation;
    }

    public void setRotation(float rotation2) {
        this.rotation = rotation2;
    }

    public float getWidth() {
        return this.width;
    }

    public void setWidth(float width2) {
        this.width = width2;
    }

    public float getHeight() {
        return this.height;
    }

    public void setHeight(float height2) {
        this.height = height2;
    }

    public Color getColor() {
        return this.color;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path2) {
        this.path = path2;
    }
}
