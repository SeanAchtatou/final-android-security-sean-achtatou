package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;
import com.kbz.esotericsoftware.spine.attachments.Attachment;

public class Skin {
    private static final Key lookup = new Key();
    final ObjectMap<Key, Attachment> attachments = new ObjectMap<>();
    final Pool<Key> keyPool = new Pool(64) {
        /* access modifiers changed from: protected */
        public Object newObject() {
            return new Key();
        }
    };
    final String name;

    public Skin(String name2) {
        if (name2 == null) {
            throw new IllegalArgumentException("name cannot be null.");
        }
        this.name = name2;
    }

    public void addAttachment(int slotIndex, String name2, Attachment attachment) {
        if (attachment == null) {
            throw new IllegalArgumentException("attachment cannot be null.");
        } else if (slotIndex < 0) {
            throw new IllegalArgumentException("slotIndex must be >= 0.");
        } else {
            Key key = this.keyPool.obtain();
            key.set(slotIndex, name2);
            this.attachments.put(key, attachment);
        }
    }

    public Attachment getAttachment(int slotIndex, String name2) {
        if (slotIndex < 0) {
            throw new IllegalArgumentException("slotIndex must be >= 0.");
        }
        lookup.set(slotIndex, name2);
        return this.attachments.get(lookup);
    }

    public void findNamesForSlot(int slotIndex, Array<String> names) {
        if (names == null) {
            throw new IllegalArgumentException("names cannot be null.");
        } else if (slotIndex < 0) {
            throw new IllegalArgumentException("slotIndex must be >= 0.");
        } else {
            ObjectMap.Keys<Key> it = this.attachments.keys().iterator();
            while (it.hasNext()) {
                Key key = (Key) it.next();
                if (key.slotIndex == slotIndex) {
                    names.add(key.name);
                }
            }
        }
    }

    public void findAttachmentsForSlot(int slotIndex, Array<Attachment> attachments2) {
        if (attachments2 == null) {
            throw new IllegalArgumentException("attachments cannot be null.");
        } else if (slotIndex < 0) {
            throw new IllegalArgumentException("slotIndex must be >= 0.");
        } else {
            ObjectMap.Entries<Key, Attachment> it = this.attachments.entries().iterator();
            while (it.hasNext()) {
                ObjectMap.Entry<Key, Attachment> entry = (ObjectMap.Entry) it.next();
                if (((Key) entry.key).slotIndex == slotIndex) {
                    attachments2.add(entry.value);
                }
            }
        }
    }

    public void clear() {
        ObjectMap.Keys<Key> it = this.attachments.keys().iterator();
        while (it.hasNext()) {
            this.keyPool.free((Key) it.next());
        }
        this.attachments.clear();
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        return this.name;
    }

    /* access modifiers changed from: package-private */
    public void attachAll(Skeleton skeleton, Skin oldSkin) {
        Attachment attachment;
        ObjectMap.Entries<Key, Attachment> it = oldSkin.attachments.entries().iterator();
        while (it.hasNext()) {
            ObjectMap.Entry<Key, Attachment> entry = (ObjectMap.Entry) it.next();
            int slotIndex = ((Key) entry.key).slotIndex;
            Slot slot = skeleton.slots.get(slotIndex);
            if (slot.attachment == entry.value && (attachment = getAttachment(slotIndex, ((Key) entry.key).name)) != null) {
                slot.setAttachment(attachment);
            }
        }
    }

    static class Key {
        int hashCode;
        String name;
        int slotIndex;

        Key() {
        }

        public void set(int slotIndex2, String name2) {
            if (name2 == null) {
                throw new IllegalArgumentException("name cannot be null.");
            }
            this.slotIndex = slotIndex2;
            this.name = name2;
            this.hashCode = ((name2.hashCode() + 31) * 31) + slotIndex2;
        }

        public int hashCode() {
            return this.hashCode;
        }

        public boolean equals(Object object) {
            if (object == null) {
                return false;
            }
            Key other = (Key) object;
            if (this.slotIndex != other.slotIndex || !this.name.equals(other.name)) {
                return false;
            }
            return true;
        }

        public String toString() {
            return this.slotIndex + ":" + this.name;
        }
    }
}
