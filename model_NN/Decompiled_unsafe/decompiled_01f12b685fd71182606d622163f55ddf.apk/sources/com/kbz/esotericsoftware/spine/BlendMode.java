package com.kbz.esotericsoftware.spine;

public enum BlendMode {
    normal(770, 1, 771),
    additive(770, 1, 1),
    multiply(774, 774, 771),
    screen(1, 1, 769);
    
    public static BlendMode[] values = values();
    int dest;
    int source;
    int sourcePMA;

    private BlendMode(int source2, int sourcePremultipledAlpha, int dest2) {
        this.source = source2;
        this.sourcePMA = sourcePremultipledAlpha;
        this.dest = dest2;
    }

    public int getSource(boolean premultipliedAlpha) {
        return premultipliedAlpha ? this.sourcePMA : this.source;
    }

    public int getDest() {
        return this.dest;
    }
}
