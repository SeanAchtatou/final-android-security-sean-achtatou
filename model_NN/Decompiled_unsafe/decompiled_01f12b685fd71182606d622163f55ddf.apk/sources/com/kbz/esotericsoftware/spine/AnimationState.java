package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.kbz.esotericsoftware.spine.Animation;

public class AnimationState {
    private AnimationStateData data;
    private final Array<Event> events = new Array<>();
    private boolean isEnd;
    private final Array<AnimationStateListener> listeners = new Array<>();
    public float playTime;
    private float timeScale = 1.0f;
    private Pool<TrackEntry> trackEntryPool = new Pool() {
        /* access modifiers changed from: protected */
        public Object newObject() {
            return new TrackEntry();
        }
    };
    private Array<TrackEntry> tracks = new Array<>();

    public interface AnimationStateListener {
        void complete(int i, int i2);

        void end(int i);

        void event(int i, Event event);

        void start(int i);
    }

    public AnimationState() {
    }

    public AnimationState(AnimationStateData data2) {
        if (data2 == null) {
            throw new IllegalArgumentException("data cannot be null.");
        }
        this.data = data2;
    }

    public boolean isEnd() {
        return this.isEnd;
    }

    public void update(float delta) {
        float delta2 = delta * this.timeScale;
        for (int i = 0; i < this.tracks.size; i++) {
            TrackEntry current = this.tracks.get(i);
            if (current != null) {
                TrackEntry next = current.next;
                if (next != null) {
                    float nextTime = current.lastTime - next.delay;
                    if (nextTime >= Animation.CurveTimeline.LINEAR) {
                        float nextDelta = delta2 * next.timeScale;
                        next.time = nextTime + nextDelta;
                        current.time += current.timeScale * delta2;
                        setCurrent(i, next);
                        next.time -= nextDelta;
                        current = next;
                    }
                } else if (!current.loop && current.lastTime >= current.endTime) {
                    clearTrack(i);
                }
                current.time += current.timeScale * delta2;
                if (current.previous != null) {
                    float previousDelta = delta2 * current.previous.timeScale;
                    current.previous.time += previousDelta;
                    current.mixTime += previousDelta;
                }
            }
        }
    }

    public void apply(Skeleton skeleton) {
        Array<Event> events2 = this.events;
        int listenerCount = this.listeners.size;
        for (int i = 0; i < this.tracks.size; i++) {
            TrackEntry current = this.tracks.get(i);
            if (current != null) {
                events2.size = 0;
                float time = current.time;
                float lastTime = current.lastTime;
                float endTime = current.endTime;
                boolean loop = current.loop;
                this.isEnd = false;
                if (!loop && time > endTime) {
                    this.isEnd = true;
                    time = endTime;
                }
                TrackEntry previous = current.previous;
                if (previous == null) {
                    current.animation.mix(skeleton, lastTime, time, loop, events2, current.mix);
                } else {
                    float previousTime = previous.time;
                    if (!previous.loop && previousTime > previous.endTime) {
                        previousTime = previous.endTime;
                    }
                    previous.animation.apply(skeleton, previousTime, previousTime, previous.loop, null);
                    float alpha = (current.mixTime / current.mixDuration) * current.mix;
                    if (alpha >= 1.0f) {
                        alpha = 1.0f;
                        this.trackEntryPool.free(previous);
                        current.previous = null;
                    }
                    current.animation.mix(skeleton, lastTime, time, loop, events2, alpha);
                }
                int nn = events2.size;
                for (int ii = 0; ii < nn; ii++) {
                    Event event = events2.get(ii);
                    if (current.listener != null) {
                        current.listener.event(i, event);
                    }
                    for (int iii = 0; iii < listenerCount; iii++) {
                        this.listeners.get(iii).event(i, event);
                    }
                }
                if (!loop ? !(lastTime >= endTime || time < endTime) : lastTime % endTime > time % endTime) {
                    int count = (int) (time / endTime);
                    if (current.listener != null) {
                        current.listener.complete(i, count);
                    }
                    int nn2 = this.listeners.size;
                    for (int ii2 = 0; ii2 < nn2; ii2++) {
                        this.listeners.get(ii2).complete(i, count);
                    }
                }
                current.lastTime = current.time;
            }
        }
    }

    public void clearTracks() {
        int n = this.tracks.size;
        for (int i = 0; i < n; i++) {
            clearTrack(i);
        }
        this.tracks.clear();
    }

    public void clearTrack(int trackIndex) {
        TrackEntry current;
        if (trackIndex < this.tracks.size && (current = this.tracks.get(trackIndex)) != null) {
            if (current.listener != null) {
                current.listener.end(trackIndex);
            }
            int n = this.listeners.size;
            for (int i = 0; i < n; i++) {
                this.listeners.get(i).end(trackIndex);
            }
            this.tracks.set(trackIndex, null);
            freeAll(current);
            if (current.previous != null) {
                this.trackEntryPool.free(current.previous);
            }
        }
    }

    private void freeAll(TrackEntry entry) {
        while (entry != null) {
            TrackEntry next = entry.next;
            this.trackEntryPool.free(entry);
            entry = next;
        }
    }

    private TrackEntry expandToIndex(int index) {
        if (index < this.tracks.size) {
            return this.tracks.get(index);
        }
        this.tracks.ensureCapacity((index - this.tracks.size) + 1);
        this.tracks.size = index + 1;
        return null;
    }

    private void setCurrent(int index, TrackEntry entry) {
        TrackEntry current = expandToIndex(index);
        if (current != null) {
            TrackEntry previous = current.previous;
            current.previous = null;
            if (current.listener != null) {
                current.listener.end(index);
            }
            int n = this.listeners.size;
            for (int i = 0; i < n; i++) {
                this.listeners.get(i).end(index);
            }
            entry.mixDuration = this.data.getMix(current.animation, entry.animation);
            if (entry.mixDuration > Animation.CurveTimeline.LINEAR) {
                entry.mixTime = Animation.CurveTimeline.LINEAR;
                if (previous == null || current.mixTime / current.mixDuration >= 0.5f) {
                    entry.previous = current;
                } else {
                    entry.previous = previous;
                    previous = current;
                }
            } else {
                this.trackEntryPool.free(current);
            }
            if (previous != null) {
                this.trackEntryPool.free(previous);
            }
        }
        this.tracks.set(index, entry);
        if (entry.listener != null) {
            entry.listener.start(index);
        }
        int n2 = this.listeners.size;
        for (int i2 = 0; i2 < n2; i2++) {
            this.listeners.get(i2).start(index);
        }
    }

    public TrackEntry setAnimation(int trackIndex, String animationName, boolean loop) {
        Animation animation = this.data.getSkeletonData().findAnimation(animationName);
        if (animation != null) {
            return setAnimation(trackIndex, animation, loop);
        }
        throw new IllegalArgumentException("Animation not found: " + animationName);
    }

    public TrackEntry setAnimation(int trackIndex, Animation animation, boolean loop) {
        TrackEntry current = expandToIndex(trackIndex);
        if (current != null) {
            freeAll(current.next);
        }
        TrackEntry entry = this.trackEntryPool.obtain();
        entry.animation = animation;
        entry.loop = loop;
        entry.endTime = animation.getDuration();
        setCurrent(trackIndex, entry);
        return entry;
    }

    public TrackEntry addAnimation(int trackIndex, String animationName, boolean loop, float delay) {
        Animation animation = this.data.getSkeletonData().findAnimation(animationName);
        if (animation != null) {
            return addAnimation(trackIndex, animation, loop, delay);
        }
        throw new IllegalArgumentException("Animation not found: " + animationName);
    }

    public TrackEntry addAnimation(int trackIndex, Animation animation, boolean loop, float delay) {
        TrackEntry entry = this.trackEntryPool.obtain();
        entry.animation = animation;
        entry.loop = loop;
        entry.endTime = animation.getDuration();
        TrackEntry last = expandToIndex(trackIndex);
        if (last != null) {
            while (last.next != null) {
                last = last.next;
            }
            last.next = entry;
        } else {
            this.tracks.set(trackIndex, entry);
        }
        if (delay <= Animation.CurveTimeline.LINEAR) {
            if (last != null) {
                delay += last.endTime - this.data.getMix(last.animation, animation);
            } else {
                delay = Animation.CurveTimeline.LINEAR;
            }
        }
        entry.delay = delay;
        return entry;
    }

    public TrackEntry getCurrent(int trackIndex) {
        if (trackIndex >= this.tracks.size) {
            return null;
        }
        return this.tracks.get(trackIndex);
    }

    public void addListener(AnimationStateListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException("listener cannot be null.");
        }
        this.listeners.add(listener);
    }

    public void removeListener(AnimationStateListener listener) {
        this.listeners.removeValue(listener, true);
    }

    public void clearListeners() {
        this.listeners.clear();
    }

    public float getTimeScale() {
        return this.timeScale;
    }

    public void setTimeScale(float timeScale2) {
        this.timeScale = timeScale2;
    }

    public AnimationStateData getData() {
        return this.data;
    }

    public void setData(AnimationStateData data2) {
        this.data = data2;
    }

    public Array<TrackEntry> getTracks() {
        return this.tracks;
    }

    public String toString() {
        StringBuilder buffer = new StringBuilder(64);
        int n = this.tracks.size;
        for (int i = 0; i < n; i++) {
            TrackEntry entry = this.tracks.get(i);
            if (entry != null) {
                if (buffer.length() > 0) {
                    buffer.append(", ");
                }
                buffer.append(entry.toString());
            }
        }
        if (buffer.length() == 0) {
            return "<none>";
        }
        return buffer.toString();
    }

    public static class TrackEntry implements Pool.Poolable {
        Animation animation;
        float delay;
        float endTime;
        float lastTime = -1.0f;
        AnimationStateListener listener;
        boolean loop;
        float mix = 1.0f;
        float mixDuration;
        float mixTime;
        TrackEntry next;
        TrackEntry previous;
        float time;
        float timeScale = 1.0f;

        public void reset() {
            this.next = null;
            this.previous = null;
            this.animation = null;
            this.listener = null;
            this.timeScale = 1.0f;
            this.lastTime = -1.0f;
            this.time = Animation.CurveTimeline.LINEAR;
        }

        public Animation getAnimation() {
            return this.animation;
        }

        public void setAnimation(Animation animation2) {
            this.animation = animation2;
        }

        public boolean getLoop() {
            return this.loop;
        }

        public void setLoop(boolean loop2) {
            this.loop = loop2;
        }

        public float getDelay() {
            return this.delay;
        }

        public void setDelay(float delay2) {
            this.delay = delay2;
        }

        public float getTime() {
            return this.time;
        }

        public void setTime(float time2) {
            this.time = time2;
        }

        public float getEndTime() {
            return this.endTime;
        }

        public void setEndTime(float endTime2) {
            this.endTime = endTime2;
        }

        public AnimationStateListener getListener() {
            return this.listener;
        }

        public void setListener(AnimationStateListener listener2) {
            this.listener = listener2;
        }

        public float getLastTime() {
            return this.lastTime;
        }

        public void setLastTime(float lastTime2) {
            this.lastTime = lastTime2;
        }

        public float getMix() {
            return this.mix;
        }

        public void setMix(float mix2) {
            this.mix = mix2;
        }

        public float getTimeScale() {
            return this.timeScale;
        }

        public void setTimeScale(float timeScale2) {
            this.timeScale = timeScale2;
        }

        public TrackEntry getNext() {
            return this.next;
        }

        public void setNext(TrackEntry next2) {
            this.next = next2;
        }

        public boolean isComplete() {
            return this.time >= this.endTime;
        }

        public String toString() {
            return this.animation == null ? "<none>" : this.animation.name;
        }
    }

    public static abstract class AnimationStateAdapter implements AnimationStateListener {
        public void event(int trackIndex, Event event) {
        }

        public void complete(int trackIndex, int loopCount) {
        }

        public void start(int trackIndex) {
        }

        public void end(int trackIndex) {
        }
    }
}
