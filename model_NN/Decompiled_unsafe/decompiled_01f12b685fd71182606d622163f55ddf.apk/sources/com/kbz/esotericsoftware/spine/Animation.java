package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.kbz.esotericsoftware.spine.attachments.Attachment;

public class Animation {
    private float duration;
    final String name;
    private final Array<Timeline> timelines;

    public interface Timeline {
        void apply(Skeleton skeleton, float f, float f2, Array<Event> array, float f3);
    }

    public Animation(String name2, Array<Timeline> timelines2, float duration2) {
        if (name2 == null) {
            throw new IllegalArgumentException("name cannot be null.");
        } else if (timelines2 == null) {
            throw new IllegalArgumentException("timelines cannot be null.");
        } else {
            this.name = name2;
            this.timelines = timelines2;
            this.duration = duration2;
        }
    }

    public Array<Timeline> getTimelines() {
        return this.timelines;
    }

    public float getDuration() {
        return this.duration;
    }

    public void setDuration(float duration2) {
        this.duration = duration2;
    }

    public void apply(Skeleton skeleton, float lastTime, float time, boolean loop, Array<Event> events) {
        if (skeleton == null) {
            throw new IllegalArgumentException("skeleton cannot be null.");
        }
        if (loop && this.duration != CurveTimeline.LINEAR) {
            time %= this.duration;
            if (lastTime > CurveTimeline.LINEAR) {
                lastTime %= this.duration;
            }
        }
        Array<Timeline> timelines2 = this.timelines;
        int n = timelines2.size;
        for (int i = 0; i < n; i++) {
            timelines2.get(i).apply(skeleton, lastTime, time, events, 1.0f);
        }
    }

    public void mix(Skeleton skeleton, float lastTime, float time, boolean loop, Array<Event> events, float alpha) {
        if (skeleton == null) {
            throw new IllegalArgumentException("skeleton cannot be null.");
        }
        if (loop && this.duration != CurveTimeline.LINEAR) {
            time %= this.duration;
            if (lastTime > CurveTimeline.LINEAR) {
                lastTime %= this.duration;
            }
        }
        Array<Timeline> timelines2 = this.timelines;
        int n = timelines2.size;
        for (int i = 0; i < n; i++) {
            timelines2.get(i).apply(skeleton, lastTime, time, events, alpha);
        }
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        return this.name;
    }

    static int binarySearch(float[] values, float target, int step) {
        int low = 0;
        int high = (values.length / step) - 2;
        if (high == 0) {
            return step;
        }
        int current = high >>> 1;
        while (true) {
            if (values[(current + 1) * step] <= target) {
                low = current + 1;
            } else {
                high = current;
            }
            if (low == high) {
                return step * (low + 1);
            }
            current = (low + high) >>> 1;
        }
    }

    static int binarySearch(float[] values, float target) {
        int low = 0;
        int high = values.length - 2;
        if (high == 0) {
            return 1;
        }
        int current = high >>> 1;
        while (true) {
            if (values[current + 1] <= target) {
                low = current + 1;
            } else {
                high = current;
            }
            if (low == high) {
                return low + 1;
            }
            current = (low + high) >>> 1;
        }
    }

    static int linearSearch(float[] values, float target, int step) {
        int i = 0;
        int last = values.length - step;
        while (i <= last) {
            if (values[i] > target) {
                return i;
            }
            i += step;
        }
        return -1;
    }

    public static abstract class CurveTimeline implements Timeline {
        public static final float BEZIER = 2.0f;
        private static final int BEZIER_SEGMENTS = 10;
        private static final int BEZIER_SIZE = 19;
        public static final float LINEAR = 0.0f;
        public static final float STEPPED = 1.0f;
        private final float[] curves;

        public CurveTimeline(int frameCount) {
            if (frameCount <= 0) {
                throw new IllegalArgumentException("frameCount must be > 0: " + frameCount);
            }
            this.curves = new float[((frameCount - 1) * 19)];
        }

        public int getFrameCount() {
            return (this.curves.length / 19) + 1;
        }

        public void setLinear(int frameIndex) {
            this.curves[frameIndex * 19] = 0.0f;
        }

        public void setStepped(int frameIndex) {
            this.curves[frameIndex * 19] = 1.0f;
        }

        public float getCurveType(int frameIndex) {
            int index = frameIndex * 19;
            if (index == this.curves.length) {
                return LINEAR;
            }
            float type = this.curves[index];
            if (type == LINEAR) {
                return LINEAR;
            }
            if (type == 1.0f) {
                return 1.0f;
            }
            return 2.0f;
        }

        public void setCurve(int frameIndex, float cx1, float cy1, float cx2, float cy2) {
            float subdiv2 = 0.1f * 0.1f;
            float subdiv3 = subdiv2 * 0.1f;
            float pre1 = 3.0f * 0.1f;
            float pre2 = 3.0f * subdiv2;
            float pre4 = 6.0f * subdiv2;
            float pre5 = 6.0f * subdiv3;
            float tmp1x = ((-cx1) * 2.0f) + cx2;
            float tmp1y = ((-cy1) * 2.0f) + cy2;
            float tmp2x = ((cx1 - cx2) * 3.0f) + 1.0f;
            float tmp2y = ((cy1 - cy2) * 3.0f) + 1.0f;
            float dfx = (cx1 * pre1) + (tmp1x * pre2) + (tmp2x * subdiv3);
            float dfy = (cy1 * pre1) + (tmp1y * pre2) + (tmp2y * subdiv3);
            float ddfx = (tmp1x * pre4) + (tmp2x * pre5);
            float ddfy = (tmp1y * pre4) + (tmp2y * pre5);
            float dddfx = tmp2x * pre5;
            float dddfy = tmp2y * pre5;
            int i = frameIndex * 19;
            float[] curves2 = this.curves;
            int i2 = i + 1;
            curves2[i] = 2.0f;
            float x = dfx;
            float y = dfy;
            int n = (i2 + 19) - 1;
            for (int i3 = i2; i3 < n; i3 += 2) {
                curves2[i3] = x;
                curves2[i3 + 1] = y;
                dfx += ddfx;
                dfy += ddfy;
                ddfx += dddfx;
                ddfy += dddfy;
                x += dfx;
                y += dfy;
            }
        }

        public float getCurvePercent(int frameIndex, float percent) {
            float prevX;
            float prevY;
            float[] curves2 = this.curves;
            int i = frameIndex * 19;
            float type = curves2[i];
            if (type == LINEAR) {
                return percent;
            }
            if (type == 1.0f) {
                return LINEAR;
            }
            int i2 = i + 1;
            float x = LINEAR;
            int start = i2;
            int n = (i2 + 19) - 1;
            while (i2 < n) {
                x = curves2[i2];
                if (x >= percent) {
                    if (i2 == start) {
                        prevX = LINEAR;
                        prevY = LINEAR;
                    } else {
                        prevX = curves2[i2 - 2];
                        prevY = curves2[i2 - 1];
                    }
                    return prevY + (((curves2[i2 + 1] - prevY) * (percent - prevX)) / (x - prevX));
                }
                i2 += 2;
            }
            float y = curves2[i2 - 1];
            return y + (((1.0f - y) * (percent - x)) / (1.0f - x));
        }
    }

    public static class RotateTimeline extends CurveTimeline {
        private static final int FRAME_VALUE = 1;
        private static final int PREV_FRAME_TIME = -2;
        int boneIndex;
        private final float[] frames;

        public RotateTimeline(int frameCount) {
            super(frameCount);
            this.frames = new float[(frameCount << 1)];
        }

        public void setBoneIndex(int boneIndex2) {
            this.boneIndex = boneIndex2;
        }

        public int getBoneIndex() {
            return this.boneIndex;
        }

        public float[] getFrames() {
            return this.frames;
        }

        public void setFrame(int frameIndex, float time, float angle) {
            int frameIndex2 = frameIndex * 2;
            this.frames[frameIndex2] = time;
            this.frames[frameIndex2 + 1] = angle;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
         arg types: [float, ?, int]
         candidates:
          com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
          com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
          com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
          com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
          com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
        public void apply(Skeleton skeleton, float lastTime, float time, Array<Event> array, float alpha) {
            float[] frames2 = this.frames;
            if (time >= frames2[0]) {
                Bone bone = skeleton.bones.get(this.boneIndex);
                if (time >= frames2[frames2.length - 2]) {
                    float amount = (bone.data.rotation + frames2[frames2.length - 1]) - bone.rotation;
                    while (amount > 180.0f) {
                        amount -= 360.0f;
                    }
                    while (amount < -180.0f) {
                        amount += 360.0f;
                    }
                    bone.rotation += amount * alpha;
                    return;
                }
                int frameIndex = Animation.binarySearch(frames2, time, 2);
                float prevFrameValue = frames2[frameIndex - 1];
                float frameTime = frames2[frameIndex];
                float percent = getCurvePercent((frameIndex >> 1) - 1, MathUtils.clamp(1.0f - ((time - frameTime) / (frames2[frameIndex - 2] - frameTime)), (float) CurveTimeline.LINEAR, 1.0f));
                float amount2 = frames2[frameIndex + 1] - prevFrameValue;
                while (amount2 > 180.0f) {
                    amount2 -= 360.0f;
                }
                while (amount2 < -180.0f) {
                    amount2 += 360.0f;
                }
                float amount3 = (bone.data.rotation + ((amount2 * percent) + prevFrameValue)) - bone.rotation;
                while (amount3 > 180.0f) {
                    amount3 -= 360.0f;
                }
                while (amount3 < -180.0f) {
                    amount3 += 360.0f;
                }
                bone.rotation += amount3 * alpha;
            }
        }
    }

    public static class TranslateTimeline extends CurveTimeline {
        static final int FRAME_X = 1;
        static final int FRAME_Y = 2;
        static final int PREV_FRAME_TIME = -3;
        int boneIndex;
        final float[] frames;

        public TranslateTimeline(int frameCount) {
            super(frameCount);
            this.frames = new float[(frameCount * 3)];
        }

        public void setBoneIndex(int boneIndex2) {
            this.boneIndex = boneIndex2;
        }

        public int getBoneIndex() {
            return this.boneIndex;
        }

        public float[] getFrames() {
            return this.frames;
        }

        public void setFrame(int frameIndex, float time, float x, float y) {
            int frameIndex2 = frameIndex * 3;
            this.frames[frameIndex2] = time;
            this.frames[frameIndex2 + 1] = x;
            this.frames[frameIndex2 + 2] = y;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
         arg types: [float, ?, int]
         candidates:
          com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
          com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
          com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
          com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
          com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
        public void apply(Skeleton skeleton, float lastTime, float time, Array<Event> array, float alpha) {
            float[] frames2 = this.frames;
            if (time >= frames2[0]) {
                Bone bone = skeleton.bones.get(this.boneIndex);
                if (time >= frames2[frames2.length - 3]) {
                    bone.x += ((bone.data.x + frames2[frames2.length - 2]) - bone.x) * alpha;
                    bone.y += ((bone.data.y + frames2[frames2.length - 1]) - bone.y) * alpha;
                    return;
                }
                int frameIndex = Animation.binarySearch(frames2, time, 3);
                float prevFrameX = frames2[frameIndex - 2];
                float prevFrameY = frames2[frameIndex - 1];
                float frameTime = frames2[frameIndex];
                float percent = getCurvePercent((frameIndex / 3) - 1, MathUtils.clamp(1.0f - ((time - frameTime) / (frames2[frameIndex - 3] - frameTime)), (float) CurveTimeline.LINEAR, 1.0f));
                bone.x += (((bone.data.x + prevFrameX) + ((frames2[frameIndex + 1] - prevFrameX) * percent)) - bone.x) * alpha;
                bone.y += (((bone.data.y + prevFrameY) + ((frames2[frameIndex + 2] - prevFrameY) * percent)) - bone.y) * alpha;
            }
        }
    }

    public static class ScaleTimeline extends TranslateTimeline {
        public ScaleTimeline(int frameCount) {
            super(frameCount);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
         arg types: [float, ?, int]
         candidates:
          com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
          com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
          com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
          com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
          com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
        public void apply(Skeleton skeleton, float lastTime, float time, Array<Event> array, float alpha) {
            float[] frames = this.frames;
            if (time >= frames[0]) {
                Bone bone = skeleton.bones.get(this.boneIndex);
                if (time >= frames[frames.length - 3]) {
                    bone.scaleX += ((bone.data.scaleX * frames[frames.length - 2]) - bone.scaleX) * alpha;
                    bone.scaleY += ((bone.data.scaleY * frames[frames.length - 1]) - bone.scaleY) * alpha;
                    return;
                }
                int frameIndex = Animation.binarySearch(frames, time, 3);
                float prevFrameX = frames[frameIndex - 2];
                float prevFrameY = frames[frameIndex - 1];
                float frameTime = frames[frameIndex];
                float percent = getCurvePercent((frameIndex / 3) - 1, MathUtils.clamp(1.0f - ((time - frameTime) / (frames[frameIndex - 3] - frameTime)), (float) CurveTimeline.LINEAR, 1.0f));
                bone.scaleX += ((bone.data.scaleX * (((frames[frameIndex + 1] - prevFrameX) * percent) + prevFrameX)) - bone.scaleX) * alpha;
                bone.scaleY += ((bone.data.scaleY * (((frames[frameIndex + 2] - prevFrameY) * percent) + prevFrameY)) - bone.scaleY) * alpha;
            }
        }
    }

    public static class ColorTimeline extends CurveTimeline {
        private static final int FRAME_A = 4;
        private static final int FRAME_B = 3;
        private static final int FRAME_G = 2;
        private static final int FRAME_R = 1;
        private static final int PREV_FRAME_TIME = -5;
        private final float[] frames;
        int slotIndex;

        public ColorTimeline(int frameCount) {
            super(frameCount);
            this.frames = new float[(frameCount * 5)];
        }

        public void setSlotIndex(int slotIndex2) {
            this.slotIndex = slotIndex2;
        }

        public int getSlotIndex() {
            return this.slotIndex;
        }

        public float[] getFrames() {
            return this.frames;
        }

        public void setFrame(int frameIndex, float time, float r, float g, float b, float a) {
            int frameIndex2 = frameIndex * 5;
            this.frames[frameIndex2] = time;
            this.frames[frameIndex2 + 1] = r;
            this.frames[frameIndex2 + 2] = g;
            this.frames[frameIndex2 + 3] = b;
            this.frames[frameIndex2 + 4] = a;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
         arg types: [float, ?, int]
         candidates:
          com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
          com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
          com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
          com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
          com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
        public void apply(Skeleton skeleton, float lastTime, float time, Array<Event> array, float alpha) {
            float r;
            float g;
            float b;
            float a;
            float[] frames2 = this.frames;
            if (time >= frames2[0]) {
                if (time >= frames2[frames2.length + PREV_FRAME_TIME]) {
                    int i = frames2.length - 1;
                    r = frames2[i - 3];
                    g = frames2[i - 2];
                    b = frames2[i - 1];
                    a = frames2[i];
                } else {
                    int frameIndex = Animation.binarySearch(frames2, time, 5);
                    float prevFrameR = frames2[frameIndex - 4];
                    float prevFrameG = frames2[frameIndex - 3];
                    float prevFrameB = frames2[frameIndex - 2];
                    float prevFrameA = frames2[frameIndex - 1];
                    float frameTime = frames2[frameIndex];
                    float percent = getCurvePercent((frameIndex / 5) - 1, MathUtils.clamp(1.0f - ((time - frameTime) / (frames2[frameIndex + PREV_FRAME_TIME] - frameTime)), (float) CurveTimeline.LINEAR, 1.0f));
                    r = prevFrameR + ((frames2[frameIndex + 1] - prevFrameR) * percent);
                    g = prevFrameG + ((frames2[frameIndex + 2] - prevFrameG) * percent);
                    b = prevFrameB + ((frames2[frameIndex + 3] - prevFrameB) * percent);
                    a = prevFrameA + ((frames2[frameIndex + 4] - prevFrameA) * percent);
                }
                Color color = skeleton.slots.get(this.slotIndex).color;
                if (alpha < 1.0f) {
                    color.add((r - color.r) * alpha, (g - color.g) * alpha, (b - color.b) * alpha, (a - color.a) * alpha);
                } else {
                    color.set(r, g, b, a);
                }
            }
        }
    }

    public static class AttachmentTimeline implements Timeline {
        final String[] attachmentNames;
        final float[] frames;
        int slotIndex;

        public AttachmentTimeline(int frameCount) {
            this.frames = new float[frameCount];
            this.attachmentNames = new String[frameCount];
        }

        public int getFrameCount() {
            return this.frames.length;
        }

        public int getSlotIndex() {
            return this.slotIndex;
        }

        public void setSlotIndex(int slotIndex2) {
            this.slotIndex = slotIndex2;
        }

        public float[] getFrames() {
            return this.frames;
        }

        public String[] getAttachmentNames() {
            return this.attachmentNames;
        }

        public void setFrame(int frameIndex, float time, String attachmentName) {
            this.frames[frameIndex] = time;
            this.attachmentNames[frameIndex] = attachmentName;
        }

        public void apply(Skeleton skeleton, float lastTime, float time, Array<Event> array, float alpha) {
            Attachment attachment = null;
            float[] frames2 = this.frames;
            if (time >= frames2[0]) {
                if (lastTime > time) {
                    lastTime = -1.0f;
                }
                int frameIndex = (time >= frames2[frames2.length + -1] ? frames2.length : Animation.binarySearch(frames2, time)) - 1;
                if (frames2[frameIndex] >= lastTime) {
                    String attachmentName = this.attachmentNames[frameIndex];
                    Slot slot = skeleton.slots.get(this.slotIndex);
                    if (attachmentName != null) {
                        attachment = skeleton.getAttachment(this.slotIndex, attachmentName);
                    }
                    slot.setAttachment(attachment);
                }
            } else if (lastTime > time) {
                apply(skeleton, lastTime, 2.14748365E9f, null, CurveTimeline.LINEAR);
            }
        }
    }

    public static class EventTimeline implements Timeline {
        private final Event[] events;
        private final float[] frames;

        public EventTimeline(int frameCount) {
            this.frames = new float[frameCount];
            this.events = new Event[frameCount];
        }

        public int getFrameCount() {
            return this.frames.length;
        }

        public float[] getFrames() {
            return this.frames;
        }

        public Event[] getEvents() {
            return this.events;
        }

        public void setFrame(int frameIndex, Event event) {
            this.frames[frameIndex] = event.time;
            this.events[frameIndex] = event;
        }

        public void apply(Skeleton skeleton, float lastTime, float time, Array<Event> firedEvents, float alpha) {
            int frameIndex;
            if (firedEvents != null) {
                float[] frames2 = this.frames;
                int frameCount = frames2.length;
                if (lastTime > time) {
                    apply(skeleton, lastTime, 2.14748365E9f, firedEvents, alpha);
                    lastTime = -1.0f;
                } else if (lastTime >= frames2[frameCount - 1]) {
                    return;
                }
                if (time >= frames2[0]) {
                    if (lastTime < frames2[0]) {
                        frameIndex = 0;
                    } else {
                        frameIndex = Animation.binarySearch(frames2, lastTime);
                        float frame = frames2[frameIndex];
                        while (frameIndex > 0 && frames2[frameIndex - 1] == frame) {
                            frameIndex--;
                        }
                    }
                    while (frameIndex < frameCount && time >= frames2[frameIndex]) {
                        firedEvents.add(this.events[frameIndex]);
                        frameIndex++;
                    }
                }
            }
        }
    }

    public static class DrawOrderTimeline implements Timeline {
        private final int[][] drawOrders;
        private final float[] frames;

        public DrawOrderTimeline(int frameCount) {
            this.frames = new float[frameCount];
            this.drawOrders = new int[frameCount][];
        }

        public int getFrameCount() {
            return this.frames.length;
        }

        public float[] getFrames() {
            return this.frames;
        }

        public int[][] getDrawOrders() {
            return this.drawOrders;
        }

        public void setFrame(int frameIndex, float time, int[] drawOrder) {
            this.frames[frameIndex] = time;
            this.drawOrders[frameIndex] = drawOrder;
        }

        public void apply(Skeleton skeleton, float lastTime, float time, Array<Event> array, float alpha) {
            int frameIndex;
            float[] frames2 = this.frames;
            if (time >= frames2[0]) {
                if (time >= frames2[frames2.length - 1]) {
                    frameIndex = frames2.length - 1;
                } else {
                    frameIndex = Animation.binarySearch(frames2, time) - 1;
                }
                Array<Slot> drawOrder = skeleton.drawOrder;
                Array<Slot> slots = skeleton.slots;
                int[] drawOrderToSetupIndex = this.drawOrders[frameIndex];
                if (drawOrderToSetupIndex == null) {
                    System.arraycopy(slots.items, 0, drawOrder.items, 0, slots.size);
                    return;
                }
                int n = drawOrderToSetupIndex.length;
                for (int i = 0; i < n; i++) {
                    drawOrder.set(i, slots.get(drawOrderToSetupIndex[i]));
                }
            }
        }
    }

    public static class FfdTimeline extends CurveTimeline {
        Attachment attachment;
        private final float[][] frameVertices;
        private final float[] frames;
        int slotIndex;

        public FfdTimeline(int frameCount) {
            super(frameCount);
            this.frames = new float[frameCount];
            this.frameVertices = new float[frameCount][];
        }

        public void setSlotIndex(int slotIndex2) {
            this.slotIndex = slotIndex2;
        }

        public int getSlotIndex() {
            return this.slotIndex;
        }

        public void setAttachment(Attachment attachment2) {
            this.attachment = attachment2;
        }

        public Attachment getAttachment() {
            return this.attachment;
        }

        public float[] getFrames() {
            return this.frames;
        }

        public float[][] getVertices() {
            return this.frameVertices;
        }

        public void setFrame(int frameIndex, float time, float[] vertices) {
            this.frames[frameIndex] = time;
            this.frameVertices[frameIndex] = vertices;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
         arg types: [float, ?, int]
         candidates:
          com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
          com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
          com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
          com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
          com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
        public void apply(Skeleton skeleton, float lastTime, float time, Array<Event> array, float alpha) {
            Slot slot = skeleton.slots.get(this.slotIndex);
            if (slot.getAttachment() == this.attachment) {
                float[] frames2 = this.frames;
                if (time >= frames2[0]) {
                    float[][] frameVertices2 = this.frameVertices;
                    int vertexCount = frameVertices2[0].length;
                    FloatArray verticesArray = slot.getAttachmentVertices();
                    if (verticesArray.size != vertexCount) {
                        alpha = 1.0f;
                    }
                    verticesArray.size = 0;
                    verticesArray.ensureCapacity(vertexCount);
                    verticesArray.size = vertexCount;
                    float[] vertices = verticesArray.items;
                    if (time >= frames2[frames2.length - 1]) {
                        float[] lastVertices = frameVertices2[frames2.length - 1];
                        if (alpha < 1.0f) {
                            for (int i = 0; i < vertexCount; i++) {
                                vertices[i] = vertices[i] + ((lastVertices[i] - vertices[i]) * alpha);
                            }
                            return;
                        }
                        System.arraycopy(lastVertices, 0, vertices, 0, vertexCount);
                        return;
                    }
                    int frameIndex = Animation.binarySearch(frames2, time);
                    float frameTime = frames2[frameIndex];
                    float percent = getCurvePercent(frameIndex - 1, MathUtils.clamp(1.0f - ((time - frameTime) / (frames2[frameIndex - 1] - frameTime)), (float) CurveTimeline.LINEAR, 1.0f));
                    float[] prevVertices = frameVertices2[frameIndex - 1];
                    float[] nextVertices = frameVertices2[frameIndex];
                    if (alpha < 1.0f) {
                        for (int i2 = 0; i2 < vertexCount; i2++) {
                            float prev = prevVertices[i2];
                            vertices[i2] = vertices[i2] + (((((nextVertices[i2] - prev) * percent) + prev) - vertices[i2]) * alpha);
                        }
                        return;
                    }
                    for (int i3 = 0; i3 < vertexCount; i3++) {
                        float prev2 = prevVertices[i3];
                        vertices[i3] = ((nextVertices[i3] - prev2) * percent) + prev2;
                    }
                }
            }
        }
    }

    public static class IkConstraintTimeline extends CurveTimeline {
        private static final int FRAME_MIX = 1;
        private static final int PREV_FRAME_BEND_DIRECTION = -1;
        private static final int PREV_FRAME_MIX = -2;
        private static final int PREV_FRAME_TIME = -3;
        private final float[] frames;
        int ikConstraintIndex;

        public IkConstraintTimeline(int frameCount) {
            super(frameCount);
            this.frames = new float[(frameCount * 3)];
        }

        public void setIkConstraintIndex(int ikConstraint) {
            this.ikConstraintIndex = ikConstraint;
        }

        public int getIkConstraintIndex() {
            return this.ikConstraintIndex;
        }

        public float[] getFrames() {
            return this.frames;
        }

        public void setFrame(int frameIndex, float time, float mix, int bendDirection) {
            int frameIndex2 = frameIndex * 3;
            this.frames[frameIndex2] = time;
            this.frames[frameIndex2 + 1] = mix;
            this.frames[frameIndex2 + 2] = (float) bendDirection;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float
         arg types: [float, ?, int]
         candidates:
          com.badlogic.gdx.math.MathUtils.clamp(double, double, double):double
          com.badlogic.gdx.math.MathUtils.clamp(int, int, int):int
          com.badlogic.gdx.math.MathUtils.clamp(long, long, long):long
          com.badlogic.gdx.math.MathUtils.clamp(short, short, short):short
          com.badlogic.gdx.math.MathUtils.clamp(float, float, float):float */
        public void apply(Skeleton skeleton, float lastTime, float time, Array<Event> array, float alpha) {
            float[] frames2 = this.frames;
            if (time >= frames2[0]) {
                IkConstraint ikConstraint = skeleton.ikConstraints.get(this.ikConstraintIndex);
                if (time >= frames2[frames2.length - 3]) {
                    ikConstraint.mix += (frames2[frames2.length - 2] - ikConstraint.mix) * alpha;
                    ikConstraint.bendDirection = (int) frames2[frames2.length - 1];
                    return;
                }
                int frameIndex = Animation.binarySearch(frames2, time, 3);
                float prevFrameMix = frames2[frameIndex - 2];
                float frameTime = frames2[frameIndex];
                ikConstraint.mix += ((prevFrameMix + ((frames2[frameIndex + 1] - prevFrameMix) * getCurvePercent((frameIndex / 3) - 1, MathUtils.clamp(1.0f - ((time - frameTime) / (frames2[frameIndex - 3] - frameTime)), (float) CurveTimeline.LINEAR, 1.0f)))) - ikConstraint.mix) * alpha;
                ikConstraint.bendDirection = (int) frames2[frameIndex - 1];
            }
        }
    }
}
