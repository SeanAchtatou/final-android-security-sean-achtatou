package com.kbz.esotericsoftware.spine.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kbz.esotericsoftware.spine.AnimationState;
import com.kbz.esotericsoftware.spine.Skeleton;
import com.kbz.esotericsoftware.spine.SkeletonRenderer;

public class SkeletonActor extends Actor {
    private SkeletonRenderer renderer;
    private Skeleton skeleton;
    AnimationState state;

    public SkeletonActor() {
    }

    public SkeletonActor(SkeletonRenderer renderer2, Skeleton skeleton2, AnimationState state2) {
        this.renderer = renderer2;
        this.skeleton = skeleton2;
        this.state = state2;
    }

    public void act(float delta) {
        this.state.update(delta);
        this.state.apply(this.skeleton);
        this.skeleton.updateWorldTransform();
        super.act(delta);
    }

    public void draw(Batch batch, float parentAlpha) {
        Color color = this.skeleton.getColor();
        float oldAlpha = color.a;
        this.skeleton.getColor().a *= parentAlpha;
        this.skeleton.setPosition(getX(), getY());
        this.renderer.draw(batch, this.skeleton);
        color.a = oldAlpha;
    }

    public SkeletonRenderer getRenderer() {
        return this.renderer;
    }

    public void setRenderer(SkeletonRenderer renderer2) {
        this.renderer = renderer2;
    }

    public Skeleton getSkeleton() {
        return this.skeleton;
    }

    public void setSkeleton(Skeleton skeleton2) {
        this.skeleton = skeleton2;
    }

    public AnimationState getAnimationState() {
        return this.state;
    }

    public void setAnimationState(AnimationState state2) {
        this.state = state2;
    }
}
