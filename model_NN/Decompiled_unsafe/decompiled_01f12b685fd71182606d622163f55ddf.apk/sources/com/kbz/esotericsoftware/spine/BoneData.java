package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.graphics.Color;

public class BoneData {
    final Color color = new Color(0.61f, 0.61f, 0.61f, 1.0f);
    boolean inheritRotation = true;
    boolean inheritScale = true;
    float length;
    final String name;
    final BoneData parent;
    float rotation;
    float scaleX = 1.0f;
    float scaleY = 1.0f;
    float x;
    float y;

    public BoneData(String name2, BoneData parent2) {
        if (name2 == null) {
            throw new IllegalArgumentException("name cannot be null.");
        }
        this.name = name2;
        this.parent = parent2;
    }

    public BoneData(BoneData bone, BoneData parent2) {
        if (bone == null) {
            throw new IllegalArgumentException("bone cannot be null.");
        }
        this.parent = parent2;
        this.name = bone.name;
        this.length = bone.length;
        this.x = bone.x;
        this.y = bone.y;
        this.rotation = bone.rotation;
        this.scaleX = bone.scaleX;
        this.scaleY = bone.scaleY;
    }

    public BoneData getParent() {
        return this.parent;
    }

    public String getName() {
        return this.name;
    }

    public float getLength() {
        return this.length;
    }

    public void setLength(float length2) {
        this.length = length2;
    }

    public float getX() {
        return this.x;
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public float getY() {
        return this.y;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public void setPosition(float x2, float y2) {
        this.x = x2;
        this.y = y2;
    }

    public float getRotation() {
        return this.rotation;
    }

    public void setRotation(float rotation2) {
        this.rotation = rotation2;
    }

    public float getScaleX() {
        return this.scaleX;
    }

    public void setScaleX(float scaleX2) {
        this.scaleX = scaleX2;
    }

    public float getScaleY() {
        return this.scaleY;
    }

    public void setScaleY(float scaleY2) {
        this.scaleY = scaleY2;
    }

    public void setScale(float scaleX2, float scaleY2) {
        this.scaleX = scaleX2;
        this.scaleY = scaleY2;
    }

    public boolean getInheritScale() {
        return this.inheritScale;
    }

    public void setInheritScale(boolean inheritScale2) {
        this.inheritScale = inheritScale2;
    }

    public boolean getInheritRotation() {
        return this.inheritRotation;
    }

    public void setInheritRotation(boolean inheritRotation2) {
        this.inheritRotation = inheritRotation2;
    }

    public Color getColor() {
        return this.color;
    }

    public String toString() {
        return this.name;
    }
}
