package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.utils.Array;

public class IkConstraintData {
    int bendDirection = 1;
    final Array<BoneData> bones = new Array<>();
    float mix = 1.0f;
    final String name;
    BoneData target;

    public IkConstraintData(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public Array<BoneData> getBones() {
        return this.bones;
    }

    public BoneData getTarget() {
        return this.target;
    }

    public void setTarget(BoneData target2) {
        this.target = target2;
    }

    public int getBendDirection() {
        return this.bendDirection;
    }

    public void setBendDirection(int bendDirection2) {
        this.bendDirection = bendDirection2;
    }

    public float getMix() {
        return this.mix;
    }

    public void setMix(float mix2) {
        this.mix = mix2;
    }

    public String toString() {
        return this.name;
    }
}
