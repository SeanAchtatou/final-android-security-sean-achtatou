package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.kbz.esotericsoftware.spine.attachments.Attachment;
import com.kbz.esotericsoftware.spine.attachments.MeshAttachment;
import com.kbz.esotericsoftware.spine.attachments.RegionAttachment;
import com.kbz.esotericsoftware.spine.attachments.SkinnedMeshAttachment;
import java.util.Iterator;

public class Skeleton {
    final Array<Bone> bones;
    final Color color;
    final SkeletonData data;
    Array<Slot> drawOrder;
    boolean flipX;
    boolean flipY;
    final Array<IkConstraint> ikConstraints;
    Skin skin;
    final Array<Slot> slots;
    float time;
    final Array<TransformConstraint> transformConstraints;
    private final Array<Updatable> updateCache = new Array<>();
    float x;
    float y;

    public Skeleton(SkeletonData data2) {
        if (data2 == null) {
            throw new IllegalArgumentException("data cannot be null.");
        }
        this.data = data2;
        this.bones = new Array<>(data2.bones.size);
        Iterator<BoneData> it = data2.bones.iterator();
        while (it.hasNext()) {
            BoneData boneData = it.next();
            this.bones.add(new Bone(boneData, this, boneData.parent == null ? null : this.bones.get(data2.bones.indexOf(boneData.parent, true))));
        }
        this.slots = new Array<>(data2.slots.size);
        this.drawOrder = new Array<>(data2.slots.size);
        Iterator<SlotData> it2 = data2.slots.iterator();
        while (it2.hasNext()) {
            SlotData slotData = it2.next();
            Slot slot = new Slot(slotData, this.bones.get(data2.bones.indexOf(slotData.boneData, true)));
            this.slots.add(slot);
            this.drawOrder.add(slot);
        }
        this.ikConstraints = new Array<>(data2.ikConstraints.size);
        Iterator<IkConstraintData> it3 = data2.ikConstraints.iterator();
        while (it3.hasNext()) {
            this.ikConstraints.add(new IkConstraint(it3.next(), this));
        }
        this.transformConstraints = new Array<>(data2.transformConstraints.size);
        Iterator<TransformConstraintData> it4 = data2.transformConstraints.iterator();
        while (it4.hasNext()) {
            this.transformConstraints.add(new TransformConstraint(it4.next(), this));
        }
        this.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        updateCache();
    }

    public Skeleton(Skeleton skeleton) {
        if (skeleton == null) {
            throw new IllegalArgumentException("skeleton cannot be null.");
        }
        this.data = skeleton.data;
        this.bones = new Array<>(skeleton.bones.size);
        Iterator<Bone> it = skeleton.bones.iterator();
        while (it.hasNext()) {
            Bone bone = it.next();
            this.bones.add(new Bone(bone, this, bone.parent == null ? null : this.bones.get(skeleton.bones.indexOf(bone.parent, true))));
        }
        this.slots = new Array<>(skeleton.slots.size);
        Iterator<Slot> it2 = skeleton.slots.iterator();
        while (it2.hasNext()) {
            Slot slot = it2.next();
            this.slots.add(new Slot(slot, this.bones.get(skeleton.bones.indexOf(slot.bone, true))));
        }
        this.drawOrder = new Array<>(this.slots.size);
        Iterator<Slot> it3 = skeleton.drawOrder.iterator();
        while (it3.hasNext()) {
            this.drawOrder.add(this.slots.get(skeleton.slots.indexOf(it3.next(), true)));
        }
        this.ikConstraints = new Array<>(skeleton.ikConstraints.size);
        Iterator<IkConstraint> it4 = skeleton.ikConstraints.iterator();
        while (it4.hasNext()) {
            this.ikConstraints.add(new IkConstraint(it4.next(), this));
        }
        this.transformConstraints = new Array<>(skeleton.transformConstraints.size);
        Iterator<TransformConstraint> it5 = skeleton.transformConstraints.iterator();
        while (it5.hasNext()) {
            this.transformConstraints.add(new TransformConstraint(it5.next(), this));
        }
        this.skin = skeleton.skin;
        this.color = new Color(skeleton.color);
        this.time = skeleton.time;
        this.flipX = skeleton.flipX;
        this.flipY = skeleton.flipY;
        updateCache();
    }

    public void updateCache() {
        Array<Bone> bones2 = this.bones;
        Array<Updatable> updateCache2 = this.updateCache;
        Array<IkConstraint> ikConstraints2 = this.ikConstraints;
        Array<TransformConstraint> transformConstraints2 = this.transformConstraints;
        int ikConstraintsCount = ikConstraints2.size;
        int transformConstraintsCount = transformConstraints2.size;
        updateCache2.clear();
        int n = bones2.size;
        for (int i = 0; i < n; i++) {
            Bone bone = bones2.get(i);
            updateCache2.add(bone);
            int ii = 0;
            while (true) {
                if (ii >= transformConstraintsCount) {
                    break;
                }
                TransformConstraint transformConstraint = transformConstraints2.get(ii);
                if (bone == transformConstraint.bone) {
                    updateCache2.add(transformConstraint);
                    break;
                }
                ii++;
            }
            int ii2 = 0;
            while (true) {
                if (ii2 >= ikConstraintsCount) {
                    break;
                }
                IkConstraint ikConstraint = ikConstraints2.get(ii2);
                if (bone == ikConstraint.bones.peek()) {
                    updateCache2.add(ikConstraint);
                    break;
                }
                ii2++;
            }
        }
    }

    public void updateWorldTransform() {
        Array<Updatable> updateCache2 = this.updateCache;
        int n = updateCache2.size;
        for (int i = 0; i < n; i++) {
            updateCache2.get(i).update();
        }
    }

    public void setToSetupPose() {
        setBonesToSetupPose();
        setSlotsToSetupPose();
    }

    public void setBonesToSetupPose() {
        Array<Bone> bones2 = this.bones;
        int n = bones2.size;
        for (int i = 0; i < n; i++) {
            bones2.get(i).setToSetupPose();
        }
        Array<IkConstraint> ikConstraints2 = this.ikConstraints;
        int n2 = ikConstraints2.size;
        for (int i2 = 0; i2 < n2; i2++) {
            IkConstraint constraint = ikConstraints2.get(i2);
            constraint.bendDirection = constraint.data.bendDirection;
            constraint.mix = constraint.data.mix;
        }
        Array<TransformConstraint> transformConstraints2 = this.transformConstraints;
        int n3 = transformConstraints2.size;
        for (int i3 = 0; i3 < n3; i3++) {
            TransformConstraint constraint2 = transformConstraints2.get(i3);
            constraint2.translateMix = constraint2.data.translateMix;
            constraint2.x = constraint2.data.x;
            constraint2.y = constraint2.data.y;
        }
    }

    public void setSlotsToSetupPose() {
        Array<Slot> slots2 = this.slots;
        System.arraycopy(slots2.items, 0, this.drawOrder.items, 0, slots2.size);
        int n = slots2.size;
        for (int i = 0; i < n; i++) {
            slots2.get(i).setToSetupPose(i);
        }
    }

    public SkeletonData getData() {
        return this.data;
    }

    public Array<Bone> getBones() {
        return this.bones;
    }

    public Bone getRootBone() {
        if (this.bones.size == 0) {
            return null;
        }
        return this.bones.first();
    }

    public Bone findBone(String boneName) {
        if (boneName == null) {
            throw new IllegalArgumentException("boneName cannot be null.");
        }
        Array<Bone> bones2 = this.bones;
        int n = bones2.size;
        for (int i = 0; i < n; i++) {
            Bone bone = bones2.get(i);
            if (bone.data.name.equals(boneName)) {
                return bone;
            }
        }
        return null;
    }

    public int findBoneIndex(String boneName) {
        if (boneName == null) {
            throw new IllegalArgumentException("boneName cannot be null.");
        }
        Array<Bone> bones2 = this.bones;
        int n = bones2.size;
        for (int i = 0; i < n; i++) {
            if (bones2.get(i).data.name.equals(boneName)) {
                return i;
            }
        }
        return -1;
    }

    public Array<Slot> getSlots() {
        return this.slots;
    }

    public Slot findSlot(String slotName) {
        if (slotName == null) {
            throw new IllegalArgumentException("slotName cannot be null.");
        }
        Array<Slot> slots2 = this.slots;
        int n = slots2.size;
        for (int i = 0; i < n; i++) {
            Slot slot = slots2.get(i);
            if (slot.data.name.equals(slotName)) {
                return slot;
            }
        }
        return null;
    }

    public int findSlotIndex(String slotName) {
        if (slotName == null) {
            throw new IllegalArgumentException("slotName cannot be null.");
        }
        Array<Slot> slots2 = this.slots;
        int n = slots2.size;
        for (int i = 0; i < n; i++) {
            if (slots2.get(i).data.name.equals(slotName)) {
                return i;
            }
        }
        return -1;
    }

    public Array<Slot> getDrawOrder() {
        return this.drawOrder;
    }

    public void setDrawOrder(Array<Slot> drawOrder2) {
        this.drawOrder = drawOrder2;
    }

    public Skin getSkin() {
        return this.skin;
    }

    public void setSkin(String skinName) {
        Skin skin2 = this.data.findSkin(skinName);
        if (skin2 == null) {
            throw new IllegalArgumentException("Skin not found: " + skinName);
        }
        setSkin(skin2);
    }

    public void setSkin(Skin newSkin) {
        Attachment attachment;
        if (newSkin != null) {
            if (this.skin != null) {
                newSkin.attachAll(this, this.skin);
            } else {
                Array<Slot> slots2 = this.slots;
                int n = slots2.size;
                for (int i = 0; i < n; i++) {
                    Slot slot = slots2.get(i);
                    String name = slot.data.attachmentName;
                    if (!(name == null || (attachment = newSkin.getAttachment(i, name)) == null)) {
                        slot.setAttachment(attachment);
                    }
                }
            }
        }
        this.skin = newSkin;
    }

    public Attachment getAttachment(String slotName, String attachmentName) {
        return getAttachment(this.data.findSlotIndex(slotName), attachmentName);
    }

    public Attachment getAttachment(int slotIndex, String attachmentName) {
        Attachment attachment;
        if (attachmentName == null) {
            throw new IllegalArgumentException("attachmentName cannot be null.");
        } else if (this.skin != null && (attachment = this.skin.getAttachment(slotIndex, attachmentName)) != null) {
            return attachment;
        } else {
            if (this.data.defaultSkin != null) {
                return this.data.defaultSkin.getAttachment(slotIndex, attachmentName);
            }
            return null;
        }
    }

    public void setAttachment(String slotName, String attachmentName) {
        if (slotName == null) {
            throw new IllegalArgumentException("slotName cannot be null.");
        }
        Array<Slot> slots2 = this.slots;
        int n = slots2.size;
        for (int i = 0; i < n; i++) {
            Slot slot = slots2.get(i);
            if (slot.data.name.equals(slotName)) {
                Attachment attachment = null;
                if (attachmentName == null || (attachment = getAttachment(i, attachmentName)) != null) {
                    slot.setAttachment(attachment);
                    return;
                }
                throw new IllegalArgumentException("Attachment not found: " + attachmentName + ", for slot: " + slotName);
            }
        }
        throw new IllegalArgumentException("Slot not found: " + slotName);
    }

    public Array<IkConstraint> getIkConstraints() {
        return this.ikConstraints;
    }

    public IkConstraint findIkConstraint(String constraintName) {
        if (constraintName == null) {
            throw new IllegalArgumentException("constraintName cannot be null.");
        }
        Array<IkConstraint> ikConstraints2 = this.ikConstraints;
        int n = ikConstraints2.size;
        for (int i = 0; i < n; i++) {
            IkConstraint ikConstraint = ikConstraints2.get(i);
            if (ikConstraint.data.name.equals(constraintName)) {
                return ikConstraint;
            }
        }
        return null;
    }

    public Array<TransformConstraint> getTransformConstraints() {
        return this.transformConstraints;
    }

    public TransformConstraint findTransformConstraint(String constraintName) {
        if (constraintName == null) {
            throw new IllegalArgumentException("constraintName cannot be null.");
        }
        Array<TransformConstraint> transformConstraints2 = this.transformConstraints;
        int n = transformConstraints2.size;
        for (int i = 0; i < n; i++) {
            TransformConstraint constraint = transformConstraints2.get(i);
            if (constraint.data.name.equals(constraintName)) {
                return constraint;
            }
        }
        return null;
    }

    public void getBounds(Vector2 offset, Vector2 size) {
        Array<Slot> drawOrder2 = this.drawOrder;
        float minX = 2.14748365E9f;
        float minY = 2.14748365E9f;
        float maxX = -2.14748365E9f;
        float maxY = -2.14748365E9f;
        int n = drawOrder2.size;
        for (int i = 0; i < n; i++) {
            Slot slot = drawOrder2.get(i);
            float[] vertices = null;
            Attachment attachment = slot.attachment;
            if (attachment instanceof RegionAttachment) {
                vertices = ((RegionAttachment) attachment).updateWorldVertices(slot, false);
            } else if (attachment instanceof MeshAttachment) {
                vertices = ((MeshAttachment) attachment).updateWorldVertices(slot, true);
            } else if (attachment instanceof SkinnedMeshAttachment) {
                vertices = ((SkinnedMeshAttachment) attachment).updateWorldVertices(slot, true);
            }
            if (vertices != null) {
                int nn = vertices.length;
                for (int ii = 0; ii < nn; ii += 5) {
                    float x2 = vertices[ii];
                    float y2 = vertices[ii + 1];
                    minX = Math.min(minX, x2);
                    minY = Math.min(minY, y2);
                    maxX = Math.max(maxX, x2);
                    maxY = Math.max(maxY, y2);
                }
            }
        }
        offset.set(minX, minY);
        size.set(maxX - minX, maxY - minY);
    }

    public Color getColor() {
        return this.color;
    }

    public void setColor(Color color2) {
        this.color.set(color2);
    }

    public boolean getFlipX() {
        return this.flipX;
    }

    public void setFlipX(boolean flipX2) {
        this.flipX = flipX2;
    }

    public boolean getFlipY() {
        return this.flipY;
    }

    public void setFlipY(boolean flipY2) {
        this.flipY = flipY2;
    }

    public void setFlip(boolean flipX2, boolean flipY2) {
        this.flipX = flipX2;
        this.flipY = flipY2;
    }

    public float getX() {
        return this.x;
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public float getY() {
        return this.y;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public void setPosition(float x2, float y2) {
        this.x = x2;
        this.y = y2;
    }

    public float getTime() {
        return this.time;
    }

    public void setTime(float time2) {
        this.time = time2;
    }

    public void update(float delta) {
        this.time += delta;
    }

    public String toString() {
        return this.data.name != null ? this.data.name : super.toString();
    }
}
