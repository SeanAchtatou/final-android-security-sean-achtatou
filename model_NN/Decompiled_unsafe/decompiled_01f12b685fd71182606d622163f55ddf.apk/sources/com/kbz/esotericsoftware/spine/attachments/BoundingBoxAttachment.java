package com.kbz.esotericsoftware.spine.attachments;

import com.kbz.esotericsoftware.spine.Bone;
import com.kbz.esotericsoftware.spine.Skeleton;

public class BoundingBoxAttachment extends Attachment {
    private float[] vertices;

    public BoundingBoxAttachment(String name) {
        super(name);
    }

    public void computeWorldVertices(Bone bone, float[] worldVertices) {
        Skeleton skeleton = bone.getSkeleton();
        float x = skeleton.getX() + bone.getWorldX();
        float y = skeleton.getY() + bone.getWorldY();
        float m00 = bone.getA();
        float m01 = bone.getB();
        float m10 = bone.getC();
        float m11 = bone.getD();
        float[] vertices2 = this.vertices;
        int n = vertices2.length;
        for (int i = 0; i < n; i += 2) {
            float px = vertices2[i];
            float py = vertices2[i + 1];
            worldVertices[i] = (px * m00) + (py * m01) + x;
            worldVertices[i + 1] = (px * m10) + (py * m11) + y;
        }
    }

    public float[] getVertices() {
        return this.vertices;
    }

    public void setVertices(float[] vertices2) {
        this.vertices = vertices2;
    }
}
