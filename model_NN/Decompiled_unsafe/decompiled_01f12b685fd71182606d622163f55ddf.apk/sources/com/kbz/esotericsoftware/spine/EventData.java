package com.kbz.esotericsoftware.spine;

public class EventData {
    float floatValue;
    int intValue;
    final String name;
    String stringValue;

    public EventData(String name2) {
        if (name2 == null) {
            throw new IllegalArgumentException("name cannot be null.");
        }
        this.name = name2;
    }

    public int getInt() {
        return this.intValue;
    }

    public void setInt(int intValue2) {
        this.intValue = intValue2;
    }

    public float getFloat() {
        return this.floatValue;
    }

    public void setFloat(float floatValue2) {
        this.floatValue = floatValue2;
    }

    public String getString() {
        return this.stringValue;
    }

    public void setString(String stringValue2) {
        this.stringValue = stringValue2;
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        return this.name;
    }
}
