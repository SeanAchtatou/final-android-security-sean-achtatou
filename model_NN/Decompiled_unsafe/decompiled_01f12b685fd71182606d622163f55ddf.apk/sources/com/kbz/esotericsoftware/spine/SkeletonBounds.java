package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.Pool;
import com.kbz.esotericsoftware.spine.attachments.Attachment;
import com.kbz.esotericsoftware.spine.attachments.BoundingBoxAttachment;

public class SkeletonBounds {
    private Array<BoundingBoxAttachment> boundingBoxes = new Array<>();
    private float maxX;
    private float maxY;
    private float minX;
    private float minY;
    private Pool<FloatArray> polygonPool = new Pool() {
        /* access modifiers changed from: protected */
        public Object newObject() {
            return new FloatArray();
        }
    };
    private Array<FloatArray> polygons = new Array<>();

    public void update(Skeleton skeleton, boolean updateAabb) {
        Array<BoundingBoxAttachment> boundingBoxes2 = this.boundingBoxes;
        Array<FloatArray> polygons2 = this.polygons;
        Array<Slot> slots = skeleton.slots;
        int slotCount = slots.size;
        boundingBoxes2.clear();
        this.polygonPool.freeAll(polygons2);
        polygons2.clear();
        for (int i = 0; i < slotCount; i++) {
            Slot slot = slots.get(i);
            Attachment attachment = slot.attachment;
            if (attachment instanceof BoundingBoxAttachment) {
                BoundingBoxAttachment boundingBox = (BoundingBoxAttachment) attachment;
                boundingBoxes2.add(boundingBox);
                FloatArray polygon = this.polygonPool.obtain();
                polygons2.add(polygon);
                int vertexCount = boundingBox.getVertices().length;
                polygon.ensureCapacity(vertexCount);
                polygon.size = vertexCount;
                boundingBox.computeWorldVertices(slot.bone, polygon.items);
            }
        }
        if (updateAabb) {
            aabbCompute();
        }
    }

    private void aabbCompute() {
        float minX2 = 2.14748365E9f;
        float minY2 = 2.14748365E9f;
        float maxX2 = -2.14748365E9f;
        float maxY2 = -2.14748365E9f;
        Array<FloatArray> polygons2 = this.polygons;
        int n = polygons2.size;
        for (int i = 0; i < n; i++) {
            FloatArray polygon = polygons2.get(i);
            float[] vertices = polygon.items;
            int nn = polygon.size;
            for (int ii = 0; ii < nn; ii += 2) {
                float x = vertices[ii];
                float y = vertices[ii + 1];
                minX2 = Math.min(minX2, x);
                minY2 = Math.min(minY2, y);
                maxX2 = Math.max(maxX2, x);
                maxY2 = Math.max(maxY2, y);
            }
        }
        this.minX = minX2;
        this.minY = minY2;
        this.maxX = maxX2;
        this.maxY = maxY2;
    }

    public boolean aabbContainsPoint(float x, float y) {
        return x >= this.minX && x <= this.maxX && y >= this.minY && y <= this.maxY;
    }

    public boolean aabbIntersectsSegment(float x1, float y1, float x2, float y2) {
        float minX2 = this.minX;
        float minY2 = this.minY;
        float maxX2 = this.maxX;
        float maxY2 = this.maxY;
        if (x1 <= minX2 && x2 <= minX2) {
            return false;
        }
        if (y1 <= minY2 && y2 <= minY2) {
            return false;
        }
        if (x1 >= maxX2 && x2 >= maxX2) {
            return false;
        }
        if (y1 >= maxY2 && y2 >= maxY2) {
            return false;
        }
        float m = (y2 - y1) / (x2 - x1);
        float y = ((minX2 - x1) * m) + y1;
        if (y > minY2 && y < maxY2) {
            return true;
        }
        float y3 = ((maxX2 - x1) * m) + y1;
        if (y3 > minY2 && y3 < maxY2) {
            return true;
        }
        float x = ((minY2 - y1) / m) + x1;
        if (x > minX2 && x < maxX2) {
            return true;
        }
        float x3 = ((maxY2 - y1) / m) + x1;
        if (x3 <= minX2 || x3 >= maxX2) {
            return false;
        }
        return true;
    }

    public boolean aabbIntersectsSkeleton(SkeletonBounds bounds) {
        return this.minX < bounds.maxX && this.maxX > bounds.minX && this.minY < bounds.maxY && this.maxY > bounds.minY;
    }

    public BoundingBoxAttachment containsPoint(float x, float y) {
        Array<FloatArray> polygons2 = this.polygons;
        int n = polygons2.size;
        for (int i = 0; i < n; i++) {
            if (containsPoint(polygons2.get(i), x, y)) {
                return this.boundingBoxes.get(i);
            }
        }
        return null;
    }

    public boolean containsPoint(FloatArray polygon, float x, float y) {
        float[] vertices = polygon.items;
        int nn = polygon.size;
        int prevIndex = nn - 2;
        boolean inside = false;
        for (int ii = 0; ii < nn; ii += 2) {
            float vertexY = vertices[ii + 1];
            float prevY = vertices[prevIndex + 1];
            if ((vertexY < y && prevY >= y) || (prevY < y && vertexY >= y)) {
                float vertexX = vertices[ii];
                if ((((y - vertexY) / (prevY - vertexY)) * (vertices[prevIndex] - vertexX)) + vertexX < x) {
                    inside = !inside;
                }
            }
            prevIndex = ii;
        }
        return inside;
    }

    public BoundingBoxAttachment intersectsSegment(float x1, float y1, float x2, float y2) {
        Array<FloatArray> polygons2 = this.polygons;
        int n = polygons2.size;
        for (int i = 0; i < n; i++) {
            if (intersectsSegment(polygons2.get(i), x1, y1, x2, y2)) {
                return this.boundingBoxes.get(i);
            }
        }
        return null;
    }

    public boolean intersectsSegment(FloatArray polygon, float x1, float y1, float x2, float y2) {
        float[] vertices = polygon.items;
        int nn = polygon.size;
        float width12 = x1 - x2;
        float height12 = y1 - y2;
        float det1 = (x1 * y2) - (y1 * x2);
        float x3 = vertices[nn - 2];
        float y3 = vertices[nn - 1];
        for (int ii = 0; ii < nn; ii += 2) {
            float x4 = vertices[ii];
            float y4 = vertices[ii + 1];
            float det2 = (x3 * y4) - (y3 * x4);
            float width34 = x3 - x4;
            float height34 = y3 - y4;
            float det3 = (width12 * height34) - (height12 * width34);
            float x = ((det1 * width34) - (width12 * det2)) / det3;
            if (((x >= x3 && x <= x4) || (x >= x4 && x <= x3)) && ((x >= x1 && x <= x2) || (x >= x2 && x <= x1))) {
                float y = ((det1 * height34) - (height12 * det2)) / det3;
                if (((y >= y3 && y <= y4) || (y >= y4 && y <= y3)) && ((y >= y1 && y <= y2) || (y >= y2 && y <= y1))) {
                    return true;
                }
            }
            x3 = x4;
            y3 = y4;
        }
        return false;
    }

    public float getMinX() {
        return this.minX;
    }

    public float getMinY() {
        return this.minY;
    }

    public float getMaxX() {
        return this.maxX;
    }

    public float getMaxY() {
        return this.maxY;
    }

    public float getWidth() {
        return this.maxX - this.minX;
    }

    public float getHeight() {
        return this.maxY - this.minY;
    }

    public Array<BoundingBoxAttachment> getBoundingBoxes() {
        return this.boundingBoxes;
    }

    public Array<FloatArray> getPolygons() {
        return this.polygons;
    }

    public FloatArray getPolygon(BoundingBoxAttachment boundingBox) {
        int index = this.boundingBoxes.indexOf(boundingBox, true);
        if (index == -1) {
            return null;
        }
        return this.polygons.get(index);
    }
}
