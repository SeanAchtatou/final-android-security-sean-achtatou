package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.esotericsoftware.spine.attachments.Attachment;
import com.kbz.esotericsoftware.spine.attachments.MeshAttachment;
import com.kbz.esotericsoftware.spine.attachments.RegionAttachment;
import com.kbz.esotericsoftware.spine.attachments.SkinnedMeshAttachment;

public class SkeletonRendererDebug {
    private static final Color aabbColor = new Color(Animation.CurveTimeline.LINEAR, 1.0f, Animation.CurveTimeline.LINEAR, 0.5f);
    private static final Color attachmentLineColor = new Color(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, 1.0f, 0.5f);
    private static final Color boneLineColor = Color.RED;
    private static final Color boneOriginColor = Color.GREEN;
    private static final Color boundingBoxColor = new Color(Animation.CurveTimeline.LINEAR, 1.0f, Animation.CurveTimeline.LINEAR, 0.8f);
    private static final Color triangleLineColor = new Color(1.0f, 0.64f, Animation.CurveTimeline.LINEAR, 0.5f);
    private float boneWidth;
    private final SkeletonBounds bounds;
    private boolean drawBones;
    private boolean drawBoundingBoxes;
    private boolean drawMeshHull;
    private boolean drawMeshTriangles;
    private boolean drawRegionAttachments;
    private boolean premultipliedAlpha;
    private float scale;
    private final ShapeRenderer shapes;

    public SkeletonRendererDebug() {
        this.drawBones = true;
        this.drawRegionAttachments = true;
        this.drawBoundingBoxes = true;
        this.drawMeshHull = true;
        this.drawMeshTriangles = true;
        this.bounds = new SkeletonBounds();
        this.scale = 1.0f;
        this.boneWidth = 2.0f;
        this.shapes = new ShapeRenderer();
    }

    public SkeletonRendererDebug(ShapeRenderer shapes2) {
        this.drawBones = true;
        this.drawRegionAttachments = true;
        this.drawBoundingBoxes = true;
        this.drawMeshHull = true;
        this.drawMeshTriangles = true;
        this.bounds = new SkeletonBounds();
        this.scale = 1.0f;
        this.boneWidth = 2.0f;
        this.shapes = shapes2;
    }

    public void draw(Skeleton skeleton) {
        float skeletonX = skeleton.getX();
        float skeletonY = skeleton.getY();
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(this.premultipliedAlpha ? 1 : 770, 771);
        ShapeRenderer shapes2 = this.shapes;
        Array<Bone> bones = skeleton.getBones();
        if (this.drawBones) {
            shapes2.setColor(boneLineColor);
            shapes2.begin(ShapeRenderer.ShapeType.Filled);
            int n = bones.size;
            for (int i = 0; i < n; i++) {
                Bone bone = bones.get(i);
                if (bone.parent != null) {
                    shapes2.rectLine(bone.worldX + skeletonX, bone.worldY + skeletonY, (bone.data.length * bone.a) + skeletonX + bone.worldX, (bone.data.length * bone.c) + skeletonY + bone.worldY, this.boneWidth * this.scale);
                }
            }
            shapes2.end();
            shapes2.begin(ShapeRenderer.ShapeType.Line);
            shapes2.x(skeletonX, skeletonY, 4.0f * this.scale);
        } else {
            shapes2.begin(ShapeRenderer.ShapeType.Line);
        }
        if (this.drawRegionAttachments) {
            shapes2.setColor(attachmentLineColor);
            Array<Slot> slots = skeleton.getSlots();
            int n2 = slots.size;
            for (int i2 = 0; i2 < n2; i2++) {
                Slot slot = slots.get(i2);
                Attachment attachment = slot.attachment;
                if (attachment instanceof RegionAttachment) {
                    float[] vertices = ((RegionAttachment) attachment).updateWorldVertices(slot, false);
                    shapes2.line(vertices[0], vertices[1], vertices[5], vertices[6]);
                    shapes2.line(vertices[5], vertices[6], vertices[10], vertices[11]);
                    shapes2.line(vertices[10], vertices[11], vertices[15], vertices[16]);
                    shapes2.line(vertices[15], vertices[16], vertices[0], vertices[1]);
                }
            }
        }
        if (this.drawMeshHull || this.drawMeshTriangles) {
            Array<Slot> slots2 = skeleton.getSlots();
            int n3 = slots2.size;
            for (int i3 = 0; i3 < n3; i3++) {
                Slot slot2 = slots2.get(i3);
                Attachment attachment2 = slot2.attachment;
                float[] vertices2 = null;
                short[] triangles = null;
                int hullLength = 0;
                if (attachment2 instanceof MeshAttachment) {
                    MeshAttachment mesh = (MeshAttachment) attachment2;
                    mesh.updateWorldVertices(slot2, false);
                    vertices2 = mesh.getWorldVertices();
                    triangles = mesh.getTriangles();
                    hullLength = mesh.getHullLength();
                } else if (attachment2 instanceof SkinnedMeshAttachment) {
                    SkinnedMeshAttachment mesh2 = (SkinnedMeshAttachment) attachment2;
                    mesh2.updateWorldVertices(slot2, false);
                    vertices2 = mesh2.getWorldVertices();
                    triangles = mesh2.getTriangles();
                    hullLength = mesh2.getHullLength();
                }
                if (!(vertices2 == null || triangles == null)) {
                    if (this.drawMeshTriangles) {
                        shapes2.setColor(triangleLineColor);
                        int nn = triangles.length;
                        for (int ii = 0; ii < nn; ii += 3) {
                            int v1 = triangles[ii] * 5;
                            int v2 = triangles[ii + 1] * 5;
                            int v3 = triangles[ii + 2] * 5;
                            shapes2.triangle(vertices2[v1], vertices2[v1 + 1], vertices2[v2], vertices2[v2 + 1], vertices2[v3], vertices2[v3 + 1]);
                        }
                    }
                    if (this.drawMeshHull && hullLength > 0) {
                        shapes2.setColor(attachmentLineColor);
                        int hullLength2 = (hullLength / 2) * 5;
                        float lastX = vertices2[hullLength2 - 5];
                        float lastY = vertices2[hullLength2 - 4];
                        int nn2 = hullLength2;
                        for (int ii2 = 0; ii2 < nn2; ii2 += 5) {
                            float x = vertices2[ii2];
                            float y = vertices2[ii2 + 1];
                            shapes2.line(x, y, lastX, lastY);
                            lastX = x;
                            lastY = y;
                        }
                    }
                }
            }
        }
        if (this.drawBoundingBoxes) {
            SkeletonBounds bounds2 = this.bounds;
            bounds2.update(skeleton, true);
            shapes2.setColor(aabbColor);
            shapes2.rect(bounds2.getMinX(), bounds2.getMinY(), bounds2.getWidth(), bounds2.getHeight());
            shapes2.setColor(boundingBoxColor);
            Array<FloatArray> polygons = bounds2.getPolygons();
            int n4 = polygons.size;
            for (int i4 = 0; i4 < n4; i4++) {
                FloatArray polygon = polygons.get(i4);
                shapes2.polygon(polygon.items, 0, polygon.size);
            }
        }
        shapes2.end();
        shapes2.begin(ShapeRenderer.ShapeType.Filled);
        if (this.drawBones) {
            shapes2.setColor(boneOriginColor);
            int n5 = bones.size;
            for (int i5 = 0; i5 < n5; i5++) {
                Bone bone2 = bones.get(i5);
                shapes2.setColor(Color.GREEN);
                shapes2.circle(bone2.worldX + skeletonX, bone2.worldY + skeletonY, 3.0f * this.scale, 8);
            }
        }
        shapes2.end();
    }

    public ShapeRenderer getShapeRenderer() {
        return this.shapes;
    }

    public void setBones(boolean bones) {
        this.drawBones = bones;
    }

    public void setScale(float scale2) {
        this.scale = scale2;
    }

    public void setRegionAttachments(boolean regionAttachments) {
        this.drawRegionAttachments = regionAttachments;
    }

    public void setBoundingBoxes(boolean boundingBoxes) {
        this.drawBoundingBoxes = boundingBoxes;
    }

    public void setMeshHull(boolean meshHull) {
        this.drawMeshHull = meshHull;
    }

    public void setMeshTriangles(boolean meshTriangles) {
        this.drawMeshTriangles = meshTriangles;
    }

    public void setPremultipliedAlpha(boolean premultipliedAlpha2) {
        this.premultipliedAlpha = premultipliedAlpha2;
    }
}
