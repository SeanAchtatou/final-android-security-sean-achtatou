package com.kbz.esotericsoftware.spine.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.kbz.esotericsoftware.spine.AnimationState;
import com.kbz.esotericsoftware.spine.AnimationStateData;
import com.kbz.esotericsoftware.spine.Skeleton;
import com.kbz.esotericsoftware.spine.SkeletonData;
import com.kbz.esotericsoftware.spine.SkeletonRenderer;
import com.kbz.esotericsoftware.spine.Skin;

public class SkeletonActorPool extends Pool<SkeletonActor> {
    private final Array<SkeletonActor> obtained;
    private SkeletonRenderer renderer;
    SkeletonData skeletonData;
    private final Pool<Skeleton> skeletonPool;
    AnimationStateData stateData;
    private final Pool<AnimationState> statePool;

    public SkeletonActorPool(SkeletonRenderer renderer2, SkeletonData skeletonData2, AnimationStateData stateData2) {
        this(renderer2, skeletonData2, stateData2, 16, Integer.MAX_VALUE);
    }

    public SkeletonActorPool(SkeletonRenderer renderer2, SkeletonData skeletonData2, AnimationStateData stateData2, int initialCapacity, int max) {
        super(initialCapacity, max);
        this.renderer = renderer2;
        this.skeletonData = skeletonData2;
        this.stateData = stateData2;
        this.obtained = new Array<>(false, initialCapacity);
        this.skeletonPool = new Pool<Skeleton>(initialCapacity, max) {
            /* access modifiers changed from: protected */
            public Skeleton newObject() {
                return new Skeleton(SkeletonActorPool.this.skeletonData);
            }

            /* access modifiers changed from: protected */
            public void reset(Skeleton skeleton) {
                skeleton.setColor(Color.WHITE);
                skeleton.setFlip(false, false);
                skeleton.setSkin((Skin) null);
                skeleton.setSkin(SkeletonActorPool.this.skeletonData.getDefaultSkin());
                skeleton.setToSetupPose();
            }
        };
        this.statePool = new Pool<AnimationState>(initialCapacity, max) {
            /* access modifiers changed from: protected */
            public AnimationState newObject() {
                return new AnimationState(SkeletonActorPool.this.stateData);
            }

            /* access modifiers changed from: protected */
            public void reset(AnimationState state) {
                state.clearTracks();
                state.clearListeners();
            }
        };
    }

    public void freeComplete() {
        Array<SkeletonActor> obtained2 = this.obtained;
        for (int i = obtained2.size - 1; i >= 0; i--) {
            SkeletonActor actor = obtained2.get(i);
            Array<AnimationState.TrackEntry> tracks = actor.state.getTracks();
            int ii = 0;
            int nn = tracks.size;
            while (true) {
                if (ii >= nn) {
                    free(actor);
                    break;
                } else if (tracks.get(ii) != null) {
                    break;
                } else {
                    ii++;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public SkeletonActor newObject() {
        SkeletonActor actor = new SkeletonActor();
        actor.setRenderer(this.renderer);
        return actor;
    }

    public SkeletonActor obtain() {
        SkeletonActor actor = (SkeletonActor) super.obtain();
        actor.setSkeleton(this.skeletonPool.obtain());
        actor.setAnimationState(this.statePool.obtain());
        this.obtained.add(actor);
        return actor;
    }

    /* access modifiers changed from: protected */
    public void reset(SkeletonActor actor) {
        actor.remove();
        this.obtained.removeValue(actor, true);
        this.skeletonPool.free(actor.getSkeleton());
        this.statePool.free(actor.getAnimationState());
    }

    public Array<SkeletonActor> getObtained() {
        return this.obtained;
    }
}
