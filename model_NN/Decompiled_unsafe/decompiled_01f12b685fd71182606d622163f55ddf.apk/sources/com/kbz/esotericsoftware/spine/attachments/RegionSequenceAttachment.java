package com.kbz.esotericsoftware.spine.attachments;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.kbz.esotericsoftware.spine.Slot;

public class RegionSequenceAttachment extends RegionAttachment {
    private float frameTime;
    private Mode mode;
    private TextureRegion[] regions;

    public enum Mode {
        forward,
        backward,
        forwardLoop,
        backwardLoop,
        pingPong,
        random
    }

    public RegionSequenceAttachment(String name) {
        super(name);
    }

    public float[] updateWorldVertices(Slot slot, boolean premultipliedAlpha) {
        if (this.regions == null) {
            throw new IllegalStateException("Regions have not been set: " + this);
        }
        int frameIndex = (int) (slot.getAttachmentTime() / this.frameTime);
        switch (this.mode) {
            case forward:
                frameIndex = Math.min(this.regions.length - 1, frameIndex);
                break;
            case forwardLoop:
                frameIndex %= this.regions.length;
                break;
            case pingPong:
                frameIndex %= this.regions.length * 2;
                if (frameIndex >= this.regions.length) {
                    frameIndex = (this.regions.length - 1) - (frameIndex - this.regions.length);
                    break;
                }
                break;
            case random:
                frameIndex = MathUtils.random(this.regions.length - 1);
                break;
            case backward:
                frameIndex = Math.max((this.regions.length - frameIndex) - 1, 0);
                break;
            case backwardLoop:
                frameIndex = (this.regions.length - (frameIndex % this.regions.length)) - 1;
                break;
        }
        setRegion(this.regions[frameIndex]);
        return super.updateWorldVertices(slot, premultipliedAlpha);
    }

    public TextureRegion[] getRegions() {
        if (this.regions != null) {
            return this.regions;
        }
        throw new IllegalStateException("Regions have not been set: " + this);
    }

    public void setRegions(TextureRegion[] regions2) {
        this.regions = regions2;
    }

    public void setFrameTime(float frameTime2) {
        this.frameTime = frameTime2;
    }

    public void setMode(Mode mode2) {
        this.mode = mode2;
    }
}
