package com.kbz.esotericsoftware.spine.utils;

import com.badlogic.gdx.utils.Pool;
import com.kbz.esotericsoftware.spine.Skeleton;
import com.kbz.esotericsoftware.spine.SkeletonData;

public class SkeletonPool extends Pool<Skeleton> {
    private SkeletonData skeletonData;

    public SkeletonPool(SkeletonData skeletonData2) {
        this.skeletonData = skeletonData2;
    }

    public SkeletonPool(SkeletonData skeletonData2, int initialCapacity) {
        super(initialCapacity);
        this.skeletonData = skeletonData2;
    }

    public SkeletonPool(SkeletonData skeletonData2, int initialCapacity, int max) {
        super(initialCapacity, max);
        this.skeletonData = skeletonData2;
    }

    /* access modifiers changed from: protected */
    public Skeleton newObject() {
        return new Skeleton(this.skeletonData);
    }
}
