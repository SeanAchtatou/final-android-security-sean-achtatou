package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.DataInput;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.SerializationException;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.esotericsoftware.spine.attachments.AtlasAttachmentLoader;
import com.kbz.esotericsoftware.spine.attachments.Attachment;
import com.kbz.esotericsoftware.spine.attachments.AttachmentLoader;
import com.kbz.esotericsoftware.spine.attachments.AttachmentType;
import com.kbz.esotericsoftware.spine.attachments.BoundingBoxAttachment;
import com.kbz.esotericsoftware.spine.attachments.MeshAttachment;
import com.kbz.esotericsoftware.spine.attachments.RegionAttachment;
import com.kbz.esotericsoftware.spine.attachments.SkinnedMeshAttachment;
import java.io.IOException;

public class SkeletonBinary {
    public static final int CURVE_BEZIER = 2;
    public static final int CURVE_LINEAR = 0;
    public static final int CURVE_STEPPED = 1;
    public static final int TIMELINE_ATTACHMENT = 3;
    public static final int TIMELINE_COLOR = 4;
    public static final int TIMELINE_ROTATE = 1;
    public static final int TIMELINE_SCALE = 0;
    public static final int TIMELINE_TRANSLATE = 2;
    private static final Color tempColor = new Color();
    private final AttachmentLoader attachmentLoader;
    private float scale = 1.0f;

    public SkeletonBinary(TextureAtlas atlas) {
        this.attachmentLoader = new AtlasAttachmentLoader(atlas);
    }

    public SkeletonBinary(AttachmentLoader attachmentLoader2) {
        this.attachmentLoader = attachmentLoader2;
    }

    public float getScale() {
        return this.scale;
    }

    public void setScale(float scale2) {
        this.scale = scale2;
    }

    public SkeletonData readSkeletonData(FileHandle file) {
        if (file == null) {
            throw new IllegalArgumentException("file cannot be null.");
        }
        float scale2 = this.scale;
        SkeletonData skeletonData = new SkeletonData();
        skeletonData.name = file.nameWithoutExtension();
        DataInput input = new DataInput(file.read(512));
        try {
            skeletonData.hash = input.readString();
            if (skeletonData.hash.isEmpty()) {
                skeletonData.hash = null;
            }
            skeletonData.version = input.readString();
            if (skeletonData.version.isEmpty()) {
                skeletonData.version = null;
            }
            skeletonData.width = input.readFloat();
            skeletonData.height = input.readFloat();
            boolean nonessential = input.readBoolean();
            if (nonessential) {
                skeletonData.imagesPath = input.readString();
                if (skeletonData.imagesPath.isEmpty()) {
                    skeletonData.imagesPath = null;
                }
            }
            int n = input.readInt(true);
            for (int i = 0; i < n; i++) {
                String name = input.readString();
                BoneData parent = null;
                int parentIndex = input.readInt(true) - 1;
                if (parentIndex != -1) {
                    parent = skeletonData.bones.get(parentIndex);
                }
                BoneData boneData = new BoneData(name, parent);
                boneData.x = input.readFloat() * scale2;
                boneData.y = input.readFloat() * scale2;
                boneData.scaleX = input.readFloat();
                boneData.scaleY = input.readFloat();
                boneData.rotation = input.readFloat();
                boneData.length = input.readFloat() * scale2;
                boneData.inheritScale = input.readBoolean();
                boneData.inheritRotation = input.readBoolean();
                if (nonessential) {
                    Color.rgba8888ToColor(boneData.color, input.readInt());
                }
                skeletonData.bones.add(boneData);
            }
            int n2 = input.readInt(true);
            for (int i2 = 0; i2 < n2; i2++) {
                IkConstraintData ikConstraintData = new IkConstraintData(input.readString());
                int nn = input.readInt(true);
                for (int ii = 0; ii < nn; ii++) {
                    ikConstraintData.bones.add(skeletonData.bones.get(input.readInt(true)));
                }
                ikConstraintData.target = skeletonData.bones.get(input.readInt(true));
                ikConstraintData.mix = input.readFloat();
                ikConstraintData.bendDirection = input.readByte();
                skeletonData.ikConstraints.add(ikConstraintData);
            }
            int n3 = input.readInt(true);
            for (int i3 = 0; i3 < n3; i3++) {
                SlotData slotData = new SlotData(input.readString(), skeletonData.bones.get(input.readInt(true)));
                Color.rgba8888ToColor(slotData.color, input.readInt());
                slotData.attachmentName = input.readString();
                slotData.blendMode = BlendMode.values[input.readInt(true)];
                skeletonData.slots.add(slotData);
            }
            Skin defaultSkin = readSkin(input, "default", nonessential);
            if (defaultSkin != null) {
                skeletonData.defaultSkin = defaultSkin;
                skeletonData.skins.add(defaultSkin);
            }
            int n4 = input.readInt(true);
            for (int i4 = 0; i4 < n4; i4++) {
                skeletonData.skins.add(readSkin(input, input.readString(), nonessential));
            }
            int n5 = input.readInt(true);
            for (int i5 = 0; i5 < n5; i5++) {
                EventData eventData = new EventData(input.readString());
                eventData.intValue = input.readInt(false);
                eventData.floatValue = input.readFloat();
                eventData.stringValue = input.readString();
                skeletonData.events.add(eventData);
            }
            int n6 = input.readInt(true);
            for (int i6 = 0; i6 < n6; i6++) {
                readAnimation(input.readString(), input, skeletonData);
            }
            try {
                input.close();
            } catch (IOException e) {
            }
            skeletonData.bones.shrink();
            skeletonData.slots.shrink();
            skeletonData.skins.shrink();
            skeletonData.events.shrink();
            skeletonData.animations.shrink();
            skeletonData.ikConstraints.shrink();
            return skeletonData;
        } catch (IOException ex) {
            throw new SerializationException("Error reading skeleton file.", ex);
        } catch (Throwable th) {
            try {
                input.close();
            } catch (IOException e2) {
            }
            throw th;
        }
    }

    private Skin readSkin(DataInput input, String skinName, boolean nonessential) throws IOException {
        int slotCount = input.readInt(true);
        if (slotCount == 0) {
            return null;
        }
        Skin skin = new Skin(skinName);
        for (int i = 0; i < slotCount; i++) {
            int slotIndex = input.readInt(true);
            int nn = input.readInt(true);
            for (int ii = 0; ii < nn; ii++) {
                String name = input.readString();
                skin.addAttachment(slotIndex, name, readAttachment(input, skin, name, nonessential));
            }
        }
        return skin;
    }

    private Attachment readAttachment(DataInput input, Skin skin, String attachmentName, boolean nonessential) throws IOException {
        float scale2 = this.scale;
        String name = input.readString();
        if (name == null) {
            name = attachmentName;
        }
        switch (AttachmentType.values[input.readByte()]) {
            case region:
                String path = input.readString();
                if (path == null) {
                    path = name;
                }
                RegionAttachment region = this.attachmentLoader.newRegionAttachment(skin, name, path);
                if (region == null) {
                    return null;
                }
                region.setPath(path);
                region.setX(input.readFloat() * scale2);
                region.setY(input.readFloat() * scale2);
                region.setScaleX(input.readFloat());
                region.setScaleY(input.readFloat());
                region.setRotation(input.readFloat());
                region.setWidth(input.readFloat() * scale2);
                region.setHeight(input.readFloat() * scale2);
                Color.rgba8888ToColor(region.getColor(), input.readInt());
                region.updateOffset();
                return region;
            case boundingbox:
                BoundingBoxAttachment box = this.attachmentLoader.newBoundingBoxAttachment(skin, name);
                if (box == null) {
                    return null;
                }
                box.setVertices(readFloatArray(input, scale2));
                return box;
            case mesh:
                String path2 = input.readString();
                if (path2 == null) {
                    path2 = name;
                }
                MeshAttachment mesh = this.attachmentLoader.newMeshAttachment(skin, name, path2);
                if (mesh == null) {
                    return null;
                }
                mesh.setPath(path2);
                float[] uvs = readFloatArray(input, 1.0f);
                short[] triangles = readShortArray(input);
                mesh.setVertices(readFloatArray(input, scale2));
                mesh.setTriangles(triangles);
                mesh.setRegionUVs(uvs);
                mesh.updateUVs();
                Color.rgba8888ToColor(mesh.getColor(), input.readInt());
                mesh.setHullLength(input.readInt(true) * 2);
                if (!nonessential) {
                    return mesh;
                }
                mesh.setEdges(readIntArray(input));
                mesh.setWidth(input.readFloat() * scale2);
                mesh.setHeight(input.readFloat() * scale2);
                return mesh;
            case skinnedmesh:
                String path3 = input.readString();
                if (path3 == null) {
                    path3 = name;
                }
                SkinnedMeshAttachment mesh2 = this.attachmentLoader.newSkinnedMeshAttachment(skin, name, path3);
                if (mesh2 == null) {
                    return null;
                }
                mesh2.setPath(path3);
                float[] uvs2 = readFloatArray(input, 1.0f);
                short[] triangles2 = readShortArray(input);
                int vertexCount = input.readInt(true);
                FloatArray weights = new FloatArray(uvs2.length * 3 * 3);
                IntArray bones = new IntArray(uvs2.length * 3);
                int i = 0;
                while (i < vertexCount) {
                    int boneCount = (int) input.readFloat();
                    bones.add(boneCount);
                    int nn = i + (boneCount * 4);
                    while (i < nn) {
                        bones.add((int) input.readFloat());
                        weights.add(input.readFloat() * scale2);
                        weights.add(input.readFloat() * scale2);
                        weights.add(input.readFloat());
                        i += 4;
                    }
                    i++;
                }
                mesh2.setBones(bones.toArray());
                mesh2.setWeights(weights.toArray());
                mesh2.setTriangles(triangles2);
                mesh2.setRegionUVs(uvs2);
                mesh2.updateUVs();
                Color.rgba8888ToColor(mesh2.getColor(), input.readInt());
                mesh2.setHullLength(input.readInt(true) * 2);
                if (!nonessential) {
                    return mesh2;
                }
                mesh2.setEdges(readIntArray(input));
                mesh2.setWidth(input.readFloat() * scale2);
                mesh2.setHeight(input.readFloat() * scale2);
                return mesh2;
            default:
                return null;
        }
    }

    private float[] readFloatArray(DataInput input, float scale2) throws IOException {
        int n = input.readInt(true);
        float[] array = new float[n];
        if (scale2 == 1.0f) {
            for (int i = 0; i < n; i++) {
                array[i] = input.readFloat();
            }
        } else {
            for (int i2 = 0; i2 < n; i2++) {
                array[i2] = input.readFloat() * scale2;
            }
        }
        return array;
    }

    private short[] readShortArray(DataInput input) throws IOException {
        int n = input.readInt(true);
        short[] array = new short[n];
        for (int i = 0; i < n; i++) {
            array[i] = input.readShort();
        }
        return array;
    }

    private int[] readIntArray(DataInput input) throws IOException {
        int n = input.readInt(true);
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = input.readInt(true);
        }
        return array;
    }

    private void readAnimation(String name, DataInput input, SkeletonData skeletonData) {
        int vertexCount;
        float[] vertices;
        Animation.TranslateTimeline timeline;
        Array<Animation.Timeline> timelines = new Array<>();
        float scale2 = this.scale;
        float duration = Animation.CurveTimeline.LINEAR;
        try {
            int n = input.readInt(true);
            for (int i = 0; i < n; i++) {
                int slotIndex = input.readInt(true);
                int nn = input.readInt(true);
                for (int ii = 0; ii < nn; ii++) {
                    int timelineType = input.readByte();
                    int frameCount = input.readInt(true);
                    switch (timelineType) {
                        case 3:
                            Animation.AttachmentTimeline timeline2 = new Animation.AttachmentTimeline(frameCount);
                            timeline2.slotIndex = slotIndex;
                            for (int frameIndex = 0; frameIndex < frameCount; frameIndex++) {
                                timeline2.setFrame(frameIndex, input.readFloat(), input.readString());
                            }
                            timelines.add(timeline2);
                            duration = Math.max(duration, timeline2.getFrames()[frameCount - 1]);
                            break;
                        case 4:
                            Animation.ColorTimeline timeline3 = new Animation.ColorTimeline(frameCount);
                            timeline3.slotIndex = slotIndex;
                            for (int frameIndex2 = 0; frameIndex2 < frameCount; frameIndex2++) {
                                float time = input.readFloat();
                                Color.rgba8888ToColor(tempColor, input.readInt());
                                timeline3.setFrame(frameIndex2, time, tempColor.r, tempColor.g, tempColor.b, tempColor.a);
                                if (frameIndex2 < frameCount - 1) {
                                    readCurve(input, frameIndex2, timeline3);
                                }
                            }
                            timelines.add(timeline3);
                            duration = Math.max(duration, timeline3.getFrames()[(frameCount * 5) - 5]);
                            break;
                    }
                }
            }
            int n2 = input.readInt(true);
            for (int i2 = 0; i2 < n2; i2++) {
                int boneIndex = input.readInt(true);
                int nn2 = input.readInt(true);
                for (int ii2 = 0; ii2 < nn2; ii2++) {
                    int timelineType2 = input.readByte();
                    int frameCount2 = input.readInt(true);
                    switch (timelineType2) {
                        case 0:
                        case 2:
                            float timelineScale = 1.0f;
                            if (timelineType2 == 0) {
                                timeline = new Animation.ScaleTimeline(frameCount2);
                            } else {
                                timeline = new Animation.TranslateTimeline(frameCount2);
                                timelineScale = scale2;
                            }
                            timeline.boneIndex = boneIndex;
                            for (int frameIndex3 = 0; frameIndex3 < frameCount2; frameIndex3++) {
                                timeline.setFrame(frameIndex3, input.readFloat(), input.readFloat() * timelineScale, input.readFloat() * timelineScale);
                                if (frameIndex3 < frameCount2 - 1) {
                                    readCurve(input, frameIndex3, timeline);
                                }
                            }
                            timelines.add(timeline);
                            duration = Math.max(duration, timeline.getFrames()[(frameCount2 * 3) - 3]);
                            break;
                        case 1:
                            Animation.RotateTimeline timeline4 = new Animation.RotateTimeline(frameCount2);
                            timeline4.boneIndex = boneIndex;
                            for (int frameIndex4 = 0; frameIndex4 < frameCount2; frameIndex4++) {
                                timeline4.setFrame(frameIndex4, input.readFloat(), input.readFloat());
                                if (frameIndex4 < frameCount2 - 1) {
                                    readCurve(input, frameIndex4, timeline4);
                                }
                            }
                            timelines.add(timeline4);
                            duration = Math.max(duration, timeline4.getFrames()[(frameCount2 * 2) - 2]);
                            break;
                    }
                }
            }
            int n3 = input.readInt(true);
            for (int i3 = 0; i3 < n3; i3++) {
                int frameCount3 = input.readInt(true);
                Animation.IkConstraintTimeline timeline5 = new Animation.IkConstraintTimeline(frameCount3);
                timeline5.ikConstraintIndex = skeletonData.getIkConstraints().indexOf(skeletonData.ikConstraints.get(input.readInt(true)), true);
                for (int frameIndex5 = 0; frameIndex5 < frameCount3; frameIndex5++) {
                    timeline5.setFrame(frameIndex5, input.readFloat(), input.readFloat(), input.readByte());
                    if (frameIndex5 < frameCount3 - 1) {
                        readCurve(input, frameIndex5, timeline5);
                    }
                }
                timelines.add(timeline5);
                duration = Math.max(duration, timeline5.getFrames()[(frameCount3 * 3) - 3]);
            }
            int n4 = input.readInt(true);
            for (int i4 = 0; i4 < n4; i4++) {
                Skin skin = skeletonData.skins.get(input.readInt(true));
                int nn3 = input.readInt(true);
                for (int ii3 = 0; ii3 < nn3; ii3++) {
                    int slotIndex2 = input.readInt(true);
                    int nnn = input.readInt(true);
                    for (int iii = 0; iii < nnn; iii++) {
                        Attachment attachment = skin.getAttachment(slotIndex2, input.readString());
                        int frameCount4 = input.readInt(true);
                        Animation.FfdTimeline timeline6 = new Animation.FfdTimeline(frameCount4);
                        timeline6.slotIndex = slotIndex2;
                        timeline6.attachment = attachment;
                        for (int frameIndex6 = 0; frameIndex6 < frameCount4; frameIndex6++) {
                            float time2 = input.readFloat();
                            if (attachment instanceof MeshAttachment) {
                                vertexCount = ((MeshAttachment) attachment).getVertices().length;
                            } else {
                                vertexCount = (((SkinnedMeshAttachment) attachment).getWeights().length / 3) * 2;
                            }
                            int end = input.readInt(true);
                            if (end == 0) {
                                vertices = attachment instanceof MeshAttachment ? ((MeshAttachment) attachment).getVertices() : new float[vertexCount];
                            } else {
                                vertices = new float[vertexCount];
                                int start = input.readInt(true);
                                int end2 = end + start;
                                if (scale2 == 1.0f) {
                                    for (int v = start; v < end2; v++) {
                                        vertices[v] = input.readFloat();
                                    }
                                } else {
                                    for (int v2 = start; v2 < end2; v2++) {
                                        vertices[v2] = input.readFloat() * scale2;
                                    }
                                }
                                if (attachment instanceof MeshAttachment) {
                                    float[] meshVertices = ((MeshAttachment) attachment).getVertices();
                                    int vn = vertices.length;
                                    for (int v3 = 0; v3 < vn; v3++) {
                                        vertices[v3] = vertices[v3] + meshVertices[v3];
                                    }
                                }
                            }
                            timeline6.setFrame(frameIndex6, time2, vertices);
                            if (frameIndex6 < frameCount4 - 1) {
                                readCurve(input, frameIndex6, timeline6);
                            }
                        }
                        timelines.add(timeline6);
                        duration = Math.max(duration, timeline6.getFrames()[frameCount4 - 1]);
                    }
                }
            }
            int drawOrderCount = input.readInt(true);
            if (drawOrderCount > 0) {
                Animation.DrawOrderTimeline timeline7 = new Animation.DrawOrderTimeline(drawOrderCount);
                int slotCount = skeletonData.slots.size;
                for (int i5 = 0; i5 < drawOrderCount; i5++) {
                    int offsetCount = input.readInt(true);
                    int[] drawOrder = new int[slotCount];
                    for (int ii4 = slotCount - 1; ii4 >= 0; ii4--) {
                        drawOrder[ii4] = -1;
                    }
                    int[] unchanged = new int[(slotCount - offsetCount)];
                    int originalIndex = 0;
                    int unchangedIndex = 0;
                    int ii5 = 0;
                    while (ii5 < offsetCount) {
                        int slotIndex3 = input.readInt(true);
                        int unchangedIndex2 = unchangedIndex;
                        int originalIndex2 = originalIndex;
                        while (originalIndex2 != slotIndex3) {
                            unchanged[unchangedIndex2] = originalIndex2;
                            unchangedIndex2++;
                            originalIndex2++;
                        }
                        originalIndex = originalIndex2 + 1;
                        drawOrder[input.readInt(true) + originalIndex2] = originalIndex2;
                        ii5++;
                        unchangedIndex = unchangedIndex2;
                    }
                    int unchangedIndex3 = unchangedIndex;
                    for (int originalIndex3 = originalIndex; originalIndex3 < slotCount; originalIndex3++) {
                        unchanged[unchangedIndex3] = originalIndex3;
                        unchangedIndex3++;
                    }
                    int unchangedIndex4 = unchangedIndex3;
                    for (int ii6 = slotCount - 1; ii6 >= 0; ii6--) {
                        if (drawOrder[ii6] == -1) {
                            unchangedIndex4--;
                            drawOrder[ii6] = unchanged[unchangedIndex4];
                        }
                    }
                    timeline7.setFrame(i5, input.readFloat(), drawOrder);
                }
                timelines.add(timeline7);
                duration = Math.max(duration, timeline7.getFrames()[drawOrderCount - 1]);
            }
            int eventCount = input.readInt(true);
            if (eventCount > 0) {
                Animation.EventTimeline timeline8 = new Animation.EventTimeline(eventCount);
                for (int i6 = 0; i6 < eventCount; i6++) {
                    float time3 = input.readFloat();
                    EventData eventData = skeletonData.events.get(input.readInt(true));
                    Event event = new Event(time3, eventData);
                    event.intValue = input.readInt(false);
                    event.floatValue = input.readFloat();
                    event.stringValue = input.readBoolean() ? input.readString() : eventData.stringValue;
                    timeline8.setFrame(i6, event);
                }
                timelines.add(timeline8);
                duration = Math.max(duration, timeline8.getFrames()[eventCount - 1]);
            }
            timelines.shrink();
            skeletonData.animations.add(new Animation(name, timelines, duration));
        } catch (IOException ex) {
            throw new SerializationException("Error reading skeleton file.", ex);
        }
    }

    private void readCurve(DataInput input, int frameIndex, Animation.CurveTimeline timeline) throws IOException {
        switch (input.readByte()) {
            case 1:
                timeline.setStepped(frameIndex);
                return;
            case 2:
                setCurve(timeline, frameIndex, input.readFloat(), input.readFloat(), input.readFloat(), input.readFloat());
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void setCurve(Animation.CurveTimeline timeline, int frameIndex, float cx1, float cy1, float cx2, float cy2) {
        timeline.setCurve(frameIndex, cx1, cy1, cx2, cy2);
    }
}
