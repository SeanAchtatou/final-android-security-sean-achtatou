package com.kbz.esotericsoftware.spine.attachments;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.NumberUtils;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.esotericsoftware.spine.Bone;
import com.kbz.esotericsoftware.spine.Skeleton;
import com.kbz.esotericsoftware.spine.Slot;

public class SkinnedMeshAttachment extends Attachment {
    private int[] bones;
    private final Color color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    private int[] edges;
    private float height;
    private int hullLength;
    private String path;
    private TextureRegion region;
    private float[] regionUVs;
    private short[] triangles;
    private float[] weights;
    private float width;
    private float[] worldVertices;

    public SkinnedMeshAttachment(String name) {
        super(name);
    }

    public void setRegion(TextureRegion region2) {
        if (region2 == null) {
            throw new IllegalArgumentException("region cannot be null.");
        }
        this.region = region2;
    }

    public TextureRegion getRegion() {
        if (this.region != null) {
            return this.region;
        }
        throw new IllegalStateException("Region has not been set: " + this);
    }

    public void updateUVs() {
        float u;
        float v;
        float width2;
        float height2;
        float[] regionUVs2 = this.regionUVs;
        int verticesLength = regionUVs2.length;
        int worldVerticesLength = (verticesLength / 2) * 5;
        if (this.worldVertices == null || this.worldVertices.length != worldVerticesLength) {
            this.worldVertices = new float[worldVerticesLength];
        }
        if (this.region == null) {
            v = Animation.CurveTimeline.LINEAR;
            u = 0.0f;
            height2 = 1.0f;
            width2 = 1.0f;
        } else {
            u = this.region.getU();
            v = this.region.getV();
            width2 = this.region.getU2() - u;
            height2 = this.region.getV2() - v;
        }
        if (!(this.region instanceof TextureAtlas.AtlasRegion) || !((TextureAtlas.AtlasRegion) this.region).rotate) {
            int i = 0;
            int w = 3;
            while (i < verticesLength) {
                this.worldVertices[w] = (regionUVs2[i] * width2) + u;
                this.worldVertices[w + 1] = (regionUVs2[i + 1] * height2) + v;
                i += 2;
                w += 5;
            }
            return;
        }
        int i2 = 0;
        int w2 = 3;
        while (i2 < verticesLength) {
            this.worldVertices[w2] = (regionUVs2[i2 + 1] * width2) + u;
            this.worldVertices[w2 + 1] = (v + height2) - (regionUVs2[i2] * height2);
            i2 += 2;
            w2 += 5;
        }
    }

    public float[] updateWorldVertices(Slot slot, boolean premultipliedAlpha) {
        Skeleton skeleton = slot.getSkeleton();
        Color skeletonColor = skeleton.getColor();
        Color meshColor = slot.getColor();
        Color regionColor = this.color;
        float a = skeletonColor.a * meshColor.a * regionColor.a * 255.0f;
        float multiplier = premultipliedAlpha ? a : 255.0f;
        float color2 = NumberUtils.intToFloatColor((((int) a) << 24) | (((int) (((skeletonColor.b * meshColor.b) * regionColor.b) * multiplier)) << 16) | (((int) (((skeletonColor.g * meshColor.g) * regionColor.g) * multiplier)) << 8) | ((int) (skeletonColor.r * meshColor.r * regionColor.r * multiplier)));
        float[] worldVertices2 = this.worldVertices;
        float x = skeleton.getX();
        float y = skeleton.getY();
        Object[] skeletonBones = skeleton.getBones().items;
        float[] weights2 = this.weights;
        int[] bones2 = this.bones;
        FloatArray ffdArray = slot.getAttachmentVertices();
        if (ffdArray.size != 0) {
            float[] ffd = ffdArray.items;
            int w = 0;
            int v = 0;
            int b = 0;
            int f = 0;
            int n = bones2.length;
            while (true) {
                int v2 = v;
                if (v2 >= n) {
                    break;
                }
                float wx = Animation.CurveTimeline.LINEAR;
                float wy = Animation.CurveTimeline.LINEAR;
                v = v2 + 1;
                int nn = bones2[v2] + v;
                while (v < nn) {
                    Bone bone = (Bone) skeletonBones[bones2[v]];
                    float vx = weights2[b] + ffd[f];
                    float vy = weights2[b + 1] + ffd[f + 1];
                    float weight = weights2[b + 2];
                    wx += ((bone.getA() * vx) + (bone.getB() * vy) + bone.getWorldX()) * weight;
                    wy += ((bone.getC() * vx) + (bone.getD() * vy) + bone.getWorldY()) * weight;
                    v++;
                    b += 3;
                    f += 2;
                }
                worldVertices2[w] = wx + x;
                worldVertices2[w + 1] = wy + y;
                worldVertices2[w + 2] = color2;
                w += 5;
            }
        } else {
            int w2 = 0;
            int v3 = 0;
            int b2 = 0;
            int n2 = bones2.length;
            while (true) {
                int v4 = v3;
                if (v4 >= n2) {
                    break;
                }
                float wx2 = Animation.CurveTimeline.LINEAR;
                float wy2 = Animation.CurveTimeline.LINEAR;
                v3 = v4 + 1;
                int nn2 = bones2[v4] + v3;
                while (v3 < nn2) {
                    Bone bone2 = (Bone) skeletonBones[bones2[v3]];
                    float vx2 = weights2[b2];
                    float vy2 = weights2[b2 + 1];
                    float weight2 = weights2[b2 + 2];
                    wx2 += ((bone2.getA() * vx2) + (bone2.getB() * vy2) + bone2.getWorldX()) * weight2;
                    wy2 += ((bone2.getC() * vx2) + (bone2.getD() * vy2) + bone2.getWorldY()) * weight2;
                    v3++;
                    b2 += 3;
                }
                worldVertices2[w2] = wx2 + x;
                worldVertices2[w2 + 1] = wy2 + y;
                worldVertices2[w2 + 2] = color2;
                w2 += 5;
            }
        }
        return worldVertices2;
    }

    public float[] getWorldVertices() {
        return this.worldVertices;
    }

    public int[] getBones() {
        return this.bones;
    }

    public void setBones(int[] bones2) {
        this.bones = bones2;
    }

    public float[] getWeights() {
        return this.weights;
    }

    public void setWeights(float[] weights2) {
        this.weights = weights2;
    }

    public short[] getTriangles() {
        return this.triangles;
    }

    public void setTriangles(short[] triangles2) {
        this.triangles = triangles2;
    }

    public float[] getRegionUVs() {
        return this.regionUVs;
    }

    public void setRegionUVs(float[] regionUVs2) {
        this.regionUVs = regionUVs2;
    }

    public Color getColor() {
        return this.color;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path2) {
        this.path = path2;
    }

    public int getHullLength() {
        return this.hullLength;
    }

    public void setHullLength(int hullLength2) {
        this.hullLength = hullLength2;
    }

    public void setEdges(int[] edges2) {
        this.edges = edges2;
    }

    public int[] getEdges() {
        return this.edges;
    }

    public float getWidth() {
        return this.width;
    }

    public void setWidth(float width2) {
        this.width = width2;
    }

    public float getHeight() {
        return this.height;
    }

    public void setHeight(float height2) {
        this.height = height2;
    }
}
