package com.kbz.esotericsoftware.spine;

public class TransformConstraintData {
    BoneData bone;
    final String name;
    BoneData target;
    float translateMix;
    float x;
    float y;

    public TransformConstraintData(String name2) {
        this.name = name2;
    }

    public String getName() {
        return this.name;
    }

    public BoneData getBone() {
        return this.bone;
    }

    public void setBone(BoneData bone2) {
        this.bone = bone2;
    }

    public BoneData getTarget() {
        return this.target;
    }

    public void setTarget(BoneData target2) {
        this.target = target2;
    }

    public float getTranslateMix() {
        return this.translateMix;
    }

    public void setTranslateMix(float translateMix2) {
        this.translateMix = translateMix2;
    }

    public float getX() {
        return this.x;
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public float getY() {
        return this.y;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public String toString() {
        return this.name;
    }
}
