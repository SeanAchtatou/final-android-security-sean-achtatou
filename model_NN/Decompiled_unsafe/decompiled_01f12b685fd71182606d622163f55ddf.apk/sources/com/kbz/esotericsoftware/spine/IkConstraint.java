package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;

public class IkConstraint implements Updatable {
    int bendDirection;
    final Array<Bone> bones;
    final IkConstraintData data;
    float mix = 1.0f;
    Bone target;

    public IkConstraint(IkConstraintData data2, Skeleton skeleton) {
        this.data = data2;
        this.mix = data2.mix;
        this.bendDirection = data2.bendDirection;
        this.bones = new Array<>(data2.bones.size);
        if (skeleton != null) {
            Iterator<BoneData> it = data2.bones.iterator();
            while (it.hasNext()) {
                this.bones.add(skeleton.findBone(it.next().name));
            }
            this.target = skeleton.findBone(data2.target.name);
        }
    }

    public IkConstraint(IkConstraint ikConstraint, Skeleton skeleton) {
        this.data = ikConstraint.data;
        this.bones = new Array<>(ikConstraint.bones.size);
        Iterator<Bone> it = ikConstraint.bones.iterator();
        while (it.hasNext()) {
            Bone bone = it.next();
            this.bones.add(skeleton.bones.get(bone.skeleton.bones.indexOf(bone, true)));
        }
        this.target = skeleton.bones.get(ikConstraint.target.skeleton.bones.indexOf(ikConstraint.target, true));
        this.mix = ikConstraint.mix;
        this.bendDirection = ikConstraint.bendDirection;
    }

    public void apply() {
        update();
    }

    public void update() {
        Bone target2 = this.target;
        Array<Bone> bones2 = this.bones;
        switch (bones2.size) {
            case 1:
                apply(bones2.first(), target2.worldX, target2.worldY, this.mix);
                return;
            case 2:
                apply(bones2.first(), bones2.get(1), target2.worldX, target2.worldY, this.bendDirection, this.mix);
                return;
            default:
                return;
        }
    }

    public Array<Bone> getBones() {
        return this.bones;
    }

    public Bone getTarget() {
        return this.target;
    }

    public void setTarget(Bone target2) {
        this.target = target2;
    }

    public float getMix() {
        return this.mix;
    }

    public void setMix(float mix2) {
        this.mix = mix2;
    }

    public int getBendDirection() {
        return this.bendDirection;
    }

    public void setBendDirection(int bendDirection2) {
        this.bendDirection = bendDirection2;
    }

    public IkConstraintData getData() {
        return this.data;
    }

    public String toString() {
        return this.data.name + " CONSTRAINT";
    }

    public static void apply(Bone bone, float targetX, float targetY, float alpha) {
        float parentRotation = bone.parent == null ? Animation.CurveTimeline.LINEAR : bone.parent.getWorldRotationX();
        float rotation = bone.rotation;
        float rotationIK = (MathUtils.atan2(targetY - bone.worldY, targetX - bone.worldX) * 57.295776f) - parentRotation;
        if (rotationIK > 180.0f) {
            rotationIK -= 360.0f;
        } else if (rotationIK < -180.0f) {
            rotationIK += 360.0f;
        }
        bone.updateWorldTransform(bone.x, bone.y, rotation + ((rotationIK - rotation) * alpha), bone.scaleX, bone.scaleY);
    }

    public static void apply(Bone parent, Bone child, float targetX, float targetY, int bendDir, float alpha) {
        int offset1;
        int sign2;
        int offset2;
        float tx;
        float ty;
        float dx;
        float dy;
        float a1;
        float a2;
        float r;
        if (alpha != Animation.CurveTimeline.LINEAR) {
            float px = parent.x;
            float py = parent.y;
            float psx = parent.scaleX;
            float psy = parent.scaleY;
            float csx = child.scaleX;
            float cy = child.y;
            if (psx < Animation.CurveTimeline.LINEAR) {
                psx = -psx;
                offset1 = 180;
                sign2 = -1;
            } else {
                offset1 = 0;
                sign2 = 1;
            }
            if (psy < Animation.CurveTimeline.LINEAR) {
                psy = -psy;
                sign2 = -sign2;
            }
            if (csx < Animation.CurveTimeline.LINEAR) {
                csx = -csx;
                offset2 = 180;
            } else {
                offset2 = 0;
            }
            Bone pp = parent.parent;
            if (pp == null) {
                tx = targetX - px;
                ty = targetY - py;
                dx = child.worldX - px;
                dy = child.worldY - py;
            } else {
                float a = pp.a;
                float b = pp.b;
                float c = pp.c;
                float d = pp.d;
                float invDet = 1.0f / ((a * d) - (b * c));
                float wx = pp.worldX;
                float wy = pp.worldY;
                float x = targetX - wx;
                float y = targetY - wy;
                tx = (((x * d) - (y * b)) * invDet) - px;
                ty = (((y * a) - (x * c)) * invDet) - py;
                float x2 = child.worldX - wx;
                float y2 = child.worldY - wy;
                dx = (((x2 * d) - (y2 * b)) * invDet) - px;
                dy = (((y2 * a) - (x2 * c)) * invDet) - py;
            }
            float l1 = (float) Math.sqrt((double) ((dx * dx) + (dy * dy)));
            float l2 = child.data.length * csx;
            if (Math.abs(psx - psy) <= 1.0E-4f) {
                float l22 = l2 * psx;
                float cos = ((((tx * tx) + (ty * ty)) - (l1 * l1)) - (l22 * l22)) / ((2.0f * l1) * l22);
                if (cos < -1.0f) {
                    cos = -1.0f;
                } else if (cos > 1.0f) {
                    cos = 1.0f;
                }
                a2 = ((float) Math.acos((double) cos)) * ((float) bendDir);
                float a3 = l1 + (l22 * cos);
                float o = l22 * MathUtils.sin(a2);
                a1 = MathUtils.atan2((ty * a3) - (tx * o), (tx * a3) + (ty * o));
            } else {
                cy = Animation.CurveTimeline.LINEAR;
                float a4 = psx * l2;
                float b2 = psy * l2;
                float ta = MathUtils.atan2(ty, tx);
                float aa = a4 * a4;
                float bb = b2 * b2;
                float dd = (tx * tx) + (ty * ty);
                float c0 = ((bb * (l1 * l1)) + (aa * dd)) - (aa * bb);
                float c1 = -2.0f * bb * l1;
                float c2 = bb - aa;
                float d2 = (c1 * c1) - ((4.0f * c2) * c0);
                if (d2 >= Animation.CurveTimeline.LINEAR) {
                    float q = (float) Math.sqrt((double) d2);
                    if (c1 < Animation.CurveTimeline.LINEAR) {
                        q = -q;
                    }
                    float q2 = (-(c1 + q)) / 2.0f;
                    float r0 = q2 / c2;
                    float r1 = c0 / q2;
                    if (Math.abs(r0) < Math.abs(r1)) {
                        r = r0;
                    } else {
                        r = r1;
                    }
                    if (r * r <= dd) {
                        float y3 = ((float) Math.sqrt((double) (dd - (r * r)))) * ((float) bendDir);
                        a1 = ta - MathUtils.atan2(y3, r);
                        a2 = MathUtils.atan2(y3 / psy, (r - l1) / psx);
                    }
                }
                float minAngle = Animation.CurveTimeline.LINEAR;
                float minDist = Float.MAX_VALUE;
                float minX = Animation.CurveTimeline.LINEAR;
                float minY = Animation.CurveTimeline.LINEAR;
                float maxAngle = Animation.CurveTimeline.LINEAR;
                float maxDist = Animation.CurveTimeline.LINEAR;
                float maxX = Animation.CurveTimeline.LINEAR;
                float maxY = Animation.CurveTimeline.LINEAR;
                float x3 = l1 + a4;
                float dist = x3 * x3;
                if (dist > Animation.CurveTimeline.LINEAR) {
                    maxAngle = Animation.CurveTimeline.LINEAR;
                    maxDist = dist;
                    maxX = x3;
                }
                float x4 = l1 - a4;
                float dist2 = x4 * x4;
                if (dist2 < Float.MAX_VALUE) {
                    minAngle = 3.1415927f;
                    minDist = dist2;
                    minX = x4;
                }
                float angle = (float) Math.acos((double) (((-a4) * l1) / (aa - bb)));
                float x5 = (MathUtils.cos(angle) * a4) + l1;
                float y4 = b2 * MathUtils.sin(angle);
                float dist3 = (x5 * x5) + (y4 * y4);
                if (dist3 < minDist) {
                    minAngle = angle;
                    minDist = dist3;
                    minX = x5;
                    minY = y4;
                }
                if (dist3 > maxDist) {
                    maxAngle = angle;
                    maxDist = dist3;
                    maxX = x5;
                    maxY = y4;
                }
                if (dd <= (minDist + maxDist) / 2.0f) {
                    a1 = ta - MathUtils.atan2(((float) bendDir) * minY, minX);
                    a2 = minAngle * ((float) bendDir);
                } else {
                    a1 = ta - MathUtils.atan2(((float) bendDir) * maxY, maxX);
                    a2 = maxAngle * ((float) bendDir);
                }
            }
            float offset = MathUtils.atan2(cy, child.x) * ((float) sign2);
            float a12 = ((a1 - offset) * 57.295776f) + ((float) offset1);
            float a22 = ((a2 + offset) * 57.295776f * ((float) sign2)) + ((float) offset2);
            if (a12 > 180.0f) {
                a12 -= 360.0f;
            } else if (a12 < -180.0f) {
                a12 += 360.0f;
            }
            if (a22 > 180.0f) {
                a22 -= 360.0f;
            } else if (a22 < -180.0f) {
                a22 += 360.0f;
            }
            float rotation = parent.rotation;
            parent.updateWorldTransform(parent.x, parent.y, rotation + ((a12 - rotation) * alpha), parent.scaleX, parent.scaleY);
            float rotation2 = child.rotation;
            child.updateWorldTransform(child.x, cy, rotation2 + ((a22 - rotation2) * alpha), child.scaleX, child.scaleY);
        }
    }
}
