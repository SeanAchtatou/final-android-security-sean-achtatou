package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.math.Vector2;
import com.kbz.esotericsoftware.spine.Animation;

public class TransformConstraint implements Updatable {
    Bone bone;
    final TransformConstraintData data;
    Bone target;
    final Vector2 temp = new Vector2();
    float translateMix;
    float x;
    float y;

    public TransformConstraint(TransformConstraintData data2, Skeleton skeleton) {
        this.data = data2;
        this.translateMix = data2.translateMix;
        if (skeleton != null) {
            this.bone = skeleton.findBone(data2.bone.name);
            this.target = skeleton.findBone(data2.target.name);
        }
    }

    public TransformConstraint(TransformConstraint constraint, Skeleton skeleton) {
        this.data = constraint.data;
        this.translateMix = this.data.translateMix;
        this.bone = skeleton.bones.get(constraint.bone.skeleton.bones.indexOf(constraint.target, true));
        this.target = skeleton.bones.get(constraint.target.skeleton.bones.indexOf(constraint.target, true));
    }

    public void apply() {
        update();
    }

    public void update() {
        float translateMix2 = this.translateMix;
        if (translateMix2 > Animation.CurveTimeline.LINEAR) {
            Bone bone2 = this.bone;
            Vector2 temp2 = this.temp;
            this.target.localToWorld(temp2.set(this.x, this.y));
            bone2.worldX += (temp2.x - bone2.worldX) * translateMix2;
            bone2.worldY += (temp2.y - bone2.worldY) * translateMix2;
        }
    }

    public Bone getBone() {
        return this.bone;
    }

    public void setBone(Bone bone2) {
        this.bone = bone2;
    }

    public Bone getTarget() {
        return this.target;
    }

    public void setTarget(Bone target2) {
        this.target = target2;
    }

    public float getTranslateMix() {
        return this.translateMix;
    }

    public void setTranslateMix(float translateMix2) {
        this.translateMix = translateMix2;
    }

    public TransformConstraintData getData() {
        return this.data;
    }

    public String toString() {
        return this.data.name;
    }
}
