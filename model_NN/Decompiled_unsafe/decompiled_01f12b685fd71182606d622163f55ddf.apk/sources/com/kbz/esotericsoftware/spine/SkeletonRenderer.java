package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Array;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.esotericsoftware.spine.attachments.Attachment;
import com.kbz.esotericsoftware.spine.attachments.MeshAttachment;
import com.kbz.esotericsoftware.spine.attachments.RegionAttachment;
import com.kbz.esotericsoftware.spine.attachments.SkeletonAttachment;
import com.kbz.esotericsoftware.spine.attachments.SkinnedMeshAttachment;

public class SkeletonRenderer<T extends Batch> {
    boolean premultipliedAlpha;

    public void draw(Batch batch, Skeleton skeleton) {
        Skeleton attachmentSkeleton;
        boolean premultipliedAlpha2 = this.premultipliedAlpha;
        BlendMode blendMode = null;
        Array<Slot> drawOrder = skeleton.drawOrder;
        int n = drawOrder.size;
        for (int i = 0; i < n; i++) {
            Slot slot = drawOrder.get(i);
            Attachment attachment = slot.attachment;
            if (attachment instanceof RegionAttachment) {
                RegionAttachment regionAttachment = (RegionAttachment) attachment;
                float[] vertices = regionAttachment.updateWorldVertices(slot, premultipliedAlpha2);
                BlendMode slotBlendMode = slot.data.getBlendMode();
                if (slotBlendMode != blendMode) {
                    blendMode = slotBlendMode;
                    batch.setBlendFunction(blendMode.getSource(premultipliedAlpha2), blendMode.getDest());
                }
                batch.draw(regionAttachment.getRegion().getTexture(), vertices, 0, 20);
            } else if ((attachment instanceof MeshAttachment) || (attachment instanceof SkinnedMeshAttachment)) {
                throw new RuntimeException("SkeletonMeshRenderer is required to render meshes.");
            } else if ((attachment instanceof SkeletonAttachment) && (attachmentSkeleton = ((SkeletonAttachment) attachment).getSkeleton()) != null) {
                Bone bone = slot.getBone();
                Bone rootBone = attachmentSkeleton.getRootBone();
                float oldScaleX = rootBone.getScaleX();
                float oldScaleY = rootBone.getScaleY();
                float oldRotation = rootBone.getRotation();
                attachmentSkeleton.setPosition(skeleton.getX() + bone.getWorldX(), skeleton.getY() + bone.getWorldY());
                rootBone.setRotation(bone.getWorldRotationX() + oldRotation);
                attachmentSkeleton.updateWorldTransform();
                draw(batch, attachmentSkeleton);
                attachmentSkeleton.setX(Animation.CurveTimeline.LINEAR);
                attachmentSkeleton.setY(Animation.CurveTimeline.LINEAR);
                rootBone.setScaleX(oldScaleX);
                rootBone.setScaleY(oldScaleY);
                rootBone.setRotation(oldRotation);
            }
        }
    }

    public void setPremultipliedAlpha(boolean premultipliedAlpha2) {
        this.premultipliedAlpha = premultipliedAlpha2;
    }
}
