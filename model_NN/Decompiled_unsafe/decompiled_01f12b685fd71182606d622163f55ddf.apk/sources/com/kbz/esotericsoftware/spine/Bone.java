package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Vector2;
import com.kbz.esotericsoftware.spine.Animation;

public class Bone implements Updatable {
    float a;
    float appliedRotation;
    float appliedScaleX;
    float appliedScaleY;
    float b;
    float c;
    float d;
    final BoneData data;
    final Bone parent;
    float rotation;
    float scaleX;
    float scaleY;
    final Skeleton skeleton;
    float worldSignX;
    float worldSignY;
    float worldX;
    float worldY;
    float x;
    float y;

    Bone(BoneData data2) {
        this.data = data2;
        this.parent = null;
        this.skeleton = null;
    }

    public Bone(BoneData data2, Skeleton skeleton2, Bone parent2) {
        if (data2 == null) {
            throw new IllegalArgumentException("data cannot be null.");
        } else if (skeleton2 == null) {
            throw new IllegalArgumentException("skeleton cannot be null.");
        } else {
            this.data = data2;
            this.skeleton = skeleton2;
            this.parent = parent2;
            setToSetupPose();
        }
    }

    public Bone(Bone bone, Skeleton skeleton2, Bone parent2) {
        if (bone == null) {
            throw new IllegalArgumentException("bone cannot be null.");
        }
        this.skeleton = skeleton2;
        this.parent = parent2;
        this.data = bone.data;
        this.x = bone.x;
        this.y = bone.y;
        this.rotation = bone.rotation;
        this.scaleX = bone.scaleX;
        this.scaleY = bone.scaleY;
    }

    public void updateWorldTransform() {
        updateWorldTransform(this.x, this.y, this.rotation, this.scaleX, this.scaleY);
    }

    public void updateWorldTransform(float x2, float y2, float rotation2, float scaleX2, float scaleY2) {
        this.appliedRotation = rotation2;
        this.appliedScaleX = scaleX2;
        this.appliedScaleY = scaleY2;
        float cos = MathUtils.cosDeg(rotation2);
        float sin = MathUtils.sinDeg(rotation2);
        float la = cos * scaleX2;
        float lb = (-sin) * scaleY2;
        float lc = sin * scaleX2;
        float ld = cos * scaleY2;
        Bone parent2 = this.parent;
        if (parent2 == null) {
            Skeleton skeleton2 = this.skeleton;
            if (skeleton2.flipX) {
                la = -la;
                lc = -lc;
                scaleX2 = -scaleX2;
                x2 = -x2;
            }
            if (skeleton2.flipY) {
                lb = -lb;
                ld = -ld;
                scaleY2 = -scaleY2;
                y2 = -y2;
            }
            this.a = la;
            this.b = lb;
            this.c = lc;
            this.d = ld;
            this.worldX = x2;
            this.worldY = y2;
            this.worldSignX = Math.signum(scaleX2);
            this.worldSignY = Math.signum(scaleY2);
            return;
        }
        float pa = parent2.a;
        float pb = parent2.b;
        float pc = parent2.c;
        float pd = parent2.d;
        this.worldX = (pa * x2) + (pb * y2) + parent2.worldX;
        this.worldY = (pc * x2) + (pd * y2) + parent2.worldY;
        this.worldSignX = parent2.worldSignX * Math.signum(scaleX2);
        this.worldSignY = parent2.worldSignY * Math.signum(scaleY2);
        if (this.data.inheritRotation && this.data.inheritScale) {
            this.a = (pa * la) + (pb * lc);
            this.b = (pa * lb) + (pb * ld);
            this.c = (pc * la) + (pd * lc);
            this.d = (pc * lb) + (pd * ld);
        } else if (this.data.inheritRotation) {
            float pa2 = 1.0f;
            float pb2 = Animation.CurveTimeline.LINEAR;
            float pc2 = Animation.CurveTimeline.LINEAR;
            float pd2 = 1.0f;
            for (Bone p = parent2; p != null; p = p.parent) {
                float cos2 = MathUtils.cosDeg(p.appliedRotation);
                float sin2 = MathUtils.sinDeg(p.appliedRotation);
                float b2 = ((-sin2) * pa2) + (pb2 * cos2);
                pa2 = (pa2 * cos2) + (pb2 * sin2);
                pb2 = b2;
                pc2 = (pc2 * cos2) + (pd2 * sin2);
                pd2 = ((-sin2) * pc2) + (pd2 * cos2);
            }
            this.a = (pa2 * la) + (pb2 * lc);
            this.b = (pa2 * lb) + (pb2 * ld);
            this.c = (pc2 * la) + (pd2 * lc);
            this.d = (pc2 * lb) + (pd2 * ld);
        } else if (this.data.inheritScale) {
            float pa3 = 1.0f;
            float pb3 = Animation.CurveTimeline.LINEAR;
            float pc3 = Animation.CurveTimeline.LINEAR;
            float pd3 = 1.0f;
            for (Bone p2 = parent2; p2 != null; p2 = p2.parent) {
                float r = p2.rotation;
                float cos3 = MathUtils.cosDeg(r);
                float sin3 = MathUtils.sinDeg(r);
                float psx = p2.appliedScaleX;
                float psy = p2.appliedScaleY;
                float za = cos3 * psx;
                float zb = (-sin3) * psy;
                float zc = sin3 * psx;
                float zd = cos3 * psy;
                float temp = (pa3 * za) + (pb3 * zc);
                float pb4 = (pa3 * zb) + (pb3 * zd);
                float pa4 = temp;
                float temp2 = (pc3 * za) + (pd3 * zc);
                float pd4 = (pc3 * zb) + (pd3 * zd);
                float pc4 = temp2;
                if (psx < Animation.CurveTimeline.LINEAR) {
                    r = 3.1415927f - r;
                }
                float cos4 = MathUtils.cosDeg(-r);
                float sin4 = MathUtils.sinDeg(-r);
                float temp3 = (pa4 * cos4) + (pb4 * sin4);
                pb3 = ((-sin4) * pa4) + (pb4 * cos4);
                pa3 = temp3;
                float temp4 = (pc4 * cos4) + (pd4 * sin4);
                pd3 = ((-sin4) * pc4) + (pd4 * cos4);
                pc3 = temp4;
            }
            this.a = (pa3 * la) + (pb3 * lc);
            this.b = (pa3 * lb) + (pb3 * ld);
            this.c = (pc3 * la) + (pd3 * lc);
            this.d = (pc3 * lb) + (pd3 * ld);
        } else {
            this.a = la;
            this.b = lb;
            this.c = lc;
            this.d = ld;
        }
    }

    public void update() {
        updateWorldTransform(this.x, this.y, this.rotation, this.scaleX, this.scaleY);
    }

    public void setToSetupPose() {
        BoneData data2 = this.data;
        this.x = data2.x;
        this.y = data2.y;
        this.rotation = data2.rotation;
        this.scaleX = data2.scaleX;
        this.scaleY = data2.scaleY;
    }

    public BoneData getData() {
        return this.data;
    }

    public Skeleton getSkeleton() {
        return this.skeleton;
    }

    public Bone getParent() {
        return this.parent;
    }

    public float getX() {
        return this.x;
    }

    public void setX(float x2) {
        this.x = x2;
    }

    public float getY() {
        return this.y;
    }

    public void setY(float y2) {
        this.y = y2;
    }

    public void setPosition(float x2, float y2) {
        this.x = x2;
        this.y = y2;
    }

    public float getRotation() {
        return this.rotation;
    }

    public void setRotation(float rotation2) {
        this.rotation = rotation2;
    }

    public float getScaleX() {
        return this.scaleX;
    }

    public void setScaleX(float scaleX2) {
        this.scaleX = scaleX2;
    }

    public float getScaleY() {
        return this.scaleY;
    }

    public void setScaleY(float scaleY2) {
        this.scaleY = scaleY2;
    }

    public void setScale(float scaleX2, float scaleY2) {
        this.scaleX = scaleX2;
        this.scaleY = scaleY2;
    }

    public void setScale(float scale) {
        this.scaleX = scale;
        this.scaleY = scale;
    }

    public float getA() {
        return this.a;
    }

    public float getB() {
        return this.b;
    }

    public float getC() {
        return this.c;
    }

    public float getD() {
        return this.d;
    }

    public float getWorldX() {
        return this.worldX;
    }

    public float getWorldY() {
        return this.worldY;
    }

    public float getWorldRotationX() {
        return ((float) Math.atan2((double) this.c, (double) this.a)) * 57.295776f;
    }

    public float getWorldRotationY() {
        return ((float) Math.atan2((double) this.d, (double) this.b)) * 57.295776f;
    }

    public float getWorldScaleX() {
        return ((float) Math.sqrt((double) ((this.a * this.a) + (this.b * this.b)))) * this.worldSignX;
    }

    public float getWorldScaleY() {
        return ((float) Math.sqrt((double) ((this.c * this.c) + (this.d * this.d)))) * this.worldSignY;
    }

    public float getWorldSignX() {
        return this.worldSignX;
    }

    public float getWorldSignY() {
        return this.worldSignY;
    }

    public Matrix3 getWorldTransform(Matrix3 worldTransform) {
        if (worldTransform == null) {
            throw new IllegalArgumentException("worldTransform cannot be null.");
        }
        float[] val = worldTransform.val;
        val[0] = this.a;
        val[3] = this.b;
        val[1] = this.c;
        val[4] = this.d;
        val[6] = this.worldX;
        val[7] = this.worldY;
        val[2] = 0.0f;
        val[5] = 0.0f;
        val[8] = 1.0f;
        return worldTransform;
    }

    public Vector2 worldToLocal(Vector2 world) {
        float x2 = world.x - this.worldX;
        float y2 = world.y - this.worldY;
        float a2 = this.a;
        float b2 = this.b;
        float c2 = this.c;
        float d2 = this.d;
        float invDet = 1.0f / ((a2 * d2) - (b2 * c2));
        world.x = ((x2 * a2) * invDet) - ((y2 * b2) * invDet);
        world.y = ((y2 * d2) * invDet) - ((x2 * c2) * invDet);
        return world;
    }

    public Vector2 localToWorld(Vector2 local) {
        float x2 = local.x;
        float y2 = local.y;
        local.x = (this.a * x2) + (this.b * y2) + this.worldX;
        local.y = (this.c * x2) + (this.d * y2) + this.worldY;
        return local;
    }

    public String toString() {
        return this.data.name;
    }
}
