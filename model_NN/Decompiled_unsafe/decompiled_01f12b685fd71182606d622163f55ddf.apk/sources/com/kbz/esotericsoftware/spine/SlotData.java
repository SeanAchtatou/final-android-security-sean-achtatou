package com.kbz.esotericsoftware.spine;

import com.badlogic.gdx.graphics.Color;

public class SlotData {
    String attachmentName;
    BlendMode blendMode;
    final BoneData boneData;
    final Color color;
    final String name;

    SlotData() {
        this.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.name = null;
        this.boneData = null;
    }

    public SlotData(String name2, BoneData boneData2) {
        this.color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        if (name2 == null) {
            throw new IllegalArgumentException("name cannot be null.");
        } else if (boneData2 == null) {
            throw new IllegalArgumentException("boneData cannot be null.");
        } else {
            this.name = name2;
            this.boneData = boneData2;
        }
    }

    public String getName() {
        return this.name;
    }

    public BoneData getBoneData() {
        return this.boneData;
    }

    public Color getColor() {
        return this.color;
    }

    public void setAttachmentName(String attachmentName2) {
        this.attachmentName = attachmentName2;
    }

    public String getAttachmentName() {
        return this.attachmentName;
    }

    public BlendMode getBlendMode() {
        return this.blendMode;
    }

    public void setBlendMode(BlendMode blendMode2) {
        this.blendMode = blendMode2;
    }

    public String toString() {
        return this.name;
    }
}
