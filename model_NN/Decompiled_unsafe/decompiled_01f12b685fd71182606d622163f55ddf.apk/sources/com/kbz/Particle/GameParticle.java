package com.kbz.Particle;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Pool;
import com.kbz.AssetManger.GAssetsManager;
import com.kbz.ParticleOld.ParticleEffect;
import com.kbz.ParticleOld.ParticleEmitter;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import java.util.Iterator;

public class GameParticle extends Group implements Disposable, Pool.Poolable, GameConstant {
    public static int count;
    public static boolean isDebug = false;
    private GParticle particle;
    /* access modifiers changed from: private */
    public Pool pool;

    public void start(int x, int y, int layer) {
    }

    public void start(int x, int y, Group group) {
    }

    public void setPool(Pool pool2) {
        this.pool = pool2;
    }

    public boolean isActive() {
        return this.pool != null;
    }

    public GameParticle(ParticleEffect effect) {
        setTransform(false);
        this.particle = new GParticle(effect);
        addActor(this.particle);
        count++;
    }

    public boolean isComplete() {
        return this.particle.isComplete();
    }

    public ParticleEffect getEffect() {
        return this.particle.getEffect();
    }

    public BoundingBox getBoundingBox() {
        return this.particle.getBoundingBox();
    }

    public void setLoop(boolean isLoop) {
        Iterator<ParticleEmitter> it = this.particle.getEffect().getEmitters().iterator();
        while (it.hasNext()) {
            it.next().setContinuous(isLoop);
        }
    }

    public void setAdditive(boolean additive) {
        Iterator<ParticleEmitter> it = this.particle.getEffect().getEmitters().iterator();
        while (it.hasNext()) {
            it.next().setAdditive(additive);
        }
    }

    public void setPremultipliedAlpha(boolean premultipliedAlpha) {
        Iterator<ParticleEmitter> it = this.particle.getEffect().getEmitters().iterator();
        while (it.hasNext()) {
            it.next().setPremultipliedAlpha(premultipliedAlpha);
        }
    }

    public void setAttached(boolean attached) {
        Iterator<ParticleEmitter> it = this.particle.getEffect().getEmitters().iterator();
        while (it.hasNext()) {
            it.next().setAttached(attached);
        }
    }

    public void setEmittersPosition(float x, float y) {
        this.particle.setEmittersPosition(x, y);
    }

    public void setScale(float scale) {
        this.particle.setScale(scale);
    }

    public void act(float delta) {
        super.act(delta);
    }

    public void scaleEffect(float scaleX, float scaleY) {
        this.particle.scaleEffect(scaleX, scaleY);
    }

    private class GParticle extends Actor implements Disposable, Pool.Poolable {
        float delta;
        private ParticleEffect effect;

        private GParticle(int particleDirectory) {
            this.effect = GAssetsManager.getParticleEffect(particleDirectory);
        }

        private GParticle(ParticleEffect effect2) {
            this.effect = new ParticleEffect(effect2);
        }

        /* access modifiers changed from: private */
        public boolean isComplete() {
            return this.effect.isComplete();
        }

        /* access modifiers changed from: private */
        public ParticleEffect getEffect() {
            return this.effect;
        }

        /* access modifiers changed from: private */
        public BoundingBox getBoundingBox() {
            return this.effect.getBoundingBox();
        }

        public void setEmittersPosition(float x, float y) {
            this.effect.setPosition(x, y);
        }

        public void act(float delta2) {
            this.delta = delta2;
        }

        public void scaleEffect(float scaleX, float scaleY) {
            this.effect.scaleEffect(scaleX, scaleY);
        }

        public void draw(Batch batch, float parentAlpha) {
            if (isVisible()) {
                if (GameParticle.this.pool != null) {
                    GameParticle.this.setAdditive(false);
                    this.effect.setPosition(getX(), getY());
                }
                if (!GameParticle.this.isTransform()) {
                    this.effect.setPosition(getX(), getY());
                }
                this.effect.draw((SpriteBatch) batch, this.delta, parentAlpha);
                this.delta = Animation.CurveTimeline.LINEAR;
            }
        }

        public void dispose() {
            this.effect.dispose();
            GAssetsManager.unload(this.effect);
        }

        public void reset() {
            this.effect.reset();
        }
    }

    public void dispose() {
        this.particle.dispose();
    }

    public void reset() {
        this.particle.reset();
    }

    public void free() {
        if (this.pool != null) {
            this.pool.free(this);
        } else {
            remove();
        }
    }
}
