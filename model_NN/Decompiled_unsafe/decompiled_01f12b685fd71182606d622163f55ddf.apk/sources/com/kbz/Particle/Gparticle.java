package com.kbz.Particle;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Pool;
import com.kbz.AssetManger.GAssetsManager;
import com.kbz.ParticleOld.ParticleEffect;
import com.kbz.ParticleOld.ParticleEmitter;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Iterator;

public class Gparticle extends Group implements Disposable, Pool.Poolable {
    public static int count;
    public static boolean isDebug = false;
    private GParticle particle;
    /* access modifiers changed from: private */
    public GParticleSystem pool;

    public GParticle getParticle() {
        return this.particle;
    }

    public void setPool(GParticleSystem pool2) {
        this.pool = pool2;
    }

    public boolean isActive() {
        return this.pool != null;
    }

    public Gparticle(int effectFile) {
        this.particle = new GParticle(effectFile);
        addActor(this.particle);
        count++;
    }

    public Gparticle(ParticleEffect effect) {
        this.particle = new GParticle(effect);
        addActor(this.particle);
        count++;
    }

    public boolean isComplete() {
        return this.particle.isComplete();
    }

    public ParticleEffect getEffect() {
        return this.particle.getEffect();
    }

    public BoundingBox getBoundingBox() {
        return this.particle.getBoundingBox();
    }

    public void setLoop(boolean isLoop) {
        Iterator<ParticleEmitter> it = this.particle.getEffect().getEmitters().iterator();
        while (it.hasNext()) {
            it.next().setContinuous(isLoop);
        }
    }

    public void setAdditive(boolean additive) {
        Iterator<ParticleEmitter> it = this.particle.getEffect().getEmitters().iterator();
        while (it.hasNext()) {
            it.next().setAdditive(additive);
        }
    }

    public void setAttached(boolean attached) {
        Iterator<ParticleEmitter> it = this.particle.getEffect().getEmitters().iterator();
        while (it.hasNext()) {
            it.next().setAttached(attached);
        }
    }

    public void setEmittersPosition(float x, float y) {
        this.particle.setEmittersPosition(x, y);
    }

    public void setScale(float scale) {
        this.particle.setScale(scale);
    }

    public void scaleEffect(float scaleX, float scaleY) {
        this.particle.scaleEffect(scaleX, scaleY);
    }

    public class GParticle extends Actor implements Disposable, Pool.Poolable {
        float delta;
        private ParticleEffect effect;

        private GParticle(int particleDirectory) {
            this.effect = GAssetsManager.getParticleEffect(particleDirectory);
        }

        private GParticle(ParticleEffect effect2) {
            this.effect = new ParticleEffect(effect2);
        }

        /* access modifiers changed from: private */
        public boolean isComplete() {
            return this.effect.isComplete();
        }

        public ParticleEffect getEffect() {
            return this.effect;
        }

        /* access modifiers changed from: private */
        public BoundingBox getBoundingBox() {
            return this.effect.getBoundingBox();
        }

        public void setEmittersPosition(float x, float y) {
            this.effect.setPosition(x, y);
        }

        public void act(float delta2) {
            this.delta = delta2;
        }

        public void scaleEffect(float scaleX, float scaleY) {
            this.effect.scaleEffect(scaleX, scaleY);
        }

        public void draw(Batch batch, float parentAlpha) {
            if (Gparticle.this.pool != null && Gparticle.this.pool.isAdditiveGroup()) {
                Gparticle.this.setAdditive(false);
            }
            if (!Gparticle.this.isTransform()) {
                this.effect.setPosition(getX(), getY());
            }
            this.effect.draw((SpriteBatch) batch, this.delta, parentAlpha);
            this.delta = Animation.CurveTimeline.LINEAR;
        }

        public void dispose() {
            this.effect.dispose();
            GAssetsManager.unload(this.effect);
        }

        public void reset() {
            this.effect.reset();
        }
    }

    public void dispose() {
        this.particle.dispose();
    }

    public void reset() {
        this.particle.reset();
    }

    public void free() {
    }
}
