package com.kbz.Particle;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;
import com.kbz.AssetManger.GAssetsManager;
import com.kbz.ParticleOld.ParticleEffect;
import com.kbz.ParticleOld.ParticleEmitter;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.util.GameLayer;
import com.sg.util.GameStage;
import java.util.Iterator;

public class GParticleSystem extends Pool<GameParticle> implements Disposable, GameConstant {
    public static ObjectMap<Integer, GParticleSystem> particleManagerTable = new ObjectMap<>();
    private boolean autoFree = true;
    private Array<GameParticle> buff = new Array<>();
    private Group defaultGroup = GameLayer.ui.getGroup();
    private ParticleEffect effectSample;
    int freeMin;
    private boolean isAdditiveGroup = false;
    private boolean isLoop = false;
    private int particleName;

    static {
        registerParticleSystemUpdate();
        registerParticleSystemUpdate();
    }

    public static GParticleSystem getGParticleSystem(int particleName2) {
        return particleManagerTable.get(Integer.valueOf(particleName2));
    }

    public static void freeAll() {
        ObjectMap.Values<GParticleSystem> it = particleManagerTable.values().iterator();
        while (it.hasNext()) {
            GParticleSystem particleSystem = (GParticleSystem) it.next();
            if (particleSystem.autoFree) {
                particleSystem.clear();
            }
        }
    }

    public static void disposeAll() {
        ObjectMap.Keys<Integer> it = particleManagerTable.keys().iterator();
        while (it.hasNext()) {
            particleManagerTable.get((Integer) it.next()).dispose();
        }
        particleManagerTable.clear();
    }

    public Array<GameParticle> getBuff() {
        return this.buff;
    }

    public int getParticleName() {
        return this.particleName;
    }

    public void setAutoFree(boolean autoFree2) {
        this.autoFree = autoFree2;
    }

    public void setDefaultGroup(Group group) {
        this.defaultGroup = group;
    }

    public GParticleSystem(int particleIndex) {
        super(1, 5);
        if (!GAssetsManager.isLoaded(PAK_ASSETS.DATAPARTICAL_PATH + DATAPARTICAL_NAME[particleIndex])) {
            GAssetsManager.initParticle(GAssetsManager.getParticleEffect(particleIndex));
        }
        init(particleIndex, this.max);
    }

    public GParticleSystem(int particleIndex, int initialCapacity, int max) {
        super(initialCapacity, max);
        if (!GAssetsManager.isLoaded(PAK_ASSETS.DATAPARTICAL_PATH + DATAPARTICAL_NAME[particleIndex])) {
            System.err.println("GParticleSystem111111111111111111:" + DATAPARTICAL_NAME[particleIndex]);
            GAssetsManager.initParticle(GAssetsManager.getParticleEffect(particleIndex));
        }
        init(particleIndex, max);
    }

    private void init(int particleIndex, int max) {
        this.particleName = particleIndex;
        this.effectSample = GAssetsManager.getParticleEffect(this.particleName);
        particleManagerTable.put(Integer.valueOf(this.particleName), this);
        this.freeMin = max;
        for (int i = 0; i < max; i++) {
            free(newObject());
        }
    }

    public boolean isAdditiveGroup() {
        return this.isAdditiveGroup;
    }

    public void setToAdditiveGroup(boolean isEffectGroup) {
        this.isAdditiveGroup = isEffectGroup;
    }

    public void setLoop(boolean isLoop2) {
        Iterator<ParticleEmitter> it = this.effectSample.getEmitters().iterator();
        while (it.hasNext()) {
            it.next().setContinuous(isLoop2);
        }
    }

    public void update() {
        Iterator<GameParticle> it = this.buff.iterator();
        while (it.hasNext()) {
            GameParticle effect = it.next();
            if (effect.isComplete()) {
                if (this.isLoop) {
                    effect.reset();
                } else {
                    free(effect);
                }
            }
        }
    }

    private static void registerParticleSystemUpdate() {
        GameStage.registerUpdateService("particleSystemUpdate", new GameStage.GUpdateService() {
            public boolean update(float delta) {
                ObjectMap.Values<GParticleSystem> it = GParticleSystem.particleManagerTable.values().iterator();
                while (it.hasNext()) {
                    ((GParticleSystem) it.next()).update();
                }
                return false;
            }
        });
    }

    public GameParticle create() {
        return create(null, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
    }

    public GameParticle create(float x, float y) {
        return create(null, x, y);
    }

    public GameParticle create(Group group, float x, float y) {
        GameParticle effect = obtain();
        effect.setPool(this);
        this.buff.add(effect);
        effect.setPosition(x, y);
        effect.reset();
        if (group != null) {
            group.addActor(effect);
        }
        return effect;
    }

    public void clear() {
        for (int i = this.buff.size - 1; i >= 0; i--) {
            GameParticle effect = this.buff.get(i);
            effect.remove();
            free(effect);
        }
        this.buff.clear();
        super.clear();
    }

    public void dispose() {
        particleManagerTable.remove(Integer.valueOf(this.particleName));
        Iterator<GameParticle> it = this.buff.iterator();
        while (it.hasNext()) {
            GameParticle effect = it.next();
            effect.remove();
            free(effect);
            effect.dispose();
        }
        this.buff.clear();
        this.effectSample.dispose();
        this.effectSample = null;
    }

    /* access modifiers changed from: protected */
    public GameParticle newObject() {
        return new GameParticle(this.effectSample);
    }

    public GameParticle getNewObject() {
        GameParticle sprite = newObject();
        sprite.reset();
        return sprite;
    }

    public GameParticle obtain() {
        this.freeMin = Math.min(getFree(), this.freeMin);
        return (GameParticle) super.obtain();
    }

    public void free(GameParticle particleSprite) {
        if (particleSprite != null) {
            particleSprite.setName(null);
            particleSprite.remove();
            particleSprite.setScale(1.0f, 1.0f);
            particleSprite.setRotation(Animation.CurveTimeline.LINEAR);
            particleSprite.setPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
            particleSprite.setEmittersPosition(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR);
            particleSprite.setTransform(true);
            particleSprite.clearActions();
            particleSprite.clearListeners();
            particleSprite.scaleEffect(1.0f, 1.0f);
            this.buff.removeValue(particleSprite, true);
            particleSprite.setPool(null);
            super.free((Object) particleSprite);
        }
    }

    public void start(int x, int y, int layer) {
        GameStage.addActor(create((float) x, (float) y), layer);
    }

    public void start(int x, int y, Group group) {
        group.addActor(create((float) x, (float) y));
    }
}
