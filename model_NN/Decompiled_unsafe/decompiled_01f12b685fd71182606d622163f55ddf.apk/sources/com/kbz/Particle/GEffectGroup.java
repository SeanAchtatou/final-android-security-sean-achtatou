package com.kbz.Particle;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Group;

public class GEffectGroup extends Group {
    public GEffectGroup() {
        setTransform(false);
    }

    public void draw(Batch batch, float parentAlpha) {
        batch.setBlendFunction(770, 1);
        super.draw(batch, parentAlpha);
        batch.setBlendFunction(770, 771);
    }

    public void free() {
        clear();
        remove();
    }
}
