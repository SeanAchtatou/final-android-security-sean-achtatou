package com.kbz.Actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kbz.tools.Tools;
import com.sg.pak.GameConstant;
import com.sg.util.GameLayer;
import com.sg.util.GameStage;

public class ActorButton extends Actor implements GameConstant {
    int anchor;
    float clipH;
    float clipW;
    protected TextureRegion img;
    int imgIndex;
    boolean isFlipX;
    boolean isFlipY;
    boolean isWait = true;
    InputListener listener = new InputListener() {
        public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            if (!ActorButton.this.isWait) {
                return true;
            }
            ActorButton.this.addAction(Actions.sequence(Actions.run(new Runnable() {
                public void run() {
                    ActorButton.this.setTouchable(Touchable.disabled);
                    ActorButton.this.isWait = false;
                }
            }), Actions.delay(0.6f), Actions.run(new Runnable() {
                public void run() {
                    ActorButton.this.setTouchable(Touchable.enabled);
                    ActorButton.this.isWait = true;
                }
            })));
            ActorButton.this.setOrigin(ActorButton.this.getWidth() / 2.0f, ActorButton.this.getHeight() / 2.0f);
            ActorButton.this.setScale(0.95f);
            return true;
        }

        public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            super.touchUp(event, x, y, pointer, button);
            ActorButton.this.setScale(1.0f);
        }
    };

    public ActorButton(TextureRegion imRegion, String buttonName, Group group) {
        this.img = imRegion;
        setSize((float) this.img.getRegionWidth(), (float) this.img.getRegionHeight());
        group.addActor(this);
        setName(buttonName);
        addListener(this.listener);
    }

    public ActorButton(int imgID, String buttonName) {
        this.imgIndex = imgID;
        this.img = Tools.getImage(imgID);
        setSize((float) this.img.getRegionWidth(), (float) this.img.getRegionHeight());
        this.anchor = 12;
        setName(buttonName);
        addListener(this.listener);
    }

    public ActorButton(int imgID, String buttonName, int x, int y) {
        this.imgIndex = imgID;
        this.img = Tools.getImage(imgID);
        setSize((float) this.img.getRegionWidth(), (float) this.img.getRegionHeight());
        this.anchor = 12;
        setPosition((float) x, (float) y);
        setName(buttonName);
        addListener(this.listener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, int):void
     arg types: [int, int, int, int, int, int]
     candidates:
      com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
      com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, int):void */
    public ActorButton(int imgID, String buttonName, int x, int y, int biglayer) {
        init(imgID, x, y, false, 12, biglayer);
        setName(buttonName);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, int):void
     arg types: [int, int, int, int, int, int]
     candidates:
      com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
      com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, int):void */
    public ActorButton(int imgID, String buttonName, int x, int y, int biglayer, int anthor) {
        init(imgID, x, y, false, anthor, biglayer);
        setName(buttonName);
    }

    public void init(int imgID, int x, int y, boolean isMirror, int anthor, int lay) {
        this.imgIndex = imgID;
        this.img = Tools.getImage(imgID);
        setXY((float) x, (float) y, anthor);
        if (isMirror) {
            setFlipX(true);
        }
        GameStage.addActor(this, lay);
        addListener(this.listener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
     arg types: [int, int, int, int, int, com.sg.util.GameLayer]
     candidates:
      com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, int):void
      com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, com.sg.util.GameLayer):void */
    public ActorButton(int imgID, String buttonName, int x, int y, GameLayer layer) {
        init(imgID, x, y, false, 12, layer);
        setName(buttonName);
    }

    public void init(int imgID, int x, int y, boolean isMirror, int anthor, GameLayer layer) {
        this.imgIndex = imgID;
        this.img = Tools.getImage(imgID);
        setXY((float) x, (float) y, anthor);
        if (isMirror) {
            setFlipX(true);
        }
        GameStage.addActor(this, layer);
        addListener(this.listener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
     arg types: [int, int, int, int, int, com.badlogic.gdx.scenes.scene2d.Group]
     candidates:
      com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, int):void
      com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
      com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void */
    public ActorButton(int imgID, String buttonName, int x, int y, Group group) {
        init(imgID, x, y, false, 12, group);
        setName(buttonName);
    }

    public ActorButton(int imgID, String buttonName, int x, int y, boolean isMirror, Group group) {
        init(imgID, x, y, isMirror, 12, group);
        setName(buttonName);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
     arg types: [int, int, int, int, int, com.badlogic.gdx.scenes.scene2d.Group]
     candidates:
      com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, int):void
      com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
      com.kbz.Actors.ActorButton.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void */
    public ActorButton(int imgID, String buttonName, int x, int y, int anthor, Group group) {
        init(imgID, x, y, false, anthor, group);
        setName(buttonName);
    }

    public void init(int imgID, int x, int y, boolean isMirror, int anthor, Group group) {
        this.img = Tools.getImage(imgID);
        setXY((float) x, (float) y, anthor);
        if (isMirror) {
            setFlipX(true);
        }
        group.addActor(this);
        addListener(this.listener);
    }

    public void setFlip(boolean isFlipX2, boolean isFlipY2) {
        this.isFlipX = isFlipX2;
        this.isFlipY = isFlipY2;
    }

    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        this.img.flip(this.isFlipX, this.isFlipY);
        batch.draw(this.img, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        this.img.flip(this.isFlipX, this.isFlipY);
        setColor(color);
    }

    public void setFlipX(boolean flipx) {
        this.isFlipX = flipx;
    }

    public void setFlipY(boolean flipy) {
        this.isFlipY = flipy;
    }

    private void setXY(float x2, float y2, int anchor2) {
        setSize((float) this.img.getRegionWidth(), (float) this.img.getRegionHeight());
        this.anchor = anchor2;
        setPosition(x2, y2, anchor2);
        setOrigin(getWidth() / 2.0f, getHeight() / 2.0f);
    }

    public void clean() {
        GameStage.removeActor(this);
    }
}
