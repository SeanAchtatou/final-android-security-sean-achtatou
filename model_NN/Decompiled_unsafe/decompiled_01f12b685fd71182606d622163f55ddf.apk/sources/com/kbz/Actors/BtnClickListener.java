package com.kbz.Actors;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

public interface BtnClickListener {
    void onClick(InputEvent inputEvent);

    void onDown(InputEvent inputEvent);

    void onUp(InputEvent inputEvent);
}
