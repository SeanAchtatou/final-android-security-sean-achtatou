package com.kbz.Actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kbz.tools.Tools;
import com.sg.util.GameStage;

public class SimpleButton extends Actor {
    public static final int BOTTOM_DOWN = 1;
    public static final int BOTTOM_NO = 2;
    public static final int BOTTOM_NORMAL = 0;
    int curSpriteIndex = 0;
    int[] imgID;
    boolean isChange = true;
    boolean isFlipX;
    boolean isFlipY;
    /* access modifiers changed from: private */
    public BtnClickListener listener;
    private int mode = 0;
    private TextureRegion[] spriteTexture;
    private boolean state = false;

    public boolean getBtnState() {
        return this.state;
    }

    public SimpleButton(int x, int y, int layer, int[] imgid) {
        init(x, y, layer, imgid);
        create();
    }

    public void init(int x, int y, int layer, int[] imgid) {
        this.spriteTexture = new TextureRegion[imgid.length];
        this.imgID = new int[imgid.length];
        for (int i = 0; i < Math.max(1, imgid.length); i++) {
            this.imgID[i] = imgid[i];
            this.spriteTexture[i] = Tools.getImage(imgid[i]);
        }
        this.curSpriteIndex = 0;
        setWidth((float) Tools.getImage(this.imgID[this.curSpriteIndex]).getRegionWidth());
        setHeight((float) Tools.getImage(this.imgID[this.curSpriteIndex]).getRegionHeight());
        setPosition((float) x, (float) y);
        GameStage.addActor(this, layer);
    }

    public SimpleButton setListener(BtnClickListener listener2) {
        this.listener = listener2;
        return this;
    }

    public void setChange() {
        this.isChange = false;
    }

    public void setMode(int mod) {
        this.mode = mod;
    }

    public void create() {
        if (this.mode == 0) {
            addListener(new ActorGestureListener() {
                public void touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    if (SimpleButton.this.listener != null) {
                        SimpleButton.this.listener.onDown(event);
                    }
                    SimpleButton.this.setTexture(1);
                    SimpleButton.this.setMode(1);
                    System.err.println("aaaaaaaaaaa");
                    super.touchDown(event, x, y, pointer, button);
                }

                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    if (SimpleButton.this.listener != null) {
                        SimpleButton.this.listener.onUp(event);
                    }
                    if (SimpleButton.this.isChange) {
                        SimpleButton.this.setColor(Color.WHITE);
                    }
                    SimpleButton.this.setTexture(0);
                    SimpleButton.this.setMode(0);
                    System.err.println("bbbbbbbbbbb");
                    super.touchUp(event, x, y, pointer, button);
                }
            });
        } else if (this.mode == 1) {
            addListener(new ActorGestureListener() {
                public void touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    if (SimpleButton.this.listener != null) {
                        SimpleButton.this.listener.onDown(event);
                    } else {
                        System.out.println("请设置按下的贴图");
                    }
                    System.err.println("ccccccccccc");
                    super.touchDown(event, x, y, pointer, button);
                }

                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    if (SimpleButton.this.listener != null) {
                        SimpleButton.this.listener.onUp(event);
                    }
                    SimpleButton.this.setTexture(2);
                    SimpleButton.this.setMode(2);
                    System.err.println("dddddddddddd");
                    super.touchUp(event, x, y, pointer, button);
                }
            });
        }
        addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                if (SimpleButton.this.listener == null || event.getPointer() != 0) {
                    System.out.println("SimpleButton@ClickListener:请添加监听");
                } else {
                    SimpleButton.this.listener.onClick(event);
                }
                System.err.println("eeeeeeeeeeeeeeee");
                super.clicked(event, x, y);
            }
        });
    }

    public SimpleButton addListener(BtnClickListener listener2) {
        this.listener = listener2;
        return this;
    }

    public void setTexture(int curindex) {
        if (curindex >= 0 && curindex < this.spriteTexture.length && this.spriteTexture != null) {
            this.curSpriteIndex = curindex;
            setWidth((float) Tools.getImage(this.imgID[this.curSpriteIndex]).getRegionWidth());
            setHeight((float) Tools.getImage(this.imgID[this.curSpriteIndex]).getRegionHeight());
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        this.spriteTexture[this.curSpriteIndex].flip(this.isFlipX, this.isFlipY);
        batch.draw(this.spriteTexture[this.curSpriteIndex], getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        this.spriteTexture[this.curSpriteIndex].flip(this.isFlipX, this.isFlipY);
        setColor(color);
    }

    public void setFlipX(boolean flipx) {
        this.isFlipX = flipx;
    }

    public void setFlipY(boolean flipy) {
        this.isFlipY = flipy;
    }

    public void clean() {
        GameStage.removeActor(this);
    }
}
