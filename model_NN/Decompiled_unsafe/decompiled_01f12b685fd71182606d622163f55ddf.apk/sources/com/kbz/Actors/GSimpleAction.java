package com.kbz.Actors;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class GSimpleAction extends Action {
    private GActInterface actInterface;

    public interface GActInterface {
        boolean run(float f, Actor actor);
    }

    public boolean act(float delta) {
        return this.actInterface.run(delta, this.actor);
    }

    public static Action simpleAction(GActInterface actInterface2) {
        GSimpleAction action = (GSimpleAction) Actions.action(GSimpleAction.class);
        action.actInterface = actInterface2;
        return action;
    }
}
