package com.kbz.Actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.kbz.esotericsoftware.spine.Animation;
import com.kbz.tools.Tools;
import com.sg.util.GameStage;

public class ActorClipImage extends Actor {
    private int anchor;
    private int cliph;
    private int clipw;
    private int clipx;
    private int clipy;
    TextureRegion img;
    int imgId;
    private int x;
    private int y;

    public void setClip(float x2, float y2, float w, float h) {
        this.clipx = (int) x2;
        this.clipy = (int) y2;
        this.clipw = (int) w;
        this.cliph = (int) h;
    }

    public void setFullClip() {
        setClip(Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) this.img.getRegionWidth(), (float) this.img.getRegionHeight());
    }

    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        float u = this.img.getU();
        float v = this.img.getV();
        float u2 = this.img.getU2();
        float v2 = this.img.getV2();
        this.img.flip(false, true);
        this.img.setRegion(this.img.getRegionX() + this.clipx, this.img.getRegionY() + this.clipy, this.clipw, this.cliph);
        this.img.flip(false, true);
        if (this.anchor == 1) {
            batch.draw(this.img, getX() - ((float) (this.clipw / 2)), getY() - ((float) (this.cliph / 2)));
        } else {
            batch.draw(this.img, getX(), getY());
        }
        setColor(color);
        this.img.setRegion(u, v, u2, v2);
    }

    public ActorClipImage(int imgID, int x2, int y2, int biglay) {
        init(imgID, x2, y2);
        setPosition((float) x2, (float) y2);
        setFullClip();
        GameStage.addActor(this, biglay);
    }

    public ActorClipImage(int imgID, int x2, int y2, Group myGroup) {
        init(imgID, x2, y2);
        setPosition((float) x2, (float) y2);
        myGroup.addActor(this);
    }

    public ActorClipImage(int imgID, Group myGroup) {
        init(imgID, 0, 0);
        myGroup.addActor(this);
    }

    public ActorClipImage(int imgID, int x2, int y2, int anchor2, Group myGroup) {
        init(imgID, x2, y2);
        setPosition((float) x2, (float) y2);
        this.anchor = anchor2;
        myGroup.addActor(this);
    }

    public ActorClipImage(int imgID, int x2, int y2, int clipx2, int clipy2, int clipw2, int cliph2, int biglay) {
        init(imgID, x2, y2);
        setPosition((float) x2, (float) y2);
        setClip((float) clipx2, (float) clipy2, (float) clipw2, (float) cliph2);
        GameStage.addActor(this, biglay);
    }

    /* access modifiers changed from: package-private */
    public void init(int imgID, int x2, int y2) {
        this.imgId = imgID;
        this.img = Tools.getImage(imgID);
        setSize((float) this.img.getRegionWidth(), (float) this.img.getRegionHeight());
        this.x = x2;
        this.y = y2;
    }

    public void clean() {
        GameStage.removeActor(this);
    }
}
