package com.kbz.Actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.kbz.tools.Tools;
import com.sg.pak.GameConstant;
import com.sg.util.GameLayer;
import com.sg.util.GameStage;

public class ActorImage extends Actor implements GameConstant {
    int anchor;
    float clipH;
    float clipW;
    protected TextureRegion img;
    int imgIndex;
    boolean isFlipX;
    boolean isFlipY;

    public ActorImage(TextureRegion imRegion, Group group) {
        this.img = imRegion;
        setSize((float) this.img.getRegionWidth(), (float) this.img.getRegionHeight());
        setTouchable(Touchable.disabled);
        group.addActor(this);
    }

    public ActorImage(int imgID) {
        this.imgIndex = imgID;
        this.img = Tools.getImage(imgID);
        setSize((float) this.img.getRegionWidth(), (float) this.img.getRegionHeight());
        setTouchable(Touchable.disabled);
        this.anchor = 12;
    }

    public ActorImage(int imgID, int x, int y) {
        this.imgIndex = imgID;
        this.img = Tools.getImage(imgID);
        setSize((float) this.img.getRegionWidth(), (float) this.img.getRegionHeight());
        setTouchable(Touchable.disabled);
        this.anchor = 12;
        setPosition((float) x, (float) y);
    }

    public ActorImage(int imgID, int x, int y, int anthor) {
        this.imgIndex = imgID;
        this.img = Tools.getImage(imgID);
        setXY((float) x, (float) y, anthor);
        setTouchable(Touchable.disabled);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, int):void
     arg types: [int, int, int, int, int, int]
     candidates:
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
      com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, int):void
      com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, int):void */
    public ActorImage(int imgID, int x, int y, int biglayer, int anthor) {
        init(imgID, x, y, false, anthor, biglayer);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, int):void
     arg types: [java.lang.String, int, int, int, int, int]
     candidates:
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, int):void
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
      com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, int):void */
    public ActorImage(String imgPath, int x, int y, int biglayer, int anthor) {
        init(imgPath, x, y, false, anthor, biglayer);
    }

    public void init(int imgID, int x, int y, boolean isMirror, int anthor, int lay) {
        this.imgIndex = imgID;
        this.img = Tools.getImage(imgID);
        setTouchable(Touchable.disabled);
        setXY((float) x, (float) y, anthor);
        if (isMirror) {
            setFlipX(true);
        }
        GameStage.addActor(this, lay);
    }

    public void init(String imgPath, int x, int y, boolean isMirror, int anthor, int lay) {
        this.img = Tools.getImage(Tools.getImageId(imgPath));
        setTouchable(Touchable.disabled);
        setXY((float) x, (float) y, anthor);
        if (isMirror) {
            setFlipX(true);
        }
        GameStage.addActor(this, lay);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
     arg types: [int, int, int, int, int, com.sg.util.GameLayer]
     candidates:
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, int):void
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, int):void
      com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.sg.util.GameLayer):void */
    public ActorImage(int imgID, int x, int y, GameLayer layer) {
        init(imgID, x, y, false, 12, layer);
    }

    public void init(int imgID, int x, int y, boolean isMirror, int anthor, GameLayer layer) {
        this.imgIndex = imgID;
        this.img = Tools.getImage(imgID);
        setXY((float) x, (float) y, anthor);
        setTouchable(Touchable.disabled);
        if (isMirror) {
            setFlipX(true);
        }
        GameStage.addActor(this, layer);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
     arg types: [int, int, int, int, int, com.badlogic.gdx.scenes.scene2d.Group]
     candidates:
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, int):void
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
      com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, int):void
      com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void */
    public ActorImage(int imgID, int x, int y, Group group) {
        init(imgID, x, y, false, 12, group);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
     arg types: [java.lang.String, int, int, int, int, com.badlogic.gdx.scenes.scene2d.Group]
     candidates:
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, int):void
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
      com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, int):void
      com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void */
    public ActorImage(String imgPath, int x, int y, Group group) {
        init(imgPath, x, y, false, 12, group);
    }

    public ActorImage(int imgID, int x, int y, boolean isMirror, Group group) {
        init(imgID, x, y, isMirror, 12, group);
    }

    public ActorImage(String imgPath, int x, int y, boolean isMirror, Group group) {
        init(imgPath, x, y, isMirror, 12, group);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
     arg types: [int, int, int, int, int, com.badlogic.gdx.scenes.scene2d.Group]
     candidates:
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, int):void
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
      com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, int):void
      com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void */
    public ActorImage(int imgID, int x, int y, int anthor, Group group) {
        init(imgID, x, y, false, anthor, group);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
     arg types: [java.lang.String, int, int, int, int, com.badlogic.gdx.scenes.scene2d.Group]
     candidates:
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, int):void
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void
      com.kbz.Actors.ActorImage.init(int, int, int, boolean, int, com.sg.util.GameLayer):void
      com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, int):void
      com.kbz.Actors.ActorImage.init(java.lang.String, int, int, boolean, int, com.badlogic.gdx.scenes.scene2d.Group):void */
    public ActorImage(String imgPath, int x, int y, int anthor, Group group) {
        init(imgPath, x, y, false, anthor, group);
    }

    public void init(int imgID, int x, int y, boolean isMirror, int anthor, Group group) {
        this.img = Tools.getImage(imgID);
        setXY((float) x, (float) y, anthor);
        if (isMirror) {
            setFlipX(true);
        }
        setTouchable(Touchable.disabled);
        group.addActor(this);
    }

    public void init(String imgPath, int x, int y, boolean isMirror, int anthor, Group group) {
        this.img = Tools.getImage(Tools.getImageId(imgPath));
        setXY((float) x, (float) y, anthor);
        if (isMirror) {
            setFlipX(true);
        }
        setTouchable(Touchable.disabled);
        group.addActor(this);
    }

    public void setFlip(boolean isFlipX2, boolean isFlipY2) {
        this.isFlipX = isFlipX2;
        this.isFlipY = isFlipY2;
    }

    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        this.img.flip(this.isFlipX, this.isFlipY);
        Batch batch2 = batch;
        batch2.draw(this.img, this.clipH + getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        this.img.flip(this.isFlipX, this.isFlipY);
        setColor(color);
    }

    public void setFlipX(boolean flipx) {
        this.isFlipX = flipx;
    }

    public void setFlipY(boolean flipy) {
        this.isFlipY = flipy;
    }

    private void setXY(float x2, float y2, int anchor2) {
        setSize((float) this.img.getRegionWidth(), (float) this.img.getRegionHeight());
        this.anchor = anchor2;
        setPosition(x2, y2, anchor2);
        setOrigin(getWidth() / 2.0f, getHeight() / 2.0f);
    }

    public void clean() {
        GameStage.removeActor(this);
    }
}
