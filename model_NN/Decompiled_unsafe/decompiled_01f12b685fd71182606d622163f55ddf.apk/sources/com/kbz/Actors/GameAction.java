package com.kbz.Actors;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Array;
import com.kbz.Sound.GameSound;
import com.sg.GameEntry.GameMain;
import com.sg.util.GameScreen;
import java.util.HashMap;

public class GameAction {
    private static HashMap<Integer, Action> actions = new HashMap<>();
    static Array<Action> actions_2 = new Array<>();
    static Array<Integer> actions_2_repeatNum = new Array<>();
    private static int index = 0;

    public static void setScreen(final GameScreen screen) {
        addAction(Actions.run(new Runnable() {
            public void run() {
                GameMain.toScreen(screen);
            }
        }));
    }

    public static void playSound(final int id) {
        addAction(Actions.run(new Runnable() {
            public void run() {
                GameSound.playSound(id);
            }
        }));
    }

    public static void playMusic(final int id) {
        addAction(Actions.run(new Runnable() {
            public void run() {
                GameSound.playMusic(id);
            }
        }));
    }

    public static void addAction(Action act) {
        actions.put(Integer.valueOf(index), act);
        index++;
    }

    public static void addAction_2(Action act) {
        actions_2.add(act);
        actions_2_repeatNum.add(1);
    }

    public static void addAction_2(Action act, int num) {
        actions_2.add(act);
        actions_2_repeatNum.add(Integer.valueOf(num));
    }

    public static final void setShow(final Actor actor, final boolean isshow) {
        addAction(Actions.run(new Runnable() {
            public void run() {
                actor.setVisible(isshow);
            }
        }));
    }

    public static final void moveTo(float x, float y, float time) {
        addAction(Actions.moveTo(x, y, time));
    }

    public static final void moveTo(float x, float y, float time, Interpolation interpolation) {
        addAction(Actions.moveTo(x, y, time, interpolation));
    }

    public static final void moveBy(float x, float y, float time) {
        addAction(Actions.moveBy(x, y, time));
    }

    public static final void moveBy(float x, float y, float time, Interpolation interpolation) {
        addAction(Actions.moveBy(x, y, time, interpolation));
    }

    public static final void scaleTo(float x, float y, float time) {
        addAction(Actions.scaleTo(x, y, time));
    }

    public static final void scaleTo(float x, float y) {
        addAction(Actions.scaleTo(x, y));
    }

    public static final void scaleTo(float x, float y, float time, Interpolation interpolation) {
        addAction(Actions.scaleTo(x, y, time, interpolation));
    }

    public static final void scaleBy(float x, float y, float time) {
        addAction(Actions.scaleBy(x, y, time));
    }

    public static final void scaleBy(float x, float y, float time, Interpolation interpolation) {
        addAction(Actions.scaleBy(x, y, time, interpolation));
    }

    public static final void alpha(float alpha, float time, Interpolation interpolation) {
        addAction(Actions.alpha(alpha, time, interpolation));
    }

    public static final void alpha(float alpha, float time) {
        addAction(Actions.alpha(alpha, time));
    }

    public static final void alpha(float alpha) {
        addAction(Actions.alpha(alpha));
    }

    public static final void rotateTo(float rotation, float time) {
        addAction(Actions.rotateTo(rotation, time));
    }

    public static final void rotateTo(float rotation, float time, Interpolation interpolation) {
        addAction(Actions.rotateTo(rotation, time, interpolation));
    }

    public static final void rotateBy(float rotation, float time) {
        addAction(Actions.rotateBy(rotation, time));
    }

    public static final void rotateBy(float rotation, float time, Interpolation interpolation) {
        addAction(Actions.rotateBy(rotation, time, interpolation));
    }

    public static final void delay(float time) {
        addAction(Actions.delay(time));
    }

    public static final void setPos(Actor actor, float x, float y, int anthor) {
        actor.setPosition(x, y);
    }

    public static final void setPos(float x, float y, Actor actor) {
        actor.setPosition(x, y);
    }

    public static final void clean() {
        index = 0;
        actions = new HashMap<>();
        actions_2 = new Array<>();
        actions_2_repeatNum = new Array<>();
    }

    public static final Action setAction(int[] kbz, boolean isSequence) {
        Action temp;
        Action[] tempActions = new Action[kbz.length];
        for (int i = 0; i < kbz.length; i++) {
            tempActions[i] = actions.get(Integer.valueOf(kbz[i]));
        }
        if (isSequence) {
            temp = Actions.sequence(tempActions);
        } else {
            temp = Actions.parallel(tempActions);
        }
        addAction_2(temp);
        return temp;
    }

    public static final Action setAction(int[] kbz, boolean isSequence, int num) {
        Action temp;
        Action[] tempActions = new Action[kbz.length];
        for (int i = 0; i < kbz.length; i++) {
            tempActions[i] = actions.get(Integer.valueOf(kbz[i]));
        }
        if (isSequence) {
            temp = Actions.sequence(tempActions);
        } else {
            temp = Actions.parallel(tempActions);
        }
        Action temp2 = Actions.repeat(num, temp);
        addAction_2(temp2);
        return temp2;
    }

    public static final void startAction(Actor actor, boolean isSequence, int repeatNum) {
        Action temp;
        if (actions_2.size < 1) {
            startAction_2(actor, isSequence, repeatNum);
            return;
        }
        Action[] tempActions = (Action[]) actions_2.toArray(Action.class);
        if (isSequence) {
            temp = Actions.sequence(tempActions);
        } else {
            temp = Actions.parallel(tempActions);
        }
        actor.addAction(Actions.repeat(repeatNum, temp));
        clean();
    }

    private static final void startAction_2(Actor actor, boolean isSequence, int repeatNum) {
        Action temp;
        Action[] tempActions = new Action[actions.size()];
        for (int i = 0; i < actions.size(); i++) {
            tempActions[i] = actions.get(Integer.valueOf(i));
        }
        if (isSequence) {
            temp = Actions.sequence(tempActions);
        } else {
            temp = Actions.parallel(tempActions);
        }
        actor.addAction(Actions.repeat(repeatNum, temp));
    }
}
