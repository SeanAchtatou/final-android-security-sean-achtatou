package com.kbz.Actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

public class GClipGroup extends Group {
    int clipH;
    int clipW;
    int clipX;
    int clipY;

    public boolean isClipping() {
        return (this.clipW == 0 || this.clipH == 0) ? false : true;
    }

    public void resetClipArea() {
        this.clipX = 0;
        this.clipY = 0;
        this.clipW = 0;
        this.clipH = 0;
    }

    public void setClipArea(int x, int y, int w, int h) {
        this.clipX = x;
        this.clipY = y;
        this.clipW = w;
        this.clipH = h;
    }

    public void begin() {
        if (isClipping()) {
            clipBegin(((float) this.clipX) + getX(), ((float) this.clipY) + getY(), (float) this.clipW, (float) this.clipH);
        }
    }

    public void end() {
        if (isClipping()) {
            clipEnd();
        }
    }

    public void draw(Batch batch, float parentAlpha) {
        begin();
        super.draw(batch, parentAlpha);
        end();
    }

    public Actor hit(float x, float y, boolean touchable) {
        if (x < ((float) this.clipX) || x > ((float) (this.clipX + this.clipW)) || y < ((float) this.clipY) || y > ((float) (this.clipY + this.clipH))) {
            return null;
        }
        return super.hit(x, y, touchable);
    }
}
