package com.kbz.Actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Colors;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.kbz.esotericsoftware.spine.Animation;
import com.sg.pak.GameConstant;
import com.sg.pak.PAK_ASSETS;
import com.sg.util.GameStage;

public class LoadFont implements GameConstant {
    private static BitmapFont BitFont;
    private static BitmapFont.BitmapFontData BitmapFontData;
    public static Texture.TextureFilter magFilter = Texture.TextureFilter.Linear;
    public static Texture.TextureFilter minFilter = Texture.TextureFilter.Linear;
    int anthor;
    Label txtlLabel;

    /* access modifiers changed from: package-private */
    public BitmapFont.BitmapFontData getBitmapFontData() {
        return BitmapFontData;
    }

    public static BitmapFont getBitmapFont() {
        return BitFont;
    }

    public Label getLable() {
        return this.txtlLabel;
    }

    public void setWrap(boolean warp) {
        this.txtlLabel.setWrap(warp);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.TextureRegion>, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.graphics.Pixmap, boolean):void
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void */
    public static void initFont() {
        BitmapFontData = new BitmapFont.BitmapFontData(Gdx.files.internal("font/font1.fnt"), true);
        BitFont = new BitmapFont(BitmapFontData, new TextureRegion(new Texture(Gdx.files.internal("font/font1_0.png"), false)), true);
        initColor();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.TextureRegion>, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.graphics.Pixmap, boolean):void
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void */
    public static void initFontMY() {
        BitmapFontData = new BitmapFont.BitmapFontData(Gdx.files.internal("font/font1.fnt"), true);
        BitFont = new BitmapFont(BitmapFontData, new TextureRegion(new Texture(Gdx.files.internal("font/font1_0.png"), false)), true);
        initColor();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
     arg types: [com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, int]
     candidates:
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.files.FileHandle, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.files.FileHandle, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.utils.Array<com.badlogic.gdx.graphics.g2d.TextureRegion>, boolean):void
      com.badlogic.gdx.graphics.g2d.BitmapFont.<init>(com.badlogic.gdx.graphics.g2d.BitmapFont$BitmapFontData, com.badlogic.gdx.graphics.g2d.TextureRegion, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void
     arg types: [com.badlogic.gdx.files.FileHandle, int]
     candidates:
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.graphics.Pixmap, boolean):void
      com.badlogic.gdx.graphics.Texture.<init>(com.badlogic.gdx.files.FileHandle, boolean):void */
    public static void initFontCu() {
        BitmapFontData = new BitmapFont.BitmapFontData(Gdx.files.internal("font/font2.fnt"), true);
        BitFont = new BitmapFont(BitmapFontData, new TextureRegion(new Texture(Gdx.files.internal("font/font2_0.png"), false)), true);
        initColor();
    }

    private static void initColor() {
        Colors.put("BtitleColour", new Color(getColor(0, 255, 0)));
        Colors.put("StitleColour", new Color(getColor(0, 186, 255)));
        Colors.put("contentColour", new Color(getColor(255, 255, 255)));
        Colors.put("keyColour", new Color(getColor(0, 252, 0)));
        Colors.put("timeColour", new Color(getColor(255, PAK_ASSETS.IMG_SHUOMINGZI01, 0)));
        Colors.put("giftColour", new Color(getColor(0, 252, 0)));
        Colors.put("otherColour", new Color(getColor(192, 0, 255)));
    }

    private static int getColor(int r, int g, int b) {
        return (r << 24) | (g << 16) | (b << 8) | 255;
    }

    public void clean() {
        GameStage.removeActor(this.txtlLabel);
    }

    /* access modifiers changed from: package-private */
    public void initLableStr(int x, int y, String str, int anthor2) {
        this.anthor = changeAnthor(anthor2);
        BitmapFont.BitmapFontData fontData = getBitmapFontData();
        BitmapFont bitmapFont = getBitmapFont();
        setTextureFilter(bitmapFont.getRegion().getTexture());
        this.txtlLabel = new Label(str, new Label.LabelStyle(bitmapFont, Color.WHITE));
        fontData.markupEnabled = true;
        this.txtlLabel.setHeight(Animation.CurveTimeline.LINEAR);
        this.txtlLabel.setColor(Color.WHITE);
        this.txtlLabel.setAlignment(this.anthor);
        this.txtlLabel.setWrap(true);
        this.txtlLabel.setPosition((float) x, (float) y, this.anthor);
    }

    public static void setTextureFilter(Texture texture) {
        texture.setFilter(minFilter, magFilter);
    }

    /* access modifiers changed from: package-private */
    public void initLableStrCu(int x, int y, String str, int anthor2) {
        this.anthor = changeAnthor(anthor2);
        BitmapFont.BitmapFontData fontData = getBitmapFontData();
        initFontCu();
        this.txtlLabel = new Label(str, new Label.LabelStyle(getBitmapFont(), Color.WHITE));
        fontData.markupEnabled = true;
        this.txtlLabel.setHeight(Animation.CurveTimeline.LINEAR);
        this.txtlLabel.setColor(Color.WHITE);
        this.txtlLabel.setAlignment(this.anthor);
        this.txtlLabel.setWrap(true);
        this.txtlLabel.setPosition((float) x, (float) y, this.anthor);
    }

    public void setText(String str) {
        BitmapFont.BitmapFontData fontData = getBitmapFontData();
        this.txtlLabel = new Label(str, new Label.LabelStyle(getBitmapFont(), Color.WHITE));
        fontData.markupEnabled = true;
        this.txtlLabel.setHeight(Animation.CurveTimeline.LINEAR);
        this.txtlLabel.setColor(Color.WHITE);
        this.txtlLabel.setAlignment(this.anthor);
        this.txtlLabel.setWrap(true);
    }

    /* access modifiers changed from: package-private */
    public int changeAnthor(int anthor2) {
        switch (anthor2) {
            case 10:
                return 12;
            case 12:
                return 10;
            case 18:
                return 20;
            case 20:
                return 18;
            default:
                return anthor2;
        }
    }
}
