package com.kbz.Actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.sg.pak.GameConstant;
import com.sg.util.GameStage;

public class ActorText extends LoadFont implements GameConstant {
    public ActorText(String text, int x, int y, int anthor, int layer) {
        initLableStr(x, y, text, anthor);
        GameStage.addActor(getLable(), layer);
    }

    public ActorText(String text, int x, int y, int anthor, Group group) {
        initLableStr(x, y, text, anthor);
        group.addActor(getLable());
    }

    public ActorText(String text, int x, int y, int layer) {
        initLableStr(x, y, text, 10);
        GameStage.addActor(getLable(), layer);
    }

    public ActorText(String text, int x, int y, Group group) {
        initLableStr(x, y, text, 12);
        group.addActor(getLable());
    }

    public void setColor(Color color) {
        this.txtlLabel.setColor(color);
    }

    public void setFontScaleXY(float scaleXY) {
        this.txtlLabel.setFontScale(scaleXY);
    }

    public void setPosition(float x, float y) {
        this.txtlLabel.setPosition(x, y, this.anthor);
    }

    public void setPosition(float x, float y, int anthor) {
        this.txtlLabel.setPosition(x, y, anthor);
    }

    public void setText(String newText) {
        this.txtlLabel.setText(newText);
    }

    public void setWidth(int width) {
        getLable().setWidth((float) width);
    }
}
