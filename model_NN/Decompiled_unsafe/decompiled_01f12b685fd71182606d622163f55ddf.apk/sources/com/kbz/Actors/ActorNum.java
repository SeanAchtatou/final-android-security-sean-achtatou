package com.kbz.Actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.kbz.tools.Tools;
import com.sg.pak.GameConstant;
import com.sg.util.GameStage;
import java.lang.reflect.Array;

public class ActorNum extends Actor implements GameConstant {
    final byte STYLE_1 = 0;
    final byte STYLE_2 = 1;
    final byte STYLE_3 = 2;
    final byte STYLE_4 = 3;
    byte anthor;
    int[] digitCounter = {0, 0};
    int[][] digits = ((int[][]) Array.newInstance(Integer.TYPE, 2, 10));
    int imgIndexFirst;
    int maxdigit;
    int num;
    int numHeight;
    int[] numLen = {0, 0};
    int numSpace;
    int numStyle;
    int numWidth;
    int num_2;
    int numberIndex;
    private TextureRegion[] spriteTexture;
    TextureRegion[] spriteTexture2;

    public ActorNum(int NumberIndex, int number, byte anthor2) {
        init(NumberIndex, number, anthor2);
    }

    public ActorNum(int NumberIndex, int number) {
        init(NumberIndex, number, this.anthor);
    }

    private void init(int NumberIndex, int number) {
        this.spriteTexture = new TextureRegion[11];
        this.numberIndex = NumberIndex;
        setImgAndWidth(NumberIndex, number);
        setNum(number);
        for (int i = 0; i < 11; i++) {
            this.spriteTexture[i] = Tools.getImage(this.imgIndexFirst + i);
        }
        this.numStyle = 0;
    }

    public ActorNum(int NumberIndex, int number, int x, int y, int layer) {
        init(NumberIndex, number);
        setPosition((float) x, (float) y);
        GameStage.addActor(this, layer);
    }

    public ActorNum(int NumberIndex, int number, int x, int y, Group myGroup) {
        init(NumberIndex, number);
        setPosition((float) x, (float) y);
        myGroup.addActor(this);
    }

    public ActorNum(int NumberIndex, int number, int x, int y, int layer, byte anthor2) {
        init(NumberIndex, number, anthor2);
        setPosition((float) x, (float) y);
        GameStage.addActor(this, layer);
    }

    public ActorNum(int NumberIndex, int number, int x, int y, Group myGroup, byte anthor2) {
        init(NumberIndex, number, anthor2);
        setPosition((float) x, (float) y);
        myGroup.addActor(this);
    }

    private void init(int NumberIndex, int number, byte anthor2) {
        this.spriteTexture = new TextureRegion[11];
        this.numberIndex = NumberIndex;
        setImgAndWidth(NumberIndex, number);
        setNum(number);
        for (int i = 0; i < 11; i++) {
            this.spriteTexture[i] = Tools.getImage(this.imgIndexFirst + i);
        }
        this.numStyle = 0;
        this.anthor = anthor2;
    }

    public ActorNum(int NumberIndex, int number, int maxdigit2, int x, int y, int layer) {
        init(NumberIndex, number, maxdigit2);
        setPosition((float) x, (float) y);
        GameStage.addActor(this, layer);
    }

    public ActorNum(int NumberIndex, int number, int maxdigit2, int x, int y, int layer, byte anthor2) {
        init(NumberIndex, number, maxdigit2);
        this.anthor = anthor2;
        setPosition((float) x, (float) y);
        GameStage.addActor(this, layer);
    }

    public ActorNum(int NumberIndex, int number, int maxdigit2, int x, int y, Group myGroup) {
        init(NumberIndex, number, maxdigit2);
        setPosition((float) x, (float) y);
        myGroup.addActor(this);
    }

    public ActorNum(int NumberIndex, int number, int maxdigit2, int x, int y, Group myGroup, byte anthor2) {
        init(NumberIndex, number, maxdigit2);
        setPosition((float) x, (float) y);
        this.anthor = anthor2;
        myGroup.addActor(this);
    }

    private void init(int NumberIndex, int number, int maxdigit2) {
        this.maxdigit = maxdigit2;
        this.spriteTexture = new TextureRegion[11];
        this.numberIndex = NumberIndex;
        setImgAndWidth(NumberIndex, number);
        setNum(number);
        for (int i = 0; i < 11; i++) {
            this.spriteTexture[i] = Tools.getImage(this.imgIndexFirst + i);
        }
        this.numStyle = 1;
    }

    public ActorNum(int NumberIndex, int number, int number2, int maxdigit2, int x, int y, int layer) {
        init(NumberIndex, number, number2, maxdigit2);
        setPosition((float) x, (float) y);
        GameStage.addActor(this, layer);
    }

    public ActorNum(int NumberIndex, int number, int number2, int maxdigit2, int x, int y, Group myGroup) {
        init(NumberIndex, number, number2, maxdigit2);
        setPosition((float) x, (float) y);
        myGroup.addActor(this);
    }

    private void init(int NumberIndex, int number, int number2, int maxdigit2) {
        this.maxdigit = maxdigit2;
        this.spriteTexture = new TextureRegion[11];
        this.numberIndex = NumberIndex;
        setImgAndWidth(NumberIndex, number);
        setNum(number, number2);
        for (int i = 0; i < 11; i++) {
            this.spriteTexture[i] = Tools.getImage(this.imgIndexFirst + i);
        }
        this.numStyle = 2;
    }

    public ActorNum(int NumberIndex, int NumberIndex2, int number, int number2, int maxdigit2, int x, int y, int layer) {
        init(NumberIndex, NumberIndex2, number, number2, maxdigit2);
        setPosition((float) x, (float) y);
        GameStage.addActor(this, layer);
    }

    public ActorNum(int NumberIndex, int NumberIndex2, int number, int number2, int maxdigit2, int x, int y, Group myGroup) {
        init(NumberIndex, NumberIndex2, number, number2, maxdigit2);
        setPosition((float) x, (float) y);
        myGroup.addActor(this);
    }

    private void init(int NumberIndex, int NumberIndex2, int number, int number2, int maxdigit2) {
        this.maxdigit = maxdigit2;
        this.spriteTexture = new TextureRegion[11];
        this.numberIndex = NumberIndex;
        setImgAndWidth(NumberIndex, number);
        setNum(number, number2);
        for (int i = 0; i < 11; i++) {
            this.spriteTexture[i] = Tools.getImage(this.imgIndexFirst + i);
        }
        this.spriteTexture2 = new TextureRegion[11];
        this.imgIndexFirst = numImage[NumberIndex2][3];
        for (int i2 = 0; i2 < 11; i2++) {
            this.spriteTexture2[i2] = Tools.getImage(this.imgIndexFirst + i2);
        }
        this.numStyle = 3;
    }

    private void setImgAndWidth(int imagefirstId, int number) {
        this.numWidth = numImage[imagefirstId][0];
        this.numHeight = numImage[imagefirstId][1];
        this.numSpace = numImage[imagefirstId][2];
        this.imgIndexFirst = numImage[imagefirstId][3];
        setWidth((float) this.numWidth);
        setHeight((float) this.numHeight);
        this.numWidth += this.numSpace;
    }

    public int getNum() {
        return this.num;
    }

    public int getNum_2() {
        return this.num_2;
    }

    public void setNum(int number) {
        this.num = number;
        this.digitCounter[0] = 0;
        this.digits[0] = new int[10];
        do {
            this.digits[0][this.digitCounter[0]] = number % 10;
            number /= 10;
            int[] iArr = this.digitCounter;
            iArr[0] = iArr[0] + 1;
        } while (number > 0);
    }

    public void setNum(int number, int number2) {
        this.num = number;
        this.num_2 = number2;
        this.digitCounter[0] = 0;
        this.digits[0] = new int[10];
        do {
            this.digits[0][this.digitCounter[0]] = number % 10;
            number /= 10;
            int[] iArr = this.digitCounter;
            iArr[0] = iArr[0] + 1;
        } while (number > 0);
        this.digitCounter[1] = 0;
        this.digits[1] = new int[10];
        do {
            this.digits[1][this.digitCounter[1]] = number2 % 10;
            number2 /= 10;
            int[] iArr2 = this.digitCounter;
            iArr2[1] = iArr2[1] + 1;
        } while (number2 > 0);
    }

    public void draw(Batch batch, float parentAlpha) {
        switch (this.numStyle) {
            case 0:
                switch (this.anthor) {
                    case 1:
                        drawNumber(batch, parentAlpha, this.digitCounter[0], ((-this.digitCounter[0]) * this.numWidth) / 2);
                        return;
                    case 2:
                        drawNumber(batch, parentAlpha, this.digitCounter[0], (-this.digitCounter[0]) * this.numWidth);
                        return;
                    default:
                        drawNumber(batch, parentAlpha, this.digitCounter[0], 0);
                        return;
                }
            case 1:
                switch (this.anthor) {
                    case 1:
                        drawNumber(batch, parentAlpha, this.maxdigit, ((-this.maxdigit) * this.numWidth) / 2);
                        return;
                    case 2:
                        drawNumber(batch, parentAlpha, this.maxdigit, (-this.maxdigit) * this.numWidth);
                        return;
                    default:
                        drawNumber(batch, parentAlpha, this.maxdigit, 0);
                        return;
                }
            case 2:
                if (this.maxdigit == 0) {
                    drawNumber_4(batch, parentAlpha, this.digitCounter[0], this.digitCounter[1], false);
                    return;
                }
                drawNumber_4(batch, parentAlpha, this.maxdigit, this.maxdigit, false);
                return;
            case 3:
                if (this.maxdigit == 0) {
                    drawNumber_4(batch, parentAlpha, this.digitCounter[0], this.digitCounter[1], true);
                    return;
                }
                drawNumber_4(batch, parentAlpha, this.maxdigit, this.maxdigit, true);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void drawNumber(Batch batch, float parentAlpha, int len, int x) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        for (int i = 0; i < len; i++) {
            batch.draw(this.spriteTexture[this.digits[0][i]], (((((float) x) + getX()) - ((float) (this.numWidth * i))) + ((float) (this.numWidth * len))) - ((float) this.numWidth), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        }
    }

    /* access modifiers changed from: package-private */
    public void drawNumber_4(Batch batch, float parentAlpha, int len1, int len2, boolean isTwoTexture) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        for (int i = 0; i < len1; i++) {
            batch.draw(this.spriteTexture[this.digits[0][i]], (getX() - ((float) (this.numWidth * i))) - ((float) this.numWidth), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        }
        batch.draw(this.spriteTexture[10], getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        if (isTwoTexture) {
            for (int i2 = 0; i2 < len2; i2++) {
                Batch batch2 = batch;
                batch2.draw(this.spriteTexture2[this.digits[1][i2]], ((float) (this.numWidth * len2)) + (getX() - ((float) (this.numWidth * i2))), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
            }
            return;
        }
        for (int i3 = 0; i3 < len2; i3++) {
            Batch batch3 = batch;
            batch3.draw(this.spriteTexture[this.digits[1][i3]], ((float) (this.numWidth * len2)) + (getX() - ((float) (this.numWidth * i3))), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        }
    }

    public void clean() {
        GameStage.removeActor(this);
    }
}
