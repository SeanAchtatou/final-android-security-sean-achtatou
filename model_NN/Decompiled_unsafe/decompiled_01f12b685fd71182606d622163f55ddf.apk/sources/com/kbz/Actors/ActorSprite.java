package com.kbz.Actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.kbz.tools.Tools;
import com.sg.pak.GameConstant;
import com.sg.util.GameStage;

public class ActorSprite extends Actor implements GameConstant {
    int curSpriteIndex = 0;
    int delayTime = 1;
    int[] imgID;
    int index;
    int index2;
    boolean isFlipX;
    boolean isFlipY;
    private TextureRegion[] spriteTexture;

    public ActorSprite() {
    }

    public ActorSprite(int... imgid) {
        create(imgid);
    }

    public ActorSprite(int x, int y, Group group, int... imgid) {
        create(imgid);
        setPosition((float) x, (float) y);
        group.addActor(this);
    }

    public ActorSprite(int x, int y, int anthor, Group group, int... imgid) {
        create(imgid);
        setPosition((float) x, (float) y, anthor);
        group.addActor(this);
    }

    public ActorSprite(int x, int y, int layer, int... imgid) {
        init(x, y, 12, layer, imgid);
    }

    public ActorSprite(int x, int y, int anthor, int layer, int... imgid) {
        init(x, y, anthor, layer, imgid);
    }

    public void init(int x, int y, int anthor, int layer, int... imgid) {
        create(imgid);
        setPosition((float) x, (float) y, anthor);
        GameStage.addActor(this, layer);
    }

    public void init(int x, int y, int layer, int... imgid) {
        create(imgid);
        setPosition((float) x, (float) y);
        GameStage.addActor(this, layer);
    }

    /* access modifiers changed from: package-private */
    public void create(int... imgid) {
        this.spriteTexture = new TextureRegion[imgid.length];
        this.imgID = new int[imgid.length];
        for (int i = 0; i < Math.max(1, imgid.length); i++) {
            this.imgID[i] = imgid[i];
            this.spriteTexture[i] = Tools.getImage(imgid[i]);
        }
        this.curSpriteIndex = 0;
        setWidth((float) Tools.getImage(this.imgID[this.curSpriteIndex]).getRegionWidth());
        setHeight((float) Tools.getImage(this.imgID[this.curSpriteIndex]).getRegionHeight());
    }

    public void setTexture(int curindex) {
        if (curindex >= 0 && this.spriteTexture != null && curindex < this.spriteTexture.length) {
            this.curSpriteIndex = curindex;
            setWidth((float) Tools.getImage(this.imgID[this.curSpriteIndex]).getRegionWidth());
            setHeight((float) Tools.getImage(this.imgID[this.curSpriteIndex]).getRegionHeight());
        }
    }

    public int getCurTextureID() {
        return this.curSpriteIndex;
    }

    public void updata() {
        this.curSpriteIndex = this.index2 / this.delayTime;
        int i = this.index2 + 1;
        this.index2 = i;
        if (i >= this.imgID.length * this.delayTime) {
            this.index = 0;
            this.index2 = 0;
        }
    }

    public void setDelay(int delay) {
        this.delayTime = delay;
    }

    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        this.spriteTexture[this.curSpriteIndex].flip(this.isFlipX, this.isFlipY);
        batch.draw(this.spriteTexture[this.curSpriteIndex], getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
        this.spriteTexture[this.curSpriteIndex].flip(this.isFlipX, this.isFlipY);
        setColor(color);
    }

    public void setFlipX(boolean flipx) {
        this.isFlipX = flipx;
    }

    public void setFlipY(boolean flipy) {
        this.isFlipY = flipy;
    }

    public void clean() {
        GameStage.removeActor(this);
    }
}
