package com.kbz.Actors;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.kbz.AssetManger.GAssetsManager;
import com.kbz.tools.DebugTools;

public class LoadingGroup extends Group {
    private boolean isBegin = true;
    private boolean isEnd = false;
    private LoadingListener listener;

    public interface LoadingListener {
        void begin();

        void end();

        void update(float f);
    }

    public LoadingGroup() {
        init();
    }

    public void addLoadingListener(LoadingListener listener2) {
        this.listener = listener2;
    }

    private void init() {
        this.isBegin = true;
        this.isEnd = false;
        setTransform(false);
        setTouchable(Touchable.childrenOnly);
    }

    public void act(float delta) {
        super.act(delta);
        if (this.isBegin) {
            System.out.println("加载开始");
            DebugTools.timeBegin();
            this.isBegin = false;
            this.listener.begin();
        }
        if (!this.isEnd) {
            GAssetsManager.update();
            this.listener.update(GAssetsManager.getProgress());
            if (GAssetsManager.isFinished() && !this.isEnd) {
                System.out.println("加载结束");
                DebugTools.timeEnd();
                this.isEnd = true;
                this.listener.end();
            }
        }
    }
}
