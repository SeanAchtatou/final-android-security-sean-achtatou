package com.kbz.Actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kbz.tools.Tools;
import com.sg.util.GameStage;

public class ActorPolygon extends Actor {
    public static final int C1 = 2;
    public static final int C2 = 7;
    public static final int C3 = 12;
    public static final int C4 = 17;
    public static final int U1 = 3;
    public static final int U2 = 8;
    public static final int U3 = 13;
    public static final int U4 = 18;
    public static final int V1 = 4;
    public static final int V2 = 9;
    public static final int V3 = 14;
    public static final int V4 = 19;
    public static final int X1 = 0;
    public static final int X2 = 5;
    public static final int X3 = 10;
    public static final int X4 = 15;
    public static final int Y1 = 1;
    public static final int Y2 = 6;
    public static final int Y3 = 11;
    public static final int Y4 = 16;
    TextureRegion region;
    float tx1;
    float tx2;
    float tx3;
    float tx4;
    float ty1;
    float ty2;
    float ty3;
    float ty4;
    float[] vertices = new float[20];

    public ActorPolygon(int imgId) {
    }

    public ActorPolygon(int imgID, int x, int y, int lay) {
        this.region = Tools.getImage(imgID);
        setPosition((float) x, (float) y);
        GameStage.addActor(this, lay);
    }

    public void setVertices(float... verticesIn) {
        this.tx1 = verticesIn[0];
        this.ty1 = verticesIn[1];
        this.tx2 = verticesIn[2];
        this.ty2 = verticesIn[3];
        this.tx3 = verticesIn[4];
        this.ty3 = verticesIn[5];
        this.tx4 = verticesIn[6];
        this.ty4 = verticesIn[7];
    }

    public void draw(Batch batch, float parentAlpha) {
        float u1 = this.region.getU();
        float v1 = this.region.getV2();
        float u2 = this.region.getU2();
        float v2 = this.region.getV();
        int w = this.region.getRegionWidth();
        int h = this.region.getRegionHeight();
        float x1 = getX();
        float y1 = getY();
        float x2 = x1 + ((float) w);
        float y2 = y1 + ((float) h);
        Color batchColor = batch.getColor();
        float color = Color.toFloatBits(batchColor.r, batchColor.g, batchColor.b, batchColor.a);
        this.vertices[0] = this.tx1 + x1;
        this.vertices[1] = this.ty1 + y1;
        this.vertices[2] = color;
        this.vertices[3] = u1;
        this.vertices[4] = v1;
        this.vertices[5] = this.tx2 + x1;
        this.vertices[6] = this.ty2 + y2;
        this.vertices[7] = color;
        this.vertices[8] = u1;
        this.vertices[9] = v2;
        this.vertices[10] = this.tx3 + x2;
        this.vertices[11] = this.ty3 + y2;
        this.vertices[12] = color;
        this.vertices[13] = u2;
        this.vertices[14] = v2;
        this.vertices[15] = this.tx4 + x2;
        this.vertices[16] = this.ty4 + y1;
        this.vertices[17] = color;
        this.vertices[18] = u2;
        this.vertices[19] = v1;
        batch.draw(this.region.getTexture(), this.vertices, 0, this.vertices.length);
    }
}
