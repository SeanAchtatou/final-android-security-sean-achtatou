package com.kbz.Actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kbz.tools.Tools;
import com.sg.pak.GameConstant;
import com.sg.util.GameStage;
import java.util.HashMap;

public class AcotrNumSprite extends Actor implements GameConstant {
    private byte anthor;
    private HashMap<Character, TextureRegion> charMap = new HashMap<>();
    private int numH;
    private String numStr = "0";
    private int numW;
    private int spacing;

    public AcotrNumSprite(int NumberIndex, String numStr2, int x, int y, byte anthor2, int layer) {
        setAtlasRegion(NumberIndex);
        setNum(numStr2);
        GameStage.addActor(this, layer);
        setPosition((float) x, (float) y);
        this.anthor = anthor2;
    }

    public AcotrNumSprite(int NumberIndex, String numStr2, int x, int y, int layer) {
        setAtlasRegion(NumberIndex);
        setNum(numStr2);
        GameStage.addActor(this, layer);
        setPosition((float) x, (float) y);
        this.anthor = 0;
    }

    public void setNum(String numStr2) {
        this.numStr = numStr2;
    }

    public String getNumStr() {
        return this.numStr;
    }

    public float getNum() {
        return Float.parseFloat(this.numStr);
    }

    public void setSpacing(int spacing2) {
        this.spacing = spacing2;
    }

    private void setAtlasRegion(int imagefirstId) {
        this.numW = numImage[imagefirstId][0];
        this.numH = numImage[imagefirstId][1];
        this.spacing = numImage[imagefirstId][2];
        setWidth((float) this.numW);
        setHeight((float) this.numH);
        this.charMap.clear();
        char c = '0';
        for (int i = 0; i < 11; i++) {
            TextureRegion curTexture = Tools.getImage(numImage[imagefirstId][3] + i);
            this.charMap.put(Character.valueOf(c), curTexture);
            if (i == 10) {
                this.charMap.put('.', curTexture);
            }
            c = (char) (c + 1);
        }
    }

    public float getNumImageWidth() {
        int digitCounter = this.numStr.length();
        return (float) ((this.numW * digitCounter) + (this.spacing * digitCounter));
    }

    public void draw(Batch batch, float parentAlpha) {
        if (this.numStr != null) {
            Color color = getColor();
            batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
            int startX = 0;
            switch (this.anthor) {
                case 1:
                    startX = (int) ((-getNumImageWidth()) / 2.0f);
                    break;
                case 2:
                    startX = (int) (-getNumImageWidth());
                    break;
            }
            for (int i = 0; i < this.numStr.length(); i++) {
                Batch batch2 = batch;
                batch2.draw(this.charMap.get(Character.valueOf(this.numStr.charAt(i))), ((float) ((this.numW + this.spacing) * i)) + ((float) startX) + getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
            }
        }
    }
}
