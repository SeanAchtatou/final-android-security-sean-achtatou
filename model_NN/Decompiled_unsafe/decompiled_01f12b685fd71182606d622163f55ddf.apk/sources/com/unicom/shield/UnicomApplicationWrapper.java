package com.unicom.shield;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import java.lang.reflect.Method;

public class UnicomApplicationWrapper extends Application {
    public static Application mApplication;
    public static String mApplicationName = "com.unicom.dcLoader.UnicomApplication";

    public void loadApplication(Context context) {
        if (mApplication == null) {
            try {
                mApplication = (Application) context.getClassLoader().loadClass(mApplicationName).newInstance();
                Method declaredMethod = Application.class.getDeclaredMethod("attach", Context.class);
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(mApplication, context);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        if (mApplication == null) {
            unipay.install(this, context);
            loadApplication(context);
        }
    }

    public void onCreate() {
        super.onCreate();
        mApplication.onCreate();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        mApplication.onConfigurationChanged(configuration);
    }

    public void onLowMemory() {
        super.onLowMemory();
        mApplication.onLowMemory();
    }
}
