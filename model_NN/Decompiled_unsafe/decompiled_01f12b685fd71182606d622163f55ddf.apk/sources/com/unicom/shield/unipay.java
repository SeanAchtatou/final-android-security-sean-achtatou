package com.unicom.shield;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class unipay {
    public static native void install_native(Application application, Context context, String str, String str2, String str3, String str4);

    public static native String string(String str);

    public static void install(Application application, Context context) {
        System.loadLibrary("me_unipay");
        install_native(application, context, context.getPackageName(), application.getApplicationInfo().sourceDir, "unipay", "unicom_classez.jar");
    }

    public static void int_999(Context context, int i) {
        SharedPreferences.Editor edit = context.getSharedPreferences("sdk_load_info", 0).edit();
        edit.putInt("unipaysdk_loadpath", i);
        edit.commit();
    }
}
