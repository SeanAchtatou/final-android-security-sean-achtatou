package com.esotericsoftware.reflectasm;

import java.lang.reflect.Modifier;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public abstract class ConstructorAccess<T> {
    boolean isNonStaticMemberClass;

    public abstract T newInstance();

    public abstract T newInstance(Object obj);

    public boolean isNonStaticMemberClass() {
        return this.isNonStaticMemberClass;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static <T> ConstructorAccess<T> get(Class<T> type) {
        int modifiers;
        String enclosingClassNameInternal;
        Class accessClass;
        String str;
        Class enclosingType = type.getEnclosingClass();
        boolean isNonStaticMemberClass2 = enclosingType != null && type.isMemberClass() && !Modifier.isStatic(type.getModifiers());
        String className = type.getName();
        String accessClassName = className + "ConstructorAccess";
        if (accessClassName.startsWith("java.")) {
            accessClassName = "reflectasm." + accessClassName;
        }
        AccessClassLoader loader = AccessClassLoader.get(type);
        synchronized (loader) {
            try {
                accessClass = loader.loadClass(accessClassName);
            } catch (Exception ex) {
                throw new RuntimeException("Non-static member class cannot be created (missing enclosing class constructor): " + type.getName(), ex);
            } catch (Exception ex2) {
                throw new RuntimeException("Class cannot be created (missing no-arg constructor): " + type.getName(), ex2);
            } catch (ClassNotFoundException e) {
                String accessClassNameInternal = accessClassName.replace('.', '/');
                String classNameInternal = className.replace('.', '/');
                if (!isNonStaticMemberClass2) {
                    enclosingClassNameInternal = null;
                    modifiers = type.getDeclaredConstructor(null).getModifiers();
                    if (Modifier.isPrivate(modifiers)) {
                        throw new RuntimeException("Class cannot be created (the no-arg constructor is private): " + type.getName());
                    }
                } else {
                    enclosingClassNameInternal = enclosingType.getName().replace('.', '/');
                    modifiers = type.getDeclaredConstructor(enclosingType).getModifiers();
                    if (Modifier.isPrivate(modifiers)) {
                        throw new RuntimeException("Non-static member class cannot be created (the enclosing class constructor is private): " + type.getName());
                    }
                }
                String superclassNameInternal = Modifier.isPublic(modifiers) ? "com/esotericsoftware/reflectasm/PublicConstructorAccess" : "com/esotericsoftware/reflectasm/ConstructorAccess";
                ClassWriter cw = new ClassWriter(0);
                cw.visit(Opcodes.V1_1, 33, accessClassNameInternal, null, superclassNameInternal, null);
                insertConstructor(cw, superclassNameInternal);
                insertNewInstance(cw, classNameInternal);
                insertNewInstanceInner(cw, classNameInternal, enclosingClassNameInternal);
                cw.visitEnd();
                accessClass = loader.defineClass(accessClassName, cw.toByteArray());
            }
        }
        try {
            ConstructorAccess<T> access = (ConstructorAccess) accessClass.newInstance();
            if ((access instanceof PublicConstructorAccess) || AccessClassLoader.areInSameRuntimeClassLoader(type, accessClass)) {
                access.isNonStaticMemberClass = isNonStaticMemberClass2;
                return access;
            }
            StringBuilder sb = new StringBuilder();
            if (!isNonStaticMemberClass2) {
                str = "Class cannot be created (the no-arg constructor is protected or package-protected, and its ConstructorAccess could not be defined in the same class loader): ";
            } else {
                str = "Non-static member class cannot be created (the enclosing class constructor is protected or package-protected, and its ConstructorAccess could not be defined in the same class loader): ";
            }
            throw new RuntimeException(sb.append(str).append(type.getName()).toString());
        } catch (Throwable t) {
            throw new RuntimeException("Exception constructing constructor access class: " + accessClassName, t);
        }
    }

    private static void insertConstructor(ClassWriter cw, String superclassNameInternal) {
        MethodVisitor mv = cw.visitMethod(1, "<init>", "()V", null, null);
        mv.visitCode();
        mv.visitVarInsn(25, 0);
        mv.visitMethodInsn(183, superclassNameInternal, "<init>", "()V");
        mv.visitInsn(177);
        mv.visitMaxs(1, 1);
        mv.visitEnd();
    }

    static void insertNewInstance(ClassWriter cw, String classNameInternal) {
        MethodVisitor mv = cw.visitMethod(1, "newInstance", "()Ljava/lang/Object;", null, null);
        mv.visitCode();
        mv.visitTypeInsn(187, classNameInternal);
        mv.visitInsn(89);
        mv.visitMethodInsn(183, classNameInternal, "<init>", "()V");
        mv.visitInsn(176);
        mv.visitMaxs(2, 1);
        mv.visitEnd();
    }

    static void insertNewInstanceInner(ClassWriter cw, String classNameInternal, String enclosingClassNameInternal) {
        MethodVisitor mv = cw.visitMethod(1, "newInstance", "(Ljava/lang/Object;)Ljava/lang/Object;", null, null);
        mv.visitCode();
        if (enclosingClassNameInternal != null) {
            mv.visitTypeInsn(187, classNameInternal);
            mv.visitInsn(89);
            mv.visitVarInsn(25, 1);
            mv.visitTypeInsn(192, enclosingClassNameInternal);
            mv.visitInsn(89);
            mv.visitMethodInsn(182, "java/lang/Object", "getClass", "()Ljava/lang/Class;");
            mv.visitInsn(87);
            mv.visitMethodInsn(183, classNameInternal, "<init>", "(L" + enclosingClassNameInternal + ";)V");
            mv.visitInsn(176);
            mv.visitMaxs(4, 2);
        } else {
            mv.visitTypeInsn(187, "java/lang/UnsupportedOperationException");
            mv.visitInsn(89);
            mv.visitLdcInsn("Not an inner class.");
            mv.visitMethodInsn(183, "java/lang/UnsupportedOperationException", "<init>", "(Ljava/lang/String;)V");
            mv.visitInsn(191);
            mv.visitMaxs(3, 2);
        }
        mv.visitEnd();
    }
}
