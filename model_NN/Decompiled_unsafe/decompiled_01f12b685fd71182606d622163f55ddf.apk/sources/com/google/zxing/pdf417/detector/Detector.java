package com.google.zxing.pdf417.detector;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.NotFoundException;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.DetectorResult;
import com.google.zxing.common.GridSampler;
import com.google.zxing.common.detector.MathUtils;
import com.kbz.esotericsoftware.spine.Animation;
import java.util.Arrays;
import java.util.Map;

public final class Detector {
    private static final int INTEGER_MATH_SHIFT = 8;
    private static final int MAX_AVG_VARIANCE = 107;
    private static final int MAX_INDIVIDUAL_VARIANCE = 204;
    private static final int PATTERN_MATCH_RESULT_SCALE_FACTOR = 256;
    private static final int SKEW_THRESHOLD = 3;
    private static final int[] START_PATTERN = {8, 1, 1, 1, 1, 1, 1, 3};
    private static final int[] START_PATTERN_REVERSE = {3, 1, 1, 1, 1, 1, 1, 8};
    private static final int[] STOP_PATTERN = {7, 1, 1, 3, 1, 1, 1, 2, 1};
    private static final int[] STOP_PATTERN_REVERSE = {1, 2, 1, 1, 1, 3, 1, 1, 7};
    private final BinaryBitmap image;

    public Detector(BinaryBitmap image2) {
        this.image = image2;
    }

    public DetectorResult detect() throws NotFoundException {
        return detect(null);
    }

    public DetectorResult detect(Map<DecodeHintType, ?> hints) throws NotFoundException {
        BitMatrix matrix = this.image.getBlackMatrix();
        boolean tryHarder = hints != null && hints.containsKey(DecodeHintType.TRY_HARDER);
        ResultPoint[] vertices = findVertices(matrix, tryHarder);
        if (vertices == null) {
            vertices = findVertices180(matrix, tryHarder);
            if (vertices != null) {
                correctCodeWordVertices(vertices, true);
            }
        } else {
            correctCodeWordVertices(vertices, false);
        }
        if (vertices == null) {
            throw NotFoundException.getNotFoundInstance();
        }
        float moduleWidth = computeModuleWidth(vertices);
        if (moduleWidth < 1.0f) {
            throw NotFoundException.getNotFoundInstance();
        }
        int dimension = computeDimension(vertices[4], vertices[6], vertices[5], vertices[7], moduleWidth);
        if (dimension < 1) {
            throw NotFoundException.getNotFoundInstance();
        }
        int ydimension = computeYDimension(vertices[4], vertices[6], vertices[5], vertices[7], moduleWidth);
        if (ydimension <= dimension) {
            ydimension = dimension;
        }
        return new DetectorResult(sampleGrid(matrix, vertices[4], vertices[5], vertices[6], vertices[7], dimension, ydimension), new ResultPoint[]{vertices[5], vertices[4], vertices[6], vertices[7]});
    }

    private static ResultPoint[] findVertices(BitMatrix matrix, boolean tryHarder) {
        int height = matrix.getHeight();
        int width = matrix.getWidth();
        ResultPoint[] result = new ResultPoint[8];
        boolean found = false;
        int[] counters = new int[START_PATTERN.length];
        int rowStep = Math.max(1, height >> (tryHarder ? 9 : 7));
        int i = 0;
        while (true) {
            if (i >= height) {
                break;
            }
            int[] loc = findGuardPattern(matrix, 0, i, width, false, START_PATTERN, counters);
            if (loc != null) {
                result[0] = new ResultPoint((float) loc[0], (float) i);
                result[4] = new ResultPoint((float) loc[1], (float) i);
                found = true;
                break;
            }
            i += rowStep;
        }
        if (found) {
            found = false;
            int i2 = height - 1;
            while (true) {
                if (i2 <= 0) {
                    break;
                }
                int[] loc2 = findGuardPattern(matrix, 0, i2, width, false, START_PATTERN, counters);
                if (loc2 != null) {
                    result[1] = new ResultPoint((float) loc2[0], (float) i2);
                    result[5] = new ResultPoint((float) loc2[1], (float) i2);
                    found = true;
                    break;
                }
                i2 -= rowStep;
            }
        }
        int[] counters2 = new int[STOP_PATTERN.length];
        if (found) {
            found = false;
            int i3 = 0;
            while (true) {
                if (i3 >= height) {
                    break;
                }
                int[] loc3 = findGuardPattern(matrix, 0, i3, width, false, STOP_PATTERN, counters2);
                if (loc3 != null) {
                    result[2] = new ResultPoint((float) loc3[1], (float) i3);
                    result[6] = new ResultPoint((float) loc3[0], (float) i3);
                    found = true;
                    break;
                }
                i3 += rowStep;
            }
        }
        if (found) {
            found = false;
            int i4 = height - 1;
            while (true) {
                if (i4 <= 0) {
                    break;
                }
                int[] loc4 = findGuardPattern(matrix, 0, i4, width, false, STOP_PATTERN, counters2);
                if (loc4 != null) {
                    result[3] = new ResultPoint((float) loc4[1], (float) i4);
                    result[7] = new ResultPoint((float) loc4[0], (float) i4);
                    found = true;
                    break;
                }
                i4 -= rowStep;
            }
        }
        if (found) {
            return result;
        }
        return null;
    }

    private static ResultPoint[] findVertices180(BitMatrix matrix, boolean tryHarder) {
        int height = matrix.getHeight();
        int halfWidth = matrix.getWidth() >> 1;
        ResultPoint[] result = new ResultPoint[8];
        boolean found = false;
        int[] counters = new int[START_PATTERN_REVERSE.length];
        int rowStep = Math.max(1, height >> (tryHarder ? 9 : 7));
        int i = height - 1;
        while (true) {
            if (i <= 0) {
                break;
            }
            int[] loc = findGuardPattern(matrix, halfWidth, i, halfWidth, true, START_PATTERN_REVERSE, counters);
            if (loc != null) {
                result[0] = new ResultPoint((float) loc[1], (float) i);
                result[4] = new ResultPoint((float) loc[0], (float) i);
                found = true;
                break;
            }
            i -= rowStep;
        }
        if (found) {
            found = false;
            int i2 = 0;
            while (true) {
                if (i2 >= height) {
                    break;
                }
                int[] loc2 = findGuardPattern(matrix, halfWidth, i2, halfWidth, true, START_PATTERN_REVERSE, counters);
                if (loc2 != null) {
                    result[1] = new ResultPoint((float) loc2[1], (float) i2);
                    result[5] = new ResultPoint((float) loc2[0], (float) i2);
                    found = true;
                    break;
                }
                i2 += rowStep;
            }
        }
        int[] counters2 = new int[STOP_PATTERN_REVERSE.length];
        if (found) {
            found = false;
            int i3 = height - 1;
            while (true) {
                if (i3 <= 0) {
                    break;
                }
                int[] loc3 = findGuardPattern(matrix, 0, i3, halfWidth, false, STOP_PATTERN_REVERSE, counters2);
                if (loc3 != null) {
                    result[2] = new ResultPoint((float) loc3[0], (float) i3);
                    result[6] = new ResultPoint((float) loc3[1], (float) i3);
                    found = true;
                    break;
                }
                i3 -= rowStep;
            }
        }
        if (found) {
            found = false;
            int i4 = 0;
            while (true) {
                if (i4 >= height) {
                    break;
                }
                int[] loc4 = findGuardPattern(matrix, 0, i4, halfWidth, false, STOP_PATTERN_REVERSE, counters2);
                if (loc4 != null) {
                    result[3] = new ResultPoint((float) loc4[0], (float) i4);
                    result[7] = new ResultPoint((float) loc4[1], (float) i4);
                    found = true;
                    break;
                }
                i4 += rowStep;
            }
        }
        if (found) {
            return result;
        }
        return null;
    }

    private static void correctCodeWordVertices(ResultPoint[] vertices, boolean upsideDown) {
        float v0x = vertices[0].getX();
        float v0y = vertices[0].getY();
        float v2x = vertices[2].getX();
        float v2y = vertices[2].getY();
        float v4x = vertices[4].getX();
        float v4y = vertices[4].getY();
        float v6x = vertices[6].getX();
        float v6y = vertices[6].getY();
        float skew = v4y - v6y;
        if (upsideDown) {
            skew = -skew;
        }
        if (skew > 3.0f) {
            float deltax = v6x - v0x;
            float deltay = v6y - v0y;
            float correction = ((v4x - v0x) * deltax) / ((deltax * deltax) + (deltay * deltay));
            vertices[4] = new ResultPoint((correction * deltax) + v0x, (correction * deltay) + v0y);
        } else if ((-skew) > 3.0f) {
            float deltax2 = v2x - v4x;
            float deltay2 = v2y - v4y;
            float correction2 = ((v2x - v6x) * deltax2) / ((deltax2 * deltax2) + (deltay2 * deltay2));
            vertices[6] = new ResultPoint(v2x - (correction2 * deltax2), v2y - (correction2 * deltay2));
        }
        float v1x = vertices[1].getX();
        float v1y = vertices[1].getY();
        float v3x = vertices[3].getX();
        float v3y = vertices[3].getY();
        float v5x = vertices[5].getX();
        float v5y = vertices[5].getY();
        float v7x = vertices[7].getX();
        float v7y = vertices[7].getY();
        float skew2 = v7y - v5y;
        if (upsideDown) {
            skew2 = -skew2;
        }
        if (skew2 > 3.0f) {
            float deltax3 = v7x - v1x;
            float deltay3 = v7y - v1y;
            float correction3 = ((v5x - v1x) * deltax3) / ((deltax3 * deltax3) + (deltay3 * deltay3));
            vertices[5] = new ResultPoint((correction3 * deltax3) + v1x, (correction3 * deltay3) + v1y);
        } else if ((-skew2) > 3.0f) {
            float deltax4 = v3x - v5x;
            float deltay4 = v3y - v5y;
            float correction4 = ((v3x - v7x) * deltax4) / ((deltax4 * deltax4) + (deltay4 * deltay4));
            vertices[7] = new ResultPoint(v3x - (correction4 * deltax4), v3y - (correction4 * deltay4));
        }
    }

    private static float computeModuleWidth(ResultPoint[] vertices) {
        return (((ResultPoint.distance(vertices[0], vertices[4]) + ResultPoint.distance(vertices[1], vertices[5])) / 34.0f) + ((ResultPoint.distance(vertices[6], vertices[2]) + ResultPoint.distance(vertices[7], vertices[3])) / 36.0f)) / 2.0f;
    }

    private static int computeDimension(ResultPoint topLeft, ResultPoint topRight, ResultPoint bottomLeft, ResultPoint bottomRight, float moduleWidth) {
        return ((((MathUtils.round(ResultPoint.distance(topLeft, topRight) / moduleWidth) + MathUtils.round(ResultPoint.distance(bottomLeft, bottomRight) / moduleWidth)) >> 1) + 8) / 17) * 17;
    }

    private static int computeYDimension(ResultPoint topLeft, ResultPoint topRight, ResultPoint bottomLeft, ResultPoint bottomRight, float moduleWidth) {
        return (MathUtils.round(ResultPoint.distance(topLeft, bottomLeft) / moduleWidth) + MathUtils.round(ResultPoint.distance(topRight, bottomRight) / moduleWidth)) >> 1;
    }

    private static BitMatrix sampleGrid(BitMatrix matrix, ResultPoint topLeft, ResultPoint bottomLeft, ResultPoint topRight, ResultPoint bottomRight, int xdimension, int ydimension) throws NotFoundException {
        return GridSampler.getInstance().sampleGrid(matrix, xdimension, ydimension, Animation.CurveTimeline.LINEAR, Animation.CurveTimeline.LINEAR, (float) xdimension, Animation.CurveTimeline.LINEAR, (float) xdimension, (float) ydimension, Animation.CurveTimeline.LINEAR, (float) ydimension, topLeft.getX(), topLeft.getY(), topRight.getX(), topRight.getY(), bottomRight.getX(), bottomRight.getY(), bottomLeft.getX(), bottomLeft.getY());
    }

    private static int[] findGuardPattern(BitMatrix matrix, int column, int row, int width, boolean whiteFirst, int[] pattern, int[] counters) {
        Arrays.fill(counters, 0, counters.length, 0);
        int patternLength = pattern.length;
        boolean isWhite = whiteFirst;
        int counterPosition = 0;
        int patternStart = column;
        for (int x = column; x < column + width; x++) {
            if (matrix.get(x, row) ^ isWhite) {
                counters[counterPosition] = counters[counterPosition] + 1;
            } else {
                if (counterPosition != patternLength - 1) {
                    counterPosition++;
                } else if (patternMatchVariance(counters, pattern, 204) < 107) {
                    return new int[]{patternStart, x};
                } else {
                    patternStart += counters[0] + counters[1];
                    System.arraycopy(counters, 2, counters, 0, patternLength - 2);
                    counters[patternLength - 2] = 0;
                    counters[patternLength - 1] = 0;
                    counterPosition--;
                }
                counters[counterPosition] = 1;
                isWhite = !isWhite;
            }
        }
        return null;
    }

    private static int patternMatchVariance(int[] counters, int[] pattern, int maxIndividualVariance) {
        int numCounters = counters.length;
        int total = 0;
        int patternLength = 0;
        for (int i = 0; i < numCounters; i++) {
            total += counters[i];
            patternLength += pattern[i];
        }
        if (total < patternLength) {
            return Integer.MAX_VALUE;
        }
        int unitBarWidth = (total << 8) / patternLength;
        int maxIndividualVariance2 = (maxIndividualVariance * unitBarWidth) >> 8;
        int totalVariance = 0;
        for (int x = 0; x < numCounters; x++) {
            int counter = counters[x] << 8;
            int scaledPattern = pattern[x] * unitBarWidth;
            int variance = counter > scaledPattern ? counter - scaledPattern : scaledPattern - counter;
            if (variance > maxIndividualVariance2) {
                return Integer.MAX_VALUE;
            }
            totalVariance += variance;
        }
        return totalVariance / total;
    }
}
