package com.google.zxing.oned;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitArray;
import com.sg.pak.PAK_ASSETS;
import java.util.Map;

public final class Code93Reader extends OneDReader {
    private static final char[] ALPHABET = ALPHABET_STRING.toCharArray();
    private static final String ALPHABET_STRING = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*";
    private static final int ASTERISK_ENCODING = CHARACTER_ENCODINGS[47];
    private static final int[] CHARACTER_ENCODINGS = {PAK_ASSETS.IMG_BOOMP, PAK_ASSETS.IMG_PPSG02, PAK_ASSETS.IMG_LS007, PAK_ASSETS.IMG_LIGHT01, PAK_ASSETS.IMG_DAJI08, PAK_ASSETS.IMG_DAJI06A, PAK_ASSETS.IMG_DAJI05, PAK_ASSETS.IMG_SHENGJITIPS02, PAK_ASSETS.IMG_BOOM01BD, PAK_ASSETS.IMG_UI_JIAOXUE001, 424, 420, PAK_ASSETS.IMG_UI002, 404, 402, PAK_ASSETS.IMG_UI_ZUANSHI02, PAK_ASSETS.IMG_XINJILU, PAK_ASSETS.IMG_TX_JIANSU04, PAK_ASSETS.IMG_TX_JIANSU02, PAK_ASSETS.IMG_GUANQIANDQ, PAK_ASSETS.IMG_BUFF_SUDU, PAK_ASSETS.IMG_SL01B, PAK_ASSETS.IMG_READYGO01, PAK_ASSETS.IMG_PENHUO01, 300, PAK_ASSETS.IMG_BUFF_GONGJI, PAK_ASSETS.IMG_JIESUAN005, PAK_ASSETS.IMG_JIESUAN003, PAK_ASSETS.IMG_JIDI004, 422, 406, 410, PAK_ASSETS.IMG_014, PAK_ASSETS.IMG_WSPARTICLE_RING09, PAK_ASSETS.IMG_ICE70, PAK_ASSETS.IMG_ICESG04, 302, PAK_ASSETS.IMG_QIANDAO011, PAK_ASSETS.IMG_QIANDAO009, PAK_ASSETS.IMG_JIESUO010, PAK_ASSETS.IMG_016, PAK_ASSETS.IMG_LIGHTD01, PAK_ASSETS.IMG_JIDI006, PAK_ASSETS.IMG_DAJI07, PAK_ASSETS.IMG_ZHUANPAN005, PAK_ASSETS.IMG_ZHUANPAN001, PAK_ASSETS.IMG_GAME_USER_SHUAGUAI01, PAK_ASSETS.IMG_TIPS04};

    public Result decodeRow(int rowNumber, BitArray row, Map<DecodeHintType, ?> map) throws NotFoundException, ChecksumException, FormatException {
        char decodedChar;
        int lastStart;
        int[] start = findAsteriskPattern(row);
        int nextStart = row.getNextSet(start[1]);
        int end = row.getSize();
        StringBuilder sb = new StringBuilder(20);
        int[] counters = new int[6];
        do {
            recordPattern(row, nextStart, counters);
            int pattern = toPattern(counters);
            if (pattern < 0) {
                throw NotFoundException.getNotFoundInstance();
            }
            decodedChar = patternToChar(pattern);
            sb.append(decodedChar);
            lastStart = nextStart;
            for (int counter : counters) {
                nextStart += counter;
            }
            nextStart = row.getNextSet(nextStart);
        } while (decodedChar != '*');
        sb.deleteCharAt(sb.length() - 1);
        if (nextStart == end || !row.get(nextStart)) {
            throw NotFoundException.getNotFoundInstance();
        } else if (sb.length() < 2) {
            throw NotFoundException.getNotFoundInstance();
        } else {
            checkChecksums(sb);
            sb.setLength(sb.length() - 2);
            return new Result(decodeExtended(sb), null, new ResultPoint[]{new ResultPoint(((float) (start[1] + start[0])) / 2.0f, (float) rowNumber), new ResultPoint(((float) (nextStart + lastStart)) / 2.0f, (float) rowNumber)}, BarcodeFormat.CODE_93);
        }
    }

    private static int[] findAsteriskPattern(BitArray row) throws NotFoundException {
        int width = row.getSize();
        int rowOffset = row.getNextSet(0);
        int counterPosition = 0;
        int[] counters = new int[6];
        int patternStart = rowOffset;
        boolean isWhite = false;
        int patternLength = counters.length;
        for (int i = rowOffset; i < width; i++) {
            if (row.get(i) ^ isWhite) {
                counters[counterPosition] = counters[counterPosition] + 1;
            } else {
                if (counterPosition != patternLength - 1) {
                    counterPosition++;
                } else if (toPattern(counters) == ASTERISK_ENCODING) {
                    return new int[]{patternStart, i};
                } else {
                    patternStart += counters[0] + counters[1];
                    System.arraycopy(counters, 2, counters, 0, patternLength - 2);
                    counters[patternLength - 2] = 0;
                    counters[patternLength - 1] = 0;
                    counterPosition--;
                }
                counters[counterPosition] = 1;
                if (!isWhite) {
                    isWhite = true;
                } else {
                    isWhite = false;
                }
            }
        }
        throw NotFoundException.getNotFoundInstance();
    }

    private static int toPattern(int[] counters) {
        int max = counters.length;
        int sum = 0;
        for (int counter : counters) {
            sum += counter;
        }
        int pattern = 0;
        for (int i = 0; i < max; i++) {
            int scaledShifted = ((counters[i] << 8) * 9) / sum;
            int scaledUnshifted = scaledShifted >> 8;
            if ((scaledShifted & 255) > 127) {
                scaledUnshifted++;
            }
            if (scaledUnshifted < 1 || scaledUnshifted > 4) {
                return -1;
            }
            if ((i & 1) == 0) {
                for (int j = 0; j < scaledUnshifted; j++) {
                    pattern = (pattern << 1) | 1;
                }
            } else {
                pattern <<= scaledUnshifted;
            }
        }
        return pattern;
    }

    private static char patternToChar(int pattern) throws NotFoundException {
        for (int i = 0; i < CHARACTER_ENCODINGS.length; i++) {
            if (CHARACTER_ENCODINGS[i] == pattern) {
                return ALPHABET[i];
            }
        }
        throw NotFoundException.getNotFoundInstance();
    }

    private static String decodeExtended(CharSequence encoded) throws FormatException {
        int length = encoded.length();
        StringBuilder decoded = new StringBuilder(length);
        int i = 0;
        while (i < length) {
            char c = encoded.charAt(i);
            if (c < 'a' || c > 'd') {
                decoded.append(c);
            } else if (i >= length - 1) {
                throw FormatException.getFormatInstance();
            } else {
                char next = encoded.charAt(i + 1);
                char decodedChar = 0;
                switch (c) {
                    case 'a':
                        if (next >= 'A' && next <= 'Z') {
                            decodedChar = (char) (next - '@');
                            break;
                        } else {
                            throw FormatException.getFormatInstance();
                        }
                        break;
                    case 'b':
                        if (next < 'A' || next > 'E') {
                            if (next >= 'F' && next <= 'W') {
                                decodedChar = (char) (next - 11);
                                break;
                            } else {
                                throw FormatException.getFormatInstance();
                            }
                        } else {
                            decodedChar = (char) (next - '&');
                            break;
                        }
                        break;
                    case 'c':
                        if (next >= 'A' && next <= 'O') {
                            decodedChar = (char) (next - ' ');
                            break;
                        } else if (next == 'Z') {
                            decodedChar = ':';
                            break;
                        } else {
                            throw FormatException.getFormatInstance();
                        }
                    case 'd':
                        if (next >= 'A' && next <= 'Z') {
                            decodedChar = (char) (next + ' ');
                            break;
                        } else {
                            throw FormatException.getFormatInstance();
                        }
                        break;
                }
                decoded.append(decodedChar);
                i++;
            }
            i++;
        }
        return decoded.toString();
    }

    private static void checkChecksums(CharSequence result) throws ChecksumException {
        int length = result.length();
        checkOneChecksum(result, length - 2, 20);
        checkOneChecksum(result, length - 1, 15);
    }

    private static void checkOneChecksum(CharSequence result, int checkPosition, int weightMax) throws ChecksumException {
        int weight = 1;
        int total = 0;
        for (int i = checkPosition - 1; i >= 0; i--) {
            total += ALPHABET_STRING.indexOf(result.charAt(i)) * weight;
            weight++;
            if (weight > weightMax) {
                weight = 1;
            }
        }
        if (result.charAt(checkPosition) != ALPHABET[total % 47]) {
            throw ChecksumException.getChecksumInstance();
        }
    }
}
