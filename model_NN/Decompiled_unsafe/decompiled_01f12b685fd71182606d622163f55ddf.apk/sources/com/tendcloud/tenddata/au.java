package com.tendcloud.tenddata;

import android.annotation.SuppressLint;
import com.sg.GameSprites.GameSpriteType;
import com.sg.pak.PAK_ASSETS;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.objectweb.asm.Opcodes;

@SuppressLint({"Assert"})
public class au {
    public static final int a = 0;
    public static final int b = 1;
    public static final int c = 0;
    public static final int d = 2;
    public static final int e = 4;
    public static final int f = 8;
    public static final int g = 16;
    public static final int h = 32;
    static final /* synthetic */ boolean i = (!au.class.desiredAssertionStatus());
    private static final int j = 76;
    private static final byte k = 61;
    private static final byte l = 10;
    private static final String m = "US-ASCII";
    private static final byte n = -5;
    private static final byte o = -1;
    private static final byte[] p = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, GameSpriteType.STATUS_PATROL, 101, GameSpriteType.STATUS_ATTACK1, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, GameSpriteType.f1TYPE_BOSS_, GameSpriteType.f0TYPE_BOSS_, GameSpriteType.f3TYPE_BOSS_2, 51, GameSpriteType.f56TYPE_ENEMY_, 53, 54, 55, 56, 57, 43, GameSpriteType.f2TYPE_BOSS_1};
    private static final byte[] q = {-9, -9, -9, -9, -9, -9, -9, -9, -9, n, n, -9, -9, n, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, n, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, GameSpriteType.f48TYPE_ENEMY_, -9, -9, -9, GameSpriteType.f29TYPE_ENEMY_, GameSpriteType.f56TYPE_ENEMY_, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, -9, -9, 26, 27, 28, 29, 30, 31, 32, GameSpriteType.f41TYPE_ENEMY_, GameSpriteType.f42TYPE_ENEMY_1, 35, 36, 37, 38, 39, 40, GameSpriteType.f28TYPE_ENEMY_2, 42, 43, 44, 45, 46, GameSpriteType.f2TYPE_BOSS_1, GameSpriteType.f1TYPE_BOSS_, GameSpriteType.f0TYPE_BOSS_, GameSpriteType.f3TYPE_BOSS_2, 51, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9};
    private static final byte[] r = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, GameSpriteType.STATUS_PATROL, 101, GameSpriteType.STATUS_ATTACK1, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, GameSpriteType.f1TYPE_BOSS_, GameSpriteType.f0TYPE_BOSS_, GameSpriteType.f3TYPE_BOSS_2, 51, GameSpriteType.f56TYPE_ENEMY_, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] s = {-9, -9, -9, -9, -9, -9, -9, -9, -9, n, n, -9, -9, n, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, n, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, GameSpriteType.f48TYPE_ENEMY_, -9, -9, GameSpriteType.f56TYPE_ENEMY_, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, GameSpriteType.f29TYPE_ENEMY_, -9, 26, 27, 28, 29, 30, 31, 32, GameSpriteType.f41TYPE_ENEMY_, GameSpriteType.f42TYPE_ENEMY_1, 35, 36, 37, 38, 39, 40, GameSpriteType.f28TYPE_ENEMY_2, 42, 43, 44, 45, 46, GameSpriteType.f2TYPE_BOSS_1, GameSpriteType.f1TYPE_BOSS_, GameSpriteType.f0TYPE_BOSS_, GameSpriteType.f3TYPE_BOSS_2, 51, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9};
    private static final byte[] t = {45, GameSpriteType.f1TYPE_BOSS_, GameSpriteType.f0TYPE_BOSS_, GameSpriteType.f3TYPE_BOSS_2, 51, GameSpriteType.f56TYPE_ENEMY_, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 95, 97, 98, 99, GameSpriteType.STATUS_PATROL, 101, GameSpriteType.STATUS_ATTACK1, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122};
    private static final byte[] u;

    public static class a extends FilterInputStream {
        private boolean a;
        private int b;
        private byte[] c;
        private int d;
        private int e;
        private int f;
        private boolean g;
        private int h;
        private byte[] i;

        public a(InputStream inputStream) {
            this(inputStream, 0);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(InputStream inputStream, int i2) {
            super(inputStream);
            boolean z = true;
            this.h = i2;
            this.g = (i2 & 8) > 0;
            this.a = (i2 & 1) <= 0 ? false : z;
            this.d = this.a ? 4 : 3;
            this.c = new byte[this.d];
            this.b = -1;
            this.f = 0;
            this.i = au.c(i2);
        }

        public int read() {
            int read;
            if (this.b < 0) {
                if (this.a) {
                    byte[] bArr = new byte[3];
                    int i2 = 0;
                    int i3 = 0;
                    while (i2 < 3) {
                        int read2 = this.in.read();
                        if (read2 < 0) {
                            break;
                        }
                        bArr[i2] = (byte) read2;
                        i2++;
                        i3++;
                    }
                    if (i3 <= 0) {
                        return -1;
                    }
                    byte[] unused = au.b(bArr, 0, i3, this.c, 0, this.h);
                    this.b = 0;
                    this.e = 4;
                } else {
                    byte[] bArr2 = new byte[4];
                    int i4 = 0;
                    while (i4 < 4) {
                        do {
                            read = this.in.read();
                            if (read < 0) {
                                break;
                            }
                        } while (this.i[read & 127] <= -5);
                        if (read < 0) {
                            break;
                        }
                        bArr2[i4] = (byte) read;
                        i4++;
                    }
                    if (i4 == 4) {
                        this.e = au.b(bArr2, 0, this.c, 0, this.h);
                        this.b = 0;
                    } else if (i4 == 0) {
                        return -1;
                    } else {
                        throw new IOException("Improperly padded Base64 input.");
                    }
                }
            }
            if (this.b < 0) {
                throw new IOException("Error in Base64 code reading stream.");
            } else if (this.b >= this.e) {
                return -1;
            } else {
                if (!this.a || !this.g || this.f < 76) {
                    this.f++;
                    byte[] bArr3 = this.c;
                    int i5 = this.b;
                    this.b = i5 + 1;
                    byte b2 = bArr3[i5];
                    if (this.b >= this.d) {
                        this.b = -1;
                    }
                    return b2 & 255;
                }
                this.f = 0;
                return 10;
            }
        }

        public int read(byte[] bArr, int i2, int i3) {
            int i4 = 0;
            while (i4 < i3) {
                int read = read();
                if (read >= 0) {
                    bArr[i2 + i4] = (byte) read;
                    i4++;
                } else if (i4 == 0) {
                    return -1;
                } else {
                    return i4;
                }
            }
            return i4;
        }
    }

    public static class b extends FilterOutputStream {
        private boolean a;
        private int b;
        private byte[] c;
        private int d;
        private int e;
        private boolean f;
        private byte[] g;
        private boolean h;
        private int i;
        private byte[] j;

        public b(OutputStream outputStream) {
            this(outputStream, 1);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(OutputStream outputStream, int i2) {
            super(outputStream);
            boolean z = true;
            this.f = (i2 & 8) != 0;
            this.a = (i2 & 1) == 0 ? false : z;
            this.d = this.a ? 3 : 4;
            this.c = new byte[this.d];
            this.b = 0;
            this.e = 0;
            this.h = false;
            this.g = new byte[4];
            this.i = i2;
            this.j = au.c(i2);
        }

        public void a() {
            if (this.b <= 0) {
                return;
            }
            if (this.a) {
                this.out.write(au.b(this.g, this.c, this.b, this.i));
                this.b = 0;
                return;
            }
            throw new IOException("Base64 input not properly padded.");
        }

        public void b() {
            a();
            this.h = true;
        }

        public void c() {
            this.h = false;
        }

        public void close() {
            a();
            super.close();
            this.c = null;
            this.out = null;
        }

        public void write(int i2) {
            if (this.h) {
                this.out.write(i2);
            } else if (this.a) {
                byte[] bArr = this.c;
                int i3 = this.b;
                this.b = i3 + 1;
                bArr[i3] = (byte) i2;
                if (this.b >= this.d) {
                    this.out.write(au.b(this.g, this.c, this.d, this.i));
                    this.e += 4;
                    if (this.f && this.e >= 76) {
                        this.out.write(10);
                        this.e = 0;
                    }
                    this.b = 0;
                }
            } else if (this.j[i2 & 127] > -5) {
                byte[] bArr2 = this.c;
                int i4 = this.b;
                this.b = i4 + 1;
                bArr2[i4] = (byte) i2;
                if (this.b >= this.d) {
                    this.out.write(this.g, 0, au.b(this.c, 0, this.g, 0, this.i));
                    this.b = 0;
                }
            } else if (this.j[i2 & 127] != -5) {
                throw new IOException("Invalid character in Base64 data.");
            }
        }

        public void write(byte[] bArr, int i2, int i3) {
            if (this.h) {
                this.out.write(bArr, i2, i3);
                return;
            }
            for (int i4 = 0; i4 < i3; i4++) {
                write(bArr[i2 + i4]);
            }
        }
    }

    static {
        byte[] bArr = new byte[PAK_ASSETS.IMG_ZUQIU_SHUOMING];
        // fill-array-data instruction
        bArr[0] = -9;
        bArr[1] = -9;
        bArr[2] = -9;
        bArr[3] = -9;
        bArr[4] = -9;
        bArr[5] = -9;
        bArr[6] = -9;
        bArr[7] = -9;
        bArr[8] = -9;
        bArr[9] = -5;
        bArr[10] = -5;
        bArr[11] = -9;
        bArr[12] = -9;
        bArr[13] = -5;
        bArr[14] = -9;
        bArr[15] = -9;
        bArr[16] = -9;
        bArr[17] = -9;
        bArr[18] = -9;
        bArr[19] = -9;
        bArr[20] = -9;
        bArr[21] = -9;
        bArr[22] = -9;
        bArr[23] = -9;
        bArr[24] = -9;
        bArr[25] = -9;
        bArr[26] = -9;
        bArr[27] = -9;
        bArr[28] = -9;
        bArr[29] = -9;
        bArr[30] = -9;
        bArr[31] = -9;
        bArr[32] = -5;
        bArr[33] = -9;
        bArr[34] = -9;
        bArr[35] = -9;
        bArr[36] = -9;
        bArr[37] = -9;
        bArr[38] = -9;
        bArr[39] = -9;
        bArr[40] = -9;
        bArr[41] = -9;
        bArr[42] = -9;
        bArr[43] = -9;
        bArr[44] = -9;
        bArr[45] = 0;
        bArr[46] = -9;
        bArr[47] = -9;
        bArr[48] = 1;
        bArr[49] = 2;
        bArr[50] = 3;
        bArr[51] = 4;
        bArr[52] = 5;
        bArr[53] = 6;
        bArr[54] = 7;
        bArr[55] = 8;
        bArr[56] = 9;
        bArr[57] = 10;
        bArr[58] = -9;
        bArr[59] = -9;
        bArr[60] = -9;
        bArr[61] = -1;
        bArr[62] = -9;
        bArr[63] = -9;
        bArr[64] = -9;
        bArr[65] = 11;
        bArr[66] = 12;
        bArr[67] = 13;
        bArr[68] = 14;
        bArr[69] = 15;
        bArr[70] = 16;
        bArr[71] = 17;
        bArr[72] = 18;
        bArr[73] = 19;
        bArr[74] = 20;
        bArr[75] = 21;
        bArr[76] = 22;
        bArr[77] = 23;
        bArr[78] = 24;
        bArr[79] = 25;
        bArr[80] = 26;
        bArr[81] = 27;
        bArr[82] = 28;
        bArr[83] = 29;
        bArr[84] = 30;
        bArr[85] = 31;
        bArr[86] = 32;
        bArr[87] = 33;
        bArr[88] = 34;
        bArr[89] = 35;
        bArr[90] = 36;
        bArr[91] = -9;
        bArr[92] = -9;
        bArr[93] = -9;
        bArr[94] = -9;
        bArr[95] = 37;
        bArr[96] = -9;
        bArr[97] = 38;
        bArr[98] = 39;
        bArr[99] = 40;
        bArr[100] = 41;
        bArr[101] = 42;
        bArr[102] = 43;
        bArr[103] = 44;
        bArr[104] = 45;
        bArr[105] = 46;
        bArr[106] = 47;
        bArr[107] = 48;
        bArr[108] = 49;
        bArr[109] = 50;
        bArr[110] = 51;
        bArr[111] = 52;
        bArr[112] = 53;
        bArr[113] = 54;
        bArr[114] = 55;
        bArr[115] = 56;
        bArr[116] = 57;
        bArr[117] = 58;
        bArr[118] = 59;
        bArr[119] = 60;
        bArr[120] = 61;
        bArr[121] = 62;
        bArr[122] = 63;
        bArr[123] = -9;
        bArr[124] = -9;
        bArr[125] = -9;
        bArr[126] = -9;
        bArr[127] = -9;
        bArr[128] = -9;
        bArr[129] = -9;
        bArr[130] = -9;
        bArr[131] = -9;
        bArr[132] = -9;
        bArr[133] = -9;
        bArr[134] = -9;
        bArr[135] = -9;
        bArr[136] = -9;
        bArr[137] = -9;
        bArr[138] = -9;
        bArr[139] = -9;
        bArr[140] = -9;
        bArr[141] = -9;
        bArr[142] = -9;
        bArr[143] = -9;
        bArr[144] = -9;
        bArr[145] = -9;
        bArr[146] = -9;
        bArr[147] = -9;
        bArr[148] = -9;
        bArr[149] = -9;
        bArr[150] = -9;
        bArr[151] = -9;
        bArr[152] = -9;
        bArr[153] = -9;
        bArr[154] = -9;
        bArr[155] = -9;
        bArr[156] = -9;
        bArr[157] = -9;
        bArr[158] = -9;
        bArr[159] = -9;
        bArr[160] = -9;
        bArr[161] = -9;
        bArr[162] = -9;
        bArr[163] = -9;
        bArr[164] = -9;
        bArr[165] = -9;
        bArr[166] = -9;
        bArr[167] = -9;
        bArr[168] = -9;
        bArr[169] = -9;
        bArr[170] = -9;
        bArr[171] = -9;
        bArr[172] = -9;
        bArr[173] = -9;
        bArr[174] = -9;
        bArr[175] = -9;
        bArr[176] = -9;
        bArr[177] = -9;
        bArr[178] = -9;
        bArr[179] = -9;
        bArr[180] = -9;
        bArr[181] = -9;
        bArr[182] = -9;
        bArr[183] = -9;
        bArr[184] = -9;
        bArr[185] = -9;
        bArr[186] = -9;
        bArr[187] = -9;
        bArr[188] = -9;
        bArr[189] = -9;
        bArr[190] = -9;
        bArr[191] = -9;
        bArr[192] = -9;
        bArr[193] = -9;
        bArr[194] = -9;
        bArr[195] = -9;
        bArr[196] = -9;
        bArr[197] = -9;
        bArr[198] = -9;
        bArr[199] = -9;
        bArr[200] = -9;
        bArr[201] = -9;
        bArr[202] = -9;
        bArr[203] = -9;
        bArr[204] = -9;
        bArr[205] = -9;
        bArr[206] = -9;
        bArr[207] = -9;
        bArr[208] = -9;
        bArr[209] = -9;
        bArr[210] = -9;
        bArr[211] = -9;
        bArr[212] = -9;
        bArr[213] = -9;
        bArr[214] = -9;
        bArr[215] = -9;
        bArr[216] = -9;
        bArr[217] = -9;
        bArr[218] = -9;
        bArr[219] = -9;
        bArr[220] = -9;
        bArr[221] = -9;
        bArr[222] = -9;
        bArr[223] = -9;
        bArr[224] = -9;
        bArr[225] = -9;
        bArr[226] = -9;
        bArr[227] = -9;
        bArr[228] = -9;
        bArr[229] = -9;
        bArr[230] = -9;
        bArr[231] = -9;
        bArr[232] = -9;
        bArr[233] = -9;
        bArr[234] = -9;
        bArr[235] = -9;
        bArr[236] = -9;
        bArr[237] = -9;
        bArr[238] = -9;
        bArr[239] = -9;
        bArr[240] = -9;
        bArr[241] = -9;
        bArr[242] = -9;
        bArr[243] = -9;
        bArr[244] = -9;
        bArr[245] = -9;
        bArr[246] = -9;
        bArr[247] = -9;
        bArr[248] = -9;
        bArr[249] = -9;
        bArr[250] = -9;
        bArr[251] = -9;
        bArr[252] = -9;
        bArr[253] = -9;
        bArr[254] = -9;
        bArr[255] = -9;
        bArr[256] = -9;
        u = bArr;
    }

    private au() {
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:27:0x0031 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:17:0x0026 */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: java.io.ObjectInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: java.io.ByteArrayInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: java.io.ObjectInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: java.io.ObjectInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: java.io.ByteArrayInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: java.io.ObjectInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: java.io.ObjectInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v7, resolved type: java.io.ObjectInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: java.io.ByteArrayInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v10, resolved type: java.io.ByteArrayInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v11, resolved type: java.io.ObjectInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v12, resolved type: java.io.ObjectInputStream} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:27:0x0031=Splitter:B:27:0x0031, B:17:0x0026=Splitter:B:17:0x0026} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object a(java.lang.String r3, int r4, java.lang.ClassLoader r5) {
        /*
            r1 = 0
            byte[] r0 = a(r3, r4)
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream     // Catch:{ IOException -> 0x0024, ClassNotFoundException -> 0x002f, all -> 0x003a }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0024, ClassNotFoundException -> 0x002f, all -> 0x003a }
            if (r5 != 0) goto L_0x001d
            java.io.ObjectInputStream r0 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x003f, ClassNotFoundException -> 0x003d }
            r0.<init>(r2)     // Catch:{ IOException -> 0x003f, ClassNotFoundException -> 0x003d }
            r1 = r0
        L_0x0012:
            java.lang.Object r0 = r1.readObject()     // Catch:{ IOException -> 0x003f, ClassNotFoundException -> 0x003d }
            r2.close()     // Catch:{ Exception -> 0x0032 }
        L_0x0019:
            r1.close()     // Catch:{ Exception -> 0x0034 }
        L_0x001c:
            return r0
        L_0x001d:
            com.tendcloud.tenddata.av r0 = new com.tendcloud.tenddata.av     // Catch:{ IOException -> 0x003f, ClassNotFoundException -> 0x003d }
            r0.<init>(r2, r5)     // Catch:{ IOException -> 0x003f, ClassNotFoundException -> 0x003d }
            r1 = r0
            goto L_0x0012
        L_0x0024:
            r0 = move-exception
            r2 = r1
        L_0x0026:
            throw r0     // Catch:{ all -> 0x0027 }
        L_0x0027:
            r0 = move-exception
        L_0x0028:
            r2.close()     // Catch:{ Exception -> 0x0036 }
        L_0x002b:
            r1.close()     // Catch:{ Exception -> 0x0038 }
        L_0x002e:
            throw r0
        L_0x002f:
            r0 = move-exception
            r2 = r1
        L_0x0031:
            throw r0     // Catch:{ all -> 0x0027 }
        L_0x0032:
            r2 = move-exception
            goto L_0x0019
        L_0x0034:
            r1 = move-exception
            goto L_0x001c
        L_0x0036:
            r2 = move-exception
            goto L_0x002b
        L_0x0038:
            r1 = move-exception
            goto L_0x002e
        L_0x003a:
            r0 = move-exception
            r2 = r1
            goto L_0x0028
        L_0x003d:
            r0 = move-exception
            goto L_0x0031
        L_0x003f:
            r0 = move-exception
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.au.a(java.lang.String, int, java.lang.ClassLoader):java.lang.Object");
    }

    public static String a(Serializable serializable) {
        return a(serializable, 0);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (5) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r4v0 */
    /* JADX WARN: Type inference failed for: r4v1, types: [java.io.ByteArrayOutputStream] */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.util.zip.GZIPOutputStream] */
    /* JADX WARN: Type inference failed for: r4v4, types: [java.io.OutputStream, java.io.ByteArrayOutputStream] */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r2v8 */
    /* JADX WARN: Type inference failed for: r2v9, types: [java.util.zip.GZIPOutputStream] */
    /* JADX WARN: Type inference failed for: r4v5 */
    /* JADX WARN: Type inference failed for: r2v12 */
    /* JADX WARN: Type inference failed for: r4v7 */
    /* JADX WARN: Type inference failed for: r4v9 */
    /* JADX WARN: Type inference failed for: r2v14 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 4 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.io.Serializable r5, int r6) {
        /*
            r1 = 0
            if (r5 != 0) goto L_0x000b
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            java.lang.String r1 = "Cannot serialize a null object."
            r0.<init>(r1)
            throw r0
        L_0x000b:
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0049, all -> 0x0077 }
            r4.<init>()     // Catch:{ IOException -> 0x0049, all -> 0x0077 }
            com.tendcloud.tenddata.au$b r3 = new com.tendcloud.tenddata.au$b     // Catch:{ IOException -> 0x0083, all -> 0x007c }
            r0 = r6 | 1
            r3.<init>(r4, r0)     // Catch:{ IOException -> 0x0083, all -> 0x007c }
            r0 = r6 & 2
            if (r0 == 0) goto L_0x0041
            java.util.zip.GZIPOutputStream r2 = new java.util.zip.GZIPOutputStream     // Catch:{ IOException -> 0x0087, all -> 0x0080 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0087, all -> 0x0080 }
            java.io.ObjectOutputStream r0 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x008a }
            r0.<init>(r2)     // Catch:{ IOException -> 0x008a }
            r1 = r0
        L_0x0026:
            r1.writeObject(r5)     // Catch:{ IOException -> 0x008a }
            r1.close()     // Catch:{ Exception -> 0x0067 }
        L_0x002c:
            r2.close()     // Catch:{ Exception -> 0x0069 }
        L_0x002f:
            r3.close()     // Catch:{ Exception -> 0x006b }
        L_0x0032:
            r4.close()     // Catch:{ Exception -> 0x006d }
        L_0x0035:
            java.lang.String r0 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x005c }
            byte[] r1 = r4.toByteArray()     // Catch:{ UnsupportedEncodingException -> 0x005c }
            java.lang.String r2 = "US-ASCII"
            r0.<init>(r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x005c }
        L_0x0040:
            return r0
        L_0x0041:
            java.io.ObjectOutputStream r0 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0087, all -> 0x0080 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x0087, all -> 0x0080 }
            r2 = r1
            r1 = r0
            goto L_0x0026
        L_0x0049:
            r0 = move-exception
            r2 = r1
            r3 = r1
            r4 = r1
        L_0x004d:
            throw r0     // Catch:{ all -> 0x004e }
        L_0x004e:
            r0 = move-exception
        L_0x004f:
            r1.close()     // Catch:{ Exception -> 0x006f }
        L_0x0052:
            r2.close()     // Catch:{ Exception -> 0x0071 }
        L_0x0055:
            r3.close()     // Catch:{ Exception -> 0x0073 }
        L_0x0058:
            r4.close()     // Catch:{ Exception -> 0x0075 }
        L_0x005b:
            throw r0
        L_0x005c:
            r0 = move-exception
            java.lang.String r0 = new java.lang.String
            byte[] r1 = r4.toByteArray()
            r0.<init>(r1)
            goto L_0x0040
        L_0x0067:
            r0 = move-exception
            goto L_0x002c
        L_0x0069:
            r0 = move-exception
            goto L_0x002f
        L_0x006b:
            r0 = move-exception
            goto L_0x0032
        L_0x006d:
            r0 = move-exception
            goto L_0x0035
        L_0x006f:
            r1 = move-exception
            goto L_0x0052
        L_0x0071:
            r1 = move-exception
            goto L_0x0055
        L_0x0073:
            r1 = move-exception
            goto L_0x0058
        L_0x0075:
            r1 = move-exception
            goto L_0x005b
        L_0x0077:
            r0 = move-exception
            r2 = r1
            r3 = r1
            r4 = r1
            goto L_0x004f
        L_0x007c:
            r0 = move-exception
            r2 = r1
            r3 = r1
            goto L_0x004f
        L_0x0080:
            r0 = move-exception
            r2 = r1
            goto L_0x004f
        L_0x0083:
            r0 = move-exception
            r2 = r1
            r3 = r1
            goto L_0x004d
        L_0x0087:
            r0 = move-exception
            r2 = r1
            goto L_0x004d
        L_0x008a:
            r0 = move-exception
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.au.a(java.io.Serializable, int):java.lang.String");
    }

    public static String a(byte[] bArr) {
        String str = null;
        try {
            str = a(bArr, 0, bArr.length, 0);
        } catch (IOException e2) {
            if (!i) {
                throw new AssertionError(e2.getMessage());
            }
        }
        if (i || str != null) {
            return str;
        }
        throw new AssertionError();
    }

    public static String a(byte[] bArr, int i2) {
        return a(bArr, 0, bArr.length, i2);
    }

    public static String a(byte[] bArr, int i2, int i3) {
        String str = null;
        try {
            str = a(bArr, i2, i3, 0);
        } catch (IOException e2) {
            if (!i) {
                throw new AssertionError(e2.getMessage());
            }
        }
        if (i || str != null) {
            return str;
        }
        throw new AssertionError();
    }

    public static String a(byte[] bArr, int i2, int i3, int i4) {
        byte[] b2 = b(bArr, i2, i3, i4);
        try {
            return new String(b2, m);
        } catch (UnsupportedEncodingException e2) {
            return new String(b2);
        }
    }

    public static void a(String str, String str2) {
        b bVar;
        try {
            bVar = new b(new FileOutputStream(str2), 0);
            try {
                bVar.write(str.getBytes(m));
                try {
                    bVar.close();
                } catch (Exception e2) {
                }
            } catch (IOException e3) {
                e = e3;
                try {
                    throw e;
                } catch (Throwable th) {
                    th = th;
                }
            }
        } catch (IOException e4) {
            e = e4;
            bVar = null;
            throw e;
        } catch (Throwable th2) {
            th = th2;
            bVar = null;
            try {
                bVar.close();
            } catch (Exception e5) {
            }
            throw th;
        }
    }

    public static void a(ByteBuffer byteBuffer, ByteBuffer byteBuffer2) {
        byte[] bArr = new byte[3];
        byte[] bArr2 = new byte[4];
        while (byteBuffer.hasRemaining()) {
            int min = Math.min(3, byteBuffer.remaining());
            byteBuffer.get(bArr, 0, min);
            b(bArr2, bArr, min, 0);
            byteBuffer2.put(bArr2);
        }
    }

    public static void a(ByteBuffer byteBuffer, CharBuffer charBuffer) {
        byte[] bArr = new byte[3];
        byte[] bArr2 = new byte[4];
        while (byteBuffer.hasRemaining()) {
            int min = Math.min(3, byteBuffer.remaining());
            byteBuffer.get(bArr, 0, min);
            b(bArr2, bArr, min, 0);
            for (int i2 = 0; i2 < 4; i2++) {
                charBuffer.put((char) (bArr2[i2] & 255));
            }
        }
    }

    public static void a(byte[] bArr, String str) {
        b bVar;
        if (bArr == null) {
            throw new NullPointerException("Data to encode was null.");
        }
        b bVar2 = null;
        try {
            bVar = new b(new FileOutputStream(str), 1);
            try {
                bVar.write(bArr);
                try {
                    bVar.close();
                } catch (Exception e2) {
                }
            } catch (IOException e3) {
                e = e3;
                try {
                    throw e;
                } catch (Throwable th) {
                    th = th;
                    bVar2 = bVar;
                }
            }
        } catch (IOException e4) {
            e = e4;
            bVar = null;
            throw e;
        } catch (Throwable th2) {
            th = th2;
            try {
                bVar2.close();
            } catch (Exception e5) {
            }
            throw th;
        }
    }

    public static byte[] a(String str) {
        return a(str, 0);
    }

    public static byte[] a(String str, int i2) {
        byte[] bytes;
        ByteArrayOutputStream byteArrayOutputStream;
        ByteArrayInputStream byteArrayInputStream;
        ByteArrayInputStream byteArrayInputStream2;
        GZIPInputStream gZIPInputStream = null;
        if (str == null) {
            throw new NullPointerException("Input string was null.");
        }
        try {
            bytes = str.getBytes(m);
        } catch (UnsupportedEncodingException e2) {
            bytes = str.getBytes();
        }
        byte[] c2 = c(bytes, 0, bytes.length, i2);
        boolean z = (i2 & 4) != 0;
        if (c2 != null && c2.length >= 4 && !z && 35615 == ((c2[0] & 255) | ((c2[1] << 8) & 65280))) {
            byte[] bArr = new byte[Opcodes.ACC_STRICT];
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    byteArrayInputStream2 = new ByteArrayInputStream(c2);
                    try {
                        GZIPInputStream gZIPInputStream2 = new GZIPInputStream(byteArrayInputStream2);
                        while (true) {
                            try {
                                int read = gZIPInputStream2.read(bArr);
                                if (read < 0) {
                                    break;
                                }
                                byteArrayOutputStream.write(bArr, 0, read);
                            } catch (IOException e3) {
                                e = e3;
                                gZIPInputStream = gZIPInputStream2;
                                byteArrayInputStream = byteArrayInputStream2;
                            } catch (Throwable th) {
                                th = th;
                                gZIPInputStream = gZIPInputStream2;
                                try {
                                    byteArrayOutputStream.close();
                                } catch (Exception e4) {
                                }
                                try {
                                    gZIPInputStream.close();
                                } catch (Exception e5) {
                                }
                                try {
                                    byteArrayInputStream2.close();
                                } catch (Exception e6) {
                                }
                                throw th;
                            }
                        }
                        c2 = byteArrayOutputStream.toByteArray();
                        try {
                            byteArrayOutputStream.close();
                        } catch (Exception e7) {
                        }
                        try {
                            gZIPInputStream2.close();
                        } catch (Exception e8) {
                        }
                        try {
                            byteArrayInputStream2.close();
                        } catch (Exception e9) {
                        }
                    } catch (IOException e10) {
                        e = e10;
                        byteArrayInputStream = byteArrayInputStream2;
                        try {
                            e.printStackTrace();
                            try {
                                byteArrayOutputStream.close();
                            } catch (Exception e11) {
                            }
                            try {
                                gZIPInputStream.close();
                            } catch (Exception e12) {
                            }
                            try {
                                byteArrayInputStream.close();
                            } catch (Exception e13) {
                            }
                            return c2;
                        } catch (Throwable th2) {
                            th = th2;
                            byteArrayInputStream2 = byteArrayInputStream;
                            byteArrayOutputStream.close();
                            gZIPInputStream.close();
                            byteArrayInputStream2.close();
                            throw th;
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        byteArrayOutputStream.close();
                        gZIPInputStream.close();
                        byteArrayInputStream2.close();
                        throw th;
                    }
                } catch (IOException e14) {
                    e = e14;
                    byteArrayInputStream = null;
                    e.printStackTrace();
                    byteArrayOutputStream.close();
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return c2;
                } catch (Throwable th4) {
                    th = th4;
                    byteArrayInputStream2 = null;
                    byteArrayOutputStream.close();
                    gZIPInputStream.close();
                    byteArrayInputStream2.close();
                    throw th;
                }
            } catch (IOException e15) {
                e = e15;
                byteArrayOutputStream = null;
                byteArrayInputStream = null;
                e.printStackTrace();
                byteArrayOutputStream.close();
                gZIPInputStream.close();
                byteArrayInputStream.close();
                return c2;
            } catch (Throwable th5) {
                th = th5;
                byteArrayOutputStream = null;
                byteArrayInputStream2 = null;
                byteArrayOutputStream.close();
                gZIPInputStream.close();
                byteArrayInputStream2.close();
                throw th;
            }
        }
        return c2;
    }

    /* access modifiers changed from: private */
    public static int b(byte[] bArr, int i2, byte[] bArr2, int i3, int i4) {
        if (bArr == null) {
            throw new NullPointerException("Source array was null.");
        } else if (bArr2 == null) {
            throw new NullPointerException("Destination array was null.");
        } else if (i2 < 0 || i2 + 3 >= bArr.length) {
            throw new IllegalArgumentException(String.format("Source array with length %d cannot have offset of %d and still process four bytes.", Integer.valueOf(bArr.length), Integer.valueOf(i2)));
        } else if (i3 < 0 || i3 + 2 >= bArr2.length) {
            throw new IllegalArgumentException(String.format("Destination array with length %d cannot have offset of %d and still store three bytes.", Integer.valueOf(bArr2.length), Integer.valueOf(i3)));
        } else {
            byte[] c2 = c(i4);
            if (bArr[i2 + 2] == 61) {
                bArr2[i3] = (byte) ((((c2[bArr[i2]] & 255) << 18) | ((c2[bArr[i2 + 1]] & 255) << 12)) >>> 16);
                return 1;
            } else if (bArr[i2 + 3] == 61) {
                int i5 = ((c2[bArr[i2]] & 255) << 18) | ((c2[bArr[i2 + 1]] & 255) << 12) | ((c2[bArr[i2 + 2]] & 255) << 6);
                bArr2[i3] = (byte) (i5 >>> 16);
                bArr2[i3 + 1] = (byte) (i5 >>> 8);
                return 2;
            } else {
                byte b2 = ((c2[bArr[i2]] & 255) << 18) | ((c2[bArr[i2 + 1]] & 255) << 12) | ((c2[bArr[i2 + 2]] & 255) << 6) | (c2[bArr[i2 + 3]] & 255);
                bArr2[i3] = (byte) (b2 >> 16);
                bArr2[i3 + 1] = (byte) (b2 >> 8);
                bArr2[i3 + 2] = (byte) b2;
                return 3;
            }
        }
    }

    public static Object b(String str) {
        return a(str, 0, (ClassLoader) null);
    }

    public static void b(String str, String str2) {
        BufferedOutputStream bufferedOutputStream;
        String d2 = d(str);
        try {
            bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(str2));
            try {
                bufferedOutputStream.write(d2.getBytes(m));
                try {
                    bufferedOutputStream.close();
                } catch (Exception e2) {
                }
            } catch (IOException e3) {
                e = e3;
                try {
                    throw e;
                } catch (Throwable th) {
                    th = th;
                }
            }
        } catch (IOException e4) {
            e = e4;
            bufferedOutputStream = null;
            throw e;
        } catch (Throwable th2) {
            th = th2;
            bufferedOutputStream = null;
            try {
                bufferedOutputStream.close();
            } catch (Exception e5) {
            }
            throw th;
        }
    }

    private static final byte[] b(int i2) {
        return (i2 & 16) == 16 ? r : (i2 & 32) == 32 ? t : p;
    }

    public static byte[] b(byte[] bArr) {
        try {
            return b(bArr, 0, bArr.length, 0);
        } catch (IOException e2) {
            if (i) {
                return null;
            }
            throw new AssertionError("IOExceptions only come from GZipping, which is turned off: " + e2.getMessage());
        }
    }

    public static byte[] b(byte[] bArr, int i2, int i3, int i4) {
        b bVar;
        ByteArrayOutputStream byteArrayOutputStream;
        ByteArrayOutputStream byteArrayOutputStream2;
        GZIPOutputStream gZIPOutputStream;
        GZIPOutputStream gZIPOutputStream2 = null;
        if (bArr == null) {
            throw new NullPointerException("Cannot serialize a null array.");
        } else if (i2 < 0) {
            throw new IllegalArgumentException("Cannot have negative offset: " + i2);
        } else if (i3 < 0) {
            throw new IllegalArgumentException("Cannot have length offset: " + i3);
        } else if (i2 + i3 > bArr.length) {
            throw new IllegalArgumentException(String.format("Cannot have offset of %d and length of %d with array of length %d", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(bArr.length)));
        } else if ((i4 & 2) != 0) {
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    bVar = new b(byteArrayOutputStream, i4 | 1);
                    try {
                        gZIPOutputStream = new GZIPOutputStream(bVar);
                    } catch (IOException e2) {
                        e = e2;
                        byteArrayOutputStream2 = byteArrayOutputStream;
                        try {
                            throw e;
                        } catch (Throwable th) {
                            th = th;
                            byteArrayOutputStream = byteArrayOutputStream2;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        try {
                            gZIPOutputStream2.close();
                        } catch (Exception e3) {
                        }
                        try {
                            bVar.close();
                        } catch (Exception e4) {
                        }
                        try {
                            byteArrayOutputStream.close();
                        } catch (Exception e5) {
                        }
                        throw th;
                    }
                } catch (IOException e6) {
                    e = e6;
                    bVar = null;
                    byteArrayOutputStream2 = byteArrayOutputStream;
                    throw e;
                } catch (Throwable th3) {
                    th = th3;
                    bVar = null;
                    gZIPOutputStream2.close();
                    bVar.close();
                    byteArrayOutputStream.close();
                    throw th;
                }
                try {
                    gZIPOutputStream.write(bArr, i2, i3);
                    gZIPOutputStream.close();
                    try {
                        gZIPOutputStream.close();
                    } catch (Exception e7) {
                    }
                    try {
                        bVar.close();
                    } catch (Exception e8) {
                    }
                    try {
                        byteArrayOutputStream.close();
                    } catch (Exception e9) {
                    }
                    return byteArrayOutputStream.toByteArray();
                } catch (IOException e10) {
                    e = e10;
                    gZIPOutputStream2 = gZIPOutputStream;
                    byteArrayOutputStream2 = byteArrayOutputStream;
                    throw e;
                } catch (Throwable th4) {
                    th = th4;
                    gZIPOutputStream2 = gZIPOutputStream;
                    gZIPOutputStream2.close();
                    bVar.close();
                    byteArrayOutputStream.close();
                    throw th;
                }
            } catch (IOException e11) {
                e = e11;
                bVar = null;
                byteArrayOutputStream2 = null;
                throw e;
            } catch (Throwable th5) {
                th = th5;
                bVar = null;
                byteArrayOutputStream = null;
                gZIPOutputStream2.close();
                bVar.close();
                byteArrayOutputStream.close();
                throw th;
            }
        } else {
            boolean z = (i4 & 8) != 0;
            int i5 = (i3 % 3 > 0 ? 4 : 0) + ((i3 / 3) * 4);
            if (z) {
                i5 += i5 / 76;
            }
            byte[] bArr2 = new byte[i5];
            int i6 = i3 - 2;
            int i7 = 0;
            int i8 = 0;
            int i9 = 0;
            while (i9 < i6) {
                b(bArr, i9 + i2, 3, bArr2, i8, i4);
                int i10 = i7 + 4;
                if (z && i10 >= 76) {
                    bArr2[i8 + 4] = 10;
                    i8++;
                    i10 = 0;
                }
                i8 += 4;
                i7 = i10;
                i9 += 3;
            }
            if (i9 < i3) {
                b(bArr, i9 + i2, i3 - i9, bArr2, i8, i4);
                i8 += 4;
            }
            if (i8 > bArr2.length - 1) {
                return bArr2;
            }
            byte[] bArr3 = new byte[i8];
            System.arraycopy(bArr2, 0, bArr3, 0, i8);
            return bArr3;
        }
    }

    /* access modifiers changed from: private */
    public static byte[] b(byte[] bArr, int i2, int i3, byte[] bArr2, int i4, int i5) {
        int i6 = 0;
        byte[] b2 = b(i5);
        int i7 = (i3 > 1 ? (bArr[i2 + 1] << 24) >>> 16 : 0) | (i3 > 0 ? (bArr[i2] << 24) >>> 8 : 0);
        if (i3 > 2) {
            i6 = (bArr[i2 + 2] << 24) >>> 24;
        }
        int i8 = i6 | i7;
        switch (i3) {
            case 1:
                bArr2[i4] = b2[i8 >>> 18];
                bArr2[i4 + 1] = b2[(i8 >>> 12) & 63];
                bArr2[i4 + 2] = 61;
                bArr2[i4 + 3] = 61;
                break;
            case 2:
                bArr2[i4] = b2[i8 >>> 18];
                bArr2[i4 + 1] = b2[(i8 >>> 12) & 63];
                bArr2[i4 + 2] = b2[(i8 >>> 6) & 63];
                bArr2[i4 + 3] = 61;
                break;
            case 3:
                bArr2[i4] = b2[i8 >>> 18];
                bArr2[i4 + 1] = b2[(i8 >>> 12) & 63];
                bArr2[i4 + 2] = b2[(i8 >>> 6) & 63];
                bArr2[i4 + 3] = b2[i8 & 63];
                break;
        }
        return bArr2;
    }

    /* access modifiers changed from: private */
    public static byte[] b(byte[] bArr, byte[] bArr2, int i2, int i3) {
        b(bArr2, 0, i2, bArr, 0, i3);
        return bArr;
    }

    public static void c(String str, String str2) {
        BufferedOutputStream bufferedOutputStream;
        byte[] c2 = c(str);
        BufferedOutputStream bufferedOutputStream2 = null;
        try {
            bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(str2));
            try {
                bufferedOutputStream.write(c2);
                try {
                    bufferedOutputStream.close();
                } catch (Exception e2) {
                }
            } catch (IOException e3) {
                e = e3;
                try {
                    throw e;
                } catch (Throwable th) {
                    th = th;
                    bufferedOutputStream2 = bufferedOutputStream;
                }
            }
        } catch (IOException e4) {
            e = e4;
            bufferedOutputStream = null;
            throw e;
        } catch (Throwable th2) {
            th = th2;
            try {
                bufferedOutputStream2.close();
            } catch (Exception e5) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public static final byte[] c(int i2) {
        return (i2 & 16) == 16 ? s : (i2 & 32) == 32 ? u : q;
    }

    public static byte[] c(String str) {
        int i2 = 0;
        a aVar = null;
        try {
            File file = new File(str);
            if (file.length() > 2147483647L) {
                throw new IOException("File is too big for this convenience method (" + file.length() + " bytes).");
            }
            byte[] bArr = new byte[((int) file.length())];
            a aVar2 = new a(new BufferedInputStream(new FileInputStream(file)), 0);
            while (true) {
                try {
                    int read = aVar2.read(bArr, i2, 4096);
                    if (read < 0) {
                        break;
                    }
                    i2 += read;
                } catch (IOException e2) {
                    e = e2;
                    aVar = aVar2;
                    try {
                        throw e;
                    } catch (Throwable th) {
                        th = th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    aVar = aVar2;
                    try {
                        aVar.close();
                    } catch (Exception e3) {
                    }
                    throw th;
                }
            }
            byte[] bArr2 = new byte[i2];
            System.arraycopy(bArr, 0, bArr2, 0, i2);
            try {
                aVar2.close();
            } catch (Exception e4) {
            }
            return bArr2;
        } catch (IOException e5) {
            e = e5;
            throw e;
        }
    }

    public static byte[] c(byte[] bArr) {
        return c(bArr, 0, bArr.length, 0);
    }

    public static byte[] c(byte[] bArr, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        if (bArr == null) {
            throw new NullPointerException("Cannot decode null source array.");
        } else if (i2 < 0 || i2 + i3 > bArr.length) {
            throw new IllegalArgumentException(String.format("Source array with length %d cannot have offset of %d and process %d bytes.", Integer.valueOf(bArr.length), Integer.valueOf(i2), Integer.valueOf(i3)));
        } else if (i3 == 0) {
            return new byte[0];
        } else {
            if (i3 < 4) {
                throw new IllegalArgumentException("Base64-encoded string must have at least four characters, but length specified was " + i3);
            }
            byte[] c2 = c(i4);
            byte[] bArr2 = new byte[((i3 * 3) / 4)];
            byte[] bArr3 = new byte[4];
            int i8 = i2;
            int i9 = 0;
            int i10 = 0;
            while (true) {
                if (i8 >= i2 + i3) {
                    i5 = i10;
                    break;
                }
                byte b2 = c2[bArr[i8] & 255];
                if (b2 >= -5) {
                    if (b2 >= -1) {
                        i6 = i9 + 1;
                        bArr3[i9] = bArr[i8];
                        if (i6 > 3) {
                            i5 = b(bArr3, 0, bArr2, i10, i4) + i10;
                            if (bArr[i8] == 61) {
                                break;
                            }
                            i7 = i5;
                            i6 = 0;
                        } else {
                            i7 = i10;
                        }
                    } else {
                        i6 = i9;
                        i7 = i10;
                    }
                    i8++;
                    i10 = i7;
                    i9 = i6;
                } else {
                    throw new IOException(String.format("Bad Base64 input character decimal %d in array position %d", Integer.valueOf(bArr[i8] & 255), Integer.valueOf(i8)));
                }
            }
            byte[] bArr4 = new byte[i5];
            System.arraycopy(bArr2, 0, bArr4, 0, i5);
            return bArr4;
        }
    }

    public static String d(String str) {
        a aVar;
        int i2 = 0;
        a aVar2 = null;
        try {
            File file = new File(str);
            byte[] bArr = new byte[Math.max((int) ((((double) file.length()) * 1.4d) + 1.0d), 40)];
            aVar = new a(new BufferedInputStream(new FileInputStream(file)), 1);
            while (true) {
                try {
                    int read = aVar.read(bArr, i2, 4096);
                    if (read < 0) {
                        break;
                    }
                    i2 += read;
                } catch (IOException e2) {
                    e = e2;
                    try {
                        throw e;
                    } catch (Throwable th) {
                        th = th;
                        aVar2 = aVar;
                    }
                }
            }
            String str2 = new String(bArr, 0, i2, m);
            try {
                aVar.close();
            } catch (Exception e3) {
            }
            return str2;
        } catch (IOException e4) {
            e = e4;
            aVar = null;
            throw e;
        } catch (Throwable th2) {
            th = th2;
            try {
                aVar2.close();
            } catch (Exception e5) {
            }
            throw th;
        }
    }
}
