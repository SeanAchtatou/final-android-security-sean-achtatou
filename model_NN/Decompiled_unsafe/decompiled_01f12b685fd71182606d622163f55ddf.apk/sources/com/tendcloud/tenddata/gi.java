package com.tendcloud.tenddata;

import android.app.Activity;
import android.os.Message;
import com.tendcloud.tenddata.gd;

final class gi implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ boolean b;
    final /* synthetic */ Activity c;

    gi(String str, boolean z, Activity activity) {
        this.a = str;
        this.b = z;
        this.c = activity;
    }

    public void run() {
        try {
            ev.a("onResume being called! pageName: " + this.a + ", FromAPI: " + String.valueOf(this.b));
            gd.a aVar = new gd.a();
            aVar.a.put("context", this.c);
            aVar.a.put("isPageOrSession", Boolean.valueOf(this.b));
            aVar.a.put("apiType", 2);
            aVar.a.put("occurTime", Long.valueOf(System.currentTimeMillis()));
            aVar.a.put("pageName", this.a);
            Message.obtain(er.a(), 102, aVar).sendToTarget();
        } catch (Throwable th) {
            if (TCAgent.LOG_ON) {
                th.printStackTrace();
            }
        }
    }
}
