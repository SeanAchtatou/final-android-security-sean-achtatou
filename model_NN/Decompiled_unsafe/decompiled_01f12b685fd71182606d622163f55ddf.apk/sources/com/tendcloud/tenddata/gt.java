package com.tendcloud.tenddata;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

final class gt {
    private final Object a;
    private final Method b;
    private final int c;
    private boolean d = true;

    gt(Object obj, Method method) {
        if (obj == null) {
            throw new NullPointerException("EventHandler target cannot be null.");
        } else if (method == null) {
            throw new NullPointerException("EventHandler method cannot be null.");
        } else {
            this.a = obj;
            this.b = method;
            method.setAccessible(true);
            this.c = ((method.hashCode() + 31) * 31) + obj.hashCode();
        }
    }

    public boolean a() {
        return this.d;
    }

    public void b() {
        this.d = false;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        gt gtVar = (gt) obj;
        return this.b.equals(gtVar.b) && this.a == gtVar.a;
    }

    public void handleEvent(Object obj) {
        if (!this.d) {
            throw new IllegalStateException(toString() + " has been invalidated and can no longer handle events.");
        }
        try {
            this.b.invoke(this.a, obj);
        } catch (IllegalAccessException e) {
            throw new AssertionError(e);
        } catch (InvocationTargetException e2) {
            if (e2.getCause() instanceof Error) {
                throw ((Error) e2.getCause());
            }
            throw e2;
        }
    }

    public int hashCode() {
        return this.c;
    }

    public String toString() {
        return "[EventHandler " + this.b + "]";
    }
}
