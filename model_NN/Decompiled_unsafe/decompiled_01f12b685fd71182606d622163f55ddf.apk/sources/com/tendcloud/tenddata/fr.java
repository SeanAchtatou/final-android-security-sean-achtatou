package com.tendcloud.tenddata;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

class fr extends Handler {
    final /* synthetic */ fq a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    fr(fq fqVar, Looper looper) {
        super(looper);
        this.a = fqVar;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                this.a.k();
                return;
            case 1:
                this.a.j();
                return;
            case 2:
            case 3:
            default:
                return;
            case 4:
                this.a.d();
                return;
            case 5:
                this.a.b();
                return;
            case 6:
                this.a.h();
                return;
        }
    }
}
