package com.tendcloud.tenddata;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import com.tendcloud.tenddata.bk;
import java.util.zip.CRC32;

final class ga {
    static Handler a;
    static String b = "";
    static int c = 0;
    private static final CRC32 d = new CRC32();
    private static final HandlerThread e = new HandlerThread("NetWorkThread");
    private static String f = "";

    static {
        a = null;
        e.start();
        a = new gb(e.getLooper());
    }

    ga() {
    }

    static final Handler a() {
        return a;
    }

    static void a(byte[] bArr) {
        try {
            byte[] b2 = b(bArr);
            d.reset();
            d.update(b2);
            bk.d a2 = bk.a(TalkingDataSMS.b, "ev1.xdrig.com", fu.c, "https://ev1.xdrig.com/smsauth/v1/" + b + "/" + Long.toHexString(d.getValue()), f, b2);
            Bundle bundle = new Bundle();
            bundle.putInt("status", a2.a());
            bundle.putString("message", a2.b());
            bundle.putString(dc.Y, "SMS");
            fu.a().onResponse(bundle);
        } catch (Throwable th) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x002f A[SYNTHETIC, Splitter:B:17:0x002f] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x003e A[SYNTHETIC, Splitter:B:25:0x003e] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x004a A[SYNTHETIC, Splitter:B:31:0x004a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] b(byte[] r5) {
        /*
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream
            r3.<init>()
            r2 = 0
            java.util.zip.Deflater r4 = new java.util.zip.Deflater
            r0 = 9
            r1 = 1
            r4.<init>(r0, r1)
            java.util.zip.DeflaterOutputStream r1 = new java.util.zip.DeflaterOutputStream     // Catch:{ FileNotFoundException -> 0x0028, IOException -> 0x0038 }
            r1.<init>(r3, r4)     // Catch:{ FileNotFoundException -> 0x0028, IOException -> 0x0038 }
            r1.write(r5)     // Catch:{ FileNotFoundException -> 0x0059, IOException -> 0x0056 }
            if (r1 == 0) goto L_0x001b
            r1.close()     // Catch:{ IOException -> 0x0023 }
        L_0x001b:
            r4.end()
            byte[] r0 = r3.toByteArray()
            return r0
        L_0x0023:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x001b
        L_0x0028:
            r0 = move-exception
            r1 = r2
        L_0x002a:
            r0.printStackTrace()     // Catch:{ all -> 0x0053 }
            if (r1 == 0) goto L_0x001b
            r1.close()     // Catch:{ IOException -> 0x0033 }
            goto L_0x001b
        L_0x0033:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x001b
        L_0x0038:
            r0 = move-exception
        L_0x0039:
            r0.printStackTrace()     // Catch:{ all -> 0x0047 }
            if (r2 == 0) goto L_0x001b
            r2.close()     // Catch:{ IOException -> 0x0042 }
            goto L_0x001b
        L_0x0042:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x001b
        L_0x0047:
            r0 = move-exception
        L_0x0048:
            if (r2 == 0) goto L_0x004d
            r2.close()     // Catch:{ IOException -> 0x004e }
        L_0x004d:
            throw r0
        L_0x004e:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004d
        L_0x0053:
            r0 = move-exception
            r2 = r1
            goto L_0x0048
        L_0x0056:
            r0 = move-exception
            r2 = r1
            goto L_0x0039
        L_0x0059:
            r0 = move-exception
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.ga.b(byte[]):byte[]");
    }
}
