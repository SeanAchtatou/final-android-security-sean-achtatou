package com.tendcloud.tenddata;

class ew {
    static final String a = "TDpref.profile.key";
    static final String b = "TDpref.session.key";
    static final String c = "TDpref.lastactivity.key";
    static final String d = "TDpref.start.key";
    static final String e = "TDpref.init.key";
    static final String f = "TDpref.actstart.key";
    static final String g = "TDpref.end.key";
    static final String h = "TDpref.ip";
    static final String i = "TD_CHANNEL_ID";
    static final String j = "TDpref_longtime";
    static final String k = "TDpref_shorttime";
    static final String l = "TDaes_key";
    static final String m = "TDisAppQuiting";
    static final String n = "TDpref.last.sdk.check";
    static final String o = "TDadditionalVersionName";
    static final String p = "TDadditionalVersionCode";
    static final String q = "TDcs";
    static final String r = "TDpref.apps_send_time.key";
    static final String s = "TDtime_gap";

    ew() {
    }

    static String a() {
        if (ab.mContext == null) {
            return null;
        }
        return bu.b(ab.mContext, j, l, (String) null);
    }

    static void a(long j2) {
        if (ab.mContext != null) {
            bu.a(ab.mContext, j, d, j2);
        }
    }

    static void a(String str) {
        if (ab.mContext != null) {
            bu.a(ab.mContext, j, l, str);
        }
    }

    static void a(boolean z) {
        bu.a(ab.mContext, j, a, z ? 1 : 0);
    }

    static String b() {
        if (ab.mContext == null) {
            return null;
        }
        return bu.b(ab.mContext, j, b, (String) null);
    }

    static void b(long j2) {
        if (ab.mContext != null) {
            bu.a(ab.mContext, j, e, j2);
        }
    }

    static void b(String str) {
        if (ab.mContext != null) {
            bu.a(ab.mContext, j, b, str);
        }
    }

    static void c() {
        if (ab.mContext != null) {
            ab.mContext.getSharedPreferences(i, 0).edit().putBoolean("location_called", true).commit();
        }
    }

    static void c(long j2) {
        if (ab.mContext != null) {
            bu.a(ab.mContext, k, f, j2);
        }
    }

    static void c(String str) {
        if (ab.mContext != null) {
            bu.a(ab.mContext, k, c, str);
        }
    }

    static void d(long j2) {
        if (ab.mContext != null) {
            bu.a(ab.mContext, k, g, j2);
        }
    }

    static void d(String str) {
        if (ab.mContext != null) {
            bu.a(ab.mContext, j, m, str);
        }
    }

    static boolean d() {
        if (ab.mContext == null) {
            return false;
        }
        return ab.mContext.getSharedPreferences(i, 0).getBoolean("location_called", false);
    }

    static String e() {
        return ab.mContext == null ? "" : bu.b(ab.mContext, k, c, "");
    }

    static void e(long j2) {
        if (ab.mContext != null) {
            bu.a(ab.mContext, j, p, j2);
        }
    }

    static void e(String str) {
        if (ab.mContext != null) {
            bu.a(ab.mContext, j, o, str);
        }
    }

    static long f() {
        if (ab.mContext == null) {
            return 0;
        }
        return bu.b(ab.mContext, j, d, 0);
    }

    static void f(long j2) {
        if (ab.mContext != null) {
            bu.a(ab.mContext, j, s, j2);
        }
    }

    static void f(String str) {
        if (ab.mContext != null) {
            bu.a(ab.mContext, j, q, str);
        }
    }

    static long g() {
        if (ab.mContext == null) {
            return 0;
        }
        return bu.b(ab.mContext, j, e, 0);
    }

    static long h() {
        if (ab.mContext == null) {
            return 0;
        }
        return bu.b(ab.mContext, k, g, 0);
    }

    static String i() {
        return ab.mContext == null ? "-1" : bu.b(ab.mContext, j, m, "-1");
    }

    static String j() {
        if (ab.mContext == null) {
            return null;
        }
        return bu.b(ab.mContext, j, o, (String) null);
    }

    static long k() {
        if (ab.mContext == null) {
            return -1;
        }
        return bu.b(ab.mContext, j, p, -1);
    }

    static String l() {
        if (ab.mContext == null) {
            return null;
        }
        return bu.b(ab.mContext, j, q, (String) null);
    }

    static long m() {
        if (ab.mContext == null) {
            return 0;
        }
        return bu.b(ab.mContext, j, s, 0);
    }

    static int n() {
        try {
            return k() != -1 ? Integer.parseInt(String.valueOf(k())) : bj.a().b(ab.mContext);
        } catch (Throwable th) {
            return -1;
        }
    }

    static String o() {
        try {
            return j() != null ? j() : bj.a().c(ab.mContext);
        } catch (Throwable th) {
            return "unknown";
        }
    }
}
