package com.tendcloud.tenddata;

import android.content.Context;
import android.content.Intent;

/* renamed from: com.tendcloud.tenddata.do  reason: invalid class name */
class Cdo {
    private static final String a = "PushLog";

    Cdo() {
    }

    static void a(Context context) {
        a(context, null, null);
    }

    static void a(Context context, String str, String str2) {
        if (context == null) {
            throw new RuntimeException("[mpush] start service error, context is required");
        }
        dy.commonInit(context);
        try {
            Intent intent = new Intent();
            intent.setAction(dc.R);
            intent.putExtra(dc.T, cv.a(context));
            context.sendBroadcast(intent);
        } catch (Throwable th) {
            cs.b("PushLog", th.getMessage());
        }
        cv.f(context, dc.z);
        ca.execute(new dp(context));
    }

    static void a(Context context, boolean z) {
        cs.a(z);
    }
}
