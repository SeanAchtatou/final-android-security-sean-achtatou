package com.tendcloud.tenddata;

import android.content.Context;
import android.content.pm.Signature;
import java.io.ByteArrayInputStream;
import java.security.cert.CertificateFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class df {
    static final double a = 1.2d;
    static final int b = 0;
    static final String c = "Android";
    static final int d = 0;
    static final JSONArray e = new JSONArray();
    static JSONObject f = new JSONObject();
    static JSONObject g = new JSONObject();
    static JSONObject h = new JSONObject();
    static JSONObject i = null;
    static JSONObject j = new JSONObject();
    static String k = bu.b(ab.mContext, ab.SETTINGS_PERF_FILE, ab.SETTINGS_PREF_PUSH_APPID, "");

    df() {
    }

    static String a(JSONObject jSONObject) {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            if (i == null) {
                i = new JSONObject();
                i.put("device", d());
                i.put("app", e());
                i.put("sdk", g());
            }
            i.put("appContext", f());
            i.put("ts", currentTimeMillis);
            i.put(dc.Y, jSONObject);
            return i.toString();
        } catch (JSONException e2) {
            return null;
        }
    }

    static void a() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("tid", bm.a(ab.mContext));
            try {
                JSONArray y = br.y(ab.mContext);
                JSONArray jSONArray = new JSONArray();
                if (y != null && y.length() > 0) {
                    jSONArray.put(y.getJSONObject(0).get("imei"));
                    if (y.length() == 2) {
                        try {
                            jSONArray.put(y.getJSONObject(1).get("imei"));
                        } catch (Throwable th) {
                        }
                    }
                }
                jSONObject.put("imeis", jSONArray);
            } catch (Throwable th2) {
            }
            JSONArray jSONArray2 = new JSONArray();
            jSONArray2.put(bm.f(ab.mContext));
            jSONObject.put("wifiMacs", jSONArray2);
            jSONObject.put("androidId", bm.b(ab.mContext));
            jSONObject.put("adId", bm.g(ab.mContext));
            jSONObject.put("serialNo", bm.a() == null ? "" : bm.a());
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("manufacture", bn.b());
            jSONObject2.put("brand", bn.c());
            jSONObject2.put("model", bn.d());
            g.put("hardwareConfig", jSONObject2);
            g.put("deviceId", jSONObject);
        } catch (Throwable th3) {
        }
    }

    static void a(String str, JSONObject jSONObject, JSONObject jSONObject2, String str2) {
        try {
            h.put("sessionId", str);
            h.put("account", jSONObject);
            h.put("subAccount", jSONObject2);
            h.put("push", str2);
        } catch (Throwable th) {
        }
    }

    static byte[] a(Context context, String str) {
        try {
            Signature[] signatureArr = context.getPackageManager().getPackageInfo(str, 64).signatures;
            CertificateFactory instance = CertificateFactory.getInstance("X.509");
            byte[] byteArray = signatureArr[0].toByteArray();
            instance.generateCertificate(new ByteArrayInputStream(byteArray));
            return byteArray;
        } catch (Throwable th) {
            return null;
        }
    }

    static void b() {
        try {
            f.put("name", bj.a().h(ab.mContext));
            f.put("globalId", bj.a().a(ab.mContext));
            f.put("versionName", bj.a().c(ab.mContext));
            f.put("versionCode", bj.a().b(ab.mContext));
            f.put("installTime", bj.a().d(ab.mContext));
            f.put("updateTime", bj.a().e(ab.mContext));
            f.put("cert", ca.a(a(ab.mContext, ab.mContext.getPackageName())));
        } catch (Throwable th) {
        }
    }

    static void c() {
        try {
            j.put("features", e);
            j.put("minorVersion", 0);
            j.put("build", 0);
            j.put("platform", c);
            j.put("version", (double) a);
        } catch (Throwable th) {
        }
    }

    private static JSONObject d() {
        return g;
    }

    private static JSONObject e() {
        return f;
    }

    private static JSONObject f() {
        return h;
    }

    private static JSONObject g() {
        return j;
    }
}
