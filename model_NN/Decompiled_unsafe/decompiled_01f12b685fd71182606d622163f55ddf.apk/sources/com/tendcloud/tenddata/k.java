package com.tendcloud.tenddata;

import android.annotation.SuppressLint;
import com.badlogic.gdx.net.HttpRequestHeader;
import com.tendcloud.tenddata.ad;
import com.tendcloud.tenddata.d;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Socket;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

@SuppressLint({"Assert"})
public abstract class k extends e implements d, Runnable {
    static final /* synthetic */ boolean d = (!k.class.desiredAssertionStatus());
    protected URI c;
    /* access modifiers changed from: private */
    public g e;
    private Socket f;
    private InputStream g;
    /* access modifiers changed from: private */
    public OutputStream h;
    private Proxy i;
    private Thread j;
    private l k;
    private Map l;
    private CountDownLatch m;
    private CountDownLatch n;
    private int o;

    class a implements Runnable {
        private a() {
        }

        public void run() {
            Thread.currentThread().setName("WebsocketWriteThread");
            while (!Thread.interrupted()) {
                try {
                    ByteBuffer byteBuffer = (ByteBuffer) k.this.e.h.take();
                    k.this.h.write(byteBuffer.array(), 0, byteBuffer.limit());
                    k.this.h.flush();
                } catch (IOException e) {
                    k.this.e.n();
                    return;
                } catch (InterruptedException e2) {
                    return;
                }
            }
        }
    }

    public k(URI uri) {
        this(uri, new n());
    }

    public k(URI uri, l lVar) {
        this(uri, lVar, null, 0);
    }

    public k(URI uri, l lVar, Map map, int i2) {
        this.c = null;
        this.e = null;
        this.f = null;
        this.i = Proxy.NO_PROXY;
        this.m = new CountDownLatch(1);
        this.n = new CountDownLatch(1);
        this.o = 0;
        if (uri == null) {
            throw new IllegalArgumentException();
        } else if (lVar == null) {
            throw new IllegalArgumentException("null as draft is permitted for `WebSocketServer` only!");
        } else {
            this.c = uri;
            this.k = lVar;
            this.l = map;
            this.o = i2;
            this.e = new g(this, lVar);
        }
    }

    private int r() {
        int port = this.c.getPort();
        if (port != -1) {
            return port;
        }
        String scheme = this.c.getScheme();
        if (scheme.equals("wss")) {
            return 443;
        }
        if (scheme.equals("ws")) {
            return 80;
        }
        throw new RuntimeException("unkonow scheme" + scheme);
    }

    private void s() {
        String path = this.c.getPath();
        String query = this.c.getQuery();
        if (path == null || path.length() == 0) {
            path = "/";
        }
        if (query != null) {
            path = path + "?" + query;
        }
        int r = r();
        aj ajVar = new aj();
        ajVar.setResourceDescriptor(path);
        ajVar.a(HttpRequestHeader.Host, this.c.getHost() + (r != 80 ? ":" + r : ""));
        if (this.l != null) {
            for (Map.Entry entry : this.l.entrySet()) {
                ajVar.a((String) entry.getKey(), (String) entry.getValue());
            }
        }
        this.e.startHandshake(ajVar);
    }

    public void a() {
        if (this.j != null) {
            this.e.close(1000);
        }
    }

    public void a(int i2, String str) {
        this.e.a(i2, str);
    }

    public void a(int i2, String str, boolean z) {
    }

    public void a(ad.a aVar, ByteBuffer byteBuffer, boolean z) {
        this.e.a(aVar, byteBuffer, z);
    }

    public void a(d dVar, int i2, String str) {
        c(i2, str);
    }

    public final void a(d dVar, int i2, String str, boolean z) {
        this.m.countDown();
        this.n.countDown();
        if (this.j != null) {
            this.j.interrupt();
        }
        try {
            if (this.f != null) {
                this.f.close();
            }
        } catch (IOException e2) {
            a(this, e2);
        }
        b(i2, str, z);
    }

    public void a(d dVar, ad adVar) {
        onFragment(adVar);
    }

    public final void a(d dVar, al alVar) {
        this.m.countDown();
        onOpen((an) alVar);
    }

    public final void a(d dVar, Exception exc) {
        onError(exc);
    }

    public final void a(d dVar, String str) {
        onMessage(str);
    }

    public final void a(d dVar, ByteBuffer byteBuffer) {
        onMessage(byteBuffer);
    }

    public InetSocketAddress b(d dVar) {
        if (this.f != null) {
            return (InetSocketAddress) this.f.getLocalSocketAddress();
        }
        return null;
    }

    public void b(int i2, String str) {
        this.e.b(i2, str);
    }

    public abstract void b(int i2, String str, boolean z);

    public void b(d dVar, int i2, String str, boolean z) {
        a(i2, str, z);
    }

    public boolean b() {
        return this.e.b();
    }

    public InetSocketAddress c() {
        return this.e.c();
    }

    public InetSocketAddress c(d dVar) {
        if (this.f != null) {
            return (InetSocketAddress) this.f.getRemoteSocketAddress();
        }
        return null;
    }

    public void c(int i2, String str) {
    }

    public void close(int i2) {
        this.e.a();
    }

    public InetSocketAddress d() {
        return this.e.d();
    }

    public boolean e() {
        return this.e.e();
    }

    public boolean f() {
        return this.e.f();
    }

    public boolean g() {
        return this.e.g();
    }

    public boolean h() {
        return this.e.h();
    }

    public boolean i() {
        return this.e.i();
    }

    public l j() {
        return this.k;
    }

    public d.a k() {
        return this.e.k();
    }

    public String l() {
        return this.c.getPath();
    }

    public URI m() {
        return this.c;
    }

    public void n() {
        if (this.j != null) {
            throw new IllegalStateException("WebSocketClient objects are not reuseable");
        }
        this.j = new Thread(this);
        this.j.start();
    }

    public boolean o() {
        n();
        this.m.await();
        return this.e.f();
    }

    public abstract void onError(Exception exc);

    public void onFragment(ad adVar) {
    }

    public abstract void onMessage(String str);

    public void onMessage(ByteBuffer byteBuffer) {
    }

    public abstract void onOpen(an anVar);

    public final void onWriteDemand(d dVar) {
    }

    public void p() {
        a();
        this.n.await();
    }

    public d q() {
        return this.e;
    }

    public void run() {
        int read;
        bk.c.put(Long.valueOf(Thread.currentThread().getId()), m().getHost());
        try {
            if (this.f == null) {
                this.f = new Socket(this.i);
            } else if (this.f.isClosed()) {
                throw new IOException();
            }
            if (!this.f.isBound()) {
                this.f.connect(new InetSocketAddress(this.c.getHost(), r()), this.o);
            }
            this.g = this.f.getInputStream();
            this.h = this.f.getOutputStream();
            s();
            this.j = new Thread(new a());
            this.j.start();
            byte[] bArr = new byte[g.c];
            while (!i() && (read = this.g.read(bArr)) != -1) {
                try {
                    this.e.decode(ByteBuffer.wrap(bArr, 0, read));
                } catch (IOException e2) {
                    this.e.n();
                } catch (RuntimeException e3) {
                    onError(e3);
                    this.e.b(y.f, e3.getMessage());
                }
            }
            this.e.n();
            if (!d && !this.f.isClosed()) {
                throw new AssertionError();
            }
        } catch (Exception e4) {
            a(this.e, e4);
            this.e.b(-1, e4.getMessage());
        }
    }

    public void send(String str) {
        this.e.send(str);
    }

    public void send(ByteBuffer byteBuffer) {
        this.e.send(byteBuffer);
    }

    public void send(byte[] bArr) {
        this.e.send(bArr);
    }

    public void sendFrame(ad adVar) {
        this.e.sendFrame(adVar);
    }

    public void setProxy(Proxy proxy) {
        if (proxy == null) {
            throw new IllegalArgumentException();
        }
        this.i = proxy;
    }

    public void setSocket(Socket socket) {
        if (this.f != null) {
            throw new IllegalStateException("socket has already been set");
        }
        this.f = socket;
    }
}
