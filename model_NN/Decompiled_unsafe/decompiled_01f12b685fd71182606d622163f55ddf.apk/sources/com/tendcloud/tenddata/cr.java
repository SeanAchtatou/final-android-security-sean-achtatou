package com.tendcloud.tenddata;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.List;

final class cr {
    private static final String a = "PushLog";
    private static a b = null;
    private static final String c = "create table IF NOT EXISTS kv (k text primary key, v text)";
    private static final String d = "insert into kv values(?,?)";
    private static final String e = "update kv set v=? where k=?";
    private static final String f = "delete from kv where k=?";
    private static final String g = "select k from kv where k like ?";
    private static final String h = "select v from kv where k like ?";
    private static final String i = "select k from kv where v=?";
    private static final String j = "select v from kv where k=?";
    private static final String k = "create table IF NOT EXISTS msg (msg_id text primary key, ct INTEGER)";
    private static final String l = "insert into msg values(?,?)";
    private static final String m = "update msg set ct=? where msg_id=?";
    private static final String n = "delete from msg where ct<=?";
    private static final String o = "select ct from msg where msg_id=?";
    private static final String p = "select ct from msg order by ct asc limit 1 offset 100";
    private static final String q = "select count(*) from msg";

    static class a extends SQLiteOpenHelper {
        public a(Context context) {
            super(context, "talkingdata_app.db", (SQLiteDatabase.CursorFactory) null, 2);
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            try {
                sQLiteDatabase.execSQL(cr.c);
                sQLiteDatabase.execSQL(cr.k);
            } catch (Throwable th) {
                cs.e("PushLog", th.getMessage());
            }
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            if (i2 == 2) {
                sQLiteDatabase.execSQL(cr.k);
            }
        }
    }

    cr() {
    }

    static List a(String str) {
        ArrayList arrayList = new ArrayList();
        if (str != null) {
            Cursor rawQuery = d().getReadableDatabase().rawQuery(g, new String[]{str});
            if (rawQuery.moveToFirst()) {
                while (!rawQuery.isAfterLast()) {
                    arrayList.add(rawQuery.getString(0));
                    rawQuery.moveToNext();
                }
            }
            rawQuery.close();
        }
        return arrayList;
    }

    static void a() {
        d().close();
    }

    static synchronized void a(Context context) {
        synchronized (cr.class) {
            if (context != null) {
                if (b == null) {
                    b = new a(context);
                }
            }
        }
    }

    static boolean a(String str, String str2) {
        if (str != null) {
            try {
                if (e(str)) {
                    d().getWritableDatabase().execSQL(e, new String[]{str2, str});
                    return true;
                }
                d().getWritableDatabase().execSQL(d, new String[]{str, str2});
                return true;
            } catch (Throwable th) {
                cs.e("PushLog", th.getMessage());
            }
        }
        return false;
    }

    static long b() {
        Throwable th;
        long j2;
        try {
            Cursor rawQuery = d().getReadableDatabase().rawQuery(q, null);
            rawQuery.moveToFirst();
            j2 = rawQuery.getLong(0);
            try {
                rawQuery.close();
            } catch (Throwable th2) {
                th = th2;
                cs.e("PushLog", th.getMessage());
                return j2;
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            j2 = 0;
            th = th4;
        }
        return j2;
    }

    static List b(String str) {
        ArrayList arrayList = new ArrayList();
        if (str != null) {
            Cursor rawQuery = d().getReadableDatabase().rawQuery(i, new String[]{str});
            if (rawQuery.moveToFirst()) {
                while (!rawQuery.isAfterLast()) {
                    arrayList.add(rawQuery.getString(0));
                    rawQuery.moveToNext();
                }
            }
            rawQuery.close();
        }
        return arrayList;
    }

    static List c(String str) {
        ArrayList arrayList = new ArrayList();
        if (str != null) {
            Cursor rawQuery = d().getReadableDatabase().rawQuery(h, new String[]{str});
            if (rawQuery.moveToFirst()) {
                while (!rawQuery.isAfterLast()) {
                    arrayList.add(rawQuery.getString(0));
                    rawQuery.moveToNext();
                }
            }
            rawQuery.close();
        }
        return arrayList;
    }

    static void c() {
        long j2;
        Cursor rawQuery = d().getReadableDatabase().rawQuery(p, null);
        if (rawQuery.moveToFirst()) {
            j2 = rawQuery.getLong(0);
            rawQuery.close();
        } else {
            j2 = 0;
        }
        if (j2 > 0) {
            d().getWritableDatabase().execSQL(n, new Object[]{Long.valueOf(j2)});
        }
    }

    private static a d() {
        if (b == null) {
            cs.e("PushLog", "Err: invoke PushManager.startPushService to init push sdk");
        }
        return b;
    }

    static String d(String str) {
        String str2 = null;
        if (str != null) {
            Cursor rawQuery = d().getReadableDatabase().rawQuery(j, new String[]{str});
            if (rawQuery.moveToFirst()) {
                str2 = rawQuery.getString(0);
            }
            rawQuery.close();
        }
        return str2;
    }

    static boolean e(String str) {
        Cursor rawQuery = d().getReadableDatabase().rawQuery(j, new String[]{str});
        boolean moveToFirst = rawQuery.moveToFirst();
        rawQuery.close();
        return moveToFirst;
    }

    static void f(String str) {
        d().getWritableDatabase().execSQL(f, new String[]{str});
    }

    static void g(String str) {
        if (str != null) {
            for (String f2 : a(str)) {
                f(f2);
            }
        }
    }

    static boolean h(String str) {
        if (str != null) {
            if (b() >= 1000) {
                c();
            }
            try {
                d().getWritableDatabase().execSQL(l, new Object[]{str, Long.valueOf(System.currentTimeMillis())});
                return true;
            } catch (Throwable th) {
                cs.e("PushLog", th.getMessage());
            }
        }
        return false;
    }

    static boolean i(String str) {
        Throwable th;
        boolean z = true;
        if (!cp.a(str)) {
            try {
                Cursor rawQuery = d().getReadableDatabase().rawQuery(o, new String[]{str});
                z = rawQuery.moveToFirst();
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th = th2;
                }
            } catch (Throwable th3) {
                Throwable th4 = th3;
                z = false;
                th = th4;
            }
        }
        return z;
        cs.e("PushLog", th.getMessage());
        return z;
    }
}
