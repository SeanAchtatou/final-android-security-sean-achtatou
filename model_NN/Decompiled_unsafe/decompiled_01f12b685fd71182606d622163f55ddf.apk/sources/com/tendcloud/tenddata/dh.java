package com.tendcloud.tenddata;

import android.os.Handler;
import android.os.HandlerThread;

final class dh {
    static final int a = 9999;
    static final int b = 101;
    static final int c = 102;
    static final int d = 103;
    static final int e = 104;
    static final int f = 105;
    private static Handler g;
    private static final HandlerThread h = new HandlerThread("PushThreadProcess");

    static {
        g = null;
        h.start();
        g = new di(h.getLooper());
    }

    dh() {
    }

    static final Handler a() {
        return g;
    }
}
