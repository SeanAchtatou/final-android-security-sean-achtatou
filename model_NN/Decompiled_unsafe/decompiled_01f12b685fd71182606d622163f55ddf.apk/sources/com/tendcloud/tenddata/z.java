package com.tendcloud.tenddata;

import com.tendcloud.tenddata.ad;
import java.nio.ByteBuffer;

public class z extends ae implements y {
    static final ByteBuffer p = ByteBuffer.allocate(0);
    private int q;
    private String r;

    public z() {
        super(ad.a.CLOSING);
        setFin(true);
    }

    public z(int i) {
        super(ad.a.CLOSING);
        setFin(true);
        a(i, "");
    }

    public z(int i, String str) {
        super(ad.a.CLOSING);
        setFin(true);
        a(i, str);
    }

    private void a(int i, String str) {
        String str2 = str == null ? "" : str;
        if (i == 1015) {
            str2 = "";
            i = 1005;
        }
        if (i != 1005) {
            byte[] a = aw.a(str2);
            ByteBuffer allocate = ByteBuffer.allocate(4);
            allocate.putInt(i);
            allocate.position(2);
            ByteBuffer allocate2 = ByteBuffer.allocate(a.length + 2);
            allocate2.put(allocate);
            allocate2.put(a);
            allocate2.rewind();
            setPayload(allocate2);
        } else if (str2.length() > 0) {
            throw new r((int) y.c, "A close frame must have a closecode if it has a reason");
        }
    }

    private void g() {
        this.q = y.e;
        ByteBuffer c = super.c();
        c.mark();
        if (c.remaining() >= 2) {
            ByteBuffer allocate = ByteBuffer.allocate(4);
            allocate.position(2);
            allocate.putShort(c.getShort());
            allocate.position(0);
            this.q = allocate.getInt();
            if (this.q == 1006 || this.q == 1015 || this.q == 1005 || this.q > 4999 || this.q < 1000 || this.q == 1004) {
                throw new s("closecode must not be sent over the wire: " + this.q);
            }
        }
        c.reset();
    }

    private void h() {
        if (this.q == 1005) {
            this.r = aw.a(super.c());
            return;
        }
        ByteBuffer c = super.c();
        int position = c.position();
        try {
            c.position(c.position() + 2);
            this.r = aw.a(c);
            c.position(position);
        } catch (IllegalArgumentException e) {
            throw new s(e);
        } catch (Throwable th) {
            c.position(position);
            throw th;
        }
    }

    public int a() {
        return this.q;
    }

    public String b() {
        return this.r;
    }

    public ByteBuffer c() {
        return this.q == 1005 ? p : super.c();
    }

    public void setPayload(ByteBuffer byteBuffer) {
        super.setPayload(byteBuffer);
        g();
        h();
    }

    public String toString() {
        return super.toString() + "code: " + this.q;
    }
}
