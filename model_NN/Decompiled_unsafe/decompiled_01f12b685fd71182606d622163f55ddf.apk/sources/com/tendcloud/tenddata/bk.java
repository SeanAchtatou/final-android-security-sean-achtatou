package com.tendcloud.tenddata;

import android.content.Context;
import android.preference.PreferenceManager;
import com.badlogic.gdx.net.HttpRequestHeader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class bk {
    public static int a = 60000;
    public static int b = 60000;
    public static volatile HashMap c = new HashMap();
    private static final int d = 600;
    private static final int e = 60000;
    private static final int f = 60000;
    private static Context g = null;
    private static volatile HashMap h = new HashMap();

    static class a {
        String a = null;
        String b = null;
        String c = null;
        String d = null;
        String e = null;

        a() {
        }
    }

    static class b {
        static final int a = 1;
        static final int b = 2;
        static final int c = 3;
        static final int d = 4;

        b() {
        }
    }

    static class c implements X509TrustManager {
        X509Certificate a;

        c(X509Certificate x509Certificate) {
            this.a = x509Certificate;
        }

        public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
        }

        public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
            int indexOf;
            if (x509CertificateArr.length == 0) {
                throw new CertificateException("No server certificate found!");
            } else if (!x509CertificateArr[0].getIssuerDN().equals(this.a.getSubjectDN())) {
                throw new CertificateException("Parent certificate of server was different than expected signing certificate");
            } else {
                try {
                    String name = x509CertificateArr[0].getSubjectDN().getName();
                    int indexOf2 = name.indexOf("CN=");
                    if (indexOf2 >= 0 && (indexOf = (name = name.substring(indexOf2 + 3)).indexOf(",")) >= 0) {
                        name = name.substring(0, indexOf);
                    }
                    String[] split = name.split("\\.");
                    String str2 = split.length >= 2 ? split[split.length - 2] + "." + split[split.length - 1] : name;
                    if (!bk.c.containsKey(Long.valueOf(Thread.currentThread().getId()))) {
                        throw new CertificateException("No valid host provided!");
                    } else if (!((String) bk.c.get(Long.valueOf(Thread.currentThread().getId()))).endsWith(str2)) {
                        throw new CertificateException("Server certificate has incorrect host name!");
                    } else {
                        x509CertificateArr[0].verify(this.a.getPublicKey());
                        x509CertificateArr[0].checkValidity();
                    }
                } catch (Throwable th) {
                    if (th instanceof CertificateException) {
                        throw new CertificateException(th);
                    }
                }
            }
        }

        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }

    public static class d {
        public int a;
        public String b;

        d(int i) {
            this(i, "");
        }

        d(int i, String str) {
            this.a = i;
            this.b = str;
        }

        public int a() {
            return this.a;
        }

        public String b() {
            return this.b;
        }
    }

    public static d a(Context context, String str, String str2, String str3, String str4, byte[] bArr) {
        g = context;
        b(str, str2);
        return a(str, str2, str3, str4, bArr);
    }

    private static d a(String str, String str2, String str3, String str4, byte[] bArr) {
        d dVar = new d(600);
        try {
            if (a(str, 2) != null) {
                dVar = a(a(str, 2), str3, str4, bArr, str);
                if (dVar.a == 600) {
                    a(str, (String) null, 2);
                }
            } else {
                if (a(str, 1) != null) {
                    dVar = a(a(str, 1), str3, str4, bArr, str);
                    if (dVar.a != 600) {
                        a(str, a(str, 1), 2);
                        a(str);
                    }
                }
                if (dVar.a == 600 && a(str, 3) != null) {
                    dVar = a(a(str, 3), str3, str4, bArr, str);
                    if (dVar.a != 600) {
                        a(str, a(str, 3), 2);
                    }
                }
                if (dVar.a == 600 && a(str, 4) != null) {
                    dVar = a(a(str, 4), str3, str4, bArr, str);
                    if (dVar.a != 600) {
                        a(str, a(str, 4), 2);
                    }
                }
            }
        } catch (Throwable th) {
        }
        return dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.bk.a(java.net.URL, java.lang.String, boolean):java.net.URLConnection
     arg types: [java.net.URL, java.lang.String, int]
     candidates:
      com.tendcloud.tenddata.bk.a(java.lang.String, java.lang.String, java.io.File):java.lang.String
      com.tendcloud.tenddata.bk.a(java.lang.String, java.lang.String, boolean):java.lang.String
      com.tendcloud.tenddata.bk.a(java.lang.String, java.lang.String, int):void
      com.tendcloud.tenddata.bk.a(java.net.URL, java.lang.String, boolean):java.net.URLConnection */
    private static d a(String str, String str2, String str3, byte[] bArr, String str4) {
        try {
            if (str2.startsWith("https://")) {
                c.put(Long.valueOf(Thread.currentThread().getId()), str4);
            }
            URLConnection a2 = (!str2.toLowerCase().startsWith("https") || !str3.trim().isEmpty()) ? a(a(str2, str), str4, true) : a(new URL(str2), str4, true);
            if (a2 == null) {
                return new d(600, "");
            }
            if (str2.toLowerCase().startsWith("https") && !str3.trim().isEmpty()) {
                a2 = a(a2, str3);
            }
            return a(bArr, a2);
        } catch (Throwable th) {
            return new d(600, "");
        }
    }

    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v2, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r2v4, types: [java.io.OutputStream] */
    /* JADX WARN: Type inference failed for: r2v6 */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r2v8 */
    /* JADX WARN: Type inference failed for: r2v10 */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r9.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        r9.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00b3, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x00c2, code lost:
        r1 = null;
        r2 = r3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0055 A[SYNTHETIC, Splitter:B:23:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005a A[SYNTHETIC, Splitter:B:26:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x005f A[SYNTHETIC, Splitter:B:29:0x005f] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x008f A[SYNTHETIC, Splitter:B:49:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0094 A[SYNTHETIC, Splitter:B:52:0x0094] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0099 A[SYNTHETIC, Splitter:B:55:0x0099] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00b3 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:9:0x0025] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.tendcloud.tenddata.bk.d a(byte[] r8, java.net.URLConnection r9) {
        /*
            r2 = 0
            r1 = 600(0x258, float:8.41E-43)
            r7 = 60000(0xea60, float:8.4078E-41)
            if (r8 == 0) goto L_0x000d
            int r0 = r8.length
            if (r0 == 0) goto L_0x000d
            if (r9 != 0) goto L_0x0015
        L_0x000d:
            com.tendcloud.tenddata.bk$d r0 = new com.tendcloud.tenddata.bk$d
            java.lang.String r2 = ""
            r0.<init>(r1, r2)
        L_0x0014:
            return r0
        L_0x0015:
            java.net.HttpURLConnection r9 = (java.net.HttpURLConnection) r9
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            java.lang.String r0 = "POST"
            r9.setRequestMethod(r0)     // Catch:{ Throwable -> 0x00b8, all -> 0x008b }
            java.io.OutputStream r3 = r9.getOutputStream()     // Catch:{ Throwable -> 0x00b8, all -> 0x008b }
            r3.write(r8)     // Catch:{ Throwable -> 0x00bc, all -> 0x00b3 }
            r3.close()     // Catch:{ Throwable -> 0x00bc, all -> 0x00b3 }
            int r0 = r9.getResponseCode()     // Catch:{ Throwable -> 0x00bc, all -> 0x00b3 }
            r1 = 400(0x190, float:5.6E-43)
            if (r0 <= r1) goto L_0x0071
            java.io.InputStream r1 = r9.getErrorStream()     // Catch:{ Throwable -> 0x00c1, all -> 0x00b3 }
            r4 = r1
        L_0x0038:
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x00c1, all -> 0x00b3 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x00c1, all -> 0x00b3 }
            r6.<init>(r4)     // Catch:{ Throwable -> 0x00c1, all -> 0x00b3 }
            r1.<init>(r6)     // Catch:{ Throwable -> 0x00c1, all -> 0x00b3 }
        L_0x0042:
            java.lang.String r2 = r1.readLine()     // Catch:{ Throwable -> 0x0051, all -> 0x00b5 }
            if (r2 == 0) goto L_0x0077
            r5.append(r2)     // Catch:{ Throwable -> 0x0051, all -> 0x00b5 }
            r2 = 10
            r5.append(r2)     // Catch:{ Throwable -> 0x0051, all -> 0x00b5 }
            goto L_0x0042
        L_0x0051:
            r2 = move-exception
            r2 = r3
        L_0x0053:
            if (r2 == 0) goto L_0x0058
            r2.close()     // Catch:{ Throwable -> 0x00a7 }
        L_0x0058:
            if (r1 == 0) goto L_0x005d
            r1.close()     // Catch:{ Throwable -> 0x00a9 }
        L_0x005d:
            if (r9 == 0) goto L_0x0062
            r9.disconnect()     // Catch:{ Throwable -> 0x00ab }
        L_0x0062:
            com.tendcloud.tenddata.bk.a = r7
            com.tendcloud.tenddata.bk.b = r7
        L_0x0066:
            com.tendcloud.tenddata.bk$d r1 = new com.tendcloud.tenddata.bk$d
            java.lang.String r2 = r5.toString()
            r1.<init>(r0, r2)
            r0 = r1
            goto L_0x0014
        L_0x0071:
            java.io.InputStream r1 = r9.getInputStream()     // Catch:{ Throwable -> 0x00c1, all -> 0x00b3 }
            r4 = r1
            goto L_0x0038
        L_0x0077:
            if (r3 == 0) goto L_0x007c
            r3.close()     // Catch:{ Throwable -> 0x00a1 }
        L_0x007c:
            if (r1 == 0) goto L_0x0081
            r1.close()     // Catch:{ Throwable -> 0x00a3 }
        L_0x0081:
            if (r9 == 0) goto L_0x0086
            r9.disconnect()     // Catch:{ Throwable -> 0x00a5 }
        L_0x0086:
            com.tendcloud.tenddata.bk.a = r7
            com.tendcloud.tenddata.bk.b = r7
            goto L_0x0066
        L_0x008b:
            r0 = move-exception
            r3 = r2
        L_0x008d:
            if (r3 == 0) goto L_0x0092
            r3.close()     // Catch:{ Throwable -> 0x00ad }
        L_0x0092:
            if (r2 == 0) goto L_0x0097
            r2.close()     // Catch:{ Throwable -> 0x00af }
        L_0x0097:
            if (r9 == 0) goto L_0x009c
            r9.disconnect()     // Catch:{ Throwable -> 0x00b1 }
        L_0x009c:
            com.tendcloud.tenddata.bk.a = r7
            com.tendcloud.tenddata.bk.b = r7
            throw r0
        L_0x00a1:
            r2 = move-exception
            goto L_0x007c
        L_0x00a3:
            r1 = move-exception
            goto L_0x0081
        L_0x00a5:
            r1 = move-exception
            goto L_0x0086
        L_0x00a7:
            r2 = move-exception
            goto L_0x0058
        L_0x00a9:
            r1 = move-exception
            goto L_0x005d
        L_0x00ab:
            r1 = move-exception
            goto L_0x0062
        L_0x00ad:
            r1 = move-exception
            goto L_0x0092
        L_0x00af:
            r1 = move-exception
            goto L_0x0097
        L_0x00b1:
            r1 = move-exception
            goto L_0x009c
        L_0x00b3:
            r0 = move-exception
            goto L_0x008d
        L_0x00b5:
            r0 = move-exception
            r2 = r1
            goto L_0x008d
        L_0x00b8:
            r0 = move-exception
            r0 = r1
            r1 = r2
            goto L_0x0053
        L_0x00bc:
            r0 = move-exception
            r0 = r1
            r1 = r2
            r2 = r3
            goto L_0x0053
        L_0x00c1:
            r1 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.bk.a(byte[], java.net.URLConnection):com.tendcloud.tenddata.bk$d");
    }

    private static synchronized String a(String str, int i) {
        String str2;
        synchronized (bk.class) {
            if (!ca.b(str) && h.containsKey(str)) {
                a aVar = (a) h.get(str);
                if (aVar != null) {
                    switch (i) {
                        case 1:
                            str2 = aVar.b;
                            break;
                        case 2:
                            str2 = aVar.d;
                            break;
                        case 3:
                            str2 = aVar.c;
                            break;
                        case 4:
                            str2 = aVar.a;
                            break;
                        default:
                            str2 = null;
                            break;
                    }
                } else {
                    str2 = null;
                }
            } else {
                str2 = null;
            }
        }
        return str2;
    }

    public static String a(String str, File file) {
        return b(str, (String) null, file);
    }

    public static String a(String str, String str2, File file) {
        return b(str, str2, file);
    }

    public static String a(String str, String str2, boolean z) {
        return b(str, str2, z);
    }

    public static String a(String str, boolean z) {
        return b(str, (String) null, z);
    }

    private static URL a(String str, String str2) {
        URL url = new URL(str);
        return br.a() ? url : new URL(url.getProtocol(), str2, url.getPort(), url.getFile());
    }

    private static URLConnection a(URL url, String str, boolean z) {
        try {
            URLConnection openConnection = url.openConnection();
            openConnection.setRequestProperty(HttpRequestHeader.AcceptEncoding, "");
            openConnection.setRequestProperty(HttpRequestHeader.UserAgent, "");
            if (str != null) {
                openConnection.setRequestProperty(HttpRequestHeader.Host, str);
                openConnection.setRequestProperty("Content-Type", "");
                if (!ca.b(str) && str.equals("ev1.xdrig.com")) {
                    openConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                }
            } else {
                String host = url.getHost();
                if (ca.b(host) || host.equals(fm.a)) {
                }
            }
            openConnection.setDoInput(true);
            if (z) {
                openConnection.setDoOutput(true);
            }
            openConnection.setConnectTimeout(a);
            openConnection.setReadTimeout(b);
            return openConnection;
        } catch (Throwable th) {
            return null;
        }
    }

    private static HttpsURLConnection a(URLConnection uRLConnection, String str) {
        try {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) uRLConnection;
            SSLContext instance = ca.a(16) ? SSLContext.getInstance("TLSv1.2") : SSLContext.getInstance("TLSv1");
            instance.init(null, new TrustManager[]{new c(b(str))}, null);
            httpsURLConnection.setHostnameVerifier(new bl());
            httpsURLConnection.setSSLSocketFactory(instance.getSocketFactory());
            return httpsURLConnection;
        } catch (Throwable th) {
            return null;
        }
    }

    public static SSLSocketFactory a(boolean z, X509Certificate x509Certificate) {
        try {
            SSLContext instance = SSLContext.getInstance("TLS");
            if (!z || x509Certificate == null) {
                instance.init(null, null, null);
            } else {
                instance.init(null, new TrustManager[]{new c(x509Certificate)}, null);
            }
            return instance.getSocketFactory();
        } catch (Throwable th) {
            return null;
        }
    }

    private static void a(String str) {
        String a2 = a(str, 1);
        if (a2 != null && !a2.equalsIgnoreCase(a(str, 3)) && g != null) {
            PreferenceManager.getDefaultSharedPreferences(g).edit().putString(ca.d(str), a(str, 1)).apply();
            a(str, a(str, 1), 3);
        }
    }

    private static synchronized void a(String str, String str2, int i) {
        synchronized (bk.class) {
            if (!ca.b(str) && h.containsKey(str)) {
                a aVar = (a) h.get(str);
                switch (i) {
                    case 1:
                        aVar.b = str2;
                        break;
                    case 2:
                        aVar.d = str2;
                        break;
                    case 3:
                        aVar.c = str2;
                        break;
                    case 4:
                        aVar.a = str2;
                        break;
                }
            }
        }
    }

    private static void a(HttpsURLConnection httpsURLConnection) {
        if (httpsURLConnection != null) {
            try {
                System.out.println("Response url: " + httpsURLConnection.getURL());
                System.out.println("Response Code : " + httpsURLConnection.getResponseCode());
                System.out.println("Cipher Suite : " + httpsURLConnection.getCipherSuite());
                System.out.println("\n");
                for (Certificate certificate : httpsURLConnection.getServerCertificates()) {
                    System.out.println("Cert Type : " + certificate.getType());
                    System.out.println("Cert Hash Code : " + certificate.hashCode());
                    System.out.println("Cert Public Key Algorithm : " + certificate.getPublicKey().getAlgorithm());
                    System.out.println("Cert Public Key Format : " + certificate.getPublicKey().getFormat());
                    System.out.println("\n");
                }
            } catch (Throwable th) {
            }
        }
    }

    private static byte[] a(InputStream inputStream) {
        try {
            GZIPInputStream gZIPInputStream = new GZIPInputStream(inputStream);
            byte[] bArr = new byte[1024];
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                int read = gZIPInputStream.read(bArr, 0, bArr.length);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    byteArrayOutputStream.flush();
                    byteArrayOutputStream.close();
                    gZIPInputStream.close();
                    return byteArray;
                }
            }
        } catch (Throwable th) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.bk.a(java.net.URL, java.lang.String, boolean):java.net.URLConnection
     arg types: [java.net.URL, ?[OBJECT, ARRAY], int]
     candidates:
      com.tendcloud.tenddata.bk.a(java.lang.String, java.lang.String, java.io.File):java.lang.String
      com.tendcloud.tenddata.bk.a(java.lang.String, java.lang.String, boolean):java.lang.String
      com.tendcloud.tenddata.bk.a(java.lang.String, java.lang.String, int):void
      com.tendcloud.tenddata.bk.a(java.net.URL, java.lang.String, boolean):java.net.URLConnection */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0071 A[SYNTHETIC, Splitter:B:27:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0098 A[SYNTHETIC, Splitter:B:43:0x0098] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String b(java.lang.String r8, java.lang.String r9, java.io.File r10) {
        /*
            r1 = 0
            java.net.URL r2 = new java.net.URL     // Catch:{ Throwable -> 0x00a6, all -> 0x0094 }
            r2.<init>(r8)     // Catch:{ Throwable -> 0x00a6, all -> 0x0094 }
            r0 = 0
            r3 = 0
            java.net.URLConnection r0 = a(r2, r0, r3)     // Catch:{ Throwable -> 0x00a6, all -> 0x0094 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Throwable -> 0x00a6, all -> 0x0094 }
            java.lang.String r3 = r8.toLowerCase()     // Catch:{ Throwable -> 0x00a9, all -> 0x00a0 }
            java.lang.String r4 = "https"
            boolean r3 = r3.startsWith(r4)     // Catch:{ Throwable -> 0x00a9, all -> 0x00a0 }
            if (r3 == 0) goto L_0x00ab
            boolean r3 = com.tendcloud.tenddata.ca.b(r9)     // Catch:{ Throwable -> 0x00a9, all -> 0x00a0 }
            if (r3 != 0) goto L_0x00ab
            java.util.HashMap r3 = com.tendcloud.tenddata.bk.c     // Catch:{ Throwable -> 0x00a9, all -> 0x00a0 }
            java.lang.Thread r4 = java.lang.Thread.currentThread()     // Catch:{ Throwable -> 0x00a9, all -> 0x00a0 }
            long r4 = r4.getId()     // Catch:{ Throwable -> 0x00a9, all -> 0x00a0 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ Throwable -> 0x00a9, all -> 0x00a0 }
            java.lang.String r2 = r2.getHost()     // Catch:{ Throwable -> 0x00a9, all -> 0x00a0 }
            r3.put(r4, r2)     // Catch:{ Throwable -> 0x00a9, all -> 0x00a0 }
            javax.net.ssl.HttpsURLConnection r0 = a(r0, r9)     // Catch:{ Throwable -> 0x00a9, all -> 0x00a0 }
            r2 = r0
        L_0x003a:
            int r0 = r2.getResponseCode()     // Catch:{ Throwable -> 0x006d, all -> 0x00a4 }
            r3 = 200(0xc8, float:2.8E-43)
            if (r0 != r3) goto L_0x008c
            java.io.InputStream r3 = r2.getInputStream()     // Catch:{ Throwable -> 0x006d, all -> 0x00a4 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Throwable -> 0x006d, all -> 0x00a4 }
            r4.<init>(r10)     // Catch:{ Throwable -> 0x006d, all -> 0x00a4 }
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ Throwable -> 0x006d, all -> 0x00a4 }
            r5 = 4096(0x1000, float:5.74E-42)
            byte[] r5 = new byte[r5]     // Catch:{ all -> 0x0065 }
        L_0x0055:
            int r6 = r3.read(r5)     // Catch:{ all -> 0x0065 }
            r7 = -1
            if (r6 == r7) goto L_0x0076
            r7 = 0
            r4.write(r5, r7, r6)     // Catch:{ all -> 0x0065 }
            r7 = 0
            r0.update(r5, r7, r6)     // Catch:{ all -> 0x0065 }
            goto L_0x0055
        L_0x0065:
            r0 = move-exception
            r4.close()     // Catch:{ Throwable -> 0x006d, all -> 0x00a4 }
            r3.close()     // Catch:{ Throwable -> 0x006d, all -> 0x00a4 }
            throw r0     // Catch:{ Throwable -> 0x006d, all -> 0x00a4 }
        L_0x006d:
            r0 = move-exception
            r0 = r2
        L_0x006f:
            if (r0 == 0) goto L_0x0074
            r0.disconnect()     // Catch:{ Throwable -> 0x009c }
        L_0x0074:
            r0 = r1
        L_0x0075:
            return r0
        L_0x0076:
            r4.close()     // Catch:{ Throwable -> 0x006d, all -> 0x00a4 }
            r3.close()     // Catch:{ Throwable -> 0x006d, all -> 0x00a4 }
            byte[] r0 = r0.digest()     // Catch:{ Throwable -> 0x006d, all -> 0x00a4 }
            java.lang.String r0 = com.tendcloud.tenddata.ca.a(r0)     // Catch:{ Throwable -> 0x006d, all -> 0x00a4 }
            if (r2 == 0) goto L_0x0075
            r2.disconnect()     // Catch:{ Throwable -> 0x008a }
            goto L_0x0075
        L_0x008a:
            r1 = move-exception
            goto L_0x0075
        L_0x008c:
            if (r2 == 0) goto L_0x0074
            r2.disconnect()     // Catch:{ Throwable -> 0x0092 }
            goto L_0x0074
        L_0x0092:
            r0 = move-exception
            goto L_0x0074
        L_0x0094:
            r0 = move-exception
            r2 = r1
        L_0x0096:
            if (r2 == 0) goto L_0x009b
            r2.disconnect()     // Catch:{ Throwable -> 0x009e }
        L_0x009b:
            throw r0
        L_0x009c:
            r0 = move-exception
            goto L_0x0074
        L_0x009e:
            r1 = move-exception
            goto L_0x009b
        L_0x00a0:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x0096
        L_0x00a4:
            r0 = move-exception
            goto L_0x0096
        L_0x00a6:
            r0 = move-exception
            r0 = r1
            goto L_0x006f
        L_0x00a9:
            r2 = move-exception
            goto L_0x006f
        L_0x00ab:
            r2 = r0
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.bk.b(java.lang.String, java.lang.String, java.io.File):java.lang.String");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.bk.a(java.net.URL, java.lang.String, boolean):java.net.URLConnection
     arg types: [java.net.URL, ?[OBJECT, ARRAY], int]
     candidates:
      com.tendcloud.tenddata.bk.a(java.lang.String, java.lang.String, java.io.File):java.lang.String
      com.tendcloud.tenddata.bk.a(java.lang.String, java.lang.String, boolean):java.lang.String
      com.tendcloud.tenddata.bk.a(java.lang.String, java.lang.String, int):void
      com.tendcloud.tenddata.bk.a(java.net.URL, java.lang.String, boolean):java.net.URLConnection */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009a A[SYNTHETIC, Splitter:B:35:0x009a] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x009f A[SYNTHETIC, Splitter:B:38:0x009f] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00b8 A[SYNTHETIC, Splitter:B:52:0x00b8] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00bd A[SYNTHETIC, Splitter:B:55:0x00bd] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String b(java.lang.String r9, java.lang.String r10, boolean r11) {
        /*
            r1 = 0
            boolean r0 = com.tendcloud.tenddata.ca.b(r9)
            if (r0 == 0) goto L_0x0009
            r0 = r1
        L_0x0008:
            return r0
        L_0x0009:
            r3 = 0
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            java.net.URL r2 = new java.net.URL     // Catch:{ Throwable -> 0x00db, all -> 0x00b4 }
            r2.<init>(r9)     // Catch:{ Throwable -> 0x00db, all -> 0x00b4 }
            r0 = 0
            r5 = 0
            java.net.URLConnection r0 = a(r2, r0, r5)     // Catch:{ Throwable -> 0x00db, all -> 0x00b4 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Throwable -> 0x00db, all -> 0x00b4 }
            java.lang.String r5 = r9.toLowerCase()     // Catch:{ Throwable -> 0x00de, all -> 0x00cf }
            java.lang.String r6 = "https"
            boolean r5 = r5.startsWith(r6)     // Catch:{ Throwable -> 0x00de, all -> 0x00cf }
            if (r5 == 0) goto L_0x00e3
            boolean r5 = com.tendcloud.tenddata.ca.b(r10)     // Catch:{ Throwable -> 0x00de, all -> 0x00cf }
            if (r5 != 0) goto L_0x00e3
            java.util.HashMap r5 = com.tendcloud.tenddata.bk.c     // Catch:{ Throwable -> 0x00de, all -> 0x00cf }
            java.lang.Thread r6 = java.lang.Thread.currentThread()     // Catch:{ Throwable -> 0x00de, all -> 0x00cf }
            long r6 = r6.getId()     // Catch:{ Throwable -> 0x00de, all -> 0x00cf }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ Throwable -> 0x00de, all -> 0x00cf }
            java.lang.String r2 = r2.getHost()     // Catch:{ Throwable -> 0x00de, all -> 0x00cf }
            r5.put(r6, r2)     // Catch:{ Throwable -> 0x00de, all -> 0x00cf }
            javax.net.ssl.HttpsURLConnection r0 = a(r0, r10)     // Catch:{ Throwable -> 0x00de, all -> 0x00cf }
            r2 = r0
        L_0x0048:
            java.lang.String r0 = "GET"
            r2.setRequestMethod(r0)     // Catch:{ Throwable -> 0x00e0, all -> 0x00d4 }
            int r0 = r2.getResponseCode()     // Catch:{ Throwable -> 0x00e0, all -> 0x00d4 }
            r5 = 200(0xc8, float:2.8E-43)
            if (r0 != r5) goto L_0x00a7
            if (r11 == 0) goto L_0x0078
            java.lang.String r0 = new java.lang.String     // Catch:{ Throwable -> 0x00e0, all -> 0x00d4 }
            java.io.InputStream r3 = r2.getInputStream()     // Catch:{ Throwable -> 0x00e0, all -> 0x00d4 }
            byte[] r3 = a(r3)     // Catch:{ Throwable -> 0x00e0, all -> 0x00d4 }
            java.lang.String r5 = "utf-8"
            r0.<init>(r3, r5)     // Catch:{ Throwable -> 0x00e0, all -> 0x00d4 }
            r4.append(r0)     // Catch:{ Throwable -> 0x00e0, all -> 0x00d4 }
        L_0x0069:
            if (r1 == 0) goto L_0x006e
            r1.close()     // Catch:{ Throwable -> 0x00c5 }
        L_0x006e:
            if (r2 == 0) goto L_0x0073
            r2.disconnect()     // Catch:{ Throwable -> 0x00c7 }
        L_0x0073:
            java.lang.String r0 = r4.toString()
            goto L_0x0008
        L_0x0078:
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x00e0, all -> 0x00d4 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x00e0, all -> 0x00d4 }
            java.io.InputStream r5 = r2.getInputStream()     // Catch:{ Throwable -> 0x00e0, all -> 0x00d4 }
            r3.<init>(r5)     // Catch:{ Throwable -> 0x00e0, all -> 0x00d4 }
            r0.<init>(r3)     // Catch:{ Throwable -> 0x00e0, all -> 0x00d4 }
        L_0x0086:
            java.lang.String r1 = r0.readLine()     // Catch:{ Throwable -> 0x0095, all -> 0x00d6 }
            if (r1 == 0) goto L_0x00a5
            java.lang.String r3 = "/n"
            r4.append(r3)     // Catch:{ Throwable -> 0x0095, all -> 0x00d6 }
            r4.append(r1)     // Catch:{ Throwable -> 0x0095, all -> 0x00d6 }
            goto L_0x0086
        L_0x0095:
            r1 = move-exception
            r1 = r0
            r0 = r2
        L_0x0098:
            if (r1 == 0) goto L_0x009d
            r1.close()     // Catch:{ Throwable -> 0x00c9 }
        L_0x009d:
            if (r0 == 0) goto L_0x0073
            r0.disconnect()     // Catch:{ Throwable -> 0x00a3 }
            goto L_0x0073
        L_0x00a3:
            r0 = move-exception
            goto L_0x0073
        L_0x00a5:
            r1 = r0
            goto L_0x0069
        L_0x00a7:
            if (r1 == 0) goto L_0x00ac
            r3.close()     // Catch:{ Throwable -> 0x00c1 }
        L_0x00ac:
            if (r2 == 0) goto L_0x00b1
            r2.disconnect()     // Catch:{ Throwable -> 0x00c3 }
        L_0x00b1:
            r0 = r1
            goto L_0x0008
        L_0x00b4:
            r0 = move-exception
            r2 = r1
        L_0x00b6:
            if (r1 == 0) goto L_0x00bb
            r1.close()     // Catch:{ Throwable -> 0x00cb }
        L_0x00bb:
            if (r2 == 0) goto L_0x00c0
            r2.disconnect()     // Catch:{ Throwable -> 0x00cd }
        L_0x00c0:
            throw r0
        L_0x00c1:
            r0 = move-exception
            goto L_0x00ac
        L_0x00c3:
            r0 = move-exception
            goto L_0x00b1
        L_0x00c5:
            r0 = move-exception
            goto L_0x006e
        L_0x00c7:
            r0 = move-exception
            goto L_0x0073
        L_0x00c9:
            r1 = move-exception
            goto L_0x009d
        L_0x00cb:
            r1 = move-exception
            goto L_0x00bb
        L_0x00cd:
            r1 = move-exception
            goto L_0x00c0
        L_0x00cf:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
            goto L_0x00b6
        L_0x00d4:
            r0 = move-exception
            goto L_0x00b6
        L_0x00d6:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00b6
        L_0x00db:
            r0 = move-exception
            r0 = r1
            goto L_0x0098
        L_0x00de:
            r2 = move-exception
            goto L_0x0098
        L_0x00e0:
            r0 = move-exception
            r0 = r2
            goto L_0x0098
        L_0x00e3:
            r2 = r0
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.bk.b(java.lang.String, java.lang.String, boolean):java.lang.String");
    }

    private static X509Certificate b(String str) {
        if (str == null || str.trim().isEmpty()) {
            return null;
        }
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(str.getBytes());
        try {
            X509Certificate x509Certificate = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(byteArrayInputStream);
            if (byteArrayInputStream == null) {
                return x509Certificate;
            }
            try {
                byteArrayInputStream.close();
                return x509Certificate;
            } catch (Throwable th) {
                return x509Certificate;
            }
        } catch (Exception e2) {
            if (byteArrayInputStream != null) {
                byteArrayInputStream.close();
            }
            return null;
        } catch (Throwable th2) {
        }
        throw th;
    }

    private static synchronized void b(String str, String str2) {
        synchronized (bk.class) {
            if (!ca.b(str) && !h.containsKey(str)) {
                a aVar = new a();
                aVar.e = str;
                aVar.a = str2;
                aVar.c = PreferenceManager.getDefaultSharedPreferences(g).getString(ca.d(str), null);
                try {
                    aVar.b = InetAddress.getByName(str).getHostAddress();
                } catch (Throwable th) {
                }
                h.put(str, aVar);
            }
        }
    }
}
