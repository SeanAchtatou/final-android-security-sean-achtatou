package com.tendcloud.tenddata;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.SystemClock;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellLocation;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.NeighboringCellInfo;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import cn.egame.terminal.paysdk.EgamePay;
import java.lang.reflect.Method;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Enumeration;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class br {
    static TelephonyManager a = null;
    static final String b = "www.talkingdata.net";
    static final int c = 80;
    static boolean d = false;
    static final long e = 300000;
    static long f = -300000;
    private static final String[] g = {"UNKNOWN", "GPRS", "EDGE", "UMTS", "CDMA", "EVDO_0", "EVDO_A", "1xRTT", "HSDPA", "HSUPA", "HSPA", "IDEN", "EVDO_B", "LTE", "EHRPD", "HSPAP"};
    private static final String[] h = {"NONE", "GSM", "CDMA", "SIP"};

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static org.json.JSONArray A(android.content.Context r10) {
        /*
            r4 = 0
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ Throwable -> 0x00a6 }
            r3.<init>()     // Catch:{ Throwable -> 0x00a6 }
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r10.getSystemService(r0)     // Catch:{ Throwable -> 0x00a6 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Throwable -> 0x00a6 }
            java.lang.String r1 = "com.android.internal.telephony.Phone"
            java.lang.Class r2 = java.lang.Class.forName(r1)     // Catch:{ Throwable -> 0x00a6 }
            java.lang.String r1 = "GEMINI_SIM_1"
            java.lang.reflect.Field r1 = r2.getField(r1)     // Catch:{ Throwable -> 0x004d }
            r5 = 1
            r1.setAccessible(r5)     // Catch:{ Throwable -> 0x004d }
            r5 = 0
            java.lang.Object r1 = r1.get(r5)     // Catch:{ Throwable -> 0x004d }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Throwable -> 0x004d }
            java.lang.String r5 = "GEMINI_SIM_2"
            java.lang.reflect.Field r2 = r2.getField(r5)     // Catch:{ Throwable -> 0x004d }
            r5 = 1
            r2.setAccessible(r5)     // Catch:{ Throwable -> 0x004d }
            r5 = 0
            java.lang.Object r2 = r2.get(r5)     // Catch:{ Throwable -> 0x004d }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Throwable -> 0x004d }
            r5 = r1
        L_0x0037:
            java.lang.Class<android.telephony.TelephonyManager> r1 = android.telephony.TelephonyManager.class
            java.lang.String r6 = "getDeviceIdGemini"
            r7 = 1
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Throwable -> 0x00a6 }
            r8 = 0
            java.lang.Class r9 = java.lang.Integer.TYPE     // Catch:{ Throwable -> 0x00a6 }
            r7[r8] = r9     // Catch:{ Throwable -> 0x00a6 }
            java.lang.reflect.Method r6 = r1.getDeclaredMethod(r6, r7)     // Catch:{ Throwable -> 0x00a6 }
            if (r0 == 0) goto L_0x004b
            if (r6 != 0) goto L_0x005a
        L_0x004b:
            r0 = r4
        L_0x004c:
            return r0
        L_0x004d:
            r1 = move-exception
            r1 = 0
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Throwable -> 0x00a6 }
            r2 = 1
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Throwable -> 0x00a6 }
            r5 = r1
            goto L_0x0037
        L_0x005a:
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x00a6 }
            r7 = 0
            r1[r7] = r5     // Catch:{ Throwable -> 0x00a6 }
            java.lang.Object r1 = r6.invoke(r0, r1)     // Catch:{ Throwable -> 0x00a6 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Throwable -> 0x00a6 }
            java.lang.String r7 = r1.trim()     // Catch:{ Throwable -> 0x00a6 }
            r1 = 1
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Throwable -> 0x00a6 }
            r8 = 0
            r1[r8] = r2     // Catch:{ Throwable -> 0x00a6 }
            java.lang.Object r1 = r6.invoke(r0, r1)     // Catch:{ Throwable -> 0x00a6 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Throwable -> 0x00a6 }
            java.lang.String r1 = r1.trim()     // Catch:{ Throwable -> 0x00a6 }
            java.lang.Boolean r6 = b(r7)     // Catch:{ Throwable -> 0x00a6 }
            boolean r6 = r6.booleanValue()     // Catch:{ Throwable -> 0x00a6 }
            if (r6 == 0) goto L_0x008f
            java.lang.Class<android.telephony.TelephonyManager> r6 = android.telephony.TelephonyManager.class
            java.lang.String r8 = "Gemini"
            org.json.JSONObject r5 = a(r6, r0, r5, r7, r8)     // Catch:{ Throwable -> 0x00a6 }
            r3.put(r5)     // Catch:{ Throwable -> 0x00a6 }
        L_0x008f:
            java.lang.Boolean r5 = b(r1)     // Catch:{ Throwable -> 0x00a6 }
            boolean r5 = r5.booleanValue()     // Catch:{ Throwable -> 0x00a6 }
            if (r5 == 0) goto L_0x00a4
            java.lang.Class<android.telephony.TelephonyManager> r5 = android.telephony.TelephonyManager.class
            java.lang.String r6 = "Gemini"
            org.json.JSONObject r0 = a(r5, r0, r2, r1, r6)     // Catch:{ Throwable -> 0x00a6 }
            r3.put(r0)     // Catch:{ Throwable -> 0x00a6 }
        L_0x00a4:
            r0 = r3
            goto L_0x004c
        L_0x00a6:
            r0 = move-exception
            r0 = r4
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.br.A(android.content.Context):org.json.JSONArray");
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static org.json.JSONArray B(android.content.Context r10) {
        /*
            r4 = 0
            org.json.JSONArray r3 = new org.json.JSONArray     // Catch:{ Throwable -> 0x00a2 }
            r3.<init>()     // Catch:{ Throwable -> 0x00a2 }
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r10.getSystemService(r0)     // Catch:{ Throwable -> 0x00a2 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Throwable -> 0x00a2 }
            java.lang.String r1 = "com.android.internal.telephony.Phone"
            java.lang.Class r2 = java.lang.Class.forName(r1)     // Catch:{ Throwable -> 0x00a2 }
            java.lang.String r1 = "GEMINI_SIM_1"
            java.lang.reflect.Field r1 = r2.getField(r1)     // Catch:{ Throwable -> 0x0096 }
            r5 = 1
            r1.setAccessible(r5)     // Catch:{ Throwable -> 0x0096 }
            r5 = 0
            java.lang.Object r1 = r1.get(r5)     // Catch:{ Throwable -> 0x0096 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Throwable -> 0x0096 }
            java.lang.String r5 = "GEMINI_SIM_2"
            java.lang.reflect.Field r2 = r2.getField(r5)     // Catch:{ Throwable -> 0x0096 }
            r5 = 1
            r2.setAccessible(r5)     // Catch:{ Throwable -> 0x0096 }
            r5 = 0
            java.lang.Object r2 = r2.get(r5)     // Catch:{ Throwable -> 0x0096 }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ Throwable -> 0x0096 }
        L_0x0036:
            java.lang.Class<android.telephony.TelephonyManager> r5 = android.telephony.TelephonyManager.class
            java.lang.String r6 = "getDefault"
            r7 = 1
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Throwable -> 0x00a2 }
            r8 = 0
            java.lang.Class r9 = java.lang.Integer.TYPE     // Catch:{ Throwable -> 0x00a2 }
            r7[r8] = r9     // Catch:{ Throwable -> 0x00a2 }
            java.lang.reflect.Method r5 = r5.getMethod(r6, r7)     // Catch:{ Throwable -> 0x00a2 }
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x00a2 }
            r7 = 0
            r6[r7] = r1     // Catch:{ Throwable -> 0x00a2 }
            java.lang.Object r1 = r5.invoke(r0, r6)     // Catch:{ Throwable -> 0x00a2 }
            android.telephony.TelephonyManager r1 = (android.telephony.TelephonyManager) r1     // Catch:{ Throwable -> 0x00a2 }
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x00a2 }
            r7 = 0
            r6[r7] = r2     // Catch:{ Throwable -> 0x00a2 }
            java.lang.Object r0 = r5.invoke(r0, r6)     // Catch:{ Throwable -> 0x00a2 }
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0     // Catch:{ Throwable -> 0x00a2 }
            java.lang.String r2 = r1.getDeviceId()     // Catch:{ Throwable -> 0x00a2 }
            java.lang.String r2 = r2.trim()     // Catch:{ Throwable -> 0x00a2 }
            java.lang.String r5 = r0.getDeviceId()     // Catch:{ Throwable -> 0x00a2 }
            java.lang.String r5 = r5.trim()     // Catch:{ Throwable -> 0x00a2 }
            java.lang.Boolean r6 = b(r2)     // Catch:{ Throwable -> 0x00a2 }
            boolean r6 = r6.booleanValue()     // Catch:{ Throwable -> 0x00a2 }
            if (r6 == 0) goto L_0x0081
            org.json.JSONObject r1 = a(r1, r2)     // Catch:{ Throwable -> 0x00a2 }
            if (r1 == 0) goto L_0x0081
            r3.put(r1)     // Catch:{ Throwable -> 0x00a2 }
        L_0x0081:
            java.lang.Boolean r1 = b(r5)     // Catch:{ Throwable -> 0x00a2 }
            boolean r1 = r1.booleanValue()     // Catch:{ Throwable -> 0x00a2 }
            if (r1 == 0) goto L_0x0094
            org.json.JSONObject r0 = a(r0, r5)     // Catch:{ Throwable -> 0x00a2 }
            if (r0 == 0) goto L_0x0094
            r3.put(r0)     // Catch:{ Throwable -> 0x00a2 }
        L_0x0094:
            r0 = r3
        L_0x0095:
            return r0
        L_0x0096:
            r1 = move-exception
            r1 = 0
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Throwable -> 0x00a2 }
            r2 = 1
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Throwable -> 0x00a2 }
            goto L_0x0036
        L_0x00a2:
            r0 = move-exception
            r0 = r4
            goto L_0x0095
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.br.B(android.content.Context):org.json.JSONArray");
    }

    private static JSONArray C(Context context) {
        JSONObject a2;
        JSONObject a3;
        try {
            JSONArray jSONArray = new JSONArray();
            Class<?> cls = Class.forName("com.android.internal.telephony.PhoneFactory");
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            String trim = telephonyManager.getDeviceId().trim();
            TelephonyManager telephonyManager2 = (TelephonyManager) context.getSystemService((String) cls.getMethod("getServiceName", String.class, Integer.TYPE).invoke(cls, "phone", 1));
            String trim2 = telephonyManager2.getDeviceId().trim();
            if (b(trim).booleanValue() && (a3 = a(telephonyManager, trim)) != null) {
                jSONArray.put(a3);
            }
            if (b(trim2).booleanValue() && (a2 = a(telephonyManager2, trim2)) != null) {
                jSONArray.put(a2);
            }
            return jSONArray;
        } catch (Throwable th) {
            return null;
        }
    }

    private static JSONArray D(Context context) {
        try {
            JSONArray jSONArray = new JSONArray();
            Class<?> cls = Class.forName("android.telephony.MSimTelephonyManager");
            Object systemService = context.getSystemService("phone_msim");
            Method method = cls.getMethod("getDeviceId", Integer.TYPE);
            String trim = ((String) method.invoke(systemService, 0)).trim();
            String trim2 = ((String) method.invoke(systemService, 1)).trim();
            if (b(trim).booleanValue()) {
                jSONArray.put(a(cls, systemService, 0, trim, ""));
            }
            if (b(trim2).booleanValue()) {
                jSONArray.put(a(cls, systemService, 1, trim2, ""));
            }
            return jSONArray;
        } catch (Throwable th) {
            return null;
        }
    }

    private static Boolean a(String str) {
        try {
            char charAt = str.length() > 0 ? str.charAt(0) : '0';
            for (int i = 0; i < str.length(); i++) {
                if (charAt != str.charAt(i)) {
                    return false;
                }
            }
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    private static String a(int i) {
        return (i < 0 || i >= g.length) ? String.valueOf(i) : g[i];
    }

    private static JSONArray a(BitSet bitSet) {
        int cardinality = bitSet.cardinality();
        if (bitSet != null && cardinality >= 1) {
            JSONArray jSONArray = new JSONArray();
            int nextSetBit = bitSet.nextSetBit(0);
            while (nextSetBit >= 0) {
                jSONArray.put(nextSetBit);
                nextSetBit = bitSet.nextSetBit(nextSetBit + 1);
            }
        }
        return null;
    }

    public static JSONObject a(int i, int i2) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("lat", i);
            jSONObject.put("lng", i2);
            jSONObject.put("unit", "qd");
        } catch (Throwable th) {
        }
        return jSONObject;
    }

    private static JSONObject a(TelephonyManager telephonyManager, SubscriptionManager subscriptionManager, int i) {
        SubscriptionInfo activeSubscriptionInfoForSimSlotIndex;
        JSONObject jSONObject = new JSONObject();
        try {
            if (ca.a(22) && (activeSubscriptionInfoForSimSlotIndex = subscriptionManager.getActiveSubscriptionInfoForSimSlotIndex(i)) != null) {
                jSONObject.put("simSerialNumber", activeSubscriptionInfoForSimSlotIndex.getIccId() == null ? "" : activeSubscriptionInfoForSimSlotIndex.getIccId());
                jSONObject.put("simOperator", activeSubscriptionInfoForSimSlotIndex.getMcc() + "0" + activeSubscriptionInfoForSimSlotIndex.getMnc());
                jSONObject.put("simOperatorName", activeSubscriptionInfoForSimSlotIndex.getCarrierName() == null ? "" : activeSubscriptionInfoForSimSlotIndex.getCarrierName());
                jSONObject.put("simCountryIso", activeSubscriptionInfoForSimSlotIndex.getCountryIso() == null ? "" : activeSubscriptionInfoForSimSlotIndex.getCountryIso());
                int subscriptionId = activeSubscriptionInfoForSimSlotIndex.getSubscriptionId();
                Method method = telephonyManager.getClass().getMethod("getSubscriberId", Integer.TYPE);
                method.setAccessible(true);
                Object invoke = method.invoke(telephonyManager, Integer.valueOf(subscriptionId));
                if (invoke == null) {
                    invoke = "";
                }
                jSONObject.put("subscriberId", invoke);
            }
        } catch (Throwable th) {
        }
        return jSONObject;
    }

    private static JSONObject a(TelephonyManager telephonyManager, String str) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("imei", str.trim());
            jSONObject.put("subscriberId", telephonyManager.getSubscriberId() == null ? "" : telephonyManager.getSubscriberId());
            jSONObject.put("simSerialNumber", telephonyManager.getSimSerialNumber() == null ? "" : telephonyManager.getSimSerialNumber());
            jSONObject.put("dataState", telephonyManager.getDataState());
            jSONObject.put("networkType", a(telephonyManager.getNetworkType()));
            jSONObject.put("networkOperator", telephonyManager.getNetworkOperator());
            jSONObject.put("phoneType", b(telephonyManager.getPhoneType()));
            jSONObject.put("simOperator", telephonyManager.getSimOperator() == null ? "" : telephonyManager.getSimOperator());
            jSONObject.put("simOperatorName", telephonyManager.getSimOperatorName() == null ? "" : telephonyManager.getSimOperatorName());
            jSONObject.put("simCountryIso", telephonyManager.getSimCountryIso() == null ? "" : telephonyManager.getSimCountryIso());
            return jSONObject;
        } catch (Throwable th) {
            return null;
        }
    }

    private static JSONObject a(Class cls, Object obj, Integer num, String str, String str2) {
        String trim;
        String trim2;
        String trim3;
        String trim4;
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("imei", str);
        try {
            Method method = cls.getMethod("getSubscriberId" + str2, Integer.TYPE);
            if (method.invoke(obj, num) == null) {
                trim4 = "";
            } else {
                trim4 = ((String) method.invoke(obj, num)).trim();
            }
            jSONObject.put("subscriberId", trim4);
        } catch (Throwable th) {
        }
        try {
            Method method2 = cls.getMethod("getSimSerialNumber" + str2, Integer.TYPE);
            if (method2.invoke(obj, num) == null) {
                trim3 = "";
            } else {
                trim3 = ((String) method2.invoke(obj, num)).trim();
            }
            jSONObject.put("simSerialNumber", trim3);
        } catch (Throwable th2) {
        }
        try {
            jSONObject.put("dataState", (Integer) cls.getMethod("getDataState" + str2, Integer.TYPE).invoke(obj, num));
        } catch (Throwable th3) {
        }
        try {
            jSONObject.put("networkType", a(((Integer) cls.getMethod("getNetworkType" + str2, Integer.TYPE).invoke(obj, num)).intValue()));
        } catch (Throwable th4) {
        }
        try {
            jSONObject.put("networkOperator", (String) cls.getMethod("getNetworkOperator" + str2, Integer.TYPE).invoke(obj, num));
        } catch (Throwable th5) {
        }
        try {
            jSONObject.put("phoneType", b(((Integer) cls.getMethod("getPhoneType" + str2, Integer.TYPE).invoke(obj, num)).intValue()));
        } catch (Throwable th6) {
        }
        try {
            Method method3 = cls.getMethod("getSimOperator" + str2, Integer.TYPE);
            if (method3.invoke(obj, num) == null) {
                trim2 = "";
            } else {
                trim2 = ((String) method3.invoke(obj, num)).trim();
            }
            jSONObject.put("simOperator", trim2);
        } catch (Throwable th7) {
        }
        try {
            Method method4 = cls.getMethod("getSimOperatorName" + str2, Integer.TYPE);
            if (method4.invoke(obj, num) == null) {
                trim = "";
            } else {
                trim = ((String) method4.invoke(obj, num)).trim();
            }
            jSONObject.put("simOperatorName", trim);
        } catch (Throwable th8) {
        }
        return jSONObject;
    }

    static void a(Context context) {
        try {
            a = (TelephonyManager) context.getSystemService("phone");
        } catch (Throwable th) {
        }
    }

    public static boolean a() {
        try {
            return ca.a(11) ? !TextUtils.isEmpty(System.getProperty("http.proxyHost")) : !TextUtils.isEmpty(Proxy.getDefaultHost());
        } catch (Throwable th) {
            return false;
        }
    }

    private static Boolean b(String str) {
        try {
            Integer valueOf = Integer.valueOf(str.length());
            if (valueOf.intValue() > 10 && valueOf.intValue() < 20 && !a(str.trim()).booleanValue()) {
                return true;
            }
        } catch (Throwable th) {
        }
        return false;
    }

    private static String b(int i) {
        return (i < 0 || i >= h.length) ? String.valueOf(i) : h[i];
    }

    public static String b(Context context) {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress() && (nextElement instanceof Inet4Address)) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (Throwable th) {
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        com.tendcloud.tenddata.br.d = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0074, code lost:
        if (r0 != null) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x007c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x007d, code lost:
        r4 = r1;
        r1 = null;
        r0 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0070 A[ExcHandler: Throwable (th java.lang.Throwable), PHI: r0 
      PHI: (r0v11 java.net.Socket) = (r0v8 java.net.Socket), (r0v8 java.net.Socket), (r0v8 java.net.Socket), (r0v15 java.net.Socket), (r0v15 java.net.Socket) binds: [B:15:0x0044, B:28:0x0065, B:29:?, B:21:0x0059, B:22:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:15:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0082 A[SYNTHETIC, Splitter:B:42:0x0082] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean c(android.content.Context r5) {
        /*
            r1 = 0
            java.lang.String r0 = "android.permission.ACCESS_NETWORK_STATE"
            boolean r0 = com.tendcloud.tenddata.ca.b(r5, r0)     // Catch:{ Throwable -> 0x0086 }
            if (r0 == 0) goto L_0x002f
            java.lang.String r0 = "connectivity"
            java.lang.Object r0 = r5.getSystemService(r0)     // Catch:{ Throwable -> 0x0086 }
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ Throwable -> 0x0086 }
            android.net.NetworkInfo r2 = r0.getActiveNetworkInfo()     // Catch:{ Throwable -> 0x0086 }
            if (r2 == 0) goto L_0x001c
            boolean r0 = r2.isConnected()     // Catch:{ Throwable -> 0x0086 }
        L_0x001b:
            return r0
        L_0x001c:
            r2 = 0
            android.net.NetworkInfo r0 = r0.getNetworkInfo(r2)     // Catch:{ Throwable -> 0x0086 }
            if (r0 == 0) goto L_0x0063
            android.net.NetworkInfo$State r0 = r0.getState()     // Catch:{ Throwable -> 0x0086 }
            android.net.NetworkInfo$State r2 = android.net.NetworkInfo.State.UNKNOWN     // Catch:{ Throwable -> 0x0086 }
            boolean r0 = r0.equals(r2)     // Catch:{ Throwable -> 0x0086 }
            if (r0 == 0) goto L_0x0063
        L_0x002f:
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Throwable -> 0x0086 }
            long r2 = com.tendcloud.tenddata.br.f     // Catch:{ Throwable -> 0x0086 }
            long r0 = r0 - r2
            r2 = 300000(0x493e0, double:1.482197E-318)
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0060
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Throwable -> 0x0086 }
            com.tendcloud.tenddata.br.f = r0     // Catch:{ Throwable -> 0x0086 }
            r0 = 0
            boolean r1 = a()     // Catch:{ Throwable -> 0x0070, all -> 0x007c }
            if (r1 == 0) goto L_0x0065
            java.net.Socket r1 = new java.net.Socket     // Catch:{ Throwable -> 0x0070, all -> 0x007c }
            java.lang.String r2 = android.net.Proxy.getDefaultHost()     // Catch:{ Throwable -> 0x0070, all -> 0x007c }
            int r3 = android.net.Proxy.getDefaultPort()     // Catch:{ Throwable -> 0x0070, all -> 0x007c }
            r1.<init>(r2, r3)     // Catch:{ Throwable -> 0x0070, all -> 0x007c }
            r0 = r1
        L_0x0058:
            r1 = 1
            com.tendcloud.tenddata.br.d = r1     // Catch:{ Throwable -> 0x0070 }
            if (r0 == 0) goto L_0x0060
            r0.close()     // Catch:{ Throwable -> 0x0088 }
        L_0x0060:
            boolean r0 = com.tendcloud.tenddata.br.d
            goto L_0x001b
        L_0x0063:
            r0 = r1
            goto L_0x001b
        L_0x0065:
            java.net.Socket r1 = new java.net.Socket     // Catch:{ Throwable -> 0x0070, all -> 0x007c }
            java.lang.String r2 = "www.talkingdata.net"
            r3 = 80
            r1.<init>(r2, r3)     // Catch:{ Throwable -> 0x0070, all -> 0x007c }
            r0 = r1
            goto L_0x0058
        L_0x0070:
            r1 = move-exception
            r1 = 0
            com.tendcloud.tenddata.br.d = r1     // Catch:{ all -> 0x008c }
            if (r0 == 0) goto L_0x0060
            r0.close()     // Catch:{ Throwable -> 0x007a }
            goto L_0x0060
        L_0x007a:
            r0 = move-exception
            goto L_0x0060
        L_0x007c:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0080:
            if (r1 == 0) goto L_0x0085
            r1.close()     // Catch:{ Throwable -> 0x008a }
        L_0x0085:
            throw r0     // Catch:{ Throwable -> 0x0086 }
        L_0x0086:
            r0 = move-exception
            goto L_0x0060
        L_0x0088:
            r0 = move-exception
            goto L_0x0060
        L_0x008a:
            r1 = move-exception
            goto L_0x0085
        L_0x008c:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.br.c(android.content.Context):boolean");
    }

    public static boolean d(Context context) {
        try {
            if (ca.b(context, "android.permission.ACCESS_NETWORK_STATE")) {
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                return activeNetworkInfo != null && activeNetworkInfo.isAvailable();
            }
        } catch (Throwable th) {
        }
        return false;
    }

    public static boolean e(Context context) {
        try {
            return ((WifiManager) context.getSystemService("wifi")).isWifiEnabled();
        } catch (Throwable th) {
            return false;
        }
    }

    public static boolean f(Context context) {
        try {
            if (a == null) {
                a(context);
            }
            return a.getSimState() == 5;
        } catch (Throwable th) {
            return false;
        }
    }

    public static boolean g(Context context) {
        try {
            if (ca.b(context, "android.permission.ACCESS_NETWORK_STATE")) {
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                return activeNetworkInfo != null && 1 == activeNetworkInfo.getType() && activeNetworkInfo.isConnected();
            }
        } catch (Throwable th) {
        }
        return false;
    }

    public static boolean h(Context context) {
        try {
            if (a == null) {
                a(context);
            }
            return a.getDataState() == 2;
        } catch (Throwable th) {
            return false;
        }
    }

    public static String i(Context context) {
        return !c(context) ? "OFFLINE" : g(context) ? "WIFI" : j(context);
    }

    public static String j(Context context) {
        try {
            if (a == null) {
                a(context);
            }
            return a(a.getNetworkType());
        } catch (Throwable th) {
            return g[0];
        }
    }

    public static String k(Context context) {
        try {
            if (a == null) {
                a(context);
            }
            return a.getNetworkOperator();
        } catch (Throwable th) {
            return null;
        }
    }

    public static String l(Context context) {
        try {
            if (a == null) {
                a(context);
            }
            return a.getSimOperator();
        } catch (Throwable th) {
            return null;
        }
    }

    public static String m(Context context) {
        try {
            if (a == null) {
                a(context);
            }
            return a.getNetworkCountryIso();
        } catch (Throwable th) {
            return null;
        }
    }

    public static String n(Context context) {
        try {
            if (a == null) {
                a(context);
            }
            return a.getSimCountryIso();
        } catch (Throwable th) {
            return null;
        }
    }

    public static String o(Context context) {
        try {
            if ((ca.a(23) && context.checkSelfPermission("android.permission.READ_PHONE_STATE") != 0) || !ca.b(context, "android.permission.READ_PHONE_STATE") || Build.VERSION.SDK_INT < 18) {
                return null;
            }
            if (a == null) {
                a(context);
            }
            return a.getGroupIdLevel1();
        } catch (Throwable th) {
            return null;
        }
    }

    public static String p(Context context) {
        try {
            if (a == null) {
                a(context);
            }
            return a.getNetworkOperatorName();
        } catch (Throwable th) {
            return null;
        }
    }

    public static String q(Context context) {
        try {
            if (a == null) {
                a(context);
            }
            return a.getSimOperatorName();
        } catch (Throwable th) {
            return null;
        }
    }

    public static JSONArray r(Context context) {
        JSONArray jSONArray = new JSONArray();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("type", "wifi");
            jSONObject.put("available", e(context));
            jSONObject.put("connected", g(context));
            jSONObject.put("current", w(context));
            jSONObject.put("scannable", x(context));
            jSONObject.put("configured", u(context));
            jSONArray.put(jSONObject);
        } catch (Throwable th) {
        }
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("type", "cellular");
            jSONObject2.put("available", f(context));
            jSONObject2.put("connected", h(context));
            jSONObject2.put("current", s(context));
            jSONObject2.put("scannable", t(context));
            jSONArray.put(jSONObject2);
        } catch (Throwable th2) {
        }
        if (jSONArray.length() > 0) {
            return jSONArray;
        }
        return null;
    }

    public static JSONArray s(Context context) {
        CdmaCellLocation cdmaCellLocation;
        try {
            if (!ca.b) {
                return null;
            }
            if (ca.a(23) && context.checkSelfPermission("android.permission.ACCESS_COARSE_LOCATION") != 0) {
                return null;
            }
            if (ca.b(context, "android.permission.ACCESS_COARSE_LOCATION") || ca.b(context, "android.permission.ACCESS_FINE_LOCATION")) {
                if (a == null) {
                    a(context);
                }
                JSONObject jSONObject = new JSONObject();
                if (ca.d || ca.e) {
                    CellLocation cellLocation = a.getCellLocation();
                    if (cellLocation instanceof GsmCellLocation) {
                        GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                        if (gsmCellLocation != null) {
                            jSONObject.put("systemId", gsmCellLocation.getLac());
                            jSONObject.put("networkId", gsmCellLocation.getCid());
                            if (ca.a(9)) {
                                jSONObject.put("basestationId", gsmCellLocation.getPsc());
                            }
                        }
                    } else if ((cellLocation instanceof CdmaCellLocation) && (cdmaCellLocation = (CdmaCellLocation) cellLocation) != null) {
                        jSONObject.put("systemId", cdmaCellLocation.getSystemId());
                        jSONObject.put("networkId", cdmaCellLocation.getNetworkId());
                        jSONObject.put("basestationId", cdmaCellLocation.getBaseStationId());
                        jSONObject.put("location", a(cdmaCellLocation.getBaseStationLatitude(), cdmaCellLocation.getBaseStationLongitude()));
                    }
                }
                jSONObject.put("type", j(context));
                jSONObject.put("mcc", k(context));
                jSONObject.put("operator", p(context));
                jSONObject.put("country", m(context));
                JSONArray jSONArray = new JSONArray();
                jSONArray.put(jSONObject);
                return jSONArray;
            }
            return null;
        } catch (Throwable th) {
        }
    }

    public static JSONArray t(Context context) {
        List<NeighboringCellInfo> neighboringCellInfo;
        Object obj;
        CellSignalStrengthGsm cellSignalStrengthGsm;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        if (!ca.b) {
            return null;
        }
        if (ca.a(23) && context.checkSelfPermission("android.permission.ACCESS_COARSE_LOCATION") != 0) {
            return null;
        }
        if (!ca.b(context, "android.permission.ACCESS_COARSE_LOCATION") && !ca.b(context, "android.permission.ACCESS_FINE_LOCATION")) {
            return null;
        }
        try {
            if (a == null) {
                a(context);
            }
            JSONArray jSONArray = new JSONArray();
            if (ca.a(17)) {
                List<CellInfo> allCellInfo = a.getAllCellInfo();
                if (allCellInfo != null) {
                    for (CellInfo next : allCellInfo) {
                        try {
                            JSONObject jSONObject = new JSONObject();
                            jSONObject.put("registered", next.isRegistered());
                            jSONObject.put("ts", next.getTimeStamp());
                            if (next instanceof CellInfoGsm) {
                                CellInfoGsm cellInfoGsm = (CellInfoGsm) next;
                                CellIdentityGsm cellIdentity = cellInfoGsm.getCellIdentity();
                                int lac = cellIdentity.getLac();
                                int cid = cellIdentity.getCid();
                                i2 = cellIdentity.getMcc();
                                i = cellIdentity.getMnc();
                                i5 = lac;
                                i4 = cid;
                                i3 = -1;
                                CellSignalStrengthGsm cellSignalStrength = cellInfoGsm.getCellSignalStrength();
                                obj = "GSM";
                                cellSignalStrengthGsm = cellSignalStrength;
                            } else if (next instanceof CellInfoCdma) {
                                CellInfoCdma cellInfoCdma = (CellInfoCdma) next;
                                CellIdentityCdma cellIdentity2 = cellInfoCdma.getCellIdentity();
                                int systemId = cellIdentity2.getSystemId();
                                int networkId = cellIdentity2.getNetworkId();
                                int basestationId = cellIdentity2.getBasestationId();
                                CellSignalStrengthCdma cellSignalStrength2 = cellInfoCdma.getCellSignalStrength();
                                jSONObject.put("cdmaDbm", cellSignalStrength2.getCdmaDbm());
                                jSONObject.put("cdmaDbm", cellSignalStrength2.getCdmaDbm());
                                jSONObject.put("cdmaEcio", cellSignalStrength2.getCdmaEcio());
                                jSONObject.put("evdoDbm", cellSignalStrength2.getEvdoDbm());
                                jSONObject.put("evdoEcio", cellSignalStrength2.getEvdoEcio());
                                jSONObject.put("evdoSnr", cellSignalStrength2.getEvdoSnr());
                                jSONObject.put("location", a(cellIdentity2.getLatitude(), cellIdentity2.getLongitude()));
                                i4 = networkId;
                                i5 = systemId;
                                i2 = -1;
                                i3 = basestationId;
                                i = -1;
                                cellSignalStrengthGsm = cellSignalStrength2;
                                obj = "CDMA";
                            } else if (next instanceof CellInfoWcdma) {
                                CellInfoWcdma cellInfoWcdma = (CellInfoWcdma) next;
                                CellIdentityWcdma cellIdentity3 = cellInfoWcdma.getCellIdentity();
                                i5 = cellIdentity3.getLac();
                                i4 = cellIdentity3.getCid();
                                i3 = cellIdentity3.getPsc();
                                i2 = cellIdentity3.getMcc();
                                i = cellIdentity3.getMnc();
                                cellSignalStrengthGsm = cellInfoWcdma.getCellSignalStrength();
                                obj = "WCDMA";
                            } else if (next instanceof CellInfoLte) {
                                CellInfoLte cellInfoLte = (CellInfoLte) next;
                                CellIdentityLte cellIdentity4 = cellInfoLte.getCellIdentity();
                                i5 = cellIdentity4.getTac();
                                i4 = cellIdentity4.getPci();
                                i3 = cellIdentity4.getCi();
                                i2 = cellIdentity4.getMcc();
                                i = cellIdentity4.getMnc();
                                cellSignalStrengthGsm = cellInfoLte.getCellSignalStrength();
                                obj = "LTE";
                            } else {
                                obj = null;
                                cellSignalStrengthGsm = null;
                                i = -1;
                                i2 = -1;
                                i3 = -1;
                                i4 = -1;
                                i5 = -1;
                            }
                            if (i5 != -1) {
                                jSONObject.put("systemId", i5);
                            }
                            if (i4 != -1) {
                                jSONObject.put("networkId", i4);
                            }
                            if (i3 != -1) {
                                jSONObject.put("basestationId", i3);
                            }
                            if (i2 != -1) {
                                jSONObject.put("mcc", i2);
                            }
                            if (i != -1) {
                                jSONObject.put("mnc", i);
                            }
                            if (cellSignalStrengthGsm != null) {
                                jSONObject.put("asuLevel", cellSignalStrengthGsm.getAsuLevel());
                                jSONObject.put("dbm", cellSignalStrengthGsm.getDbm());
                            }
                            jSONObject.put("type", obj);
                            jSONArray.put(jSONObject);
                        } catch (Throwable th) {
                        }
                    }
                }
            } else if (ca.a(5) && ((ca.d || ca.e) && (neighboringCellInfo = a.getNeighboringCellInfo()) != null)) {
                for (NeighboringCellInfo neighboringCellInfo2 : neighboringCellInfo) {
                    try {
                        JSONObject jSONObject2 = new JSONObject();
                        jSONObject2.put("systemId", neighboringCellInfo2.getLac());
                        jSONObject2.put("netId", neighboringCellInfo2.getCid());
                        jSONObject2.put("basestationId", neighboringCellInfo2.getPsc());
                        jSONObject2.put("asuLevel", neighboringCellInfo2.getRssi());
                        jSONObject2.put("type", a(neighboringCellInfo2.getNetworkType()));
                        jSONArray.put(jSONObject2);
                    } catch (Throwable th2) {
                    }
                }
            }
            return jSONArray.length() > 0 ? jSONArray : null;
        } catch (Throwable th3) {
            return null;
        }
    }

    public static JSONArray u(Context context) {
        List<WifiConfiguration> configuredNetworks;
        try {
            if (!ca.b || !ca.b(context, "android.permission.ACCESS_WIFI_STATE") || (configuredNetworks = ((WifiManager) context.getSystemService("wifi")).getConfiguredNetworks()) == null) {
                return null;
            }
            JSONArray jSONArray = new JSONArray();
            for (WifiConfiguration next : configuredNetworks) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("networkId", next.networkId);
                    jSONObject.put(EgamePay.PAY_PARAMS_KEY_PRIORITY, next.priority);
                    jSONObject.put("name", next.SSID);
                    jSONObject.put(dc.V, next.BSSID);
                    jSONObject.put("allowedKeyManagement", a(next.allowedKeyManagement));
                    jSONObject.put("allowedAuthAlgorithms", a(next.allowedAuthAlgorithms));
                    jSONObject.put("allowedGroupCiphers", a(next.allowedGroupCiphers));
                    jSONObject.put("allowedPairwiseCiphers", a(next.allowedPairwiseCiphers));
                    jSONArray.put(jSONObject);
                } catch (Throwable th) {
                }
            }
            return jSONArray.length() > 0 ? jSONArray : null;
        } catch (Throwable th2) {
            return null;
        }
    }

    public static String v(Context context) {
        String str;
        WifiInfo connectionInfo;
        try {
            if (!ca.b) {
                return null;
            }
            if (ca.b(context, "android.permission.ACCESS_WIFI_STATE")) {
                WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
                if (wifiManager.isWifiEnabled() && g(context) && (connectionInfo = wifiManager.getConnectionInfo()) != null && connectionInfo.getBSSID() != null) {
                    try {
                        str = connectionInfo.getSSID();
                    } catch (Throwable th) {
                        str = null;
                    }
                    return str;
                }
            }
            str = null;
            return str;
        } catch (Throwable th2) {
            str = null;
        }
    }

    public static JSONArray w(Context context) {
        WifiInfo connectionInfo;
        try {
            if (!ca.b) {
                return null;
            }
            if (ca.b(context, "android.permission.ACCESS_WIFI_STATE")) {
                WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
                if (!(!wifiManager.isWifiEnabled() || (connectionInfo = wifiManager.getConnectionInfo()) == null || connectionInfo.getBSSID() == null)) {
                    String bssid = connectionInfo.getBSSID();
                    JSONArray jSONArray = new JSONArray();
                    JSONObject jSONObject = new JSONObject();
                    try {
                        jSONObject.put("name", connectionInfo.getSSID());
                        jSONObject.put(dc.V, bssid);
                        jSONObject.put("level", connectionInfo.getRssi());
                        jSONObject.put("hidden", connectionInfo.getHiddenSSID());
                        jSONObject.put("ip", connectionInfo.getIpAddress());
                        jSONObject.put("speed", connectionInfo.getLinkSpeed());
                        jSONObject.put("networkId", connectionInfo.getNetworkId());
                        jSONObject.put("mac", connectionInfo.getMacAddress());
                        DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
                        if (dhcpInfo != null) {
                            JSONObject jSONObject2 = new JSONObject();
                            jSONObject2.put("dns1", dhcpInfo.dns1);
                            jSONObject2.put("dns2", dhcpInfo.dns2);
                            jSONObject2.put("gw", dhcpInfo.gateway);
                            jSONObject2.put("ip", dhcpInfo.ipAddress);
                            jSONObject2.put("mask", dhcpInfo.netmask);
                            jSONObject2.put("server", dhcpInfo.serverAddress);
                            jSONObject2.put("leaseDuration", dhcpInfo.leaseDuration);
                            jSONObject.put("dhcp", jSONObject2);
                        }
                    } catch (Throwable th) {
                    }
                    jSONArray.put(jSONObject);
                    return jSONArray;
                }
            }
            return null;
        } catch (Throwable th2) {
        }
    }

    public static JSONArray x(Context context) {
        if (!ca.b || (!ca.d && !ca.e)) {
            return null;
        }
        try {
            if (ca.b(context, "android.permission.ACCESS_WIFI_STATE")) {
                WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
                if (wifiManager.isWifiEnabled() || (Build.VERSION.SDK_INT >= 18 && wifiManager.isScanAlwaysAvailable())) {
                    if (ca.b(context, "android.permission.CHANGE_WIFI_STATE")) {
                        try {
                            Object obj = new Object();
                            context.registerReceiver(new bs(obj, context), new IntentFilter("android.net.wifi.SCAN_RESULTS"));
                            wifiManager.startScan();
                            synchronized (obj) {
                                obj.wait(2000);
                            }
                        } catch (Throwable th) {
                        }
                    }
                    List<ScanResult> scanResults = wifiManager.getScanResults();
                    if (scanResults != null) {
                        JSONArray jSONArray = new JSONArray();
                        for (ScanResult next : scanResults) {
                            if (next.level >= -85) {
                                JSONObject jSONObject = new JSONObject();
                                try {
                                    jSONObject.put(dc.V, next.BSSID);
                                    jSONObject.put("name", next.SSID);
                                    jSONObject.put("level", next.level);
                                    jSONObject.put("freq", next.frequency);
                                    if (ca.a(17)) {
                                        jSONObject.put("ts", next.timestamp);
                                        jSONObject.put("scanTs", (System.currentTimeMillis() - SystemClock.elapsedRealtime()) + (next.timestamp / 1000));
                                    }
                                    jSONArray.put(jSONObject);
                                } catch (Throwable th2) {
                                }
                            }
                        }
                        return jSONArray;
                    }
                }
            }
        } catch (Throwable th3) {
        }
        return null;
    }

    public static JSONArray y(Context context) {
        JSONArray jSONArray = new JSONArray();
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            ArrayList arrayList = new ArrayList();
            if (ca.a(22)) {
                SubscriptionManager subscriptionManager = (SubscriptionManager) context.getSystemService("telephony_subscription_service");
                try {
                    JSONObject a2 = a(telephonyManager, subscriptionManager, 0);
                    a2.put("imei", telephonyManager.getDeviceId());
                    jSONArray.put(a2);
                } catch (Throwable th) {
                }
                try {
                    JSONObject a3 = a(telephonyManager, subscriptionManager, 1);
                    a3.put("imei", (!ca.a(23) || telephonyManager.getPhoneCount() != 2) ? null : telephonyManager.getDeviceId(1));
                    if (a3.length() > 0) {
                        jSONArray.put(a3);
                    }
                } catch (Throwable th2) {
                }
            } else {
                String deviceId = telephonyManager.getDeviceId();
                if (b(deviceId.trim()).booleanValue()) {
                    arrayList.add(deviceId.trim());
                    JSONObject a4 = a(telephonyManager, deviceId);
                    if (a4 != null) {
                        jSONArray.put(a4);
                    }
                }
                try {
                    TelephonyManager telephonyManager2 = (TelephonyManager) context.getSystemService("phone1");
                    String deviceId2 = telephonyManager2.getDeviceId();
                    if (deviceId2 != null && b(deviceId2).booleanValue() && !arrayList.contains(deviceId2)) {
                        arrayList.add(deviceId2);
                        JSONObject a5 = a(telephonyManager2, deviceId2);
                        if (a5 != null) {
                            jSONArray.put(a5);
                        }
                    }
                } catch (Throwable th3) {
                }
                try {
                    TelephonyManager telephonyManager3 = (TelephonyManager) context.getSystemService("phone2");
                    String deviceId3 = telephonyManager3.getDeviceId();
                    if (deviceId3 != null && b(deviceId3).booleanValue() && !arrayList.contains(deviceId3)) {
                        arrayList.add(deviceId3);
                        JSONObject a6 = a(telephonyManager3, deviceId3);
                        if (a6 != null) {
                            jSONArray.put(a6);
                        }
                    }
                } catch (Throwable th4) {
                }
                JSONArray D = D(context);
                JSONArray C = C(context);
                if (C == null) {
                    C = D;
                }
                JSONArray B = B(context);
                if (B == null) {
                    B = C;
                }
                JSONArray A = A(context);
                if (A != null) {
                    B = A;
                }
                if (B != null && B.length() > 0) {
                    for (int i = 0; i < B.length(); i++) {
                        JSONObject jSONObject = B.getJSONObject(i);
                        String string = jSONObject.getString("imei");
                        if (!arrayList.contains(string)) {
                            arrayList.add(string);
                            jSONArray.put(jSONObject);
                        }
                    }
                }
            }
        } catch (Throwable th5) {
        }
        return jSONArray;
    }

    public static JSONObject z(Context context) {
        try {
            JSONObject jSONObject = new JSONObject();
            try {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                jSONObject.put("dataState", telephonyManager.getDataState());
                jSONObject.put("networkType", a(telephonyManager.getNetworkType()));
                jSONObject.put("networkOperator", telephonyManager.getNetworkOperator());
                jSONObject.put("phoneType", b(telephonyManager.getPhoneType()));
                return jSONObject;
            } catch (Throwable th) {
                return jSONObject;
            }
        } catch (Throwable th2) {
            return null;
        }
    }
}
