package com.tendcloud.tenddata;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class fc extends Handler {
    fc(Looper looper) {
        super(looper);
    }

    public void handleMessage(Message message) {
        try {
            if (message.obj != null && (message.obj instanceof ex)) {
                ex unused = fb.e = (ex) message.obj;
                int i = fb.e.i;
                if (fb.e.f == null) {
                    fb.a().c();
                    return;
                }
                gd.c = false;
                fb.a().a(fb.e.a, fb.e.b, fb.e.c, fb.e.e, fb.e.f, fb.e.g, i, fb.e.h);
            }
        } catch (Throwable th) {
        }
    }
}
