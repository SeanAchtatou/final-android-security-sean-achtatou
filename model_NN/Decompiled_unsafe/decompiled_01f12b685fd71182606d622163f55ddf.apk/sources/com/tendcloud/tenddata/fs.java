package com.tendcloud.tenddata;

import android.os.Environment;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONObject;

final class fs {
    static final String a = "Android";
    private static volatile fs b = null;
    private final String c = "sdk";
    private final String d = "env";
    private final String e = "networks";
    private final String f = "locations";
    private final String g = "appInstall";
    private final String h = "appUninstall";
    private final String i = "apps";
    private final String j = "appQuit";
    private final String k = "trafficStats";
    private final String l = "/tdCacheLogs/";
    private JSONArray m = new JSONArray();

    fs() {
    }

    static fs a() {
        if (b == null) {
            synchronized (fs.class) {
                if (b == null) {
                    b = new fs();
                }
            }
        }
        return b;
    }

    private JSONObject a(String str, String str2, String str3, String str4) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("asad", str);
            jSONObject.put("ras", str2);
            if (str2 == null || str2.isEmpty()) {
                return null;
            }
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("sessionId", str3);
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("domain", "env");
            jSONObject3.put("name", str4);
            jSONObject3.put("data", jSONObject);
            return a(jSONObject3, (JSONArray) null, jSONObject2, (String) null);
        } catch (Throwable th) {
            return null;
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.json.JSONObject a(org.json.JSONObject r5, org.json.JSONArray r6, org.json.JSONObject r7, java.lang.String r8) {
        /*
            r4 = this;
            r1 = 0
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0050 }
            r0.<init>()     // Catch:{ Throwable -> 0x0050 }
            java.lang.String r1 = "device"
            org.json.JSONObject r2 = g()     // Catch:{ Throwable -> 0x004e }
            r0.put(r1, r2)     // Catch:{ Throwable -> 0x004e }
            java.lang.String r1 = "app"
            org.json.JSONObject r2 = h()     // Catch:{ Throwable -> 0x004e }
            r0.put(r1, r2)     // Catch:{ Throwable -> 0x004e }
            java.lang.String r1 = "ts"
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x004e }
            r0.put(r1, r2)     // Catch:{ Throwable -> 0x004e }
            java.lang.String r1 = "sdk"
            org.json.JSONObject r2 = i()     // Catch:{ Throwable -> 0x004e }
            r0.put(r1, r2)     // Catch:{ Throwable -> 0x004e }
            if (r8 == 0) goto L_0x0042
            boolean r1 = r8.isEmpty()     // Catch:{ Throwable -> 0x004e }
            if (r1 != 0) goto L_0x0042
            if (r6 == 0) goto L_0x0042
            r0.put(r8, r6)     // Catch:{ Throwable -> 0x004e }
            java.lang.String r1 = "appContext"
            r0.put(r1, r7)     // Catch:{ Throwable -> 0x004e }
        L_0x003c:
            java.lang.String r1 = "action"
            r0.put(r1, r5)     // Catch:{ Throwable -> 0x004e }
        L_0x0041:
            return r0
        L_0x0042:
            if (r8 != 0) goto L_0x003c
            if (r6 != 0) goto L_0x003c
            if (r7 == 0) goto L_0x003c
            java.lang.String r1 = "appContext"
            r0.put(r1, r7)     // Catch:{ Throwable -> 0x004e }
            goto L_0x003c
        L_0x004e:
            r1 = move-exception
            goto L_0x0041
        L_0x0050:
            r0 = move-exception
            r0 = r1
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.fs.a(org.json.JSONObject, org.json.JSONArray, org.json.JSONObject, java.lang.String):org.json.JSONObject");
    }

    private JSONObject b(String str, String str2) {
        JSONObject jSONObject = new JSONObject();
        if (str == null || str.isEmpty()) {
            return null;
        }
        try {
            jSONObject.put("pkgName", str);
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("domain", "sdk");
            jSONObject2.put("name", str2);
            jSONObject2.put("data", jSONObject);
            return a(jSONObject2, (JSONArray) null, (JSONObject) null, (String) null);
        } catch (Throwable th) {
            return null;
        }
    }

    private JSONObject b(String str, String str2, String str3, String str4) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("asad", str);
            jSONObject.put("ruas", str2);
            if (str2 == null || str2.isEmpty()) {
                return null;
            }
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("sessionId", str3);
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("domain", "env");
            jSONObject3.put("name", str4);
            jSONObject3.put("data", jSONObject);
            return a(jSONObject3, (JSONArray) null, jSONObject2, (String) null);
        } catch (Throwable th) {
            return null;
        }
    }

    private JSONObject b(JSONObject jSONObject, String str) {
        if (jSONObject == null) {
            return null;
        }
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("sessionId", str);
            JSONArray jSONArray = new JSONArray();
            jSONArray.put(jSONObject);
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("domain", "env");
            jSONObject3.put("name", "locationUpdate");
            return a(jSONObject3, jSONArray, jSONObject2, "locations");
        } catch (Throwable th) {
            return null;
        }
    }

    private JSONObject c(String str, String str2, String str3) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("aas", str);
            if (str == null || str.isEmpty()) {
                return null;
            }
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("sessionId", str2);
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("domain", "env");
            jSONObject3.put("name", str3);
            jSONObject3.put("data", jSONObject);
            return a(jSONObject3, (JSONArray) null, jSONObject2, (String) null);
        } catch (Throwable th) {
            return null;
        }
    }

    private JSONObject c(JSONArray jSONArray, JSONArray jSONArray2, String str) {
        if (jSONArray == null && jSONArray2 == null) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("sessionid", str);
            JSONArray jSONArray3 = new JSONArray();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("type", "wifi");
            jSONObject2.put("scannable", jSONArray);
            jSONObject2.put("current", jSONArray2);
            jSONArray3.put(jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("domain", "env");
            jSONObject3.put("name", "wifiUpdate");
            return a((JSONObject) null, jSONArray3, jSONObject, "networks");
        } catch (Throwable th) {
            return null;
        }
    }

    private JSONObject c(JSONObject jSONObject, String str) {
        if (jSONObject == null) {
            return null;
        }
        try {
            if (jSONObject.length() <= 0) {
                return null;
            }
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("domain", "env");
            jSONObject2.put("name", str);
            jSONObject2.put("data", jSONObject);
            return a(jSONObject2, (JSONArray) null, (JSONObject) null, (String) null);
        } catch (Throwable th) {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d A[SYNTHETIC, Splitter:B:14:0x002d] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0036 A[SYNTHETIC, Splitter:B:19:0x0036] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] c(java.lang.String r7) {
        /*
            r6 = this;
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream
            r2.<init>()
            r1 = 0
            java.util.zip.Deflater r3 = new java.util.zip.Deflater
            r0 = 9
            r4 = 1
            r3.<init>(r0, r4)
            java.util.zip.DeflaterOutputStream r0 = new java.util.zip.DeflaterOutputStream     // Catch:{ Throwable -> 0x0029, all -> 0x0033 }
            r0.<init>(r2, r3)     // Catch:{ Throwable -> 0x0029, all -> 0x0033 }
            java.lang.String r1 = "UTF-8"
            byte[] r1 = r7.getBytes(r1)     // Catch:{ Throwable -> 0x0043, all -> 0x003e }
            r0.write(r1)     // Catch:{ Throwable -> 0x0043, all -> 0x003e }
            if (r0 == 0) goto L_0x0021
            r0.close()     // Catch:{ Throwable -> 0x003a }
        L_0x0021:
            r3.end()
            byte[] r0 = r2.toByteArray()
            return r0
        L_0x0029:
            r0 = move-exception
            r0 = r1
        L_0x002b:
            if (r0 == 0) goto L_0x0021
            r0.close()     // Catch:{ Throwable -> 0x0031 }
            goto L_0x0021
        L_0x0031:
            r0 = move-exception
            goto L_0x0021
        L_0x0033:
            r0 = move-exception
        L_0x0034:
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ Throwable -> 0x003c }
        L_0x0039:
            throw r0
        L_0x003a:
            r0 = move-exception
            goto L_0x0021
        L_0x003c:
            r1 = move-exception
            goto L_0x0039
        L_0x003e:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0034
        L_0x0043:
            r1 = move-exception
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.fs.c(java.lang.String):byte[]");
    }

    private JSONObject d(JSONArray jSONArray, JSONArray jSONArray2, String str) {
        if (jSONArray == null && jSONArray2 == null) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("sessionId", str);
            JSONArray jSONArray3 = new JSONArray();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("type", "cellular");
            jSONObject2.put("scannable", jSONArray2);
            jSONObject2.put("current", jSONArray);
            jSONArray3.put(jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("domain", "env");
            jSONObject3.put("name", "cellUpdate");
            return a(jSONObject3, jSONArray3, jSONObject, "networks");
        } catch (Throwable th) {
            return null;
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    @android.annotation.TargetApi(23)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void e() {
        /*
            r3 = this;
            org.json.JSONArray r0 = r3.m
            int r0 = r0.length()
            r1 = 10
            if (r0 <= r1) goto L_0x001c
            r0 = 23
            boolean r0 = com.tendcloud.tenddata.ca.a(r0)     // Catch:{ Throwable -> 0x0078 }
            if (r0 == 0) goto L_0x001d
            android.content.Context r0 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x0078 }
            java.lang.String r1 = "android.permission.READ_EXTERNAL_STORAGE"
            int r0 = r0.checkSelfPermission(r1)     // Catch:{ Throwable -> 0x0078 }
            if (r0 == 0) goto L_0x001d
        L_0x001c:
            return
        L_0x001d:
            java.lang.String r0 = android.os.Environment.getExternalStorageState()     // Catch:{ Throwable -> 0x0078 }
            java.lang.String r1 = "mounted"
            boolean r0 = r1.equals(r0)     // Catch:{ Throwable -> 0x0078 }
            if (r0 == 0) goto L_0x006f
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x0078 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0078 }
            r1.<init>()     // Catch:{ Throwable -> 0x0078 }
            java.io.File r2 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Throwable -> 0x0078 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x0078 }
            java.lang.String r2 = "/tdCacheLogs/"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x0078 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x0078 }
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0078 }
            boolean r1 = r0.exists()     // Catch:{ Throwable -> 0x0078 }
            if (r1 != 0) goto L_0x004e
            r0.mkdir()     // Catch:{ Throwable -> 0x0078 }
        L_0x004e:
            java.io.File r1 = new java.io.File     // Catch:{ Throwable -> 0x0078 }
            java.lang.String r2 = r3.f()     // Catch:{ Throwable -> 0x0078 }
            r1.<init>(r0, r2)     // Catch:{ Throwable -> 0x0078 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Throwable -> 0x0078 }
            r2.<init>(r1)     // Catch:{ Throwable -> 0x0078 }
            org.json.JSONArray r0 = r3.m     // Catch:{ Throwable -> 0x0073, all -> 0x007a }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x0073, all -> 0x007a }
            byte[] r0 = r3.c(r0)     // Catch:{ Throwable -> 0x0073, all -> 0x007a }
            r2.write(r0)     // Catch:{ Throwable -> 0x0073, all -> 0x007a }
            r2.flush()     // Catch:{ Throwable -> 0x0073, all -> 0x007a }
            r2.close()     // Catch:{ Throwable -> 0x0078 }
        L_0x006f:
            r3.d()
            goto L_0x001c
        L_0x0073:
            r0 = move-exception
            r2.close()     // Catch:{ Throwable -> 0x0078 }
            goto L_0x006f
        L_0x0078:
            r0 = move-exception
            goto L_0x006f
        L_0x007a:
            r0 = move-exception
            r2.close()     // Catch:{ Throwable -> 0x0078 }
            throw r0     // Catch:{ Throwable -> 0x0078 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.fs.e():void");
    }

    private String f() {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append(new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date()));
            sb.append("-");
            sb.append(Integer.toHexString(System.identityHashCode(ab.mContext.getApplicationInfo())));
            sb.append("-");
            sb.append(String.valueOf(System.currentTimeMillis()));
            return sb.toString();
        } catch (Throwable th) {
            return sb.toString();
        }
    }

    private static JSONObject g() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("tid", bm.a(ab.mContext));
            try {
                JSONArray y = br.y(ab.mContext);
                JSONArray jSONArray = new JSONArray();
                if (y != null && y.length() > 0) {
                    jSONArray.put(y.getJSONObject(0).get("imei"));
                    if (y.length() == 2) {
                        try {
                            jSONArray.put(y.getJSONObject(1).get("imei"));
                        } catch (Throwable th) {
                        }
                    }
                }
                jSONObject2.put("imeis", jSONArray);
            } catch (Throwable th2) {
            }
            JSONArray jSONArray2 = new JSONArray();
            jSONArray2.put(bm.f(ab.mContext));
            jSONObject2.put("wifiMacs", jSONArray2);
            jSONObject2.put("androidId", bm.b(ab.mContext));
            jSONObject2.put("adId", bm.g(ab.mContext));
            jSONObject2.put("serialNo", bm.a() == null ? "" : bm.a());
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("manufacture", bn.b());
            jSONObject3.put("brand", bn.c());
            jSONObject3.put("model", bn.d());
            jSONObject.put("hardwareConfig", jSONObject3);
            jSONObject.put("deviceId", jSONObject2);
        } catch (Throwable th3) {
        }
        return jSONObject;
    }

    private static JSONObject h() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("appKey", ab.getAppId(ab.mContext));
            jSONObject.put("name", bj.a().h(ab.mContext));
            jSONObject.put("globalId", bj.a().a(ab.mContext));
            jSONObject.put("versionName", bj.a().c(ab.mContext));
            jSONObject.put("versionCode", bj.a().b(ab.mContext));
            jSONObject.put("installTime", bj.a().d(ab.mContext));
            jSONObject.put("updateTime", bj.a().e(ab.mContext));
            jSONObject.put("channel", ab.getPartnerId(ab.mContext));
        } catch (Throwable th) {
        }
        return jSONObject;
    }

    private static JSONObject i() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("platform", a);
            jSONObject.put("version", "Android+TD+V2.2.46 gp");
        } catch (Throwable th) {
        }
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        e();
        this.m.put(b(str, "appInstall"));
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2) {
        e();
        this.m.put(c(str, str2, "apps"));
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2, String str3) {
        e();
        this.m.put(a(str, str2, str3, "apps"));
    }

    /* access modifiers changed from: package-private */
    public void a(JSONArray jSONArray, JSONArray jSONArray2, String str) {
        e();
        this.m.put(c(jSONArray, jSONArray2, str));
    }

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject) {
        e();
        this.m.put(c(jSONObject, "trafficStats"));
    }

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject, String str) {
        e();
        this.m.put(b(jSONObject, str));
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        e();
        this.m.put(b(str, "appUninstall"));
    }

    /* access modifiers changed from: package-private */
    public void b(String str, String str2, String str3) {
        e();
        this.m.put(b(str, str2, str3, "apps"));
    }

    /* access modifiers changed from: package-private */
    public void b(JSONArray jSONArray, JSONArray jSONArray2, String str) {
        e();
        this.m.put(d(jSONArray, jSONArray2, str));
    }

    /* access modifiers changed from: package-private */
    public byte[] b() {
        try {
            if (this.m.length() > 0) {
                return c(this.m.toString());
            }
        } catch (Throwable th) {
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public File[] c() {
        return new File(Environment.getExternalStorageDirectory() + "/tdCacheLogs/").listFiles();
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.m = new JSONArray();
    }
}
