package com.tendcloud.tenddata;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class zx extends Service {
    private final IBinder a = new a();

    public class a extends Binder {
        public a() {
        }

        /* access modifiers changed from: package-private */
        public zx a() {
            return zx.this;
        }
    }

    public IBinder onBind(Intent intent) {
        return this.a;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        return 1;
    }
}
