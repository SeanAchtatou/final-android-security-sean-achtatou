package com.tendcloud.tenddata;

import android.content.Context;
import android.os.Message;
import com.tendcloud.tenddata.gd;
import java.util.Map;

final class gh implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ String b;
    final /* synthetic */ Map c;
    final /* synthetic */ Context d;

    gh(String str, String str2, Map map, Context context) {
        this.a = str;
        this.b = str2;
        this.c = map;
        this.d = context;
    }

    public void run() {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("onEvent being called! eventId: ");
            sb.append(this.a);
            sb.append(", eventLabel: ");
            sb.append(this.b);
            sb.append(", eventMap: ");
            sb.append(this.c == null ? "null" : "mapSize: " + String.valueOf(this.c.size()));
            if (!this.a.equals("__tx.env")) {
                ev.a(sb.toString());
            }
            gd.a aVar = new gd.a();
            aVar.a.put("context", this.d);
            aVar.a.put("apiType", 4);
            aVar.a.put("eventId", ca.a(this.a));
            aVar.a.put("eventLabel", this.b == null ? null : ca.a(this.b));
            aVar.a.put("map", this.c);
            aVar.a.put("occurTime", String.valueOf(System.currentTimeMillis()));
            Message.obtain(er.a(), 102, aVar).sendToTarget();
        } catch (Throwable th) {
            if (TCAgent.LOG_ON) {
                th.printStackTrace();
            }
        }
    }
}
