package com.tendcloud.tenddata;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.provider.BaseColumns;
import com.tendcloud.tenddata.ff;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

final class ez extends el {
    static final String a = "TDtcagent.db";
    private static volatile ez b = null;
    private static final int c = 6;
    private static final String d = "10";
    private static SQLiteDatabase e = null;
    private static int f = 0;
    private static final int j = 8192000;
    private final int g = 1;
    private final int h = 2;
    private final int i = 3;

    static final class a implements BaseColumns {
        static final String a = "name";
        static final String b = "start_time";
        static final String c = "duration";
        static final String d = "session_id";
        static final String e = "refer";
        static final String f = "realtime";
        static final String g = "activity";
        static final String[] h = {"_id", a, b, c, d, e, f};

        a() {
        }

        static final void a(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("CREATE TABLE activity (_id INTEGER PRIMARY KEY autoincrement,name TEXT,start_time LONG,duration INTEGER,session_id TEXT,refer TEXT,realtime LONG)");
        }

        static final void b(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS activity");
        }
    }

    static final class b implements BaseColumns {
        static final String a = "event_id";
        static final String b = "event_label";
        static final String c = "session_id";
        static final String d = "occurtime";
        static final String e = "paramap";
        static final String f = "app_event";
        static final String[] g = {"_id", a, b, c, d, e};

        b() {
        }

        static final void a(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("CREATE TABLE app_event (_id INTEGER PRIMARY KEY autoincrement,event_id TEXT,event_label TEXT,session_id TEXT,occurtime LONG,paramap BLOB)");
        }

        static final void b(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS app_event");
        }
    }

    static final class c implements BaseColumns {
        static final String a = "error_time";
        static final String b = "message";
        static final String c = "repeat";
        static final String d = "shorthashcode";
        static final String e = "error_report";
        static final String[] f = {"_id", a, b, c, d};

        c() {
        }

        static final void a(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("CREATE TABLE error_report (_id INTEGER PRIMARY KEY autoincrement,error_time LONG,message BLOB,repeat INTERGER,shorthashcode TEXT)");
        }

        static final void b(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS error_report");
        }
    }

    static final class d implements BaseColumns {
        static final String a = "session_id";
        static final String b = "start_time";
        static final String c = "duration";
        static final String d = "is_launch";
        static final String e = "interval";
        static final String f = "is_connected";
        static final String g = "session";
        static final String[] h = {"_id", a, b, c, d, e, f};

        d() {
        }

        static final void a(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("CREATE TABLE session (_id INTEGER PRIMARY KEY autoincrement,session_id TEXT,start_time LONG,duration INTEGER,is_launch INTEGER,interval LONG, is_connected INTEGER)");
        }

        static final void b(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS session");
        }
    }

    private ez() {
    }

    /* JADX INFO: finally extract failed */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private synchronized long a(String str, ContentValues contentValues, String str2, String[] strArr, int i2) {
        long j2;
        j2 = 0;
        if (!ca.b(str)) {
            e.beginTransaction();
            switch (i2) {
                case 1:
                    j2 = e.insert(str, null, contentValues);
                    try {
                        e.setTransactionSuccessful();
                        e.endTransaction();
                        break;
                    } catch (Throwable th) {
                        e.endTransaction();
                        throw th;
                    }
                case 2:
                    j2 = (long) e.update(str, contentValues, str2, strArr);
                    e.setTransactionSuccessful();
                    e.endTransaction();
                    break;
                case 3:
                    j2 = (long) e.delete(str, str2, strArr);
                    e.setTransactionSuccessful();
                    e.endTransaction();
                    break;
                default:
                    e.setTransactionSuccessful();
                    e.endTransaction();
                    break;
            }
        }
        return j2;
    }

    private Map a(byte[] bArr) {
        ByteArrayInputStream byteArrayInputStream;
        DataInputStream dataInputStream;
        Throwable th;
        ByteArrayInputStream byteArrayInputStream2;
        DataInputStream dataInputStream2;
        Object readUTF;
        if (bArr == null || bArr.length == 0) {
            return null;
        }
        try {
            HashMap hashMap = new HashMap();
            byteArrayInputStream = new ByteArrayInputStream(bArr);
            try {
                dataInputStream = new DataInputStream(byteArrayInputStream);
                try {
                    int readInt = dataInputStream.readInt();
                    for (int i2 = 0; i2 < readInt; i2++) {
                        String readUTF2 = dataInputStream.readUTF();
                        int readInt2 = dataInputStream.readInt();
                        if (readInt2 == 66) {
                            readUTF = Double.valueOf(dataInputStream.readDouble());
                        } else if (readInt2 == 88) {
                            readUTF = dataInputStream.readUTF();
                        } else {
                            a(byteArrayInputStream);
                            a(dataInputStream);
                            return null;
                        }
                        hashMap.put(readUTF2, readUTF);
                    }
                    a(byteArrayInputStream);
                    a(dataInputStream);
                    return hashMap;
                } catch (Throwable th2) {
                    th = th2;
                    a(byteArrayInputStream);
                    a(dataInputStream);
                    throw th;
                }
            } catch (Throwable th3) {
                dataInputStream = null;
                th = th3;
                a(byteArrayInputStream);
                a(dataInputStream);
                throw th;
            }
        } catch (Throwable th4) {
            dataInputStream = null;
            byteArrayInputStream = null;
            th = th4;
            a(byteArrayInputStream);
            a(dataInputStream);
            throw th;
        }
    }

    private void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable th) {
            }
        }
    }

    private byte[] a(Map map, boolean z) {
        DataOutputStream dataOutputStream;
        ByteArrayOutputStream byteArrayOutputStream;
        DataOutputStream dataOutputStream2;
        ByteArrayOutputStream byteArrayOutputStream2;
        if (map == null || map.size() == 0) {
            return null;
        }
        int size = map.size() > 50 ? 50 : map.size();
        try {
            byteArrayOutputStream2 = new ByteArrayOutputStream();
            try {
                dataOutputStream2 = new DataOutputStream(byteArrayOutputStream2);
                try {
                    dataOutputStream2.writeInt(size);
                    int i2 = 0;
                    Iterator it = map.entrySet().iterator();
                    while (true) {
                        int i3 = i2;
                        if (!it.hasNext()) {
                            break;
                        }
                        Map.Entry entry = (Map.Entry) it.next();
                        dataOutputStream2.writeUTF((String) entry.getKey());
                        Object value = entry.getValue();
                        if (value instanceof Number) {
                            dataOutputStream2.writeInt(66);
                            dataOutputStream2.writeDouble(((Number) value).doubleValue());
                        } else {
                            dataOutputStream2.writeInt(88);
                            if (z) {
                                dataOutputStream2.writeUTF(ca.a(value.toString()));
                            } else {
                                dataOutputStream2.writeUTF(value.toString());
                            }
                        }
                        i2 = i3 + 1;
                        if (i2 == 50) {
                            break;
                        }
                    }
                    byte[] byteArray = byteArrayOutputStream2.toByteArray();
                    a(byteArrayOutputStream2);
                    a(dataOutputStream2);
                    return byteArray;
                } catch (Throwable th) {
                    th = th;
                    a(byteArrayOutputStream2);
                    a(dataOutputStream2);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                dataOutputStream2 = null;
                a(byteArrayOutputStream2);
                a(dataOutputStream2);
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream2 = null;
            byteArrayOutputStream2 = null;
            a(byteArrayOutputStream2);
            a(dataOutputStream2);
            throw th;
        }
    }

    static ez f() {
        if (b == null) {
            synchronized (ez.class) {
                if (b == null) {
                    b = new ez();
                }
            }
        }
        return b;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void i() {
        /*
            r6 = this;
            monitor-enter(r6)
            android.content.Context r0 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            if (r0 == 0) goto L_0x0064
            android.database.sqlite.SQLiteDatabase r0 = com.tendcloud.tenddata.ez.e     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            if (r0 != 0) goto L_0x0078
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            android.content.Context r1 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            java.io.File r1 = r1.getFilesDir()     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            java.lang.String r2 = "TDtcagent.db"
            r0.<init>(r1, r2)     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            boolean r1 = r0.exists()     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            java.io.File r2 = r0.getParentFile()     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            boolean r2 = r2.exists()     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            if (r2 != 0) goto L_0x002b
            java.io.File r2 = r0.getParentFile()     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            r2.mkdirs()     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
        L_0x002b:
            r2 = 0
            android.database.sqlite.SQLiteDatabase r2 = android.database.sqlite.SQLiteDatabase.openOrCreateDatabase(r0, r2)     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            com.tendcloud.tenddata.ez.e = r2     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            long r2 = r0.length()     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            r4 = 6144000(0x5dc000, double:3.0355393E-317)
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x0054
            r2 = 0
            com.tendcloud.tenddata.gd.f = r2     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            long r2 = r0.length()     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            r4 = 8089600(0x7b7000, double:3.9967934E-317)
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x0054
            j()     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            r6.k()     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            r0 = 1
            com.tendcloud.tenddata.gd.f = r0     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
        L_0x0054:
            android.database.sqlite.SQLiteDatabase r0 = com.tendcloud.tenddata.ez.e     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            r2 = 8192000(0x7d0000, double:4.047386E-317)
            r0.setMaximumSize(r2)     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            r0 = 1
            com.tendcloud.tenddata.ez.f = r0     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            if (r1 != 0) goto L_0x0066
            r6.k()     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
        L_0x0064:
            monitor-exit(r6)
            return
        L_0x0066:
            android.database.sqlite.SQLiteDatabase r0 = com.tendcloud.tenddata.ez.e     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            int r0 = r0.getVersion()     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            r1 = 6
            if (r1 <= r0) goto L_0x0064
            j()     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            r6.k()     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            goto L_0x0064
        L_0x0076:
            r0 = move-exception
            goto L_0x0064
        L_0x0078:
            int r0 = com.tendcloud.tenddata.ez.f     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            int r0 = r0 + 1
            com.tendcloud.tenddata.ez.f = r0     // Catch:{ Throwable -> 0x0076, all -> 0x007f }
            goto L_0x0064
        L_0x007f:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.ez.i():void");
    }

    private static void j() {
        e.setVersion(6);
        d.b(e);
        a.b(e);
        b.b(e);
        c.b(e);
    }

    private void k() {
        e.setVersion(6);
        d.a(e);
        a.a(e);
        b.a(e);
        c.a(e);
    }

    private synchronized void l() {
        f--;
        f = Math.max(0, f);
        if (f == 0 && e != null) {
            e.close();
            e = null;
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized long a(long j2) {
        return a("activity", (ContentValues) null, "_id<=? AND duration !=? ", new String[]{String.valueOf(j2), eq.a("0")}, 3);
    }

    /* access modifiers changed from: package-private */
    public synchronized long a(long j2, long j3) {
        ContentValues contentValues;
        contentValues = new ContentValues();
        contentValues.put("duration", eq.a(String.valueOf((j3 - e(j2)) / 1000)));
        return a("activity", contentValues, "_id=?", new String[]{String.valueOf(j2)}, 2);
    }

    /* access modifiers changed from: package-private */
    public synchronized long a(long j2, String str) {
        long j3;
        ContentValues contentValues = new ContentValues();
        contentValues.put("error_time", eq.a(String.valueOf(j2)));
        ff.c cVar = new ff.c();
        StringBuffer stringBuffer = new StringBuffer("");
        try {
            long a2 = a(str, cVar, stringBuffer);
            if (0 == a2) {
                contentValues.put("message", str.getBytes("UTF-8"));
                contentValues.put("repeat", eq.a(String.valueOf(1)));
                contentValues.put("shorthashcode", eq.a(stringBuffer.toString()));
                j3 = a("error_report", contentValues, (String) null, (String[]) null, 1);
            } else {
                contentValues.put("repeat", eq.a(String.valueOf(cVar.b + 1)));
                j3 = a("error_report", contentValues, "_id=?", new String[]{String.valueOf(a2)}, 2);
            }
        } catch (Throwable th) {
            j3 = 0;
        }
        return j3;
    }

    /* access modifiers changed from: package-private */
    public synchronized long a(String str) {
        ContentValues contentValues;
        contentValues = new ContentValues();
        contentValues.put("is_launch", eq.a(String.valueOf(2)));
        return a("session", contentValues, "session_id=?", new String[]{eq.a(str)}, 2);
    }

    /* access modifiers changed from: package-private */
    public synchronized long a(String str, int i2) {
        ContentValues contentValues;
        contentValues = new ContentValues();
        contentValues.put("duration", eq.a(String.valueOf(i2)));
        return a("session", contentValues, "session_id=?", new String[]{eq.a(str)}, 2);
    }

    /* access modifiers changed from: package-private */
    public synchronized long a(String str, long j2, long j3, int i2) {
        ContentValues contentValues;
        contentValues = new ContentValues();
        contentValues.put("session_id", eq.a(str));
        contentValues.put("start_time", eq.a(String.valueOf(j2)));
        contentValues.put("duration", eq.a(String.valueOf(0)));
        contentValues.put("is_launch", eq.a(String.valueOf(0)));
        contentValues.put("interval", eq.a(String.valueOf(j3)));
        contentValues.put("is_connected", eq.a(String.valueOf(i2)));
        return a("session", contentValues, (String) null, (String[]) null, 1);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00e9, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00f8, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00f8 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:5:0x0015] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00fb A[SYNTHETIC, Splitter:B:49:0x00fb] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long a(java.lang.String r11, com.tendcloud.tenddata.ff.c r12, java.lang.StringBuffer r13) {
        /*
            r10 = this;
            r9 = 3
            r8 = 0
            monitor-enter(r10)
            android.database.sqlite.SQLiteDatabase r0 = com.tendcloud.tenddata.ez.e     // Catch:{ Throwable -> 0x0108, all -> 0x0105 }
            java.lang.String r1 = "error_report"
            java.lang.String[] r2 = com.tendcloud.tenddata.ez.c.f     // Catch:{ Throwable -> 0x0108, all -> 0x0105 }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.String r7 = "_id"
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0108, all -> 0x0105 }
            java.lang.String r0 = "\r\n"
            java.lang.String[] r0 = r11.split(r0)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            int r1 = r0.length     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            if (r1 >= r9) goto L_0x0025
            r0 = 0
            if (r2 == 0) goto L_0x0023
            r2.close()     // Catch:{ all -> 0x00df }
        L_0x0023:
            monitor-exit(r10)
            return r0
        L_0x0025:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            r1.<init>()     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            r3 = 0
            r3 = r0[r3]     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            java.lang.String r3 = "\r\n"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            r3 = 1
            r3 = r0[r3]     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            java.lang.String r3 = "\r\n"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            r3 = 2
            r0 = r0[r3]     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            java.lang.String r1 = com.tendcloud.tenddata.ca.c(r0)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            r13.append(r1)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            boolean r1 = r2.moveToFirst()     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            if (r1 == 0) goto L_0x00ff
        L_0x005c:
            boolean r1 = r2.isAfterLast()     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            if (r1 != 0) goto L_0x00ff
            r1 = 1
            java.lang.String r1 = r2.getString(r1)     // Catch:{ Throwable -> 0x00e2, all -> 0x00f8 }
            java.lang.String r1 = com.tendcloud.tenddata.eq.b(r1)     // Catch:{ Throwable -> 0x00e2, all -> 0x00f8 }
            long r4 = java.lang.Long.parseLong(r1)     // Catch:{ Throwable -> 0x00e2, all -> 0x00f8 }
            r12.a = r4     // Catch:{ Throwable -> 0x00e2, all -> 0x00f8 }
            r1 = 2
            byte[] r1 = r2.getBlob(r1)     // Catch:{ Throwable -> 0x00e2, all -> 0x00f8 }
            r12.d = r1     // Catch:{ Throwable -> 0x00e2, all -> 0x00f8 }
            r1 = 3
            java.lang.String r1 = r2.getString(r1)     // Catch:{ Throwable -> 0x00e2, all -> 0x00f8 }
            java.lang.String r1 = com.tendcloud.tenddata.eq.b(r1)     // Catch:{ Throwable -> 0x00e2, all -> 0x00f8 }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Throwable -> 0x00e2, all -> 0x00f8 }
            r12.b = r1     // Catch:{ Throwable -> 0x00e2, all -> 0x00f8 }
            java.lang.String r1 = new java.lang.String     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            byte[] r3 = r12.d     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            java.lang.String r4 = "UTF-8"
            r1.<init>(r3, r4)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            int r3 = r1.length()     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            int r4 = r0.length()     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            if (r3 < r4) goto L_0x005c
            java.lang.String r3 = "\r\n"
            java.lang.String[] r1 = r1.split(r3)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            int r3 = r1.length     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            if (r3 < r9) goto L_0x005c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            r3.<init>()     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            r4 = 0
            r4 = r1[r4]     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            java.lang.String r4 = "\r\n"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            r4 = 1
            r4 = r1[r4]     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            java.lang.String r4 = "\r\n"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            r4 = 2
            r1 = r1[r4]     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            boolean r1 = r1.equals(r0)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            if (r1 == 0) goto L_0x00f3
            r0 = 0
            long r0 = r2.getLong(r0)     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            if (r2 == 0) goto L_0x0023
            r2.close()     // Catch:{ all -> 0x00df }
            goto L_0x0023
        L_0x00df:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x00e2:
            r1 = move-exception
            r2.moveToNext()     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            goto L_0x005c
        L_0x00e8:
            r0 = move-exception
            r0 = r2
        L_0x00ea:
            if (r0 == 0) goto L_0x00ef
            r0.close()     // Catch:{ all -> 0x00df }
        L_0x00ef:
            r0 = 0
            goto L_0x0023
        L_0x00f3:
            r2.moveToNext()     // Catch:{ Throwable -> 0x00e8, all -> 0x00f8 }
            goto L_0x005c
        L_0x00f8:
            r0 = move-exception
        L_0x00f9:
            if (r2 == 0) goto L_0x00fe
            r2.close()     // Catch:{ all -> 0x00df }
        L_0x00fe:
            throw r0     // Catch:{ all -> 0x00df }
        L_0x00ff:
            if (r2 == 0) goto L_0x00ef
            r2.close()     // Catch:{ all -> 0x00df }
            goto L_0x00ef
        L_0x0105:
            r0 = move-exception
            r2 = r8
            goto L_0x00f9
        L_0x0108:
            r0 = move-exception
            r0 = r8
            goto L_0x00ea
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.ez.a(java.lang.String, com.tendcloud.tenddata.ff$c, java.lang.StringBuffer):long");
    }

    /* access modifiers changed from: package-private */
    public synchronized long a(String str, String str2, long j2, int i2, String str3, long j3) {
        ContentValues contentValues;
        contentValues = new ContentValues();
        contentValues.put("session_id", eq.a(str));
        contentValues.put("name", eq.a(str2));
        contentValues.put("start_time", eq.a(String.valueOf(j2)));
        contentValues.put("duration", eq.a(String.valueOf(i2)));
        contentValues.put("refer", eq.a(str3));
        contentValues.put("realtime", eq.a(String.valueOf(j3)));
        return a("activity", contentValues, (String) null, (String[]) null, 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.ez.a(java.util.Map, boolean):byte[]
     arg types: [java.util.Map, int]
     candidates:
      com.tendcloud.tenddata.ez.a(long, long):long
      com.tendcloud.tenddata.ez.a(long, java.lang.String):long
      com.tendcloud.tenddata.ez.a(java.lang.String, int):long
      com.tendcloud.tenddata.ez.a(java.lang.String, long):java.util.List
      com.tendcloud.tenddata.el.a(long, long):long
      com.tendcloud.tenddata.el.a(long, java.lang.String):long
      com.tendcloud.tenddata.el.a(java.lang.String, int):long
      com.tendcloud.tenddata.el.a(java.lang.String, long):java.util.List
      com.tendcloud.tenddata.ez.a(java.util.Map, boolean):byte[] */
    /* access modifiers changed from: package-private */
    public synchronized long a(String str, String str2, String str3, long j2, Map map) {
        ContentValues contentValues;
        contentValues = new ContentValues();
        contentValues.put("event_id", eq.a(str2));
        contentValues.put("event_label", eq.a(str3));
        contentValues.put("session_id", eq.a(str));
        contentValues.put("occurtime", eq.a(String.valueOf(j2)));
        contentValues.put("paramap", a(map, true));
        return a("app_event", contentValues, (String) null, (String[]) null, 1);
    }

    /* access modifiers changed from: package-private */
    public synchronized long a(List list) {
        long j2;
        int size = list.size();
        if (size != 0) {
            int i2 = size - 1;
            long j3 = 0;
            while (true) {
                if (i2 < 0) {
                    j2 = j3;
                    break;
                }
                try {
                    Cursor cursor = null;
                    Cursor rawQuery = e.rawQuery("SELECT MAX(_id) from activity where duration != 0 and session_id =?", new String[]{eq.a(((ff.j) list.get(i2)).a)});
                    try {
                        if (rawQuery.moveToFirst()) {
                            j2 = rawQuery.getLong(0);
                            if (j2 == 0) {
                                j3 = j2;
                            } else if (rawQuery != null) {
                                try {
                                    rawQuery.close();
                                } catch (Throwable th) {
                                }
                            }
                        }
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        i2--;
                    } catch (Throwable th2) {
                        th = th2;
                        cursor = rawQuery;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    j2 = j3;
                }
            }
        } else {
            j2 = 0;
        }
        return j2;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0083, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0084, code lost:
        r8 = r0;
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r8.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0083 A[ExcHandler: all (r1v1 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:6:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0088 A[SYNTHETIC, Splitter:B:29:0x0088] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List a(java.lang.String r11, long r12) {
        /*
            r10 = this;
            r8 = 0
            monitor-enter(r10)
            java.util.ArrayList r9 = new java.util.ArrayList     // Catch:{ all -> 0x008c }
            r9.<init>()     // Catch:{ all -> 0x008c }
            android.database.sqlite.SQLiteDatabase r0 = com.tendcloud.tenddata.ez.e     // Catch:{ Throwable -> 0x0097, all -> 0x0095 }
            java.lang.String r1 = "activity"
            java.lang.String[] r2 = com.tendcloud.tenddata.ez.a.h     // Catch:{ Throwable -> 0x0097, all -> 0x0095 }
            java.lang.String r3 = "session_id=? AND duration !=? "
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x0097, all -> 0x0095 }
            r5 = 0
            java.lang.String r6 = com.tendcloud.tenddata.eq.a(r11)     // Catch:{ Throwable -> 0x0097, all -> 0x0095 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x0097, all -> 0x0095 }
            r5 = 1
            java.lang.String r6 = "0"
            java.lang.String r6 = com.tendcloud.tenddata.eq.a(r6)     // Catch:{ Throwable -> 0x0097, all -> 0x0095 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x0097, all -> 0x0095 }
            r5 = 0
            r6 = 0
            java.lang.String r7 = "_id"
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0097, all -> 0x0095 }
            boolean r1 = r0.moveToFirst()     // Catch:{ Throwable -> 0x0076, all -> 0x0083 }
            if (r1 == 0) goto L_0x008f
        L_0x0030:
            boolean r1 = r0.isAfterLast()     // Catch:{ Throwable -> 0x0076, all -> 0x0083 }
            if (r1 != 0) goto L_0x008f
            com.tendcloud.tenddata.ff$a r1 = new com.tendcloud.tenddata.ff$a     // Catch:{ Throwable -> 0x0076, all -> 0x0083 }
            r1.<init>()     // Catch:{ Throwable -> 0x0076, all -> 0x0083 }
            r2 = 1
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Throwable -> 0x007e, all -> 0x0083 }
            java.lang.String r2 = com.tendcloud.tenddata.eq.b(r2)     // Catch:{ Throwable -> 0x007e, all -> 0x0083 }
            r1.a = r2     // Catch:{ Throwable -> 0x007e, all -> 0x0083 }
            r2 = 2
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Throwable -> 0x007e, all -> 0x0083 }
            java.lang.String r2 = com.tendcloud.tenddata.eq.b(r2)     // Catch:{ Throwable -> 0x007e, all -> 0x0083 }
            long r2 = java.lang.Long.parseLong(r2)     // Catch:{ Throwable -> 0x007e, all -> 0x0083 }
            r1.b = r2     // Catch:{ Throwable -> 0x007e, all -> 0x0083 }
            r2 = 3
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Throwable -> 0x007e, all -> 0x0083 }
            java.lang.String r2 = com.tendcloud.tenddata.eq.b(r2)     // Catch:{ Throwable -> 0x007e, all -> 0x0083 }
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Throwable -> 0x007e, all -> 0x0083 }
            r1.c = r2     // Catch:{ Throwable -> 0x007e, all -> 0x0083 }
            r2 = 5
            java.lang.String r2 = r0.getString(r2)     // Catch:{ Throwable -> 0x007e, all -> 0x0083 }
            java.lang.String r2 = com.tendcloud.tenddata.eq.b(r2)     // Catch:{ Throwable -> 0x007e, all -> 0x0083 }
            r1.d = r2     // Catch:{ Throwable -> 0x007e, all -> 0x0083 }
            r9.add(r1)     // Catch:{ Throwable -> 0x0076, all -> 0x0083 }
            r0.moveToNext()     // Catch:{ Throwable -> 0x0076, all -> 0x0083 }
            goto L_0x0030
        L_0x0076:
            r1 = move-exception
        L_0x0077:
            if (r0 == 0) goto L_0x007c
            r0.close()     // Catch:{ all -> 0x008c }
        L_0x007c:
            monitor-exit(r10)
            return r9
        L_0x007e:
            r1 = move-exception
            r0.moveToNext()     // Catch:{ Throwable -> 0x0076, all -> 0x0083 }
            goto L_0x0030
        L_0x0083:
            r1 = move-exception
            r8 = r0
            r0 = r1
        L_0x0086:
            if (r8 == 0) goto L_0x008b
            r8.close()     // Catch:{ all -> 0x008c }
        L_0x008b:
            throw r0     // Catch:{ all -> 0x008c }
        L_0x008c:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x008f:
            if (r0 == 0) goto L_0x007c
            r0.close()     // Catch:{ all -> 0x008c }
            goto L_0x007c
        L_0x0095:
            r0 = move-exception
            goto L_0x0086
        L_0x0097:
            r0 = move-exception
            r0 = r8
            goto L_0x0077
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.ez.a(java.lang.String, long):java.util.List");
    }

    /* access modifiers changed from: package-private */
    public void a() {
        i();
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        if (ab.mContext == null && context != null) {
            ab.mContext = context;
        }
        i();
    }

    /* access modifiers changed from: package-private */
    public synchronized long b(long j2) {
        return a("app_event", (ContentValues) null, "_id<=? ", new String[]{String.valueOf(j2)}, 3);
    }

    /* access modifiers changed from: package-private */
    public synchronized long b(String str) {
        return a("session", (ContentValues) null, "session_id=?", new String[]{eq.a(str)}, 3);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0047, code lost:
        if (r4 == null) goto L_0x000a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r4.close();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long b(java.util.List r10) {
        /*
            r9 = this;
            r2 = 0
            monitor-enter(r9)
            int r0 = r10.size()     // Catch:{ all -> 0x0060 }
            if (r0 != 0) goto L_0x000c
            r0 = r2
        L_0x000a:
            monitor-exit(r9)
            return r0
        L_0x000c:
            int r0 = r0 + -1
            r5 = r0
        L_0x000f:
            if (r5 < 0) goto L_0x004e
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x004d }
            r0.<init>()     // Catch:{ Throwable -> 0x004d }
            java.lang.String r1 = "SELECT MAX(_id) from app_event where session_id =?"
            r0.append(r1)     // Catch:{ Throwable -> 0x004d }
            r1 = 0
            android.database.sqlite.SQLiteDatabase r4 = com.tendcloud.tenddata.ez.e     // Catch:{ all -> 0x0059 }
            java.lang.String r6 = r0.toString()     // Catch:{ all -> 0x0059 }
            r0 = 1
            java.lang.String[] r7 = new java.lang.String[r0]     // Catch:{ all -> 0x0059 }
            r8 = 0
            java.lang.Object r0 = r10.get(r5)     // Catch:{ all -> 0x0059 }
            com.tendcloud.tenddata.ff$j r0 = (com.tendcloud.tenddata.ff.j) r0     // Catch:{ all -> 0x0059 }
            java.lang.String r0 = r0.a     // Catch:{ all -> 0x0059 }
            java.lang.String r0 = com.tendcloud.tenddata.eq.a(r0)     // Catch:{ all -> 0x0059 }
            r7[r8] = r0     // Catch:{ all -> 0x0059 }
            android.database.Cursor r4 = r4.rawQuery(r6, r7)     // Catch:{ all -> 0x0059 }
            boolean r0 = r4.moveToFirst()     // Catch:{ all -> 0x0063 }
            if (r0 == 0) goto L_0x0050
            r0 = 0
            long r0 = r4.getLong(r0)     // Catch:{ all -> 0x0063 }
            int r6 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r6 == 0) goto L_0x0050
            if (r4 == 0) goto L_0x000a
            r4.close()     // Catch:{ Throwable -> 0x004d }
            goto L_0x000a
        L_0x004d:
            r0 = move-exception
        L_0x004e:
            r0 = r2
            goto L_0x000a
        L_0x0050:
            if (r4 == 0) goto L_0x0055
            r4.close()     // Catch:{ Throwable -> 0x004d }
        L_0x0055:
            int r0 = r5 + -1
            r5 = r0
            goto L_0x000f
        L_0x0059:
            r0 = move-exception
        L_0x005a:
            if (r1 == 0) goto L_0x005f
            r1.close()     // Catch:{ Throwable -> 0x004d }
        L_0x005f:
            throw r0     // Catch:{ Throwable -> 0x004d }
        L_0x0060:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x0063:
            r0 = move-exception
            r1 = r4
            goto L_0x005a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.ez.b(java.util.List):long");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0078, code lost:
        if (r0 != null) goto L_0x007a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0084, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0085, code lost:
        r7 = r1;
        r1 = r0;
        r0 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0077 A[ExcHandler: Throwable (th java.lang.Throwable), Splitter:B:4:0x0007] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0084 A[ExcHandler: all (r1v4 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:6:0x0025] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x008a A[SYNTHETIC, Splitter:B:29:0x008a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List b(java.lang.String r9, long r10) {
        /*
            r8 = this;
            r0 = 0
            monitor-enter(r8)
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x008e }
            r1.<init>()     // Catch:{ all -> 0x008e }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0077, all -> 0x0097 }
            r2.<init>()     // Catch:{ Throwable -> 0x0077, all -> 0x0097 }
            java.lang.String r3 = "SELECT COUNT(_id), MAX(occurtime), event_id, event_label, paramap from app_event where session_id = ? group by event_id, event_label, paramap"
            r2.append(r3)     // Catch:{ Throwable -> 0x0077, all -> 0x0097 }
            android.database.sqlite.SQLiteDatabase r3 = com.tendcloud.tenddata.ez.e     // Catch:{ Throwable -> 0x0077, all -> 0x0097 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0077, all -> 0x0097 }
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x0077, all -> 0x0097 }
            r5 = 0
            java.lang.String r6 = com.tendcloud.tenddata.eq.a(r9)     // Catch:{ Throwable -> 0x0077, all -> 0x0097 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x0077, all -> 0x0097 }
            android.database.Cursor r0 = r3.rawQuery(r2, r4)     // Catch:{ Throwable -> 0x0077, all -> 0x0097 }
            boolean r2 = r0.moveToFirst()     // Catch:{ Throwable -> 0x0077, all -> 0x0084 }
            if (r2 == 0) goto L_0x0091
        L_0x002b:
            boolean r2 = r0.isAfterLast()     // Catch:{ Throwable -> 0x0077, all -> 0x0084 }
            if (r2 != 0) goto L_0x0091
            com.tendcloud.tenddata.ff$b r2 = new com.tendcloud.tenddata.ff$b     // Catch:{ Throwable -> 0x0077, all -> 0x0084 }
            r2.<init>()     // Catch:{ Throwable -> 0x0077, all -> 0x0084 }
            r3 = 0
            int r3 = r0.getInt(r3)     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            r2.c = r3     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            r3 = 1
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            java.lang.String r3 = com.tendcloud.tenddata.eq.b(r3)     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            long r4 = java.lang.Long.parseLong(r3)     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            r2.d = r4     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            r3 = 2
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            java.lang.String r3 = com.tendcloud.tenddata.eq.b(r3)     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            r2.a = r3     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            r3 = 3
            java.lang.String r3 = r0.getString(r3)     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            java.lang.String r3 = com.tendcloud.tenddata.eq.b(r3)     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            r2.b = r3     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            r3 = 0
            r2.e = r3     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            r3 = 4
            byte[] r3 = r0.getBlob(r3)     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            java.util.Map r3 = r8.a(r3)     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            r2.e = r3     // Catch:{ Throwable -> 0x007f, all -> 0x0084 }
            r1.add(r2)     // Catch:{ Throwable -> 0x0077, all -> 0x0084 }
            r0.moveToNext()     // Catch:{ Throwable -> 0x0077, all -> 0x0084 }
            goto L_0x002b
        L_0x0077:
            r2 = move-exception
            if (r0 == 0) goto L_0x007d
            r0.close()     // Catch:{ all -> 0x008e }
        L_0x007d:
            monitor-exit(r8)
            return r1
        L_0x007f:
            r2 = move-exception
            r0.moveToNext()     // Catch:{ Throwable -> 0x0077, all -> 0x0084 }
            goto L_0x002b
        L_0x0084:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x0088:
            if (r1 == 0) goto L_0x008d
            r1.close()     // Catch:{ all -> 0x008e }
        L_0x008d:
            throw r0     // Catch:{ all -> 0x008e }
        L_0x008e:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        L_0x0091:
            if (r0 == 0) goto L_0x007d
            r0.close()     // Catch:{ all -> 0x008e }
            goto L_0x007d
        L_0x0097:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0088
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.ez.b(java.lang.String, long):java.util.List");
    }

    /* access modifiers changed from: package-private */
    public void b() {
        l();
    }

    /* access modifiers changed from: package-private */
    public synchronized long c(long j2) {
        return a("error_report", (ContentValues) null, "_id<=?", new String[]{String.valueOf(j2)}, 3);
    }

    /* access modifiers changed from: package-private */
    public synchronized long c(String str) {
        return a("activity", (ContentValues) null, "session_id=? ", new String[]{eq.a(str)}, 3);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        b();
    }

    /* access modifiers changed from: package-private */
    public synchronized long d() {
        return DatabaseUtils.queryNumEntries(e, "session");
    }

    /* access modifiers changed from: package-private */
    public synchronized long d(String str) {
        return a("app_event", (ContentValues) null, "session_id=? ", new String[]{eq.a(str)}, 3);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0086, code lost:
        r0 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0096, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0096 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:6:0x0025] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0099 A[SYNTHETIC, Splitter:B:33:0x0099] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List d(long r10) {
        /*
            r9 = this;
            monitor-enter(r9)
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x009d }
            r2.<init>()     // Catch:{ all -> 0x009d }
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            r1.<init>()     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            java.lang.String r3 = "SELECT error_time,message,repeat, shorthashcode from error_report where _id<=?"
            r1.append(r3)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            android.database.sqlite.SQLiteDatabase r3 = com.tendcloud.tenddata.ez.e     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            r5 = 0
            java.lang.String r6 = java.lang.String.valueOf(r10)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            android.database.Cursor r1 = r3.rawQuery(r1, r4)     // Catch:{ Throwable -> 0x00ab, all -> 0x00a6 }
            boolean r0 = r1.moveToFirst()     // Catch:{ Throwable -> 0x0085, all -> 0x0096 }
            if (r0 == 0) goto L_0x00a0
            android.content.Context r0 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x0085, all -> 0x0096 }
            if (r0 == 0) goto L_0x008e
            int r0 = com.tendcloud.tenddata.ew.n()     // Catch:{ Throwable -> 0x0085, all -> 0x0096 }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ Throwable -> 0x0085, all -> 0x0096 }
        L_0x0037:
            boolean r3 = r1.isAfterLast()     // Catch:{ Throwable -> 0x0085, all -> 0x0096 }
            if (r3 != 0) goto L_0x00a0
            com.tendcloud.tenddata.ff$i r3 = new com.tendcloud.tenddata.ff$i     // Catch:{ Throwable -> 0x0085, all -> 0x0096 }
            r3.<init>()     // Catch:{ Throwable -> 0x0085, all -> 0x0096 }
            r4 = 3
            r3.a = r4     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            com.tendcloud.tenddata.ff$c r4 = new com.tendcloud.tenddata.ff$c     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            r4.<init>()     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            r5 = 0
            java.lang.String r5 = r1.getString(r5)     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            java.lang.String r5 = com.tendcloud.tenddata.eq.b(r5)     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            long r6 = java.lang.Long.parseLong(r5)     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            r4.a = r6     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            r5 = 1
            byte[] r5 = r1.getBlob(r5)     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            r4.d = r5     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            r5 = 2
            java.lang.String r5 = r1.getString(r5)     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            java.lang.String r5 = com.tendcloud.tenddata.eq.b(r5)     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            int r5 = java.lang.Integer.parseInt(r5)     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            r4.b = r5     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            r5 = 3
            java.lang.String r5 = r1.getString(r5)     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            java.lang.String r5 = com.tendcloud.tenddata.eq.b(r5)     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            r4.e = r5     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            r4.c = r0     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            r3.d = r4     // Catch:{ Throwable -> 0x0091, all -> 0x0096 }
            r2.add(r3)     // Catch:{ Throwable -> 0x0085, all -> 0x0096 }
            r1.moveToNext()     // Catch:{ Throwable -> 0x0085, all -> 0x0096 }
            goto L_0x0037
        L_0x0085:
            r0 = move-exception
            r0 = r1
        L_0x0087:
            if (r0 == 0) goto L_0x008c
            r0.close()     // Catch:{ all -> 0x009d }
        L_0x008c:
            monitor-exit(r9)
            return r2
        L_0x008e:
            java.lang.String r0 = ""
            goto L_0x0037
        L_0x0091:
            r3 = move-exception
            r1.moveToNext()     // Catch:{ Throwable -> 0x0085, all -> 0x0096 }
            goto L_0x0037
        L_0x0096:
            r0 = move-exception
        L_0x0097:
            if (r1 == 0) goto L_0x009c
            r1.close()     // Catch:{ all -> 0x009d }
        L_0x009c:
            throw r0     // Catch:{ all -> 0x009d }
        L_0x009d:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x00a0:
            if (r1 == 0) goto L_0x008c
            r1.close()     // Catch:{ all -> 0x009d }
            goto L_0x008c
        L_0x00a6:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x0097
        L_0x00ab:
            r1 = move-exception
            goto L_0x0087
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.ez.d(long):java.util.List");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0052 A[SYNTHETIC, Splitter:B:28:0x0052] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long e(long r10) {
        /*
            r9 = this;
            r8 = 0
            monitor-enter(r9)
            android.database.sqlite.SQLiteDatabase r0 = com.tendcloud.tenddata.ez.e     // Catch:{ Throwable -> 0x0044, all -> 0x004f }
            java.lang.String r1 = "activity"
            java.lang.String[] r2 = com.tendcloud.tenddata.ez.a.h     // Catch:{ Throwable -> 0x0044, all -> 0x004f }
            java.lang.String r3 = "_id=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Throwable -> 0x0044, all -> 0x004f }
            r5 = 0
            java.lang.String r6 = java.lang.String.valueOf(r10)     // Catch:{ Throwable -> 0x0044, all -> 0x004f }
            r4[r5] = r6     // Catch:{ Throwable -> 0x0044, all -> 0x004f }
            r5 = 0
            r6 = 0
            java.lang.String r7 = "_id"
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Throwable -> 0x0044, all -> 0x004f }
            boolean r0 = r2.moveToFirst()     // Catch:{ Throwable -> 0x0059, all -> 0x0056 }
            if (r0 == 0) goto L_0x003c
            boolean r0 = r2.isAfterLast()     // Catch:{ Throwable -> 0x0059, all -> 0x0056 }
            if (r0 != 0) goto L_0x003c
            r0 = 6
            java.lang.String r0 = r2.getString(r0)     // Catch:{ Throwable -> 0x0059, all -> 0x0056 }
            java.lang.String r0 = com.tendcloud.tenddata.eq.b(r0)     // Catch:{ Throwable -> 0x0059, all -> 0x0056 }
            long r0 = java.lang.Long.parseLong(r0)     // Catch:{ Throwable -> 0x0059, all -> 0x0056 }
            if (r2 == 0) goto L_0x003a
            r2.close()     // Catch:{ all -> 0x004c }
        L_0x003a:
            monitor-exit(r9)
            return r0
        L_0x003c:
            if (r2 == 0) goto L_0x0041
            r2.close()     // Catch:{ all -> 0x004c }
        L_0x0041:
            r0 = 0
            goto L_0x003a
        L_0x0044:
            r0 = move-exception
            r0 = r8
        L_0x0046:
            if (r0 == 0) goto L_0x0041
            r0.close()     // Catch:{ all -> 0x004c }
            goto L_0x0041
        L_0x004c:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        L_0x004f:
            r0 = move-exception
        L_0x0050:
            if (r8 == 0) goto L_0x0055
            r8.close()     // Catch:{ all -> 0x004c }
        L_0x0055:
            throw r0     // Catch:{ all -> 0x004c }
        L_0x0056:
            r0 = move-exception
            r8 = r2
            goto L_0x0050
        L_0x0059:
            r0 = move-exception
            r0 = r2
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.ez.e(long):long");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0049 A[SYNTHETIC, Splitter:B:28:0x0049] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long e(java.lang.String r5) {
        /*
            r4 = this;
            r0 = 0
            monitor-enter(r4)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x003a, all -> 0x0044 }
            r1.<init>()     // Catch:{ Throwable -> 0x003a, all -> 0x0044 }
            java.lang.String r2 = "SELECT MAX(_id) from "
            r1.append(r2)     // Catch:{ Throwable -> 0x003a, all -> 0x0044 }
            r1.append(r5)     // Catch:{ Throwable -> 0x003a, all -> 0x0044 }
            android.database.sqlite.SQLiteDatabase r2 = com.tendcloud.tenddata.ez.e     // Catch:{ Throwable -> 0x003a, all -> 0x0044 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x003a, all -> 0x0044 }
            r3 = 0
            android.database.Cursor r2 = r2.rawQuery(r1, r3)     // Catch:{ Throwable -> 0x003a, all -> 0x0044 }
            boolean r0 = r2.moveToFirst()     // Catch:{ Throwable -> 0x004f, all -> 0x004d }
            if (r0 == 0) goto L_0x0032
            boolean r0 = r2.isAfterLast()     // Catch:{ Throwable -> 0x004f, all -> 0x004d }
            if (r0 != 0) goto L_0x0032
            r0 = 0
            long r0 = r2.getLong(r0)     // Catch:{ Throwable -> 0x004f, all -> 0x004d }
            if (r2 == 0) goto L_0x0030
            r2.close()     // Catch:{ all -> 0x0041 }
        L_0x0030:
            monitor-exit(r4)
            return r0
        L_0x0032:
            if (r2 == 0) goto L_0x0037
            r2.close()     // Catch:{ all -> 0x0041 }
        L_0x0037:
            r0 = 0
            goto L_0x0030
        L_0x003a:
            r1 = move-exception
        L_0x003b:
            if (r0 == 0) goto L_0x0037
            r0.close()     // Catch:{ all -> 0x0041 }
            goto L_0x0037
        L_0x0041:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0044:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0047:
            if (r2 == 0) goto L_0x004c
            r2.close()     // Catch:{ all -> 0x0041 }
        L_0x004c:
            throw r0     // Catch:{ all -> 0x0041 }
        L_0x004d:
            r0 = move-exception
            goto L_0x0047
        L_0x004f:
            r0 = move-exception
            r0 = r2
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.ez.e(java.lang.String):long");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c5, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00c6, code lost:
        r9 = r0;
        r0 = r1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00c5 A[ExcHandler: all (r1v1 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:21:0x0072] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00ca A[SYNTHETIC, Splitter:B:46:0x00ca] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List e() {
        /*
            r14 = this;
            r10 = 3
            r11 = 2
            r13 = 1
            r9 = 0
            monitor-enter(r14)
            java.util.ArrayList r12 = new java.util.ArrayList     // Catch:{ all -> 0x00ce }
            r12.<init>()     // Catch:{ all -> 0x00ce }
            android.database.sqlite.SQLiteDatabase r0 = com.tendcloud.tenddata.ez.e     // Catch:{ Throwable -> 0x00db, all -> 0x00d9 }
            java.lang.String r1 = "session"
            java.lang.String[] r2 = com.tendcloud.tenddata.ez.d.h     // Catch:{ Throwable -> 0x00db, all -> 0x00d9 }
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            java.lang.String r7 = "_id"
            java.lang.String r8 = "10"
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ Throwable -> 0x00db, all -> 0x00d9 }
            boolean r1 = r0.moveToFirst()     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            if (r1 == 0) goto L_0x00d3
        L_0x0022:
            boolean r1 = r0.isAfterLast()     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            if (r1 != 0) goto L_0x00d3
            com.tendcloud.tenddata.ff$j r2 = new com.tendcloud.tenddata.ff$j     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            r2.<init>()     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            r1 = 1
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            java.lang.String r1 = com.tendcloud.tenddata.eq.b(r1)     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            r2.a = r1     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            r1 = 2
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            java.lang.String r1 = com.tendcloud.tenddata.eq.b(r1)     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            long r4 = java.lang.Long.parseLong(r1)     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            r2.b = r4     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            r1 = 3
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            java.lang.String r1 = com.tendcloud.tenddata.eq.b(r1)     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            r2.g = r1     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            r1 = 4
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            java.lang.String r1 = com.tendcloud.tenddata.eq.b(r1)     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            if (r1 == 0) goto L_0x00b7
            java.lang.String r3 = "null"
            boolean r3 = r1.equals(r3)     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            if (r3 != 0) goto L_0x00b7
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            if (r1 != 0) goto L_0x00b7
            r1 = 1
            r2.c = r1     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
        L_0x0072:
            int r1 = r2.c     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            if (r13 != r1) goto L_0x0092
            r1 = 5
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            java.lang.String r1 = com.tendcloud.tenddata.eq.b(r1)     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            r2.j = r1     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            int r1 = r2.j     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            if (r1 >= 0) goto L_0x008c
            r1 = 0
            r2.j = r1     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
        L_0x008c:
            int r1 = r2.j     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            int r1 = r1 / 1000
            r2.g = r1     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
        L_0x0092:
            r1 = 6
            java.lang.String r1 = r0.getString(r1)     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            java.lang.String r1 = com.tendcloud.tenddata.eq.b(r1)     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            r2.k = r1     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            long r4 = com.tendcloud.tenddata.ew.m()     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            r2.l = r4     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            r12.add(r2)     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            r0.moveToNext()     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            goto L_0x0022
        L_0x00af:
            r1 = move-exception
        L_0x00b0:
            if (r0 == 0) goto L_0x00b5
            r0.close()     // Catch:{ all -> 0x00ce }
        L_0x00b5:
            monitor-exit(r14)
            return r12
        L_0x00b7:
            int r1 = r2.g     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            if (r1 == 0) goto L_0x00d1
            r1 = r10
        L_0x00bc:
            r2.c = r1     // Catch:{ Throwable -> 0x00bf, all -> 0x00c5 }
            goto L_0x0072
        L_0x00bf:
            r1 = move-exception
            r0.moveToNext()     // Catch:{ Throwable -> 0x00af, all -> 0x00c5 }
            goto L_0x0022
        L_0x00c5:
            r1 = move-exception
            r9 = r0
            r0 = r1
        L_0x00c8:
            if (r9 == 0) goto L_0x00cd
            r9.close()     // Catch:{ all -> 0x00ce }
        L_0x00cd:
            throw r0     // Catch:{ all -> 0x00ce }
        L_0x00ce:
            r0 = move-exception
            monitor-exit(r14)
            throw r0
        L_0x00d1:
            r1 = r11
            goto L_0x00bc
        L_0x00d3:
            if (r0 == 0) goto L_0x00b5
            r0.close()     // Catch:{ all -> 0x00ce }
            goto L_0x00b5
        L_0x00d9:
            r0 = move-exception
            goto L_0x00c8
        L_0x00db:
            r0 = move-exception
            r0 = r9
            goto L_0x00b0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.ez.e():java.util.List");
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return e != null && e.isOpen();
    }

    /* access modifiers changed from: package-private */
    public void h() {
        try {
            l();
            File file = new File(ab.mContext.getFilesDir(), a);
            if (!file.exists()) {
                return;
            }
            if (Build.VERSION.SDK_INT >= 16) {
                SQLiteDatabase.deleteDatabase(file);
            } else {
                file.delete();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
