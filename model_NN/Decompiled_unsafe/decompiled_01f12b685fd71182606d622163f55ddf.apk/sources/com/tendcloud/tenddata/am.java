package com.tendcloud.tenddata;

import java.util.Collections;
import java.util.Iterator;
import java.util.TreeMap;

public class am implements ai {
    private byte[] a;
    private TreeMap b = new TreeMap(String.CASE_INSENSITIVE_ORDER);

    public String a(String str) {
        String str2 = (String) this.b.get(str);
        return str2 == null ? "" : str2;
    }

    public void a(String str, String str2) {
        this.b.put(str, str2);
    }

    public boolean b(String str) {
        return this.b.containsKey(str);
    }

    public Iterator c() {
        return Collections.unmodifiableSet(this.b.keySet()).iterator();
    }

    public byte[] d() {
        return this.a;
    }

    public void setContent(byte[] bArr) {
        this.a = bArr;
    }
}
