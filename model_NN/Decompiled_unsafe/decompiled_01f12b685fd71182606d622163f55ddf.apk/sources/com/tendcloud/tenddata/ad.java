package com.tendcloud.tenddata;

import java.nio.ByteBuffer;

public interface ad {

    public enum a {
        CONTINUOUS,
        TEXT,
        BINARY,
        PING,
        PONG,
        CLOSING
    }

    void append(ad adVar);

    ByteBuffer c();

    boolean d();

    boolean e();

    a f();
}
