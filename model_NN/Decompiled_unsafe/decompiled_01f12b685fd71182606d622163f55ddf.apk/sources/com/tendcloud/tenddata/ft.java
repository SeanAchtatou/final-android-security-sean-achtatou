package com.tendcloud.tenddata;

import android.util.Log;
import com.tendcloud.tenddata.gd;
import java.lang.Thread;
import java.util.HashMap;

class ft {
    private static volatile ft a = null;

    static class a implements Thread.UncaughtExceptionHandler {
        private Thread.UncaughtExceptionHandler a = Thread.getDefaultUncaughtExceptionHandler();

        a() {
        }

        public void uncaughtException(Thread thread, Throwable th) {
            if (ab.b) {
                ft.a(th, String.valueOf(System.currentTimeMillis()));
                Log.w("TDLog", "UncaughtException in Thread " + thread.getName(), th);
            }
            if (this.a != null) {
                this.a.uncaughtException(thread, th);
            }
        }
    }

    static {
        try {
            gp.a().register(a());
        } catch (Throwable th) {
        }
    }

    private ft() {
        b();
    }

    public static ft a() {
        if (a == null) {
            synchronized (ft.class) {
                if (a == null) {
                    a = new ft();
                }
            }
        }
        return a;
    }

    private static final String a(Throwable th) {
        int i = 50;
        StringBuilder sb = new StringBuilder();
        sb.append(th.toString());
        sb.append("\r\n");
        StackTraceElement[] stackTrace = th.getStackTrace();
        if (stackTrace.length <= 50) {
            i = stackTrace.length;
        }
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("\t");
            sb.append(stackTrace[i2]);
            sb.append("\r\n");
        }
        Throwable cause = th.getCause();
        if (cause != null) {
            a(sb, stackTrace, cause, 1);
        }
        return sb.toString();
    }

    private static final void a(StringBuilder sb, StackTraceElement[] stackTraceElementArr, Throwable th, int i) {
        int i2 = 50;
        StackTraceElement[] stackTrace = th.getStackTrace();
        int length = stackTrace.length - 1;
        int length2 = stackTraceElementArr.length - 1;
        while (length >= 0 && length2 >= 0 && stackTrace[length].equals(stackTraceElementArr[length2])) {
            length2--;
            length--;
        }
        if (length <= 50) {
            i2 = length;
        }
        sb.append("Caused by : ");
        sb.append(th);
        sb.append("\r\n");
        for (int i3 = 0; i3 <= i2; i3++) {
            sb.append("\t");
            sb.append(stackTrace[i3]);
            sb.append("\r\n");
        }
        if (i < 5 && th.getCause() != null) {
            a(sb, stackTrace, th, i + 1);
        }
    }

    static final void a(Throwable th, String str) {
        int i = 0;
        if (ab.mContext != null) {
            Throwable th2 = th;
            while (th2.getCause() != null) {
                th2 = th2.getCause();
            }
            StackTraceElement[] stackTrace = th2.getStackTrace();
            StringBuilder sb = new StringBuilder();
            sb.append(th2.getClass().getName()).append(":");
            String packageName = ab.mContext.getPackageName();
            int i2 = 0;
            while (i2 < 3 && i < stackTrace.length) {
                String className = stackTrace[i].getClassName();
                if ((!className.startsWith("java.") || packageName.startsWith("java.")) && ((!className.startsWith("javax.") || packageName.startsWith("javax.")) && ((!className.startsWith("android.") || packageName.startsWith("android.")) && (!className.startsWith("com.android.") || packageName.startsWith("com.android."))))) {
                    sb.append(stackTrace[i].toString()).append(":");
                    i2++;
                }
                i++;
            }
            long currentTimeMillis = str.trim().isEmpty() ? System.currentTimeMillis() : Long.valueOf(str).longValue();
            gd.b.a(1).a();
            gd.b.a(1).a(currentTimeMillis, a(th));
            gd.b.a(1).b();
            ew.d(System.currentTimeMillis());
        }
    }

    private static final void b() {
        Thread.setDefaultUncaughtExceptionHandler(new a());
    }

    /* access modifiers changed from: package-private */
    public void a(HashMap hashMap) {
        try {
            if (!hashMap.containsKey("throwable")) {
                return;
            }
            if (hashMap.containsKey("occurTime")) {
                a((Throwable) hashMap.get("throwable"), String.valueOf(hashMap.get("occurTime")));
            } else {
                a((Throwable) hashMap.get("throwable"), "");
            }
        } catch (Throwable th) {
        }
    }

    public final void onTDEBEventError(gd.a aVar) {
        if (aVar != null && aVar.a != null && Integer.parseInt(String.valueOf(aVar.a.get("apiType"))) == 5) {
            aVar.a.put("controller", a());
            a(aVar.a);
        }
    }
}
