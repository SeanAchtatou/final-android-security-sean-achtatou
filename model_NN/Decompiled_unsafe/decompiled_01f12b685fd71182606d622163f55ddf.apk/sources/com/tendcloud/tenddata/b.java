package com.tendcloud.tenddata;

import android.annotation.SuppressLint;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLEngineResult;
import javax.net.ssl.SSLSession;

@SuppressLint({"Assert"})
public class b implements i, ByteChannel {
    protected static ByteBuffer a = ByteBuffer.allocate(0);
    static final /* synthetic */ boolean m = (!b.class.desiredAssertionStatus());
    protected ExecutorService b;
    protected List c;
    protected ByteBuffer d;
    protected ByteBuffer e;
    protected ByteBuffer f;
    protected SocketChannel g;
    protected SelectionKey h;
    protected SSLEngine i;
    protected SSLEngineResult j;
    protected SSLEngineResult k;
    protected int l = 0;

    public b(SocketChannel socketChannel, SSLEngine sSLEngine, ExecutorService executorService, SelectionKey selectionKey) {
        if (socketChannel == null || sSLEngine == null || executorService == null) {
            throw new IllegalArgumentException("parameter must not be null");
        }
        this.g = socketChannel;
        this.i = sSLEngine;
        this.b = executorService;
        SSLEngineResult sSLEngineResult = new SSLEngineResult(SSLEngineResult.Status.BUFFER_UNDERFLOW, sSLEngine.getHandshakeStatus(), 0, 0);
        this.k = sSLEngineResult;
        this.j = sSLEngineResult;
        this.c = new ArrayList(3);
        if (selectionKey != null) {
            selectionKey.interestOps(selectionKey.interestOps() | 4);
            this.h = selectionKey;
        }
        a(sSLEngine.getSession());
        this.g.write(b(a));
        j();
    }

    private int a(ByteBuffer byteBuffer, ByteBuffer byteBuffer2) {
        int remaining = byteBuffer.remaining();
        int remaining2 = byteBuffer2.remaining();
        if (remaining > remaining2) {
            remaining = Math.min(remaining, remaining2);
            for (int i2 = 0; i2 < remaining; i2++) {
                byteBuffer2.put(byteBuffer.get());
            }
        } else {
            byteBuffer2.put(byteBuffer);
        }
        return remaining;
    }

    private void a(Future future) {
        boolean z = false;
        while (true) {
            try {
                future.get();
                break;
            } catch (InterruptedException e2) {
                z = true;
            }
        }
        if (z) {
            try {
                Thread.currentThread().interrupt();
            } catch (ExecutionException e3) {
                throw new RuntimeException(e3);
            }
        }
    }

    private synchronized ByteBuffer b(ByteBuffer byteBuffer) {
        this.e.compact();
        this.k = this.i.wrap(byteBuffer, this.e);
        this.e.flip();
        return this.e;
    }

    private int c(ByteBuffer byteBuffer) {
        if (this.d.hasRemaining()) {
            return a(this.d, byteBuffer);
        }
        if (!this.d.hasRemaining()) {
            this.d.clear();
        }
        if (this.f.hasRemaining()) {
            k();
            int a2 = a(this.d, byteBuffer);
            if (a2 <= 0) {
                return 0;
            }
            return a2;
        }
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0038, code lost:
        if (d() == false) goto L_0x000b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003a, code lost:
        a(r0);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void j() {
        /*
            r3 = this;
            monitor-enter(r3)
            javax.net.ssl.SSLEngine r0 = r3.i     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult$HandshakeStatus r0 = r0.getHandshakeStatus()     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult$HandshakeStatus r1 = javax.net.ssl.SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING     // Catch:{ all -> 0x0031 }
            if (r0 != r1) goto L_0x000d
        L_0x000b:
            monitor-exit(r3)
            return
        L_0x000d:
            java.util.List r0 = r3.c     // Catch:{ all -> 0x0031 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0031 }
            if (r0 != 0) goto L_0x003e
            java.util.List r0 = r3.c     // Catch:{ all -> 0x0031 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x0031 }
        L_0x001b:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x003e
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0031 }
            java.util.concurrent.Future r0 = (java.util.concurrent.Future) r0     // Catch:{ all -> 0x0031 }
            boolean r2 = r0.isDone()     // Catch:{ all -> 0x0031 }
            if (r2 == 0) goto L_0x0034
            r1.remove()     // Catch:{ all -> 0x0031 }
            goto L_0x001b
        L_0x0031:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x0034:
            boolean r1 = r3.d()     // Catch:{ all -> 0x0031 }
            if (r1 == 0) goto L_0x000b
            r3.a(r0)     // Catch:{ all -> 0x0031 }
            goto L_0x000b
        L_0x003e:
            javax.net.ssl.SSLEngine r0 = r3.i     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult$HandshakeStatus r0 = r0.getHandshakeStatus()     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult$HandshakeStatus r1 = javax.net.ssl.SSLEngineResult.HandshakeStatus.NEED_UNWRAP     // Catch:{ all -> 0x0031 }
            if (r0 != r1) goto L_0x0092
            boolean r0 = r3.d()     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x0058
            javax.net.ssl.SSLEngineResult r0 = r3.j     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult$Status r0 = r0.getStatus()     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult$Status r1 = javax.net.ssl.SSLEngineResult.Status.BUFFER_UNDERFLOW     // Catch:{ all -> 0x0031 }
            if (r0 != r1) goto L_0x0075
        L_0x0058:
            java.nio.ByteBuffer r0 = r3.f     // Catch:{ all -> 0x0031 }
            r0.compact()     // Catch:{ all -> 0x0031 }
            java.nio.channels.SocketChannel r0 = r3.g     // Catch:{ all -> 0x0031 }
            java.nio.ByteBuffer r1 = r3.f     // Catch:{ all -> 0x0031 }
            int r0 = r0.read(r1)     // Catch:{ all -> 0x0031 }
            r1 = -1
            if (r0 != r1) goto L_0x0070
            java.io.IOException r0 = new java.io.IOException     // Catch:{ all -> 0x0031 }
            java.lang.String r1 = "connection closed unexpectedly by peer"
            r0.<init>(r1)     // Catch:{ all -> 0x0031 }
            throw r0     // Catch:{ all -> 0x0031 }
        L_0x0070:
            java.nio.ByteBuffer r0 = r3.f     // Catch:{ all -> 0x0031 }
            r0.flip()     // Catch:{ all -> 0x0031 }
        L_0x0075:
            java.nio.ByteBuffer r0 = r3.d     // Catch:{ all -> 0x0031 }
            r0.compact()     // Catch:{ all -> 0x0031 }
            r3.k()     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult r0 = r3.j     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult$HandshakeStatus r0 = r0.getHandshakeStatus()     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult$HandshakeStatus r1 = javax.net.ssl.SSLEngineResult.HandshakeStatus.FINISHED     // Catch:{ all -> 0x0031 }
            if (r0 != r1) goto L_0x0092
            javax.net.ssl.SSLEngine r0 = r3.i     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLSession r0 = r0.getSession()     // Catch:{ all -> 0x0031 }
            r3.a(r0)     // Catch:{ all -> 0x0031 }
            goto L_0x000b
        L_0x0092:
            r3.e()     // Catch:{ all -> 0x0031 }
            java.util.List r0 = r3.c     // Catch:{ all -> 0x0031 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0031 }
            if (r0 != 0) goto L_0x00a7
            javax.net.ssl.SSLEngine r0 = r3.i     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult$HandshakeStatus r0 = r0.getHandshakeStatus()     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult$HandshakeStatus r1 = javax.net.ssl.SSLEngineResult.HandshakeStatus.NEED_WRAP     // Catch:{ all -> 0x0031 }
            if (r0 != r1) goto L_0x00c7
        L_0x00a7:
            java.nio.channels.SocketChannel r0 = r3.g     // Catch:{ all -> 0x0031 }
            java.nio.ByteBuffer r1 = com.tendcloud.tenddata.b.a     // Catch:{ all -> 0x0031 }
            java.nio.ByteBuffer r1 = r3.b(r1)     // Catch:{ all -> 0x0031 }
            r0.write(r1)     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult r0 = r3.k     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult$HandshakeStatus r0 = r0.getHandshakeStatus()     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult$HandshakeStatus r1 = javax.net.ssl.SSLEngineResult.HandshakeStatus.FINISHED     // Catch:{ all -> 0x0031 }
            if (r0 != r1) goto L_0x00c7
            javax.net.ssl.SSLEngine r0 = r3.i     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLSession r0 = r0.getSession()     // Catch:{ all -> 0x0031 }
            r3.a(r0)     // Catch:{ all -> 0x0031 }
            goto L_0x000b
        L_0x00c7:
            boolean r0 = com.tendcloud.tenddata.b.m     // Catch:{ all -> 0x0031 }
            if (r0 != 0) goto L_0x00db
            javax.net.ssl.SSLEngine r0 = r3.i     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult$HandshakeStatus r0 = r0.getHandshakeStatus()     // Catch:{ all -> 0x0031 }
            javax.net.ssl.SSLEngineResult$HandshakeStatus r1 = javax.net.ssl.SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING     // Catch:{ all -> 0x0031 }
            if (r0 != r1) goto L_0x00db
            java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ all -> 0x0031 }
            r0.<init>()     // Catch:{ all -> 0x0031 }
            throw r0     // Catch:{ all -> 0x0031 }
        L_0x00db:
            r0 = 1
            r3.l = r0     // Catch:{ all -> 0x0031 }
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.b.j():void");
    }

    private synchronized ByteBuffer k() {
        while (true) {
            int remaining = this.d.remaining();
            this.j = this.i.unwrap(this.f, this.d);
            if (this.j.getStatus() != SSLEngineResult.Status.OK || (remaining == this.d.remaining() && this.i.getHandshakeStatus() != SSLEngineResult.HandshakeStatus.NEED_UNWRAP)) {
                this.d.flip();
            }
        }
        this.d.flip();
        return this.d;
    }

    private boolean l() {
        SSLEngineResult.HandshakeStatus handshakeStatus = this.i.getHandshakeStatus();
        return handshakeStatus == SSLEngineResult.HandshakeStatus.FINISHED || handshakeStatus == SSLEngineResult.HandshakeStatus.NOT_HANDSHAKING;
    }

    public int a(ByteBuffer byteBuffer) {
        return c(byteBuffer);
    }

    public SelectableChannel a(boolean z) {
        return this.g.configureBlocking(z);
    }

    /* access modifiers changed from: protected */
    public void a(SSLSession sSLSession) {
        int applicationBufferSize = sSLSession.getApplicationBufferSize();
        int packetBufferSize = sSLSession.getPacketBufferSize();
        if (this.d == null) {
            this.d = ByteBuffer.allocate(applicationBufferSize);
            this.e = ByteBuffer.allocate(packetBufferSize);
            this.f = ByteBuffer.allocate(packetBufferSize);
        } else {
            if (this.d.capacity() != applicationBufferSize) {
                this.d = ByteBuffer.allocate(applicationBufferSize);
            }
            if (this.e.capacity() != packetBufferSize) {
                this.e = ByteBuffer.allocate(packetBufferSize);
            }
            if (this.f.capacity() != packetBufferSize) {
                this.f = ByteBuffer.allocate(packetBufferSize);
            }
        }
        this.d.rewind();
        this.d.flip();
        this.f.rewind();
        this.f.flip();
        this.e.rewind();
        this.e.flip();
        this.l++;
    }

    public boolean a() {
        return this.e.hasRemaining() || !l();
    }

    public boolean a(SocketAddress socketAddress) {
        return this.g.connect(socketAddress);
    }

    public void b() {
        write(this.e);
    }

    public boolean c() {
        return this.d.hasRemaining() || !(!this.f.hasRemaining() || this.j.getStatus() == SSLEngineResult.Status.BUFFER_UNDERFLOW || this.j.getStatus() == SSLEngineResult.Status.CLOSED);
    }

    public void close() {
        this.i.closeOutbound();
        this.i.getSession().invalidate();
        if (this.g.isOpen()) {
            this.g.write(b(a));
        }
        this.g.close();
        this.b.shutdownNow();
    }

    public boolean d() {
        return this.g.isBlocking();
    }

    /* access modifiers changed from: protected */
    public void e() {
        while (true) {
            Runnable delegatedTask = this.i.getDelegatedTask();
            if (delegatedTask != null) {
                this.c.add(this.b.submit(delegatedTask));
            } else {
                return;
            }
        }
    }

    public boolean f() {
        return this.g.isConnected();
    }

    public boolean g() {
        return this.g.finishConnect();
    }

    public Socket h() {
        return this.g.socket();
    }

    public boolean i() {
        return this.i.isInboundDone();
    }

    public boolean isOpen() {
        return this.g.isOpen();
    }

    public int read(ByteBuffer byteBuffer) {
        if (!byteBuffer.hasRemaining()) {
            return 0;
        }
        if (!l()) {
            if (d()) {
                while (!l()) {
                    j();
                }
            } else {
                j();
                if (!l()) {
                    return 0;
                }
            }
        }
        if (this.l <= 1) {
            a(this.i.getSession());
        }
        int c2 = c(byteBuffer);
        if (c2 != 0) {
            return c2;
        }
        if (m || this.d.position() == 0) {
            this.d.clear();
            if (!this.f.hasRemaining()) {
                this.f.clear();
            } else {
                this.f.compact();
            }
            if ((d() || this.j.getStatus() == SSLEngineResult.Status.BUFFER_UNDERFLOW) && this.g.read(this.f) == -1) {
                return -1;
            }
            this.f.flip();
            k();
            int a2 = a(this.d, byteBuffer);
            return (a2 != 0 || !d()) ? a2 : read(byteBuffer);
        }
        throw new AssertionError();
    }

    public int write(ByteBuffer byteBuffer) {
        if (!l()) {
            j();
            return 0;
        }
        if (this.l <= 1) {
            a(this.i.getSession());
        }
        return this.g.write(b(byteBuffer));
    }
}
