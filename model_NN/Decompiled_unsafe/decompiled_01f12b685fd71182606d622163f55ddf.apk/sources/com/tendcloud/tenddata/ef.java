package com.tendcloud.tenddata;

import android.R;
import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

abstract class ef {
    private static final String d = "TD.ResReader";
    private final Context a;
    private final Map b = new HashMap();
    private final SparseArray c = new SparseArray();

    static class a extends ef {
        private final String a;

        protected a(String str, Context context) {
            super(context);
            this.a = str;
            b();
        }

        /* access modifiers changed from: protected */
        public Class a() {
            return R.drawable.class;
        }

        /* access modifiers changed from: protected */
        public String a(Context context) {
            return this.a + ".R$drawable";
        }
    }

    static class b extends ef {
        private final String a;

        public b(String str, Context context) {
            super(context);
            this.a = str;
            b();
        }

        /* access modifiers changed from: protected */
        public Class a() {
            return R.id.class;
        }

        /* access modifiers changed from: protected */
        public String a(Context context) {
            return this.a + ".R$id";
        }
    }

    protected ef(Context context) {
        this.a = context;
    }

    private static void a(Class cls, String str, Map map) {
        try {
            Field[] fields = cls.getFields();
            for (Field field : fields) {
                if (Modifier.isStatic(field.getModifiers()) && field.getType() == Integer.TYPE) {
                    String name = field.getName();
                    int i = field.getInt(null);
                    if (str != null) {
                        name = str + ":" + name;
                    }
                    map.put(name, Integer.valueOf(i));
                }
            }
        } catch (IllegalAccessException e) {
        }
    }

    /* access modifiers changed from: protected */
    public abstract Class a();

    /* access modifiers changed from: package-private */
    public String a(int i) {
        return (String) this.c.get(i);
    }

    /* access modifiers changed from: protected */
    public abstract String a(Context context);

    /* access modifiers changed from: package-private */
    public boolean a(String str) {
        return this.b.containsKey(str);
    }

    /* access modifiers changed from: package-private */
    public int b(String str) {
        return ((Integer) this.b.get(str)).intValue();
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.b.clear();
        this.c.clear();
        a(a(), "android", this.b);
        String a2 = a(this.a);
        try {
            a(Class.forName(a2), null, this.b);
        } catch (ClassNotFoundException e) {
            Log.w(d, "Class not found from '" + a2);
        }
        for (Map.Entry entry : this.b.entrySet()) {
            this.c.put(((Integer) entry.getValue()).intValue(), entry.getKey());
        }
    }
}
