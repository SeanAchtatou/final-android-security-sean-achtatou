package com.tendcloud.tenddata;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import com.datalab.tools.Constant;
import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import org.json.JSONArray;
import org.json.JSONObject;

final class gd {
    static final boolean a = false;
    static volatile boolean b = false;
    static volatile boolean c = false;
    static boolean d = false;
    static em e;
    static boolean f = true;
    private static boolean g = false;
    private static final HandlerThread h = new HandlerThread("PauseEventThread");
    private static Handler i;
    private static Random j = new Random();

    static class a {
        HashMap a = new HashMap();

        a() {
        }
    }

    static class b {
        b() {
        }

        static el a(int i) {
            switch (i) {
                case 1:
                    return ez.f();
                default:
                    return null;
            }
        }
    }

    static {
        i = null;
        h.start();
        i = new ge(h.getLooper());
    }

    gd() {
    }

    private static long a(String str) {
        fa faVar = new fa();
        if (faVar.a(str, 5000)) {
            return ((faVar.a() + SystemClock.elapsedRealtime()) - faVar.b()) - System.currentTimeMillis();
        }
        return -1;
    }

    static String a(Context context) {
        if (context != null && !b) {
            a(context, ab.getAppId(context), ab.getPartnerId(context));
        }
        return bm.a(context);
    }

    private static String a(Bundle bundle, String str) {
        if (bundle != null) {
            for (String equalsIgnoreCase : bundle.keySet()) {
                if (equalsIgnoreCase.equalsIgnoreCase(str)) {
                    return String.valueOf(bundle.get(str));
                }
            }
        }
        return null;
    }

    public static void a() {
        String str = null;
        try {
            int i2 = Calendar.getInstance().get(11);
            int i3 = 950;
            if (i2 >= 1 && i2 <= 6) {
                i3 = 200;
            }
            int nextInt = j.nextInt(1000);
            if (nextInt <= i3) {
                TreeMap treeMap = new TreeMap();
                treeMap.put("loc", bz.b(ab.mContext));
                if (f) {
                    JSONArray r = br.r(ab.mContext);
                    if (r != null) {
                        str = r.toString();
                    }
                    treeMap.put("net", str);
                }
                if (nextInt % 2 == 0) {
                    Long[][] e2 = bz.e(ab.mContext);
                    treeMap.put("ruas", Arrays.toString(e2[0]));
                    treeMap.put("ras", Arrays.toString(e2[1]));
                }
                TCAgent.onEvent(ab.mContext, "__tx.env", null, treeMap);
            }
        } catch (Throwable th) {
        }
    }

    static void a(double d2, double d3, String str) {
        try {
            if (TCAgent.LOG_ON) {
                ev.a("setLocation being called! latitude: " + d2 + " longitude: " + d3 + " location Provider : " + str);
            }
            if (!ca.b(str)) {
                TreeMap treeMap = new TreeMap();
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("lat", d2);
                jSONObject.put("lng", d3);
                jSONObject.put("ts", System.currentTimeMillis());
                jSONObject.put("provider", str);
                treeMap.put("loc", jSONObject.toString());
                TCAgent.onEvent(ab.mContext, "__tx.env", null, treeMap);
            } else if (TCAgent.LOG_ON) {
                ev.c("location Provider is null or empty, failed to setLocation ");
            }
        } catch (Throwable th) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.gd.a(android.app.Activity, java.lang.String, boolean):void
     arg types: [android.app.Activity, java.lang.String, int]
     candidates:
      com.tendcloud.tenddata.gd.a(double, double, java.lang.String):void
      com.tendcloud.tenddata.gd.a(android.app.Activity, java.lang.String, java.lang.String):void
      com.tendcloud.tenddata.gd.a(android.content.Context, java.lang.String, int):void
      com.tendcloud.tenddata.gd.a(android.content.Context, java.lang.String, java.lang.String):void
      com.tendcloud.tenddata.gd.a(android.app.Activity, java.lang.String, boolean):void */
    static void a(Activity activity) {
        a(activity, activity.getLocalClassName(), true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.gd.a(android.content.Context, java.lang.String, java.lang.String):void
     arg types: [android.app.Activity, java.lang.String, java.lang.String]
     candidates:
      com.tendcloud.tenddata.gd.a(double, double, java.lang.String):void
      com.tendcloud.tenddata.gd.a(android.app.Activity, java.lang.String, java.lang.String):void
      com.tendcloud.tenddata.gd.a(android.app.Activity, java.lang.String, boolean):void
      com.tendcloud.tenddata.gd.a(android.content.Context, java.lang.String, int):void
      com.tendcloud.tenddata.gd.a(android.content.Context, java.lang.String, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.gd.a(android.app.Activity, java.lang.String, boolean):void
     arg types: [android.app.Activity, java.lang.String, int]
     candidates:
      com.tendcloud.tenddata.gd.a(double, double, java.lang.String):void
      com.tendcloud.tenddata.gd.a(android.app.Activity, java.lang.String, java.lang.String):void
      com.tendcloud.tenddata.gd.a(android.content.Context, java.lang.String, int):void
      com.tendcloud.tenddata.gd.a(android.content.Context, java.lang.String, java.lang.String):void
      com.tendcloud.tenddata.gd.a(android.app.Activity, java.lang.String, boolean):void */
    static void a(Activity activity, String str, String str2) {
        if (!b) {
            a((Context) activity, str, str2);
        }
        a(activity, activity.getLocalClassName(), false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.gd.a(android.content.Context, java.lang.String, java.lang.String):void
     arg types: [android.app.Activity, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      com.tendcloud.tenddata.gd.a(double, double, java.lang.String):void
      com.tendcloud.tenddata.gd.a(android.app.Activity, java.lang.String, java.lang.String):void
      com.tendcloud.tenddata.gd.a(android.app.Activity, java.lang.String, boolean):void
      com.tendcloud.tenddata.gd.a(android.content.Context, java.lang.String, int):void
      com.tendcloud.tenddata.gd.a(android.content.Context, java.lang.String, java.lang.String):void */
    static void a(Activity activity, String str, boolean z) {
        if (!b) {
            a((Context) activity, (String) null, (String) null);
        }
        if (ew.i() != null && ew.i().equals(Constant.S_C)) {
            k();
        }
        ew.d("0");
        i.removeMessages(0);
        if (activity == null || (activity.getChangingConfigurations() & 128) != 128) {
            ca.execute(new gi(str, z, activity));
            return;
        }
        ev.a("Ignore page changing during screen switch");
        g = true;
    }

    static void a(Context context, String str) {
        if (g) {
            g = false;
            return;
        }
        ev.a("onPageStart being called! pageName: " + str);
        if (ca.b(str) && (context instanceof Activity)) {
            str = ((Activity) context).getLocalClassName();
        }
        a(context, str, 6);
    }

    private static void a(Context context, String str, int i2) {
        if (context != null && !b) {
            a(context, (String) null, (String) null);
        }
        ca.execute(new gk(i2, str));
    }

    static void a(Context context, String str, String str2) {
        if (context == null) {
            ev.a("Init failed Context is null");
            return;
        }
        System.currentTimeMillis();
        ab.mContext = context.getApplicationContext();
        ca.c = "TDLog";
        if (!b) {
            try {
                Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
                String a2 = a(bundle, "TD_APP_ID");
                String a3 = a(bundle, "TD_CHANNEL_ID");
                if (!ca.b(a2)) {
                    str = a2;
                }
                if (ca.b(a3)) {
                    a3 = str2;
                }
                String a4 = ca.a(context, "ChannelConfig.json");
                if (ca.b(a4)) {
                    a4 = a3;
                }
                if (ca.b(a4)) {
                    a4 = "Default";
                }
                if (ca.b(str)) {
                    ev.c("[SDKInit] TD AppId is null");
                } else if (!ca.b(context, "android.permission.INTERNET")) {
                    ev.c("[SDKInit] Permission \"android.permission.INTERNET\" is needed.");
                } else {
                    ab.a(str, a4);
                    try {
                        c(context);
                        ca.execute(new gg(context));
                        b = true;
                        ev.a("Analytics SDK Initializing...\n\tSDK_VERSION is: Android+TD+V2.2.46 gp\n\tApp ID is: " + ab.m + "\n\tApp Channel is: " + ab.u + "\n\tSDK_OVC is: " + "TDOVC+546b29b5db721edf46070d53eb03cc90+TalkingData");
                    } catch (Throwable th) {
                        ev.a("[SDKInit] Failed to initialize!", th);
                    }
                }
            } catch (Throwable th2) {
                ev.a("[SDKInit] Failed to initialize!", th2);
            }
        } else {
            gc.a();
            fl.a();
            fq.a();
            k();
        }
    }

    static void a(Context context, String str, String str2, Map map) {
        ca.execute(new gh(str, str2, map, context));
    }

    static void a(Context context, Throwable th) {
        ca.execute(new gl(th, context));
    }

    static void a(String str, long j2) {
        ew.e(str);
        ew.e(j2);
    }

    static void b(Activity activity) {
        b(activity, activity.getLocalClassName(), true);
    }

    static void b(Activity activity, String str, boolean z) {
        ew.d("1");
        i.removeMessages(0);
        i.sendEmptyMessageDelayed(0, ab.y);
        ca.execute(new gj(str, z));
    }

    static void b(Context context, String str) {
        ev.a("onPageEnd being called! pageName: " + str);
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            if (ca.b(str)) {
                str = activity.getLocalClassName();
            }
            if ((activity.getChangingConfigurations() & 128) == 128) {
                g = true;
                return;
            }
        }
        a(context, str, 7);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.ca.a(java.lang.Class, com.tendcloud.tenddata.bv, java.lang.String, java.lang.String):void
     arg types: [java.lang.Class<?>, com.tendcloud.tenddata.gm, java.lang.String, java.lang.String]
     candidates:
      com.tendcloud.tenddata.ca.a(java.lang.Object, com.tendcloud.tenddata.bv, java.lang.String, java.lang.String):void
      com.tendcloud.tenddata.ca.a(java.lang.Class, com.tendcloud.tenddata.bv, java.lang.String, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x006a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006b, code lost:
        com.tendcloud.tenddata.ev.c("registerActivityLifecycleListener " + r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:3:0x0009, B:15:0x005b] */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void c(android.content.Context r6) {
        /*
            r0 = 14
            boolean r0 = com.tendcloud.tenddata.ca.a(r0)
            if (r0 == 0) goto L_0x0054
            r0 = 0
            android.content.Context r1 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x0086 }
            boolean r1 = r1 instanceof android.app.Activity     // Catch:{ Throwable -> 0x0086 }
            if (r1 == 0) goto L_0x0049
            android.content.Context r0 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x0086 }
            android.app.Activity r0 = (android.app.Activity) r0     // Catch:{ Throwable -> 0x0086 }
            android.app.Application r0 = r0.getApplication()     // Catch:{ Throwable -> 0x0086 }
        L_0x0017:
            if (r0 == 0) goto L_0x0048
            boolean r1 = com.tendcloud.tenddata.gd.d     // Catch:{ Throwable -> 0x0086 }
            if (r1 != 0) goto L_0x0048
            java.lang.String r1 = "android.app.Application$ActivityLifecycleCallbacks"
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch:{ Throwable -> 0x0086 }
            java.lang.Class r2 = r0.getClass()     // Catch:{ Throwable -> 0x0086 }
            java.lang.String r3 = "registerActivityLifecycleCallbacks"
            r4 = 1
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x0086 }
            r5 = 0
            r4[r5] = r1     // Catch:{ Throwable -> 0x0086 }
            java.lang.reflect.Method r1 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x0086 }
            com.tendcloud.tenddata.em r2 = new com.tendcloud.tenddata.em     // Catch:{ Throwable -> 0x0086 }
            r2.<init>()     // Catch:{ Throwable -> 0x0086 }
            com.tendcloud.tenddata.gd.e = r2     // Catch:{ Throwable -> 0x0086 }
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0086 }
            r3 = 0
            com.tendcloud.tenddata.em r4 = com.tendcloud.tenddata.gd.e     // Catch:{ Throwable -> 0x0086 }
            r2[r3] = r4     // Catch:{ Throwable -> 0x0086 }
            r1.invoke(r0, r2)     // Catch:{ Throwable -> 0x0086 }
            r0 = 1
            com.tendcloud.tenddata.gd.d = r0     // Catch:{ Throwable -> 0x0086 }
        L_0x0048:
            return
        L_0x0049:
            android.content.Context r1 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x0086 }
            boolean r1 = r1 instanceof android.app.Application     // Catch:{ Throwable -> 0x0086 }
            if (r1 == 0) goto L_0x0017
            android.content.Context r0 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x0086 }
            android.app.Application r0 = (android.app.Application) r0     // Catch:{ Throwable -> 0x0086 }
            goto L_0x0017
        L_0x0054:
            com.tendcloud.tenddata.gm r0 = new com.tendcloud.tenddata.gm
            r0.<init>(r6)
            java.lang.String r1 = "android.app.ActivityManagerNative"
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch:{ Throwable -> 0x006a }
            java.lang.String r2 = "gDefault"
            java.lang.String r3 = "android.app.IActivityManager"
            com.tendcloud.tenddata.ca.a(r1, r0, r2, r3)     // Catch:{ Throwable -> 0x006a }
            r0 = 1
            com.tendcloud.tenddata.gd.d = r0     // Catch:{ Throwable -> 0x006a }
            goto L_0x0048
        L_0x006a:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "registerActivityLifecycleListener "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.tendcloud.tenddata.ev.c(r0)
            goto L_0x0048
        L_0x0086:
            r0 = move-exception
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.gd.c(android.content.Context):void");
    }

    /* access modifiers changed from: private */
    public static void d(Context context) {
        String a2 = ca.a();
        if (a2 != null) {
            TreeMap treeMap = new TreeMap();
            treeMap.put("sysproperty", a2);
            TCAgent.onEvent(context, "__tx.env", null, treeMap);
        }
        new Thread(new gn()).start();
    }

    /* access modifiers changed from: private */
    public static void i() {
        try {
            if (ew.i().equals("1")) {
                er.a().removeMessages(103);
                a aVar = new a();
                aVar.a.put("apiType", 3);
                aVar.a.put("occurTime", String.valueOf(System.currentTimeMillis()));
                aVar.a.put("isPageOrSession", false);
                aVar.a.put("sessionEnd", 1);
                Message.obtain(er.a(), 102, aVar).sendToTarget();
                ew.d(Constant.S_C);
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public static void j() {
        if (ew.g() == 0) {
            ew.b(System.currentTimeMillis());
        } else if (System.currentTimeMillis() - ew.g() > 86400000) {
            ca.e = true;
        }
    }

    /* access modifiers changed from: private */
    public static void k() {
        a aVar = new a();
        aVar.a.put("apiType", 1);
        aVar.a.put("controller", gc.a());
        Message obtain = Message.obtain();
        obtain.what = 102;
        obtain.obj = aVar;
        er.a().sendMessageDelayed(obtain, 100);
        m();
    }

    /* access modifiers changed from: private */
    public static boolean l() {
        try {
            Calendar instance = Calendar.getInstance();
            long j2 = (long) ((instance.get(6) * 1000) + instance.get(3));
            instance.setTimeInMillis(ew.g());
            return System.currentTimeMillis() - ew.g() < 5000 || Math.abs((((long) (instance.get(3) + (instance.get(6) * 1000))) / 1000) - (j2 / 1000)) == 1 || Math.abs((j2 % 100) - (bu.b(ab.mContext, ab.SETTINGS_PERF_FILE, "TD_sdk_last_send_time", 0) % 100)) >= 1;
        } catch (Throwable th) {
        }
    }

    private static void m() {
        JSONArray y = br.y(ab.mContext);
        if (y != null && y.length() == 2) {
            TreeMap treeMap = new TreeMap();
            treeMap.put("imeis", y.toString());
            JSONObject z = br.z(ab.mContext);
            treeMap.put("phoneNetworkInfo", z != null ? z.toString() : null);
            TCAgent.onEvent(ab.mContext, "__tx.env", null, treeMap);
        }
        String a2 = bm.a();
        if (a2 != null) {
            TreeMap treeMap2 = new TreeMap();
            treeMap2.put("SerialNo", a2);
            TCAgent.onEvent(ab.mContext, "__tx.env", null, treeMap2);
        }
    }

    /* access modifiers changed from: private */
    public static FileChannel n() {
        RandomAccessFile randomAccessFile;
        try {
            File file = new File(ab.mContext.getFilesDir(), "td.lock");
            if (!file.exists() && !file.createNewFile()) {
                return null;
            }
            randomAccessFile = new RandomAccessFile(file, "rw");
            try {
                return randomAccessFile.getChannel();
            } catch (Throwable th) {
                th = th;
            }
        } catch (Throwable th2) {
            th = th2;
            randomAccessFile = null;
            if (randomAccessFile != null) {
                try {
                    randomAccessFile.close();
                } catch (Exception e2) {
                    if (TCAgent.LOG_ON) {
                        e2.printStackTrace();
                    }
                }
            }
            if (!TCAgent.LOG_ON) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static void o() {
        new Thread(new gf()).start();
    }

    /* access modifiers changed from: private */
    public static void p() {
        long a2 = a("1.cn.pool.ntp.org");
        if (a2 != -1) {
            ew.f(a2);
            return;
        }
        long a3 = a("0.asia.pool.ntp.org");
        if (a3 != -1) {
            ew.f(a3);
            return;
        }
        long a4 = a("2.asia.pool.ntp.org");
        if (a4 != -1) {
            ew.f(a4);
        }
    }
}
