package com.tendcloud.tenddata;

import android.content.Context;
import android.os.Handler;
import java.nio.channels.FileChannel;
import java.util.Map;
import java.util.TreeMap;

public class ab {
    static final String A = "TD_APP_ID";
    static final String B = "TD_CHANNEL_ID";
    public static final boolean DEBUG = false;
    public static final String SETTINGS_PERF_ANALYTICS_APPID = "TD_appId";
    public static final String SETTINGS_PERF_FILE = "TD_app_pefercen_profile";
    public static final String SETTINGS_PREF_PUSH_APPID = "TD_push_appId";
    static final Map a = new TreeMap();
    static boolean b = false;
    static Handler c = null;
    static final int d = 1;
    static final String e = "Android+";
    static FileChannel f = null;
    static long g = 0;
    static boolean h = false;
    static boolean i = true;
    static boolean j = true;
    static final String k = "+V2.2.46 gp";
    static final String l = "TDLog";
    static String m = "";
    public static Context mContext = null;
    public static final String mPublishPrefix = "TD";
    static final String n = "TD_analytics_appId";
    static final String o = "TD_game_appId";
    static final String p = "TD_tracking_appId";
    static final String q = "TD_channelId";
    static final String r = "TD_sdk_last_send_time";
    static final String s = "isDeveloper";
    static final String t = "TDOVC+546b29b5db721edf46070d53eb03cc90+TalkingData";
    static String u = "Default";
    static final String v = "Android+TD+V2.2.46 gp";
    static long w = 0;
    static final int x = 120000;
    static long y = 30000;
    static final int z = 100;

    class a {
        static final int a = 1;
        static final int b = 2;

        a() {
        }
    }

    static class b {
        static final int a = 1;
        static final int b = 2;
        static final int c = 3;
        static final int d = 4;
        static final int e = 5;
        static final int f = 6;
        static final int g = 7;

        b() {
        }
    }

    class c {
        static final int a = 1;
        static final int b = 2;
        static final int c = 3;
        static final int d = -1;

        c() {
        }
    }

    static void a(String str, String str2) {
        if (mContext != null) {
            c = new Handler(mContext.getMainLooper());
        }
        if (str == null || str.trim().isEmpty() || !str.contains("-")) {
            m = str;
        } else {
            String str3 = null;
            try {
                str3 = str.split("-")[1];
            } catch (Throwable th) {
            }
            m = str3;
        }
        if (str2 != null && !str2.trim().isEmpty()) {
            u = str2;
        }
        ca.execute(new go());
    }

    static void a(boolean z2) {
        try {
            bu.a(mContext, SETTINGS_PERF_FILE, s, z2 ? 1 : 0);
        } catch (Throwable th) {
        }
    }

    static boolean a() {
        try {
            return bu.b(mContext, SETTINGS_PERF_FILE, s, 0) != 0;
        } catch (Throwable th) {
            return false;
        }
    }

    public static String getAdTrackingAppId() {
        return bu.b(mContext, SETTINGS_PERF_FILE, p, (String) null);
    }

    public static String getAppAnalyticsAppId() {
        return bu.b(mContext, SETTINGS_PERF_FILE, n, (String) null);
    }

    public static String getAppId(Context context) {
        if (ca.b(m) && context != null) {
            m = bu.b(context, SETTINGS_PERF_FILE, SETTINGS_PERF_ANALYTICS_APPID, "");
        }
        return m;
    }

    public static String getChannelId() {
        return bu.b(mContext, SETTINGS_PERF_FILE, q, (String) null);
    }

    public static String getGameAnalyticsAppId() {
        return bu.b(mContext, SETTINGS_PERF_FILE, o, (String) null);
    }

    public static String getPartnerId(Context context) {
        if (ca.b(u) || u.equals("Default")) {
            u = bu.b(context, SETTINGS_PERF_FILE, q, "");
        }
        return u;
    }

    public static String getPushAppId(Context context) {
        if (ca.b(m) && context != null) {
            m = bu.b(context, SETTINGS_PERF_FILE, SETTINGS_PREF_PUSH_APPID, "");
        }
        return m;
    }

    public static void setAdTrackingAppId(String str) {
        bu.a(mContext, SETTINGS_PERF_FILE, p, str);
    }

    public static void setAppAnalyticsAppId(String str) {
        bu.a(mContext, SETTINGS_PERF_FILE, n, str);
    }

    public static void setChannelId(String str) {
        bu.a(mContext, SETTINGS_PERF_FILE, q, str);
    }

    public static void setGameAnalyticsAppId(String str) {
        bu.a(mContext, SETTINGS_PERF_FILE, o, str);
    }
}
