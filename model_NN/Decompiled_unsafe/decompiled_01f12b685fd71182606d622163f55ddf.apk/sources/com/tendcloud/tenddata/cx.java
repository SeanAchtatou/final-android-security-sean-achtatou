package com.tendcloud.tenddata;

import android.content.Context;
import android.content.Intent;
import com.tendcloud.tenddata.ct;
import com.tendcloud.tenddata.dn;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import org.json.JSONObject;

class cx {
    public static long a = 270000;
    private static final String b = "PushLog";
    private static cx c;
    private InputStream d;
    private OutputStream e;
    private long f;
    private final String g;
    private final a h;
    private final Context i;

    public interface a {
        void onMsg(Intent intent);
    }

    private cx(String str, a aVar, Context context) {
        this.g = str;
        this.h = aVar;
        this.i = context;
    }

    public static synchronized cx a(String str, Context context, a aVar) {
        cx cxVar;
        synchronized (cx.class) {
            if (c == null) {
                c = new cx(str, aVar, context);
            }
            cxVar = c;
        }
        return cxVar;
    }

    private void a(dn.b bVar) {
        int i2 = 0;
        try {
            JSONObject jSONObject = new JSONObject(cv.a(bVar).d);
            JSONObject jSONObject2 = new JSONObject();
            String optString = jSONObject.optString(dc.V);
            ca.execute(new cy(this, optString));
            if (b(optString)) {
                jSONObject2.put("app", jSONObject.optString("app"));
                jSONObject2.put(dc.W, jSONObject.optString(dc.W));
                jSONObject2.put("content", jSONObject.optString("content"));
                jSONObject2.put(dc.ad, jSONObject.optString(dc.ad));
                if (!jSONObject.isNull(dc.Y)) {
                    jSONObject2.put(dc.Y, jSONObject.getJSONObject(dc.Y));
                    i2 = jSONObject.getJSONObject(dc.Y).getInt(dc.aa);
                }
                if (!jSONObject.isNull(dc.ac)) {
                    jSONObject2.put(dc.ac, jSONObject.getJSONObject(dc.ac));
                }
                Intent intent = new Intent();
                if (i2 == 0) {
                    intent.setAction(dc.O);
                    intent.putExtra(dc.v, jSONObject2.toString());
                } else {
                    intent.setAction(dc.N);
                    intent.putExtra(dc.u, jSONObject2.toString());
                }
                String str = bVar.e;
                if (str.equals(dc.E)) {
                    for (String str2 : cv.e(this.i)) {
                        intent.setPackage(str2);
                        this.h.onMsg(intent);
                    }
                    return;
                }
                intent.setPackage(str);
                this.h.onMsg(intent);
            }
        } catch (Throwable th) {
            cs.e("PushLog", th.getMessage());
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        try {
            if (this.e != null) {
                dn.a aVar = new dn.a();
                aVar.a = str;
                this.e.write(cv.a(cq.a(this.g, cv.a(aVar))));
                this.e.flush();
            }
        } catch (Throwable th) {
            cs.e("PushLog", th.getMessage());
        }
    }

    private void a(byte[] bArr) {
        if (bArr.length > 1) {
            switch (bArr[0]) {
                case 0:
                    dn.b bVar = (dn.b) cv.a(bArr, dn.b.class);
                    if (bVar != null) {
                        a(bVar);
                        return;
                    }
                    return;
                case 1:
                    this.f = ((dn.c) cv.a(bArr, dn.c.class)).a;
                    a();
                    return;
                default:
                    return;
            }
        }
    }

    private boolean b(String str) {
        boolean i2 = cr.i(str);
        if (!i2) {
            cr.h(str);
        }
        return !i2;
    }

    private void d() {
        this.e.write(cv.a(("aes,ack|||" + this.g).getBytes()));
        this.e.flush();
    }

    public void a() {
        try {
            if (this.e != null) {
                dn.c cVar = new dn.c();
                long j = this.f + 1;
                this.f = j;
                cVar.a = j;
                this.e.write(cv.a(cq.a(this.g, cv.a(cVar))));
                this.e.flush();
                return;
            }
            c();
        } catch (Throwable th) {
            c();
            cs.e("PushLog", th.getMessage());
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        DataInputStream dataInputStream = new DataInputStream(this.d);
        byte[] bArr = new byte[dataInputStream.readInt()];
        dataInputStream.readFully(bArr);
        a(cq.b(this.g, bArr));
    }

    public void c() {
        try {
            this.d.close();
            this.e.close();
        } catch (Throwable th) {
        }
        this.d = null;
        this.e = null;
    }

    public void connect(ct.a aVar) {
        c();
        this.f = 0;
        Socket socket = new Socket();
        socket.setSoTimeout(600000);
        socket.connect(new InetSocketAddress(aVar.a, aVar.b), 8000);
        this.d = socket.getInputStream();
        this.e = socket.getOutputStream();
        d();
    }
}
