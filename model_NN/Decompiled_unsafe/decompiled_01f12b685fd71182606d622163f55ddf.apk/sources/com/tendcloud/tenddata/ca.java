package com.tendcloud.tenddata;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import com.sg.GameSprites.GameSpriteType;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Proxy;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.InflaterInputStream;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import org.json.JSONObject;

public class ca {
    static final boolean a = false;
    public static boolean b = true;
    public static String c = "TDLog";
    public static boolean d = false;
    public static boolean e = false;
    static final /* synthetic */ boolean f = (!ca.class.desiredAssertionStatus());
    private static final String g = "UTF-8";
    private static String h = "ge";
    private static String i = dc.aa;
    private static String j = "rop";
    private static final ExecutorService k = Executors.newSingleThreadExecutor();
    private static final byte l = 61;
    private static final String m = "US-ASCII";
    private static final byte[] n = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, GameSpriteType.STATUS_PATROL, 101, GameSpriteType.STATUS_ATTACK1, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, GameSpriteType.f1TYPE_BOSS_, GameSpriteType.f0TYPE_BOSS_, GameSpriteType.f3TYPE_BOSS_2, 51, GameSpriteType.f56TYPE_ENEMY_, 53, 54, 55, 56, 57, 43, GameSpriteType.f2TYPE_BOSS_1};
    private static byte[] o = {1, 2, 3, 4, 5, 6, 7, 8};

    public static String a() {
        return null;
    }

    public static String a(Context context, String str) {
        try {
            InputStream open = context.getAssets().open(str);
            byte[] bArr = new byte[open.available()];
            open.read(bArr);
            open.close();
            return new JSONObject(new String(bArr)).getString("td_channel_id");
        } catch (Throwable th) {
            return null;
        }
    }

    private static String a(Bundle bundle, String str) {
        if (bundle == null) {
            return null;
        }
        for (String equalsIgnoreCase : bundle.keySet()) {
            if (equalsIgnoreCase.equalsIgnoreCase(str)) {
                return String.valueOf(bundle.get(str));
            }
        }
        return null;
    }

    public static final String a(String str) {
        return str.length() > 256 ? str.substring(0, 256) : str;
    }

    public static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : bArr) {
            byte b3 = b2 & 255;
            if (b3 < 16) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(b3));
        }
        return sb.toString();
    }

    public static String a(byte[] bArr, int i2, int i3) {
        byte[] b2 = b(bArr, i2, i3);
        try {
            return new String(b2, m);
        } catch (UnsupportedEncodingException e2) {
            return new String(b2);
        }
    }

    public static void a(Class cls, bv bvVar, String str, String str2) {
        Field declaredField = cls.getDeclaredField(str);
        declaredField.setAccessible(true);
        Object obj = declaredField.get(null);
        Class<?> cls2 = Class.forName(str2);
        cc ccVar = new cc(bvVar, obj);
        declaredField.set(null, Proxy.newProxyInstance(cls.getClass().getClassLoader(), new Class[]{cls2}, ccVar));
    }

    public static void a(Object obj, bv bvVar, String str, String str2) {
        Field declaredField = obj.getClass().getDeclaredField(str);
        declaredField.setAccessible(true);
        Object obj2 = declaredField.get(obj);
        Class<?> cls = Class.forName(str2);
        cb cbVar = new cb(bvVar, obj2);
        declaredField.set(obj, Proxy.newProxyInstance(obj.getClass().getClassLoader(), new Class[]{cls}, cbVar));
    }

    public static boolean a(int i2) {
        return Build.VERSION.SDK_INT >= i2;
    }

    public static boolean a(Context context) {
        return false;
    }

    private static byte[] a(byte[] bArr, int i2, int i3, byte[] bArr2, int i4) {
        int i5 = 0;
        byte[] bArr3 = n;
        int i6 = (i3 > 1 ? (bArr[i2 + 1] << 24) >>> 16 : 0) | (i3 > 0 ? (bArr[i2] << 24) >>> 8 : 0);
        if (i3 > 2) {
            i5 = (bArr[i2 + 2] << 24) >>> 24;
        }
        int i7 = i5 | i6;
        switch (i3) {
            case 1:
                bArr2[i4] = bArr3[i7 >>> 18];
                bArr2[i4 + 1] = bArr3[(i7 >>> 12) & 63];
                bArr2[i4 + 2] = 61;
                bArr2[i4 + 3] = 61;
                break;
            case 2:
                bArr2[i4] = bArr3[i7 >>> 18];
                bArr2[i4 + 1] = bArr3[(i7 >>> 12) & 63];
                bArr2[i4 + 2] = bArr3[(i7 >>> 6) & 63];
                bArr2[i4 + 3] = 61;
                break;
            case 3:
                bArr2[i4] = bArr3[i7 >>> 18];
                bArr2[i4 + 1] = bArr3[(i7 >>> 12) & 63];
                bArr2[i4 + 2] = bArr3[(i7 >>> 6) & 63];
                bArr2[i4 + 3] = bArr3[i7 & 63];
                break;
        }
        return bArr2;
    }

    public static byte[] a(byte[] bArr, byte[] bArr2) {
        SecureRandom secureRandom = new SecureRandom();
        SecretKey generateSecret = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(bArr));
        Cipher instance = Cipher.getInstance("DES/CBC/PKCS5Padding");
        instance.init(2, generateSecret, new IvParameterSpec(new byte[]{1, 2, 3, 4, 5, 6, 7, 8}), secureRandom);
        InflaterInputStream inflaterInputStream = new InflaterInputStream(new ByteArrayInputStream(instance.doFinal(bArr2)));
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inflaterInputStream.read();
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(read);
        }
    }

    public static byte[] a(int[] iArr, int[] iArr2) {
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr[i2] = ((iArr[i2] * iArr2[(iArr2.length - 1) - i2]) - (iArr[(iArr.length - 1) - i2] * iArr2[i2])) + "kiG9w0BAQUFADCBqjELMAkGA0JFSUpJTkcxEDAOBgNVBAcMB0JFSUpJTkcxFjAUBgNVB".charAt(i2);
            iArr2[i2] = ((iArr2[i2] * iArr[(iArr.length - 1) - i2]) + (iArr2[(iArr2.length - 1) - i2] * iArr[i2])) - "kiG9w0BAQUFADCBqjELMAkGA0JFSUpJTkcxEDAOBgNVBAcMB0JFSUpJTkcxFjAUBgNVB".charAt(("kiG9w0BAQUFADCBqjELMAkGA0JFSUpJTkcxEDAOBgNVBAcMB0JFSUpJTkcxFjAUBgNVB".length() - 1) - i2);
        }
        return (Arrays.toString(iArr) + Arrays.hashCode(iArr2)).getBytes();
    }

    public static String b(byte[] bArr) {
        String str = null;
        try {
            str = a(bArr, 0, bArr.length);
        } catch (IOException e2) {
            if (!f) {
                throw new AssertionError(e2.getMessage());
            }
        }
        if (f || str != null) {
            return str;
        }
        throw new AssertionError();
    }

    public static boolean b(Context context, String str) {
        try {
            return context.checkCallingOrSelfPermission(str) == 0;
        } catch (Throwable th) {
            return false;
        }
    }

    public static final boolean b(String str) {
        return str == null || "".equals(str.trim());
    }

    public static byte[] b(byte[] bArr, int i2, int i3) {
        if (bArr == null) {
            throw new NullPointerException("Cannot serialize a null array.");
        } else if (i2 < 0) {
            throw new IllegalArgumentException("Cannot have negative offset: " + i2);
        } else if (i3 < 0) {
            throw new IllegalArgumentException("Cannot have length offset: " + i3);
        } else if (i2 + i3 > bArr.length) {
            throw new IllegalArgumentException(String.format("Cannot have offset of %d and length of %d with array of length %d", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(bArr.length)));
        } else {
            byte[] bArr2 = new byte[((i3 % 3 > 0 ? 4 : 0) + ((i3 / 3) * 4))];
            int i4 = i3 - 2;
            int i5 = 0;
            int i6 = 0;
            while (i6 < i4) {
                a(bArr, i6 + i2, 3, bArr2, i5);
                i6 += 3;
                i5 += 4;
            }
            if (i6 < i3) {
                a(bArr, i6 + i2, i3 - i6, bArr2, i5);
                i5 += 4;
            }
            if (i5 > bArr2.length - 1) {
                return bArr2;
            }
            byte[] bArr3 = new byte[i5];
            System.arraycopy(bArr2, 0, bArr3, 0, i5);
            return bArr3;
        }
    }

    public static byte[] b(byte[] bArr, byte[] bArr2) {
        try {
            SecretKey generateSecret = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(bArr2));
            Cipher instance = Cipher.getInstance("DES/CBC/PKCS5Padding");
            instance.init(1, generateSecret, new IvParameterSpec(o));
            return instance.doFinal(bArr);
        } catch (Exception e2) {
            return null;
        }
    }

    public static String c(Context context, String str) {
        try {
            return a(context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData, str);
        } catch (PackageManager.NameNotFoundException e2) {
            return null;
        }
    }

    public static String c(String str) {
        try {
            return a(MessageDigest.getInstance("MD5").digest(str.getBytes(g)));
        } catch (Exception e2) {
            return null;
        }
    }

    public static byte[] c(byte[] bArr, byte[] bArr2) {
        try {
            SecretKey generateSecret = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(bArr2));
            Cipher instance = Cipher.getInstance("DES/CBC/PKCS5Padding");
            instance.init(2, generateSecret, new IvParameterSpec(o));
            return instance.doFinal(bArr);
        } catch (Exception e2) {
            return null;
        }
    }

    public static String d(String str) {
        if (str == null) {
            return null;
        }
        try {
            return a(MessageDigest.getInstance("SHA-256").digest(str.getBytes(g)));
        } catch (Exception e2) {
            return null;
        }
    }

    public static FileChannel d(Context context, String str) {
        RandomAccessFile randomAccessFile;
        try {
            File file = new File(context.getFilesDir(), str + "td.lock");
            if (!file.exists()) {
                file.createNewFile();
            }
            randomAccessFile = new RandomAccessFile(file, "rw");
            try {
                return randomAccessFile.getChannel();
            } catch (Throwable th) {
            }
        } catch (Throwable th2) {
            randomAccessFile = null;
            try {
                randomAccessFile.close();
                return null;
            } catch (Exception e2) {
                return null;
            }
        }
    }

    public static void execute(Runnable runnable) {
        k.execute(runnable);
    }
}
