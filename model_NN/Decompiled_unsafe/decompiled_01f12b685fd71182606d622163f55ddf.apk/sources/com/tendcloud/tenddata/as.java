package com.tendcloud.tenddata;

import android.annotation.SuppressLint;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@SuppressLint({"Assert"})
public abstract class as extends e implements Runnable {
    public static int a = Runtime.getRuntime().availableProcessors();
    static final /* synthetic */ boolean b = (!as.class.desiredAssertionStatus());
    private final Collection c;
    private final InetSocketAddress d;
    private ServerSocketChannel e;
    private Selector f;
    private List g;
    private Thread h;
    private volatile AtomicBoolean i;
    private List j;
    private List k;
    private BlockingQueue l;
    private int m;
    private AtomicInteger n;
    private a o;

    public interface a extends f {
        ByteChannel a(SocketChannel socketChannel, SelectionKey selectionKey);

        g b(e eVar, l lVar, Socket socket);

        g b(e eVar, List list, Socket socket);
    }

    public class b extends Thread {
        static final /* synthetic */ boolean a = (!as.class.desiredAssertionStatus());
        private BlockingQueue c = new LinkedBlockingQueue();

        public b() {
            setName("WebSocketWorker-" + getId());
            setUncaughtExceptionHandler(new at(this, as.this));
        }

        public void put(g gVar) {
            this.c.put(gVar);
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.tendcloud.tenddata.g} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r5 = this;
                r2 = 0
            L_0x0001:
                java.util.concurrent.BlockingQueue r1 = r5.c     // Catch:{ InterruptedException -> 0x001f, RuntimeException -> 0x002a }
                java.lang.Object r1 = r1.take()     // Catch:{ InterruptedException -> 0x001f, RuntimeException -> 0x002a }
                r0 = r1
                com.tendcloud.tenddata.g r0 = (com.tendcloud.tenddata.g) r0     // Catch:{ InterruptedException -> 0x001f, RuntimeException -> 0x002a }
                r2 = r0
                java.util.concurrent.BlockingQueue r1 = r2.i     // Catch:{ InterruptedException -> 0x001f, RuntimeException -> 0x002a }
                java.lang.Object r1 = r1.poll()     // Catch:{ InterruptedException -> 0x001f, RuntimeException -> 0x002a }
                java.nio.ByteBuffer r1 = (java.nio.ByteBuffer) r1     // Catch:{ InterruptedException -> 0x001f, RuntimeException -> 0x002a }
                boolean r3 = com.tendcloud.tenddata.as.b.a     // Catch:{ InterruptedException -> 0x001f, RuntimeException -> 0x002a }
                if (r3 != 0) goto L_0x0021
                if (r1 != 0) goto L_0x0021
                java.lang.AssertionError r1 = new java.lang.AssertionError     // Catch:{ InterruptedException -> 0x001f, RuntimeException -> 0x002a }
                r1.<init>()     // Catch:{ InterruptedException -> 0x001f, RuntimeException -> 0x002a }
                throw r1     // Catch:{ InterruptedException -> 0x001f, RuntimeException -> 0x002a }
            L_0x001f:
                r1 = move-exception
            L_0x0020:
                return
            L_0x0021:
                r2.decode(r1)     // Catch:{ all -> 0x0031 }
                com.tendcloud.tenddata.as r3 = com.tendcloud.tenddata.as.this     // Catch:{ InterruptedException -> 0x001f, RuntimeException -> 0x002a }
                r3.a(r1)     // Catch:{ InterruptedException -> 0x001f, RuntimeException -> 0x002a }
                goto L_0x0001
            L_0x002a:
                r1 = move-exception
                com.tendcloud.tenddata.as r3 = com.tendcloud.tenddata.as.this
                r3.c(r2, r1)
                goto L_0x0020
            L_0x0031:
                r3 = move-exception
                com.tendcloud.tenddata.as r4 = com.tendcloud.tenddata.as.this     // Catch:{ InterruptedException -> 0x001f, RuntimeException -> 0x002a }
                r4.a(r1)     // Catch:{ InterruptedException -> 0x001f, RuntimeException -> 0x002a }
                throw r3     // Catch:{ InterruptedException -> 0x001f, RuntimeException -> 0x002a }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.as.b.run():void");
        }
    }

    public as() {
        this(new InetSocketAddress(80), a, null);
    }

    public as(InetSocketAddress inetSocketAddress) {
        this(inetSocketAddress, a, null);
    }

    public as(InetSocketAddress inetSocketAddress, int i2) {
        this(inetSocketAddress, i2, null);
    }

    public as(InetSocketAddress inetSocketAddress, int i2, List list) {
        this(inetSocketAddress, i2, list, new HashSet());
    }

    public as(InetSocketAddress inetSocketAddress, int i2, List list, Collection collection) {
        this.i = new AtomicBoolean(false);
        this.m = 0;
        this.n = new AtomicInteger(0);
        this.o = new ar();
        if (inetSocketAddress == null || i2 < 1 || collection == null) {
            throw new IllegalArgumentException("address and connectionscontainer must not be null and you need at least 1 decoder");
        }
        if (list == null) {
            this.g = Collections.emptyList();
        } else {
            this.g = list;
        }
        this.d = inetSocketAddress;
        this.c = collection;
        this.k = new LinkedList();
        this.j = new ArrayList(i2);
        this.l = new LinkedBlockingQueue();
        for (int i3 = 0; i3 < i2; i3++) {
            b bVar = new b();
            this.j.add(bVar);
            bVar.start();
        }
    }

    public as(InetSocketAddress inetSocketAddress, List list) {
        this(inetSocketAddress, a, list);
    }

    private void a(g gVar) {
        if (gVar.j == null) {
            gVar.j = (b) this.j.get(this.m % this.j.size());
            this.m++;
        }
        gVar.j.put(gVar);
    }

    /* access modifiers changed from: private */
    public void a(ByteBuffer byteBuffer) {
        if (this.l.size() <= this.n.intValue()) {
            this.l.put(byteBuffer);
        }
    }

    private void a(SelectionKey selectionKey, d dVar, IOException iOException) {
        SelectableChannel channel;
        if (dVar != null) {
            dVar.b(y.f, iOException.getMessage());
        } else if (selectionKey != null && (channel = selectionKey.channel()) != null && channel.isOpen()) {
            try {
                channel.close();
            } catch (IOException e2) {
            }
            if (g.d) {
                System.out.println("Connection closed because of" + iOException);
            }
        }
    }

    /* access modifiers changed from: private */
    public void c(d dVar, Exception exc) {
        b(dVar, exc);
        try {
            b();
        } catch (IOException e2) {
            b((d) null, e2);
        } catch (InterruptedException e3) {
            Thread.currentThread().interrupt();
            b((d) null, e3);
        }
    }

    private Socket h(d dVar) {
        return ((SocketChannel) ((g) dVar).f.channel()).socket();
    }

    private ByteBuffer j() {
        return (ByteBuffer) this.l.take();
    }

    public ap a(d dVar, l lVar, af afVar) {
        return super.a(dVar, lVar, afVar);
    }

    public void a() {
        if (this.h != null) {
            throw new IllegalStateException(getClass().getName() + " can only be started once.");
        }
        new Thread(this).start();
    }

    public void a(d dVar, int i2, String str) {
        b(dVar, i2, str);
    }

    public final void a(d dVar, int i2, String str, boolean z) {
        this.f.wakeup();
        try {
            if (f(dVar)) {
                d(dVar, i2, str, z);
            }
            try {
            } catch (InterruptedException e2) {
                Thread.currentThread().interrupt();
            }
        } finally {
            try {
                e(dVar);
            } catch (InterruptedException e3) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @Deprecated
    public void a(d dVar, ad adVar) {
        d(dVar, adVar);
    }

    public final void a(d dVar, al alVar) {
        if (g(dVar)) {
            b(dVar, (af) alVar);
        }
    }

    public final void a(d dVar, Exception exc) {
        b(dVar, exc);
    }

    public final void a(d dVar, String str) {
        b(dVar, str);
    }

    public final void a(d dVar, ByteBuffer byteBuffer) {
        b(dVar, byteBuffer);
    }

    /* access modifiers changed from: protected */
    public boolean a(SelectionKey selectionKey) {
        return true;
    }

    public InetSocketAddress b(d dVar) {
        return (InetSocketAddress) h(dVar).getLocalSocketAddress();
    }

    public void b() {
        stop(0);
    }

    public void b(d dVar, int i2, String str) {
    }

    public void b(d dVar, int i2, String str, boolean z) {
        c(dVar, i2, str, z);
    }

    public abstract void b(d dVar, af afVar);

    public abstract void b(d dVar, Exception exc);

    public abstract void b(d dVar, String str);

    public void b(d dVar, ByteBuffer byteBuffer) {
    }

    public InetSocketAddress c(d dVar) {
        return (InetSocketAddress) h(dVar).getRemoteSocketAddress();
    }

    public Collection c() {
        return this.c;
    }

    public void c(d dVar, int i2, String str, boolean z) {
    }

    public InetSocketAddress d() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public void d(d dVar) {
        if (this.n.get() < (this.j.size() * 2) + 1) {
            this.n.incrementAndGet();
            this.l.put(g());
        }
    }

    public abstract void d(d dVar, int i2, String str, boolean z);

    public void d(d dVar, ad adVar) {
    }

    public int e() {
        int port = d().getPort();
        return (port != 0 || this.e == null) ? port : this.e.socket().getLocalPort();
    }

    /* access modifiers changed from: protected */
    public void e(d dVar) {
    }

    public List f() {
        return Collections.unmodifiableList(this.g);
    }

    /* access modifiers changed from: protected */
    public boolean f(d dVar) {
        boolean remove;
        synchronized (this.c) {
            remove = this.c.remove(dVar);
            if (!b && !remove) {
                throw new AssertionError();
            }
        }
        if (this.i.get() && this.c.size() == 0) {
            this.h.interrupt();
        }
        return remove;
    }

    public ByteBuffer g() {
        return ByteBuffer.allocate(g.c);
    }

    /* access modifiers changed from: protected */
    public boolean g(d dVar) {
        boolean add;
        if (!this.i.get()) {
            synchronized (this.c) {
                add = this.c.add(dVar);
                if (!b && !add) {
                    throw new AssertionError();
                }
            }
            return add;
        }
        dVar.close(y.b);
        return true;
    }

    /* access modifiers changed from: protected */
    public String h() {
        return "<cross-domain-policy><allow-access-from domain=\"*\" to-ports=\"" + e() + "\" /></cross-domain-policy>";
    }

    public final f i() {
        return this.o;
    }

    public final void onWriteDemand(d dVar) {
        g gVar = (g) dVar;
        try {
            gVar.f.interestOps(5);
        } catch (CancelledKeyException e2) {
            gVar.h.clear();
        }
        this.f.wakeup();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v82, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: com.tendcloud.tenddata.g} */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x01c9, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01ca, code lost:
        r4 = r2;
        r2 = r3;
        r3 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01cd, code lost:
        if (r3 != null) goto L_0x01cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:?, code lost:
        r3.cancel();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x01d2, code lost:
        a(r3, r4, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x01d7, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:?, code lost:
        c((com.tendcloud.tenddata.d) null, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x01de, code lost:
        if (r11.j != null) goto L_0x01e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x01e0, code lost:
        r3 = r11.j.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x01ea, code lost:
        if (r3.hasNext() != false) goto L_0x01ec;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x01ec, code lost:
        ((com.tendcloud.tenddata.as.b) r3.next()).interrupt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x01f8, code lost:
        if (r11.e != null) goto L_0x01fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:?, code lost:
        r11.e.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0201, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0202, code lost:
        b((com.tendcloud.tenddata.d) null, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x020a, code lost:
        if (r11.j != null) goto L_0x020c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x020c, code lost:
        r3 = r11.j.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x0216, code lost:
        if (r3.hasNext() != false) goto L_0x0218;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0218, code lost:
        ((com.tendcloud.tenddata.as.b) r3.next()).interrupt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0224, code lost:
        if (r11.e != null) goto L_0x0226;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:?, code lost:
        r11.e.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x022d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x022e, code lost:
        b((com.tendcloud.tenddata.d) null, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x0235, code lost:
        if (r11.j == null) goto L_0x024d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x0237, code lost:
        r3 = r11.j.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:0x0241, code lost:
        if (r3.hasNext() == false) goto L_0x024d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x0243, code lost:
        ((com.tendcloud.tenddata.as.b) r3.next()).interrupt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x024f, code lost:
        if (r11.e == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:?, code lost:
        r11.e.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:0x0258, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x0259, code lost:
        b((com.tendcloud.tenddata.d) null, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0260, code lost:
        if (r11.e != null) goto L_0x0262;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:?, code lost:
        r11.e.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x0269, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x026a, code lost:
        b((com.tendcloud.tenddata.d) null, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x026f, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0270, code lost:
        r3 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0273, code lost:
        if (r11.j != null) goto L_0x0275;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x0275, code lost:
        r4 = r11.j.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x027f, code lost:
        if (r4.hasNext() != false) goto L_0x0281;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x0281, code lost:
        ((com.tendcloud.tenddata.as.b) r4.next()).interrupt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003b, code lost:
        r11.h.setName("WebsocketSelector" + r11.h.getId());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x028d, code lost:
        if (r11.e != null) goto L_0x028f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:?, code lost:
        r11.e.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x0294, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x0295, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x0296, code lost:
        b((com.tendcloud.tenddata.d) null, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x029a, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x029b, code lost:
        r4 = null;
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x029f, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x02a2, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x02a3, code lost:
        r3 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x02a6, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x02a7, code lost:
        r10 = r4;
        r4 = r2;
        r2 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x02ac, code lost:
        r4 = r2;
        r6 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x02b0, code lost:
        r6 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r11.e = java.nio.channels.ServerSocketChannel.open();
        r11.e.configureBlocking(false);
        r2 = r11.e.socket();
        r2.setReceiveBufferSize(com.tendcloud.tenddata.g.c);
        r2.bind(r11.d);
        r11.f = java.nio.channels.Selector.open();
        r11.e.register(r11.f, r11.e.validOps());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:196:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:203:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:204:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x008e, code lost:
        if (r11.h.isInterrupted() != false) goto L_0x0233;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r11.f.select();
        r7 = r11.f.selectedKeys().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x009e, code lost:
        r4 = null;
        r6 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a5, code lost:
        if (r7.hasNext() == false) goto L_0x018c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a7, code lost:
        r3 = r7.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b3, code lost:
        if (r3.isValid() != false) goto L_0x00bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b5, code lost:
        r6 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b7, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b8, code lost:
        c((com.tendcloud.tenddata.d) null, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00c1, code lost:
        if (r3.isAcceptable() == false) goto L_0x00ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00c7, code lost:
        if (a(r3) != false) goto L_0x00ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00c9, code lost:
        r3.cancel();
        r6 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ce, code lost:
        r2 = r11.e.accept();
        r2.configureBlocking(false);
        r6 = r11.o.b(r11, r11.g, r2.socket());
        r6.f = r2.register(r11.f, 1, r6);
        r6.g = r11.o.a(r2, r6.f);
        r7.remove();
        d(r6);
        r6 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0103, code lost:
        if (r3.isReadable() == false) goto L_0x013f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0105, code lost:
        r4 = r3.attachment();
        r6 = j();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0117, code lost:
        if (com.tendcloud.tenddata.c.a(r6, r4, r4.g) == false) goto L_0x016d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x011d, code lost:
        if (r6.hasRemaining() == false) goto L_0x0161;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x011f, code lost:
        r4.i.put(r6);
        a(r4);
        r7.remove();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x012e, code lost:
        if ((r4.g instanceof com.tendcloud.tenddata.i) == false) goto L_0x013f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0138, code lost:
        if (((com.tendcloud.tenddata.i) r4.g).c() == false) goto L_0x013f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x013a, code lost:
        r11.k.add(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0143, code lost:
        if (r3.isWritable() == false) goto L_0x02b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0145, code lost:
        r2 = (com.tendcloud.tenddata.g) r3.attachment();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0151, code lost:
        if (com.tendcloud.tenddata.c.a(r2, r2.g) == false) goto L_0x02ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0157, code lost:
        if (r3.isValid() == false) goto L_0x02ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0159, code lost:
        r3.interestOps(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x015d, code lost:
        r4 = r2;
        r6 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0165, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0169, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        a(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0174, code lost:
        if (r11.j != null) goto L_0x0176;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0176, code lost:
        r3 = r11.j.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0180, code lost:
        if (r3.hasNext() != false) goto L_0x0182;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0182, code lost:
        ((com.tendcloud.tenddata.as.b) r3.next()).interrupt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0192, code lost:
        if (r11.k.isEmpty() != false) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0194, code lost:
        r2 = (com.tendcloud.tenddata.g) r11.k.remove(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:?, code lost:
        r3 = (com.tendcloud.tenddata.i) r2.g;
        r4 = j();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01a9, code lost:
        if (com.tendcloud.tenddata.c.a(r4, r2, r3) == false) goto L_0x01b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01ab, code lost:
        r11.k.add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01b4, code lost:
        if (r4.hasRemaining() == false) goto L_0x01c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01b6, code lost:
        r2.i.put(r4);
        a(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01be, code lost:
        r4 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01c0, code lost:
        a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01c4, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:?, code lost:
        a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01c8, code lost:
        throw r3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x01cf A[SYNTHETIC, Splitter:B:103:0x01cf] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0207 A[ExcHandler: InterruptedException (e java.lang.InterruptedException), Splitter:B:21:0x0090] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x016a A[ExcHandler: CancelledKeyException (e java.nio.channels.CancelledKeyException), Splitter:B:24:0x00a1] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0171 A[ExcHandler: ClosedByInterruptException (e java.nio.channels.ClosedByInterruptException), Splitter:B:21:0x0090] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r11 = this;
            r5 = 0
            monitor-enter(r11)
            java.lang.Thread r2 = r11.h     // Catch:{ all -> 0x0027 }
            if (r2 == 0) goto L_0x002a
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0027 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0027 }
            r3.<init>()     // Catch:{ all -> 0x0027 }
            java.lang.Class r4 = r11.getClass()     // Catch:{ all -> 0x0027 }
            java.lang.String r4 = r4.getName()     // Catch:{ all -> 0x0027 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0027 }
            java.lang.String r4 = " can only be started once."
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0027 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0027 }
            r2.<init>(r3)     // Catch:{ all -> 0x0027 }
            throw r2     // Catch:{ all -> 0x0027 }
        L_0x0027:
            r2 = move-exception
            monitor-exit(r11)     // Catch:{ all -> 0x0027 }
            throw r2
        L_0x002a:
            java.lang.Thread r2 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x0027 }
            r11.h = r2     // Catch:{ all -> 0x0027 }
            java.util.concurrent.atomic.AtomicBoolean r2 = r11.i     // Catch:{ all -> 0x0027 }
            boolean r2 = r2.get()     // Catch:{ all -> 0x0027 }
            if (r2 == 0) goto L_0x003a
            monitor-exit(r11)     // Catch:{ all -> 0x0027 }
        L_0x0039:
            return
        L_0x003a:
            monitor-exit(r11)     // Catch:{ all -> 0x0027 }
            java.lang.Thread r2 = r11.h
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "WebsocketSelector"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.Thread r4 = r11.h
            long r6 = r4.getId()
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r3 = r3.toString()
            r2.setName(r3)
            java.nio.channels.ServerSocketChannel r2 = java.nio.channels.ServerSocketChannel.open()     // Catch:{ IOException -> 0x00b7 }
            r11.e = r2     // Catch:{ IOException -> 0x00b7 }
            java.nio.channels.ServerSocketChannel r2 = r11.e     // Catch:{ IOException -> 0x00b7 }
            r3 = 0
            r2.configureBlocking(r3)     // Catch:{ IOException -> 0x00b7 }
            java.nio.channels.ServerSocketChannel r2 = r11.e     // Catch:{ IOException -> 0x00b7 }
            java.net.ServerSocket r2 = r2.socket()     // Catch:{ IOException -> 0x00b7 }
            int r3 = com.tendcloud.tenddata.g.c     // Catch:{ IOException -> 0x00b7 }
            r2.setReceiveBufferSize(r3)     // Catch:{ IOException -> 0x00b7 }
            java.net.InetSocketAddress r3 = r11.d     // Catch:{ IOException -> 0x00b7 }
            r2.bind(r3)     // Catch:{ IOException -> 0x00b7 }
            java.nio.channels.Selector r2 = java.nio.channels.Selector.open()     // Catch:{ IOException -> 0x00b7 }
            r11.f = r2     // Catch:{ IOException -> 0x00b7 }
            java.nio.channels.ServerSocketChannel r2 = r11.e     // Catch:{ IOException -> 0x00b7 }
            java.nio.channels.Selector r3 = r11.f     // Catch:{ IOException -> 0x00b7 }
            java.nio.channels.ServerSocketChannel r4 = r11.e     // Catch:{ IOException -> 0x00b7 }
            int r4 = r4.validOps()     // Catch:{ IOException -> 0x00b7 }
            r2.register(r3, r4)     // Catch:{ IOException -> 0x00b7 }
        L_0x0088:
            java.lang.Thread r2 = r11.h     // Catch:{ RuntimeException -> 0x01d7 }
            boolean r2 = r2.isInterrupted()     // Catch:{ RuntimeException -> 0x01d7 }
            if (r2 != 0) goto L_0x0233
            java.nio.channels.Selector r2 = r11.f     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029a, InterruptedException -> 0x0207 }
            r2.select()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029a, InterruptedException -> 0x0207 }
            java.nio.channels.Selector r2 = r11.f     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029a, InterruptedException -> 0x0207 }
            java.util.Set r2 = r2.selectedKeys()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029a, InterruptedException -> 0x0207 }
            java.util.Iterator r7 = r2.iterator()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029a, InterruptedException -> 0x0207 }
            r4 = r5
            r6 = r5
        L_0x00a1:
            boolean r2 = r7.hasNext()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x02a2, InterruptedException -> 0x0207 }
            if (r2 == 0) goto L_0x018c
            java.lang.Object r2 = r7.next()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x02a2, InterruptedException -> 0x0207 }
            r0 = r2
            java.nio.channels.SelectionKey r0 = (java.nio.channels.SelectionKey) r0     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x02a2, InterruptedException -> 0x0207 }
            r3 = r0
            boolean r2 = r3.isValid()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            if (r2 != 0) goto L_0x00bd
            r6 = r3
            goto L_0x00a1
        L_0x00b7:
            r2 = move-exception
            r11.c(r5, r2)
            goto L_0x0039
        L_0x00bd:
            boolean r2 = r3.isAcceptable()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            if (r2 == 0) goto L_0x00ff
            boolean r2 = r11.a(r3)     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            if (r2 != 0) goto L_0x00ce
            r3.cancel()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            r6 = r3
            goto L_0x00a1
        L_0x00ce:
            java.nio.channels.ServerSocketChannel r2 = r11.e     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            java.nio.channels.SocketChannel r2 = r2.accept()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            r6 = 0
            r2.configureBlocking(r6)     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            com.tendcloud.tenddata.as$a r6 = r11.o     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            java.util.List r8 = r11.g     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            java.net.Socket r9 = r2.socket()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            com.tendcloud.tenddata.g r6 = r6.b(r11, r8, r9)     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            java.nio.channels.Selector r8 = r11.f     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            r9 = 1
            java.nio.channels.SelectionKey r8 = r2.register(r8, r9, r6)     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            r6.f = r8     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            com.tendcloud.tenddata.as$a r8 = r11.o     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            java.nio.channels.SelectionKey r9 = r6.f     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            java.nio.channels.ByteChannel r2 = r8.a(r2, r9)     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            r6.g = r2     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            r7.remove()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            r11.d(r6)     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            r6 = r3
            goto L_0x00a1
        L_0x00ff:
            boolean r2 = r3.isReadable()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            if (r2 == 0) goto L_0x013f
            java.lang.Object r2 = r3.attachment()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            r0 = r2
            com.tendcloud.tenddata.g r0 = (com.tendcloud.tenddata.g) r0     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            r4 = r0
            java.nio.ByteBuffer r6 = r11.j()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            java.nio.channels.ByteChannel r2 = r4.g     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            boolean r2 = com.tendcloud.tenddata.c.a(r6, r4, r2)     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            if (r2 == 0) goto L_0x016d
            boolean r2 = r6.hasRemaining()     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            if (r2 == 0) goto L_0x0161
            java.util.concurrent.BlockingQueue r2 = r4.i     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            r2.put(r6)     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            r11.a(r4)     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            r7.remove()     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            java.nio.channels.ByteChannel r2 = r4.g     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            boolean r2 = r2 instanceof com.tendcloud.tenddata.i     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            if (r2 == 0) goto L_0x013f
            java.nio.channels.ByteChannel r2 = r4.g     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            com.tendcloud.tenddata.i r2 = (com.tendcloud.tenddata.i) r2     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            boolean r2 = r2.c()     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            if (r2 == 0) goto L_0x013f
            java.util.List r2 = r11.k     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            r2.add(r4)     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
        L_0x013f:
            boolean r2 = r3.isWritable()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            if (r2 == 0) goto L_0x02b0
            java.lang.Object r2 = r3.attachment()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            com.tendcloud.tenddata.g r2 = (com.tendcloud.tenddata.g) r2     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            java.nio.channels.ByteChannel r4 = r2.g     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x02a6, InterruptedException -> 0x0207 }
            boolean r4 = com.tendcloud.tenddata.c.a(r2, r4)     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x02a6, InterruptedException -> 0x0207 }
            if (r4 == 0) goto L_0x02ac
            boolean r4 = r3.isValid()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x02a6, InterruptedException -> 0x0207 }
            if (r4 == 0) goto L_0x02ac
            r4 = 1
            r3.interestOps(r4)     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x02a6, InterruptedException -> 0x0207 }
            r4 = r2
            r6 = r3
            goto L_0x00a1
        L_0x0161:
            r11.a(r6)     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            goto L_0x013f
        L_0x0165:
            r2 = move-exception
            r11.a(r6)     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
            throw r2     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x029f, InterruptedException -> 0x0207 }
        L_0x016a:
            r2 = move-exception
            goto L_0x0088
        L_0x016d:
            r11.a(r6)     // Catch:{ IOException -> 0x0165, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            goto L_0x013f
        L_0x0171:
            r2 = move-exception
            java.util.List r2 = r11.j
            if (r2 == 0) goto L_0x01f6
            java.util.List r2 = r11.j
            java.util.Iterator r3 = r2.iterator()
        L_0x017c:
            boolean r2 = r3.hasNext()
            if (r2 == 0) goto L_0x01f6
            java.lang.Object r2 = r3.next()
            com.tendcloud.tenddata.as$b r2 = (com.tendcloud.tenddata.as.b) r2
            r2.interrupt()
            goto L_0x017c
        L_0x018c:
            java.util.List r2 = r11.k     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x02a2, InterruptedException -> 0x0207 }
            boolean r2 = r2.isEmpty()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x02a2, InterruptedException -> 0x0207 }
            if (r2 != 0) goto L_0x0088
            java.util.List r2 = r11.k     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x02a2, InterruptedException -> 0x0207 }
            r3 = 0
            java.lang.Object r2 = r2.remove(r3)     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x02a2, InterruptedException -> 0x0207 }
            com.tendcloud.tenddata.g r2 = (com.tendcloud.tenddata.g) r2     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x02a2, InterruptedException -> 0x0207 }
            java.nio.channels.ByteChannel r3 = r2.g     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x01c9, InterruptedException -> 0x0207 }
            com.tendcloud.tenddata.i r3 = (com.tendcloud.tenddata.i) r3     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x01c9, InterruptedException -> 0x0207 }
            java.nio.ByteBuffer r4 = r11.j()     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x01c9, InterruptedException -> 0x0207 }
            boolean r3 = com.tendcloud.tenddata.c.a(r4, r2, r3)     // Catch:{ IOException -> 0x01c4, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            if (r3 == 0) goto L_0x01b0
            java.util.List r3 = r11.k     // Catch:{ IOException -> 0x01c4, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            r3.add(r2)     // Catch:{ IOException -> 0x01c4, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
        L_0x01b0:
            boolean r3 = r4.hasRemaining()     // Catch:{ IOException -> 0x01c4, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            if (r3 == 0) goto L_0x01c0
            java.util.concurrent.BlockingQueue r3 = r2.i     // Catch:{ IOException -> 0x01c4, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            r3.put(r4)     // Catch:{ IOException -> 0x01c4, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            r11.a(r2)     // Catch:{ IOException -> 0x01c4, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
        L_0x01be:
            r4 = r2
            goto L_0x018c
        L_0x01c0:
            r11.a(r4)     // Catch:{ IOException -> 0x01c4, CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, InterruptedException -> 0x0207 }
            goto L_0x01be
        L_0x01c4:
            r3 = move-exception
            r11.a(r4)     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x01c9, InterruptedException -> 0x0207 }
            throw r3     // Catch:{ CancelledKeyException -> 0x016a, ClosedByInterruptException -> 0x0171, IOException -> 0x01c9, InterruptedException -> 0x0207 }
        L_0x01c9:
            r3 = move-exception
            r4 = r2
            r2 = r3
            r3 = r6
        L_0x01cd:
            if (r3 == 0) goto L_0x01d2
            r3.cancel()     // Catch:{ RuntimeException -> 0x01d7 }
        L_0x01d2:
            r11.a(r3, r4, r2)     // Catch:{ RuntimeException -> 0x01d7 }
            goto L_0x0088
        L_0x01d7:
            r2 = move-exception
            r3 = 0
            r11.c(r3, r2)     // Catch:{ all -> 0x026f }
            java.util.List r2 = r11.j
            if (r2 == 0) goto L_0x025e
            java.util.List r2 = r11.j
            java.util.Iterator r3 = r2.iterator()
        L_0x01e6:
            boolean r2 = r3.hasNext()
            if (r2 == 0) goto L_0x025e
            java.lang.Object r2 = r3.next()
            com.tendcloud.tenddata.as$b r2 = (com.tendcloud.tenddata.as.b) r2
            r2.interrupt()
            goto L_0x01e6
        L_0x01f6:
            java.nio.channels.ServerSocketChannel r2 = r11.e
            if (r2 == 0) goto L_0x0039
            java.nio.channels.ServerSocketChannel r2 = r11.e     // Catch:{ IOException -> 0x0201 }
            r2.close()     // Catch:{ IOException -> 0x0201 }
            goto L_0x0039
        L_0x0201:
            r2 = move-exception
            r11.b(r5, r2)
            goto L_0x0039
        L_0x0207:
            r2 = move-exception
            java.util.List r2 = r11.j
            if (r2 == 0) goto L_0x0222
            java.util.List r2 = r11.j
            java.util.Iterator r3 = r2.iterator()
        L_0x0212:
            boolean r2 = r3.hasNext()
            if (r2 == 0) goto L_0x0222
            java.lang.Object r2 = r3.next()
            com.tendcloud.tenddata.as$b r2 = (com.tendcloud.tenddata.as.b) r2
            r2.interrupt()
            goto L_0x0212
        L_0x0222:
            java.nio.channels.ServerSocketChannel r2 = r11.e
            if (r2 == 0) goto L_0x0039
            java.nio.channels.ServerSocketChannel r2 = r11.e     // Catch:{ IOException -> 0x022d }
            r2.close()     // Catch:{ IOException -> 0x022d }
            goto L_0x0039
        L_0x022d:
            r2 = move-exception
            r11.b(r5, r2)
            goto L_0x0039
        L_0x0233:
            java.util.List r2 = r11.j
            if (r2 == 0) goto L_0x024d
            java.util.List r2 = r11.j
            java.util.Iterator r3 = r2.iterator()
        L_0x023d:
            boolean r2 = r3.hasNext()
            if (r2 == 0) goto L_0x024d
            java.lang.Object r2 = r3.next()
            com.tendcloud.tenddata.as$b r2 = (com.tendcloud.tenddata.as.b) r2
            r2.interrupt()
            goto L_0x023d
        L_0x024d:
            java.nio.channels.ServerSocketChannel r2 = r11.e
            if (r2 == 0) goto L_0x0039
            java.nio.channels.ServerSocketChannel r2 = r11.e     // Catch:{ IOException -> 0x0258 }
            r2.close()     // Catch:{ IOException -> 0x0258 }
            goto L_0x0039
        L_0x0258:
            r2 = move-exception
            r11.b(r5, r2)
            goto L_0x0039
        L_0x025e:
            java.nio.channels.ServerSocketChannel r2 = r11.e
            if (r2 == 0) goto L_0x0039
            java.nio.channels.ServerSocketChannel r2 = r11.e     // Catch:{ IOException -> 0x0269 }
            r2.close()     // Catch:{ IOException -> 0x0269 }
            goto L_0x0039
        L_0x0269:
            r2 = move-exception
            r11.b(r5, r2)
            goto L_0x0039
        L_0x026f:
            r2 = move-exception
            r3 = r2
            java.util.List r2 = r11.j
            if (r2 == 0) goto L_0x028b
            java.util.List r2 = r11.j
            java.util.Iterator r4 = r2.iterator()
        L_0x027b:
            boolean r2 = r4.hasNext()
            if (r2 == 0) goto L_0x028b
            java.lang.Object r2 = r4.next()
            com.tendcloud.tenddata.as$b r2 = (com.tendcloud.tenddata.as.b) r2
            r2.interrupt()
            goto L_0x027b
        L_0x028b:
            java.nio.channels.ServerSocketChannel r2 = r11.e
            if (r2 == 0) goto L_0x0294
            java.nio.channels.ServerSocketChannel r2 = r11.e     // Catch:{ IOException -> 0x0295 }
            r2.close()     // Catch:{ IOException -> 0x0295 }
        L_0x0294:
            throw r3
        L_0x0295:
            r2 = move-exception
            r11.b(r5, r2)
            goto L_0x0294
        L_0x029a:
            r2 = move-exception
            r4 = r5
            r3 = r5
            goto L_0x01cd
        L_0x029f:
            r2 = move-exception
            goto L_0x01cd
        L_0x02a2:
            r2 = move-exception
            r3 = r6
            goto L_0x01cd
        L_0x02a6:
            r4 = move-exception
            r10 = r4
            r4 = r2
            r2 = r10
            goto L_0x01cd
        L_0x02ac:
            r4 = r2
            r6 = r3
            goto L_0x00a1
        L_0x02b0:
            r6 = r3
            goto L_0x00a1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.as.run():void");
    }

    public final void setWebSocketFactory(a aVar) {
        this.o = aVar;
    }

    public void stop(int i2) {
        ArrayList<d> arrayList;
        if (this.i.compareAndSet(false, true)) {
            synchronized (this.c) {
                arrayList = new ArrayList<>(this.c);
            }
            for (d close : arrayList) {
                close.close(y.b);
            }
            synchronized (this) {
                if (this.h != null) {
                    if (Thread.currentThread() != this.h) {
                    }
                    if (this.h != Thread.currentThread()) {
                        if (arrayList.size() > 0) {
                            this.h.join((long) i2);
                        }
                        this.h.interrupt();
                        this.h.join();
                    }
                }
            }
        }
    }
}
