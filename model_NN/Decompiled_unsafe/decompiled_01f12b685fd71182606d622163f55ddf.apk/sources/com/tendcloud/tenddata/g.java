package com.tendcloud.tenddata;

import android.annotation.SuppressLint;
import com.tendcloud.tenddata.ad;
import com.tendcloud.tenddata.as;
import com.tendcloud.tenddata.d;
import com.tendcloud.tenddata.l;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@SuppressLint({"Assert"})
public class g implements d {
    public static int c = 16384;
    public static boolean d = false;
    public static final List e = new ArrayList(4);
    static final /* synthetic */ boolean k = (!g.class.desiredAssertionStatus());
    public SelectionKey f;
    public ByteChannel g;
    public final BlockingQueue h;
    public final BlockingQueue i;
    public volatile as.b j;
    private volatile boolean l;
    private d.a m;
    private final h n;
    private List o;
    private l p;
    private d.b q;
    private ad.a r;
    private ByteBuffer s;
    private af t;
    private String u;
    private Integer v;
    private Boolean w;
    private String x;

    static {
        e.add(new n());
        e.add(new m());
        e.add(new p());
        e.add(new o());
    }

    public g(h hVar, l lVar) {
        this.l = false;
        this.m = d.a.NOT_YET_CONNECTED;
        this.p = null;
        this.r = null;
        this.s = ByteBuffer.allocate(0);
        this.t = null;
        this.u = null;
        this.v = null;
        this.w = null;
        this.x = null;
        if (hVar == null || (lVar == null && this.q == d.b.SERVER)) {
            throw new IllegalArgumentException("parameters must not be null");
        }
        this.h = new LinkedBlockingQueue();
        this.i = new LinkedBlockingQueue();
        this.n = hVar;
        this.q = d.b.CLIENT;
        if (lVar != null) {
            this.p = lVar.c();
        }
    }

    @Deprecated
    public g(h hVar, l lVar, Socket socket) {
        this(hVar, lVar);
    }

    public g(h hVar, List list) {
        this(hVar, (l) null);
        this.q = d.b.SERVER;
        if (list == null || list.isEmpty()) {
            this.o = e;
        } else {
            this.o = list;
        }
    }

    @Deprecated
    public g(h hVar, List list, Socket socket) {
        this(hVar, list);
    }

    private void a(al alVar) {
        if (d) {
            System.out.println("open using draft: " + this.p.getClass().getSimpleName());
        }
        this.m = d.a.OPEN;
        try {
            this.n.a(this, alVar);
        } catch (RuntimeException e2) {
            this.n.a(this, e2);
        }
    }

    private void a(Collection collection) {
        if (!f()) {
            throw new x();
        }
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            sendFrame((ad) it.next());
        }
    }

    private void a(List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            d((ByteBuffer) it.next());
        }
    }

    private boolean a(ByteBuffer byteBuffer) {
        ByteBuffer byteBuffer2;
        if (this.s.capacity() == 0) {
            byteBuffer2 = byteBuffer;
        } else {
            if (this.s.remaining() < byteBuffer.remaining()) {
                ByteBuffer allocate = ByteBuffer.allocate(this.s.capacity() + byteBuffer.remaining());
                this.s.flip();
                allocate.put(this.s);
                this.s = allocate;
            }
            this.s.put(byteBuffer);
            this.s.flip();
            byteBuffer2 = this.s;
        }
        byteBuffer2.mark();
        try {
            if (this.p == null && c(byteBuffer2) == l.b.MATCHED) {
                try {
                    d(ByteBuffer.wrap(aw.a(this.n.a(this))));
                    a(-3, "");
                } catch (r e2) {
                    c(y.f, "remote peer closed connection before flashpolicy could be transmitted", true);
                }
                return false;
            }
            try {
                if (this.q != d.b.SERVER) {
                    if (this.q == d.b.CLIENT) {
                        this.p.setParseMode(this.q);
                        al d2 = this.p.d(byteBuffer2);
                        if (!(d2 instanceof an)) {
                            b(y.c, "wrong http function", false);
                            return false;
                        }
                        an anVar = (an) d2;
                        if (this.p.a(this.t, anVar) == l.b.MATCHED) {
                            try {
                                this.n.a(this, this.t, anVar);
                                a(anVar);
                                return true;
                            } catch (r e3) {
                                b(e3.a(), e3.getMessage(), false);
                                return false;
                            } catch (RuntimeException e4) {
                                this.n.a(this, e4);
                                b(-1, e4.getMessage(), false);
                                return false;
                            }
                        } else {
                            a((int) y.c, "draft " + this.p + " refuses handshake");
                        }
                    }
                    return false;
                } else if (this.p == null) {
                    for (l c2 : this.o) {
                        l c3 = c2.c();
                        try {
                            c3.setParseMode(this.q);
                            byteBuffer2.reset();
                            al d3 = c3.d(byteBuffer2);
                            if (!(d3 instanceof af)) {
                                b(y.c, "wrong http function", false);
                                return false;
                            }
                            af afVar = (af) d3;
                            if (c3.a(afVar) == l.b.MATCHED) {
                                this.x = afVar.a();
                                try {
                                    a(c3.a(c3.a(afVar, this.n.a(this, c3, afVar)), this.q));
                                    this.p = c3;
                                    a(afVar);
                                    return true;
                                } catch (r e5) {
                                    b(e5.a(), e5.getMessage(), false);
                                    return false;
                                } catch (RuntimeException e6) {
                                    this.n.a(this, e6);
                                    b(-1, e6.getMessage(), false);
                                    return false;
                                }
                            } else {
                                continue;
                            }
                        } catch (t e7) {
                        }
                    }
                    if (this.p == null) {
                        a((int) y.c, "no draft matches");
                    }
                    return false;
                } else {
                    al d4 = this.p.d(byteBuffer2);
                    if (!(d4 instanceof af)) {
                        b(y.c, "wrong http function", false);
                        return false;
                    }
                    af afVar2 = (af) d4;
                    if (this.p.a(afVar2) == l.b.MATCHED) {
                        a(afVar2);
                        return true;
                    }
                    a((int) y.c, "the handshake did finaly not match");
                    return false;
                }
            } catch (t e8) {
                close(e8);
            }
        } catch (q e9) {
            q qVar = e9;
            if (this.s.capacity() == 0) {
                byteBuffer2.reset();
                int a = qVar.a();
                if (a == 0) {
                    a = byteBuffer2.capacity() + 16;
                } else if (!k && qVar.a() < byteBuffer2.remaining()) {
                    throw new AssertionError();
                }
                this.s = ByteBuffer.allocate(a);
                this.s.put(byteBuffer);
            } else {
                this.s.position(this.s.limit());
                this.s.limit(this.s.capacity());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.g.a(int, java.lang.String, boolean):void
     arg types: [int, java.lang.String, int]
     candidates:
      com.tendcloud.tenddata.g.a(com.tendcloud.tenddata.ad$a, java.nio.ByteBuffer, boolean):void
      com.tendcloud.tenddata.d.a(com.tendcloud.tenddata.ad$a, java.nio.ByteBuffer, boolean):void
      com.tendcloud.tenddata.g.a(int, java.lang.String, boolean):void */
    private void b(ByteBuffer byteBuffer) {
        String str;
        int i2;
        try {
            for (ad adVar : this.p.c(byteBuffer)) {
                if (d) {
                    System.out.println("matched frame: " + adVar);
                }
                ad.a f2 = adVar.f();
                boolean d2 = adVar.d();
                if (f2 == ad.a.CLOSING) {
                    if (adVar instanceof y) {
                        y yVar = (y) adVar;
                        i2 = yVar.a();
                        str = yVar.b();
                    } else {
                        str = "";
                        i2 = 1005;
                    }
                    if (this.m == d.a.CLOSING) {
                        a(i2, str, true);
                    } else if (this.p.b() == l.a.TWOWAY) {
                        c(i2, str, true);
                    } else {
                        b(i2, str, false);
                    }
                } else if (f2 == ad.a.PING) {
                    this.n.b(this, adVar);
                } else if (f2 == ad.a.PONG) {
                    this.n.c(this, adVar);
                } else if (!d2 || f2 == ad.a.CONTINUOUS) {
                    if (f2 != ad.a.CONTINUOUS) {
                        if (this.r != null) {
                            throw new r((int) y.c, "Previous continuous frame sequence not completed.");
                        }
                        this.r = f2;
                    } else if (d2) {
                        if (this.r == null) {
                            throw new r((int) y.c, "Continuous frame sequence was not started.");
                        }
                        this.r = null;
                    } else if (this.r == null) {
                        throw new r((int) y.c, "Continuous frame sequence was not started.");
                    }
                    try {
                        this.n.a(this, adVar);
                    } catch (RuntimeException e2) {
                        this.n.a(this, e2);
                    }
                } else if (this.r != null) {
                    throw new r((int) y.c, "Continuous frame sequence not completed.");
                } else if (f2 == ad.a.TEXT) {
                    try {
                        this.n.a(this, aw.a(adVar.c()));
                    } catch (RuntimeException e3) {
                        this.n.a(this, e3);
                    }
                } else if (f2 == ad.a.BINARY) {
                    try {
                        this.n.a(this, adVar.c());
                    } catch (RuntimeException e4) {
                        this.n.a(this, e4);
                    }
                } else {
                    throw new r((int) y.c, "non control or continious frame expected");
                }
            }
        } catch (r e5) {
            this.n.a(this, e5);
            close(e5);
        }
    }

    private l.b c(ByteBuffer byteBuffer) {
        byteBuffer.mark();
        if (byteBuffer.limit() > l.c.length) {
            return l.b.NOT_MATCHED;
        }
        if (byteBuffer.limit() < l.c.length) {
            throw new q(l.c.length);
        }
        int i2 = 0;
        while (byteBuffer.hasRemaining()) {
            if (l.c[i2] != byteBuffer.get()) {
                byteBuffer.reset();
                return l.b.NOT_MATCHED;
            }
            i2++;
        }
        return l.b.MATCHED;
    }

    private void c(int i2, String str, boolean z) {
        if (this.m != d.a.CLOSING && this.m != d.a.CLOSED) {
            if (this.m == d.a.OPEN) {
                if (i2 != 1006) {
                    if (this.p.b() != l.a.NONE) {
                        if (!z) {
                            try {
                                this.n.a(this, i2, str);
                            } catch (RuntimeException e2) {
                                this.n.a(this, e2);
                            }
                        }
                        try {
                            sendFrame(new z(i2, str));
                        } catch (r e3) {
                            this.n.a(this, e3);
                            b(y.f, "generated frame is invalid", false);
                        }
                    }
                    b(i2, str, z);
                } else if (k || !z) {
                    this.m = d.a.CLOSING;
                    b(i2, str, false);
                    return;
                } else {
                    throw new AssertionError();
                }
            } else if (i2 != -3) {
                b(-1, str, false);
            } else if (k || z) {
                b(-3, str, true);
            } else {
                throw new AssertionError();
            }
            if (i2 == 1002) {
                b(i2, str, z);
            }
            this.m = d.a.CLOSING;
            this.s = null;
        }
    }

    private void d(ByteBuffer byteBuffer) {
        if (d) {
            System.out.println("write(" + byteBuffer.remaining() + "): {" + (byteBuffer.remaining() > 1000 ? "too big to display" : new String(byteBuffer.array())) + "}");
        }
        this.h.add(byteBuffer);
        this.n.onWriteDemand(this);
    }

    public void a() {
        close(1000);
    }

    public void a(int i2, String str) {
        c(i2, str, false);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0040, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0041, code lost:
        r2.n.a(r2, r0);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(int r3, java.lang.String r4, boolean r5) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.tendcloud.tenddata.d$a r0 = r2.m     // Catch:{ all -> 0x0036 }
            com.tendcloud.tenddata.d$a r1 = com.tendcloud.tenddata.d.a.CLOSED     // Catch:{ all -> 0x0036 }
            if (r0 != r1) goto L_0x0009
        L_0x0007:
            monitor-exit(r2)
            return
        L_0x0009:
            java.nio.channels.SelectionKey r0 = r2.f     // Catch:{ all -> 0x0036 }
            if (r0 == 0) goto L_0x0012
            java.nio.channels.SelectionKey r0 = r2.f     // Catch:{ all -> 0x0036 }
            r0.cancel()     // Catch:{ all -> 0x0036 }
        L_0x0012:
            java.nio.channels.ByteChannel r0 = r2.g     // Catch:{ all -> 0x0036 }
            if (r0 == 0) goto L_0x001b
            java.nio.channels.ByteChannel r0 = r2.g     // Catch:{ IOException -> 0x0039 }
            r0.close()     // Catch:{ IOException -> 0x0039 }
        L_0x001b:
            com.tendcloud.tenddata.h r0 = r2.n     // Catch:{ RuntimeException -> 0x0040 }
            r0.a(r2, r3, r4, r5)     // Catch:{ RuntimeException -> 0x0040 }
        L_0x0020:
            com.tendcloud.tenddata.l r0 = r2.p     // Catch:{ all -> 0x0036 }
            if (r0 == 0) goto L_0x0029
            com.tendcloud.tenddata.l r0 = r2.p     // Catch:{ all -> 0x0036 }
            r0.a()     // Catch:{ all -> 0x0036 }
        L_0x0029:
            r0 = 0
            r2.t = r0     // Catch:{ all -> 0x0036 }
            com.tendcloud.tenddata.d$a r0 = com.tendcloud.tenddata.d.a.CLOSED     // Catch:{ all -> 0x0036 }
            r2.m = r0     // Catch:{ all -> 0x0036 }
            java.util.concurrent.BlockingQueue r0 = r2.h     // Catch:{ all -> 0x0036 }
            r0.clear()     // Catch:{ all -> 0x0036 }
            goto L_0x0007
        L_0x0036:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x0039:
            r0 = move-exception
            com.tendcloud.tenddata.h r1 = r2.n     // Catch:{ all -> 0x0036 }
            r1.a(r2, r0)     // Catch:{ all -> 0x0036 }
            goto L_0x001b
        L_0x0040:
            r0 = move-exception
            com.tendcloud.tenddata.h r1 = r2.n     // Catch:{ all -> 0x0036 }
            r1.a(r2, r0)     // Catch:{ all -> 0x0036 }
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.g.a(int, java.lang.String, boolean):void");
    }

    /* access modifiers changed from: protected */
    public void a(int i2, boolean z) {
        a(i2, "", z);
    }

    public void a(ad.a aVar, ByteBuffer byteBuffer, boolean z) {
        a((Collection) this.p.a(aVar, byteBuffer, z));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.g.a(int, java.lang.String, boolean):void
     arg types: [int, java.lang.String, int]
     candidates:
      com.tendcloud.tenddata.g.a(com.tendcloud.tenddata.ad$a, java.nio.ByteBuffer, boolean):void
      com.tendcloud.tenddata.d.a(com.tendcloud.tenddata.ad$a, java.nio.ByteBuffer, boolean):void
      com.tendcloud.tenddata.g.a(int, java.lang.String, boolean):void */
    public void b(int i2, String str) {
        a(i2, str, false);
    }

    /* access modifiers changed from: protected */
    public synchronized void b(int i2, String str, boolean z) {
        if (!this.l) {
            this.v = Integer.valueOf(i2);
            this.u = str;
            this.w = Boolean.valueOf(z);
            this.l = true;
            this.n.onWriteDemand(this);
            try {
                this.n.b(this, i2, str, z);
            } catch (RuntimeException e2) {
                this.n.a(this, e2);
            }
            if (this.p != null) {
                this.p.a();
            }
            this.t = null;
        }
        return;
    }

    public boolean b() {
        return !this.h.isEmpty();
    }

    public InetSocketAddress c() {
        return this.n.c(this);
    }

    public void close(int i2) {
        c(i2, "", false);
    }

    public void close(r rVar) {
        c(rVar.a(), rVar.getMessage(), false);
    }

    public InetSocketAddress d() {
        return this.n.b(this);
    }

    public void decode(ByteBuffer byteBuffer) {
        if (k || byteBuffer.hasRemaining()) {
            if (d) {
                System.out.println("process(" + byteBuffer.remaining() + "): {" + (byteBuffer.remaining() > 1000 ? "too big to display" : new String(byteBuffer.array(), byteBuffer.position(), byteBuffer.remaining())) + "}");
            }
            if (this.m != d.a.NOT_YET_CONNECTED) {
                b(byteBuffer);
            } else if (a(byteBuffer)) {
                if (!k && this.s.hasRemaining() == byteBuffer.hasRemaining() && byteBuffer.hasRemaining()) {
                    throw new AssertionError();
                } else if (byteBuffer.hasRemaining()) {
                    b(byteBuffer);
                } else if (this.s.hasRemaining()) {
                    b(this.s);
                }
            }
            if (!k && !g() && !h() && byteBuffer.hasRemaining()) {
                throw new AssertionError();
            }
            return;
        }
        throw new AssertionError();
    }

    public boolean e() {
        if (k || !this.l || this.m == d.a.CONNECTING) {
            return this.m == d.a.CONNECTING;
        }
        throw new AssertionError();
    }

    public boolean f() {
        if (k || this.m != d.a.OPEN || !this.l) {
            return this.m == d.a.OPEN;
        }
        throw new AssertionError();
    }

    public boolean g() {
        return this.m == d.a.CLOSING;
    }

    public boolean h() {
        return this.l;
    }

    public int hashCode() {
        return super.hashCode();
    }

    public boolean i() {
        return this.m == d.a.CLOSED;
    }

    public l j() {
        return this.p;
    }

    public d.a k() {
        return this.m;
    }

    public String l() {
        return this.x;
    }

    public void m() {
        if (this.w == null) {
            throw new IllegalStateException("this method must be used in conjuction with flushAndClose");
        }
        a(this.v.intValue(), this.u, this.w.booleanValue());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.g.a(int, boolean):void
     arg types: [int, int]
     candidates:
      com.tendcloud.tenddata.g.a(int, java.lang.String):void
      com.tendcloud.tenddata.d.a(int, java.lang.String):void
      com.tendcloud.tenddata.g.a(int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.g.a(int, boolean):void
     arg types: [?, int]
     candidates:
      com.tendcloud.tenddata.g.a(int, java.lang.String):void
      com.tendcloud.tenddata.d.a(int, java.lang.String):void
      com.tendcloud.tenddata.g.a(int, boolean):void */
    public void n() {
        if (k() == d.a.NOT_YET_CONNECTED) {
            a(-1, true);
        } else if (this.l) {
            a(this.v.intValue(), this.u, this.w.booleanValue());
        } else if (this.p.b() == l.a.NONE) {
            a(1000, true);
        } else if (this.p.b() != l.a.ONEWAY) {
            a((int) y.f, true);
        } else if (this.q == d.b.SERVER) {
            a((int) y.f, true);
        } else {
            a(1000, true);
        }
    }

    public void send(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Cannot send 'null' data to a WebSocketImpl.");
        }
        a((Collection) this.p.a(str, this.q == d.b.CLIENT));
    }

    public void send(ByteBuffer byteBuffer) {
        if (byteBuffer == null) {
            throw new IllegalArgumentException("Cannot send 'null' data to a WebSocketImpl.");
        }
        a((Collection) this.p.a(byteBuffer, this.q == d.b.CLIENT));
    }

    public void send(byte[] bArr) {
        send(ByteBuffer.wrap(bArr));
    }

    public void sendFrame(ad adVar) {
        if (d) {
            System.out.println("send frame: " + adVar);
        }
        d(this.p.a(adVar));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.h.a(com.tendcloud.tenddata.d, com.tendcloud.tenddata.af):void
     arg types: [com.tendcloud.tenddata.g, com.tendcloud.tenddata.af]
     candidates:
      com.tendcloud.tenddata.h.a(com.tendcloud.tenddata.d, com.tendcloud.tenddata.ad):void
      com.tendcloud.tenddata.h.a(com.tendcloud.tenddata.d, com.tendcloud.tenddata.al):void
      com.tendcloud.tenddata.h.a(com.tendcloud.tenddata.d, java.lang.Exception):void
      com.tendcloud.tenddata.h.a(com.tendcloud.tenddata.d, java.lang.String):void
      com.tendcloud.tenddata.h.a(com.tendcloud.tenddata.d, java.nio.ByteBuffer):void
      com.tendcloud.tenddata.h.a(com.tendcloud.tenddata.d, com.tendcloud.tenddata.af):void */
    public void startHandshake(ah ahVar) {
        if (k || this.m != d.a.CONNECTING) {
            this.t = this.p.a(ahVar);
            this.x = ahVar.a();
            if (k || this.x != null) {
                try {
                    this.n.a((d) this, this.t);
                    a(this.p.a(this.t, this.q));
                } catch (r e2) {
                    throw new t("Handshake data rejected by client.");
                } catch (RuntimeException e3) {
                    this.n.a(this, e3);
                    throw new t("rejected because of" + e3);
                }
            } else {
                throw new AssertionError();
            }
        } else {
            throw new AssertionError("shall only be called once");
        }
    }

    public String toString() {
        return super.toString();
    }
}
