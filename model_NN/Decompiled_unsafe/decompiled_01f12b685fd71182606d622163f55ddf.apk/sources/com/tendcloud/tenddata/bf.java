package com.tendcloud.tenddata;

import java.io.IOException;

public abstract class bf {
    protected volatile int b = -1;

    public static final bf a(bf bfVar, byte[] bArr) {
        return b(bfVar, bArr, 0, bArr.length);
    }

    public static final void a(bf bfVar, byte[] bArr, int i, int i2) {
        try {
            ay a = ay.a(bArr, i, i2);
            bfVar.writeTo(a);
            a.b();
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public static final byte[] a(bf bfVar) {
        byte[] bArr = new byte[bfVar.e()];
        a(bfVar, bArr, 0, bArr.length);
        return bArr;
    }

    public static final bf b(bf bfVar, byte[] bArr, int i, int i2) {
        try {
            ax a = ax.a(bArr, i, i2);
            bfVar.a(a);
            a.checkLastTagWas(0);
            return bfVar;
        } catch (be e) {
            throw e;
        } catch (IOException e2) {
            throw new RuntimeException("Reading from a byte array threw an IOException (should never happen).");
        }
    }

    /* access modifiers changed from: protected */
    public int a() {
        return 0;
    }

    public abstract bf a(ax axVar);

    /* renamed from: c */
    public bf clone() {
        return (bf) super.clone();
    }

    public int d() {
        if (this.b < 0) {
            e();
        }
        return this.b;
    }

    public int e() {
        int a = a();
        this.b = a;
        return a;
    }

    public String toString() {
        return bg.a(this);
    }

    public void writeTo(ay ayVar) {
    }
}
