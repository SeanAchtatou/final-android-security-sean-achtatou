package com.tendcloud.tenddata;

import com.tendcloud.tenddata.as;
import java.lang.Thread;

class at implements Thread.UncaughtExceptionHandler {
    final /* synthetic */ as a;
    final /* synthetic */ as.b b;

    at(as.b bVar, as asVar) {
        this.b = bVar;
        this.a = asVar;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        Thread.getDefaultUncaughtExceptionHandler().uncaughtException(thread, th);
    }
}
