package com.tendcloud.tenddata;

import com.badlogic.gdx.net.HttpRequestHeader;
import com.tendcloud.tenddata.ad;
import com.tendcloud.tenddata.l;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class o extends l {
    public static final byte f = 13;
    public static final byte g = 10;
    public static final byte h = 0;
    public static final byte i = -1;
    protected boolean j = false;
    protected List k = new LinkedList();
    protected ByteBuffer l;
    private final Random m = new Random();

    public ah a(ah ahVar) {
        ahVar.a("Upgrade", "WebSocket");
        ahVar.a("Connection", "Upgrade");
        if (!ahVar.b(HttpRequestHeader.Origin)) {
            ahVar.a(HttpRequestHeader.Origin, "random" + this.m.nextInt());
        }
        return ahVar;
    }

    public ai a(af afVar, ap apVar) {
        apVar.setHttpStatusMessage("Web Socket Protocol Handshake");
        apVar.a("Upgrade", "WebSocket");
        apVar.a("Connection", afVar.a("Connection"));
        apVar.a("WebSocket-Origin", afVar.a(HttpRequestHeader.Origin));
        apVar.a("WebSocket-Location", "ws://" + afVar.a(HttpRequestHeader.Host) + afVar.a());
        return apVar;
    }

    public l.b a(af afVar) {
        return (!afVar.b(HttpRequestHeader.Origin) || !a(afVar)) ? l.b.NOT_MATCHED : l.b.MATCHED;
    }

    public l.b a(af afVar, an anVar) {
        return (!afVar.a("WebSocket-Origin").equals(anVar.a(HttpRequestHeader.Origin)) || !a(anVar)) ? l.b.NOT_MATCHED : l.b.MATCHED;
    }

    public ByteBuffer a(ad adVar) {
        if (adVar.f() != ad.a.TEXT) {
            throw new RuntimeException("only text frames supported");
        }
        ByteBuffer c = adVar.c();
        ByteBuffer allocate = ByteBuffer.allocate(c.remaining() + 2);
        allocate.put((byte) 0);
        c.mark();
        allocate.put(c);
        c.reset();
        allocate.put((byte) -1);
        allocate.flip();
        return allocate;
    }

    public List a(String str, boolean z) {
        ae aeVar = new ae();
        try {
            aeVar.setPayload(ByteBuffer.wrap(aw.a(str)));
            aeVar.setFin(true);
            aeVar.setOptcode(ad.a.TEXT);
            aeVar.setTransferemasked(z);
            return Collections.singletonList(aeVar);
        } catch (r e) {
            throw new w(e);
        }
    }

    public List a(ByteBuffer byteBuffer, boolean z) {
        throw new RuntimeException("not yet implemented");
    }

    public void a() {
        this.j = false;
        this.l = null;
    }

    public l.a b() {
        return l.a.NONE;
    }

    public l c() {
        return new o();
    }

    public List c(ByteBuffer byteBuffer) {
        List e = e(byteBuffer);
        if (e != null) {
            return e;
        }
        throw new r(y.c);
    }

    public ByteBuffer e() {
        return ByteBuffer.allocate(b);
    }

    /* access modifiers changed from: protected */
    public List e(ByteBuffer byteBuffer) {
        while (byteBuffer.hasRemaining()) {
            byte b = byteBuffer.get();
            if (b == 0) {
                if (this.j) {
                    throw new s("unexpected START_OF_FRAME");
                }
                this.j = true;
            } else if (b == -1) {
                if (!this.j) {
                    throw new s("unexpected END_OF_FRAME");
                }
                if (this.l != null) {
                    this.l.flip();
                    ae aeVar = new ae();
                    aeVar.setPayload(this.l);
                    aeVar.setFin(true);
                    aeVar.setOptcode(ad.a.TEXT);
                    this.k.add(aeVar);
                    this.l = null;
                    byteBuffer.mark();
                }
                this.j = false;
            } else if (!this.j) {
                return null;
            } else {
                if (this.l == null) {
                    this.l = e();
                } else if (!this.l.hasRemaining()) {
                    this.l = f(this.l);
                }
                this.l.put(b);
            }
        }
        List list = this.k;
        this.k = new LinkedList();
        return list;
    }

    public ByteBuffer f(ByteBuffer byteBuffer) {
        byteBuffer.flip();
        ByteBuffer allocate = ByteBuffer.allocate(a(byteBuffer.capacity() * 2));
        allocate.put(byteBuffer);
        return allocate;
    }
}
