package com.tendcloud.tenddata;

final class ci extends cg {
    static final /* synthetic */ boolean G = (!ci.class.desiredAssertionStatus());
    private static final int H = 4096;
    private static int I = 4096;
    private static int J = 4096;
    private final ck[] K = new ck[4096];
    private int L = 0;
    private int M = 0;
    private cj N;
    private final int[] O = new int[4];
    private final cm P = new cm();

    ci(cl clVar, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        super(clVar, new cd(i4, Math.max(i5, I), J, i6, 273, i7), i, i2, i3, i4, i6);
        for (int i8 = 0; i8 < 4096; i8++) {
            this.K[i8] = new ck();
        }
    }

    private void a(int i, int i2, int i3, int i4, int i5) {
        int i6;
        if (this.N.a[this.N.c - 1] > i3) {
            this.N.c = 0;
            while (this.N.a[this.N.c] < i3) {
                this.N.c++;
            }
            int[] iArr = this.N.a;
            cj cjVar = this.N;
            int i7 = cjVar.c;
            cjVar.c = i7 + 1;
            iArr[i7] = i3;
        }
        if (this.N.a[this.N.c - 1] >= i5) {
            while (this.M < this.L + this.N.a[this.N.c - 1]) {
                ck[] ckVarArr = this.K;
                int i8 = this.M + 1;
                this.M = i8;
                ckVarArr[i8].a();
            }
            int a = a(i4, this.K[this.L].a);
            int i9 = 0;
            while (i5 > this.N.a[i6]) {
                i9 = i6 + 1;
            }
            while (true) {
                int i10 = i6;
                int i11 = this.N.b[i10];
                int a2 = a(a, i11, i5, i2);
                if (a2 < this.K[this.L + i5].c) {
                    this.K[this.L + i5].a(a2, this.L, i11 + 4);
                }
                if (i5 != this.N.a[i10]) {
                    i6 = i10;
                } else {
                    int a3 = this.y.a(i5 + 1, i11, Math.min(this.C, (i3 - i5) - 1));
                    if (a3 >= 2) {
                        this.P.a(this.K[this.L].a);
                        this.P.d();
                        int a4 = this.y.a(i5, 0);
                        int b = this.y.b(0);
                        int a5 = this.y.a(i5, 1);
                        this.P.c();
                        int b2 = b(0, a3, this.P, (i + i5 + 1) & this.m) + this.z.a(a4, b, a5, i + i5, this.P) + a2;
                        int i12 = this.L + i5 + 1 + a3;
                        while (this.M < i12) {
                            ck[] ckVarArr2 = this.K;
                            int i13 = this.M + 1;
                            this.M = i13;
                            ckVarArr2[i13].a();
                        }
                        if (b2 < this.K[i12].c) {
                            this.K[i12].a(b2, this.L, i11 + 4, i5, 0);
                        }
                    }
                    i6 = i10 + 1;
                    if (i6 == this.N.c) {
                        return;
                    }
                }
                i5++;
            }
        }
    }

    private void b(int i, int i2, int i3, int i4) {
        boolean z;
        int a;
        int a2;
        int b = this.y.b(0);
        int b2 = this.y.b(this.K[this.L].b[0] + 1);
        int a3 = this.K[this.L].c + this.z.a(b, b2, this.y.b(1), i, this.K[this.L].a);
        if (a3 < this.K[this.L + 1].c) {
            this.K[this.L + 1].a(a3, this.L, -1);
            z = true;
        } else {
            z = false;
        }
        if (b2 == b && ((this.K[this.L + 1].d == this.L || this.K[this.L + 1].e != 0) && (a2 = a(i4, this.K[this.L].a, i2)) <= this.K[this.L + 1].c)) {
            this.K[this.L + 1].a(a2, this.L, 0);
            z = true;
        }
        if (!z && b2 != b && i3 > 2 && (a = this.y.a(1, this.K[this.L].b[0], Math.min(this.C, i3 - 1))) >= 2) {
            this.P.a(this.K[this.L].a);
            this.P.c();
            int b3 = b(0, a, this.P, (i + 1) & this.m) + a3;
            int i5 = a + this.L + 1;
            while (this.M < i5) {
                ck[] ckVarArr = this.K;
                int i6 = this.M + 1;
                this.M = i6;
                ckVarArr[i6].a();
            }
            if (b3 < this.K[i5].c) {
                this.K[i5].b(b3, this.L, 0);
            }
        }
    }

    private int c(int i, int i2, int i3, int i4) {
        int i5 = 2;
        int min = Math.min(i3, this.C);
        int i6 = 0;
        while (true) {
            int i7 = i6;
            int i8 = i5;
            if (i7 >= 4) {
                return i8;
            }
            int b = this.y.b(this.K[this.L].b[i7], min);
            if (b < 2) {
                i5 = i8;
            } else {
                while (this.M < this.L + b) {
                    ck[] ckVarArr = this.K;
                    int i9 = this.M + 1;
                    this.M = i9;
                    ckVarArr[i9].a();
                }
                int a = a(i4, i7, this.K[this.L].a, i2);
                for (int i10 = b; i10 >= 2; i10--) {
                    int b2 = this.B.b(i10, i2) + a;
                    if (b2 < this.K[this.L + i10].c) {
                        this.K[this.L + i10].a(b2, this.L, i7);
                    }
                }
                i5 = i7 == 0 ? b + 1 : i8;
                int a2 = this.y.a(b + 1, this.K[this.L].b[i7], Math.min(this.C, (i3 - b) - 1));
                if (a2 >= 2) {
                    int b3 = a + this.B.b(b, i2);
                    this.P.a(this.K[this.L].a);
                    this.P.e();
                    int a3 = this.y.a(b, 0);
                    int b4 = this.y.b(0);
                    int a4 = this.y.a(b, 1);
                    this.P.c();
                    int b5 = b(0, a2, this.P, (i + b + 1) & this.m) + this.z.a(a3, b4, a4, i + b, this.P) + b3;
                    int i11 = this.L + b + 1 + a2;
                    while (this.M < i11) {
                        ck[] ckVarArr2 = this.K;
                        int i12 = this.M + 1;
                        this.M = i12;
                        ckVarArr2[i12].a();
                    }
                    if (b5 < this.K[i11].c) {
                        this.K[i11].a(b5, this.L, i7, b, 0);
                    }
                }
            }
            i6 = i7 + 1;
        }
    }

    private int i() {
        int i;
        this.M = this.L;
        int i2 = this.K[this.L].d;
        do {
            ck ckVar = this.K[this.L];
            if (ckVar.f) {
                this.K[i2].d = this.L;
                this.K[i2].e = -1;
                i = i2 - 1;
                this.L = i2;
                if (ckVar.g) {
                    this.K[i].d = i + 1;
                    this.K[i].e = ckVar.i;
                    this.L = i;
                    i = ckVar.h;
                }
            } else {
                i = i2;
            }
            i2 = this.K[i].d;
            this.K[i].d = this.L;
            this.L = i;
        } while (this.L > 0);
        this.L = this.K[0].d;
        this.D = this.K[this.L].e;
        return this.L;
    }

    private void j() {
        int i;
        int i2;
        int i3 = this.K[this.L].d;
        if (G || i3 < this.L) {
            if (this.K[this.L].f) {
                i3--;
                if (this.K[this.L].g) {
                    this.K[this.L].a.a(this.K[this.K[this.L].h].a);
                    if (this.K[this.L].i < 4) {
                        this.K[this.L].a.e();
                    } else {
                        this.K[this.L].a.d();
                    }
                } else {
                    this.K[this.L].a.a(this.K[i3].a);
                }
                this.K[this.L].a.c();
            } else {
                this.K[this.L].a.a(this.K[i3].a);
            }
            if (i3 != this.L - 1) {
                if (!this.K[this.L].f || !this.K[this.L].g) {
                    i = this.K[this.L].e;
                    if (i < 4) {
                        this.K[this.L].a.e();
                        i2 = i3;
                    } else {
                        this.K[this.L].a.d();
                        i2 = i3;
                    }
                } else {
                    int i4 = this.K[this.L].h;
                    int i5 = this.K[this.L].i;
                    this.K[this.L].a.e();
                    i2 = i4;
                    i = i5;
                }
                if (i < 4) {
                    this.K[this.L].b[0] = this.K[i2].b[i];
                    int i6 = 1;
                    while (i6 <= i) {
                        this.K[this.L].b[i6] = this.K[i2].b[i6 - 1];
                        i6++;
                    }
                    while (i6 < 4) {
                        this.K[this.L].b[i6] = this.K[i2].b[i6];
                        i6++;
                    }
                    return;
                }
                this.K[this.L].b[0] = i - 4;
                System.arraycopy(this.K[i2].b, 0, this.K[this.L].b, 1, 3);
            } else if (G || this.K[this.L].e == 0 || this.K[this.L].e == -1) {
                if (this.K[this.L].e == 0) {
                    this.K[this.L].a.f();
                } else {
                    this.K[this.L].a.c();
                }
                System.arraycopy(this.K[i3].b, 0, this.K[this.L].b, 0, 4);
            } else {
                throw new AssertionError();
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: package-private */
    public int a() {
        int i;
        int a;
        int i2;
        if (this.L < this.M) {
            int i3 = this.K[this.L].d - this.L;
            this.L = this.K[this.L].d;
            this.D = this.K[this.L].e;
            return i3;
        } else if (G || this.L == this.M) {
            this.L = 0;
            this.M = 0;
            this.D = -1;
            if (this.E == -1) {
                this.N = g();
            }
            int min = Math.min(this.y.e(), 273);
            if (min < 2) {
                return 1;
            }
            int i4 = 0;
            int i5 = 0;
            while (i4 < 4) {
                this.O[i4] = this.y.b(this.n[i4], min);
                if (this.O[i4] < 2) {
                    this.O[i4] = 0;
                    i2 = i5;
                } else {
                    i2 = this.O[i4] > this.O[i5] ? i4 : i5;
                }
                i4++;
                i5 = i2;
            }
            if (this.O[i5] >= this.C) {
                this.D = i5;
                c(this.O[i5] - 1);
                return this.O[i5];
            }
            if (this.N.c > 0) {
                int i6 = this.N.a[this.N.c - 1];
                int i7 = this.N.b[this.N.c - 1];
                if (i6 >= this.C) {
                    this.D = i7 + 4;
                    c(i6 - 1);
                    return i6;
                }
                i = i6;
            } else {
                i = 0;
            }
            int b = this.y.b(0);
            int b2 = this.y.b(this.n[0] + 1);
            if (i < 2 && b != b2 && this.O[i5] < 2) {
                return 1;
            }
            int f = this.y.f();
            int i8 = f & this.m;
            this.K[1].a(this.z.a(b, b2, this.y.b(1), f, this.o), 0, -1);
            int a2 = a(this.o, i8);
            int b3 = b(a2, this.o);
            if (b2 == b && (a = a(b3, this.o, i8)) < this.K[1].c) {
                this.K[1].a(a, 0, 0);
            }
            this.M = Math.max(i, this.O[i5]);
            if (this.M >= 2) {
                h();
                this.K[0].a.a(this.o);
                System.arraycopy(this.n, 0, this.K[0].b, 0, 4);
                for (int i9 = this.M; i9 >= 2; i9--) {
                    this.K[i9].a();
                }
                for (int i10 = 0; i10 < 4; i10++) {
                    int i11 = this.O[i10];
                    if (i11 >= 2) {
                        int a3 = a(b3, i10, this.o, i8);
                        do {
                            int b4 = this.B.b(i11, i8) + a3;
                            if (b4 < this.K[i11].c) {
                                this.K[i11].a(b4, 0, i10);
                            }
                            i11--;
                        } while (i11 >= 2);
                    }
                }
                int max = Math.max(this.O[0] + 1, 2);
                if (max <= i) {
                    int a4 = a(a2, this.o);
                    int i12 = 0;
                    while (max > this.N.a[i12]) {
                        i12++;
                    }
                    while (true) {
                        int i13 = this.N.b[i12];
                        int a5 = a(a4, i13, max, i8);
                        if (a5 < this.K[max].c) {
                            this.K[max].a(a5, 0, i13 + 4);
                        }
                        if (max == this.N.a[i12] && (i12 = i12 + 1) == this.N.c) {
                            break;
                        }
                        max++;
                    }
                }
                int min2 = Math.min(this.y.e(), 4095);
                int i14 = f;
                while (true) {
                    int i15 = this.L + 1;
                    this.L = i15;
                    if (i15 >= this.M) {
                        break;
                    }
                    this.N = g();
                    if (this.N.c > 0 && this.N.a[this.N.c - 1] >= this.C) {
                        break;
                    }
                    min2--;
                    i14++;
                    int i16 = i14 & this.m;
                    j();
                    int a6 = a(this.K[this.L].a, i16) + this.K[this.L].c;
                    int b5 = b(a6, this.K[this.L].a);
                    b(i14, i16, min2, b5);
                    if (min2 >= 2) {
                        int c = c(i14, i16, min2, b5);
                        if (this.N.c > 0) {
                            a(i14, i16, min2, a6, c);
                        }
                    }
                }
                return i();
            } else if (G || this.M == 0) {
                this.D = this.K[1].e;
                return 1;
            } else {
                throw new AssertionError(this.M);
            }
        } else {
            throw new AssertionError();
        }
    }

    public void c() {
        this.L = 0;
        this.M = 0;
        super.c();
    }
}
