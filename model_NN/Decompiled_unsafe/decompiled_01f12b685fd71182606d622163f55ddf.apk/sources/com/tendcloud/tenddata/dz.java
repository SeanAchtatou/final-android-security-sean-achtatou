package com.tendcloud.tenddata;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewTreeObserver;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

class dz {
    private WeakHashMap a = new WeakHashMap();
    private final Handler b = new Handler(Looper.getMainLooper());
    private final Map c = new HashMap();
    private final Set d = new HashSet();

    static class a implements ViewTreeObserver.OnGlobalLayoutListener, Runnable {
        private volatile boolean a = false;
        private boolean b = true;
        private final WeakReference c;
        private final ej d;
        private final Handler e;

        a(View view, ej ejVar, Handler handler) {
            this.d = ejVar;
            this.c = new WeakReference(view);
            this.e = handler;
            ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.addOnGlobalLayoutListener(this);
            }
            run();
        }

        private void b() {
            if (this.b) {
                View view = (View) this.c.get();
                if (view != null) {
                    ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
                    if (viewTreeObserver.isAlive()) {
                        viewTreeObserver.removeGlobalOnLayoutListener(this);
                    }
                }
                this.d.a();
            }
            this.b = false;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.a = true;
            this.e.post(this);
        }

        public void onGlobalLayout() {
            run();
        }

        public void run() {
            if (this.b) {
                View view = (View) this.c.get();
                if (view == null || this.a) {
                    b();
                    return;
                }
                this.d.a(view);
                this.e.removeCallbacks(this);
                this.e.postDelayed(this, 1000);
            }
        }
    }

    dz() {
    }

    private void a(View view, List list) {
        synchronized (this.d) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                this.d.add(new a(view, (ej) list.get(i), this.b));
            }
        }
    }

    private void c() {
        if (Thread.currentThread() == this.b.getLooper().getThread()) {
            d();
        } else {
            this.b.post(new ea(this));
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        List list;
        List list2;
        for (Activity activity : a()) {
            String canonicalName = activity.getClass().getCanonicalName();
            View rootView = activity.getWindow().getDecorView().getRootView();
            synchronized (this.c) {
                list = (List) this.c.get(canonicalName);
                list2 = (List) this.c.get(null);
            }
            if (list != null) {
                a(rootView, list);
            }
            if (list2 != null) {
                a(rootView, list2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Set a() {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            return Collections.unmodifiableSet(this.a.keySet());
        }
        throw new RuntimeException("Can't remove an activity when not on the UI thread");
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity) {
        if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
            throw new RuntimeException("Can't add an activity when not on the UI thread");
        }
        this.a.put(activity, activity.getLocalClassName());
        c();
    }

    /* access modifiers changed from: package-private */
    public void a(Map map) {
        synchronized (this.d) {
            for (a a2 : this.d) {
                a2.a();
            }
            this.d.clear();
        }
        synchronized (this.c) {
            this.c.clear();
            this.c.putAll(map);
        }
        c();
    }

    /* access modifiers changed from: package-private */
    public void b(Activity activity) {
        if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
            throw new RuntimeException("Can't remove an activity when not on the UI thread");
        }
        this.a.remove(activity);
        synchronized (this.d) {
            for (a a2 : this.d) {
                a2.a();
            }
            this.d.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            return this.a.isEmpty();
        }
        throw new RuntimeException("Can't check isEmpty() when not on the UI thread");
    }
}
