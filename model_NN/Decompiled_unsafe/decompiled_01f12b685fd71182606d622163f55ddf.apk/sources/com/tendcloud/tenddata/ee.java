package com.tendcloud.tenddata;

import android.view.View;
import java.lang.reflect.Method;

class ee {
    private final String a;
    private final Object[] b;
    private final Class c;
    private final Class d;
    private final Method e;

    ee(Class cls, String str, Object[] objArr, Class cls2) {
        this.a = str;
        this.b = objArr;
        this.c = cls2;
        this.e = b(cls);
        if (this.e == null) {
            throw new NoSuchMethodException("Method " + cls.getName() + "." + this.a + " doesn't exit");
        }
        this.d = this.e.getDeclaringClass();
    }

    private static Class a(Class cls) {
        return cls == Byte.class ? Byte.TYPE : cls == Short.class ? Short.TYPE : cls == Integer.class ? Integer.TYPE : cls == Long.class ? Long.TYPE : cls == Float.class ? Float.TYPE : cls == Double.class ? Double.TYPE : cls == Boolean.class ? Boolean.TYPE : cls == Character.class ? Character.TYPE : cls;
    }

    private Method b(Class cls) {
        Class[] clsArr = new Class[this.b.length];
        for (int i = 0; i < this.b.length; i++) {
            clsArr[i] = this.b[i].getClass();
        }
        for (Method method : cls.getMethods()) {
            String name = method.getName();
            Class<?>[] parameterTypes = method.getParameterTypes();
            if (name.equals(this.a) && parameterTypes.length == this.b.length && a(this.c).isAssignableFrom(a(method.getReturnType()))) {
                boolean z = true;
                for (int i2 = 0; i2 < parameterTypes.length && z; i2++) {
                    z = a(parameterTypes[i2]).isAssignableFrom(a(clsArr[i2]));
                }
                if (z) {
                    return method;
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public Object a(View view) {
        return a(view, this.b);
    }

    /* access modifiers changed from: package-private */
    public Object a(View view, Object[] objArr) {
        if (this.d.isAssignableFrom(view.getClass())) {
            try {
                return this.e.invoke(view, objArr);
            } catch (Exception e2) {
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public boolean a(Object[] objArr) {
        Class<?>[] parameterTypes = this.e.getParameterTypes();
        if (objArr.length != parameterTypes.length) {
            return false;
        }
        for (int i = 0; i < objArr.length; i++) {
            Class a2 = a(parameterTypes[i]);
            if (objArr[i] == null) {
                if (a2 == Byte.TYPE || a2 == Short.TYPE || a2 == Integer.TYPE || a2 == Long.TYPE || a2 == Float.TYPE || a2 == Double.TYPE || a2 == Boolean.TYPE || a2 == Character.TYPE) {
                    return false;
                }
            } else if (!a2.isAssignableFrom(a(objArr[i].getClass()))) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public Object[] a() {
        return this.b;
    }

    public String toString() {
        return "[Caller " + this.a + "(" + this.b + ")" + "]";
    }
}
