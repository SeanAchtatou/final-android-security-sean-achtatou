package com.tendcloud.tenddata;

import com.sg.pak.PAK_ASSETS;
import java.util.Arrays;

final class bh {
    final int a;
    final byte[] b;

    bh(int i, byte[] bArr) {
        this.a = i;
        this.b = bArr;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return 0 + ay.h(this.a) + this.b.length;
    }

    /* access modifiers changed from: package-private */
    public void a(ay ayVar) {
        ayVar.writeRawVarint32(this.a);
        ayVar.writeRawBytes(this.b);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof bh)) {
            return false;
        }
        bh bhVar = (bh) obj;
        return this.a == bhVar.a && Arrays.equals(this.b, bhVar.b);
    }

    public int hashCode() {
        return ((this.a + PAK_ASSETS.IMG_GONGGAO011) * 31) + Arrays.hashCode(this.b);
    }
}
