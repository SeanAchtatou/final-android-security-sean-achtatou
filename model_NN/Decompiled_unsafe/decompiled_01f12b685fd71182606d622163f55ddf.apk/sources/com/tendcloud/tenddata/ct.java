package com.tendcloud.tenddata;

import android.content.Context;
import com.tendcloud.tenddata.bk;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class ct {
    private static final String a = "PushLog";
    /* access modifiers changed from: private */
    public static String b = "";

    static class a {
        public String a;
        public int b;

        a() {
        }

        public boolean a() {
            return !cp.a(this.a) && this.b != 0;
        }

        public String toString() {
            return String.format("%s:%d", this.a, Integer.valueOf(this.b));
        }
    }

    ct() {
    }

    static a a(Context context) {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new cw("app", cv.b(context)));
        String b2 = b(context, String.format("/api/q/a/%s", cv.a(context)));
        bk.d a2 = bk.a(context, cv.a(), dc.G, b2, b, a(arrayList));
        if (a2.a == 200) {
            String[] split = a2.b.trim().split(":");
            if (split.length == 2) {
                aVar.a = split[0];
                aVar.b = Integer.valueOf(split[1]).intValue();
                cr.a(dc.D, "true");
            }
        } else {
            cs.b("PushLog", "[push] get connector address failed." + a2.a + "    " + a2.b);
        }
        return aVar;
    }

    static byte[] a(List list) {
        if (list == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            cw cwVar = (cw) it.next();
            stringBuffer.append(cwVar.a() + "=" + cwVar.b() + "&");
        }
        if (stringBuffer.length() > 0) {
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        }
        return stringBuffer.toString().getBytes();
    }

    /* access modifiers changed from: private */
    public static String b(Context context, String str) {
        return dc.F + str;
    }

    static void b(Context context) {
        if (cp.a(cr.d(dc.m))) {
            ca.execute(new cu(context));
        }
    }
}
