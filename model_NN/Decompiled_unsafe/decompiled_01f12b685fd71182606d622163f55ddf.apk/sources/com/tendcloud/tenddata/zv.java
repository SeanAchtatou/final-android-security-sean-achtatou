package com.tendcloud.tenddata;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Message;
import com.datalab.tools.Constant;
import com.tendcloud.tenddata.dg;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONObject;

public class zv extends BroadcastReceiver {
    public static final String TALKINGDATA_MESSAGE_CLICK = "com.talkingdata.message.click";
    public static final String TALKINGDATA_NOTIFICATION_CANCEL = "com.talkingdata.notification.cancel";
    public static final String TALKINGDATA_NOTIFICATION_CLICK = "com.talkingdata.notification.click";
    static String a = zv.class.getSimpleName();
    static final String b = "TalkingData_Push_SharedPreferences";

    enum a {
        baidu,
        getui,
        jpush,
        nick
    }

    static String a(a aVar) {
        switch (aVar) {
            case baidu:
                return "api_key";
            case getui:
            default:
                return "PUSH_APPID";
            case jpush:
                return "JPUSH_APPKEY";
            case nick:
                return "TD_APP_ID";
        }
    }

    static synchronized void a(Context context, String str, a aVar) {
        String b2;
        synchronized (zv.class) {
            String c = ca.c(context, a(aVar));
            if (aVar == a.nick && c == null) {
                c = bu.b(context, ab.SETTINGS_PERF_FILE, ab.SETTINGS_PREF_PUSH_APPID, (String) null);
            }
            if (c != null && ((b2 = bu.b(context, (String) b, aVar + c, (String) null)) == null || (b2 != null && !b2.equalsIgnoreCase(str)))) {
                Message obtain = Message.obtain();
                obtain.what = 102;
                obtain.obj = new dg(c, str, aVar.name());
                dh.a().sendMessage(obtain);
                bu.a(ab.mContext, b, aVar.name() + c, str);
            }
        }
    }

    public static HashMap getMapFromJsonString(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            HashMap hashMap = new HashMap();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap.put(next, jSONObject.get(next).toString());
            }
            return hashMap;
        } catch (Exception e) {
            return null;
        }
    }

    public void onMessageReceived(Context context, String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject != null) {
                if (jSONObject.has(Constant.SIGN)) {
                    String string = jSONObject.getString(Constant.SIGN);
                    Message obtain = Message.obtain();
                    obtain.what = 103;
                    obtain.obj = new dg(string, null, dg.a.ARRIVED, 0);
                    dh.a().sendMessage(obtain);
                }
                int i = jSONObject.getInt("type");
                if (i == 1) {
                    dd.a(context, jSONObject);
                    return;
                }
                if (i == 2 || i == 3 || i == 4) {
                }
            }
        } catch (Throwable th) {
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onNofiticationClick(android.content.Context r9, android.content.Intent r10) {
        /*
            r8 = this;
            r0 = 0
            java.lang.String r1 = "custom"
            java.lang.String r1 = r10.getStringExtra(r1)     // Catch:{ Throwable -> 0x00a2 }
            java.lang.String r2 = "sign"
            java.lang.String r2 = r10.getStringExtra(r2)     // Catch:{ Throwable -> 0x00a2 }
            java.lang.String r3 = "appkey"
            java.lang.String r3 = r10.getStringExtra(r3)     // Catch:{ Throwable -> 0x00a2 }
            java.lang.String r4 = "ex"
            java.lang.String r4 = r10.getStringExtra(r4)     // Catch:{ Throwable -> 0x00a2 }
            java.lang.String r5 = com.tendcloud.tenddata.ab.getPushAppId(r9)     // Catch:{ Throwable -> 0x00a2 }
            boolean r3 = r3.equals(r5)     // Catch:{ Throwable -> 0x00a2 }
            if (r3 != 0) goto L_0x0024
        L_0x0023:
            return
        L_0x0024:
            if (r4 == 0) goto L_0x002a
            java.util.HashMap r0 = getMapFromJsonString(r4)     // Catch:{ Throwable -> 0x00a2 }
        L_0x002a:
            android.os.Message r3 = android.os.Message.obtain()     // Catch:{ Throwable -> 0x00a2 }
            r4 = 103(0x67, float:1.44E-43)
            r3.what = r4     // Catch:{ Throwable -> 0x00a2 }
            com.tendcloud.tenddata.dg r4 = new com.tendcloud.tenddata.dg     // Catch:{ Throwable -> 0x00a2 }
            r5 = 0
            com.tendcloud.tenddata.dg$a r6 = com.tendcloud.tenddata.dg.a.CLICK     // Catch:{ Throwable -> 0x00a2 }
            r7 = 0
            r4.<init>(r2, r5, r6, r7)     // Catch:{ Throwable -> 0x00a2 }
            r3.obj = r4     // Catch:{ Throwable -> 0x00a2 }
            android.os.Handler r2 = com.tendcloud.tenddata.dh.a()     // Catch:{ Throwable -> 0x00a2 }
            r2.sendMessage(r3)     // Catch:{ Throwable -> 0x00a2 }
            if (r1 == 0) goto L_0x0086
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0068 }
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0068 }
            java.lang.String r2 = "activity"
            boolean r2 = r0.isNull(r2)     // Catch:{ Throwable -> 0x0068 }
            if (r2 == 0) goto L_0x006a
            java.lang.String r0 = r9.getPackageName()     // Catch:{ Throwable -> 0x0068 }
            android.content.pm.PackageManager r2 = r9.getPackageManager()     // Catch:{ Throwable -> 0x0068 }
            android.content.Intent r0 = r2.getLaunchIntentForPackage(r0)     // Catch:{ Throwable -> 0x0068 }
            java.lang.String r2 = "custom"
            r0.putExtra(r2, r1)     // Catch:{ Throwable -> 0x0068 }
            r9.startActivity(r0)     // Catch:{ Throwable -> 0x0068 }
            goto L_0x0023
        L_0x0068:
            r0 = move-exception
            goto L_0x0023
        L_0x006a:
            java.lang.String r2 = "activity"
            java.lang.String r0 = r0.getString(r2)     // Catch:{ Throwable -> 0x0068 }
            android.content.Intent r2 = new android.content.Intent     // Catch:{ Throwable -> 0x0068 }
            r2.<init>()     // Catch:{ Throwable -> 0x0068 }
            r2.setClassName(r9, r0)     // Catch:{ Throwable -> 0x0068 }
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            r2.addFlags(r0)     // Catch:{ Throwable -> 0x0068 }
            java.lang.String r0 = "custom"
            r2.putExtra(r0, r1)     // Catch:{ Throwable -> 0x0068 }
            r9.startActivity(r2)     // Catch:{ Throwable -> 0x0068 }
            goto L_0x0023
        L_0x0086:
            java.lang.String r1 = r9.getPackageName()     // Catch:{ Throwable -> 0x00a2 }
            android.content.pm.PackageManager r2 = r9.getPackageManager()     // Catch:{ Throwable -> 0x00a2 }
            android.content.Intent r1 = r2.getLaunchIntentForPackage(r1)     // Catch:{ Throwable -> 0x00a2 }
            if (r0 == 0) goto L_0x0099
            java.lang.String r2 = "ex"
            r1.putExtra(r2, r0)     // Catch:{ Throwable -> 0x00a2 }
        L_0x0099:
            r0 = 536870912(0x20000000, float:1.0842022E-19)
            r1.addFlags(r0)     // Catch:{ Throwable -> 0x00a2 }
            r9.startActivity(r1)     // Catch:{ Throwable -> 0x00a2 }
            goto L_0x0023
        L_0x00a2:
            r0 = move-exception
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.zv.onNofiticationClick(android.content.Context, android.content.Intent):void");
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r7, android.content.Intent r8) {
        /*
            r6 = this;
            java.lang.String r0 = "TD_app_pefercen_profile"
            java.lang.String r1 = "TD_push_appId"
            r2 = 0
            java.lang.String r0 = com.tendcloud.tenddata.bu.b(r7, r0, r1, r2)     // Catch:{ Throwable -> 0x003a }
            java.lang.String r1 = "appkey"
            java.lang.String r1 = r8.getStringExtra(r1)     // Catch:{ Throwable -> 0x003a }
            if (r0 == 0) goto L_0x0019
            if (r1 == 0) goto L_0x001a
            boolean r0 = r1.equals(r0)     // Catch:{ Throwable -> 0x003a }
            if (r0 != 0) goto L_0x001a
        L_0x0019:
            return
        L_0x001a:
            android.content.Context r0 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x003a }
            if (r0 != 0) goto L_0x0020
            com.tendcloud.tenddata.ab.mContext = r7     // Catch:{ Throwable -> 0x003a }
        L_0x0020:
            java.lang.String r0 = r8.getAction()     // Catch:{ Throwable -> 0x003a }
            java.lang.String r1 = "com.baidu.android.pushservice.action.MESSAGE"
            boolean r1 = r0.equals(r1)     // Catch:{ Throwable -> 0x003a }
            if (r1 == 0) goto L_0x003c
            android.os.Bundle r0 = r8.getExtras()     // Catch:{ Throwable -> 0x003a }
            java.lang.String r1 = "message_string"
            java.lang.String r0 = r0.getString(r1)     // Catch:{ Throwable -> 0x003a }
            r6.onMessageReceived(r7, r0)     // Catch:{ Throwable -> 0x003a }
            goto L_0x0019
        L_0x003a:
            r0 = move-exception
            goto L_0x0019
        L_0x003c:
            java.lang.String r1 = "com.baidu.android.pushservice.action.notification.CLICK"
            boolean r1 = r0.equals(r1)     // Catch:{ Throwable -> 0x003a }
            if (r1 != 0) goto L_0x0019
            java.lang.String r1 = "com.baidu.android.pushservice.action.RECEIVE"
            boolean r1 = r0.equals(r1)     // Catch:{ Throwable -> 0x003a }
            if (r1 == 0) goto L_0x006d
            java.lang.String r0 = "content"
            byte[] r0 = r8.getByteArrayExtra(r0)     // Catch:{ Throwable -> 0x003a }
            if (r0 == 0) goto L_0x0065
            java.lang.String r0 = new java.lang.String     // Catch:{ Throwable -> 0x003a }
            java.lang.String r1 = "content"
            byte[] r1 = r8.getByteArrayExtra(r1)     // Catch:{ Throwable -> 0x003a }
            r0.<init>(r1)     // Catch:{ Throwable -> 0x003a }
            com.tendcloud.tenddata.zv$a r1 = com.tendcloud.tenddata.zv.a.baidu     // Catch:{ Throwable -> 0x003a }
            a(r7, r0, r1)     // Catch:{ Throwable -> 0x003a }
            goto L_0x0019
        L_0x0065:
            java.lang.String r0 = com.tendcloud.tenddata.zv.a     // Catch:{ Throwable -> 0x003a }
            java.lang.String r1 = "BAIDU push id is null"
            android.util.Log.e(r0, r1)     // Catch:{ Throwable -> 0x003a }
            goto L_0x0019
        L_0x006d:
            java.lang.String r1 = "cn.jpush.android.intent.REGISTRATION"
            boolean r1 = r0.equals(r1)     // Catch:{ Throwable -> 0x003a }
            if (r1 == 0) goto L_0x0085
            android.os.Bundle r0 = r8.getExtras()     // Catch:{ Throwable -> 0x003a }
            java.lang.String r1 = "cn.jpush.android.REGISTRATION_ID"
            java.lang.String r0 = r0.getString(r1)     // Catch:{ Throwable -> 0x003a }
            com.tendcloud.tenddata.zv$a r1 = com.tendcloud.tenddata.zv.a.jpush     // Catch:{ Throwable -> 0x003a }
            a(r7, r0, r1)     // Catch:{ Throwable -> 0x003a }
            goto L_0x0019
        L_0x0085:
            java.lang.String r1 = "cn.jpush.android.intent.MESSAGE_RECEIVED"
            boolean r1 = r0.equals(r1)     // Catch:{ Throwable -> 0x003a }
            if (r1 == 0) goto L_0x009c
            android.os.Bundle r0 = r8.getExtras()     // Catch:{ Throwable -> 0x003a }
            java.lang.String r1 = "cn.jpush.android.MESSAGE"
            java.lang.String r0 = r0.getString(r1)     // Catch:{ Throwable -> 0x003a }
            r6.onMessageReceived(r7, r0)     // Catch:{ Throwable -> 0x003a }
            goto L_0x0019
        L_0x009c:
            java.lang.String r1 = "com.talkingdata.notification.click"
            boolean r1 = r0.equals(r1)     // Catch:{ Throwable -> 0x003a }
            if (r1 == 0) goto L_0x00c1
            android.os.Bundle r0 = r8.getExtras()     // Catch:{ Throwable -> 0x003a }
            java.lang.String r1 = "id"
            r2 = 0
            int r1 = r0.getInt(r1, r2)     // Catch:{ Throwable -> 0x003a }
            java.lang.String r0 = "notification"
            java.lang.Object r0 = r7.getSystemService(r0)     // Catch:{ Throwable -> 0x003a }
            android.app.NotificationManager r0 = (android.app.NotificationManager) r0     // Catch:{ Throwable -> 0x003a }
            if (r1 == 0) goto L_0x00bc
            r0.cancel(r1)     // Catch:{ Throwable -> 0x003a }
        L_0x00bc:
            r6.onNofiticationClick(r7, r8)     // Catch:{ Throwable -> 0x003a }
            goto L_0x0019
        L_0x00c1:
            java.lang.String r1 = "com.talkingdata.notification.cancel"
            boolean r1 = r0.equals(r1)     // Catch:{ Throwable -> 0x003a }
            if (r1 == 0) goto L_0x00eb
            java.lang.String r0 = "sign"
            java.lang.String r0 = r8.getStringExtra(r0)     // Catch:{ Throwable -> 0x003a }
            android.os.Message r1 = android.os.Message.obtain()     // Catch:{ Throwable -> 0x003a }
            r2 = 103(0x67, float:1.44E-43)
            r1.what = r2     // Catch:{ Throwable -> 0x003a }
            com.tendcloud.tenddata.dg r2 = new com.tendcloud.tenddata.dg     // Catch:{ Throwable -> 0x003a }
            r3 = 0
            com.tendcloud.tenddata.dg$a r4 = com.tendcloud.tenddata.dg.a.CANCEL     // Catch:{ Throwable -> 0x003a }
            r5 = 0
            r2.<init>(r0, r3, r4, r5)     // Catch:{ Throwable -> 0x003a }
            r1.obj = r2     // Catch:{ Throwable -> 0x003a }
            android.os.Handler r0 = com.tendcloud.tenddata.dh.a()     // Catch:{ Throwable -> 0x003a }
            r0.sendMessage(r1)     // Catch:{ Throwable -> 0x003a }
            goto L_0x0019
        L_0x00eb:
            java.lang.String r1 = "com.talkingdata.message.click"
            boolean r1 = r0.equals(r1)     // Catch:{ Throwable -> 0x003a }
            if (r1 != 0) goto L_0x0019
            java.lang.String r1 = "com.igexin.sdk.action"
            boolean r1 = r0.startsWith(r1)     // Catch:{ Throwable -> 0x003a }
            if (r1 == 0) goto L_0x0129
            android.os.Bundle r0 = r8.getExtras()     // Catch:{ Throwable -> 0x003a }
            java.lang.String r1 = "action"
            int r1 = r0.getInt(r1)     // Catch:{ Throwable -> 0x003a }
            switch(r1) {
                case 10001: goto L_0x010a;
                case 10002: goto L_0x011c;
                default: goto L_0x0108;
            }     // Catch:{ Throwable -> 0x003a }
        L_0x0108:
            goto L_0x0019
        L_0x010a:
            java.lang.String r1 = "payload"
            byte[] r0 = r0.getByteArray(r1)     // Catch:{ Throwable -> 0x003a }
            if (r0 == 0) goto L_0x0019
            java.lang.String r1 = new java.lang.String     // Catch:{ Throwable -> 0x003a }
            r1.<init>(r0)     // Catch:{ Throwable -> 0x003a }
            r6.onMessageReceived(r7, r1)     // Catch:{ Throwable -> 0x003a }
            goto L_0x0019
        L_0x011c:
            java.lang.String r1 = "clientid"
            java.lang.String r0 = r0.getString(r1)     // Catch:{ Throwable -> 0x003a }
            com.tendcloud.tenddata.zv$a r1 = com.tendcloud.tenddata.zv.a.getui     // Catch:{ Throwable -> 0x003a }
            a(r7, r0, r1)     // Catch:{ Throwable -> 0x003a }
            goto L_0x0019
        L_0x0129:
            java.lang.String r1 = "android.talkingdata.action.media.SILENT"
            boolean r1 = r0.equals(r1)     // Catch:{ Throwable -> 0x003a }
            if (r1 == 0) goto L_0x0177
            java.lang.String r0 = "mpush_message_string"
            java.lang.String r0 = r8.getStringExtra(r0)     // Catch:{ Throwable -> 0x003a }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Throwable -> 0x0174 }
            r1.<init>(r0)     // Catch:{ Throwable -> 0x0174 }
            java.lang.String r0 = "app"
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Throwable -> 0x0174 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0174 }
            r2.<init>()     // Catch:{ Throwable -> 0x0174 }
            java.lang.String r3 = com.tendcloud.tenddata.dq.b     // Catch:{ Throwable -> 0x0174 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0174 }
            java.lang.String r3 = "-"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0174 }
            java.lang.String r3 = com.tendcloud.tenddata.ab.getPushAppId(r7)     // Catch:{ Throwable -> 0x0174 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0174 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0174 }
            boolean r0 = r0.equals(r2)     // Catch:{ Throwable -> 0x0174 }
            if (r0 == 0) goto L_0x0019
            boolean r0 = com.tendcloud.tenddata.dd.a     // Catch:{ Throwable -> 0x0174 }
            if (r0 != 0) goto L_0x0019
            java.lang.String r0 = "content"
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Throwable -> 0x0174 }
            r6.onMessageReceived(r7, r0)     // Catch:{ Throwable -> 0x0174 }
            goto L_0x0019
        L_0x0174:
            r0 = move-exception
            goto L_0x0019
        L_0x0177:
            java.lang.String r1 = "android.talkingdata.action.media.TD.TOKEN"
            boolean r0 = r0.equals(r1)     // Catch:{ Throwable -> 0x003a }
            if (r0 == 0) goto L_0x0019
            java.lang.String r0 = "mpush_token"
            java.lang.String r0 = r8.getStringExtra(r0)     // Catch:{ Throwable -> 0x003a }
            com.tendcloud.tenddata.zv$a r1 = com.tendcloud.tenddata.zv.a.nick     // Catch:{ Throwable -> 0x003a }
            a(r7, r0, r1)     // Catch:{ Throwable -> 0x003a }
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.zv.onReceive(android.content.Context, android.content.Intent):void");
    }
}
