package com.tendcloud.tenddata;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

public class zy extends Service {
    private static final String a = "PushLog";
    private Handler b = new Handler();
    private final IBinder c = new a();

    public class a extends Binder {
        public a() {
        }

        /* access modifiers changed from: package-private */
        public zy a() {
            return zy.this;
        }
    }

    public IBinder onBind(Intent intent) {
        return this.c;
    }

    public void onCreate() {
        super.onCreate();
        Context applicationContext = getApplicationContext();
        try {
            new ds(this, applicationContext).start();
            dy.commonInit(applicationContext);
            du.a(applicationContext).initPushSDK(null);
        } catch (Throwable th) {
            cs.e("PushLog", "create service err" + th.toString());
        }
    }

    public void onDestroy() {
        cs.a("PushLog", "onDestroy");
        du.a(getApplicationContext()).b();
        Intent intent = new Intent(dc.M);
        intent.putExtra(dc.y, dc.B);
        sendBroadcast(intent);
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        Context applicationContext = getApplicationContext();
        dt dtVar = new dt(this, applicationContext, intent);
        try {
            if (du.a(applicationContext).a()) {
                this.b.postDelayed(dtVar, 1500);
                return 1;
            }
            dtVar.run();
            return 1;
        } catch (Throwable th) {
            cs.e("PushLog", "start command err " + th.toString());
            return 1;
        }
    }
}
