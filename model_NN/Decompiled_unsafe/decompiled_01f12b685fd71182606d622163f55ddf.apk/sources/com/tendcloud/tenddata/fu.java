package com.tendcloud.tenddata;

import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import com.sg.pak.PAK_ASSETS;
import java.util.HashMap;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

final class fu {
    static final String b = "ev1.xdrig.com";
    static final String c = null;
    static final String d = "v1";
    static final String e = "https";
    static final String f = "https://ev1.xdrig.com/smsauth/v1/";
    private static final int g = 20000;
    private static long h = 0;
    private static TalkingDataSMSApplyCallback i = null;
    private static TalkingDataSMSVerifyCallback j = null;
    private static String k = "";
    private static volatile fu l = null;
    HashMap a = new HashMap();

    fu() {
    }

    static fu a() {
        if (l == null) {
            synchronized (fu.class) {
                if (l == null) {
                    l = new fu();
                }
            }
        }
        return l;
    }

    private void a(String str, int i2, String str2) {
        if (str.equals("verify") && j != null) {
            a(str, i2, str2, j);
            j = null;
        } else if (str.equals("apply") && i != null) {
            a(str, i2, str2, i);
            i = null;
        }
    }

    private void a(String str, int i2, String str2, TalkingDataSMSApplyCallback talkingDataSMSApplyCallback) {
        try {
            if (TalkingDataSMS.c != null) {
                TalkingDataSMS.c.post(new fv(this, str, talkingDataSMSApplyCallback, i2, str2));
            } else {
                Log.e("TalkingDataSDK", "The method of initSMS has not be called or init failed.");
            }
        } catch (Throwable th) {
        }
    }

    private void a(String str, int i2, String str2, TalkingDataSMSVerifyCallback talkingDataSMSVerifyCallback) {
        try {
            if (TalkingDataSMS.c != null) {
                TalkingDataSMS.c.post(new fw(this, str, talkingDataSMSVerifyCallback, i2, str2));
            } else {
                Log.e("TalkingDataSDK", "The method of initSMS has not be called or init failed.");
            }
        } catch (Throwable th) {
        }
    }

    private boolean c() {
        return Pattern.compile("[0-9]*").matcher(this.a.get("mobile").toString()).matches() && Pattern.compile("[0-9]*").matcher(this.a.get("countryCode").toString()).matches();
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] d() {
        /*
            r7 = this;
            r0 = 0
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Throwable -> 0x009b }
            java.lang.String r2 = "JSON"
            java.lang.String r2 = com.tendcloud.tenddata.fx.a(r2)     // Catch:{ Throwable -> 0x009b }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x009b }
            java.lang.String r2 = "action"
            org.json.JSONObject r2 = r1.getJSONObject(r2)     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r3 = "domain"
            java.lang.String r4 = "sms"
            r2.put(r3, r4)     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r3 = "name"
            java.lang.String r4 = com.tendcloud.tenddata.fu.k     // Catch:{ Throwable -> 0x00b0 }
            r2.put(r3, r4)     // Catch:{ Throwable -> 0x00b0 }
            java.util.HashMap r3 = new java.util.HashMap     // Catch:{ Throwable -> 0x00b0 }
            r3.<init>()     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r4 = "mobile"
            java.util.HashMap r5 = r7.a     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r6 = "mobile"
            java.lang.Object r5 = r5.get(r6)     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x00b0 }
            r3.put(r4, r5)     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r4 = "secretId"
            java.lang.String r5 = com.tendcloud.tenddata.fx.j     // Catch:{ Throwable -> 0x00b0 }
            r3.put(r4, r5)     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r4 = "countryCode"
            java.util.HashMap r5 = r7.a     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r6 = "countryCode"
            java.lang.Object r5 = r5.get(r6)     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x00b0 }
            r3.put(r4, r5)     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r4 = com.tendcloud.tenddata.fu.k     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r5 = "verify"
            boolean r4 = r4.equals(r5)     // Catch:{ Throwable -> 0x00b0 }
            if (r4 == 0) goto L_0x0069
            java.lang.String r4 = "securityCode"
            java.util.HashMap r5 = r7.a     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r6 = "securityCode"
            java.lang.Object r5 = r5.get(r6)     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x00b0 }
            r3.put(r4, r5)     // Catch:{ Throwable -> 0x00b0 }
        L_0x0069:
            java.lang.String r4 = com.tendcloud.tenddata.fu.k     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r5 = "apply"
            boolean r4 = r4.equals(r5)     // Catch:{ Throwable -> 0x00b0 }
            if (r4 == 0) goto L_0x008e
            java.util.HashMap r4 = r7.a     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r5 = "transactionId"
            java.lang.Object r4 = r4.get(r5)     // Catch:{ Throwable -> 0x00b0 }
            if (r4 == 0) goto L_0x008e
            java.lang.String r4 = "transactionId"
            java.util.HashMap r5 = r7.a     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r6 = "transactionId"
            java.lang.Object r5 = r5.get(r6)     // Catch:{ Throwable -> 0x00b0 }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x00b0 }
            r3.put(r4, r5)     // Catch:{ Throwable -> 0x00b0 }
        L_0x008e:
            java.lang.String r4 = "data"
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Throwable -> 0x00b0 }
            r5.<init>(r3)     // Catch:{ Throwable -> 0x00b0 }
            r2.put(r4, r5)     // Catch:{ Throwable -> 0x00b0 }
        L_0x0098:
            if (r1 != 0) goto L_0x00a7
        L_0x009a:
            return r0
        L_0x009b:
            r1 = move-exception
            r1 = r0
        L_0x009d:
            java.lang.String r2 = com.tendcloud.tenddata.fu.k
            r3 = 614(0x266, float:8.6E-43)
            java.lang.String r4 = "SMS SDK inner error"
            r7.a(r2, r3, r4)
            goto L_0x0098
        L_0x00a7:
            java.lang.String r0 = r1.toString()
            byte[] r0 = r0.getBytes()
            goto L_0x009a
        L_0x00b0:
            r2 = move-exception
            goto L_0x009d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.fu.d():byte[]");
    }

    /* access modifiers changed from: package-private */
    public void a(Bundle bundle) {
        fx.a();
        fx.b();
        fx.c();
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("appKey", fx.i);
            jSONObject.put("service", "sms");
            fx.e.put("appKey", new JSONArray().put(jSONObject));
        } catch (Throwable th) {
        }
    }

    public void b() {
        TalkingDataSMSVerifyCallback talkingDataSMSVerifyCallback;
        TalkingDataSMSApplyCallback talkingDataSMSApplyCallback = null;
        try {
            if (!c()) {
                Log.d("TalkingDataSDK", "The parameter should be number.");
                return;
            }
            k = this.a.get(dc.Y).toString();
            if (k.equals("apply")) {
                talkingDataSMSApplyCallback = (TalkingDataSMSApplyCallback) this.a.get("callback");
                talkingDataSMSVerifyCallback = null;
            } else {
                talkingDataSMSVerifyCallback = k.equals("verify") ? (TalkingDataSMSVerifyCallback) this.a.get("callback") : null;
            }
            if (k.equals("apply") && System.currentTimeMillis() - h <= 60000) {
                a(k, (int) PAK_ASSETS.IMG_K06, "Only one apply request can be handled in 60s.", talkingDataSMSApplyCallback);
            } else if (!k.equals("verify") || j == null) {
                if (k.equals("apply")) {
                    i = talkingDataSMSApplyCallback;
                } else if (k.equals("verify")) {
                    j = talkingDataSMSVerifyCallback;
                }
                byte[] d2 = d();
                if (d2 == null) {
                    throw new Exception("SMS SDK inner error.");
                }
                if (k.equals("apply")) {
                    h = System.currentTimeMillis();
                }
                Message message = new Message();
                Bundle bundle = new Bundle();
                bundle.putString(dc.Y, k);
                if (k.equals("verify")) {
                    bundle.putInt("timeout", g);
                }
                message.setData(bundle);
                message.obj = d2;
                ga.a.sendMessage(message);
            } else {
                a(k, (int) PAK_ASSETS.IMG_K06A, "The verification request is too frequent, please try again later.", talkingDataSMSVerifyCallback);
            }
        } catch (Throwable th) {
            a(k, PAK_ASSETS.IMG_LGM02, "SMS SDK inner error.");
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void onResponse(android.os.Bundle r6) {
        /*
            r5 = this;
            r4 = 200(0xc8, float:2.8E-43)
            monitor-enter(r5)
            java.lang.String r0 = "action"
            java.lang.String r0 = r6.getString(r0)     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            java.lang.String r1 = "SMS"
            boolean r0 = r0.equals(r1)     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            if (r0 == 0) goto L_0x0054
            java.lang.String r0 = "status"
            int r2 = r6.getInt(r0)     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            if (r2 == r4) goto L_0x0023
            java.lang.String r0 = com.tendcloud.tenddata.fu.k     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            java.lang.String r1 = "apply"
            boolean r0 = r0.equals(r1)     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            if (r0 != 0) goto L_0x002f
        L_0x0023:
            java.lang.String r0 = com.tendcloud.tenddata.fu.k     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            java.lang.String r1 = "verify"
            boolean r0 = r0.equals(r1)     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            if (r0 == 0) goto L_0x0033
            if (r2 != r4) goto L_0x0033
        L_0x002f:
            r0 = 0
            com.tendcloud.tenddata.fu.h = r0     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
        L_0x0033:
            java.lang.String r0 = "message"
            java.lang.String r3 = r6.getString(r0)     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            r1 = 0
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x0056 }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0056 }
        L_0x003f:
            if (r2 == r4) goto L_0x006e
            java.lang.String r1 = "errorMessage"
            boolean r1 = r0.has(r1)     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            if (r1 == 0) goto L_0x006e
            r1 = 600(0x258, float:8.41E-43)
            if (r2 != r1) goto L_0x0060
            java.lang.String r0 = com.tendcloud.tenddata.fu.k     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            java.lang.String r1 = "SMS SDK inner error."
            r5.a(r0, r2, r1)     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
        L_0x0054:
            monitor-exit(r5)
            return
        L_0x0056:
            r0 = move-exception
            java.lang.String r0 = com.tendcloud.tenddata.fu.k     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            java.lang.String r3 = "SMS SDK inner error."
            r5.a(r0, r2, r3)     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            r0 = r1
            goto L_0x003f
        L_0x0060:
            java.lang.String r1 = com.tendcloud.tenddata.fu.k     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            java.lang.String r3 = "errorMessage"
            java.lang.String r0 = r0.getString(r3)     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            r5.a(r1, r2, r0)     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            goto L_0x0054
        L_0x006c:
            r0 = move-exception
            goto L_0x0054
        L_0x006e:
            if (r2 != r4) goto L_0x0054
            java.lang.String r1 = "transactionId"
            boolean r1 = r0.has(r1)     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            if (r1 == 0) goto L_0x0054
            java.lang.String r1 = com.tendcloud.tenddata.fu.k     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            java.lang.String r3 = "transactionId"
            java.lang.String r0 = r0.getString(r3)     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            r5.a(r1, r2, r0)     // Catch:{ Throwable -> 0x006c, all -> 0x0084 }
            goto L_0x0054
        L_0x0084:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0086 }
        L_0x0086:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.fu.onResponse(android.os.Bundle):void");
    }
}
