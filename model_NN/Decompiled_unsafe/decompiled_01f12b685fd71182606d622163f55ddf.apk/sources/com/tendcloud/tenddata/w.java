package com.tendcloud.tenddata;

public class w extends RuntimeException {
    private static final long a = -6468967874576651628L;

    public w() {
    }

    public w(String str) {
        super(str);
    }

    public w(String str, Throwable th) {
        super(str, th);
    }

    public w(Throwable th) {
        super(th);
    }
}
