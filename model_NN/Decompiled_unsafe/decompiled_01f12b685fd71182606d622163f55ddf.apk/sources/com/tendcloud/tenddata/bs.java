package com.tendcloud.tenddata;

import android.content.BroadcastReceiver;
import android.content.Context;

final class bs extends BroadcastReceiver {
    final /* synthetic */ Object a;
    final /* synthetic */ Context b;

    bs(Object obj, Context context) {
        this.a = obj;
        this.b = context;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r4, android.content.Intent r5) {
        /*
            r3 = this;
            java.lang.Object r1 = r3.a     // Catch:{ Throwable -> 0x0019 }
            monitor-enter(r1)     // Catch:{ Throwable -> 0x0019 }
            java.lang.Object r0 = r3.a     // Catch:{ Throwable -> 0x000f, all -> 0x001b }
            r0.notifyAll()     // Catch:{ Throwable -> 0x000f, all -> 0x001b }
            android.content.Context r0 = r3.b     // Catch:{ all -> 0x0016 }
            r0.unregisterReceiver(r3)     // Catch:{ all -> 0x0016 }
        L_0x000d:
            monitor-exit(r1)     // Catch:{ all -> 0x0016 }
        L_0x000e:
            return
        L_0x000f:
            r0 = move-exception
            android.content.Context r0 = r3.b     // Catch:{ all -> 0x0016 }
            r0.unregisterReceiver(r3)     // Catch:{ all -> 0x0016 }
            goto L_0x000d
        L_0x0016:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0016 }
            throw r0     // Catch:{ Throwable -> 0x0019 }
        L_0x0019:
            r0 = move-exception
            goto L_0x000e
        L_0x001b:
            r0 = move-exception
            android.content.Context r2 = r3.b     // Catch:{ all -> 0x0016 }
            r2.unregisterReceiver(r3)     // Catch:{ all -> 0x0016 }
            throw r0     // Catch:{ all -> 0x0016 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.bs.onReceive(android.content.Context, android.content.Intent):void");
    }
}
