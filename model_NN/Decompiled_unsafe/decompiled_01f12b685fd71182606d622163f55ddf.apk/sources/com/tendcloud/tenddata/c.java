package com.tendcloud.tenddata;

import com.tendcloud.tenddata.d;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;

public class c {
    public static boolean a(g gVar, ByteChannel byteChannel) {
        i iVar;
        ByteBuffer byteBuffer = (ByteBuffer) gVar.h.peek();
        if (byteBuffer != null) {
            do {
                byteChannel.write(byteBuffer);
                if (byteBuffer.remaining() > 0) {
                    return false;
                }
                gVar.h.poll();
                byteBuffer = (ByteBuffer) gVar.h.peek();
            } while (byteBuffer != null);
            iVar = null;
        } else if (byteChannel instanceof i) {
            iVar = (i) byteChannel;
            if (iVar.a()) {
                iVar.b();
            }
        } else {
            iVar = null;
        }
        if (gVar.h.isEmpty() && gVar.h() && gVar.j().d() == d.b.SERVER) {
            synchronized (gVar) {
                gVar.m();
            }
        }
        if (iVar != null) {
            return !((i) byteChannel).a();
        }
        return true;
    }

    public static boolean a(ByteBuffer byteBuffer, g gVar, i iVar) {
        byteBuffer.clear();
        int a = iVar.a(byteBuffer);
        byteBuffer.flip();
        if (a != -1) {
            return iVar.c();
        }
        gVar.n();
        return false;
    }

    public static boolean a(ByteBuffer byteBuffer, g gVar, ByteChannel byteChannel) {
        byteBuffer.clear();
        int read = byteChannel.read(byteBuffer);
        byteBuffer.flip();
        if (read != -1) {
            return read != 0;
        }
        gVar.n();
        return false;
    }
}
