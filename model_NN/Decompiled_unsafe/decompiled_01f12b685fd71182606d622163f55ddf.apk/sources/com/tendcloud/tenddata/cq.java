package com.tendcloud.tenddata;

import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

class cq {
    private static final String a = "PushLog";
    private static final String b = "growls523?19:coudiest";
    private static final String c = "AES/CBC/PKCS5Padding";
    private static final byte[] d = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    private cq() {
    }

    private static SecretKeySpec a(String str) {
        return new SecretKeySpec(cv.b(str + b), "AES");
    }

    static byte[] a(String str, byte[] bArr) {
        try {
            SecretKeySpec a2 = a(str);
            Cipher instance = Cipher.getInstance(c);
            instance.init(1, a2, new IvParameterSpec(d));
            byte[] doFinal = instance.doFinal(bArr);
            byte[] bArr2 = new byte[(doFinal.length + d.length)];
            System.arraycopy(d, 0, bArr2, 0, d.length);
            System.arraycopy(doFinal, 0, bArr2, d.length, doFinal.length);
            return bArr2;
        } catch (Throwable th) {
            throw new GeneralSecurityException(th);
        }
    }

    static byte[] b(String str, byte[] bArr) {
        try {
            SecretKeySpec a2 = a(str);
            if (bArr.length <= d.length) {
                throw new RuntimeException("bad data to decryption");
            }
            byte[] bArr2 = new byte[d.length];
            System.arraycopy(bArr, 0, bArr2, 0, bArr2.length);
            byte[] bArr3 = new byte[(bArr.length - d.length)];
            System.arraycopy(bArr, bArr2.length, bArr3, 0, bArr3.length);
            Cipher instance = Cipher.getInstance(c);
            instance.init(2, a2, new IvParameterSpec(bArr2));
            return instance.doFinal(bArr3);
        } catch (Throwable th) {
            throw new GeneralSecurityException(th);
        }
    }
}
