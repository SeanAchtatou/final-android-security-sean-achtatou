package com.tendcloud.tenddata;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;

final class av extends ObjectInputStream {
    final /* synthetic */ ClassLoader a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    av(InputStream inputStream, ClassLoader classLoader) {
        super(inputStream);
        this.a = classLoader;
    }

    public Class resolveClass(ObjectStreamClass objectStreamClass) {
        Class<?> cls = Class.forName(objectStreamClass.getName(), false, this.a);
        return cls == null ? super.resolveClass(objectStreamClass) : cls;
    }
}
