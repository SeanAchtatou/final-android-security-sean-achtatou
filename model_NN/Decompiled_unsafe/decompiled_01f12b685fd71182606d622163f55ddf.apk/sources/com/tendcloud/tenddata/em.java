package com.tendcloud.tenddata;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

@TargetApi(14)
class em implements Application.ActivityLifecycleCallbacks {
    dz a;

    /* access modifiers changed from: package-private */
    public void a(dz dzVar) {
        this.a = dzVar;
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
        gd.b(activity, activity.getLocalClassName(), false);
        if (this.a != null) {
            this.a.b(activity);
            if (this.a.b()) {
                fo.a().d();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.gd.a(android.app.Activity, java.lang.String, boolean):void
     arg types: [android.app.Activity, java.lang.String, int]
     candidates:
      com.tendcloud.tenddata.gd.a(double, double, java.lang.String):void
      com.tendcloud.tenddata.gd.a(android.app.Activity, java.lang.String, java.lang.String):void
      com.tendcloud.tenddata.gd.a(android.content.Context, java.lang.String, int):void
      com.tendcloud.tenddata.gd.a(android.content.Context, java.lang.String, java.lang.String):void
      com.tendcloud.tenddata.gd.a(android.app.Activity, java.lang.String, boolean):void */
    public void onActivityResumed(Activity activity) {
        gd.a(activity, activity.getLocalClassName(), false);
        fo.a().b();
        fo.a().c();
        if (this.a != null) {
            this.a.a(activity);
        }
        fm.a();
        ab.h = true;
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
    }
}
