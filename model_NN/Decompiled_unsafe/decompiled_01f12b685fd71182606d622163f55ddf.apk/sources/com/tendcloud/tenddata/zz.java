package com.tendcloud.tenddata;

import android.app.Activity;
import android.content.Context;
import java.util.Map;
import java.util.Random;

public final class zz implements ao {
    private static volatile zz e = null;
    private final String a = "u.talkingdata.net";
    private final String b = "https";
    private final String c = "https://u.talkingdata.net/ota/a/TD/android/ver";
    private final String d = "https://u.talkingdata.net/ota/a/TD/android/sdk.zip";

    public zz() {
        e = this;
    }

    static synchronized zz f() {
        zz zzVar;
        synchronized (zz.class) {
            if (e == null) {
                synchronized (zz.class) {
                    if (e == null) {
                        e = new zz();
                    }
                }
            }
            zzVar = e;
        }
        return zzVar;
    }

    public String a() {
        return "+V2.2.46 gp";
    }

    public void a(Activity activity) {
        try {
            gd.a(activity);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void a(Context context) {
        try {
            a(context, null, null);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void a(Context context, String str, String str2) {
        try {
            gd.a(context, str, str2);
            ft.a();
            fo.a();
            ca.execute(new en(this));
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void a(Context context, String str, String str2, Map map) {
        try {
            gd.a(context, str, str2, map);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void a(Context context, Throwable th) {
        try {
            gd.a(context, th);
        } catch (Throwable th2) {
            th2.printStackTrace();
        }
    }

    public void a(String str) {
        bu.a(ab.mContext, "TDpref_longtime", "TDpref.last.sdk.check", System.currentTimeMillis());
    }

    public String b(Context context) {
        try {
            return gd.a(context);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public void b(Activity activity) {
        try {
            gd.b(activity);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public boolean b() {
        long abs = Math.abs((System.currentTimeMillis() - bu.b(ab.mContext, "TDpref_longtime", "TDpref.last.sdk.check", bj.a().e(ab.mContext))) / 86400000);
        if (abs <= 7) {
            return ((float) abs) * new Random().nextFloat() > 2.0f;
        }
    }

    public String c() {
        return "https://u.talkingdata.net/ota/a/TD/android/ver";
    }

    public void c(boolean z) {
        try {
            ab.b = z;
            if (TCAgent.LOG_ON) {
                ev.a("[PreSettings] setReportUncaughtExceptions: " + z);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public String d() {
        return "https://u.talkingdata.net/ota/a/TD/android/sdk.zip";
    }

    public void e() {
    }

    public void initialize(Context context, String str) {
        if (context != null) {
            ab.mContext = context;
        }
    }

    public void onPageEnd(Context context, String str) {
        try {
            gd.b(context, str);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void onPageStart(Context context, String str) {
        try {
            gd.a(context, str);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void onResume(Activity activity, String str, String str2) {
        try {
            gd.a(activity, str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void setLocation(double d2, double d3, String str) {
        try {
            gd.a(d2, d3, str);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
