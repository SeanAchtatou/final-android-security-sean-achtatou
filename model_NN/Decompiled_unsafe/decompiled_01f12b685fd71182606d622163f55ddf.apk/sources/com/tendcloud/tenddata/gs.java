package com.tendcloud.tenddata;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

final class gs {
    private static final Map a = new ConcurrentHashMap();

    private gs() {
    }

    static Map a(Object obj) {
        HashMap hashMap = new HashMap();
        try {
            Class<?> cls = obj.getClass();
            if (!a.containsKey(cls)) {
                a((Class) cls);
            }
            Map map = (Map) a.get(cls);
            if (map != null && !map.isEmpty()) {
                for (Map.Entry entry : map.entrySet()) {
                    HashSet hashSet = new HashSet();
                    for (Method gtVar : (Set) entry.getValue()) {
                        hashSet.add(new gt(obj, gtVar));
                    }
                    hashMap.put(entry.getKey(), hashSet);
                }
            }
        } catch (Throwable th) {
        }
        return hashMap;
    }

    private static void a(Class cls) {
        HashMap hashMap = new HashMap();
        for (Method method : cls.getDeclaredMethods()) {
            if (method.getName().startsWith("onTDEBEvent") && method.getParameterTypes().length == 1) {
                Class<?>[] parameterTypes = method.getParameterTypes();
                if (parameterTypes.length != 1) {
                    throw new IllegalArgumentException("Method " + method + " must have one and only one argument.");
                }
                Class<?> cls2 = parameterTypes[0];
                if (cls2.isInterface()) {
                    throw new IllegalArgumentException("Method " + method + " must have a argument whose type is a class which can be instantialized.");
                } else if ((method.getModifiers() & 1) == 0) {
                    throw new IllegalArgumentException("Method " + method + " must be 'public'.");
                } else {
                    Object obj = (Set) hashMap.get(cls2);
                    if (obj == null) {
                        obj = new HashSet();
                        hashMap.put(cls2, obj);
                    }
                    obj.add(method);
                }
            }
        }
        a.put(cls, hashMap);
    }
}
