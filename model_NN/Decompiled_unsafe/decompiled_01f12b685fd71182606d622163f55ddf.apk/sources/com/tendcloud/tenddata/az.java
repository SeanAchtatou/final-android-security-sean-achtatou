package com.tendcloud.tenddata;

public abstract class az extends bf {
    protected bb a;

    /* access modifiers changed from: protected */
    public int a() {
        if (this.a == null) {
            return 0;
        }
        int i = 0;
        for (int i2 = 0; i2 < this.a.a(); i2++) {
            i += this.a.a(i2).a();
        }
        return i;
    }

    /* renamed from: b */
    public az clone() {
        az azVar = (az) super.clone();
        bd.a(this, azVar);
        return azVar;
    }

    public void writeTo(ay ayVar) {
        if (this.a != null) {
            for (int i = 0; i < this.a.a(); i++) {
                this.a.a(i).a(ayVar);
            }
        }
    }
}
