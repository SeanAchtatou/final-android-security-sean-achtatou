package com.tendcloud.tenddata;

public class r extends Exception {
    private static final long a = 3731842424390998726L;
    private int b;

    public r(int i) {
        this.b = i;
    }

    public r(int i, String str) {
        super(str);
        this.b = i;
    }

    public r(int i, String str, Throwable th) {
        super(str, th);
        this.b = i;
    }

    public r(int i, Throwable th) {
        super(th);
        this.b = i;
    }

    public int a() {
        return this.b;
    }
}
