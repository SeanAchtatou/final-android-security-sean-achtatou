package com.tendcloud.tenddata;

import android.content.Context;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Looper;

public class bw {
    private static volatile bw b = null;
    private static final long g = 10000;
    private static final int h = 10;
    /* access modifiers changed from: private */
    public Context a = null;
    private final int c = 250;
    private final int d = 18;
    /* access modifiers changed from: private */
    public long e = 0;
    private final int f = 5;
    /* access modifiers changed from: private */
    public SensorManager i;
    /* access modifiers changed from: private */
    public a j = null;
    private Handler k = new bx(this, Looper.getMainLooper());
    /* access modifiers changed from: private */
    public SensorEventListener l = new by(this);

    public interface a {
        void a();
    }

    private bw(Context context) {
        try {
            this.a = context;
            this.i = (SensorManager) context.getSystemService("sensor");
            this.i.registerListener(this.l, this.i.getDefaultSensor(1), 1);
            this.k.sendEmptyMessageDelayed(10, g);
        } catch (Throwable th) {
        }
    }

    public static bw a(Context context) {
        if (b == null) {
            synchronized (bw.class) {
                if (b == null) {
                    b = new bw(context);
                }
            }
        }
        return b;
    }

    public void registerTestDeviceListener(a aVar) {
        this.j = aVar;
    }
}
