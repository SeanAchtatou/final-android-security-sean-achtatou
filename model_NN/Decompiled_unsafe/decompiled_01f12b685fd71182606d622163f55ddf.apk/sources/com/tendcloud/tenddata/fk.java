package com.tendcloud.tenddata;

import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import cn.egame.terminal.sdk.EgameCoreReceiver;
import com.tendcloud.tenddata.ff;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import org.json.JSONArray;

class fk {
    private static final int a = 20480;
    private static final int b = 7200000;
    private static ff.d c = null;
    private static ff.e d = null;

    fk() {
    }

    static ff.f a(el elVar) {
        boolean z;
        int i;
        Context context = ab.mContext;
        ff.f fVar = new ff.f();
        fVar.a = bm.a(context);
        fVar.b = ab.m;
        fVar.c = b();
        fVar.d = c();
        int a2 = 0 + fVar.a() + 3;
        if (e()) {
            ff.i iVar = new ff.i();
            iVar.a = 1;
            iVar.c = a();
            fVar.e.add(iVar);
            int a3 = a2 + iVar.c.a() + fh.c(iVar.a);
            z = true;
            i = a3;
        } else {
            z = false;
            i = a2;
        }
        elVar.a();
        fVar.h = elVar.e("error_report");
        List<ff.j> e = elVar.e();
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        int i3 = i;
        for (ff.j jVar : e) {
            int i4 = i2 + 1;
            jVar.h = elVar.a(jVar.a, fVar.f);
            jVar.i = elVar.b(jVar.a, fVar.g);
            boolean z2 = false;
            if (jVar.i != null) {
                Iterator it = jVar.i.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (!((ff.b) it.next()).a.startsWith("__")) {
                            z2 = true;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            ff.i iVar2 = new ff.i();
            iVar2.a = 2;
            iVar2.b = jVar;
            int a4 = jVar.a();
            if (a4 + i3 > a && i4 != 1) {
                break;
            }
            int i5 = a4 + i3;
            arrayList.add(jVar);
            if (SystemClock.elapsedRealtime() - ab.g < EgameCoreReceiver.PUSK_USER_ACTION_DEFAULT_INTERVAL_TIME && jVar.c == 2 && jVar.h.size() == 0) {
                if (jVar.i.size() == 0) {
                    i3 = i5;
                    i2 = i4;
                } else if (!z2 && !gd.c) {
                    i3 = i5;
                    i2 = i4;
                }
            }
            fVar.e.add(iVar2);
            i3 = i5;
            i2 = i4;
        }
        fVar.f = elVar.a(arrayList);
        fVar.g = elVar.b(arrayList);
        if (fVar.h > 0) {
            for (ff.i add : elVar.d(fVar.h)) {
                fVar.e.add(add);
            }
        }
        elVar.b();
        if (z || fVar.e.size() != 0) {
            return fVar;
        }
        return null;
    }

    static ff.g a() {
        Context context = ab.mContext;
        ff.g gVar = new ff.g();
        String[] k = bn.k();
        try {
            gVar.a = k[0];
            try {
                gVar.b = Integer.valueOf(k[1]).intValue();
            } catch (Throwable th) {
            }
            gVar.d = k[2];
            try {
                gVar.c = Float.valueOf(k[3]).floatValue();
            } catch (Throwable th2) {
            }
        } catch (Exception e) {
        }
        int[] m = bn.m();
        gVar.g = m[0];
        gVar.h = m[1];
        try {
            int[] n = bn.n();
            gVar.i = n[0];
            gVar.j = n[1];
            gVar.k = n[2];
            gVar.l = n[3];
        } catch (Throwable th3) {
        }
        gVar.m = bn.o();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        gVar.n = ((float) displayMetrics.widthPixels) / displayMetrics.xdpi;
        gVar.o = ((float) displayMetrics.heightPixels) / displayMetrics.ydpi;
        gVar.p = displayMetrics.densityDpi;
        gVar.q = Build.DISPLAY;
        gVar.r = "unknown";
        try {
            gVar.r = (String) Class.forName("android.os.SystemProperties").getDeclaredMethod("get", String.class).invoke(null, "gsm.version.baseband");
        } catch (Exception e2) {
        }
        String c2 = bm.c(context);
        if (c2 != null) {
            gVar.s = c2;
        }
        String f = bm.f(context);
        if (f != null) {
            gVar.t = f;
        }
        try {
            gVar.y = bm.e(context);
            gVar.A = bm.d(context);
            gVar.B = bm.b(context);
        } catch (Exception e3) {
        }
        return gVar;
    }

    static void a(ff.f fVar, el elVar) {
        elVar.a();
        List<ff.i> list = fVar.e;
        elVar.a(fVar.f);
        elVar.b(fVar.g);
        elVar.c(fVar.h);
        for (ff.i iVar : list) {
            switch (iVar.a) {
                case 1:
                    ew.a(false);
                    break;
                case 2:
                    ff.j jVar = iVar.b;
                    if (jVar.c != 1) {
                        if (jVar.c != 3) {
                            break;
                        } else {
                            elVar.b(jVar.a);
                            elVar.c(jVar.a);
                            elVar.d(jVar.a);
                            break;
                        }
                    } else {
                        elVar.a(jVar.a);
                        break;
                    }
            }
        }
        elVar.b();
    }

    private static synchronized ff.d b() {
        ff.d dVar;
        synchronized (fk.class) {
            if (c != null) {
                dVar = c;
            } else if (ab.mContext == null) {
                dVar = null;
            } else {
                c = new ff.d();
                c.a = ab.mContext.getPackageName();
                c.b = ew.o();
                c.c = String.valueOf(ew.n());
                c.d = ew.g();
                c.e = "Android+TD+V2.2.46 gp";
                c.f = ab.u;
                c.h = bj.a().d(ab.mContext);
                c.i = bj.a().e(ab.mContext);
                dVar = c;
            }
        }
        return dVar;
    }

    private static synchronized ff.e c() {
        ff.e eVar;
        synchronized (fk.class) {
            if (d == null) {
                if (ab.mContext == null) {
                    eVar = null;
                } else {
                    d = new ff.e();
                    d.s = bm.h(ab.mContext);
                    d.a = bn.f();
                    d.b = String.valueOf(bn.g());
                    d.d = Build.CPU_ABI;
                    d.e = bn.a(ab.mContext);
                    d.f = bn.j();
                    d.g = br.p(ab.mContext);
                    d.h = bn.i();
                    d.i = ((TimeZone.getDefault().getRawOffset() / 1000) / 60) / 60;
                    d.j = "Android+" + Build.VERSION.RELEASE;
                    d.r = System.currentTimeMillis() - SystemClock.elapsedRealtime();
                }
            }
            d();
            eVar = d;
        }
        return eVar;
    }

    private static void d() {
        Location location = null;
        for (Location location2 : bz.a(ab.mContext)) {
            if (location != null && location2.getTime() <= location.getTime()) {
                location2 = location;
            }
            location = location2;
        }
        ff.h hVar = new ff.h();
        if (location != null) {
            hVar.b = location.getLatitude();
            hVar.a = location.getLongitude();
        }
        d.c = hVar;
        d.k = br.g(ab.mContext) ? 0 : 1;
        d.l = br.i(ab.mContext);
        d.o = br.k(ab.mContext);
        d.n = br.l(ab.mContext);
        d.p = bz.b(ab.mContext);
        d.t = br.r(ab.mContext).toString();
        JSONArray d2 = bz.d(ab.mContext);
        if (e() || new Random().nextInt(100) > 90) {
            d.u = d2 == null ? "" : d2.toString();
        }
    }

    private static synchronized boolean e() {
        boolean z;
        synchronized (fk.class) {
            z = bu.b(ab.mContext, "TDpref_longtime", "TDpref.profile.key", 1) != 0;
        }
        return z;
    }
}
