package com.tendcloud.tenddata;

public final class bi {
    static final int a = 0;
    static final int b = 1;
    static final int c = 2;
    static final int d = 3;
    static final int e = 4;
    static final int f = 5;
    static final int g = 3;
    static final int h = 7;
    public static final byte[] i = new byte[0];

    private bi() {
    }

    static int a(int i2) {
        return i2 & 7;
    }

    static int a(int i2, int i3) {
        return (i2 << 3) | i3;
    }

    public static boolean a(ax axVar, int i2) {
        return axVar.a(i2);
    }

    public static int b(int i2) {
        return i2 >>> 3;
    }
}
