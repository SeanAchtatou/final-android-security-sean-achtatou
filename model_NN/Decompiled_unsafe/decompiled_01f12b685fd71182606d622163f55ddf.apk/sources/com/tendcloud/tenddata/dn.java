package com.tendcloud.tenddata;

interface dn {

    public static final class a extends bf {
        private static volatile a[] c;
        public String a;

        public a() {
            f();
        }

        public static a[] b() {
            if (c == null) {
                synchronized (bd.a) {
                    if (c == null) {
                        c = new a[0];
                    }
                }
            }
            return c;
        }

        /* access modifiers changed from: protected */
        public int a() {
            return super.a() + ay.b(1, this.a);
        }

        /* renamed from: b */
        public a a(ax axVar) {
            while (true) {
                int a2 = axVar.a();
                switch (a2) {
                    case 0:
                        break;
                    case 10:
                        this.a = axVar.k();
                        break;
                    default:
                        if (bi.a(axVar, a2)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public a f() {
            this.a = "";
            this.b = -1;
            return this;
        }

        public void writeTo(ay ayVar) {
            ayVar.a(1, this.a);
            super.writeTo(ayVar);
        }
    }

    public static final class b extends bf {
        public static final int a = 0;
        public static final int c = 1;
        public String d;
        public String e;
        public int f;
        public String g;
        public byte[] h;
        public long i;

        public b() {
            b();
        }

        public static b a(byte[] bArr) {
            return (b) bf.a(new b(), bArr);
        }

        /* access modifiers changed from: protected */
        public int a() {
            int a2 = super.a() + ay.b(1, this.d) + ay.b(2, this.e) + ay.b(3, this.f);
            if (!this.g.equals("")) {
                a2 += ay.b(4, this.g);
            }
            return a2 + ay.b(5, this.h) + ay.c(6, this.i);
        }

        public b b() {
            this.d = "";
            this.e = "";
            this.f = 0;
            this.g = "";
            this.h = bi.i;
            this.i = 0;
            this.b = -1;
            return this;
        }

        /* renamed from: b */
        public b a(ax axVar) {
            while (true) {
                int a2 = axVar.a();
                switch (a2) {
                    case 0:
                        break;
                    case 10:
                        this.d = axVar.k();
                        break;
                    case 18:
                        this.e = axVar.k();
                        break;
                    case 24:
                        int g2 = axVar.g();
                        switch (g2) {
                            case 0:
                            case 1:
                                this.f = g2;
                                continue;
                        }
                    case 34:
                        this.g = axVar.k();
                        break;
                    case 42:
                        this.h = axVar.l();
                        break;
                    case 48:
                        this.i = axVar.f();
                        break;
                    default:
                        if (bi.a(axVar, a2)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public void writeTo(ay ayVar) {
            ayVar.a(1, this.d);
            ayVar.a(2, this.e);
            ayVar.a(3, this.f);
            if (!this.g.equals("")) {
                ayVar.a(4, this.g);
            }
            ayVar.a(5, this.h);
            ayVar.a(6, this.i);
            super.writeTo(ayVar);
        }
    }

    public static final class c extends bf {
        public long a;
        public long c;

        public c() {
            b();
        }

        public static c a(byte[] bArr) {
            return (c) bf.a(new c(), bArr);
        }

        /* access modifiers changed from: protected */
        public int a() {
            int a2 = super.a() + ay.c(1, this.a);
            return this.c != 0 ? a2 + ay.c(2, this.c) : a2;
        }

        public c b() {
            this.a = 0;
            this.c = 0;
            this.b = -1;
            return this;
        }

        /* renamed from: b */
        public c a(ax axVar) {
            while (true) {
                int a2 = axVar.a();
                switch (a2) {
                    case 0:
                        break;
                    case 8:
                        this.a = axVar.f();
                        break;
                    case 16:
                        this.c = axVar.f();
                        break;
                    default:
                        if (bi.a(axVar, a2)) {
                            break;
                        } else {
                            break;
                        }
                }
            }
            return this;
        }

        public void writeTo(ay ayVar) {
            ayVar.a(1, this.a);
            if (this.c != 0) {
                ayVar.a(2, this.c);
            }
            super.writeTo(ayVar);
        }
    }
}
