package com.tendcloud.tenddata;

import com.tendcloud.tenddata.ad;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class ae implements ac {
    protected static byte[] a_ = new byte[0];
    protected boolean b_;
    protected ad.a c_;
    protected boolean d_;
    private ByteBuffer e;

    public ae() {
    }

    public ae(ad.a aVar) {
        this.c_ = aVar;
        this.e = ByteBuffer.wrap(a_);
    }

    public ae(ad adVar) {
        this.b_ = adVar.d();
        this.c_ = adVar.f();
        this.e = adVar.c();
        this.d_ = adVar.e();
    }

    public void append(ad adVar) {
        ByteBuffer c = adVar.c();
        if (this.e == null) {
            this.e = ByteBuffer.allocate(c.remaining());
            c.mark();
            this.e.put(c);
            c.reset();
        } else {
            c.mark();
            this.e.position(this.e.limit());
            this.e.limit(this.e.capacity());
            if (c.remaining() > this.e.remaining()) {
                ByteBuffer allocate = ByteBuffer.allocate(c.remaining() + this.e.capacity());
                this.e.flip();
                allocate.put(this.e);
                allocate.put(c);
                this.e = allocate;
            } else {
                this.e.put(c);
            }
            this.e.rewind();
            c.reset();
        }
        this.b_ = adVar.d();
    }

    public ByteBuffer c() {
        return this.e;
    }

    public boolean d() {
        return this.b_;
    }

    public boolean e() {
        return this.d_;
    }

    public ad.a f() {
        return this.c_;
    }

    public void setFin(boolean z) {
        this.b_ = z;
    }

    public void setOptcode(ad.a aVar) {
        this.c_ = aVar;
    }

    public void setPayload(ByteBuffer byteBuffer) {
        this.e = byteBuffer;
    }

    public void setTransferemasked(boolean z) {
        this.d_ = z;
    }

    public String toString() {
        return "Framedata{ optcode:" + f() + ", fin:" + d() + ", payloadlength:[pos:" + this.e.position() + ", len:" + this.e.remaining() + "], payload:" + Arrays.toString(aw.a(new String(this.e.array()))) + "}";
    }
}
