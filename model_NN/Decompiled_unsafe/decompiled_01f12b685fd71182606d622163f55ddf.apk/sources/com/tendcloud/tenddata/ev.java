package com.tendcloud.tenddata;

import android.util.Log;

class ev {
    ev() {
    }

    static void a(String str) {
        if (TCAgent.LOG_ON) {
            Log.i("TDLog", str);
        }
    }

    static void a(String str, Throwable th) {
        if (TCAgent.LOG_ON) {
            Log.e("TDLog", str, th);
        }
    }

    static void a(Throwable th) {
    }

    static void a(String... strArr) {
    }

    static void b(String str) {
        if (TCAgent.LOG_ON) {
            Log.d("TDLog", str);
        }
    }

    static void b(String... strArr) {
    }

    static void c(String str) {
        if (TCAgent.LOG_ON) {
            Log.e("TDLog", str);
        }
    }

    static void c(String... strArr) {
    }
}
