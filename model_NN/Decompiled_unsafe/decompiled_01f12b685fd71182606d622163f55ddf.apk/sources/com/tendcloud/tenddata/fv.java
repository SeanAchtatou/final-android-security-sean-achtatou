package com.tendcloud.tenddata;

class fv implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ TalkingDataSMSApplyCallback b;
    final /* synthetic */ int c;
    final /* synthetic */ String d;
    final /* synthetic */ fu e;

    fv(fu fuVar, String str, TalkingDataSMSApplyCallback talkingDataSMSApplyCallback, int i, String str2) {
        this.e = fuVar;
        this.a = str;
        this.b = talkingDataSMSApplyCallback;
        this.c = i;
        this.d = str2;
    }

    public void run() {
        if (this.a.equals("apply") && this.b != null) {
            if (this.c == 200) {
                this.b.onApplySucc(this.d);
            } else {
                this.b.onApplyFailed(this.c, this.d);
            }
        }
    }
}
