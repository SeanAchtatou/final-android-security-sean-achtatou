package com.tendcloud.tenddata;

import android.content.Context;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.zip.CRC32;

public class bq {
    private static final long j = 3145728;
    private static final String k = "td-cache";
    Context a;
    File b;
    RandomAccessFile c;
    String d;
    CRC32 e = new CRC32();
    Lock f = new ReentrantLock();
    long g = 0;
    long h = 0;
    long i = -1;

    public bq(Context context, String str) {
        try {
            this.a = context;
            this.d = str;
            this.b = context.getDir(k, 0);
            e();
            try {
                f();
            } catch (IOException e2) {
            }
            if (this.c.length() > j) {
                d();
            }
        } catch (Throwable th) {
        }
    }

    private void a(byte[] bArr) {
        try {
            this.f.lock();
            this.c.seek(this.c.length());
            this.c.writeByte(31);
            this.e.reset();
            this.e.update(bArr);
            this.c.writeInt((int) this.e.getValue());
            this.c.writeShort(bArr.length);
            this.c.write(bArr);
            this.c.writeByte(31);
        } finally {
            this.f.unlock();
        }
    }

    private boolean a(long j2) {
        try {
            this.f.lock();
            try {
                this.c.seek(j2);
                byte readByte = this.c.readByte();
                if (readByte == 31) {
                    int readInt = this.c.readInt();
                    short readShort = this.c.readShort();
                    if (readShort >= 0 && this.c.getFilePointer() + ((long) readShort) <= this.c.length()) {
                        this.e.reset();
                        for (int i2 = 0; i2 < readShort; i2++) {
                            this.e.update(this.c.read());
                        }
                        if (this.c.readByte() == 31 && readInt == ((int) this.e.getValue())) {
                            this.h = this.c.getFilePointer();
                            return true;
                        }
                    }
                } else if (readByte == 46) {
                    int readInt2 = this.c.readInt();
                    byte readByte2 = this.c.readByte();
                    if (readInt2 >= 0 && ((long) readInt2) < this.c.length() && readByte2 == 46) {
                        this.h = this.c.getFilePointer();
                        this.g = (long) readInt2;
                        this.f.unlock();
                        return false;
                    }
                }
            } catch (Exception e2) {
            }
            this.h = 1 + j2;
            this.f.unlock();
            return false;
        } finally {
            this.f.unlock();
        }
    }

    private byte[] a(long j2, boolean z) {
        try {
            this.f.lock();
            try {
                this.c.seek(j2);
                byte readByte = this.c.readByte();
                if (readByte == 31) {
                    int readInt = this.c.readInt();
                    int readShort = this.c.readShort();
                    if (readShort >= 0 && this.c.getFilePointer() + ((long) readShort) <= this.c.length()) {
                        byte[] bArr = new byte[readShort];
                        this.c.readFully(bArr);
                        if (this.c.readByte() == 31) {
                            this.e.reset();
                            this.e.update(bArr);
                            if (readInt == ((int) this.e.getValue())) {
                                this.h = this.c.getFilePointer();
                                return bArr;
                            }
                        }
                    }
                } else if (readByte == 46) {
                    int readInt2 = this.c.readInt();
                    byte readByte2 = this.c.readByte();
                    if (readInt2 >= 0 && ((long) readInt2) < this.c.length() && readByte2 == 46) {
                        this.h = this.c.getFilePointer();
                        if (z) {
                            this.g = (long) readInt2;
                        }
                        this.f.unlock();
                        return null;
                    }
                }
            } catch (Exception e2) {
            }
            this.h = 1 + j2;
            this.f.unlock();
            return null;
        } finally {
            this.f.unlock();
        }
    }

    private void b(long j2) {
        try {
            this.f.lock();
            this.c.seek(this.c.length());
            this.c.writeByte(46);
            this.c.writeInt((int) j2);
            this.c.writeByte(46);
        } finally {
            this.f.unlock();
        }
    }

    /* JADX INFO: finally extract failed */
    private void d() {
        this.h = this.g < this.i ? this.i : this.g;
        File file = new File(this.b, this.d + ".tmp");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        while (this.h < this.c.length()) {
            try {
                byte[] a2 = a(this.h, false);
                if (a2 != null) {
                    fileOutputStream.write(a2);
                }
            } catch (Throwable th) {
                fileOutputStream.flush();
                fileOutputStream.close();
                this.c.close();
                throw th;
            }
        }
        fileOutputStream.flush();
        fileOutputStream.close();
        this.c.close();
        File file2 = new File(this.b, this.d);
        file2.delete();
        file.renameTo(file2);
        e();
        this.g = 0;
        this.h = 0;
    }

    private void e() {
        this.c = new RandomAccessFile(new File(this.b, this.d), "rw");
    }

    private void f() {
        boolean z = false;
        while (this.h < this.c.length()) {
            if (this.i == -1 && this.c.length() - this.h < j) {
                this.i = this.h;
            }
            long j2 = this.h;
            if (a(j2) && !z) {
                z = true;
                if (this.g == 0) {
                    this.g = j2;
                }
            }
        }
    }

    public List a(int i2) {
        LinkedList linkedList = new LinkedList();
        try {
            this.h = this.g;
            this.c.seek(this.h);
            while (this.h < this.c.length()) {
                byte[] a2 = a(this.h, false);
                if (a2 != null) {
                    linkedList.add(a2);
                }
                if (linkedList.size() >= i2) {
                    break;
                }
            }
        } catch (IOException e2) {
        }
        if (linkedList.size() == 0) {
            this.g = this.h;
        }
        return linkedList;
    }

    public void a() {
        b(this.h);
        this.g = this.h;
    }

    public void b() {
        this.c.getFD().sync();
    }

    public void c() {
        b();
        this.c.close();
    }

    public void write(byte[] bArr) {
        a(bArr);
    }
}
