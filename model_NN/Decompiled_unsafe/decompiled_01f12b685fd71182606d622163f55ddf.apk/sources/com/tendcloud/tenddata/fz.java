package com.tendcloud.tenddata;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class fz extends Handler {
    fz(Looper looper) {
        super(looper);
    }

    public void handleMessage(Message message) {
        try {
            if (message.obj != null && (message.obj instanceof fu)) {
                Bundle data = message.getData();
                fu fuVar = (fu) message.obj;
                switch (message.what) {
                    case 101:
                        fuVar.a(data);
                        return;
                    case 102:
                        fuVar.b();
                        return;
                    default:
                        return;
                }
            }
        } catch (Throwable th) {
        }
    }
}
