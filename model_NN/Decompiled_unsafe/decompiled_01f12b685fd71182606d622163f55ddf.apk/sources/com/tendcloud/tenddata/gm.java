package com.tendcloud.tenddata;

import android.app.Activity;
import android.content.Context;
import java.lang.reflect.Method;

final class gm implements bv {
    final /* synthetic */ Context a;

    gm(Context context) {
        this.a = context;
    }

    public void a(Object obj, Method method, Object[] objArr) {
        String name = method.getName();
        if (!(this.a instanceof Activity)) {
            return;
        }
        if (name.equalsIgnoreCase("activityPaused")) {
            gd.a((Activity) this.a);
        } else if (name.equalsIgnoreCase("activityIdle")) {
            gd.b((Activity) this.a);
        }
    }

    public void a(Object obj, Method method, Object[] objArr, Object obj2) {
    }
}
