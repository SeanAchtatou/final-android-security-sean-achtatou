package com.tendcloud.tenddata;

import java.lang.reflect.Array;

public abstract class cg {
    static final /* synthetic */ boolean F = (!cg.class.desiredAssertionStatus());
    private static final int G = 2096879;
    private static final int H = 8166;
    private static final int I = 128;
    private static final int J = 16;
    static final int a = 16;
    static final int b = 2;
    static final int c = 273;
    static final int d = 4;
    static final int e = 64;
    static final int f = 4;
    static final int g = 14;
    static final int h = 128;
    static final int i = 4;
    static final int j = 16;
    static final int k = 15;
    static final int l = 4;
    final a A;
    final a B;
    final int C;
    int D = 0;
    int E = -1;
    /* access modifiers changed from: private */
    public final cl K;
    private int L = 0;
    private int M = 0;
    private final int N;
    private final int[][] O;
    private final int[][] P = ((int[][]) Array.newInstance(Integer.TYPE, 4, 128));
    private final int[] Q = new int[16];
    private int R = 0;
    final int m;
    final int[] n = new int[4];
    final cm o = new cm();
    final short[][] p = ((short[][]) Array.newInstance(Short.TYPE, 12, 16));
    final short[] q = new short[12];
    final short[] r = new short[12];
    final short[] s = new short[12];
    final short[] t = new short[12];
    final short[][] u = ((short[][]) Array.newInstance(Short.TYPE, 12, 16));
    final short[][] v = ((short[][]) Array.newInstance(Short.TYPE, 4, 64));
    final short[][] w = {new short[2], new short[2], new short[4], new short[4], new short[8], new short[8], new short[16], new short[16], new short[32], new short[32]};
    final short[] x = new short[16];
    final cd y;
    final b z;

    class a {
        static final int a = 8;
        static final int b = 8;
        static final int c = 256;
        private static final int i = 32;
        final short[] d = new short[2];
        final short[][] e = ((short[][]) Array.newInstance(Short.TYPE, 16, 8));
        final short[][] f = ((short[][]) Array.newInstance(Short.TYPE, 16, 8));
        final short[] g = new short[256];
        private final int[] j;
        private final int[][] k;

        a(int i2, int i3) {
            int i4 = 1 << i2;
            this.j = new int[i4];
            this.k = (int[][]) Array.newInstance(Integer.TYPE, i4, Math.max((i3 - 2) + 1, 16));
        }

        private void a(int i2) {
            int a2 = cl.a(this.d[0], 0);
            int i3 = 0;
            while (i3 < 8) {
                this.k[i2][i3] = cl.b(this.e[i2], i3) + a2;
                i3++;
            }
            int a3 = cl.a(this.d[0], 1);
            int a4 = cl.a(this.d[1], 0);
            while (i3 < 16) {
                this.k[i2][i3] = a3 + a4 + cl.b(this.f[i2], i3 - 8);
                i3++;
            }
            int a5 = cl.a(this.d[1], 1);
            while (i3 < this.k[i2].length) {
                this.k[i2][i3] = a3 + a5 + cl.b(this.g, (i3 - 8) - 8);
                i3++;
            }
        }

        /* access modifiers changed from: package-private */
        public void a() {
            cl.initProbs(this.d);
            for (short[] initProbs : this.e) {
                cl.initProbs(initProbs);
            }
            for (int i2 = 0; i2 < this.e.length; i2++) {
                cl.initProbs(this.f[i2]);
            }
            cl.initProbs(this.g);
            for (int i3 = 0; i3 < this.j.length; i3++) {
                this.j[i3] = 0;
            }
        }

        /* access modifiers changed from: package-private */
        public void a(int i2, int i3) {
            int i4 = i2 - 2;
            if (i4 < 8) {
                cg.this.K.a(this.d, 0, 0);
                cg.this.K.a(this.e[i3], i4);
            } else {
                cg.this.K.a(this.d, 0, 1);
                int i5 = i4 - 8;
                if (i5 < 8) {
                    cg.this.K.a(this.d, 1, 0);
                    cg.this.K.a(this.f[i3], i5);
                } else {
                    cg.this.K.a(this.d, 1, 1);
                    cg.this.K.a(this.g, i5 - 8);
                }
            }
            int[] iArr = this.j;
            iArr[i3] = iArr[i3] - 1;
        }

        /* access modifiers changed from: package-private */
        public int b(int i2, int i3) {
            return this.k[i3][i2 - 2];
        }

        /* access modifiers changed from: package-private */
        public void b() {
            for (int i2 = 0; i2 < this.j.length; i2++) {
                if (this.j[i2] <= 0) {
                    this.j[i2] = 32;
                    a(i2);
                }
            }
        }
    }

    class b {
        static final /* synthetic */ boolean b = (!cg.class.desiredAssertionStatus());
        a[] a;
        private final int d;
        private final int e;

        class a {
            final short[] a;

            private a() {
                this.a = new short[768];
            }

            /* access modifiers changed from: package-private */
            public int a(int i) {
                int i2 = 0;
                int i3 = i | 256;
                do {
                    i2 += cl.a(this.a[i3 >>> 8], (i3 >>> 7) & 1);
                    i3 <<= 1;
                } while (i3 < 65536);
                return i2;
            }

            /* access modifiers changed from: package-private */
            public int a(int i, int i2) {
                int i3 = 0;
                int i4 = 256;
                int i5 = i | 256;
                do {
                    i2 <<= 1;
                    i3 += cl.a(this.a[(i2 & i4) + i4 + (i5 >>> 8)], (i5 >>> 7) & 1);
                    i5 <<= 1;
                    i4 &= (i2 ^ i5) ^ -1;
                } while (i5 < 65536);
                return i3;
            }

            /* access modifiers changed from: package-private */
            public void a() {
                cl.initProbs(this.a);
            }

            /* access modifiers changed from: package-private */
            public void b() {
                int b2 = cg.this.y.b(cg.this.E) | 256;
                if (cg.this.o.g()) {
                    do {
                        cg.this.K.a(this.a, b2 >>> 8, (b2 >>> 7) & 1);
                        b2 <<= 1;
                    } while (b2 < 65536);
                } else {
                    int b3 = cg.this.y.b(cg.this.n[0] + 1 + cg.this.E);
                    int i = b2;
                    int i2 = 256;
                    do {
                        b3 <<= 1;
                        cg.this.K.a(this.a, (b3 & i2) + i2 + (i >>> 8), (i >>> 7) & 1);
                        i <<= 1;
                        i2 &= (b3 ^ i) ^ -1;
                    } while (i < 65536);
                }
                cg.this.o.c();
            }
        }

        b(int i, int i2) {
            this.d = i;
            this.e = (1 << i2) - 1;
            this.a = new a[(1 << (i + i2))];
            for (int i3 = 0; i3 < this.a.length; i3++) {
                this.a[i3] = new a();
            }
        }

        /* access modifiers changed from: package-private */
        public final int a(int i, int i2) {
            return (i >> (8 - this.d)) + ((this.e & i2) << this.d);
        }

        /* access modifiers changed from: package-private */
        public int a(int i, int i2, int i3, int i4, cm cmVar) {
            int a2 = cl.a(cg.this.p[cmVar.b()][cg.this.m & i4], 0);
            int a3 = a(i3, i4);
            return (cmVar.g() ? this.a[a3].a(i) : this.a[a3].a(i, i2)) + a2;
        }

        /* access modifiers changed from: package-private */
        public void a() {
            for (a a2 : this.a) {
                a2.a();
            }
        }

        /* access modifiers changed from: package-private */
        public void b() {
            if (b || cg.this.E >= 0) {
                this.a[0].b();
                return;
            }
            throw new AssertionError();
        }

        /* access modifiers changed from: package-private */
        public void c() {
            if (b || cg.this.E >= 0) {
                this.a[a(cg.this.y.b(cg.this.E + 1), cg.this.y.f() - cg.this.E)].b();
                return;
            }
            throw new AssertionError();
        }
    }

    cg(cl clVar, cd cdVar, int i2, int i3, int i4, int i5, int i6) {
        this.m = (1 << i4) - 1;
        this.K = clVar;
        this.y = cdVar;
        this.C = i6;
        this.z = new b(i2, i3);
        this.A = new a(i4, i6);
        this.B = new a(i4, i6);
        this.N = b(i5 - 1) + 1;
        this.O = (int[][]) Array.newInstance(Integer.TYPE, 4, this.N);
        c();
    }

    static final int a(int i2) {
        if (i2 < 6) {
            return i2 - 2;
        }
        return 3;
    }

    public static cg a(cl clVar, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        return Runtime.getRuntime().availableProcessors() <= 2 ? new ch(clVar, i2, i3, i4, i5, i6, i7, i8) : new ci(clVar, i2, i3, i4, i5, i6, i7, i8);
    }

    private void a(int i2, int i3, int i4) {
        this.o.d();
        this.A.a(i3, i4);
        int b2 = b(i2);
        this.K.a(this.v[a(i3)], b2);
        if (b2 >= 4) {
            int i5 = (b2 >>> 1) - 1;
            int i6 = i2 - (((b2 & 1) | 2) << i5);
            if (b2 < 14) {
                this.K.c(this.w[b2 - 4], i6);
            } else {
                this.K.b(i6 >>> 4, i5 - 4);
                this.K.c(this.x, i6 & 15);
                this.M--;
            }
        }
        this.n[3] = this.n[2];
        this.n[2] = this.n[1];
        this.n[1] = this.n[0];
        this.n[0] = i2;
        this.L--;
    }

    public static int b(int i2) {
        int i3;
        if (i2 <= 4) {
            return i2;
        }
        int i4 = 31;
        if ((-65536 & i2) == 0) {
            i3 = i2 << 16;
            i4 = 15;
        } else {
            i3 = i2;
        }
        if ((-16777216 & i3) == 0) {
            i3 <<= 8;
            i4 -= 8;
        }
        if ((-268435456 & i3) == 0) {
            i3 <<= 4;
            i4 -= 4;
        }
        if ((-1073741824 & i3) == 0) {
            i3 <<= 2;
            i4 -= 2;
        }
        if ((i3 & Integer.MIN_VALUE) == 0) {
            i4--;
        }
        return (i4 << 1) + ((i2 >>> (i4 - 1)) & 1);
    }

    private void b(int i2, int i3, int i4) {
        int i5 = 0;
        if (i2 == 0) {
            this.K.a(this.r, this.o.b(), 0);
            cl clVar = this.K;
            short[] sArr = this.u[this.o.b()];
            if (i3 != 1) {
                i5 = 1;
            }
            clVar.a(sArr, i4, i5);
        } else {
            int i6 = this.n[i2];
            this.K.a(this.r, this.o.b(), 1);
            if (i2 == 1) {
                this.K.a(this.s, this.o.b(), 0);
            } else {
                this.K.a(this.s, this.o.b(), 1);
                this.K.a(this.t, this.o.b(), i2 - 2);
                if (i2 == 3) {
                    this.n[3] = this.n[2];
                }
                this.n[2] = this.n[1];
            }
            this.n[1] = this.n[0];
            this.n[0] = i6;
        }
        if (i3 == 1) {
            this.o.f();
            return;
        }
        this.B.a(i3, i4);
        this.o.e();
    }

    private boolean i() {
        if (!F && this.E != -1) {
            throw new AssertionError();
        } else if (!this.y.a(0)) {
            return false;
        } else {
            c(1);
            this.K.a(this.p[this.o.b()], 0, 0);
            this.z.b();
            this.E--;
            if (F || this.E == -1) {
                this.R++;
                if (F || this.R == 1) {
                    return true;
                }
                throw new AssertionError();
            }
            throw new AssertionError();
        }
    }

    private boolean j() {
        if (!this.y.a(this.E + 1)) {
            return false;
        }
        int a2 = a();
        if (F || this.E >= 0) {
            int f2 = (this.y.f() - this.E) & this.m;
            if (this.D != -1) {
                this.K.a(this.p[this.o.b()], f2, 1);
                if (this.D < 4) {
                    if (F || this.y.a(-this.E, this.n[this.D], a2) == a2) {
                        this.K.a(this.q, this.o.b(), 1);
                        b(this.D, a2, f2);
                    } else {
                        throw new AssertionError();
                    }
                } else if (F || this.y.a(-this.E, this.D - 4, a2) == a2) {
                    this.K.a(this.q, this.o.b(), 0);
                    a(this.D - 4, a2, f2);
                } else {
                    throw new AssertionError();
                }
            } else if (F || a2 == 1) {
                this.K.a(this.p[this.o.b()], f2, 0);
                this.z.c();
            } else {
                throw new AssertionError();
            }
            this.E -= a2;
            this.R += a2;
            return true;
        }
        throw new AssertionError();
    }

    private void k() {
        this.L = 128;
        for (int i2 = 0; i2 < 4; i2++) {
            for (int i3 = 0; i3 < this.N; i3++) {
                this.O[i2][i3] = cl.b(this.v[i2], i3);
            }
            for (int i4 = 14; i4 < this.N; i4++) {
                int[] iArr = this.O[i2];
                iArr[i4] = cl.a(((i4 >>> 1) - 1) - 4) + iArr[i4];
            }
            for (int i5 = 0; i5 < 4; i5++) {
                this.P[i2][i5] = this.O[i2][i5];
            }
        }
        int i6 = 4;
        int i7 = 4;
        while (i6 < 14) {
            int i8 = ((i6 & 1) | 2) << ((i6 >>> 1) - 1);
            int length = this.w[i6 - 4].length;
            int i9 = i7;
            for (int i10 = 0; i10 < length; i10++) {
                int d2 = cl.d(this.w[i6 - 4], i9 - i8);
                for (int i11 = 0; i11 < 4; i11++) {
                    this.P[i11][i9] = this.O[i11][i6] + d2;
                }
                i9++;
            }
            i6++;
            i7 = i9;
        }
        if (!F && i7 != 128) {
            throw new AssertionError();
        }
    }

    private void l() {
        this.M = 16;
        for (int i2 = 0; i2 < 16; i2++) {
            this.Q[i2] = cl.d(this.x, i2);
        }
    }

    /* access modifiers changed from: package-private */
    public abstract int a();

    /* access modifiers changed from: package-private */
    public int a(int i2, int i3, int i4, int i5) {
        int b2 = this.A.b(i4, i5) + i2;
        int a2 = a(i4);
        if (i3 < 128) {
            return b2 + this.P[a2][i3];
        }
        return b2 + this.O[a2][b(i3)] + this.Q[i3 & 15];
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, int i3, cm cmVar, int i4) {
        if (i3 == 0) {
            return cl.a(this.r[cmVar.b()], 0) + cl.a(this.u[cmVar.b()][i4], 1) + i2;
        }
        int a2 = cl.a(this.r[cmVar.b()], 1) + i2;
        return i3 == 1 ? a2 + cl.a(this.s[cmVar.b()], 0) : a2 + cl.a(this.s[cmVar.b()], 1) + cl.a(this.t[cmVar.b()], i3 - 2);
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, cm cmVar) {
        return cl.a(this.q[cmVar.b()], 0) + i2;
    }

    /* access modifiers changed from: package-private */
    public int a(int i2, cm cmVar, int i3) {
        return cl.a(this.r[cmVar.b()], 0) + i2 + cl.a(this.u[cmVar.b()][i3], 0);
    }

    /* access modifiers changed from: package-private */
    public int a(cm cmVar, int i2) {
        return cl.a(this.p[cmVar.b()][i2], 1);
    }

    /* access modifiers changed from: package-private */
    public int b(int i2, int i3, cm cmVar, int i4) {
        return a(b(a(cmVar, i4), cmVar), i2, cmVar, i4) + this.B.b(i3, i4);
    }

    /* access modifiers changed from: package-private */
    public int b(int i2, cm cmVar) {
        return cl.a(this.q[cmVar.b()], 1) + i2;
    }

    public cd b() {
        return this.y;
    }

    public void c() {
        this.n[0] = 0;
        this.n[1] = 0;
        this.n[2] = 0;
        this.n[3] = 0;
        this.o.a();
        for (short[] initProbs : this.p) {
            cl.initProbs(initProbs);
        }
        cl.initProbs(this.q);
        cl.initProbs(this.r);
        cl.initProbs(this.s);
        cl.initProbs(this.t);
        for (short[] initProbs2 : this.u) {
            cl.initProbs(initProbs2);
        }
        for (short[] initProbs3 : this.v) {
            cl.initProbs(initProbs3);
        }
        for (short[] initProbs4 : this.w) {
            cl.initProbs(initProbs4);
        }
        cl.initProbs(this.x);
        this.z.a();
        this.A.a();
        this.B.a();
        this.L = 0;
        this.M = 0;
        this.R += this.E + 1;
        this.E = -1;
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        this.E += i2;
        this.y.skip(i2);
    }

    public int d() {
        return this.R;
    }

    public void e() {
        this.R = 0;
    }

    public boolean f() {
        if (!this.y.b() && !i()) {
            return false;
        }
        while (this.R <= G && this.K.b() <= H) {
            if (!j()) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public cj g() {
        this.E++;
        cj a2 = this.y.a();
        if (F || this.y.a(a2)) {
            return a2;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public void h() {
        if (this.L <= 0) {
            k();
        }
        if (this.M <= 0) {
            l();
        }
        this.A.b();
        this.B.b();
    }
}
