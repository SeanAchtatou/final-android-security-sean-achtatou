package com.tendcloud.tenddata;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.JsonWriter;
import android.util.Pair;
import com.tendcloud.tenddata.ed;
import com.tendcloud.tenddata.ef;
import com.tendcloud.tenddata.ej;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.net.ssl.SSLSocketFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@TargetApi(16)
class eg implements ej.g {
    private static final int A = 8;
    private static final int B = 9;
    private static final int C = 10;
    private static final int D = 11;
    private static final int E = 12;
    static final int b = 1;
    public static final int c = 13;
    public static final String d = "";
    static final boolean e = false;
    static final boolean f = false;
    static final String g = "TDDynamicEvent";
    static final String h = "TDExceptionLog";
    private static final String i = "cls.xdrig.com";
    private static final String j = "wss";
    private static final String k = "/codeless/app/sdk/";
    /* access modifiers changed from: private */
    public static volatile eg l = null;
    private static final String r = "talkingdata.viewcrawler.changes";
    private static final String s = "config.events";
    private static final int t = 0;
    private static final int u = 2;
    private static final int v = 3;
    private static final int w = 4;
    private static final int x = 5;
    private static final int y = 6;
    private static final int z = 7;
    final dz a = new dz();
    private final Context m;
    /* access modifiers changed from: private */
    public final ec n;
    /* access modifiers changed from: private */
    public final Map o = d();
    /* access modifiers changed from: private */
    public final d p;
    /* access modifiers changed from: private */
    public final float q = Resources.getSystem().getDisplayMetrics().scaledDensity;

    class a implements ed.a {
        private a() {
        }

        private void a(int i, JSONObject jSONObject) {
            Message obtainMessage = eg.a().p.obtainMessage(i);
            if (jSONObject != null) {
                obtainMessage.obj = jSONObject;
            }
            eg.a().p.sendMessage(obtainMessage);
        }

        public void a() {
            a(4, null);
        }

        public void b() {
            eg.this.p.sendMessage(eg.this.p.obtainMessage(8));
        }

        public void bindEvents(JSONObject jSONObject) {
            a(6, jSONObject);
        }

        public void clearEdits(JSONObject jSONObject) {
            a(10, jSONObject);
        }

        public void performEdit(JSONObject jSONObject) {
            a(3, jSONObject);
        }

        public void sendSnapshot(JSONObject jSONObject) {
            eg.a().p.removeMessages(2);
            a(2, jSONObject);
        }

        public void setTweaks(JSONObject jSONObject) {
            a(11, jSONObject);
        }
    }

    static class b {
        public final String a;
        public final JSONObject b;
        public final Pair c;

        public b(String str, JSONObject jSONObject, Pair pair) {
            this.a = str;
            this.b = jSONObject;
            this.c = pair;
        }
    }

    static class c {
        public final JSONObject a;
        public final Pair b;

        public c(JSONObject jSONObject, Pair pair) {
            this.a = jSONObject;
            this.b = pair;
        }
    }

    class d extends Handler {
        final /* synthetic */ eg a;
        private ed b;
        private ei c = null;
        private final Context d;
        private final String e;
        private final Lock f;
        private final eb g;
        private final Map h;
        private final List i;
        private final List j;
        private final List k;
        private final List l;
        private final List m;
        private final Set n;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(eg egVar, Context context, String str, Looper looper, ej.g gVar) {
            super(looper);
            String str2 = null;
            this.a = egVar;
            this.d = context;
            this.e = str;
            this.g = new eb(new ef.b(0 == 0 ? context.getPackageName() : str2, context), gVar);
            this.h = new HashMap();
            this.i = new ArrayList();
            this.j = new ArrayList();
            this.k = new ArrayList();
            this.l = new ArrayList();
            this.m = new ArrayList();
            this.n = new HashSet();
            this.f = new ReentrantLock();
            this.f.lock();
        }

        private void a(ej.e eVar) {
            if (this.b != null) {
                JsonWriter jsonWriter = new JsonWriter(new OutputStreamWriter(this.b.b()));
                try {
                    jsonWriter.beginObject();
                    jsonWriter.name("type").value("layout_error");
                    jsonWriter.name("exception_type").value(eVar.a());
                    jsonWriter.name("cid").value(eVar.b());
                    jsonWriter.endObject();
                    try {
                        jsonWriter.close();
                    } catch (IOException e2) {
                    }
                } catch (IOException e3) {
                    try {
                        jsonWriter.close();
                    } catch (IOException e4) {
                    }
                } catch (Throwable th) {
                    try {
                        jsonWriter.close();
                    } catch (IOException e5) {
                    }
                    throw th;
                }
            }
        }

        private void a(String str) {
            if (this.b != null) {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put("error_message", str);
                } catch (JSONException e2) {
                }
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.b.b());
                try {
                    outputStreamWriter.write("{\"type\": \"error\", ");
                    outputStreamWriter.write("\"payload\": ");
                    outputStreamWriter.write(jSONObject.toString());
                    outputStreamWriter.write("}");
                    try {
                        outputStreamWriter.close();
                    } catch (IOException e3) {
                    }
                } catch (IOException e4) {
                    try {
                        outputStreamWriter.close();
                    } catch (IOException e5) {
                    }
                } catch (Throwable th) {
                    try {
                        outputStreamWriter.close();
                    } catch (IOException e6) {
                    }
                    throw th;
                }
            }
        }

        private void a(JSONArray jSONArray) {
            SharedPreferences.Editor edit = h().edit();
            edit.putString(eg.r, jSONArray.toString());
            edit.apply();
            c();
        }

        private void a(JSONObject jSONObject) {
            long currentTimeMillis = System.currentTimeMillis();
            try {
                JSONObject jSONObject2 = jSONObject.getJSONObject("payload");
                if (jSONObject2.has("config")) {
                    this.c = this.g.a(jSONObject2);
                }
                if (this.c != null) {
                    System.currentTimeMillis();
                    BufferedOutputStream b2 = this.b.b();
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(b2);
                    try {
                        outputStreamWriter.write("{");
                        outputStreamWriter.write("\"type\": \"snapshot_response\",");
                        outputStreamWriter.write("\"payload\": {");
                        outputStreamWriter.write("\"activities\":");
                        outputStreamWriter.flush();
                        this.c.a(eg.l.a, b2);
                        outputStreamWriter.write(",\"snapshot_time_millis\": ");
                        outputStreamWriter.write(Long.toString(System.currentTimeMillis() - currentTimeMillis));
                        outputStreamWriter.write("}");
                        outputStreamWriter.write("}");
                        try {
                            outputStreamWriter.close();
                        } catch (IOException e2) {
                        }
                    } catch (IOException e3) {
                        try {
                            outputStreamWriter.close();
                        } catch (IOException e4) {
                        }
                    } catch (Throwable th) {
                        try {
                            outputStreamWriter.close();
                        } catch (IOException e5) {
                        }
                        throw th;
                    }
                }
            } catch (Throwable th2) {
            }
        }

        private void b() {
            SharedPreferences h2 = h();
            String string = h2.getString(eg.r, null);
            if (string != null) {
                try {
                    JSONArray jSONArray = new JSONArray(string);
                    int length = jSONArray.length();
                    for (int i2 = 0; i2 < length; i2++) {
                        JSONObject jSONObject = jSONArray.getJSONObject(i2);
                        this.n.add(new Pair(Integer.valueOf(jSONObject.getInt("experiment_id")), Integer.valueOf(jSONObject.getInt(dc.V))));
                    }
                } catch (JSONException e2) {
                    SharedPreferences.Editor edit = h2.edit();
                    edit.remove(eg.r);
                    edit.remove(eg.s);
                    edit.apply();
                }
            }
        }

        private void b(String str) {
            if (this.b != null) {
                JsonWriter jsonWriter = new JsonWriter(new OutputStreamWriter(this.b.b()));
                try {
                    jsonWriter.beginObject();
                    jsonWriter.name("type").value("track_message");
                    jsonWriter.name("payload");
                    jsonWriter.beginObject();
                    jsonWriter.name("event_name").value(str);
                    jsonWriter.endObject();
                    jsonWriter.endObject();
                    jsonWriter.flush();
                    try {
                        jsonWriter.close();
                    } catch (IOException e2) {
                    }
                } catch (IOException e3) {
                    try {
                        jsonWriter.close();
                    } catch (IOException e4) {
                    }
                } catch (Throwable th) {
                    try {
                        jsonWriter.close();
                    } catch (IOException e5) {
                    }
                    throw th;
                }
            }
        }

        private void b(JSONArray jSONArray) {
            SharedPreferences.Editor edit = h().edit();
            edit.putString(eg.s, jSONArray.toString());
            edit.apply();
            c();
        }

        private void b(JSONObject jSONObject) {
            try {
                JSONArray jSONArray = jSONObject.getJSONObject("payload").getJSONArray("actions");
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                    String a2 = eg.b(jSONObject2, "target_activity");
                    this.h.put(jSONObject2.getString(dc.V), new Pair(a2, jSONObject2));
                }
                g();
            } catch (JSONException e2) {
            }
        }

        private void c() {
            SharedPreferences h2 = h();
            String string = h2.getString(eg.r, null);
            String string2 = h2.getString(eg.s, null);
            if (string != null) {
                try {
                    this.k.clear();
                    this.l.clear();
                    JSONArray jSONArray = new JSONArray(string);
                    int length = jSONArray.length();
                    for (int i2 = 0; i2 < length; i2++) {
                        JSONObject jSONObject = jSONArray.getJSONObject(i2);
                        Pair pair = new Pair(Integer.valueOf(jSONObject.getInt("experiment_id")), Integer.valueOf(jSONObject.getInt(dc.V)));
                        JSONArray jSONArray2 = jSONObject.getJSONArray("actions");
                        for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                            JSONObject jSONObject2 = jSONArray2.getJSONObject(i3);
                            this.k.add(new b(eg.b(jSONObject2, "target_activity"), jSONObject2, pair));
                        }
                        JSONArray jSONArray3 = jSONObject.getJSONArray("tweaks");
                        int length2 = jSONArray3.length();
                        for (int i4 = 0; i4 < length2; i4++) {
                            this.l.add(new c(jSONArray3.getJSONObject(i4), pair));
                        }
                    }
                } catch (JSONException e2) {
                    SharedPreferences.Editor edit = h2.edit();
                    edit.remove(eg.r);
                    edit.remove(eg.s);
                    edit.apply();
                }
            }
            if (string2 != null) {
                JSONArray jSONArray4 = new JSONArray(string2);
                this.m.clear();
                for (int i5 = 0; i5 < jSONArray4.length(); i5++) {
                    JSONObject jSONObject3 = jSONArray4.getJSONObject(i5);
                    this.m.add(new Pair(eg.b(jSONObject3, "target_activity"), jSONObject3));
                }
            }
            g();
        }

        private void c(JSONObject jSONObject) {
            try {
                JSONArray jSONArray = jSONObject.getJSONObject("payload").getJSONArray("actions");
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    this.h.remove(jSONArray.getString(i2));
                }
            } catch (JSONException e2) {
            }
            g();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tendcloud.tenddata.bk.a(boolean, java.security.cert.X509Certificate):javax.net.ssl.SSLSocketFactory
         arg types: [int, java.security.cert.X509Certificate]
         candidates:
          com.tendcloud.tenddata.bk.a(byte[], java.net.URLConnection):com.tendcloud.tenddata.bk$d
          com.tendcloud.tenddata.bk.a(java.lang.String, int):java.lang.String
          com.tendcloud.tenddata.bk.a(java.lang.String, java.io.File):java.lang.String
          com.tendcloud.tenddata.bk.a(java.lang.String, boolean):java.lang.String
          com.tendcloud.tenddata.bk.a(java.lang.String, java.lang.String):java.net.URL
          com.tendcloud.tenddata.bk.a(java.net.URLConnection, java.lang.String):javax.net.ssl.HttpsURLConnection
          com.tendcloud.tenddata.bk.a(boolean, java.security.cert.X509Certificate):javax.net.ssl.SSLSocketFactory */
        private void d() {
            if (this.b == null || !this.b.a()) {
                String str = "wss://cls.xdrig.com/codeless/app/sdk/" + ab.getAppId(ab.mContext);
                try {
                    if (eg.j.equals(eg.j)) {
                        SSLSocketFactory a2 = !ca.b("") ? bk.a(true, eg.b("")) : bk.a(false, eg.b(""));
                        if (a2 != null) {
                            this.b = new ed(new URI(str), new a(), a2.createSocket());
                            return;
                        }
                        return;
                    }
                    this.b = new ed(new URI(str), new a(), null);
                } catch (URISyntaxException e2) {
                } catch (Exception e3) {
                }
            }
        }

        private void d(JSONObject jSONObject) {
            try {
                JSONArray jSONArray = jSONObject.getJSONObject("payload").getJSONArray(fo.a);
                int length = jSONArray.length();
                this.j.clear();
                for (int i2 = 0; i2 < length; i2++) {
                    try {
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                        this.j.add(new Pair(eg.b(jSONObject2, "target_activity"), jSONObject2));
                    } catch (JSONException e2) {
                    }
                }
                g();
            } catch (JSONException e3) {
            }
        }

        private void e() {
            if (this.b != null) {
                JsonWriter jsonWriter = new JsonWriter(new OutputStreamWriter(this.b.b()));
                try {
                    jsonWriter.beginObject();
                    jsonWriter.name("type").value("device_info_response");
                    jsonWriter.name("payload").beginObject();
                    jsonWriter.name("scaled_density").value((double) eg.l.q);
                    jsonWriter.name("ssid").value(br.v(this.d));
                    for (Map.Entry entry : eg.a().o.entrySet()) {
                        jsonWriter.name((String) entry.getKey()).value((String) entry.getValue());
                    }
                    jsonWriter.endObject();
                    jsonWriter.endObject();
                    try {
                        jsonWriter.close();
                    } catch (IOException e2) {
                    }
                } catch (IOException e3) {
                    try {
                        jsonWriter.close();
                    } catch (IOException e4) {
                    }
                } catch (Throwable th) {
                    try {
                        jsonWriter.close();
                    } catch (IOException e5) {
                    }
                    throw th;
                }
            }
        }

        private void f() {
            this.h.clear();
            this.j.clear();
            this.c = null;
            g();
        }

        private void g() {
            Object arrayList;
            try {
                ArrayList arrayList2 = new ArrayList();
                HashSet hashSet = new HashSet();
                int size = this.m.size();
                for (int i2 = 0; i2 < size; i2++) {
                    Pair pair = (Pair) this.m.get(i2);
                    try {
                        ej a2 = this.g.a((JSONObject) pair.second, eg.a().n);
                        if (a2 != null) {
                            arrayList2.add(new Pair(pair.first, a2));
                        }
                    } catch (Throwable th) {
                    }
                }
                int size2 = this.j.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    Pair pair2 = (Pair) this.j.get(i3);
                    try {
                        arrayList2.add(new Pair(pair2.first, this.g.a((JSONObject) pair2.second, eg.l.n)));
                    } catch (Throwable th2) {
                    }
                }
                HashMap hashMap = new HashMap();
                int size3 = arrayList2.size();
                for (int i4 = 0; i4 < size3; i4++) {
                    Pair pair3 = (Pair) arrayList2.get(i4);
                    if (hashMap.containsKey(pair3.first)) {
                        arrayList = (List) hashMap.get(pair3.first);
                    } else {
                        arrayList = new ArrayList();
                        hashMap.put(pair3.first, arrayList);
                    }
                    arrayList.add(pair3.second);
                }
                eg.l.a.a(hashMap);
                this.n.addAll(hashSet);
            } catch (Throwable th3) {
            }
        }

        private SharedPreferences h() {
            return this.d.getSharedPreferences(fo.b + this.e, 0);
        }

        public void a() {
            this.f.unlock();
        }

        public void handleMessage(Message message) {
            this.f.lock();
            try {
                switch (message.what) {
                    case 0:
                        b();
                        c();
                        break;
                    case 1:
                        d();
                        break;
                    case 2:
                        a((JSONObject) message.obj);
                        break;
                    case 3:
                        b((JSONObject) message.obj);
                        break;
                    case 4:
                        e();
                        break;
                    case 5:
                        b((JSONArray) message.obj);
                        break;
                    case 6:
                        d((JSONObject) message.obj);
                        break;
                    case 7:
                        b((String) message.obj);
                        break;
                    case 8:
                        f();
                        break;
                    case 9:
                        a((JSONArray) message.obj);
                        break;
                    case 10:
                        c((JSONObject) message.obj);
                        break;
                    case 12:
                        a((ej.e) message.obj);
                        break;
                    case 13:
                        sendEvent((Hashtable) message.obj);
                        break;
                }
            } finally {
                this.f.unlock();
            }
        }

        public void sendEvent(Hashtable hashtable) {
            if (this.b == null || !this.b.a()) {
                try {
                    String obj = hashtable.get(dc.V).toString();
                    hashtable.remove(dc.V);
                    TCAgent.onEvent(ab.mContext, obj, "", hashtable);
                } catch (Throwable th) {
                }
            } else {
                JsonWriter jsonWriter = new JsonWriter(new OutputStreamWriter(this.b.b()));
                try {
                    jsonWriter.beginObject();
                    jsonWriter.name("type").value("event_triggered_response");
                    for (Map.Entry entry : hashtable.entrySet()) {
                        jsonWriter.name((String) entry.getKey()).value(entry.getValue() + "");
                    }
                    jsonWriter.endObject();
                    try {
                        jsonWriter.close();
                    } catch (IOException e2) {
                    }
                } catch (IOException e3) {
                    try {
                        jsonWriter.close();
                    } catch (IOException e4) {
                    }
                } catch (Throwable th2) {
                    try {
                        jsonWriter.close();
                    } catch (IOException e5) {
                    }
                    throw th2;
                }
            }
        }
    }

    private eg(Context context, String str, em emVar) {
        this.m = context;
        emVar.a(this.a);
        HandlerThread handlerThread = new HandlerThread("CodelessViewCrawler");
        handlerThread.setPriority(10);
        handlerThread.start();
        this.p = new d(this, context, str, handlerThread.getLooper(), this);
        fo.a().a(this.p);
        this.n = new ec(this.p);
        c();
    }

    static eg a() {
        return l;
    }

    static eg a(Context context, String str, em emVar) {
        if (l == null) {
            synchronized (eg.class) {
                if (l == null) {
                    l = new eg(context, str, emVar);
                }
            }
        }
        return l;
    }

    /* access modifiers changed from: private */
    public static String b(JSONObject jSONObject, String str) {
        if (!jSONObject.has(str) || jSONObject.isNull(str)) {
            return null;
        }
        return jSONObject.getString(str);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0020, code lost:
        r0 = null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.security.cert.X509Certificate b(java.lang.String r3) {
        /*
            r1 = 0
            boolean r0 = com.tendcloud.tenddata.ca.b(r3)
            if (r0 == 0) goto L_0x0008
        L_0x0007:
            return r1
        L_0x0008:
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream
            byte[] r2 = r3.getBytes()
            r0.<init>(r2)
            java.lang.String r2 = "X.509"
            java.security.cert.CertificateFactory r2 = java.security.cert.CertificateFactory.getInstance(r2)     // Catch:{ Exception -> 0x001f, all -> 0x0022 }
            java.security.cert.Certificate r0 = r2.generateCertificate(r0)     // Catch:{ Exception -> 0x001f, all -> 0x0022 }
            java.security.cert.X509Certificate r0 = (java.security.cert.X509Certificate) r0     // Catch:{ Exception -> 0x001f, all -> 0x0022 }
        L_0x001d:
            r1 = r0
            goto L_0x0007
        L_0x001f:
            r0 = move-exception
            r0 = r1
            goto L_0x001d
        L_0x0022:
            r0 = move-exception
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.eg.b(java.lang.String):java.security.cert.X509Certificate");
    }

    /* access modifiers changed from: package-private */
    public d b() {
        return this.p;
    }

    public void c() {
        this.p.a();
        this.p.sendMessage(this.p.obtainMessage(0));
    }

    public Map d() {
        HashMap hashMap = new HashMap();
        hashMap.put("sdk_version", "+V2.2.46 gp");
        hashMap.put("system_name", "Android");
        hashMap.put("system_version", Build.VERSION.RELEASE == null ? "UNKNOWN" : Build.VERSION.RELEASE);
        hashMap.put("device_manufacturer", Build.MANUFACTURER == null ? "UNKNOWN" : Build.MANUFACTURER);
        hashMap.put("device_brand", Build.BRAND == null ? "UNKNOWN" : Build.BRAND);
        hashMap.put("device_model", Build.MODEL == null ? "UNKNOWN" : Build.MODEL);
        try {
            PackageInfo packageInfo = this.m.getPackageManager().getPackageInfo(this.m.getPackageName(), 0);
            hashMap.put("app_version", packageInfo.versionName);
            hashMap.put("app_version_code", Integer.toString(packageInfo.versionCode));
        } catch (PackageManager.NameNotFoundException e2) {
        }
        return hashMap;
    }

    public void onLayoutError(ej.e eVar) {
        Message obtainMessage = this.p.obtainMessage();
        obtainMessage.what = 12;
        obtainMessage.obj = eVar;
        this.p.sendMessage(obtainMessage);
    }

    public void setEventBindings(JSONArray jSONArray) {
        Message obtainMessage = this.p.obtainMessage(5);
        obtainMessage.obj = jSONArray;
        this.p.sendMessage(obtainMessage);
    }

    public void setVariants(JSONArray jSONArray) {
        Message obtainMessage = this.p.obtainMessage(9);
        obtainMessage.obj = jSONArray;
        this.p.sendMessage(obtainMessage);
    }
}
