package com.tendcloud.tenddata;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

final class gb extends Handler {
    gb(Looper looper) {
        super(looper);
    }

    public void handleMessage(Message message) {
        try {
            ga.b = message.getData().getString(dc.Y);
            if (message.obj != null && (message.obj instanceof byte[])) {
                byte[] bArr = (byte[]) message.obj;
                if (ga.b.equals("verify")) {
                    ga.c = message.getData().getInt("timeout");
                    bk.a = ga.c;
                    bk.b = ga.c;
                }
                ga.a(bArr);
            }
        } catch (Throwable th) {
        }
    }
}
