package com.tendcloud.tenddata;

public final class bb implements Cloneable {
    private static final bc a = new bc();
    private boolean b;
    private int[] c;
    private bc[] d;
    private int e;

    bb() {
        this(10);
    }

    bb(int i) {
        this.b = false;
        int b2 = b(i);
        this.c = new int[b2];
        this.d = new bc[b2];
        this.e = 0;
    }

    private boolean a(int[] iArr, int[] iArr2, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            if (iArr[i2] != iArr2[i2]) {
                return false;
            }
        }
        return true;
    }

    private boolean a(bc[] bcVarArr, bc[] bcVarArr2, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            if (!bcVarArr[i2].equals(bcVarArr2[i2])) {
                return false;
            }
        }
        return true;
    }

    private int b(int i) {
        return c(i * 4) / 4;
    }

    private int c(int i) {
        for (int i2 = 4; i2 < 32; i2++) {
            if (i <= (1 << i2) - 12) {
                return (1 << i2) - 12;
            }
        }
        return i;
    }

    private void d() {
        int i = this.e;
        int[] iArr = this.c;
        bc[] bcVarArr = this.d;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            bc bcVar = bcVarArr[i3];
            if (bcVar != a) {
                if (i3 != i2) {
                    iArr[i2] = iArr[i3];
                    bcVarArr[i2] = bcVar;
                    bcVarArr[i3] = null;
                }
                i2++;
            }
        }
        this.b = false;
        this.e = i2;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        if (this.b) {
            d();
        }
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public bc a(int i) {
        if (this.b) {
            d();
        }
        return this.d[i];
    }

    public boolean b() {
        return a() == 0;
    }

    /* renamed from: c */
    public final bb clone() {
        int a2 = a();
        bb bbVar = new bb(a2);
        System.arraycopy(this.c, 0, bbVar.c, 0, a2);
        for (int i = 0; i < a2; i++) {
            if (this.d[i] != null) {
                bbVar.d[i] = this.d[i].clone();
            }
        }
        bbVar.e = a2;
        return bbVar;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof bb)) {
            return false;
        }
        bb bbVar = (bb) obj;
        if (a() != bbVar.a()) {
            return false;
        }
        return a(this.c, bbVar.c, this.e) && a(this.d, bbVar.d, this.e);
    }

    public int hashCode() {
        if (this.b) {
            d();
        }
        int i = 17;
        for (int i2 = 0; i2 < this.e; i2++) {
            i = (((i * 31) + this.c[i2]) * 31) + this.d[i2].hashCode();
        }
        return i;
    }
}
