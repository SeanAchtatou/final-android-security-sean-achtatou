package com.tendcloud.tenddata;

import java.io.IOException;

public class be extends IOException {
    private static final long a = -1616151763072450476L;

    public be(String str) {
        super(str);
    }

    static be a() {
        return new be("While parsing a protocol message, the input ended unexpectedly in the middle of a field. This could mean either than the input has been truncated or that an embedded message misreported its own length.");
    }

    static be b() {
        return new be("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    static be c() {
        return new be("CodedInputStream encountered a malformed varint.");
    }

    static be d() {
        return new be("Protocol message contained an invalid tag (zero).");
    }

    static be e() {
        return new be("Protocol message end-group tag did not match expected tag.");
    }

    static be f() {
        return new be("Protocol message tag had invalid wire type.");
    }

    static be g() {
        return new be("Protocol message had too many levels of nesting. May be malicious. Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }
}
