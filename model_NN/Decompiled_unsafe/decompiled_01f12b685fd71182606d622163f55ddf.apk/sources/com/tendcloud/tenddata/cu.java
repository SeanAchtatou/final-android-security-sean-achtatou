package com.tendcloud.tenddata;

import android.content.Context;
import com.datalab.tools.Constant;
import com.tendcloud.tenddata.bk;
import java.util.ArrayList;

final class cu implements Runnable {
    final /* synthetic */ Context a;

    cu(Context context) {
        this.a = context;
    }

    public void run() {
        String b = cv.b(this.a);
        ArrayList arrayList = new ArrayList();
        arrayList.add(new cw("app", b));
        arrayList.add(new cw("os", "android"));
        arrayList.add(new cw("ver", bn.a()));
        arrayList.add(new cw("made", bn.b()));
        arrayList.add(new cw("brand", bn.c() + "@" + bn.d()));
        arrayList.add(new cw("screen", bn.a(this.a)));
        arrayList.add(new cw("lang", bn.i()));
        arrayList.add(new cw("country", cv.f(this.a)));
        arrayList.add(new cw("zone", bn.j()));
        arrayList.add(new cw("fmt", Constant.S_C));
        bk.d a2 = bk.a(this.a, cv.a(), dc.G, ct.b(this.a, String.format("/api/statis/%s/%s", cv.a(this.a), b)), ct.b, ct.a(arrayList));
        if (a2.a == 200) {
            cr.a(dc.m, "true");
        } else {
            cs.b(dq.a, "setStatisOnly failed " + a2.a + "   " + a2.b);
        }
    }
}
