package com.tendcloud.tenddata;

public interface TalkingDataSMSApplyCallback {
    void onApplyFailed(int i, String str);

    void onApplySucc(String str);
}
