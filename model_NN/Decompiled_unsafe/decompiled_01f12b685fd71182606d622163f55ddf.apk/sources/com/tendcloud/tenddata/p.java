package com.tendcloud.tenddata;

import android.annotation.SuppressLint;
import com.badlogic.gdx.net.HttpRequestHeader;
import com.tendcloud.tenddata.ad;
import com.tendcloud.tenddata.d;
import com.tendcloud.tenddata.l;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@SuppressLint({"UseValueOf"})
public class p extends o {
    private static final byte[] n = {-1, 0};
    private boolean m = false;
    private final Random o = new Random();

    private static byte[] a(String str) {
        try {
            long parseLong = Long.parseLong(str.replaceAll("[^0-9]", ""));
            long length = (long) (str.split(" ").length - 1);
            if (length == 0) {
                throw new t("invalid Sec-WebSocket-Key (/key2/)");
            }
            long longValue = new Long(parseLong / length).longValue();
            return new byte[]{(byte) ((int) (longValue >> 24)), (byte) ((int) ((longValue << 8) >> 24)), (byte) ((int) ((longValue << 16) >> 24)), (byte) ((int) ((longValue << 24) >> 24))};
        } catch (NumberFormatException e) {
            throw new t("invalid Sec-WebSocket-Key (/key1/ or /key2/)");
        }
    }

    public static byte[] a(String str, String str2, byte[] bArr) {
        byte[] a = a(str);
        byte[] a2 = a(str2);
        try {
            return MessageDigest.getInstance("MD5").digest(new byte[]{a[0], a[1], a[2], a[3], a2[0], a2[1], a2[2], a2[3], bArr[0], bArr[1], bArr[2], bArr[3], bArr[4], bArr[5], bArr[6], bArr[7]});
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private static String f() {
        Random random = new Random();
        long nextInt = (long) (random.nextInt(12) + 1);
        String l = Long.toString(((long) (random.nextInt(Math.abs(new Long(4294967295L / nextInt).intValue())) + 1)) * nextInt);
        int nextInt2 = random.nextInt(12) + 1;
        for (int i = 0; i < nextInt2; i++) {
            int abs = Math.abs(random.nextInt(l.length()));
            char nextInt3 = (char) (random.nextInt(95) + 33);
            if (nextInt3 >= '0' && nextInt3 <= '9') {
                nextInt3 = (char) (nextInt3 - 15);
            }
            l = new StringBuilder(l).insert(abs, nextInt3).toString();
        }
        String str = l;
        for (int i2 = 0; ((long) i2) < nextInt; i2++) {
            str = new StringBuilder(str).insert(Math.abs(random.nextInt(str.length() - 1) + 1), " ").toString();
        }
        return str;
    }

    public ah a(ah ahVar) {
        ahVar.a("Upgrade", "WebSocket");
        ahVar.a("Connection", "Upgrade");
        ahVar.a("Sec-WebSocket-Key1", f());
        ahVar.a("Sec-WebSocket-Key2", f());
        if (!ahVar.b(HttpRequestHeader.Origin)) {
            ahVar.a(HttpRequestHeader.Origin, "random" + this.o.nextInt());
        }
        byte[] bArr = new byte[8];
        this.o.nextBytes(bArr);
        ahVar.setContent(bArr);
        return ahVar;
    }

    public ai a(af afVar, ap apVar) {
        apVar.setHttpStatusMessage("WebSocket Protocol Handshake");
        apVar.a("Upgrade", "WebSocket");
        apVar.a("Connection", afVar.a("Connection"));
        apVar.a("Sec-WebSocket-Origin", afVar.a(HttpRequestHeader.Origin));
        apVar.a("Sec-WebSocket-Location", "ws://" + afVar.a(HttpRequestHeader.Host) + afVar.a());
        String a = afVar.a("Sec-WebSocket-Key1");
        String a2 = afVar.a("Sec-WebSocket-Key2");
        byte[] d = afVar.d();
        if (a == null || a2 == null || d == null || d.length != 8) {
            throw new t("Bad keys");
        }
        apVar.setContent(a(a, a2, d));
        return apVar;
    }

    public l.b a(af afVar) {
        return (!afVar.a("Upgrade").equals("WebSocket") || !afVar.a("Connection").contains("Upgrade") || afVar.a("Sec-WebSocket-Key1").length() <= 0 || afVar.a("Sec-WebSocket-Key2").isEmpty() || !afVar.b(HttpRequestHeader.Origin)) ? l.b.NOT_MATCHED : l.b.MATCHED;
    }

    public l.b a(af afVar, an anVar) {
        if (this.m) {
            return l.b.NOT_MATCHED;
        }
        try {
            if (!anVar.a("Sec-WebSocket-Origin").equals(afVar.a(HttpRequestHeader.Origin)) || !a(anVar)) {
                return l.b.NOT_MATCHED;
            }
            byte[] d = anVar.d();
            if (d != null && d.length != 0) {
                return Arrays.equals(d, a(afVar.a("Sec-WebSocket-Key1"), afVar.a("Sec-WebSocket-Key2"), afVar.d())) ? l.b.MATCHED : l.b.NOT_MATCHED;
            }
            throw new q();
        } catch (t e) {
            throw new RuntimeException("bad handshakerequest", e);
        }
    }

    public ByteBuffer a(ad adVar) {
        return adVar.f() == ad.a.CLOSING ? ByteBuffer.wrap(n) : super.a(adVar);
    }

    public l.a b() {
        return l.a.ONEWAY;
    }

    public l c() {
        return new p();
    }

    public List c(ByteBuffer byteBuffer) {
        byteBuffer.mark();
        List e = super.e(byteBuffer);
        if (e == null) {
            byteBuffer.reset();
            e = this.k;
            this.j = true;
            if (this.l == null) {
                this.l = ByteBuffer.allocate(2);
                if (byteBuffer.remaining() > this.l.remaining()) {
                    throw new s();
                }
                this.l.put(byteBuffer);
                if (this.l.hasRemaining()) {
                    this.k = new LinkedList();
                } else if (Arrays.equals(this.l.array(), n)) {
                    e.add(new z(1000));
                } else {
                    throw new s();
                }
            } else {
                throw new s();
            }
        }
        return e;
    }

    public al d(ByteBuffer byteBuffer) {
        ai a = a(byteBuffer, this.d);
        if ((a.b("Sec-WebSocket-Key1") || this.d == d.b.CLIENT) && !a.b("Sec-WebSocket-Version")) {
            byte[] bArr = new byte[(this.d == d.b.SERVER ? 8 : 16)];
            try {
                byteBuffer.get(bArr);
                a.setContent(bArr);
            } catch (BufferUnderflowException e) {
                throw new q(byteBuffer.capacity() + 16);
            }
        }
        return a;
    }
}
