package com.tendcloud.tenddata;

import java.util.List;
import java.util.Map;

class cp {
    cp() {
    }

    static boolean a(Object obj) {
        return obj == null;
    }

    static boolean a(String str) {
        return str == null || str.length() == 0;
    }

    static boolean a(List list) {
        return list == null || list.size() == 0;
    }

    static boolean a(Map map) {
        return map == null;
    }
}
