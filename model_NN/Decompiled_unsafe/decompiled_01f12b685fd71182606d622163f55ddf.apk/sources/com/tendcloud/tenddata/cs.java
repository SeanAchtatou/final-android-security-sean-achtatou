package com.tendcloud.tenddata;

import android.util.Log;

class cs {
    private static boolean a = true;

    cs() {
    }

    static void a(String str, String str2) {
        if (a && !cp.a(str2)) {
            Log.d(str, str2);
        }
    }

    static void a(String str, String str2, Throwable th) {
        if (a && !cp.a(str2)) {
            Log.e(str, str2, th);
        }
    }

    static void a(boolean z) {
        a = z;
    }

    static void b(String str, String str2) {
        if (a && !cp.a(str2)) {
            Log.e(str, str2);
        }
    }

    static void c(String str, String str2) {
        if (!cp.a(str2)) {
            Log.e(str, str2);
        }
    }

    static void d(String str, String str2) {
        if (a && !cp.a(str2)) {
            Log.i(str, str2);
        }
    }

    static void e(String str, String str2) {
        if (a && !cp.a(str2)) {
            Log.w(str, str2);
        }
    }
}
