package com.tendcloud.tenddata;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.Signature;
import java.io.File;

public class bj {
    private static volatile bj a = null;
    private PackageInfo b = null;

    private bj() {
    }

    public static bj a() {
        if (a == null) {
            synchronized (bj.class) {
                if (a == null) {
                    a = new bj();
                }
            }
        }
        return a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
        r0 = false;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean i(android.content.Context r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            android.content.pm.PackageInfo r0 = r3.b     // Catch:{ Throwable -> 0x0018, all -> 0x001b }
            if (r0 != 0) goto L_0x0015
            android.content.pm.PackageManager r0 = r4.getPackageManager()     // Catch:{ Throwable -> 0x0018, all -> 0x001b }
            java.lang.String r1 = r4.getPackageName()     // Catch:{ Throwable -> 0x0018, all -> 0x001b }
            r2 = 64
            android.content.pm.PackageInfo r0 = r0.getPackageInfo(r1, r2)     // Catch:{ Throwable -> 0x0018, all -> 0x001b }
            r3.b = r0     // Catch:{ Throwable -> 0x0018, all -> 0x001b }
        L_0x0015:
            r0 = 1
        L_0x0016:
            monitor-exit(r3)
            return r0
        L_0x0018:
            r0 = move-exception
            r0 = 0
            goto L_0x0016
        L_0x001b:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.bj.i(android.content.Context):boolean");
    }

    public String a(Context context) {
        if (context == null) {
            return null;
        }
        i(context);
        return context.getPackageName();
    }

    public int b(Context context) {
        if (context == null) {
            return -1;
        }
        try {
            if (i(context)) {
                return this.b.versionCode;
            }
            return -1;
        } catch (Throwable th) {
            return -1;
        }
    }

    public String c(Context context) {
        if (context == null) {
            return "unknown";
        }
        try {
            return !i(context) ? "unknown" : this.b.versionName;
        } catch (Throwable th) {
            return "unknown";
        }
    }

    public long d(Context context) {
        if (context == null) {
            return -1;
        }
        try {
            if (!i(context) || !ca.a(9)) {
                return -1;
            }
            return this.b.firstInstallTime;
        } catch (Throwable th) {
            return -1;
        }
    }

    public long e(Context context) {
        if (context == null) {
            return -1;
        }
        try {
            if (!i(context) || !ca.a(9)) {
                return -1;
            }
            return this.b.lastUpdateTime;
        } catch (Throwable th) {
            return -1;
        }
    }

    public long f(Context context) {
        if (context == null) {
            return -1;
        }
        try {
            i(context);
            return new File(context.getApplicationInfo().sourceDir).length();
        } catch (Throwable th) {
            return -1;
        }
    }

    public String g(Context context) {
        if (context == null) {
            return null;
        }
        try {
            if (!i(context)) {
                return null;
            }
            Signature[] signatureArr = this.b.signatures;
            if (signatureArr.length < 1) {
                return null;
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(signatureArr[0].toCharsString());
            return stringBuffer.toString();
        } catch (Throwable th) {
            return null;
        }
    }

    public String h(Context context) {
        if (context == null) {
            return null;
        }
        try {
            i(context);
            return context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
        } catch (Throwable th) {
            return null;
        }
    }
}
