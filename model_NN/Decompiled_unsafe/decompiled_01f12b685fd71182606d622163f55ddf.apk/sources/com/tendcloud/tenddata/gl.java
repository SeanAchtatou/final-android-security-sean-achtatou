package com.tendcloud.tenddata;

import android.content.Context;
import android.os.Message;
import com.tendcloud.tenddata.gd;

final class gl implements Runnable {
    final /* synthetic */ Throwable a;
    final /* synthetic */ Context b;

    gl(Throwable th, Context context) {
        this.a = th;
        this.b = context;
    }

    public void run() {
        try {
            if (this.a != null) {
                if (this.b != null && !gd.b) {
                    gd.a(this.b, (String) null, (String) null);
                }
                gd.a aVar = new gd.a();
                aVar.a.put("apiType", 5);
                aVar.a.put("occurTime", String.valueOf(System.currentTimeMillis()));
                aVar.a.put("throwable", this.a);
                Message.obtain(er.a(), 102, aVar).sendToTarget();
            }
        } catch (Throwable th) {
        }
    }
}
