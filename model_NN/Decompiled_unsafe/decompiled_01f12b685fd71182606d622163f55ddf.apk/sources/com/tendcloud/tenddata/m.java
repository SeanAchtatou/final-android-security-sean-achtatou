package com.tendcloud.tenddata;

import android.annotation.SuppressLint;
import com.tendcloud.tenddata.ad;
import com.tendcloud.tenddata.d;
import com.tendcloud.tenddata.l;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@SuppressLint({"Assert", "UseValueOf"})
public class m extends l {
    static final /* synthetic */ boolean f = (!m.class.desiredAssertionStatus());
    private ByteBuffer g;
    private ad h = null;
    private final Random i = new Random();

    class a extends Throwable {
        private static final long b = 7330519489840500997L;
        private int c;

        public a(int i) {
            this.c = i;
        }

        public int a() {
            return this.c;
        }
    }

    private byte a(ad.a aVar) {
        if (aVar == ad.a.CONTINUOUS) {
            return 0;
        }
        if (aVar == ad.a.TEXT) {
            return 1;
        }
        if (aVar == ad.a.BINARY) {
            return 2;
        }
        if (aVar == ad.a.CLOSING) {
            return 8;
        }
        if (aVar == ad.a.PING) {
            return 9;
        }
        if (aVar == ad.a.PONG) {
            return 10;
        }
        throw new RuntimeException("Don't know how to handle " + aVar.toString());
    }

    private ad.a a(byte b) {
        switch (b) {
            case 0:
                return ad.a.CONTINUOUS;
            case 1:
                return ad.a.TEXT;
            case 2:
                return ad.a.BINARY;
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            default:
                throw new s("unknow optcode " + ((int) ((short) b)));
            case 8:
                return ad.a.CLOSING;
            case 9:
                return ad.a.PING;
            case 10:
                return ad.a.PONG;
        }
    }

    private String a(String str) {
        try {
            return au.a(MessageDigest.getInstance("SHA1").digest((str.trim() + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11").getBytes()));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    private byte[] a(long j, int i2) {
        byte[] bArr = new byte[i2];
        int i3 = (i2 * 8) - 8;
        for (int i4 = 0; i4 < i2; i4++) {
            bArr[i4] = (byte) ((int) (j >>> (i3 - (i4 * 8))));
        }
        return bArr;
    }

    public static int b(al alVar) {
        String a2 = alVar.a("Sec-WebSocket-Version");
        if (a2.length() <= 0) {
            return -1;
        }
        try {
            return new Integer(a2.trim()).intValue();
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    public ah a(ah ahVar) {
        ahVar.a("Upgrade", "websocket");
        ahVar.a("Connection", "Upgrade");
        ahVar.a("Sec-WebSocket-Version", "8");
        byte[] bArr = new byte[16];
        this.i.nextBytes(bArr);
        ahVar.a("Sec-WebSocket-Key", au.a(bArr));
        return ahVar;
    }

    public ai a(af afVar, ap apVar) {
        apVar.a("Upgrade", "websocket");
        apVar.a("Connection", afVar.a("Connection"));
        apVar.setHttpStatusMessage("Switching Protocols");
        String a2 = afVar.a("Sec-WebSocket-Key");
        if (a2 == null) {
            throw new t("missing Sec-WebSocket-Key");
        }
        apVar.a("Sec-WebSocket-Accept", a(a2));
        return apVar;
    }

    public l.b a(af afVar) {
        int b = b(afVar);
        return (b == 7 || b == 8) ? a(afVar) ? l.b.MATCHED : l.b.NOT_MATCHED : l.b.NOT_MATCHED;
    }

    public l.b a(af afVar, an anVar) {
        if (!afVar.b("Sec-WebSocket-Key") || !anVar.b("Sec-WebSocket-Accept")) {
            return l.b.NOT_MATCHED;
        }
        return a(afVar.a("Sec-WebSocket-Key")).equals(anVar.a("Sec-WebSocket-Accept")) ? l.b.MATCHED : l.b.NOT_MATCHED;
    }

    public ByteBuffer a(ad adVar) {
        byte b = Byte.MIN_VALUE;
        int i2 = 0;
        ByteBuffer c = adVar.c();
        boolean z = this.d == d.b.CLIENT;
        int i3 = c.remaining() <= 125 ? 1 : c.remaining() <= 65535 ? 2 : 8;
        ByteBuffer allocate = ByteBuffer.allocate((z ? 4 : 0) + (i3 > 1 ? i3 + 1 : i3) + 1 + c.remaining());
        allocate.put((byte) (((byte) (adVar.d() ? -128 : 0)) | a(adVar.f())));
        byte[] a2 = a((long) c.remaining(), i3);
        if (f || a2.length == i3) {
            if (i3 == 1) {
                byte b2 = a2[0];
                if (!z) {
                    b = 0;
                }
                allocate.put((byte) (b2 | b));
            } else if (i3 == 2) {
                if (!z) {
                    b = 0;
                }
                allocate.put((byte) (b | 126));
                allocate.put(a2);
            } else if (i3 == 8) {
                if (!z) {
                    b = 0;
                }
                allocate.put((byte) (b | Byte.MAX_VALUE));
                allocate.put(a2);
            } else {
                throw new RuntimeException("Size representation not supported/specified");
            }
            if (z) {
                ByteBuffer allocate2 = ByteBuffer.allocate(4);
                allocate2.putInt(this.i.nextInt());
                allocate.put(allocate2.array());
                while (c.hasRemaining()) {
                    allocate.put((byte) (c.get() ^ allocate2.get(i2 % 4)));
                    i2++;
                }
            } else {
                allocate.put(c);
            }
            if (f || allocate.remaining() == 0) {
                allocate.flip();
                return allocate;
            }
            throw new AssertionError(allocate.remaining());
        }
        throw new AssertionError();
    }

    public List a(String str, boolean z) {
        ae aeVar = new ae();
        try {
            aeVar.setPayload(ByteBuffer.wrap(aw.a(str)));
            aeVar.setFin(true);
            aeVar.setOptcode(ad.a.TEXT);
            aeVar.setTransferemasked(z);
            return Collections.singletonList(aeVar);
        } catch (r e) {
            throw new w(e);
        }
    }

    public List a(ByteBuffer byteBuffer, boolean z) {
        ae aeVar = new ae();
        try {
            aeVar.setPayload(byteBuffer);
            aeVar.setFin(true);
            aeVar.setOptcode(ad.a.BINARY);
            aeVar.setTransferemasked(z);
            return Collections.singletonList(aeVar);
        } catch (r e) {
            throw new w(e);
        }
    }

    public void a() {
        this.g = null;
    }

    public l.a b() {
        return l.a.TWOWAY;
    }

    public l c() {
        return new m();
    }

    public List c(ByteBuffer byteBuffer) {
        LinkedList linkedList = new LinkedList();
        if (this.g != null) {
            try {
                byteBuffer.mark();
                int remaining = byteBuffer.remaining();
                int remaining2 = this.g.remaining();
                if (remaining2 > remaining) {
                    this.g.put(byteBuffer.array(), byteBuffer.position(), remaining);
                    byteBuffer.position(remaining + byteBuffer.position());
                    return Collections.emptyList();
                }
                this.g.put(byteBuffer.array(), byteBuffer.position(), remaining2);
                byteBuffer.position(byteBuffer.position() + remaining2);
                linkedList.add(e((ByteBuffer) this.g.duplicate().position(0)));
                this.g = null;
            } catch (a e) {
                this.g.limit();
                ByteBuffer allocate = ByteBuffer.allocate(a(e.a()));
                if (f || allocate.limit() > this.g.limit()) {
                    this.g.rewind();
                    allocate.put(this.g);
                    this.g = allocate;
                    return c(byteBuffer);
                }
                throw new AssertionError();
            }
        }
        while (byteBuffer.hasRemaining()) {
            byteBuffer.mark();
            try {
                linkedList.add(e(byteBuffer));
            } catch (a e2) {
                byteBuffer.reset();
                this.g = ByteBuffer.allocate(a(e2.a()));
                this.g.put(byteBuffer);
            }
        }
        return linkedList;
    }

    public ad e(ByteBuffer byteBuffer) {
        ac aeVar;
        int i2 = 2;
        int remaining = byteBuffer.remaining();
        if (remaining < 2) {
            throw new a(2);
        }
        byte b = byteBuffer.get();
        boolean z = (b >> 8) != 0;
        byte b2 = (byte) ((b & Byte.MAX_VALUE) >> 4);
        if (b2 != 0) {
            throw new s("bad rsv " + ((int) b2));
        }
        byte b3 = byteBuffer.get();
        boolean z2 = (b3 & Byte.MIN_VALUE) != 0;
        int i3 = (byte) (b3 & Byte.MAX_VALUE);
        ad.a a2 = a((byte) (b & 15));
        if (z || !(a2 == ad.a.PING || a2 == ad.a.PONG || a2 == ad.a.CLOSING)) {
            if (i3 < 0 || i3 > 125) {
                if (a2 == ad.a.PING || a2 == ad.a.PONG || a2 == ad.a.CLOSING) {
                    throw new s("more than 125 octets");
                } else if (i3 == 126) {
                    if (remaining < 4) {
                        throw new a(4);
                    }
                    byte[] bArr = new byte[3];
                    bArr[1] = byteBuffer.get();
                    bArr[2] = byteBuffer.get();
                    i3 = new BigInteger(bArr).intValue();
                    i2 = 4;
                } else if (remaining < 10) {
                    throw new a(10);
                } else {
                    byte[] bArr2 = new byte[8];
                    for (int i4 = 0; i4 < 8; i4++) {
                        bArr2[i4] = byteBuffer.get();
                    }
                    long longValue = new BigInteger(bArr2).longValue();
                    if (longValue > 2147483647L) {
                        throw new v("Payloadsize is to big...");
                    }
                    i2 = 10;
                    i3 = (int) longValue;
                }
            }
            int i5 = (z2 ? 4 : 0) + i2 + i3;
            if (remaining < i5) {
                throw new a(i5);
            }
            ByteBuffer allocate = ByteBuffer.allocate(a(i3));
            if (z2) {
                byte[] bArr3 = new byte[4];
                byteBuffer.get(bArr3);
                for (int i6 = 0; i6 < i3; i6++) {
                    allocate.put((byte) (byteBuffer.get() ^ bArr3[i6 % 4]));
                }
            } else {
                allocate.put(byteBuffer.array(), byteBuffer.position(), allocate.limit());
                byteBuffer.position(byteBuffer.position() + allocate.limit());
            }
            if (a2 == ad.a.CLOSING) {
                aeVar = new z();
            } else {
                aeVar = new ae();
                aeVar.setFin(z);
                aeVar.setOptcode(a2);
            }
            allocate.flip();
            aeVar.setPayload(allocate);
            return aeVar;
        }
        throw new s("control frames may no be fragmented");
    }
}
