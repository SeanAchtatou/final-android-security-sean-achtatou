package com.tendcloud.tenddata;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class zw extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        cr.a(context);
        try {
            String action = intent.getAction();
            if (dc.H.equals(action) || dc.I.equals(action) || dc.J.equals(action) || dc.L.equals(action) || dc.K.equals(action)) {
                dr.a(context);
            } else if (dc.N.equals(action)) {
                dr.b(context, intent);
            } else if (dc.M.equals(action)) {
                dr.a(context, intent);
            } else if (dc.Q.equals(action)) {
                dr.d(context, intent);
            } else if (dc.O.equals(action)) {
                dr.c(context, intent);
            }
        } catch (Throwable th) {
            cs.a("PushServiceReceiver", "onReceive err", th);
        }
    }
}
