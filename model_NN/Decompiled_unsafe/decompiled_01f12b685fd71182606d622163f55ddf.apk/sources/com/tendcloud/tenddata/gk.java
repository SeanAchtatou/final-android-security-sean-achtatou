package com.tendcloud.tenddata;

import android.os.Message;
import com.tendcloud.tenddata.gd;

final class gk implements Runnable {
    final /* synthetic */ int a;
    final /* synthetic */ String b;

    gk(int i, String str) {
        this.a = i;
        this.b = str;
    }

    public void run() {
        try {
            if (gd.b) {
                gd.a aVar = new gd.a();
                aVar.a.put("apiType", Integer.valueOf(this.a));
                aVar.a.put("occurTime", String.valueOf(System.currentTimeMillis()));
                aVar.a.put("pageName", this.b == null ? "" : ca.a(this.b));
                Message.obtain(er.a(), 102, aVar).sendToTarget();
            }
        } catch (Throwable th) {
        }
    }
}
