package com.tendcloud.tenddata;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64OutputStream;
import android.util.DisplayMetrics;
import android.util.JsonWriter;
import android.util.LruCache;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.kbz.esotericsoftware.spine.Animation;
import com.tendcloud.tenddata.eb;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONObject;

@TargetApi(16)
class ei {
    /* access modifiers changed from: private */
    public static String f = "";
    private static final int g = 255;
    private final c a = new c();
    private final List b;
    private final b c = new b(255);
    private final Handler d = new Handler(Looper.getMainLooper());
    private final ef e;

    static class a {
        private Bitmap a = null;
        private final Paint b = new Paint(2);

        /* access modifiers changed from: package-private */
        public synchronized String a(Bitmap.CompressFormat compressFormat, int i, OutputStream outputStream) {
            String str;
            if (this.a == null || this.a.getWidth() == 0 || this.a.getHeight() == 0) {
                outputStream.write("null".getBytes());
                str = "";
            } else {
                outputStream.write(34);
                String a2 = ei.a(this.a);
                if (a2 != null && !a2.equals(ei.f)) {
                    String unused = ei.f = a2;
                    Base64OutputStream base64OutputStream = new Base64OutputStream(outputStream, 2);
                    this.a.compress(compressFormat, i, base64OutputStream);
                    base64OutputStream.flush();
                }
                outputStream.write(34);
                str = ei.a(this.a);
            }
            return str;
        }

        /* access modifiers changed from: package-private */
        public synchronized void a(int i, int i2, int i3, Bitmap bitmap) {
            if (!(this.a != null && this.a.getWidth() == i && this.a.getHeight() == i2)) {
                try {
                    this.a = Bitmap.createBitmap(i, i2, Bitmap.Config.RGB_565);
                } catch (OutOfMemoryError e) {
                    this.a = null;
                }
                if (this.a != null) {
                    this.a.setDensity(i3);
                }
            }
            if (this.a != null) {
                new Canvas(this.a).drawBitmap(bitmap, (float) Animation.CurveTimeline.LINEAR, (float) Animation.CurveTimeline.LINEAR, this.b);
            }
            return;
        }
    }

    static class b extends LruCache {
        public b(int i) {
            super(i);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public String create(Class cls) {
            return cls.getCanonicalName();
        }
    }

    static class c implements Callable {
        private dz a;
        private final List b = new ArrayList();
        private final DisplayMetrics c = new DisplayMetrics();
        private final a d = new a();
        private final int e = 160;

        private void a(d dVar) {
            Bitmap bitmap;
            Bitmap bitmap2;
            Boolean bool;
            Bitmap bitmap3;
            View view = dVar.b;
            try {
                Method declaredMethod = View.class.getDeclaredMethod("createSnapshot", Bitmap.Config.class, Integer.TYPE, Boolean.TYPE);
                declaredMethod.setAccessible(true);
                bitmap = (Bitmap) declaredMethod.invoke(view, Bitmap.Config.RGB_565, -1, false);
            } catch (NoSuchMethodException e2) {
                bitmap = null;
            } catch (Exception e3) {
                bitmap = null;
            }
            Boolean bool2 = null;
            if (bitmap == null) {
                try {
                    bool2 = Boolean.valueOf(view.isDrawingCacheEnabled());
                    view.setDrawingCacheEnabled(true);
                    view.buildDrawingCache(true);
                    Boolean bool3 = bool2;
                    bitmap2 = view.getDrawingCache();
                    bool = bool3;
                } catch (RuntimeException e4) {
                    bitmap3 = bitmap;
                }
            } else {
                bitmap2 = bitmap;
                bool = null;
            }
            bitmap3 = bitmap2;
            bool2 = bool;
            float f = 1.0f;
            if (bitmap3 != null) {
                int density = bitmap3.getDensity();
                if (density != 0) {
                    f = 160.0f / ((float) density);
                }
                int width = bitmap3.getWidth();
                int height = bitmap3.getHeight();
                int width2 = (int) (((double) (((float) bitmap3.getWidth()) * f)) + 0.5d);
                int height2 = (int) (((double) (((float) bitmap3.getHeight()) * f)) + 0.5d);
                if (width > 0 && height > 0 && width2 > 0 && height2 > 0) {
                    this.d.a(width2, height2, 160, bitmap3);
                }
            }
            if (bool2 != null && !bool2.booleanValue()) {
                view.setDrawingCacheEnabled(false);
            }
            dVar.d = f;
            dVar.c = this.d;
        }

        /* renamed from: a */
        public List call() {
            this.b.clear();
            for (Activity activity : this.a.a()) {
                String canonicalName = activity.getClass().getCanonicalName();
                View rootView = activity.getWindow().getDecorView().getRootView();
                activity.getWindowManager().getDefaultDisplay().getMetrics(this.c);
                this.b.add(new d(canonicalName, rootView));
            }
            int size = this.b.size();
            for (int i = 0; i < size; i++) {
                a((d) this.b.get(i));
            }
            return this.b;
        }

        public void findInActivities(dz dzVar) {
            this.a = dzVar;
        }
    }

    static class d {
        final String a;
        final View b;
        a c = null;
        float d = 1.0f;

        d(String str, View view) {
            this.a = str;
            this.b = view;
        }
    }

    ei(List list, ef efVar) {
        this.b = list;
        this.e = efVar;
    }

    static String a(Bitmap bitmap) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            return ca.a(MessageDigest.getInstance("MD5").digest(byteArrayOutputStream.toByteArray()));
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
            return "";
        }
    }

    private void b(JsonWriter jsonWriter, View view) {
        float f2;
        float f3 = Animation.CurveTimeline.LINEAR;
        int id = view.getId();
        String a2 = -1 == id ? null : this.e.a(id);
        jsonWriter.beginObject();
        jsonWriter.name("hashCode").value((long) view.hashCode());
        jsonWriter.name(dc.V).value((long) id);
        jsonWriter.name("id_name").value(a2);
        CharSequence contentDescription = view.getContentDescription();
        if (contentDescription == null) {
            jsonWriter.name("contentDescription").nullValue();
        } else {
            jsonWriter.name("contentDescription").value(contentDescription.toString());
        }
        Object tag = view.getTag();
        if (tag == null) {
            jsonWriter.name("tag").nullValue();
        } else if (tag instanceof CharSequence) {
            jsonWriter.name("tag").value(tag.toString());
        }
        jsonWriter.name("top").value((long) view.getTop());
        jsonWriter.name("left").value((long) view.getLeft());
        jsonWriter.name("width").value((long) view.getWidth());
        jsonWriter.name("height").value((long) view.getHeight());
        jsonWriter.name("scrollX").value((long) view.getScrollX());
        jsonWriter.name("scrollY").value((long) view.getScrollY());
        jsonWriter.name("visibility").value((long) view.getVisibility());
        if (Build.VERSION.SDK_INT >= 11) {
            f3 = view.getTranslationX();
            f2 = view.getTranslationY();
        } else {
            f2 = 0.0f;
        }
        jsonWriter.name("translationX").value((double) f3);
        jsonWriter.name("translationY").value((double) f2);
        jsonWriter.name("classes");
        jsonWriter.beginArray();
        Class<?> cls = view.getClass();
        while (true) {
            Class<?> cls2 = cls;
            jsonWriter.value((String) this.c.get(cls2));
            cls = cls2.getSuperclass();
            if (cls == Object.class || cls == null) {
                jsonWriter.endArray();
                c(jsonWriter, view);
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            }
        }
        jsonWriter.endArray();
        c(jsonWriter, view);
        ViewGroup.LayoutParams layoutParams2 = view.getLayoutParams();
        if (layoutParams2 instanceof RelativeLayout.LayoutParams) {
            int[] rules = ((RelativeLayout.LayoutParams) layoutParams2).getRules();
            jsonWriter.name("layoutRules");
            jsonWriter.beginArray();
            for (int i : rules) {
                jsonWriter.value((long) i);
            }
            jsonWriter.endArray();
        }
        jsonWriter.name("subviews");
        jsonWriter.beginArray();
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = viewGroup.getChildAt(i2);
                if (childAt != null) {
                    jsonWriter.value((long) childAt.hashCode());
                }
            }
        }
        jsonWriter.endArray();
        jsonWriter.endObject();
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup2 = (ViewGroup) view;
            int childCount2 = viewGroup2.getChildCount();
            for (int i3 = 0; i3 < childCount2; i3++) {
                View childAt2 = viewGroup2.getChildAt(i3);
                if (childAt2 != null) {
                    b(jsonWriter, childAt2);
                }
            }
        }
    }

    private void c(JsonWriter jsonWriter, View view) {
        Object a2;
        Class<?> cls = view.getClass();
        for (eb.b bVar : this.b) {
            if (!(!bVar.b.isAssignableFrom(cls) || bVar.c == null || (a2 = bVar.c.a(view)) == null)) {
                if (a2 instanceof Number) {
                    jsonWriter.name(bVar.a).value((Number) a2);
                } else if (a2 instanceof Boolean) {
                    jsonWriter.name(bVar.a).value(((Boolean) a2).booleanValue());
                } else if (a2 instanceof ColorStateList) {
                    jsonWriter.name(bVar.a).value(Integer.valueOf(((ColorStateList) a2).getDefaultColor()));
                } else if (a2 instanceof Drawable) {
                    Drawable drawable = (Drawable) a2;
                    Rect bounds = drawable.getBounds();
                    jsonWriter.name(bVar.a);
                    jsonWriter.beginObject();
                    jsonWriter.name("classes");
                    jsonWriter.beginArray();
                    for (Class<?> cls2 = drawable.getClass(); cls2 != Object.class; cls2 = cls2.getSuperclass()) {
                        jsonWriter.value(cls2.getCanonicalName());
                    }
                    jsonWriter.endArray();
                    jsonWriter.name("dimensions");
                    jsonWriter.beginObject();
                    jsonWriter.name("left").value((long) bounds.left);
                    jsonWriter.name("right").value((long) bounds.right);
                    jsonWriter.name("top").value((long) bounds.top);
                    jsonWriter.name("bottom").value((long) bounds.bottom);
                    jsonWriter.endObject();
                    if (drawable instanceof ColorDrawable) {
                        jsonWriter.name("color").value((long) ((ColorDrawable) drawable).getColor());
                    }
                    jsonWriter.endObject();
                } else {
                    jsonWriter.name(bVar.a).value(a2.toString());
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public List a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void a(JsonWriter jsonWriter, View view) {
        jsonWriter.beginArray();
        b(jsonWriter, view);
        jsonWriter.endArray();
    }

    /* access modifiers changed from: package-private */
    public void a(dz dzVar, OutputStream outputStream) {
        this.a.findInActivities(dzVar);
        FutureTask futureTask = new FutureTask(this.a);
        this.d.post(futureTask);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        List emptyList = Collections.emptyList();
        outputStreamWriter.write("[");
        try {
            emptyList = (List) futureTask.get(1, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e2) {
        }
        System.currentTimeMillis();
        int size = emptyList.size();
        int i = 0;
        while (true) {
            if (i < (size == 0 ? 0 : 1)) {
                if (i > 0) {
                    outputStreamWriter.write(",");
                }
                d dVar = (d) emptyList.get(i);
                outputStreamWriter.write("{");
                outputStreamWriter.write("\"activity\":");
                outputStreamWriter.write(JSONObject.quote(dVar.a));
                outputStreamWriter.write(",");
                outputStreamWriter.write("\"scale\":");
                outputStreamWriter.write(String.format("%s", Float.valueOf(dVar.d)));
                outputStreamWriter.write(",");
                outputStreamWriter.write("\"serialized_objects\":");
                JsonWriter jsonWriter = new JsonWriter(outputStreamWriter);
                jsonWriter.beginObject();
                jsonWriter.name("rootObject").value((long) dVar.b.hashCode());
                jsonWriter.name("objects");
                a(jsonWriter, dVar.b);
                jsonWriter.endObject();
                jsonWriter.flush();
                outputStreamWriter.write(",");
                outputStreamWriter.write("\"screenshot\":");
                outputStreamWriter.flush();
                dVar.c.a(Bitmap.CompressFormat.JPEG, 60, outputStream);
                outputStreamWriter.write(",");
                outputStreamWriter.write("\"image_hash\":");
                outputStreamWriter.write(JSONObject.quote(f));
                outputStreamWriter.write("}");
                i++;
            } else {
                outputStreamWriter.write("]");
                outputStreamWriter.flush();
                return;
            }
        }
    }
}
