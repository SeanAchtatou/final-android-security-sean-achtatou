package com.tendcloud.tenddata;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Message;
import android.os.PowerManager;
import android.os.Process;
import android.os.Vibrator;
import android.text.Html;
import com.datalab.tools.Constant;
import com.tendcloud.tenddata.dg;
import java.util.List;
import java.util.TreeMap;
import org.json.JSONArray;
import org.json.JSONObject;

public class dd {
    public static boolean a = false;
    static final String b = "push.xdrig.com";
    static String c = null;
    static final String d = "https";
    static final String e = "https://push.xdrig.com/push";
    static String f = "v1";
    static final String g = "push";
    static final String h = "deviceToken";
    static final String i = "message";
    static boolean j = false;
    static final long k = 864000000;
    private static volatile dd l = null;

    static {
        j = false;
        try {
            j = c(ab.mContext);
            if (j) {
                if (!a) {
                    new Thread(new de()).start();
                }
                b();
            }
        } catch (Throwable th) {
        }
    }

    private dd() {
    }

    public static dd a() {
        if (l == null) {
            synchronized (dd.class) {
                if (l == null) {
                    l = new dd();
                }
            }
        }
        return l;
    }

    static void a(long j2, Context context) {
        try {
            if (ca.b(context, "android.permission.WAKE_LOCK")) {
                ((PowerManager) context.getSystemService("power")).newWakeLock(805306394, "TDAcquireWakeLock").acquire(j2);
            }
        } catch (Throwable th) {
        }
    }

    private static void a(Context context, int i2) {
        try {
            if (ca.b(context, "android.permission.VIBRATE")) {
                ((Vibrator) context.getSystemService("vibrator")).vibrate((long) i2);
            }
        } catch (Throwable th) {
        }
    }

    static void a(Context context, String str) {
        try {
            Message obtain = Message.obtain();
            obtain.what = 103;
            obtain.obj = new dg(str, null, dg.a.UNSHOWN, 0);
            dh.a().sendMessage(obtain);
        } catch (Throwable th) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.dd.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tendcloud.tenddata.dd.a(long, android.content.Context):void
      com.tendcloud.tenddata.dd.a(android.content.Context, int):void
      com.tendcloud.tenddata.dd.a(android.content.Context, java.lang.String):void
      com.tendcloud.tenddata.dd.a(android.content.Context, org.json.JSONObject):void
      com.tendcloud.tenddata.dd.a(java.lang.String, boolean):void */
    static void a(Context context, JSONObject jSONObject) {
        Notification build;
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("msg");
            String string = jSONObject2.getJSONObject(dc.W).getString(dc.Z);
            String string2 = jSONObject2.getJSONObject("content").getString(dc.Z);
            String string3 = jSONObject.getString(Constant.SIGN);
            a(string3, false);
            int currentTimeMillis = (int) System.currentTimeMillis();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
            if (Build.VERSION.SDK_INT < 23) {
                build = new Notification(0, Html.fromHtml(string), System.currentTimeMillis());
                build.icon = e(context);
                build.flags = 16;
            } else {
                build = new Notification.Builder(context).setContentTitle(Html.fromHtml(string)).setContentText(Html.fromHtml(string2)).setSmallIcon(e(context)).build();
                build.flags = 16;
            }
            Intent intent = new Intent(zv.TALKINGDATA_NOTIFICATION_CLICK);
            intent.putExtra(Constant.SIGN, string3);
            intent.putExtra("appkey", ab.getPushAppId(context));
            if (jSONObject2.has("config") && jSONObject2.getJSONObject("config") != null) {
                JSONObject jSONObject3 = jSONObject2.getJSONObject("config");
                if (jSONObject3.has("sound") && jSONObject3.getInt("sound") > 0) {
                    build.defaults |= 1;
                }
                if (jSONObject3.has("vibrate") && jSONObject3.getInt("vibrate") > 0) {
                    build.defaults |= 2;
                }
                if (jSONObject3.has("wakeup") && jSONObject3.getInt("wakeup") > 0) {
                    a(2000, context);
                }
                if (jSONObject3.has("clearable") && jSONObject3.getInt("clearable") == 0) {
                    build.flags = 32;
                    intent.putExtra(dc.V, currentTimeMillis);
                }
            }
            if (!jSONObject.isNull("custom")) {
                intent.putExtra("custom", jSONObject.getJSONObject("custom").toString());
            }
            if (!jSONObject.isNull(dc.ac)) {
                intent.putExtra(dc.ac, jSONObject.getJSONObject(dc.ac).toString());
            }
            PendingIntent broadcast = PendingIntent.getBroadcast(context, currentTimeMillis + 1, intent, 268435456);
            Intent intent2 = new Intent(zv.TALKINGDATA_NOTIFICATION_CANCEL);
            intent2.putExtra(Constant.SIGN, string3);
            intent2.putExtra("appkey", ab.getPushAppId(context));
            PendingIntent broadcast2 = PendingIntent.getBroadcast(context, currentTimeMillis + 2, intent2, 1073741824);
            if (Build.VERSION.SDK_INT < 23) {
                build.getClass().getMethod("setLatestEventInfo", Context.class, CharSequence.class, CharSequence.class, PendingIntent.class).invoke(build, context, Html.fromHtml(string), Html.fromHtml(string2), null);
            }
            build.contentIntent = broadcast;
            build.deleteIntent = broadcast2;
            notificationManager.notify(currentTimeMillis, build);
        } catch (Throwable th) {
        }
    }

    private static void a(String str, String str2, String str3) {
        boolean z = false;
        JSONArray jSONArray = new JSONArray();
        try {
            String e2 = dj.e();
            if (e2.equals("")) {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("3rdAppId", str);
                jSONObject.put("channel", str2);
                jSONObject.put("dt", str3);
                jSONArray.put(jSONObject);
                dj.b(jSONArray.toString());
                return;
            }
            JSONArray jSONArray2 = new JSONArray(e2);
            for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                JSONObject jSONObject2 = jSONArray2.getJSONObject(i2);
                String string = jSONObject2.getString("channel");
                String string2 = jSONObject2.getString("dt");
                String string3 = jSONObject2.getString("3rdAppId");
                if (string.equals(str2)) {
                    z = true;
                    if (!string3.equals(str)) {
                        jSONObject2.put("3rdAppId", str);
                    }
                    if (!string2.equals(str3)) {
                        jSONObject2.put("dt", str3);
                    }
                }
                jSONArray.put(jSONObject2);
            }
            if (!z) {
                JSONObject jSONObject3 = new JSONObject();
                jSONObject3.put("channel", str2);
                jSONObject3.put("3rdAppId", str);
                jSONObject3.put("dt", str3);
                jSONArray.put(jSONObject3);
            }
            dj.b(jSONArray.toString());
        } catch (Throwable th) {
        }
    }

    static void a(String str, boolean z) {
        try {
            Message obtain = Message.obtain();
            obtain.what = 103;
            obtain.obj = new dg(str, null, z ? dg.a.INAPP_SHOW : dg.a.SHOW, 0);
            dh.a().sendMessage(obtain);
        } catch (Throwable th) {
        }
    }

    private static void a(JSONObject jSONObject) {
        try {
            df.a(null, new JSONObject().put("accountId", dj.a(ab.mContext)), null, dj.e());
            if (dj.a(df.a(jSONObject))) {
                Message obtain = Message.obtain();
                obtain.what = 0;
                dk.a().sendMessage(obtain);
            }
        } catch (Throwable th) {
        }
    }

    protected static boolean a(Context context) {
        if (!ca.b(context, "android.permission.GET_TASKS")) {
            return false;
        }
        try {
            if (((ActivityManager) context.getSystemService("activity")).getRunningTasks(1).get(0).baseActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        } catch (Throwable th) {
        }
        return false;
    }

    static void b() {
        Message obtain = Message.obtain();
        obtain.what = 101;
        dh.a().sendMessage(obtain);
        if (Math.abs(System.currentTimeMillis() - dj.d().longValue()) > k) {
            f();
        }
    }

    private static boolean c(Context context) {
        try {
            String str = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).processName;
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
            if (runningAppProcesses != null) {
                for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                    if (Process.myPid() == next.pid && next.processName.equals(str)) {
                        return true;
                    }
                }
            }
        } catch (Throwable th) {
        }
        return false;
    }

    /* access modifiers changed from: private */
    public static void d(Context context) {
        try {
            dq.startPushService(context);
        } catch (Throwable th) {
        }
    }

    private static int e(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).icon;
        } catch (Throwable th) {
            return 0;
        }
    }

    private static int f(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).uid;
        } catch (Throwable th) {
            return 12089;
        }
    }

    static void f() {
        try {
            String e2 = dj.e();
            if (!e2.equals("")) {
                JSONArray jSONArray = new JSONArray(e2);
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i2);
                    Message obtain = Message.obtain();
                    obtain.what = 102;
                    obtain.obj = new dg(jSONObject.getString("3rdAppId"), jSONObject.getString("dt"), jSONObject.getString("channel"));
                    dh.a().sendMessage(obtain);
                    dj.a(System.currentTimeMillis());
                }
            }
        } catch (Throwable th) {
        }
    }

    private static void g(Context context) {
        try {
            Ringtone ringtone = RingtoneManager.getRingtone(context, RingtoneManager.getDefaultUri(2));
            if (ringtone != null && !ringtone.isPlaying()) {
                ringtone.play();
            }
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(dg dgVar) {
        if (dgVar != null) {
            try {
                if (!dgVar.equals("")) {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("domain", g);
                    jSONObject.put("name", i);
                    TreeMap treeMap = new TreeMap();
                    if (dgVar.e() == null) {
                        treeMap.put("msgSign", dgVar.d());
                        treeMap.put(dc.Y, Integer.valueOf(dgVar.f().a()));
                    } else {
                        treeMap.put("msgSign", dgVar.d());
                        treeMap.put(dc.Y, Integer.valueOf(dgVar.f().a()));
                        treeMap.put("url", dgVar.e());
                    }
                    if (dgVar.g() > 0) {
                        treeMap.put("duration", Integer.valueOf(dgVar.g()));
                    }
                    jSONObject.put("data", new JSONObject(treeMap));
                    a(jSONObject);
                }
            } catch (Throwable th) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(dg dgVar) {
        if (dgVar != null) {
            try {
                if (!dgVar.equals("")) {
                    a(dgVar.b(), dgVar.c(), dgVar.a());
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("domain", g);
                    jSONObject.put("name", h);
                    TreeMap treeMap = new TreeMap();
                    treeMap.put("channel", dgVar.c());
                    treeMap.put("appId", dgVar.b());
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("dt", dgVar.a());
                    treeMap.put(h, jSONObject2);
                    jSONObject.put("data", new JSONObject(treeMap));
                    a(jSONObject);
                    dj.a(System.currentTimeMillis());
                }
            } catch (Throwable th) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        try {
            df.c();
            df.a();
            df.b();
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("appKey", df.k);
            jSONObject.put("service", dq.b);
            df.f.put("appKey", new JSONArray().put(jSONObject));
            dk.a().sendEmptyMessage(3);
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        dj.b();
    }

    /* access modifiers changed from: package-private */
    public void e() {
        dj.c();
    }
}
