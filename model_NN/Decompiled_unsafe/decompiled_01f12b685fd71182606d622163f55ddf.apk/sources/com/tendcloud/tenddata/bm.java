package com.tendcloud.tenddata;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.datalab.tools.Constant;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.objectweb.asm.signature.SignatureVisitor;

public class bm {
    static TelephonyManager a = null;
    static String b = null;
    private static final String c = "pref.deviceid.key";
    private static final String d = "00:00:00:00:00:00";
    private static final Pattern e = Pattern.compile("^([0-9A-F]{2}:){5}([0-9A-F]{2})$");
    private static final Pattern f = Pattern.compile("[0-3][0-9a-f]{24,32}");
    private static final Pattern g = Pattern.compile("[0-3][0-9a-f]{32}");
    private static final String h = ".tcookieid";
    private static String i = null;

    public static final String a() {
        try {
            if (ca.a(9)) {
                return Build.SERIAL;
            }
        } catch (Throwable th) {
        }
        return null;
    }

    public static synchronized String a(Context context) {
        String str;
        synchronized (bm.class) {
            if (b == null) {
                b = k(context);
            }
            str = b;
        }
        return str;
    }

    static String a(Context context, boolean z) {
        if (ca.a(23) && context.checkSelfPermission("android.permission.READ_EXTERNAL_STORAGE") != 0) {
            return null;
        }
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return "";
        }
        String a2 = a(new File(Environment.getExternalStorageDirectory(), z ? h : h + j(context)));
        return ca.b(a2) ? a(new File(Environment.getExternalStorageDirectory(), ".tid" + j(context))) : a2;
    }

    private static String a(File file) {
        try {
            if (file.exists() && file.canRead()) {
                FileInputStream fileInputStream = new FileInputStream(file);
                byte[] bArr = new byte[128];
                int read = fileInputStream.read(bArr);
                fileInputStream.close();
                return new String(bArr, 0, read);
            }
        } catch (Throwable th) {
        }
        return null;
    }

    private static void a(Context context, String str) {
        try {
            File[] listFiles = new File("/").listFiles();
            if (listFiles != null && listFiles.length != 0) {
                for (File file : listFiles) {
                    if (file.isDirectory() && !"/sdcard".equals(file.getAbsolutePath())) {
                        if (file.canWrite() && !new File(file, h + j(context)).exists()) {
                            a(new File(file, h), str);
                        }
                        if (file.listFiles() != null) {
                            for (File file2 : file.listFiles()) {
                                if (file2.isDirectory() && file2.canWrite() && !new File(file2, h + j(context)).exists()) {
                                    a(new File(file2, h), str);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Throwable th) {
        }
    }

    private static void a(Context context, String str, boolean z) {
        a(new File(Environment.getExternalStorageDirectory(), z ? h : h + j(context)), str);
    }

    private static void a(File file, String str) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(str.getBytes());
            fileOutputStream.close();
            if (ca.a(9)) {
                file.getClass().getMethod("setReadable", Boolean.TYPE, Boolean.TYPE).invoke(file, true, false);
                return;
            }
            Runtime.getRuntime().exec("chmod 444 " + file.getAbsolutePath());
        } catch (Throwable th) {
        }
    }

    static String b() {
        String str = null;
        try {
            File[] listFiles = new File("/").listFiles();
            if (listFiles != null && listFiles.length != 0) {
                loop0:
                for (File file : listFiles) {
                    if (file.isDirectory() && !"/sdcard".equals(file.getAbsolutePath())) {
                        if (file.canWrite()) {
                            str = a(new File(file, h));
                            if (!ca.b(str)) {
                                break;
                            }
                        }
                        if (file.listFiles() != null) {
                            for (File file2 : file.listFiles()) {
                                if (file2.isDirectory()) {
                                    str = a(new File(file2, h));
                                    if (!ca.b(str)) {
                                        break loop0;
                                    }
                                }
                            }
                            continue;
                        } else {
                            continue;
                        }
                    }
                }
            }
        } catch (Throwable th) {
        }
        return str;
    }

    public static String b(Context context) {
        try {
            return Settings.Secure.getString(context.getContentResolver(), "android_id");
        } catch (Throwable th) {
            return null;
        }
    }

    private static void b(Context context, String str) {
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences("tdid", 0);
            if (sharedPreferences != null) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString(c, str);
                edit.commit();
            }
        } catch (Throwable th) {
        }
    }

    public static String c(Context context) {
        String str;
        try {
            if (ca.a(23) && context.checkSelfPermission("android.permission.READ_PHONE_STATE") != 0) {
                return null;
            }
            if (ca.b(context, "android.permission.READ_PHONE_STATE")) {
                if (a == null) {
                    init(context);
                }
                JSONArray y = br.y(context);
                if (y == null || y.length() != 2) {
                    str = null;
                } else {
                    try {
                        str = y.getJSONObject(1).getString("imei");
                    } catch (Exception e2) {
                        str = null;
                    }
                }
                return str == null ? a.getDeviceId() : str;
            }
            return null;
        } catch (Throwable th) {
        }
    }

    static boolean c() {
        boolean z;
        try {
            z = ca.a(9) ? ((Boolean) Environment.class.getMethod("isExternalStorageRemovable", new Class[0]).invoke(null, new Object[0])).booleanValue() : true;
        } catch (Throwable th) {
            z = true;
        }
        return !z;
    }

    public static String d(Context context) {
        try {
            if ((ca.a(23) && context.checkSelfPermission("android.permission.READ_PHONE_STATE") != 0) || !ca.b(context, "android.permission.READ_PHONE_STATE")) {
                return null;
            }
            if (a == null) {
                init(context);
            }
            return a.getSimSerialNumber();
        } catch (Throwable th) {
            return null;
        }
    }

    public static String e(Context context) {
        try {
            if ((ca.a(23) && context.checkSelfPermission("android.permission.READ_PHONE_STATE") != 0) || !ca.b(context, "android.permission.READ_PHONE_STATE")) {
                return null;
            }
            if (a == null) {
                init(context);
            }
            return a.getSubscriberId();
        } catch (Throwable th) {
            return null;
        }
    }

    public static String f(Context context) {
        WifiInfo connectionInfo;
        String str;
        String str2 = null;
        if (!ca.b) {
            return null;
        }
        try {
            if (ca.a(23)) {
                try {
                    ArrayList<NetworkInterface> list = Collections.list(NetworkInterface.getNetworkInterfaces());
                    if (list == null || list.size() <= 0) {
                        return "02:00:00:00:00:00";
                    }
                    for (NetworkInterface networkInterface : list) {
                        if (networkInterface.getName().equalsIgnoreCase("wlan0")) {
                            byte[] hardwareAddress = networkInterface.getHardwareAddress();
                            if (hardwareAddress == null) {
                                return "";
                            }
                            StringBuilder sb = new StringBuilder();
                            int length = hardwareAddress.length;
                            for (int i2 = 0; i2 < length; i2++) {
                                sb.append(String.format("%02X:", Byte.valueOf(hardwareAddress[i2])));
                            }
                            if (sb.length() > 0) {
                                sb.deleteCharAt(sb.length() - 1);
                            }
                            str2 = sb.toString().toUpperCase().trim();
                        }
                    }
                    str = str2;
                    try {
                        return ca.b(str) ? "02:00:00:00:00:00" : str;
                    } catch (Throwable th) {
                        return str;
                    }
                } catch (Throwable th2) {
                    str = null;
                }
            } else {
                if (ca.b(context, "android.permission.ACCESS_WIFI_STATE")) {
                    WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
                    if (wifiManager.isWifiEnabled() && (connectionInfo = wifiManager.getConnectionInfo()) != null) {
                        String macAddress = connectionInfo.getMacAddress();
                        if (macAddress == null) {
                            return macAddress;
                        }
                        String trim = macAddress.toUpperCase().trim();
                        if (d.equals(trim) || !e.matcher(trim).matches()) {
                            return null;
                        }
                        return trim;
                    }
                }
                return null;
            }
        } catch (Throwable th3) {
            return null;
        }
    }

    public static final String g(Context context) {
        try {
            return (String) Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info").getMethod("getId", new Class[0]).invoke(Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", Context.class).invoke(null, context), new Object[0]);
        } catch (Throwable th) {
            return null;
        }
    }

    public static final String h(Context context) {
        String f2 = f(context);
        if (!TextUtils.isEmpty(f2)) {
            f2 = String.valueOf(Long.parseLong(f2.replaceAll(":", ""), 16));
        }
        String b2 = b(context);
        String c2 = c(context);
        String e2 = e(context);
        String d2 = d(context);
        String a2 = a(context);
        String g2 = g(context);
        return 2 + "|" + f2 + "|" + b2 + "|" + c2 + "|" + e2 + "|" + d2 + "|" + a2 + "|" + g2 + "|" + a();
    }

    static String i(Context context) {
        String b2 = bu.b(context, "tdid", c, (String) null);
        return ca.b(b2) ? PreferenceManager.getDefaultSharedPreferences(context).getString(c, null) : b2;
    }

    public static void init(Context context) {
        a = (TelephonyManager) context.getSystemService("phone");
    }

    static String j(Context context) {
        if (i == null) {
            try {
                Sensor[] sensorArr = new Sensor[64];
                for (Sensor next : ((SensorManager) context.getSystemService("sensor")).getSensorList(-1)) {
                    if (next.getType() < sensorArr.length && next.getType() >= 0) {
                        sensorArr[next.getType()] = next;
                    }
                }
                StringBuffer stringBuffer = new StringBuffer();
                for (int i2 = 0; i2 < sensorArr.length; i2++) {
                    if (sensorArr[i2] != null) {
                        stringBuffer.append(i2).append('.').append(sensorArr[i2].getVendor()).append((char) SignatureVisitor.SUPER).append(sensorArr[i2].getName()).append((char) SignatureVisitor.SUPER).append(sensorArr[i2].getVersion()).append(10);
                    }
                }
                i = String.valueOf(stringBuffer.toString().hashCode());
            } catch (Throwable th) {
            }
        }
        return i;
    }

    private static String k(Context context) {
        String str;
        int i2 = 0;
        String i3 = i(context);
        String b2 = b();
        boolean c2 = c();
        String a2 = a(context, c2);
        String[] strArr = {i3, b2, a2};
        int length = strArr.length;
        int i4 = 0;
        while (true) {
            if (i4 >= length) {
                str = null;
                break;
            }
            str = strArr[i4];
            if (!ca.b(str) && g.matcher(str).matches()) {
                break;
            }
            i4++;
        }
        if (ca.b(str) && !ca.b(i3) && Math.random() < 0.99d) {
            int length2 = strArr.length;
            while (true) {
                if (i2 >= length2) {
                    break;
                }
                String str2 = strArr[i2];
                if (!ca.b(str2) && f.matcher(str2).matches()) {
                    str = str2;
                    break;
                }
                i2++;
            }
        }
        if (ca.b(str)) {
            str = l(context);
        }
        if (!str.equals(i3)) {
            b(context, str);
        }
        if (!str.equals(a2)) {
            a(context, str, c2);
        }
        if (!str.equals(b2)) {
            a(context, str);
        }
        return str;
    }

    private static String l(Context context) {
        return Constant.S_D + ca.c(m(context));
    }

    private static String m(Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append(c(context)).append((char) SignatureVisitor.SUPER).append(f(context)).append((char) SignatureVisitor.SUPER).append(b(context));
        return sb.toString();
    }
}
