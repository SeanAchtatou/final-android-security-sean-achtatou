package com.tendcloud.tenddata;

import android.content.Context;

public class bp implements bo {
    private static final String b = "dyn_update_check_ts";
    Context a;

    public String a() {
        return "1";
    }

    public void a(String str) {
        bu.a(this.a, "actionstablepref", b, System.currentTimeMillis());
    }

    public boolean b() {
        long abs = Math.abs((System.currentTimeMillis() - bu.b(this.a, "actionstablepref", b, System.currentTimeMillis())) / 86400000);
        if (abs <= 7) {
            return ((double) abs) * Math.random() > 2.0d;
        }
    }

    public String c() {
        return "https://u.talkingdata.net/ota/common/android/dynamic/ver";
    }

    public String d() {
        return "https://u.talkingdata.net/ota/common/android/dynamic/sdk.zip";
    }

    public void e() {
    }

    public void f() {
    }

    public void initialize(Context context, String str) {
        this.a = context;
    }
}
