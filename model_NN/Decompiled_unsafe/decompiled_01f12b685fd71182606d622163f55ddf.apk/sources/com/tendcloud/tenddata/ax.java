package com.tendcloud.tenddata;

public final class ax {
    private static final int k = 64;
    private static final int l = 67108864;
    private final byte[] a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g = Integer.MAX_VALUE;
    private int h;
    private int i = 64;
    private int j = l;

    private ax(byte[] bArr, int i2, int i3) {
        this.a = bArr;
        this.b = i2;
        this.c = i2 + i3;
        this.e = i2;
    }

    public static long a(long j2) {
        return (j2 >>> 1) ^ (-(1 & j2));
    }

    public static ax a(byte[] bArr) {
        return a(bArr, 0, bArr.length);
    }

    public static ax a(byte[] bArr, int i2, int i3) {
        return new ax(bArr, i2, i3);
    }

    public static int b(int i2) {
        return (i2 >>> 1) ^ (-(i2 & 1));
    }

    private void y() {
        this.c += this.d;
        int i2 = this.c;
        if (i2 > this.g) {
            this.d = i2 - this.g;
            this.c -= this.d;
            return;
        }
        this.d = 0;
    }

    public int a() {
        if (w()) {
            this.f = 0;
            return 0;
        }
        this.f = s();
        if (this.f != 0) {
            return this.f;
        }
        throw be.d();
    }

    public void a(bf bfVar, int i2) {
        if (this.h >= this.i) {
            throw be.g();
        }
        this.h++;
        bfVar.a(this);
        checkLastTagWas(bi.a(i2, 4));
        this.h--;
    }

    public boolean a(int i2) {
        switch (bi.a(i2)) {
            case 0:
                g();
                return true;
            case 1:
                v();
                return true;
            case 2:
                skipRawBytes(s());
                return true;
            case 3:
                b();
                checkLastTagWas(bi.a(bi.b(i2), 4));
                return true;
            case 4:
                return false;
            case 5:
                u();
                return true;
            default:
                throw be.f();
        }
    }

    public void b() {
        int a2;
        do {
            a2 = a();
            if (a2 == 0) {
                return;
            }
        } while (a(a2));
    }

    public double c() {
        return Double.longBitsToDouble(v());
    }

    public int c(int i2) {
        if (i2 < 0) {
            throw be.b();
        }
        int i3 = this.e + i2;
        int i4 = this.g;
        if (i3 > i4) {
            throw be.a();
        }
        this.g = i3;
        y();
        return i4;
    }

    public void checkLastTagWas(int i2) {
        if (this.f != i2) {
            throw be.e();
        }
    }

    public float d() {
        return Float.intBitsToFloat(u());
    }

    public byte[] d(int i2) {
        if (i2 < 0) {
            throw be.b();
        } else if (this.e + i2 > this.g) {
            skipRawBytes(this.g - this.e);
            throw be.a();
        } else if (i2 <= this.c - this.e) {
            byte[] bArr = new byte[i2];
            System.arraycopy(this.a, this.e, bArr, 0, i2);
            this.e += i2;
            return bArr;
        } else {
            throw be.a();
        }
    }

    public long e() {
        return t();
    }

    public long f() {
        return t();
    }

    public int g() {
        return s();
    }

    public long h() {
        return v();
    }

    public int i() {
        return u();
    }

    public boolean j() {
        return s() != 0;
    }

    public String k() {
        int s = s();
        if (s > this.c - this.e || s <= 0) {
            return new String(d(s), "UTF-8");
        }
        String str = new String(this.a, this.e, s, "UTF-8");
        this.e = s + this.e;
        return str;
    }

    public byte[] l() {
        int s = s();
        if (s > this.c - this.e || s <= 0) {
            return s == 0 ? bi.i : d(s);
        }
        byte[] bArr = new byte[s];
        System.arraycopy(this.a, this.e, bArr, 0, s);
        this.e = s + this.e;
        return bArr;
    }

    public int m() {
        return s();
    }

    public int n() {
        return s();
    }

    public int o() {
        return u();
    }

    public long p() {
        return v();
    }

    public void popLimit(int i2) {
        this.g = i2;
        y();
    }

    public int q() {
        return b(s());
    }

    public long r() {
        return a(t());
    }

    public void readMessage(bf bfVar) {
        int s = s();
        if (this.h >= this.i) {
            throw be.g();
        }
        int c2 = c(s);
        this.h++;
        bfVar.a(this);
        checkLastTagWas(0);
        this.h--;
        popLimit(c2);
    }

    public int s() {
        byte x = x();
        if (x >= 0) {
            return x;
        }
        byte b2 = x & Byte.MAX_VALUE;
        byte x2 = x();
        if (x2 >= 0) {
            return b2 | (x2 << 7);
        }
        byte b3 = b2 | ((x2 & Byte.MAX_VALUE) << 7);
        byte x3 = x();
        if (x3 >= 0) {
            return b3 | (x3 << 14);
        }
        byte b4 = b3 | ((x3 & Byte.MAX_VALUE) << 14);
        byte x4 = x();
        if (x4 >= 0) {
            return b4 | (x4 << 21);
        }
        byte b5 = b4 | ((x4 & Byte.MAX_VALUE) << 21);
        byte x5 = x();
        byte b6 = b5 | (x5 << 28);
        if (x5 >= 0) {
            return b6;
        }
        for (int i2 = 0; i2 < 5; i2++) {
            if (x() >= 0) {
                return b6;
            }
        }
        throw be.c();
    }

    public void skipRawBytes(int i2) {
        if (i2 < 0) {
            throw be.b();
        } else if (this.e + i2 > this.g) {
            skipRawBytes(this.g - this.e);
            throw be.a();
        } else if (i2 <= this.c - this.e) {
            this.e += i2;
        } else {
            throw be.a();
        }
    }

    public long t() {
        long j2 = 0;
        for (int i2 = 0; i2 < 64; i2 += 7) {
            byte x = x();
            j2 |= ((long) (x & Byte.MAX_VALUE)) << i2;
            if ((x & 128) == 0) {
                return j2;
            }
        }
        throw be.c();
    }

    public int u() {
        return (x() & 255) | ((x() & 255) << 8) | ((x() & 255) << 16) | ((x() & 255) << 24);
    }

    public long v() {
        byte x = x();
        byte x2 = x();
        return ((((long) x2) & 255) << 8) | (((long) x) & 255) | ((((long) x()) & 255) << 16) | ((((long) x()) & 255) << 24) | ((((long) x()) & 255) << 32) | ((((long) x()) & 255) << 40) | ((((long) x()) & 255) << 48) | ((((long) x()) & 255) << 56);
    }

    public boolean w() {
        return this.e == this.c;
    }

    public byte x() {
        if (this.e == this.c) {
            throw be.a();
        }
        byte[] bArr = this.a;
        int i2 = this.e;
        this.e = i2 + 1;
        return bArr[i2];
    }
}
