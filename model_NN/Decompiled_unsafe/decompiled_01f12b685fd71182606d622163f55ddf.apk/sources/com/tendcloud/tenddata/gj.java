package com.tendcloud.tenddata;

import android.os.Message;
import com.tendcloud.tenddata.gd;

final class gj implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ boolean b;

    gj(String str, boolean z) {
        this.a = str;
        this.b = z;
    }

    public void run() {
        try {
            ev.a("onPause being called! pageName: " + this.a);
            gd.a aVar = new gd.a();
            aVar.a.put("isPageOrSession", Boolean.valueOf(this.b));
            aVar.a.put("apiType", 3);
            aVar.a.put("occurTime", String.valueOf(System.currentTimeMillis()));
            aVar.a.put("pageName", this.a);
            Message.obtain(er.a(), 102, aVar).sendToTarget();
        } catch (Throwable th) {
            if (TCAgent.LOG_ON) {
                th.printStackTrace();
            }
        }
    }
}
