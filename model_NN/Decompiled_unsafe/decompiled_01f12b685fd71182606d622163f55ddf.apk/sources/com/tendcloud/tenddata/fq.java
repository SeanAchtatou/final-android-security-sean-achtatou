package com.tendcloud.tenddata;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.net.TrafficStats;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.sg.pak.PAK_ASSETS;
import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.CRC32;
import org.json.JSONArray;
import org.json.JSONObject;

final class fq {
    static boolean a = true;
    static String b = "v1";
    private static volatile fq c = null;
    private static String k;
    private static final CRC32 v = new CRC32();
    private static String w = "";
    private Handler d;
    private HandlerThread e;
    private List f;
    private long g;
    private Map h;
    private StringBuffer i;
    private StringBuffer j;
    private WifiManager l;
    private boolean m;
    /* access modifiers changed from: private */
    public boolean n;
    private int o;
    /* access modifiers changed from: private */
    public int p;
    private FileChannel q;
    private final String r;
    private final String s;
    private final String t;
    private final String u;

    static class a {
        static final int a = 0;
        static final int b = 1;
        static final int c = 2;
        static final int d = 3;
        static final int e = 4;
        static final int f = 5;
        static final int g = 6;
        static final int h = 7;
        private static final long i = 60000;
        private static final double j = 0.7d;
        private static final long k = 180000;
        private static final long l = 180000;
        private static final long m = 604800000;
        private static final long n = 600000;
        private static final long o = 600000;
        private static final long p = 600000;
        private static final long q = 1800000;
        private static final long r = 3600000;

        a() {
        }
    }

    class b extends BroadcastReceiver {
        private String b = null;

        b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tendcloud.tenddata.fq.a(com.tendcloud.tenddata.fq, boolean):boolean
         arg types: [com.tendcloud.tenddata.fq, int]
         candidates:
          com.tendcloud.tenddata.fq.a(com.tendcloud.tenddata.fq, int):int
          com.tendcloud.tenddata.fq.a(int, long):void
          com.tendcloud.tenddata.fq.a(com.tendcloud.tenddata.fq, boolean):boolean */
        public void onReceive(Context context, Intent intent) {
            try {
                this.b = intent.getAction();
                if (!"android.intent.action.SCREEN_ON".equals(this.b)) {
                    if ("android.intent.action.SCREEN_OFF".equals(this.b)) {
                        boolean unused = fq.this.n = false;
                    } else if (dc.J.equals(this.b)) {
                        boolean unused2 = fq.this.n = true;
                    }
                }
            } catch (Throwable th) {
            }
        }
    }

    class c extends PhoneStateListener {
        long a = 0;
        long b = 0;
        int c;

        c() {
        }

        private void a() {
            try {
                this.a = System.currentTimeMillis();
                if (this.c != fq.this.p && this.c > 1 && this.a - this.b > 180000) {
                    String str = null;
                    if (ab.h) {
                        str = ew.b();
                    }
                    JSONArray s = br.s(ab.mContext);
                    JSONArray t = br.t(ab.mContext);
                    if (!(s == null && t == null)) {
                        fs.a().b(s, t, str);
                    }
                    this.b = this.a;
                    int unused = fq.this.p = this.c;
                }
            } catch (Throwable th) {
            }
        }

        public void onCellLocationChanged(CellLocation cellLocation) {
            try {
                if (cellLocation.getClass().equals(GsmCellLocation.class)) {
                    this.c = ((GsmCellLocation) cellLocation).getLac();
                    a();
                } else if (cellLocation.getClass().equals(CdmaCellLocation.class)) {
                    this.c = ((CdmaCellLocation) cellLocation).getNetworkId();
                    a();
                }
            } catch (Throwable th) {
            }
        }
    }

    private fq() {
        this.d = null;
        this.e = null;
        this.f = null;
        this.i = new StringBuffer();
        this.j = new StringBuffer();
        this.m = false;
        this.n = true;
        this.o = 0;
        this.p = 0;
        this.r = "me.xdrig.com";
        this.s = null;
        this.t = "https";
        this.u = "https://me.xdrig.com";
        this.h = new HashMap();
        this.e = new HandlerThread("envHandlerThread");
        this.e.start();
        this.d = new fr(this, this.e.getLooper());
        a(4, 0);
    }

    private int a(byte[] bArr) {
        try {
            v.reset();
            v.update(bArr);
            StringBuilder sb = new StringBuilder("https://me.xdrig.com");
            sb.append("/" + b);
            sb.append("/" + Long.toHexString(v.getValue()));
            return bk.a(ab.mContext, "me.xdrig.com", this.s, sb.toString(), w, bArr).a();
        } catch (Throwable th) {
            return PAK_ASSETS.IMG_G02;
        }
    }

    private long a(String str) {
        long j2 = 1125899906842597L;
        int length = str.length();
        for (int i2 = 0; i2 < length; i2++) {
            j2 = (j2 * 131) + ((long) str.charAt(i2));
        }
        return j2;
    }

    static fq a() {
        if (c == null) {
            synchronized (fq.class) {
                if (c == null) {
                    c = new fq();
                }
            }
        }
        return c;
    }

    private String a(Context context) {
        return null;
    }

    private void a(int i2, long j2) {
        Message obtain = Message.obtain();
        obtain.what = i2;
        this.d.sendMessageDelayed(obtain, j2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0050, code lost:
        r1 = th;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0050 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:13:0x001a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] a(java.io.File r8) {
        /*
            r3 = 0
            r0 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x003c, all -> 0x0044 }
            r2.<init>(r8)     // Catch:{ Throwable -> 0x003c, all -> 0x0044 }
            long r4 = r8.length()     // Catch:{ Throwable -> 0x0052, all -> 0x0050 }
            r6 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r1 <= 0) goto L_0x0016
            r2.close()     // Catch:{ Throwable -> 0x004a }
        L_0x0015:
            return r0
        L_0x0016:
            int r1 = (int) r4
            byte[] r1 = new byte[r1]     // Catch:{ Throwable -> 0x0052, all -> 0x0050 }
            r4 = r3
        L_0x001a:
            int r5 = r1.length     // Catch:{ Throwable -> 0x0027, all -> 0x0050 }
            if (r4 >= r5) goto L_0x002e
            if (r3 < 0) goto L_0x002e
            int r3 = r1.length     // Catch:{ Throwable -> 0x0027, all -> 0x0050 }
            int r3 = r3 - r4
            int r3 = r2.read(r1, r4, r3)     // Catch:{ Throwable -> 0x0027, all -> 0x0050 }
            int r4 = r4 + r3
            goto L_0x001a
        L_0x0027:
            r1 = move-exception
            r2.close()     // Catch:{ Throwable -> 0x002c }
            goto L_0x0015
        L_0x002c:
            r1 = move-exception
            goto L_0x0015
        L_0x002e:
            int r3 = r1.length     // Catch:{ Throwable -> 0x0052, all -> 0x0050 }
            if (r4 >= r3) goto L_0x0037
            r2.close()     // Catch:{ Throwable -> 0x0035 }
            goto L_0x0015
        L_0x0035:
            r1 = move-exception
            goto L_0x0015
        L_0x0037:
            r2.close()     // Catch:{ Throwable -> 0x004c }
            r0 = r1
            goto L_0x0015
        L_0x003c:
            r1 = move-exception
            r1 = r0
        L_0x003e:
            r1.close()     // Catch:{ Throwable -> 0x0042 }
            goto L_0x0015
        L_0x0042:
            r1 = move-exception
            goto L_0x0015
        L_0x0044:
            r1 = move-exception
            r2 = r0
        L_0x0046:
            r2.close()     // Catch:{ Throwable -> 0x004e }
            throw r1
        L_0x004a:
            r1 = move-exception
            goto L_0x0015
        L_0x004c:
            r1 = move-exception
            goto L_0x0015
        L_0x004e:
            r1 = move-exception
            goto L_0x0015
        L_0x0050:
            r1 = move-exception
            goto L_0x0046
        L_0x0052:
            r1 = move-exception
            r1 = r2
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.fq.a(java.io.File):byte[]");
    }

    private void c() {
        try {
            if (this.q == null) {
                return;
            }
            if (this.q.tryLock() != null) {
                this.m = true;
            } else {
                this.m = false;
            }
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        try {
            Thread.sleep(5000);
            File file = new File(ab.mContext.getFilesDir(), "tdlock.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            this.q = new RandomAccessFile(file, "rw").getChannel();
            c();
            if (!this.m) {
                b();
            } else {
                e();
            }
        } catch (Throwable th) {
        }
    }

    private void e() {
        g();
        h();
        j();
        f();
        k();
    }

    private void f() {
        try {
            JSONObject jSONObject = new JSONObject();
            if (ca.a(8)) {
                jSONObject.put("wifiTx", TrafficStats.getTotalTxBytes());
                jSONObject.put("wifiRx", TrafficStats.getTotalRxBytes());
                jSONObject.put("mobileTx", TrafficStats.getMobileTxBytes());
                jSONObject.put("mobileRx", TrafficStats.getMobileRxBytes());
                fs.a().a(jSONObject);
            }
        } catch (Throwable th) {
        }
    }

    private void g() {
        if (ab.mContext != null) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.intent.action.SCREEN_ON");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            intentFilter.addAction(dc.J);
            ab.mContext.registerReceiver(new b(), intentFilter);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    @android.annotation.TargetApi(23)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void h() {
        /*
            r4 = this;
            r0 = 23
            boolean r0 = com.tendcloud.tenddata.ca.a(r0)     // Catch:{ Throwable -> 0x003b }
            if (r0 == 0) goto L_0x003d
            android.content.Context r0 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x0036 }
            java.lang.String r1 = "android.permission.READ_PHONE_STATE"
            int r0 = r0.checkSelfPermission(r1)     // Catch:{ Throwable -> 0x0036 }
            if (r0 == 0) goto L_0x001a
            r0 = 6
            r2 = 3600000(0x36ee80, double:1.7786363E-317)
            r4.a(r0, r2)     // Catch:{ Throwable -> 0x0036 }
        L_0x0019:
            return
        L_0x001a:
            android.content.Context r0 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x0036 }
            java.lang.String r1 = "android.permission.ACCESS_COARSE_LOCATION"
            int r0 = r0.checkSelfPermission(r1)     // Catch:{ Throwable -> 0x0036 }
            if (r0 == 0) goto L_0x0037
            android.content.Context r0 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x0036 }
            java.lang.String r1 = "android.permission.ACCESS_FINE_LOCATION"
            int r0 = r0.checkSelfPermission(r1)     // Catch:{ Throwable -> 0x0036 }
            if (r0 == 0) goto L_0x0037
            r0 = 6
            r2 = 3600000(0x36ee80, double:1.7786363E-317)
            r4.a(r0, r2)     // Catch:{ Throwable -> 0x0036 }
            goto L_0x0019
        L_0x0036:
            r0 = move-exception
        L_0x0037:
            r4.i()     // Catch:{ Throwable -> 0x003b }
            goto L_0x0019
        L_0x003b:
            r0 = move-exception
            goto L_0x0019
        L_0x003d:
            android.content.Context r0 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x005c }
            java.lang.String r1 = "android.permission.READ_PHONE_STATE"
            boolean r0 = com.tendcloud.tenddata.ca.b(r0, r1)     // Catch:{ Throwable -> 0x005c }
            if (r0 == 0) goto L_0x0019
            android.content.Context r0 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x005c }
            java.lang.String r1 = "android.permission.ACCESS_COARSE_LOCATION"
            boolean r0 = com.tendcloud.tenddata.ca.b(r0, r1)     // Catch:{ Throwable -> 0x005c }
            if (r0 != 0) goto L_0x0037
            android.content.Context r0 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x005c }
            java.lang.String r1 = "android.permission.ACCESS_FINE_LOCATION"
            boolean r0 = com.tendcloud.tenddata.ca.b(r0, r1)     // Catch:{ Throwable -> 0x005c }
            if (r0 != 0) goto L_0x0037
            goto L_0x0019
        L_0x005c:
            r0 = move-exception
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.fq.h():void");
    }

    private void i() {
        if (ab.mContext != null) {
            try {
                Context context = ab.mContext;
                Context context2 = ab.mContext;
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                if (telephonyManager.getSimState() == 5) {
                    telephonyManager.getCellLocation();
                    telephonyManager.listen(new c(), 16);
                    CellLocation.requestLocationUpdate();
                }
            } catch (Throwable th) {
            }
        }
    }

    /* access modifiers changed from: private */
    @TargetApi(23)
    public void j() {
        if (a) {
            if (ca.a(23)) {
                if (!(ab.mContext.checkSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0 || ab.mContext.checkSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0)) {
                    return;
                }
            } else if (!ca.b(ab.mContext, "android.permission.ACCESS_COARSE_LOCATION") && !ca.b(ab.mContext, "android.permission.ACCESS_FINE_LOCATION")) {
                return;
            }
            try {
                Context context = ab.mContext;
                Context context2 = ab.mContext;
                LocationManager locationManager = (LocationManager) context.getSystemService("location");
                boolean isProviderEnabled = locationManager.isProviderEnabled("gps");
                boolean isProviderEnabled2 = locationManager.isProviderEnabled("network");
                if (isProviderEnabled || isProviderEnabled2) {
                    Location lastKnownLocation = locationManager.getLastKnownLocation("passive");
                    String str = null;
                    if (ab.h) {
                        str = ew.b();
                    }
                    if (lastKnownLocation != null) {
                        try {
                            JSONObject jSONObject = new JSONObject();
                            jSONObject.put("lat", lastKnownLocation.getLatitude());
                            jSONObject.put("lng", lastKnownLocation.getLongitude());
                            jSONObject.put("ts", lastKnownLocation.getTime());
                            if (ca.a(17)) {
                                jSONObject.put("elapsed", lastKnownLocation.getElapsedRealtimeNanos());
                            }
                            if (lastKnownLocation.hasAltitude()) {
                                jSONObject.put("altitude", lastKnownLocation.getAltitude());
                            }
                            if (lastKnownLocation.hasAccuracy()) {
                                jSONObject.put("accurate", (double) lastKnownLocation.getAccuracy());
                            }
                            if (lastKnownLocation.hasBearing()) {
                                jSONObject.put("bearing", (double) lastKnownLocation.getBearing());
                            }
                            if (lastKnownLocation.hasSpeed()) {
                                jSONObject.put("speed", (double) lastKnownLocation.getSpeed());
                            }
                            jSONObject.put("provider", lastKnownLocation.getProvider());
                            fs.a().a(jSONObject, str);
                        } catch (Throwable th) {
                        }
                    }
                }
                a(1, 180000);
            } catch (Throwable th2) {
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void k() {
        /*
            r5 = this;
            r1 = 0
            android.content.Context r0 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x0040 }
            java.lang.String r2 = "connectivity"
            java.lang.Object r0 = r0.getSystemService(r2)     // Catch:{ Throwable -> 0x0040 }
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ Throwable -> 0x0040 }
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()     // Catch:{ Throwable -> 0x0040 }
            if (r0 == 0) goto L_0x0060
            boolean r2 = r0.isAvailable()     // Catch:{ Throwable -> 0x0040 }
            if (r2 == 0) goto L_0x0060
            int r2 = r0.getType()     // Catch:{ Throwable -> 0x0040 }
            r3 = 1
            if (r2 != r3) goto L_0x0068
            com.tendcloud.tenddata.fs r0 = com.tendcloud.tenddata.fs.a()     // Catch:{ Throwable -> 0x0040 }
            java.io.File[] r2 = r0.c()     // Catch:{ Throwable -> 0x0040 }
            if (r2 == 0) goto L_0x0047
            int r3 = r2.length     // Catch:{ Throwable -> 0x0040 }
            r0 = r1
        L_0x002a:
            if (r0 >= r3) goto L_0x0047
            r1 = r2[r0]     // Catch:{ Throwable -> 0x0040 }
            byte[] r4 = a(r1)     // Catch:{ Throwable -> 0x003b, all -> 0x0042 }
            r5.a(r4)     // Catch:{ Throwable -> 0x003b, all -> 0x0042 }
            r1.delete()     // Catch:{ Throwable -> 0x0040 }
        L_0x0038:
            int r0 = r0 + 1
            goto L_0x002a
        L_0x003b:
            r4 = move-exception
            r1.delete()     // Catch:{ Throwable -> 0x0040 }
            goto L_0x0038
        L_0x0040:
            r0 = move-exception
        L_0x0041:
            return
        L_0x0042:
            r0 = move-exception
            r1.delete()     // Catch:{ Throwable -> 0x0040 }
            throw r0     // Catch:{ Throwable -> 0x0040 }
        L_0x0047:
            com.tendcloud.tenddata.fs r0 = com.tendcloud.tenddata.fs.a()     // Catch:{ Throwable -> 0x0040 }
            byte[] r0 = r0.b()     // Catch:{ Throwable -> 0x0040 }
            if (r0 == 0) goto L_0x0041
            int r0 = r5.a(r0)     // Catch:{ Throwable -> 0x0040 }
            r1 = 200(0xc8, float:2.8E-43)
            if (r0 != r1) goto L_0x0060
            com.tendcloud.tenddata.fs r0 = com.tendcloud.tenddata.fs.a()     // Catch:{ Throwable -> 0x0040 }
            r0.d()     // Catch:{ Throwable -> 0x0040 }
        L_0x0060:
            r0 = 0
            r2 = 600000(0x927c0, double:2.964394E-318)
            r5.a(r0, r2)     // Catch:{ Throwable -> 0x0040 }
            goto L_0x0041
        L_0x0068:
            int r1 = r0.getType()     // Catch:{ Throwable -> 0x0040 }
            r2 = 9
            if (r1 == r2) goto L_0x0060
            int r0 = r0.getType()     // Catch:{ Throwable -> 0x0040 }
            if (r0 != 0) goto L_0x0060
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.fq.k():void");
    }

    /* access modifiers changed from: package-private */
    public void b() {
        try {
            c();
            if (!this.m) {
                a(5, 1800000);
            } else {
                e();
            }
        } catch (Throwable th) {
        }
    }
}
