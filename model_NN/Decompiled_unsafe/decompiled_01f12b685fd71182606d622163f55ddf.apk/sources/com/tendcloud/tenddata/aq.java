package com.tendcloud.tenddata;

import com.tendcloud.tenddata.as;
import java.net.Socket;
import java.nio.channels.ByteChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;

public class aq implements as.a {
    protected SSLContext a;
    protected ExecutorService b;

    public aq(SSLContext sSLContext) {
        this(sSLContext, Executors.newSingleThreadScheduledExecutor());
    }

    public aq(SSLContext sSLContext, ExecutorService executorService) {
        if (sSLContext == null || executorService == null) {
            throw new IllegalArgumentException();
        }
        this.a = sSLContext;
        this.b = executorService;
    }

    public ByteChannel a(SocketChannel socketChannel, SelectionKey selectionKey) {
        SSLEngine createSSLEngine = this.a.createSSLEngine();
        createSSLEngine.setUseClientMode(false);
        return new b(socketChannel, createSSLEngine, this.b, selectionKey);
    }

    /* renamed from: b */
    public g a(e eVar, l lVar, Socket socket) {
        return new g(eVar, lVar);
    }

    /* renamed from: b */
    public g a(e eVar, List list, Socket socket) {
        return new g(eVar, list);
    }
}
