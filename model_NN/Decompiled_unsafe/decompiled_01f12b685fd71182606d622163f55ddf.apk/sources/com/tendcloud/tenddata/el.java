package com.tendcloud.tenddata;

import java.util.List;
import java.util.Map;

abstract class el {
    el() {
    }

    /* access modifiers changed from: package-private */
    public abstract long a(long j);

    /* access modifiers changed from: package-private */
    public abstract long a(long j, long j2);

    /* access modifiers changed from: package-private */
    public abstract long a(long j, String str);

    /* access modifiers changed from: package-private */
    public abstract long a(String str);

    /* access modifiers changed from: package-private */
    public abstract long a(String str, int i);

    /* access modifiers changed from: package-private */
    public abstract long a(String str, long j, long j2, int i);

    /* access modifiers changed from: package-private */
    public abstract long a(String str, String str2, long j, int i, String str3, long j2);

    /* access modifiers changed from: package-private */
    public abstract long a(String str, String str2, String str3, long j, Map map);

    /* access modifiers changed from: package-private */
    public abstract long a(List list);

    /* access modifiers changed from: package-private */
    public abstract List a(String str, long j);

    /* access modifiers changed from: package-private */
    public abstract void a();

    /* access modifiers changed from: package-private */
    public abstract long b(long j);

    /* access modifiers changed from: package-private */
    public abstract long b(String str);

    /* access modifiers changed from: package-private */
    public abstract long b(List list);

    /* access modifiers changed from: package-private */
    public abstract List b(String str, long j);

    /* access modifiers changed from: package-private */
    public abstract void b();

    /* access modifiers changed from: package-private */
    public abstract long c(long j);

    /* access modifiers changed from: package-private */
    public abstract long c(String str);

    /* access modifiers changed from: package-private */
    public abstract void c();

    /* access modifiers changed from: package-private */
    public abstract long d();

    /* access modifiers changed from: package-private */
    public abstract long d(String str);

    /* access modifiers changed from: package-private */
    public abstract List d(long j);

    /* access modifiers changed from: package-private */
    public abstract long e(String str);

    /* access modifiers changed from: package-private */
    public abstract List e();
}
