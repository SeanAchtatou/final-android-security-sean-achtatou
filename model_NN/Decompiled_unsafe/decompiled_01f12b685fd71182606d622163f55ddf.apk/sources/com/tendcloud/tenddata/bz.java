package com.tendcloud.tenddata;

import android.content.Context;
import android.location.Location;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class bz {
    public static List a(Context context) {
        ArrayList arrayList = new ArrayList();
        if (ca.b && ca.a(23) && context.checkSelfPermission("android.permission.ACCESS_COARSE_LOCATION") != 0) {
        }
        return arrayList;
    }

    public static String b(Context context) {
        List<Location> a = a(context);
        StringBuffer stringBuffer = new StringBuffer();
        for (Location location : a) {
            stringBuffer.append(location.getLatitude()).append(',').append(location.getLongitude()).append(',').append(location.hasAltitude() ? Double.valueOf(location.getAltitude()) : "").append(',').append(location.getTime()).append(',').append(location.hasAccuracy() ? Float.valueOf(location.getAccuracy()) : "").append(',').append(location.hasBearing() ? Float.valueOf(location.getBearing()) : "").append(',').append(location.hasSpeed() ? Float.valueOf(location.getSpeed()) : "").append(',').append(location.getProvider()).append(':');
        }
        return stringBuffer.toString();
    }

    public static JSONArray c(Context context) {
        List<Location> a = a(context);
        JSONArray jSONArray = new JSONArray();
        for (Location location : a) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("lat", location.getLatitude());
                jSONObject.put("lng", location.getLongitude());
                jSONObject.put("ts", location.getTime());
                if (ca.a(17)) {
                    jSONObject.put("elapsed", location.getElapsedRealtimeNanos());
                }
                if (location.hasAltitude()) {
                    jSONObject.put("altitude", location.getAltitude());
                }
                if (location.hasAccuracy()) {
                    jSONObject.put("accurate", (double) location.getAccuracy());
                }
                if (location.hasBearing()) {
                    jSONObject.put("bearing", (double) location.getBearing());
                }
                if (location.hasSpeed()) {
                    jSONObject.put("speed", (double) location.getSpeed());
                }
                jSONObject.put("provider", location.getProvider());
                jSONArray.put(jSONObject);
            } catch (Throwable th) {
            }
        }
        return jSONArray;
    }

    public static JSONArray d(Context context) {
        return null;
    }

    public static Long[][] e(Context context) {
        return new Long[3][];
    }
}
