package com.tendcloud.tenddata;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tendcloud.tenddata.ej;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

class ec implements ej.f {
    private static final int d = 128;
    private static final int e = 1000;
    /* access modifiers changed from: private */
    public final Handler a;
    private final Runnable b = new a();
    /* access modifiers changed from: private */
    public final Map c = new HashMap();

    final class a implements Runnable {
        private a() {
        }

        public void run() {
            long currentTimeMillis = System.currentTimeMillis();
            synchronized (ec.this.c) {
                Iterator it = ec.this.c.entrySet().iterator();
                while (it.hasNext()) {
                    c cVar = (c) ((Map.Entry) it.next()).getValue();
                    if (currentTimeMillis - cVar.a > 1000) {
                        TCAgent.onEvent(ab.mContext, cVar.b, "", cVar.c);
                        it.remove();
                    }
                }
                if (!ec.this.c.isEmpty()) {
                    ec.this.a.postDelayed(this, 500);
                }
            }
        }
    }

    static class b {
        private final int a;

        public b(View view, String str) {
            this.a = view.hashCode() ^ str.hashCode();
        }

        public boolean equals(Object obj) {
            return (obj instanceof b) && this.a == obj.hashCode();
        }

        public int hashCode() {
            return this.a;
        }
    }

    static class c {
        public final long a;
        public final String b;
        public final Hashtable c;

        public c(String str, Hashtable hashtable, long j) {
            this.b = str;
            this.c = hashtable;
            this.a = j;
        }
    }

    ec(Handler handler) {
        this.a = handler;
    }

    private static String a(View view) {
        if (view instanceof TextView) {
            CharSequence text = ((TextView) view).getText();
            if (text != null) {
                return text.toString();
            }
            return null;
        } else if (!(view instanceof ViewGroup)) {
            return null;
        } else {
            StringBuilder sb = new StringBuilder();
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            boolean z = false;
            for (int i = 0; i < childCount && sb.length() < 128; i++) {
                String a2 = a(viewGroup.getChildAt(i));
                if (a2 != null && a2.length() > 0) {
                    if (z) {
                        sb.append(",");
                    }
                    sb.append(a2);
                    z = true;
                }
            }
            if (sb.length() > 128) {
                return sb.substring(0, 128);
            }
            if (z) {
                return sb.toString();
            }
            return null;
        }
    }

    public void a(View view, String str, boolean z) {
        long currentTimeMillis = System.currentTimeMillis();
        Hashtable hashtable = new Hashtable();
        String a2 = a(view);
        if (a2 == null) {
            a2 = "";
        }
        hashtable.put("text", a2);
        hashtable.put("dynamic_event", true);
        hashtable.put(dc.V, str);
        hashtable.put("time", Long.valueOf(currentTimeMillis));
        if (z) {
            b bVar = new b(view, str);
            c cVar = new c(str, hashtable, currentTimeMillis);
            synchronized (this.c) {
                boolean isEmpty = this.c.isEmpty();
                this.c.put(bVar, cVar);
                if (isEmpty) {
                    this.a.postDelayed(this.b, 1000);
                }
            }
            return;
        }
        Message obtainMessage = this.a.obtainMessage(13);
        obtainMessage.obj = hashtable;
        this.a.sendMessage(obtainMessage);
    }
}
