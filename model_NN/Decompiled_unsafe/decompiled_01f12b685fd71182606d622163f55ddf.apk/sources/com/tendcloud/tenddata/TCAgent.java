package com.tendcloud.tenddata;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import java.util.Map;

public final class TCAgent {
    public static boolean ENABLE_MULTI_PROCESS_POST = false;
    public static boolean LOG_ON = true;
    private static ao a;

    private static synchronized void a(Context context) {
        synchronized (TCAgent.class) {
            ab.mContext = context.getApplicationContext();
            if (a == null) {
                System.currentTimeMillis();
                a = zz.f();
                try {
                    a = (ao) new ag().a(context, "analytics", ab.mPublishPrefix, ao.class, zz.class, "com.tendcloud.tenddata.zz");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return;
    }

    public static String getAppId(Context context) {
        return ab.getAppId(context);
    }

    public static synchronized String getDeviceId(Context context) {
        String str;
        synchronized (TCAgent.class) {
            try {
                a(context);
                str = a.b(context);
            } catch (Throwable th) {
                th.printStackTrace();
                str = null;
            }
        }
        return str;
    }

    public static String getPartnerId(Context context) {
        return ab.getPartnerId(context);
    }

    public static boolean getSDKInitialized() {
        return gd.b;
    }

    public static synchronized void init(Context context) {
        synchronized (TCAgent.class) {
            try {
                a(context);
                a.a(context);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return;
    }

    public static synchronized void init(Context context, String str, String str2) {
        synchronized (TCAgent.class) {
            try {
                dq.b = "app";
                dq.setDebugMode(LOG_ON);
                a(context);
                a.a(context, str, str2);
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return;
    }

    public static void onError(Context context, Throwable th) {
        try {
            a(context);
            a.a(context, th);
        } catch (Throwable th2) {
            th2.printStackTrace();
        }
    }

    public static void onEvent(Context context, String str) {
        onEvent(context, str, "");
    }

    public static void onEvent(Context context, String str, String str2) {
        onEvent(context, str, str2, null);
    }

    public static void onEvent(Context context, String str, String str2, Map map) {
        try {
            a(context);
            a.a(context, str, str2, map);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void onPageEnd(Context context, String str) {
        try {
            a(context);
            a.onPageEnd(context, str);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void onPageStart(Context context, String str) {
        try {
            a(context);
            a.onPageStart(context, str);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    @Deprecated
    public static void onPause(Activity activity) {
        try {
            a(activity);
            a.b(activity);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    @Deprecated
    public static void onResume(Activity activity) {
        try {
            a(activity);
            a.a(activity);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    @Deprecated
    public static void onResume(Activity activity, String str, String str2) {
        try {
            a(activity);
            a.onResume(activity, str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void removeGlobalKV(String str) {
        if (LOG_ON && str != null) {
            Log.i("TDLog", "removeGlobalKV# key:" + str);
        }
        ab.a.remove(str);
    }

    public static void setAdditionalVersionNameAndCode(String str, long j) {
        try {
            gd.a(str, j);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void setAgentOption(int i) {
        if (LOG_ON) {
            Log.i("TDLog", "AgentOption.AO_Current = " + i);
        }
        AgentOption.a = i;
        if (i == 3) {
            ez.f().h();
            if (LOG_ON) {
                Log.i("TDLog", "Cleared local cache.");
            }
        }
    }

    public static void setGlobalKV(String str, Object obj) {
        if (!(!LOG_ON || str == null || obj == null)) {
            Log.i("TDLog", "setGlobalKV# key:" + str + " value:" + obj.toString());
        }
        ab.a.put(str, obj);
    }

    public static void setLocation(Context context, double d, double d2, String str) {
        try {
            a(context);
            a.setLocation(d, d2, str);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void setPushDisabled() {
        dd.a = true;
    }

    public static void setReportUncaughtExceptions(boolean z) {
        try {
            ab.b = z;
            if (LOG_ON) {
                Log.i("TDLog", "[PreSettings] setReportUncaughtExceptions: " + z);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
