package com.tendcloud.tenddata;

import android.content.Context;
import android.content.SharedPreferences;

final class dj {
    static bq a = null;
    static String b = "TD_database_app_push";
    static String c = "utf-8";
    static byte[] d = null;
    static final String e = "TD_app_pref_file";
    static final String f = "pref.accountid.key";
    static SharedPreferences g = null;
    private static final String h = "LAST_SENT_PUSH_TIME";
    private static final String i = "appcontext_push";

    static {
        try {
            a = new bq(ab.mContext, b);
            g = ab.mContext.getSharedPreferences(ab.SETTINGS_PERF_FILE, 0);
            String c2 = ca.c(ab.mContext.getPackageName());
            if (ab.mContext == null || c2 == null) {
                d = dd.class.getSimpleName().getBytes();
            } else {
                d = c2.getBytes();
            }
        } catch (Throwable th) {
        }
    }

    dj() {
    }

    protected static String a(Context context) {
        return context.getSharedPreferences(e, 0).getString(f, "-1");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static synchronized java.util.List a() {
        /*
            java.lang.Class<com.tendcloud.tenddata.dj> r2 = com.tendcloud.tenddata.dj.class
            monitor-enter(r2)
            r0 = 0
            com.tendcloud.tenddata.bq r1 = com.tendcloud.tenddata.dj.a     // Catch:{ Throwable -> 0x003d, all -> 0x003a }
            r3 = 100
            java.util.List r3 = r1.a(r3)     // Catch:{ Throwable -> 0x003d, all -> 0x003a }
            int r1 = r3.size()     // Catch:{ Throwable -> 0x003d, all -> 0x003a }
            if (r1 <= 0) goto L_0x0038
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ Throwable -> 0x003d, all -> 0x003a }
            r1.<init>()     // Catch:{ Throwable -> 0x003d, all -> 0x003a }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ Throwable -> 0x0036, all -> 0x003a }
        L_0x001b:
            boolean r0 = r3.hasNext()     // Catch:{ Throwable -> 0x0036, all -> 0x003a }
            if (r0 == 0) goto L_0x003f
            java.lang.Object r0 = r3.next()     // Catch:{ Throwable -> 0x0036, all -> 0x003a }
            byte[] r0 = (byte[]) r0     // Catch:{ Throwable -> 0x0036, all -> 0x003a }
            byte[] r4 = com.tendcloud.tenddata.dj.d     // Catch:{ Throwable -> 0x0036, all -> 0x003a }
            byte[] r0 = com.tendcloud.tenddata.ca.c(r0, r4)     // Catch:{ Throwable -> 0x0036, all -> 0x003a }
            java.lang.String r4 = new java.lang.String     // Catch:{ Throwable -> 0x0036, all -> 0x003a }
            r4.<init>(r0)     // Catch:{ Throwable -> 0x0036, all -> 0x003a }
            r1.add(r4)     // Catch:{ Throwable -> 0x0036, all -> 0x003a }
            goto L_0x001b
        L_0x0036:
            r0 = move-exception
            r0 = r1
        L_0x0038:
            monitor-exit(r2)
            return r0
        L_0x003a:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x003d:
            r1 = move-exception
            goto L_0x0038
        L_0x003f:
            r0 = r1
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.dj.a():java.util.List");
    }

    static void a(long j) {
        SharedPreferences.Editor edit = g.edit();
        edit.putLong(h, j);
        edit.commit();
    }

    static synchronized boolean a(String str) {
        boolean z = false;
        synchronized (dj.class) {
            if (str != null) {
                if (str.length() > 0) {
                    try {
                        a.write(ca.b(str.getBytes(), d));
                        a.b();
                        z = true;
                    } catch (Throwable th) {
                    }
                }
            }
        }
        return z;
    }

    static void b() {
        try {
            a.a();
        } catch (Throwable th) {
        }
    }

    static void b(String str) {
        SharedPreferences.Editor edit = g.edit();
        edit.putString(i, str);
        edit.commit();
    }

    static void c() {
    }

    static Long d() {
        return Long.valueOf(g.getLong(h, System.currentTimeMillis()));
    }

    static String e() {
        return g.getString(i, "");
    }
}
