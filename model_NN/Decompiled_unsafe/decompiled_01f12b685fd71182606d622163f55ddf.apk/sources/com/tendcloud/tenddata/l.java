package com.tendcloud.tenddata;

import com.sg.GameSprites.GameSpriteType;
import com.tendcloud.tenddata.ad;
import com.tendcloud.tenddata.d;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public abstract class l {
    public static int a = 1000;
    public static int b = 64;
    public static final byte[] c = aw.a("<policy-file-request/>\u0000");
    protected d.b d = null;
    protected ad.a e = null;

    public enum a {
        NONE,
        ONEWAY,
        TWOWAY
    }

    public enum b {
        MATCHED,
        NOT_MATCHED
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public static ai a(ByteBuffer byteBuffer, d.b bVar) {
        ap apVar;
        String b2 = b(byteBuffer);
        if (b2 == null) {
            throw new q(byteBuffer.capacity() + 128);
        }
        String[] split = b2.split(" ", 3);
        if (split.length != 3) {
            throw new t();
        }
        if (bVar == d.b.CLIENT) {
            ap akVar = new ak();
            ap apVar2 = akVar;
            apVar2.setHttpStatus(Short.parseShort(split[1]));
            apVar2.setHttpStatusMessage(split[2]);
            apVar = akVar;
        } else {
            aj ajVar = new aj();
            ajVar.setResourceDescriptor(split[1]);
            apVar = ajVar;
        }
        String b3 = b(byteBuffer);
        while (b3 != null && b3.length() > 0) {
            String[] split2 = b3.split(":", 2);
            if (split2.length != 2) {
                throw new t("not an http header");
            }
            apVar.a(split2[0], split2[1].replaceFirst("^ +", ""));
            b3 = b(byteBuffer);
        }
        if (b3 != null) {
            return apVar;
        }
        throw new q();
    }

    public static ByteBuffer a(ByteBuffer byteBuffer) {
        ByteBuffer allocate = ByteBuffer.allocate(byteBuffer.remaining());
        byte b2 = GameSpriteType.f1TYPE_BOSS_;
        while (byteBuffer.hasRemaining()) {
            byte b3 = byteBuffer.get();
            allocate.put(b3);
            if (b2 == 13 && b3 == 10) {
                allocate.limit(allocate.position() - 2);
                allocate.position(0);
                return allocate;
            }
            b2 = b3;
        }
        byteBuffer.position(byteBuffer.position() - allocate.position());
        return null;
    }

    public static String b(ByteBuffer byteBuffer) {
        ByteBuffer a2 = a(byteBuffer);
        if (a2 == null) {
            return null;
        }
        return aw.a(a2.array(), 0, a2.limit());
    }

    public int a(int i) {
        if (i >= 0) {
            return i;
        }
        throw new r((int) y.c, "Negative count");
    }

    public abstract ah a(ah ahVar);

    public abstract ai a(af afVar, ap apVar);

    public abstract b a(af afVar);

    public abstract b a(af afVar, an anVar);

    public abstract ByteBuffer a(ad adVar);

    public List a(ad.a aVar, ByteBuffer byteBuffer, boolean z) {
        if (aVar == ad.a.BINARY || aVar == ad.a.TEXT || aVar == ad.a.TEXT) {
            if (this.e != null) {
                this.e = ad.a.CONTINUOUS;
            } else {
                this.e = aVar;
            }
            ae aeVar = new ae(this.e);
            try {
                aeVar.setPayload(byteBuffer);
                aeVar.setFin(z);
                if (z) {
                    this.e = null;
                } else {
                    this.e = aVar;
                }
                return Collections.singletonList(aeVar);
            } catch (r e2) {
                throw new RuntimeException(e2);
            }
        } else {
            throw new IllegalArgumentException("Only Opcode.BINARY or  Opcode.TEXT are allowed");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.l.a(com.tendcloud.tenddata.al, com.tendcloud.tenddata.d$b, boolean):java.util.List
     arg types: [com.tendcloud.tenddata.al, com.tendcloud.tenddata.d$b, int]
     candidates:
      com.tendcloud.tenddata.l.a(com.tendcloud.tenddata.ad$a, java.nio.ByteBuffer, boolean):java.util.List
      com.tendcloud.tenddata.l.a(com.tendcloud.tenddata.al, com.tendcloud.tenddata.d$b, boolean):java.util.List */
    public List a(al alVar, d.b bVar) {
        return a(alVar, bVar, true);
    }

    public List a(al alVar, d.b bVar, boolean z) {
        StringBuilder sb = new StringBuilder(100);
        if (alVar instanceof af) {
            sb.append("GET ");
            sb.append(((af) alVar).a());
            sb.append(" HTTP/1.1");
        } else if (alVar instanceof an) {
            sb.append("HTTP/1.1 101 " + ((an) alVar).a());
        } else {
            throw new RuntimeException("unknow role");
        }
        sb.append("\r\n");
        Iterator c2 = alVar.c();
        while (c2.hasNext()) {
            String str = (String) c2.next();
            String a2 = alVar.a(str);
            sb.append(str);
            sb.append(": ");
            sb.append(a2);
            sb.append("\r\n");
        }
        sb.append("\r\n");
        byte[] b2 = aw.b(sb.toString());
        byte[] d2 = z ? alVar.d() : null;
        ByteBuffer allocate = ByteBuffer.allocate((d2 == null ? 0 : d2.length) + b2.length);
        allocate.put(b2);
        if (d2 != null) {
            allocate.put(d2);
        }
        allocate.flip();
        return Collections.singletonList(allocate);
    }

    public abstract List a(String str, boolean z);

    public abstract List a(ByteBuffer byteBuffer, boolean z);

    public abstract void a();

    /* access modifiers changed from: protected */
    public boolean a(al alVar) {
        return alVar.a("Upgrade").equalsIgnoreCase("websocket") && alVar.a("Connection").toLowerCase(Locale.ENGLISH).contains("upgrade");
    }

    public abstract a b();

    public abstract l c();

    public abstract List c(ByteBuffer byteBuffer);

    public al d(ByteBuffer byteBuffer) {
        return a(byteBuffer, this.d);
    }

    public d.b d() {
        return this.d;
    }

    public void setParseMode(d.b bVar) {
        this.d = bVar;
    }
}
