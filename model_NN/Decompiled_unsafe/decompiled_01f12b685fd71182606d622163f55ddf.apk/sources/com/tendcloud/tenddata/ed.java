package com.tendcloud.tenddata;

import com.tendcloud.tenddata.ad;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;

class ed {
    private static final int d = 30000;
    /* access modifiers changed from: private */
    public static final ByteBuffer e = ByteBuffer.allocate(0);
    /* access modifiers changed from: private */
    public final a a;
    /* access modifiers changed from: private */
    public final b b;
    private final URI c;

    interface a {
        void a();

        void b();

        void bindEvents(JSONObject jSONObject);

        void clearEdits(JSONObject jSONObject);

        void performEdit(JSONObject jSONObject);

        void sendSnapshot(JSONObject jSONObject);

        void setTweaks(JSONObject jSONObject);
    }

    class b extends k {
        b(URI uri, int i, Socket socket) {
            super(uri, new n(), null, i);
            setSocket(socket);
        }

        public void b(int i, String str, boolean z) {
            ed.this.a.b();
        }

        public void onError(Exception exc) {
            if (exc == null || exc.getMessage() != null) {
            }
        }

        public void onMessage(String str) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                String string = jSONObject.getString("type");
                if (string.equals("device_info_request")) {
                    ed.this.a.a();
                } else if (string.equals("snapshot_request")) {
                    ed.this.a.sendSnapshot(jSONObject);
                } else if (string.equals("change_request")) {
                    ed.this.a.performEdit(jSONObject);
                } else if (string.equals("dynamic_event_request")) {
                    ed.this.a.bindEvents(jSONObject);
                } else if (string.equals("clear_request")) {
                    ed.this.a.clearEdits(jSONObject);
                } else if (string.equals("tweak_request")) {
                    ed.this.a.setTweaks(jSONObject);
                }
            } catch (JSONException e2) {
            }
        }

        public void onOpen(an anVar) {
            ab.a(true);
        }
    }

    class c extends OutputStream {
        private c() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tendcloud.tenddata.k.a(com.tendcloud.tenddata.ad$a, java.nio.ByteBuffer, boolean):void
         arg types: [com.tendcloud.tenddata.ad$a, java.nio.ByteBuffer, int]
         candidates:
          com.tendcloud.tenddata.k.a(int, java.lang.String, boolean):void
          com.tendcloud.tenddata.k.a(com.tendcloud.tenddata.d, int, java.lang.String):void
          com.tendcloud.tenddata.e.a(com.tendcloud.tenddata.d, com.tendcloud.tenddata.l, com.tendcloud.tenddata.af):com.tendcloud.tenddata.ap
          com.tendcloud.tenddata.e.a(com.tendcloud.tenddata.d, com.tendcloud.tenddata.af, com.tendcloud.tenddata.an):void
          com.tendcloud.tenddata.h.a(com.tendcloud.tenddata.d, com.tendcloud.tenddata.l, com.tendcloud.tenddata.af):com.tendcloud.tenddata.ap
          com.tendcloud.tenddata.h.a(com.tendcloud.tenddata.d, int, java.lang.String):void
          com.tendcloud.tenddata.h.a(com.tendcloud.tenddata.d, com.tendcloud.tenddata.af, com.tendcloud.tenddata.an):void
          com.tendcloud.tenddata.k.a(com.tendcloud.tenddata.ad$a, java.nio.ByteBuffer, boolean):void */
        public void close() {
            try {
                ed.this.b.a(ad.a.TEXT, ed.e, true);
            } catch (x e) {
                e.printStackTrace();
            } catch (w e2) {
                e2.printStackTrace();
            }
        }

        public void write(int i) {
            write(new byte[]{(byte) i}, 0, 1);
        }

        public void write(byte[] bArr) {
            write(bArr, 0, bArr.length);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tendcloud.tenddata.k.a(com.tendcloud.tenddata.ad$a, java.nio.ByteBuffer, boolean):void
         arg types: [com.tendcloud.tenddata.ad$a, java.nio.ByteBuffer, int]
         candidates:
          com.tendcloud.tenddata.k.a(int, java.lang.String, boolean):void
          com.tendcloud.tenddata.k.a(com.tendcloud.tenddata.d, int, java.lang.String):void
          com.tendcloud.tenddata.e.a(com.tendcloud.tenddata.d, com.tendcloud.tenddata.l, com.tendcloud.tenddata.af):com.tendcloud.tenddata.ap
          com.tendcloud.tenddata.e.a(com.tendcloud.tenddata.d, com.tendcloud.tenddata.af, com.tendcloud.tenddata.an):void
          com.tendcloud.tenddata.h.a(com.tendcloud.tenddata.d, com.tendcloud.tenddata.l, com.tendcloud.tenddata.af):com.tendcloud.tenddata.ap
          com.tendcloud.tenddata.h.a(com.tendcloud.tenddata.d, int, java.lang.String):void
          com.tendcloud.tenddata.h.a(com.tendcloud.tenddata.d, com.tendcloud.tenddata.af, com.tendcloud.tenddata.an):void
          com.tendcloud.tenddata.k.a(com.tendcloud.tenddata.ad$a, java.nio.ByteBuffer, boolean):void */
        public void write(byte[] bArr, int i, int i2) {
            try {
                ed.this.b.a(ad.a.TEXT, ByteBuffer.wrap(bArr, i, i2), false);
            } catch (x e) {
                e.printStackTrace();
            } catch (w e2) {
                e2.printStackTrace();
            }
        }
    }

    ed(URI uri, a aVar, Socket socket) {
        this.a = aVar;
        this.c = uri;
        try {
            this.b = new b(uri, 30000, socket);
            this.b.o();
        } catch (InterruptedException e2) {
            throw new Exception(e2);
        }
    }

    private static String a(ByteBuffer byteBuffer) {
        try {
            CharBuffer decode = Charset.forName("UTF-8").newDecoder().decode(byteBuffer);
            byteBuffer.flip();
            return decode.toString();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return !this.b.i() && !this.b.g() && !this.b.h();
    }

    /* access modifiers changed from: package-private */
    public BufferedOutputStream b() {
        return new BufferedOutputStream(new c());
    }
}
