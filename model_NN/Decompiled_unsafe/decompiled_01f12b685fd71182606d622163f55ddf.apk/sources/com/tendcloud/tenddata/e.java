package com.tendcloud.tenddata;

import com.tendcloud.tenddata.ad;
import java.net.InetSocketAddress;

public abstract class e implements h {
    public ap a(d dVar, l lVar, af afVar) {
        return new ak();
    }

    public String a(d dVar) {
        InetSocketAddress d = dVar.d();
        if (d == null) {
            throw new t("socket not bound");
        }
        StringBuffer stringBuffer = new StringBuffer(90);
        stringBuffer.append("<cross-domain-policy><allow-access-from domain=\"*\" to-ports=\"");
        stringBuffer.append(d.getPort());
        stringBuffer.append("\" /></cross-domain-policy>\u0000");
        return stringBuffer.toString();
    }

    public void a(d dVar, ad adVar) {
    }

    public void a(d dVar, af afVar) {
    }

    public void a(d dVar, af afVar, an anVar) {
    }

    public void b(d dVar, ad adVar) {
        ae aeVar = new ae(adVar);
        aeVar.setOptcode(ad.a.PONG);
        dVar.sendFrame(aeVar);
    }

    public void c(d dVar, ad adVar) {
    }
}
