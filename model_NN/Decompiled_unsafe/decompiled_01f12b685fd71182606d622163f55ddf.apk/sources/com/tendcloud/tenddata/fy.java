package com.tendcloud.tenddata;

import android.os.Handler;
import android.os.HandlerThread;

final class fy {
    static final int a = 9999;
    static final int b = 101;
    static final int c = 102;
    private static Handler d;
    private static final HandlerThread e = new HandlerThread("ProcessingThread");

    static {
        d = null;
        e.start();
        d = new fz(e.getLooper());
    }

    fy() {
    }

    static final Handler a() {
        return d;
    }
}
