package com.tendcloud.tenddata;

import android.os.Handler;
import android.os.HandlerThread;

final class er {
    static final int a = 9999;
    static final int b = 101;
    static final int c = 102;
    static final int d = 103;
    private static Handler e;
    private static final HandlerThread f = new HandlerThread("ProcessingThread");

    static {
        e = null;
        f.start();
        e = new es(f.getLooper());
    }

    er() {
    }

    static final Handler a() {
        return e;
    }
}
