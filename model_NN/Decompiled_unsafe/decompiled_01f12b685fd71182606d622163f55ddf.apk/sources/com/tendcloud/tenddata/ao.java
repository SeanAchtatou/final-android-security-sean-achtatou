package com.tendcloud.tenddata;

import android.app.Activity;
import android.content.Context;
import java.util.Map;

interface ao extends u {
    void a(Activity activity);

    void a(Context context);

    void a(Context context, String str, String str2);

    void a(Context context, String str, String str2, Map map);

    void a(Context context, Throwable th);

    String b(Context context);

    void b(Activity activity);

    void c(boolean z);

    void onPageEnd(Context context, String str);

    void onPageStart(Context context, String str);

    void onResume(Activity activity, String str, String str2);

    void setLocation(double d, double d2, String str);
}
