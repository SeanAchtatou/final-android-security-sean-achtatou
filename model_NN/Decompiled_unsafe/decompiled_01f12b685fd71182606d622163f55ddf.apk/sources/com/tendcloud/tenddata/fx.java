package com.tendcloud.tenddata;

import android.content.Context;
import android.content.pm.Signature;
import java.io.ByteArrayInputStream;
import java.security.cert.CertificateFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class fx {
    static final int a = 1;
    static final int b = 0;
    static final int c = 0;
    static final String d = "Android";
    static JSONObject e = new JSONObject();
    static JSONObject f = new JSONObject();
    static JSONObject g = new JSONObject();
    static JSONObject h = null;
    static String i = null;
    static String j = null;

    fx() {
    }

    static String a(String str) {
        try {
            if (h == null) {
                h = new JSONObject();
                h.put("device", d());
                h.put("app", e());
                h.put("sdk", f());
                h.put(dc.Y, new JSONObject());
            }
            return h.toString();
        } catch (JSONException e2) {
            return null;
        }
    }

    static void a() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("tid", bm.a(TalkingDataSMS.b));
            try {
                JSONArray y = br.y(TalkingDataSMS.b);
                JSONArray jSONArray = new JSONArray();
                if (y != null) {
                    jSONArray.put(y.getJSONObject(0).get("imei"));
                    if (y.length() == 2) {
                        jSONArray.put(y.getJSONObject(1).get("imei"));
                    }
                }
                jSONObject.put("imeis", jSONArray);
            } catch (Exception e2) {
            }
            JSONArray jSONArray2 = new JSONArray();
            jSONArray2.put(bm.f(TalkingDataSMS.b));
            jSONObject.put("wifiMacs", jSONArray2);
            jSONObject.put("androidId", bm.b(TalkingDataSMS.b));
            jSONObject.put("adId", bm.g(TalkingDataSMS.b));
            jSONObject.put("serialNo", bm.a() == null ? "" : bm.a());
            f.put("deviceId", jSONObject);
        } catch (Throwable th) {
        }
    }

    static byte[] a(Context context, String str) {
        try {
            Signature[] signatureArr = context.getPackageManager().getPackageInfo(str, 64).signatures;
            CertificateFactory instance = CertificateFactory.getInstance("X.509");
            byte[] byteArray = signatureArr[0].toByteArray();
            instance.generateCertificate(new ByteArrayInputStream(byteArray));
            return byteArray;
        } catch (Throwable th) {
            return null;
        }
    }

    static void b() {
        try {
            e.put("name", bj.a().h(TalkingDataSMS.b));
            e.put("globalId", bj.a().a(TalkingDataSMS.b));
            e.put("versionName", bj.a().c(TalkingDataSMS.b));
            e.put("versionCode", bj.a().b(TalkingDataSMS.b));
            e.put("installTime", bj.a().d(TalkingDataSMS.b));
            e.put("updateTime", bj.a().e(TalkingDataSMS.b));
            e.put("cert", ca.a(a(TalkingDataSMS.b, TalkingDataSMS.b.getPackageName())));
        } catch (Throwable th) {
        }
    }

    static void c() {
        try {
            g.put("version", 1);
            g.put("minorVersion", 0);
            g.put("build", 0);
            g.put("platform", d);
        } catch (Throwable th) {
        }
    }

    private static JSONObject d() {
        return f;
    }

    private static JSONObject e() {
        return e;
    }

    private static JSONObject f() {
        return g;
    }
}
