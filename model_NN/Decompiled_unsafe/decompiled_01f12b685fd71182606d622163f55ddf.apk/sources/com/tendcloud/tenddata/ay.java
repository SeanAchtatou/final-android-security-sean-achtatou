package com.tendcloud.tenddata;

import com.sg.pak.PAK_ASSETS;
import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ReadOnlyBufferException;

public final class ay {
    public static final int a = 4;
    public static final int b = 8;
    private static final int c = 3;
    private final ByteBuffer d;

    public static class a extends IOException {
        private static final long a = -6947486886997889499L;

        a(int i, int i2) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space (pos " + i + " limit " + i2 + ").");
        }
    }

    private ay(ByteBuffer byteBuffer) {
        this.d = byteBuffer;
        this.d.order(ByteOrder.LITTLE_ENDIAN);
    }

    private ay(byte[] bArr, int i, int i2) {
        this(ByteBuffer.wrap(bArr, i, i2));
    }

    public static int a(double d2) {
        return 8;
    }

    public static int a(float f) {
        return 4;
    }

    public static int a(int i) {
        if (i >= 0) {
            return h(i);
        }
        return 10;
    }

    public static int a(int i, double d2) {
        return g(i) + a(d2);
    }

    public static int a(int i, float f) {
        return g(i) + a(f);
    }

    public static int a(int i, bf bfVar) {
        return (g(i) * 2) + a(bfVar);
    }

    public static int a(int i, boolean z) {
        return g(i) + a(z);
    }

    public static int a(long j) {
        return f(j);
    }

    public static int a(bf bfVar) {
        return bfVar.e();
    }

    private static int a(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        while (i < length && charSequence.charAt(i) < 128) {
            i++;
        }
        int i2 = i;
        int i3 = length;
        while (true) {
            if (i2 < length) {
                char charAt = charSequence.charAt(i2);
                if (charAt >= 2048) {
                    i3 += a(charSequence, i2);
                    break;
                }
                i2++;
                i3 = ((127 - charAt) >>> 31) + i3;
            } else {
                break;
            }
        }
        if (i3 >= length) {
            return i3;
        }
        throw new IllegalArgumentException("UTF-8 length does not fit in int: " + (((long) i3) + 4294967296L));
    }

    private static int a(CharSequence charSequence, int i) {
        int length = charSequence.length();
        int i2 = 0;
        int i3 = i;
        while (i3 < length) {
            char charAt = charSequence.charAt(i3);
            if (charAt < 2048) {
                i2 += (127 - charAt) >>> 31;
            } else {
                i2 += 2;
                if (55296 <= charAt && charAt <= 57343) {
                    if (Character.codePointAt(charSequence, i3) < 65536) {
                        throw new IllegalArgumentException("Unpaired surrogate at index " + i3);
                    }
                    i3++;
                }
            }
            i3++;
        }
        return i2;
    }

    private static int a(CharSequence charSequence, byte[] bArr, int i, int i2) {
        int i3;
        int length = charSequence.length();
        int i4 = 0;
        int i5 = i + i2;
        while (i4 < length && i4 + i < i5) {
            char charAt = charSequence.charAt(i4);
            if (charAt >= 128) {
                break;
            }
            bArr[i + i4] = (byte) charAt;
            i4++;
        }
        if (i4 == length) {
            return i + length;
        }
        int i6 = i + i4;
        while (i4 < length) {
            char charAt2 = charSequence.charAt(i4);
            if (charAt2 < 128 && i6 < i5) {
                i3 = i6 + 1;
                bArr[i6] = (byte) charAt2;
            } else if (charAt2 < 2048 && i6 <= i5 - 2) {
                int i7 = i6 + 1;
                bArr[i6] = (byte) ((charAt2 >>> 6) | 960);
                i3 = i7 + 1;
                bArr[i7] = (byte) ((charAt2 & '?') | 128);
            } else if ((charAt2 < 55296 || 57343 < charAt2) && i6 <= i5 - 3) {
                int i8 = i6 + 1;
                bArr[i6] = (byte) ((charAt2 >>> 12) | PAK_ASSETS.IMG_CHUANGGUAN001);
                int i9 = i8 + 1;
                bArr[i8] = (byte) (((charAt2 >>> 6) & 63) | 128);
                i3 = i9 + 1;
                bArr[i9] = (byte) ((charAt2 & '?') | 128);
            } else if (i6 <= i5 - 4) {
                if (i4 + 1 != charSequence.length()) {
                    i4++;
                    char charAt3 = charSequence.charAt(i4);
                    if (Character.isSurrogatePair(charAt2, charAt3)) {
                        int codePoint = Character.toCodePoint(charAt2, charAt3);
                        int i10 = i6 + 1;
                        bArr[i6] = (byte) ((codePoint >>> 18) | PAK_ASSETS.IMG_SHUOMINGZI03);
                        int i11 = i10 + 1;
                        bArr[i10] = (byte) (((codePoint >>> 12) & 63) | 128);
                        int i12 = i11 + 1;
                        bArr[i11] = (byte) (((codePoint >>> 6) & 63) | 128);
                        i3 = i12 + 1;
                        bArr[i12] = (byte) ((codePoint & 63) | 128);
                    }
                }
                throw new IllegalArgumentException("Unpaired surrogate at index " + (i4 - 1));
            } else if (55296 > charAt2 || charAt2 > 57343 || (i4 + 1 != charSequence.length() && Character.isSurrogatePair(charAt2, charSequence.charAt(i4 + 1)))) {
                throw new ArrayIndexOutOfBoundsException("Failed writing " + charAt2 + " at index " + i6);
            } else {
                throw new IllegalArgumentException("Unpaired surrogate at index " + i4);
            }
            i4++;
            i6 = i3;
        }
        return i6;
    }

    public static int a(String str) {
        int a2 = a((CharSequence) str);
        return a2 + h(a2);
    }

    public static int a(boolean z) {
        return 1;
    }

    public static ay a(byte[] bArr) {
        return a(bArr, 0, bArr.length);
    }

    public static ay a(byte[] bArr, int i, int i2) {
        return new ay(bArr, i, i2);
    }

    private static void a(CharSequence charSequence, ByteBuffer byteBuffer) {
        if (byteBuffer.isReadOnly()) {
            throw new ReadOnlyBufferException();
        } else if (byteBuffer.hasArray()) {
            try {
                byteBuffer.position(a(charSequence, byteBuffer.array(), byteBuffer.arrayOffset() + byteBuffer.position(), byteBuffer.remaining()) - byteBuffer.arrayOffset());
            } catch (ArrayIndexOutOfBoundsException e) {
                BufferOverflowException bufferOverflowException = new BufferOverflowException();
                bufferOverflowException.initCause(e);
                throw bufferOverflowException;
            }
        } else {
            b(charSequence, byteBuffer);
        }
    }

    public static int b(int i) {
        return 4;
    }

    public static int b(int i, int i2) {
        return g(i) + a(i2);
    }

    public static int b(int i, long j) {
        return g(i) + a(j);
    }

    public static int b(int i, bf bfVar) {
        return g(i) + b(bfVar);
    }

    public static int b(int i, String str) {
        return g(i) + a(str);
    }

    public static int b(int i, byte[] bArr) {
        return g(i) + b(bArr);
    }

    public static int b(long j) {
        return f(j);
    }

    public static int b(bf bfVar) {
        int e = bfVar.e();
        return e + h(e);
    }

    public static int b(byte[] bArr) {
        return h(bArr.length) + bArr.length;
    }

    private static void b(CharSequence charSequence, ByteBuffer byteBuffer) {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            char charAt = charSequence.charAt(i);
            if (charAt < 128) {
                byteBuffer.put((byte) charAt);
            } else if (charAt < 2048) {
                byteBuffer.put((byte) ((charAt >>> 6) | 960));
                byteBuffer.put((byte) ((charAt & '?') | 128));
            } else if (charAt < 55296 || 57343 < charAt) {
                byteBuffer.put((byte) ((charAt >>> 12) | PAK_ASSETS.IMG_CHUANGGUAN001));
                byteBuffer.put((byte) (((charAt >>> 6) & 63) | 128));
                byteBuffer.put((byte) ((charAt & '?') | 128));
            } else {
                if (i + 1 != charSequence.length()) {
                    i++;
                    char charAt2 = charSequence.charAt(i);
                    if (Character.isSurrogatePair(charAt, charAt2)) {
                        int codePoint = Character.toCodePoint(charAt, charAt2);
                        byteBuffer.put((byte) ((codePoint >>> 18) | PAK_ASSETS.IMG_SHUOMINGZI03));
                        byteBuffer.put((byte) (((codePoint >>> 12) & 63) | 128));
                        byteBuffer.put((byte) (((codePoint >>> 6) & 63) | 128));
                        byteBuffer.put((byte) ((codePoint & 63) | 128));
                    }
                }
                throw new IllegalArgumentException("Unpaired surrogate at index " + (i - 1));
            }
            i++;
        }
    }

    public static int c(int i) {
        return h(i);
    }

    public static int c(int i, int i2) {
        return g(i) + b(i2);
    }

    public static int c(int i, long j) {
        return g(i) + b(j);
    }

    public static int c(long j) {
        return 8;
    }

    public static int d(int i) {
        return h(i);
    }

    public static int d(int i, int i2) {
        return g(i) + c(i2);
    }

    public static int d(int i, long j) {
        return g(i) + c(j);
    }

    public static int d(long j) {
        return 8;
    }

    public static int e(int i) {
        return 4;
    }

    public static int e(int i, int i2) {
        return g(i) + d(i2);
    }

    public static int e(int i, long j) {
        return g(i) + d(j);
    }

    public static int e(long j) {
        return f(g(j));
    }

    public static int f(int i) {
        return h(i(i));
    }

    public static int f(int i, int i2) {
        return g(i) + e(i2);
    }

    public static int f(int i, long j) {
        return g(i) + e(j);
    }

    public static int f(long j) {
        if ((-128 & j) == 0) {
            return 1;
        }
        if ((-16384 & j) == 0) {
            return 2;
        }
        if ((-2097152 & j) == 0) {
            return 3;
        }
        if ((-268435456 & j) == 0) {
            return 4;
        }
        if ((-34359738368L & j) == 0) {
            return 5;
        }
        if ((-4398046511104L & j) == 0) {
            return 6;
        }
        if ((-562949953421312L & j) == 0) {
            return 7;
        }
        if ((-72057594037927936L & j) == 0) {
            return 8;
        }
        return (Long.MIN_VALUE & j) == 0 ? 9 : 10;
    }

    public static int g(int i) {
        return h(bi.a(i, 0));
    }

    public static int g(int i, int i2) {
        return g(i) + f(i2);
    }

    public static long g(long j) {
        return (j << 1) ^ (j >> 63);
    }

    public static int h(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (-268435456 & i) == 0 ? 4 : 5;
    }

    public static int i(int i) {
        return (i << 1) ^ (i >> 31);
    }

    public int a() {
        return this.d.remaining();
    }

    public void a(int i, int i2) {
        h(i, 0);
        writeInt32NoTag(i2);
    }

    public void a(int i, long j) {
        h(i, 0);
        writeInt64NoTag(j);
    }

    public void a(int i, String str) {
        h(i, 2);
        writeStringNoTag(str);
    }

    public void a(int i, byte[] bArr) {
        h(i, 2);
        writeBytesNoTag(bArr);
    }

    public void b() {
        if (a() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    public void b(byte[] bArr, int i, int i2) {
        if (this.d.remaining() >= i2) {
            this.d.put(bArr, i, i2);
            return;
        }
        throw new a(this.d.position(), this.d.limit());
    }

    public void h(int i, int i2) {
        writeRawVarint32(bi.a(i, i2));
    }

    public void writeBoolNoTag(boolean z) {
        writeRawByte(z ? 1 : 0);
    }

    public void writeBytesNoTag(byte[] bArr) {
        writeRawVarint32(bArr.length);
        writeRawBytes(bArr);
    }

    public void writeDoubleNoTag(double d2) {
        writeRawLittleEndian64(Double.doubleToLongBits(d2));
    }

    public void writeEnumNoTag(int i) {
        writeRawVarint32(i);
    }

    public void writeFixed32NoTag(int i) {
        writeRawLittleEndian32(i);
    }

    public void writeFixed64NoTag(long j) {
        writeRawLittleEndian64(j);
    }

    public void writeFloatNoTag(float f) {
        writeRawLittleEndian32(Float.floatToIntBits(f));
    }

    public void writeGroupNoTag(bf bfVar) {
        bfVar.writeTo(this);
    }

    public void writeInt32NoTag(int i) {
        if (i >= 0) {
            writeRawVarint32(i);
        } else {
            writeRawVarint64((long) i);
        }
    }

    public void writeInt64NoTag(long j) {
        writeRawVarint64(j);
    }

    public void writeMessageNoTag(bf bfVar) {
        writeRawVarint32(bfVar.d());
        bfVar.writeTo(this);
    }

    public void writeRawByte(byte b2) {
        if (!this.d.hasRemaining()) {
            throw new a(this.d.position(), this.d.limit());
        }
        this.d.put(b2);
    }

    public void writeRawByte(int i) {
        writeRawByte((byte) i);
    }

    public void writeRawBytes(byte[] bArr) {
        b(bArr, 0, bArr.length);
    }

    public void writeRawLittleEndian32(int i) {
        if (this.d.remaining() < 4) {
            throw new a(this.d.position(), this.d.limit());
        }
        this.d.putInt(i);
    }

    public void writeRawLittleEndian64(long j) {
        if (this.d.remaining() < 8) {
            throw new a(this.d.position(), this.d.limit());
        }
        this.d.putLong(j);
    }

    public void writeRawVarint32(int i) {
        while ((i & -128) != 0) {
            writeRawByte((i & 127) | 128);
            i >>>= 7;
        }
        writeRawByte(i);
    }

    public void writeRawVarint64(long j) {
        while ((-128 & j) != 0) {
            writeRawByte((((int) j) & 127) | 128);
            j >>>= 7;
        }
        writeRawByte((int) j);
    }

    public void writeSFixed32NoTag(int i) {
        writeRawLittleEndian32(i);
    }

    public void writeSFixed64NoTag(long j) {
        writeRawLittleEndian64(j);
    }

    public void writeSInt32NoTag(int i) {
        writeRawVarint32(i(i));
    }

    public void writeSInt64NoTag(long j) {
        writeRawVarint64(g(j));
    }

    public void writeStringNoTag(String str) {
        try {
            int h = h(str.length());
            if (h == h(str.length() * 3)) {
                int position = this.d.position();
                if (this.d.remaining() < h) {
                    throw new a(h + position, this.d.limit());
                }
                this.d.position(position + h);
                a(str, this.d);
                int position2 = this.d.position();
                this.d.position(position);
                writeRawVarint32((position2 - position) - h);
                this.d.position(position2);
                return;
            }
            writeRawVarint32(a((CharSequence) str));
            a(str, this.d);
        } catch (BufferOverflowException e) {
            a aVar = new a(this.d.position(), this.d.limit());
            aVar.initCause(e);
            throw aVar;
        }
    }

    public void writeUInt32NoTag(int i) {
        writeRawVarint32(i);
    }

    public void writeUInt64NoTag(long j) {
        writeRawVarint64(j);
    }
}
