package com.tendcloud.tenddata;

import android.content.Context;
import android.content.Intent;
import android.net.LocalServerSocket;
import android.os.Handler;
import com.tendcloud.tenddata.ct;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

class du {
    private static final String a = "PushLog";
    private static du b = null;
    /* access modifiers changed from: private */
    public Context c;
    private LocalServerSocket d;
    /* access modifiers changed from: private */
    public cx e;
    /* access modifiers changed from: private */
    public BlockingQueue f;
    private Handler g = new Handler();
    /* access modifiers changed from: private */
    public boolean h;

    class a implements Runnable {
        private a() {
        }

        /* synthetic */ a(du duVar, dv dvVar) {
            this();
        }

        public void run() {
            cs.a("PushLog", "start read");
            while (du.this.h) {
                if (br.c(du.this.c)) {
                    try {
                        ct.a a2 = ct.a(du.this.c);
                        if (a2.a()) {
                            du.this.e.connect(a2);
                            while (du.this.h) {
                                du.this.e.b();
                            }
                        } else {
                            cs.e("PushLog", "loc failed");
                        }
                    } catch (Throwable th) {
                        bu.a(du.this.c, dc.i, dc.r, (long) (((int) bu.b(du.this.c, dc.i, dc.r, 0)) + 1));
                        cs.e("PushLog", "read err :" + th.getMessage());
                    }
                } else {
                    cs.e("PushLog", "Network is not connected");
                }
                if (du.this.h) {
                    try {
                        Thread.sleep(100000);
                    } catch (Throwable th2) {
                        cs.e("PushLog", "thread sleep error ");
                    }
                }
            }
            du.this.e.c();
        }
    }

    private du(Context context) {
        this.c = context;
        this.e = cx.a(cv.a(context), context, new dv(this, context));
        this.f = new LinkedBlockingQueue(1);
        new Thread(new dw(this)).start();
    }

    static synchronized du a(Context context) {
        du duVar;
        synchronized (du.class) {
            if (b == null) {
                b = new du(context);
            }
            duVar = b;
        }
        return duVar;
    }

    public synchronized boolean a() {
        boolean z;
        int i;
        String str = null;
        synchronized (this) {
            List<String> e2 = cv.e(this.c);
            int a2 = cz.a(this.c, "");
            for (String str2 : e2) {
                int a3 = cz.a(this.c, str2);
                if (a3 != 0) {
                    if (a3 > a2) {
                        i = a3;
                    } else {
                        str2 = str;
                        i = a2;
                    }
                    a2 = i;
                    str = str2;
                }
            }
            if (!(this.d == null || str == null)) {
                try {
                    this.d.close();
                    this.d = null;
                    this.h = false;
                } catch (Throwable th) {
                    cs.e("PushLog", "release global lock err: " + th.toString());
                }
            }
            if (str != null) {
                z = true;
                cv.d(this.c, str);
            } else {
                z = false;
            }
            this.g.postDelayed(new dx(this, e2, str), 200);
        }
        return z;
    }

    public synchronized void b() {
        try {
            if (this.d != null) {
                this.d.close();
                this.d = null;
            }
            this.e.c();
            this.h = false;
        } catch (Throwable th) {
            cs.a("PushLog", "stop global lock err: ", th);
        }
        return;
    }

    public synchronized void initPushSDK(Intent intent) {
        if (intent != null) {
            String stringExtra = intent.getStringExtra(dc.y);
            if (stringExtra != null && stringExtra.equals(dc.A)) {
                this.f.offer(stringExtra);
                cv.a(this.c, cx.a);
            } else if (stringExtra != null) {
                if (stringExtra.equals(dc.z)) {
                    cv.a(this.c, cx.a);
                }
            }
        }
        try {
            this.d = new LocalServerSocket(cv.c(this.c));
            this.h = true;
            Thread thread = new Thread(new a(this, null));
            thread.setName("t-read");
            thread.start();
        } catch (Throwable th) {
        }
    }
}
