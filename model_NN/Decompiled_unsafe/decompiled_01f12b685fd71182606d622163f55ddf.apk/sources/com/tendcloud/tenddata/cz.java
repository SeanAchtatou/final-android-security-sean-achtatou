package com.tendcloud.tenddata;

import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

class cz {
    private static final String a = "PushLog";

    cz() {
    }

    public static int a(Context context, String str) {
        String b = b(context, str, dc.g);
        if (!cp.a(b)) {
            return Integer.parseInt(b);
        }
        return 0;
    }

    public static void a(Context context, String str, String str2) {
        try {
            FileOutputStream openFileOutput = context.openFileOutput(str, 3);
            openFileOutput.write(str2.getBytes());
            openFileOutput.close();
        } catch (Throwable th) {
        }
    }

    public static String b(Context context, String str, String str2) {
        String str3;
        try {
            if (!cp.a(str)) {
                context = context.createPackageContext(str, 2);
            }
            File fileStreamPath = context.getFileStreamPath(str2);
            if (fileStreamPath == null || !fileStreamPath.exists()) {
                return null;
            }
            try {
                FileInputStream openFileInput = context.openFileInput(str2);
                byte[] bArr = new byte[openFileInput.available()];
                openFileInput.read(bArr);
                openFileInput.close();
                str3 = new String(bArr);
            } catch (Throwable th) {
                str3 = null;
            }
            return str3;
        } catch (Throwable th2) {
            str3 = null;
        }
    }

    public static void b(Context context, String str) {
        a(context, dc.h, str);
    }

    public static void putCodeContent(Context context) {
        a(context, dc.g, "26");
    }
}
