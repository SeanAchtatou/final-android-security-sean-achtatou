package com.tendcloud.tenddata;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;

final class gp {
    private static volatile gp a = null;
    private final ConcurrentMap b = new ConcurrentHashMap();
    private final ThreadLocal c = new gq(this);
    private final ThreadLocal d = new gr(this);
    private final Map e = new HashMap();

    static class a {
        final Object a;
        final gt b;

        public a(Object obj, gt gtVar) {
            this.a = obj;
            this.b = gtVar;
        }
    }

    private gp() {
    }

    public static gp a() {
        if (a == null) {
            synchronized (gp.class) {
                if (a == null) {
                    a = new gp();
                }
            }
        }
        return a;
    }

    private static void a(String str, InvocationTargetException invocationTargetException) {
        Throwable cause = invocationTargetException.getCause();
        if (cause != null) {
            throw new RuntimeException(str + ": " + cause.getMessage(), cause);
        }
        throw new RuntimeException(str + ": " + invocationTargetException.getMessage(), invocationTargetException);
    }

    private Set c(Class cls) {
        LinkedList linkedList = new LinkedList();
        HashSet hashSet = new HashSet();
        linkedList.add(cls);
        while (!linkedList.isEmpty()) {
            Class cls2 = (Class) linkedList.remove(0);
            hashSet.add(cls2);
            Class superclass = cls2.getSuperclass();
            if (superclass != null) {
                linkedList.add(superclass);
            }
        }
        return hashSet;
    }

    /* access modifiers changed from: package-private */
    public Set a(Class cls) {
        return (Set) this.b.get(cls);
    }

    /* access modifiers changed from: protected */
    public void a(Object obj, gt gtVar) {
        ((ConcurrentLinkedQueue) this.c.get()).offer(new a(obj, gtVar));
    }

    /* access modifiers changed from: package-private */
    public Set b(Class cls) {
        Set set = (Set) this.e.get(cls);
        if (set != null) {
            return set;
        }
        Set c2 = c(cls);
        this.e.put(cls, c2);
        return c2;
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (!((Boolean) this.d.get()).booleanValue()) {
            this.d.set(true);
            while (true) {
                try {
                    a aVar = (a) ((ConcurrentLinkedQueue) this.c.get()).poll();
                    if (aVar != null) {
                        if (aVar.b.a()) {
                            b(aVar.a, aVar.b);
                        }
                    } else {
                        return;
                    }
                } finally {
                    this.d.set(false);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(Object obj, gt gtVar) {
        try {
            gtVar.handleEvent(obj);
        } catch (InvocationTargetException e2) {
            a("Could not dispatch event: " + obj.getClass() + " to handler " + gtVar, e2);
        }
    }

    public void post(Object obj) {
        boolean z;
        if (obj == null) {
            throw new NullPointerException("Event to post must not be null.");
        }
        boolean z2 = false;
        Iterator it = b(obj.getClass()).iterator();
        while (true) {
            z = z2;
            if (!it.hasNext()) {
                break;
            }
            Set<gt> a2 = a((Class) it.next());
            if (a2 != null && !a2.isEmpty()) {
                z = true;
                for (gt a3 : a2) {
                    a(obj, a3);
                }
            }
            z2 = z;
        }
        if (!z && !(obj instanceof gu)) {
            post(new gu(this, obj));
        }
        b();
    }

    public void register(Object obj) {
        if (obj != null) {
            Map a2 = gs.a(obj);
            for (Class cls : a2.keySet()) {
                Set set = (Set) this.b.get(cls);
                if (set == null) {
                    CopyOnWriteArraySet copyOnWriteArraySet = new CopyOnWriteArraySet();
                    set = (Set) this.b.putIfAbsent(cls, copyOnWriteArraySet);
                    if (set == null) {
                        set = copyOnWriteArraySet;
                    }
                }
                if (!set.addAll((Set) a2.get(cls))) {
                    throw new IllegalArgumentException("Object already registered.");
                }
            }
        }
    }

    public void unregister(Object obj) {
        if (obj == null) {
            throw new NullPointerException("Object to unregister must not be null.");
        }
        for (Map.Entry entry : gs.a(obj).entrySet()) {
            Set<gt> a2 = a((Class) entry.getKey());
            Collection collection = (Collection) entry.getValue();
            if (a2 == null || !a2.containsAll(collection)) {
                throw new IllegalArgumentException("Missing event handler for a method. Is " + obj.getClass() + " registered?");
            }
            for (gt gtVar : a2) {
                if (collection.contains(gtVar)) {
                    gtVar.b();
                }
            }
            a2.removeAll(collection);
        }
    }
}
