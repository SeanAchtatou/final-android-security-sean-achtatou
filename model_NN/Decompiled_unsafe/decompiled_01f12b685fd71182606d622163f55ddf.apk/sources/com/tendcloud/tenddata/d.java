package com.tendcloud.tenddata;

import com.tendcloud.tenddata.ad;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

public interface d {
    public static final int a = 80;
    public static final int b = 443;

    public enum a {
        NOT_YET_CONNECTED,
        CONNECTING,
        OPEN,
        CLOSING,
        CLOSED
    }

    public enum b {
        CLIENT,
        SERVER
    }

    void a();

    void a(int i, String str);

    void a(ad.a aVar, ByteBuffer byteBuffer, boolean z);

    void b(int i, String str);

    boolean b();

    InetSocketAddress c();

    void close(int i);

    InetSocketAddress d();

    boolean e();

    boolean f();

    boolean g();

    boolean h();

    boolean i();

    l j();

    a k();

    String l();

    void send(String str);

    void send(ByteBuffer byteBuffer);

    void send(byte[] bArr);

    void sendFrame(ad adVar);
}
