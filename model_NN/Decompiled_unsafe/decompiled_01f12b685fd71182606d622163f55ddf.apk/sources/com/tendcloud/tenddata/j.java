package com.tendcloud.tenddata;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;

public abstract class j extends a {
    protected final ByteBuffer a;

    public j(ByteChannel byteChannel) {
        super(byteChannel);
        try {
            this.a = ByteBuffer.wrap(e().getBytes("ASCII"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public abstract String e();

    public int write(ByteBuffer byteBuffer) {
        return !this.a.hasRemaining() ? super.write(byteBuffer) : super.write(this.a);
    }
}
