package com.tendcloud.tenddata;

import com.sg.pak.PAK_ASSETS;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class cf extends OutputStream {
    static final int a = 8192;
    static final /* synthetic */ boolean b = (!cf.class.desiredAssertionStatus());
    private OutputStream c;
    private final DataOutputStream d;
    private final cd e;
    private final cl f;
    private final cg g;
    private final int h;
    private boolean i = true;
    private boolean j = true;
    private boolean k = true;
    private int l = 0;
    private boolean m = false;
    private IOException n = null;
    private final byte[] o = new byte[1];

    public cf(OutputStream outputStream, byte[] bArr) {
        if (outputStream == null) {
            throw new NullPointerException();
        }
        this.c = outputStream;
        this.d = new DataOutputStream(outputStream);
        this.f = new cl(8192);
        int length = bArr != null ? bArr.length + 4096 : 4096;
        this.g = cg.a(this.f, 1, 0, 0, length, a(length), (Runtime.getRuntime().availableProcessors() * 4) + 24, (int) (16.0d + (((double) Runtime.getRuntime().availableProcessors()) * 2.5d)));
        this.e = this.g.b();
        if (bArr != null && bArr.length > 0) {
            this.e.a(length, bArr);
            this.i = false;
        }
        this.h = 1;
    }

    private static int a(int i2) {
        if (8192 > i2) {
            return 8192 - i2;
        }
        return 0;
    }

    private void a(int i2, int i3) {
        this.d.writeByte((this.k ? this.i ? PAK_ASSETS.IMG_LENGDONG_SHUOMING : 192 : this.j ? 160 : 128) | ((i2 - 1) >>> 16));
        this.d.writeShort(i2 - 1);
        this.d.writeShort(i3 - 1);
        if (this.k) {
            this.d.writeByte(this.h);
        }
        this.f.write(this.c);
        this.k = false;
        this.j = false;
        this.i = false;
    }

    private void b() {
        int c2 = this.f.c();
        int d2 = this.g.d();
        if (!b && c2 <= 0) {
            throw new AssertionError(c2);
        } else if (b || d2 > 0) {
            if (c2 + 2 < d2) {
                a(d2, c2);
            } else {
                this.g.c();
                d2 = this.g.d();
                if (b || d2 > 0) {
                    b(d2);
                } else {
                    throw new AssertionError(d2);
                }
            }
            this.l -= d2;
            this.g.e();
            this.f.a();
        } else {
            throw new AssertionError(d2);
        }
    }

    private void b(int i2) {
        while (i2 > 0) {
            int min = Math.min(i2, 8192);
            this.d.writeByte(this.i ? 1 : 2);
            this.d.writeShort(min - 1);
            this.e.a(this.c, i2, min);
            i2 -= min;
            this.i = false;
        }
        this.j = true;
    }

    private void c() {
        if (!b && this.m) {
            throw new AssertionError();
        } else if (this.n != null) {
            throw this.n;
        } else {
            this.e.d();
            while (this.l > 0) {
                try {
                    this.g.f();
                    b();
                } catch (IOException e2) {
                    this.n = e2;
                    throw e2;
                }
            }
            this.c.write(0);
            this.m = true;
        }
    }

    public void a() {
        if (!this.m) {
            c();
            this.m = true;
        }
    }

    public void close() {
        if (this.c != null) {
            if (!this.m) {
                try {
                    c();
                } catch (IOException e2) {
                }
            }
            try {
                this.c.close();
            } catch (IOException e3) {
                if (this.n == null) {
                    this.n = e3;
                }
            }
            this.c = null;
        }
        if (this.n != null) {
            throw this.n;
        }
    }

    public void flush() {
        if (this.n != null) {
            throw this.n;
        } else if (this.m) {
            throw new IOException("Stream finished or closed");
        } else {
            try {
                this.e.c();
                while (this.l > 0) {
                    this.g.f();
                    b();
                }
                this.c.flush();
            } catch (IOException e2) {
                this.n = e2;
                throw e2;
            }
        }
    }

    public void write(int i2) {
        this.o[0] = (byte) i2;
        write(this.o, 0, 1);
    }

    public void write(byte[] bArr, int i2, int i3) {
        if (i2 < 0 || i3 < 0 || i2 + i3 < 0 || i2 + i3 > bArr.length) {
            throw new IndexOutOfBoundsException();
        } else if (this.n != null) {
            throw this.n;
        } else if (this.m) {
            throw new IOException("Stream finished or closed");
        } else {
            while (i3 > 0) {
                try {
                    int a2 = this.e.a(bArr, i2, i3);
                    i2 += a2;
                    i3 -= a2;
                    this.l = a2 + this.l;
                    if (this.g.f()) {
                        b();
                    }
                } catch (IOException e2) {
                    this.n = e2;
                    throw e2;
                }
            }
        }
    }
}
