package com.tendcloud.tenddata;

import com.tendcloud.tenddata.et;
import java.util.Comparator;

class eu implements Comparator {
    final /* synthetic */ et a;

    eu(et etVar) {
        this.a = etVar;
    }

    /* renamed from: a */
    public int compare(et.d dVar, et.d dVar2) {
        if (dVar.c == dVar2.c) {
            return 0;
        }
        return dVar.c < dVar2.c ? 1 : -1;
    }
}
