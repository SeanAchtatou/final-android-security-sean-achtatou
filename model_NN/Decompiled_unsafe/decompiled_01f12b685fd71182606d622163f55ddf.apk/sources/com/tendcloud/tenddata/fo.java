package com.tendcloud.tenddata;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Process;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;

class fo {
    public static final String a = "events";
    public static final String b = "TalingDataConfig";
    public static final String c = "config.";
    private static volatile fo d = null;
    private boolean e;
    private eg f;
    private final int g;
    private a h;
    /* access modifiers changed from: private */
    public Handler i;

    static class a implements Runnable {
        private volatile boolean a = true;

        public void a() {
            if (fo.a().i != null) {
                this.a = false;
                fo.a().i.post(this);
            }
        }

        public void b() {
            if (fo.a().i != null) {
                this.a = true;
                fo.a().i.removeCallbacks(this);
            }
        }

        public void run() {
            if (fo.a().i != null) {
                if (!this.a) {
                    fo.a().i.sendMessage(fo.a().i.obtainMessage(1));
                }
                fo.a().i.postDelayed(this, 5000);
            }
        }
    }

    static class b implements Runnable {
        private b() {
        }

        public void run() {
            bw.a(ab.mContext).registerTestDeviceListener(new fp(this));
        }
    }

    static {
        try {
            gp.a().register(a());
        } catch (Throwable th) {
        }
    }

    private fo() {
        this.e = false;
        this.g = 5000;
        this.h = null;
        this.h = new a();
    }

    public static fo a() {
        if (d == null) {
            synchronized (fo.class) {
                if (d == null) {
                    d = new fo();
                }
            }
        }
        return d;
    }

    private static boolean a(Context context) {
        try {
            String str = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).processName;
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
            if (runningAppProcesses != null) {
                for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                    if (Process.myPid() == next.pid && next.processName.equals(str)) {
                        return true;
                    }
                }
            }
        } catch (Throwable th) {
        }
        return TCAgent.ENABLE_MULTI_PROCESS_POST;
    }

    private boolean e() {
        if (!Build.HARDWARE.equals("goldfish") && !Build.HARDWARE.equals("vbox86")) {
            return false;
        }
        if (!Build.BRAND.startsWith("generic") && !Build.BRAND.startsWith("Android")) {
            return false;
        }
        if (!Build.DEVICE.startsWith("generic") && !Build.DEVICE.startsWith("vbox86")) {
            return false;
        }
        if (Build.PRODUCT.contains("sdk") || Build.PRODUCT.contains("Genymotion") || Build.PRODUCT.contains("vbox86")) {
            return Build.MODEL.toLowerCase(Locale.US).contains("sdk") || Build.MODEL.toLowerCase(Locale.US).contains("api");
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a(Handler handler) {
        this.i = handler;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (!this.e) {
            try {
                if (Build.VERSION.SDK_INT >= 16 && gd.e != null && a(ab.mContext)) {
                    this.f = eg.a(ab.mContext, ab.m, gd.e);
                }
                new Thread(new b()).start();
                this.e = true;
            } catch (Throwable th) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (e()) {
            this.h.a();
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (e()) {
            this.h.b();
        }
    }

    public final void onTDEBEventCodelessEvent(ep epVar) {
        Object obj;
        try {
            Object obj2 = epVar.a.get("cloudSettingsType");
            if (obj2 != null && obj2.toString().equals("codeless") && (obj = epVar.a.get("data")) != null && (obj instanceof JSONArray)) {
                bu.a(ab.mContext, b + ab.getAppId(ab.mContext), "config.events", obj.toString());
                if (this.f != null) {
                    this.f.setEventBindings((JSONArray) obj);
                }
            }
        } catch (Throwable th) {
        }
    }
}
