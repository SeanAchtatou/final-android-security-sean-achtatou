package com.tendcloud.tenddata;

final class ce {
    static final int[] a = new int[256];
    private static final int b = -306674912;
    private static final int c = 1024;
    private static final int d = 1023;
    private static final int e = 65536;
    private static final int f = 65535;
    private final int g;
    private final int[] h = new int[1024];
    private final int[] i = new int[e];
    private final int[] j;
    private int k = 0;
    private int l = 0;
    private int m = 0;

    static {
        for (int i2 = 0; i2 < 256; i2++) {
            int i3 = i2;
            for (int i4 = 0; i4 < 8; i4++) {
                i3 = (i3 & 1) != 0 ? (i3 >>> 1) ^ b : i3 >>> 1;
            }
            a[i2] = i3;
        }
    }

    ce(int i2) {
        this.j = new int[a(i2)];
        this.g = this.j.length - 1;
    }

    static int a(int i2) {
        int i3 = i2 - 1;
        int i4 = i3 | (i3 >>> 1);
        int i5 = i4 | (i4 >>> 2);
        int i6 = i5 | (i5 >>> 4);
        int i7 = ((i6 | (i6 >>> 8)) >>> 1) | f;
        if (i7 > 16777216) {
            i7 >>>= 1;
        }
        return i7 + 1;
    }

    static int b(int i2) {
        return ((66560 + a(i2)) / 256) + 4;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.h[this.k];
    }

    /* access modifiers changed from: package-private */
    public void a(byte[] bArr, int i2) {
        byte b2 = a[bArr[i2] & 255] ^ (bArr[i2 + 1] & 255);
        this.k = b2 & 1023;
        byte b3 = b2 ^ ((bArr[i2 + 2] & 255) << 8);
        this.l = f & b3;
        this.m = (b3 ^ (a[bArr[i2 + 3] & 255] << 5)) & this.g;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.i[this.l];
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return this.j[this.m];
    }

    /* access modifiers changed from: package-private */
    public void c(int i2) {
        this.h[this.k] = i2;
        this.i[this.l] = i2;
        this.j[this.m] = i2;
    }

    /* access modifiers changed from: package-private */
    public void d(int i2) {
        cd.a(this.h, i2);
        cd.a(this.i, i2);
        cd.a(this.j, i2);
    }
}
