package com.tendcloud.tenddata;

import android.content.Context;

public class dq {
    public static final String a = "PushLog";
    public static String b = null;
    private static Context c;

    public static void setDebugMode(boolean z) {
        Cdo.a(c, z);
    }

    public static void startPushService(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("[push] start service error, context is required");
        }
        c = context.getApplicationContext();
        Cdo.a(c);
        dy.a();
    }
}
