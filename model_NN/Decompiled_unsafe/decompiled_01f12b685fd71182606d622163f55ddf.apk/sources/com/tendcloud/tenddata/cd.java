package com.tendcloud.tenddata;

import java.io.OutputStream;

final class cd {
    static final /* synthetic */ boolean e = (!cd.class.desiredAssertionStatus());
    final int a;
    final int b;
    final byte[] c;
    int d = -1;
    private final int f;
    private final int g;
    private int h = -1;
    private boolean i = false;
    private int j = 0;
    private int k = 0;
    private final ce l;
    private final int[] m;
    private final cj n;
    private final int o;
    private final int p;
    private int q = -1;
    private int r;

    cd(int i2, int i3, int i4, int i5, int i6, int i7) {
        this.c = new byte[a(i2, i3, i4, i6)];
        this.f = i3 + i2;
        this.g = i4 + i6;
        this.a = i6;
        this.b = i5;
        this.l = new ce(i2);
        this.p = i2 + 1;
        this.m = new int[this.p];
        this.r = this.p;
        this.n = new cj(i5 - 1);
        this.o = i7 <= 0 ? (i5 / 4) + 4 : i7;
    }

    private static int a(int i2, int i3, int i4, int i5) {
        return i3 + i2 + i4 + i5 + Math.min((i2 / 2) + 4096, 536870912);
    }

    static void a(int[] iArr, int i2) {
        for (int i3 = 0; i3 < iArr.length; i3++) {
            if (iArr[i3] <= i2) {
                iArr[i3] = 0;
            } else {
                iArr[i3] = iArr[i3] - i2;
            }
        }
    }

    private int g() {
        int c2 = c(4, 4);
        if (c2 != 0) {
            int i2 = this.r + 1;
            this.r = i2;
            if (i2 == Integer.MAX_VALUE) {
                int i3 = Integer.MAX_VALUE - this.p;
                this.l.d(i3);
                a(this.m, i3);
                this.r -= i3;
            }
            int i4 = this.q + 1;
            this.q = i4;
            if (i4 == this.p) {
                this.q = 0;
            }
        }
        return c2;
    }

    private void h() {
        int i2 = ((this.d + 1) - this.f) & -16;
        System.arraycopy(this.c, i2, this.c, 0, this.j - i2);
        this.d -= i2;
        this.h -= i2;
        this.j -= i2;
    }

    private void i() {
        if (this.k > 0 && this.d < this.h) {
            this.d -= this.k;
            int i2 = this.k;
            this.k = 0;
            skip(i2);
            if (!e && this.k >= i2) {
                throw new AssertionError();
            }
        }
    }

    public int a(int i2, int i3) {
        return this.c[(this.d + i2) - i3] & 255;
    }

    public int a(int i2, int i3, int i4) {
        int i5 = this.d + i2;
        int i6 = (i5 - i3) - 1;
        int i7 = 0;
        while (i7 < i4 && this.c[i5 + i7] == this.c[i6 + i7]) {
            i7++;
        }
        return i7;
    }

    public int a(byte[] bArr, int i2, int i3) {
        if (e || !this.i) {
            if (this.d >= this.c.length - this.g) {
                h();
            }
            if (i3 > this.c.length - this.j) {
                i3 = this.c.length - this.j;
            }
            System.arraycopy(bArr, i2, this.c, this.j, i3);
            this.j += i3;
            if (this.j >= this.g) {
                this.h = this.j - this.g;
            }
            i();
            return i3;
        }
        throw new AssertionError();
    }

    public cj a() {
        int i2;
        int i3;
        int i4;
        int i5 = 3;
        this.n.c = 0;
        int i6 = this.a;
        int i7 = this.b;
        int g2 = g();
        if (g2 >= i6) {
            g2 = i7;
            i2 = i6;
        } else if (g2 == 0) {
            return this.n;
        } else {
            if (i7 > g2) {
                i2 = g2;
            } else {
                int i8 = i7;
                i2 = g2;
                g2 = i8;
            }
        }
        this.l.a(this.c, this.d);
        int a2 = this.r - this.l.a();
        int b2 = this.r - this.l.b();
        int c2 = this.l.c();
        this.l.c(this.r);
        this.m[this.q] = c2;
        if (a2 >= this.p || this.c[this.d - a2] != this.c[this.d]) {
            i3 = 0;
        } else {
            this.n.a[0] = 2;
            this.n.b[0] = a2 - 1;
            this.n.c = 1;
            i3 = 2;
        }
        if (a2 == b2 || b2 >= this.p || this.c[this.d - b2] != this.c[this.d]) {
            b2 = a2;
        } else {
            int[] iArr = this.n.b;
            cj cjVar = this.n;
            int i9 = cjVar.c;
            cjVar.c = i9 + 1;
            iArr[i9] = b2 - 1;
            i3 = 3;
        }
        if (this.n.c > 0) {
            while (i3 < i2 && this.c[(this.d + i3) - b2] == this.c[this.d + i3]) {
                i3++;
            }
            this.n.a[this.n.c - 1] = i3;
            if (i3 >= g2) {
                return this.n;
            }
        }
        if (i3 >= 3) {
            i5 = i3;
        }
        int i10 = this.o;
        int i11 = i5;
        int i12 = c2;
        while (true) {
            int i13 = this.r - i12;
            int i14 = i10 - 1;
            if (i10 != 0 && i13 < this.p) {
                int i15 = this.m[(i13 > this.q ? this.p : 0) + (this.q - i13)];
                if (this.c[(this.d + i11) - i13] == this.c[this.d + i11] && this.c[this.d - i13] == this.c[this.d]) {
                    i4 = 0;
                    do {
                        i4++;
                        if (i4 >= i2) {
                            break;
                        }
                    } while (this.c[(this.d + i4) - i13] == this.c[this.d + i4]);
                    if (i4 > i11) {
                        this.n.a[this.n.c] = i4;
                        this.n.b[this.n.c] = i13 - 1;
                        this.n.c++;
                        if (i4 >= g2) {
                            return this.n;
                        }
                        i11 = i4;
                        i10 = i14;
                        i12 = i15;
                    }
                }
                i4 = i11;
                i11 = i4;
                i10 = i14;
                i12 = i15;
            }
        }
        return this.n;
    }

    public void a(int i2, byte[] bArr) {
        if (!e && b()) {
            throw new AssertionError();
        } else if (!e && this.j != 0) {
            throw new AssertionError();
        } else if (bArr != null) {
            int min = Math.min(bArr.length, i2);
            System.arraycopy(bArr, bArr.length - min, this.c, 0, min);
            this.j += min;
            skip(min);
        }
    }

    public void a(OutputStream outputStream, int i2, int i3) {
        outputStream.write(this.c, (this.d + 1) - i2, i3);
    }

    public boolean a(int i2) {
        return this.d - i2 < this.h;
    }

    public boolean a(cj cjVar) {
        int min = Math.min(e(), this.a);
        for (int i2 = 0; i2 < cjVar.c; i2++) {
            if (b(cjVar.b[i2], min) != cjVar.a[i2]) {
                return false;
            }
        }
        return true;
    }

    public int b(int i2) {
        return this.c[this.d - i2] & 255;
    }

    public int b(int i2, int i3) {
        int i4 = (this.d - i2) - 1;
        int i5 = 0;
        while (i5 < i3 && this.c[this.d + i5] == this.c[i4 + i5]) {
            i5++;
        }
        return i5;
    }

    public boolean b() {
        return this.d != -1;
    }

    /* access modifiers changed from: package-private */
    public int c(int i2, int i3) {
        if (e || i2 >= i3) {
            this.d++;
            int i4 = this.j - this.d;
            if (i4 >= i2) {
                return i4;
            }
            if (i4 >= i3 && this.i) {
                return i4;
            }
            this.k++;
            return 0;
        }
        throw new AssertionError();
    }

    public void c() {
        this.h = this.j - 1;
        i();
    }

    public void d() {
        this.h = this.j - 1;
        this.i = true;
        i();
    }

    public int e() {
        if (e || b()) {
            return this.j - this.d;
        }
        throw new AssertionError();
    }

    public int f() {
        return this.d;
    }

    public void skip(int i2) {
        if (e || i2 >= 0) {
            while (true) {
                int i3 = i2 - 1;
                if (i2 <= 0) {
                    return;
                }
                if (g() != 0) {
                    this.l.a(this.c, this.d);
                    this.m[this.q] = this.l.c();
                    this.l.c(this.r);
                    i2 = i3;
                } else {
                    i2 = i3;
                }
            }
        } else {
            throw new AssertionError();
        }
    }
}
