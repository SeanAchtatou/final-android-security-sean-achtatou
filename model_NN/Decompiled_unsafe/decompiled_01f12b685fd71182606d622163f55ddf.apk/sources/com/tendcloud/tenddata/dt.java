package com.tendcloud.tenddata;

import android.content.Context;
import android.content.Intent;

class dt implements Runnable {
    final /* synthetic */ Context a;
    final /* synthetic */ Intent b;
    final /* synthetic */ zy c;

    dt(zy zyVar, Context context, Intent intent) {
        this.c = zyVar;
        this.a = context;
        this.b = intent;
    }

    public void run() {
        try {
            du.a(this.a).initPushSDK(this.b);
        } catch (Throwable th) {
            cs.e(dq.a, "start ping err " + th.toString());
        }
    }
}
