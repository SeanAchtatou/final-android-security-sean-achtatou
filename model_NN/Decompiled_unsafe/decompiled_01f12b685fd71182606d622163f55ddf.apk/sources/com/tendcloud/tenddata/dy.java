package com.tendcloud.tenddata;

import android.content.Context;

class dy {
    dy() {
    }

    public static boolean a() {
        String str = "false";
        try {
            str = cr.d(dc.D);
        } catch (Throwable th) {
            cs.e("UtilInit", "client inited err" + th);
        }
        return "true".equals(str);
    }

    public static void commonInit(Context context) {
        cr.a(context);
        cz.b(context, context.getPackageName() + ":push");
    }
}
