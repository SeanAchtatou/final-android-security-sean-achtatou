package com.tendcloud.tenddata;

final class cm {
    static final int a = 12;
    private static final int b = 7;
    private static final int c = 0;
    private static final int d = 3;
    private static final int e = 7;
    private static final int f = 8;
    private static final int g = 9;
    private static final int h = 10;
    private static final int i = 11;
    private int j;

    cm() {
    }

    cm(cm cmVar) {
        this.j = cmVar.j;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.j = 0;
    }

    /* access modifiers changed from: package-private */
    public void a(cm cmVar) {
        this.j = cmVar.j;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.j <= 3) {
            this.j = 0;
        } else if (this.j <= 9) {
            this.j -= 3;
        } else {
            this.j -= 6;
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        int i2 = 7;
        if (this.j >= 7) {
            i2 = 10;
        }
        this.j = i2;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.j = this.j < 7 ? 8 : 11;
    }

    /* access modifiers changed from: package-private */
    public void f() {
        this.j = this.j < 7 ? 9 : 11;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return this.j < 7;
    }
}
