package com.tendcloud.tenddata;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

public interface h {
    ap a(d dVar, l lVar, af afVar);

    String a(d dVar);

    void a(d dVar, int i, String str);

    void a(d dVar, int i, String str, boolean z);

    void a(d dVar, ad adVar);

    void a(d dVar, af afVar);

    void a(d dVar, af afVar, an anVar);

    void a(d dVar, al alVar);

    void a(d dVar, Exception exc);

    void a(d dVar, String str);

    void a(d dVar, ByteBuffer byteBuffer);

    InetSocketAddress b(d dVar);

    void b(d dVar, int i, String str, boolean z);

    void b(d dVar, ad adVar);

    InetSocketAddress c(d dVar);

    void c(d dVar, ad adVar);

    void onWriteDemand(d dVar);
}
