package com.tendcloud.tenddata;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import java.util.zip.CRC32;

final class fb {
    private static volatile fb a = null;
    /* access modifiers changed from: private */
    public static ex e = null;
    private final CRC32 b = new CRC32();
    private Handler c = null;
    private final HandlerThread d = new HandlerThread("NetWorkThread");

    static {
        a().d.start();
        a().c = new fc(a().d.getLooper());
    }

    fb() {
    }

    public static fb a() {
        if (a == null) {
            synchronized (fb.class) {
                if (a == null) {
                    a = new fb();
                }
            }
        }
        return a;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(java.lang.String r7, java.lang.String r8, java.lang.String r9, java.lang.String r10, byte[] r11, java.lang.Object r12, int r13, boolean r14) {
        /*
            r6 = this;
            monitor-enter(r6)
            if (r14 == 0) goto L_0x006f
            r5 = r11
        L_0x0004:
            java.util.zip.CRC32 r0 = r6.b     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            r0.reset()     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.util.zip.CRC32 r0 = r6.b     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            r0.update(r5)     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            if (r13 != 0) goto L_0x0074
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            r0.<init>()     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.lang.String r1 = "?crc="
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.util.zip.CRC32 r1 = r6.b     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            long r2 = r1.getValue()     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.lang.String r1 = java.lang.Long.toHexString(r2)     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.lang.String r3 = r0.toString()     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
        L_0x0031:
            android.content.Context r0 = com.tendcloud.tenddata.ab.mContext     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            r1 = r9
            r2 = r8
            r4 = r10
            com.tendcloud.tenddata.bk$d r0 = com.tendcloud.tenddata.bk.a(r0, r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            long r2 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            com.tendcloud.tenddata.ab.g = r2     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            com.tendcloud.tenddata.ey r1 = new com.tendcloud.tenddata.ey     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            r1.<init>()     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            android.os.Bundle r2 = r1.a     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.lang.String r3 = "statusCode"
            int r4 = r0.a     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            r2.putInt(r3, r4)     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            android.os.Bundle r2 = r1.a     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.lang.String r3 = "responseMsg"
            java.lang.String r0 = r0.b     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            r2.putString(r3, r0)     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            android.os.Bundle r0 = r1.a     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.lang.String r2 = "action"
            com.tendcloud.tenddata.ex r3 = com.tendcloud.tenddata.fb.e     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.lang.String r3 = r3.d     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            r0.putString(r2, r3)     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            r1.b = r12     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            r1.c = r13     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            com.tendcloud.tenddata.gp r0 = com.tendcloud.tenddata.gp.a()     // Catch:{ Throwable -> 0x009b, all -> 0x0096 }
            r0.post(r1)     // Catch:{ Throwable -> 0x009b, all -> 0x0096 }
        L_0x006d:
            monitor-exit(r6)
            return
        L_0x006f:
            byte[] r5 = r6.a(r11)     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            goto L_0x0004
        L_0x0074:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            r0.<init>()     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.lang.StringBuilder r0 = r0.append(r7)     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.lang.String r1 = "?crc="
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.util.zip.CRC32 r1 = r6.b     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            long r2 = r1.getValue()     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.lang.String r1 = java.lang.Long.toHexString(r2)     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            java.lang.String r3 = r0.toString()     // Catch:{ Throwable -> 0x0099, all -> 0x0096 }
            goto L_0x0031
        L_0x0096:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0099:
            r0 = move-exception
            goto L_0x006d
        L_0x009b:
            r0 = move-exception
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.fb.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, byte[], java.lang.Object, int, boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002a A[SYNTHETIC, Splitter:B:10:0x002a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final byte[] a(byte[] r6) {
        /*
            r5 = this;
            java.util.zip.Deflater r0 = new java.util.zip.Deflater
            r1 = 9
            r0.<init>(r1)
            r0.setInput(r6)
            r2 = 0
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x006d }
            int r3 = r6.length     // Catch:{ all -> 0x006d }
            r1.<init>(r3)     // Catch:{ all -> 0x006d }
            r0.finish()     // Catch:{ all -> 0x0027 }
            r2 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r2]     // Catch:{ all -> 0x0027 }
        L_0x0018:
            boolean r3 = r0.finished()     // Catch:{ all -> 0x0027 }
            if (r3 != 0) goto L_0x002e
            int r3 = r0.deflate(r2)     // Catch:{ all -> 0x0027 }
            r4 = 0
            r1.write(r2, r4, r3)     // Catch:{ all -> 0x0027 }
            goto L_0x0018
        L_0x0027:
            r0 = move-exception
        L_0x0028:
            if (r1 == 0) goto L_0x002d
            r1.close()     // Catch:{ Exception -> 0x006b }
        L_0x002d:
            throw r0
        L_0x002e:
            if (r1 == 0) goto L_0x0033
            r1.close()     // Catch:{ Exception -> 0x0069 }
        L_0x0033:
            r1.close()
            byte[] r0 = r1.toByteArray()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Original: "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r6.length
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.tendcloud.tenddata.ev.a(r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Compressed: "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r0.length
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.tendcloud.tenddata.ev.a(r1)
            return r0
        L_0x0069:
            r0 = move-exception
            goto L_0x0033
        L_0x006b:
            r1 = move-exception
            goto L_0x002d
        L_0x006d:
            r0 = move-exception
            r1 = r2
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.fb.a(byte[]):byte[]");
    }

    /* access modifiers changed from: private */
    public final void c() {
        ey eyVar = new ey();
        eyVar.a = null;
        eyVar.b = null;
        try {
            gp.a().post(eyVar);
        } catch (Throwable th) {
        }
    }

    public void onTDEBEventSubmitRequest(ex exVar) {
        Message message = new Message();
        message.obj = exVar;
        this.c.sendMessage(message);
    }
}
