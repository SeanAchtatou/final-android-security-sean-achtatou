package com.tendcloud.tenddata;

import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.SocketChannel;

public class a implements i {
    private final ByteChannel a;

    public a(i iVar) {
        this.a = iVar;
    }

    public a(ByteChannel byteChannel) {
        this.a = byteChannel;
    }

    public int a(ByteBuffer byteBuffer) {
        if (this.a instanceof i) {
            return ((i) this.a).a(byteBuffer);
        }
        return 0;
    }

    public boolean a() {
        if (this.a instanceof i) {
            return ((i) this.a).a();
        }
        return false;
    }

    public void b() {
        if (this.a instanceof i) {
            ((i) this.a).b();
        }
    }

    public boolean c() {
        if (this.a instanceof i) {
            return ((i) this.a).c();
        }
        return false;
    }

    public void close() {
        this.a.close();
    }

    public boolean d() {
        if (this.a instanceof SocketChannel) {
            return ((SocketChannel) this.a).isBlocking();
        }
        if (this.a instanceof i) {
            return ((i) this.a).d();
        }
        return false;
    }

    public boolean isOpen() {
        return this.a.isOpen();
    }

    public int read(ByteBuffer byteBuffer) {
        return this.a.read(byteBuffer);
    }

    public int write(ByteBuffer byteBuffer) {
        return this.a.write(byteBuffer);
    }
}
