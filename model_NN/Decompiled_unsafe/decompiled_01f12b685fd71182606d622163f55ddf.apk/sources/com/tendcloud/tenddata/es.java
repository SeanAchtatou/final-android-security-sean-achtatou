package com.tendcloud.tenddata;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.tendcloud.tenddata.gd;

final class es extends Handler {
    es(Looper looper) {
        super(looper);
    }

    public void handleMessage(Message message) {
        if (AgentOption.a()) {
            switch (message.what) {
                case 102:
                    if (message.obj != null && (message.obj instanceof gd.a)) {
                        gd.a aVar = (gd.a) message.obj;
                        gd.b.a(1).a();
                        try {
                            gp.a().post(aVar);
                        } catch (Throwable th) {
                        }
                        gd.b.a(1).b();
                        return;
                    }
                    return;
                case 103:
                    if (message.obj != null && (message.obj instanceof ex)) {
                        try {
                            gp.a().post((ex) message.obj);
                            return;
                        } catch (Throwable th2) {
                            return;
                        }
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }
}
