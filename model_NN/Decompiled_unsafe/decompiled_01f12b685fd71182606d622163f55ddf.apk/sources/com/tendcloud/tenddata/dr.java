package com.tendcloud.tenddata;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import com.datalab.tools.Constant;
import java.util.Date;
import java.util.HashMap;
import org.json.JSONObject;

final class dr {
    private static final String a = "PushLog";
    private static String b;

    dr() {
    }

    private static HashMap a(JSONObject jSONObject) {
        return !jSONObject.isNull(dc.ac) ? cv.d(jSONObject.getJSONObject(dc.ac).toString()) : new HashMap();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private static JSONObject a(JSONObject jSONObject, String str, String str2) {
        int i = 1;
        int i2 = 0;
        JSONObject jSONObject2 = new JSONObject();
        try {
            int optInt = jSONObject.getJSONObject(dc.Y).optInt(dc.ab);
            if (optInt != 1) {
                if (optInt == 2) {
                    i = 0;
                    i2 = 1;
                } else if (optInt == 3) {
                    i2 = 1;
                } else {
                    i = 0;
                }
            }
            JSONObject jSONObject3 = new JSONObject();
            JSONObject jSONObject4 = new JSONObject();
            JSONObject jSONObject5 = new JSONObject();
            JSONObject jSONObject6 = new JSONObject();
            JSONObject jSONObject7 = new JSONObject();
            JSONObject jSONObject8 = new JSONObject(jSONObject.optString(dc.ac));
            jSONObject5.put("fontSize", 0);
            jSONObject5.put("fontBold", false);
            jSONObject5.put("fontItalic", false);
            jSONObject5.put("fontLineThrough", false);
            jSONObject5.put(dc.Z, str);
            jSONObject6.put("fontSize", 0);
            jSONObject6.put("fontBold", false);
            jSONObject6.put("fontItalic", false);
            jSONObject6.put("fontLineThrough", false);
            jSONObject6.put(dc.Z, str2);
            jSONObject7.put("sound", i);
            jSONObject7.put("vibrate", i2);
            jSONObject7.put("clearable", 1);
            jSONObject7.put("soundName", "");
            jSONObject4.put(dc.W, jSONObject5);
            jSONObject4.put("content", jSONObject6);
            jSONObject4.put("config", jSONObject7);
            jSONObject3.put("type", 1);
            jSONObject3.put(Constant.SIGN, b);
            jSONObject3.put("msg", jSONObject4);
            jSONObject3.put(dc.ac, jSONObject8);
            jSONObject2.put("app", jSONObject.optString("app"));
            jSONObject2.put("content", jSONObject3);
        } catch (Throwable th) {
            cs.e("PushLog", th.getMessage());
        }
        return jSONObject2;
    }

    protected static void a(Context context) {
        cv.f(context, dc.A);
    }

    protected static void a(Context context, Intent intent) {
        String stringExtra = intent.getStringExtra(dc.y);
        if (dc.B.equals(stringExtra)) {
            cs.a(context.getPackageName(), "relive at " + new Date());
            cv.f(context, dc.z);
        } else if (dc.A.equals(stringExtra)) {
            cs.a(context.getPackageName(), "ping at " + new Date());
        } else if (dc.C.equals(stringExtra)) {
            try {
                ((PowerManager) context.getSystemService("power")).newWakeLock(268435482, "Gank").acquire(30000);
            } catch (Throwable th) {
                cs.e("PushLog", th.getMessage());
            }
        }
    }

    private static boolean a(Context context, String str) {
        return cv.b(context).equals(str);
    }

    private static Intent b(JSONObject jSONObject) {
        Intent intent = new Intent();
        try {
            String optString = jSONObject.optString(dc.W);
            String optString2 = jSONObject.optString("content");
            HashMap a2 = a(jSONObject);
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put(dc.W, optString);
            jSONObject2.put("content", optString2);
            jSONObject2.put(dc.ac, jSONObject.optJSONObject(dc.ac));
            intent.putExtra(dc.W, optString);
            intent.putExtra("content", optString2);
            intent.putExtra(dc.ac, a2);
            intent.putExtra(dc.S, jSONObject2.toString());
            intent.putExtra(dc.w, jSONObject.toString());
        } catch (Throwable th) {
            cs.e("PushLog", "get msg error" + th);
        }
        return intent;
    }

    protected static void b(Context context, Intent intent) {
    }

    protected static void c(Context context, Intent intent) {
        String stringExtra = intent.getStringExtra(dc.v);
        if (!cp.a(stringExtra)) {
            try {
                JSONObject jSONObject = new JSONObject(stringExtra);
                if (a(context, jSONObject.optString("app"))) {
                    jSONObject.optString(dc.V);
                    jSONObject.optString(dc.W);
                    jSONObject.optString("content");
                    HashMap a2 = a(jSONObject);
                    Intent b2 = b(jSONObject);
                    b2.setPackage(context.getPackageName());
                    b2.setAction(dc.P);
                    context.sendBroadcast(b2);
                    if (a2 == null || a2.size() > 0) {
                    }
                }
            } catch (Throwable th) {
                cs.e("PushLog", th.getMessage());
            }
        }
    }

    protected static void d(Context context, Intent intent) {
    }
}
