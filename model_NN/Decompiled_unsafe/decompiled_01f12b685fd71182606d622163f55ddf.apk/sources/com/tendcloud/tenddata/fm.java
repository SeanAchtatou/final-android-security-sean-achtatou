package com.tendcloud.tenddata;

import com.badlogic.gdx.net.HttpResponseHeader;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

class fm {
    public static final String a = "conf.xdrig.com";
    public static final long b = 259200000;
    protected static final String c = "isDeveloper";
    private static String f = null;
    private static String g = "https";
    private static final String h = (g + "://" + a + "/sdk/conf?");
    private static final long i = 5;
    private static long j = 0;
    private static long k = 0;
    private static final String l = "frequency";
    private static final String m = "interval";
    private static final String n = "configVersion";
    private static final String o = "lastGetCloudSettingsTime";
    private static final String p = "SDKInitNumber";
    private static final String q = "SDKInitTime";
    private static final String r = "TD";
    private static volatile fm s = null;
    private static final String t = "";
    protected final String d = ("TDCloudSettingsConfig" + ab.getAppId(ab.mContext));
    HashMap e = new HashMap();

    static class a {
        String a = null;
        long b = -1;
        String c = null;
        String d = null;
        int e = 10;

        a() {
        }

        private long a(String str) {
            if (str == null || str.trim().isEmpty()) {
                return 0;
            }
            String substring = str.substring(str.length() - 1);
            if (b(substring)) {
                return Long.parseLong(str);
            }
            if (substring.equals("s")) {
                return Long.parseLong(str.substring(0, str.length() - 2));
            }
            if (substring.equals("m")) {
                return Long.parseLong(str.substring(0, str.length() - 2)) * 60;
            }
            if (substring.equals("h")) {
                return Long.parseLong(str.substring(0, str.length() - 2)) * 60 * 60;
            }
            return 0;
        }

        private final boolean b(String str) {
            if (str == null || "".equals(str.trim())) {
                return false;
            }
            return str.matches("^[0-9]*$");
        }

        /* access modifiers changed from: package-private */
        public void a(JSONObject jSONObject) {
            if (jSONObject != null) {
                try {
                    this.a = jSONObject.optString("Name");
                    this.b = a(jSONObject.optString("Interval"));
                    this.c = jSONObject.optString("EnableTime");
                    this.d = jSONObject.optString("DisableTime");
                    this.e = jSONObject.optInt("MinPowerThreshold");
                } catch (Throwable th) {
                }
            }
        }
    }

    static class b {
        private static volatile b g = null;
        String a = "Analytics";
        int b = 0;
        int c = 10;
        int d = 0;
        HashMap e;
        HashMap f;

        private b() {
        }

        static b a() {
            return a((String) null);
        }

        static b a(String str) {
            if (str == null || str.isEmpty()) {
                return g;
            }
            if (g == null) {
                synchronized (b.class) {
                    if (g == null) {
                        g = new b();
                        try {
                            JSONObject jSONObject = new JSONObject(str);
                            if (jSONObject != null) {
                                g.b = jSONObject.optInt(HttpResponseHeader.Status);
                                g.c = jSONObject.optInt("MaxSize");
                                g.d = jSONObject.optInt("networkFilter");
                                JSONArray optJSONArray = jSONObject.optJSONArray("Elements");
                                if (optJSONArray != null) {
                                    for (int i = 0; i < optJSONArray.length(); i++) {
                                        JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
                                        if (jSONObject2 != null) {
                                            a(jSONObject2);
                                        }
                                    }
                                }
                                JSONObject optJSONObject = jSONObject.optJSONObject("UserDefines");
                                if (optJSONObject != null) {
                                    Iterator<String> keys = optJSONObject.keys();
                                    while (keys.hasNext()) {
                                        String next = keys.next();
                                        g.f.put(next, optJSONObject.optString(next));
                                    }
                                }
                            }
                        } catch (Throwable th) {
                        }
                    }
                }
            }
            return g;
        }

        private static void a(JSONObject jSONObject) {
            if (jSONObject != null) {
                try {
                    a aVar = new a();
                    aVar.a(jSONObject);
                    g.e.put(aVar.a, aVar);
                } catch (Throwable th) {
                }
            }
        }
    }

    static {
        try {
            gp.a().register(a());
        } catch (Throwable th) {
        }
    }

    private fm() {
        new Thread(new fn(this)).start();
    }

    static fm a() {
        if (s == null) {
            synchronized (fm.class) {
                if (s == null) {
                    s = new fm();
                }
            }
        }
        return s;
    }

    /* access modifiers changed from: private */
    public boolean b() {
        try {
            if (ab.a()) {
                bu.a(ab.mContext, this.d, p, 0);
                return true;
            }
            bu.a(ab.mContext, this.d, p, bu.b(ab.mContext, this.d, p, 0) + 1);
            long currentTimeMillis = System.currentTimeMillis() - bu.b(ab.mContext, this.d, o, System.currentTimeMillis());
            long b2 = bu.b(ab.mContext, this.d, m, 0);
            long b3 = bu.b(ab.mContext, this.d, l, 1);
            long b4 = bu.b(ab.mContext, this.d, p, 0);
            if ((b3 == 1 && currentTimeMillis >= b2) || (b3 != 1 && b4 >= b3)) {
                bu.a(ab.mContext, this.d, p, 0);
                return true;
            }
            return false;
        } catch (Throwable th) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tendcloud.tenddata.bk.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.tendcloud.tenddata.bk.a(java.lang.String, java.lang.String, java.io.File):java.lang.String
      com.tendcloud.tenddata.bk.a(java.net.URL, java.lang.String, boolean):java.net.URLConnection
      com.tendcloud.tenddata.bk.a(java.lang.String, java.lang.String, int):void
      com.tendcloud.tenddata.bk.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public void c() {
        long j2 = b;
        long j3 = i;
        try {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("id=" + ab.getAppId(ab.mContext));
            stringBuffer.append("&p=1");
            stringBuffer.append("&v=" + URLEncoder.encode(bj.a().c(ab.mContext) + "+" + bj.a().b(ab.mContext), "UTF-8"));
            stringBuffer.append("&sv=" + URLEncoder.encode("+V2.2.46 gp".substring(1), "UTF-8"));
            String b2 = bu.b(ab.mContext, this.d, n, "a");
            stringBuffer.append(b2.equals("") ? "" : "&cv=" + URLEncoder.encode(b2, "UTF-8"));
            String a2 = bk.a(h + stringBuffer.toString(), "", true);
            if (a2 != null) {
                bu.a(ab.mContext, this.d, o, System.currentTimeMillis());
                JSONObject jSONObject = new JSONObject(a2);
                if (jSONObject.has("cv")) {
                    bu.a(ab.mContext, this.d, n, jSONObject.get("cv") + "");
                }
                if (jSONObject.has("pipline_settings")) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject("pipline_settings");
                    if (jSONObject2.has(l)) {
                        j = jSONObject2.getLong(l);
                        if (j < i) {
                            j3 = j < 1 ? 1 : j;
                        }
                        j = j3;
                        bu.a(ab.mContext, this.d, l, j);
                    }
                    if (jSONObject2.has(m)) {
                        k = jSONObject2.getLong(m);
                        if (j > 1) {
                            j2 = 0;
                        } else if (k <= b) {
                            j2 = k;
                        }
                        k = j2;
                        bu.a(ab.mContext, this.d, m, k);
                    }
                }
                if (jSONObject.has(fo.a)) {
                    JSONArray jSONArray = jSONObject.getJSONArray(fo.a);
                    ep epVar = new ep();
                    epVar.a.put("cloudSettingsType", "codeless");
                    epVar.a.put("data", jSONArray);
                    gp.a().post(epVar);
                }
            }
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(HashMap hashMap) {
        ev.a("Enter cloud settings initFundamentalData");
        b.a(ew.l());
    }
}
