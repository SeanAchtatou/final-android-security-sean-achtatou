package com.tendcloud.tenddata;

import com.sg.pak.PAK_ASSETS;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class bc implements Cloneable {
    private ba a;
    private Object b;
    private List c = new ArrayList();

    bc() {
    }

    private byte[] c() {
        byte[] bArr = new byte[a()];
        a(ay.a(bArr));
        return bArr;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        int i = 0;
        if (this.b != null) {
            return this.a.a(this.b);
        }
        Iterator it = this.c.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = ((bh) it.next()).a() + i2;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ay ayVar) {
        if (this.b != null) {
            this.a.a(this.b, ayVar);
            return;
        }
        for (bh a2 : this.c) {
            a2.a(ayVar);
        }
    }

    /* renamed from: b */
    public final bc clone() {
        bc bcVar = new bc();
        try {
            bcVar.a = this.a;
            if (this.c == null) {
                bcVar.c = null;
            } else {
                bcVar.c.addAll(this.c);
            }
            if (this.b != null) {
                if (this.b instanceof bf) {
                    bcVar.b = ((bf) this.b).clone();
                } else if (this.b instanceof byte[]) {
                    bcVar.b = ((byte[]) this.b).clone();
                } else if (this.b instanceof byte[][]) {
                    byte[][] bArr = (byte[][]) this.b;
                    byte[][] bArr2 = new byte[bArr.length][];
                    bcVar.b = bArr2;
                    for (int i = 0; i < bArr.length; i++) {
                        bArr2[i] = (byte[]) bArr[i].clone();
                    }
                } else if (this.b instanceof boolean[]) {
                    bcVar.b = ((boolean[]) this.b).clone();
                } else if (this.b instanceof int[]) {
                    bcVar.b = ((int[]) this.b).clone();
                } else if (this.b instanceof long[]) {
                    bcVar.b = ((long[]) this.b).clone();
                } else if (this.b instanceof float[]) {
                    bcVar.b = ((float[]) this.b).clone();
                } else if (this.b instanceof double[]) {
                    bcVar.b = ((double[]) this.b).clone();
                } else if (this.b instanceof bf[]) {
                    bf[] bfVarArr = (bf[]) this.b;
                    bf[] bfVarArr2 = new bf[bfVarArr.length];
                    bcVar.b = bfVarArr2;
                    for (int i2 = 0; i2 < bfVarArr.length; i2++) {
                        bfVarArr2[i2] = bfVarArr[i2].clone();
                    }
                }
            }
            return bcVar;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof bc)) {
            return false;
        }
        bc bcVar = (bc) obj;
        if (this.b == null || bcVar.b == null) {
            if (this.c != null && bcVar.c != null) {
                return this.c.equals(bcVar.c);
            }
            try {
                return Arrays.equals(c(), bcVar.c());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else if (this.a == bcVar.a) {
            return !this.a.t.isArray() ? this.b.equals(bcVar.b) : this.b instanceof byte[] ? Arrays.equals((byte[]) this.b, (byte[]) bcVar.b) : this.b instanceof int[] ? Arrays.equals((int[]) this.b, (int[]) bcVar.b) : this.b instanceof long[] ? Arrays.equals((long[]) this.b, (long[]) bcVar.b) : this.b instanceof float[] ? Arrays.equals((float[]) this.b, (float[]) bcVar.b) : this.b instanceof double[] ? Arrays.equals((double[]) this.b, (double[]) bcVar.b) : this.b instanceof boolean[] ? Arrays.equals((boolean[]) this.b, (boolean[]) bcVar.b) : Arrays.deepEquals((Object[]) this.b, (Object[]) bcVar.b);
        } else {
            return false;
        }
    }

    public int hashCode() {
        try {
            return Arrays.hashCode(c()) + PAK_ASSETS.IMG_GONGGAO011;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
