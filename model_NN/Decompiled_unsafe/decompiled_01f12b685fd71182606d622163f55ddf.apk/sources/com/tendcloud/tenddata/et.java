package com.tendcloud.tenddata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

final class et {
    private static int a = 2;
    private static int b = 6;
    private static int c = 6;
    private static int d = -40;
    private static int e = 4;
    private b f;

    class a {
        private String b;
        private String c;
        private byte d;
        private byte e;
        private byte f;

        a() {
            this.b = "";
            this.c = "00:00:00:00:00:00";
            this.d = -127;
            this.e = 1;
            this.f = 1;
        }

        a(String str, String str2, byte b2, byte b3, byte b4) {
            this.b = str;
            this.c = str2;
            this.d = b2;
            this.e = b3;
            this.f = b4;
        }

        /* access modifiers changed from: package-private */
        public String a() {
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public void a(byte b2) {
            this.d = b2;
        }

        /* access modifiers changed from: package-private */
        public void a(String str) {
            this.b = str;
        }

        /* access modifiers changed from: package-private */
        public String b() {
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public void b(byte b2) {
            this.e = b2;
        }

        /* access modifiers changed from: package-private */
        public void b(String str) {
            this.c = str;
        }

        /* access modifiers changed from: package-private */
        public byte c() {
            return this.d;
        }

        /* access modifiers changed from: package-private */
        public void c(byte b2) {
            this.f = b2;
        }

        /* access modifiers changed from: package-private */
        public byte d() {
            return this.e;
        }

        /* access modifiers changed from: package-private */
        public byte e() {
            return this.f;
        }

        /* access modifiers changed from: package-private */
        public a f() {
            return new a(this.b, this.c, this.d, this.e, this.f);
        }
    }

    static class b {
        static final int a = 10;
        static final int b = 3;
        static final int c = 50;
        static final int d = -85;
        private int e = 10;
        private int f = 3;
        private int g = 50;
        private int h = d;

        b() {
        }

        /* access modifiers changed from: package-private */
        public int a() {
            return this.e;
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            this.e = i;
        }

        /* access modifiers changed from: package-private */
        public int b() {
            return this.f;
        }

        /* access modifiers changed from: package-private */
        public void b(int i) {
            this.f = i;
        }

        /* access modifiers changed from: package-private */
        public int c() {
            return this.g;
        }

        /* access modifiers changed from: package-private */
        public void c(int i) {
            this.g = i;
        }

        /* access modifiers changed from: package-private */
        public int d() {
            return this.h;
        }

        /* access modifiers changed from: package-private */
        public void d(int i) {
            this.h = i;
        }
    }

    class c {
        private int b;
        private long c;
        private List d;
        private Map e;

        c() {
        }

        /* access modifiers changed from: package-private */
        public int a() {
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public Map a(boolean z) {
            if (this.e == null || z) {
                this.e = new HashMap();
                for (a aVar : this.d) {
                    this.e.put(aVar.b(), aVar);
                }
            }
            return this.e;
        }

        /* access modifiers changed from: package-private */
        public void a(int i) {
            this.b = i;
        }

        /* access modifiers changed from: package-private */
        public void a(long j) {
            this.c = j;
        }

        /* access modifiers changed from: package-private */
        public void a(List list) {
            this.d = list;
        }

        /* access modifiers changed from: package-private */
        public long b() {
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public List c() {
            return this.d;
        }

        /* access modifiers changed from: package-private */
        public c d() {
            c cVar = new c();
            cVar.a(this.b);
            cVar.a(this.c);
            LinkedList linkedList = new LinkedList();
            for (a f : this.d) {
                linkedList.add(f.f());
            }
            cVar.a(linkedList);
            return cVar;
        }
    }

    class d {
        c a;
        c b;
        double c;

        d(c cVar, c cVar2, double d2) {
            this.a = cVar;
            this.b = cVar2;
            this.c = d2;
        }
    }

    class e {
        Object a;
        Object b;

        e(Object obj, Object obj2) {
            this.a = obj;
            this.b = obj2;
        }
    }

    et() {
        this(new b());
    }

    et(b bVar) {
        this.f = bVar;
    }

    /* access modifiers changed from: package-private */
    public double a(int i, int i2) {
        double d2 = 0.0d;
        if (i >= 0 || i2 >= 0) {
            return 0.0d;
        }
        double d3 = (double) ((i + i2) / 2);
        double abs = Math.abs(((double) i) - d3);
        if (abs > ((double) a)) {
            d2 = abs - ((double) a);
        }
        return Math.pow((d2 + d3) / d3, (double) b);
    }

    /* access modifiers changed from: package-private */
    public double a(c cVar, c cVar2) {
        Map a2 = cVar.a(false);
        Map a3 = cVar2.a(false);
        HashSet hashSet = new HashSet();
        double d2 = 0.0d;
        double d3 = 0.0d;
        int i = 0;
        int i2 = 0;
        for (Map.Entry entry : a2.entrySet()) {
            a aVar = (a) entry.getValue();
            a aVar2 = (a) a3.get(entry.getKey());
            i += aVar.c();
            if (aVar2 == null) {
                hashSet.add(aVar);
            } else {
                i2++;
                double b2 = b(aVar.c(), aVar2.c());
                d3 += b2;
                d2 += a(aVar.c(), aVar2.c()) * b2;
            }
            d2 = d2;
            d3 = d3;
        }
        if (i2 == 0) {
            return 0.0d;
        }
        for (Map.Entry entry2 : a3.entrySet()) {
            i += ((a) entry2.getValue()).c();
            if (!a2.containsKey(entry2.getKey())) {
                hashSet.add(entry2.getValue());
            }
        }
        double d4 = 0.0d;
        int max = Math.max(this.f.d(), (int) (((double) (i / ((cVar.c().size() + cVar2.c().size()) - 0))) + 1.2d));
        Iterator it = hashSet.iterator();
        while (true) {
            double d5 = d4;
            if (it.hasNext()) {
                d4 = ((a) it.next()).c() > max ? 1.0d + d5 : d5;
            } else {
                return (1.0d - Math.pow(d5 / (((double) (i2 * 2)) + d5), (double) e)) * (d2 / d3);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public double a(c cVar, List list) {
        double d2 = 0.0d;
        Iterator it = list.iterator();
        while (true) {
            double d3 = d2;
            if (!it.hasNext()) {
                return d3;
            }
            d2 = Math.max(a((c) it.next(), cVar), d3);
        }
    }

    /* access modifiers changed from: package-private */
    public double a(List list, List list2) {
        double d2 = 0.0d;
        if (list.isEmpty() || list2.isEmpty()) {
            return 0.0d;
        }
        LinkedList linkedList = new LinkedList();
        b(list, list2, linkedList);
        int i = 0;
        Iterator it = linkedList.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return d2 / ((double) i2);
            }
            d dVar = (d) it.next();
            if (!(dVar.a == null || dVar.b == null)) {
                d2 += dVar.c;
                i2++;
            }
            i = i2;
        }
    }

    /* access modifiers changed from: package-private */
    public double a(List list, List list2, List list3) {
        double d2 = 0.0d;
        if (list.isEmpty() || list2.isEmpty()) {
            list3.addAll(list);
            list3.addAll(list2);
            return 0.0d;
        }
        LinkedList linkedList = new LinkedList();
        b(list, list2, linkedList);
        int i = 0;
        Iterator it = linkedList.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return d2 / ((double) i2);
            }
            d dVar = (d) it.next();
            if (dVar.a != null && dVar.b != null) {
                d2 += dVar.c;
                i2++;
                list3.add(b(dVar.a, dVar.b));
            } else if (list3.size() < this.f.b()) {
                list3.add(dVar.a == null ? dVar.b.d() : dVar.a.d());
            }
            i = i2;
        }
    }

    /* access modifiers changed from: package-private */
    public double b(int i, int i2) {
        if (i >= 0 || i2 >= 0) {
            return 0.0d;
        }
        double max = (double) Math.max(i, i2);
        if (max >= ((double) d)) {
            return 1.0d;
        }
        return Math.pow((max + 128.0d) / ((double) (d + 128)), (double) c);
    }

    /* access modifiers changed from: package-private */
    public c b(c cVar, c cVar2) {
        Map a2 = cVar.a(false);
        Map a3 = cVar2.a(false);
        TreeMap treeMap = new TreeMap();
        c cVar3 = new c();
        cVar3.a(cVar2.b());
        cVar3.a(cVar2.a());
        LinkedList linkedList = new LinkedList();
        cVar3.a(linkedList);
        for (Map.Entry entry : a2.entrySet()) {
            a aVar = (a) entry.getValue();
            a aVar2 = (a) a3.get(entry.getKey());
            if (aVar2 == null) {
                double d2 = (double) (-aVar.c());
                while (treeMap.containsKey(Double.valueOf(d2))) {
                    d2 += 1.0E-4d;
                }
                treeMap.put(Double.valueOf(d2), aVar);
            } else {
                linkedList.add(new a(aVar2.a(), aVar2.b(), (byte) ((aVar.c() + aVar2.c()) / 2), aVar2.d(), aVar2.e()));
            }
        }
        for (Map.Entry entry2 : a3.entrySet()) {
            if (!a2.containsKey(entry2.getKey())) {
                double d3 = (double) (-((a) entry2.getValue()).c());
                while (treeMap.containsKey(Double.valueOf(d3))) {
                    d3 += 1.0E-4d;
                }
                treeMap.put(Double.valueOf(d3), entry2.getValue());
            }
        }
        for (Map.Entry entry3 : treeMap.entrySet()) {
            byte b2 = (byte) ((int) (-((Double) entry3.getKey()).doubleValue()));
            if (linkedList.size() >= this.f.c() || b2 < this.f.d()) {
                break;
            }
            linkedList.add(entry3.getValue());
        }
        return cVar3;
    }

    /* access modifiers changed from: package-private */
    public void b(List list, List list2, List list3) {
        ArrayList arrayList = new ArrayList();
        HashSet<c> hashSet = new HashSet<>();
        HashSet<c> hashSet2 = new HashSet<>();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next();
            Iterator it2 = list2.iterator();
            while (it2.hasNext()) {
                c cVar2 = (c) it2.next();
                hashSet2.add(cVar2);
                arrayList.add(new d(cVar, cVar2, a(cVar, cVar2)));
            }
            hashSet.add(cVar);
        }
        Collections.sort(arrayList, new eu(this));
        list3.clear();
        Iterator it3 = arrayList.iterator();
        while (it3.hasNext()) {
            d dVar = (d) it3.next();
            if (hashSet.contains(dVar.a) && hashSet2.contains(dVar.b)) {
                hashSet.remove(dVar.a);
                hashSet2.remove(dVar.b);
                list3.add(dVar);
            }
        }
        for (c dVar2 : hashSet) {
            list3.add(new d(dVar2, null, 0.0d));
        }
        for (c dVar3 : hashSet2) {
            list3.add(new d(null, dVar3, 0.0d));
        }
    }
}
