package com.tendcloud.tenddata;

import android.os.Handler;
import android.os.HandlerThread;
import java.util.Iterator;
import java.util.List;
import java.util.zip.CRC32;

final class dk {
    static final int a = 0;
    static final int b = 1;
    static final int c = 2;
    static final int d = 3;
    static long e = 300000;
    static Handler f;
    private static String g = "utf-8";
    private static final CRC32 h = new CRC32();
    private static String i = "";
    private static final HandlerThread j = new HandlerThread("NetWorkThread");

    static {
        f = null;
        j.start();
        f = new dl(j.getLooper());
    }

    dk() {
    }

    static final Handler a() {
        return f;
    }

    static String a(List list) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            Iterator it = list.iterator();
            while (it.hasNext()) {
                sb.append((String) it.next());
                sb.append(",");
            }
            sb.deleteCharAt(sb.length() - 1);
            sb.append("]");
            return sb.toString();
        } catch (Throwable th) {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d A[SYNTHETIC, Splitter:B:14:0x002d] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0037 A[SYNTHETIC, Splitter:B:20:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0040 A[SYNTHETIC, Splitter:B:25:0x0040] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] a(java.lang.String r6) {
        /*
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream
            r2.<init>()
            r1 = 0
            java.util.zip.Deflater r3 = new java.util.zip.Deflater
            r0 = 9
            r4 = 1
            r3.<init>(r0, r4)
            java.util.zip.DeflaterOutputStream r0 = new java.util.zip.DeflaterOutputStream     // Catch:{ FileNotFoundException -> 0x0029, Throwable -> 0x0033, all -> 0x003d }
            r0.<init>(r2, r3)     // Catch:{ FileNotFoundException -> 0x0029, Throwable -> 0x0033, all -> 0x003d }
            java.lang.String r1 = "UTF-8"
            byte[] r1 = r6.getBytes(r1)     // Catch:{ FileNotFoundException -> 0x004f, Throwable -> 0x004d, all -> 0x0048 }
            r0.write(r1)     // Catch:{ FileNotFoundException -> 0x004f, Throwable -> 0x004d, all -> 0x0048 }
            if (r0 == 0) goto L_0x0021
            r0.close()     // Catch:{ IOException -> 0x0044 }
        L_0x0021:
            r3.end()
            byte[] r0 = r2.toByteArray()
            return r0
        L_0x0029:
            r0 = move-exception
            r0 = r1
        L_0x002b:
            if (r0 == 0) goto L_0x0021
            r0.close()     // Catch:{ IOException -> 0x0031 }
            goto L_0x0021
        L_0x0031:
            r0 = move-exception
            goto L_0x0021
        L_0x0033:
            r0 = move-exception
            r0 = r1
        L_0x0035:
            if (r0 == 0) goto L_0x0021
            r0.close()     // Catch:{ IOException -> 0x003b }
            goto L_0x0021
        L_0x003b:
            r0 = move-exception
            goto L_0x0021
        L_0x003d:
            r0 = move-exception
        L_0x003e:
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ IOException -> 0x0046 }
        L_0x0043:
            throw r0
        L_0x0044:
            r0 = move-exception
            goto L_0x0021
        L_0x0046:
            r1 = move-exception
            goto L_0x0043
        L_0x0048:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x003e
        L_0x004d:
            r1 = move-exception
            goto L_0x0035
        L_0x004f:
            r1 = move-exception
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.dk.a(java.lang.String):byte[]");
    }

    private static boolean b(String str) {
        try {
            byte[] a2 = a(str);
            h.reset();
            h.update(a2);
            StringBuilder sb = new StringBuilder("https://push.xdrig.com/push");
            sb.append("/" + dd.f);
            sb.append("/" + Long.toHexString(h.getValue()));
            cs.d(dq.a, "Send pushEvent");
            int a3 = bk.a(ab.mContext, "push.xdrig.com", dd.c, sb.toString(), i, a2).a();
            if (a3 == 200) {
                return true;
            }
            cs.d(dq.a, "Send pushEvent# response status code: " + a3);
            return false;
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: private */
    public static void c() {
        List a2;
        try {
            if (br.c(ab.mContext) && ab.mContext != null && (a2 = dj.a()) != null && a2.size() > 0) {
                boolean b2 = b(a(a2));
                if (b2) {
                    dj.b();
                } else {
                    if (!f.hasMessages(2)) {
                        a().sendEmptyMessageDelayed(2, e);
                    }
                    dj.c();
                }
                cs.d(dq.a, "Send pushEvent success : " + b2);
            }
        } catch (Throwable th) {
        }
    }
}
