package com.tendcloud.tenddata;

final class dg {
    private String a = null;
    private String b = null;
    private String c = null;
    private String d;
    private String e;
    private a f;
    private int g;

    public enum a {
        ARRIVED(0),
        CLICK(1),
        SHOW(2),
        UNSHOWN(3),
        CANCEL(4),
        INAPP_SHOW(11),
        INAPP_DURATION(12),
        INAPP_UNSHOW(13);
        
        private final int i;

        private a(int i2) {
            this.i = i2;
        }

        public int a() {
            return this.i;
        }
    }

    dg() {
    }

    dg(String str, String str2, a aVar, int i) {
        this.e = str;
        this.f = aVar;
        this.d = str2;
        this.g = i;
    }

    dg(String str, String str2, String str3) {
        this.c = str;
        this.b = str2;
        this.a = str3;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public String e() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public a f() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public int g() {
        return this.g;
    }
}
