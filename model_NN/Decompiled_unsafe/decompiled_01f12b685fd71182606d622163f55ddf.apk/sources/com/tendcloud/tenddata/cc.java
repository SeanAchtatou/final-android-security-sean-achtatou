package com.tendcloud.tenddata;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

final class cc implements InvocationHandler {
    final /* synthetic */ bv a;
    final /* synthetic */ Object b;

    cc(bv bvVar, Object obj) {
        this.a = bvVar;
        this.b = obj;
    }

    public Object invoke(Object obj, Method method, Object[] objArr) {
        this.a.a(obj, method, objArr);
        Object invoke = method.invoke(this.b, objArr);
        this.a.a(obj, method, objArr, invoke);
        return invoke;
    }
}
