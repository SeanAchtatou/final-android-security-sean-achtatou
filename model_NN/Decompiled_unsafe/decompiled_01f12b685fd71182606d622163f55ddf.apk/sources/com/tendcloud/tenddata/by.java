package com.tendcloud.tenddata;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Vibrator;

class by implements SensorEventListener {
    final /* synthetic */ bw a;
    private int b = 0;

    by(bw bwVar) {
        this.a = bwVar;
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            int type = sensorEvent.sensor.getType();
            if (currentTimeMillis - this.a.e > 250 && type == 1) {
                long unused = this.a.e = currentTimeMillis;
                float[] fArr = sensorEvent.values;
                if (Math.abs(fArr[0]) > 18.0f || Math.abs(fArr[1]) > 18.0f || Math.abs(fArr[2]) > 18.0f) {
                    this.b++;
                }
                if (this.b >= 5) {
                    if (this.a.j != null) {
                        if (ca.b(this.a.a, "android.permission.VIBRATE")) {
                            ((Vibrator) this.a.a.getSystemService("vibrator")).vibrate(100);
                        }
                        this.a.j.a();
                        if (this.a.i != null) {
                            this.a.i.unregisterListener(this.a.l);
                        }
                    }
                    this.b = 0;
                }
            }
        } catch (Throwable th) {
        }
    }
}
