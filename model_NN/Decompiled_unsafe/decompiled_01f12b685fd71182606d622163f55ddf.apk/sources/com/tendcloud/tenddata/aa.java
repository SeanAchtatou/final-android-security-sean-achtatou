package com.tendcloud.tenddata;

import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import com.datalab.tools.Constant;
import com.tendcloud.tenddata.ff;
import com.tendcloud.tenddata.gd;
import com.wali.gamecenter.report.io.HttpConnectionManager;
import java.io.ByteArrayOutputStream;
import java.nio.channels.FileLock;
import java.util.HashMap;
import java.util.Random;
import java.util.zip.GZIPOutputStream;

class aa {
    private static volatile aa f = null;
    private static bv g = null;
    private static Handler n;
    private static HandlerThread o = new HandlerThread("prepareSubmitHandlerThread");
    private final String a = "av1.xdrig.com";
    private final String b = null;
    private final String c = "https";
    private final String d = "https://av1.xdrig.com/g/d";
    private volatile boolean e = false;
    private ek h = a(1);
    private final int i = HttpConnectionManager.GPRS_WAIT_TIMEOUT;
    private int j = HttpConnectionManager.GPRS_WAIT_TIMEOUT;
    private boolean k = false;
    private long l = (SystemClock.elapsedRealtime() - ((long) this.j));
    private Random m = new Random();
    private FileLock p = null;
    private final String q = "";

    final class a extends ek {
        a(String str, String str2, String str3) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = 1;
        }

        /* access modifiers changed from: package-private */
        public byte[] a(HashMap hashMap) {
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
                GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
                fh fhVar = new fh(gZIPOutputStream);
                if (!hashMap.containsKey("entity") || hashMap.get("entity") == null || !(hashMap.get("entity") instanceof ff.f)) {
                    return null;
                }
                fhVar.a((ff.f) hashMap.get("entity"));
                gZIPOutputStream.finish();
                gZIPOutputStream.close();
                return byteArrayOutputStream.toByteArray();
            } catch (Throwable th) {
                th.printStackTrace();
                return null;
            }
        }
    }

    static {
        n = null;
        o.start();
        n = new fi(o.getLooper());
        try {
            gp.a().register(a());
        } catch (Throwable th) {
        }
    }

    private aa() {
    }

    static aa a() {
        if (f == null) {
            synchronized (aa.class) {
                if (f == null) {
                    f = new aa();
                }
            }
        }
        return f;
    }

    private final ek a(int i2) {
        switch (i2) {
            case 1:
                return new a("av1.xdrig.com", this.b, "https://av1.xdrig.com/g/d");
            default:
                return null;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b() {
        if (!this.e) {
            try {
                if (ab.f != null) {
                    this.p = ab.f.tryLock();
                }
                if (this.p == null) {
                    ev.b("Aborted submitting, file cannot be accessed due to lock.");
                } else if (br.c(ab.mContext)) {
                    ff.f a2 = fk.a(gd.b.a(1));
                    ex exVar = new ex();
                    exVar.c = this.h.a;
                    exVar.b = this.h.b;
                    exVar.a = this.h.c;
                    exVar.d = "Analytics";
                    exVar.e = "";
                    exVar.g = a2;
                    if (a2 == null) {
                        ev.a("No new data found!");
                        exVar.f = null;
                    } else {
                        ev.a("New data found, Submitting...");
                        HashMap hashMap = new HashMap();
                        hashMap.put("entity", a2);
                        exVar.f = this.h.a(hashMap);
                        exVar.h = true;
                    }
                    Message obtain = Message.obtain();
                    obtain.obj = exVar;
                    obtain.what = 103;
                    er.a().sendMessage(obtain);
                    this.e = true;
                }
            } catch (Throwable th) {
                c();
            }
        }
        return;
    }

    private void c() {
        if (this.p != null) {
            try {
                this.p.release();
            } catch (Throwable th) {
            }
        }
    }

    private final void d() {
        if (!ew.d() && ab.mContext != null) {
            LocationManager locationManager = (LocationManager) ab.mContext.getApplicationContext().getSystemService("location");
            WifiManager wifiManager = (WifiManager) ab.mContext.getApplicationContext().getSystemService("wifi");
            try {
                ca.a(locationManager, g, "mService", "android.location.ILocationManager");
                ca.a(wifiManager, g, "mService", "android.net.wifi.IWifiManager");
            } catch (Throwable th) {
                ev.c(th.getMessage());
            }
        }
    }

    private void e() {
        ca.execute(new fj(this));
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(boolean z) {
        long j2 = 0;
        int i2 = 1;
        synchronized (this) {
            if (AgentOption.b()) {
                Message obtain = Message.obtain();
                if (!z) {
                    i2 = 0;
                }
                obtain.what = i2;
                if (!z) {
                    long elapsedRealtime = SystemClock.elapsedRealtime() - this.l;
                    if (elapsedRealtime >= 0) {
                        if (this.l > 0 && elapsedRealtime < 120000) {
                            j2 = 120000 - elapsedRealtime;
                        }
                        n.removeMessages(0);
                        n.removeMessages(1);
                        n.sendMessageDelayed(obtain, j2);
                    }
                } else {
                    if (this.k) {
                        this.j = HttpConnectionManager.GPRS_WAIT_TIMEOUT;
                    }
                    if (SystemClock.elapsedRealtime() - this.l > ((long) this.j)) {
                        n.removeMessages(0);
                        n.removeMessages(1);
                        n.sendMessage(obtain);
                    }
                }
            }
        }
    }

    public final void onTDEBEventResponse(ey eyVar) {
        try {
            this.l = SystemClock.elapsedRealtime();
            if (eyVar != null) {
                if (eyVar.a != null && String.valueOf(eyVar.a.get(dc.Y)).equals("Analytics")) {
                    if (Integer.valueOf(String.valueOf(eyVar.a.get("statusCode"))).intValue() == 200) {
                        ev.a("Data submitting Succeed!");
                        ff.f fVar = null;
                        if (eyVar.b != null && (eyVar.b instanceof ff.f)) {
                            fVar = (ff.f) eyVar.b;
                        }
                        fk.a(fVar, gd.b.a(1));
                        this.j = 120000;
                        this.k = true;
                    } else {
                        e();
                        this.j = (this.m.nextInt(60000) - 30000) + 120000;
                        ev.a("Data submitting Failed!");
                        this.k = false;
                    }
                }
                if (!br.c(ab.mContext)) {
                    e();
                }
                if (ew.i() != null && !ew.i().equals(Constant.S_C)) {
                    a(false);
                }
            }
            c();
            this.e = false;
        } catch (Throwable th) {
        }
    }
}
