package com.tendcloud.tenddata;

import com.tendcloud.tenddata.eh;
import com.tendcloud.tenddata.ej;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class eb {
    private static final Class[] c = new Class[0];
    private static final List d = Collections.emptyList();
    private final ef a;
    private final ej.g b;

    static class a {
        final ej a;
        final List b;

        private a(ej ejVar, List list) {
            this.a = ejVar;
            this.b = list;
        }
    }

    class b {
        public final String a;
        public final Class b;
        public final ee c;
        private final String e;

        public b(String str, Class cls, ee eeVar, String str2) {
            this.a = str;
            this.b = cls;
            this.c = eeVar;
            this.e = str2;
        }

        public ee a(Object[] objArr) {
            if (this.e == null) {
                return null;
            }
            return new ee(this.b, this.e, objArr, Void.TYPE);
        }

        public String toString() {
            return "[PropertyDescription " + this.a + "," + this.b + ", " + this.c + "/" + this.e + "]";
        }
    }

    eb(ef efVar, ej.g gVar) {
        this.a = efVar;
        this.b = gVar;
    }

    private b a(Class cls, JSONObject jSONObject) {
        ee eeVar;
        try {
            String string = jSONObject.getString("name");
            if (jSONObject.has("get")) {
                JSONObject jSONObject2 = jSONObject.getJSONObject("get");
                eeVar = new ee(cls, jSONObject2.getString("selector"), c, Class.forName(jSONObject2.getJSONObject("result").getString("type")));
            } else {
                eeVar = null;
            }
            return new b(string, cls, eeVar, jSONObject.has("set") ? jSONObject.getJSONObject("set").getString("selector") : null);
        } catch (Throwable th) {
            return null;
        }
    }

    private Integer a(int i, String str, ef efVar) {
        int i2;
        if (str == null) {
            i2 = -1;
        } else if (!efVar.a(str)) {
            return null;
        } else {
            i2 = efVar.b(str);
        }
        if (-1 == i2 || -1 == i || i2 == i) {
            return -1 != i2 ? Integer.valueOf(i2) : Integer.valueOf(i);
        }
        return null;
    }

    private static String a(JSONObject jSONObject, String str) {
        if (!jSONObject.has(str) || jSONObject.isNull(str)) {
            return null;
        }
        return jSONObject.getString(str);
    }

    /* access modifiers changed from: package-private */
    public ei a(JSONObject jSONObject) {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = jSONObject.getJSONObject("config").getJSONArray("classes");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                Class<?> cls = Class.forName(jSONObject2.getString("name"));
                JSONArray jSONArray2 = jSONObject2.getJSONArray("properties");
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    arrayList.add(a(cls, jSONArray2.getJSONObject(i2)));
                }
            }
            return new ei(arrayList, this.a);
        } catch (Throwable th) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public ej a(JSONObject jSONObject, ej.f fVar) {
        try {
            String string = jSONObject.getString(dc.V);
            String string2 = jSONObject.getString("type");
            List a2 = a(jSONObject.getJSONArray("path"), this.a);
            if (a2.size() == 0) {
            }
            if ("click".equals(string2)) {
                return new ej.a(a2, 1, string, fVar);
            }
            if ("selected".equals(string2)) {
                return new ej.a(a2, 4, string, fVar);
            }
            if ("text_changed".equals(string2)) {
                return new ej.b(a2, string, fVar);
            }
            if ("detected".equals(string2)) {
                return new ej.i(a2, string, fVar);
            }
            return null;
        } catch (JSONException e) {
        }
    }

    /* access modifiers changed from: package-private */
    public List a(JSONArray jSONArray, ef efVar) {
        int i;
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            JSONObject jSONObject = jSONArray.getJSONObject(i2);
            String a2 = a(jSONObject, "prefix");
            String a3 = a(jSONObject, "class");
            int optInt = jSONObject.optInt("index", -1);
            String a4 = a(jSONObject, "description");
            int optInt2 = jSONObject.optInt(dc.V, -1);
            String a5 = a(jSONObject, "id_name");
            String a6 = a(jSONObject, "tag");
            if ("shortest".equals(a2)) {
                i = 1;
            } else if (a2 != null) {
                return d;
            } else {
                i = 0;
            }
            Integer a7 = a(optInt2, a5, efVar);
            if (a7 == null) {
                return d;
            }
            arrayList.add(new eh.c(i, a3, optInt, a7.intValue(), a4, a6));
        }
        return arrayList;
    }
}
