package com.tendcloud.tenddata;

import android.content.Context;
import android.os.HandlerThread;

public class co {
    public static Context a = null;
    public static String b = null;
    public static String c = null;
    public static final String d = "app";
    static HandlerThread e = new HandlerThread("TalkingDataMain");

    static {
        e.start();
    }

    public static String a(Context context) {
        return bm.a(context);
    }

    public static void a(Context context, String str) {
        b = str;
        init(context);
    }

    public static void a(Context context, String str, String str2) {
        c = str2;
        a(context, str);
    }

    public static String b(Context context) {
        return ab.getPartnerId(context);
    }

    public static void init(Context context) {
        try {
            String appAnalyticsAppId = ab.getAppAnalyticsAppId();
            if (!ca.b(appAnalyticsAppId)) {
                TCAgent.init(context, appAnalyticsAppId, ab.getChannelId());
            }
        } catch (Throwable th) {
        }
    }
}
