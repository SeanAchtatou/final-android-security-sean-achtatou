package com.tendcloud.tenddata;

class fa {
    private static final String a = "SntpClient";
    private static final int b = 16;
    private static final int c = 24;
    private static final int d = 32;
    private static final int e = 40;
    private static final int f = 48;
    private static final int g = 123;
    private static final int h = 3;
    private static final int i = 3;
    private static final long j = 2208988800L;
    private long k;
    private long l;
    private long m;

    fa() {
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private long a(byte[] bArr, int i2) {
        byte b2 = bArr[i2];
        byte b3 = bArr[i2 + 1];
        byte b4 = bArr[i2 + 2];
        byte b5 = bArr[i2 + 3];
        byte b6 = b2 & true;
        int i3 = b2;
        if (b6 == true) {
            i3 = (b2 & Byte.MAX_VALUE) + 128;
        }
        byte b7 = b3 & true;
        int i4 = b3;
        if (b7 == true) {
            i4 = (b3 & Byte.MAX_VALUE) + 128;
        }
        byte b8 = b4 & true;
        int i5 = b4;
        if (b8 == true) {
            i5 = (b4 & Byte.MAX_VALUE) + 128;
        }
        byte b9 = b5 & true;
        int i6 = b5;
        if (b9 == true) {
            i6 = (b5 & Byte.MAX_VALUE) + 128;
        }
        return ((long) i6) + (((long) i4) << 16) + (((long) i3) << 24) + (((long) i5) << 8);
    }

    private void a(byte[] bArr, int i2, long j2) {
        long j3 = j2 / 1000;
        long j4 = j3 + j;
        int i3 = i2 + 1;
        bArr[i2] = (byte) ((int) (j4 >> 24));
        int i4 = i3 + 1;
        bArr[i3] = (byte) ((int) (j4 >> 16));
        int i5 = i4 + 1;
        bArr[i4] = (byte) ((int) (j4 >> 8));
        int i6 = i5 + 1;
        bArr[i5] = (byte) ((int) (j4 >> 0));
        long j5 = (4294967296L * (j2 - (1000 * j3))) / 1000;
        int i7 = i6 + 1;
        bArr[i6] = (byte) ((int) (j5 >> 24));
        int i8 = i7 + 1;
        bArr[i7] = (byte) ((int) (j5 >> 16));
        int i9 = i8 + 1;
        bArr[i8] = (byte) ((int) (j5 >> 8));
        int i10 = i9 + 1;
        bArr[i9] = (byte) ((int) (Math.random() * 255.0d));
    }

    private long b(byte[] bArr, int i2) {
        return ((a(bArr, i2) - j) * 1000) + ((a(bArr, i2 + 4) * 1000) / 4294967296L);
    }

    public long a() {
        return this.k;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r20, int r21) {
        /*
            r19 = this;
            r3 = 0
            java.net.DatagramSocket r2 = new java.net.DatagramSocket     // Catch:{ Exception -> 0x007b, all -> 0x0083 }
            r2.<init>()     // Catch:{ Exception -> 0x007b, all -> 0x0083 }
            r0 = r21
            r2.setSoTimeout(r0)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            java.net.InetAddress r3 = java.net.InetAddress.getByName(r20)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r4 = 48
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            java.net.DatagramPacket r5 = new java.net.DatagramPacket     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            int r6 = r4.length     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r7 = 123(0x7b, float:1.72E-43)
            r5.<init>(r4, r6, r3, r7)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r3 = 0
            r6 = 27
            r4[r3] = r6     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            long r8 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r3 = 40
            r0 = r19
            r0.a(r4, r3, r6)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r2.send(r5)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            java.net.DatagramPacket r3 = new java.net.DatagramPacket     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            int r5 = r4.length     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r3.<init>(r4, r5)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r2.receive(r3)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            long r10 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            long r12 = r10 - r8
            long r6 = r6 + r12
            r3 = 24
            r0 = r19
            long r12 = r0.b(r4, r3)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r3 = 32
            r0 = r19
            long r14 = r0.b(r4, r3)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r3 = 40
            r0 = r19
            long r4 = r0.b(r4, r3)     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            long r8 = r10 - r8
            long r16 = r4 - r14
            long r8 = r8 - r16
            long r12 = r14 - r12
            long r4 = r4 - r6
            long r4 = r4 + r12
            r12 = 2
            long r4 = r4 / r12
            long r4 = r4 + r6
            r0 = r19
            r0.k = r4     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r0 = r19
            r0.l = r10     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            r0 = r19
            r0.m = r8     // Catch:{ Exception -> 0x0091, all -> 0x008a }
            if (r2 == 0) goto L_0x0079
            r2.close()
        L_0x0079:
            r2 = 1
        L_0x007a:
            return r2
        L_0x007b:
            r2 = move-exception
        L_0x007c:
            r2 = 0
            if (r3 == 0) goto L_0x007a
            r3.close()
            goto L_0x007a
        L_0x0083:
            r2 = move-exception
        L_0x0084:
            if (r3 == 0) goto L_0x0089
            r3.close()
        L_0x0089:
            throw r2
        L_0x008a:
            r3 = move-exception
            r18 = r3
            r3 = r2
            r2 = r18
            goto L_0x0084
        L_0x0091:
            r3 = move-exception
            r3 = r2
            goto L_0x007c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tendcloud.tenddata.fa.a(java.lang.String, int):boolean");
    }

    public long b() {
        return this.l;
    }

    public long c() {
        return this.m;
    }
}
