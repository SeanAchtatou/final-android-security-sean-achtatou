package com.tendcloud.tenddata;

import com.kbz.esotericsoftware.spine.Animation;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

class ff {

    static class a implements fd, fg {
        String a = "";
        long b = 0;
        int c = 0;
        String d = "";

        a() {
        }

        public int a() {
            return fh.c(4) + fh.c(this.a) + fh.c(this.b) + fh.c(this.c) + fh.c(this.d);
        }

        public void messagePack(fh fhVar) {
            fhVar.b(4);
            fhVar.a(this.a);
            fhVar.a(this.b);
            fhVar.a(this.c);
            fhVar.a(this.d);
        }

        public String toString() {
            return "Activity{" + "name:" + this.a + ",start:" + this.b + ",duration:" + this.c + ",refer:" + this.d;
        }
    }

    static class b implements fd, fg {
        String a = "";
        String b = "";
        int c = 0;
        long d;
        Map e;

        b() {
        }

        public int a() {
            return fh.c(3) + fh.c(this.a) + fh.c(this.b) + fh.c(this.c);
        }

        public void messagePack(fh fhVar) {
            fhVar.b(5);
            fhVar.a(this.a);
            fhVar.a(this.b);
            fhVar.a(this.c);
            fhVar.a(this.d);
            fhVar.a(this.e);
        }

        public String toString() {
            return "AppEvent{" + "id:" + this.a + ",label:" + this.b + ",count:" + this.c + ",ts:" + this.d + ",kv:" + this.e + '}';
        }
    }

    static class c implements fd, fg {
        long a = 0;
        int b = 1;
        String c = "";
        byte[] d = new byte[0];
        String e = "";

        c() {
        }

        public int a() {
            return fh.c(5) + fh.c(this.a) + fh.c(this.b) + fh.c(this.c) + fh.b(this.d) + fh.c(this.e);
        }

        public void messagePack(fh fhVar) {
            fhVar.b(5);
            fhVar.a(this.a);
            fhVar.a(this.b);
            fhVar.a(this.c);
            fhVar.a(this.d);
            fhVar.a(this.e);
        }
    }

    static class d implements fd, fg {
        String a = "";
        String b = "";
        String c = "";
        long d = 0;
        String e = "";
        String f = "";
        boolean g = false;
        long h = 0;
        long i = 0;

        d() {
        }

        public int a() {
            return fh.c(9) + fh.c(this.a) + fh.c(this.b) + fh.c(this.c) + fh.c(this.d) + fh.c(this.e) + fh.c(this.f) + fh.b(this.g) + fh.c(this.h) + fh.c(this.i);
        }

        public void messagePack(fh fhVar) {
            fhVar.b(9);
            fhVar.a(this.a);
            fhVar.a(this.b);
            fhVar.a(this.c);
            fhVar.a(this.d);
            fhVar.a(this.e);
            fhVar.a(this.f);
            fhVar.a(this.g);
            fhVar.a(this.h);
            fhVar.a(this.i);
        }
    }

    static class e implements fd, fg {
        String a = "";
        String b = "";
        h c = new h();
        String d = "";
        String e = "";
        String f = "";
        String g = "";
        String h = "";
        int i = 8;
        String j = "";
        int k = -1;
        String l = "";
        boolean m = false;
        String n = "";
        String o = "";
        String p = "";
        String q = "";
        long r = 0;
        String s = "";
        String t = "";
        String u = "";
        int v;
        int w;
        String x = "";

        e() {
        }

        public int a() {
            return fh.c(24) + fh.c(this.a) + fh.c(this.b) + this.c.a() + fh.c(this.d) + fh.c(this.e) + fh.c(this.f) + fh.c(this.g) + fh.c(this.h) + fh.c(this.i) + fh.c(this.j) + fh.c(this.k) + fh.c(this.l) + fh.b(this.m) + fh.c(this.n) + fh.c(this.o) + fh.c(this.p) + fh.c(this.q) + fh.c(this.r) + fh.c(this.s) + fh.c(this.t) + fh.c(this.u) + fh.c(this.v) + fh.c(this.w) + fh.c(this.x);
        }

        public void messagePack(fh fhVar) {
            fhVar.b(24);
            fhVar.a(this.a);
            fhVar.a(this.b);
            fhVar.a(this.c);
            fhVar.a(this.d);
            fhVar.a(this.e);
            fhVar.a(this.f);
            fhVar.a(this.g);
            fhVar.a(this.h);
            fhVar.a(this.i);
            fhVar.a(this.j);
            fhVar.a(this.k);
            fhVar.a(this.l);
            fhVar.a(this.m);
            fhVar.a(this.n);
            fhVar.a(this.o);
            fhVar.a(this.p);
            fhVar.a(this.q);
            fhVar.a(this.r).a(this.s).a(this.t).a(this.u).a(this.v).a(this.w).a(this.x);
        }
    }

    static class f implements fe, fg {
        String a = "";
        String b = "";
        d c = new d();
        e d = new e();
        List e = new ArrayList();
        long f = 0;
        long g = 0;
        long h = 0;
        Long[][] i;

        f() {
        }

        public int a() {
            return fh.c(5) + fh.c(this.a) + fh.c(this.b) + this.c.a() + this.d.a();
        }

        public void messagePack(fh fhVar) {
            fhVar.b(6);
            fhVar.a(this.a);
            fhVar.a(this.b);
            fhVar.a(this.c);
            fhVar.a(this.d);
            fhVar.b(this.e.size());
            for (i a2 : this.e) {
                fhVar.a(a2);
            }
            if (this.i == null) {
                fhVar.b();
                return;
            }
            fhVar.b(this.i.length);
            for (Long[] a3 : this.i) {
                fhVar.a(a3);
            }
        }
    }

    static class g implements fd, fg {
        String A = "";
        String B = "";
        String C = "";
        String a = "";
        int b = 0;
        float c = Animation.CurveTimeline.LINEAR;
        String d = "";
        String e = "";
        String f = "";
        int g = 0;
        int h = 0;
        int i = 0;
        int j = 0;
        int k = 0;
        int l = 0;
        int m = 0;
        float n = Animation.CurveTimeline.LINEAR;
        float o = Animation.CurveTimeline.LINEAR;
        int p = 0;
        String q = "";
        String r = "";
        String s = "";
        String t = "";
        String u = "";
        String v = "";
        String w = "";
        boolean x = false;
        String y = "";
        String z = "";

        g() {
        }

        public int a() {
            return fh.c(29) + fh.c(this.a) + fh.c(this.b) + fh.b(this.c) + fh.c(this.d) + fh.c(this.e) + fh.c(this.f) + fh.c(this.g) + fh.c(this.h) + fh.c(this.i) + fh.c(this.j) + fh.c(this.k) + fh.c(this.l) + fh.c(this.m) + fh.b(this.n) + fh.b(this.o) + fh.c(this.p) + fh.c(this.q) + fh.c(this.r) + fh.c(this.s) + fh.c(this.t) + fh.c(this.u) + fh.c(this.v) + fh.c(this.w) + fh.b(this.x) + fh.c(this.y) + fh.c(this.z) + fh.c(this.A) + fh.c(this.B) + fh.c(this.C);
        }

        public void messagePack(fh fhVar) {
            fhVar.b(29);
            fhVar.a(this.a);
            fhVar.a(this.b);
            fhVar.a(this.c);
            fhVar.a(this.d);
            fhVar.a(this.e);
            fhVar.a(this.f);
            fhVar.a(this.g);
            fhVar.a(this.h);
            fhVar.a(this.i);
            fhVar.a(this.j);
            fhVar.a(this.k);
            fhVar.a(this.l);
            fhVar.a(this.m);
            fhVar.a(this.n);
            fhVar.a(this.o);
            fhVar.a(this.p);
            fhVar.a(this.q);
            fhVar.a(this.r);
            fhVar.a(this.s);
            fhVar.a(this.t);
            fhVar.a(this.u);
            fhVar.a(this.v);
            fhVar.a(this.w);
            fhVar.a(this.x);
            fhVar.a(this.y);
            fhVar.a(this.z);
            fhVar.a(this.A);
            fhVar.a(this.B);
            fhVar.a(this.C);
        }
    }

    static class h implements fd, fg {
        double a = 0.0d;
        double b = 0.0d;

        h() {
        }

        public int a() {
            return fh.c(2) + fh.b(this.a) + fh.b(this.b);
        }

        public void messagePack(fh fhVar) {
            fhVar.b(2);
            fhVar.a(this.a);
            fhVar.a(this.b);
        }
    }

    static class i implements fg {
        int a = -1;
        j b;
        g c;
        c d;

        i() {
        }

        public void messagePack(fh fhVar) {
            fhVar.b(2);
            fhVar.a(this.a);
            switch (this.a) {
                case 1:
                    fhVar.a(this.c);
                    return;
                case 2:
                    fhVar.a(this.b);
                    return;
                case 3:
                    fhVar.a(this.d);
                    return;
                default:
                    throw new IOException("unknown TMessageType");
            }
        }
    }

    static class j implements fd, fg {
        static final int d = 1;
        static final int e = 2;
        static final int f = 3;
        String a = "";
        long b = 0;
        int c = 0;
        int g = 0;
        List h = new ArrayList();
        List i = new ArrayList();
        int j = 0;
        int k = 0;
        long l = 0;

        j() {
        }

        public int a() {
            int i2;
            int c2 = fh.c(8) + fh.c(this.a) + fh.c(this.b) + fh.c(this.c) + fh.c(this.g) + fh.c(this.k) + fh.c(this.h.size());
            Iterator it = this.h.iterator();
            while (true) {
                i2 = c2;
                if (!it.hasNext()) {
                    break;
                }
                c2 = ((a) it.next()).a() + i2;
            }
            int c3 = fh.c(this.i.size()) + i2;
            Iterator it2 = this.i.iterator();
            while (true) {
                int i3 = c3;
                if (!it2.hasNext()) {
                    return fh.c(this.l) + i3;
                }
                c3 = ((b) it2.next()).a() + i3;
            }
        }

        public void messagePack(fh fhVar) {
            fhVar.b(8);
            fhVar.a(this.a);
            fhVar.a(this.b);
            fhVar.a(this.c);
            fhVar.a(this.g);
            fhVar.b(this.h.size());
            for (a a2 : this.h) {
                fhVar.a(a2);
            }
            fhVar.b(this.i.size());
            for (b a3 : this.i) {
                fhVar.a(a3);
            }
            fhVar.a(this.k);
            fhVar.a(this.l);
        }

        public String toString() {
            return "Session{" + "id:" + this.a + ",start:" + this.b + ",status:" + this.c + ",duration:" + this.g + ",connected:" + this.k + ",time_gap:" + this.l + '}';
        }
    }

    ff() {
    }
}
