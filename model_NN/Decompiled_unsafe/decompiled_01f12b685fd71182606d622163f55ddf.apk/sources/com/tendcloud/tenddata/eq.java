package com.tendcloud.tenddata;

import android.util.Base64;
import java.security.MessageDigest;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

final class eq {
    private static final String a = "AES/CBC/PKCS7Padding";
    private static final String b = "UTF-8";
    private static final String c = "SHA-256";
    private static final byte[] d = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    private eq() {
    }

    static String a(String str) {
        try {
            return Base64.encodeToString(a(d(ab.getAppId(ab.mContext)), d, str.getBytes(b)), 2);
        } catch (Throwable th) {
            return null;
        }
    }

    private static String a(byte[] bArr) {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] cArr2 = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            byte b2 = bArr[i] & 255;
            cArr2[i * 2] = cArr[b2 >>> 4];
            cArr2[(i * 2) + 1] = cArr[b2 & 15];
        }
        return new String(cArr2);
    }

    private static void a(String str, String str2) {
    }

    private static void a(String str, byte[] bArr) {
    }

    private static byte[] a(SecretKeySpec secretKeySpec, byte[] bArr, byte[] bArr2) {
        Cipher instance = Cipher.getInstance(a);
        instance.init(1, secretKeySpec, new IvParameterSpec(bArr));
        return instance.doFinal(bArr2);
    }

    static String b(String str) {
        try {
            String a2 = ew.a();
            return new String(b(a2 == null ? d(ab.getAppId(ab.mContext)) : new SecretKeySpec(Base64.decode(a2, 2), "AES"), d, Base64.decode(str, 2)), b);
        } catch (Throwable th) {
            return null;
        }
    }

    private static byte[] b(SecretKeySpec secretKeySpec, byte[] bArr, byte[] bArr2) {
        Cipher instance = Cipher.getInstance(a);
        instance.init(2, secretKeySpec, new IvParameterSpec(bArr));
        return instance.doFinal(bArr2);
    }

    private static SecretKeySpec c(String str) {
        MessageDigest instance = MessageDigest.getInstance(c);
        byte[] bytes = str.getBytes(b);
        instance.update(bytes, 0, bytes.length);
        byte[] digest = instance.digest();
        ew.a(Base64.encodeToString(digest, 2));
        return new SecretKeySpec(digest, "AES");
    }

    private static SecretKeySpec d(String str) {
        String a2 = ew.a();
        return a2 != null ? new SecretKeySpec(Base64.decode(a2, 2), "AES") : c(str);
    }
}
