package com.tendcloud.tenddata;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;
import com.tendcloud.tenddata.eh;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.WeakHashMap;

@TargetApi(16)
abstract class ej implements eh.a {
    private final List a;
    private final eh b = new eh();

    static class a extends d {
        /* access modifiers changed from: private */
        public final int a;
        private final WeakHashMap b = new WeakHashMap();

        /* renamed from: com.tendcloud.tenddata.ej$a$a  reason: collision with other inner class name */
        class C0000a extends View.AccessibilityDelegate {
            private View.AccessibilityDelegate b;

            C0000a(View.AccessibilityDelegate accessibilityDelegate) {
                this.b = accessibilityDelegate;
            }

            /* access modifiers changed from: package-private */
            public View.AccessibilityDelegate a() {
                return this.b;
            }

            /* access modifiers changed from: package-private */
            public void a(C0000a aVar) {
                if (this.b == aVar) {
                    this.b = aVar.a();
                } else if (this.b instanceof C0000a) {
                    ((C0000a) this.b).a(aVar);
                }
            }

            /* access modifiers changed from: package-private */
            public boolean a(String str) {
                if (a.this.e() == str) {
                    return true;
                }
                if (this.b instanceof C0000a) {
                    return ((C0000a) this.b).a(str);
                }
                return false;
            }

            public void sendAccessibilityEvent(View view, int i) {
                if (i == a.this.a) {
                    a.this.b(view);
                }
                if (this.b != null) {
                    this.b.sendAccessibilityEvent(view, i);
                }
            }
        }

        a(List list, int i, String str, f fVar) {
            super(list, str, fVar, false);
            this.a = i;
        }

        private View.AccessibilityDelegate c(View view) {
            try {
                return (View.AccessibilityDelegate) view.getClass().getMethod("getAccessibilityDelegate", new Class[0]).invoke(view, new Object[0]);
            } catch (NoSuchMethodException e) {
                return null;
            } catch (IllegalAccessException e2) {
                return null;
            } catch (InvocationTargetException e3) {
                return null;
            }
        }

        public void a() {
            for (Map.Entry entry : this.b.entrySet()) {
                View view = (View) entry.getKey();
                C0000a aVar = (C0000a) entry.getValue();
                View.AccessibilityDelegate c = c(view);
                if (c == aVar) {
                    view.setAccessibilityDelegate(aVar.a());
                } else if (c instanceof C0000a) {
                    ((C0000a) c).a(aVar);
                }
            }
            this.b.clear();
        }

        public void accumulate(View view) {
            View.AccessibilityDelegate c = c(view);
            if (!(c instanceof C0000a) || !((C0000a) c).a(e())) {
                C0000a aVar = new C0000a(c);
                view.setAccessibilityDelegate(aVar);
                this.b.put(view, aVar);
            }
        }

        /* access modifiers changed from: protected */
        public String d() {
            return e() + " event when (" + this.a + ")";
        }
    }

    static class b extends d {
        private final Map a = new HashMap();

        class a implements TextWatcher {
            private final View b;

            public a(View view) {
                this.b = view;
            }

            public void afterTextChanged(Editable editable) {
                b.this.b(this.b);
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        }

        public b(List list, String str, f fVar) {
            super(list, str, fVar, true);
        }

        public void a() {
            for (Map.Entry entry : this.a.entrySet()) {
                ((TextView) entry.getKey()).removeTextChangedListener((TextWatcher) entry.getValue());
            }
            this.a.clear();
        }

        public void accumulate(View view) {
            if (view instanceof TextView) {
                TextView textView = (TextView) view;
                a aVar = new a(textView);
                TextWatcher textWatcher = (TextWatcher) this.a.get(textView);
                if (textWatcher != null) {
                    textView.removeTextChangedListener(textWatcher);
                }
                textView.addTextChangedListener(aVar);
                this.a.put(textView, aVar);
            }
        }

        /* access modifiers changed from: protected */
        public String d() {
            return e() + " on Text Change";
        }
    }

    static class c {
        private c() {
        }

        private boolean a(TreeMap treeMap, View view, List list) {
            if (list.contains(view)) {
                return false;
            }
            if (treeMap.containsKey(view)) {
                List list2 = (List) treeMap.remove(view);
                list.add(view);
                int size = list2.size();
                for (int i = 0; i < size; i++) {
                    if (!a(treeMap, (View) list2.get(i), list)) {
                        return false;
                    }
                }
                list.remove(view);
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public boolean a(TreeMap treeMap) {
            ArrayList arrayList = new ArrayList();
            while (!treeMap.isEmpty()) {
                if (!a(treeMap, (View) treeMap.firstKey(), arrayList)) {
                    return false;
                }
            }
            return true;
        }
    }

    static abstract class d extends ej {
        private final f a;
        private final String b;
        private final boolean c;

        public d(List list, String str, f fVar, boolean z) {
            super(list);
            this.a = fVar;
            this.b = str;
            this.c = z;
        }

        /* access modifiers changed from: protected */
        public void b(View view) {
            this.a.a(view, this.b, this.c);
        }

        /* access modifiers changed from: protected */
        public String e() {
            return this.b;
        }
    }

    static class e {
        private final String a;
        private final String b;

        public e(String str, String str2) {
            this.a = str;
            this.b = str2;
        }

        /* access modifiers changed from: package-private */
        public String a() {
            return this.a;
        }

        /* access modifiers changed from: package-private */
        public String b() {
            return this.b;
        }
    }

    interface f {
        void a(View view, String str, boolean z);
    }

    interface g {
        void onLayoutError(e eVar);
    }

    static class h extends ej {
        private final ee a;
        private final ee b;
        private final WeakHashMap c = new WeakHashMap();
        private final Object[] d = new Object[1];

        h(List list, ee eeVar, ee eeVar2) {
            super(list);
            this.a = eeVar;
            this.b = eeVar2;
        }

        public void a() {
            for (Map.Entry entry : this.c.entrySet()) {
                View view = (View) entry.getKey();
                Object value = entry.getValue();
                if (value != null) {
                    this.d[0] = value;
                    this.a.a(view, this.d);
                }
            }
        }

        public void accumulate(View view) {
            if (this.b != null) {
                Object[] a2 = this.a.a();
                if (1 == a2.length) {
                    Object obj = a2[0];
                    Object a3 = this.b.a(view);
                    if (obj != a3) {
                        if (obj != null) {
                            if (!(obj instanceof Bitmap) || !(a3 instanceof Bitmap)) {
                                if ((obj instanceof BitmapDrawable) && (a3 instanceof BitmapDrawable)) {
                                    Bitmap bitmap = ((BitmapDrawable) obj).getBitmap();
                                    Bitmap bitmap2 = ((BitmapDrawable) a3).getBitmap();
                                    if (bitmap != null && bitmap.sameAs(bitmap2)) {
                                        return;
                                    }
                                } else if (obj.equals(a3)) {
                                    return;
                                }
                            } else if (((Bitmap) obj).sameAs((Bitmap) a3)) {
                                return;
                            }
                        }
                        if (!(a3 instanceof Bitmap) && !(a3 instanceof BitmapDrawable) && !this.c.containsKey(view)) {
                            this.d[0] = a3;
                            if (this.a.a(this.d)) {
                                this.c.put(view, a3);
                            } else {
                                this.c.put(view, null);
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
            this.a.a(view);
        }

        /* access modifiers changed from: protected */
        public String d() {
            return "Property Mutator";
        }
    }

    static class i extends d {
        private boolean a = false;

        public i(List list, String str, f fVar) {
            super(list, str, fVar, false);
        }

        public void a() {
        }

        public void accumulate(View view) {
            if (view != null && !this.a) {
                b(view);
            }
            this.a = view != null;
        }

        /* access modifiers changed from: protected */
        public String d() {
            return e() + " when Detected";
        }
    }

    protected ej(List list) {
        this.a = list;
    }

    /* access modifiers changed from: package-private */
    public abstract void a();

    /* access modifiers changed from: package-private */
    public void a(View view) {
        this.b.a(view, this.a, this);
    }

    /* access modifiers changed from: protected */
    public List b() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public eh c() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public abstract String d();
}
