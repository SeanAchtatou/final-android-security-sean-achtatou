package com.tendcloud.tenddata;

import android.view.View;
import android.view.ViewGroup;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

class eh {
    private boolean a = false;
    private final b b = new b();

    interface a {
        void accumulate(View view);
    }

    static class b {
        private static final int c = 256;
        private final int[] a = new int[256];
        private int b = 0;

        b() {
        }

        /* access modifiers changed from: package-private */
        public int a(int i) {
            return this.a[i];
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return this.a.length == this.b;
        }

        /* access modifiers changed from: package-private */
        public int b() {
            int i = this.b;
            this.b++;
            this.a[i] = 0;
            return i;
        }

        /* access modifiers changed from: package-private */
        public void b(int i) {
            int[] iArr = this.a;
            iArr[i] = iArr[i] + 1;
        }

        /* access modifiers changed from: package-private */
        public void c() {
            this.b--;
            if (this.b < 0) {
                throw new ArrayIndexOutOfBoundsException(this.b);
            }
        }
    }

    static class c {
        static final int g = 0;
        static final int h = 1;
        final int a;
        final String b;
        final int c;
        final int d;
        final String e;
        final String f;

        c(int i, String str, int i2, int i3, String str2, String str3) {
            this.a = i;
            this.b = str;
            this.c = i2;
            this.d = i3;
            this.e = str2;
            this.f = str3;
        }

        public String toString() {
            try {
                JSONObject jSONObject = new JSONObject();
                if (this.a == 1) {
                    jSONObject.put("prefix", "shortest");
                }
                if (this.b != null) {
                    jSONObject.put("class", this.b);
                }
                if (this.c > -1) {
                    jSONObject.put("index", this.c);
                }
                if (this.d > -1) {
                    jSONObject.put(dc.V, this.d);
                }
                if (this.e != null) {
                    jSONObject.put("contentDescription", this.e);
                }
                if (this.f != null) {
                    jSONObject.put("tag", this.f);
                }
                return jSONObject.toString();
            } catch (JSONException e2) {
                throw new RuntimeException("Can't serialize PathElement to String", e2);
            }
        }
    }

    eh() {
    }

    private View a(c cVar, View view, int i) {
        int a2 = this.b.a(i);
        if (a(cVar, view)) {
            this.b.b(i);
            if (cVar.c == -1 || cVar.c == a2) {
                return view;
            }
        }
        if (cVar.a == 1 && (view instanceof ViewGroup)) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i2 = 0; i2 < childCount; i2++) {
                View a3 = a(cVar, viewGroup.getChildAt(i2), i);
                if (a3 != null) {
                    return a3;
                }
            }
        }
        return null;
    }

    private boolean a(c cVar, View view) {
        if (cVar.b != null && !a(view, cVar.b)) {
            return false;
        }
        if (-1 != cVar.d && view.getId() != cVar.d) {
            return false;
        }
        if (cVar.e != null && !cVar.e.equals(view.getContentDescription())) {
            return false;
        }
        return cVar.f == null || (view.getTag() != null && cVar.f.equals(view.getTag().toString()));
    }

    private static boolean a(Object obj, String str) {
        for (Class<?> cls = obj.getClass(); !cls.getCanonicalName().equals(str); cls = cls.getSuperclass()) {
            if (cls == Object.class) {
                return false;
            }
        }
        return true;
    }

    private void b(View view, List list, a aVar) {
        if (list.isEmpty()) {
            this.a = true;
            aVar.accumulate(view);
        } else if ((view instanceof ViewGroup) && !this.b.a()) {
            ViewGroup viewGroup = (ViewGroup) view;
            c cVar = (c) list.get(0);
            List subList = list.subList(1, list.size());
            int childCount = viewGroup.getChildCount();
            int b2 = this.b.b();
            for (int i = 0; i < childCount; i++) {
                View a2 = a(cVar, viewGroup.getChildAt(i), b2);
                if (a2 != null) {
                    b(a2, subList, aVar);
                }
                if (cVar.c >= 0 && this.b.a(b2) > cVar.c) {
                    break;
                }
            }
            this.b.c();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view, List list, a aVar) {
        if (!list.isEmpty() && !this.b.a()) {
            List subList = list.subList(1, list.size());
            View a2 = a((c) list.get(0), view, this.b.b());
            this.b.c();
            if (a2 != null) {
                b(a2, subList, aVar);
            }
        }
    }
}
