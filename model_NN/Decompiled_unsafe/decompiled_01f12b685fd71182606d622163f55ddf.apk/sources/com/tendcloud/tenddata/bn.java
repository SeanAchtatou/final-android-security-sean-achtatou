package com.tendcloud.tenddata;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class bn {
    private static final int a = 3600000;

    private static int a(String str) {
        String str2 = "";
        try {
            Matcher matcher = Pattern.compile("([0-9]+)").matcher(str);
            if (matcher.find()) {
                str2 = matcher.toMatchResult().group(0);
            }
            return Integer.valueOf(str2).intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public static String a() {
        return "Android+" + Build.VERSION.RELEASE;
    }

    public static String a(Context context) {
        try {
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            if (displayMetrics != null) {
                int i = displayMetrics.widthPixels;
                int i2 = displayMetrics.heightPixels;
                return Math.min(i, i2) + "*" + Math.max(i, i2) + "*" + displayMetrics.densityDpi;
            }
        } catch (Throwable th) {
        }
        return "";
    }

    private static String a(String str, String str2) {
        try {
            String lowerCase = str.toLowerCase();
            if (lowerCase.startsWith("unknown") || lowerCase.startsWith("alps") || lowerCase.startsWith("android") || lowerCase.startsWith("sprd") || lowerCase.startsWith("spreadtrum") || lowerCase.startsWith("rockchip") || lowerCase.startsWith("wondermedia") || lowerCase.startsWith("mtk") || lowerCase.startsWith("mt65") || lowerCase.startsWith("nvidia") || lowerCase.startsWith("brcm") || lowerCase.startsWith("marvell") || str2.toLowerCase().contains(lowerCase)) {
                return null;
            }
            return str;
        } catch (Throwable th) {
            return null;
        }
    }

    public static String b() {
        return Build.MANUFACTURER.trim();
    }

    private static String b(String str) {
        String str2 = null;
        try {
            FileReader fileReader = new FileReader(str);
            try {
                char[] cArr = new char[1024];
                BufferedReader bufferedReader = new BufferedReader(fileReader, 1024);
                while (true) {
                    int read = bufferedReader.read(cArr, 0, 1024);
                    if (-1 == read) {
                        break;
                    }
                    str2 = str2 + new String(cArr, 0, read);
                }
                bufferedReader.close();
                fileReader.close();
            } catch (IOException e) {
            }
        } catch (Throwable th) {
        }
        return str2;
    }

    public static String c() {
        return Build.BRAND.trim();
    }

    public static String d() {
        return Build.MODEL.trim();
    }

    public static int e() {
        return TimeZone.getDefault().getRawOffset() / a;
    }

    public static String f() {
        try {
            String trim = Build.MODEL.trim();
            String a2 = a(Build.MANUFACTURER.trim(), trim);
            if (TextUtils.isEmpty(a2)) {
                a2 = a(Build.BRAND.trim(), trim);
            }
            if (a2 == null) {
                a2 = "";
            }
            return a2 + ":" + trim;
        } catch (Throwable th) {
            return "";
        }
    }

    public static int g() {
        return Build.VERSION.SDK_INT;
    }

    public static String h() {
        return Build.VERSION.RELEASE;
    }

    public static String i() {
        return Locale.getDefault().getLanguage();
    }

    public static String j() {
        return Locale.getDefault().getCountry();
    }

    public static String[] k() {
        boolean z;
        FileReader fileReader;
        BufferedReader bufferedReader;
        String[] strArr = new String[4];
        for (int i = 0; i < 4; i++) {
            strArr[i] = "";
        }
        ArrayList arrayList = new ArrayList();
        try {
            fileReader = new FileReader("/proc/cpuinfo");
            bufferedReader = new BufferedReader(fileReader, 1024);
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    arrayList.add(readLine);
                } else {
                    try {
                        break;
                    } catch (IOException e) {
                        z = true;
                    } catch (Throwable th) {
                        z = true;
                    }
                }
            }
            bufferedReader.close();
            fileReader.close();
            z = true;
        } catch (Throwable th2) {
            z = false;
        }
        String[] strArr2 = {"Processor\\s*:\\s*(.*)", "CPU\\s*variant\\s*:\\s*0x(.*)", "Hardware\\s*:\\s*(.*)"};
        if (z) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < 3; i2++) {
                Pattern compile = Pattern.compile(strArr2[i2]);
                for (int i3 = 0; i3 < size; i3++) {
                    Matcher matcher = compile.matcher((String) arrayList.get(i3));
                    if (matcher.find()) {
                        strArr[i2] = matcher.toMatchResult().group(1);
                    }
                }
            }
        }
        strArr[3] = b("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq");
        return strArr;
    }

    public static String[] l() {
        return null;
    }

    public static int[] m() {
        FileReader fileReader;
        BufferedReader bufferedReader;
        int[] iArr = {0, 0};
        int[] iArr2 = new int[4];
        for (int i = 0; i < 4; i++) {
            iArr2[i] = 0;
        }
        try {
            fileReader = new FileReader("/proc/meminfo");
            bufferedReader = new BufferedReader(fileReader, 1024);
            for (int i2 = 0; i2 < 4; i2++) {
                iArr2[i2] = a(bufferedReader.readLine());
            }
            iArr[0] = iArr2[0];
            iArr[1] = iArr2[3] + iArr2[1] + iArr2[2];
            try {
                bufferedReader.close();
                fileReader.close();
            } catch (IOException e) {
            }
        } catch (IOException e2) {
            try {
                bufferedReader.close();
                fileReader.close();
            } catch (IOException e3) {
            }
        } catch (Throwable th) {
        }
        return iArr;
    }

    public static int[] n() {
        try {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getAbsolutePath());
            int availableBlocks = statFs.getAvailableBlocks();
            StatFs statFs2 = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
            return new int[]{(statFs.getBlockCount() * (statFs.getBlockSize() / 512)) / 2, ((statFs.getBlockSize() / 512) * availableBlocks) / 2, (statFs2.getBlockCount() * (statFs2.getBlockSize() / 512)) / 2, ((statFs2.getBlockSize() / 512) * statFs2.getAvailableBlocks()) / 2};
        } catch (Throwable th) {
            return null;
        }
    }

    public static int o() {
        try {
            Matcher matcher = Pattern.compile("\\s*([0-9]+)").matcher(b("/sys/class/power_supply/battery/full_bat"));
            if (matcher.find()) {
                return Integer.valueOf(matcher.toMatchResult().group(0)).intValue();
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }
}
