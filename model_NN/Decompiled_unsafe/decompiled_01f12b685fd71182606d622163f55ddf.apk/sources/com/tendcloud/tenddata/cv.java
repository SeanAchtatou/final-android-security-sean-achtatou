package com.tendcloud.tenddata;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import com.apptalkingdata.push.service.PushService;
import com.tendcloud.tenddata.dn;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

final class cv {
    public static final byte a = 0;
    public static final byte b = 1;
    public static final byte c = 2;
    private static final String d = "PushLog";
    private static int e = -1;
    private static String f;
    private static String g;

    cv() {
    }

    static dm a(dn.b bVar) {
        dm dmVar = new dm();
        dmVar.a = bVar.d;
        dmVar.b = bVar.i;
        dmVar.c = bVar.f;
        dmVar.d = new String(bVar.h);
        return dmVar;
    }

    static Object a(byte[] bArr, Class cls) {
        if (bArr == null || bArr.length < 1) {
            return null;
        }
        try {
            byte[] bArr2 = new byte[(bArr.length - 1)];
            System.arraycopy(bArr, 1, bArr2, 0, bArr.length - 1);
            if (cls == dn.c.class) {
                return dn.c.a(bArr2);
            }
            if (cls == dn.b.class) {
                return dn.b.a(bArr2);
            }
            return null;
        } catch (Throwable th) {
            cs.e("PushLog", "unpack message error" + th);
            return null;
        }
    }

    static String a() {
        return dc.F.split("/")[2];
    }

    static String a(int i, String str, String str2) {
        if (str.length() >= i) {
            return str;
        }
        String str3 = "";
        for (int i2 = 0; i2 < i - str.length(); i2++) {
            str3 = str3 + str2;
        }
        return str3 + str;
    }

    static String a(long j, DateFormat dateFormat) {
        return dateFormat.format(new Date(j));
    }

    static String a(Context context) {
        if (f != null && !f.isEmpty()) {
            return f;
        }
        try {
            f = bm.a(context);
            cs.a("PushLog", "TCAgent getDeviceId " + f);
            if (f != null && !f.isEmpty()) {
                return f;
            }
        } catch (Throwable th) {
            cs.a("PushLog", "[push] Error getDeviceId " + th);
        }
        if (f == null || f.isEmpty()) {
            cs.e("PushLog", "Get Push Id Error uuid is null");
        }
        return f;
    }

    static String a(Context context, String str) {
        if (context == null || str == null) {
            cs.e("PushLog", "context & meta Key required");
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo.metaData == null) {
                return null;
            }
            return applicationInfo.metaData.getString(str);
        } catch (Throwable th) {
            cs.e("PushLog", "fail to get application info:" + th);
            return null;
        }
    }

    static String a(String str) {
        return b(b(str));
    }

    @SuppressLint({"NewApi"})
    static void a(Context context, long j) {
        Intent intent = new Intent(context, PushService.class);
        intent.putExtra(dc.y, dc.A);
        PendingIntent service = PendingIntent.getService(context, 0, intent, 134217728);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        alarmManager.cancel(service);
        long currentTimeMillis = System.currentTimeMillis() + j;
        if (Build.VERSION.SDK_INT < 19) {
            alarmManager.set(0, currentTimeMillis, service);
        } else {
            alarmManager.setExact(0, currentTimeMillis, service);
        }
    }

    static boolean a(Context context, String str, String str2) {
        try {
            return e(context.createPackageContext(str, 2), str2);
        } catch (Throwable th) {
            return false;
        }
    }

    static boolean a(String str, Context context) {
        long j = dc.c;
        long currentTimeMillis = System.currentTimeMillis();
        long b2 = bu.b(context, dc.i, str, currentTimeMillis);
        if (!br.g(context)) {
            j = 21600000;
        }
        return currentTimeMillis - b2 >= j || currentTimeMillis - b2 == 0;
    }

    static byte[] a(Object obj) {
        byte[] bArr;
        byte b2 = -1;
        if (obj instanceof dn.c) {
            dn.c cVar = (dn.c) obj;
            bArr = dn.c.a((dn.c) obj);
            b2 = 1;
        } else if (obj instanceof dn.b) {
            dn.b bVar = (dn.b) obj;
            bArr = dn.b.a((dn.b) obj);
            b2 = 0;
        } else if (obj instanceof dn.a) {
            dn.a aVar = (dn.a) obj;
            bArr = dn.a.a((dn.a) obj);
            b2 = 2;
        } else {
            bArr = null;
        }
        if (bArr == null) {
            cs.b("PushLog", "bad obj");
            return null;
        }
        ByteBuffer allocate = ByteBuffer.allocate(bArr.length + 1);
        allocate.put(b2);
        allocate.put(bArr);
        return allocate.array();
    }

    static byte[] a(byte[] bArr) {
        ByteBuffer allocate = ByteBuffer.allocate(bArr.length + 4);
        allocate.putInt(bArr.length);
        allocate.put(bArr);
        return allocate.array();
    }

    static String b(Context context) {
        try {
            g = cr.d(dc.q);
            if (g != null && !g.isEmpty()) {
                return g;
            }
            g = "app-" + TCAgent.getAppId(context);
            if (g == null || g.isEmpty()) {
                cs.e("PushLog", "[push] start service error, app id is required");
            }
            cr.a(dc.q, g);
            return g;
        } catch (Throwable th) {
            cs.e("PushLog", "get AppId Error" + th);
        }
    }

    static String b(byte[] bArr) {
        StringBuilder sb = new StringBuilder("");
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= bArr.length) {
                return sb.toString();
            }
            String hexString = Integer.toHexString(bArr[i2] & 255);
            if (hexString.length() == 1) {
                hexString = "0" + hexString;
            }
            sb.append(hexString);
            i = i2 + 1;
        }
    }

    static void b(Context context, String str) {
        if (!a(context, str, PushService.class.getName())) {
            d(context, str);
        }
        c(context, str);
    }

    static byte[] b(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.reset();
            instance.update(str.getBytes());
            return instance.digest();
        } catch (Throwable th) {
            cs.b("PushLog", "hexMD5 failed");
            return null;
        }
    }

    static String c(Context context) {
        return a("com.mrocker.push.servicehttp://" + a() + a(context));
    }

    static void c(Context context, String str) {
        try {
            Intent intent = new Intent();
            intent.setClassName(str, zx.class.getName());
            context.startService(intent);
        } catch (Throwable th) {
            cs.e("PushLog", "restart service error: " + th);
        }
    }

    static byte[] c(String str) {
        try {
            FileInputStream fileInputStream = new FileInputStream(new File(str));
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1000);
            byte[] bArr = new byte[1000];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    fileInputStream.close();
                    byteArrayOutputStream.close();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (Throwable th) {
            cs.e("PushLog", "getBytes   err :" + th);
            return null;
        }
    }

    static int d(Context context) {
        if (e == -1) {
            try {
                e = context.getPackageManager().getPackageInfo(context.getPackageName(), 16384).versionCode;
            } catch (Throwable th) {
                cs.e("PushLog", th.getMessage());
            }
        }
        return e;
    }

    static HashMap d(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            HashMap hashMap = new HashMap();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap.put(next, jSONObject.get(next).toString());
            }
            return hashMap;
        } catch (Throwable th) {
            cs.e("PushLog", "getMapFromJsonString error !");
            return null;
        }
    }

    static void d(Context context, String str) {
        try {
            Intent intent = new Intent();
            intent.setClassName(str, PushService.class.getName());
            context.startService(intent);
        } catch (Throwable th) {
            cs.e("PushLog", "restart service error: " + th.getMessage());
        }
    }

    static List e(Context context) {
        PackageManager packageManager = context.getPackageManager();
        HashSet hashSet = new HashSet();
        for (ResolveInfo resolveInfo : packageManager.queryBroadcastReceivers(new Intent(dc.N), 0)) {
            hashSet.add(resolveInfo.activityInfo.packageName);
        }
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(hashSet);
        return arrayList;
    }

    static boolean e(Context context, String str) {
        boolean z;
        List<ActivityManager.RunningServiceInfo> runningServices = ((ActivityManager) context.getSystemService("activity")).getRunningServices(Integer.MAX_VALUE);
        if (runningServices.size() <= 0) {
            return false;
        }
        String str2 = context.getPackageName() + ":push";
        Iterator<ActivityManager.RunningServiceInfo> it = runningServices.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = false;
                break;
            }
            ActivityManager.RunningServiceInfo next = it.next();
            if (next.service.getClassName().equals(str) && next.process.equals(str2)) {
                z = true;
                break;
            }
        }
        return z;
    }

    static String f(Context context) {
        String displayCountry = context.getResources().getConfiguration().locale.getDisplayCountry();
        return ("中国".equals(displayCountry) || "ZH".equals(displayCountry) || "zh".equals(displayCountry)) ? "CN" : displayCountry;
    }

    static void f(Context context, String str) {
        try {
            cz.putCodeContent(context);
            Intent intent = new Intent(context, PushService.class);
            if (!cp.a(str)) {
                intent.putExtra(dc.y, str);
            }
            context.startService(intent);
        } catch (Throwable th) {
            cs.e("PushLog", "start service err" + th.toString());
        }
    }
}
