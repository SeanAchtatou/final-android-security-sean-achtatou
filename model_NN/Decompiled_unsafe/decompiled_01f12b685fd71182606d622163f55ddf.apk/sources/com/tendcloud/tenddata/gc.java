package com.tendcloud.tenddata;

import android.os.SystemClock;
import com.tendcloud.tenddata.gd;
import java.util.HashMap;
import java.util.UUID;

class gc {
    private static volatile gc a = null;
    private HashMap b = new HashMap();
    private String c = null;
    private el d = gd.b.a(1);
    private boolean e = false;

    static {
        try {
            gp.a().register(a());
        } catch (Throwable th) {
        }
    }

    private gc() {
    }

    public static gc a() {
        if (a == null) {
            synchronized (gc.class) {
                if (a == null) {
                    a = new gc();
                }
            }
        }
        return a;
    }

    private final void a(long j, String str, String str2) {
        gd.c = true;
        if (!this.b.containsKey(str)) {
            if (str2 == null || str2.isEmpty()) {
                ev.a("onPageStart being called!, pagename: " + str);
            } else {
                ev.a("onPageStart being called!, pagename: " + str + ", refer: " + str2);
            }
            if (str != null) {
                ew.c(j);
                ab.w = this.d.a(ew.b(), str, j, 0, str2, SystemClock.elapsedRealtime());
                if (!this.b.containsKey(str)) {
                    this.b.put(str, Long.valueOf(ab.w));
                }
            }
        }
    }

    private final void a(String str) {
        if (str != null && !str.trim().isEmpty()) {
            long h = ew.h() - ew.f();
            if (h < 500) {
                h = -1000;
            }
            this.d.a(str, ((int) h) / 1000);
        }
    }

    private final void b() {
        aa.a().a(true);
    }

    private final void b(long j) {
        long j2 = 0;
        a(ew.b());
        String uuid = UUID.randomUUID().toString();
        long h = ew.h();
        long j3 = j - h;
        if (0 != h) {
            j2 = j3;
        }
        int i = (ab.mContext == null || !br.c(ab.mContext)) ? -1 : 1;
        ew.b(uuid);
        ew.a(j);
        ev.a(String.format("sessionId: %s, status: %s", uuid, String.valueOf(this.d.a(uuid, j, j2, i))));
    }

    private final void b(String str) {
        if (this.b.containsKey(str)) {
            long longValue = c(str).longValue();
            if (longValue != -1) {
                this.d.a(longValue, SystemClock.elapsedRealtime());
            }
            if (this.e) {
                ew.c(str);
            }
            this.c = str;
        }
    }

    private final void b(HashMap hashMap) {
        long f = ew.f();
        long h = ew.h();
        if (h <= f) {
            h = f;
        }
        long longValue = Long.valueOf(String.valueOf(hashMap.get("occurTime"))).longValue();
        if (longValue - h > ab.y) {
            a(longValue);
        } else {
            ev.a("[Session] - Same session as before!");
        }
        aa.a().a(true);
    }

    private Long c(String str) {
        if (!this.b.containsKey(str)) {
            return Long.valueOf(ab.w);
        }
        Long l = (Long) this.b.get(str);
        this.b.remove(str);
        return l;
    }

    private final void c(HashMap hashMap) {
        String valueOf = String.valueOf(hashMap.get("pageName"));
        if (hashMap.containsKey("sessionEnd")) {
            a(ew.b());
            aa.a().a(true);
            return;
        }
        ew.d(Long.valueOf(String.valueOf(hashMap.get("occurTime"))).longValue());
        ew.c(valueOf);
        this.e = false;
        this.c = null;
    }

    /* access modifiers changed from: package-private */
    public void a(long j) {
        ev.a("[Session] - New session!");
        b(j);
        ew.c("");
        this.e = true;
    }

    /* access modifiers changed from: package-private */
    public void a(HashMap hashMap) {
        try {
            switch (Integer.parseInt(String.valueOf(hashMap.get("apiType")))) {
                case 1:
                    b();
                    return;
                case 2:
                    if (Boolean.parseBoolean(hashMap.get("isPageOrSession").toString())) {
                        a(Long.valueOf(String.valueOf(hashMap.get("occurTime"))).longValue(), String.valueOf(hashMap.get("pageName")), this.c == null ? ew.e() : this.c);
                        return;
                    } else {
                        b(hashMap);
                        return;
                    }
                case 3:
                    if (Boolean.parseBoolean(hashMap.get("isPageOrSession").toString())) {
                        b(String.valueOf(hashMap.get("pageName")));
                        return;
                    } else {
                        c(hashMap);
                        return;
                    }
                case 4:
                case 5:
                default:
                    return;
                case 6:
                    a(Long.valueOf(String.valueOf(hashMap.get("occurTime"))).longValue(), String.valueOf(hashMap.get("pageName")), this.c == null ? ew.e() : this.c);
                    return;
                case 7:
                    b(String.valueOf(hashMap.get("pageName")));
                    return;
            }
        } catch (Throwable th) {
        }
    }

    public final void onTDEBEventSession(gd.a aVar) {
        if (aVar != null && aVar.a != null) {
            int parseInt = Integer.parseInt(String.valueOf(aVar.a.get("apiType")));
            if (parseInt == 1 || parseInt == 2 || parseInt == 3 || parseInt == 6 || parseInt == 7) {
                aVar.a.put("controller", a());
                if (!String.valueOf(aVar.a.get("occurTime")).trim().isEmpty()) {
                    a(aVar.a);
                }
            }
        }
    }
}
