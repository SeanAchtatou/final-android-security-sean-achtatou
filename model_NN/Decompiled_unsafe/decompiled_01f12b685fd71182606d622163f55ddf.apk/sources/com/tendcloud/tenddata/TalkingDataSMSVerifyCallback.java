package com.tendcloud.tenddata;

public interface TalkingDataSMSVerifyCallback {
    void onVerifyFailed(int i, String str);

    void onVerifySucc(String str);
}
