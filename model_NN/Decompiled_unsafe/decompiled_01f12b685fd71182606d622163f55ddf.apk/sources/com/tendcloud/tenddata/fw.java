package com.tendcloud.tenddata;

class fw implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ TalkingDataSMSVerifyCallback b;
    final /* synthetic */ int c;
    final /* synthetic */ String d;
    final /* synthetic */ fu e;

    fw(fu fuVar, String str, TalkingDataSMSVerifyCallback talkingDataSMSVerifyCallback, int i, String str2) {
        this.e = fuVar;
        this.a = str;
        this.b = talkingDataSMSVerifyCallback;
        this.c = i;
        this.d = str2;
    }

    public void run() {
        if (this.a.equals("verify") && this.b != null) {
            if (this.c == 200) {
                this.b.onVerifySucc(this.d);
            } else {
                this.b.onVerifyFailed(this.c, this.d);
            }
        }
    }
}
