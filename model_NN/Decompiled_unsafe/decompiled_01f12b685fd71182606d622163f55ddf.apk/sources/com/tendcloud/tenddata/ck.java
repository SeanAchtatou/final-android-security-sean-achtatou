package com.tendcloud.tenddata;

final class ck {
    private static final int j = 1073741824;
    final cm a = new cm();
    final int[] b = new int[4];
    int c;
    int d;
    int e;
    boolean f;
    boolean g;
    int h;
    int i;

    ck() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.c = j;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4) {
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.f = false;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, int i4, int i5, int i6) {
        this.c = i2;
        this.d = i3 + i5 + 1;
        this.e = i6;
        this.f = true;
        this.g = true;
        this.h = i3;
        this.i = i4;
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, int i3, int i4) {
        this.c = i2;
        this.d = i3 + 1;
        this.e = i4;
        this.f = true;
        this.g = false;
    }
}
