package com.tendcloud.tenddata;

import android.content.Context;
import com.tendcloud.tenddata.gd;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

class fl {
    private static volatile fl a = null;
    private el b = gd.b.a(1);

    static {
        try {
            gp.a().register(a());
        } catch (Throwable th) {
        }
    }

    private fl() {
    }

    public static fl a() {
        if (a == null) {
            synchronized (fl.class) {
                if (a == null) {
                    a = new fl();
                }
            }
        }
        return a;
    }

    private final void a(String str, String str2, String str3, Map map) {
        if (ew.b() != null && !ew.b().isEmpty()) {
            this.b.a(ew.b(), str, str2, Long.valueOf(str3).longValue(), map);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(HashMap hashMap) {
        if (!hashMap.containsKey("map") || !(hashMap.get("map") instanceof Map)) {
            a(String.valueOf(hashMap.get("eventId")), String.valueOf(hashMap.get("eventLabel")), String.valueOf(hashMap.get("occurTime")), null);
        } else {
            a(String.valueOf(hashMap.get("eventId")), String.valueOf(hashMap.get("eventLabel")), String.valueOf(hashMap.get("occurTime")), (Map) hashMap.get("map"));
        }
    }

    public final void onTDEBEventAppEvent(gd.a aVar) {
        if (aVar != null && aVar.a != null && Integer.parseInt(String.valueOf(aVar.a.get("apiType"))) == 4) {
            if (!gd.b) {
                Object obj = aVar.a.get("context");
                if (obj != null && (obj instanceof Context)) {
                    gd.a((Context) obj, (String) null, (String) null);
                } else {
                    return;
                }
            }
            TreeMap treeMap = new TreeMap();
            String valueOf = String.valueOf(aVar.a.get("eventId"));
            if (ab.a.size() > 0 && valueOf != null && !valueOf.startsWith("__")) {
                treeMap.putAll(ab.a);
            }
            Object obj2 = aVar.a.get("map");
            if (obj2 != null && (obj2 instanceof Map)) {
                treeMap.putAll((Map) obj2);
            }
            aVar.a.put("controller", a());
            aVar.a.put("map", treeMap);
            a(aVar.a);
        }
    }
}
