package com.tendcloud.tenddata;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ba {
    public static final int a = 1;
    public static final int b = 2;
    public static final int c = 3;
    public static final int d = 4;
    public static final int e = 5;
    public static final int f = 6;
    public static final int g = 7;
    public static final int h = 8;
    public static final int i = 9;
    public static final int j = 10;
    public static final int k = 11;
    public static final int l = 12;
    public static final int m = 13;
    public static final int n = 14;
    public static final int o = 15;
    public static final int p = 16;
    public static final int q = 17;
    public static final int r = 18;
    protected final int s;
    protected final Class t;
    public final int u;
    protected final boolean v;

    static class a extends ba {
        private final int w;
        private final int x;

        public a(int i, Class cls, int i2, boolean z, int i3, int i4) {
            super(i, cls, i2, z);
            this.w = i3;
            this.x = i4;
        }

        private int d(Object obj) {
            int i = 0;
            int length = Array.getLength(obj);
            switch (this.s) {
                case 1:
                case 6:
                case 16:
                    return length * 8;
                case 2:
                case 7:
                case 15:
                    return length * 4;
                case 3:
                    int i2 = 0;
                    while (i2 < length) {
                        int b = ay.b(Array.getLong(obj, i2)) + i;
                        i2++;
                        i = b;
                    }
                    return i;
                case 4:
                    int i3 = 0;
                    while (i3 < length) {
                        int a = ay.a(Array.getLong(obj, i3)) + i;
                        i3++;
                        i = a;
                    }
                    return i;
                case 5:
                    int i4 = 0;
                    while (i4 < length) {
                        int a2 = ay.a(Array.getInt(obj, i4)) + i;
                        i4++;
                        i = a2;
                    }
                    return i;
                case 8:
                    return length;
                case 9:
                case 10:
                case 11:
                case 12:
                default:
                    throw new IllegalArgumentException("Unexpected non-packable type " + this.s);
                case 13:
                    int i5 = 0;
                    while (i5 < length) {
                        int c = ay.c(Array.getInt(obj, i5)) + i;
                        i5++;
                        i = c;
                    }
                    return i;
                case 14:
                    int i6 = 0;
                    while (i6 < length) {
                        int d = ay.d(Array.getInt(obj, i6)) + i;
                        i6++;
                        i = d;
                    }
                    return i;
                case 17:
                    int i7 = 0;
                    while (i7 < length) {
                        int f = ay.f(Array.getInt(obj, i7)) + i;
                        i7++;
                        i = f;
                    }
                    return i;
                case 18:
                    int i8 = 0;
                    while (i8 < length) {
                        int e = ay.e(Array.getLong(obj, i8)) + i;
                        i8++;
                        i = e;
                    }
                    return i;
            }
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* access modifiers changed from: protected */
        public Object a(ax axVar) {
            try {
                switch (this.s) {
                    case 1:
                        return Double.valueOf(axVar.c());
                    case 2:
                        return Float.valueOf(axVar.d());
                    case 3:
                        return Long.valueOf(axVar.f());
                    case 4:
                        return Long.valueOf(axVar.e());
                    case 5:
                        return Integer.valueOf(axVar.g());
                    case 6:
                        return Long.valueOf(axVar.h());
                    case 7:
                        return Integer.valueOf(axVar.i());
                    case 8:
                        return Boolean.valueOf(axVar.j());
                    case 9:
                        return axVar.k();
                    case 10:
                    case 11:
                    default:
                        throw new IllegalArgumentException("Unknown type " + this.s);
                    case 12:
                        return axVar.l();
                    case 13:
                        return Integer.valueOf(axVar.m());
                    case 14:
                        return Integer.valueOf(axVar.n());
                    case 15:
                        return Integer.valueOf(axVar.o());
                    case 16:
                        return Long.valueOf(axVar.p());
                    case 17:
                        return Integer.valueOf(axVar.q());
                    case 18:
                        return Long.valueOf(axVar.r());
                }
            } catch (IOException e) {
                throw new IllegalArgumentException("Error reading extension field", e);
            }
            throw new IllegalArgumentException("Error reading extension field", e);
        }

        /* access modifiers changed from: protected */
        public void a(bh bhVar, List list) {
            if (bhVar.a == this.w) {
                list.add(a(ax.a(bhVar.b)));
                return;
            }
            ax a = ax.a(bhVar.b);
            try {
                a.c(a.s());
                while (!a.w()) {
                    list.add(a(a));
                }
            } catch (IOException e) {
                throw new IllegalArgumentException("Error reading extension field", e);
            }
        }

        /* access modifiers changed from: protected */
        public int b(Object obj) {
            if (this.u == this.w) {
                return ba.super.b(obj);
            }
            if (this.u == this.x) {
                int d = d(obj);
                return d + ay.h(d) + ay.h(this.u);
            }
            throw new IllegalArgumentException("Unexpected repeated extension tag " + this.u + ", unequal to both non-packed variant " + this.w + " and packed variant " + this.x);
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* access modifiers changed from: protected */
        public final void b(Object obj, ay ayVar) {
            try {
                ayVar.writeRawVarint32(this.u);
                switch (this.s) {
                    case 1:
                        ayVar.writeDoubleNoTag(((Double) obj).doubleValue());
                        return;
                    case 2:
                        ayVar.writeFloatNoTag(((Float) obj).floatValue());
                        return;
                    case 3:
                        ayVar.writeInt64NoTag(((Long) obj).longValue());
                        return;
                    case 4:
                        ayVar.writeUInt64NoTag(((Long) obj).longValue());
                        return;
                    case 5:
                        ayVar.writeInt32NoTag(((Integer) obj).intValue());
                        return;
                    case 6:
                        ayVar.writeFixed64NoTag(((Long) obj).longValue());
                        return;
                    case 7:
                        ayVar.writeFixed32NoTag(((Integer) obj).intValue());
                        return;
                    case 8:
                        ayVar.writeBoolNoTag(((Boolean) obj).booleanValue());
                        return;
                    case 9:
                        ayVar.writeStringNoTag((String) obj);
                        return;
                    case 10:
                    case 11:
                    default:
                        throw new IllegalArgumentException("Unknown type " + this.s);
                    case 12:
                        ayVar.writeBytesNoTag((byte[]) obj);
                        return;
                    case 13:
                        ayVar.writeUInt32NoTag(((Integer) obj).intValue());
                        return;
                    case 14:
                        ayVar.writeEnumNoTag(((Integer) obj).intValue());
                        return;
                    case 15:
                        ayVar.writeSFixed32NoTag(((Integer) obj).intValue());
                        return;
                    case 16:
                        ayVar.writeSFixed64NoTag(((Long) obj).longValue());
                        return;
                    case 17:
                        ayVar.writeSInt32NoTag(((Integer) obj).intValue());
                        return;
                    case 18:
                        ayVar.writeSInt64NoTag(((Long) obj).longValue());
                        return;
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
            throw new IllegalStateException(e);
        }

        /* access modifiers changed from: protected */
        public final int c(Object obj) {
            int b = bi.b(this.u);
            switch (this.s) {
                case 1:
                    return ay.a(b, ((Double) obj).doubleValue());
                case 2:
                    return ay.a(b, ((Float) obj).floatValue());
                case 3:
                    return ay.c(b, ((Long) obj).longValue());
                case 4:
                    return ay.b(b, ((Long) obj).longValue());
                case 5:
                    return ay.b(b, ((Integer) obj).intValue());
                case 6:
                    return ay.d(b, ((Long) obj).longValue());
                case 7:
                    return ay.c(b, ((Integer) obj).intValue());
                case 8:
                    return ay.a(b, ((Boolean) obj).booleanValue());
                case 9:
                    return ay.b(b, (String) obj);
                case 10:
                case 11:
                default:
                    throw new IllegalArgumentException("Unknown type " + this.s);
                case 12:
                    return ay.b(b, (byte[]) obj);
                case 13:
                    return ay.d(b, ((Integer) obj).intValue());
                case 14:
                    return ay.e(b, ((Integer) obj).intValue());
                case 15:
                    return ay.f(b, ((Integer) obj).intValue());
                case 16:
                    return ay.e(b, ((Long) obj).longValue());
                case 17:
                    return ay.g(b, ((Integer) obj).intValue());
                case 18:
                    return ay.f(b, ((Long) obj).longValue());
            }
        }

        /* access modifiers changed from: protected */
        public void c(Object obj, ay ayVar) {
            int i = 0;
            if (this.u == this.w) {
                ba.super.c(obj, ayVar);
            } else if (this.u == this.x) {
                int length = Array.getLength(obj);
                int d = d(obj);
                try {
                    ayVar.writeRawVarint32(this.u);
                    ayVar.writeRawVarint32(d);
                    switch (this.s) {
                        case 1:
                            while (i < length) {
                                ayVar.writeDoubleNoTag(Array.getDouble(obj, i));
                                i++;
                            }
                            return;
                        case 2:
                            while (i < length) {
                                ayVar.writeFloatNoTag(Array.getFloat(obj, i));
                                i++;
                            }
                            return;
                        case 3:
                            while (i < length) {
                                ayVar.writeInt64NoTag(Array.getLong(obj, i));
                                i++;
                            }
                            return;
                        case 4:
                            while (i < length) {
                                ayVar.writeUInt64NoTag(Array.getLong(obj, i));
                                i++;
                            }
                            return;
                        case 5:
                            while (i < length) {
                                ayVar.writeInt32NoTag(Array.getInt(obj, i));
                                i++;
                            }
                            return;
                        case 6:
                            while (i < length) {
                                ayVar.writeFixed64NoTag(Array.getLong(obj, i));
                                i++;
                            }
                            return;
                        case 7:
                            while (i < length) {
                                ayVar.writeFixed32NoTag(Array.getInt(obj, i));
                                i++;
                            }
                            return;
                        case 8:
                            while (i < length) {
                                ayVar.writeBoolNoTag(Array.getBoolean(obj, i));
                                i++;
                            }
                            return;
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                        default:
                            throw new IllegalArgumentException("Unpackable type " + this.s);
                        case 13:
                            while (i < length) {
                                ayVar.writeUInt32NoTag(Array.getInt(obj, i));
                                i++;
                            }
                            return;
                        case 14:
                            while (i < length) {
                                ayVar.writeEnumNoTag(Array.getInt(obj, i));
                                i++;
                            }
                            return;
                        case 15:
                            while (i < length) {
                                ayVar.writeSFixed32NoTag(Array.getInt(obj, i));
                                i++;
                            }
                            return;
                        case 16:
                            while (i < length) {
                                ayVar.writeSFixed64NoTag(Array.getLong(obj, i));
                                i++;
                            }
                            return;
                        case 17:
                            while (i < length) {
                                ayVar.writeSInt32NoTag(Array.getInt(obj, i));
                                i++;
                            }
                            return;
                        case 18:
                            while (i < length) {
                                ayVar.writeSInt64NoTag(Array.getLong(obj, i));
                                i++;
                            }
                            return;
                    }
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            } else {
                throw new IllegalArgumentException("Unexpected repeated extension tag " + this.u + ", unequal to both non-packed variant " + this.w + " and packed variant " + this.x);
            }
        }
    }

    private ba(int i2, Class cls, int i3, boolean z) {
        this.s = i2;
        this.t = cls;
        this.u = i3;
        this.v = z;
    }

    @Deprecated
    public static ba a(int i2, Class cls, int i3) {
        return new ba(i2, cls, i3, false);
    }

    public static ba a(int i2, Class cls, long j2) {
        return new ba(i2, cls, (int) j2, false);
    }

    public static ba a(int i2, Class cls, long j2, long j3, long j4) {
        return new a(i2, cls, (int) j2, true, (int) j3, (int) j4);
    }

    public static ba b(int i2, Class cls, long j2) {
        return new ba(i2, cls, (int) j2, true);
    }

    private Object b(List list) {
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < list.size(); i2++) {
            bh bhVar = (bh) list.get(i2);
            if (bhVar.b.length != 0) {
                a(bhVar, arrayList);
            }
        }
        int size = arrayList.size();
        if (size == 0) {
            return null;
        }
        Object cast = this.t.cast(Array.newInstance(this.t.getComponentType(), size));
        for (int i3 = 0; i3 < size; i3++) {
            Array.set(cast, i3, arrayList.get(i3));
        }
        return cast;
    }

    public static ba c(int i2, Class cls, long j2) {
        return new a(i2, cls, (int) j2, false, 0, 0);
    }

    private Object c(List list) {
        if (list.isEmpty()) {
            return null;
        }
        return this.t.cast(a(ax.a(((bh) list.get(list.size() - 1)).b)));
    }

    /* access modifiers changed from: package-private */
    public int a(Object obj) {
        return this.v ? b(obj) : c(obj);
    }

    /* access modifiers changed from: protected */
    public Object a(ax axVar) {
        Class<?> componentType = this.v ? this.t.getComponentType() : this.t;
        try {
            switch (this.s) {
                case 10:
                    bf bfVar = (bf) componentType.newInstance();
                    axVar.a(bfVar, bi.b(this.u));
                    return bfVar;
                case 11:
                    bf bfVar2 = (bf) componentType.newInstance();
                    axVar.readMessage(bfVar2);
                    return bfVar2;
                default:
                    throw new IllegalArgumentException("Unknown type " + this.s);
            }
        } catch (InstantiationException e2) {
            throw new IllegalArgumentException("Error creating instance of class " + componentType, e2);
        } catch (IllegalAccessException e3) {
            throw new IllegalArgumentException("Error creating instance of class " + componentType, e3);
        } catch (IOException e4) {
            throw new IllegalArgumentException("Error reading extension field", e4);
        }
    }

    /* access modifiers changed from: package-private */
    public final Object a(List list) {
        if (list == null) {
            return null;
        }
        return this.v ? b(list) : c(list);
    }

    /* access modifiers changed from: protected */
    public void a(bh bhVar, List list) {
        list.add(a(ax.a(bhVar.b)));
    }

    /* access modifiers changed from: package-private */
    public void a(Object obj, ay ayVar) {
        if (this.v) {
            c(obj, ayVar);
        } else {
            b(obj, ayVar);
        }
    }

    /* access modifiers changed from: protected */
    public int b(Object obj) {
        int i2 = 0;
        int length = Array.getLength(obj);
        for (int i3 = 0; i3 < length; i3++) {
            if (Array.get(obj, i3) != null) {
                i2 += c(Array.get(obj, i3));
            }
        }
        return i2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void b(Object obj, ay ayVar) {
        try {
            ayVar.writeRawVarint32(this.u);
            switch (this.s) {
                case 10:
                    int b2 = bi.b(this.u);
                    ayVar.writeGroupNoTag((bf) obj);
                    ayVar.h(b2, 4);
                    return;
                case 11:
                    ayVar.writeMessageNoTag((bf) obj);
                    return;
                default:
                    throw new IllegalArgumentException("Unknown type " + this.s);
            }
        } catch (IOException e2) {
            throw new IllegalStateException(e2);
        }
        throw new IllegalStateException(e2);
    }

    /* access modifiers changed from: protected */
    public int c(Object obj) {
        int b2 = bi.b(this.u);
        switch (this.s) {
            case 10:
                return ay.a(b2, (bf) obj);
            case 11:
                return ay.b(b2, (bf) obj);
            default:
                throw new IllegalArgumentException("Unknown type " + this.s);
        }
    }

    /* access modifiers changed from: protected */
    public void c(Object obj, ay ayVar) {
        int length = Array.getLength(obj);
        for (int i2 = 0; i2 < length; i2++) {
            Object obj2 = Array.get(obj, i2);
            if (obj2 != null) {
                b(obj2, ayVar);
            }
        }
    }
}
