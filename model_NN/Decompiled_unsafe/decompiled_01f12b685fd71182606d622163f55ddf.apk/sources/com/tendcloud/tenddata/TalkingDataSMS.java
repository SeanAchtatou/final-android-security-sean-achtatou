package com.tendcloud.tenddata;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.wali.gamecenter.report.ReportOrigin;

public final class TalkingDataSMS {
    static final String a = "TalkingDataSDK";
    static Context b = null;
    static Handler c = null;

    public static final void applyAuthCode(String str, String str2, TalkingDataSMSApplyCallback talkingDataSMSApplyCallback) {
        reapplyAuthCode(str, str2, null, talkingDataSMSApplyCallback);
    }

    public static void init(Context context, String str, String str2) {
        Log.d(a, "TalkingData SMS SDK version:1.0.0");
        if (str == null || str.equals("")) {
            Log.e(a, "The parameter of appId should not be null.");
            return;
        }
        Log.d(a, "init appId:" + str);
        if (str2 == null || str2.equals("")) {
            Log.e(a, "The parameter of secretId should not be null");
            return;
        }
        try {
            fx.i = str;
            fx.j = str2;
            b = context;
            c = new Handler(context.getMainLooper());
            fu fuVar = new fu();
            fuVar.a.put(dc.Y, "init");
            fuVar.a.put(ReportOrigin.ORIGIN_CATEGORY, "SMS");
            Message obtain = Message.obtain();
            obtain.what = 101;
            obtain.obj = fuVar;
            fy.a().sendMessage(obtain);
        } catch (Throwable th) {
        }
    }

    public static final void reapplyAuthCode(String str, String str2, String str3, TalkingDataSMSApplyCallback talkingDataSMSApplyCallback) {
        if (str == null || str.equals("")) {
            Log.e(a, "The parameter of countryCode should not be null.");
        } else if (str2 == null || str2.equals("")) {
            Log.e(a, "The parameter of mobile should not be null");
        } else {
            try {
                fu fuVar = new fu();
                fuVar.a.put("countryCode", str);
                fuVar.a.put("mobile", str2);
                fuVar.a.put(dc.Y, "apply");
                fuVar.a.put("transactionId", str3);
                fuVar.a.put("callback", talkingDataSMSApplyCallback);
                Message obtain = Message.obtain();
                obtain.what = 102;
                obtain.obj = fuVar;
                fy.a().sendMessage(obtain);
            } catch (Throwable th) {
            }
        }
    }

    public static final void verifyAuthCode(String str, String str2, String str3, TalkingDataSMSVerifyCallback talkingDataSMSVerifyCallback) {
        if (str == null || str.equals("")) {
            Log.e(a, "The parameter of countryCode should not be null.");
        } else if (str2 == null || str2.equals("")) {
            Log.e(a, "The parameter of mobile should not be null");
        } else if (str3 == null || str3.equals("")) {
            Log.e(a, "The parameter of securityCode should not be null");
        } else {
            try {
                fu fuVar = new fu();
                fuVar.a.put("countryCode", str);
                fuVar.a.put("mobile", str2);
                fuVar.a.put("securityCode", str3);
                fuVar.a.put(dc.Y, "verify");
                fuVar.a.put("callback", talkingDataSMSVerifyCallback);
                Message obtain = Message.obtain();
                obtain.what = 102;
                obtain.obj = fuVar;
                fy.a().sendMessage(obtain);
            } catch (Throwable th) {
            }
        }
    }
}
