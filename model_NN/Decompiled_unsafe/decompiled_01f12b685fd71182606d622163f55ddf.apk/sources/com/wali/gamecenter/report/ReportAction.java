package com.wali.gamecenter.report;

public interface ReportAction {
    public static final String ADS_REPORT = "adsreport";
    public static final String DOWNLOAD_TASK = "downloadtask";
    public static final String GAMECENTER = "gamecenter";
    public static final String XM_CLIENT = "xm_client";
    public static final String XM_INSTALL = "xm_install";
}
