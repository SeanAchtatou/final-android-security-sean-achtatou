package com.wali.gamecenter.report;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.wali.gamecenter.report.utils.Base64;
import com.wali.gamecenter.report.utils.ReportUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import org.json.JSONException;
import org.json.JSONObject;

public class Report implements Parcelable {
    public static final Parcelable.Creator<Report> CREATOR = new Parcelable.Creator<Report>() {
        public Report createFromParcel(Parcel parcel) {
            return new Report(parcel);
        }

        public Report[] newArray(int i) {
            return new Report[i];
        }
    };
    private String action;
    private String adsCv;
    private String adsId;
    private String adsType;
    private String cid;
    private String client;
    private String curPage;
    private String curPageId;
    private String curPagePkgName;
    private String from;
    private String fromId;
    private String fromLabel;
    private String gamecenterVersion;
    private String imei;
    private String level;
    boolean mHasCallCreate = false;
    private String moduleId;
    private String origin;
    private String position;
    private String pp1;
    private String pp2;
    private String tm;
    private String trace;
    private String tt;
    private ReportType type;

    public class Builder {
        ReportParams P = new ReportParams();

        public Report create() {
            Report report = new Report();
            report.apply(this.P);
            return report;
        }

        public Builder setAction(String str) {
            this.P.action = str;
            return this;
        }

        public Builder setAdsCv(String str) {
            this.P.adsCv = str;
            return this;
        }

        public Builder setAdsId(String str) {
            this.P.adsId = str;
            return this;
        }

        public Builder setAdsType(String str) {
            this.P.adsType = str;
            return this;
        }

        public Builder setCid(String str) {
            this.P.cid = str;
            return this;
        }

        public Builder setClient(String str) {
            this.P.client = str;
            return this;
        }

        public Builder setCurPage(String str) {
            this.P.curPage = str;
            return this;
        }

        public Builder setCurPageId(String str) {
            this.P.curPageId = str;
            return this;
        }

        public Builder setCurPagePkgName(String str) {
            this.P.curPagePkgName = str;
            return this;
        }

        public Builder setDownloadMode(String str) {
            this.P.tt = str;
            return this;
        }

        public Builder setFrom(String str) {
            this.P.from = str;
            return this;
        }

        public Builder setFromId(String str) {
            this.P.fromId = str;
            return this;
        }

        public Builder setFromLabel(String str) {
            this.P.fromLabel = str;
            return this;
        }

        public Builder setModuleId(String str) {
            this.P.moduleId = str;
            return this;
        }

        public Builder setOriginManual(String str) {
            this.P.originManual = str;
            return this;
        }

        public Builder setPosition(String str) {
            this.P.position = str;
            return this;
        }

        public Builder setTrace(String str) {
            this.P.trace = str;
            return this;
        }

        public Builder setType(ReportType reportType) {
            this.P.type = reportType;
            return this;
        }
    }

    Report() {
    }

    Report(Parcel parcel) {
        this.level = parcel.readString();
        this.action = parcel.readString();
        this.type = ReportType.fromInt(parcel.readInt());
        this.origin = parcel.readString();
        this.client = parcel.readString();
        this.gamecenterVersion = parcel.readString();
        this.pp1 = parcel.readString();
        this.pp2 = parcel.readString();
        this.imei = parcel.readString();
        this.from = parcel.readString();
        this.fromId = parcel.readString();
        this.fromLabel = parcel.readString();
        this.curPage = parcel.readString();
        this.curPageId = parcel.readString();
        this.position = parcel.readString();
        this.adsId = parcel.readString();
        this.tm = parcel.readString();
        this.tt = parcel.readString();
        this.cid = parcel.readString();
        this.adsType = parcel.readString();
        this.adsCv = parcel.readString();
        this.moduleId = parcel.readString();
    }

    public void apply(ReportParams reportParams) {
        if (reportParams.action == null) {
            this.action = ReportAction.GAMECENTER;
        } else {
            this.action = reportParams.action;
        }
        if (reportParams.type == null) {
            this.type = ReportType.STATISTICS;
        } else {
            this.type = reportParams.type;
        }
        this.client = reportParams.client;
        this.from = reportParams.from;
        this.fromId = reportParams.fromId;
        this.fromLabel = reportParams.fromLabel;
        this.curPage = reportParams.curPage;
        this.curPageId = reportParams.curPageId;
        this.position = reportParams.position;
        this.cid = reportParams.cid;
        this.tt = reportParams.tt;
        this.moduleId = reportParams.moduleId;
        this.trace = reportParams.trace;
        this.curPagePkgName = reportParams.curPagePkgName;
        if (!TextUtils.isEmpty(reportParams.originManual)) {
            this.origin = reportParams.originManual;
        }
        this.mHasCallCreate = true;
    }

    public int describeContents() {
        return 0;
    }

    public String getAction() {
        return this.action;
    }

    public String getClient() {
        return this.client;
    }

    /* access modifiers changed from: package-private */
    public String getParamsToJSON(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            if (TextUtils.isEmpty(this.action)) {
                this.action = ReportAction.GAMECENTER;
            }
            jSONObject.put("ac", this.action);
            if (!TextUtils.isEmpty(this.adsType)) {
                jSONObject.put("type", this.adsType);
            } else {
                if (this.type == null) {
                    this.type = ReportType.STATISTICS;
                }
                jSONObject.put("type", this.type.toString());
            }
            if (!TextUtils.isEmpty(this.adsId)) {
                jSONObject.put("adId", URLEncoder.encode(this.adsId, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.adsCv)) {
                jSONObject.put("cv", URLEncoder.encode(this.adsCv, "UTF-8"));
            }
            if (TextUtils.isEmpty(this.origin)) {
                this.origin = ReportOrigin.getOrigin(context);
            }
            if (TextUtils.isEmpty(this.origin)) {
                this.origin = ReportOrigin.ORIGIN_OTHER;
            }
            jSONObject.put("origin", URLEncoder.encode(this.origin, "UTF-8"));
            if (!TextUtils.isEmpty(this.client)) {
                jSONObject.put("client", this.client);
            }
            if (!TextUtils.isEmpty(this.from)) {
                jSONObject.put("from", URLEncoder.encode(this.from, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.fromId)) {
                jSONObject.put("fromId", URLEncoder.encode(this.fromId, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.fromLabel)) {
                jSONObject.put("fromLabel", URLEncoder.encode(this.fromLabel, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.curPage)) {
                if (this.curPage.startsWith("http") || this.curPage.startsWith("https")) {
                    this.curPage = Base64.encode(this.curPage.getBytes("UTF-8"));
                }
                jSONObject.put("curPage", URLEncoder.encode(this.curPage, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.curPageId)) {
                jSONObject.put("curPageId", URLEncoder.encode(this.curPageId, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.position)) {
                jSONObject.put("position", URLEncoder.encode(this.position, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.tt)) {
                jSONObject.put("tt", this.tt);
            }
            if (!TextUtils.isEmpty(this.moduleId)) {
                jSONObject.put("moduleId", this.moduleId);
            }
            if (!TextUtils.isEmpty(this.trace)) {
                jSONObject.put("trace", URLEncoder.encode(this.trace, "UTF-8"));
            }
            if (!TextUtils.isEmpty(this.curPagePkgName)) {
                jSONObject.put("curPagePkgName", URLEncoder.encode(this.curPagePkgName, "UTF-8"));
            }
            ReportBaseParams.getInstance().setBaseParamsToJSON(jSONObject);
            if (!TextUtils.isEmpty(this.cid)) {
                jSONObject.put("cid", URLEncoder.encode(this.cid, "UTF-8"));
            }
            String[] securityParameters = ReportUtils.getSecurityParameters(ReportBaseParams.getInstance().uid, this.curPageId, this.type.toString());
            if (securityParameters != null && securityParameters.length > 0) {
                jSONObject.put("pp1", securityParameters[0]);
                jSONObject.put("pp2", securityParameters[1]);
            }
            jSONObject.put("tm", Calendar.getInstance().getTimeInMillis());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        return jSONObject.toString();
    }

    /* access modifiers changed from: package-private */
    public String getParamsToString(Context context) {
        StringBuilder sb = new StringBuilder();
        try {
            if (TextUtils.isEmpty(this.action)) {
                this.action = ReportAction.GAMECENTER;
            }
            sb.append("ac=").append(this.action).append("&");
            if (!TextUtils.isEmpty(this.adsType)) {
                sb.append("type=").append(this.adsType).append("&");
            } else {
                if (this.type == null) {
                    this.type = ReportType.STATISTICS;
                }
                sb.append("type=").append(this.type.toString()).append("&");
            }
            if (!TextUtils.isEmpty(this.adsId)) {
                sb.append("adId").append(URLEncoder.encode(this.adsId, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.adsCv)) {
                sb.append("cv=").append(URLEncoder.encode(this.adsCv, "UTF-8")).append("&");
            }
            if (TextUtils.isEmpty(this.origin)) {
                this.origin = ReportOrigin.getOrigin(context);
            }
            if (TextUtils.isEmpty(this.origin)) {
                this.origin = ReportOrigin.ORIGIN_OTHER;
            }
            sb.append("origin=").append(URLEncoder.encode(this.origin, "UTF-8")).append("&");
            if (!TextUtils.isEmpty(this.client)) {
                sb.append("client=").append(this.client).append("&");
            }
            if (!TextUtils.isEmpty(this.from)) {
                sb.append("from=").append(URLEncoder.encode(this.from, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.fromId)) {
                sb.append("fromId=").append(URLEncoder.encode(this.fromId, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.fromLabel)) {
                sb.append("fromLabel=").append(URLEncoder.encode(this.fromLabel, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.curPage)) {
                if (this.curPage.startsWith("http") || this.curPage.startsWith("https")) {
                    this.curPage = Base64.encode(this.curPage.getBytes("UTF-8"));
                }
                sb.append("curPage=").append(URLEncoder.encode(this.curPage, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.curPageId)) {
                sb.append("curPageId=").append(URLEncoder.encode(this.curPageId, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.position)) {
                sb.append("position=").append(URLEncoder.encode(this.position, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.tt)) {
                sb.append("tt=").append(this.tt).append("&");
            }
            if (!TextUtils.isEmpty(this.moduleId)) {
                sb.append("moduleId=").append(this.moduleId).append("&");
            }
            if (!TextUtils.isEmpty(this.trace)) {
                sb.append("trace=").append(URLEncoder.encode(this.trace, "UTF-8")).append("&");
            }
            if (!TextUtils.isEmpty(this.curPagePkgName)) {
                sb.append("curPagePkgName=").append(URLEncoder.encode(this.curPagePkgName, "UTF-8")).append("&");
            }
            sb.append(ReportBaseParams.getInstance().getBaseParamsString(false));
            if (!TextUtils.isEmpty(this.cid)) {
                sb.append("cid=").append(this.cid).append("&");
            }
            String[] securityParameters = ReportUtils.getSecurityParameters(ReportBaseParams.getInstance().uid, this.curPageId, this.type.toString());
            if (securityParameters != null && securityParameters.length > 0) {
                sb.append("pp1=").append(securityParameters[0]).append("&");
                sb.append("pp2=").append(securityParameters[1]).append("&");
            }
            sb.append("tm=").append(Calendar.getInstance().getTimeInMillis());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public ReportType getType() {
        return this.type;
    }

    public void save() {
        ReportManager.getInstance().saveReport(this);
    }

    public void send() {
        if (!ReportManager.getInstance().isDebugging || this.mHasCallCreate) {
            ReportManager.getInstance().sendReport(this);
            return;
        }
        throw new IllegalArgumentException("have not call create method");
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.level);
        parcel.writeString(this.action);
        parcel.writeInt(this.type.ordinal());
        parcel.writeString(this.origin);
        parcel.writeString(this.client);
        parcel.writeString(this.gamecenterVersion);
        parcel.writeString(this.pp1);
        parcel.writeString(this.pp2);
        parcel.writeString(this.imei);
        parcel.writeString(this.from);
        parcel.writeString(this.fromId);
        parcel.writeString(this.fromLabel);
        parcel.writeString(this.curPage);
        parcel.writeString(this.curPageId);
        parcel.writeString(this.position);
        parcel.writeString(this.adsId);
        parcel.writeString(this.tm);
        parcel.writeString(this.tt);
        parcel.writeString(this.cid);
        parcel.writeString(this.adsType);
        parcel.writeString(this.adsCv);
        parcel.writeString(this.moduleId);
    }
}
