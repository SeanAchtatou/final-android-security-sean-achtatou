package com.wali.gamecenter.report;

public class ReplaceReportParams {
    String action;
    String client;
    String curPageid;
    String fromApp;
    String fromPkgName;
    String installtype;
    String iuid;
    String loginstatus;
    String mipackagename;
    String otherplatform;
    String packagename;
    String path;
    String pvcode;
    String type;
}
