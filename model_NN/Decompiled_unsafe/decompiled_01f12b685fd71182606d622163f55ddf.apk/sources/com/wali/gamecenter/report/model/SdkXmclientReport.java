package com.wali.gamecenter.report.model;

import android.content.Context;
import com.wali.gamecenter.report.ReportAction;

public class SdkXmclientReport extends BaseReport {
    private String CID;
    private String appid;
    private String cpChannel;
    private String cuipage;
    private String cuipageid;
    private String from;
    private String position;
    private String uid;
    private String version;

    public SdkXmclientReport(Context context) {
        super(context);
        setAc(ReportAction.XM_CLIENT);
    }

    public String getAppid() {
        return this.appid;
    }

    public String getCID() {
        return this.CID;
    }

    public String getCpChannel() {
        return this.cpChannel;
    }

    public String getCuipage() {
        return this.cuipage;
    }

    public String getCuipageid() {
        return this.cuipageid;
    }

    public String getFrom() {
        return this.from;
    }

    public String getPosition() {
        return this.position;
    }

    public String getUid() {
        return this.uid;
    }

    public String getVersion() {
        return this.version;
    }

    public void setAppid(String str) {
        this.appid = str;
    }

    public void setCID(String str) {
        this.CID = str;
    }

    public void setCpChannel(String str) {
        this.cpChannel = str;
    }

    public void setCuipage(String str) {
        this.cuipage = str;
    }

    public void setCuipageid(String str) {
        this.cuipageid = str;
    }

    public void setFrom(String str) {
        this.from = str;
    }

    public void setPosition(String str) {
        this.position = str;
    }

    public void setUid(String str) {
        this.uid = str;
    }

    public void setVersion(String str) {
        this.version = str;
    }
}
