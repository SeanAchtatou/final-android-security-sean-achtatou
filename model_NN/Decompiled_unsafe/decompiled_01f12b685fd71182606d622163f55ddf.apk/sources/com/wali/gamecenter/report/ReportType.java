package com.wali.gamecenter.report;

public enum ReportType {
    STATISTICS,
    DOWNLOAD,
    HTTP,
    H5GAME,
    VIEW,
    LOGIN,
    PAY,
    MESS,
    LINK,
    UPDATE,
    FLOATWIN,
    CUSTOM,
    BBS,
    STRATEGY,
    COUPON,
    WEBVIEW,
    PAYRESULT,
    SPEEDUP,
    JARCRASH;

    public static ReportType fromInt(int i) {
        if (i == STATISTICS.ordinal()) {
            return STATISTICS;
        }
        if (i == DOWNLOAD.ordinal()) {
            return DOWNLOAD;
        }
        if (i == HTTP.ordinal()) {
            return HTTP;
        }
        if (i == H5GAME.ordinal()) {
            return H5GAME;
        }
        if (i == VIEW.ordinal()) {
            return VIEW;
        }
        if (i == LOGIN.ordinal()) {
            return LOGIN;
        }
        if (i == PAY.ordinal()) {
            return PAY;
        }
        if (i == MESS.ordinal()) {
            return MESS;
        }
        if (i == LINK.ordinal()) {
            return LINK;
        }
        if (i == UPDATE.ordinal()) {
            return UPDATE;
        }
        if (i == FLOATWIN.ordinal()) {
            return FLOATWIN;
        }
        if (i == CUSTOM.ordinal()) {
            return CUSTOM;
        }
        if (i == LOGIN.ordinal()) {
            return LOGIN;
        }
        if (i == BBS.ordinal()) {
            return BBS;
        }
        if (i == STRATEGY.ordinal()) {
            return STRATEGY;
        }
        if (i == COUPON.ordinal()) {
            return COUPON;
        }
        if (i == WEBVIEW.ordinal()) {
            return WEBVIEW;
        }
        if (i == PAYRESULT.ordinal()) {
            return PAYRESULT;
        }
        if (i == SPEEDUP.ordinal()) {
            return SPEEDUP;
        }
        if (i == JARCRASH.ordinal()) {
            return JARCRASH;
        }
        throw new IllegalArgumentException("Create ReportType Illegal value=" + i);
    }

    public String toString() {
        return this == DOWNLOAD ? "download" : this == HTTP ? "http" : this == H5GAME ? "h5game" : this == VIEW ? "view" : this == LOGIN ? "login" : this == PAY ? "pay" : this == MESS ? "mess" : this == LINK ? "link" : this == UPDATE ? "update" : this == FLOATWIN ? "floatwin" : this == CUSTOM ? "custom" : this == BBS ? "bbs" : this == STRATEGY ? "strategy" : this == COUPON ? "coupon" : this == WEBVIEW ? "webview" : this == PAYRESULT ? "payresult" : this == SPEEDUP ? "speedup" : this == JARCRASH ? "jarcrash" : "statistics";
    }
}
