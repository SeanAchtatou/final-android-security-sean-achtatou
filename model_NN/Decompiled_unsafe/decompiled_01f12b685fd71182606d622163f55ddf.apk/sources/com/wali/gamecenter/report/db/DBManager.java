package com.wali.gamecenter.report.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.wali.gamecenter.report.db.DaoMaster;

public class DBManager {
    private static final String DB_NAME = "report2.db";
    private static DaoMaster.DevOpenHelper sDB;
    private static DaoMaster sDaoMaster;
    private static DaoSession sDaoSession;

    public static DaoSession getDaoSession() {
        return sDaoSession;
    }

    public static SQLiteDatabase getReadableDB() {
        if (sDB != null) {
            return sDB.getReadableDatabase();
        }
        return null;
    }

    public static ReportDataDao getReportDao() {
        if (getDaoSession() != null) {
            return getDaoSession().getReportDataDao();
        }
        return null;
    }

    public static SQLiteDatabase getWritableDB() {
        if (sDB != null) {
            return sDB.getWritableDatabase();
        }
        return null;
    }

    public static void init(Context context) {
        sDB = new DaoMaster.DevOpenHelper(context, DB_NAME, null);
        try {
            sDaoMaster = new DaoMaster(getWritableDB());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (sDaoMaster != null) {
            sDaoSession = sDaoMaster.newSession();
        }
    }
}
